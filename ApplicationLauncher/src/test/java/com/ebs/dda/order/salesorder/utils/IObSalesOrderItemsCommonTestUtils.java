package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class IObSalesOrderItemsCommonTestUtils extends DObSalesOrderCommonTestUtils {

  public IObSalesOrderItemsCommonTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsExecution() {

    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());

    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoicePostingDetails.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append(",")
        .append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(",")
        .append("db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(",")
        .append("db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append(",").append("db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");
    str.append(",").append("db-scripts/inventory/stock-transformation/CObStockTransformation.sql");
    str.append(",")
        .append("db-scripts/inventory/stock-transformation/DObStockTransformationScript.sql");
    str.append(",")
        .append(
            "db-scripts/inventory/store-transaction/TObStockTransformationStoreTransaction.sql");
    str.append(",")
        .append(
            "db-scripts/inventory/stock-transformation/IObStockTransformationDetailsScript.sql");
    str.append(getSalesOrderScripts());

    return str.toString();
  }

  public String getSalesOrderScripts() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCycleDates_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes.sql");
    return str.toString();
  }

  public void assertThatTheFollowingSalesOrderItemsExist(DataTable salesOrderItemsDataTable) {
    List<Map<String, String>> salesOrderItemsList =
        salesOrderItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderItemRow : salesOrderItemsList) {
      Object salesOrderCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObSalesOrderItem", constructSalesOrderItemQueryParams(salesOrderItemRow));
      assertNotNull(
          "SalesOrderItem with code "
              + salesOrderItemRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE)
              + "  Doesn't exits",
          salesOrderCode);

      salesOrderCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesOrderItemLastSalesPrice",
              constructSalesOrderItemLastSalesPriceQueryParams(salesOrderItemRow));
      assertNotNull(
          "sales Order "
              + salesOrderItemRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE)
              + " doesn't exist",
          salesOrderCode);

      salesOrderCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesOrderItemAvailableQty",
              constructSalesOrderItemAvailabilityQueryParams(salesOrderItemRow));
      assertNotNull(
          salesOrderItemRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE) + " Doesn't Exist!",
          salesOrderCode);
    }
  }

  public Object[] constructSalesOrderItemQueryParams(Map<String, String> salesOrderDataRow) {
    return new Object[] {
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE),
      Long.parseLong(salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.ITEM_ID)),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.ITEM_TYPE),
      salesOrderDataRow.get(IFeatureFileCommonKeys.ITEM),
      salesOrderDataRow.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      salesOrderDataRow.get(IFeatureFileCommonKeys.QTY),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.SALES_PRICE),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.TOTAL_AMOUNT)
    };
  }

  private Object[] constructSalesOrderItemLastSalesPriceQueryParams(
      Map<String, String> salesOrderDataRow) {
    return new Object[] {
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.LAST_SALES_PRICE).isEmpty()
          ? null
          : new BigDecimal(
              salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.LAST_SALES_PRICE)),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE),
      salesOrderDataRow.get(IFeatureFileCommonKeys.ITEM),
      salesOrderDataRow.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
    };
  }

  private Object[] constructSalesOrderItemAvailabilityQueryParams(
      Map<String, String> salesOrderDataRow) {
    return new Object[] {
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.AVAILABLE_QUANTITIES).isEmpty()
          ? null
          : salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.AVAILABLE_QUANTITIES),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE),
      salesOrderDataRow.get(IFeatureFileCommonKeys.ITEM),
      salesOrderDataRow.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
    };
  }

  public String getLockItemsSectionUrl(String salesOrderCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObSalesOrderRestURLs.COMMAND);
    lockUrl.append("/");
    lockUrl.append(salesOrderCode);
    lockUrl.append(IDObSalesOrderRestURLs.LOCK_SALES_ORDER_ITEMS_SECTION);
    return lockUrl.toString();
  }

  public String getViewSalesOrderItemsURL(String salesOrderCode) {
    StringBuilder readUrl = new StringBuilder();
    readUrl.append(IDObSalesOrderRestURLs.QUERY);
    readUrl.append("/");
    readUrl.append(salesOrderCode);
    readUrl.append(IDObSalesOrderRestURLs.VIEW_ITEMS);
    return readUrl.toString();
  }
}
