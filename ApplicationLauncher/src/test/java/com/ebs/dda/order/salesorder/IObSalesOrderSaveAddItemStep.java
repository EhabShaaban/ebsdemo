package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderSaveAddItemTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewItemsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesOrderSaveAddItemStep extends SpringBootRunner implements En {

  private IObSalesOrderSaveAddItemTestUtils utils;
  private IObSalesOrderViewItemsTestUtils viewItemsTestUtils;

  public IObSalesOrderSaveAddItemStep() {

    Given(
        "^Items section of SalesOrder with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockUrl = utils.getLockItemsSectionUrl(salesOrderCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              salesOrderCode,
              IDObSalesOrderSectionNames.ITEMS_SECTION,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME);
        });

    When(
        "^\"([^\"]*)\" saves the following new item to Items section of SalesOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String salesOrderCode, String dateTime, DataTable newItemDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.saveAddItem(cookie, salesOrderCode, newItemDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesOrder with Code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable salesOrderDataTable) -> {
          utils.assertThatSalesOrderIsUpdated(salesOrderCode, salesOrderDataTable);
        });

    Then(
        "^Items section of SalesOrder with Code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable itemsDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getViewSalesOrderItemsURL(salesOrderCode));
          userActionsTestUtils.setUserResponse(userName, response);
          viewItemsTestUtils.assertCorrectValuesAreDisplayedInItemSection(response, itemsDataTable);
        });

    Then(
        "^TaxesSummary section of SalesOrder with code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable taxesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          viewItemsTestUtils.assertThatItemTaxesResponseIsCorrect(response, taxesDataTable);
        });

    Then(
        "^SalesOrderSummary section of SalesOrder with code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String salesOrderCode, String userName, DataTable summaryDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          viewItemsTestUtils.assertThatItemSummaryResponseIsCorrect(response, summaryDataTable);
        });

    Then(
        "^the lock by \"([^\"]*)\" on Items section of SalesOrder with Code \"([^\"]*)\" is released$",
        (String userName, String salesOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesOrderCode,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesOrderSectionNames.ITEMS_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObSalesOrderSaveAddItemTestUtils(getProperties(), entityManagerDatabaseConnector);
    viewItemsTestUtils =
        new IObSalesOrderViewItemsTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Add SalesOrder Item,")) {
      databaseConnector.executeSQLScript(utils.getDbClearScriptsExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Add SalesOrder Item,")) {
      utils.unfreeze();
    }
  }
}
