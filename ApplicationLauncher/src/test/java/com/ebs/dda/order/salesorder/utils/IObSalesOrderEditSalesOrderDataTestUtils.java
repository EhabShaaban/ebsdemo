package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import java.util.Map;

public class IObSalesOrderEditSalesOrderDataTestUtils extends DObSalesOrderCommonTestUtils {

  public String SALES_ORDER_JMX_LOCK_BEAN_NAME = "SalesOrderEntityLockCommand";

  public IObSalesOrderEditSalesOrderDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");

    return str.toString();
  }

  public String getLockSalesOrderDataUrl(String code) {
    return IDObSalesOrderRestURLs.COMMAND
        + "/"
        + code
        + IDObSalesOrderRestURLs.LOCK_SALES_ORDER_DATA_SECTION;
  }

  public String getUnLockSalesOrderDataUrl(String code) {
    return IDObSalesOrderRestURLs.COMMAND
        + "/"
        + code
        + IDObSalesOrderRestURLs.UNLOCK_SALES_ORDER_SECTION;
  }
}
