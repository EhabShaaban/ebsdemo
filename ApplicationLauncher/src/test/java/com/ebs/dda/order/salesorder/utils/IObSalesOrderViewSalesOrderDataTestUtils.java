package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderDataGeneralModel;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class IObSalesOrderViewSalesOrderDataTestUtils extends DObSalesOrderCommonTestUtils {

  public IObSalesOrderViewSalesOrderDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    return str.toString();
  }

  public void assertThatTheFollowingSalesOrderDataExist(DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrderDataList =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderDataRow : salesOrderDataList) {
      Object[] salesOrderData =
          (Object[])
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getIObSalesOrderData", constructSalesOrderDataQueryParams(salesOrderDataRow));
      assertNotNull(salesOrderData);

      assertThatActualDateEqualsExpectedAndIfEmptyAssertNull(
          salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE),
          salesOrderData[1]);
    }
  }

  private Object[] constructSalesOrderDataQueryParams(Map<String, String> salesOrderDataRow) {
    return new Object[] {
      salesOrderDataRow.get(IFeatureFileCommonKeys.PAYMENT_TERMS),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.CUSTOMER_ADDRESS),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.CURRENCY_ISO),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.CONTACT_PERSON),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.CREDIT_LIMIT),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.NOTES),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE),
      salesOrderDataRow.get(IFeatureFileCommonKeys.CUSTOMER)
    };
  }

  public Response readSalesOrderData(Cookie cookie, String salesOrderCode) {
    return sendGETRequest(cookie, getViewSalesOrderDataUrl(salesOrderCode));
  }

  private String getViewSalesOrderDataUrl(String salesOrderCode) {
    return IDObSalesOrderRestURLs.QUERY
        + "/"
        + salesOrderCode
        + IDObSalesOrderRestURLs.VIEW_SALES_ORDER_DATA;
  }

  public void assertThatViewSalesOrderDataResponseIsCorrect(
      Response response, DataTable salesOrderDataTable) throws Exception {
    Map<String, String> expectedSalesOrderData =
        salesOrderDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualSalesOrderData = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(expectedSalesOrderData, actualSalesOrderData);

    mapAssertion.assertValue(
        ISalesOrderFeatureFileCommonKeys.SO_CODE, IIObSalesOrderDataGeneralModel.SALES_ORDER_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.CUSTOMER,
        IIObSalesOrderDataGeneralModel.CUSTOMER_CODE,
        IIObSalesOrderDataGeneralModel.CUSTOMER_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PAYMENT_TERMS,
        IIObSalesOrderDataGeneralModel.PAYMENT_TERM_CODE,
        IIObSalesOrderDataGeneralModel.PAYMENT_TERM_NAME);

    mapAssertion.assertLocalizedValue(
        ISalesOrderFeatureFileCommonKeys.CUSTOMER_ADDRESS,
        IIObSalesOrderDataGeneralModel.CUSTOMER_ADDRESS);

    mapAssertion.assertValue(
        ISalesOrderFeatureFileCommonKeys.CURRENCY_ISO, IIObSalesOrderDataGeneralModel.CURRENCY_ISO);

    mapAssertion.assertLocalizedValue(
        ISalesOrderFeatureFileCommonKeys.CONTACT_PERSON,
        IIObSalesOrderDataGeneralModel.CONTACT_PERSON_NAME);

    mapAssertion.assertDateTime(
        ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE,
        IIObSalesOrderDataGeneralModel.EXPECTED_DELIVERY_DATE);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.CREDIT_LIMIT, IIObSalesOrderDataGeneralModel.CREDIT_LIMIT);

    mapAssertion.assertValue(
        ISalesOrderFeatureFileCommonKeys.NOTES, IIObSalesOrderDataGeneralModel.NOTES);
  }
}
