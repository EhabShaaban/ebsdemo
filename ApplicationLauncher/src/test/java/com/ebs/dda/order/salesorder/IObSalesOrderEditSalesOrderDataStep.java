package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderEditSalesOrderDataTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class IObSalesOrderEditSalesOrderDataStep extends SpringBootRunner implements En {
  private IObSalesOrderEditSalesOrderDataTestUtils utils;

  public IObSalesOrderEditSalesOrderDataStep() {

    When(
        "^\"([^\"]*)\" requests to edit SalesOrderData section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          String lockUrl = utils.getLockSalesOrderDataUrl(salesOrderCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesOrderData section of SalesOrder with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String salesOrderCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName,
              salesOrderCode,
              IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME);
        });

    Given(
            "^\"([^\"]*)\" first opened the SalesOrderData section of SalesOrder with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String salesOrderCode) -> {
          String lockUrl = utils.getLockSalesOrderDataUrl(salesOrderCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesOrderCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesOrderData section of SalesOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String salesOrderCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesOrderCode,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION);
        });

    Given(
        "^SalesOrderData section of SalesOrder with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = utils.getLockSalesOrderDataUrl(salesOrderCode);
          Response response = userActionsTestUtils.lockSection(userName, lock_url, salesOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving SalesOrderData section of SalesOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String salesOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unLockUrl = utils.getUnLockSalesOrderDataUrl(salesOrderCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on SalesOrderData section of SalesOrder with code \"([^\"]*)\" is released$",
        (String userName, String salesOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              salesOrderCode,
              utils.SALES_ORDER_JMX_LOCK_BEAN_NAME,
              IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving SalesOrderData section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          String unLockUrl = utils.getUnLockSalesOrderDataUrl(salesOrderCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new IObSalesOrderEditSalesOrderDataTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

  @Before
  public void setup(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Request to edit Sales Order Data section")
              || contains(scenario.getName(), "Request to Cancel saving Sales Order Data section")) {
          databaseConnector.createConnection();
          databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
      }
  }

  @After
  public void after(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Request to edit Sales Order Data section")
              || contains(scenario.getName(), "Request to Cancel saving Sales Order Data section")) {
          utils.unfreeze();
          userActionsTestUtils.unlockAllSections(
                  IDObSalesOrderRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
          databaseConnector.closeConnection();
      }
  }
}
