package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewReturnDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesReturnOrderViewReturnDetailsStep extends SpringBootRunner implements En {

  IObSalesReturnOrderViewReturnDetailsTestUtils utils;

  public IObSalesReturnOrderViewReturnDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view ReturnDetails section of SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readReturnDetails(cookie, salesReturnOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of ReturnDetails section for SalesReturnOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode, String userName, DataTable returnDetailsTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertSalesReturnOrderReturnDetailsTableIsCorrect(response, returnDetailsTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObSalesReturnOrderViewReturnDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Sales Return Order ReturnDetails section")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }
}
