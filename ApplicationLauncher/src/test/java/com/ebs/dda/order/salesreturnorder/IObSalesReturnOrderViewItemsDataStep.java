package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewItemsDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesReturnOrderViewItemsDataStep extends SpringBootRunner implements En {

  private IObSalesReturnOrderViewItemsDataTestUtils utils;

  public IObSalesReturnOrderViewItemsDataStep() {
    Given(
        "^the following SalesReturnOrders have no Items:$",
        (DataTable itemsDataTable) -> {
          utils.assertThatSalesReturnOrderHasNoItems(itemsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view Items section of SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readItemsData(cookie, salesReturnOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of Items section for SalesReturnOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode, String userName, DataTable itemDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayedInItemSection(response, itemDataTable);
        });

    Then(
        "^the following values for TaxesSummary for SalesReturnOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode, String userName, DataTable taxesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemTaxesResponseIsCorrect(response, taxesDataTable);
        });
    Then(
        "^the following values for ItemsSummary for salesReturnOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode, String userName, DataTable itemSummaryDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemSummaryResponseIsCorrect(response, itemSummaryDataTable);
        });

    Then(
        "^Items section for SalesReturnOrder with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String salesReturnCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemResponseIsEmpty(response);
        });

    Given(
        "^the following SalesReturnOrders have no Taxes:$",
        (DataTable salesReturnOrdersDataTable) -> {
          utils.assertThatSalesReturnOrderHasNoTaxes(salesReturnOrdersDataTable);
        });

    Then(
        "^TaxesSummary section for SalesReturnOrder with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String salesReturnOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTaxesResponseIsEmpty(response);
        });

    Given(
        "^the following SalesReturnOrders have the following TaxDetails:$",
        (DataTable taxDetailsDataTable) -> {
          utils.assertThatTaxDetailsExist(taxDetailsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObSalesReturnOrderViewItemsDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View SalesReturnOrder Items section")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }
}
