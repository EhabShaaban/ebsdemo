package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesOrderViewGeneralDataTestUtils extends DObSalesOrderCommonTestUtils {

  public IObSalesOrderViewGeneralDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertCorrectValuesAreDisplayedInGeneralData(
      Response response, DataTable generalDataTable) throws Exception {
    Map<String, String> generalDataExpected =
        generalDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> generalDataActual = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(generalDataExpected, generalDataActual);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TYPE,
        IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_CODE,
        IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObSalesOrderGeneralModel.PURCHASE_UNIT_CODE,
        IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE,
        IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_CODE,
        IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObSalesOrderGeneralModel.CREATION_INFO_NAME);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObSalesOrderGeneralModel.USER_CODE);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObSalesOrderGeneralModel.CURRENT_STATES);

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObSalesOrderGeneralModel.CREATION_DATE);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    return str.toString();
  }
}
