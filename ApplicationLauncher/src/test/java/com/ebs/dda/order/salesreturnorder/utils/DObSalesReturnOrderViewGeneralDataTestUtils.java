package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrderGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

public class DObSalesReturnOrderViewGeneralDataTestUtils
    extends DObSalesReturnOrderCommonTestUtils {
  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[View][General Data]";

  public DObSalesReturnOrderViewGeneralDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readGeneralData(Cookie cookie, String salesReturnOrderCode) {
    return sendGETRequest(cookie, getViewGeneralDataUrl(salesReturnOrderCode));
  }

  private String getViewGeneralDataUrl(String salesReturnOrderCode) {
    return IDObSalesReturnOrderRestURLs.QUERY
        + "/"
        + salesReturnOrderCode
        + IDObSalesReturnOrderRestURLs.VIEW_GENERAL_DATA;
  }

  public void assertSalesReturnOrderGeneralDataIsCorrect(
      Response response, DataTable generalDataTable) throws Exception {
    Map<String, String> expectedGeneralData =
        generalDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualGeneralData = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedGeneralData, actualGeneralData);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObSalesReturnOrderGeneralModel.USER_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TYPE,
        IDObSalesReturnOrderGeneralModel.SALES_RETURN_ORDER_TYPE_CODE,
        IDObSalesReturnOrderGeneralModel.SALES_RETURN_ORDER_TYPE_NAME);

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObSalesReturnOrderGeneralModel.CREATION_DATE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObSalesReturnOrderGeneralModel.CREATION_INFO);

    mapAssertion.assertValue(
        IOrderFeatureFileCommonKeys.DOCUMENT_OWNER,
        IDObSalesReturnOrderGeneralModel.DOCUMENT_OWNER_NAME);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObSalesReturnOrderGeneralModel.CURRENT_STATES);
  }
}
