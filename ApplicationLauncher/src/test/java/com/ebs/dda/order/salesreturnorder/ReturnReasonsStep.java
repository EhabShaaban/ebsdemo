package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.ReturnReasonsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class ReturnReasonsStep extends SpringBootRunner implements En {

  private ReturnReasonsTestUtils utils;

  public ReturnReasonsStep() {

    Given(
        "^the following Reasons exist:$",
        (DataTable returnReasonDataTable) -> {
          utils.assertThatReturnReasonsExist(returnReasonDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all SalesReturnOrder Reasons$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllReturnReasons(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following ReturnReasons values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable returnReasonDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadAllReturnReasonsResponseIsCorrect(response, returnReasonDataTable);
        });

    Then(
        "^total number of ReturnReasons returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer totalNumberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfRecordsIsCorrect(response, totalNumberOfRecords);
        });

    And(
        "^the total number of existing ReturnReasons is (\\d+)$",
        (Long numberOfReturnReasons) -> {
          this.utils.assertThatTotalNumberOfReturnReasonsIsCorrect(numberOfReturnReasons);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new ReturnReasonsTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
