package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesOrderViewGeneralDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  IObSalesOrderViewGeneralDataTestUtils utils;

  public IObSalesOrderViewGeneralDataStep() {
    Given(
        "^the following GeneralData for SalesOrder exists:$",
        (DataTable generalDataTable) -> {
          utils.checkSalesOrderGeneralDataExists(generalDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
            Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
            StringBuilder readUrl = new StringBuilder();
            readUrl.append(IDObSalesOrderRestURLs.QUERY);
            readUrl.append("/");
            readUrl.append(salesOrderCode);
            readUrl.append(IDObSalesOrderRestURLs.GENERAL_DATA);
            Response response = utils.sendGETRequest(userCookie, String.valueOf(readUrl));
            userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of GeneralData section for SalesOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
            (String salesOrderCode, String userName, DataTable generalDataTable) -> {
                Response response = userActionsTestUtils.getUserResponse(userName);
                utils.assertCorrectValuesAreDisplayedInGeneralData(response, generalDataTable);
            });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
    utils = new IObSalesOrderViewGeneralDataTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View GeneralData section")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
        }
  }

  @After
  public void after(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "View GeneralData section")) {
          databaseConnector.closeConnection();
      }
  }
}
