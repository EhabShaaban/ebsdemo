package com.ebs.dda.order.salesorder.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/ordermanagement/salesorder/SO_Approve.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Sales_Order_Data_Auth.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Sales_Order_Data_HP.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Sales_Order_Data_Val.feature",
      "classpath:features/ordermanagement/salesorder/SO_RequestEdit_Sales_Order_Data.feature",
      "classpath:features/ordermanagement/salesorder/SO_View_SalesOrderData.feature",
      "classpath:features/ordermanagement/salesorder/SO_RequestEdit_Company_And_Store_Data.feature",
      "classpath:features/ordermanagement/salesorder/SO_AllowedActions_HomeScreen.feature",
      "classpath:features/ordermanagement/salesorder/SO_AllowedActions_ObjectScreen.feature",
      "classpath:features/ordermanagement/salesorder/SalesOrderTypes_Dropdowns.feature",
      "classpath:features/ordermanagement/salesorder/SO_Create_Auth.feature",
      "classpath:features/ordermanagement/salesorder/SO_Create_HP.feature",
      "classpath:features/ordermanagement/salesorder/SO_Create_Val.feature",
      "classpath:features/ordermanagement/salesorder/SO_ViewAll.feature",
      "classpath:features/ordermanagement/salesorder/SO_View_GeneralData.feature",
      "classpath:features/ordermanagement/salesorder/SO_View_CompanyAndStoreData.feature",
      "classpath:features/ordermanagement/salesorder/SO_Delete.feature",
      "classpath:features/ordermanagement/salesorder/SO_Item_UOM_Dropdown.feature",
      "classpath:features/ordermanagement/salesorder/SO_View_Items.feature",
      "classpath:features/ordermanagement/salesorder/SO_Delete_Items.feature",
      "classpath:features/ordermanagement/salesorder/SO_RequestAdd_Item.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Company_And_Store_Data_Auth.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Company_And_Store_Data_HP.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Company_And_Store_Data_Val.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Add_Item_Auth.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Add_Item_HP.feature",
      "classpath:features/ordermanagement/salesorder/SO_Save_Add_Item_Val.feature",
      "classpath:features/ordermanagement/salesorder/ApprovedSalesOrders_Dropdown.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.order.salesorder"},
    strict = true,
    tags = "not @Future")
public class DObSalesOrderCucumberRunner {}
