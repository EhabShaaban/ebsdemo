package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.salesresponsible.DObSalesResponsibleReadAllTestUtils;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderCreateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;

public class DObSalesOrderCreateStep extends SpringBootRunner implements En {

  private DObSalesOrderCreateTestUtils utils;
  private DObSalesResponsibleReadAllTestUtils salesResponsibleReadAllTestUtils;
  public DObSalesOrderCreateStep() {
    Given(
        "^the following SalesResponsibles exist:$",
        (DataTable salesResponsiblesDataTable) -> {
          salesResponsibleReadAllTestUtils.assertThatSalesResponsiblesExist(
              salesResponsiblesDataTable);
        });
    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    Given(
        "^Last created SalesOrder was with code \"([^\"]*)\"$",
        (String lastCreatedCode) -> {
          utils.assertThatLastCreatedCodeIsCorrect(lastCreatedCode);
        });

    When(
        "^\"([^\"]*)\" creates SalesOrder with the following values:$",
        (String userName, DataTable salesOrderDataTable) -> {
          Response response =
              utils.createSalesOrder(
                  salesOrderDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new SalesOrder is created as follows:$",
        (DataTable salesOrderDataTable) -> {
          utils.assertThatSalesOrderIsCreatedSuccessfully(salesOrderDataTable);
        });

    Then(
        "^the CompanyAndStore Section of SalesOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable companyDataTable) -> {
          utils.assertThatCompanyIsCreatedInCompanyAndStoreSection(
              salesOrderCode, companyDataTable);
        });

    Then(
        "^the SalesOrderData Section of SalesOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable iobSalesOrderDataTable) -> {
          utils.assertThatIObSalesOrderDataIsCreatedSuccessfully(
              salesOrderCode, iobSalesOrderDataTable);
        });

    Then(
        "^the following values for Taxes for SalesOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesOrderCode, DataTable taxesSummary) -> {
          utils.assertThatSalesOrderTaxesIsUpdatedAsFollows(salesOrderCode, taxesSummary);
        });

    When("^\"([^\"]*)\" requests to create SalesOrder$", (String userName) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response response = utils.requestCreateSalesOrder(cookie);
      userActionsTestUtils.setUserResponse(userName, response);
    });

  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSalesOrderCreateTestUtils(getProperties(), entityManagerDatabaseConnector);
    salesResponsibleReadAllTestUtils =
        new DObSalesResponsibleReadAllTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create SalesOrder")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create SalesOrder")) {
      utils.unfreeze();
    }
  }
}
