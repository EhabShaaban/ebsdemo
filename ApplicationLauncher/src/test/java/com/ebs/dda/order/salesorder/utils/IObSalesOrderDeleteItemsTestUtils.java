package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;

import java.util.Map;

import static org.junit.Assert.assertNull;

public class IObSalesOrderDeleteItemsTestUtils extends IObSalesOrderItemsCommonTestUtils {

  public IObSalesOrderDeleteItemsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    return super.getDbScriptsExecution();
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/inventory/stockavailability/stockavailabilityClear.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems_ViewAll.sql");
    return str.toString();
  }

  public String getSalesOrderDeleteItemsURL(String salesOrderCode, String itemId) {
    StringBuilder readUrl = new StringBuilder();
    readUrl.append(IDObSalesOrderRestURLs.COMMAND);
    readUrl.append("/");
    readUrl.append(salesOrderCode);
    readUrl.append(IDObSalesOrderRestURLs.DELETE_ITEMS);
    readUrl.append(itemId);
    return readUrl.toString();
  }

  public void assertThatSalesOrderItemIsDeletedById(String salesOrderItemId) {
    Object actualSalesOrderItem =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderItemById", Long.valueOf(salesOrderItemId));
    assertNull("Sales Order Item does not deleted successfully", actualSalesOrderItem);
  }
}
