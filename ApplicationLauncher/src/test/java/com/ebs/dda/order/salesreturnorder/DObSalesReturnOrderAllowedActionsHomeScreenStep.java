package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderCommonTestUtils;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesReturnOrderAllowedActionsHomeScreenStep extends SpringBootRunner
    implements En {

  private DObSalesReturnOrderCommonTestUtils utils;

  public DObSalesReturnOrderAllowedActionsHomeScreenStep() {
    When(
        "^\"([^\"]*)\" requests to read actions of SalesReturnOrder home screen$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IDObSalesReturnOrderRestURLs.AUTHORIZED_ACTIONS);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSalesReturnOrderCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
