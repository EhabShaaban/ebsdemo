package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderCompanyDataGeneralModel;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Map;

public class IObSalesReturnOrderViewCompanyDataTestUtils
    extends DObSalesReturnOrderCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[CompanyData]";

  public IObSalesReturnOrderViewCompanyDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readCompanyData(Cookie cookie, String salesReturnOrderCode) {
    return sendGETRequest(cookie, getViewCompanyDataUrl(salesReturnOrderCode));
  }

  private String getViewCompanyDataUrl(String salesReturnOrderCode) {
    return IDObSalesReturnOrderRestURLs.QUERY
        + "/"
        + salesReturnOrderCode
        + IDObSalesReturnOrderRestURLs.COMPANY_DATA;
  }

  public void assertThatTheFollowingSalesReturnOrdersExistWithCompanyData(
      String salesReturnOrderCode, Map<String, String> companyDataMap) {
    Object company =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getIObSalesReturnOrderCompanyData",
            getCompanyDataQueryParams(salesReturnOrderCode, companyDataMap));

    Assert.assertNotNull(
        ASSERTION_MSG
            + " company data of sales return order "
            + salesReturnOrderCode
            + " does not exist",
        company);
  }

  private Object[] getCompanyDataQueryParams(
      String salesReturnOrderCode, Map<String, String> companyDataMap) {
    return new Object[] {
      salesReturnOrderCode,
      companyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      companyDataMap.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void assertThatReadCompanyDataResponseIsCorrect(
      Response response, DataTable companyDataTable) throws Exception {

    Map<String, String> expectedCompanyDataMap =
        companyDataTable.asMaps(String.class, String.class).get(0);

    Map<String, Object> actualCompanyDataMap = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(expectedCompanyDataMap, actualCompanyDataMap);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IIObSalesReturnOrderCompanyDataGeneralModel.BUSINESS_UNIT_CODE,
        IIObSalesReturnOrderCompanyDataGeneralModel.BUSINESS_UNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        IIObSalesReturnOrderCompanyDataGeneralModel.COMPANY_CODE,
        IIObSalesReturnOrderCompanyDataGeneralModel.COMPANY_NAME);
  }
}
