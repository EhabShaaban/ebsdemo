package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.inventory.storetransaction.utils.DObStockAvailabilitiesViewAllTestUtils;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderItemUOMReadAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObSalesOrderItemUOMReadAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObSalesOrderItemUOMReadAllTestUtils utils;
  private DObStockAvailabilitiesViewAllTestUtils stockAvailabilitiesViewAllTestUtils;

  public DObSalesOrderItemUOMReadAllStep() {

    Given(
        "^the following SalesOrders with the following Company And Store exist:$",
        (DataTable salesOrderDataTable) -> {
          utils.assertThatSalesOrderWithTheFollowingCompanyAndStoreExist(salesOrderDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read UOMs for Item with code \"([^\"]*)\" of SalesOrder with code \"([^\"]*)\" in a dropdown$",
        (String userName, String itemCode, String salesOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getReadItemUOMDataUrl(salesOrderCode, itemCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following UOM values will be presented to \"([^\"]*)\" in the dropdown:$",
        (String userName, DataTable itemUOMDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatActualItemUOMAreReturned(response, itemUOMDataTable);
          utils.assertThatActualLastSalesPriceAndAvailableQuantityAreReturned(
              response, itemUOMDataTable);
        });

    Then(
        "^total number of records returned to \"([^\"]*)\" is \"(\\d+)\"$",
        (String userName, Integer uomCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatUOMResponseCountIsCorrect(response, uomCount);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObSalesOrderItemUOMReadAllTestUtils(getProperties(), entityManagerDatabaseConnector);
      stockAvailabilitiesViewAllTestUtils = new DObStockAvailabilitiesViewAllTestUtils(getProperties());
      stockAvailabilitiesViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read Item's UOMs dropdown")) {
      beforeSetup();
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read Item's UOMs dropdown")) {
      afterSetup();
    }
  }

  private void beforeSetup() throws Exception {
    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(AccountingDocumentCommonTestUtils.clearAccountingDocuments());
      databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
      hasBeenExecutedOnce = true;
    }
      databaseConnector.executeSQLScript(stockAvailabilitiesViewAllTestUtils.clearStockAvailabilities());
  }

  private void afterSetup() throws SQLException {
    databaseConnector.closeConnection();
    utils.unfreeze();
  }
}
