package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceItemsTestUtils;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderCreateTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewCompanyDataTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewReturnDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObSalesReturnOrderCreateStep extends SpringBootRunner implements En {

  private DObSalesReturnOrderCreateTestUtils utils;
  private IObSalesInvoiceItemsTestUtils salesInvoiceItemsTestUtils;

  private IObSalesReturnOrderViewCompanyDataTestUtils salesReturnOrderViewCompanyDataTestUtils;
  private IObSalesReturnOrderViewReturnDetailsTestUtils salesReturnOrderViewReturnDetailsTestUtils;

  public DObSalesReturnOrderCreateStep() {
    Given(
        "^the following ProductManagers exist:$",
        (DataTable productManagerDataTable) -> {
          utils.assertThatTheFollowingProductManagersExist(productManagerDataTable);
        });

    Given(
        "^the following SalesInvoices exist with the following State, Customer, BusinessUnit,Company:$",
        (DataTable salesInvoiceDataTable) -> {
          utils.assertSalesInvoicesWithStateCustomerBusinessUnitCompanyExist(salesInvoiceDataTable);
        });

    Given(
        "^the following SalesInvoices have the following Taxes:$",
        (DataTable salesInvoiceTaxesDataTable) -> {
          utils.assertThatTheFollowingTaxesExistForSalesInvoice(salesInvoiceTaxesDataTable);
        });

    Given(
        "^Last created SalesReturnOrder was with code \"([^\"]*)\"$",
        (String lastEntityCode) -> {
          utils.createLastEntityCode(DObSalesReturnOrder.class.getSimpleName(), lastEntityCode);
        });

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    When(
        "^\"([^\"]*)\" creates SalesReturnOrder with the following values:$",
        (String userName, DataTable salesReturnOrderDataTable) -> {
          Response response =
              utils.createSalesReturnOrder(
                  salesReturnOrderDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new SalesReturnOrder is created as follows:$",
        (DataTable salesReturnOrderDataTable) -> {
          utils.assertThatSalesReturnOrderIsCreatedSuccessfully(salesReturnOrderDataTable);
        });

    Then(
        "^the Company Section of SalesReturnOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesReturnOrderCode, DataTable sroCompanyDataTable) -> {
          utils.assertThatSalesReturnOrderCompanyDataIsCreatedSuccessfully(
              salesReturnOrderCode, sroCompanyDataTable, salesReturnOrderViewCompanyDataTestUtils);
        });

    Then(
        "^the SalesReturnDetails Section of SalesReturnOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesReturnOrderCode, DataTable sroReturnDetailsDataTable) -> {
          utils.assertThatSalesReturnOrderReturnDetailsDataIsCreatedSuccessfully(
              salesReturnOrderCode,
              sroReturnDetailsDataTable,
              salesReturnOrderViewReturnDetailsTestUtils);
        });

    Then(
        "^the Items Section of SalesReturnOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesReturnOrderCode, DataTable salesReturnItemsDataTable) -> {
          utils.assertThatSalesReturnOrderItemsDataIsCreatedSuccessfully(
              salesReturnOrderCode, salesReturnItemsDataTable);
        });

    Then(
        "^the Taxes Section of SalesReturnOrder with code \"([^\"]*)\" is updated as follows:$",
        (String salesReturnOrderCode, DataTable salesReturnTaxesDataTable) -> {
          utils.assertThatSalesReturnOrderTaxesDetailsIsCreatedSuccessfully(
              salesReturnOrderCode, salesReturnTaxesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to creates SalesReturnOrder$",
        (String userName) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.requestCreateSalesReturnOrder(userCookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSalesReturnOrderCreateTestUtils(getProperties(), entityManagerDatabaseConnector);
    salesInvoiceItemsTestUtils = new IObSalesInvoiceItemsTestUtils(getProperties());
    salesInvoiceItemsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    salesReturnOrderViewCompanyDataTestUtils =
        new IObSalesReturnOrderViewCompanyDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    salesReturnOrderViewReturnDetailsTestUtils =
        new IObSalesReturnOrderViewReturnDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create SalesReturnOrder,")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create SalesReturnOrder,")) {
      utils.unfreeze();
    }
  }
}
