package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;

import java.util.Map;

public class DObSalesOrderAllowedActionsObjectScreenTestUtils extends DObSalesOrderCommonTestUtils {

  public DObSalesOrderAllowedActionsObjectScreenTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    return str.toString();
  }
}
