package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderTaxGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class IObSalesReturnOrderViewItemsDataTestUtils extends DObSalesReturnOrderCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[ItemData]";

  public IObSalesReturnOrderViewItemsDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readItemsData(Cookie cookie, String salesReturnOrderCode) {
    return sendGETRequest(cookie, getViewItemsDataUrl(salesReturnOrderCode));
  }

  private String getViewItemsDataUrl(String salesReturnOrderCode) {
    return IDObSalesReturnOrderRestURLs.QUERY
        + "/"
        + salesReturnOrderCode
        + IDObSalesReturnOrderRestURLs.ITEM_DATA;
  }

  public void assertThatSalesReturnOrderItemDataExists(DataTable itemDataTable) {
    List<Map<String, String>> itemDataListMap = itemDataTable.asMaps(String.class, String.class);

    for (Map<String, String> itemDataMap : itemDataListMap) {
      String salesReturnOrderCode = itemDataMap.get(IFeatureFileCommonKeys.CODE);
      assertThatSalesReturnOrderItemExists(salesReturnOrderCode, itemDataMap);
    }
  }

  private void assertThatSalesReturnOrderItemExists(
      String salesReturnOrderCode, Map<String, String> itemDataMap) {
    Object item =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getIObSalesReturnOrderItem",
            constructSalesReturnOrderItemDataQueryParams(itemDataMap));

    Assert.assertNotNull(
        ASSERTION_MSG
            + " Item data of sales return order "
            + salesReturnOrderCode
            + " with Item Id "
            + itemDataMap.get(IOrderFeatureFileCommonKeys.SRO_ITEM_ID)
            + " does not exist",
        item);
  }

  private Object[] constructSalesReturnOrderItemDataQueryParams(Map<String, String> itemDataMap) {
    return new Object[] {
      itemDataMap.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_REASON),
      itemDataMap.get(IFeatureFileCommonKeys.CODE),
      Long.valueOf(itemDataMap.get(IOrderFeatureFileCommonKeys.SRO_ITEM_ID)),
      itemDataMap.get(IFeatureFileCommonKeys.ITEM),
      itemDataMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      itemDataMap.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY),
      itemDataMap.get(ISalesReturnOrderFeatureFileCommonKeys.MAX_RETURN_QUANTITY),
      itemDataMap.get(IOrderFeatureFileCommonKeys.SALES_PRICE),
      itemDataMap.get(IOrderFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT)
    };
  }

  public void assertCorrectValuesAreDisplayedInItemSection(
      Response response, DataTable itemDataTable) throws Exception {
    List<Map<String, String>> expectedItemList = itemDataTable.asMaps(String.class, String.class);
    HashMap<String, Object> actualItemResponse = getMapFromResponse(response);
    List<HashMap<String, Object>> actualItemList =
        readListOfMapFromMap(actualItemResponse, "Items");
    for (int i = 0; i < actualItemList.size(); i++) {
      MapAssertion mapAssertion = new MapAssertion(expectedItemList.get(i), actualItemList.get(i));
      assertActualAndExpectedItemsFromResponse(mapAssertion);
    }
  }

  private void assertActualAndExpectedItemsFromResponse(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.ITEM,
        IIObSalesReturnOrderItemGeneralModel.ITEM_CODE,
        IIObSalesReturnOrderItemGeneralModel.ITEM_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.ITEM_ORDER_UNIT,
        IIObSalesReturnOrderItemGeneralModel.ORDER_UNIT_CODE,
        IIObSalesReturnOrderItemGeneralModel.ORDER_UNIT_SYMBOL);

    mapAssertion.assertCodeAndLocalizedNameValue(
        ISalesReturnOrderFeatureFileCommonKeys.RETURN_REASON,
        IIObSalesReturnOrderItemGeneralModel.RETURN_REASON_CODE,
        IIObSalesReturnOrderItemGeneralModel.RETURN_REASON_NAME);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY,
        IIObSalesReturnOrderItemGeneralModel.RETURN_QUANTITY);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesReturnOrderFeatureFileCommonKeys.MAX_RETURN_QUANTITY,
        IIObSalesReturnOrderItemGeneralModel.MAX_RETURN_QUANTITY);

    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.SALES_PRICE, IIObSalesReturnOrderItemGeneralModel.SALES_PRICE);

    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT,
        IIObSalesReturnOrderItemGeneralModel.TOTAL_AMOUNT);
  }

  public void assertThatItemTaxesResponseIsCorrect(Response response, DataTable taxesDataTable)
      throws Exception {
    List<Map<String, String>> expectedTaxesList = taxesDataTable.asMaps(String.class, String.class);
    HashMap<String, Object> actualItemResponse = getMapFromResponse(response);
    List<HashMap<String, Object>> actualTaxesList =
        readListOfMapFromMap(actualItemResponse, "Taxes");
    for (int i = 0; i < actualTaxesList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedTaxesList.get(i), actualTaxesList.get(i));
      assertActualAndExpectedTaxesFromResponse(mapAssertion);
    }
  }

  private void assertActualAndExpectedTaxesFromResponse(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        ISalesReturnOrderFeatureFileCommonKeys.TAX,
        IIObSalesReturnOrderTaxGeneralModel.TAX_CODE,
        IIObSalesReturnOrderTaxGeneralModel.TAX_NAME);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesReturnOrderFeatureFileCommonKeys.TAX_PERCENTAGE,
        IIObSalesReturnOrderTaxGeneralModel.TAX_PERCENTAGE);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesReturnOrderFeatureFileCommonKeys.TAX_AMOUNT,
        IIObSalesReturnOrderTaxGeneralModel.TAX_AMOUNT);
  }

  public void assertThatItemSummaryResponseIsCorrect(
      Response response, DataTable itemSummaryDataTable) throws Exception {
    Map<String, String> expectedItemsSummary =
        itemSummaryDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualItemDataResponse = getMapFromResponse(response);
    HashMap<String, Object> actualItemsSummary =
        readMapFromString(actualItemDataResponse, "ItemSummary");
    MapAssertion mapAssertion = new MapAssertion(expectedItemsSummary, actualItemsSummary);
    assertActualAndExpectedItemsSummaryFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedItemsSummaryFromResponse(MapAssertion mapAssertion) {
    mapAssertion.assertBigDecimalNumberValue(
        ISalesReturnOrderFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES,
        IIObSalesReturnOrderItemGeneralModel.TOTAL_AMOUNT_BEFORE_TAXES);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesReturnOrderFeatureFileCommonKeys.TOTAL_TAXES,
        IIObSalesReturnOrderItemGeneralModel.TOTAL_TAXES);

    mapAssertion.assertBigDecimalNumberValue(
        ISalesReturnOrderFeatureFileCommonKeys.TOTAL_AMOUNT_AFTER_TAXES,
        IIObSalesReturnOrderItemGeneralModel.TOTAL_AMOUNT_AFTER_TAXES);
  }

  public void assertThatSalesReturnOrderHasNoItems(DataTable salesReturnOrdersTable) {
    List<Map<String, String>> salesOrdersList =
        salesReturnOrdersTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderRow : salesOrdersList) {
      Object salesOrderCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObSalesReturnOrderItemBySalesReturnOrderCode",
              salesOrderRow.get(IFeatureFileCommonKeys.CODE));
      assertNull(salesOrderCode);
    }
  }

  public void assertThatItemResponseIsEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualItemsList = getItemsFromResponse(response);
    assertEquals(0, actualItemsList.size());
  }

  private List<HashMap<String, Object>> getItemsFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemsResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualItemsResponse, "Items");
  }

  public void assertThatSalesReturnOrderHasNoTaxes(DataTable salesReturnOrdersDataTable) {

    List<Map<String, String>> salesReturnOrdersList =
        salesReturnOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesReturnOrderRow : salesReturnOrdersList) {
      Object taxId =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObSalesReturnOrderTaxesBySalesReturnOrderCode",
              salesReturnOrderRow.get(IFeatureFileCommonKeys.CODE));
      assertNull(taxId);
    }
  }

  public void assertThatTaxesResponseIsEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualTaxesList = getTaxesFromResponse(response);
    assertEquals(0, actualTaxesList.size());
  }

  private List<HashMap<String, Object>> getTaxesFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemsResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualItemsResponse, "Taxes");
  }

  public void assertThatTaxDetailsExist(DataTable taxDetailsDataTable) {
    List<Map<String, String>> taxDetailsMaps =
        taxDetailsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> taxDetailsMap : taxDetailsMaps) {
      Object salesReturnOrderTaxes =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCreatedIObSalesReturnOrderTax", constructTaxesDetailQueryParams(taxDetailsMap));
      assertNotNull(taxDetailsMap.toString(), salesReturnOrderTaxes);
    }
  }

  private Object[] constructTaxesDetailQueryParams(Map<String, String> taxDetailsMap) {
    return new Object[] {
      taxDetailsMap.get(IFeatureFileCommonKeys.CODE),
      taxDetailsMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX),
      taxDetailsMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }
}
