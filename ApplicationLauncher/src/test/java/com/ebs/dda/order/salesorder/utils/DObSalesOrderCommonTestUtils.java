package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.IDObSalesOrderSectionNames;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DObSalesOrderCommonTestUtils extends CommonTestUtils {

  protected final String ASSERTION_MSG = "[SO]";
  public String SALES_ORDER_JMX_LOCK_BEAN_NAME = "SalesOrderEntityLockCommand";

  public DObSalesOrderCommonTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public static String getDbClearScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/order/salesorder/sales_order_Clear.sql");
    return str.toString();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    return str.toString();
  }

  public void createSalesOrderGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesOrderGeneralData",
          constructInsertSalesOrderGeneralDataQueryParams(generalDataMap));
    }
  }

  private Object[] constructInsertSalesOrderGeneralDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.STATE),
      generalDataMap.get(IOrderFeatureFileCommonKeys.DOCUMENT_OWNER),
      generalDataMap.get(IFeatureFileCommonKeys.TYPE),
      generalDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      generalDataMap.get(IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE)
    };
  }

  public void createSalesOrderCompanyData(DataTable companyDataTable) {
    List<Map<String, String>> companyDataMaps = companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> companyDataMap : companyDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesOrderCompanyData",
          constructInsertSalesOrderCompanyDataQueryParams(companyDataMap));
    }
  }

  private Object[] constructInsertSalesOrderCompanyDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.COMPANY),
      generalDataMap.get(IFeatureFileCommonKeys.STORE)
    };
  }

  public void createSalesOrderSalesOrderData(DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrderDataMaps =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderDataMap : salesOrderDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesOrderSalesOrderDataData",
          constructInsertSalesOrderSalesOrderDataQueryParams(salesOrderDataMap));
    }
  }

  private Object[] constructInsertSalesOrderSalesOrderDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.CUSTOMER),
      generalDataMap.get(ISalesOrderFeatureFileCommonKeys.CUSTOMER_ADDRESS),
      generalDataMap.get(ISalesOrderFeatureFileCommonKeys.CUSTOMER_CONTACT_PERSON),
      generalDataMap.get(IFeatureFileCommonKeys.PAYMENT_TERMS),
      generalDataMap.get(ISalesOrderFeatureFileCommonKeys.CURRENCY_ISO),
      generalDataMap.get(ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE),
      generalDataMap.get(ISalesOrderFeatureFileCommonKeys.NOTES).trim().isEmpty()
          ? null
          : generalDataMap.get(ISalesOrderFeatureFileCommonKeys.NOTES)
    };
  }

  public void createSalesOrderItems(DataTable itemsTable) {
    List<Map<String, String>> itemsMaps = itemsTable.asMaps(String.class, String.class);
    for (Map<String, String> itemsMap : itemsMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesOrderItem", constructInsertSalesOrderItemQueryParams(itemsMap));
    }
  }

  private Object[] constructInsertSalesOrderItemQueryParams(Map<String, String> itemsMap) {
    return new Object[] {
      itemsMap.get(IFeatureFileCommonKeys.CODE),
      itemsMap.get(IFeatureFileCommonKeys.ITEM),
      itemsMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      itemsMap.get(IFeatureFileCommonKeys.QTY),
      itemsMap.get(IOrderFeatureFileCommonKeys.SALES_PRICE)
    };
  }

  public void createSalesOrderTaxes(DataTable taxesTable) {
    List<Map<String, String>> taxesMaps = taxesTable.asMaps(String.class, String.class);
    for (Map<String, String> taxesMap : taxesMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesOrderTax", constructInsertSalesOrderTaxQueryParams(taxesMap));
    }
  }

  private Object[] constructInsertSalesOrderTaxQueryParams(Map<String, String> taxesMap) {
    return new Object[] {
      taxesMap.get(IFeatureFileCommonKeys.CODE),
      taxesMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX),
      taxesMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX),
      taxesMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }

  public void insertSalesOrderTotalAndRemaining(DataTable totalAndRemainingDT) {
    List<Map<String, String>> totalAndRemainingMap =
        totalAndRemainingDT.asMaps(String.class, String.class);
    for (Map<String, String> record : totalAndRemainingMap) {
      Object[] params = constructInsertSalesOrderTotalAndRemainingQueryParams(record);
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "insertSalesOrderTotalAndRemaining", params);
    }
  }

  private Object[] constructInsertSalesOrderTotalAndRemainingQueryParams(
      Map<String, String> record) {
    return new Object[] {
      record.get(ISalesReturnOrderFeatureFileCommonKeys.TOTAL_AMOUNT_AFTER_TAXES),
      record.get(ISalesReturnOrderFeatureFileCommonKeys.REMAINING),
      record.get(IFeatureFileCommonKeys.CODE)
    };
  }

  public void checkSalesOrderGeneralDataExists(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      Object salesOrder =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesOrderGeneralData",
              constructSalesOrderGeneralDataQueryParams(generalDataMap));
      assertNotNull("Sales Order doesn't exist", salesOrder);
      DateTime actualCreationDate = new DateTime(salesOrder).withZone(DateTimeZone.UTC);
      assertDateEquals(
          generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
          actualCreationDate.minusSeconds(actualCreationDate.getSecondOfMinute()));
    }
  }

  private Object[] constructSalesOrderGeneralDataQueryParams(Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.TYPE),
      generalDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      generalDataMap.get(IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE),
      '%' + generalDataMap.get(IFeatureFileCommonKeys.STATE) + '%',
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY)
    };
  }

  public void assertThatTheFollowingCompanyAndStoreDataExist(DataTable companyStoreDataTable) {
    List<Map<String, String>> companyStoreDataList =
        companyStoreDataTable.asMaps(String.class, String.class);
    for (Map<String, String> companyStoreDataRow : companyStoreDataList) {
      Object companyStoreData =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObCompanyAndStoreData", constructCustomerQueryParams(companyStoreDataRow));
      assertNotNull(
          "Company and store  data for SalesOrder "
              + companyStoreDataRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE)
              + " Doesn't exist",
          companyStoreData);
    }
  }

  private Object[] constructCustomerQueryParams(Map<String, String> companyStoreDataRow) {
    return new Object[] {
      companyStoreDataRow.get(IFeatureFileCommonKeys.STORE),
      companyStoreDataRow.get(ISalesOrderFeatureFileCommonKeys.SO_CODE),
      companyStoreDataRow.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void assertThatSalesOrdersExist(DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrderListMap =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderMap : salesOrderListMap) {
      DObSalesOrderGeneralModel salesOrderGeneralModel =
          (DObSalesOrderGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesOrderViewAll", constructSalesOrderQueryParams(salesOrderMap));
      assertNotNull(
          "Sales Order with code "
              + salesOrderMap.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exists",
          salesOrderGeneralModel);

      assertThatActualDateEqualsExpectedAndIfEmptyAssertNull(
          salesOrderMap.get(ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE),
          salesOrderGeneralModel.getExpectedDeliveryDate());
    }
  }

  protected void assertThatActualDateEqualsExpectedAndIfEmptyAssertNull(
      String expectedDateStr, Object actualDateObject) {
    if (expectedDateStr.equals("")) {
      assertNull(actualDateObject, "The actual date must be null");
    } else {
      assertDateEquals(expectedDateStr, new DateTime(actualDateObject).withZone(DateTimeZone.UTC));
    }
  }

  private Object[] constructSalesOrderQueryParams(Map<String, String> salesOrderMap) {
    return new Object[] {
      salesOrderMap.get(IFeatureFileCommonKeys.CODE),
      salesOrderMap.get(IFeatureFileCommonKeys.TYPE),
      salesOrderMap.get(IFeatureFileCommonKeys.CUSTOMER),
      salesOrderMap.get(IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE),
      salesOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      "%" + salesOrderMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IDObSalesOrderSectionNames.COMPANY_AND_STORE_SECTION,
            IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION,
            IDObSalesOrderSectionNames.ITEMS_SECTION));
  }

  protected String getFirstValue(Map<String, String> salesOrderDataMap, String key) {
    return salesOrderDataMap.get(key).split(" - ")[0].trim();
  }
}
