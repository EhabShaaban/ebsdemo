package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderAllowedActionsObjectScreenTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObSalesOrderAllowedActionsObjectScreenStep extends SpringBootRunner implements En {

  private DObSalesOrderAllowedActionsObjectScreenTestUtils utils;

  public DObSalesOrderAllowedActionsObjectScreenStep() {

    Given(
        "^the following SalesOrders exist:$",
        (DataTable salesOrderDataTable) -> {
          utils.assertThatSalesOrdersExist(salesOrderDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of SalesOrder with code \"([^\"]*)\"$",
        (String userName, String salesOrderCode) -> {
          String url = IDObSalesOrderRestURLs.AUTHORIZED_ACTIONS + salesOrderCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObSalesOrderAllowedActionsObjectScreenTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in SalesOrder Object Screen")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read allowed actions in SalesOrder Object Screen")) {
      databaseConnector.closeConnection();
    }
  }
}
