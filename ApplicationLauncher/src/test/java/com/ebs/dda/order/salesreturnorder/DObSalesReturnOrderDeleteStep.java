package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesReturnOrderDeleteStep extends SpringBootRunner implements En {

  private DObSalesReturnOrderDeleteTestUtils utils;

  public DObSalesReturnOrderDeleteStep() {

    When(
        "^\"([^\"]*)\" requests to delete SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesReturnOrderRestUrl(salesReturnOrderCode);
          Response response = utils.sendDeleteRequest(userCookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesReturnOrder with code \"([^\"]*)\" is deleted$",
        (String salesReturnOrderCode) -> {
          utils.assertThatSalesReturnOrderIsDeletedSuccessfully(salesReturnOrderCode);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" section of SalesReturnOrder with code \"([^\"]*)\" in edit mode$",
        (String userName, String sectionName, String salesReturnOrderCode) -> {});
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSalesReturnOrderDeleteTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete SalesReturnOrder,")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }
}
