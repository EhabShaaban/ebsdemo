package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewCompanyDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSalesReturnOrderViewCompanyDataStep extends SpringBootRunner implements En {

  private IObSalesReturnOrderViewCompanyDataTestUtils utils;

  public IObSalesReturnOrderViewCompanyDataStep() {

    When(
        "^\"([^\"]*)\" requests to view CompanyData section of SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readCompanyData(cookie, salesReturnOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of CompanyData section for SalesReturnOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesReturnCode, String userName, DataTable companyDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadCompanyDataResponseIsCorrect(response, companyDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObSalesReturnOrderViewCompanyDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Sales Return Order CompanyData section")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }
}
