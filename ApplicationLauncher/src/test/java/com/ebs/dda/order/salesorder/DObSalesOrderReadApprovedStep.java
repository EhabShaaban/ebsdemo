package com.ebs.dda.order.salesorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderReadApprovedTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObSalesOrderReadApprovedStep extends SpringBootRunner implements En {

    private DObSalesOrderReadApprovedTestUtils utils;

    public DObSalesOrderReadApprovedStep() {
        When(
                "^\"([^\"]*)\" requests to read list of Approved SalesOrders with BU \"([^\"]*)\"$",
                (String userName, String businessUnit) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    String restUrl =
                            IDObSalesOrderRestURLs.QUERY
                                    + "/"
                                    + businessUnit.split(" - ")[0]
                                    + IDObSalesOrderRestURLs.READ_APPROVED_SALES_ORDERS;
                    Response response = utils.sendGETRequest(cookie, restUrl);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following SalesOrder values will be presented to \"([^\"]*)\":$",
                (String userName, DataTable salesOrdersDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatSalesOrdersActualResponseMatchesExpected(response, salesOrdersDataTable);
                });

        Then(
                "^total number of SalesOrders returned to \"([^\"]*)\" is equal to (\\d+)$",
                (String userName, Integer recordsNumber) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
                });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObSalesOrderReadApprovedTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of SalesOrders -")) {
            databaseConnector.createConnection();
            databaseConnector.executeSQLScript(utils.getDbScripts());
        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "Read list of SalesOrders -")) {
            databaseConnector.closeConnection();
        }
    }
}
