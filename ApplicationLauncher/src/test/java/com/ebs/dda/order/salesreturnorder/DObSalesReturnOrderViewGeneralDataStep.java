package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesReturnOrderViewGeneralDataStep extends SpringBootRunner implements En {

  private DObSalesReturnOrderViewGeneralDataTestUtils utils;

  public DObSalesReturnOrderViewGeneralDataStep() {

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readGeneralData(cookie, salesReturnOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of GeneralData section for SalesReturnOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesReturnOrderCode, String userName, DataTable generalDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertSalesReturnOrderGeneralDataIsCorrect(response, generalDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObSalesReturnOrderViewGeneralDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Sales Return Order GeneralData section")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }
}
