package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesOrderReadApprovedTestUtils extends DObSalesOrderCommonTestUtils {

  public DObSalesOrderReadApprovedTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    return str.toString();
  }

  public void assertThatSalesOrdersActualResponseMatchesExpected(
      Response response, DataTable salesOrdersDataTable) throws Exception {
    List<Map<String, String>> expectedSalesOrders =
        salesOrdersDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualSalesOrders = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedSalesOrders.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedSalesOrders.get(i), actualSalesOrders.get(i));

      mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObSalesOrderGeneralModel.USER_CODE);

      mapAssertion.assertCodeAndLocalizedNameValue(
              IFeatureFileCommonKeys.TYPE,
              IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_CODE,
              IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_NAME);

      mapAssertion.assertCodeAndLocalizedNameValue(
              IFeatureFileCommonKeys.CUSTOMER,
              IDObSalesOrderGeneralModel.CUSTOMER_CODE,
              IDObSalesOrderGeneralModel.CUSTOMER_NAME);

      mapAssertion.assertCodeAndLocalizedNameValue(
              IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE,
              IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_CODE,
              IDObSalesOrderGeneralModel.SALES_RESPONSIBLE_NAME);

      mapAssertion.assertCodeAndLocalizedNameValue(
              IFeatureFileCommonKeys.BUSINESS_UNIT,
              IDObSalesOrderGeneralModel.PURCHASE_UNIT_CODE,
              IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME);


      mapAssertion.assertValueContains(
              IFeatureFileCommonKeys.STATE, IDObSalesOrderGeneralModel.CURRENT_STATES);

      mapAssertion.assertDateTime(
          ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE,
          IDObSalesOrderGeneralModel.EXPECTED_DELIVERY_DATE);

    }
  }
}
