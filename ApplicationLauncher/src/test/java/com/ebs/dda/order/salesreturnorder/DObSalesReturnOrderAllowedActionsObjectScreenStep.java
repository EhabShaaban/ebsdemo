package com.ebs.dda.order.salesreturnorder;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObSalesReturnOrderAllowedActionsObjectScreenStep extends SpringBootRunner
    implements En {

  private DObSalesReturnOrderCommonTestUtils utils;

  public DObSalesReturnOrderAllowedActionsObjectScreenStep() {
    When(
        "^\"([^\"]*)\" requests to read actions of SalesReturnOrder with code \"([^\"]*)\"$",
        (String userName, String salesReturnOrderCode) -> {
          String url = IDObSalesReturnOrderRestURLs.AUTHORIZED_ACTIONS + salesReturnOrderCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSalesReturnOrderCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in SalesReturnOrder Object Screen")) {
      databaseConnector.executeSQLScript(utils.clearSalesReturnOrder());
    }
  }
}
