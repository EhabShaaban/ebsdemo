package com.ebs.dda.order.salesorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.item.IMObItemUOMGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderItemGeneralModel;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObSalesOrderItemUOMReadAllTestUtils extends DObSalesOrderCommonTestUtils {

  public DObSalesOrderItemUOMReadAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String prepareDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoicePostingDetails.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append(",").append("db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");
    str.append(",").append("db-scripts/inventory/stock-transformation/CObStockTransformation.sql");
    str.append(",")
        .append("db-scripts/inventory/stock-transformation/DObStockTransformationScript.sql");
    str.append(",")
        .append(
            "db-scripts/inventory/store-transaction/TObStockTransformationStoreTransaction.sql");
    str.append(",")
        .append(
            "db-scripts/inventory/stock-transformation/IObStockTransformationDetailsScript.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCycleDates_ViewAll.sql");

    return str.toString();
  }

  public void assertThatActualItemUOMAreReturned(Response response, DataTable itemUOMDataTable)
      throws Exception {
    List<Map<String, String>> expectedUOMList = itemUOMDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualUOMList = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedUOMList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(
              expectedUOMList.get(i), readMapFromString(actualUOMList.get(i), "UnitOfMeasure"));
      assertActualAndExpectedUOMFromResponse(mapAssertion);
    }
  }

  private void assertActualAndExpectedUOMFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.UOM,
        IMObItemUOMGeneralModel.USER_CODE,
        IMObItemUOMGeneralModel.SYMBOL);
  }

  public void assertThatActualLastSalesPriceAndAvailableQuantityAreReturned(
      Response response, DataTable itemUOMDataTable) throws Exception {
    List<Map<String, String>> expectedUOMList = itemUOMDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualUOMList = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedUOMList.size(); i++) {
      MapAssertion mapAssertion = new MapAssertion(expectedUOMList.get(i), actualUOMList.get(i));
      assertActualAndExpectedLastSalesPriceAndAvailableQuatityFromResponse(mapAssertion);
    }
  }

  private void assertActualAndExpectedLastSalesPriceAndAvailableQuatityFromResponse(
      MapAssertion mapAssertion) {
    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.LAST_SALES_PRICE,
        IIObSalesOrderItemGeneralModel.LAST_SALES_PRICE);
    mapAssertion.assertBigDecimalNumberValue(
        ISalesOrderFeatureFileCommonKeys.AVAILABLE_QUANTITIES,
        IIObSalesOrderItemGeneralModel.AVAILABLE_QUANTITY);
  }

  public void assertThatUOMResponseCountIsCorrect(Response response, int uomCount) {
    List<HashMap<String, Object>> actualUOM = getListOfMapsFromResponse(response);
    Assert.assertEquals(uomCount, actualUOM.size());
  }

  public void assertThatSalesOrderWithTheFollowingCompanyAndStoreExist(
      DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrderListMap =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderMap : salesOrderListMap) {
      DObSalesOrderGeneralModel salesOrderGeneralModel =
          (DObSalesOrderGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesOrderDataWithCompanyAndStoreData",
                  constructSalesOrderWithCompanyAndStoreDataQueryParams(salesOrderMap));
      assertNotNull(
          "Sales Order with code "
              + salesOrderMap.get(ISalesOrderFeatureFileCommonKeys.SO_CODE)
              + " doesn't exists ",
          salesOrderGeneralModel);
    }
  }

  private Object[] constructSalesOrderWithCompanyAndStoreDataQueryParams(
      Map<String, String> salesOrderMap) {
    return new Object[] {
      salesOrderMap.get(IFeatureFileCommonKeys.COMPANY),
      salesOrderMap.get(IFeatureFileCommonKeys.STORE),
      salesOrderMap.get(ISalesOrderFeatureFileCommonKeys.SO_CODE),
      salesOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      salesOrderMap.get(IFeatureFileCommonKeys.CUSTOMER),
      "%" + salesOrderMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public String getReadItemUOMDataUrl(String salesOrderCode, String itemCode) {
    return IDObSalesOrderRestURLs.QUERY
        + "/"
        + salesOrderCode
        + IDObSalesOrderRestURLs.READ_ALL_ITEM_UOM
        + "/"
        + itemCode;
  }
}
