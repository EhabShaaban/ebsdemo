package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderDetailsGeneralModel;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class IObSalesReturnOrderViewReturnDetailsTestUtils
    extends DObSalesReturnOrderCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[View][ReturnDetails]";

  public IObSalesReturnOrderViewReturnDetailsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatSalesReturnOrderReturnDetailsExists(DataTable returnDetailsTable) {
    List<Map<String, String>> returnDetailsMaps =
        returnDetailsTable.asMaps(String.class, String.class);
    for (Map<String, String> returnDetailsMap : returnDetailsMaps) {
      String salesReturnOrderCode = returnDetailsMap.get(IFeatureFileCommonKeys.CODE);
      assertThatSalesReturnDetailsExists(salesReturnOrderCode, returnDetailsMap);
    }
  }

  public void assertThatSalesReturnDetailsExists(
      String salesReturnOrderCode, Map<String, String> returnDetailsMap) {
    Object salesReturnOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesReturnOrderReturnDetails",
            constructSalesReturnOrderReturnDetailsQueryParams(
                salesReturnOrderCode, returnDetailsMap));
    assertNotNull(ASSERTION_MSG + " Sales Return Order Details doesn't exist", salesReturnOrder);
  }

  private Object[] constructSalesReturnOrderReturnDetailsQueryParams(
      String salesReturnOrderCode, Map<String, String> returnDetailsTable) {
    return new Object[] {
      salesReturnOrderCode,
      returnDetailsTable.get(ISalesFeatureFileCommonKeys.SI_CODE),
      returnDetailsTable.get(IFeatureFileCommonKeys.CUSTOMER),
      returnDetailsTable.get(ISalesOrderFeatureFileCommonKeys.CURRENCY_ISO)
    };
  }

  public Response readReturnDetails(Cookie cookie, String salesReturnOrderCode) {
    return sendGETRequest(cookie, getViewReturnDetailsUrl(salesReturnOrderCode));
  }

  private String getViewReturnDetailsUrl(String salesReturnOrderCode) {
    return IDObSalesReturnOrderRestURLs.QUERY
        + "/"
        + salesReturnOrderCode
        + IDObSalesReturnOrderRestURLs.VIEW_RETURN_DETAILS;
  }

  public void assertSalesReturnOrderReturnDetailsTableIsCorrect(
      Response response, DataTable returnDetailsTable) throws Exception {
    Map<String, String> expectedReturnDetails =
        returnDetailsTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualReturnDetails = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedReturnDetails, actualReturnDetails);

    mapAssertion.assertValue(
        ISalesFeatureFileCommonKeys.SI_CODE,
        IIObSalesReturnOrderDetailsGeneralModel.SALES_INVOICE_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.CUSTOMER,
        IIObSalesReturnOrderDetailsGeneralModel.CUSTOMER_CODE,
        IIObSalesReturnOrderDetailsGeneralModel.CUSTOMER_NAME);

    mapAssertion.assertValue(
        ISalesOrderFeatureFileCommonKeys.CURRENCY_ISO,
        IIObSalesReturnOrderDetailsGeneralModel.CURRENCY_ISO);
  }
}
