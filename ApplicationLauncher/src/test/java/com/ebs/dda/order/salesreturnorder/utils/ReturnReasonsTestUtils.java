package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.lookups.ILObReason;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderCreateTestUtils;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ReturnReasonsTestUtils extends DObSalesOrderCreateTestUtils {

  protected String ASSERTION_MSG = "[ReturnReason]";

  public ReturnReasonsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatReturnReasonsExist(DataTable returnReasonDataTable) {
    List<Map<String, String>> returnReasonsList =
        returnReasonDataTable.asMaps(String.class, String.class);
    for (Map<String, String> returnReasonRow : returnReasonsList) {
      Object returnReason =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getReturnReasonByCodeAndName", constructReturnReasonQueryParams(returnReasonRow));
      assertNotNull(ASSERTION_MSG + "Return Reason is not exist", returnReason);
    }
  }

  private Object[] constructReturnReasonQueryParams(Map<String, String> returnReasonRow) {
    return new Object[] {
      returnReasonRow.get(IFeatureFileCommonKeys.REASON),
      returnReasonRow.get(IFeatureFileCommonKeys.TYPE)
    };
  }

  public Response readAllReturnReasons(Cookie cookie) {
    String restURL = IDObSalesReturnOrderRestURLs.READ_ALL_RETURN_REASONS;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReadAllReturnReasonsResponseIsCorrect(
      Response response, DataTable returnReasonDataTable) throws Exception {

    List<Map<String, String>> expectedReturnReasons =
        returnReasonDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualReturnReasons = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedReturnReasons.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(expectedReturnReasons.get(i), actualReturnReasons.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IFeatureFileCommonKeys.REASON, ILObReason.USER_CODE, ILObReason.NAME);
    }
  }

  public void assertThatTotalNumberOfRecordsIsCorrect(
      Response response, Integer totalNumberOfRecords) {

    List<HashMap<String, Object>> returnReasons = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + " total Number Of Records is not correct",
        totalNumberOfRecords.intValue(),
        returnReasons.size());
  }

  public void assertThatTotalNumberOfReturnReasonsIsCorrect(Long expectedNumberOfReturnReasons) {
    Object actualNumberOfReturnReasons =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getReturnReasonsForSalesReturnCount");
    assertEquals(
        ASSERTION_MSG + " total Number Of Records is not correct",
        expectedNumberOfReturnReasons,
        actualNumberOfReturnReasons);
  }
}
