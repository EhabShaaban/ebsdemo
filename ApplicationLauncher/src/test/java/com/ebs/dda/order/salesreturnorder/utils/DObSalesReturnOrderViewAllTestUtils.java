package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesReturnOrderViewAllTestUtils extends DObSalesReturnOrderCommonTestUtils {

  public DObSalesReturnOrderViewAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatTotalNumberOfSalesReturnOrdersEquals(Long totalRecordsNumber) {
    Object expectedTotalNumberOfSalesReturnOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getTotalNumberOfSalesReturnOrderViewAll");

    Assert.assertEquals(totalRecordsNumber, expectedTotalNumberOfSalesReturnOrder);
  }

  public void assertSalesReturnOrderDataIsCorrect(
      DataTable salesReturnOrderDataTable, Response response) throws Exception {
    List<Map<String, String>> expectedSalesReturnOrderList =
        salesReturnOrderDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualSalesReturnOrderResponse =
        getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedSalesReturnOrderList.size(); i++) {
      assertCorrectShownData(
          expectedSalesReturnOrderList.get(i), actualSalesReturnOrderResponse.get(i));
    }
  }

  private void assertCorrectShownData(
      Map<String, String> expectedMap, HashMap<String, Object> actualMap) throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedMap, actualMap);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObSalesReturnOrderGeneralModel.USER_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TYPE,
        IDObSalesReturnOrderGeneralModel.SALES_RETURN_ORDER_TYPE_CODE,
        IDObSalesReturnOrderGeneralModel.SALES_RETURN_ORDER_TYPE_NAME);

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObSalesReturnOrderGeneralModel.CREATION_DATE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObSalesReturnOrderGeneralModel.PURCHASE_UNIT_CODE,
        IDObSalesReturnOrderGeneralModel.PURCHASE_UNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.CUSTOMER,
        IDObSalesReturnOrderGeneralModel.CUSTOMER_CODE,
        IDObSalesReturnOrderGeneralModel.CUSTOMER_NAME);

    mapAssertion.assertValue(
        ISalesFeatureFileCommonKeys.SI_CODE, IDObSalesReturnOrderGeneralModel.SALES_INVOICE_CODE);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObSalesReturnOrderGeneralModel.CURRENT_STATES);
  }
}
