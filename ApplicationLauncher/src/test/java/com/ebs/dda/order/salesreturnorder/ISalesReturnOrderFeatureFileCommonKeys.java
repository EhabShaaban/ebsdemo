package com.ebs.dda.order.salesreturnorder;

public interface ISalesReturnOrderFeatureFileCommonKeys {

  String RETURN_REASON = "ReturnReason";
  String RETURN_QUANTITY = "ReturnQuantity";
  String MAX_RETURN_QUANTITY = "MaxReturnQuantity";
  String RETURN_QUANTITY_IN_BASE_UNIT = "ReturnQtyinBaseUnit";
  String TAX = "Tax";
  String TAX_PERCENTAGE = "TaxPercentage";
  String TAX_AMOUNT = "TaxAmount";

  String TOTAL_AMOUNT_BEFORE_TAXES = "TotalAmountBeforeTaxes";
  String TOTAL_TAXES = "TotalTaxes";
  String TOTAL_AMOUNT_AFTER_TAXES = "TotalAmountAfterTaxes";
  String SHIPPING_DATE = "ShippingDate";
  String REMAINING = "Remaining";
}
