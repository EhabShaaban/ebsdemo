package com.ebs.dda.order.salesreturnorder;

public interface IDObSalesReturnOrderRestURLs {

  String QUERY = "/services/order/salesreturnorder";
  String LOB_REASON_QUERY = "/services/masterdataobjects/reason";

  String VIEW_ALL = QUERY;
  String VIEW_GENERAL_DATA = "/generaldata";
  String VIEW_RETURN_DETAILS = "/returndetails";
  String COMPANY_DATA = "/companydata";
  String ITEM_DATA = "/items";
  String AUTHORIZED_ACTIONS = QUERY + "/authorizedactions/";
  String READ_ALL_SALES_RETURN_ORDER_TYPES = QUERY + "/salesreturnordertypes";
  String READ_ALL_RETURN_REASONS = LOB_REASON_QUERY + "/";
  String READ_ITEM_ORDER_UNITS = QUERY + "/itemorderunits/";

  String COMMAND = "/command/order/salesreturnorder";

  String CREATE_SALES_RETURN_ORDER = COMMAND + "/";
  String REQUEST_EDIT_ITEM = COMMAND + "/";

  String LOCK_ITEM_SECTION = "/lock/items";
  String UNLOCK_ITEM_SECTION = "/unlock/items";
  String UNLOCK_ALL_SECTION = "/unlock/lockedsections";

  String MARK_AS_SHIPPED = "/markasshipped";

  String SALES_RETURN_ORDER_FOR_GR = QUERY + "/salesreturnordersforgoodsreceipt/";
  String SALES_RETURN_ORDER_FOR_AN = QUERY + "/salesreturnordersforcreditnote/";

  String SAVE_EDIT_ITEM_SECTION = "/update/item/";
}
