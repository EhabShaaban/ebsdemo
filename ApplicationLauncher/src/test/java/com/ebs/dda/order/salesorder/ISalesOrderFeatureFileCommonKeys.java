package com.ebs.dda.order.salesorder;

public interface ISalesOrderFeatureFileCommonKeys {

  String SO_CODE = "SOCode";
  String ITEM_ID = "ItemId";
  String ITEM_TYPE = "ItemType";
  String CUSTOMER_ADDRESS = "CustomerAddress";
  String CURRENCY_CODE = "CurrencyCode";
  String CURRENCY_ISO = "CurrencyISO";
  String CREDIT_LIMIT = "CreditLimit";
  String CONTACT_PERSON = "ContactPerson";
  String CUSTOMER_CONTACT_PERSON = "CustomerContactPerson";
  String EXPECTED_DELIVERY_DATE = "ExpectedDeliveryDate";
  String NOTES = "Notes";
  String SALES_PRICE = "SalesPrice";
  String SALES_PRICE_DATE = "SalesPriceDate";
  String TOTAL_AMOUNT = "TotalAmount";
  String LAST_SALES_PRICE = "LastSalesPrice";
  String AVAILABLE_QUANTITIES = "AvailableQty";
  String TAX = "Tax";
  String TAX_PERCENTAGE = "TaxPercentage";
  String TAX_AMOUNT = "TaxAmount";

  String TOTAL_AMOUNT_BEFORE_TAXES = "TotalAmountBeforeTaxes";
  String TOTAL_TAXES = "TotalTaxes";
  String TOTAL_AMOUNT_AFTER_TAXES = "TotalAmountAfterTaxes";

  String APPROVAL_DATE = "ApprovalDate";
}
