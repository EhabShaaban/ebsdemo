package com.ebs.dda.order.salesorder.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesorder.IIObSalesOrderDataValueObject;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesOrderSaveSalesOrderDataTestUtils
    extends DObSalesOrderCommonTestUtils {

  public IObSalesOrderSaveSalesOrderDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response saveSalesOrderDataSection(
      String salesOrderCode, DataTable salesOrderDataDataTable, Cookie cookie) {
    JsonObject salesOrderDataJsonObject = getSalesOrderDataJsonObject(salesOrderDataDataTable);
    StringBuilder restURL = new StringBuilder();
    restURL.append(IDObSalesOrderRestURLs.COMMAND).append('/');
    restURL.append(salesOrderCode);
    restURL.append(IDObSalesOrderRestURLs.UPDATE_SALES_ORDER_DATA_SECTION);
    return sendPUTRequest(cookie, String.valueOf(restURL), salesOrderDataJsonObject);
  }

  private JsonObject getSalesOrderDataJsonObject(DataTable salesOrderDataDataTable) {
    Map<String, String> salesOrderDataMap =
        salesOrderDataDataTable.asMaps(String.class, String.class).get(0);

    JsonObject jsonObject = new JsonObject();

    jsonObject.addProperty(
        IIObSalesOrderDataValueObject.PAYMENT_TERM_CODE,
        convertEmptyStringToNull(
            getFirstValue(salesOrderDataMap, IFeatureFileCommonKeys.PAYMENT_TERMS)));

    jsonObject.addProperty(
        IIObSalesOrderDataValueObject.CUSTOMER_ADDRESS_ID,
        getObjectIdIfNotNull(salesOrderDataMap, ISalesOrderFeatureFileCommonKeys.CUSTOMER_ADDRESS));

    jsonObject.addProperty(
        IIObSalesOrderDataValueObject.CURRENCY_CODE,
        convertEmptyStringToNull(
            getFirstValue(salesOrderDataMap, IFeatureFileCommonKeys.CURRENCY)));

    jsonObject.addProperty(
        IIObSalesOrderDataValueObject.CONTACT_PERSON_ID,
        getObjectIdIfNotNull(salesOrderDataMap, ISalesOrderFeatureFileCommonKeys.CONTACT_PERSON));

    jsonObject.addProperty(
        IIObSalesOrderDataValueObject.EXPECTED_DELIVERY_DATE,
        convertEmptyStringToNull(
            salesOrderDataMap.get(ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE)));

    jsonObject.addProperty(
        IIObSalesOrderDataValueObject.NOTES,
        convertEmptyStringToNull(salesOrderDataMap.get(ISalesOrderFeatureFileCommonKeys.NOTES)));

    return jsonObject;
  }

  private Long getObjectIdIfNotNull(Map<String, String> salesOrderDataMap, String key) {
    String value = convertEmptyStringToNull(getFirstValue(salesOrderDataMap, key));
    return value != null ? Long.valueOf(value) : null;
  }

  private String getSecondValue(Map<String, String> salesOrderDataMap, String key) {
    return salesOrderDataMap.get(key).split(" - ")[1];
  }

  public void assertThatSalesOrderDataIsUpdatedAsFollows(
      String salesOrderCode, DataTable salesOrderDataDataTable) {
    Map<String, String> salesOrderDataMap =
        salesOrderDataDataTable.asMaps(String.class, String.class).get(0);

    Object[] salesOrderDataObject =
        (Object[])
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedIObSalesOrderData",
                constructSalesOrderDataQueryParams(salesOrderCode, salesOrderDataMap));

    assertNotNull(salesOrderDataObject);

    assertThatActualDateEqualsExpectedAndIfEmptyAssertNull(
        salesOrderDataMap.get(ISalesOrderFeatureFileCommonKeys.EXPECTED_DELIVERY_DATE),
        salesOrderDataObject[1]);
  }

  private Object[] constructSalesOrderDataQueryParams(
      String salesOrderCode, Map<String, String> salesOrderDataMap) {
    return new Object[] {
      salesOrderDataMap.get(IFeatureFileCommonKeys.PAYMENT_TERMS),
      getSecondValueIfObjectNotNullOrEmpty(
          salesOrderDataMap, ISalesOrderFeatureFileCommonKeys.CUSTOMER_ADDRESS),
      getSecondValueIfObjectNotNullOrEmpty(salesOrderDataMap, IFeatureFileCommonKeys.CURRENCY),
      getSecondValueIfObjectNotNullOrEmpty(
          salesOrderDataMap, ISalesOrderFeatureFileCommonKeys.CONTACT_PERSON),
      salesOrderDataMap.get(ISalesOrderFeatureFileCommonKeys.NOTES),
      salesOrderCode
    };
  }

  private String getSecondValueIfObjectNotNullOrEmpty(
      Map<String, String> salesOrderDataMap, String key) {
    String object = salesOrderDataMap.get(key);
    return (object == null || object.isEmpty()) ? "" : getSecondValue(salesOrderDataMap, key);
  }
}
