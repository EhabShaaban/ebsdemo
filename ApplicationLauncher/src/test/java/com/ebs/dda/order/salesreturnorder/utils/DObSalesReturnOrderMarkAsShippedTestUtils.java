package com.ebs.dda.order.salesreturnorder.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObSalesReturnOrderMarkAsShippedTestUtils extends DObSalesReturnOrderCommonTestUtils {

  private final String ASSERTION_MSG = super.ASSERTION_MSG + "[MarkAsShipped]";

  public DObSalesReturnOrderMarkAsShippedTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getSalesReturnOrderMarkAsShippedRestUrl(String salesReturnOrderCode) {
    return IDObSalesReturnOrderRestURLs.COMMAND
        + "/"
        + salesReturnOrderCode
        + IDObSalesReturnOrderRestURLs.MARK_AS_SHIPPED;
  }

  public void assertThatCycleDatesIsUpdated(
      String salesReturnOrderCode, DataTable cycleDatesDataTable) {
    Map<String, String> expectedCycleDatesMap =
        cycleDatesDataTable.asMaps(String.class, String.class).get(0);

    Object actualCycleDates =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedSalesReturnOrderShippingDate", salesReturnOrderCode);

    assertNotNull(
        ASSERTION_MSG + " Cycle Date of Sales Order " + salesReturnOrderCode + " is not updated",
        actualCycleDates);

    assertDateEquals(
        expectedCycleDatesMap.get(ISalesReturnOrderFeatureFileCommonKeys.SHIPPING_DATE),
        new DateTime(actualCycleDates).withZone(DateTimeZone.UTC));
  }

  public void assertThatSalesInvoiceItemsUpdated(
      String salesInvoiceCode, DataTable salesInvoiceItemsTable) {
    List<Map<String, String>> salesInvoiceItems =
        salesInvoiceItemsTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceItem : salesInvoiceItems) {
      assertThatSalesInvoiceHasItem(salesInvoiceCode, salesInvoiceItem);
    }
  }
}
