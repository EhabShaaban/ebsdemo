package com.ebs.dda.order.salesorder.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderCreateValueObject;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.IDObSalesOrderRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

public class DObSalesOrderCreateTestUtils extends DObSalesOrderCommonTestUtils {

    public DObSalesOrderCreateTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        super(properties);
        setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    return str.toString();
  }

  public void assertThatLastCreatedCodeIsCorrect(String lastCreatedCode) {
    Object lastEntityCode =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getLastInsertedEntityCodeByEntityType", "DObSalesOrder");
    assertEquals(lastCreatedCode, lastEntityCode);
  }
  public Response createSalesOrder(DataTable salesOrderDataTable, Cookie cookie) {
    JsonObject salesOrder = prepareSalesOrderJsonObject(salesOrderDataTable);
    String restURL = IDObSalesOrderRestURLs.CREATE;
    return sendPostRequest(cookie, restURL, salesOrder);
  }

  private JsonObject prepareSalesOrderJsonObject(DataTable salesOrderDataTable) {
    Map<String, String> salesOrderMap =
        salesOrderDataTable.asMaps(String.class, String.class).get(0);

    JsonObject salesOrderJsonObject = new JsonObject();

    addValueToJsonObject(
        salesOrderJsonObject,
        IDObSalesOrderCreateValueObject.TYPE_CODE,
        salesOrderMap.get(IOrderFeatureFileCommonKeys.TYPE));

    addValueToJsonObject(
        salesOrderJsonObject,
        IDObSalesOrderCreateValueObject.BUSINESS_UNIT_CODE,
        salesOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT));

    addValueToJsonObject(
        salesOrderJsonObject,
        IDObSalesOrderCreateValueObject.COMPANY_CODE,
        salesOrderMap.get(IFeatureFileCommonKeys.COMPANY));

    addValueToJsonObject(
        salesOrderJsonObject,
        IDObSalesOrderCreateValueObject.CUSTOMER_CODE,
        salesOrderMap.get(IFeatureFileCommonKeys.CUSTOMER));

    addValueToJsonObject(
        salesOrderJsonObject,
        IDObSalesOrderCreateValueObject.SALES_RESPONSIBLE_CODE,
        salesOrderMap.get(IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE));

    return salesOrderJsonObject;
  }

  public void assertThatSalesOrderIsCreatedSuccessfully(DataTable salesOrderDataTable) {
    Map<String, String> salesOrderData =
        salesOrderDataTable.asMaps(String.class, String.class).get(0);
    Object[] salesOrder = (Object[]) retrieveSalesOrderData(salesOrderData);

    Assert.assertNotNull(salesOrder);

    assertDateEquals(
        salesOrderData.get(IFeatureFileCommonKeys.CREATION_DATE),
        new DateTime(salesOrder[0]).withZone(DateTimeZone.UTC));

    assertDateEquals(
            salesOrderData.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            new DateTime(salesOrder[1]).withZone(DateTimeZone.UTC));
  }

  private Object retrieveSalesOrderData(Map<String, String> salesOrderRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedSalesOrder", constructCreatedSalesOrderQueryParams(salesOrderRow));
  }

  private Object[] constructCreatedSalesOrderQueryParams(Map<String, String> salesOrderRow) {
    return new Object[] {
      salesOrderRow.get(IFeatureFileCommonKeys.CODE),
      salesOrderRow.get(IFeatureFileCommonKeys.CREATED_BY),
      salesOrderRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      salesOrderRow.get(IOrderFeatureFileCommonKeys.TYPE),
      salesOrderRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      salesOrderRow.get(IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE),
      '%' + salesOrderRow.get(IFeatureFileCommonKeys.STATE) + '%'
    };
  }

  public void assertThatIObSalesOrderDataIsCreatedSuccessfully(
      String salesOrderCode, DataTable iobSalesOrderDataTable) {
    Map<String, String> iobSalesOrderData =
        iobSalesOrderDataTable.asMaps(String.class, String.class).get(0);
    Object salesOrderData = retrieveIObSalesOrderData(salesOrderCode, iobSalesOrderData);
    Assert.assertNotNull(salesOrderData);
  }

  private Object retrieveIObSalesOrderData(
      String salesOrderCode, Map<String, String> iobSalesOrderData) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedIObSalesOrderData",
        constructCreatedIObSalesOrderDataQueryParams(salesOrderCode, iobSalesOrderData));
  }

  private Object[] constructCreatedIObSalesOrderDataQueryParams(
      String salesOrderCode, Map<String, String> iobSalesOrderData) {
    return new Object[] {salesOrderCode, iobSalesOrderData.get(IFeatureFileCommonKeys.CUSTOMER)};
  }

  public void assertThatCompanyIsCreatedInCompanyAndStoreSection(
      String salesOrderCode, DataTable companyDataTable) {
    Map<String, String> companyData = companyDataTable.asMaps(String.class, String.class).get(0);
    Object companyStoreData = retrieveIObCompanyStoreData(salesOrderCode, companyData);
    Assert.assertNotNull(
        ASSERTION_MSG + " Company does not created for code " + salesOrderCode, companyStoreData);
  }

  private Object retrieveIObCompanyStoreData(
      String salesOrderCode, Map<String, String> iobSalesOrderData) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedCompanyFromIObCompanyAndStoreData",
        constructCreatedCompanyFromIObSalesOrderCompanyAndStoreDataQueryParams(
            salesOrderCode, iobSalesOrderData));
  }

  private Object[] constructCreatedCompanyFromIObSalesOrderCompanyAndStoreDataQueryParams(
      String salesOrderCode, Map<String, String> iobSalesOrderData) {
    return new Object[] {salesOrderCode, iobSalesOrderData.get(IFeatureFileCommonKeys.COMPANY)};
  }

  public void assertThatSalesOrderTaxesIsUpdatedAsFollows(
      String salesOrderCode, DataTable taxesSummary) {
    List<Map<String, String>> salesOrderTaxesList = taxesSummary.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderTax : salesOrderTaxesList) {
      assertThatTaxesExistForSalesOrder(salesOrderCode, salesOrderTax);
    }
  }

  private void assertThatTaxesExistForSalesOrder(
      String salesOrderCode, Map<String, String> salesOrderTaxMap) {
    Object taxData = retrieveIObSalesOrderTaxObject(salesOrderCode, salesOrderTaxMap);
    Assert.assertNotNull(ASSERTION_MSG + " Tax does not exist for code " + salesOrderCode, taxData);
  }

  private Object retrieveIObSalesOrderTaxObject(
      String salesOrderCode, Map<String, String> salesOrderTaxMap) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getSalesOrderTax", constructSalesOrderTaxQueryParams(salesOrderCode, salesOrderTaxMap));
  }

  private Object[] constructSalesOrderTaxQueryParams(
      String salesOrderCode, Map<String, String> salesOrderDataRow) {
    return new Object[] {
      salesOrderCode,
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.TAX),
      salesOrderDataRow.get(ISalesOrderFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }
  public Response requestCreateSalesOrder(Cookie cookie) {
    String restURL = IDObSalesOrderRestURLs.CREATE;
    return sendGETRequest(cookie, restURL);
  }
}
