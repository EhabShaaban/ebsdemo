package com.ebs.dda.generalfunctions.logout_lockrelease;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class GeneralLogoutLockReleaseStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private GeneralLogoutLockReleaseTestUtils logoutLockReleaseTestUtils;

  public GeneralLogoutLockReleaseStep() {

    Given(
        "^the following Vendors exist:$",
        (DataTable vendorDataTable) -> {
          logoutLockReleaseTestUtils.assertThatVendorsExist(vendorDataTable);
        });

    Given(
        "^\"([^\"]*)\" opens ConsigneeData section of PO with code \"([^\"]*)\" in edit mode successfully$",
        (String userName, String purchaseOrderCode) -> {
          String lock_url = IPurchaseOrderRestUrls.LOCK_CONSIGNEE_SECTION + purchaseOrderCode;
          Response response = userActionsTestUtils.lockSection(userName,
              lock_url, purchaseOrderCode);
          logoutLockReleaseTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" opens GeneralData section of Item with code \"([^\"]*)\" in edit mode successfully$",
        (String userName, String itemCode) -> {
          String lock_url = IMObItemRestUrls.LOCK_GENERAL_DATA_SECTION + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lock_url, itemCode);
          logoutLockReleaseTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" opens GeneralData section of Vendor with code \"([^\"]*)\" in edit mode successfully$",
        (String userName, String vendorCode) -> {
            String lockUrl = IMObVendorRestUrls.LOCK_GENERAL_DATA_SECTION + vendorCode;
            Response response = userActionsTestUtils.lockSection(userName, lockUrl, vendorCode);
          logoutLockReleaseTestUtils.assertResponseSuccessStatus(response);
        });

    When(
        "^\"([^\"]*)\" requests to open ConsigneeData section of PO with code \"([^\"]*)\" in edit mode$",
        (String userName, String purchaseOrderCode) -> {
          String lock_url = IPurchaseOrderRestUrls.LOCK_CONSIGNEE_SECTION + purchaseOrderCode;
          Response response = userActionsTestUtils.lockSection(userName,
              lock_url, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to open GeneralData section of Item with code \"([^\"]*)\" in edit mode$",
        (String userName, String itemCode) -> {
          String lock_url = IMObItemRestUrls.LOCK_GENERAL_DATA_SECTION + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lock_url, itemCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to open GeneralData section of Vendor with code \"([^\"]*)\" in edit mode$",
        (String userName, String vendorCode) -> {
            String lockUrl = IMObVendorRestUrls.LOCK_GENERAL_DATA_SECTION + vendorCode;
            Response response = userActionsTestUtils.lockSection(userName, lockUrl, vendorCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" logs out$",
        (String userName) -> {
          userActionsTestUtils.logout(userName);
        });

    Then(
        "^\"([^\"]*)\" section of PO with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String sectionName, String purchaseOrderCode, String userName) -> {
          logoutLockReleaseTestUtils.assertThatResourceIsLockedByUser(
              userName,
              purchaseOrderCode,
              sectionName,
              logoutLockReleaseTestUtils.PO_JMX_LOCK_BEAN_NAME);
        });

    Then(
        "^\"([^\"]*)\" section of Item with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String sectionName, String itemCode, String userName) -> {
          logoutLockReleaseTestUtils.assertThatResourceIsLockedByUser(
              userName, itemCode, sectionName, logoutLockReleaseTestUtils.ITEM_JMX_LOCK_BEAN_NAME);
        });

    Then(
        "^\"([^\"]*)\" section of Vendor with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String sectionName, String vendorCode, String userName) -> {
          logoutLockReleaseTestUtils.assertThatResourceIsLockedByUser(
              userName,
              vendorCode,
              sectionName,
              logoutLockReleaseTestUtils.VENDOR_JMX_LOCK_BEAN_NAME);
        });

    Then(
        "^\"([^\"]*)\" section of GR with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String sectionName, String goodsReceiptCode, String userName) -> {
          logoutLockReleaseTestUtils.assertThatResourceIsLockedByUser(
              userName,
              goodsReceiptCode,
              sectionName,
              logoutLockReleaseTestUtils.GR_JMX_LOCK_BEAN_NAME);
        });
  }

  @Before
  public void before() throws Exception {
    logoutLockReleaseTestUtils = new GeneralLogoutLockReleaseTestUtils(getProperties());
    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
      databaseConnector.executeSQLScript(logoutLockReleaseTestUtils.prepareGeneralFunctionsData());
      hasBeenExecutedOnce = true;
    }
  }

  @After
  public void after() throws Exception {
    logoutLockReleaseTestUtils.unfreeze();
    databaseConnector.closeConnection();
  }
}
