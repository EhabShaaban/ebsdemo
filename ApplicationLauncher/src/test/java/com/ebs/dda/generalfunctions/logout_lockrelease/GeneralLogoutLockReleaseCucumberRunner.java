package com.ebs.dda.generalfunctions.logout_lockrelease;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"classpath:features/general-functions/LogOut_(LockReleased).feature"},
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.generalfunctions.logout_lockrelease"},
    strict = true,
    tags = "not @Future")
public class GeneralLogoutLockReleaseCucumberRunner {
}
