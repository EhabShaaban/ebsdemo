package com.ebs.dda.generalfunctions.logout_lockrelease;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.masterdata.vendor.interfaces.ITableKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IVendorExpectedKeys;
import cucumber.api.DataTable;
import java.lang.management.ManagementFactory;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.apache.commons.collections.CollectionUtils;

public class GeneralLogoutLockReleaseTestUtils extends CommonTestUtils {

  protected String VENDOR_JMX_LOCK_BEAN_NAME = "vendorLockCommandJMXBean";
  protected String ITEM_JMX_LOCK_BEAN_NAME = "ItemEntityLockCommand";
  protected String PO_JMX_LOCK_BEAN_NAME = "purchaseOrderLockCommandJMXBean";
  protected String GR_JMX_LOCK_BEAN_NAME = "GoodsReceiptEntityLockCommand";

  GeneralLogoutLockReleaseTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String prepareGeneralFunctionsData() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/CObApprovalPoliciesSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptPurchaseOrderScript_ViewAll.sql");

    return str.toString();
  }

  public void assertThatResourceIsLockedByUser(
      String userName, String vendorCode, String sectionName, String objectName) throws Exception {
    assertTrue(isSectionLockedByUser(userName, vendorCode, sectionName, objectName));
  }

  private boolean isSectionLockedByUser(
      String userName, String vendorCode, String sectionName, String objectName) throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name = new ObjectName("com.ebs.dda.commands.general:name=" + objectName);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, vendorCode};
    String[] parameterTypes =
        new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
        (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);

    return null != lockReleaseDetails;
  }

  public void assertThatVendorsExist(DataTable vendorDataTable) throws Exception {
    for (int i = 0; i < vendorDataTable.raw().size() - 1; i++) {
      Map<String, String> expectedMap = vendorDataTable.asMaps(String.class, String.class).get(i);
      Map<String, Object> actualMap = constructActualVendorMap(expectedMap.get("Code"));
      assertThatVendorDataIsCorrect(expectedMap, actualMap);
    }
  }

  protected Map<String, Object> constructActualVendorMap(String code) throws Exception {
    String query = "SELECT * FROM MObVendorGeneralModel WHERE code = ?";
    return getDatabaseRecord(query, code);
  }

  protected Map<String, Object> getDatabaseRecord(String query, Object... params) throws Exception {
    PreparedStatement preparedStatement = constructPreparedStatement(query, params);
    ResultSet resultSet = executeQuery(preparedStatement);
    return convertResultSetToMap(resultSet);
  }

  private Map<String, Object> convertResultSetToMap(ResultSet resultSet) throws SQLException {

    ResultSetMetaData metaData = resultSet.getMetaData();
    int colCount = metaData.getColumnCount();
    Map<String, Object> actualData = new HashMap<>();
    while (resultSet.next()) {

      for (int i = 1; i <= colCount; i++) {
        actualData.put(metaData.getColumnLabel(i), resultSet.getObject(i));
      }
    }
    return actualData;
  }

  private void assertThatVendorDataIsCorrect(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    assertThatPurchaseUnitsCodeAreEquals(
        expectedMap.get(IVendorExpectedKeys.PURCHASING_UNIT),
        (String) actualMap.get(ITableKeys.PURCHASE_UNITS_CODES));

    assertEquals(expectedMap.get("Code"), actualMap.get(ITableKeys.VENDOR_CODE));
  }

  protected void assertThatPurchaseUnitsCodeAreEquals(
      String expectedPurchaseUnitCodes, String actualPurchaseUnitCodes) {
    List<String> expectedPurchaseUnitCodesList =
        convertPurchasenitCodesStringToList(expectedPurchaseUnitCodes);
    List<String> actualPurchaseUnitCodesList =
        convertPurchasenitCodesStringToList(actualPurchaseUnitCodes);
    assertTrue(
        CollectionUtils.isEqualCollection(
            expectedPurchaseUnitCodesList, actualPurchaseUnitCodesList));
  }

  protected List<String> convertPurchasenitCodesStringToList(String purchaseUnitsCodes) {
    List<String> purchaseUnitCodes =
        Arrays.asList(purchaseUnitsCodes.replaceAll("\\s", "").split(","));

    return purchaseUnitCodes;
  }
}
