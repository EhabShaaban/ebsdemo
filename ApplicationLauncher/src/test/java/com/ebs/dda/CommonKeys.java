package com.ebs.dda;

public interface CommonKeys {
  String ERROR = "ERROR";
  String FAIL = "FAIL";
  String SUCCESS = "SUCCESS";
  String STATUS = "status";
  String RESULT = "result";
  String CODE = "code";
  String USER_CODE = "userCode";
  String NAME = "name";
  String ERRORS_VALIDATION_RESULT = "errorsValidationResult";
  String USER_NAME = "username";
  String PASSWORD = "password";
  String CURRENT_USER_LOCK = "getLockForCurrentUserOfResource";
  String SERVER_PORT = "serverPort";
  String DATABASE_URL = "databaseURL";
  String DATABASE_USER = "databaseUser";
  String DATABASE_PASSWORD = "databasePassword";
  String LOGIN_URL = "loginUrl";
  String COOKIE_NAME = "cookieName";
  String ENUM_VALUE = "value";
  String DATA = "data";
  String ID = "id";
  String TOTAL_NUMBER_OF_RECOREDS = "totalNumberOfRecords";
  String URL_PREFIX = "urlPrefix";
}
