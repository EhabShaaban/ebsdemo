package com.ebs.dda;

import java.util.HashMap;

import test.util.DatabaseConnector;

@Deprecated
public class CommonTestUtils_ {
  public static final String URL_PREFIX = "urlPrefix";
  protected DatabaseConnector databaseConnector;
  protected HashMap<String, Long> userRecord;
  protected HashMap<String, Long> roleRecord;
  protected HashMap<String, Long> permissionRecord;
  protected HashMap<String, Long> authzConditionRecord;
  protected String urlPrefix = "";

  public CommonTestUtils_() {
    userRecord = new HashMap<>();
    roleRecord = new HashMap<>();
    permissionRecord = new HashMap<>();
    authzConditionRecord = new HashMap<>();
    // ================================

    databaseConnector = DatabaseConnector.getInstance();
  }
}
