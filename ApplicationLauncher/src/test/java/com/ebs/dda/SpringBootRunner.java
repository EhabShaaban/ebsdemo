package com.ebs.dda;

import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidatorFactory;
import com.ebs.dda.purchases.utils.DObPurchaseOrderStateTransitionTestUtils;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import test.util.DatabaseConnector;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(
    classes = {ApplicationTestLauncher.class},
    webEnvironment = WebEnvironment.DEFINED_PORT)
public class SpringBootRunner implements InitializingBean {

  @Value("${postgresql.url}")
  protected String databaseURL;

  @Value("${postgresql.username}")
  protected String databaseUser;

  @Value("${postgresql.password}")
  protected String databasePassword;

  protected DatabaseConnector databaseConnector;
  @Autowired protected EntityManagerDatabaseConnector entityManagerDatabaseConnector;
  JsonSchemaValidatorFactory jsonSchemaFactory;
  Map<String, Object> properties;
  @LocalServerPort private int serverPort;
  @Value("${shiro.loginUrl}")
  private String loginUrl;
  @Value("${shiro.uid.cookie.name}")
  private String cookieName;
  @Value("${baseURL}")
  private String baseURL;

  @Autowired
  protected UserActionsTestUtils userActionsTestUtils;
  protected DObPurchaseOrderStateTransitionTestUtils stateTransitionUtils;

  @Override
  public void afterPropertiesSet() {
    properties = new HashMap<>();
    String restURL = baseURL + ":" + serverPort;

    properties.put(CommonKeys.DATABASE_URL, databaseURL);
    properties.put(CommonKeys.DATABASE_USER, databaseUser);
    properties.put(CommonKeys.DATABASE_PASSWORD, databasePassword);
    properties.put(CommonKeys.SERVER_PORT, serverPort);
    properties.put(CommonKeys.LOGIN_URL, loginUrl);
    properties.put(CommonKeys.COOKIE_NAME, cookieName);
    properties.put(CommonKeys.URL_PREFIX, restURL);

    DatabaseConnector.setConnectionValues(databaseURL, databaseUser, databasePassword);
    databaseConnector = DatabaseConnector.getInstance();
  }

  protected Map<String, Object> getProperties() {
    return new HashMap<>(properties);
  }

  protected boolean contains(String source, String target) {
    return source.contains(target);
  }

}
