package com.ebs.dda;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestUtilsBeans {

  @Value("${postgresql.url}")
  protected String databaseURL;

  @Value("${postgresql.username}")
  protected String databaseUser;

  @Value("${postgresql.password}")
  protected String databasePassword;

  @Value("${server.port}")
  private int serverPort;

  @Value("${shiro.loginUrl}")
  private String loginUrl;

  @Value("${shiro.uid.cookie.name}")
  private String cookieName;

  @Value("${baseURL}")
  private String baseURL;

  private Map<String, Object>  getProperties() {
    Map<String, Object> properties = new HashMap<>();
    String restURL = baseURL + ":" + serverPort;
    properties.put(CommonKeys.DATABASE_URL, databaseURL);
    properties.put(CommonKeys.DATABASE_USER, databaseUser);
    properties.put(CommonKeys.DATABASE_PASSWORD, databasePassword);
    properties.put(CommonKeys.SERVER_PORT, serverPort);
    properties.put(CommonKeys.LOGIN_URL, loginUrl);
    properties.put(CommonKeys.COOKIE_NAME, cookieName);
    properties.put(CommonKeys.URL_PREFIX, restURL);
    return properties;
  }

  @Bean("userActionsTestUtils")
  public UserActionsTestUtils userActionsTestUtils(){
    return new UserActionsTestUtils(getProperties());
  }

}