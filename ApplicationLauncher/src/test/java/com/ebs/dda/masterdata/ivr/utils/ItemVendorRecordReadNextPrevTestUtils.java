package com.ebs.dda.masterdata.ivr.utils;

import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.masterdata.ivr.IExpectedKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class ItemVendorRecordReadNextPrevTestUtils extends ItemVendorRecordCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Next/Prev]";

  public ItemVendorRecordReadNextPrevTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ivr-read-NextPrev-SqlScript.sql");
    return str.toString();
  }

  public Response requestToViewPreviousIVR(Cookie userCookie, String currentIVRCode) {
    String restURL = "/services/masterdataobjects/itemvendor/previous/" + currentIVRCode;
    return sendGETRequest(userCookie, restURL);
  }

  public Response requestToViewNextIVR(Cookie userCookie, String currentIVRCode) {
    String restURL = "/services/masterdataobjects/itemvendor/next/" + currentIVRCode;
    return sendGETRequest(userCookie, restURL);
  }

  public void assertThatNextIVRCodeIsCorrect(String expectedNextIVRCode, Response nextIVRResponse) {
    String actualNextCode = nextIVRResponse.body().jsonPath().getString("nextCode");
    Assert.assertEquals(
        ASSERTION_MSG + " Next IVR code is not correct", expectedNextIVRCode, actualNextCode);
  }

  public void assertThatPreviousIVRCodeIsCorrect(
      String expectedPreviousIVRCode, Response previousIVRResponse) {
    String actualPreviousCode = previousIVRResponse.body().jsonPath().getString("previousCode");
    Assert.assertEquals(
        ASSERTION_MSG + " Previous IVR code is not correct",
        expectedPreviousIVRCode,
        actualPreviousCode);
  }

  public void assertThatItemVendorRecordsExistsByCodeAndPurchaseUnitCode(
      DataTable itemVendorRecordsList) {
    List<Map<String, String>> itemVendorRecords =
        itemVendorRecordsList.asMaps(String.class, String.class);
    for (Map<String, String> itemVendorRecord : itemVendorRecords) {
      ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
          (ItemVendorRecordGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getItemVendorRecordByCodeAndPurchaseUnitCode",
                  constructItemVendorRecordQueryParams(itemVendorRecord));

      Assert.assertNotNull(ASSERTION_MSG + " Record does not exist", itemVendorRecordGeneralModel);
    }
  }

  private Object[] constructItemVendorRecordQueryParams(Map<String, String> itemVendorRecord) {
    return new Object[] {
      itemVendorRecord.get(IExpectedKeys.IVR_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.PURCHASE_UNIT_CODE_KEY)
    };
  }
}
