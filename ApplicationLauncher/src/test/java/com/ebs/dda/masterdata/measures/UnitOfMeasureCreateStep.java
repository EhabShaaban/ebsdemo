package com.ebs.dda.masterdata.measures;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.measures.utils.CObMeasureCreateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class UnitOfMeasureCreateStep extends SpringBootRunner implements En {

  private CObMeasureCreateTestUtils utils;

  public UnitOfMeasureCreateStep() {

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    Given(
        "^Last created Unit Of Measure was with code \"([^\"]*)\"$",
        (String lastCreatedCode) -> {
          utils.assertThatLastCreatedCodeIsCorrect(lastCreatedCode);
        });

    When(
        "^\"([^\"]*)\" creates Unit Of Measure with the following values:$",
        (String userName, DataTable unitOfMeasureDataTable) -> {
          Response response =
              utils.createUnitOfMeasure(
                  unitOfMeasureDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new Unit Of Measure is created with code \"([^\"]*)\" as follows in the General Data:$",
        (String unitOfMeasureCode, DataTable accountingNoteDataTable) -> {
          utils.assertThatUnitOfMeasureWithCodeIsCreatedSuccessfully(unitOfMeasureCode, accountingNoteDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new CObMeasureCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Unit Of Measure,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create Unit Of Measure,")) {
      utils.unfreeze();
    }
  }
}
