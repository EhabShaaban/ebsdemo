package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorReadAllDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class MObVendorReadAllDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObVendorReadAllDropDownTestUtils utils;

  public MObVendorReadAllDropDownStep() {

    Given(
        "^the following Vendors exist:$",
        (DataTable vendorDataTable) -> {
          utils.assertThatVendorsExistByCodeAndNameAndPurchaseUnitsCodeNameConcat(vendorDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all Vendors$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, IMObVendorRestUrls.READ_ALL_VENDORS);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Vendors values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable vendorsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatResponseVendorDataIsCorrect(response, vendorsDataTable);
        });

    Then(
        "^total number of Vendors returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, String recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsReturned(response, Integer.valueOf(recordsNumber));
        });

    When(
        "^\"([^\"]*)\" requests to read all Vendors That related to Business Unit with code \"([^\"]*)\"$",
        (String userName, String businessUnitCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IMObVendorRestUrls.READ_ALL_VENDORS_By_Business_Unit + businessUnitCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new MObVendorReadAllDropDownTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read list of Vendors dropdown,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
