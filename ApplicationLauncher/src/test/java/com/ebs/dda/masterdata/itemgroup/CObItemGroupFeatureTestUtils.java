package com.ebs.dda.masterdata.itemgroup;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.jpa.masterdata.item.CObMaterialGroupGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class CObItemGroupFeatureTestUtils extends CommonTestUtils {


    public CObItemGroupFeatureTestUtils(Map<String, Object> properties) {
        setProperties(properties);
        this.urlPrefix = (String) getProperty(URL_PREFIX);
    }

    public String getMasterDataDbScriptsOneTimeExecution() {
        StringBuilder str = new StringBuilder();
        str.append("db-scripts/clearDataSqlScript.sql");
        str.append("," + "db-scripts/master-data/itemGroup/CObMaterialGroup.sql");
        return str.toString();
    }



    public void assertItemGroupsExist(DataTable itemGroupList) {
        CObMaterialGroupGeneralModel materialGroupGeneralModel;

        List<Map<String, String>> itemGroupData = itemGroupList.asMaps(String.class, String.class);

        for (Map<String, String> itemGroupRow : itemGroupData) {
            String itemGroupCode = itemGroupRow.get("Code");
            String itemGroupParentCode = itemGroupRow.get("ParentCode");
            String itemGroupParentName = itemGroupRow.get("ParentName");
            String itemGroupName = itemGroupRow.get("Name");
            String itemGroupLevel = itemGroupRow.get("GroupLevel");
            if (!itemGroupParentCode.isEmpty()) {

                materialGroupGeneralModel =
                        (CObMaterialGroupGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getCobmaterialgroupgeneralmodelByNameAndCodeAndLevelAndParentCodeAndParentName",
                            itemGroupCode,
                            itemGroupName,
                            itemGroupLevel,
                            itemGroupParentCode,
                            itemGroupParentName);
            } else {
                materialGroupGeneralModel =
                        (CObMaterialGroupGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getCobmaterialgroupgeneralmodelByNameAndCodeAndLevel",
                            itemGroupCode,
                            itemGroupName,
                            itemGroupLevel);
            }
            Assert.assertNotNull(materialGroupGeneralModel);

        }
    }

    public void assertThatItemGroupWithCodeAndLevelExists(String level, String code) {
        CObMaterialGroupGeneralModel materialGroupGeneralModel =
                (CObMaterialGroupGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getCobmaterialgroupgeneralmodelByCodeAndLevel", code, level);
        Assert.assertNotNull(materialGroupGeneralModel);
    }

    public void assertThatItemGroupWithCodeIsNotExists(String code) {
        CObMaterialGroupGeneralModel materialGroupGeneralModel =
                (CObMaterialGroupGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getCobmaterialgroupgeneralmodelByCode", code);
        Assert.assertNull(materialGroupGeneralModel);
    }

    public JsonObject prepareItemJsonObject(DataTable itemGroupsDataTable) {
        Map<String, String> itemGroup = itemGroupsDataTable.asMaps(String.class, String.class).get(0);
        JsonObject itemData = new JsonObject();
        itemData.addProperty("name", itemGroup.get("Name"));
        itemData.addProperty("type", itemGroup.get("GroupLevel"));
        if (itemGroup.get("ParentCode").isEmpty()) {
            itemData.add("parentCode", null);
        } else {
            itemData.addProperty("parentCode", itemGroup.get("ParentCode"));
        }

        return itemData;
    }


}
