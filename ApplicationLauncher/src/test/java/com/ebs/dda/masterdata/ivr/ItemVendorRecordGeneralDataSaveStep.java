package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordGeneralDataSaveTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class ItemVendorRecordGeneralDataSaveStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordGeneralDataSaveTestUtils utils;

  public ItemVendorRecordGeneralDataSaveStep() {

    When(
        "^\"([^\"]*)\" saves GeneralData section of IVR with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String username, String ivrCode, String saveTime, DataTable ivrDataTable) -> {
          utils.freeze(saveTime);
          Cookie loginCookie = userActionsTestUtils.getUserCookie(username);
          JsonObject ivrGeneralDataJsonObject =
              utils.createGeneralDataSectionValueObject(ivrCode, ivrDataTable);
          Response saveIVRGeneralDataResponse =
              utils.saveIVRGeneralDataSection(loginCookie, ivrGeneralDataJsonObject, ivrCode);
          userActionsTestUtils.setUserResponse(username, saveIVRGeneralDataResponse);
        });

    When(
        "^\"([^\"]*)\" saves GeneralData section of IVR with code \"([^\"]*)\" with the following values:$",
        (String username, String ivrCode, DataTable ivrDataTable) -> {
          Cookie loginCookie = userActionsTestUtils.getUserCookie(username);
          JsonObject ivrGeneralDataJsonObject =
              utils.createGeneralDataSectionValueObject(ivrCode, ivrDataTable);
          Response saveIVRGeneralDataResponse =
              utils.saveIVRGeneralDataSection(loginCookie, ivrGeneralDataJsonObject, ivrCode);
          userActionsTestUtils.setUserResponse(username, saveIVRGeneralDataResponse);
        });

    Then(
        "^IVR with code \"([^\"]*)\" is updated as follows:$",
        (String ivrCode, DataTable ivrDataTable) -> {
          utils.assertDataIsUpdated(ivrCode, ivrDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new ItemVendorRecordGeneralDataSaveTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "IVR Save GeneralData Section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "IVR Save GeneralData Section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockSectionForAllUsers(IVRRestUrls.UNLOCK_GENERAL_DATA_SECTION);
    }
  }
}
