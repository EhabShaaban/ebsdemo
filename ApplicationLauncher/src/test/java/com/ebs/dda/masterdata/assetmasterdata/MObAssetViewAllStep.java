package com.ebs.dda.masterdata.assetmasterdata;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.assetmasterdata.IMObAssetGeneralModel;
import com.ebs.dda.masterdata.assetmasterdata.utils.MObAssetViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObAssetViewAllStep extends SpringBootRunner implements En {

  private MObAssetViewAllTestUtils utils;

  public MObAssetViewAllStep() {

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of AssetMasterData with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IMObAssetRestURLs.VIEW_ALL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\" in AssetMasterData Home Screen:$",
        (String userName, DataTable assetData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatAssetDataIsCorrect(assetData, response);
        });

    Then(
        "^the total number of records presented to \"([^\"]*)\" in Home Screen are (\\d+)$",
        (String userName, Integer recordsCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsReturned(response, recordsCount);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of AssetMasterData with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(IMObAssetGeneralModel.USER_CODE, userCodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IMObAssetRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of AssetMasterData with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String typeEquals, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getEqualsFilterField(IMObAssetGeneralModel.ASSET_TYPE_CODE, typeEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IMObAssetRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of AssetMasterData with filter applied on Name which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String assetName,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getContainsFilterField(IMObAssetGeneralModel.ASSET_NAME, assetName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IMObAssetRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of AssetMasterData with filter applied on BusinessUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String businessUnitName,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IMObAssetGeneralModel.BUSINESS_UNIT_NAMES, businessUnitName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IMObAssetRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of AssetMasterData with filter applied on OldNumber which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String OldNumberContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(IMObAssetGeneralModel.OLD_NUMBER, OldNumberContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IMObAssetRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObAssetViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all AssetMasterData,")) {
      databaseConnector.executeSQLScript(utils.getDbClearScriptsExecution());
    }
  }
}
