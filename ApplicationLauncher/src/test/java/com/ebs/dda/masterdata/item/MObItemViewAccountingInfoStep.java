package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemViewAccountingInfoTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemViewAccountingInfoStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemViewAccountingInfoTestUtils utils;

  public MObItemViewAccountingInfoStep() {
    Given(
        "^the following AccountingDetails for Items exist:$",
        (DataTable accountingInfoDataTable) -> {
          utils.assertThatItemHasAccountInfo(accountingInfoDataTable);
        });
    When(
        "^\"([^\"]*)\" requests to view AccoutingInfo section of Item with code \"([^\"]*)\"$",
        (String user, String itemCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(user);
          Response itemAccountingInfoResponse =
              utils.sendReadAccountingInfoDetailsRequest(userCookie, itemCode);
          userActionsTestUtils.setUserResponse(user, itemAccountingInfoResponse);
        });

    Then(
        "^the following values of AccoutingInfo section for Item with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String itemCode, String userName, DataTable expectedAccountingInfoTable) -> {
          Response itemAccountingInfoResponse = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseDataIsCorrect(
              expectedAccountingInfoTable, itemAccountingInfoResponse);
        });
      Then(
              "^the following values of AccoutingInfo section for Service Item with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
              (String itemCode, String userName, DataTable expectedAccountingInfoTable) -> {
                  Response itemAccountingInfoResponse = userActionsTestUtils.getUserResponse(userName);
                  utils.assertResponseDataIsCorrectForServiceItem(
                          expectedAccountingInfoTable, itemAccountingInfoResponse);
              });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemViewAccountingInfoTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View AccountingInfo section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }
}
