package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemUOMSaveTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;

public class MObItemUOMSaveStep extends SpringBootRunner implements En {

  private static Boolean hasBeenExecutedOnce = false;
  private MObItemUOMSaveTestUtils utils;

  public MObItemUOMSaveStep() {

    When(
        "^\"([^\"]*)\" saves UnitOfMeasures section of Item with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String username, String itemCode, String saveTime, DataTable dataTable) -> {
          utils.freeze(saveTime);
          Cookie loginCookie = userActionsTestUtils.getUserCookie(username);
          JsonObject itemUOMsJsonObject = utils.createItemUOMJsonObject(itemCode, dataTable);
          Response saveItemUOMResponse =
              utils.saveItemUnitOfMeasureSection(loginCookie, itemUOMsJsonObject);
          userActionsTestUtils.setUserResponse(username, saveItemUOMResponse);
        });

    Then(
        "^Item with Code \"([^\"]*)\" is updated as follows:$",
        (String itemCode, DataTable dataTable) -> {
          utils.assertModificationDataIsUpdated(itemCode, dataTable);
        });

    Then(
        "^UnitOfMeasures section of Item with code \"([^\"]*)\" is updated as follows:$",
        (String itemCode, DataTable dataTable) -> {
          utils.assertDataIsUpdated(itemCode, dataTable);
        });

    Then(
        "^the following error message is attached to \"([^\"]*)\" field \"([^\"]*)\" and sent to \"([^\"]*)\" with codes:$",
        (String fieldName, String errorCode, String userName, DataTable dataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatAlternativeUnitFieldHasErrorMessageCode(
              response, errorCode, fieldName, dataTable);
        });

    When(
        "^\"([^\"]*)\" saves UoM section of Item with Code \"([^\"]*)\" with the following values:$",
        (String userName, String itemCode, DataTable dataTable) -> {
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          JsonObject itemUOMsJsonObject = utils.createItemUOMJsonObject(itemCode, dataTable);
          Response saveItemUOMResponse =
              utils.saveItemUnitOfMeasureSection(loginCookie, itemUOMsJsonObject);
          userActionsTestUtils.setUserResponse(userName, saveItemUOMResponse);
        });

    When(
        "^\"([^\"]*)\" saves UnitOfMeasures section of Item with code \"([^\"]*)\" at \"([^\"]*)\" with empty list$",
        (String userName, String itemCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          JsonObject itemUOMsJsonObject = utils.createEmptyItemUOMJsonObject(itemCode);
          Response saveItemUOMResponse =
              utils.saveItemUnitOfMeasureSection(loginCookie, itemUOMsJsonObject);
          userActionsTestUtils.setUserResponse(userName, saveItemUOMResponse);
        });

    And(
        "^UnitOfMeasures section of Item with code \"([^\"]*)\" is updated with empty list$",
        (String itemCode) -> {
          utils.assertThatUomSectionIsUpdatedWithEmptyList(itemCode);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemUOMSaveTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save UnitOfMeasures section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Save UnitOfMeasures section")) {
      userActionsTestUtils.unlockSectionForAllUsers(IMObItemRestUrls.UNLOCK_UNIT_OF_MEASURE_URL);
      utils.unfreeze();
    }
  }
}
