package com.ebs.dda.masterdata.vendor.utils;

import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class MObVendorReadNextPrevTestUtils extends MObVendorCommonTestUtils {

  public MObVendorReadNextPrevTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    return str.toString();
  }

  public String prepareDbScript() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/vendor/MObVendor_next_prev.sql");
    return str.toString();
  }

  public Response requestToViewPreviousVendor(Cookie userCookie, String currentVendorCode) {
    String restURL = "/services/masterdataobjects/vendors/previous/" + currentVendorCode;
    return sendGETRequest(userCookie, restURL);
  }

  public Response requestToViewNextVendor(Cookie userCookie, String currentVendorCode) {
    String restURL = "/services/masterdataobjects/vendors/next/" + currentVendorCode;
    return sendGETRequest(userCookie, restURL);
  }
}
