package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.customer.IMObCustomerRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class MObCustomerTypesTestUtils extends MObCustomerCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Types][dropDown]";

  public MObCustomerTypesTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readAllCustomerTypes(Cookie cookie) {
    String restURL = IMObCustomerRestURLs.READ_ALL_CUSTOMER_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatCustomerTypesResponseIsCorrect(
      DataTable customerTypesDataTable, Response response) {
    List<Map<String, String>> expectedCustomerTypesMapList =
        customerTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualCustomerTypesList = getListOfMapsFromResponse(response);
    Assert.assertEquals(expectedCustomerTypesMapList.size(), actualCustomerTypesList.size());
    for (int i = 0; i < expectedCustomerTypesMapList.size(); i++) {
      assertThatExpectedCustomerTypeMatchesActual(
          expectedCustomerTypesMapList.get(i), actualCustomerTypesList.get(i));
    }
  }

  private void assertThatExpectedCustomerTypeMatchesActual(
      Map<String, String> expectedCustomerType, Object actualCustomerType) {
    Assert.assertEquals(
        expectedCustomerType.get(IMasterDataFeatureFileCommonKeys.TYPE),
        actualCustomerType);
  }

  public void assertThatTotalNumberOfCustomerTypesEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualCustomerTypes = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of Customer Types is not correct",
        expectedTotalNumber.intValue(),
        actualCustomerTypes.size());
  }
}
