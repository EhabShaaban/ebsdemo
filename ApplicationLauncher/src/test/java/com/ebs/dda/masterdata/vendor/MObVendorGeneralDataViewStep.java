package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class MObVendorGeneralDataViewStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private MObVendorGeneralDataTestUtils utils;

  public MObVendorGeneralDataViewStep() {

    Given(
        "^the following GeneralData for Vendors exist:$",
        (DataTable vendorDataTable) -> {
          utils.assertThatVendorGeneralDataExist(vendorDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of Vendor with code \"([^\"]*)\"$",
        (String userName, String vendorCode) -> {
          String readUrl = IMObVendorRestUrls.VENDOR_VIEW_GENERAL_DATA + vendorCode;
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response vendorReadHeaderResponse = utils.sendGETRequest(userCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, vendorReadHeaderResponse);
        });

    Then(
        "^the following values of GeneralData section of Vendor with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String vendorCode, String userName, DataTable vendorGeneralData) -> {
          Response vendorReadHeaderResponse = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatResponseDataIsCorrect(vendorReadHeaderResponse, vendorGeneralData);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new MObVendorGeneralDataTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View Vendor GeneralData section,")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.prepareDbScript());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View Vendor GeneralData section,")) {
      utils.unfreeze();
    }
  }
}
