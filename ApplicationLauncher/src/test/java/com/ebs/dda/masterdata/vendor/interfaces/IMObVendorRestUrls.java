package com.ebs.dda.masterdata.vendor.interfaces;

public interface IMObVendorRestUrls {

  String COMMAND_VENDOR = "/command/vendor/";
  String UNLOCK_ALL_VENDOR = COMMAND_VENDOR + "unlock/lockedsections/";
  String LOCK_GENERAL_DATA_SECTION = COMMAND_VENDOR + "lockgeneraldata/";
  String UNLOCK_GENERAL_DATA_SECTION = COMMAND_VENDOR + "unlockgeneraldata/";
  String SAVE_VENDOR_GENERAL_DATA_URL = COMMAND_VENDOR + "update/generaldata/";

  String LOCK_ACCOUNTING_DETAILS_SECTION = COMMAND_VENDOR + "lockaccountingdetails/";
  String UNLOCK_ACCOUNTING_DETAILS_SECTION = COMMAND_VENDOR + "unlockaccountingdetails/";
  String UPDATE_ACCOUNTING_DETAILS_SECTION = COMMAND_VENDOR + "update/accountdetails/";

  String VENDOR_READ_ALL_URL = "/services/masterdataobjects/vendors";
  String VENDOR_VIEW_GENERAL_DATA = VENDOR_READ_ALL_URL + "/generaldata/";
  String DELETE_VENDOR = COMMAND_VENDOR + "delete/";

  String AUTHORIZED_ACTIONS = VENDOR_READ_ALL_URL + "/authorizedactions/";
  String READ_ACCOUNTING_INFO_RESTURL = VENDOR_READ_ALL_URL + "/accountinginfo/";
  String READ_ALL_VENDORS = VENDOR_READ_ALL_URL + "/readauthorizedvendors/";
  String READ_ALL_VENDORS_By_Business_Unit = VENDOR_READ_ALL_URL + "/";
  String READ_ALL_VENDOR_TYPES = VENDOR_READ_ALL_URL + "/gettypes/";

  String CREATE_VENDOR_URL = COMMAND_VENDOR + "create";
}
