package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.masterdata.item.IMObItemSectionNames;
import cucumber.api.DataTable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class MObItemRequestEditAccountingDetailsTestUtils extends IObItemAccountingDetailsCommonTestUtils {

  public MObItemRequestEditAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  @Override
  public List<String> getSectionsNames() {
    return new ArrayList<>(Arrays.asList(IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION));
  }

  public void assertThatAccountsExist(DataTable accountsDataTable) {
    List<Map<String, String>> accounts = accountsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> account : accounts) {
      assertThatAccountExists(account);
    }
  }

  private void assertThatAccountExists(Map<String, String> account) {
    HObChartOfAccountsGeneralModel accountsGeneralModel =
            (HObChartOfAccountsGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getChartOfAccounts", constructAccountQueryParams(account));

    assertNotNull("This Account Does not exist", accountsGeneralModel);
  }

  private Object[] constructAccountQueryParams(Map<String, String> account) {
    return new Object[] {
            account.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
            account.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME),
            account.get(IAccountingFeatureFileCommonKeys.LEVEL),
            "%\"" + account.get(IFeatureFileCommonKeys.STATE) + "\"%"
    };
  }
}
