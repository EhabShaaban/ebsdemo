package com.ebs.dda.masterdata.ivr;

public interface IExpectedKeys {

  String CREATION_DATE = "CreationDate";
  String IVR_CODE_KEY = "IVRCode";
  String ITEM_CODE_KEY = "ItemCode";
  String ITEM_NAME_KEY = "ItemName";
  String VENDOR_CODE_KEY = "VendorCode";
  String VENDOR_NAME_KEY = "VendorName";
  String UOM_CODE_KEY = "UOMCode";
  String UOM_SYMBOL_KEY = "UOMSymbol";
  String PURCHASE_UNIT_CODE_KEY = "PurchaseUnitCode";
  String PURCHASE_UNIT_NAME_KEY = "PurchaseUnitName";
  String ITEM_CODE_AT_VENDOR_KEY = "ItemCodeAtVendor";
  String OLD_ITEM_NUMBER_KEY = "OldItemReference";
  String PRICE_KEY = "Price";
  String CURRENCY_CODE_KEY = "CurrencyCode";
  String CURRENCY_NAME_KEY = "Currency";
}
