package com.ebs.dda.masterdata.purchaseresponsible;

import com.ebs.dda.SpringBootRunner;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class CObPurchaseResponsibleDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private CObPurchaseResponsibleDropDownTestUtils utils;

  public CObPurchaseResponsibleDropDownStep() {
    Given(
        "^the following are ALL PurchaseResponsibles that exist:$",
        (DataTable purchaseResponsibleDataTable) -> {
          utils.assertThatPurchaseResponsiblesExist(purchaseResponsibleDataTable);
        });
      Given(
              "^the total number of existing PurchaseResponsibles are (\\d+)$",
              (Integer recordsNumber) -> {
                  utils.assertThatAllPurchaseResponsiblesExist(recordsNumber);
              });
    When(
        "^\"([^\"]*)\" requests to read all PurchaseResponsibles$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.readAllPurchaseResponsibles());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following PurchaseResponsibles values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable purchaseResponsibleDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatResponseMatchesExpected(purchaseResponsibleDataTable, response);
        });

    Then(
        "^total number of PurchaseResponsibles returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
        });
  }

  @Before
  public void setup() throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new CObPurchaseResponsibleDropDownTestUtils(properties, entityManagerDatabaseConnector);

    databaseConnector.createConnection();
    if (!hasBeenExecuted) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      hasBeenExecuted = true;
    }
  }

  @After
  public void afterEachScenario() throws SQLException {
    databaseConnector.closeConnection();
  }
}
