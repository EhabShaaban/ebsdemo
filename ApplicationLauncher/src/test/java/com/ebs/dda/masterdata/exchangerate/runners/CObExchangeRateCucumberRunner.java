package com.ebs.dda.masterdata.exchangerate.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {

    "classpath:features/masterdata/exchange-rate/ER_AllowedActions.feature",
    "classpath:features/masterdata/exchange-rate/ER_Create_HP.feature",
    "classpath:features/masterdata/exchange-rate/ER_Create_Val.feature",
    "classpath:features/masterdata/exchange-rate/ER_Create_Auth.feature",
    "classpath:features/masterdata/exchange-rate/ER_ViewAll.feature",
    "classpath:features/masterdata/exchange-rate/ER_CBE_Update_HP.feature"

}, glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.exchangerate"}, strict = true,
    tags = "not @Future")
public class CObExchangeRateCucumberRunner {

}
