package com.ebs.dda.masterdata.vendor.utils;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.vendor.CObVendor;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.masterdata.MasterDataTestUtils;
import com.ebs.dda.masterdata.vendor.IMObVendorSectionNames;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorFeatureFileCommonKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.interfaces.IResponseKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IVendorExpectedKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MObVendorCommonTestUtils extends MasterDataTestUtils {
  private final String requestLocale = "en";
  public String VENDOR_JMX_LOCK_BEAN_NAME = "vendorLockCommandJMXBean";

  protected String ASSERTION_MSG = "[VENDOR] ";

  public MObVendorCommonTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  protected MObVendorGeneralModel assertThatVendorExists(Map<String, String> expectedVendor) {
    MObVendorGeneralModel mObVendorGeneralModel =
        (MObVendorGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorGeneralModel", constructVendorsQueryParams(expectedVendor));
    assertNotNull(
        ASSERTION_MSG + expectedVendor.get(IFeatureFileCommonKeys.VENDOR_CODE) + " Doesn't exist!",
        mObVendorGeneralModel);
    assertThatBusinessUnitsAreEqual(
        expectedVendor.get(IFeatureFileCommonKeys.VENDOR_CODE),
        getBusinessUnitCodes(expectedVendor.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(", ")),
        mObVendorGeneralModel.getPurchasingUnitCodes());

    return mObVendorGeneralModel;
  }

  private Object[] constructVendorsQueryParams(Map<String, String> vendor) {
    return new Object[] {
      vendor.get(IFeatureFileCommonKeys.VENDOR_CODE),
      vendor.get(IFeatureFileCommonKeys.VENDOR_NAME),
      vendor.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE),
      vendor.get(IMObVendorFeatureFileCommonKeys.OLD_VENDOR_REFERENCE),
      vendor.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR),
      vendor.get(IMObVendorFeatureFileCommonKeys.VENDOR_TYPE)
    };
  }

  public void assertThatVendorsExistByCodeAndNameAndPurchaseUnits(DataTable vendorsData) {
    List<List<String>> vendorsList = vendorsData.raw();
    for (int i = 1; i < vendorsList.size(); i++) {
      String code = vendorsList.get(i).get(0);
      String vendorName = vendorsList.get(i).get(1);
      String purchaseUnitCodes = vendorsList.get(i).get(2);
      assertThatVendorsExistByCodeAndNameAndPurchaseUnits(code, vendorName, purchaseUnitCodes);
    }
  }

  private void assertThatVendorsExistByCodeAndNameAndPurchaseUnits(
      String code, String vendorName, String purchaseUnitCodes) {
    MObVendorGeneralModel vendor =
        (MObVendorGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorByCodeAndName", code, vendorName);
    Assert.assertNotNull(ASSERTION_MSG + "Vendor with code " + code + " doesn't exists", vendor);
    String actualPurchaseUnitsCodes = vendor.getPurchasingUnitCodes();
    assertThatPurchaseUnitsCodeAreEquals(purchaseUnitCodes, actualPurchaseUnitsCodes);
  }

  public void assertThatVendorsExistByCodeAndNameAndPurchaseUnitsCodeNameConcat(
      DataTable vendorsData) {
    List<List<String>> vendorsList = vendorsData.raw();
    for (int i = 1; i < vendorsList.size(); i++) {
      String code = vendorsList.get(i).get(0);
      String vendorName = vendorsList.get(i).get(1);
      String purchaseUnitCodes = vendorsList.get(i).get(2);
      assertThatVendorExistsByCodeAndNameAndPurchaseUnitsNameCodeConcat(
          code, vendorName, purchaseUnitCodes);
    }
  }

  private void assertThatVendorExistsByCodeAndNameAndPurchaseUnitsNameCodeConcat(
      String code, String vendorName, String expectedPurchaseUnitCodeName) {
    String[] purchaseUnitsCodeNameList = expectedPurchaseUnitCodeName.split(",");
    String expectedPurchaseUniteCodes = "";
    for (String purchaseUnitCode : purchaseUnitsCodeNameList) {
      String kept = purchaseUnitCode.substring(0, purchaseUnitCode.indexOf(" - "));
      expectedPurchaseUniteCodes = expectedPurchaseUniteCodes.concat(kept + ",");
    }
    assertThatVendorsExistByCodeAndNameAndPurchaseUnits(
        code, vendorName, expectedPurchaseUniteCodes);
  }

  // TODO: To be removed and use assertThatVendorsExistByCodeAndNameAndPurchaseUnits after
  // TODO: refactoring feature files
  public void assertThatVendorExistsByCodeAndName(DataTable vendorsData) {
    List<List<String>> vendorsList = vendorsData.raw();
    for (int i = 1; i < vendorsList.size(); i++) {
      String code = vendorsList.get(i).get(0);
      String vendorName = vendorsList.get(i).get(1);

      MObVendorGeneralModel vendor =
          (MObVendorGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorByCodeAndName", code, vendorName);
      Assert.assertNotNull(ASSERTION_MSG + "Vendor with code " + code + " does not exist", vendor);
    }
  }

  //  TODO: To be moved to MasterData common
  public void assertThatItemVendorRecordsExist(DataTable ivrDataTable) {
    List<Map<String, String>> ivrList = ivrDataTable.asMaps(String.class, String.class);
    for (Map<String, String> ivrRow : ivrList) {
      String ivrCode = ivrRow.get(IFeatureFileCommonKeys.CODE);
      String itemCode = ivrRow.get(IFeatureFileCommonKeys.ITEM_CODE);
      String vendorCode = ivrRow.get(IFeatureFileCommonKeys.VENDOR_CODE);
      assertThatItemVendorExist(ivrCode, itemCode, vendorCode);
    }
  }

  private void assertThatItemVendorExist(String ivrCode, String itemCode, String vendorCode) {
    Object ivr =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getIVRByCodeAndItemCodeAndVendorCode", ivrCode, itemCode, vendorCode);
    Assert.assertNotNull(ASSERTION_MSG + " IVR with code " + ivrCode + " is not exist", ivr);
  }

  public void assertThatVendorHasAccountInfo(DataTable accountingInfoDataTable) throws Exception {
    List<Map<String, String>> vendorAccountInfoMaps =
        accountingInfoDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedVendorAccountInfoMap : vendorAccountInfoMaps) {
      IObVendorAccountingInfoGeneralModel accountingInfoGeneralModel =
          getIObAccountingInfoGeneralModel(
              expectedVendorAccountInfoMap.get(IFeatureFileCommonKeys.VENDOR_CODE));
      ObjectMapper mapperObj = new ObjectMapper();
      String localizedString =
          mapperObj.writeValueAsString(accountingInfoGeneralModel.getAccountName());
      assertEquals(
          ASSERTION_MSG + IMObVendorFeatureFileCommonKeys.ACCOUNT_NAME + " Does not exist",
          expectedVendorAccountInfoMap.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_NAME),
          getLocaleFromLocalizedResponse(localizedString, requestLocale));

      assertEquals(
          ASSERTION_MSG + IMObVendorFeatureFileCommonKeys.ACCOUNT_CODE + " Does not exist",
          expectedVendorAccountInfoMap.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_CODE),
          accountingInfoGeneralModel.getAccountCode());
    }
  }

  protected IObVendorAccountingInfoGeneralModel getIObAccountingInfoGeneralModel(
      String vendorCode) {
    return (IObVendorAccountingInfoGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getVendorAccountingDetails", vendorCode);
  }

  public Response deleteVendorByCode(Cookie loginCookie, String vendorCode) {
    String deleteURL = IMObVendorRestUrls.DELETE_VENDOR + vendorCode;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  public void assertThatVendorDataIsCorrect(DataTable vendorDataTable, Response response)
      throws Exception {
    List<Map<String, String>> expectedVendorData =
        vendorDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualVendorDataList = getListOfMapsFromResponse(response);

    assertEquals(
        ASSERTION_MSG + "Expected size differs from actual size",
        expectedVendorData.size(),
        actualVendorDataList.size());
    for (int i = 0; i < expectedVendorData.size(); i++) {
      assertThatResponseDataIsCorrect(expectedVendorData.get(i), actualVendorDataList.get(i));
    }
  }

  private void assertThatResponseDataIsCorrect(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {

    assertEquals(
        ASSERTION_MSG + IVendorExpectedKeys.VENDOR_CODE + " Doesn't equal",
        actualMap.get(IResponseKeys.VENDOR_CODE),
        expectedMap.get(IFeatureFileCommonKeys.VENDOR_CODE));

    assertEquals(
        ASSERTION_MSG + IVendorExpectedKeys.VENDOR_TYPE + " Doesn't equal",
        expectedMap.get(IMObVendorFeatureFileCommonKeys.VENDOR_TYPE).split(" - ")[0],
        actualMap.get(IMObVendorGeneralModel.VENDOR_TYPE_CODE));

    assertEquals(
        ASSERTION_MSG + IVendorExpectedKeys.VENDOR_TYPE + " Doesn't equal",
        expectedMap.get(IMObVendorFeatureFileCommonKeys.VENDOR_TYPE).split(" - ")[1],
        getEnglishLocaleFromLocalizedResponse(
            actualMap.get(IMObVendorGeneralModel.VENDOR_TYPE_NAME).toString()));

    assertThatVendorNameIsCorrect(expectedMap, actualMap);

    assertThatPurchaseResponsibleIsCorrect(expectedMap, actualMap);

    orderActualBusinessUnits(actualMap);

    assertThatPurchaseUnitsAreEquals(expectedMap, actualMap);

    assertEquals(
        ASSERTION_MSG + IVendorExpectedKeys.OLD_VENDOR_REFERENCE + " Doesn't equal",
        expectedMap.get(IMObVendorFeatureFileCommonKeys.OLD_VENDOR_REFERENCE),
        actualMap.get(IMObVendorGeneralModel.OLD_VENDOR_NUMBER));

    assertEquals(
        ASSERTION_MSG + IVendorExpectedKeys.ACCOUNT_WITH_VENDOR + " Doesn't equal",
        expectedMap.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR),
        actualMap.get(IMObVendorGeneralModel.ACCOUNT_WITH_VENDOR));
  }

  public void assertThatAllVendorsExist(Integer recordsNumber) {
    Long vendorsCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("getVendorsCount");
    assertEquals(
        ASSERTION_MSG + "Total number of vendors not equals",
        Long.valueOf(recordsNumber),
        vendorsCount);
  }

  protected void orderActualBusinessUnits(Map<String, Object> actualMap) {
    String vendorPurchasingUnits = (String) actualMap.get(IMObVendorGeneralModel.PURCHASING_UNIT);
    actualMap.replace(
        IMObVendorGeneralModel.PURCHASING_UNIT, getOrderBusinessUnits(vendorPurchasingUnits));
  }

  protected String getOrderBusinessUnits(String vendorPurchasingUnits) {
    String[] purchasingUnits = vendorPurchasingUnits.split(",");
    Arrays.sort(
        purchasingUnits,
        Comparator.comparing(String::length).thenComparing(String::compareToIgnoreCase));

    StringBuilder sortedPurchasingUnits = new StringBuilder();
    for (String purchaseUnit : purchasingUnits) {
      sortedPurchasingUnits.append(purchaseUnit.trim()).append(", ");
    }
    String sortedPurchasingUnitStr =
        StringUtils.removeEnd(String.valueOf(sortedPurchasingUnits), ", ");
    return sortedPurchasingUnitStr;
  }

  private void assertThatVendorNameIsCorrect(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {
    String expectedValue = expectedMap.get(IFeatureFileCommonKeys.VENDOR_NAME);
    ObjectMapper mapperObj = new ObjectMapper();
    String actualLocalizedValueAsString =
        mapperObj.writeValueAsString(actualMap.get(IMObVendorGeneralModel.VENDOR_NAME));
    String actualValue =
        getLocaleFromLocalizedResponse(actualLocalizedValueAsString, requestLocale);

    Assert.assertEquals(
        ASSERTION_MSG + IFeatureFileCommonKeys.VENDOR_NAME + " Doesn't equal",
        expectedValue,
        actualValue);
  }

  private void assertThatPurchaseResponsibleIsCorrect(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {
    String expectedValue =
        expectedMap.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE).split(" - ")[1];
    ObjectMapper mapperObj = new ObjectMapper();
    String actualLocalizedValueAsString =
        mapperObj.writeValueAsString(
            actualMap.get(IMObVendorGeneralModel.PURCHASING_RESPONSIBLE_NAME));
    String actualValue = getEnglishLocaleFromLocalizedResponse(actualLocalizedValueAsString);

    Assert.assertEquals(
        ASSERTION_MSG + IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE + " Doesn't equal",
        expectedValue,
        actualValue);
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(Arrays.asList(IMObVendorSectionNames.GENERAL_DATA_SECTION));
  }

  public void assertThatVendorTypesExist(DataTable vendorTypesDataTable) {
    List<Map<String, String>> vendorTypesList =
        vendorTypesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendorType : vendorTypesList) {
      CObVendor cObVendor =
          (CObVendor)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorTypeByCode", vendorType.get(IFeatureFileCommonKeys.CODE));
      Assert.assertNotNull(ASSERTION_MSG + "VendorType does not exist", cObVendor);
    }
  }

  protected void assertThatPurchaseUnitsAreEquals(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    MapAssertion mapAssertion = new MapAssertion(expectedMap, actualMap);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT, IMObVendorGeneralModel.PURCHASING_UNIT);
  }

  public void assertThatResourceLockedByUserIsReleased(
      String userName, String vendorCode, String sectionName) throws Exception {
    Assert.assertFalse(
        ASSERTION_MSG + "The Lock is not released ",
        isSectionLockedByUser(userName, vendorCode, sectionName));
  }

  public void assertThatResourceIsLockedByUser(
      String userName, String vendorCode, String sectionName) throws Exception {
    Assert.assertTrue(
        ASSERTION_MSG + "The Section is not locked by user ",
        isSectionLockedByUser(userName, vendorCode, sectionName));
  }

  private boolean isSectionLockedByUser(String userName, String vendorCode, String sectionName)
      throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=" + VENDOR_JMX_LOCK_BEAN_NAME);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, vendorCode};
    String[] parameterTypes =
        new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
        (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);

    return null != lockReleaseDetails;
  }

  public void assertThatVendorsHaveTheFollowingTaxes(DataTable vendorTaxesDataTable) {
    List<Map<String, String>> vendorTaxesMap =
        vendorTaxesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendorTaxMap : vendorTaxesMap) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createLObVendorTax", constructVendorTaxesQueryParameters(vendorTaxMap));
    }
  }

  private Object[] constructVendorTaxesQueryParameters(Map<String, String> vendorTaxMap) {
    return new Object[] {
      vendorTaxMap.get(IAccountingFeatureFileCommonKeys.TAX),
      vendorTaxMap.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE),
      vendorTaxMap.get(IFeatureFileCommonKeys.VENDOR)
    };
  }

  public void createVendorGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createVendorGeneralData", constructInsertVendorGeneralDataQueryParams(generalDataMap));
    }
  }

  private Object[] constructInsertVendorGeneralDataQueryParams(Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.STATE),
      generalDataMap.get(IFeatureFileCommonKeys.VENDOR_NAME),
      generalDataMap.get(IFeatureFileCommonKeys.VENDOR_NAME),
      generalDataMap.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE),
      generalDataMap.get(IMObVendorFeatureFileCommonKeys.OLD_VENDOR_REFERENCE),
      generalDataMap.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR),
      generalDataMap.get(IMObVendorFeatureFileCommonKeys.VENDOR_TYPE),
    };
  }

  public void createVendorBusinessUnits(DataTable businessUnitsTable) {
    List<Map<String, String>> businessUnitsMaps =
        businessUnitsTable.asMaps(String.class, String.class);
    for (Map<String, String> businessUnitMap : businessUnitsMaps) {
      String code = businessUnitMap.get(IFeatureFileCommonKeys.CODE);
      String businessUnits = businessUnitMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
      insertVendorBusinessUnits(code, businessUnits);
    }
  }

  private void insertVendorBusinessUnits(String code, String businessUnits) {
    String[] businessUnitList = businessUnits.split(",");
    for (String businessUnit : businessUnitList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createVendorBusinessUnit", code, businessUnit.trim(), businessUnit.trim());
    }
  }
}
