package com.ebs.dda.masterdata.vendor.utils;

import static org.junit.Assert.assertEquals;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.vendor.ICObVendor;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CObVendorTypesReadAllDropDownTestUtils extends MObVendorCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[VendorTypes] ";

  public CObVendorTypesReadAllDropDownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/vendor/CObVendor.sql");
    return str.toString();
  }

  public void assertThatActualVendorTypesAreReturned(
      Response response, DataTable vendorTypesDataTable) {
    List<Map<String, String>> expectedVendorTypesList =
        vendorTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualVendorTypesList = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedVendorTypesList.size(); i++) {
      assertEquals(
          ASSERTION_MSG + "Does not equal",
          expectedVendorTypesList.get(i).get(IFeatureFileCommonKeys.CODE),
          actualVendorTypesList.get(i).get(ICObVendor.CODE));
    }
  }

  public void assertThatVendorTypesCountEquals(Integer recordsNumber) {
    Long vendorTypesCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorTypesCount");
    assertEquals(
        ASSERTION_MSG + "Total count not equals", Long.valueOf(recordsNumber), vendorTypesCount);
  }
}
