package com.ebs.dda.masterdata.exchangerate;

import org.springframework.beans.factory.annotation.Autowired;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.commands.masterdata.exchagerate.CObExchangeRatesUpdateCommandRemotly;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCBEUpdateTestUtils;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCreateCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class ExchangeRateCBEUpdateSteps extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "Update ExchangeRate";
  private CObExchangeRateCBEUpdateTestUtils util;

  @Autowired
  private CObExchangeRatesUpdateCommandRemotly exRatesUpdateCmd;

  public ExchangeRateCBEUpdateSteps() {
    Given("^the following ExchangeRate of the Central Bank of Egypt:$", (DataTable exRatesDT) -> {
      util.mockCBERemoteServerExchangeRates(exRatesUpdateCmd, exRatesDT);
    });

    Then(
        "^The system retrieve currency prices from CBE and Create ExchangeRates with following values:$",
        (DataTable exRatesDT) -> {
          exRatesUpdateCmd.executeCommand(null);
          util.assertThatExchangeRatesAreSaved(exRatesDT);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    util = new CObExchangeRateCBEUpdateTestUtils(getProperties(), entityManagerDatabaseConnector);
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector
          .executeSQLScript(CObExchangeRateCreateCommonTestUtils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(CObExchangeRateCreateCommonTestUtils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
      util.unfreeze();
    }
  }
}
