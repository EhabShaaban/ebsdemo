package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.MObCustomerCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObCustomerAllowedActionsObjectScreenStep extends SpringBootRunner
    implements En {

  private MObCustomerCommonTestUtils utils;

  public MObCustomerAllowedActionsObjectScreenStep() {
    When(
        "^\"([^\"]*)\" requests to read actions of Customer with code \"([^\"]*)\"$",
        (String userName, String customerCode) -> {
          String url = IMObCustomerRestURLs.AUTHORIZED_ACTIONS + customerCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new MObCustomerCommonTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Customer Object Screen,")) {
      databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
    }
  }

}
