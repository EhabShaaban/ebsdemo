package com.ebs.dda.masterdata.exchangerate.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class CObExchangeRateCommonTestUtils extends CommonTestUtils {

  public CObExchangeRateCommonTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertThatTheFollowingExchangeRatesExist(DataTable erDataTable) {
    List<Map<String, String>> exchangeRatesListMap = erDataTable.asMaps(String.class, String.class);
    for (Map<String, String> exchangeRateMap : exchangeRatesListMap) {
      CObExchangeRate cObExchangeRate =
              (CObExchangeRate)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getExchangeRateByFirstAndSecondCurrencies",
                              constructExchangeRatesQueryParams(exchangeRateMap));
      Assert.assertNotNull(
              "Exchange Rate doesn't exist " + exchangeRatesListMap.toString(), cObExchangeRate);
    }
  }

  private Object[] constructExchangeRatesQueryParams(Map<String, String> exchangeRateMap) {
    return new Object[]{
            exchangeRateMap.get(IFeatureFileCommonKeys.CODE),
            exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.FIRST_CURRENCY_ISO),
            exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_CURRENCY_ISO),
            Double.parseDouble(exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE)),
            exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.VALID_FROM)
    };
  }

  public void assertTotalNumberOfExchangeRatesExist(
          Integer expectedERCount, String FirstCurrencyIso, String SecondCurrencyIso) {
    Long actuaERCount =
            (Long)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getExchangeRatesCount", FirstCurrencyIso, SecondCurrencyIso);
    Assert.assertEquals(Long.valueOf(expectedERCount), actuaERCount);
  }

  public void assertNoExchangeRatesExist(String FirstCurrencyIso, String SecondCurrencyIso) {
    Long actuaERCount =
            (Long)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getExchangeRatesCount", FirstCurrencyIso, SecondCurrencyIso);
    Assert.assertEquals(Long.valueOf(0), actuaERCount);
  }
}
