package com.ebs.dda.masterdata.vendor.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorAccountingInfoValueObject;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.masterdata.vendor.IMObVendorSectionNames;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.interfaces.IVendorExpectedKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MObVendorUpdateAccountingDetailsTestUtils extends MObVendorCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[AccountingDetails][Update] ";

  public MObVendorUpdateAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    return str.toString();
  }

  public String prepareDbScript() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    return str.toString();
  }

  @Override
  public List<String> getSectionsNames() {
    return new ArrayList<>(Arrays.asList(IMObVendorSectionNames.ACCOUNTING_DETAILS_SECTION));
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, VENDOR_JMX_LOCK_BEAN_NAME, sectionName);
  }

  public void assertThatAccountsExist(DataTable accountsDataTable) {
    List<Map<String, String>> accounts = accountsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> account : accounts) {
      assertThatAccountExists(account);
    }
  }

  private void assertThatAccountExists(Map<String, String> account) {
    HObChartOfAccountsGeneralModel accountsGeneralModel =
        (HObChartOfAccountsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getChartOfAccounts",
                account.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
                account.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME),
                account.get(IAccountingFeatureFileCommonKeys.LEVEL),
                "%\"" + account.get(IFeatureFileCommonKeys.STATE) + "\"%");

    assertNotNull(ASSERTION_MSG + "This Account Does not exist", accountsGeneralModel);
  }

  public Response saveVendorAccountingDetailsSection(
      Cookie loginCookie, String vendorCode, DataTable AccountInfoDataTable) {
    JsonObject AccountInfoJsonData = getAccountInfoJsonObject(AccountInfoDataTable);

    String restURL = IMObVendorRestUrls.UPDATE_ACCOUNTING_DETAILS_SECTION + vendorCode;
    return sendPUTRequest(loginCookie, restURL, AccountInfoJsonData);
  }

  private JsonObject getAccountInfoJsonObject(DataTable AccountInfoTable) {
    Map<String, String> expectedAccountInfoMap =
        AccountInfoTable.asMaps(String.class, String.class).get(0);
    JsonObject AccountInfoData = new JsonObject();
    AccountInfoData.addProperty(
        IMObVendorAccountingInfoValueObject.ACCOUNT_CODE,
        expectedAccountInfoMap.get(IVendorExpectedKeys.ACCOUNT));
    return AccountInfoData;
  }

  public void assertThatIObVendorAccountUpdatedWithGivenValues(
      String vendorCode, DataTable AccountInfoDataTable) {
    Map<String, String> accountInfoMap =
        AccountInfoDataTable.asMaps(String.class, String.class).get(0);

    IObVendorAccountingInfoGeneralModel accountingInfoGeneralModel =
        getIObAccountingInfoGeneralModel(vendorCode);

    assertEquals(ASSERTION_MSG +
        IVendorExpectedKeys.ACCOUNT + " Doesn't equal",
        accountInfoMap.get(IVendorExpectedKeys.ACCOUNT),
        accountingInfoGeneralModel.getAccountCode());

    assertDateEquals(
            accountInfoMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            accountingInfoGeneralModel.getModifiedDate());

    assertEquals(ASSERTION_MSG +
        IFeatureFileCommonKeys.LAST_UPDATED_BY + " Doesn't equal",
        accountInfoMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
        accountingInfoGeneralModel.getModificationInfo());
  }
}
