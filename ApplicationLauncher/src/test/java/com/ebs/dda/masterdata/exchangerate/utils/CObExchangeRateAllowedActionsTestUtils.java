package com.ebs.dda.masterdata.exchangerate.utils;

import java.util.Map;

public class CObExchangeRateAllowedActionsTestUtils extends CObExchangeRateCommonTestUtils {

  public CObExchangeRateAllowedActionsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",db-scripts/CObCurrencySqlScript.sql");
    str.append(",db-scripts/master-data/exchange-rate/exchange-rate-type.sql");
    str.append(",db-scripts/master-data/exchange-rate/exchange-rate-view-all.sql");
    return str.toString();
  }
}
