package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.masterdata.customer.IMObCustomerRestURLs;

import java.util.Map;

import static org.junit.Assert.assertNull;

public class MObCustomerDeleteTestUtils extends MObCustomerCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Delete]";

  public MObCustomerDeleteTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getDeleteSalesCustomerRestUrl(String customerCode) {
    StringBuilder deleteUrl = new StringBuilder();
    deleteUrl.append(IMObCustomerRestURLs.COMMAND);
    deleteUrl.append("/");
    deleteUrl.append(customerCode);
    return deleteUrl.toString();
  }

  public void assertThatCustomerIsDeletedSuccessfully(String customerCode) {
    Object actualCustomer =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCustomerByCode", customerCode);
    assertNull(ASSERTION_MSG + "Customer does not deleted successfully", actualCustomer);
  }
}
