package com.ebs.dda.masterdata.itemgroup.readall;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.masterdata.itemgroup.CObItemGroupFeatureTestUtils;
import com.ebs.dda.masterdata.itemgroup.IResponseKeys;
import com.ebs.dda.masterdata.itemgroup.ItemGroupFeatureFileKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;

public class CObItemGroupReadAllUtil extends CObItemGroupFeatureTestUtils {

  private String requestlocale = "en";
  private ObjectMapper objectMapper;

  public CObItemGroupReadAllUtil(Map<String, Object> properties) {
    super(properties);
    objectMapper = new ObjectMapper();
  }

  public void assertItemGroupDataIsCorrect(DataTable ItemGroupData, Response response)
      throws JSONException, IOException {
    List<Map<String, String>> ItemGroupList = ItemGroupData.asMaps(String.class, String.class);
    List<HashMap<String, Object>> ItemGroupDataResponse =
        response.body().jsonPath().getList("data");
    for (int i = 1; i < ItemGroupList.size(); i++) {
      assertCorrectShownData(ItemGroupDataResponse.get(i - 1), ItemGroupList.get(i - 1));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> responsedItemGroup, Map<String, String> itemGroupRowMap)
      throws IOException, JSONException {

    String code = itemGroupRowMap.get(IFeatureFileCommonKeys.CODE);
    String name = itemGroupRowMap.get(IFeatureFileCommonKeys.NAME);
    String groupLevel = itemGroupRowMap.get(ItemGroupFeatureFileKeys.GROUP_LEVEL);
    String parentCode = itemGroupRowMap.get(ItemGroupFeatureFileKeys.PARENT_CODE);
    String parentName = itemGroupRowMap.get(ItemGroupFeatureFileKeys.PARENT_NAME);

    assertEquals(code, getAsString(responsedItemGroup.get(IResponseKeys.CODE)));
    assertEquals(parentCode, getAsString(responsedItemGroup.get(IResponseKeys.PARENT_CODE)));
    assertEquals(groupLevel, getEnumValue(responsedItemGroup.get(IResponseKeys.GROUP_LEVEL)));

    String LocalizedName = getLocalizedValue(responsedItemGroup.get(IResponseKeys.NAME));
    assertEquals(name, getLocaleFromLocalizedResponse(LocalizedName, requestlocale));

    String LocalizedParentName =
        getLocalizedValue(responsedItemGroup.get(IResponseKeys.PARENT_NAME));

    assertEquals(parentName, getLocaleFromLocalizedResponse(LocalizedParentName, requestlocale));
  }

  private String getAsString(Object obj) {
    return obj.toString();
  }

  private Object getEnumValue(Object enumObject) {
    return ((HashMap) enumObject).get(CommonKeys.ENUM_VALUE);
  }

  private String getLocalizedValue(Object obj) throws IOException {
    return objectMapper.writeValueAsString(obj);
  }
}
