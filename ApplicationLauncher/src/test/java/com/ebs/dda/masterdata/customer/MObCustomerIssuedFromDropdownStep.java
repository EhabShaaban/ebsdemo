package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.MObCustomerIssuedFromTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObCustomerIssuedFromDropdownStep extends SpringBootRunner implements En {

  private MObCustomerIssuedFromTestUtils utils;

  public MObCustomerIssuedFromDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all IssuedFrom$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllIssuedFrom(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following IssuedFrom values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable issuedFromDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatIssuedFromResponseIsCorrect(issuedFromDataTable, response);
        });

    Given(
        "^the following IssuedFrom exist:$",
        (DataTable issuedFromDataTable) -> {
          utils.assertThatIssuedFromExist(issuedFromDataTable);
        });

    Then(
        "^total number of IssuedFrom returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer customerTypesCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfIssuedFromEquals(customerTypesCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObCustomerIssuedFromTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of IssuedFrom dropdown")) {
            databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
        }
    }
}
