package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.IMObItemCreateValueObject;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class MObItemCreateTestUtils extends MObItemCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Create]";

  public MObItemCreateTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("db-scripts/clearDataSqlScript.sql");
    stringBuilder.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    stringBuilder.append(",").append("db-scripts/CObMeasureScript.sql");
    stringBuilder.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    stringBuilder.append(",").append("db-scripts/master-data/item/MasterData.sql");
    stringBuilder.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    stringBuilder.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    stringBuilder.append(",").append("db-scripts/master-data/item/CObItem.sql");
    stringBuilder.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    return stringBuilder.toString();
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    return str.toString();
  }

  public JsonObject prepareItemJsonObject(DataTable itemGeneralData) {
    List<String> items = itemGeneralData.raw().get(1);
    JsonObject itemData = new JsonObject();
    itemData.addProperty(IMObItemCreateValueObject.ITEM_NAME, items.get(0));
    itemData.addProperty(IMObItemCreateValueObject.ITEM_GROUP_CODE, convertEmptyStringToNull(items.get(1)));
    itemData.addProperty(IMObItemCreateValueObject.ITEM_BASE_UNIT_Code, items.get(2));
    setBatchManagedProperty(itemData, items.get(3));
    itemData.addProperty(IMObItemCreateValueObject.ITEM_OLD_NUMBER, items.get(4));
    JsonArray itemPurchaseUnits = prepareItemUnitsJsonArray(Arrays.asList(items.get(5).split(",")));
    itemData.add(IMObItemCreateValueObject.PURCHASE_UNITS, itemPurchaseUnits);
    itemData.addProperty(IMObItemCreateValueObject.MARKET_NAME, items.get(6));
    itemData.addProperty(IMObItemCreateValueObject.ITEM_TYPE, items.get(7));
    return itemData;
  }

  public void assertThatItemExists(DataTable createdItemValues) {
    Map<String, String> createdItem = createdItemValues.asMaps(String.class, String.class).get(0);

    MObItemGeneralModel itemGeneralModel =
        (MObItemGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCreatedItem", constructCreateItemParams(createdItem));
    assertNotNull(ASSERTION_MSG + " Item is not created",itemGeneralModel);
    assertThatPurchaseUnitsCodeAreEquals(
        createdItem.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
        itemGeneralModel.getPurchasingUnitCodes());
  }

  private JsonArray prepareItemUnitsJsonArray(List<String> purchaseUnitCodes) {
    JsonArray itemPurchaseUnits = new JsonArray();
    if (!purchaseUnitCodes.isEmpty()) {
      for (String purchaseUnit : purchaseUnitCodes) {
        JsonObject itemPurchaseUnit = new JsonObject();
        itemPurchaseUnit.addProperty(PURCHASE_UNIT_CODE, purchaseUnit.trim());
        itemPurchaseUnits.add(itemPurchaseUnit);
      }
    }
    return itemPurchaseUnits;
  }

  private Object[] constructCreateItemParams(Map<String, String> createdItemValues) {
    return new Object[] {
            createdItemValues.get(IMasterDataFeatureFileCommonKeys.ITEM_GROUP),
            createdItemValues.get(IFeatureFileCommonKeys.CREATED_BY),
            createdItemValues.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            createdItemValues.get(IFeatureFileCommonKeys.CREATION_DATE),
            createdItemValues.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            createdItemValues.get(IFeatureFileCommonKeys.ITEM_CODE),
            createdItemValues.get(IFeatureFileCommonKeys.NAME),
            createdItemValues.get(IMasterDataFeatureFileCommonKeys.BASE_UNIT),
            createdItemValues.get(IMasterDataFeatureFileCommonKeys.OLD_ITEM_REFERENCE),
            createdItemValues.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
            createdItemValues.get(IFeatureFileCommonKeys.TYPE)
    };
  }

  private void setBatchManagedProperty(JsonObject itemData, String itemBatchManagementRequired) {
    if (itemBatchManagementRequired.equals("True") || itemBatchManagementRequired.equals("False")) {
      itemData.addProperty(IMObItemCreateValueObject.ITEM_BATCH_MANAGED, Boolean.parseBoolean(itemBatchManagementRequired));
    }
  }
}
