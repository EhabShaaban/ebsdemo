package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import com.ebs.dda.masterdata.item.utils.MObItemViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemViewAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemViewAllTestUtils utils;

  public MObItemViewAllStep() {
    Given(
        "^the following ViewAll data for Items exist:$",
        (DataTable itemViewAllDataTable) -> {
          utils.assertThatViewAllItemsExist(itemViewAllDataTable);
        });

    Given(
        "^the total number of existing Items are (\\d+)$",
        (Integer recordsCount) -> {
          utils.assertTotalNumberOfItemsExist(recordsCount);
        });

    When(
        "^\"([^\"]*)\" requests to view all Items in Home Screen$",
        (String userName) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restURL = IMObItemRestUrls.READ_ALL_ITEMS_URLS;
          Response response = utils.sendGETRequest(userCookie, restURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\" in Home Screen:$",
        (String userName, DataTable expectedItemsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertItemResponseCorrect(response, expectedItemsDataTable);
        });
    Then(
        "^the total number of records presented to \"([^\"]*)\" in Home screen are (\\d+)$",
        (String userName, Integer recordsCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsReturned(response, recordsCount);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable expectedItemsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.withLocale(utils.getRequestLocale());
          utils.assertItemResponseCorrect(response, expectedItemsDataTable);
        });

    And(
        "^the total number of records presented to \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with filter applied on ItemCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String itemCode, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String itemCodeFilter = utils.getContainsFilterField("userCode", itemCode);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IMObItemRestUrls.ITEM_READ_ALL_URL,
                  pageNumber,
                  rowsNumber,
                  itemCodeFilter);
          userActionsTestUtils.setUserResponse(username, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with filter applied on ItemType which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String username,
            Integer pageNumber,
            String itemTypeName,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String itemTypeFilter =
              utils.getEqualsFilterField(IMObItemGeneralModel.ITEM_TYPE_NAME, itemTypeName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObItemRestUrls.ITEM_READ_ALL_URL,
                  pageNumber,
                  rowsNumber,
                  itemTypeFilter,
                  locale);
          userActionsTestUtils.setUserResponse(username, response);
        });
    And(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer pageSize) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, pageSize);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with filter applied on Name which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName, Integer pageNumber, String itemName, Integer pageSize, String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String itemNameFilter = utils.getContainsFilterField("itemName", itemName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObItemRestUrls.ITEM_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  itemNameFilter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with filter applied on ItemGroup which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String itemGroupName,
            Integer pageSize,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String itemGroupNameFilter = utils.getContainsFilterField("itemGroupName", itemGroupName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObItemRestUrls.ITEM_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  itemGroupNameFilter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with filter applied on  BaseUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName, Integer pageNumber, String baseUnit, Integer pageSize, String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String baseUnitFilter = utils.getContainsFilterField("basicUnitOfMeasureName", baseUnit);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObItemRestUrls.ITEM_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  baseUnitFilter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with filter applied on PurchaseUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String purchaseUnitName,
            Integer recordsNumberPerPage,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String purchaseUnitNameFilter =
              utils.getContainsFilterField("purchasingUnitNames", purchaseUnitName);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IMObItemRestUrls.ITEM_READ_ALL_URL,
                  pageNumber,
                  recordsNumberPerPage,
                  purchaseUnitNameFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with filter applied on BatchManaged which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String itemBatch, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String itemBatchFilter = utils.getEqualsFilterField("isBatchManaged", itemBatch);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IMObItemRestUrls.ITEM_READ_ALL_URL,
                  pageNumber,
                  rowsNumber,
                  itemBatchFilter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Items with (\\d+) records per page and the following filters applied on Item while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            Integer pageSize,
            String locale,
            DataTable filters) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getFilters(filters);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IMObItemRestUrls.ITEM_READ_ALL_URL, pageNumber, pageSize, filter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemViewAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all Items")
        || contains(scenario.getName(), "Filter Items")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
