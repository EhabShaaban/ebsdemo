package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MObItemReadAuthorizedItemsTestUtils extends MObItemCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Item][Dropdown][ViewAll]";

  public MObItemReadAuthorizedItemsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertThatAuthorizedItemsAreReturned(Response response, DataTable itemsDataTable) {
    assertResponseSuccessStatus(response);

    List<Map<String, String>> expectedItemsList = itemsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItemsList = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedItemsList.size(); i++) {
      assertThatActualAccountMatchesExpected(expectedItemsList.get(i), actualItemsList.get(i));
    }
  }

  private void assertThatActualAccountMatchesExpected(
      Map<String, String> expectedItem, HashMap<String, Object> actualItem) {
    assertEquals(
        ASSERTION_MSG + "Item Code is not correct",
        expectedItem.get(ISalesFeatureFileCommonKeys.ITEM).split(" - ")[0],
        actualItem.get(IMObItemGeneralModel.CODE));
    assertStringArrayEqualLocalizedStringArray(
        ASSERTION_MSG + "Item Name is not correct",
        expectedItem.get(ISalesFeatureFileCommonKeys.ITEM).split(" - ")[1],
        actualItem.get(IMObItemGeneralModel.ITEM_NAME).toString());
    assertEquals(
        ASSERTION_MSG + "UOM Code is not correct",
        expectedItem.get(ISalesFeatureFileCommonKeys.UOM).split(" - ")[0],
        actualItem.get(IMObItemGeneralModel.BASIC_UOM_CODE));
    assertStringArrayEqualLocalizedStringArray(
        ASSERTION_MSG + "UOM Symbol is not correct",
        expectedItem.get(ISalesFeatureFileCommonKeys.UOM).split(" - ")[1],
        actualItem.get(IMObItemGeneralModel.BASIC_UOM_SYMBOL).toString());
    assertThatBusinessUnitsAreEqual(
        expectedItem.get(IFeatureFileCommonKeys.ITEM_CODE),
        getBusinessUnitCodes(expectedItem.get(IFeatureFileCommonKeys.PURCHASE_UNIT).split(", ")),
        actualItem.get(IMObItemGeneralModel.PURCHASE_UNIT_CODES).toString());
  }
}
