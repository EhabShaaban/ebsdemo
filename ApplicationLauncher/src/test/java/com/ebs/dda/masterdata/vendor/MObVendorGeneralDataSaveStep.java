package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorGeneralDataTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;

public class MObVendorGeneralDataSaveStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private MObVendorGeneralDataTestUtils utils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  public MObVendorGeneralDataSaveStep() {

    Given(
        "^the following purchase responsible exist:$",
        (DataTable purchaseResponsiblesData) -> {
          enterpriseTestUtils.assertThatPurchaseResponsiblesExist(purchaseResponsiblesData);
        });

    Given(
        "^the following IVR exist:$",
        (DataTable iVRDataTable) -> {
          utils.assertThatItemVendorRecordsExistsByVendorAndItemCodes(iVRDataTable);
        });

    When(
        "^\"([^\"]*)\" saves GeneralData section of Vendor with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String username, String vendorCode, String dateTime, DataTable dataTable) -> {
          utils.freeze(dateTime);
          JsonObject vendorJsonObject =
              utils.createVendorGeneralDataJsonObject(vendorCode, dataTable);
          Response response =
              utils.saveVendorGeneralDataSection(
                  userActionsTestUtils.getUserCookie(username), vendorJsonObject, vendorCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^Vendor with code \"([^\"]*)\" is updated as follows:$",
        (String vendorCode, DataTable dataTable) -> {
          utils.assertThatGeneralDataIsUpdated(vendorCode, dataTable);
        });

    When(
        "^\"([^\"]*)\" saves GeneralData section of Vendor with code \"([^\"]*)\" with the following values:$",
        (String username, String vendorCode, DataTable dataTable) -> {
          JsonObject vendorJsonObject =
              utils.createVendorGeneralDataJsonObject(vendorCode, dataTable);
          Response response =
              utils.saveVendorGeneralDataSection(
                  userActionsTestUtils.getUserCookie(username), vendorJsonObject, vendorCode);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new MObVendorGeneralDataTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Vendor GeneralData section,")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.prepareDbScript());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Vendor GeneralData section,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IMObVendorRestUrls.UNLOCK_ALL_VENDOR, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Vendor GeneralData section,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.prepareDbScript());
      databaseConnector.closeConnection();
    }
  }
}
