package com.ebs.dda.masterdata.assetmasterdata.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.assetmasterdata.ICObAssetType;
import com.ebs.dda.masterdata.assetmasterdata.IMObAssetRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class MObAssetTypesTestUtil extends MObAssetCommonTestUtils {

  public MObAssetTypesTestUtil(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatAssetTypesExist(DataTable assetTypeDataTable) {
    List<Map<String, String>> assetTypesList =
        assetTypeDataTable.asMaps(String.class, String.class);
    for (Map<String, String> assetTypeRow : assetTypesList) {
      Object assetType =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getAssetTypeByCodeAndName", constructAssetTypeQueryParams(assetTypeRow));
      assertNotNull(ASSERTION_MSG + "Asset Type is not exist", assetType);
    }
  }

  private Object[] constructAssetTypeQueryParams(Map<String, String> assetTypeRow) {
    return new Object[] {
      assetTypeRow.get(IFeatureFileCommonKeys.CODE), assetTypeRow.get(IFeatureFileCommonKeys.NAME)
    };
  }

  public Response readAllAssetTypes(Cookie cookie) {
    String restURL = IMObAssetRestURLs.READ_ALL_ASSET_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReadAllAssetTypesResponseIsCorrect(
      Response response, DataTable assetTypeDataTable) throws Exception {

    List<Map<String, String>> expectedAssetTypes =
        assetTypeDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualAssetTypes = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedAssetTypes.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(expectedAssetTypes.get(i), actualAssetTypes.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IFeatureFileCommonKeys.TYPE, ICObAssetType.USER_CODE, ICObAssetType.NAME);
    }
  }

  public void assertThatTotalNumberOfRecordsIsCorrect(
      Response response, Integer totalNumberOfRecords) {
    List<HashMap<String, Object>> assetTypes = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + " total Number Of Records is not correct",
        totalNumberOfRecords.intValue(),
        assetTypes.size());
  }
}
