package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomerCreateValueObject;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.customer.IMObCustomerRestURLs;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class MObCustomerCreateTestUtils extends MObCustomerCommonTestUtils {

  public MObCustomerCreateTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response createCustomer(DataTable customerGeneralData, Cookie cookie) {
    JsonObject customer = prepareCustomerJsonObject(customerGeneralData);
    String restURL = IMObCustomerRestURLs.CREATE;
    return sendPostRequest(cookie, restURL, customer);
  }

  public JsonObject prepareCustomerJsonObject(DataTable customerGeneralData) {
    Map<String, String> customerDataMap =
        customerGeneralData.asMaps(String.class, String.class).get(0);
    JsonObject customerJsonObject = new JsonObject();

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.LEGAL_NAME,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.LEGAL_NAME));

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.MARKET_NAME,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME));

    addBusinessUnitCodesList(customerDataMap, customerJsonObject);

    customerJsonObject.addProperty(
        IMObCustomerCreateValueObject.CREDIT_LIMIT,
        Double.valueOf(customerDataMap.get(IMasterDataFeatureFileCommonKeys.CREDIT_LIMIT)));

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.CURRENCY_ISO,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.CURRENCY));

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.REGISTRATION_NUMBER,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.REGISTRATION_NUMBER));

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.ISSUED_FROM_CODE,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.ISSUED_FROM));

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.TAX_ADMINISTRATION_CODE,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.TAX_ADMINISTRATION));

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.TAX_CARD_NUMBER,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.TAX_CARD_NUMBER));

    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.TAX_FILE_NUMBER,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.TAX_FILE_NUMBER));
    addValueToJsonObject(
        customerJsonObject,
        IMObCustomerCreateValueObject.OLD_CUSTOMER_NUMBER,
        customerDataMap.get(IMasterDataFeatureFileCommonKeys.OLD_CUSTOMER_NUMBER));

    return customerJsonObject;
  }

  private void addBusinessUnitCodesList(
      Map<String, String> customerDataMap, JsonObject customerJsonObject) {
    JsonArray businessUnits = new JsonArray();
    List<String> businessUnitCodesList =
        convertBusinessUnitCodesStringToList(
            customerDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT));
    businessUnitCodesList.forEach(businessUnits::add);
    customerJsonObject.add(IMObCustomerCreateValueObject.BUSINESS_UNIT_CODES, businessUnits);
  }

  public void assertNewCustomerGeneralDataCreated(
      String customerCode, DataTable customerDataTable) {
    Map<String, String> customerMap = customerDataTable.asMaps(String.class, String.class).get(0);
    Object customerBusinessUnitCodes =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCreatedCustomerGeneralData",
            constructCustomerQueryParam(customerCode, customerMap));
    Assert.assertNotNull("Customer doesn't exist", customerBusinessUnitCodes);

    String[] businessUnits = customerMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(",");
    String[] actualBusinessUnitsCodes = String.valueOf(customerBusinessUnitCodes).split(",");
    Assert.assertEquals(
        " Business units not equals", businessUnits.length, actualBusinessUnitsCodes.length);
    assertThatBusinessUnitsAreEqual(
        customerCode,
        getBusinessUnitCodes(businessUnits),
        String.valueOf(customerBusinessUnitCodes));
  }

  private Object[] constructCustomerQueryParam(
      String customerCode, Map<String, String> customerMap) {
    return new Object[] {
      customerCode,
      customerMap.get(IFeatureFileCommonKeys.CREATED_BY),
      customerMap.get(IFeatureFileCommonKeys.CREATED_BY),
      customerMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      customerMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      customerMap.get(IFeatureFileCommonKeys.STATE),
      customerMap.get(IMasterDataFeatureFileCommonKeys.LEGAL_NAME),
      customerMap.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
      customerMap.get(IMasterDataFeatureFileCommonKeys.OLD_CUSTOMER_NUMBER)
    };
  }

  public void assertThatCustomerLegalRegistrationDataIsCreatedSuccessfully(
      String customerCode, DataTable legalRegistrationDataTable) {
    Map<String, String> legalRegistrationDataRow =
        legalRegistrationDataTable.asMaps(String.class, String.class).get(0);
    Object legalRegistrationData =
        retrieveCustomerLegalRegistrationData(customerCode, legalRegistrationDataRow);
    Assert.assertNotNull(
        ASSERTION_MSG + " Customer LegalRegistrationData doesn't exists", legalRegistrationData);
  }

  private Object retrieveCustomerLegalRegistrationData(
      String customerCode, Map<String, String> legalRegistrationDataRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedCustomerLegalRegistrationData",
        constructCreatedCustomerLegalRegistrationDataQueryParams(
            customerCode, legalRegistrationDataRow));
  }

  private Object[] constructCreatedCustomerLegalRegistrationDataQueryParams(
      String customerCode, Map<String, String> legalRegistrationDataRow) {
    return new Object[] {
      customerCode,
      legalRegistrationDataRow.get(IMasterDataFeatureFileCommonKeys.REGISTRATION_NUMBER),
      legalRegistrationDataRow.get(IMasterDataFeatureFileCommonKeys.ISSUED_FROM),
      legalRegistrationDataRow.get(IMasterDataFeatureFileCommonKeys.TAX_CARD_NUMBER),
      legalRegistrationDataRow.get(IMasterDataFeatureFileCommonKeys.TAX_ADMINISTRATION),
      legalRegistrationDataRow.get(IMasterDataFeatureFileCommonKeys.TAX_FILE_NUMBER)
    };
  }

  public Response requestCreateCustomer(Cookie cookie) {
    String restURL = IMObCustomerRestURLs.CREATE;
    return sendGETRequest(cookie, restURL);
  }
}
