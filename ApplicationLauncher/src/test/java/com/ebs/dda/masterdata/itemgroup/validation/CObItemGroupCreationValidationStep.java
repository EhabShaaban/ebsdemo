package com.ebs.dda.masterdata.itemgroup.validation;

import com.ebs.dda.SpringBootRunner;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;


public class CObItemGroupCreationValidationStep extends SpringBootRunner implements En {


    private CObItemGroupCreationValidationUtil itemGroupCreateFeatureTestUtils;
    private Boolean hasBeenExecutedOnce = false;

    public CObItemGroupCreationValidationStep() {

    And(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          itemGroupCreateFeatureTestUtils.freeze(currentDateTime);
        });

    And(
        "^Last created ItemGroup as \"([^\"]*)\" was with code \"([^\"]*)\"$",
        (String level, String code) -> {
          itemGroupCreateFeatureTestUtils.assertThatItemGroupWithCodeAndLevelExists(level, code);
        });


        When("^\"([^\"]*)\" creates ItemGroup with the following values:$", (String userName, DataTable itemGroupsDataTable) -> {

            Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
            Response response = itemGroupCreateFeatureTestUtils.createItemGroup(userCookie, itemGroupsDataTable);
            userActionsTestUtils.setUserResponse(userName, response);
        });
  }

    @Before
    public void setup() throws Exception {
        Map<String, Object> properties = getProperties();
        itemGroupCreateFeatureTestUtils = new CObItemGroupCreationValidationUtil(properties);
      itemGroupCreateFeatureTestUtils
          .setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
      databaseConnector.createConnection();
        if (!hasBeenExecutedOnce) {
            databaseConnector.executeSQLScript(
                    itemGroupCreateFeatureTestUtils.getMasterDataDbScriptsOneTimeExecution());
            hasBeenExecutedOnce = true;
        }

    }

  @After
  public void afterEachScenario() throws SQLException {
    itemGroupCreateFeatureTestUtils.unfreeze();
    databaseConnector.closeConnection();
  }
}
