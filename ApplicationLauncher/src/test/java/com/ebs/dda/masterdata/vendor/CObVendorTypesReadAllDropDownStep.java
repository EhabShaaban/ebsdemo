package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.CObVendorTypesReadAllDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class CObVendorTypesReadAllDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private CObVendorTypesReadAllDropDownTestUtils utils;

  public CObVendorTypesReadAllDropDownStep() {
    Given(
        "^the following VendorTypes exist:$",
        (DataTable vendorTypesDataTable) -> {
          utils.assertThatVendorTypesExist(vendorTypesDataTable);
        });

    Given(
        "^the total number of existing VendorTypes are (\\d+)$",
        (Integer recordsNumber) -> {
          utils.assertThatVendorTypesCountEquals(recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read VendorTypes in a dropdown$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IMObVendorRestUrls.READ_ALL_VENDOR_TYPES);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following VendorTypes dropdown values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable vendorTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatActualVendorTypesAreReturned(response, vendorTypesDataTable);
        });

    Then(
        "^total number of records returned to \"([^\"]*)\" in the dropdown is \"([^\"]*)\"$",
        (String userName, String recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsReturned(response, Integer.valueOf(recordsNumber));
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new CObVendorTypesReadAllDropDownTestUtils(properties, entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read VendorTypes dropdown,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
