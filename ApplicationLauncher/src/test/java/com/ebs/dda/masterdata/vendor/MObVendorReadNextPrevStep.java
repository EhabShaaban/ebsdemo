package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.utils.MObVendorReadNextPrevTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObVendorReadNextPrevStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObVendorReadNextPrevTestUtils utils;

  public MObVendorReadNextPrevStep() {

    When(
        "^\"([^\"]*)\" requests to view next Vendor of current Vendor with code \"([^\"]*)\"$",
        (String username, String currentVendorCode) -> {
          Response response =
              utils.requestToViewNextVendor(
                  userActionsTestUtils.getUserCookie(username), currentVendorCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the next Vendor code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String nextVendorCode, String username) -> {
          utils.assertThatNextInstanceCodeIsCorrect(
              nextVendorCode, userActionsTestUtils.getUserResponse(username));
        });

    When(
        "^\"([^\"]*)\" requests to view previous Vendor of current Vendor with code \"([^\"]*)\"$",
        (String username, String currentVendorCode) -> {
          Response response =
              utils.requestToViewPreviousVendor(
                  userActionsTestUtils.getUserCookie(username), currentVendorCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the previous Vendor code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String previousVendorCode, String username) -> {
          utils.assertThatPreviousInstanceCodeIsCorrect(
              previousVendorCode, userActionsTestUtils.getUserResponse(username));
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObVendorReadNextPrevTestUtils(getProperties());
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read Vendor Next/Previous Page,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.prepareDbScript());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read Vendor Next/Previous Page,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.prepareDbScript());
      databaseConnector.closeConnection();
    }
  }
}
