package com.ebs.dda.masterdata.item.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.IIObAlternativeUoMGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MObItemReadItemUomTestUtils extends MObItemCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[UOM][Dropdown][ReadAll]";

  public MObItemReadItemUomTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertThatItemUomsAreReturned(Response response, DataTable itemUOMDataTable) {
    assertResponseSuccessStatus(response);

    List<Map<String, String>> expectedItemsList =
        itemUOMDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItemsList = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedItemsList.size(); i++) {
      assertEquals(
          ASSERTION_MSG + " UOM Code is not correct",
          expectedItemsList.get(i).get(ISalesFeatureFileCommonKeys.UOM).split(" - ")[0],
          actualItemsList.get(i).get(IIObAlternativeUoMGeneralModel.USER_CODE));
      assertStringArrayEqualLocalizedStringArray(
          ASSERTION_MSG + " UOM Symbol is not correct",
          expectedItemsList.get(i).get(ISalesFeatureFileCommonKeys.UOM).split(" - ")[1],
          actualItemsList.get(i).get(IIObAlternativeUoMGeneralModel.SYMBOL).toString());
    }
  }

  public void assertThatUOMsExistForItem(String itemCodeName, DataTable itemUOMsDataTable) {
    List<Map<String, String>> expectedItemUomList =
        itemUOMsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> uomSymbol : expectedItemUomList) {
      Object itemUOM =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getItemUOMbyItemAndSymbol", constructUomQueryParams(itemCodeName, uomSymbol));
      assertNotNull(ASSERTION_MSG + uomSymbol + " Item UOM doesn't exists", itemUOM);
    }
  }

  private Object[] constructUomQueryParams(String itemCodeName, Map<String, String> uomSymbol) {
    return new Object[] {itemCodeName, uomSymbol.get(ISalesFeatureFileCommonKeys.UOM)};
  }
}
