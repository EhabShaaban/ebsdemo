package com.ebs.dda.masterdata.ivr.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.masterdata.ivr.IExpectedKeys;
import com.ebs.dda.masterdata.ivr.IResponseKeys;
import com.ebs.dda.masterdata.ivr.IVRFeatureFileKeys;
import com.ebs.dda.masterdata.ivr.IVRRestUrls;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.Assert.*;

public class ItemVendorRecordGeneralDataSaveTestUtils extends ItemVendorRecordCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[GeneralData][Save]";

  public ItemVendorRecordGeneralDataSaveTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    return str.toString();
  }

  public Response saveIVRGeneralDataSection(
      Cookie userCookie, JsonObject ivrGeneralDataJsonObject, String ivrCode) {
    String restURL = IVRRestUrls.SAVE_IVR_URL + ivrCode;
    return sendPUTRequest(userCookie, restURL, ivrGeneralDataJsonObject);
  }

  public JsonObject createGeneralDataSectionValueObject(
      String ivrCode, DataTable valueObjectTable) {
    JsonObject itemUOMDataObjects = new JsonObject();
    Map<String, String> ivrValueObject = valueObjectTable.asMaps(String.class, String.class).get(0);
    itemUOMDataObjects.addProperty(IResponseKeys.IVR_CODE_KEY, ivrCode);

    addCurrencyOptionalFieldToJson(itemUOMDataObjects, ivrValueObject);
    addPriceOptionalFieldToJson(itemUOMDataObjects, ivrValueObject);

    itemUOMDataObjects.addProperty(
        IResponseKeys.ITEM_CODE_AT_VENDOR,
        ivrValueObject.get(IFeatureFileCommonKeys.ITEM_CODE_AT_VENDOR));
    return itemUOMDataObjects;
  }

  private void addCurrencyOptionalFieldToJson(
      JsonObject itemUOMDataObjects, Map<String, String> ivrValueObject) {
    if (ivrValueObject.get(IVRFeatureFileKeys.CURRENCY_CODE).isEmpty()) {
      itemUOMDataObjects.add(IResponseKeys.CURRENCY_CODE_KEY, JsonNull.INSTANCE);
    } else {
      itemUOMDataObjects.addProperty(
          IResponseKeys.CURRENCY_CODE_KEY, ivrValueObject.get(IVRFeatureFileKeys.CURRENCY_CODE));
    }
  }

  public void assertDataIsUpdated(String ivrCode, DataTable expectedDataTable) throws Exception {
    Map<String, String> expectedDataMap =
        expectedDataTable.asMaps(String.class, String.class).get(0);

    ItemVendorRecordGeneralModel ivrResult =
        (ItemVendorRecordGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getIVRGeneralData",
                    ivrCode,
                    expectedDataMap.get(IExpectedKeys.CREATION_DATE),
                    expectedDataMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                    expectedDataMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                    expectedDataMap.get(IExpectedKeys.ITEM_CODE_KEY),
                    expectedDataMap.get(IExpectedKeys.VENDOR_CODE_KEY),
                    expectedDataMap.get(IExpectedKeys.UOM_CODE_KEY),
                    expectedDataMap.get(IExpectedKeys.ITEM_CODE_AT_VENDOR_KEY),
                    expectedDataMap.get(IExpectedKeys.OLD_ITEM_NUMBER_KEY),
                    expectedDataMap.get(IExpectedKeys.PURCHASE_UNIT_CODE_KEY));

    checkPriceExist(expectedDataMap, ivrResult);
    checkCurrencyExist(expectedDataMap, ivrResult);

    assertNotNull(ASSERTION_MSG + " IVR is not updated", ivrResult);
  }

  private void checkCurrencyExist(
      Map<String, String> expectedDataMap, ItemVendorRecordGeneralModel ivrResult) {
    if (expectedDataMap.get(IExpectedKeys.CURRENCY_CODE_KEY).isEmpty()) {
      assertNull(ASSERTION_MSG + " CurrencyCode is not null", ivrResult.getCurrencyCode());
    } else {
      assertEquals(
          ASSERTION_MSG + " CurrencyCode is not correct",
          ivrResult.getCurrencyCode(),
          expectedDataMap.get(IExpectedKeys.CURRENCY_CODE_KEY));
    }
  }

  private void checkPriceExist(
      Map<String, String> expectedDataMap, ItemVendorRecordGeneralModel ivrResult) {
    if (expectedDataMap.get(IExpectedKeys.PRICE_KEY).isEmpty()) {
      assertNull(ASSERTION_MSG + " Price is not null", ivrResult.getPrice());
    } else {
      assertEquals(
          ASSERTION_MSG + " Price is not correct",
          ivrResult.getPrice(),
          Double.parseDouble(expectedDataMap.get(IExpectedKeys.PRICE_KEY)),
          0.001);
    }
  }
}
