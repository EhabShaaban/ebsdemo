package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.masterdata.item.IExpectedKeys;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.ebs.dda.masterdata.item.IResponseKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MObItemViewAccountingInfoTestUtils extends MObItemCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[AccountDetails][View]";

  public MObItemViewAccountingInfoTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    return str.toString();
  }

  public void assertResponseDataIsCorrect(
      DataTable expectedAccountingInfoTable, Response itemAccountingInfoResponse) throws Exception {
    Map<String, String> actualMap =
        itemAccountingInfoResponse.body().jsonPath().get(CommonKeys.RESULT);
    Map<String, String> expectedMap =
        expectedAccountingInfoTable.asMaps(String.class, String.class).get(0);

    ObjectMapper mapperObj = new ObjectMapper();
    String localizedString =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.ACCOUNT_NAME));
    assertEquals(
        ASSERTION_MSG + " Account Name is not correct",
        expectedMap.get(IExpectedKeys.ACCOUNT_NAME),
        getLocaleFromLocalizedResponse(localizedString, "en"));
    assertEquals(
        ASSERTION_MSG + " Account Code is not correct",
        expectedMap.get(IExpectedKeys.ACCOUNT_CODE),
        actualMap.get(IResponseKeys.ACCOUNT_CODE));
  }
  public void assertResponseDataIsCorrectForServiceItem(
      DataTable expectedAccountingInfoTable, Response itemAccountingInfoResponse) throws Exception {
    Map<String, String> actualMap =
        itemAccountingInfoResponse.body().jsonPath().get(CommonKeys.RESULT);
    Map<String, String> expectedMap =
        expectedAccountingInfoTable.asMaps(String.class, String.class).get(0);

    ObjectMapper mapperObj = new ObjectMapper();
    String localizedString =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.ACCOUNT_NAME));
    assertEquals(
        ASSERTION_MSG + " Account Name is not correct",
        expectedMap.get(IExpectedKeys.ACCOUNT_NAME),
        getLocaleFromLocalizedResponse(localizedString, "en"));
    assertEquals(
        ASSERTION_MSG + " Account Code is not correct",
        expectedMap.get(IExpectedKeys.ACCOUNT_CODE),
        actualMap.get(IResponseKeys.ACCOUNT_CODE));

    boolean costFactor =
        Boolean.parseBoolean(expectedMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR));
    assertEquals(
        ASSERTION_MSG + " Cost Factor is not correct",
        costFactor,
        actualMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR_FIELD));
  }

  public Response sendReadAccountingInfoDetailsRequest(Cookie userCookie, String itemCode) {
    String readUrl = IMObItemRestUrls.READ_ACCOUNTING_INFO_RESTURL + itemCode;
    return sendGETRequest(userCookie, readUrl);
  }
}
