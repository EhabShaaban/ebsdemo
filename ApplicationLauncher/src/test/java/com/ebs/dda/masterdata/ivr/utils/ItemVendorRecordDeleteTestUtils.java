package com.ebs.dda.masterdata.ivr.utils;

import org.junit.Assert;

import java.util.Map;

public class ItemVendorRecordDeleteTestUtils extends ItemVendorRecordCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[DELETE]";

  public ItemVendorRecordDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    return str.toString();
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    return str.toString();
  }

  public void assertThatItemVendorRecordDeleted(String ivrCode) {
    Object itemVendorRecord =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getItemVendorRecordByCode", ivrCode);
    Assert.assertNull(ASSERTION_MSG + " Record " + ivrCode + " is not deleted", itemVendorRecord);
  }
}
