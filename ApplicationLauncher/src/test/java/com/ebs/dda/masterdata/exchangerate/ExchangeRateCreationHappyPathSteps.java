package com.ebs.dda.masterdata.exchangerate;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCreateTestUtils;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCreateValidationTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class ExchangeRateCreationHappyPathSteps extends SpringBootRunner implements En {

  private boolean hasBeenExecutedOnce = false;
  private CObExchangeRateCreateTestUtils exchangeRateCreationTestUtils;

  public ExchangeRateCreationHappyPathSteps() {

    Given("^CurrentDateTime = \"([^\"]*)\"$", (String currentDateTime) -> {
      exchangeRateCreationTestUtils.freeze(currentDateTime);
    });

    Given("^Last created ExchangeRate was with code \"([^\"]*)\"$", (String userCode) -> {
      exchangeRateCreationTestUtils.assertLastCreatedExchangeRateByCode(userCode);
    });

    When("^\"([^\"]*)\" creates ExchangeRate with following values:$",
        (String userName, DataTable exchangeRateData) -> {
          Response response = exchangeRateCreationTestUtils.createExchangeRate(exchangeRateData,
              userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then("^a new ExchangeRate is created as follows:$", (DataTable exchangeRateData) -> {
      exchangeRateCreationTestUtils.assertExchangeRateIsCreated(exchangeRateData);
    });
  }

  @Before(order = 1)
  public void setup(Scenario scenario) throws Exception {
    exchangeRateCreationTestUtils = new CObExchangeRateCreateTestUtils(getProperties());
    exchangeRateCreationTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Create ExchangeRate with all mandatory fields")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            CObExchangeRateCreateValidationTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(CObExchangeRateCreateValidationTestUtils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create ExchangeRate with all mandatory fields")) {
      databaseConnector.closeConnection();
      exchangeRateCreationTestUtils.unfreeze();
    }
  }
}
