package com.ebs.dda.masterdata.vendor.interfaces;

public interface ITableKeys {
  String CODE = "Code";
  String VENDOR = "Vendor";
  String VENDOR_CODE = "code";
  String VENDOR_NAME = "vendorname";
  String PURCHASE_UNITS_CODES = "purchaseunitscodes";
}
