package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemGeneralDataTestUtils;
import com.ebs.dda.masterdata.productmanager.utils.CObProductManagerReadAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class MObItemEditGeneralDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemGeneralDataTestUtils utils;
  private CObProductManagerReadAllTestUtils productManagerReadAllTestUtils;

  public MObItemEditGeneralDataStep() {

    Given(
        "^the following ProductManagers exist:$",
        (DataTable productManagerDataTable) -> {
          productManagerReadAllTestUtils.assertThatTheFollowingProductManagersExist(
              productManagerDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to edit GeneralData section of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          String lock_url = IMObItemRestUrls.LOCK_GENERAL_DATA_SECTION + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lock_url, itemCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^GeneralData section of Item with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String itemCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, itemCode, IMObItemSectionNames.GENERAL_DATA_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened the Item with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String itemCode) -> {
          String lock_url = IMObItemRestUrls.LOCK_GENERAL_DATA_SECTION + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lock_url, itemCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, itemCode, IMObItemSectionNames.GENERAL_DATA_SECTION);
        });

    Given(
        "^GeneralData section of Item with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String itemCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = IMObItemRestUrls.LOCK_GENERAL_DATA_SECTION + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lock_url, itemCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, itemCode, IMObItemSectionNames.GENERAL_DATA_SECTION);
        });

    Then(
        "^the lock by \"([^\"]*)\" on GeneralData section of Item with code \"([^\"]*)\" is released$",
        (String userName, String itemCode) -> {
          utils.assertThatLockIsReleased(
              userName, itemCode, IMObItemSectionNames.GENERAL_DATA_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving GeneralData section of Item with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockUrl = IMObItemRestUrls.UNLOCK_GENERAL_DATA_SECTION + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving GeneralData section of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          String unlockUrl = IMObItemRestUrls.UNLOCK_GENERAL_DATA_SECTION + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemGeneralDataTestUtils(getProperties());
    productManagerReadAllTestUtils =
        new CObProductManagerReadAllTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit General data section")
        || contains(scenario.getName(), "Request to Cancel saving General data section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to edit General data section")
        || contains(scenario.getName(), "Request to Cancel saving General data section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockSectionForAllUsers(IMObItemRestUrls.UNLOCK_GENERAL_DATA_SECTION);
    }
  }
}
