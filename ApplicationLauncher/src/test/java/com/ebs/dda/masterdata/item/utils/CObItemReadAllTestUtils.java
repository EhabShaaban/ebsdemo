package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.ICObItem;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CObItemReadAllTestUtils extends MObItemCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[ItemType][Dropdown][ReadAll]";

  public String getDbScriptsOneTimeExecution() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("db-scripts/clearDataSqlScript.sql");
    stringBuilder.append(",").append("db-scripts/master-data/item/CObItem.sql");
    return stringBuilder.toString();
  }

  public CObItemReadAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertThatActualItemTypesAreReturned(
      Response response, DataTable itemTypesDataTable) {
    List<Map<String, String>> expectedItemTypesList =
        itemTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItemTypesList = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedItemTypesList.size(); i++) {
      assertEquals(
          ASSERTION_MSG + " Item Type Code is not correct",
          expectedItemTypesList.get(i).get(IFeatureFileCommonKeys.CODE),
          actualItemTypesList.get(i).get(ICObItem.CODE));
    }
  }
}
