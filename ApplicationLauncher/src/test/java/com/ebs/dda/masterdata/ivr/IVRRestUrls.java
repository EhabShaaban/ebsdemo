package com.ebs.dda.masterdata.ivr;

public interface IVRRestUrls {

  String COMMAND_URL = "/command/itemvendorrecord/";
  String QUERY_URL = "/services/masterdataobjects/itemvendor/";
  String CREATE_URL = COMMAND_URL + "create";
  String VIEW_ALL_URL = "/services/masterdataobjects/itemvendor";
  String OBJECT_SCREEN_ALLOWED_ACTIONS_URL = QUERY_URL + "authorizedactions/";
  String HOME_SCREEN_ALLOWED_ACTIONS_URL = QUERY_URL + "authorizedactions";
  String DELETE_URL = COMMAND_URL + "delete/";
	String SAVE_IVR_URL = COMMAND_URL + "saveGeneralData/";

  String VIEW_GENERAL_DATA_URL = "/services/masterdataobjects/itemvendor/generaldata/";
  String LOCK_GENERAL_DATA_SECTION = COMMAND_URL + "lock/generaldatasection/";
  String UNLOCK_GENERAL_DATA_SECTION = COMMAND_URL + "unlock/generaldatasection/";
}
