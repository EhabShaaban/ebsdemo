package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorDeleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;

public class MObVendorDeleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObVendorDeleteTestUtils utils;

  public MObVendorDeleteStep() {

    Given(
        "^the following IVR exits:$",
        (DataTable ivrDataTable) -> {
          utils.assertThatItemVendorRecordsExist(ivrDataTable);
        });

    Given(
        "^the following PurchaseOrders exist with Vendor:$",
        (DataTable purchaseOrdersDataTable) -> {
          utils.assertThatPurchaseOrderHasVendor(purchaseOrdersDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to delete Vendor with code \"([^\"]*)\"$",
        (String userName, String vendorCode) -> {
          Response vendorDeleteResponse =
              utils.deleteVendorByCode(userActionsTestUtils.getUserCookie(userName), vendorCode);
          userActionsTestUtils.setUserResponse(userName, vendorDeleteResponse);
        });

    Then(
        "^Vendor with code \"([^\"]*)\" is deleted from the system$",
        (String vendorCode) -> {
          utils.assertVendorIsDeleted(vendorCode);
        });

    And(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" of Vendor with code \"([^\"]*)\" in edit mode$",
        (String username, String sectionName, String vendorCode) -> {
          String lockSectionUrl = IMObVendorRestUrls.LOCK_GENERAL_DATA_SECTION + vendorCode;
          Response lockResponse =
              userActionsTestUtils.lockSection(username, lockSectionUrl, vendorCode);
          utils.assertResponseSuccessStatus(lockResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObVendorDeleteTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete Vendor,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Delete Vendor,")) {
      userActionsTestUtils.unlockAllSections(
          IMObVendorRestUrls.UNLOCK_ALL_VENDOR, utils.getSectionsNames());
      utils.unfreeze();
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete Vendor,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
      databaseConnector.closeConnection();
    }
  }
}
