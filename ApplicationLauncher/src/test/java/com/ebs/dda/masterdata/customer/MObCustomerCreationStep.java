package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomerCreateValueObject;
import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.masterdata.customer.utils.MObCustomerCreateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObCustomerCreationStep extends SpringBootRunner implements En {

  private MObCustomerCreateTestUtils utils;

  public MObCustomerCreationStep() {

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String dateTime) -> {
          utils.freeze(dateTime);
        });

    Given(
        "^Last created Customer was with code \"([^\"]*)\"$",
        (String customerCode) -> {
          utils.createLastEntityCode(MObCustomer.class.getSimpleName(), customerCode);
        });

    When(
        "^\"([^\"]*)\" creates Customer with the following values:$",
        (String userName, DataTable customerGeneralData) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.createCustomer(customerGeneralData, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new Customer is created with code \"([^\"]*)\" as follows in the General Data:$",
        (String customerCode, DataTable customerDataTable) -> {
          utils.assertNewCustomerGeneralDataCreated(customerCode, customerDataTable);
        });

    Then(
        "^the LegalRegistrationData Section for the Customer with Code \"([^\"]*)\" is updated as follows:$",
        (String customerCode, DataTable legalRegistrationDataTable) -> {
            utils.assertThatCustomerLegalRegistrationDataIsCreatedSuccessfully(customerCode, legalRegistrationDataTable);
        });

      When("^\"([^\"]*)\" requests to create Customer$", (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.requestCreateCustomer(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
      });
      Then(
              "^the following error message \"([^\"]*)\" is returned and sent to \"([^\"]*)\"$",
              (String messageCode, String userName) -> {
                  Response response = userActionsTestUtils.getUserResponse(userName);
                  utils.assertThatFieldHasErrorMessageCode(
                          response, messageCode, IMObCustomerCreateValueObject.OLD_CUSTOMER_NUMBER);
              });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Customer,")) {
      utils = new MObCustomerCreateTestUtils(getProperties(), entityManagerDatabaseConnector);
      databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Customer,")) {
      utils.unfreeze();
    }
  }
}
