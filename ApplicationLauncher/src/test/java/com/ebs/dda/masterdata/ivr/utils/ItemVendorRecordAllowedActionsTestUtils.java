package com.ebs.dda.masterdata.ivr.utils;

import java.util.Map;

public class ItemVendorRecordAllowedActionsTestUtils extends ItemVendorRecordCommonTestUtils {

  public ItemVendorRecordAllowedActionsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    return str.toString();
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    return str.toString();
  }
}
