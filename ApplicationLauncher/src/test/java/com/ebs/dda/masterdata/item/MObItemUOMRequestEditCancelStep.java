package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.IObItemUOMCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;

public class MObItemUOMRequestEditCancelStep extends SpringBootRunner implements En {

  private static Boolean hasBeenExecutedOnce = false;
  private IObItemUOMCommonTestUtils utils;

  public MObItemUOMRequestEditCancelStep() {

    When(
        "\"([^\"]*)\" requests to edit UnitOfMeasures section of Item with code \"([^\"]*)\"",
        (String username, String itemCode) -> {
          String lock_url = IMObItemRestUrls.LOCK_UNIT_OF_MEASURE_URL + itemCode;
          Response response = userActionsTestUtils.lockSection(username, lock_url, itemCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^UnitOfMeasures section of Item with code \"([^\"]*)\" becomes in the edit mode and locked by \"([^\"]*)\"$",
        (String itemCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, itemCode, IMObItemSectionNames.UOM_SECTION);
        });

    And(
        "^\"([^\"]*)\" first opened UnitOfMeasures section of Item with code \"([^\"]*)\" in the edit mode "
            + "successfully$",
        (String userName, String itemCode) -> {
          String lock_url = IMObItemRestUrls.LOCK_UNIT_OF_MEASURE_URL + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lock_url, itemCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving UnitOfMeasures section of Item with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String date) -> {
          utils.freeze(date);
          Response response =
              userActionsTestUtils.unlockSection(
                  userName, IMObItemRestUrls.UNLOCK_UNIT_OF_MEASURE_URL + purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on UnitOfMeasures section of Item with code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IMObItemSectionNames.UOM_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving UnitOfMeasures section of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          Response response =
              userActionsTestUtils.unlockSection(
                  userName, IMObItemRestUrls.UNLOCK_UNIT_OF_MEASURE_URL + itemCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    And(
        "^UnitOfMeasures section of Item with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String itemCode, String userName, String date) -> {
          utils.freeze(date);
          Response response =
              userActionsTestUtils.lockSection(
                  userName, IMObItemRestUrls.LOCK_UNIT_OF_MEASURE_URL + itemCode, itemCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, itemCode, IMObItemSectionNames.UOM_SECTION);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObItemUOMCommonTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit UnitOfMeasures section")
        || contains(scenario.getName(), "Cancel save UnitOfMeasures section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to edit UnitOfMeasures section")
        || contains(scenario.getName(), "Cancel save UnitOfMeasures section")) {
      userActionsTestUtils.unlockSectionForAllUsers(IMObItemRestUrls.UNLOCK_UNIT_OF_MEASURE_URL);
      utils.unfreeze();
    }
  }
}
