package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.Assert.assertEquals;


public class MObItemReadNextPrevTestUtils extends MObItemCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Next/Prev]";

  public MObItemReadNextPrevTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
      str.append("db-scripts/master-data/item/MObItem_next_prev.sql");
    return str.toString();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    return str.toString();
  }

  public Response requestToViewPreviousItem(Cookie userCookie, String currentItemCode) {
    String restURL = IMObItemRestUrls.READ_PREVIOUS_ITEMS_URLS + currentItemCode;
    return sendGETRequest(userCookie, restURL);
  }

  public Response requestToViewNextItem(Cookie userCookie, String currentItemCode) {
    String restURL = IMObItemRestUrls.READ_NEXT_ITEMS_URLS + currentItemCode;
    return sendGETRequest(userCookie, restURL);
  }

  public void assertThatNextItemCodeIsCorrect(
      String expectedNextItemCode, Response nextItemResponse) {
    String actualNextCode = nextItemResponse.body().jsonPath().getString("nextCode");
    assertEquals(ASSERTION_MSG + " Next IMR code is not correct",expectedNextItemCode, actualNextCode);
  }

  public void assertThatPreviousItemCodeIsCorrect(
      String expectedPreviousItemCode, Response previousItemResponse) {
    String actualPreviousCode = previousItemResponse.body().jsonPath().getString("previousCode");
    assertEquals(ASSERTION_MSG + " Previous IMR code is not correct",expectedPreviousItemCode, actualPreviousCode);
  }
}
