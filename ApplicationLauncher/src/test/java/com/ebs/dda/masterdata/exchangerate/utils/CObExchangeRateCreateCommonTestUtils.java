package com.ebs.dda.masterdata.exchangerate.utils;

import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRateValueObject;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateRestURLs;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class CObExchangeRateCreateCommonTestUtils extends CObExchangeRateCommonTestUtils {

  public CObExchangeRateCreateCommonTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/clearUnsedCompanyBankDetails.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    return str.toString();
  }

  public static String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append(",db-scripts/master-data/exchange-rate/exchange-rate-view-all.sql");
    return str.toString();
  }

  public Response createExchangeRate(DataTable exchangeRateData, Cookie cookie) {
    JsonObject exchangeRate = prepareEcxhangeRateJsonObject(exchangeRateData);
    String restURL = ICObExchangeRateRestURLs.CREATE_EXCHANGE_RATE;
    return sendPostRequest(cookie, restURL, exchangeRate);
  }

  private JsonObject prepareEcxhangeRateJsonObject(DataTable ecxhangeRateDataTable) {
    Map<String, String> exchangeRate =
        ecxhangeRateDataTable.asMaps(String.class, String.class).get(0);
    JsonObject ecxhangeRateJsonObject = new JsonObject();

    ecxhangeRateJsonObject.addProperty(
        ICObExchangeRateValueObject.FIRST_CURRENCY,
        exchangeRate.get(ICObExchangeRateFeatureFileCommonKeys.FIRST_CURRENCY));
    try {
      ecxhangeRateJsonObject.addProperty(
          ICObExchangeRateValueObject.SECOND_VALUE,
          !exchangeRate.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE).isEmpty()
              ? Double.parseDouble(
                  exchangeRate.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE))
              : null);
    } catch (NumberFormatException e) {
      ecxhangeRateJsonObject.addProperty(
          ICObExchangeRateValueObject.SECOND_VALUE,
          exchangeRate.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE));
    }

    return ecxhangeRateJsonObject;
  }
}
