package com.ebs.dda.masterdata.enterprise;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.jpa.masterdata.company.IObCompanyTreasuriesGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CObEnterpriseTestUtils extends CommonTestUtils {

  public EntityManagerDatabaseConnector entityManagerDatabaseConnector;

  public CObEnterpriseTestUtils(EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    this.entityManagerDatabaseConnector = entityManagerDatabaseConnector;
  }

  public void assertThatPlantsExist(DataTable plantsDataTable) {
    List<Map<String, String>> plants = plantsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> plant : plants) {

      String plantCode = plant.get(IFeatureFileCommonKeys.CODE);
      String plantEnglishName = plant.get(IFeatureFileCommonKeys.NAME);

      Object plantGeneralModel =
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                      "getPlantByCodeAndName", plantCode, plantEnglishName);

      assertNotNull(plantGeneralModel);
    }
  }

  public void assertThatStorehousesExist(DataTable storehousesDataTable) {
    List<Map<String, String>> storehouses = storehousesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> storehouse : storehouses) {

      String storehouseCode = storehouse.get(IFeatureFileCommonKeys.CODE);
      String storehouseEnglishName = storehouse.get(IFeatureFileCommonKeys.NAME);

      Object storehouseGeneralModel =
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                      "getStorehouseByCodeAndName", storehouseCode, storehouseEnglishName);

      assertNotNull(storehouseGeneralModel);
    }
  }

  public void assertThatPurchaseUnitsExist(DataTable purchaseUnits) {

    List<Map<String, String>> purchaseUnitsList = purchaseUnits.asMaps(String.class, String.class);
    for (Map<String, String> purchaseUnit : purchaseUnitsList) {

      String purchaseUnitCode = purchaseUnit.get(IFeatureFileCommonKeys.CODE);
      String purchaseUnitName = purchaseUnit.get(IFeatureFileCommonKeys.NAME);

      CObPurchasingUnitGeneralModel purchaseUnitGeneralModel =
              (CObPurchasingUnitGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getPurchaseUnitByCodeAndName", purchaseUnitCode, purchaseUnitName);

      assertNotNull(purchaseUnitGeneralModel);
    }
  }

  public void assertThatPurchaseResponsiblesExist(DataTable purchaseResponsibleDataTable) {
    List<Map<String, String>> purchaseResponsibleList =
            purchaseResponsibleDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseResponsible : purchaseResponsibleList) {

      String purchaseResponsibleCode = purchaseResponsible.get(IFeatureFileCommonKeys.CODE);
      String purchaseResponsibleName = purchaseResponsible.get(IFeatureFileCommonKeys.NAME);

      CObPurchasingResponsibleGeneralModel purchaseResponsibleGeneralModel =
              (CObPurchasingResponsibleGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getPurchaseResponsibleByCodeAndName",
                              purchaseResponsibleCode,
                              purchaseResponsibleName);

      assertNotNull(purchaseResponsibleGeneralModel);
    }
  }

  public void assertThatComapnyExist(DataTable companyDataTable) {
    List<Map<String, String>> companyList = companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> company : companyList) {

      String companyCode = company.get(IFeatureFileCommonKeys.CODE);
      String companyName = company.get(IFeatureFileCommonKeys.NAME);

      CObCompanyGeneralModel cObCompanyGeneralModel =
              (CObCompanyGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getCompanyesWihCodeAndName", companyCode, companyName);

      assertNotNull(cObCompanyGeneralModel);
    }
  }

  public void assertThatResponseDataIsCorrect(Response response, DataTable dataTable, String key)
          throws JSONException, IOException {

    List<Map<String, String>> expectedNameList = dataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> responseData = getListOfMapsFromResponse(response);

    assertEquals(expectedNameList.size(), responseData.size());

    for (int itr = 0; itr < expectedNameList.size(); itr++) {
      assertThatCodeAndNameAreEqual(expectedNameList.get(itr), responseData.get(itr), key);
    }
  }

  private void assertThatCodeAndNameAreEqual(
          Map<String, String> expectedData, HashMap<String, Object> actualData, String key)
          throws IOException, JSONException {
    assertEquals(expectedData.get(IFeatureFileCommonKeys.CODE), actualData.get(IResponseKeys.CODE));
    assertEquals(
            expectedData.get(IFeatureFileCommonKeys.NAME),
            getLocaleFromLocalizedResponse(getLocalizedValue(actualData.get(key)), "en"));
  }

  public String getLocalizedValue(Object obj) throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();

    return objectMapper.writeValueAsString(obj);
  }

  public void assertThatCompanyTreasuriesExist(
          String company, DataTable companyTreasuriesDataTable) {
    List<Map<String, String>> companyTreasuriesList =
            companyTreasuriesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> companyTreasuries : companyTreasuriesList) {
      IObCompanyTreasuriesGeneralModel companyTreasuriesGeneralModel =
              (IObCompanyTreasuriesGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getCompanyTreasuries",
                              constructCompanyTreasuriesParams(company, companyTreasuries));

      assertNotNull(companyTreasuriesGeneralModel);
    }
  }

  private Object[] constructCompanyTreasuriesParams(
          String company, Map<String, String> companyTreasuries) {
    return new Object[]{
            company,
            companyTreasuries.get(IFeatureFileCommonKeys.CODE),
            companyTreasuries.get(IFeatureFileCommonKeys.NAME)
    };
  }
}
