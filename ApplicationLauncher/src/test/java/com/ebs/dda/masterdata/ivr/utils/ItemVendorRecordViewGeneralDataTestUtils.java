package com.ebs.dda.masterdata.ivr.utils;

import com.ebs.dda.CommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static com.ebs.dda.masterdata.ivr.IVRRestUrls.VIEW_GENERAL_DATA_URL;

public class ItemVendorRecordViewGeneralDataTestUtils extends
    ItemVendorRecordCommonTestUtils {

  public ItemVendorRecordViewGeneralDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response readItemVendorRecordGeneralData(Cookie cookie, String ivrCode) {
    String restURL = VIEW_GENERAL_DATA_URL + ivrCode;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatResponseDataIsCorrect(
          DataTable expectedIVRTable, Response ivrDataResponse) throws Exception {
    Map<String, Object> actualMap =
        ivrDataResponse.body().jsonPath().get(CommonKeys.DATA);
    Map<String, String> expectedMap = expectedIVRTable.asMaps(String.class, String.class).get(0);
    assertExpectedDataEqualActualData(expectedMap, actualMap);
  }

  private void assertExpectedDataEqualActualData(
      Map<String, String> expectedMap,
      Map<String, Object> actualMap)
      throws Exception {
    assertThatCreationDateIsCorrect(actualMap, expectedMap);
    assertThatItemVendorCodeExistsInResponse(expectedMap, actualMap);
    assertThatItemExistsInResponse(expectedMap, actualMap);
    assertThatVendorExistsInResponse(expectedMap, actualMap);
    assertThatUOMExistsInResponse(expectedMap, actualMap);
    assertThatItemCodeAtVendorExistsInResponse(expectedMap, actualMap);
    assertThatPriceExistsInResponse(expectedMap, actualMap);
    assertThatCurrencyExistsInResponse(expectedMap, actualMap);
    assertThatOldItemNumberExistsInResponse(expectedMap, actualMap);
    assertThatPurchaseUnitExistsInResponse(expectedMap, actualMap);
  }
}
