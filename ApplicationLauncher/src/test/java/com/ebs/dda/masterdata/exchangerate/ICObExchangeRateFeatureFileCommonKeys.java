package com.ebs.dda.masterdata.exchangerate;

public interface ICObExchangeRateFeatureFileCommonKeys {

    String EXCHANGE_RATE_CODE = "Code";
    String FIRST_CURRENCY = "FirstCurrency";
    String SECOND_VALUE = "SecondValue";
    String CURRENT_STATE = "CurrentState";
    String VALID_FROM = "ValidationFrom";
    String VALID_FROM_DATE = "ValidFrom";
    String FIRST_VALUE = "FirstValue";
    String SECOND_CURRENCY = "SecondCurrency";

    String FIRST_CURRENCY_ISO = "FirstCurrencyIso";
    String SECOND_CURRENCY_ISO = "SecondCurrencyIso";
}
