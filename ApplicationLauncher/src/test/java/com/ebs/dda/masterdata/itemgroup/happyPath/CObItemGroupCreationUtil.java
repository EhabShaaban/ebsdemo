package com.ebs.dda.masterdata.itemgroup.happyPath;

import com.ebs.dda.jpa.masterdata.item.CObMaterialGroupGeneralModel;
import com.ebs.dda.masterdata.itemgroup.CObItemGroupFeatureTestUtils;
import com.ebs.dda.masterdata.itemgroup.ICObItemGroupRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class CObItemGroupCreationUtil extends CObItemGroupFeatureTestUtils {

    public CObItemGroupCreationUtil(Map<String, Object> properties) {
        super(properties);
    }

    public void assertItemGroupsCreated(DataTable itemGroupList) throws Exception {
        CObMaterialGroupGeneralModel materialGroupGeneralModel;

        List<Map<String, String>> itemGroupData = itemGroupList.asMaps(String.class, String.class);

        for (Map<String, String> itemGroupRow : itemGroupData) {
            String itemGroupCode = itemGroupRow.get("Code");
            String itemGroupParentCode = itemGroupRow.get("ParentCode");
            String itemGroupParentName = itemGroupRow.get("ParentName");
            String itemGroupName = itemGroupRow.get("Name");
            String itemGroupLevel = itemGroupRow.get("GroupLevel");
            String lastUpdatedBy = itemGroupRow.get("LastUpdatedBy");
            String createdBy = itemGroupRow.get("CreatedBy");
            String creationDate = itemGroupRow.get("CreationDate");
            String lastUpdateDate = itemGroupRow.get("LastUpdateDate");

            if (!itemGroupParentCode.isEmpty()) {

                materialGroupGeneralModel =
                    (CObMaterialGroupGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getCobmaterialgroupgeneralmodelByNameAndCodeAndLevelAndParentCodeAndParentName",
                            itemGroupCode,
                            itemGroupName,
                            itemGroupLevel,
                            itemGroupParentCode,
                            itemGroupParentName);

            } else {
                materialGroupGeneralModel =
                    (CObMaterialGroupGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getCobmaterialgroupgeneralmodelByNameAndCodeAndLevel",
                            itemGroupCode,
                            itemGroupName,
                            itemGroupLevel);
            }
            Assert.assertEquals(lastUpdatedBy, materialGroupGeneralModel.getModificationInfo());
            Assert.assertEquals(createdBy, materialGroupGeneralModel.getCreationInfo());
            assertDateEquals(creationDate, materialGroupGeneralModel.getCreationDate());
            assertDateEquals(lastUpdateDate, materialGroupGeneralModel.getModifiedDate());
        }
    }

    Response createItemGroup(Cookie userCookie, DataTable itemGroupsDataTable) {
        JsonObject itemGroupData = prepareItemJsonObject(itemGroupsDataTable);
        String restURL = ICObItemGroupRestUrls.CREATE_ITEM_URL;
        return sendPostRequest(userCookie, restURL, itemGroupData);
    }
}
