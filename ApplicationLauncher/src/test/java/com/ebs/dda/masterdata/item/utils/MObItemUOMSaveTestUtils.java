package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.masterdata.item.IExpectedKeys;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.ebs.dda.masterdata.item.IResponseKeys;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class MObItemUOMSaveTestUtils extends IObItemUOMCommonTestUtils {

  private static final String CODES = "Codes";

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[UOM][Save]";

  public MObItemUOMSaveTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    return str.toString();
  }

  public void assertModificationDataIsUpdated(String itemCode, DataTable expectedDataTable) {
    Map<String, String> expectedDataMap =
        expectedDataTable.asMaps(String.class, String.class).get(0);

    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getItemModificationDetailsByItemCode", itemCode);
    assertTrue(ASSERTION_MSG + " Item record is not updated", actualData.size() == 1);
    assertModificationDataUpdatedValues(expectedDataMap, actualData);
  }

  private void assertModificationDataUpdatedValues(
      Map<String, String> expectedDataMap, List<Object[]> actualData) {
    Object[] actualApprover = actualData.get(0);
    assertEquals(
        ASSERTION_MSG + " Last updated date is not correct",
        expectedDataMap.get(IExpectedKeys.LAST_UPDATED_DATE),
        actualApprover[0]);
    assertEquals(
        ASSERTION_MSG + " Last updated by is not correct",
        expectedDataMap.get(IExpectedKeys.LAST_UPDATED_BY),
        actualApprover[1]);
  }

  public void assertDataIsUpdated(String itemCode, DataTable expectedDataTable) {
    List<Map<String, String>> expectedDataList =
        expectedDataTable.asMaps(String.class, String.class);

    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getItemUnitOfMeasureByItemCodeAndOrdered", itemCode);
    assertTrue(
        ASSERTION_MSG + " Item UOM record is not updated",
        actualData.size() == expectedDataList.size());
    assertUnitOfMeasureUpdatedValues(expectedDataList, actualData);
  }

  private void assertUnitOfMeasureUpdatedValues(
      List<Map<String, String>> expectedDataList, List<Object[]> actualData) {
    int index = 0;
    for (Map<String, String> expectedDataMap : expectedDataList) {
      Object[] actualApprover = actualData.get(index);
      assertEquals(
          ASSERTION_MSG + " AlternateUnit Code is not correct",
          expectedDataMap.get(IExpectedKeys.ALETRNATIVE_UNIT_CODE),
          actualApprover[0]);
      assertEquals(
          ASSERTION_MSG + " AlternateUnit Name is not correct",
          expectedDataMap.get(IExpectedKeys.ALTERNATIVE_UNIT_NAME),
          actualApprover[1]);
      assertEquals(
          ASSERTION_MSG + " Conversion Factor is not correct",
          Float.parseFloat(expectedDataMap.get(IExpectedKeys.CONVERSION_FACTOR)),
          actualApprover[2]);
      assertEquals(
          ASSERTION_MSG + " Old Item Number is not correct",
          expectedDataMap.get(IExpectedKeys.OLD_ITEM_NUMBER),
          actualApprover[3]);
      index++;
    }
  }

  public void assertThatAlternativeUnitFieldHasErrorMessageCode(
      Response response, String messageCode, String fieldName, DataTable dataTable) {
    HashMap<String, Object> errors = response.body().jsonPath().get("errorsValidationResult");
    List<Map<String, String>> alternativeUnitCodes = dataTable.asMaps(String.class, String.class);

    alternativeUnitCodes.stream()
        .forEach(
            alternativeUnitCode -> {
              String objectKey = fieldName + '-' + alternativeUnitCode.get(CODES);
              String objectCode = (String) errors.get(objectKey);
              assertEquals(messageCode, objectCode);
            });
  }

  public JsonObject createEmptyItemUOMJsonObject(String itemCode) {
    JsonObject itemUOMDataObjects = new JsonObject();
    JsonArray itemUOMsJsonArray = new JsonArray();
    itemUOMDataObjects.addProperty(IResponseKeys.ITEM_CODE, itemCode);
    itemUOMDataObjects.add(IResponseKeys.ITEM_UOMS, itemUOMsJsonArray);
    return itemUOMDataObjects;
  }

  public JsonObject createItemUOMJsonObject(String itemCode, DataTable itemUOMDataTable) {
    JsonObject itemUOMDataObjects = new JsonObject();
    List<Map<String, String>> itemUOMs = itemUOMDataTable.asMaps(String.class, String.class);
    JsonArray itemUOMsJsonArray = createJsonArrayOfUOMs(itemUOMs);
    itemUOMDataObjects.addProperty(IResponseKeys.ITEM_CODE, itemCode);
    itemUOMDataObjects.add(IResponseKeys.ITEM_UOMS, itemUOMsJsonArray);
    return itemUOMDataObjects;
  }

  private JsonArray createJsonArrayOfUOMs(List<Map<String, String>> itemUOMs) {
    JsonArray itemUOMsJsonArray = new JsonArray();
    for (Map<String, String> itemUOM : itemUOMs) {
      JsonObject itemUOMDataObject = new JsonObject();
      addPropertiesToItemUOM(itemUOMDataObject, itemUOM);
      itemUOMsJsonArray.add(itemUOMDataObject);
    }
    return itemUOMsJsonArray;
  }

  private void addPropertiesToItemUOM(JsonObject itemUOMObject, Map<String, String> itemUOM) {
    itemUOMObject.addProperty(
        IResponseKeys.ALTERNATIVE_UNIT_CODE, itemUOM.get(IExpectedKeys.ALETRNATIVE_UNIT_CODE));
    addConversionFactorProperty(itemUOMObject, itemUOM.get(IExpectedKeys.CONVERSION_FACTOR));
    itemUOMObject.addProperty(
        IResponseKeys.OLD_ITEM_NUMBER, itemUOM.get(IExpectedKeys.OLD_ITEM_NUMBER));
  }

  private void addConversionFactorProperty(JsonObject itemUOMObject, String entry) {
    Float conversionFactor = null;
    try {
      conversionFactor = Float.parseFloat(entry);
    } catch (Exception ex) {
    }
    if (conversionFactor == null) {
      itemUOMObject.addProperty(IResponseKeys.CONVERSION_FACTOR, entry);
    } else {
      itemUOMObject.addProperty(IResponseKeys.CONVERSION_FACTOR, conversionFactor);
    }
  }

  public Response saveItemUnitOfMeasureSection(Cookie userCookie, JsonObject itemUOMsObject) {
    return sendPUTRequest(userCookie, IMObItemRestUrls.SAVE_ITEM_UOM_URL, itemUOMsObject);
  }

  public void assertThatUomSectionIsUpdatedWithEmptyList(String itemCode) {
    List uomList =
        entityManagerDatabaseConnector.executeNativeNamedQuery("getItemUomList", itemCode);
    Assert.assertEquals(
        ASSERTION_MSG + " unit of measures list of item " + itemCode + " is not empty",
        0,
        uomList.size());
  }
}
