package com.ebs.dda.masterdata.vendor.utils;

import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MObVendorViewAllTestUtils extends MObVendorCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[ViewAll] ";

  public MObVendorViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    return str.toString();
  }

  public Response viewAllVendors(Cookie cookie) {
    return sendGETRequest(
        cookie, IMObVendorRestUrls.VENDOR_READ_ALL_URL + "/page=0/rows=15/filters=");
  }

  public void assertThatVendorsExist(DataTable vendorDataTable) {
    List<Map<String, String>> vendorList = vendorDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendor : vendorList) {
      assertThatVendorExists(vendor);
    }
  }

  public void assertNoVendorDataIsReturned(Response response) {
    List<Map<String, Object>> actualVendorDataList = response.body().jsonPath().getList("data");
    assertEquals(
        ASSERTION_MSG + "The vendor home screen doesn't empty", 0, actualVendorDataList.size());
  }
}
