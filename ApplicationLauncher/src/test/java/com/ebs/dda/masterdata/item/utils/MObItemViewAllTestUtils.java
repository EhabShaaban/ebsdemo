package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.item.IExpectedKeys;
import com.ebs.dda.masterdata.item.IResponseKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class MObItemViewAllTestUtils extends MObItemCommonTestUtils {

  protected static final String COMMERCIAL_ITEM_TYPE = "COMMERCIAL";
  private ObjectMapper objectMapper;
  private String requestLocale = "en";

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[ViewAll]";

  public MObItemViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {

    StringBuilder str = new StringBuilder();
    str.append(super.getMasterDataDbScriptsOneTimeExecution());
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");

    return str.toString();
  }

  public void withLocale(String locale) {
    this.requestLocale = locale;
  }

  public void assertThatResponseHasItems(Response response, List<Map<String, Object>> expectedData)
      throws Exception {
    List<Map<String, Object>> actualData = response.body().jsonPath().get(CommonKeys.DATA);
    assertEquals(
        ASSERTION_MSG + "Expected items size differs from actual items size",
        expectedData.size(),
        actualData.size());
    for (int i = 0; i < expectedData.size(); i++) {
      assertThatItemExistsInResponse(expectedData.get(i), actualData.get(i));
    }
  }

  public void assertItemResponseCorrect(Response response, DataTable expectedItemsDataTable)
      throws Exception {
    List<Map<String, Object>> expectedData =
        expectedItemsDataTable.asMaps(String.class, Object.class);
    assertThatResponseHasItems(response, expectedData);
  }

  private void assertThatItemExistsInResponse(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) throws Exception {
    String expectedItemCode = expectedDataItem.get(IExpectedKeys.ITEM_CODE).toString();
    String currentItemCode = actualDataItem.get(IResponseKeys.USER_CODE).toString();
    if (currentItemCode.equals(expectedItemCode)) {
      assertThatAllItemAttributesExist(expectedDataItem, actualDataItem);
      return;
    }
    Assert.fail(ASSERTION_MSG + " Item Record is not in response");
  }

  private void assertThatAllItemAttributesExist(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) throws Exception {
    assertThatItemNameIsCorrect(expectedDataItem, actualDataItem);
    assertEquals(
        ASSERTION_MSG + " BaseUnit Code is not correct",
        expectedDataItem.get(IMasterDataFeatureFileCommonKeys.BASE_UNIT).toString().split(" - ")[0],
        actualDataItem.get(IMObItemGeneralModel.BASIC_UOM_CODE));
    assertThatBasicUnitNameIsCorrect(expectedDataItem, actualDataItem);
    String itemTypeCode =
        expectedDataItem.get(IFeatureFileCommonKeys.TYPE).toString().split(" - ")[0];
    if (itemTypeCode.equals(COMMERCIAL_ITEM_TYPE)) {
      assertEquals(
          ASSERTION_MSG + " Item Group is not correct",
          expectedDataItem
              .get(IMasterDataFeatureFileCommonKeys.ITEM_GROUP)
              .toString()
              .split(" - ")[0],
          actualDataItem.get(IMObItemGeneralModel.ITEM_GROUP_CODE));
    } else {
      assertNull(
          ASSERTION_MSG + " Item Group exist",
          actualDataItem.get(IMObItemGeneralModel.ITEM_GROUP_CODE));
    }

    assertThatBatchManagedIsCorrect(expectedDataItem, actualDataItem);
    assertThatOldItemNumberIsCorrect(expectedDataItem, actualDataItem);
    assertThatBusinessUnitsAreEqual(
        expectedDataItem.get(IFeatureFileCommonKeys.ITEM_CODE).toString(),
        getBusinessUnitCodes(
            expectedDataItem.get(IFeatureFileCommonKeys.BUSINESS_UNIT).toString().split(", ")),
        actualDataItem.get(IMObItemGeneralModel.PURCHASE_UNIT_CODES).toString());
    assertEquals(
        ASSERTION_MSG + " Item Type Code is not correct",
        expectedDataItem.get(IFeatureFileCommonKeys.TYPE).toString().split(" - ")[0],
        actualDataItem.get(IMObItemGeneralModel.ITEM_TYPE_CODE));
    assertEquals(
        ASSERTION_MSG + " Item Type Name is not correct",
        expectedDataItem.get(IFeatureFileCommonKeys.TYPE).toString().split(" - ")[1],
        getLocaleFromLocalizedResponse(
            actualDataItem.get(IMObItemGeneralModel.ITEM_TYPE_NAME).toString(), requestLocale));
  }

  private void assertThatItemNameIsCorrect(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) throws Exception {
    String expectedValue = expectedDataItem.get(IExpectedKeys.NAME).toString();
    ObjectMapper mapperObj = new ObjectMapper();
    String actualLocalizedValueAsString =
        mapperObj.writeValueAsString(actualDataItem.get(IResponseKeys.ITEM_NAME));
    String actualValue = getEnglishLocaleFromLocalizedResponse(actualLocalizedValueAsString);

    Assert.assertEquals(ASSERTION_MSG + "Item Name is not correct", expectedValue, actualValue);
  }

  private void assertThatBasicUnitNameIsCorrect(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) throws Exception {
    Object expectedValue = expectedDataItem.get(IExpectedKeys.BASE_UNIT).toString().split(" - ")[1];
    String basicUnitLocalizedName =
        objectMapper.writeValueAsString(actualDataItem.get(IResponseKeys.BASE_UNIT));
    Object actualValue = getLocaleFromLocalizedResponse(basicUnitLocalizedName, requestLocale);
    Assert.assertEquals(
        ASSERTION_MSG + " BaseUnit Name is not correct", expectedValue, actualValue);
  }

  private void assertThatBatchManagedIsCorrect(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) {
    Boolean expectedValue =
        Boolean.parseBoolean(expectedDataItem.get(IExpectedKeys.BATCH_MANAGED).toString());
    Boolean actualValue =
        Boolean.parseBoolean(actualDataItem.get(IResponseKeys.BATCH_MANAGED).toString());
    Assert.assertEquals(
        ASSERTION_MSG + " Batch Managed is not correct", expectedValue, actualValue);
  }

  private void assertThatOldItemNumberIsCorrect(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) {
    Object expectedValue = expectedDataItem.get(IExpectedKeys.OLD_ITEM_REFERENCE);
    Object actualValue = actualDataItem.get(IResponseKeys.OLD_ITEM_REFERENCE);
    Assert.assertEquals(
        ASSERTION_MSG + " Old Item Number is not correct", expectedValue, actualValue);
  }

  public void assertThatViewAllItemsExist(DataTable itemViewAllDataTable) {
    List<Map<String, String>> itemsList = itemViewAllDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsList) {
      MObItemGeneralModel mObItemGeneralModel =
          (MObItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getViewAllItems", constructViewAllItemQueryParams(item));
      assertNotNull(
          ASSERTION_MSG
              + " item with code : "
              + item.get(IFeatureFileCommonKeys.ITEM_CODE)
              + " Doesn't exist!",
          mObItemGeneralModel);
      assertThatBusinessUnitsAreEqual(
          item.get(IFeatureFileCommonKeys.ITEM_CODE),
          getBusinessUnitCodes(item.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(", ")),
          mObItemGeneralModel.getPurchasingUnitCodes());
    }
  }

  private Object[] constructViewAllItemQueryParams(Map<String, String> item) {

    return new Object[] {
      item.get(IMasterDataFeatureFileCommonKeys.ITEM_GROUP).split(" - ")[0],
      item.get(IFeatureFileCommonKeys.ITEM_CODE),
      item.get(IFeatureFileCommonKeys.NAME),
      item.get(IMasterDataFeatureFileCommonKeys.BASE_UNIT).split(" - ")[0],
      item.get(IMasterDataFeatureFileCommonKeys.BASE_UNIT).split(" - ")[1],
      Boolean.valueOf(item.get(IMasterDataFeatureFileCommonKeys.BATCH_MANAGED)),
      item.get(IMasterDataFeatureFileCommonKeys.OLD_ITEM_REFERENCE),
      item.get(IMasterDataFeatureFileCommonKeys.ITEM_TYPE).split(" - ")[0],
      item.get(IMasterDataFeatureFileCommonKeys.ITEM_TYPE).split(" - ")[1]
    };
  }

  public void assertTotalNumberOfItemsExist(Integer expectedItemsCount) {
    Long actualItemsCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getViewAllItemsCount");
    assertEquals(
        ASSERTION_MSG + " Total number of Items is not equal response size",
        Long.valueOf(expectedItemsCount),
        actualItemsCount);
  }

  public String getRequestLocale() {
    return requestLocale;
  }
}
