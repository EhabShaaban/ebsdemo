package com.ebs.dda.masterdata.itemgroup;

public interface ICObItemGroupRestUrls {


  String ITEM_GROUP_READ_ALL_URL = "/services/material";
  String CREATE_ITEM_URL = "/command/itemGroup/create";
  String DELETE_ITEM_URL = "/command/itemGroup/delete/";


}
