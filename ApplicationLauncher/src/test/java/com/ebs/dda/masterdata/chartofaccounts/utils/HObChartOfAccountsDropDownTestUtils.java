package com.ebs.dda.masterdata.chartofaccounts.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccountsGeneralModel;
import com.ebs.dda.masterdata.chartofaccounts.IChartOfAccountsRestUrls;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class HObChartOfAccountsDropDownTestUtils extends CommonTestUtils {

  private ObjectMapper objectMapper;

  public HObChartOfAccountsDropDownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    return str.toString();
  }

  public String readAllChartOfAccounts() {
    return IChartOfAccountsRestUrls.CHART_OF_ACCOUNT_LIMITED_LEAFS;
  }

  public void assertThatChartOfAccountsExist(DataTable chartOfAccountsDataTable) {
    List<Map<String, String>> chartOfAccountsList =
        chartOfAccountsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> chartOfAccount : chartOfAccountsList) {

      HObChartOfAccountsGeneralModel hObChartOfAccountsGeneralModel =
          (HObChartOfAccountsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getChartOfAccounts", constructCOAQueryParams(chartOfAccount));

      assertNotNull(hObChartOfAccountsGeneralModel);
    }
  }

  private Object[] constructCOAQueryParams(Map<String, String> chartOfAccount) {
    return new Object[] {
      chartOfAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT).split(" - ")[0],
      chartOfAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT).split(" - ")[1],
      chartOfAccount.get(IAccountingFeatureFileCommonKeys.LEVEL),
      "%" + chartOfAccount.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertResponseMatchesExpected(Response response, DataTable chartOfAccountsDataTable)
      throws IOException, JSONException {
    List<Map<String, String>> chartOfAccountsExpectedList =
        chartOfAccountsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> chartOfAccountsActualList = getListOfMapsFromResponse(response);
    for (int i = 0; i < chartOfAccountsActualList.size(); i++) {
      assertActualAccountMatchesExpected(
          chartOfAccountsExpectedList.get(i), chartOfAccountsActualList.get(i));
    }
  }

  private void assertActualAccountMatchesExpected(
      Map<String, String> expectedChartOfAccounts, HashMap<String, Object> actualChartOfAccounts)
      throws IOException, JSONException {
    assertEquals(
        expectedChartOfAccounts.get(IAccountingFeatureFileCommonKeys.ACCOUNT).split(" - ")[0],
        actualChartOfAccounts.get(IHObChartOfAccountsGeneralModel.USER_CODE));

    assertEquals(
        expectedChartOfAccounts.get(IAccountingFeatureFileCommonKeys.ACCOUNT).split(" - ")[1],
        getEnglishLocaleFromLocalizedResponse(
            objectMapper.writeValueAsString(
                actualChartOfAccounts.get(IHObChartOfAccountsGeneralModel.ACCOUNT_NAME))));

    assertEquals(
        expectedChartOfAccounts.get(IAccountingFeatureFileCommonKeys.LEVEL),
        actualChartOfAccounts.get(IHObChartOfAccountsGeneralModel.LEVEL));

    assertTrue(
        actualChartOfAccounts
            .get(IHObChartOfAccountsGeneralModel.STATE)
            .toString()
            .contains(expectedChartOfAccounts.get(IFeatureFileCommonKeys.STATE)));
  }

  public void assertThatAllChartOfAccountsExist(Integer recordsNumber) {
    Long accountsCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getChartOfAccountsCount");
    assertEquals(Long.valueOf(recordsNumber), accountsCount);
  }
}
