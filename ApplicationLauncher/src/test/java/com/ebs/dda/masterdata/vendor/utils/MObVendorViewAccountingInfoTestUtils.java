package com.ebs.dda.masterdata.vendor.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.interfaces.IResponseKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IVendorExpectedKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MObVendorViewAccountingInfoTestUtils extends MObVendorCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[AccountingDetails][View] ";

  public MObVendorViewAccountingInfoTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    return str.toString();
  }

  public void assertResponseDataIsCorrect(
      DataTable expectedAccountingInfoTable, Response vendorAccountingInfoResponse)
      throws Exception {
    Map<String, String> actualMap =
        vendorAccountingInfoResponse.body().jsonPath().get(CommonKeys.RESULT);
    Map<String, String> expectedMap =
        expectedAccountingInfoTable.asMaps(String.class, String.class).get(0);
    ObjectMapper mapperObj = new ObjectMapper();
    String localizedString =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.ACCOUNT_NAME));
    assertEquals(ASSERTION_MSG +
        IVendorExpectedKeys.ACCOUNT_NAME + " Doesn't equal",
        expectedMap.get(IVendorExpectedKeys.ACCOUNT_NAME),
        getLocaleFromLocalizedResponse(localizedString, "en"));
    assertEquals(ASSERTION_MSG +
        IVendorExpectedKeys.ACCOUNT_CODE + " Doesn't equal",
        expectedMap.get(IVendorExpectedKeys.ACCOUNT_CODE),
        actualMap.get(IResponseKeys.ACCOUNT_CODE));
  }

  public Response sendReadAccountingInfoDetailsRequest(Cookie userCookie, String vendorCode) {
    String readUrl = IMObVendorRestUrls.READ_ACCOUNTING_INFO_RESTURL + vendorCode;
    return sendGETRequest(userCookie, readUrl);
  }

  public void assertThatVendorHasAccountInfo(DataTable accountingInfoDataTable) throws Exception {
    List<Map<String, String>> vendorAccountInfoMaps =
        accountingInfoDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedVendorAccountInfoMap : vendorAccountInfoMaps) {
      IObVendorAccountingInfoGeneralModel accountingInfoGeneralModel =
          getIObAccountingInfoGeneralModel(
              expectedVendorAccountInfoMap.get(IVendorExpectedKeys.VENDOR_CODE));
      ObjectMapper mapperObj = new ObjectMapper();
      String localizedString =
          mapperObj.writeValueAsString(accountingInfoGeneralModel.getAccountName());
      assertEquals(ASSERTION_MSG +
          IVendorExpectedKeys.ACCOUNT_NAME + " Doesn't equal",
          expectedVendorAccountInfoMap.get(IVendorExpectedKeys.ACCOUNT_NAME),
          getLocaleFromLocalizedResponse(localizedString, "en"));
      assertEquals(ASSERTION_MSG +
          IVendorExpectedKeys.ACCOUNT_CODE + " Doesn't equal",
          expectedVendorAccountInfoMap.get(IVendorExpectedKeys.ACCOUNT_CODE),
          accountingInfoGeneralModel.getAccountCode());

      String actualBusinessUnitsOrdered =
          getOrderBusinessUnits(accountingInfoGeneralModel.getVendorPurchaseUnitCodes());

      String expectedBusinessUnitsOrdered =
          getOrderBusinessUnits(
              expectedVendorAccountInfoMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT));

      assertEquals(ASSERTION_MSG +
          "BusinessUnits are not equal", expectedBusinessUnitsOrdered, actualBusinessUnitsOrdered);
    }
  }
}
