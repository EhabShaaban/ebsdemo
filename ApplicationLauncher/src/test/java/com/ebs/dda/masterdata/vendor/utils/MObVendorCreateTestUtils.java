package com.ebs.dda.masterdata.vendor.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.vendor.CObVendor;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorCreationValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorFeatureFileCommonKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IVendorExpectedKeys;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class MObVendorCreateTestUtils extends MObVendorCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Create] ";

  public MObVendorCreateTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/vendor/Vendor_Create_Clear.sql");
    return str.toString();
  }

  public JsonObject prepareVendorJsonObject(DataTable vendorData) {
    Map<String, String> vendorDataMap = vendorData.asMaps(String.class, String.class).get(0);

    JsonObject vendorObject = new JsonObject();

    addValueToJsonObject(
        vendorObject,
        IMObVendorCreationValueObject.VENDOR_NAME,
        vendorDataMap.get(IVendorExpectedKeys.VENDOR_NAME));
    vendorObject.add(
            IMObVendorCreationValueObject.PURCHASE_UNITS,
            preparePurchaseUnitsJsonArray(vendorDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT)));

    addValueToJsonObject(
        vendorObject,
        IMObVendorCreationValueObject.PURCHASE_RESPONSIBLE_CODE,
        vendorDataMap.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE));

    addValueToJsonObject(
        vendorObject,
        IMObVendorCreationValueObject.OLD_VENDOR_REFERENCE,
        vendorDataMap.get(IMObVendorFeatureFileCommonKeys.OLD_VENDOR_REFERENCE));

    addValueToJsonObject(
        vendorObject,
        IMObVendorCreationValueObject.ACCOUNT_WITH_VENDOR,
        vendorDataMap.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR));

    addValueToJsonObject(
        vendorObject,
        IMObVendorCreationValueObject.VENDOR_TYPE,
        vendorDataMap.get(IMObVendorFeatureFileCommonKeys.VENDOR_TYPE));

    return vendorObject;
  }

  private JsonArray preparePurchaseUnitsJsonArray(String purchaseUnitData) {
    String[] purchaseUnitCodes = purchaseUnitData.split(",");
    JsonArray vendorPurchaseUnits = new JsonArray();
    for (String purchaseUnitCode : purchaseUnitCodes) {
      JsonObject vendorPurchaseUnit = new JsonObject();
      vendorPurchaseUnit.addProperty(
          IMObVendorCreationValueObject.PURCHASE_UNIT_CODE, purchaseUnitCode.trim());
      vendorPurchaseUnits.add(vendorPurchaseUnit);
    }
    return vendorPurchaseUnits;
  }

  public void assertCreatedVendorDataExists(DataTable vendorData) {
    Map<String, String> createdVendorMap = vendorData.asMaps(String.class, String.class).get(0);

    MObVendorGeneralModel vendorGeneralModel =
        (MObVendorGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCreatedVendor", constructCreateVendorParams(createdVendorMap));

    assertNotNull(ASSERTION_MSG + "Vendor Doesn't exist", vendorGeneralModel);
    String expectedPurchaseUnitCodes = createdVendorMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    assertThatPurchaseUnitsCodeAreEquals(
        expectedPurchaseUnitCodes, vendorGeneralModel.getPurchasingUnitCodes());
    String expectedAccountWithVendor =
        createdVendorMap.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR);
    String actualAccountWithVendor = vendorGeneralModel.getAccountWithVendor();
    if (!expectedAccountWithVendor.isEmpty()) {
      assertEquals(
          ASSERTION_MSG + IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR + " Doesn't equal",
          expectedAccountWithVendor,
          actualAccountWithVendor);
    }
  }

  private Object[] constructCreateVendorParams(Map<String, String> vendorData) {
    return new Object[] {
      vendorData.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE),
      vendorData.get(IMObVendorFeatureFileCommonKeys.OLD_VENDOR_REFERENCE),
      vendorData.get(IFeatureFileCommonKeys.CREATED_BY),
      vendorData.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      vendorData.get(IFeatureFileCommonKeys.CREATION_DATE),
      vendorData.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      vendorData.get(IFeatureFileCommonKeys.VENDOR_CODE),
      vendorData.get(IFeatureFileCommonKeys.VENDOR_NAME),
      vendorData.get(IMObVendorFeatureFileCommonKeys.VENDOR_TYPE)
    };
  }

  public void assertThatPurchaseResponsibleDoesNotExist(DataTable purchaseResponsiblesDataTable) {
    Map<String, String> purchaseResponsible =
        purchaseResponsiblesDataTable.asMaps(String.class, String.class).get(0);
    String purchaseResponsibleName =
        (String)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseResponsibleNameByCode",
                purchaseResponsible.get(IFeatureFileCommonKeys.CODE));
    Assert.assertNull(ASSERTION_MSG + "PurchaseResponsible Doesn't exist", purchaseResponsibleName);
  }

  public void assertThatBusinessUnitsDoesNotExist(DataTable businessUnitsDataTable) {
    Map<String, String> businessUnit =
        businessUnitsDataTable.asMaps(String.class, String.class).get(0);
    String businessUnitName =
        (String)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseUnitNameByCode", businessUnit.get(IFeatureFileCommonKeys.CODE));
    Assert.assertNull(ASSERTION_MSG + "BusinessUnit Doesn't exist", businessUnitName);
  }

  public void assertThatVendorTypesDoesNotExist(DataTable vendorTypesDataTable) {
    Map<String, String> vendorType = vendorTypesDataTable.asMaps(String.class, String.class).get(0);
    CObVendor cObVendor =
        (CObVendor)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorTypeByCode", vendorType.get(IFeatureFileCommonKeys.CODE));
    Assert.assertNull(ASSERTION_MSG + "VendorType still exist", cObVendor);
  }
}
