package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemReadAuthorizedItemsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemReadAuthorizedItemsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private MObItemReadAuthorizedItemsTestUtils utils;

  public MObItemReadAuthorizedItemsStep() {

    When(
        "^\"([^\"]*)\" requests to read all Items$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IMObItemRestUrls.READ_AUTHORIZED_ITEMS_URL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Items values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable itemsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatAuthorizedItemsAreReturned(response, itemsDataTable);
        });
    Then(
        "^total number of records returned to \"([^\"]*)\" is \"([^\"]*)\"$",
        (String userName, String recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsReturned(response, Integer.valueOf(recordsNumber));
        });

    When(
        "^\"([^\"]*)\" requests to read all Items with business unit \"([^\"]*)\"$",
        (String userName, String businessUnit) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(
                  cookie, IMObItemRestUrls.ITEM_READ_ALL_URL + "/" + businessUnit.split(" - ")[0]);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read all Items with type code \"([^\"]*)\"$",
        (String userName, String type) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IMObItemRestUrls.ITEM_READ_ALL_URL + "/type/" + type);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new MObItemReadAuthorizedItemsTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of Items dropdown")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }
}
