package com.ebs.dda.masterdata.employee;

public interface IMObEmployeeRestUrls {

  String EMPLOYEE_QUERY_URL = "/services/masterdataobjects/employees";
  String READ_ALL = EMPLOYEE_QUERY_URL + "/";

}
