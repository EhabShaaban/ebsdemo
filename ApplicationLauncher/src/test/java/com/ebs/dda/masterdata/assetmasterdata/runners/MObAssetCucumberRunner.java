package com.ebs.dda.masterdata.assetmasterdata.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/assetmasterdata/AssetMasterData_Types_Dropdown.feature",
      "classpath:features/masterdata/assetmasterdata/AssetMasterData_AllowedActions_HomeScreen.feature",
      "classpath:features/masterdata/assetmasterdata/AssetMasterData_ViewAll.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.assetmasterdata"},
    strict = true,
    tags = "not @Future")
public class MObAssetCucumberRunner {}
