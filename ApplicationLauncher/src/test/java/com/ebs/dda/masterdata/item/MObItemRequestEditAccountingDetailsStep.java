package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemRequestEditAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObItemRequestEditAccountingDetailsStep extends SpringBootRunner implements En {
  private static boolean hasBeenExecuted = false;
  private MObItemRequestEditAccountingDetailsTestUtils utils;

  public MObItemRequestEditAccountingDetailsStep() {

    Given(
        "^the following Accounts exist:$",
        (DataTable AccountsDataTable) -> {
          utils.assertThatAccountsExist(AccountsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to edit AccountingDetails section of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          String lockUrl = IMObItemRestUrls.LOCK_ACCOUNTING_DETAILS_SECTION + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, itemCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^AccountingDetails section of Item with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String itemCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, itemCode, IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened the AccountingDetails section Item with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String itemCode) -> {
          String lockUrl = IMObItemRestUrls.LOCK_ACCOUNTING_DETAILS_SECTION + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, itemCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^AccountingDetails section of Item with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String itemCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          Response response =
              userActionsTestUtils.lockSection(
                  userName, IMObItemRestUrls.LOCK_ACCOUNTING_DETAILS_SECTION + itemCode, itemCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, itemCode, IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving AccountingDetails section of Item with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String date) -> {
          utils.freeze(date);
          String unLockURL = IMObItemRestUrls.UNLOCK_ACCOUNTING_DETAILS_SECTION + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on AccountingDetails section of Item with code \"([^\"]*)\" is released$",
        (String userName, String itemCode) -> {
          utils.assertThatLockIsReleased(
              userName, itemCode, IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving AccountingDetails section of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          String unLockURL = IMObItemRestUrls.UNLOCK_ACCOUNTING_DETAILS_SECTION + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemRequestEditAccountingDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Accounting Details section")
        || contains(scenario.getName(), "Request to Cancel saving Accounting Details section")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Accounting Details section")
        || contains(scenario.getName(), "Request to Cancel saving Accounting Details section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IMObItemRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
