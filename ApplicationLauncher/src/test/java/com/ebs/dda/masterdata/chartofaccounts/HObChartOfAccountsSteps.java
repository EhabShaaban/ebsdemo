package com.ebs.dda.masterdata.chartofaccounts;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccountCreateValueObject;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsAllowedActionsTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsCreateHPTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsCreateValTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;
import org.springframework.beans.factory.InitializingBean;

public class HObChartOfAccountsSteps extends SpringBootRunner implements En, InitializingBean {

	public static final String CREATION_SCENARIO_NAME = "Create GLAccount";
	public static final String ALLOWED_ACTION_SCENARIO_NAME = "Allowed Actions Chart Of Accounts";
	private static boolean hasBeenExecutedOnce = false;
	private HObChartOfAccountsCreateHPTestUtils utils;
	private HObChartOfAccountsCreateValTestUtils chartOfAccountsCreateValTestUtils;
	private HObChartOfAccountsAllowedActionsTestUtils chartOfAccountsAllowedActionsTestUtils;

	public HObChartOfAccountsSteps() {

		Given("^CurrentDateTime = \"([^\"]*)\"$", (String currentDateTime) -> {
			utils.freeze(currentDateTime);
		});

		Given("^Last created GLAccount of Level \"([^\"]*)\" was with AccountCode \"([^\"]*)\"$",
						(String level, String code) -> {
							utils.assertThatIChartOfAccountWithCodeAndLevelExists(level, code);
						});

		When("^\"([^\"]*)\" creates GLAccount with following values:$",
						(String username, DataTable accountTable) -> {
							Response response = utils.createAccount(accountTable,
											userActionsTestUtils.getUserCookie(username));
							userActionsTestUtils.setUserResponse(username, response);
						});

		Then("^a new GLAccount is created as follows:$", (DataTable AccountTable) -> {
			utils.assertThatAccountIsCreatedSuccessfully(AccountTable);
		});

		Given("^last created GLAccount below GLAccount \"([^\"]*)\" was with code \"([^\"]*)\"$",
						(String account, String lastChildCode) -> {
							utils.assertLastCreatedAccountBelowTheParentAccount(account,
											lastChildCode);
						});

		Given("^GLAccount with code \"([^\"]*)\" has no child$", (String accountCode) -> {
			utils.assertHasNoChild(accountCode);
		});

		Then("^the following error message is attached to ParentCode field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
						(String msgCode, String userName) -> {
							Response userResponse = userActionsTestUtils.getUserResponse(userName);
							chartOfAccountsCreateValTestUtils.assertThatFieldHasErrorMessageCode(
											userResponse, msgCode,
											IHObChartOfAccountCreateValueObject.PARENT);
						});

		When("^\"([^\"]*)\" requests to read actions of Chart Of Accounts home screen$",
						(String userName) -> {
							String readUrl = IChartOfAccountsRestUrls.AUTHORIZED_ACTIONS;
							Response response = chartOfAccountsAllowedActionsTestUtils
											.sendGETRequest(userActionsTestUtils
															.getUserCookie(userName), readUrl);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
						(String actionsDataTable, String userName) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							chartOfAccountsAllowedActionsTestUtils
											.assertThatTheFollowingActionsExist(response,
															actionsDataTable);
						});
		And("^the GLAccount Details with code \"([^\"]*)\" is updated as follows:$",
						(String accountCode, DataTable accountDetailsDataTable) -> {
							utils.assertThatGLAccountDetailsIsUpdated(accountCode,
											accountDetailsDataTable);
						});
		And("^the GLAccounts configuration is updated as follows:$",
						(DataTable glAccountConfigurationDataTable) -> {
							utils.assertThatGLAccountsConfigurationIsUpdated(
											glAccountConfigurationDataTable);
						});
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		utils = new HObChartOfAccountsCreateHPTestUtils(properties);
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		chartOfAccountsCreateValTestUtils = new HObChartOfAccountsCreateValTestUtils(
						getProperties());
		chartOfAccountsCreateValTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		chartOfAccountsAllowedActionsTestUtils = new HObChartOfAccountsAllowedActionsTestUtils(
						properties);
		chartOfAccountsAllowedActionsTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	@Before
	public void before(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), ALLOWED_ACTION_SCENARIO_NAME)
						|| contains(scenario.getName(), CREATION_SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecutedOnce) {
				databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
				hasBeenExecutedOnce = true;
			}
			databaseConnector.executeSQLScript(
							HObChartOfAccountsCreateHPTestUtils.clearChartOfAccounts());

		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), ALLOWED_ACTION_SCENARIO_NAME)
						|| contains(scenario.getName(), CREATION_SCENARIO_NAME)) {
			databaseConnector.closeConnection();
			utils.unfreeze();
		}
	}
}
