package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.MObCustomerDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObCustomerDeleteStep extends SpringBootRunner implements En {

  private MObCustomerDeleteTestUtils utils;

  public MObCustomerDeleteStep() {
    When(
        "^\"([^\"]*)\" requests to delete Customer with code \"([^\"]*)\"$",
        (String userName, String customerCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesCustomerRestUrl(customerCode);
          Response response = utils.sendDeleteRequest(userCookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Customer with code \"([^\"]*)\" is deleted$",
        (String customerCode) -> {
          utils.assertThatCustomerIsDeletedSuccessfully(customerCode);
        });
    Given(
        "^\"([^\"]*)\" first deleted the Customer with code \"([^\"]*)\" successfully$",
        (String userName, String customerCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteSalesCustomerRestUrl(customerCode);
          utils.sendDeleteRequest(userCookie, restUrl);
          utils.assertThatCustomerIsDeletedSuccessfully(customerCode);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObCustomerDeleteTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete Customer,")) {
      databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
    }
  }
}
