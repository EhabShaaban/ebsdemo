package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordGeneralDataRequestEditCancelTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class ItemVendorRecordGeneralDataRequestEditCancelStep extends SpringBootRunner
    implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordGeneralDataRequestEditCancelTestUtils utils;

  public ItemVendorRecordGeneralDataRequestEditCancelStep() {

    When(
        "^\"([^\"]*)\" requests to edit GeneralData section of IVR with code \"([^\"]*)\"$",
        (String userName, String itemVendorRecordCode) -> {
          String lock_url = IVRRestUrls.LOCK_GENERAL_DATA_SECTION + itemVendorRecordCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, itemVendorRecordCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^GeneralData section of IVR with \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String itemVendorRecordCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              itemVendorRecordCode,
              IItemVendorRecordSectionNames.GENERAL_DATA_SECTION,
              utils.IVR_JMX_LOCK_BEAN_NAME);
        });

    Given(
        "^\"([^\"]*)\" first opened GeneralData section of IVR with code \"([^\"]*)\" in edit mode successfully$",
        (String userName, String itemVendorRecordCode) -> {
          String lock_url = IVRRestUrls.LOCK_GENERAL_DATA_SECTION + itemVendorRecordCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, itemVendorRecordCode);
          userActionsTestUtils.setUserResponse(userName, response);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              itemVendorRecordCode,
              IItemVendorRecordSectionNames.GENERAL_DATA_SECTION,
              utils.IVR_JMX_LOCK_BEAN_NAME);
        });

    Given(
        "^GeneralData section of IVR with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String itemVendorRecordCode, String userName, String date) -> {
          utils.freeze(date);
          String lockURL = IVRRestUrls.LOCK_GENERAL_DATA_SECTION + itemVendorRecordCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, itemVendorRecordCode);
          userActionsTestUtils.setUserResponse(userName, response);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              itemVendorRecordCode,
              IItemVendorRecordSectionNames.GENERAL_DATA_SECTION,
              utils.IVR_JMX_LOCK_BEAN_NAME);
        });

    When(
        "^\"([^\"]*)\" cancels saving  GeneralData section of IVR with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemVendorRecordCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockUrl = IVRRestUrls.UNLOCK_GENERAL_DATA_SECTION + itemVendorRecordCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on GeneralData section of IVR with code \"([^\"]*)\" is released$",
        (String username, String itemVendorRecordCode) -> {
          utils.assertThatLockIsReleased(
              username, itemVendorRecordCode, IItemVendorRecordSectionNames.GENERAL_DATA_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving GeneralData section of IVR with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemVendorRecordCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockUrl = IVRRestUrls.UNLOCK_GENERAL_DATA_SECTION + itemVendorRecordCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving GeneralData section of IVR with code \"([^\"]*)\"$",
        (String userName, String itemVendorRecordCode) -> {
          String unlockUrl = IVRRestUrls.UNLOCK_GENERAL_DATA_SECTION + itemVendorRecordCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following authorized reads are returned to \"([^\"]*)\": \"([^\"]*)\"$",
        (String userName, String authorizedReadsData) -> {
          List<String> authorizedReadsList =
              utils.convertAuthorizedReadsStringToList(authorizedReadsData);
          utils.assertThatUserHasReadsAuthorization(
              authorizedReadsList, userActionsTestUtils.getUserResponse(userName));
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new ItemVendorRecordGeneralDataRequestEditCancelTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit GeneralData Section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getViewAllDbScriptsPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to edit GeneralData Section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockSectionForAllUsers(IVRRestUrls.UNLOCK_GENERAL_DATA_SECTION);
    }
  }
}
