package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.ebs.dda.masterdata.item.IResponseKeys;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MObItemGeneralDataTestUtils extends MObItemCommonTestUtils {

  public MObItemGeneralDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
        str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    return str.toString();
  }

  public JsonObject createItemGeneralDataJsonObject(
      String itemCode, DataTable itemGeneralDataTable) {
    JsonObject itemGeneralDataObject = new JsonObject();
    Map<String, String> itemGeneralData =
        itemGeneralDataTable.asMaps(String.class, String.class).get(0);
    JsonArray purchaseUnitArray = getPurchaseUnits(itemGeneralData);
    addPropertiesToItemGeneralData(
        itemGeneralDataObject, purchaseUnitArray, itemCode, itemGeneralData);
    return itemGeneralDataObject;
  }

  public Response saveItemGeneralDataSection(
      Cookie userCookie, JsonObject itemObject, String itemCode) {
    String restURL = IMObItemRestUrls.SAVE_ITEM_GENERAL_DATA_URL + itemCode;
    return sendPUTRequest(userCookie, restURL, itemObject);
  }

  private JsonArray getPurchaseUnits(Map<String, String> itemGeneralData) {
    List<String> purchaseUnits = Arrays.asList(itemGeneralData.get("PurchasingUnit").split(","));
    purchaseUnits.replaceAll(String::trim);
    return createJsonArray(purchaseUnits);
  }

  private JsonArray createJsonArray(List<String> purchaseUnits) {
    JsonArray purchaseUnitArray = new JsonArray();
    for (String purchaseUnit : purchaseUnits) {
      purchaseUnitArray.add(purchaseUnit);
    }
    return purchaseUnitArray;
  }

  private void addPropertiesToItemGeneralData(
      JsonObject itemGeneralDataObject,
      JsonArray purchaseUnitArray,
      String itemCode,
      Map<String, String> itemGeneralData) {
    itemGeneralDataObject.add(IResponseKeys.PURCHASING_UNITS_CODES, purchaseUnitArray);
    itemGeneralDataObject.addProperty(
        IResponseKeys.ITEM_NAME, itemGeneralData.get(IFeatureFileCommonKeys.NAME));
    itemGeneralDataObject.addProperty(
        IMObItem.MARKET_NAME, itemGeneralData.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME));
    itemGeneralDataObject.addProperty(
            IMObItem.PRODUCT_MANAGER_CODE,
            convertEmptyStringToNull(getFirstValue(itemGeneralData, IFeatureFileCommonKeys.PRODUCT_MANAGER)));
    itemGeneralDataObject.addProperty(IResponseKeys.ITEM_CODE, itemCode);
  }

  private String getFirstValue(Map<String, String> itemGeneralData, String key) {
    return itemGeneralData.get(key).split(" - ")[0];
  }
}
