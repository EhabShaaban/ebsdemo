package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.IObCustomerAddressesReadAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObCustomerAddressReadAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObCustomerAddressesReadAllTestUtils utils;

  public IObCustomerAddressReadAllStep() {
    Given(
        "^the following Addresses for Customers exist:$",
        (DataTable customerAddressesDataTable) -> {
          utils.assertThatTheFollowingCustomersHaveTheFollowingAddresses(
              customerAddressesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all CustomerAddresses for Customer with code \"([^\"]*)\" in a dropdown$",
        (String userName, String customerCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getCustomerAddressesUrl(customerCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following CustomerAddresses values will be presented to \"([^\"]*)\" in the dropdown:$",
        (String userName, DataTable customerAddressesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadAllAddressesResponseIsCorrect(response, customerAddressesDataTable);
        });

    Then(
        "^total number of CustomerAddresses returned to \"([^\"]*)\" in the dropdown is equal to (\\d+)$",
        (String userName, Integer customerAddressesCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatCustomerAddressesResponseCountIsCorrect(response, customerAddressesCount);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of Customer Addresses")) {
      beforeSetup();
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of Customer Addresses")) {
      afterSetup();
    }
  }

  private void beforeSetup() throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new IObCustomerAddressesReadAllTestUtils(properties, entityManagerDatabaseConnector);

    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
      databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
      hasBeenExecutedOnce = true;
    }
  }

  private void afterSetup() throws SQLException {
    databaseConnector.closeConnection();
    utils.unfreeze();
  }
}
