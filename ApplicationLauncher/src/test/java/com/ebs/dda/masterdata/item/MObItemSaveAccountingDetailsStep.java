package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.item.IMObItemAccountingInfoValueObject;
import com.ebs.dda.masterdata.item.utils.MObItemSaveAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemSaveAccountingDetailsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private MObItemSaveAccountingDetailsTestUtils utils;

  public MObItemSaveAccountingDetailsStep() {

    When(
        "^\"([^\"]*)\" saves AccountingDetails section of Item with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String itemCode, String dateTime, DataTable AccountInfoDataTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveItemAccountingDetailsSection(userCookie, itemCode, AccountInfoDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

      When(
              "^\"([^\"]*)\" saves AccountingDetails section of Service Item with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
              (String userName, String itemCode, String dateTime, DataTable AccountInfoDataTable) -> {
                  utils.freeze(dateTime);
                  Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                  Response response =
                          utils.saveServiceItemAccountingDetailsSection(userCookie, itemCode, AccountInfoDataTable);
                  userActionsTestUtils.setUserResponse(userName, response);
              });

    Then(
        "^AccountingDetails Section of Item with Code \"([^\"]*)\" is updated as follows:$",
        ((String itemCode, DataTable AccountInfoDataTable) -> {
          utils.assertThatItemAccountUpdatedWithGivenValues(itemCode, AccountInfoDataTable);
        }));

      Then(
              "^AccountingDetails Section of Service Item with Code \"([^\"]*)\" is updated as follows:$",
              ((String itemCode, DataTable AccountInfoDataTable) -> {
                  utils.assertThatServiceItemAccountUpdatedWithGivenValues(itemCode, AccountInfoDataTable);
              }));

    Then(
        "^the following error message is attached to Account field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IMObItemAccountingInfoValueObject.ACCOUNT_CODE);
        });

    When(
        "^\"([^\"]*)\" saves AccountingDetails section of Item with Code \"([^\"]*)\" with the following values:$",
        (String userName, String itemCode, DataTable accountInfoDatatable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveItemAccountingDetailsSection(userCookie, itemCode, accountInfoDatatable);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemSaveAccountingDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Accounting Details section")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Accounting Details section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IMObItemRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
