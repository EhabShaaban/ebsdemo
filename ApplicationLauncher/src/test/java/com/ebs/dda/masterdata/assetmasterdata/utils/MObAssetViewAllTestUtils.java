package com.ebs.dda.masterdata.assetmasterdata.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.assetmasterdata.IMObAssetGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MObAssetViewAllTestUtils extends MObAssetCommonTestUtils {

  public MObAssetViewAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatAssetDataIsCorrect(DataTable assetssDataTable, Response response) throws Exception {
    List<Map<String, String>> expectedAssetsList =
        assetssDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualAssetsList = getListOfMapsFromResponse(response);
    Assert.assertEquals("size not equals", expectedAssetsList.size(), actualAssetsList.size());
    for (int i = 0; i < expectedAssetsList.size(); i++) {
      assertCorrectShownData(expectedAssetsList.get(i), actualAssetsList.get(i));
    }
  }

  private void assertCorrectShownData(
      Map<String, String> expectedAsset, HashMap<String, Object> actualAsset) throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedAsset, actualAsset);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IMObAssetGeneralModel.USER_CODE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IMasterDataFeatureFileCommonKeys.TYPE,
        IMObAssetGeneralModel.ASSET_TYPE_CODE,
        IMObAssetGeneralModel.ASSET_TYPE_NAME);
    mapAssertion.assertLocalizedValue(
        IFeatureFileCommonKeys.NAME, IMObAssetGeneralModel.ASSET_NAME);

    assertThatBusinessUnitCodesEquals(expectedAsset, actualAsset);

    mapAssertion.assertValue(
        IMasterDataFeatureFileCommonKeys.OLD_NUMBER, IMObAssetGeneralModel.OLD_NUMBER);
  }

  private void assertThatBusinessUnitCodesEquals(
      Map<String, String> expectedCustomer, HashMap<String, Object> actualCustomer) {
    String[] businessUnits = expectedCustomer.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(",");
    assertThatBusinessUnitsAreEqual(
        expectedCustomer.get(IFeatureFileCommonKeys.CODE),
        getBusinessUnitCodes(businessUnits),
        String.valueOf(actualCustomer.get(IMObAssetGeneralModel.BUSINESS_UNIT_CODES)));
  }
}
