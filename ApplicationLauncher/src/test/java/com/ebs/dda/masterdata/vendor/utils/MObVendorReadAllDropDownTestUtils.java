package com.ebs.dda.masterdata.vendor.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MObVendorReadAllDropDownTestUtils extends MObVendorCommonTestUtils {

  public MObVendorReadAllDropDownTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    return str.toString();
  }

  public void assertThatResponseVendorDataIsCorrect(Response response, DataTable vendorDataTable)
      throws Exception {
    List<Map<String, String>> vendorsList = vendorDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> vendorResponse = getListOfMapsFromResponse(response);
    for (int i = 0; i < vendorResponse.size(); i++) {
      MapAssertion mapAssertion = new MapAssertion(vendorsList.get(i), vendorResponse.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IFeatureFileCommonKeys.VENDOR, IMObVendor.CODE, IMObVendor.VENDOR_NAME);
    }
  }
}
