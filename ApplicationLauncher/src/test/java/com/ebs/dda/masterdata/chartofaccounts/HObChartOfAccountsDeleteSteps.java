package com.ebs.dda.masterdata.chartofaccounts;

import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsDeleteTestUtils;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class HObChartOfAccountsDeleteSteps extends SpringBootRunner
				implements En, InitializingBean {

	public static final String SCENARIO_NAME = "Delete GLAccount";
	private HObChartOfAccountsDeleteTestUtils utils;
	private DObJournalEntryTestUtils journalEntryTestUtils;
	private static boolean hasBeenExecuted = false;

	public HObChartOfAccountsDeleteSteps() {

		When("^\"([^\"]*)\" requests to delete GLAccount with code \"([^\"]*)\"$",
						(String userName, String glAccountCode) -> {
							Response response = utils.deleteGLAccount(
											userActionsTestUtils.getUserCookie(userName),
											glAccountCode);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Given("^\"([^\"]*)\" first deleted the GLAccount with code \"([^\"]*)\" successfully$",
						(String userName, String glAccountCode) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							Response response = utils.deleteGLAccount(userCookie, glAccountCode);
							utils.assertResponseSuccessStatus(response);
						});
		Then("^the following GLAccounts configuration relation with code \"([^\"]*)\" is deleted successfully$",
						(String glConfigurationCode) -> {
							utils.assertThatGLAccountConfigurationDoesNotExist(glConfigurationCode);

						});
		Then("^the following GLAccounts configuration relation with code \"([^\"]*)\" is not deleted$",
						(String glConfigurationCode) -> {
							utils.assertThatGLAccountConfigurationExists(glConfigurationCode);
						});
		Then("^the following GLAccount with code \"([^\"]*)\" is deleted successfully$",
						(String glAccountCode) -> {
							utils.assertThatGLAccountDoesNotExist(glAccountCode);
						});
		Then("^the following GLAccount with code \"([^\"]*)\" is not deleted$",
						(String glAccountCode) -> {
							utils.assertThatGLAccountExists(glAccountCode);

						});
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		utils = new HObChartOfAccountsDeleteTestUtils(properties);
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		journalEntryTestUtils = new DObJournalEntryTestUtils(properties);
		journalEntryTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	@Before
	public void before(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				journalEntryTestUtils.executeJournalEntryDetailSubAccountDeterminationFunction();
				hasBeenExecuted = true;
			}
			databaseConnector.executeSQLScript(
							HObChartOfAccountsDeleteTestUtils.clearChartOfAccounts());
			databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());

		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
			utils.unfreeze();
		}
	}
}
