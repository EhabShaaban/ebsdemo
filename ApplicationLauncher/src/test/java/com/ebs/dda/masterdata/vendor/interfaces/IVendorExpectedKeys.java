package com.ebs.dda.masterdata.vendor.interfaces;

public interface IVendorExpectedKeys {
  String VENDOR_CODE = "VendorCode";
  String VENDOR_NAME = "VendorName";
  String VENDOR_TYPE = "TypeName";
  String PURCHASING_UNIT = "PurchasingUnit";
  String PURCHASING_RESPONSIBLE = "PurchasingResponsible";
  String OLD_VENDOR_REFERENCE = "OldVendorReference";
  String ACCOUNT_WITH_VENDOR = "AccountWithVendor";
  String CREATION_DATE = "CreationDate";
  String ACCOUNT_CODE = "AccountCode";
  String ACCOUNT_NAME = "AccountName";

  String ACCOUNT = "Account";
}
