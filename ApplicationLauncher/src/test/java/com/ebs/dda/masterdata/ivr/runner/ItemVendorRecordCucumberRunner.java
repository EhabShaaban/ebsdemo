package com.ebs.dda.masterdata.ivr.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/item-vendor-record/IVR_Create_HP.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_Create_Val.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_Create_Auth.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_RequestEdit_GeneralData.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_View_GeneralData.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_Save_GeneralData_Val.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_Save_GeneralData_Auth.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_Save_GeneralData_HP.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_Delete.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_AllowedActions.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_NextPrev.feature",
      "classpath:features/masterdata/item-vendor-record/IVR_ViewAll.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.ivr"},
    strict = true,
    tags = "not @Future")
public class ItemVendorRecordCucumberRunner {}
