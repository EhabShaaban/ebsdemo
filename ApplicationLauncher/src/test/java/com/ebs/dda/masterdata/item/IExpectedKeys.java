package com.ebs.dda.masterdata.item;

public interface IExpectedKeys {
  String ALETRNATIVE_UNIT_CODE = "AlternateUnitCode";
  String CONVERSION_FACTOR = "ConversionFactor";
  String OLD_ITEM_NUMBER = "OldItemNumber";

  String LAST_UPDATED_BY = "LastUpdatedBy";
  String LAST_UPDATED_DATE = "LastUpdateDate";
  String ALTERNATIVE_UNIT_NAME = "AlternateUnit";

  String CREATION_DATE = "CreationDate";
  String MODIFICATION_DATE = "LastUpdateDate";
  String PURCHASING_UNIT = "PurchasingUnit";
  String PURCHASING_UNIT_CODES = "purchaseunitscodes";

  String ITEM_CODE = "ItemCode";
  String ITEM_NAME = "ItemName";
  String BASE_UNIT = "BaseUnit";
  String OLD_ITEM_REFERENCE = "OldItemReference";
  String BATCH_MANAGED = "BatchManaged";
  String ITEM_GROUP = "ItemGroup";
  String ACCOUNT_CODE = "AccountCode";
  String ACCOUNT_NAME = "AccountName";

  String ALTERNATIVE_UNIT = "AlternativeUnit";
  String NAME = "Name";


}
