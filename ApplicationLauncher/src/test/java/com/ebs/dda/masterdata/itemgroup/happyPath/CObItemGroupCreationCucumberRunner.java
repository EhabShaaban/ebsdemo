package com.ebs.dda.masterdata.itemgroup.happyPath;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"classpath:features/masterdata/item-group/ItemGroup_Create_HP.feature"},
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.itemgroup.happyPath"},
    strict = true,
    tags = "not @Future")
public class CObItemGroupCreationCucumberRunner {
  @Test
  public void test() {}
}
