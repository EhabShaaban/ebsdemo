package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemReadNextPrevTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObItemReadNextPrevStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemReadNextPrevTestUtils utils;

  public MObItemReadNextPrevStep() {

    Given(
        "^the following Items with the following order exist:$",
        (DataTable itemDataTable) -> {
          utils.assertItemsExistByCodeAndPurchaseUnits(itemDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view next Item of current Item with code \"([^\"]*)\"$",
        (String username, String currentItemCode) -> {
          Response response =
              utils.requestToViewNextItem(
                  userActionsTestUtils.getUserCookie(username), currentItemCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the next Item code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String nextItemCode, String username) -> {
          utils.assertThatNextItemCodeIsCorrect(
              nextItemCode, userActionsTestUtils.getUserResponse(username));
        });

    When(
        "^\"([^\"]*)\" requests to view previous Item of current Item with code \"([^\"]*)\"$",
        (String username, String currentItemCode) -> {
          Response response =
              utils.requestToViewPreviousItem(
                  userActionsTestUtils.getUserCookie(username), currentItemCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the previous Item code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String previousItemCode, String username) -> {
          utils.assertThatPreviousItemCodeIsCorrect(
              previousItemCode, userActionsTestUtils.getUserResponse(username));
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemReadNextPrevTestUtils(getProperties());
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read Next in Item")
        || contains(scenario.getName(), "Read Previous in Item")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }
}
