package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.customer.IIObCustomerAddressGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.customer.IMObCustomerRestURLs;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class IObCustomerAddressesReadAllTestUtils extends MObCustomerCommonTestUtils {

  public IObCustomerAddressesReadAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String prepareDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    return str.toString();
  }

  public void assertThatTheFollowingCustomersHaveTheFollowingAddresses(
      DataTable customerAddressesDataTable) {

    List<Map<String, String>> customerAddressesList =
        customerAddressesDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedCustomerAddress : customerAddressesList) {

      Object actualCustomerAddress =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCustomerAddress", constructCustomerAddressQueryParams(expectedCustomerAddress));

      Assert.assertNotNull(actualCustomerAddress);
    }
  }

  private Object[] constructCustomerAddressQueryParams(Map<String, String> customerAddressMap) {
    return new Object[] {
      customerAddressMap.get(IFeatureFileCommonKeys.CUSTOMER),
      Integer.parseInt(
          customerAddressMap.get(IMasterDataFeatureFileCommonKeys.CUSTOMER_ADDRESS_ID)),
      customerAddressMap.get(IMasterDataFeatureFileCommonKeys.ADDRESS_NAME),
      customerAddressMap.get(IMasterDataFeatureFileCommonKeys.ADDRESS_DESCRIPTION)
    };
  }

  public String getCustomerAddressesUrl(String customerCode) {
    return IMObCustomerRestURLs.QUERY
        + "/"
        + customerCode
        + IMObCustomerRestURLs.READ_ALL_ADDRESSES;
  }

  public void assertThatReadAllAddressesResponseIsCorrect(
      Response response, DataTable customerAddressesDataTable) {

    List<Map<String, String>> expectedAddresses =
        customerAddressesDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualAddresses = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedAddresses.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedAddresses.get(i), actualAddresses.get(i));

      mapAssertion.assertIntegerValue(
          IMasterDataFeatureFileCommonKeys.CUSTOMER_ADDRESS_ID, IIObCustomerAddressGeneralModel.ID);

      mapAssertion.assertLocalizedValue(
          IMasterDataFeatureFileCommonKeys.ADDRESS_NAME,
          IIObCustomerAddressGeneralModel.ADDRESS_NAME);

      mapAssertion.assertLocalizedValue(
          IMasterDataFeatureFileCommonKeys.ADDRESS_DESCRIPTION,
          IIObCustomerAddressGeneralModel.ADDRESS_DESCRIPTION);
    }
  }

  public void assertThatCustomerAddressesResponseCountIsCorrect(
      Response response, int customerAddressesCount) {
    List<HashMap<String, Object>> actualAddresses = getListOfMapsFromResponse(response);
    Assert.assertEquals(customerAddressesCount, actualAddresses.size());
  }
}
