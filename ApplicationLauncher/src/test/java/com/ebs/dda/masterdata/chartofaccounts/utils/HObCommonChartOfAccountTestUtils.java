package com.ebs.dda.masterdata.chartofaccounts.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class HObCommonChartOfAccountTestUtils extends CommonTestUtils {

	public HObCommonChartOfAccountTestUtils(Map<String, Object> properties) {
		setProperties(properties);
	}

	public String getDbScriptsOneTimeExecution() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/clearDataSqlScript.sql");
		return str.toString();
	}

	public static String clearChartOfAccounts() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/master-data/chart-of-accounts/HObChartOfAccounts_Clear.sql");
		return str.toString();
	}

	public void assetGLAccountConfigurationsExist(DataTable glAccountConfigDataTable) {
		List<Map<String, String>> glAccountConfigMaps = glAccountConfigDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> glAccountConfigMap : glAccountConfigMaps) {
			assetGLAccountConfigurationExists(glAccountConfigMap);
		}
	}

	private void assetGLAccountConfigurationExists(Map<String, String> glAccountConfigMap) {
		LObGlobalGLAccountConfigGeneralModel accountConfigGeneralModel = (LObGlobalGLAccountConfigGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getGLAccountConfig",
										constructGLAccountConfigQueryParams(glAccountConfigMap));
		Assert.assertNotNull("GLAccount Config does not exist!", accountConfigGeneralModel);
	}

	private Object[] constructGLAccountConfigQueryParams(Map<String, String> glAccountConfigMap) {
		return new Object[] {
						glAccountConfigMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT_CONFIG),
						glAccountConfigMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT), };
	}

	public void assertThatAccountsHasSubledger(DataTable accountsSubledgerDataTable) {
		List<Map<String, String>> accountsSubledger = accountsSubledgerDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> accountSubledger : accountsSubledger) {
			HObChartOfAccountsGeneralModel accountSubLedgerRecord = (HObChartOfAccountsGeneralModel) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getAccountAndSubLedger",
											accountSubledger.get(
															IAccountingFeatureFileCommonKeys.SUBLEDGER),
											accountSubledger.get(
															IAccountingFeatureFileCommonKeys.GL_ACCOUNT));
			assertNotNull("This Account has no Subledger", accountSubLedgerRecord);
		}
	}


}
