package com.ebs.dda.masterdata.assetmasterdata.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import java.util.Map;

public class MObAssetAllowedActionsHomeScreenTestUtil extends MObAssetCommonTestUtils {

  public MObAssetAllowedActionsHomeScreenTestUtil(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }
}
