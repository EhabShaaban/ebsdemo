package com.ebs.dda.masterdata.item.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.CObItem;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.MasterDataTestUtils;
import com.ebs.dda.masterdata.item.IExpectedKeys;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.ebs.dda.masterdata.item.IMObItemSectionNames;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MObItemCommonTestUtils extends MasterDataTestUtils {
	protected String ASSERTION_MSG = "[IMR]";
	Map<String, String> sectionsLockUrls;
	Map<String, String> sectionsUnlockUrls;
	private String ITEM_JMX_LOCK_BEAN_NAME = "ItemEntityLockCommand";

	public MObItemCommonTestUtils(Map<String, Object> properties) {
		super(properties);
		prepareSectionsLockUrlsMap();
		prepareSectionsUnlockUrlsMap();
	}

	public String getDbScriptsOneTimeExecution() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("db-scripts/clearDataSqlScript.sql");
		stringBuilder.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
		stringBuilder.append(",").append("db-scripts/CObMeasureScript.sql");
		stringBuilder.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
		stringBuilder.append(",").append("db-scripts/master-data/item/MasterData.sql");
		stringBuilder.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
		stringBuilder.append(",").append("db-scripts/master-data/customer/MasterData.sql");
		stringBuilder.append(",").append("db-scripts/master-data/item/CObItem.sql");
		stringBuilder.append(",").append("db-scripts/master-data/item/MObItem.sql");
		stringBuilder.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
		stringBuilder.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
		stringBuilder.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
		stringBuilder.append(",")
						.append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
		stringBuilder.append(",")
						.append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
		return stringBuilder.toString();
	}

	public String getDbScriptPath() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/master-data/item/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
		str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
		str.append("," + "db-scripts/master-data/item/CObItem.sql");
		str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
		str.append("," + "db-scripts/master-data/item/MObItem.sql");
		str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
		str.append("," + "db-scripts/master-data/item/purchase-order-items.sql");

		return str.toString();
	}

	public Response deleteItemByCode(Cookie userCookie, String itemCode) {
		String deleteURL = IMObItemRestUrls.DELETE_ITEM + itemCode;
		return sendDeleteRequest(userCookie, deleteURL);
	}

	public void assertThatResourceIsLockedByUser(String userName, String itemCode,
					String sectionName) throws Exception {
		assertThatResourceIsLockedByUser(userName, itemCode, sectionName, ITEM_JMX_LOCK_BEAN_NAME);
	}

	public void assertThatLockIsReleased(String userName, String code, String sectionName)
					throws Exception {
		super.assertThatLockIsReleased(userName, code, ITEM_JMX_LOCK_BEAN_NAME, sectionName);
	}

	public void assertItemsExistWithGeneralData(DataTable itemGeneralDataTable) throws Exception {
		List<Map<String, String>> itemGeneralDataMaps = itemGeneralDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> expectedItemGeneralDataMap : itemGeneralDataMaps) {
			MObItemGeneralModel itemGeneralModel = (MObItemGeneralModel) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getItemGeneralData",
											constructItemGeneralDataQueryParams(
															expectedItemGeneralDataMap));
			assertNotNull("Failure in Item with code: "
							+ expectedItemGeneralDataMap.get(IExpectedKeys.ITEM_CODE),
							itemGeneralModel);
			assertDateEquals(expectedItemGeneralDataMap.get(IExpectedKeys.CREATION_DATE),
							itemGeneralModel.getCreationDate());
			if (itemGeneralModel.getTypeCode().equals("COMMERCIAL")) {
				assertEquals(Boolean.parseBoolean(
								expectedItemGeneralDataMap.get(IExpectedKeys.BATCH_MANAGED)),
								itemGeneralModel.getIsBatchManaged());
			}
			assertThatPurchaseUnitsCodeAreEquals(
							expectedItemGeneralDataMap.get(IExpectedKeys.PURCHASING_UNIT),
							itemGeneralModel.getPurchasingUnitCodes());
		}
	}

	public void assertThatItemHasAccountInfo(DataTable accountingInfoDataTable) throws Exception {
		List<Map<String, String>> itemAccountInfoMaps = accountingInfoDataTable.asMaps(String.class,
						String.class);

		for (Map<String, String> expectedItemAccountInfoMap : itemAccountInfoMaps) {
			IObItemAccountingInfoGeneralModel accountingInfoGeneralModel = getIObAccountingInfoGeneralModel(
							expectedItemAccountInfoMap
											.get(IAccountingFeatureFileCommonKeys.ITEM_CODE));
			if (accountingInfoGeneralModel != null) {
				ObjectMapper mapperObj = new ObjectMapper();
				String localizedString = mapperObj
								.writeValueAsString(accountingInfoGeneralModel.getAccountName());
				assertEquals(expectedItemAccountInfoMap.get(IExpectedKeys.ACCOUNT_NAME),
								getLocaleFromLocalizedResponse(localizedString, "en"));

				assertEquals(expectedItemAccountInfoMap.get(IExpectedKeys.ACCOUNT_CODE),
								accountingInfoGeneralModel.getAccountCode());
			} else {
				assertTrue(expectedItemAccountInfoMap.get(IExpectedKeys.ACCOUNT_NAME).isEmpty());

				assertTrue(expectedItemAccountInfoMap.get(IExpectedKeys.ACCOUNT_CODE).isEmpty());
			}
		}
	}

	protected IObItemAccountingInfoGeneralModel getIObAccountingInfoGeneralModel(String itemCode) {
		return (IObItemAccountingInfoGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getItemAccountingDetails", itemCode);
	}

	public String getLockSectionUrl(String sectionName) {
		return sectionsLockUrls.get(sectionName);
	}

	protected Object[] constructItemGeneralDataQueryParams(
					Map<String, String> expectedItemGeneralDataMap) {
		return new Object[] { expectedItemGeneralDataMap.get(IExpectedKeys.ITEM_GROUP),
						expectedItemGeneralDataMap.get(IExpectedKeys.ITEM_CODE),
						expectedItemGeneralDataMap.get(IExpectedKeys.ITEM_NAME),
						expectedItemGeneralDataMap.get(IExpectedKeys.BASE_UNIT),
						expectedItemGeneralDataMap.get(IExpectedKeys.OLD_ITEM_REFERENCE),
						expectedItemGeneralDataMap
										.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
						expectedItemGeneralDataMap.get(IFeatureFileCommonKeys.TYPE),
						expectedItemGeneralDataMap.get(IFeatureFileCommonKeys.PRODUCT_MANAGER) };
	}

	private void prepareSectionsLockUrlsMap() {
		sectionsLockUrls = new HashMap<>();
		sectionsLockUrls.put(IMObItemSectionNames.GENERAL_DATA_SECTION,
						IMObItemRestUrls.LOCK_GENERAL_DATA_SECTION);
		sectionsLockUrls.put(IMObItemSectionNames.UOM_SECTION,
						IMObItemRestUrls.LOCK_UNIT_OF_MEASURE_URL);
	}

	private void prepareSectionsUnlockUrlsMap() {
		sectionsUnlockUrls = new HashMap<>();
		sectionsUnlockUrls.put(IMObItemSectionNames.GENERAL_DATA_SECTION,
						IMObVendorRestUrls.UNLOCK_GENERAL_DATA_SECTION);
		sectionsUnlockUrls.put(IMObItemSectionNames.UOM_SECTION,
						IMObItemRestUrls.UNLOCK_UNIT_OF_MEASURE_URL);
	}

	public void assertItemsExistByCodeNameBaseUnitAndBatchManaged(DataTable itemsData) {
		List<Map<String, String>> itemsMaps = itemsData.asMaps(String.class, String.class);
		for (Map<String, String> itemMap : itemsMaps) {
			MObItemGeneralModel itemGeneralModel = (MObItemGeneralModel) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult(
											"getItemByCodeNameBaseUnitAndBatchManaged",
											constructItemsQueryParams(itemMap));
			assertNotNull("Item with code: " + itemMap.get(IFeatureFileCommonKeys.CODE)
							+ " doesn't exist!", itemGeneralModel);
		}
	}

	private Object[] constructItemsQueryParams(Map<String, String> itemMap) {
		return new Object[] { itemMap.get(IFeatureFileCommonKeys.CODE),
						itemMap.get(IFeatureFileCommonKeys.NAME),
						itemMap.get(IFeatureFileCommonKeys.ITEM_BASE_UNIT).split(" - ")[0],
						Boolean.valueOf(itemMap.get(IFeatureFileCommonKeys.IS_BATCH_MANAGED)) };
	}

	public List<String> getSectionsNames() {
		return new ArrayList<>(Arrays.asList(IMObItemSectionNames.GENERAL_DATA_SECTION,
						IMObItemSectionNames.UOM_SECTION,
						IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION));
	}

	public void assertThatItemsExist(DataTable itemsDataTable) {
		List<Map<String, String>> itemsList = itemsDataTable.asMaps(String.class, String.class);
		for (Map<String, String> item : itemsList) {
			assertThatItemExist(item);
		}
	}

	public void assertThatItemExist(Map<String, String> item) {
		Object[] queryParams = constructItemQueryParams(item);

		MObItemGeneralModel mObItemGeneralModel = (MObItemGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult(
										"getItemByCodeNameTypeUomAndPurchaseUnit", queryParams);
		assertNotNull(mObItemGeneralModel);
		assertThatBusinessUnitsAreEqual(item.get(IFeatureFileCommonKeys.ITEM_CODE),
						getBusinessUnitCodes(
										item.get(IFeatureFileCommonKeys.PURCHASE_UNIT).split(", ")),
						mObItemGeneralModel.getPurchasingUnitCodes());
	}

	public Object[] constructItemQueryParams(Map<String, String> item) {
		return new Object[] { item.get(IFeatureFileCommonKeys.ITEM),
						item.get(IFeatureFileCommonKeys.TYPE),
						item.get(IFeatureFileCommonKeys.UOM) };
	}

	public void assertThatItemTypesExist(DataTable itemTypesDataTable) {
		List<Map<String, String>> itemTypesList = itemTypesDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> itemType : itemTypesList) {
			CObItem cObItem = (CObItem) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getItemTypesByCode",
											itemType.get(IFeatureFileCommonKeys.CODE));
			assertNotNull(cObItem);
		}
	}

	public void insertItemsGeneralData(DataTable itemsDataTable) {
		List<Map<String, String>> itemsList = itemsDataTable.asMaps(String.class, String.class);
		for (Map<String, String> item : itemsList) {

			entityManagerDatabaseConnector.executeInsertQuery("insertItemGeneralData",
							constructItemsGeneralDataInsertionParams(item));
		}
	}

	private Object[] constructItemsGeneralDataInsertionParams(Map<String, String> item) {
		return new Object[] { item.get(IFeatureFileCommonKeys.CODE),
						item.get(IFeatureFileCommonKeys.ITEM_NAME),
						item.get(IFeatureFileCommonKeys.ITEM_NAME),
						item.get(IMasterDataFeatureFileCommonKeys.OLD_ITEM_NUMBER),
						item.get(IFeatureFileCommonKeys.BASE_UNIT),
						item.get(IMasterDataFeatureFileCommonKeys.ITEM_CLASS),
						item.get(IFeatureFileCommonKeys.IS_BATCH_MANAGED),
						item.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
						item.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
						item.get(IMasterDataFeatureFileCommonKeys.ITEM_TYPE),
						item.get(IFeatureFileCommonKeys.PRODUCT_MANAGER) };
	}

	public void insertItemsAlternateUnitOfMeasures(DataTable alternateUnitOfMeasuresDataTable) {
		List<Map<String, String>> alternateUnitOfMeasuresList = alternateUnitOfMeasuresDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> alternateUnitOfMeasure : alternateUnitOfMeasuresList) {

			entityManagerDatabaseConnector.executeInsertQuery("insertItemAlternateUnitOfMeasures",
							constructAlternateUnitOfMeasuresInsertionParams(
											alternateUnitOfMeasure));
		}
	}

	private Object[] constructAlternateUnitOfMeasuresInsertionParams(
					Map<String, String> alternateUnitOfMeasure) {
		return new Object[] { alternateUnitOfMeasure.get(IFeatureFileCommonKeys.ITEM_CODE),
						alternateUnitOfMeasure.get(IFeatureFileCommonKeys.ITEM_CODE),
						alternateUnitOfMeasure
										.get(IMasterDataFeatureFileCommonKeys.ALTERNATIVE_UNIT),
						alternateUnitOfMeasure
										.get(IMasterDataFeatureFileCommonKeys.CONVERSION_FACTOR), };
	}

	public void insertItemBusinessUnits(DataTable businessUnitsDataTable) {
		List<Map<String, String>> businessUnitsList = businessUnitsDataTable
				.asMaps(String.class, String.class);
		for (Map<String, String> businessUnit : businessUnitsList) {
			String code = businessUnit.get(IFeatureFileCommonKeys.CODE);
			String businessUnits = businessUnit.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
			insertItemsBusinessUnits(code, businessUnits);
		}
	}
	private void insertItemsBusinessUnits(String code, String businessUnits) {
		String[] businessUnitList = businessUnits.split(",");
		for (String businessUnit : businessUnitList) {
			entityManagerDatabaseConnector.executeInsertQueryForObject(
					"insertItemBusinessUnit", code, businessUnit.trim(), businessUnit.trim());
		}
	}
}
