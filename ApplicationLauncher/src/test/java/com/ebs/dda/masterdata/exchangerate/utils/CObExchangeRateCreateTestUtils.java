package com.ebs.dda.masterdata.exchangerate.utils;

import static com.ebs.dda.IFeatureFileCommonKeys.CODE;
import static com.ebs.dda.IFeatureFileCommonKeys.CREATED_BY;
import static com.ebs.dda.IFeatureFileCommonKeys.CREATION_DATE;
import static com.ebs.dda.IFeatureFileCommonKeys.LAST_UPDATED_BY;
import static com.ebs.dda.IFeatureFileCommonKeys.LAST_UPDATE_DATE;
import static com.ebs.dda.IFeatureFileCommonKeys.STATE;
import static com.ebs.dda.IFeatureFileCommonKeys.TYPE;
import static com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys.FIRST_VALUE;
import static com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.Assert;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys;
import cucumber.api.DataTable;

public class CObExchangeRateCreateTestUtils extends CObExchangeRateCreateCommonTestUtils {

  public CObExchangeRateCreateTestUtils(Map<String, Object> properties) {
    super(properties);
    this.urlPrefix = (String) getProperty(URL_PREFIX);
  }

  private static void assertPaymentRequestExistsWithCorrectData(
      Optional<CObExchangeRate> exchangeRateOptional, Map<String, String> exchangeRateRow) {
    Assert.assertTrue(exchangeRateOptional.isPresent());
  }

  public void assertLastCreatedExchangeRateByCode(String exchangeRateCode) {
    CObExchangeRate exchangeRate = (CObExchangeRate) entityManagerDatabaseConnector
        .executeNativeNamedQuerySingleResult("getLastCreatedExchangeRate");
    Assert.assertEquals(exchangeRate.getUserCode(), exchangeRateCode);
  }

  public void assertExchangeRateIsCreated(DataTable exchangeRateTable) {
    List<Map<String, String>> ExchangeRateData =
        exchangeRateTable.asMaps(String.class, String.class);
    for (Map<String, String> exchangeRateRow : ExchangeRateData) {
      Optional<CObExchangeRate> exchangeRateOptional = retrieveExchangeRateData(exchangeRateRow);
      assertPaymentRequestExistsWithCorrectData(exchangeRateOptional, exchangeRateRow);
    }
  }

  private Optional<CObExchangeRate> retrieveExchangeRateData(Map<String, String> exchangeRateRow) {
    Object[] params = constructERSelectionParams(exchangeRateRow);
    return Optional.ofNullable((CObExchangeRate) entityManagerDatabaseConnector
        .executeNativeNamedQuerySingleResult("getExchangeRateByAllFields", params));
  }

  private Object[] constructERSelectionParams(Map<String, String> exchangeRateRow) {

    Object[] params = new Object[] {

        exchangeRateRow.get(CODE), "%" + exchangeRateRow.get(STATE) + "%",
        exchangeRateRow.get(CREATION_DATE),
        exchangeRateRow.get(ICObExchangeRateFeatureFileCommonKeys.VALID_FROM),
        exchangeRateRow.get(LAST_UPDATE_DATE), exchangeRateRow.get(CREATED_BY),
        exchangeRateRow.get(LAST_UPDATED_BY), Double.parseDouble(exchangeRateRow.get(FIRST_VALUE)),
        Double.parseDouble(exchangeRateRow.get(SECOND_VALUE)), exchangeRateRow.get(TYPE)

    };
    return params;
  }
}
