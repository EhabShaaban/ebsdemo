package com.ebs.dda.masterdata.ivr.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.MasterDataTestUtils;
import com.ebs.dda.masterdata.ivr.IExpectedKeys;
import com.ebs.dda.masterdata.ivr.IResponseKeys;
import com.ebs.dda.masterdata.ivr.IVRFeatureFileKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class ItemVendorRecordCommonTestUtils extends MasterDataTestUtils {

  public String IVR_JMX_LOCK_BEAN_NAME = "ItemVendorRecoredLockCommand";

  private String requestLocale = "en";
  private ObjectMapper mapperObj;

  protected String ASSERTION_MSG = "[IVR]";

  public ItemVendorRecordCommonTestUtils(Map<String, Object> properties) {
    super(properties);
    mapperObj = new ObjectMapper();
  }

  public void withLocale(String locale) {
    this.requestLocale = locale;
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/vendor/CObVendor.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    return str.toString();
  }

  public Response deleteItemVendorRecord(Cookie loginCookie, String itemVendorCode) {
    String deleteURL = "/command/itemvendorrecord/delete/" + itemVendorCode;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  public void assertThatIVRsExistOrderedInResponse(
      List<Map<String, String>> expectedList, List<Map<String, Object>> actualList)
      throws Exception {
    Assert.assertEquals(expectedList.size(), actualList.size());
    for (int i = 0; i < actualList.size(); i++) {
      assertThatItemVendorCodeExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatItemExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatVendorExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatUOMExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatPurchaseUnitExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatItemCodeAtVendorExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatPriceExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatCurrencyExistsInResponse(expectedList.get(i), actualList.get(i));
      assertThatItemOldItemReferenceExistsInResponse(expectedList.get(i), actualList.get(i));
    }
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, IVR_JMX_LOCK_BEAN_NAME, sectionName);
  }

  protected void assertThatItemVendorCodeExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    String expectedItemVendorCode = expectedMap.get(IExpectedKeys.IVR_CODE_KEY);
    String actualItemVendorCode = actualMap.get(IResponseKeys.IVR_CODE_KEY).toString();
    Assert.assertEquals(
            ASSERTION_MSG + " IVR code is not correct in response", expectedItemVendorCode, actualItemVendorCode);
  }
  protected void assertThatItemOldItemReferenceExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    String expectedOldItemReference = expectedMap.get(IExpectedKeys.OLD_ITEM_NUMBER_KEY);
    String actualOldItemReference = actualMap.get(IResponseKeys.OLD_ITEM_NUMBER_KEY).toString();
    Assert.assertEquals(
            ASSERTION_MSG + " OldItemReference is not correct in response", expectedOldItemReference, actualOldItemReference);
  }

  protected void assertThatItemExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {
    String expectedItemCode = expectedMap.get(IExpectedKeys.ITEM_CODE_KEY);
    String actualItemCode = actualMap.get(IResponseKeys.ITEM_CODE_KEY).toString();
    Assert.assertEquals(
            ASSERTION_MSG + " ItemCode is not correct in response", expectedItemCode, actualItemCode);

    String expectedItemName = expectedMap.get(IExpectedKeys.ITEM_NAME_KEY);
    String localizedItemName =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.ITEM_NAME_KEY));
    String actualItemName = getLocaleFromLocalizedResponse(localizedItemName, requestLocale);
    Assert.assertEquals(
            ASSERTION_MSG + " ItemName is not correct in response", expectedItemName, actualItemName);
  }

  protected void assertThatVendorExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {
    String expectedVendorCode = expectedMap.get(IExpectedKeys.VENDOR_CODE_KEY);
    String actualVendorCode = actualMap.get(IResponseKeys.VENDOR_CODE_KEY).toString();
    Assert.assertEquals(
            ASSERTION_MSG + " VendorCode is not correct in response", expectedVendorCode, actualVendorCode);

    String expectedVendorName = expectedMap.get(IExpectedKeys.VENDOR_NAME_KEY);
    String localizedVendorName =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.VENDOR_NAME_KEY));
    String actualVendorName = getLocaleFromLocalizedResponse(localizedVendorName, requestLocale);
    Assert.assertEquals(
            ASSERTION_MSG + " VendorName is not correct in response", expectedVendorName, actualVendorName);
  }

  protected void assertThatPurchaseUnitExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {
    String expectedPurchaseUnitName = expectedMap.get(IExpectedKeys.PURCHASE_UNIT_NAME_KEY);
    String localizedPurchaseUnitName =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.PURCHASE_UNIT_NAME_KEY));
    String actualPurchaseUnitName =
        getLocaleFromLocalizedResponse(localizedPurchaseUnitName, requestLocale);
    Assert.assertEquals(
        ASSERTION_MSG + " BusinessUnitName is not correct in response",
        expectedPurchaseUnitName,
        actualPurchaseUnitName);
  }

  protected void assertThatItemCodeAtVendorExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    String expectedItemVendorCode = expectedMap.get(IExpectedKeys.ITEM_CODE_AT_VENDOR_KEY);
    String actualItemVendorCode = actualMap.get(IResponseKeys.ITEM_CODE_AT_VENDOR_KEY).toString();
    Assert.assertEquals(
        ASSERTION_MSG + " ItemCodeAtVendor is not correct in response",
        expectedItemVendorCode,
        actualItemVendorCode);
  }

  protected void assertThatUOMExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {
    String expectedUOMSymbol = expectedMap.get(IExpectedKeys.UOM_SYMBOL_KEY);
    String localizedUOMSymbol =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.UOM_SYMBOL_KEY));
    String actualUOMSymbol = getLocaleFromLocalizedResponse(localizedUOMSymbol, requestLocale);
    Assert.assertEquals(
        ASSERTION_MSG + " UOM symbol is not correct in response", expectedUOMSymbol, actualUOMSymbol);
  }

  protected void assertThatPriceExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    Double expectedPrice = Double.parseDouble(expectedMap.get(IExpectedKeys.PRICE_KEY));
    Double actualPrice = Double.parseDouble(actualMap.get(IResponseKeys.PRICE_KEY).toString());
    Assert.assertEquals(ASSERTION_MSG + " Price is not correct in response", expectedPrice, actualPrice);
  }

  protected void assertThatCurrencyExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) throws Exception {
    String expectedCurrencyName = expectedMap.get(IExpectedKeys.CURRENCY_NAME_KEY);
    String localizedCurrencyName =
        mapperObj.writeValueAsString(actualMap.get(IResponseKeys.CURRENCY_NAME_KEY));
    String actualCurrencyName =
        getLocaleFromLocalizedResponse(localizedCurrencyName, requestLocale);
    Assert.assertEquals(
        ASSERTION_MSG + " CurrencyName is not correct in response", expectedCurrencyName, actualCurrencyName);
  }

  protected void assertThatOldItemNumberExistsInResponse(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    String expectedOldItemNumber = expectedMap.get(IExpectedKeys.OLD_ITEM_NUMBER_KEY);
    String actualOldItemNumber = actualMap.get(IResponseKeys.OLD_ITEM_NUMBER_KEY).toString();
    Assert.assertEquals(
        ASSERTION_MSG + " OldItemNumber is not correct in response",
        expectedOldItemNumber,
        actualOldItemNumber);
  }

  public void assertThatItemVendorRecordsExists(DataTable itemVendorRecordsList) {
    List<Map<String, String>> itemVendorRecords =
        itemVendorRecordsList.asMaps(String.class, String.class);
    for (Map<String, String> itemVendorRecord : itemVendorRecords) {
      ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
          (ItemVendorRecordGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getItemVendorRecord", constructItemVendorRecordQueryParams(itemVendorRecord));

      Assert.assertNotNull(
          ASSERTION_MSG
              + " Record "
              + itemVendorRecord.get(IFeatureFileCommonKeys.IVR_CODE)
              + " does not exist",
          itemVendorRecordGeneralModel);
    }
  }

  private Object[] constructItemVendorRecordQueryParams(Map<String, String> itemVendorRecord) {
    return new Object[] {
      itemVendorRecord.get(IExpectedKeys.IVR_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.ITEM_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.VENDOR_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.UOM_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.ITEM_CODE_AT_VENDOR_KEY),
      Double.parseDouble(itemVendorRecord.get(IExpectedKeys.PRICE_KEY)),
      itemVendorRecord.get(IExpectedKeys.CURRENCY_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.PURCHASE_UNIT_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.CREATION_DATE)
    };
  }

  public void assertThatItemVendorRecordsWithoutCreationDateExists(
      DataTable itemVendorRecordsList) {
    List<Map<String, String>> itemVendorRecords =
        itemVendorRecordsList.asMaps(String.class, String.class);
    for (Map<String, String> itemVendorRecord : itemVendorRecords) {
      ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
          (ItemVendorRecordGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getItemVendorRecordWithoutCreationDate",
                  constructItemVendorRecordQueryParamsWithoutCreationDate(itemVendorRecord));

      Assert.assertNotNull(itemVendorRecordGeneralModel);
    }
  }

  private Object[] constructItemVendorRecordQueryParamsWithoutCreationDate(
      Map<String, String> itemVendorRecord) {
    return new Object[] {
      itemVendorRecord.get(IExpectedKeys.IVR_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.ITEM_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.VENDOR_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.UOM_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.ITEM_CODE_AT_VENDOR_KEY),
      Double.parseDouble(itemVendorRecord.get(IExpectedKeys.PRICE_KEY)),
      itemVendorRecord.get(IExpectedKeys.CURRENCY_CODE_KEY),
      itemVendorRecord.get(IExpectedKeys.PURCHASE_UNIT_CODE_KEY)
    };
  }

  protected void addPriceOptionalFieldToJson(
      JsonObject itemUOMDataObjects, Map<String, String> ivrValueObject) {
    if (ivrValueObject.get(IVRFeatureFileKeys.PRICE).isEmpty()) {
      itemUOMDataObjects.add(IResponseKeys.PRICE_KEY, JsonNull.INSTANCE);
    } else {
      try {
        itemUOMDataObjects.addProperty(
            IResponseKeys.PRICE_KEY,
            Double.parseDouble(ivrValueObject.get(IVRFeatureFileKeys.PRICE)));
      } catch (NumberFormatException ex) {
        itemUOMDataObjects.addProperty(
            IResponseKeys.PRICE_KEY, ivrValueObject.get(IVRFeatureFileKeys.PRICE));
      }
    }
  }
  public void insertItemVendorRecords(DataTable itemVendorRecordsDataTable) {
    List<Map<String, String>> itemVendorRecordsList = itemVendorRecordsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> itemVendorRecord : itemVendorRecordsList) {

      entityManagerDatabaseConnector.executeInsertQuery("insertItemVendorRecord",
              constructItemVendorRecordsInsertionParams(itemVendorRecord));
    }
  }
  private Object[] constructItemVendorRecordsInsertionParams(Map<String, String> item) {
    String[] priceWithCurrency = item.get(IMasterDataFeatureFileCommonKeys.UNIT_PRICE).split(" ");
    String price = priceWithCurrency[0];
    String currency = priceWithCurrency[1];
    return new Object[] {
            item.get(IFeatureFileCommonKeys.CODE),
            item.get(IFeatureFileCommonKeys.VENDOR),
            item.get(IFeatureFileCommonKeys.ITEM),
            item.get(IMasterDataFeatureFileCommonKeys.OLD_ITEM_NUMBER),
            item.get(IMasterDataFeatureFileCommonKeys.UNIT_OF_MEASURE),
            currency,
            item.get(IFeatureFileCommonKeys.ITEM_CODE_AT_VENDOR),
            price,
            item.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
             };
  }
}
