package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.MObCustomerHomeScreenAllowedActionsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObCustomerHomeScreenAllowedActionsStep extends SpringBootRunner implements En {

  private MObCustomerHomeScreenAllowedActionsTestUtils utils;

  public MObCustomerHomeScreenAllowedActionsStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of Customers home screen$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, IMObCustomerRestURLs.AUTHORIZED_ACTIONS);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Customers Home Screen,")) {
      utils =
          new MObCustomerHomeScreenAllowedActionsTestUtils(
              getProperties(), entityManagerDatabaseConnector);
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Customers Home Screen,")) {
      utils.unfreeze();
    }
  }
}
