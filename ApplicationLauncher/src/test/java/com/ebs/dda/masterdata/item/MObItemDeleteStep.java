package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemDeleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.List;

public class MObItemDeleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemDeleteTestUtils utils;

  public MObItemDeleteStep() {

    Given(
        "^the following IVR exits:$",
        (DataTable ivrDataTable) -> {
          utils.assertThatItemVendorRecordsExist(ivrDataTable);
        });

    Given(
        "^the following PurchaseOrders exist:$",
        (DataTable purchaseOrderDataTable) -> {
          List<List<String>> ordersList = purchaseOrderDataTable.raw();
          for (int i = 1; i < ordersList.size(); i++) {
            String code = ordersList.get(i).get(0);
            utils.assertThatPurchaseOrderExist(code);
          }
        });

    Given(
        "^PurchaseOrder with code \"([^\"]*)\" has the following OrderItems:$",
        (String purchaseOrderCode, DataTable orderItems) -> {
          List<List<String>> itemsList = orderItems.raw();
          for (int i = 1; i < itemsList.size(); i++) {
            String itemCode = itemsList.get(i).get(0);
            utils.assertThatThisOrderHasThisItem(purchaseOrderCode, itemCode);
          }
        });

    When(
        "^\"([^\"]*)\" requests to delete Item with code \"([^\"]*)\"$",
        (String username, String itemCode) -> {
          Response deletePurchaseOrderResponse =
              utils.deleteItemByCode(userActionsTestUtils.getUserCookie(username), itemCode);
          userActionsTestUtils.setUserResponse(username, deletePurchaseOrderResponse);
        });
    Then(
        "^Item with code \"([^\"]*)\" is deleted from the system$",
        (String itemCode) -> {
          utils.assertThatItemNotExist(itemCode);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" of Item with code \"([^\"]*)\" in edit mode$",
        (String userName, String sectionName, String itemCode) -> {
          String lockSectionUrl = utils.getLockSectionUrl(sectionName) + itemCode;
          Response lockRepponse =
              userActionsTestUtils.lockSection(userName, lockSectionUrl, itemCode);
          utils.assertResponseSuccessStatus(lockRepponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemDeleteTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete Item")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getMasterDataDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Delete Item")) {
      userActionsTestUtils.unlockAllSections(
          IMObItemRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
      utils.unfreeze();
    }
  }
}
