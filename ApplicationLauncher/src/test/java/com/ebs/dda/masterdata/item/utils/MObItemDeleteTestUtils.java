package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class MObItemDeleteTestUtils extends MObItemCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[DELETE]";

  public MObItemDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  private void assertThatItemVendorExist(String itemCode, String vendorCode) {
    Object ivr =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getIVRByItemAndVendor", constructIVRQueryParams(itemCode, vendorCode));
    Assert.assertNotNull(ASSERTION_MSG + " IVR is not exist", ivr);
  }

  public void assertThatItemNotExist(String imrCode) {
    Object item =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getItemMasterRecordByCode", imrCode);
    Assert.assertNull(ASSERTION_MSG + " Item with code : " + imrCode + " is not deleted", item);
  }

  public void assertThatPurchaseOrderExist(String code) {
    Object po =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getDObOrderByCode", code);
    Assert.assertNotNull(ASSERTION_MSG + " po with code : " + code + " is not exist", po);
  }

  public void assertThatItemVendorRecordsExist(DataTable ivrDataTable) {
    List<Map<String, String>> ivrList = ivrDataTable.asMaps(String.class, String.class);
    for (Map<String, String> ivrRow : ivrList) {
      String itemCode = ivrRow.get(IFeatureFileCommonKeys.ITEM_CODE);
      String vendorCode = ivrRow.get(IFeatureFileCommonKeys.VENDOR_CODE);
      assertThatItemVendorExist(itemCode, vendorCode);
    }
  }

  public void assertThatThisOrderHasThisItem(String purchaseOrderCode, String itemCode) {
    Object poItem =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getIObOrderLineDetailsGeneralModel",
            constructPOItemQueryParams(purchaseOrderCode, itemCode));
    Assert.assertNotNull(
        ASSERTION_MSG
            + " po with code : "
            + purchaseOrderCode
            + " did not have item with code : "
            + itemCode,
        poItem);
  }

  private Object[] constructPOItemQueryParams(String purchaseOrderCode, String itemCode) {
    return new Object[] {purchaseOrderCode, itemCode};
  }

  private Object[] constructIVRQueryParams(String itemCode, String vendorCode) {
    return new Object[] {itemCode, vendorCode};
  }
}
