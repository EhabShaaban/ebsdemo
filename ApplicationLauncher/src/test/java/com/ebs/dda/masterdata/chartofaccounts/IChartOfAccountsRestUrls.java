package com.ebs.dda.masterdata.chartofaccounts;

public interface IChartOfAccountsRestUrls {

  String CHART_OF_ACCOUNT_COMMAND = "/command/masterdata/chartofaccount";
  String CREATE_ACCOUNT = CHART_OF_ACCOUNT_COMMAND + "/";

  String CHART_OF_ACCOUNTS_BASE_URL = "/services/chartofaccounts";

  String AUTHORIZED_ACTIONS = CHART_OF_ACCOUNTS_BASE_URL + "/authorizedactions";
  String CHART_OF_ACCOUNT_LIMITED_LEAFS = CHART_OF_ACCOUNTS_BASE_URL + "/limitedleafs";


}
