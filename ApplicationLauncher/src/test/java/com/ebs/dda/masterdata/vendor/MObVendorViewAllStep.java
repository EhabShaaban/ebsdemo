package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class MObVendorViewAllStep extends SpringBootRunner implements En, InitializingBean {

  private static boolean hasBeenExecuted = false;
  private MObVendorViewAllTestUtils utils;

  public MObVendorViewAllStep() {

    Given(
        "^the following ViewAll data for Vendors exist:$",
        (DataTable vendorDataTable) -> {
          utils.assertThatVendorsExist(vendorDataTable);
        });

    Given(
        "^the total number of existing Vendors are (\\d+)$",
        (Integer vendorsNumber) -> {
          utils.assertThatAllVendorsExist(vendorsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to view all Vendors in Home Screen$",
        (String userName) -> {
          Response response = utils.viewAllVendors(userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\" in Vendors Home Screen:$",
        (String userName, DataTable vendorDataTable) -> {
          utils.assertThatVendorDataIsCorrect(
              vendorDataTable, userActionsTestUtils.getUserResponse(userName));
        });

    Then(
        "^the total number of records presented to \"([^\"]*)\" in Home Screen are (\\d+)$",
        (String userName, Integer recordsCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsReturned(response, recordsCount);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with filter applied on VendorCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName,
            Integer pageNumber,
            String vendorCodePart,
            Integer totalRecordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String userCodeFilter = utils.getContainsFilterField("userCode", vendorCodePart);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  totalRecordsNumber,
                  userCodeFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    Then(
        "^no values are displayed to \"([^\"]*)\" and empty grid state is viewed in Home Screen$",
        (String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertNoVendorDataIsReturned(response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with filter applied on AccountWithVendor which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName,
            Integer pageNumber,
            String accountWithVendorPart,
            Integer totalRecordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String accountWithVendorFilter =
              utils.getContainsFilterField("accountWithVendor", accountWithVendorPart);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  totalRecordsNumber,
                  accountWithVendorFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with filter applied on OldVendorNumber which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName,
            Integer pageNumber,
            String oldVendorNumberPart,
            Integer totalRecordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String oldVendorNumberFilter =
              utils.getContainsFilterField("oldVendorNumber", oldVendorNumberPart);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  totalRecordsNumber,
                  oldVendorNumberFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with filter applied on VendorName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String vendorNamePart,
            Integer totalRecordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String vendorNameFilter = utils.getContainsFilterField("vendorName", vendorNamePart);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  totalRecordsNumber,
                  vendorNameFilter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with filter applied on PurchaseResponsible which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String purchaseResponsibleNamePart,
            Integer totalRecordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String purchaseResponsibleNameFilter =
              utils.getContainsFilterField("purchaseResponsibleName", purchaseResponsibleNamePart);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  totalRecordsNumber,
                  purchaseResponsibleNameFilter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with filter applied on PurchaseUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String purchaseUnitName,
            Integer recordsNumberPerPage,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String purchaseUnitNameFilter =
              utils.getContainsFilterField("purchasingUnitNames", purchaseUnitName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  recordsNumberPerPage,
                  purchaseUnitNameFilter,
                  locale);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with (\\d+) records per page and the following filters applied on Vendor while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            Integer pageSize,
            String locale,
            DataTable filters) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getFilters(filters);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendors with filter applied on VendorType which equal \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String vendorType,
            Integer recordsNumberPerPage,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String vendorTypeFilter = utils.getEqualsFilterField("typeName", vendorType);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IMObVendorRestUrls.VENDOR_READ_ALL_URL,
                  pageNumber,
                  recordsNumberPerPage,
                  vendorTypeFilter,
                  locale);

          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObVendorViewAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all Vendors,")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all Vendors,")) {
      utils.unfreeze();
    }
  }
}
