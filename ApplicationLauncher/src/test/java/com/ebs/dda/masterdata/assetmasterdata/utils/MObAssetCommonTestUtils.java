package com.ebs.dda.masterdata.assetmasterdata.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

public class MObAssetCommonTestUtils extends CommonTestUtils {

  protected final String ASSERTION_MSG = "[Asset][Master][Data]";

  public MObAssetCommonTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbClearScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/asset-master-data/asset_master_data_Clear.sql");
    return str.toString();
  }

  public void createAssetMasterDataGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createAssetMasterDataGeneralData",
          constructInsertAssetMasterDataGeneralDataQueryParams(generalDataMap));
    }
  }

  private Object[] constructInsertAssetMasterDataGeneralDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.TYPE),
      generalDataMap.get(IFeatureFileCommonKeys.NAME),
      generalDataMap.get(IFeatureFileCommonKeys.NAME),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.OLD_NUMBER),
      generalDataMap.get(IFeatureFileCommonKeys.STATE)
    };
  }

  public void createAssetMasterDataBusinessUnits(DataTable businessUnitsTable) {
    List<Map<String, String>> businessUnitsMaps =
        businessUnitsTable.asMaps(String.class, String.class);
    for (Map<String, String> businessUnitMap : businessUnitsMaps) {
      String code = businessUnitMap.get(IFeatureFileCommonKeys.CODE);
      String businessUnits = businessUnitMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
      insertAssetMasterDataBusinessUnits(code, businessUnits);
    }
  }

  private void insertAssetMasterDataBusinessUnits(String code, String businessUnits) {
    String[] businessUnitList = businessUnits.split(",");
    for (String businessUnit : businessUnitList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createAssetMasterDataBusinessUnits", code, businessUnit.trim());
    }
  }
}
