package com.ebs.dda.masterdata.chartofaccounts;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class HObChartOfAccountsDropDownStep extends SpringBootRunner implements En, InitializingBean {


  private static boolean hasBeenExecuted = false;
  private HObChartOfAccountsDropDownTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new HObChartOfAccountsDropDownTestUtils(properties, entityManagerDatabaseConnector);
    }

    public HObChartOfAccountsDropDownStep() {
    Given(
        "^the following ChartOfAccounts exist:$",
        (DataTable chartOfAccountsDataTable) -> {
          utils.assertThatChartOfAccountsExist(chartOfAccountsDataTable);
        });
    Given(
        "^the total number of existing ChartOfAccounts are (\\d+)$",
        (Integer recordsNumber) -> {
          utils.assertThatAllChartOfAccountsExist(recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read all ChartOfAccounts that has an \"([^\"]*)\" State and Level\"([^\"]*)\"$",
        (String userName, String chartOfAccountState, String chartOfAccountLevel) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.readAllChartOfAccounts());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following ChartOfAccounts values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable chartOfAccountsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseMatchesExpected(response, chartOfAccountsDataTable);
        });

    Then(
        "^total number of ChartOfAccounts returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Dropdowns ChartOfAccounts")) {
          databaseConnector.createConnection();
          if (!hasBeenExecuted) {
              databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
              hasBeenExecuted = true;
          }
      }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Dropdowns ChartOfAccounts")) {
          databaseConnector.closeConnection();
      }
  }
}
