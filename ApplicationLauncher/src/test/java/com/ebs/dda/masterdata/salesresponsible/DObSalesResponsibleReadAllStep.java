package com.ebs.dda.masterdata.salesresponsible;

import com.ebs.dda.SpringBootRunner;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class DObSalesResponsibleReadAllStep extends SpringBootRunner implements En {

  private DObSalesResponsibleReadAllTestUtils utils;

  public DObSalesResponsibleReadAllStep() {
    Given(
        "^the following SalesResponsibles exist:$",
        (DataTable salesResponsiblesDataTable) -> {
          utils.assertThatSalesResponsiblesExist(salesResponsiblesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all SalesResponsibles$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllSalesResponsibles(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesResponsibles values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesResponsiblesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadAllSalesResponsibleResponseIsCorrect(
              response, salesResponsiblesDataTable);
        });

    Then(
        "^total number of SalesResponsibles returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer totalNumberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfRecordsIsCorrect(response, totalNumberOfRecords);
        });
  }

  @Before
  public void setup() throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObSalesResponsibleReadAllTestUtils(properties, entityManagerDatabaseConnector);
    databaseConnector.executeSQLScript(utils.getMasterDataDbScriptsOneTimeExecution());
  }
}
