package com.ebs.dda.masterdata.itemgroup.delete;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:features/masterdata/item-group/ItemGroup_Delete.feature"},
        glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.itemgroup.delete"},
        strict = true,
        tags = "not @Future")
public class CObItemGroupDeleteCucumberRunner {
    @Test
    public void test() {
    }
}
