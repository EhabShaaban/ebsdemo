package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.MObCustomerTaxAdministrationTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObCustomerTaxAdministrationDropdownStep extends SpringBootRunner implements En {

  private MObCustomerTaxAdministrationTestUtils utils;

  public MObCustomerTaxAdministrationDropdownStep() {
    Given(
        "^the following TaxAdministration exist:$",
        (DataTable taxAdministrationDataTable) -> {
          utils.assertThatTaxAdministrationExist(taxAdministrationDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all TaxAdministration$",
        (String userName) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            Response response = utils.readAllTaxAdministration(cookie);
            userActionsTestUtils.setUserResponse(userName,response);
        });

    Then(
        "^the following TaxAdministration values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable taxAdministrationDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertThatTaxAdministrationResponseIsCorrect(taxAdministrationDataTable, response);
        });

    Then(
        "^total number of TaxAdministration returned to \"([^\"]*)\" is equal to (\\d+)$",
            (String userName, Integer taxAdministrationCount) -> {
                Response response = userActionsTestUtils.getUserResponse(userName);
                utils.assertThatTotalNumberOfTaxAdministrationEquals(taxAdministrationCount, response);
            });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObCustomerTaxAdministrationTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
