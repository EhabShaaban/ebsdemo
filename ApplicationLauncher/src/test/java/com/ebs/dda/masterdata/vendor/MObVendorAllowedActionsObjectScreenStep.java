package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorAllowedActionsObjectScreenTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObVendorAllowedActionsObjectScreenStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObVendorAllowedActionsObjectScreenTestUtils utils;

  public MObVendorAllowedActionsObjectScreenStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of Vendor with code \"([^\"]*)\"$",
        (String userName, String vendorCode) -> {
          String readUrl = IMObVendorRestUrls.AUTHORIZED_ACTIONS + vendorCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following actions are displayed to \"([^\"]*)\":$",
        (String userName, DataTable allowedActions) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of Vendor home screen$",
        (String userName) -> {
          String readUrl = IMObVendorRestUrls.AUTHORIZED_ACTIONS;
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(loginCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObVendorAllowedActionsObjectScreenTestUtils(getProperties());
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Vendor Allowed Actions,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
