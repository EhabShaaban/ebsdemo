package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.IObCustomerContactPersonReadAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObCustomerContactPersonReadAllStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private IObCustomerContactPersonReadAllTestUtils utils;

    public IObCustomerContactPersonReadAllStep() {
        Given(
                "^the following ContactPersons for Customers exist:$",
                (DataTable customerContactPersonDataTable) -> {
                    utils.assertThatTheFollowingCustomersHaveTheFollowingContactPersons(
                            customerContactPersonDataTable);
                });

        When(
                "^\"([^\"]*)\" requests to read all ContactPersons for selected Customer \"([^\"]*)\" in a dropdown$",
                (String userName, String customerCode) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response response =
                            utils.sendGETRequest(cookie, utils.getCustomerContatPersonUrl(customerCode));
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following ContactPersons values will be presented to \"([^\"]*)\" in the dropdown:$",
                (String userName, DataTable customerContactPersonDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatResponseMatchesExpectedContactPerson(
                            customerContactPersonDataTable, response);
                });

        Then(
                "^total number of ContactPersons returned to \"([^\"]*)\" in the dropdown is equal to (\\d+)$",
                (String userName, Integer customerContactPersonCount) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatCustomerContactPersonResponseCountIsCorrect(
                            response, customerContactPersonCount);
                });
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of ContactPersons")) {
            beforeSetup();
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of ContactPersons")) {
            afterSetup();
        }
    }

    private void beforeSetup() throws Exception {
        Map<String, Object> properties = getProperties();
        utils =
                new IObCustomerContactPersonReadAllTestUtils(properties, entityManagerDatabaseConnector);

        databaseConnector.createConnection();
        if (!hasBeenExecutedOnce) {
            databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
            hasBeenExecutedOnce = true;
        }
    }

    private void afterSetup() throws SQLException {
        databaseConnector.closeConnection();
        utils.unfreeze();
    }
}
