package com.ebs.dda.masterdata;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.masterdata.ivr.IVRFeatureFileKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IResponseKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IVendorExpectedKeys;
import cucumber.api.DataTable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;

public class MasterDataTestUtils extends CommonTestUtils {

  public static final String PURCHASE_UNIT_CODES = "purchaseunitscodes";
  public static final String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  protected static final String ENTITY_CODE = "Code";
  protected static final String PURCHASING_UNIT = "PurchasingUnit";
  protected static final String COMMERCIAL_ITEM_TYPE = "COMMERCIAL";

  protected String ASSERTION_MSG = "[MasterData] ";

  public MasterDataTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = (String) getProperty(URL_PREFIX);
  }

  public String getMasterDataDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/vendor/CObVendor.sql");
    return str.toString();
  }

  public void assertItemExist(DataTable itemsDataTable) throws Exception {
    List<List<String>> itemsList = itemsDataTable.raw();
    for (int i = 1; i < itemsList.size(); i++) {
      Object[] params = constructItemExistenceQueryParams(itemsList.get(i));
      assertItemExistInDB(params);
    }
  }

  public Object[] constructItemExistenceQueryParams(List<String> itemValue) {
    return new Object[] {
      itemValue.get(0), itemValue.get(1), itemValue.get(3), itemValue.get(5), itemValue.get(7)
    };
  }

  public void assertItemExistInDB(Object[] params) throws Exception {
    String query =
        "SELECT * From mobitemgeneralmodel WHERE code = ? "
            + "AND name::json->>'en'=? "
            + "AND basicunitofmeasurecode=? "
            + "AND olditemnumber=? "
            + "AND typecode=? ";
    PreparedStatement preparedStatement = constructPreparedStatement(query, params);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    assertTrue(isNotEmptyResultSet(resultSet));
    if (!resultSet.getString(IMObItemGeneralModel.ITEM_TYPE_CODE).equals(COMMERCIAL_ITEM_TYPE)) {
      assertNull(resultSet.getString(IMObItemGeneralModel.ITEM_GROUP_CODE));
      assertFalse(resultSet.getBoolean(IMObItemGeneralModel.IS_BATCH_MANAGED));
    }
  }

  public void assertItemsExistByCodeAndPurchaseUnits(DataTable itemsData) throws Exception {
    List<Map<String, String>> itemsMaps = itemsData.asMaps(String.class, String.class);
    String query = "SELECT * From mobitemgeneralmodel WHERE code = ?";
    for (Map<String, String> itemMap : itemsMaps) {
      PreparedStatement preparedStatement =
          constructPreparedStatement(query, itemMap.get(ENTITY_CODE));
      ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
      resultSet.next();
      String actualPurchaseunitscodes = resultSet.getString(PURCHASE_UNIT_CODES);
      assertThatPurchaseUnitsCodeAreEquals(itemMap.get(PURCHASING_UNIT), actualPurchaseunitscodes);
    }
  }

  protected void assertThatPurchaseUnitsCodeAreEquals(
      String expectedPurchaseUnitCodes, String actualPurchaseUnitCodes) {
    List<String> expectedPurchaseUnitCodesList =
        convertBusinessUnitCodesStringToList(expectedPurchaseUnitCodes);
    List<String> actualPurchaseUnitCodesList =
        convertBusinessUnitCodesStringToList(actualPurchaseUnitCodes);

    assertTrue(
        ASSERTION_MSG + "The PurchaseUnitCodesList are not equal",
        CollectionUtils.isEqualCollection(
            expectedPurchaseUnitCodesList, actualPurchaseUnitCodesList));
  }

  protected List<String> convertBusinessUnitCodesStringToList(String purchaseUnitsCodes) {
    List<String> businessUnitCodes =
        Arrays.asList(purchaseUnitsCodes.replaceAll("\\s", "").split(","));
    return businessUnitCodes;
  }

  // To be removed after refactor delete IVR
  public void assertThatItemVendorRecordsExistsByVendorAndItemCodes(
      DataTable itemVendorRecordsList) {
    List<Map<String, String>> itemVendorRecords =
        itemVendorRecordsList.asMaps(String.class, String.class);
    for (Map<String, String> itemVendorRecord : itemVendorRecords) {
      List<ItemVendorRecordGeneralModel> itemVendorRecordGeneralModels =
          entityManagerDatabaseConnector.executeNativeNamedQuery(
              "getMiniItemVendorRecord", constructItemVendorRecordQueryParams(itemVendorRecord));

      Assert.assertTrue(itemVendorRecordGeneralModels.size() >= 1);
    }
  }

  private Object[] constructItemVendorRecordQueryParams(Map<String, String> itemVendorRecord) {
    return new Object[] {
      itemVendorRecord.get(IVRFeatureFileKeys.ITEM_CODE),
      itemVendorRecord.get(IVRFeatureFileKeys.VENDOR_CODE),
      itemVendorRecord.get(IVRFeatureFileKeys.PURCHASING_UNIT)
    };
  }

  protected void assertThatCreationDateIsCorrect(
      Map<String, Object> actualMap, Map<String, String> expectedMap) {
    DateTimeFormatter format = DateTimeFormat.forPattern(DateFormats.DATE_TIME_WITH_TIMEZONE);
    DateTime responseDate =
        DateTime.parse(actualMap.get(IResponseKeys.CREATION_DATE).toString(), format)
            .withZone(DateTimeZone.UTC);

    assertDateEquals(expectedMap.get(IVendorExpectedKeys.CREATION_DATE), responseDate);
  }
}
