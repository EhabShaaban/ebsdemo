package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.item.IMObItemCreateValueObject;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.masterdata.item.utils.MObItemCreateTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;

// Created By Hossam Hassan @ 10/9/18
public class MObItemCreationStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemCreateTestUtils utils;

  public MObItemCreationStep() {

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    Given(
        "^Last created Item was with code \"([^\"]*)\"$",
        (String itemCode) -> {
          utils.assertThatLastEntityCodeIs(MObItem.class.getSimpleName(), itemCode);
        });

    When(
        "^\"([^\"]*)\" creates Item with the following values:$",
        (String userName, DataTable itemGeneralData) -> {
          JsonObject itemData = utils.prepareItemJsonObject(itemGeneralData);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendPostRequest(userCookie, IMObItemRestUrls.CREATE_ITEM_URL, itemData);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new Item is created with the following values:$",
        (DataTable itemDataTable) -> {
          utils.assertThatItemExists(itemDataTable);
        });

    Then(
        "^the following error message is attached to Item Group field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String messageCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IMObItemCreateValueObject.ITEM_GROUP_CODE);
        });

    Then(
        "^the following error message is attached to BaseUnit field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String messageCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IMObItemCreateValueObject.BASE_UNIT_CODE);
        });

    Then(
        "^the following error message is attached to PurchasingUnit field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String messageCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IMObItemCreateValueObject.PURCHASE_UNITS);
        });

    Then(
        "^the following error message is attached to ItemType field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String messageCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IMObItemCreateValueObject.ITEM_TYPE);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Item")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create Item")) {
      utils.unfreeze();
    }
  }
}
