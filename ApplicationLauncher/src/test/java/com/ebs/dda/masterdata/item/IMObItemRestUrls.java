package com.ebs.dda.masterdata.item;

public interface IMObItemRestUrls {

  String COMMAND_ITEM = "/command/item/";
  String CREATE_ITEM_URL = COMMAND_ITEM + "create";
  String LOCK_GENERAL_DATA_SECTION = COMMAND_ITEM + "lock/generaldatasection/";
  String UNLOCK_GENERAL_DATA_SECTION = COMMAND_ITEM + "unlock/generaldatasection/";
  String LOCK_UNIT_OF_MEASURE_URL = COMMAND_ITEM + "lock/unitofmeasure/";
  String UNLOCK_UNIT_OF_MEASURE_URL = COMMAND_ITEM + "unlock/unitofmeasure/";
  String SAVE_ITEM_UOM_URL = "/command/item/update/unitofmeasure";
  String UNLOCK_LOCKED_SECTIONS = COMMAND_ITEM + "unlock/lockedsections/";

  String ITEM_READ_ALL_URL = "/services/masterdataobjects/items";

  String DELETE_ITEM = COMMAND_ITEM + "delete/";
  String AUTHORIZED_ACTIONS = ITEM_READ_ALL_URL + "/authorizedactions/";

  String READ_ACCOUNTING_INFO_RESTURL = "/services/masterdataobjects/items/accountinginfo/";
  String READ_AUTHORIZED_ITEMS_URL = ITEM_READ_ALL_URL + "/readauthorizeditems/";
  String READ_ITEMS_UOM_URL = ITEM_READ_ALL_URL + "/baseandalternativeunitofmeasures/";
  String LOCK_ACCOUNTING_DETAILS_SECTION = COMMAND_ITEM + "lock/accountingdetails/";
  String UNLOCK_ACCOUNTING_DETAILS_SECTION = COMMAND_ITEM + "unlock/accountingdetails/";
  String UPDATE_ACCOUNTING_DETAILS_SECTION = COMMAND_ITEM + "update/accountingdetails/";
  String READ_ITEM_TYPES_URL = ITEM_READ_ALL_URL + "/gettypes/";
  String READ_GENERAL_DATA_URL = ITEM_READ_ALL_URL + "/generaldata/";
  String SAVE_ITEM_GENERAL_DATA_URL = COMMAND_ITEM + "update/generaldata/";

  String READ_ALL_ITEMS_URLS = "/services/masterdataobjects/items/page=0/rows=50/filters=";

  String READ_PREVIOUS_ITEMS_URLS = "/services/masterdataobjects/items/previous/";
  String READ_NEXT_ITEMS_URLS = "/services/masterdataobjects/items/next/";

  String READ_ALL_UNIT_OF_MEASURES_URL = ITEM_READ_ALL_URL + "/unitofmeasure/";

  String READ_ITEM_CONVERSION_FACTOR_URL = ITEM_READ_ALL_URL + "/alternativeunitofmeasure/";
}
