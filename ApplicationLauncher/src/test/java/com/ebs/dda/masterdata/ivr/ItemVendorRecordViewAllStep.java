package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Locale;
import java.util.Map;

public class ItemVendorRecordViewAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordViewAllTestUtils utils;

  public ItemVendorRecordViewAllStep() {
    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable expectedDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          List<Map<String, String>> expectedList =
              expectedDataTable.asMaps(String.class, String.class);
          List<Map<String, Object>> actualList = response.body().jsonPath().getList("data");
          utils.withLocale(utils.getRequestLocale());
          utils.assertThatIVRsExistOrderedInResponse(expectedList, actualList);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on IVRCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String ivrCode, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String ivrCodeFilter = utils.getContainsFilterField("userCode", ivrCode);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, rowsNumber, ivrCodeFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on OldItemReference which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String oldItemReference, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String oldItemReferenceFilter = utils.getContainsFilterField("oldItemNumber", oldItemReference);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, rowsNumber, oldItemReferenceFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on Vendor which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName, Integer pageNumber, String vendor, Integer rowsNumber, String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String vendorFilter = utils.getContainsFilterField("vendor", vendor);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, rowsNumber, vendorFilter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on Item which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName, Integer pageNumber, String item, Integer rowsNumber, String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String itemFilter = utils.getContainsFilterField("item", item);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, rowsNumber, itemFilter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on UOM which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName, Integer pageNumber, String uom, Integer rowsNumber, String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String uomFilter = utils.getEqualsFilterField("uomCode", uom);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, rowsNumber, uomFilter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
          Locale stepLocale = Locale.forLanguageTag(locale);
          utils.setRequestLocale(stepLocale.getLanguage());
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on ItemCodeAtVendor which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String ivc, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String ivcFilter = utils.getContainsFilterField("itemVendorCode", ivc);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, rowsNumber, ivcFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on PurchaseUnitName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String purchaseUnitName,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String purchaseUnitNameFilter =
              utils.getContainsFilterField("purchaseUnitName", purchaseUnitName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IVRRestUrls.VIEW_ALL_URL,
                  pageNumber,
                  rowsNumber,
                  purchaseUnitNameFilter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with filter applied on Price which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String price, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String priceFilter = utils.getEqualsFilterField("price", price);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, rowsNumber, priceFilter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVR with (\\d+) records per page and the following filters applied on IVR while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            Integer pageSize,
            String locale,
            DataTable filters) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getFilters(filters);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IVRRestUrls.VIEW_ALL_URL, pageNumber, pageSize, filter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of IVRs with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IVRRestUrls.VIEW_ALL_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new ItemVendorRecordViewAllTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all IVRs")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }
}
