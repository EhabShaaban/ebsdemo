package com.ebs.dda.masterdata.vendor.interfaces;

public interface IResponseKeys {
  String VENDOR_CODE = "userCode";
  String VENDOR_NAME = "vendorName";
  String PURCHASING_RESPONSIBLE = "purchaseResponsibleName";
  String CREATION_DATE = "creationDate";
  String ACCOUNT_NAME = "accountName";
  String ACCOUNT_CODE = "accountCode";
}
