package com.ebs.dda.masterdata.enterprise;

public interface IResponseKeys {
  String CODE = "userCode";
  String NAME = "name";
  String COMPANY_NAME = "companyName";
}
