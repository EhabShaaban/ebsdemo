package com.ebs.dda.masterdata.itemgroup;

public interface IResponseKeys {

  String GROUP_LEVEL = "level";
  String PARENT_CODE = "parentCode";
  String PARENT_NAME = "parentName";
  String NAME = "name";
  String CODE = "userCode";


}
