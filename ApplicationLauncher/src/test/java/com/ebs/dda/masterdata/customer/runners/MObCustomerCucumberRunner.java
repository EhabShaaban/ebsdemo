package com.ebs.dda.masterdata.customer.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/customer/Customer_Create_Auth.feature",
      "classpath:features/masterdata/customer/Customer_Create_HP.feature",
      "classpath:features/masterdata/customer/Customer_Create_Val.feature",
      "classpath:features/masterdata/customer/Customer_Delete.feature",
      "classpath:features/masterdata/customer/Customer_AllowedActions_ObjectScreen.feature",
      "classpath:features/masterdata/customer/Customer_AllowedActions_HomeScreen.feature",
      "classpath:features/masterdata/customer/Customer_IssuedFrom_Dropdown.feature",
      "classpath:features/masterdata/customer/Customer_ViewAll.feature",
      "classpath:features/masterdata/customer/Customer_Types_Dropdown.feature",
      "classpath:features/masterdata/customer/Customer_Address_Dropdown.feature",
      "classpath:features/masterdata/customer/Customer_Contact_Person_Dropdown.feature",
      "classpath:features/masterdata/customer/Customer_TaxAdministration_Dropdown.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.customer"},
    strict = true,
    tags = "not @Future")
public class MObCustomerCucumberRunner {}
