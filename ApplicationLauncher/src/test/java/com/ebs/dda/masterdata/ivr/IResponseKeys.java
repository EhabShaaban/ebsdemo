package com.ebs.dda.masterdata.ivr;

public interface IResponseKeys {

  String IVR_CODE_KEY = "userCode";
  String ITEM_CODE_KEY = "itemCode";
  String ITEM_NAME_KEY = "itemName";
  String VENDOR_CODE_KEY = "vendorCode";
  String VENDOR_NAME_KEY = "vendorName";
  String UOM_SYMBOL_KEY = "uomSymbol";
  String PURCHASE_UNIT_NAME_KEY = "purchaseUnitName";
  String ITEM_CODE_AT_VENDOR_KEY = "itemVendorCode";
  String OLD_ITEM_NUMBER_KEY = "oldItemNumber";
  String PRICE_KEY = "price";
  String CURRENCY_CODE_KEY = "currencyCode";
  String CURRENCY_NAME_KEY = "currencyName";
	String ITEM_CODE_AT_VENDOR = "itemCodeAtVendor";
}
