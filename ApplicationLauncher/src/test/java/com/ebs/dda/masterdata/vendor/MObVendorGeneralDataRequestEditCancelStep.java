package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorGeneralDataTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObVendorGeneralDataRequestEditCancelStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private MObVendorGeneralDataTestUtils utils;

  public MObVendorGeneralDataRequestEditCancelStep() {

    When(
        "^\"([^\"]*)\" requests to edit GeneralData section of Vendor with code \"([^\"]*)\"$",
        (String userName, String vendorCode) -> {
          String lockUrl = IMObVendorRestUrls.LOCK_GENERAL_DATA_SECTION + vendorCode;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, vendorCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^GeneralData section of Vendor with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String vendorCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName, vendorCode, IMObVendorSectionNames.GENERAL_DATA_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened the Vendor with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String vendorCode) -> {
          String lockUrl = IMObVendorRestUrls.LOCK_GENERAL_DATA_SECTION + vendorCode;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, vendorCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^GeneralData section of Vendor with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String vendorCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockUrl = IMObVendorRestUrls.LOCK_GENERAL_DATA_SECTION + vendorCode;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, vendorCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving GeneralData section of Vendor with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String vendorCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockUrl = IMObVendorRestUrls.UNLOCK_GENERAL_DATA_SECTION + vendorCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on GeneralData section of Vendor with code \"([^\"]*)\" is released$",
        (String userName, String vendorCode) -> {
          utils.assertThatResourceLockedByUserIsReleased(
              userName, vendorCode, IMObVendorSectionNames.GENERAL_DATA_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving GeneralData section of Vendor with code \"([^\"]*)\"$",
        (String userName, String vendorCode) -> {
          String unlockUrl = IMObVendorRestUrls.UNLOCK_GENERAL_DATA_SECTION + vendorCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObVendorGeneralDataTestUtils(getProperties());
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request Edit Vendor GeneralData section,")
        || scenario.getName().contains("Request Cancel Edit Vendor GeneralData section,")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.prepareDbScript());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request Edit Vendor GeneralData section,")
        || scenario.getName().contains("Request Cancel Edit Vendor GeneralData section,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IMObVendorRestUrls.UNLOCK_ALL_VENDOR, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request Edit Vendor GeneralData section,")
        || scenario.getName().contains("Request Cancel Edit Vendor GeneralData section,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.prepareDbScript());
      databaseConnector.closeConnection();
    }
  }
}
