package com.ebs.dda.masterdata.productmanager.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.productmanager.ICObProductManager;
import com.ebs.dda.masterdata.enterprise.ICObEnterpriseRestUrls;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class CObProductManagerReadAllTestUtils extends CommonTestUtils {

  protected String ASSERTION_MSG = "[ProductManager]";

  public CObProductManagerReadAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    return str.toString();
  }

  public void assertThatTheFollowingProductManagersExist(DataTable productManagerDataTable) {
    List<Map<String, String>> productManagerList =
        productManagerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> productManagerRow : productManagerList) {
      Object productManager =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getProductManagerByCodeAndName",
              constructProductManagerQueryParams(productManagerRow));
      assertNotNull(ASSERTION_MSG + " Product Manager is not exist", productManager);
    }
  }

  private Object[] constructProductManagerQueryParams(Map<String, String> productManagerRow) {
    return new Object[] {
      productManagerRow.get(IFeatureFileCommonKeys.CODE),
      productManagerRow.get(IFeatureFileCommonKeys.NAME),
    };
  }

  public Response readAllProductManagers(Cookie cookie) {
    String restURL = ICObEnterpriseRestUrls.READ_ALL_PRODUCT_MANAGERS;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReadAllProductManagerResponseIsCorrect(
      Response response, DataTable productManagerDataTable) throws Exception {
    List<Map<String, String>> expectedProductManagers =
        productManagerDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualProductManagers = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedProductManagers.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedProductManagers.get(i), actualProductManagers.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IFeatureFileCommonKeys.PRODUCT_MANAGER,
          ICObProductManager.USER_CODE,
          ICObProductManager.NAME);
    }
  }

  public void assertThatTotalNumberOfRecordsIsCorrect(
      Response response, Integer totalNumberOfRecords) {
    List<HashMap<String, Object>> productManagers = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + " Number of records is not correct",
        totalNumberOfRecords.intValue(),
        productManagers.size());
  }

  public void assertTotalNumberOfExistingProductManagersAreCorrect(Integer recordsNumber) {
    String productManagerObjectTypeCode = "10";
    Long productManagersCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCobEnterpriseObjectsCount", productManagerObjectTypeCode);

    assertEquals(Long.valueOf(recordsNumber), productManagersCount);
  }
}
