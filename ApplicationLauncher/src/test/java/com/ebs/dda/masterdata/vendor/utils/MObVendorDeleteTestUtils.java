package com.ebs.dda.masterdata.vendor.utils;

import static org.junit.Assert.assertNull;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class MObVendorDeleteTestUtils extends MObVendorCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Delete] ";

  public MObVendorDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    return str.toString();
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    return str.toString();
  }

  public void assertThatPurchaseOrderHasVendor(DataTable purchaseOrderDataTable) {
    List<Map<String, String>> purchaseOrders =
        purchaseOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderData : purchaseOrders) {
      assertThatVendorExistsInPurchaseOrder(purchaseOrderData);
    }
  }

  private void assertThatVendorExistsInPurchaseOrder(Map<String, String> expectedOrder) {
    Object orderByCodeAndVendor =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPurchaseOrderByCodeAndVendor",
            expectedOrder.get(IFeatureFileCommonKeys.CODE),
            expectedOrder.get(IFeatureFileCommonKeys.VENDOR_CODE));
    Assert.assertNotNull(
        ASSERTION_MSG
            + "Purchase Order with code "
            + expectedOrder.get(IFeatureFileCommonKeys.CODE)
            + " doesn't exist with vendor code "
            + expectedOrder.get(IFeatureFileCommonKeys.VENDOR_CODE),
        orderByCodeAndVendor);
  }

  public void assertVendorIsDeleted(String vendorCode) throws Exception {
    MObVendorGeneralModel vendorGeneralModel = getVendorGeneralModelCode(vendorCode);
    assertNull(
        ASSERTION_MSG + "Vendor with code " + vendorCode + " is not deleted", vendorGeneralModel);
  }
}
