package com.ebs.dda.masterdata.chartofaccounts.utils;

import com.ebs.dda.CommonTestUtils;

import java.util.Map;

public class HObChartOfAccountsAllowedActionsTestUtils extends CommonTestUtils {

  public HObChartOfAccountsAllowedActionsTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    return str.toString();
  }


}
