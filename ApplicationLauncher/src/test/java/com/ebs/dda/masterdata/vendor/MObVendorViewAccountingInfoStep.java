package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.utils.MObVendorViewAccountingInfoTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class MObVendorViewAccountingInfoStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObVendorViewAccountingInfoTestUtils utils;

  public MObVendorViewAccountingInfoStep() {

    Given(
        "^the following AccountingInfo for Vendors exist:$",
        (DataTable accountingInfoDataTable) -> {
          utils.assertThatVendorHasAccountInfo(accountingInfoDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view AccountingInfo section of Vendor with code \"([^\"]*)\"$",
        (String user, String vendorCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(user);
          Response vendorAccountingInfoResponse =
              utils.sendReadAccountingInfoDetailsRequest(userCookie, vendorCode);
          userActionsTestUtils.setUserResponse(user, vendorAccountingInfoResponse);
        });

    Then(
        "^the following values of AccountingInfo section for Vendor with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String vendorCode, String userName, DataTable expectedAccountingInfoTable) -> {
          Response vendorAccountingInfoResponse = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseDataIsCorrect(
              expectedAccountingInfoTable, vendorAccountingInfoResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new MObVendorViewAccountingInfoTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View Vendor AccountingInfo section,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
