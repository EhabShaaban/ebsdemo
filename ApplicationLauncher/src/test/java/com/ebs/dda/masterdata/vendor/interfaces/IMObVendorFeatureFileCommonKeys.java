package com.ebs.dda.masterdata.vendor.interfaces;

public interface IMObVendorFeatureFileCommonKeys {
  String ACCOUNT_WITH_VENDOR = "AccountWithVendor";
  String OLD_VENDOR_REFERENCE = "OldVendorReference";
  String ACCOUNT_CODE = "AccountCode";
  String ACCOUNT_NAME = "AccountName";
  String VENDOR_TYPE = "VendorType";
}
