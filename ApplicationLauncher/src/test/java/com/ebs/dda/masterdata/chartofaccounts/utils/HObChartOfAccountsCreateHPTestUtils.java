package com.ebs.dda.masterdata.chartofaccounts.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import com.ebs.dda.dbo.jpa.entities.lookups.LObGlobalGLAccountConfig;
import org.junit.Assert;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccountCreateValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.masterdata.chartofaccounts.IChartOfAccountsRestUrls;
import com.google.gson.JsonObject;

import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class HObChartOfAccountsCreateHPTestUtils extends HObCommonChartOfAccountTestUtils {

	public HObChartOfAccountsCreateHPTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	private static JsonObject prepareChartOfAccountJsonObject(DataTable chartOfAccountDataTable) {
		Map<String, String> chartOfAccounts = chartOfAccountDataTable
						.asMaps(String.class, String.class).get(0);
		JsonObject chartOfAccountJsonObject = new JsonObject();
		addValueToJsonObject(chartOfAccountJsonObject,
						IHObChartOfAccountCreateValueObject.ACCOUNT_NAME,
						chartOfAccounts.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME));
		addValueToJsonObject(chartOfAccountJsonObject, IHObChartOfAccountCreateValueObject.LEVEL,
						String.valueOf(chartOfAccounts
										.get(IAccountingFeatureFileCommonKeys.LEVEL)));
		addValueToJsonObject(chartOfAccountJsonObject,
						IHObChartOfAccountCreateValueObject.OLD_ACCOUNT_CODE,
						String.valueOf(chartOfAccounts
										.get(IAccountingFeatureFileCommonKeys.OLD_ACCOUNT_CODE)));

		addAccountParentToJsonObject(chartOfAccounts, chartOfAccountJsonObject);

		addValueToJsonObject(chartOfAccountJsonObject,
						IHObChartOfAccountCreateValueObject.ACCOUNT_TYPE,
						chartOfAccounts.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE));
		addValueToJsonObject(chartOfAccountJsonObject,
						IHObChartOfAccountCreateValueObject.ACCOUNTING_ENTRY,
						chartOfAccounts.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT));
		addValueToJsonObject(chartOfAccountJsonObject,
						IHObChartOfAccountCreateValueObject.MAPPED_ACCOUNT,
						chartOfAccounts.get(IAccountingFeatureFileCommonKeys.MAPPED_ACCOUNT));

		return chartOfAccountJsonObject;
	}

	private static void addAccountParentToJsonObject(Map<String, String> chartOfAccounts, JsonObject chartOfAccountJsonObject) {
		String accountParent = chartOfAccounts.get(IAccountingFeatureFileCommonKeys.ACCOUNT_PARENT);
		if (accountParent != null && !accountParent.isEmpty()) {
			String[] accountParentFullName = accountParent.split(" - ");
			addValueToJsonObject(chartOfAccountJsonObject,
							IHObChartOfAccountCreateValueObject.PARENT, accountParentFullName[0]);
		}
	}
	public void assertThatIChartOfAccountWithCodeAndLevelExists(String level, String code) {
		HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel = (HObChartOfAccountsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult(
										"getLastCreatedChartOfAccountWithLevel", level);
		assertEquals(code, chartOfAccountsGeneralModel.getUserCode());
	}

	public Response createAccount(DataTable accountTable, Cookie cookie) {
		JsonObject chartOfAccount = prepareChartOfAccountJsonObject(accountTable);
		String restURL = IChartOfAccountsRestUrls.CREATE_ACCOUNT;
		return sendPostRequest(cookie, restURL, chartOfAccount);
	}

	public void assertThatAccountIsCreatedSuccessfully(DataTable accountTable) throws Exception {

		Map<String, String> accountMap = convertEmptyStringsToNulls(
						accountTable.asMaps(String.class, String.class).get(0));
		HObChartOfAccountsGeneralModel accountsGeneralModel = (HObChartOfAccountsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getCreatedGLAccount",
										constructGetCreatedGLAccountQueryParams(accountMap));
		Assert.assertNotNull(accountsGeneralModel);
	}

	private Object[] constructGetCreatedGLAccountQueryParams(Map<String, String> accountMap) {
		return new Object[] { accountMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
						accountMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME),
						accountMap.get(IFeatureFileCommonKeys.CREATION_DATE),
						accountMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
						accountMap.get(IFeatureFileCommonKeys.CREATED_BY),
						accountMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
						'%' + accountMap.get(IFeatureFileCommonKeys.STATE) + '%'

		};
	}

	public void assertLastCreatedAccountBelowTheParentAccount(String parentAccount,
					String lastChildCode) {
		String accountCode = parentAccount.split(" - ")[0];
		HObChartOfAccountsGeneralModel accountsGeneralModel = (HObChartOfAccountsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult(
										"getLastCreatedChartOfAccountBelowAccount", accountCode, lastChildCode);
		assertEquals(lastChildCode, accountsGeneralModel.getUserCode());
	}

	public void assertHasNoChild(String accountCode) {
		HObChartOfAccountsGeneralModel accountsGeneralModel = (HObChartOfAccountsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getAccountWithParentCode",
										accountCode);
		Assert.assertNull(accountsGeneralModel);
	}

	public void insertGlAccounts(DataTable glAccountsDataTable) {
		List<Map<String, String>> glAccountsMaps = glAccountsDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> glAccount : glAccountsMaps) {
			entityManagerDatabaseConnector.executeInsertQueryForObject("createGLAccount",
							constructGlAccountsInsertionParams(glAccount));
		}
	}

	private Object[] constructGlAccountsInsertionParams(Map<String, String> glAccount) {
		return new Object[] { glAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
						glAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME),
						glAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME),
						glAccount.get(IFeatureFileCommonKeys.STATE),
						glAccount.get(IAccountingFeatureFileCommonKeys.LEVEL),
						glAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_PARENT),
						glAccount.get(IAccountingFeatureFileCommonKeys.OLD_ACCOUNT_CODE),
						glAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE),
						glAccount.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
						glAccount.get(IAccountingFeatureFileCommonKeys.SUBLEDGER),
						glAccount.get(IAccountingFeatureFileCommonKeys.MAPPED_ACCOUNT) };
	}

	public void assertThatGLAccountDetailsIsUpdated(String accountCode,
					DataTable accountDetailsDataTable) {

			Map<String, String> accountMap = convertEmptyStringsToNulls(
					accountDetailsDataTable.asMaps(String.class, String.class).get(0));
			HObChartOfAccountsGeneralModel accountsGeneralModel = (HObChartOfAccountsGeneralModel) entityManagerDatabaseConnector
					.executeNativeNamedQuerySingleResult("getUpdatedGLAccountDetails",
							constructGetUpdatedGLAccountDetailsQueryParams(accountCode,
									accountMap));
			Assert.assertNotNull("GL Accounts Details Is not updated" + accountMap.toString(), accountsGeneralModel);

	}

	private Object[] constructGetUpdatedGLAccountDetailsQueryParams(String accountCode,
					Map<String, String> accountMap) {
		return new Object[] { accountMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE),
						accountMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_PARENT),
						accountMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
						accountMap.get(IAccountingFeatureFileCommonKeys.SUBLEDGER),
						accountMap.get(IAccountingFeatureFileCommonKeys.MAPPED_ACCOUNT),
						accountCode, accountMap.get(IAccountingFeatureFileCommonKeys.LEVEL),
						accountMap.get(IAccountingFeatureFileCommonKeys.OLD_ACCOUNT_CODE) };
	}

	public void assertThatGLAccountsConfigurationIsUpdated(DataTable glAccountConfigurationDataTable) {
		Map<String, String> glAccountConfigurationMap = convertEmptyStringsToNulls(
				glAccountConfigurationDataTable.asMaps(String.class, String.class).get(0));
		LObGlobalGLAccountConfig accountsConfiguration = (LObGlobalGLAccountConfig) entityManagerDatabaseConnector
				.executeNativeNamedQuerySingleResult("getUpdatedGLAccountConfiguration",
						constructGetUpdatedGLAccountConfigurationQueryParams(
								glAccountConfigurationMap));
		String configCode = glAccountConfigurationMap.get(IAccountingFeatureFileCommonKeys.GL_CONFIGURATION_CODE);
		if (configCode != null) {
			Assert.assertNotNull("GL Accounts Configuration Is not updated" + glAccountConfigurationMap.toString(), accountsConfiguration);
		}else {
			Assert.assertNull("GL Accounts Configuration Is updated improperly" + glAccountConfigurationMap.toString(), accountsConfiguration);
		}
	}
	private Object[] constructGetUpdatedGLAccountConfigurationQueryParams(
																	Map<String, String> glAccountConfigurationMap) {
		return new Object[] {
				glAccountConfigurationMap.get(IAccountingFeatureFileCommonKeys.GL_CONFIGURATION_CODE),
				glAccountConfigurationMap.get(IAccountingFeatureFileCommonKeys.GL_CONFIGURATION_ACCOUNT),
			 };
	}
}
