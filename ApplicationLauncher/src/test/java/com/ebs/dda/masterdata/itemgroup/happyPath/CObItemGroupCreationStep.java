package com.ebs.dda.masterdata.itemgroup.happyPath;

import com.ebs.dda.SpringBootRunner;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;

public class CObItemGroupCreationStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private CObItemGroupCreationUtil itemGroupCreateFeatureTestUtils;

  public CObItemGroupCreationStep() {

    And(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          itemGroupCreateFeatureTestUtils.freeze(currentDateTime);
        });

    And(
        "^Last created ItemGroup as \"([^\"]*)\" was with code \"([^\"]*)\"$",
        (String level, String code) -> {
          itemGroupCreateFeatureTestUtils.assertThatItemGroupWithCodeAndLevelExists(level, code);
        });

    When(
        "^\"([^\"]*)\" creates ItemGroup with the following values:$",
        (String userName, DataTable itemGroupsDataTable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              itemGroupCreateFeatureTestUtils.createItemGroup(userCookie, itemGroupsDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new ItemGroup is created with the following values:$",
        (DataTable itemGroupsDataTable) -> {
          itemGroupCreateFeatureTestUtils.assertItemGroupsCreated(itemGroupsDataTable);
        });
  }

  @Before
  public void setup() throws Exception {
    itemGroupCreateFeatureTestUtils = new CObItemGroupCreationUtil(getProperties());
    itemGroupCreateFeatureTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
      databaseConnector.executeSQLScript(
          itemGroupCreateFeatureTestUtils.getMasterDataDbScriptsOneTimeExecution());
      hasBeenExecutedOnce = true;
    }
  }

  @After
  public void afterEachScenario() throws SQLException {
    itemGroupCreateFeatureTestUtils.unfreeze();
    databaseConnector.closeConnection();
  }
}
