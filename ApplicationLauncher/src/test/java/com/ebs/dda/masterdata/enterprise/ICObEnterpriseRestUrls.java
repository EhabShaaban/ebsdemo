package com.ebs.dda.masterdata.enterprise;

public interface ICObEnterpriseRestUrls {

  String ENTERPRISE_URL = "/services/enterprise";
  String COMPANIES_URL = ENTERPRISE_URL + "/companies";
  String READ_BUSINESS_UNIT_URL = ENTERPRISE_URL + "/purchaseunits/readforpurchaseorder";
  String READ_COMPANIES_URL = COMPANIES_URL + "/getall";
  String READ_PLANT_STOREHOUSES_URL = ENTERPRISE_URL + "/storehouses/byPlantCode/";
  String READ_COMPANY_STOREHOUSES_URL = ENTERPRISE_URL + "/storehouses/byCompanyCode/";
  String READ_COLLECTION_RESPONSIBLE_URL = ENTERPRISE_URL + "/collectionresponsible/getall";
  String READ_PURCHASING_RESPONSIBLE_URL = ENTERPRISE_URL + "/purchasereponsibles/all";
  String READ_ALL_SALES_RESPONSIBLES = ENTERPRISE_URL + "/salesresponsibles/";
  String READ_ALL_PRODUCT_MANAGERS = ENTERPRISE_URL + "/productmanagers/";
}
