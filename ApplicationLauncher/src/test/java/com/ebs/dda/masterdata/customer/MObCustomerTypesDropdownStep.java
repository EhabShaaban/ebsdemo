package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.customer.utils.MObCustomerTypesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObCustomerTypesDropdownStep extends SpringBootRunner implements En {

  private MObCustomerTypesTestUtils utils;

  public MObCustomerTypesDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all CustomerTypes$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllCustomerTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following CustomerTypes values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable customerTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatCustomerTypesResponseIsCorrect(customerTypesDataTable, response);
        });

    Then(
        "^total number of CustomerTypes returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer customerTypesCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfCustomerTypesEquals(customerTypesCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObCustomerTypesTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
