package com.ebs.dda.masterdata.assetmasterdata;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.assetmasterdata.utils.MObAssetTypesTestUtil;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class MObAssetTypesStep extends SpringBootRunner implements En {

  private MObAssetTypesTestUtil utils;

  public MObAssetTypesStep() {
    Given(
        "^the following AssetMasterData Types Exist:$",
        (DataTable assetTypeDataTable) -> {
          utils.assertThatAssetTypesExist(assetTypeDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all AssetMasterData Types$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllAssetTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following AssetMasterData Types values will be presented to \"([^\"]*)\":$",
            (String userName, DataTable assetTypeDataTable) -> {
                Response response = userActionsTestUtils.getUserResponse(userName);
                utils.assertThatReadAllAssetTypesResponseIsCorrect(
                        response, assetTypeDataTable);
            });

    Then(
        "^total number of AssetMasterData Types returned to \"([^\"]*)\" is equal to (\\d+)$",
            (String userName, Integer totalNumberOfRecords) -> {
                Response response = userActionsTestUtils.getUserResponse(userName);
                utils.assertThatTotalNumberOfRecordsIsCorrect(response, totalNumberOfRecords);
            });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new MObAssetTypesTestUtil(properties, entityManagerDatabaseConnector);
  }
}
