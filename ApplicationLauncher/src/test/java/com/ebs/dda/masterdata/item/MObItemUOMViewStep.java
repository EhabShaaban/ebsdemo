package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemUOMViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MObItemUOMViewStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemUOMViewTestUtils utils;

  public MObItemUOMViewStep() {

    Given(
        "^the following sub-roles are admin sub-roles:$",
        (DataTable adminSubRolesDataTable) -> {
          utils.assertThatRolesAreAdminRoles(adminSubRolesDataTable);
        });

    And(
        "^the following UnitOfMeasures section exist in Item with code \"([^\"]*)\":$",
        (String item, DataTable itemsData) -> {
          utils.assertUOMsExistInItemWithCode(itemsData, item);
        });

    When(
        "^\"([^\"]*)\" requests to view UnitOfMeasures section of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readUnitOfMeasures(itemCode, userCookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^an empty UnitOfMeasures section of Item with code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String itemCode, String userName) -> {
          utils.assertThatResponseHasItemWithoutAnyUnitOfMeasures(
              userActionsTestUtils.getUserResponse(userName));
        });

    Then(
        "^the following values of UnitOfMeasures section of Item with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String itemCode, String userName, DataTable itemsData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          List<Map<String, Object>> expectedData = itemsData.asMaps(String.class, Object.class);
          utils.assertThatResponseHasItemAndUnitOfMeasuresExist(response, expectedData, itemCode);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemUOMViewTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View UnitOfMeasures section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View UnitOfMeasures section")) {
      utils.unfreeze();
    }
  }
}
