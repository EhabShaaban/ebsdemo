package com.ebs.dda.masterdata.productmanager;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.productmanager.utils.CObProductManagerReadAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class CObProductManagerReadAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private CObProductManagerReadAllTestUtils utils;

  public CObProductManagerReadAllStep() {

    Given(
        "^the following ProductManagers exist:$",
        (DataTable productManagerDataTable) -> {
          utils.assertThatTheFollowingProductManagersExist(productManagerDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all ProductManagers in a dropdown$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllProductManagers(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following ProductManagers values will be presented to \"([^\"]*)\" in a dropdown:$",
        (String userName, DataTable productManagerDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadAllProductManagerResponseIsCorrect(response, productManagerDataTable);
        });

    Then(
        "^total number of ProductManagers returned to \"([^\"]*)\" in a dropdown is equal to (\\d+)$",
        (String userName, Integer totalNumberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfRecordsIsCorrect(response, totalNumberOfRecords);
        });

    Given(
        "^the total number of existing ProductManagers are (\\d+)$",
        (Integer recordsNumber) -> {
            utils.assertTotalNumberOfExistingProductManagersAreCorrect(recordsNumber);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new CObProductManagerReadAllTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of ProductManagers dropdown")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }
}
