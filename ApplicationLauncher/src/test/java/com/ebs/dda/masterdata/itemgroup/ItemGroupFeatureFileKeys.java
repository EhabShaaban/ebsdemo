package com.ebs.dda.masterdata.itemgroup;

public interface ItemGroupFeatureFileKeys {

  String GROUP_LEVEL = "GroupLevel";
  String PARENT_CODE = "ParentCode";
  String PARENT_NAME = "ParentName";


}
