package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCreateTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MObVendorCreateStep extends SpringBootRunner implements En {

  private MObVendorCreateTestUtils utils;

  public MObVendorCreateStep() {

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    Given(
        "^Last created Vendor was with code \"([^\"]*)\"$",
        (String vendorCode) -> {
          utils.createLastEntityCode(MObVendor.class.getSimpleName(), vendorCode);
        });

    When(
        "^\"([^\"]*)\" creates Vendor with the following values:$",
        (String userName, DataTable vendorDataTable) -> {
          JsonObject vendorObject = utils.prepareVendorJsonObject(vendorDataTable);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendPostRequest(userCookie, IMObVendorRestUrls.CREATE_VENDOR_URL, vendorObject);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new Vendor is created with the following values:$",
        (DataTable vendorDataTable) -> {
          utils.assertCreatedVendorDataExists(vendorDataTable);
        });

    Given(
        "^the following PurchaseResponsibles exist:$",
        (DataTable purchaseResponsiblesData) -> {
          List<List<String>> purchaseOrders = purchaseResponsiblesData.raw();
          for (int i = 1; i < purchaseOrders.size(); i++) {
            utils.assertThatPurchaseResponsibleExist(purchaseOrders.get(i));
          }
        });

    Given(
        "^the following PurchaseResponsibles doesn't exit:$",
        (DataTable purchaseResponsiblesDataTable) -> {
          utils.assertThatPurchaseResponsibleDoesNotExist(purchaseResponsiblesDataTable);
        });

    Given(
        "^the following BusinessUnits doesn't exit:$",
        (DataTable businessUnitsDataTable) -> {
          utils.assertThatBusinessUnitsDoesNotExist(businessUnitsDataTable);
        });

    Given(
        "^the following VendorTypes doesn't exit:$",
        (DataTable vendorTypesDataTable) -> {
          utils.assertThatVendorTypesDoesNotExist(vendorTypesDataTable);
        });

    Then(
        "^the following error message is attached to BusinessUnit field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String messageCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, MObVendorCreateTestUtils.PURCHASE_UNIT_CODE);
        });

    Then(
        "^the following error message is attached to PurchasingResponsible field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String messageCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IMObVendorGeneralModel.PURCHASING_RESPONSIBLE_CODE);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new MObVendorCreateTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Create Vendor,")) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Create Vendor,")) {
      utils.unfreeze();
    }
  }
}
