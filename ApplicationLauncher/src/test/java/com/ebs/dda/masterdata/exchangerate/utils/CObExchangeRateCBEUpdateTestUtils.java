package com.ebs.dda.masterdata.exchangerate.utils;

import static com.ebs.dda.IFeatureFileCommonKeys.*;
import static com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys.*;
import static com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys.IS_DEFAULT;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys;
import org.junit.Assert;
import org.mockito.Mockito;

import com.ebs.dac.foundation.realization.exchangerates.CBEExchangeRateClient;
import com.ebs.dac.foundation.realization.exchangerates.CBEExchangeRateDTO;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.commands.masterdata.exchagerate.CObExchangeRatesUpdateCommandRemotly;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateType;

import cucumber.api.DataTable;

public class CObExchangeRateCBEUpdateTestUtils extends CommonTestUtils {

	public CObExchangeRateCBEUpdateTestUtils(Map<String, Object> properties,
					EntityManagerDatabaseConnector dbManager) {
		setProperties(properties);
		setEntityManagerDatabaseConnector(dbManager);
	}

	public void assertThatExchangeRateEntryStrategies(DataTable strategiesDT) {
		List<Map<String, String>> strategiesListMap = strategiesDT.asMaps(String.class,
						String.class);
		for (Map<String, String> strategyMap : strategiesListMap) {

			Object[] params = constructExRateTypeInsertionParams(strategyMap);
			CObExchangeRateType exRateType = (CObExchangeRateType) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getExchangeRateTypes", params);

			Assert.assertNotNull(
							"CobExchangeRateType is not exist, given: " + Arrays.toString(params),
							exRateType);

		}
	}

	private Object[] constructExRateTypeInsertionParams(Map<String, String> strategyMap) {
		Object[] params = new Object[] { strategyMap.get(CODE), strategyMap.get(NAME),
						Boolean.valueOf(strategyMap.get(IS_DEFAULT)) };
		return params;
	}

	public void mockCBERemoteServerExchangeRates(
					CObExchangeRatesUpdateCommandRemotly exRatesUpdateCmd, DataTable exRatesDT) {

		CBEExchangeRateClient mockedCbeClient = Mockito.mock(CBEExchangeRateClient.class);

		List<Map<String, String>> exRatesMapsList = exRatesDT.asMaps(String.class, String.class);
		CBEExchangeRateDTO[] exRateDtos = exRatesMapsList.stream()
						.map(exRate -> convertToDto(exRate)).toArray(CBEExchangeRateDTO[]::new);

		Mockito.when(mockedCbeClient.getLatestExchangeRates()).thenReturn(exRateDtos);

		exRatesUpdateCmd.setCbeClient(mockedCbeClient);
	}

	private CBEExchangeRateDTO convertToDto(Map<String, String> exRateMap) {
		CBEExchangeRateDTO dto = new CBEExchangeRateDTO();
		dto.setCurrencyISO(exRateMap.get(CURRENCY_ISO));
		dto.setSell(Double.valueOf(exRateMap.get(CURRENCY_PRICE)));
		return dto;
	}

	public void assertThatExchangeRatesAreSaved(DataTable exRatesDT) {
		List<Map<String, String>> exRatesMapsList = exRatesDT.asMaps(String.class, String.class);

		for (Map<String, String> exRateMap : exRatesMapsList) {

			Object[] params = constructExRateSelectionQueryParams(exRateMap);

			CObExchangeRateGeneralModel exchangeRate = (CObExchangeRateGeneralModel) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getCBEExchangeRates", params);

			Assert.assertNotNull("Exchange rate is not found, given: " + Arrays.toString(params),
							exchangeRate);
		}

	}

	private Object[] constructExRateSelectionQueryParams(Map<String, String> exRateMap) {
		return new Object[] { exRateMap.get(CODE), exRateMap.get(TYPE),
						exRateMap.get(FIRST_CURRENCY), exRateMap.get(SECOND_CURRENCY),
						Double.valueOf(exRateMap.get(SECOND_VALUE)), exRateMap.get(VALID_FROM),
						exRateMap.get(CREATED_BY) };
	}

	public void assertThatLatestExchangeRateOfDefaultStrategy(String firstCurrency,
					String secondCurrency, DataTable latestExchangeRateDataTable) {
		List<Map<String, String>> exRatesMapsList = latestExchangeRateDataTable.asMaps(String.class,
						String.class);

		for (Map<String, String> exRateMap : exRatesMapsList) {

			Object[] params = constructLatestExchangeRateOfDefaultStrategySelectionQueryParams(
							exRateMap, firstCurrency, secondCurrency);

			CObExchangeRateGeneralModel exchangeRate = (CObExchangeRateGeneralModel) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult(
											"getLatestExchangeRateOfDefaultStrategy", params);

			Assert.assertNotNull("Exchange rate is not found, given: " + Arrays.toString(params),
							exchangeRate);
		}

	}

	private Object[] constructLatestExchangeRateOfDefaultStrategySelectionQueryParams(
					Map<String, String> exRateMap, String firstCurrency, String secondCurrency) {
		return new Object[] {
				exRateMap.get(CODE),
				firstCurrency,
				secondCurrency,
				exRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE),
				exRateMap.get(ICObExchangeRateFeatureFileCommonKeys.VALID_FROM),
				exRateMap.get(TYPE),
				firstCurrency,
				secondCurrency
		};
	}

	public void createExchangeRateRecords(DataTable exchangeRateRecords) {
		List<Map<String, String>> exchangeRateMaps = exchangeRateRecords.asMaps(String.class, String.class);
		for (Map<String, String> exchangeRateMap : exchangeRateMaps) {
			entityManagerDatabaseConnector.executeInsertQueryForObject(
					"createExchangeRateRecords",
					constructInsertExchangeRateRecordsQueryParams(exchangeRateMap));
		}
	}

	private Object[] constructInsertExchangeRateRecordsQueryParams(
			Map<String, String> exchangeRateMap) {
		return new Object[] {
				exchangeRateMap.get(IFeatureFileCommonKeys.CODE),
				exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.VALID_FROM),
				Double.parseDouble(exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE)),
				exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.FIRST_CURRENCY_ISO),
				exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_CURRENCY_ISO),
				exchangeRateMap.get(IFeatureFileCommonKeys.TYPE)
		};
	}
}
