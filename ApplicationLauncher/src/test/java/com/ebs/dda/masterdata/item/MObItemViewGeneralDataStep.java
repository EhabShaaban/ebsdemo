package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemViewGeneralDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemViewGeneralDataTestUtils utils;

  public MObItemViewGeneralDataStep() {

    Given(
        "^the following GeneralData for Items exist:$",
        (DataTable itemGeneralDataTable) -> {
          utils.assertItemsExistWithGeneralData(itemGeneralDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          String readUrl = IMObItemRestUrls.READ_GENERAL_DATA_URL + itemCode;
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response itemGeneralDataResponse = utils.sendGETRequest(userCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, itemGeneralDataResponse);
        });

    Then(
        "^the following values of GeneralData section for Item with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String itemCode, String userName, DataTable expectedItemTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseDataIsCorrect(expectedItemTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemViewGeneralDataTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GeneralData section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getMasterDataDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }
}
