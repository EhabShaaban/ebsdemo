package com.ebs.dda.masterdata.vendor;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.vendor.IIObVendorAccountingInfo;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.utils.MObVendorUpdateAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObVendorUpdateAccountingDetailsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private MObVendorUpdateAccountingDetailsTestUtils utils;

  public MObVendorUpdateAccountingDetailsStep() {

    Given(
        "^the following Accounts exist:$",
        (DataTable accountsDataTable) -> {
          utils.assertThatAccountsExist(accountsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to edit AccountingDetails section of Vendor with code \"([^\"]*)\"$",
        (String userName, String vendorCode) -> {
          String lockUrl = IMObVendorRestUrls.LOCK_ACCOUNTING_DETAILS_SECTION + vendorCode;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, vendorCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^AccountingDetails section of Vendor with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String vendorCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName, vendorCode, IMObVendorSectionNames.ACCOUNTING_DETAILS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened the AccountingDetails section Vendor with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String vendorCode) -> {
          String lockUrl = IMObVendorRestUrls.LOCK_ACCOUNTING_DETAILS_SECTION + vendorCode;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, vendorCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^AccountingDetails section of Vendor with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String vendorCode, String userName, String date) -> {
          utils.freeze(date);
          Response response =
              userActionsTestUtils.lockSection(
                  userName,
                  IMObVendorRestUrls.LOCK_ACCOUNTING_DETAILS_SECTION + vendorCode,
                  vendorCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, vendorCode, IMObVendorSectionNames.ACCOUNTING_DETAILS_SECTION);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving AccountingDetails section of Vendor with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String vendorCode, String date) -> {
          utils.freeze(date);
          String unLockURL = IMObVendorRestUrls.UNLOCK_ACCOUNTING_DETAILS_SECTION + vendorCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on AccountingDetails section of Vendor with code \"([^\"]*)\" is released$",
        (String userName, String vendorCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              vendorCode,
              utils.VENDOR_JMX_LOCK_BEAN_NAME,
              IMObVendorSectionNames.ACCOUNTING_DETAILS_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving AccountingDetails section of Vendor with code \"([^\"]*)\"$",
        (String userName, String vendorCode) -> {
          String unLockURL = IMObVendorRestUrls.UNLOCK_ACCOUNTING_DETAILS_SECTION + vendorCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" saves AccountingDetails section of Vendor with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String vendorCode, String dateTime, DataTable AccountInfoDataTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveVendorAccountingDetailsSection(
                  userCookie, vendorCode, AccountInfoDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following error message is attached to Account field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IIObVendorAccountingInfo.ACCOUNT_CODE);
        });

    When(
        "^\"([^\"]*)\" saves AccountingDetails section of Vendor with code \"([^\"]*)\" with the following values:$",
        (String userName, String vendorCode, DataTable AccountInfoDataTable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveVendorAccountingDetailsSection(
                  userCookie, vendorCode, AccountInfoDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Vendor AccountingDetails with code \"([^\"]*)\" is updated as follows:$",
        (String vendorCode, DataTable AccountInfoDataTable) -> {
          utils.assertThatIObVendorAccountUpdatedWithGivenValues(vendorCode, AccountInfoDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObVendorUpdateAccountingDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Update Vendor AccountingDetails section,")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.prepareDbScript());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Update Vendor AccountingDetails section,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IMObVendorRestUrls.UNLOCK_ALL_VENDOR, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Update Vendor AccountingDetails section,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.prepareDbScript());
      databaseConnector.closeConnection();
    }
  }
}
