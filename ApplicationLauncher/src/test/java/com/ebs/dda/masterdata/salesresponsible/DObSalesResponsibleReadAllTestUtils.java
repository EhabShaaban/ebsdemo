package com.ebs.dda.masterdata.salesresponsible;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.salesresponsible.ICObSalesResponsible;
import com.ebs.dda.masterdata.MasterDataTestUtils;
import com.ebs.dda.masterdata.enterprise.ICObEnterpriseRestUrls;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObSalesResponsibleReadAllTestUtils extends MasterDataTestUtils {

  public DObSalesResponsibleReadAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public Response readAllSalesResponsibles(Cookie cookie) {
    String restURL = ICObEnterpriseRestUrls.READ_ALL_SALES_RESPONSIBLES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReadAllSalesResponsibleResponseIsCorrect(
      Response response, DataTable salesResponsiblesDataTable) throws Exception {

    List<Map<String, String>> expectedSalesResponsibles =
        salesResponsiblesDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualSalesResponsible = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedSalesResponsibles.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(expectedSalesResponsibles.get(i), actualSalesResponsible.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IOrderFeatureFileCommonKeys.SALES_RESPONSIBLE,
          ICObSalesResponsible.USER_CODE,
          ICObSalesResponsible.NAME);
    }
  }

  public void assertThatTotalNumberOfRecordsIsCorrect(
      Response response, Integer totalNumberOfRecords) {

    List<HashMap<String, Object>> salesResponsible = getListOfMapsFromResponse(response);

    Assert.assertEquals(totalNumberOfRecords.intValue(), salesResponsible.size());
  }

  public void assertThatSalesResponsiblesExist(DataTable salesResponsiblesDataTable) {
    List<Map<String, String>> salesResponsiblesList =
        salesResponsiblesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesResponsibleRow : salesResponsiblesList) {
      Object salesResponsible =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesResponsibleByCodeAndName",
              constructSalesResponsibleQueryParams(salesResponsibleRow));
      Assert.assertNotNull(salesResponsible);
    }
  }

  private Object[] constructSalesResponsibleQueryParams(Map<String, String> salesResponsibleRow) {
    return new Object[] {
      salesResponsibleRow.get(IFeatureFileCommonKeys.CODE),
      salesResponsibleRow.get(IFeatureFileCommonKeys.NAME),
    };
  }
}
