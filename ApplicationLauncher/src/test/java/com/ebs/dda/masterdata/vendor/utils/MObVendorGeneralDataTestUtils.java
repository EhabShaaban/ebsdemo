package com.ebs.dda.masterdata.vendor.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorFeatureFileCommonKeys;
import com.ebs.dda.masterdata.vendor.interfaces.IMObVendorRestUrls;
import com.ebs.dda.masterdata.vendor.interfaces.IVendorExpectedKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.json.JSONException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MObVendorGeneralDataTestUtils extends MObVendorCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[GeneralData] ";
  private ObjectMapper objectMapper;

  public MObVendorGeneralDataTestUtils(Map<String, Object> properties) {
    super(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    return str.toString();
  }

  public String prepareDbScript() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/vendor/ItemVendorRecord.sql");
    return str.toString();
  }

  public void assertThatVendorGeneralDataExist(DataTable vendorDataTable) throws Exception {
    List<Map<String, String>> vendorGeneralDataMaps =
        vendorDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedVendorGeneralDataMap : vendorGeneralDataMaps) {
      MObVendorGeneralModel vendorGeneralModel =
          assertThatVendorExists(expectedVendorGeneralDataMap);
      assertDateEquals(
          expectedVendorGeneralDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
          vendorGeneralModel.getCreationDate());
    }
  }


  public void assertThatResponseDataIsCorrect(
      Response vendorReadHeaderResponse, DataTable vendorGeneralData) throws Exception {

    Map<String, Object> actualMap =
        vendorReadHeaderResponse.body().jsonPath().get(CommonKeys.RESULT);
    Map<String, String> expectedMap = vendorGeneralData.asMaps(String.class, String.class).get(0);

    assertTrue(ASSERTION_MSG +
            IFeatureFileCommonKeys.VENDOR_NAME + "Doesn't equals",
        (actualMap.get(IMObVendorGeneralModel.VENDOR_NAME))
            .toString()
            .contains((expectedMap.get(IFeatureFileCommonKeys.VENDOR_NAME))));

    assertThatVendorDataEquals(actualMap, expectedMap);

    assertThatCreationDateIsCorrect(actualMap, expectedMap);

    orderActualBusinessUnits(actualMap);

    assertThatPurchaseUnitsAreEquals(expectedMap, actualMap);
  }

  private String getPurchaseResponsible(Map<String, Object> actualMap)
      throws IOException, JSONException {
    String purchaseResponsibleName =
        objectMapper.writeValueAsString(
            actualMap.get(IMObVendorGeneralModel.PURCHASING_RESPONSIBLE_NAME));
    String purchaseResponsible =
        actualMap.get(IMObVendorGeneralModel.PURCHASING_RESPONSIBLE_CODE)
            + " - "
            + getEnglishLocaleFromLocalizedResponse(purchaseResponsibleName);
    return purchaseResponsible;
  }

  private void assertThatVendorDataEquals(
      Map<String, Object> actualMap, Map<String, String> expectedMap)
      throws JSONException, IOException {
    assertEquals(ASSERTION_MSG +
            IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE + " Doesn't equals",
        expectedMap.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE),
        getPurchaseResponsible(actualMap));
    assertEquals(ASSERTION_MSG +
            IMObVendorFeatureFileCommonKeys.OLD_VENDOR_REFERENCE + " Doesn't equals",
        expectedMap.get(IMObVendorFeatureFileCommonKeys.OLD_VENDOR_REFERENCE),
        actualMap.get(IMObVendorGeneralModel.OLD_VENDOR_NUMBER));
    assertEquals(ASSERTION_MSG +
            IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR + " Doesn't equals",
        expectedMap.get(IMObVendorFeatureFileCommonKeys.ACCOUNT_WITH_VENDOR),
        actualMap.get(IMObVendorGeneralModel.ACCOUNT_WITH_VENDOR));
  }

  public JsonObject createVendorGeneralDataJsonObject(
      String vendorCode, DataTable vendorGeneralDataTable) {
    JsonObject vendorGeneralDataObject = new JsonObject();
    Map<String, String> expectedVendorGeneralData =
        vendorGeneralDataTable.asMaps(String.class, String.class).get(0);
    JsonArray purchaseUnitArray = getPurchaseUnits(expectedVendorGeneralData);
    addPropertiesToVendorGeneralData(
        vendorGeneralDataObject, purchaseUnitArray, vendorCode, expectedVendorGeneralData);
    return vendorGeneralDataObject;
  }

  private JsonArray getPurchaseUnits(Map<String, String> vendorGeneralData) {
    List<String> purchaseUnits =
        Arrays.asList(vendorGeneralData.get(IVendorExpectedKeys.PURCHASING_UNIT).split(","));
    purchaseUnits.replaceAll(String::trim);
    return createJsonArray(purchaseUnits);
  }

  private JsonArray createJsonArray(List<String> purchaseUnits) {
    JsonArray purchaseUnitArray = new JsonArray();
    for (String purchaseUnit : purchaseUnits) {
      purchaseUnitArray.add(purchaseUnit);
    }
    return purchaseUnitArray;
  }

  private void addPropertiesToVendorGeneralData(
      JsonObject vendorGeneralDataObject,
      JsonArray purchaseUnitArray,
      String vendorCode,
      Map<String, String> expectedVendorGeneralData) {
    vendorGeneralDataObject.add(
        IMObVendorGeneralDataValueObject.PURCHASE_UNIT_CODES, purchaseUnitArray);
    vendorGeneralDataObject.addProperty(IMObVendorGeneralDataValueObject.VENDOR_CODE, vendorCode);
    vendorGeneralDataObject.addProperty(
        IMObVendorGeneralDataValueObject.VENDOR_NAME,
        expectedVendorGeneralData.get(IFeatureFileCommonKeys.NAME));
    vendorGeneralDataObject.addProperty(
        IMObVendorGeneralDataValueObject.PURCHASE_RESPONSIBLE_CODE,
        expectedVendorGeneralData.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE));
    vendorGeneralDataObject.addProperty(
        IMObVendorGeneralDataValueObject.ACCOUNT_WITH_VENDOR,
        expectedVendorGeneralData.get(IVendorExpectedKeys.ACCOUNT_WITH_VENDOR));
  }

  public Response saveVendorGeneralDataSection(
      Cookie userCookie, JsonObject vendorObject, String vendorCode) {
    String restURL = IMObVendorRestUrls.SAVE_VENDOR_GENERAL_DATA_URL + vendorCode;
    return sendPUTRequest(userCookie, restURL, vendorObject);
  }

  public void assertThatGeneralDataIsUpdated(String vendorCode, DataTable expectedDataTable) {
    Map<String, String> expectedVendorGeneralDataMap =
        expectedDataTable.asMaps(String.class, String.class).get(0);

    MObVendorGeneralModel vendorGeneralModel = assertThatVendorExists(expectedVendorGeneralDataMap);

    assertDateEquals(
            expectedVendorGeneralDataMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            vendorGeneralModel.getModifiedDate());

    assertEquals(
        ASSERTION_MSG
            + expectedVendorGeneralDataMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY)
            + " for vendor with code "
            + vendorCode
            + " Not equals",
        expectedVendorGeneralDataMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
        vendorGeneralModel.getModificationInfo());
  }
}
