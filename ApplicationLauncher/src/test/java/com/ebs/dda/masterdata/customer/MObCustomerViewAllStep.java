package com.ebs.dda.masterdata.customer;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomerGeneralModel;
import com.ebs.dda.masterdata.customer.utils.MObCustomerViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class MObCustomerViewAllStep extends SpringBootRunner implements En {

  private MObCustomerViewAllTestUtils utils;

  public MObCustomerViewAllStep() {

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IMObCustomerRestURLs.VIEW_ALL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable customerData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatCustomerDataIsCorrect(customerData, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(IMObCustomerGeneralModel.USER_CODE, userCodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IMObCustomerRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with filter applied on OldCustomerNumber which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username,
            Integer pageNumber,
            String OldCustomerNumberContains,
            Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(
                  IMObCustomerGeneralModel.OLD_CUSTOMER_NUMBER, OldCustomerNumberContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IMObCustomerRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with filter applied on MarketName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String marketName,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(IMObCustomerGeneralModel.MARKET_NAME, marketName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IMObCustomerRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with filter applied on LegalName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String legalName,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(IMObCustomerGeneralModel.LEGAL_NAME, legalName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IMObCustomerRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String type, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getEqualsFilterField(IMObCustomerGeneralModel.TYPE, type);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IMObCustomerRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with filter applied on BusinessUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String businessUnitCode,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IMObCustomerGeneralModel.BUSINESS_UNIT_NAMES, businessUnitCode);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie, IMObCustomerRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Customers with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String stateName, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(IMObCustomerGeneralModel.CURRENT_STATES, stateName);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IMObCustomerRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new MObCustomerViewAllTestUtils(properties, entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all Customers,")) {
      databaseConnector.executeSQLScript(utils.prepareDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all Customers,")) {
      utils.unfreeze();
    }
  }
}
