package com.ebs.dda.masterdata.exchangerate;

public interface ICObExchangeRateRestURLs {

  String BASE_URL = "/services/masterdata/exchangerate";
  String EXCHANGERATE_COMMAND = "/command/masterdata/exchangerate";
  String CREATE_EXCHANGE_RATE = EXCHANGERATE_COMMAND + "/";
  String AUTHORIZED_ACTIONS = BASE_URL + "/authorizedactions";
  String VIEW_ALL_URL = BASE_URL;
}
