package com.ebs.dda.masterdata.exchangerate;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateRestURLs;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateAllowedActionsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;


public class CObExchangeRateAllowedActionsStep extends SpringBootRunner implements En {
    private static boolean hasBeenExecutedOnce = false;
    private CObExchangeRateAllowedActionsTestUtils utils;


    public CObExchangeRateAllowedActionsStep() {
        When(
                "^\"([^\"]*)\" requests to read actions of ExchangeRate in home screen$",
                (String userName) -> {
                    String url = ICObExchangeRateRestURLs.AUTHORIZED_ACTIONS;
                    Response response =
                            utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following actions are displayed to \"([^\"]*)\":$",
                (String userName, DataTable actionsDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatTheFollowingActionsExist(response, actionsDataTable);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        utils = new CObExchangeRateAllowedActionsTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
      if (contains(scenario.getName(), "Read allowed actions in ExchangeRate")) {
        databaseConnector.createConnection();
        if (!hasBeenExecutedOnce) {
          databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
          hasBeenExecutedOnce = true;
        }
      }
    }

    @After
    public void after(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Read allowed actions in ExchangeRate")) {
        databaseConnector.closeConnection();
      }
    }
}
