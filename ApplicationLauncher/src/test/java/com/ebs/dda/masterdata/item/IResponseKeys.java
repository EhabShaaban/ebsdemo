package com.ebs.dda.masterdata.item;

public interface IResponseKeys {

  String ITEM_CODE = "itemCode";
  String ITEM_UOMS = "itemUnitOfMeasures";

  String ALTERNATIVE_UNIT_CODE = "alternativeUnitCode";
  String ALTERNATIVE_UNIT_NAME = "alternativeUnitOfMeasureName";
  String CONVERSION_FACTOR = "conversionFactor";
  String OLD_ITEM_NUMBER = "oldItemNumber";

  String ITEM_NAME = "itemName";
  String BASE_UNIT = "basicUnitOfMeasureName";
  String OLD_ITEM_REFERENCE = "oldItemNumber";
  String BATCH_MANAGED = "isBatchManaged";
  String ITEM_GROUP = "itemGroupName";
  String CREATION_DATE = "creationDate";
  String PURCHASING_UNITS = "purchasingUnitNames";
  String PURCHASING_UNITS_CODES = "purchasingUnitCodes";

  String ACCOUNT_NAME = "accountName";
  String ACCOUNT_CODE = "accountCode";

  String USER_CODE = "userCode";

}
