package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.masterdata.item.IExpectedKeys;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.ebs.dda.masterdata.item.IResponseKeys;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class IObItemUOMCommonTestUtils extends MObItemCommonTestUtils {

  public IObItemUOMCommonTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
        str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    return str.toString();
  }

  public void assertThatItemHasUnitOfMeasure(Object... params) {
    Object uom =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("getItemUOM", params);
    Assert.assertNotNull(ASSERTION_MSG + " Item did not have UOM", uom);
  }

  public Object[] constructItemHasUnitOfMeasureQueryParams(String item, List<String> itemUOMData) {
    return (isUOMParamsEmpty(itemUOMData))
            ? null
            : new Object[]{
            item.split(" - ")[0],
            itemUOMData.get(0).split(" - ")[0],
            Float.parseFloat(itemUOMData.get(1)),
            itemUOMData.get(2)
    };
  }

  private boolean isUOMParamsEmpty(List<String> itemUOMData) {
    for (String parameter : itemUOMData) {
      if (!parameter.isEmpty()) return false;
    }
    return true;
  }

  public JsonObject createItemUOMJsonObject(String itemCode, DataTable itemUOMDataTable) {
    JsonObject itemUOMDataObjects = new JsonObject();
    List<Map<String, String>> itemUOMs = itemUOMDataTable.asMaps(String.class, String.class);
    JsonArray itemUOMsJsonArray = createJsonArrayOfUOMs(itemUOMs);
    itemUOMDataObjects.addProperty(IResponseKeys.ITEM_CODE, itemCode);
    itemUOMDataObjects.add(IResponseKeys.ITEM_UOMS, itemUOMsJsonArray);
    return itemUOMDataObjects;
  }

  public Response saveItemUnitOfMeasureSection(Cookie userCookie, JsonObject itemUOMsObject) {
    String restURL = IMObItemRestUrls.SAVE_ITEM_UOM_URL;
    return sendPUTRequest(userCookie, restURL, itemUOMsObject);
  }

  private JsonArray createJsonArrayOfUOMs(List<Map<String, String>> itemUOMs) {
    JsonArray itemUOMsJsonArray = new JsonArray();
    for (Map<String, String> itemUOM : itemUOMs) {
      JsonObject itemUOMDataObject = new JsonObject();
      addPropertiesToItemUOM(itemUOMDataObject, itemUOM);
      itemUOMsJsonArray.add(itemUOMDataObject);
    }
    return itemUOMsJsonArray;
  }

  private void addPropertiesToItemUOM(JsonObject itemUOMObject, Map<String, String> itemUOM) {
    itemUOMObject.addProperty(
        IResponseKeys.ALTERNATIVE_UNIT_CODE, itemUOM.get(IExpectedKeys.ALETRNATIVE_UNIT_CODE));
    Float conversionFactor = Float.parseFloat(itemUOM.get(IExpectedKeys.CONVERSION_FACTOR));
    itemUOMObject.addProperty(IResponseKeys.CONVERSION_FACTOR, conversionFactor);
    itemUOMObject.addProperty(
        IResponseKeys.OLD_ITEM_NUMBER, itemUOM.get(IExpectedKeys.OLD_ITEM_NUMBER));
  }
}
