package com.ebs.dda.masterdata.chartofaccounts;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryTestUtils;
import com.ebs.dda.accounting.settlement.IDObSettlementRestURLs;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccountCreateValueObject;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsAllowedActionsTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsCreateHPTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsCreateValTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsDeleteTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;
import org.springframework.beans.factory.InitializingBean;

public class HObChartOfAccountsAllowedActionsOSStep extends SpringBootRunner
    implements En, InitializingBean {

  public static final String SCENARIO_NAME =
      "Read allowed actions in ChartOfAccounts Object Screen";
  private HObCommonChartOfAccountTestUtils utils;

  public HObChartOfAccountsAllowedActionsOSStep() {

    When("^\"([^\"]*)\" requests to read actions of ChartOfAccounts with code \"([^\"]*)\"$",
        (String username, String glAccountCode) -> {
          String url = IChartOfAccountsRestUrls.AUTHORIZED_ACTIONS + '/' + glAccountCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(username), url);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then("^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });

  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new HObCommonChartOfAccountTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(HObChartOfAccountsDeleteTestUtils.clearChartOfAccounts());
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());

    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
      utils.unfreeze();
    }
  }
}
