package com.ebs.dda.masterdata;

public interface IMasterDataFeatureFileCommonKeys {

  String MARKET_NAME = "MarketName";
  String LEGAL_NAME = "LegalName";
  String TYPE = "Type";
  String CREDIT_LIMIT = "CreditLimit";
  String CURRENCY = "Currency";
  String BUSINESS_UNITS = "BusinessUnits";
  String CUSTOMER_CODE = "CustomerCode";
  String CUSTOMER_ADDRESS_ID = "CustomerAddressId";
  String ADDRESS_NAME = "AddressName";
  String ADDRESS_DESCRIPTION = "AddressDescription";
  String CUSTOMER_CONTACT_PERSON_ID = "CustomerContactPersonId";
  String CONTACT_PERSON_NAME = "ContactPersonName";
  String REGISTRATION_NUMBER = "RegistrationNumber";
  String ISSUED_FROM = "IssuedFrom";
  String TAX_CARD_NUMBER = "TaxCardNumber";
  String TAX_ADMINISTRATION = "TaxAdministration";
  String TAX_FILE_NUMBER = "TaxFileNumber";
  String OLD_CUSTOMER_NUMBER = "OldCustomerNumber";
  String OLD_NUMBER = "OldNumber";

  String ITEM_GROUP = "ItemGroup";
  String BATCH_MANAGED = "BatchManaged";
  String BASE_UNIT = "BaseUnit";
  String OLD_ITEM_REFERENCE = "OldItemReference";
  String ITEM_TYPE = "ItemType";

  String EMPLOYEE = "Employee";
  Object IS_DEFAULT = "isDefault";
  String ITEM_CLASS = "ItemClass" ;
  String OLD_ITEM_NUMBER = "OldItemNumber";
  String ALTERNATIVE_UNIT = "AlternativeUnit";
  String CONVERSION_FACTOR = "ConversionFactor";
  String UNIT_OF_MEASURE = "UnitOfMeasure";
  String UNIT_PRICE = "UnitPrice";

  String COMPANY_GROUP = "CompanyGroup";

}
