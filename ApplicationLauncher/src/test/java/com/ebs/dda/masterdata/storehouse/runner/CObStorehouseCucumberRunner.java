package com.ebs.dda.masterdata.storehouse.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"classpath:features/masterdata/storehouse/Storehouse_Dropdowns.feature"},
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.storehouse"},
    strict = true,
    tags = "not @Future")
public class CObStorehouseCucumberRunner {}
