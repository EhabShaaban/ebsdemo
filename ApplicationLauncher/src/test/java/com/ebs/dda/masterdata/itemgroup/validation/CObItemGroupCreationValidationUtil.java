package com.ebs.dda.masterdata.itemgroup.validation;

import com.ebs.dda.masterdata.itemgroup.CObItemGroupFeatureTestUtils;
import com.ebs.dda.masterdata.itemgroup.ICObItemGroupRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class CObItemGroupCreationValidationUtil extends CObItemGroupFeatureTestUtils {

  public CObItemGroupCreationValidationUtil(Map<String, Object> properties) {
    super(properties);
  }

  Response createItemGroup(Cookie userCookie, DataTable itemGroupsDataTable) {
    JsonObject itemGroupData = prepareItemJsonObject(itemGroupsDataTable);
    String restURL = ICObItemGroupRestUrls.CREATE_ITEM_URL;
    return sendPostRequest(userCookie, restURL, itemGroupData);
  }
}
