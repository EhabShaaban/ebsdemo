package com.ebs.dda.masterdata.chartofaccounts.utils;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccountsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class HObChartOfAccountsViewAllTestUtils extends CommonTestUtils {

  private ObjectMapper objectMapper;

  public HObChartOfAccountsViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts_ViewAll.sql");
    return str.toString();
  }

  public void assertThatAccountsExist(DataTable accountsDataTable) {
    List<Map<String, String>> accounts = accountsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> account : accounts) {
      assertThatAccountExists(account);
    }
  }

  private void assertThatAccountExists(Map<String, String> account) {
    HObChartOfAccountsGeneralModel accountsGeneralModel =
        (HObChartOfAccountsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getChartOfAccounts",
                account.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
                account.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME),
                    account.get(IAccountingFeatureFileCommonKeys.LEVEL),
                    "%\"" + account.get(IFeatureFileCommonKeys.STATE) + "\"%");

    assertNotNull("This Account Does not exist", accountsGeneralModel);

    assertThatFieldExists(
        account.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE),
        accountsGeneralModel.getAccountTypeEn());

    assertThatFieldExists(
        account.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
        accountsGeneralModel.getCreditDebitEn());

    assertEquals(
        account.get(IAccountingFeatureFileCommonKeys.PARENT_CODE_NAME),
        accountsGeneralModel.getParentCodeName());
  }

  private void assertThatFieldExists(String expectedValue, String actualValue) {
    Assert.assertEquals(convertEmptyStringToNull(expectedValue), actualValue);
  }

  public void assertThatChartOfAccountsDataMatches(Response userResponse, DataTable accountsDataTable)
      throws IOException, JSONException {
    List<Map<String, String>> expectedAccounts =
        accountsDataTable.asMaps(String.class, String.class);

    assertResponseSuccessStatus(userResponse);
    assertCorrectNumberOfRows(userResponse, expectedAccounts.size());

    List<HashMap<String, Object>> listOfAccounts = getListOfMapsFromResponse(userResponse);

    for (int i = 0; i < expectedAccounts.size(); i++) {
      assertThatChartOfAccountMatchesTheExpected(expectedAccounts.get(i), listOfAccounts.get(i));
    }
  }

  private void assertThatChartOfAccountMatchesTheExpected(
      Map<String, String> expectedAccount, HashMap<String, Object> actualAccount)
      throws IOException, JSONException {

    assertEquals(
            expectedAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
        actualAccount.get(IHObChartOfAccountsGeneralModel.USER_CODE));
    assertEquals(
            expectedAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_NAME),
            getActualValue(actualAccount.get(IHObChartOfAccountsGeneralModel.ACCOUNT_NAME)));
    assertEquals(
            convertEmptyStringToNull(
                    expectedAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE)),
            actualAccount.get(IHObChartOfAccountsGeneralModel.ACCOUNT_TYPE));
    assertEquals(
            expectedAccount.get(IAccountingFeatureFileCommonKeys.LEVEL),
        actualAccount.get(IHObChartOfAccountsGeneralModel.LEVEL));
    assertEquals(
                    expectedAccount.get(IAccountingFeatureFileCommonKeys.PARENT_CODE_NAME),
        actualAccount.get(IHObChartOfAccountsGeneralModel.PARENT_CODE_NAME));
    assertEquals(
            convertEmptyStringToNull(
                    expectedAccount.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT)),
            actualAccount.get(IHObChartOfAccountsGeneralModel.CREDIT_DEBIT));
    assertThatStatesArrayContainsValue(
        getStatesArray(
            String.valueOf(actualAccount.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME))),
        expectedAccount.get(IFeatureFileCommonKeys.STATE));
  }

  private String getActualValue(Object value) throws IOException, JSONException {
    return value != null
        ? getEnglishLocaleFromLocalizedResponse(objectMapper.writeValueAsString(value))
        : null;
  }
}
