package com.ebs.dda.masterdata.purchaseresponsible;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.ICObPurchasingResponsibleGeneralModel;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.masterdata.enterprise.ICObEnterpriseRestUrls;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class CObPurchaseResponsibleDropDownTestUtils extends CObEnterpriseTestUtils {

  public CObPurchaseResponsibleDropDownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(entityManagerDatabaseConnector);
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");

    return str.toString();
  }

  public String readAllPurchaseResponsibles() {
    return ICObEnterpriseRestUrls.READ_PURCHASING_RESPONSIBLE_URL;
  }

  public void assertThatPurchaseResponsiblesExist(DataTable purchaseResponsibleDataTable) {
    List<Map<String, String>> purchaseResponsiblesList =
        purchaseResponsibleDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseResponsible : purchaseResponsiblesList) {

      CObPurchasingResponsibleGeneralModel cObPurchasingResponsibleGeneralModel =
          (CObPurchasingResponsibleGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseResponsibleByCodeAndNameAndState",
                  constructPurchaseResponsibleQueryParams(purchaseResponsible));

      assertNotNull(cObPurchasingResponsibleGeneralModel);
    }
  }

  private Object[] constructPurchaseResponsibleQueryParams(
      Map<String, String> purchaseResponsible) {
    return new Object[] {
      purchaseResponsible.get(IFeatureFileCommonKeys.CODE),
      purchaseResponsible.get(IFeatureFileCommonKeys.NAME),
      "%" + purchaseResponsible.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatResponseMatchesExpected(
      DataTable purchaseResponsibleDataTable, Response response) throws IOException, JSONException {
    List<Map<String, String>> purchaseResponsibleExpectedList =
        purchaseResponsibleDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> purchaseResponsibleActualList =
        getListOfMapsFromResponse(response);

    for (int i = 0; i < purchaseResponsibleActualList.size(); i++) {
      assertThatExpectedPurchaseResponsibleMatchesActual(
          purchaseResponsibleExpectedList.get(i), purchaseResponsibleActualList.get(i));
    }
  }

  private void assertThatExpectedPurchaseResponsibleMatchesActual(
      Map<String, String> expectedPurchaseResponsible,
      HashMap<String, Object> actualPurchaseResponsible)
      throws IOException, JSONException {
    assertEquals(
        expectedPurchaseResponsible.get(IFeatureFileCommonKeys.CODE),
        actualPurchaseResponsible.get(ICObPurchasingResponsibleGeneralModel.CODE));
    assertEquals(
        expectedPurchaseResponsible.get(IFeatureFileCommonKeys.NAME),
        getLocaleFromLocalizedResponse(
            getLocalizedValue(
                actualPurchaseResponsible.get(ICObPurchasingResponsibleGeneralModel.NAME)),
            "en"));
  }

  public void assertThatAllPurchaseResponsiblesExist(Integer recordsNumber) {
    Long purchasingResponsiblesCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseResponsiblesCount");

    assertEquals(Long.valueOf(recordsNumber), purchasingResponsiblesCount);
  }
}
