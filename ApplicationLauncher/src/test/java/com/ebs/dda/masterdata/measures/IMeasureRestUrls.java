package com.ebs.dda.masterdata.measures;

public interface IMeasureRestUrls {
  String COMMAND = "/command/measure";

  String CREATE = COMMAND + "/";
}
