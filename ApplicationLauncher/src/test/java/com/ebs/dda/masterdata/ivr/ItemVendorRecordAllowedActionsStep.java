package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordAllowedActionsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class ItemVendorRecordAllowedActionsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordAllowedActionsTestUtils utils;

  public ItemVendorRecordAllowedActionsStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of IVR with IVRCode \"([^\"]*)\"$",
        (String userName, String itemVendorRecordCode) -> {
          String url = IVRRestUrls.OBJECT_SCREEN_ALLOWED_ACTIONS_URL + itemVendorRecordCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String allowedActions, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of IVR home screen$",
        (String userName) -> {
          Response response =
              utils.sendGETRequest(
                  userActionsTestUtils.getUserCookie(userName),
                  IVRRestUrls.HOME_SCREEN_ALLOWED_ACTIONS_URL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new ItemVendorRecordAllowedActionsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "ViewAll IVRs")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsPath());
    }
  }
}
