package com.ebs.dda.masterdata.chartofaccounts.utils;

import java.util.Map;

import org.junit.Assert;

import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.masterdata.chartofaccounts.IChartOfAccountsRestUrls;

import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class HObChartOfAccountsDeleteTestUtils extends HObCommonChartOfAccountTestUtils {

	public HObChartOfAccountsDeleteTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public String getDbScriptsOneTimeExecution() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/clearDataSqlScript.sql");
		str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
		str.append(",").append("db-scripts/accounting/fiscal-year/CObFiscalYear.sql");
		str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
		str.append(",").append(
						"db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyDataViewAll.sql");
		return str.toString();
	}

	public Response deleteGLAccount(Cookie userCookie, String glAccountCode) {
		String deleteURL = IChartOfAccountsRestUrls.CHART_OF_ACCOUNT_COMMAND + "/" + glAccountCode;
		return sendDeleteRequest(userCookie, deleteURL);
	}

	public void assertThatGLAccountConfigurationExists(String glConfigurationCode) {
		LObGlobalGLAccountConfigGeneralModel config = getGlConfigurationByCode(glConfigurationCode);
		Assert.assertNotNull(config);

	}

	private LObGlobalGLAccountConfigGeneralModel getGlConfigurationByCode(
					String glConfigurationCode) {
		LObGlobalGLAccountConfigGeneralModel config = (LObGlobalGLAccountConfigGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getGlConfigurationByCode",
										glConfigurationCode);
		return config;
	}

	public void assertThatGLAccountConfigurationDoesNotExist(String glConfigurationCode) {
		LObGlobalGLAccountConfigGeneralModel config = getGlConfigurationByCode(glConfigurationCode);

		Assert.assertNull(config);
	}

	public void assertThatGLAccountDoesNotExist(String glAccountCode) {
		HObChartOfAccountsGeneralModel glAccount = getGlAccountByCode(glAccountCode);
		Assert.assertNull(glAccount);

	}

	public void assertThatGLAccountExists(String glAccountCode) {
		HObChartOfAccountsGeneralModel glAccount = getGlAccountByCode(glAccountCode);
		Assert.assertNotNull(glAccount);
	}

	private HObChartOfAccountsGeneralModel getGlAccountByCode(String glConfigurationCode) {
		HObChartOfAccountsGeneralModel glAccount = (HObChartOfAccountsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getGlAccountByCode",
										glConfigurationCode);
		return glAccount;
	}
}
