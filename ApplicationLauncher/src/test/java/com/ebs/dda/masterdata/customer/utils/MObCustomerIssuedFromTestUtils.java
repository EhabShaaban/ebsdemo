package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.lookups.ILObIssueFrom;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.customer.IMObCustomerRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MObCustomerIssuedFromTestUtils extends MObCustomerCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[IssuedFrom][dropDown]";

  public MObCustomerIssuedFromTestUtils(Map<String, Object> properties, EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readAllIssuedFrom(Cookie cookie) {
    String restURL = IMObCustomerRestURLs.READ_ALL_ISSUED_FROM;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatIssuedFromResponseIsCorrect(
      DataTable issuedFromDataTable, Response response) throws Exception {
    List<Map<String, String>> expectedIssuedFromList =
        issuedFromDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualIssuedFromList = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedIssuedFromList.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(expectedIssuedFromList.get(i), actualIssuedFromList.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IMasterDataFeatureFileCommonKeys.ISSUED_FROM,
          ILObIssueFrom.USER_CODE,
          ILObIssueFrom.NAME);
    }
  }

  public void assertThatIssuedFromExist(DataTable IssuedFromDataTable) {
    List<Map<String, String>> issuedFromMapList =
        IssuedFromDataTable.asMaps(String.class, String.class);

    for (Map<String, String> issuedFromMapRow : issuedFromMapList) {
      Object issuedFrom =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIssuedFromByCodeAndName", constructIssuedFromQueryParams(issuedFromMapRow));
      assertNotNull(issuedFrom);
    }
  }

  private Object[] constructIssuedFromQueryParams(Map<String, String> issuedFromMapRow) {
    return new Object[] {
      issuedFromMapRow.get(IFeatureFileCommonKeys.CODE),
      issuedFromMapRow.get(IFeatureFileCommonKeys.NAME)
    };
  }

  public void assertThatTotalNumberOfIssuedFromEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualIssuedFrom = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of IssuedFrom is not correct",
        expectedTotalNumber.intValue(),
        actualIssuedFrom.size());
  }
  public String prepareDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    return str.toString();
  }
}
