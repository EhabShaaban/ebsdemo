package com.ebs.dda.masterdata.measures.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/unit-of-measure/UOM_Create_Val.feature",
      "classpath:features/masterdata/unit-of-measure/UOM_Create_HP.feature",
      "classpath:features/masterdata/unit-of-measure/UOM_Create_Auth.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.measures"},
    strict = true,
    tags = "not @Future")
public class CObMeasureCucumberRunner {}
