package com.ebs.dda.masterdata.purchaseresponsible;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/purchase-responsible/PurchaseResponsible_Dropdowns.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.enterprise.purchaseresponsible.reads"},
    strict = true,
    tags = "not @Future")
public class CObPurchaseResponsibleDropDownCucumberRunner {}
