package com.ebs.dda.masterdata.itemgroup.delete;

import com.ebs.dda.SpringBootRunner;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class CObItemGroupDeletionStep extends SpringBootRunner implements En {

  private CObItemGroupDeletionUtil itemGroupDeletionFeatureTestUtils;

  public CObItemGroupDeletionStep() {

    When(
        "^\"([^\"]*)\" requests to delete ItemGroup with code \"([^\"]*)\"$",
        (String username, String itemCode) -> {
          Response deleteItemGroupResponse =
              itemGroupDeletionFeatureTestUtils.deleteItemGroupByCode(
                  userActionsTestUtils.getUserCookie(username), itemCode);
          userActionsTestUtils.setUserResponse(username, deleteItemGroupResponse);
        });

    Then(
        "^ItemGroup with code \"([^\"]*)\" is deleted from the system$",
        (String code) -> {
          itemGroupDeletionFeatureTestUtils.assertThatItemGroupWithCodeIsNotExists(code);
        });

    Given(
        "^\"([^\"]*)\" first deletes ItemGroup with code \"([^\"]*)\" successfully$",
        (String username, String itemCode) -> {
          Response deletePurchaseOrderResponse =
              itemGroupDeletionFeatureTestUtils.deleteItemGroupByCode(
                  userActionsTestUtils.getUserCookie(username), itemCode);
          userActionsTestUtils.setUserResponse(username, deletePurchaseOrderResponse);
        });
  }

  @Before
  public void setup() throws Exception {
    itemGroupDeletionFeatureTestUtils = new CObItemGroupDeletionUtil(getProperties());
    itemGroupDeletionFeatureTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    databaseConnector.createConnection();
    databaseConnector.executeSQLScript(
        itemGroupDeletionFeatureTestUtils.getMasterDataDbScriptsOneTimeExecution());
  }

  @After
  public void afterEachScenario() throws SQLException {
    itemGroupDeletionFeatureTestUtils.unfreeze();
    databaseConnector.closeConnection();
  }
}
