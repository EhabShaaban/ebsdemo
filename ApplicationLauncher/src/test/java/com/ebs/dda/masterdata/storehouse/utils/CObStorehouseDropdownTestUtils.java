package com.ebs.dda.masterdata.storehouse.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.masterdata.enterprise.ICObEnterpriseRestUrls;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class CObStorehouseDropdownTestUtils extends CommonTestUtils {

  public CObStorehouseDropdownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super();
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    return str.toString();
  }

  public void assertThatCompanyHasTheFollowingPlants(
      String companyCode, DataTable plantsDataTable) {
    List<Map<String, String>> plantsList = plantsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedPlantMap : plantsList) {

      Object actualPlant =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPlantByCompany", constructPlantQueryParams(companyCode, expectedPlantMap));

      Assert.assertNotNull(actualPlant);
    }
  }

  private Object[] constructPlantQueryParams(
      String companyCode, Map<String, String> expectedPlantMap) {
    return new Object[] {companyCode, expectedPlantMap.get(IFeatureFileCommonKeys.PLANT)};
  }

  public void assertThatPlantHasTheFollowingStorehouses(
      String storehouseCode, DataTable storehousesDataTable) {
    List<Map<String, String>> storehousesMapList =
        storehousesDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedStorehouseMap : storehousesMapList) {

      Object actualStorehouse =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getStorehouseByPlant",
              constructStorehouseQueryParams(storehouseCode, expectedStorehouseMap));

      Assert.assertNotNull(actualStorehouse);
    }
  }

  private Object[] constructStorehouseQueryParams(
      String storehouseCode, Map<String, String> expectedStorehouseMap) {
    return new Object[] {
      storehouseCode, expectedStorehouseMap.get(IFeatureFileCommonKeys.STOREHOUSE)
    };
  }

  public String getStorehousesByCompanyCodeUrl(String companyCode) {
    return ICObEnterpriseRestUrls.READ_COMPANY_STOREHOUSES_URL + companyCode;
  }

  public void assertThatStorehousesResponseIsCorrect(Response response, DataTable plantsDataTable)
      throws Exception {

    List<Map<String, String>> expectedStorehouses =
        plantsDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualStorehouses = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedStorehouses.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedStorehouses.get(i), actualStorehouses.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IFeatureFileCommonKeys.STOREHOUSE, ICObStorehouse.USER_CODE, ICObStorehouse.NAME);
    }
  }

  public void assertThatStorehousesResponseCountIsCorrect(
      Response response, int storehousesNumber) {
    List<HashMap<String, Object>> actualAddresses = getListOfMapsFromResponse(response);
    Assert.assertEquals(storehousesNumber, actualAddresses.size());
  }

  public String getStorehousesByPlantCodeUrl(String plantCode) {
    return ICObEnterpriseRestUrls.READ_PLANT_STOREHOUSES_URL + plantCode;
  }
}
