package com.ebs.dda.masterdata.exchangerate.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

public class CObExchangeRateCreateValidationTestUtils extends CObExchangeRateCreateCommonTestUtils {

  public CObExchangeRateCreateValidationTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatTheFollowingExchangeRatesExist(DataTable erDataTable) {

    List<Map<String, String>> exchangeRateMapsList = erDataTable.asMaps(String.class, String.class);
    for (Map<String, String> exchangeRateMap : exchangeRateMapsList) {
      Object[] queryParams = constructExchangeRateQueryParams(exchangeRateMap);
      assertThatExchangeRateExists(queryParams);
    }
  }

  public void assertThatExchangeRateExists(Object[] queryParams) {
    CObExchangeRate exchangeRate = (CObExchangeRate) entityManagerDatabaseConnector
        .executeNativeNamedQuerySingleResult("getExchangeRate", queryParams);

    assertNotNull(exchangeRate);
  }

  private Object[] constructExchangeRateQueryParams(Map<String, String> exchangeRateMap) {
    return new Object[] {exchangeRateMap.get(IFeatureFileCommonKeys.CODE),
        "%" + exchangeRateMap.get(IFeatureFileCommonKeys.STATE) + "%",
        exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.FIRST_CURRENCY),
        exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_CURRENCY),
        exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.VALID_FROM),
        exchangeRateMap.get(IFeatureFileCommonKeys.TYPE)};
  }

  public Response requestCreateExchangeRate(Cookie cookie) {
    String restURL = ICObExchangeRateRestURLs.CREATE_EXCHANGE_RATE;
    return sendGETRequest(cookie, restURL);
  }
}
