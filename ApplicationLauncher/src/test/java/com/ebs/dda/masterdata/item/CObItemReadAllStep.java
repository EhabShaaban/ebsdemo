package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.CObItemReadAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class CObItemReadAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private CObItemReadAllTestUtils utils;

  public CObItemReadAllStep() {
    Given(
        "^the following ItemTypes exist:$",
        (DataTable itemTypesDataTable) -> {
          utils.assertThatItemTypesExist(itemTypesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read Item Types$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, IMObItemRestUrls.READ_ITEM_TYPES_URL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following ItemTypes values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable itemTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatActualItemTypesAreReturned(response, itemTypesDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new CObItemReadAllTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of Item Types dropdown")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }
}
