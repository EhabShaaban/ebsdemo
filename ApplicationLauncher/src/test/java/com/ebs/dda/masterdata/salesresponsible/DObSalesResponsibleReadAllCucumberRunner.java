package com.ebs.dda.masterdata.salesresponsible;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/sales-responsible/SalesResponsibles_Dropdowns.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.salesresponsible"},
    strict = true,
    tags = "not @Future")
public class DObSalesResponsibleReadAllCucumberRunner {}
