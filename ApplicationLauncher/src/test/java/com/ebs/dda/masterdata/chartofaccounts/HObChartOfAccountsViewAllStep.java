package com.ebs.dda.masterdata.chartofaccounts;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccountsGeneralModel;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class HObChartOfAccountsViewAllStep extends SpringBootRunner implements En, InitializingBean {

  private static boolean hasBeenExecutedOnce = false;
  private HObChartOfAccountsViewAllTestUtils chartOfAccountsViewAllTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        chartOfAccountsViewAllTestUtils = new HObChartOfAccountsViewAllTestUtils(properties);
        chartOfAccountsViewAllTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }

    public HObChartOfAccountsViewAllStep() {

    Given(
        "^the following GL Accounts exist:$",
        (DataTable accountsDataTable) -> {
          chartOfAccountsViewAllTestUtils.assertThatAccountsExist(accountsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Accounts with filter applied on AccountCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              chartOfAccountsViewAllTestUtils.getContainsFilterField(
                  IHObChartOfAccountsGeneralModel.USER_CODE, filteringValue);
          Response response =
              chartOfAccountsViewAllTestUtils.requestToReadFilteredData(
                      cookie,
                  IChartOfAccountsRestUrls.CHART_OF_ACCOUNTS_BASE_URL,
                      pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Accounts with filter applied on AccountType which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer numberOfRecords,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              chartOfAccountsViewAllTestUtils.getContainsFilterField(
                  IHObChartOfAccountsGeneralModel.ACCOUNT_TYPE, filteringValue);
          Response response =
              chartOfAccountsViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IChartOfAccountsRestUrls.CHART_OF_ACCOUNTS_BASE_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable accountsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          chartOfAccountsViewAllTestUtils.assertThatChartOfAccountsDataMatches(
              response, accountsDataTable);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer numberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          chartOfAccountsViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
              response, numberOfRecords);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Accounts with filter applied on Level which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              chartOfAccountsViewAllTestUtils.getEqualsFilterField(
                  IHObChartOfAccountsGeneralModel.LEVEL, filteringValue);
          Response response =
              chartOfAccountsViewAllTestUtils.requestToReadFilteredData(
                      cookie,
                  IChartOfAccountsRestUrls.CHART_OF_ACCOUNTS_BASE_URL,
                      pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Accounts with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              chartOfAccountsViewAllTestUtils.getContainsFilterField(
                  IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME, filteringValue);
          Response response =
              chartOfAccountsViewAllTestUtils.requestToReadFilteredData(
                      cookie,
                  IChartOfAccountsRestUrls.CHART_OF_ACCOUNTS_BASE_URL,
                      pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Accounts with (\\d+) records per page and the following filters applied on Accounts$",
        (String userName, Integer pageNumber, Integer numberOfRecords, DataTable filters) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String fieldFilter = chartOfAccountsViewAllTestUtils.getFilters(filters);

          Response response =
              chartOfAccountsViewAllTestUtils.requestToReadFilteredData(
                      cookie,
                  IChartOfAccountsRestUrls.CHART_OF_ACCOUNTS_BASE_URL,
                      pageNumber,
                      numberOfRecords,
                      fieldFilter);
            userActionsTestUtils.setUserResponse(userName, response);
        });
      When(
              "^\"([^\"]*)\" requests to read page (\\d+) of Accounts with (\\d+) records per page while current locale is \"([^\"]*)\" and the following filters applied on Accounts$",
              (String userName,
               Integer pageNumber,
               Integer numberOfRecords,
               String locale,
               DataTable filters) -> {
                  Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                  String fieldFilter = chartOfAccountsViewAllTestUtils.getFilters(filters);
                  Response response =
                          chartOfAccountsViewAllTestUtils.requestToReadFilteredDataWithLocale(
                                  cookie,
                              IChartOfAccountsRestUrls.CHART_OF_ACCOUNTS_BASE_URL,
                                  pageNumber,
                  numberOfRecords,
                                  fieldFilter, locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "View All Chart Of Accounts")) {
          databaseConnector.createConnection();
          if (!hasBeenExecutedOnce) {
              databaseConnector.executeSQLScript(
                      chartOfAccountsViewAllTestUtils.getDbScriptsOneTimeExecution());
              hasBeenExecutedOnce = true;
          }
      }
  }

  @After
  public void after(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "View All Chart Of Accounts")) {
          databaseConnector.closeConnection();
      }
  }
}
