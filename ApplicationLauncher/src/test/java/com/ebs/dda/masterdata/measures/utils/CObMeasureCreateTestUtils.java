package com.ebs.dda.masterdata.measures.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasureCreateValueObject;
import com.ebs.dda.masterdata.measures.IMeasureRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

public class CObMeasureCreateTestUtils extends CObMeasuresCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Create]";

  public CObMeasureCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/measures/CObMeasure_Clear.sql");
    return str.toString();
  }

  public void assertThatLastCreatedCodeIsCorrect(String lastCreatedCode) {
    assertThatLastEntityCodeIs("CObMeasure", lastCreatedCode);
  }

  public Response createUnitOfMeasure(DataTable unitOfMeasureDataTable, Cookie cookie) {
    JsonObject unitOfMeasure = prepareUnitOfMeasureJsonObject(unitOfMeasureDataTable);
    String restURL = IMeasureRestUrls.CREATE;
    return sendPostRequest(cookie, restURL, unitOfMeasure);
  }

  private JsonObject prepareUnitOfMeasureJsonObject(DataTable unitOfMeasureDataTable) {
    Map<String, String> unitOfMeasureMap =
        unitOfMeasureDataTable.asMaps(String.class, String.class).get(0);
    JsonObject unitOfMeasureJsonObject = new JsonObject();

    addValueToJsonObject(
        unitOfMeasureJsonObject,
        ICObMeasureCreateValueObject.UNIT_OF_MEASURE_NAME,
        unitOfMeasureMap.get(IFeatureFileCommonKeys.NAME));

    unitOfMeasureJsonObject.addProperty(
        ICObMeasureCreateValueObject.IS_STANDARD,
        Boolean.valueOf(unitOfMeasureMap.get(IFeatureFileCommonKeys.IS_STANDARD)));

    return unitOfMeasureJsonObject;
  }

  public void assertThatUnitOfMeasureWithCodeIsCreatedSuccessfully(
      String unitOfMeasureCode, DataTable unitOfMeasureDataTable) {
    Map<String, String> unitOfMeasureData =
        unitOfMeasureDataTable.asMaps(String.class, String.class).get(0);
    Object[] unitOfMeasure =
        (Object[]) retrieveUnitOfMeasureData(unitOfMeasureCode, unitOfMeasureData);

    Assert.assertNotNull(unitOfMeasure);
    assertDateEquals(
        unitOfMeasureData.get(IFeatureFileCommonKeys.CREATION_DATE),
        new DateTime(unitOfMeasure[0]).withZone(DateTimeZone.UTC));

    assertDateEquals(
        unitOfMeasureData.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(unitOfMeasure[1]).withZone(DateTimeZone.UTC));
  }

  private Object retrieveUnitOfMeasureData(
      String unitOfMeasureCode, Map<String, String> unitOfMeasureRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedUnitOfMeasure",
        constructCreatedUnitOfMeasureQueryParams(unitOfMeasureCode, unitOfMeasureRow));
  }

  private Object[] constructCreatedUnitOfMeasureQueryParams(
      String unitOfMeasureCode, Map<String, String> unitOfMeasureRow) {
    return new Object[] {
      unitOfMeasureCode,
      '%' + unitOfMeasureRow.get(IFeatureFileCommonKeys.STATE) + '%',
      unitOfMeasureRow.get(IFeatureFileCommonKeys.CREATED_BY),
      unitOfMeasureRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      unitOfMeasureRow.get(IFeatureFileCommonKeys.NAME),
      unitOfMeasureRow.get(IFeatureFileCommonKeys.SYMBOL),
      Boolean.parseBoolean(unitOfMeasureRow.get(IFeatureFileCommonKeys.IS_STANDARD))
    };
  }
}
