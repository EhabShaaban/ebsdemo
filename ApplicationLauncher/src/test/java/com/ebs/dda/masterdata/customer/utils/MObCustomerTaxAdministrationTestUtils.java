package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.lookups.ILObTaxAdministrative;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.customer.IMObCustomerRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class MObCustomerTaxAdministrationTestUtils extends MObCustomerCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[TaxAdministration][dropDown]";

  public MObCustomerTaxAdministrationTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatTaxAdministrationExist(DataTable taxAdministrationDataTable) {
    List<Map<String, String>> taxAdministrationMapList =
        taxAdministrationDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedTaxAdministrationMap : taxAdministrationMapList) {
      Object actualTaxAdministration =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getTaxAdministration",
              constructTaxAdministrationQueryParams(expectedTaxAdministrationMap));

      Assert.assertNotNull(
          ASSERTION_MSG
              + "Tax Administration with name: "
              + expectedTaxAdministrationMap.get(IFeatureFileCommonKeys.NAME)
              + " does not exist",
          actualTaxAdministration);
    }
  }

  private Object[] constructTaxAdministrationQueryParams(
      Map<String, String> expectedTaxAdministrationMap) {
    return new Object[] {
      expectedTaxAdministrationMap.get(IFeatureFileCommonKeys.CODE),
      expectedTaxAdministrationMap.get(IFeatureFileCommonKeys.NAME)
    };
  }

  public Response readAllTaxAdministration(Cookie cookie) {
    String restURL = IMObCustomerRestURLs.READ_ALL_TAX_ADMINISTRATION;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatTaxAdministrationResponseIsCorrect(
      DataTable taxAdministrationDataTable, Response response) throws Exception {
    List<Map<String, String>> expectedTaxAdministrationMapList =
        taxAdministrationDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualTaxAdministrationList = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedTaxAdministrationMapList.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(
              expectedTaxAdministrationMapList.get(i), actualTaxAdministrationList.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IMasterDataFeatureFileCommonKeys.TAX_ADMINISTRATION,
          ILObTaxAdministrative.USER_CODE,
          ILObTaxAdministrative.NAME);
    }
  }

  public void assertThatTotalNumberOfTaxAdministrationEquals(
      Integer taxAdministrationCount, Response response) {
    List<HashMap<String, Object>> actualTaxAdministration = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of TaxAdministration is not correct",
        taxAdministrationCount.intValue(),
        actualTaxAdministration.size());
  }
}
