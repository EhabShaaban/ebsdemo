package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.IMObItemAccountingInfoValueObject;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MObItemSaveAccountingDetailsTestUtils extends IObItemAccountingDetailsCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[AccountingDetails][Save]";

  public MObItemSaveAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response saveItemAccountingDetailsSection(
      Cookie loginCookie, String itemCode, DataTable accountInfoDataTable) {
    JsonObject AccountInfoJsonData = getAccountInfoJsonObject(accountInfoDataTable);
    String restURL = IMObItemRestUrls.UPDATE_ACCOUNTING_DETAILS_SECTION + itemCode;
    return sendPUTRequest(loginCookie, restURL, AccountInfoJsonData);
  }

  public Response saveServiceItemAccountingDetailsSection(
      Cookie loginCookie, String itemCode, DataTable accountInfoDataTable) {
    JsonObject AccountInfoJsonData = getAccountInfoJsonObjectForServiceItem(accountInfoDataTable);
    String restURL = IMObItemRestUrls.UPDATE_ACCOUNTING_DETAILS_SECTION + itemCode;
    return sendPUTRequest(loginCookie, restURL, AccountInfoJsonData);
  }

  private JsonObject getAccountInfoJsonObjectForServiceItem(DataTable accountInfoDataTable) {
    Map<String, String> accountInfoMap =
        accountInfoDataTable.asMaps(String.class, String.class).get(0);
    JsonObject AccountInfoData = new JsonObject();
    AccountInfoData.addProperty(
        IMObItemAccountingInfoValueObject.ACCOUNT_CODE,
        accountInfoMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE));
    String costFactorStringValue = accountInfoMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR);
    if(costFactorStringValue.equals("true") || costFactorStringValue.equals("false")){
      boolean costFactorBooleanValue =
              Boolean.parseBoolean(accountInfoMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR));
      AccountInfoData.addProperty(IMObItemAccountingInfoValueObject.COST_FACTOR, costFactorBooleanValue);
    }else{
      AccountInfoData.addProperty(IMObItemAccountingInfoValueObject.COST_FACTOR, costFactorStringValue);
    }
    return AccountInfoData;
  }

  private JsonObject getAccountInfoJsonObject(DataTable accountInfoDataTable) {
    Map<String, String> accountInfoMap =
        accountInfoDataTable.asMaps(String.class, String.class).get(0);
    JsonObject AccountInfoData = new JsonObject();
    AccountInfoData.addProperty(
        IMObItemAccountingInfoValueObject.ACCOUNT_CODE,
        accountInfoMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE));
    return AccountInfoData;
  }

  public void assertThatItemAccountUpdatedWithGivenValues(
      String itemCode, DataTable accountInfoDataTable) {
    Map<String, String> accountInfoMap =
        accountInfoDataTable.asMaps(String.class, String.class).get(0);

    IObItemAccountingInfoGeneralModel accountingInfoGeneralModel =
        getIObAccountingInfoGeneralModel(itemCode);

    assertEquals(
        ASSERTION_MSG + " Account Code is not correct",
        accountInfoMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
        accountingInfoGeneralModel.getAccountCode());

    assertDateEquals(
            accountInfoMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            accountingInfoGeneralModel.getModifiedDate());

    assertEquals(
        ASSERTION_MSG + " Last updated by is not correct",
        accountInfoMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
        accountingInfoGeneralModel.getModificationInfo());
  }

  public void assertThatServiceItemAccountUpdatedWithGivenValues(
      String itemCode, DataTable accountInfoDataTable) {
    Map<String, String> accountInfoMap =
        accountInfoDataTable.asMaps(String.class, String.class).get(0);

    IObItemAccountingInfoGeneralModel accountingInfoGeneralModel =
        getIObAccountingInfoGeneralModel(itemCode);

    assertEquals(
        ASSERTION_MSG + " Account Code is not correct",
        accountInfoMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE),
        accountingInfoGeneralModel.getAccountCode());

    assertDateEquals(
            accountInfoMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            accountingInfoGeneralModel.getModifiedDate());

    assertEquals(
        ASSERTION_MSG + " Last updated by is not correct",
        accountInfoMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
        accountingInfoGeneralModel.getModificationInfo());
    assertEquals(
        ASSERTION_MSG + " Cost Factor is not correct",
        Boolean.parseBoolean(accountInfoMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR)),
        accountingInfoGeneralModel.getCostFactor());
  }
}
