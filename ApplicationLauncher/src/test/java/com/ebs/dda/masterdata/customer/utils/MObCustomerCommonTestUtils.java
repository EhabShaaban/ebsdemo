package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.MasterDataTestUtils;
import cucumber.api.DataTable;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class MObCustomerCommonTestUtils extends MasterDataTestUtils {

  protected String ASSERTION_MSG = "[Customer]";

  public MObCustomerCommonTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String prepareDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/customer/Customer_ViewAll_Clear.sql");
    return str.toString();
  }

  public void assertThatCustomersExist(DataTable customerDataTable) {

    List<Map<String, String>> customerList = customerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> customer : customerList) {

      String PurchaseUnits = customer.get(IFeatureFileCommonKeys.BUSINESS_UNIT);

      List<String> purchaseUnitList = Arrays.asList(PurchaseUnits.split("\\s*,\\s*"));
      for (String purchaseUnit : purchaseUnitList) {
        IObCustomerBusinessUnitGeneralModel customerBusinessUnitGeneralModel =
            (IObCustomerBusinessUnitGeneralModel)
                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getCustomerByCodeNameAndPurchaseUnit",
                    constructCustomerQueryParams(customer, purchaseUnit));

        assertNotNull(customerBusinessUnitGeneralModel);
      }
    }
  }

  private Object[] constructCustomerQueryParams(Map<String, String> customer, String purchaseUnit) {
    return new Object[] {
      customer.get(IFeatureFileCommonKeys.CODE),
      customer.get(IFeatureFileCommonKeys.NAME),
      purchaseUnit
    };
  }

  public void createCustomerGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createCustomerGeneralData",
          constructInsertCustomerGeneralDataQueryParams(generalDataMap));
    }
  }

  private Object[] constructInsertCustomerGeneralDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      generalDataMap.get(IFeatureFileCommonKeys.STATE),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.LEGAL_NAME),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.LEGAL_NAME),
      generalDataMap.get(IFeatureFileCommonKeys.TYPE),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.CREDIT_LIMIT),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.CURRENCY),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.OLD_CUSTOMER_NUMBER)
    };
  }

  public void createCustomerBusinessUnits(DataTable businessUnitsTable) {
    List<Map<String, String>> businessUnitsMaps =
        businessUnitsTable.asMaps(String.class, String.class);
    for (Map<String, String> businessUnitMap : businessUnitsMaps) {
      String code = businessUnitMap.get(IFeatureFileCommonKeys.CODE);
      String businessUnits = businessUnitMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
      insertCustomerBusinessUnits(code, businessUnits);
    }
  }

  private void insertCustomerBusinessUnits(String code, String businessUnits) {
    String[] businessUnitList = businessUnits.split(",");
    for (String businessUnit : businessUnitList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createCustomerBusinessUnit", code, businessUnit.trim());
    }
  }

  public void createCustomerAddress(DataTable customerAddressTable) {
    List<Map<String, String>> customerAddressMaps =
        customerAddressTable.asMaps(String.class, String.class);
    for (Map<String, String> customerAddressMap : customerAddressMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createCustomerAddresses",
          constructInsertCustomerAddressesDataQueryParams(customerAddressMap));
    }
  }

  private Object[] constructInsertCustomerAddressesDataQueryParams(
      Map<String, String> customerAddressMap) {
    return new Object[] {
      customerAddressMap.get(IFeatureFileCommonKeys.CODE),
      customerAddressMap.get(IMasterDataFeatureFileCommonKeys.ADDRESS_NAME),
      customerAddressMap.get(IMasterDataFeatureFileCommonKeys.ADDRESS_DESCRIPTION),
      customerAddressMap.get(IMasterDataFeatureFileCommonKeys.TYPE)
    };
  }

  public void createCustomerContactPerson(DataTable contactPersonTable) {
    List<Map<String, String>> contactPersonMaps =
            contactPersonTable.asMaps(String.class, String.class);
    for (Map<String, String> contactPersonMap : contactPersonMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
              "createCustomerContactPerson",
              constructInsertCustomercontactPersonDataQueryParams(contactPersonMap));
    }
  }

  private Object[] constructInsertCustomercontactPersonDataQueryParams(
          Map<String, String> contactPersonMap) {
    return new Object[] {
            contactPersonMap.get(IFeatureFileCommonKeys.CODE),
            contactPersonMap.get(IMasterDataFeatureFileCommonKeys.CONTACT_PERSON_NAME)
    };
  }
  public void createCustomerLegalRegistrationData(DataTable legalRegistrationDataTable) {
    List<Map<String, String>> legalRegistrationDataMaps =
        legalRegistrationDataTable.asMaps(String.class, String.class);
    for (Map<String, String> legalRegistrationDataMap : legalRegistrationDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createCustomerLegalRegistrationData",
          constructInsertCustomerLegalRegistrationDataQueryParams(legalRegistrationDataMap));
    }
  }

  private Object[] constructInsertCustomerLegalRegistrationDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(IFeatureFileCommonKeys.CODE),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.REGISTRATION_NUMBER),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.ISSUED_FROM),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.TAX_CARD_NUMBER),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.TAX_FILE_NUMBER),
      generalDataMap.get(IMasterDataFeatureFileCommonKeys.TAX_ADMINISTRATION)
    };
  }
}
