package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomerGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MObCustomerViewAllTestUtils extends MObCustomerCommonTestUtils {

  public MObCustomerViewAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String prepareDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/Customer_ViewAll_Clear.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    return str.toString();
  }

  public void assertThatCustomerDataIsCorrect(DataTable customersDataTable, Response response) {
    List<Map<String, String>> expectedCustomersList =
        customersDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualCustomersList = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        "size not equals", expectedCustomersList.size(), actualCustomersList.size());
    for (int i = 0; i < expectedCustomersList.size(); i++) {
      assertCorrectShownData(expectedCustomersList.get(i), actualCustomersList.get(i));
    }
  }

  private void assertCorrectShownData(
      Map<String, String> expectedCustomer, HashMap<String, Object> actualCustomer) {

    MapAssertion mapAssertion = new MapAssertion(expectedCustomer, actualCustomer);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IMObCustomerGeneralModel.USER_CODE);
    mapAssertion.assertLocalizedValue(
        IMasterDataFeatureFileCommonKeys.MARKET_NAME, IMObCustomerGeneralModel.MARKET_NAME);
    mapAssertion.assertLocalizedValue(
        IMasterDataFeatureFileCommonKeys.LEGAL_NAME, IMObCustomerGeneralModel.LEGAL_NAME);

    assertThatBusinessUnitCodesEquals(expectedCustomer, actualCustomer);

    mapAssertion.assertValue(IMasterDataFeatureFileCommonKeys.OLD_CUSTOMER_NUMBER, IMObCustomerGeneralModel.OLD_CUSTOMER_NUMBER);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IMObCustomerGeneralModel.CURRENT_STATES);
  }

  private void assertThatBusinessUnitCodesEquals(
      Map<String, String> expectedCustomer, HashMap<String, Object> actualCustomer) {
    String[] businessUnits = expectedCustomer.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(",");
    assertThatBusinessUnitsAreEqual(
        expectedCustomer.get(IFeatureFileCommonKeys.CODE),
        getBusinessUnitCodes(businessUnits),
        String.valueOf(actualCustomer.get(IMObCustomerGeneralModel.BUSINESS_UNIT_CODES)));
  }
}
