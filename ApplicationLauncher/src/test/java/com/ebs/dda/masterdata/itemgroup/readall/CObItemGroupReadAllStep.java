package com.ebs.dda.masterdata.itemgroup.readall;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.itemgroup.ICObItemGroupRestUrls;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class CObItemGroupReadAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
    private CObItemGroupReadAllUtil util;

    public CObItemGroupReadAllStep() {

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of item groups with (\\d+) records per page$",
        (String username, Integer pageNumber, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String itemCodeFilter = "";
          Response response =
              util.requestToReadFilteredData(
                  cookie,
                      ICObItemGroupRestUrls.ITEM_GROUP_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  itemCodeFilter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable ItemGroupData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          util.assertResponseSuccessStatus(response);
          util.assertItemGroupDataIsCorrect(ItemGroupData, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer pageSize) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          util.assertTotalNumberOfRecordsAreCorrect(response, pageSize);

        });
  }

  @Before
  public void setup() throws Exception {
    Map<String, Object> properties = getProperties();
      util = new CObItemGroupReadAllUtil(properties);
    util.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
      databaseConnector.executeSQLScript(
          util.getMasterDataDbScriptsOneTimeExecution());
      hasBeenExecutedOnce = true;
    }
  }

  @After
  public void afterEachScenario() throws SQLException {
      databaseConnector.closeConnection();
  }
}
