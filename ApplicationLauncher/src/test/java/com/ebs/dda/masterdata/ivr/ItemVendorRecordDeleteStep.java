package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;

public class ItemVendorRecordDeleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordDeleteTestUtils utils;

  public ItemVendorRecordDeleteStep() {

    When(
        "^\"([^\"]*)\" requests to delete IVR with Code \"([^\"]*)\" successfully$",
        (String username, String itemVendorCode) -> {
          Response deleteItemVendorRecordResponse =
              utils.deleteItemVendorRecord(
                  userActionsTestUtils.getUserCookie(username), itemVendorCode);
          userActionsTestUtils.setUserResponse(username, deleteItemVendorRecordResponse);
        });

    Then(
        "^IVR with Code \"([^\"]*)\" is deleted from the system$",
        (String itemVendorCode) -> {
          utils.assertThatItemVendorRecordDeleted(itemVendorCode);
        });

    When(
        "^\"([^\"]*)\" requests to delete IVR with Code \"([^\"]*)\"$",
        (String userName, String itemVendorCode) -> {
          Response response =
              utils.deleteItemVendorRecord(
                  userActionsTestUtils.getUserCookie(userName), itemVendorCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new ItemVendorRecordDeleteTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete IVR")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Delete IVR")) {
      userActionsTestUtils.unlockSectionForAllUsers(IVRRestUrls.UNLOCK_GENERAL_DATA_SECTION);
    }
  }
}
