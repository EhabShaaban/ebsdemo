package com.ebs.dda.masterdata.item.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.item.IExpectedKeys;
import com.ebs.dda.masterdata.item.IResponseKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class MObItemViewGeneralDataTestUtils extends MObItemGeneralDataTestUtils {

  private ObjectMapper mapperObj;

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[GeneralData][View]";

  public MObItemViewGeneralDataTestUtils(Map<String, Object> properties) {
    super(properties);
    mapperObj = new ObjectMapper();
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
      str.append("db-scripts/master-data/item/MasterData.sql");
      str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
      str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    return str.toString();
  }
  public void assertResponseDataIsCorrect(
      DataTable expectedItemTable, Response itemGeneralDataResponse) throws Exception {
    Map<String, Object> actualMap =
        itemGeneralDataResponse.body().jsonPath().get(CommonKeys.RESULT);
    Map<String, String> expectedMap = expectedItemTable.asMaps(String.class, String.class).get(0);
    assertExpectedDataEqualActualData(actualMap, expectedMap);
  }

  private void assertExpectedDataEqualActualData(
      Map<String, Object> actualMap, Map<String, String> expectedMap) throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedMap, actualMap);
    mapAssertion.assertLocalizedValue(IExpectedKeys.ITEM_NAME, IMObItemGeneralModel.ITEM_NAME);
    mapAssertion.assertLocalizedValue(
        IExpectedKeys.ITEM_GROUP, IMObItemGeneralModel.ITEM_GROUP_NAME);
    mapAssertion.assertLocalizedValue(IExpectedKeys.BASE_UNIT, IMObItemGeneralModel.BASIC_UOM_NAME);
    mapAssertion.assertValue(
        IExpectedKeys.OLD_ITEM_REFERENCE, IMObItemGeneralModel.OLD_ITEM_REFERENCE);
    mapAssertion.assertValue(
        IMasterDataFeatureFileCommonKeys.MARKET_NAME, IMObItemGeneralModel.MARKET_NAME);
    mapAssertion.assertValue(IFeatureFileCommonKeys.TYPE, IMObItemGeneralModel.ITEM_TYPE_CODE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PRODUCT_MANAGER,
        IMObItemGeneralModel.PRODUCT_MANAGER_CODE,
        IMObItemGeneralModel.PRODUCT_MANAGER_NAME);

    assertThatPurchaseUnitNamesAreEquals(actualMap, expectedMap);
    if (!expectedMap.get(IExpectedKeys.BATCH_MANAGED).isEmpty()) {
      assertEquals(
          ASSERTION_MSG + " Batch Managed is not Correct",
          Boolean.parseBoolean(expectedMap.get(IExpectedKeys.BATCH_MANAGED)),
          Boolean.parseBoolean(actualMap.get(IResponseKeys.BATCH_MANAGED).toString()));
    }
    DateTimeFormatter format = DateTimeFormat.forPattern(DateFormats.DATE_TIME_WITH_TIMEZONE);
    DateTime responseDate =
        DateTime.parse(actualMap.get(IResponseKeys.CREATION_DATE).toString(), format)
            .withZone(DateTimeZone.UTC);
    assertDateEquals(expectedMap.get(IExpectedKeys.CREATION_DATE), responseDate);
  }

  private void assertThatPurchaseUnitNamesAreEquals(
      Map<String, Object> actualMap, Map<String, String> expectedMap) throws Exception {

    List<String> expectedPurchaseUnits =
        convertBusinessUnitCodesStringToList(expectedMap.get(IExpectedKeys.PURCHASING_UNIT));

    List<String> actualPurchaseUnitNames =
        getActualPurchaseUnitNames(
            (List<Map<String, String>>) actualMap.get(IResponseKeys.PURCHASING_UNITS));

    assertTrue(
        ASSERTION_MSG + " Purchasing Unit Names are not Correct",
        CollectionUtils.isEqualCollection(expectedPurchaseUnits, actualPurchaseUnitNames));
  }

  private List<String> getActualPurchaseUnitNames(List<Map<String, String>> actualPurchaseUnits)
      throws Exception {
    List<String> actualPurchaseUnitNames = new ArrayList<>();

    for (Map<String, String> actualPurchaseUnitName : actualPurchaseUnits) {
      String purchaseUnitName = mapperObj.writeValueAsString(actualPurchaseUnitName.get("values"));
      actualPurchaseUnitNames.add(getEnglishLocale(purchaseUnitName));
    }
    return actualPurchaseUnitNames;
  }
}
