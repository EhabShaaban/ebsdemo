package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemReadConversionFactorTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemReadConversionFactorStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private MObItemReadConversionFactorTestUtils utils;

    public MObItemReadConversionFactorStep() {
        When(
                "^\"([^\"]*)\" requests to read BaseUnit and ConversionFactor for Item \"([^\"]*)\" with AlternativeUOM \"([^\"]*)\"$",
                (String userName, String item, String alternativeUOM) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);

                    String url =
                            IMObItemRestUrls.READ_ITEM_CONVERSION_FACTOR_URL
                                    + item.split(" - ")[0]
                                    + "/"
                                    + alternativeUOM.split(" - ")[0];
                    Response response = utils.sendGETRequest(cookie, url);

                    userActionsTestUtils.setUserResponse(userName, response);
                });
        When(
                "^\"([^\"]*)\" requests to read BaseUnit and ConversionFactor for Item \"([^\"]*)\" with BaseUnit \"([^\"]*)\"$",
                (String userName, String item, String baseUnit) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);

                    String url =
                            IMObItemRestUrls.READ_ITEM_CONVERSION_FACTOR_URL
                                    + item.split(" - ")[0]
                                    + "/"
                                    + baseUnit.split(" - ")[0];
                    Response response = utils.sendGETRequest(cookie, url);

                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following BaseUnit and ConversionFactor values will be presented to \"([^\"]*)\":$",
                (String userName, DataTable itemUOMDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertItemConversionFactorAndBaseUnitMatchExpected(response, itemUOMDataTable);
                });
        Then(
                "^total number of Item BaseUnit and ConversionFactor records returned to \"([^\"]*)\" is \"([^\"]*)\"$",
                (String userName, String recordsNumber) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatResponseHasOnlyOneRecord(response);
                });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils =
                new MObItemReadConversionFactorTestUtils(getProperties(), entityManagerDatabaseConnector);
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read Item's BaseUnit & ConversionFactor")) {
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
        }
    }
}
