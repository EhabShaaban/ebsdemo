package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.jpa.masterdata.item.IIObAlternativeUoMGeneralModel;
import com.ebs.dda.masterdata.item.IExpectedKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class MObItemReadConversionFactorTestUtils extends MObItemCommonTestUtils {

  public MObItemReadConversionFactorTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertItemConversionFactorAndBaseUnitMatchExpected(
      Response response, DataTable itemUOMDataTable) throws Exception {

    Map<String, String> expectedItemConversionFactor =
        itemUOMDataTable.asMaps(String.class, String.class).get(0);

    assertThatResponseHasOnlyOneRecord(response);
    HashMap<String, Object> actualItemConversionFactor = getMapFromResponse(response);

    assertEquals(
        expectedItemConversionFactor.get(IExpectedKeys.BASE_UNIT).split(" - ")[0],
        actualItemConversionFactor.get(IIObAlternativeUoMGeneralModel.BASIC_UOM_CODE),
        "basicUnitOfMeasureCode doesn't match expected!");
    assertEquals(
        expectedItemConversionFactor.get(IExpectedKeys.BASE_UNIT).split(" - ")[1],
        getEnglishValue(
            actualItemConversionFactor.get(IIObAlternativeUoMGeneralModel.BASIC_UOM_SYMBOL)),
        "basicUnitOfMeasureSymbol doesn't match expected!");
    assertEquals(
        new BigDecimal(expectedItemConversionFactor.get(IExpectedKeys.CONVERSION_FACTOR)),
        actualItemConversionFactor.get(IIObAlternativeUoMGeneralModel.CONVERSION_FACTOR),
        "conversionFactor doesn't match expected!");
  }

  public void assertThatResponseHasOnlyOneRecord(Response response) {
    try {
      List<HashMap<String, Object>> listOfMapsFromResponse = getListOfMapsFromResponse(response);
      assertNull(
          listOfMapsFromResponse,
          "Response returned " + listOfMapsFromResponse.size() + " alternatives!");
    } catch (Exception e) {
      assertTrue(e instanceof ClassCastException);
    }
  }
}
