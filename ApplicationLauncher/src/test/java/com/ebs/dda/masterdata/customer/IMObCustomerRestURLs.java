package com.ebs.dda.masterdata.customer;

public interface IMObCustomerRestURLs {
  String QUERY = "/services/masterdataobjects/customers";
  String COMMAND = "/command/customer";

  String VIEW_ALL = QUERY;
  String CREATE = COMMAND + "/";

  String READ_ALL_ADDRESSES = "/addresses";

  String READ_ALL_CONTACT_PERSONS = "/contactpersons";
  String READ_ALL_CUSTOMER_TYPES = QUERY + "/types";
  String READ_ALL_ISSUED_FROM = QUERY + "/issuedfrom";
  String READ_ALL_TAX_ADMINISTRATION = QUERY + "/taxadministration";
  String AUTHORIZED_ACTIONS = QUERY + "/authorizedactions/";
}
