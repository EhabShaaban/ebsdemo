package com.ebs.dda.masterdata.ivr.utils;

import io.restassured.response.Response;
import org.apache.commons.collections.CollectionUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ItemVendorRecordGeneralDataRequestEditCancelTestUtils
    extends ItemVendorRecordCommonTestUtils {

  public ItemVendorRecordGeneralDataRequestEditCancelTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getViewAllDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    return str.toString();
  }

  public void assertThatUserHasReadsAuthorization(
      List<String> expectedAuthorizedReads, Response lockResponse) {

    Map<String, List<String>> configuration = lockResponse.body().jsonPath().get("configuration");
    List<String> authorizedReads = configuration.get("authorizedReads");

    assertTrue(CollectionUtils.isEqualCollection(expectedAuthorizedReads, authorizedReads));
  }

  public List<String> convertAuthorizedReadsStringToList(String authorizedReadsString) {
    List<String> authorizedReads =
        Arrays.asList(authorizedReadsString.replaceAll("\\s", "").split(","));
    return authorizedReads;
  }
}
