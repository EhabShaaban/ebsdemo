package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.item.IExpectedKeys;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.Map;

public class MObItemSaveGeneralDataTestUtils extends MObItemGeneralDataTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[GeneralData][Save]";

  public MObItemSaveGeneralDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertDataIsUpdated(String itemCode, DataTable expectedDataTable) {
    Map<String, String> expectedDataMap =
        expectedDataTable.asMaps(String.class, String.class).get(0);

    Object actualItemRecord =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedItemGeneralData",
            constructItemGeneralDataQueryParams(itemCode, expectedDataMap));
    Assert.assertNotNull(ASSERTION_MSG + " IMR is not updated", actualItemRecord);
    assertThatPurchaseUnitsCodeAreEquals(
        expectedDataMap.get(IExpectedKeys.PURCHASING_UNIT), actualItemRecord.toString());
  }

  protected Object[] constructItemGeneralDataQueryParams(
      String itemCode, Map<String, String> expectedDataMap) {
    return new Object[] {
            itemCode,
            expectedDataMap.get(IExpectedKeys.ITEM_NAME),
            expectedDataMap.get(IExpectedKeys.ITEM_GROUP),
            expectedDataMap.get(IExpectedKeys.BASE_UNIT),
            Boolean.parseBoolean(expectedDataMap.get(IExpectedKeys.BATCH_MANAGED)),
            expectedDataMap.get(IExpectedKeys.OLD_ITEM_REFERENCE),
            expectedDataMap.get(IExpectedKeys.CREATION_DATE),
            expectedDataMap.get(IExpectedKeys.MODIFICATION_DATE),
            expectedDataMap.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
            expectedDataMap.get(IFeatureFileCommonKeys.TYPE),
            expectedDataMap.get(IFeatureFileCommonKeys.PRODUCT_MANAGER)
    };
  }
}
