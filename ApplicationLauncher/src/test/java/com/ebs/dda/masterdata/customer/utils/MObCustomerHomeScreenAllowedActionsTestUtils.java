package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import java.util.Map;

public class MObCustomerHomeScreenAllowedActionsTestUtils extends MObCustomerCommonTestUtils {

  public MObCustomerHomeScreenAllowedActionsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }
}
