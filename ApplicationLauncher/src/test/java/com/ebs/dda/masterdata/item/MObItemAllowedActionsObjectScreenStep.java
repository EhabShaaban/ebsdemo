package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemAllowedActionsObjectScreenStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemCommonTestUtils utils;

  public MObItemAllowedActionsObjectScreenStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of Item with code \"([^\"]*)\"$",
        (String userName, String itemCode) -> {
          String readUrl = IMObItemRestUrls.AUTHORIZED_ACTIONS + itemCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following actions are displayed to \"([^\"]*)\":$",
        (String userName, DataTable allowedActions) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);

          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
    When(
        "^\"([^\"]*)\" requests to read actions of Item home screen$",
        (String userName) -> {
          String readUrl = IMObItemRestUrls.AUTHORIZED_ACTIONS;
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(loginCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemCommonTestUtils(getProperties());
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read Allowed Actions for Item")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
