package com.ebs.dda.masterdata.ivr;

public interface IVRFeatureFileKeys {
  String ITEM_CODE = "ItemCode";
  String VENDOR_CODE = "VendorCode";
  String ITEM_VENDOR_CODE = "ItemCodeAtVendor";
  String CURRENCY_CODE = "CurrencyCode";
  String PRICE = "Price";
  String PURCHASING_UNIT = "PurchasingUnit";
  String UOM_CODE = "UoMCode";
  String OLD_ITEM_NUMBER = "OldItemReference";
}
