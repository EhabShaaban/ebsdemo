package com.ebs.dda.masterdata.exchangerate.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRateGeneralModel;
import com.ebs.dda.masterdata.exchangerate.ICObExchangeRateFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CObExchangeRateViewAllTestUtils extends CObExchangeRateCommonTestUtils {

  public CObExchangeRateViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",db-scripts/CObCurrencySqlScript.sql");
    str.append(",db-scripts/master-data/exchange-rate/exchange-rate-type.sql");
    str.append(",db-scripts/master-data/exchange-rate/exchange-rate-view-all.sql");
    return str.toString();
  }

  public void assertThatTheFollowingExchangeRatesExist(DataTable erDataTable) {
    List<Map<String, String>> exchangeRatesListMap = erDataTable.asMaps(String.class, String.class);
    for (Map<String, String> exchangeRateMap : exchangeRatesListMap) {
      Object[] params = constructExchangeRatesRecordQueryParams(exchangeRateMap);
      CObExchangeRateGeneralModel cObExchangeRateGeneralModel =
          (CObExchangeRateGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getExchangeRateRecord", params);
      Assert.assertNotNull(
          "Exchange Rate doesn't exist, given " + Arrays.toString(params),
          cObExchangeRateGeneralModel);
    }
  }

  private Object[] constructExchangeRatesRecordQueryParams(Map<String, String> exchangeRateMap) {
    return new Object[] {
      exchangeRateMap.get(IFeatureFileCommonKeys.TYPE),
      exchangeRateMap.get(IFeatureFileCommonKeys.CODE),
      exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.FIRST_CURRENCY),
      exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_CURRENCY),
      Double.parseDouble(exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE)),
      exchangeRateMap.get(ICObExchangeRateFeatureFileCommonKeys.VALID_FROM_DATE),
      exchangeRateMap.get(IFeatureFileCommonKeys.CREATED_BY)
    };
  }

  public void assertThatExchangeRateDataMatches(Response response, DataTable exchangeRateDataTable)
      throws Exception {
    List<Map<String, String>> expectedExchangeRateData =
        exchangeRateDataTable.asMaps(String.class, String.class);

    assertResponseSuccessStatus(response);
    List<HashMap<String, Object>> actualExchangeRateData = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedExchangeRateData.size(); i++) {
      assertThatExpectedExchangeRateEqualsActualExchangeRate(
          expectedExchangeRateData.get(i), actualExchangeRateData.get(i));
    }
  }

  private void assertThatExpectedExchangeRateEqualsActualExchangeRate(
      Map<String, String> expectedExchangeRateData, HashMap<String, Object> actualExchangeRateData)
          {
      MapAssertion mapAssertion = new MapAssertion(expectedExchangeRateData, actualExchangeRateData);
      mapAssertion.assertValue(ICObExchangeRateFeatureFileCommonKeys.EXCHANGE_RATE_CODE, ICObExchangeRateGeneralModel.EXCHANGE_RATE_CODE);
      mapAssertion.assertConcatenatingValue(ICObExchangeRateFeatureFileCommonKeys.FIRST_CURRENCY,
              ICObExchangeRateGeneralModel.FIRST_CURRENCY_CODE,
              ICObExchangeRateGeneralModel.FIRST_CURRENCY_ISO,
              " - "
      );
      mapAssertion.assertConcatenatingValue(ICObExchangeRateFeatureFileCommonKeys.SECOND_CURRENCY,
              ICObExchangeRateGeneralModel.SECOND_CURRENCY_CODE,
              ICObExchangeRateGeneralModel.SECOND_CURRENCY_ISO,
              " - "
      );

      mapAssertion.assertBigDecimalNumberValue(ICObExchangeRateFeatureFileCommonKeys.SECOND_VALUE,
              ICObExchangeRateGeneralModel.SECOND_VALUE);
      mapAssertion.assertDateTime(ICObExchangeRateFeatureFileCommonKeys.VALID_FROM_DATE,
              ICObExchangeRateGeneralModel.VALID_FROM
      );
      mapAssertion.assertValue(IFeatureFileCommonKeys.CREATED_BY, ICObExchangeRateGeneralModel.CREATED_BY);
      mapAssertion.assertLocalizedValue(IFeatureFileCommonKeys.TYPE, ICObExchangeRateGeneralModel.EXCHANGE_RATE_TYPE_NAME);
  }

}
