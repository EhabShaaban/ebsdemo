package com.ebs.dda.masterdata.vendor.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/vendor/Vendor_ViewAll.feature",
      "classpath:features/masterdata/vendor/Vendor_Dropdown.feature",
      "classpath:features/masterdata/vendor/VendorTypes_Dropdown.feature",
      "classpath:features/masterdata/vendor/Vendor_NextPrev.feature",
      "classpath:features/masterdata/vendor/Vendor_Delete.feature",
      "classpath:features/masterdata/vendor/Vendor_AllowedActions.feature",
      "classpath:features/masterdata/vendor/Vendor_View_GeneralData.feature",
      "classpath:features/masterdata/vendor/Vendor_RequestEdit_GeneralData.feature",
      "classpath:features/masterdata/vendor/Vendor_Save_GeneralData_HP.feature",
      "classpath:features/masterdata/vendor/Vendor_Save_GeneralData_Val.feature",
      "classpath:features/masterdata/vendor/Vendor_Save_GeneralData_Auth.feature",
      "classpath:features/masterdata/vendor/Vendor_View_AccountingData.feature",
      "classpath:features/masterdata/vendor/Vendor_Update_AccountingData.feature",
      "classpath:features/masterdata/vendor/Vendor_Create_HP.feature",
      "classpath:features/masterdata/vendor/Vendor_Create_Val.feature",
      "classpath:features/masterdata/vendor/Vendor_Create_Auth.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.vendor"},
    strict = true,
    tags = "not @Future")
public class MObVendorCucumberRunner {}
