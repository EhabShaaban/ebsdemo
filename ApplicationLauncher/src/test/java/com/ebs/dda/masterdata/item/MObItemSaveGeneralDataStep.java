package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralDataValueObject;
import com.ebs.dda.masterdata.item.utils.MObItemSaveGeneralDataTestUtils;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordCommonTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;

public class MObItemSaveGeneralDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MObItemSaveGeneralDataTestUtils utils;
  private ItemVendorRecordCommonTestUtils ivrUtils;

  public MObItemSaveGeneralDataStep() {

    Given(
        "^the following IVR exist:$",
        (DataTable itemVendorRecordsList) -> {
          ivrUtils.assertThatItemVendorRecordsExistsByVendorAndItemCodes(itemVendorRecordsList);
        });

    When(
        "^\"([^\"]*)\" saves GeneralData section of Item with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String username, String itemCode, String dateTime, DataTable dataTable) -> {
          utils.freeze(dateTime);
          JsonObject itemJsonObject = utils.createItemGeneralDataJsonObject(itemCode, dataTable);
          Response response =
              utils.saveItemGeneralDataSection(
                  userActionsTestUtils.getUserCookie(username), itemJsonObject, itemCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^GeneralData Section of Item with Code \"([^\"]*)\" is updated as follows:$",
        (String itemCode, DataTable dataTable) -> {
          utils.assertDataIsUpdated(itemCode, dataTable);
        });

    Then(
        "^the following error message is attached to field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String messageCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IMObItemGeneralDataValueObject.PURCHASE_UNIT_FIELD);
        });

    When(
        "^\"([^\"]*)\" saves GeneralData section of Item with Code \"([^\"]*)\" with the following values:$",
        (String username, String itemCode, DataTable dataTable) -> {
          JsonObject itemJsonObject = utils.createItemGeneralDataJsonObject(itemCode, dataTable);
          Response response =
              utils.saveItemGeneralDataSection(
                  userActionsTestUtils.getUserCookie(username), itemJsonObject, itemCode);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemSaveGeneralDataTestUtils(getProperties());
    ivrUtils = new ItemVendorRecordCommonTestUtils(getProperties());
    ivrUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save GeneralData section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getMasterDataDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Save GeneralData section")) {
      userActionsTestUtils.unlockSectionForAllUsers(IMObItemRestUrls.UNLOCK_GENERAL_DATA_SECTION);
      utils.unfreeze();
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save GeneralData section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
