package com.ebs.dda.masterdata.item.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/item/Item_AllowedActions.feature",
      "classpath:features/masterdata/item/Item_Save_AccountingDetails_Val_Auth.feature",
      "classpath:features/masterdata/item/Item_Dropdown.feature",
      "classpath:features/masterdata/item/Item_Save_AccountingDetails_HP.feature",
      "classpath:features/masterdata/item/Item_View_AccountingData.feature",
      "classpath:features/masterdata/item/Item_RequestEdit_AccountingDetails.feature",
      "classpath:features/masterdata/item/Item_Create_HP.feature",
      "classpath:features/masterdata/item/Item_Create_Val.feature",
      "classpath:features/masterdata/item/Item_Create_Auth.feature",
      "classpath:features/masterdata/item/Item_Delete.feature",
      "classpath:features/masterdata/item/Item_Types_Dropdown.feature",
      "classpath:features/masterdata/item/Item_Dropdown_UOM_.feature",
      "classpath:features/masterdata/item/Item_ViewAll.feature",
      "classpath:features/masterdata/item/Item_View_GeneralData.feature",
      "classpath:features/masterdata/item/Item_RequestEdit_GeneralData.feature",
      "classpath:features/masterdata/item/Item_Save_GeneralData_HP.feature",
      "classpath:features/masterdata/item/Item_Save_GeneralData_Val.feature",
      "classpath:features/masterdata/item/Item_Save_GeneralData_Auth.feature",
      "classpath:features/masterdata/item/Item_NextPrev.feature",
      "classpath:features/masterdata/item/Item_View_UOM.feature",
      "classpath:features/masterdata/item/Item_RequestEdit_UoM.feature",
      "classpath:features/masterdata/item/Item_Save_UOM_HP.feature",
      "classpath:features/masterdata/item/Item_Save_UOM_Val.feature",
      "classpath:features/masterdata/item/Item_Save_UOM_Auth.feature",
      "classpath:features/masterdata/item/Item_ConversionFactor_Dropdown.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.item"},
    strict = true,
    tags = "not @Future")
public class MObItemCucumberRunner {}
