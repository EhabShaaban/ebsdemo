package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordViewAllTestUtils;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordViewGeneralDataTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class ItemVendorRecordViewGeneralDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordViewGeneralDataTestUtils utils;
  private ItemVendorRecordViewAllTestUtils itemVendorRecordViewAllTestUtils;
  private MObVendorCommonTestUtils vendorCommonTestUtils;

  public ItemVendorRecordViewGeneralDataStep() {

    Given(
        "^the following Vendors exist:$",
        (DataTable vendorDataTable) -> {
          vendorCommonTestUtils.assertThatVendorsExistByCodeAndNameAndPurchaseUnits(
              vendorDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of IVR with code \"([^\"]*)\"$",
        (String userName, String ivrCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response itemGeneralDataResponse =
              utils.readItemVendorRecordGeneralData(userCookie, ivrCode);
          userActionsTestUtils.setUserResponse(userName, itemGeneralDataResponse);
        });

    Then(
        "^the following values of GeneralData section of IVR with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String ivrCode, String userName, DataTable ivrDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatResponseDataIsCorrect(ivrDataTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new ItemVendorRecordViewGeneralDataTestUtils(properties);
    vendorCommonTestUtils = new MObVendorCommonTestUtils(properties);
    itemVendorRecordViewAllTestUtils = new ItemVendorRecordViewAllTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    itemVendorRecordViewAllTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "IVR View GeneralData Section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(itemVendorRecordViewAllTestUtils.getDbScriptsPath());
    }
  }
}
