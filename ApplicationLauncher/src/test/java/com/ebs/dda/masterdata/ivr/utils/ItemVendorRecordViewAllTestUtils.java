package com.ebs.dda.masterdata.ivr.utils;

import java.util.Map;

public class ItemVendorRecordViewAllTestUtils extends ItemVendorRecordCommonTestUtils {

  private String requestLocale = "en";

  public ItemVendorRecordViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    return str.toString();
  }

  public void setRequestLocale(String requestLocale) {
    this.requestLocale = requestLocale;
  }

  public String getRequestLocale() {
    return requestLocale;
  }
}
