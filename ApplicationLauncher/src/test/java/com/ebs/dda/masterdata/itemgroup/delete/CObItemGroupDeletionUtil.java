package com.ebs.dda.masterdata.itemgroup.delete;

import com.ebs.dda.masterdata.itemgroup.CObItemGroupFeatureTestUtils;
import com.ebs.dda.masterdata.itemgroup.ICObItemGroupRestUrls;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class CObItemGroupDeletionUtil extends CObItemGroupFeatureTestUtils {

    ICObItemGroupRestUrls ICObItemGroupRestUrls;

    public CObItemGroupDeletionUtil(Map<String, Object> properties) {
        super(properties);
    }

    public Response deleteItemGroupByCode(Cookie userCookie, String itemCode) {
        String deleteURL = ICObItemGroupRestUrls.DELETE_ITEM_URL + itemCode;
        return sendDeleteRequest(userCookie, deleteURL);
    }
}
