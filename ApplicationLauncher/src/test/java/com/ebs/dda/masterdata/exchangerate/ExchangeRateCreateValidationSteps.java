package com.ebs.dda.masterdata.exchangerate;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCreateValidationTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class ExchangeRateCreateValidationSteps extends SpringBootRunner implements En {

  private boolean hasBeenExecutedOnce = false;
  private CObExchangeRateCreateValidationTestUtils utils;

  public ExchangeRateCreateValidationSteps() {
    Given("^the following Exchange Rates data exist:$", (DataTable erDataTable) -> {
      utils.assertThatTheFollowingExchangeRatesExist(erDataTable);
    });

    When("^\"([^\"]*)\" creates ExchangeRate with following values at \"([^\"]*)\":$",
        (String userName, String date, DataTable exchangeRateData) -> {
          utils.freezeDate(date);
          Response response = utils.createExchangeRate(exchangeRateData,
              userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^the last created ExchangeRate with code \"([^\"]*)\" with currency \"([^\"]*)\" is valid from \"([^\"]*)\"$",
        (String erCode, String currencyCode, String validFromDate) -> {
          utils.assertThatExchangeRateExists(new Object[] {erCode, currencyCode, validFromDate});
        });
    When("^\"([^\"]*)\" requests to create ExchangeRate$", (String userName) -> {
      Response response =
          utils.requestCreateExchangeRate(userActionsTestUtils.getUserCookie(userName));
      userActionsTestUtils.setUserResponse(userName, response);
    });
  }

  @Before(order = 2)
  public void setup(Scenario scenario) throws Exception {
    utils = new CObExchangeRateCreateValidationTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Val Create Exchange Rate with")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            CObExchangeRateCreateValidationTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(CObExchangeRateCreateValidationTestUtils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Val Create Exchange Rate with")) {
      databaseConnector.closeConnection();
      utils.unfreeze();
    }
  }
}
