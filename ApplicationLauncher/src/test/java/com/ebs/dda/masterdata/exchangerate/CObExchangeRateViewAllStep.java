package com.ebs.dda.masterdata.exchangerate;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRateGeneralModel;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class CObExchangeRateViewAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private CObExchangeRateViewAllTestUtils exchangeRateViewAllTestUtils;

  public CObExchangeRateViewAllStep() {

    Given("^the following ExchangeRates exist:$", (DataTable erDataTable) -> {
      exchangeRateViewAllTestUtils.assertThatTheFollowingExchangeRatesExist(erDataTable);
    });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of ExchangeRates with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = exchangeRateViewAllTestUtils.requestToReadFilteredData(userCookie,
              ICObExchangeRateRestURLs.VIEW_ALL_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then("^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable exchangeRateDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          exchangeRateViewAllTestUtils.assertThatExchangeRateDataMatches(response,
              exchangeRateDataTable);
        });

    Then("^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          exchangeRateViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(response,
              totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of ExchangeRates with filter applied on FirstCurrency which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString = exchangeRateViewAllTestUtils.getContainsFilterField(
              ICObExchangeRateGeneralModel.FIRST_CURRENCY_ISO, filteringValue);
          Response response = exchangeRateViewAllTestUtils.requestToReadFilteredData(cookie,
              ICObExchangeRateRestURLs.VIEW_ALL_URL, pageNumber, numberOfRecords, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of ExchangeRates with filter applied on SecondCurrency which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString = exchangeRateViewAllTestUtils.getContainsFilterField(
              ICObExchangeRateGeneralModel.SECOND_CURRENCY_ISO, filteringValue);
          Response response = exchangeRateViewAllTestUtils.requestToReadFilteredData(cookie,
              ICObExchangeRateRestURLs.VIEW_ALL_URL, pageNumber, numberOfRecords, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of ExchangeRates with filter applied on SecondValue which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString = exchangeRateViewAllTestUtils
              .getEqualsFilterField(ICObExchangeRateGeneralModel.SECOND_VALUE, filteringValue);
          Response response = exchangeRateViewAllTestUtils.requestToReadFilteredData(cookie,
              ICObExchangeRateRestURLs.VIEW_ALL_URL, pageNumber, numberOfRecords, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of ExchangeRates with filter applied on ValidFrom which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString = exchangeRateViewAllTestUtils
              .getContainsFilterField(ICObExchangeRateGeneralModel.VALID_FROM, String.valueOf(
                  exchangeRateViewAllTestUtils.getDateTimeInMilliSecondsFormat(filteringValue)));
          Response response = exchangeRateViewAllTestUtils.requestToReadFilteredData(cookie,
              ICObExchangeRateRestURLs.VIEW_ALL_URL, pageNumber, numberOfRecords, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of ExchangeRates with filter applied on CreatedBy which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString = exchangeRateViewAllTestUtils
              .getContainsFilterField(ICObExchangeRateGeneralModel.CREATED_BY, filteringValue);
          Response response = exchangeRateViewAllTestUtils.requestToReadFilteredData(cookie,
              ICObExchangeRateRestURLs.VIEW_ALL_URL, pageNumber, numberOfRecords, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of ExchangeRates with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString = exchangeRateViewAllTestUtils.getEqualsFilterField(
              ICObExchangeRateGeneralModel.EXCHANGE_RATE_TYPE, filteringValue);
          Response response = exchangeRateViewAllTestUtils.requestToReadFilteredData(
              cookie, ICObExchangeRateRestURLs.VIEW_ALL_URL, pageNumber, numberOfRecords,
              filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    exchangeRateViewAllTestUtils = new CObExchangeRateViewAllTestUtils(getProperties());
    exchangeRateViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "View all ExchangeRates")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector
            .executeSQLScript(CObExchangeRateViewAllTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all ExchangeRates")) {
      databaseConnector.closeConnection();
    }
  }
}
