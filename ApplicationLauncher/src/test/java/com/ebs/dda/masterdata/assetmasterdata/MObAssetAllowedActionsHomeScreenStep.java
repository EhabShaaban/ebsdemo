package com.ebs.dda.masterdata.assetmasterdata;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.assetmasterdata.utils.MObAssetAllowedActionsHomeScreenTestUtil;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class MObAssetAllowedActionsHomeScreenStep extends SpringBootRunner implements En {

  private MObAssetAllowedActionsHomeScreenTestUtil utils;

  public MObAssetAllowedActionsHomeScreenStep() {
    When(
        "^\"([^\"]*)\" requests to read actions of AssetMasterData home screen$",
        (String userName) -> {
          String url = IMObAssetRestURLs.AUTHORIZED_ACTIONS;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new MObAssetAllowedActionsHomeScreenTestUtil(
            getProperties(), entityManagerDatabaseConnector);
  }
}
