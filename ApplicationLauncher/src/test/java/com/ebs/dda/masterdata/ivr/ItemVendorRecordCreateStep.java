package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.jpa.masterdata.ivr.IItemVendorRecordValueObject;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordCreateTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class ItemVendorRecordCreateStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordCreateTestUtils utils;

  public ItemVendorRecordCreateStep() {

    Given(
        "^the Item with code \"([^\"]*)\" has the following Base UOM:$",
        (String itemCode, DataTable dataTable) -> {
          utils.assertBasicUnitOfMeasureExistsInItem(itemCode, dataTable);
        });

    Given(
        "^the Item with code \"([^\"]*)\" has the following Alternate UoMs:$",
        (String itemCode, DataTable dataTable) -> {
          utils.assertUnitOfMeasureExistsInItem(itemCode, dataTable);
        });

    Given(
        "^Last created ItemVendorRecord was with code \"([^\"]*)\"$",
        (String ivrCode) -> {
          utils.assertThatLastEntityCodeIs(ItemVendorRecord.class.getSimpleName(),ivrCode);
        });

    Given(
        "^\"([^\"]*)\" creates IVR with the following values at \"([^\"]*)\":$",
        (String userName, String currentDateTime, DataTable requestData) -> {
          utils.freeze(currentDateTime);
          JsonObject requestedData = utils.prepareIVRJsonObject(requestData);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendPostRequest(userCookie, IVRRestUrls.CREATE_URL, requestedData);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new IVR is created with the following values:$",
        (DataTable itemVendorRecordData) -> {
          utils.assertThatItemVendorRecordExists(itemVendorRecordData);
        });

    Given(
        "^the following IVRs exist:$",
        (DataTable itemVendorRecordsDataTable) -> {
          utils.assertThatItemVendorRecordsExists(itemVendorRecordsDataTable);
        });

    When(
        "^\"([^\"]*)\" creates IVR with the following values:$",
        (String userName, DataTable requestData) -> {
          JsonObject requestedData = utils.prepareIVRJsonObject(requestData);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendPostRequest(userCookie, IVRRestUrls.CREATE_URL, requestedData);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following error message \"([^\"]*)\" is returned and sent to \"([^\"]*)\"$",
        (String messageCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IItemVendorRecordValueObject.IVR);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new ItemVendorRecordCreateTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create IVR")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create IVR")) {
      utils.unfreeze();
    }
  }
}
