package com.ebs.dda.masterdata.ivr.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoMGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.masterdata.ivr.IVRFeatureFileKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemVendorRecordCreateTestUtils extends ItemVendorRecordCommonTestUtils {

  public static final String ITEM_CODE = "itemCode";
  public static final String VENDOR_CODE = "vendorCode";
  public static final String ITEM_VENDOR_CODE = "itemVendorCode";
  public static final String CURRENCY_CODE = "currencyCode";
  public static final String PURCHASE_UNITS = "purchaseUnitCode";

  public static final String UNITOFMEASURE = "unitOfMeasureCode";
  public static final String OLD_ITEM_NUMBER = "oldItemNumber";

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Create]";

  public ItemVendorRecordCreateTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    return str.toString();
  }

  public void assertThatItemVendorRecordExists(DataTable itemVendorRecordData) throws Exception {
    Map<String, String> itemVendorRecordMap =
        itemVendorRecordData.asMaps(String.class, String.class).get(0);
    ItemVendorRecord itemVendorRecord =
        (ItemVendorRecord)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCreatedIVR",
                itemVendorRecordMap.get(IFeatureFileCommonKeys.CREATED_BY),
                itemVendorRecordMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                itemVendorRecordMap.get(IFeatureFileCommonKeys.IVR_CODE),
                itemVendorRecordMap.get(IVRFeatureFileKeys.ITEM_CODE),
                itemVendorRecordMap.get(IVRFeatureFileKeys.VENDOR_CODE),
                itemVendorRecordMap.get(IVRFeatureFileKeys.PURCHASING_UNIT),
                itemVendorRecordMap.get(IVRFeatureFileKeys.UOM_CODE),
                itemVendorRecordMap.get(IVRFeatureFileKeys.OLD_ITEM_NUMBER));
    assertNotNull(ASSERTION_MSG + " Record is not created", itemVendorRecord);
    assertPriceValueIsEqualToDB(itemVendorRecordMap, itemVendorRecord);
    assertEquals(
        itemVendorRecordMap.get(IVRFeatureFileKeys.ITEM_VENDOR_CODE),
        itemVendorRecord.getItemVendorCode(),
        ASSERTION_MSG + " Created IVR ItemVendorCode is not correct");
  }

  private void assertPriceValueIsEqualToDB(
      Map<String, String> itemVendorRecordMap, ItemVendorRecord itemVendorRecord) {
    String priceValue = itemVendorRecordMap.get(IVRFeatureFileKeys.PRICE);
    if (!priceValue.isEmpty()) {
      assertEquals(
          priceValue,
          itemVendorRecord.getPrice().toString(),
          ASSERTION_MSG + " Created IVR Price is not correct");
    } else {
      assertNull(ASSERTION_MSG + " Created IVR Price is not null", itemVendorRecord.getPrice());
    }
  }

  public JsonObject prepareIVRJsonObject(DataTable itemVendorRecordData) {
    Map<String, String> itemVendorRecord =
        itemVendorRecordData.asMaps(String.class, String.class).get(0);
    JsonObject parsedData = new JsonObject();
    parsedData.addProperty(ITEM_CODE, itemVendorRecord.get(IVRFeatureFileKeys.ITEM_CODE));
    parsedData.addProperty(VENDOR_CODE, itemVendorRecord.get(IVRFeatureFileKeys.VENDOR_CODE));
    parsedData.addProperty(
        ITEM_VENDOR_CODE, itemVendorRecord.get(IVRFeatureFileKeys.ITEM_VENDOR_CODE));
    addPriceOptionalFieldToJson(parsedData, itemVendorRecord);
    addCurrencyCodeToJsonObject(parsedData, itemVendorRecord);
    parsedData.addProperty(
        PURCHASE_UNITS, itemVendorRecord.get(IVRFeatureFileKeys.PURCHASING_UNIT));
    parsedData.addProperty(UNITOFMEASURE, itemVendorRecord.get(IVRFeatureFileKeys.UOM_CODE));
    parsedData.addProperty(
        OLD_ITEM_NUMBER, itemVendorRecord.get(IVRFeatureFileKeys.OLD_ITEM_NUMBER));
    return parsedData;
  }

  private void addCurrencyCodeToJsonObject(
      JsonObject parsedData, Map<String, String> itemVendorRecord) {
    String currencyCode = itemVendorRecord.get(IVRFeatureFileKeys.CURRENCY_CODE);
    if (!currencyCode.isEmpty()) {
      parsedData.addProperty(CURRENCY_CODE, itemVendorRecord.get(IVRFeatureFileKeys.CURRENCY_CODE));
    }
  }

  public void assertBasicUnitOfMeasureExistsInItem(String itemCode, DataTable measureData) {
    Map<String, String> measure = measureData.asMaps(String.class, String.class).get(0);
    String measureCode = measure.get(IFeatureFileCommonKeys.CODE);
    String measureSymbol = measure.get(IFeatureFileCommonKeys.SYMBOL);
    MObItemGeneralModel actualItem =
        (MObItemGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getBasicUnitOfMeasuresInItemByCodeAndSymbol", itemCode, measureCode);
    CObMeasure actualMeasure =
        (CObMeasure)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getMeasureByCodeAndSymbol", measureCode, measureSymbol);
    assertNotNull(actualItem);
    assertNotNull(actualMeasure);
  }

  public void assertUnitOfMeasureExistsInItem(String itemCode, DataTable measuresData) {
    List<Map<String, String>> measures = measuresData.asMaps(String.class, String.class);
    for (Map<String, String> measure : measures) {
      String measureCode = measure.get(IFeatureFileCommonKeys.CODE);
      String measureSymbol = measure.get(IFeatureFileCommonKeys.SYMBOL);

      IObAlternativeUoMGeneralModel actualMeasure =
          (IObAlternativeUoMGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getUnitOfMeasuresInItemByCodeAndSymbol", itemCode, measureCode, measureSymbol);

      assertNotNull(actualMeasure);
    }
  }
}
