package com.ebs.dda.masterdata.customer.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.customer.IIObCustomerContactPersonGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.customer.IMObCustomerRestURLs;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class IObCustomerContactPersonReadAllTestUtils extends MObCustomerCommonTestUtils {

  public IObCustomerContactPersonReadAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String prepareDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    return str.toString();
  }

  public void assertThatTheFollowingCustomersHaveTheFollowingContactPersons(
      DataTable customerContactPersonDataTable) {
    List<Map<String, String>> customerContactPersonsList =
        customerContactPersonDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedCustomerContactPerson : customerContactPersonsList) {

      Object actualCustomerContactPerson =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCustomerContactPerson",
              constructCustomerContactPersonQueryParams(expectedCustomerContactPerson));
      Assert.assertNotNull(actualCustomerContactPerson);
    }
  }

  private Object[] constructCustomerContactPersonQueryParams(
      Map<String, String> customerContactPersonMap) {
    return new Object[] {
      customerContactPersonMap.get(IFeatureFileCommonKeys.CUSTOMER),
      Integer.parseInt(
          customerContactPersonMap.get(
              IMasterDataFeatureFileCommonKeys.CUSTOMER_CONTACT_PERSON_ID)),
      customerContactPersonMap.get(ISalesOrderFeatureFileCommonKeys.CONTACT_PERSON)
    };
  }

  public String getCustomerContatPersonUrl(String customerCode) {
    return IMObCustomerRestURLs.QUERY
        + "/"
        + customerCode
        + IMObCustomerRestURLs.READ_ALL_CONTACT_PERSONS;
  }

  public void assertThatResponseMatchesExpectedContactPerson(
      DataTable customerContactPersonDataTable, Response response) {
    List<Map<String, String>> contactPersonExpectedList =
        customerContactPersonDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> contactPersonActualList = getListOfMapsFromResponse(response);

    for (int i = 0; i < contactPersonActualList.size(); i++) {
      assertThatExpectedContactPersonMatchesActual(
          contactPersonExpectedList.get(i), contactPersonActualList.get(i));
    }
  }

  private void assertThatExpectedContactPersonMatchesActual(
      Map<String, String> expectedContactPerson, HashMap<String, Object> actualContactPerson) {
    MapAssertion mapAssertion = new MapAssertion(expectedContactPerson, actualContactPerson);
    mapAssertion.assertLocalizedValue(
        ISalesOrderFeatureFileCommonKeys.CONTACT_PERSON,
        IIObCustomerContactPersonGeneralModel.CONTACT_PERSON_NAME);
  }

  public void assertThatCustomerContactPersonResponseCountIsCorrect(
      Response response, int customerContactPersonCount) {
    List<HashMap<String, Object>> actualContactPersons = getListOfMapsFromResponse(response);
    Assert.assertEquals(customerContactPersonCount, actualContactPersons.size());
  }
}
