package com.ebs.dda.masterdata.assetmasterdata;

public interface IMObAssetRestURLs {
  String QUERY = "/services/masterdataobjects/asset";
  String AUTHORIZED_ACTIONS = QUERY + "/authorizedactions";
  String VIEW_ALL = QUERY;

  String READ_ALL_ASSET_TYPES = QUERY + "/assettypes";

}
