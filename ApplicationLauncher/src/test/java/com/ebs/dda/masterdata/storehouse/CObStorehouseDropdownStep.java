package com.ebs.dda.masterdata.storehouse;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.storehouse.utils.CObStorehouseDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class CObStorehouseDropdownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private CObStorehouseDropdownTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new CObStorehouseDropdownTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  public CObStorehouseDropdownStep() {
    Given(
        "^the following Plants exist for Company \"([^\"]*)\":$",
        (String companyCodeName, DataTable plantsDataTable) -> {
          String companyCode = utils.getCodeFromCodeName(companyCodeName);
          utils.assertThatCompanyHasTheFollowingPlants(companyCode, plantsDataTable);
        });

    Given(
        "^the following Storehouses exist for Plant \"([^\"]*)\":$",
        (String storehouseCodeName, DataTable storehousesDataTable) -> {
          String storehouseCode = utils.getCodeFromCodeName(storehouseCodeName);
          utils.assertThatPlantHasTheFollowingStorehouses(storehouseCode, storehousesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read all Storehouses for Company \"([^\"]*)\"$",
        (String userName, String companyCodeName) -> {
          String companyCode = utils.getCodeFromCodeName(companyCodeName);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getStorehousesByCompanyCodeUrl(companyCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read all Storehouses for Plant \"([^\"]*)\"$",
        (String userName, String companyCodeName) -> {
          String plantCode = utils.getCodeFromCodeName(companyCodeName);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getStorehousesByPlantCodeUrl(plantCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Storehouses values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable plantsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatStorehousesResponseIsCorrect(response, plantsDataTable);
        });

    Then(
        "^total number of Storehouses returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer storehousesNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatStorehousesResponseCountIsCorrect(response, storehousesNumber);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of Storehouses dropdown")) {
      beforeSetup();
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of Storehouses dropdown")) {
      afterSetup();
    }
  }

  private void beforeSetup() throws Exception {
    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      hasBeenExecutedOnce = true;
    }
  }

  private void afterSetup() throws SQLException {
    databaseConnector.closeConnection();
  }
}
