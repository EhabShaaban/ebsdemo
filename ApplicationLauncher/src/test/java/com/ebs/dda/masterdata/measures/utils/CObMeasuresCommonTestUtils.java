package com.ebs.dda.masterdata.measures.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class CObMeasuresCommonTestUtils extends CommonTestUtils {
  protected final String ASSERTION_MSG = "[UOM]";

  public CObMeasuresCommonTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public CObMeasuresCommonTestUtils() {
  }

  public void assertMeasureExistsByCodeAndSymbolAndName(DataTable measuresData) {
    List<Map<String, String>> measures = measuresData.asMaps(String.class, String.class);
    for (Map<String, String> measure : measures) {
      String measureCode = measure.get(IFeatureFileCommonKeys.CODE);
      String measureSymbol = measure.get(IFeatureFileCommonKeys.SYMBOL);
      String measureName = measure.get(IFeatureFileCommonKeys.NAME);

      CObMeasure actualMeasure =
          (CObMeasure)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getMeasureByCodeAndSymbolAndName", measureCode, measureSymbol, measureName);

      assertNotNull(actualMeasure);
    }
  }
}
