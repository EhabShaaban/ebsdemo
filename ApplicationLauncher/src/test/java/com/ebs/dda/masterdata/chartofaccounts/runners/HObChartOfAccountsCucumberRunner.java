package com.ebs.dda.masterdata.chartofaccounts.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/masterdata/chart-of-accounts/COA_Create_HP.feature",
      "classpath:features/masterdata/chart-of-accounts/COA_Create_Auth.feature",
      "classpath:features/masterdata/chart-of-accounts/COA_Create_Val.feature",
      "classpath:features/masterdata/chart-of-accounts/COA_AllowedActions.feature",
      "classpath:features/masterdata/chart-of-accounts/COA_AllowedActions_OS.feature",
      "classpath:features/masterdata/chart-of-accounts/COA_ViewAll.feature",
      "classpath:features/masterdata/chart-of-accounts/COA_Dropdowns.feature",
      "classpath:features/masterdata/chart-of-accounts/COA_Delete.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.chartofaccounts"},
    strict = true,
    tags = "not @Future")
public class HObChartOfAccountsCucumberRunner {}
