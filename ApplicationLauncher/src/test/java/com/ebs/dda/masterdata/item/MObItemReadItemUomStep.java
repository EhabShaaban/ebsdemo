package com.ebs.dda.masterdata.item;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.item.utils.MObItemReadItemUomTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class MObItemReadItemUomStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private MObItemReadItemUomTestUtils utils;

  public MObItemReadItemUomStep() {
    When(
        "^\"([^\"]*)\" requests to read UOMs for Item \"([^\"]*)\"$",
        (String userName, String item) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(
                  cookie, IMObItemRestUrls.READ_ITEMS_UOM_URL + item.split(" - ")[0]);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following UOM values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable itemUOMDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemUomsAreReturned(response, itemUOMDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MObItemReadItemUomTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read Item's UOMs dropdown")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }
}
