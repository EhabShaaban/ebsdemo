package com.ebs.dda.masterdata.ivr;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordReadNextPrevTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;

public class ItemVendorRecordReadNextPrevStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private ItemVendorRecordReadNextPrevTestUtils ivrReadNextPrevTestUtils;

  public ItemVendorRecordReadNextPrevStep() {

    Given(
        "^the following Items with the following order exist:$",
        (DataTable itemDataTable) -> {
          ivrReadNextPrevTestUtils.assertItemsExistByCodeAndPurchaseUnits(itemDataTable);
        });

    Given(
        "^the following IVRs with the following order exist:$",
        (DataTable itemVendorRecordsList) -> {
          ivrReadNextPrevTestUtils.assertThatItemVendorRecordsExistsByCodeAndPurchaseUnitCode(
              itemVendorRecordsList);
        });

    When(
        "^\"([^\"]*)\" requests to view next IVR of current IVR with code \"([^\"]*)\"$",
        (String username, String ivrCode) -> {
          Response response =
              ivrReadNextPrevTestUtils.requestToViewNextIVR(
                  userActionsTestUtils.getUserCookie(username), ivrCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the next IVR code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String ivrCode, String username) -> {
          ivrReadNextPrevTestUtils.assertThatNextIVRCodeIsCorrect(
              ivrCode, userActionsTestUtils.getUserResponse(username));
        });

    When(
        "^\"([^\"]*)\" requests to view previous IVR of current IVR with code \"([^\"]*)\"$",
        (String username, String ivrCode) -> {
          Response response =
              ivrReadNextPrevTestUtils.requestToViewPreviousIVR(
                  userActionsTestUtils.getUserCookie(username), ivrCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the previous IVR code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String ivrCode, String username) -> {
          ivrReadNextPrevTestUtils.assertThatPreviousIVRCodeIsCorrect(
              ivrCode, userActionsTestUtils.getUserResponse(username));
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    ivrReadNextPrevTestUtils = new ItemVendorRecordReadNextPrevTestUtils(properties);
    ivrReadNextPrevTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "IVR Read Next and Previous")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(ivrReadNextPrevTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(ivrReadNextPrevTestUtils.getDbScriptsPath());
    }
  }
}
