package com.ebs.dda.masterdata.productmanager.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {"classpath:features/masterdata/product-manager/ProductManager_Dropdown.feature"},
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.masterdata.productmanager"},
    strict = true,
    tags = "not @Future")
public class CObProductManagerCucumberRunner {}
