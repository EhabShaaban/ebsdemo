package com.ebs.dda.masterdata.item.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.masterdata.item.IExpectedKeys;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.ebs.dda.masterdata.item.IResponseKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class MObItemUOMViewTestUtils extends IObItemUOMCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[UOM][View]";

  public MObItemUOMViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response readUnitOfMeasures(String itemCode, Cookie cookie) {
    String url = IMObItemRestUrls.READ_ALL_UNIT_OF_MEASURES_URL + itemCode;
    return sendGETRequest(cookie, url);
  }



  public void assertUOMsExistInItemWithCode(DataTable itemsData, String itemCode) {
    List<List<String>> itemsList = itemsData.raw();
    for (int i = 1; i < itemsList.size(); i++) {
      List<String> itemUOMData = itemsList.get(i);
      Object[] params = constructItemHasUnitOfMeasureQueryParams(itemCode, itemUOMData);
      if (params != null) {
        assertThatItemHasUnitOfMeasure(params);
      }
    }
  }

    public void assertThatResponseHasItemWithoutAnyUnitOfMeasures(Response response) {
    List<Map<String, Object>> unitOfMeasures = response.body().jsonPath().get(CommonKeys.DATA);
    Assert.assertTrue(ASSERTION_MSG + " UOM is not empty", unitOfMeasures.isEmpty());
  }

  public void assertThatResponseHasItemAndUnitOfMeasuresExist(
      Response response, List<Map<String, Object>> expectedData, String itemCode) throws Exception {
    List<Map<String, Object>> actualData = response.body().jsonPath().get(CommonKeys.DATA);
      assertThatUnitOfMeasureExistInResponse(itemCode, expectedData, actualData);
  }

  private void assertThatUnitOfMeasureExistInResponse(
      String itemCode, List<Map<String, Object>> expectedDataItem, List<Map<String, Object>> actualData)
      throws Exception {
    for (int i = 0; i < actualData.size(); i++) {
      String currentItemCode = actualData.get(i).get(IResponseKeys.ITEM_CODE).toString();
      if (currentItemCode.equals(itemCode)) {
        Map<String, Object> selectedItem = actualData.get(i);
            assertThatExpectedAlternativeUnitEqualsActual(expectedDataItem.get(i), selectedItem);
            assertThatExpectedConversionFactorEqualsActual(expectedDataItem.get(i), selectedItem);
            assertThatExpectedOldItemNumberEqualsActual(expectedDataItem.get(i), selectedItem);
      }
    }
  }

  private void assertThatExpectedAlternativeUnitEqualsActual(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) throws Exception {
    String expectedValue = expectedDataItem.get(IExpectedKeys.ALTERNATIVE_UNIT).toString();
    ObjectMapper mapperObj = new ObjectMapper();
    String actualLocalizedValueAsString =
        mapperObj.writeValueAsString(actualDataItem.get(IResponseKeys.ALTERNATIVE_UNIT_NAME));
    String actualValue = getEnglishLocaleFromLocalizedResponse(actualLocalizedValueAsString);
    Assert.assertEquals(
        ASSERTION_MSG + " Alternative Unit is not correct", expectedValue, actualValue);
  }

  private void assertThatExpectedConversionFactorEqualsActual(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) {
    Float expectedValue =
        Float.parseFloat(expectedDataItem.get(IExpectedKeys.CONVERSION_FACTOR).toString());
    Float actualValue =
        Float.parseFloat(actualDataItem.get(IResponseKeys.CONVERSION_FACTOR).toString());
    Assert.assertEquals(
            ASSERTION_MSG + " Conversion Factor is not correct", expectedValue, actualValue);
  }

  private void assertThatExpectedOldItemNumberEqualsActual(
      Map<String, Object> expectedDataItem, Map<String, Object> actualDataItem) {
    String expectedValue = expectedDataItem.get(IExpectedKeys.OLD_ITEM_NUMBER).toString();
    String actualValue = actualDataItem.get(IResponseKeys.OLD_ITEM_NUMBER).toString();
    Assert.assertEquals(
            ASSERTION_MSG + " Old Item Number is not correct", expectedValue, actualValue);
  }
}
