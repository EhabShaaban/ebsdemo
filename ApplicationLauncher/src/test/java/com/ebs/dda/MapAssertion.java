package com.ebs.dda;

import com.ebs.dac.security.shiro.twofactorauth.otp.TimeBasedOneTimePassword;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;

public class MapAssertion {

  private static final Logger log = LoggerFactory.getLogger(TimeBasedOneTimePassword.class);

  private final Map<String, String> expected;
  private final Map<String, Object> actual;
  private final ObjectMapper mapperObj;

  public MapAssertion(Map<String, String> expected, Map<String, Object> actual) {
    this.expected = expected;
    this.actual = actual;
    mapperObj = new ObjectMapper();
  }

  public void assertValue(String expectedName, String actualName) {
    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + actual.get(actualName),
          expectedValue,
          actual.get(actualName));
    }
  }

  public void assertBooleanValue(String expectedName, String actualName) {
    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + actual.get(actualName),
          Boolean.valueOf(expectedValue),
          actual.get(actualName));
    }
  }

  public void assertDateTime(String expectedName, String actualName) {
    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      DateTimeFormatter expectedFormatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
      DateTimeFormatter actualFormatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
      DateTime expectedDateTime = expectedFormatter.parseDateTime(expectedValue);
      DateTime actualDateTime =
          actualFormatter.parseDateTime(String.valueOf(actual.get(actualName)));
      actualDateTime = actualDateTime.minusSeconds(actualDateTime.getSecondOfMinute());
      Assert.assertEquals(
          "expectedDateTime: " + expectedDateTime + " | not equals: " + actualDateTime,
          expectedDateTime,
          actualDateTime);
    }
  }

  public void assertValueContains(String expectedName, String actualName) {
    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      String actualValue = String.valueOf(actual.get(actualName));
      Assert.assertTrue(
          "actualValue: " + actualValue + " not contains: " + expectedValue,
          actualValue.contains(expectedValue));
    }
  }

  public void assertLocalizedTypeAndCodeAndLocalizedNameValue(
      String expectedName, String actualType, String actualCode, String actualName)
      throws Exception {
    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      String localizedTypeString = mapperObj.writeValueAsString(actual.get(actualType));
      Assert.assertNotEquals("localizedTypeString is Null", localizedTypeString, "null");
      String type = getLocaleFromLocalizedResponse(localizedTypeString, "en");
      String codeAndName = buildStringFromCodeAndLocalizedString(actualCode, actualName);
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + type + " - " + codeAndName,
          expectedValue,
          type + " - " + codeAndName);
    }
  }

  public void assertCodeAndLocalizedNameValue(
      String expectedName, String actualCode, String actualName) throws Exception {
    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      String codeAndName = buildStringFromCodeAndLocalizedString(actualCode, actualName);
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + codeAndName,
          expectedValue,
          String.valueOf(codeAndName));
    }
  }

  private String buildStringFromCodeAndLocalizedString(String actualCode, String actualName)
      throws Exception {
    StringBuilder codeAndName = new StringBuilder();
    codeAndName.append(actual.get(actualCode));
    String localizedString = mapperObj.writeValueAsString(actual.get(actualName));
    if (!localizedString.equals("null")) {
      codeAndName.append(" - ");
      codeAndName.append(getLocaleFromLocalizedResponse(localizedString, "en"));
    }
    return String.valueOf(codeAndName);
  }

  public void assertLocalizedValueAndCodeValue(
      String expectedName, String actualName, String actualCode) throws Exception {
    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      String nameAndCode = buildStringFromLocalizedStringAndCode(actualName, actualCode);
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + nameAndCode,
          expectedValue,
          String.valueOf(nameAndCode));
    }
  }

  private String buildStringFromLocalizedStringAndCode(String actualName, String actualCode)
      throws Exception {
    StringBuilder nameAndCode = new StringBuilder();
    String localizedString = mapperObj.writeValueAsString(actual.get(actualName));
    if (!localizedString.equals("null")) {
      nameAndCode.append(getLocaleFromLocalizedResponse(localizedString, "en"));
    }
    if (actual.get(actualCode) != null) {
      nameAndCode.append(" - ");
      nameAndCode.append(actual.get(actualCode));
    }
    return String.valueOf(nameAndCode);
  }

  public void assertDoubleValue(String expectedName, String actualName) {

    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      expectedValue = String.valueOf(Double.parseDouble(expectedValue));
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + actual.get(actualName),
          expectedValue,
          String.valueOf(actual.get(actualName)));
    }
  }

  //  TODO: To be removed and uses the assertBigDecimalNumberValue() instead
  @Deprecated
  public void assertBigDecimalValue(String expectedName, String actualName) {

    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + actual.get(actualName),
          String.valueOf(Double.parseDouble(expectedValue)),
          String.valueOf(Double.parseDouble(actual.get(actualName).toString())));
    }
  }

  public void assertBigDecimalNumberValue(String expectedName, String actualName) {
    String expectedValue = expected.get(expectedName);

    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      String actualValue = actual.get(actualName).toString();

      BigDecimal expectedBigDecimal = new BigDecimal(expectedValue);
      BigDecimal actualBigDecimal = new BigDecimal(actualValue);

      Assert.assertEquals(
          "expectedValue for "
              + expectedName
              + " : "
              + expectedBigDecimal
              + " | not equals: "
              + actualBigDecimal,
          0,
          expectedBigDecimal.compareTo(actualBigDecimal));
    }
  }

  public void assertBigDecimalNumberValueWithCompositeValue(
      String expectedName, String actualName, String actualCompositeKey) {
    String expectedValue = expected.get(expectedName).split(" ")[0];
    String expectedCompositeValue = expected.get(expectedName).split(" ")[1];

    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      String actualValue = actual.get(actualName).toString();

      BigDecimal expectedBigDecimal = new BigDecimal(expectedValue);
      BigDecimal actualBigDecimal = new BigDecimal(actualValue);

      Assert.assertEquals(
          "expectedValue: " + expectedBigDecimal + " | not equals: " + actualBigDecimal,
          0,
          expectedBigDecimal.compareTo(actualBigDecimal));
    }
    Assert.assertEquals(expectedCompositeValue, actual.get(actualCompositeKey).toString());
  }

  public void assertIntegerValue(String expectedName, String actualName) {

    String expectedValue = expected.get(expectedName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actual.get(actualName));
    } else {
      expectedValue = String.valueOf(Integer.parseInt(expectedValue));
      Assert.assertEquals(
          "expectedValue: " + expectedValue + " | not equals: " + actual.get(actualName),
          expectedValue,
          String.valueOf(actual.get(actualName)));
    }
  }

  public void assertLocalizedValue(String expectedName, String actualName) {
    String expectedValue = expected.get(expectedName);
    Object actualValue = actual.get(actualName);
    if (expectedValue.isEmpty()) {
      Assert.assertNull(actualValue);
    } else {
      assertLocalizeStringIfNotNull(expectedValue, actualValue);
    }
  }

  private void assertLocalizeStringIfNotNull(String expectedValue, Object actualValue) {
    try {
      String localizedString = mapperObj.writeValueAsString(actualValue);
      Assert.assertEquals(
          "expectedValue: "
              + expectedValue
              + " | not equals: "
              + getLocaleFromLocalizedResponse(localizedString, "en"),
          expectedValue,
          getLocaleFromLocalizedResponse(localizedString, "en"));
    } catch (Exception e) {
      Assert.fail("actualValue: " + actualValue + " | not equals expected: " + expectedValue);
      log.error("Could not map object", e);
    }
  }

  private String getLocaleFromLocalizedResponse(String localizedString, String locale)
      throws JSONException {
    JSONObject jsonObject = new JSONObject(localizedString);
    return jsonObject.getJSONObject("values").getString(locale);
  }

  public void assertConcatenatingValue(
      String expectedKey, Object actualFirstKey, Object actualSecondKey, String delimiter) {
    String expectedValue = expected.get(expectedKey);
    StringBuilder actualValue = new StringBuilder();
    actualValue.append(actual.get(actualFirstKey));
    actualValue.append(delimiter);
    actualValue.append(actual.get(actualSecondKey));
    if (expectedValue.isEmpty()) {
      Assert.assertTrue(actualValue.toString().contains("null"));
    } else {
      Assert.assertEquals(expectedValue, actualValue.toString());
    }
  }

  public void assertEquationValues(
      String expectedKey,
      List<String> leftHandSideKeys,
      List<String> rightHandSideKeys,
      String delimiter) {
    String expectedValue = expected.get(expectedKey);
    StringBuilder actualValue = new StringBuilder();
    String actualLeftHandSideValue = constructLeftHandSide(leftHandSideKeys);
    String actualRightHandSideValue = constructRightHandSide(rightHandSideKeys);
    actualValue.append(actualLeftHandSideValue + delimiter + actualRightHandSideValue);
    Assert.assertEquals(expectedValue, actualValue.toString());
  }

  private String constructLeftHandSide(List<String> leftHandSideKeys) {
    StringBuilder actualLeftHandSideValue = new StringBuilder();
    for (String key : leftHandSideKeys) {
      actualLeftHandSideValue.append((actual.get(key)));
      actualLeftHandSideValue.append(" ");
    }
    return actualLeftHandSideValue.toString();
  }

  private String constructRightHandSide(List<String> rightHandSideKeys) {
    StringBuilder actualRightHandSideValue = new StringBuilder();
    for (String key : rightHandSideKeys) {
      actualRightHandSideValue.append(" ");
      actualRightHandSideValue.append((actual.get(key)));
    }
    return actualRightHandSideValue.toString();
  }
}
