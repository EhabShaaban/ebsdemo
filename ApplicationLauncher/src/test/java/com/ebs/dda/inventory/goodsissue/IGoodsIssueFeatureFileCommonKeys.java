package com.ebs.dda.inventory.goodsissue;

public interface IGoodsIssueFeatureFileCommonKeys {

    String GI_CODE = "GICode";
    String TYPE = "Type";
    String CUSTOMER = "Customer";
    String ADDRESS = "Address";
    String CONTACT_PERSON = "ContactPerson";
    String SALES_INVOICE = "SalesInvoice";
    String SALES_ORDER = "SalesOrder";
    String REF_DOCUMENT = "RefDocument";
    String SALES_REPRESENTATIVE = "SalesRepresentative";
    String KAP = "KAP";
    String COMMENTS = "Comments";
    String ITEM = "Item";
    String ITEM_EN = "Item En";
    String ITEM_AR = "Item Ar";
    String UOM = "UOM";
    String QUANTITY = "Quantity";
    String ACTIVATED_BY = "ActivatedBy";
    String ACTIVATION_DATE = "ActivationDate";
    String REMAINING_IN_STORE = "RemainingInStore";
    String NOTES = "Notes";
    String QTY_BASE = "QtyBase" ;
}
