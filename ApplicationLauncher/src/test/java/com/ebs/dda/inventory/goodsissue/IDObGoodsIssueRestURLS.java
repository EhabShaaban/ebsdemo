package com.ebs.dda.inventory.goodsissue;

public interface IDObGoodsIssueRestURLS {

  String BASE_URL = "/services/goodsissue";
  String AUTHORIZED_ACTIONS = BASE_URL + "/authorizedactions";
  String VIEW_GENERAL_DATA = "/generaldata";
  String VIEW_ALL_URL = BASE_URL;
  String VIEW_COMPANY = "/company";
  String VIEW_REF_DOCUMENT_DATA = "/refdocumentdata";
  String VIEW_ITEMS = "/items";

  String GOODS_ISSUE_COMMAND = "/command/goodsissue";
  String CREATE_GOODS_ISSUE = GOODS_ISSUE_COMMAND + "/";
  String DELETE_GOODS_ISSUE = GOODS_ISSUE_COMMAND + "/";
  String GOODS_ISSUE_ACTIVATE = "/activate";
  String VIEW_GI_TYPES = "/goodsissuetypes";

}
