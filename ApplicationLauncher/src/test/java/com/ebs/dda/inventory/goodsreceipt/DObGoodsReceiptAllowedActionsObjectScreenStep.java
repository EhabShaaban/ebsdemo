package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptAllowedActionsObjectScreenTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObGoodsReceiptAllowedActionsObjectScreenStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptAllowedActionsObjectScreenTestUtils allowedActionsObjectScreenTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    allowedActionsObjectScreenTestUtils =
        new DObGoodsReceiptAllowedActionsObjectScreenTestUtils(getProperties());

    allowedActionsObjectScreenTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  public DObGoodsReceiptAllowedActionsObjectScreenStep() {
    Given(
        "^the following GoodsReceipts exist:$",
        (DataTable goodsReceiptsDataTable) -> {
          allowedActionsObjectScreenTestUtils.assertGoodsReceiptExist(goodsReceiptsDataTable);
        });
    When(
        "^\"([^\"]*)\" requests to read actions of GR with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          String readUrl = allowedActionsObjectScreenTestUtils.readRestUrl + goodsReceiptCode;
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              allowedActionsObjectScreenTestUtils.sendGETRequest(loginCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          allowedActionsObjectScreenTestUtils.assertThatTheFollowingActionsExist(
              response, allowedActions);
        });

    Then(
        "^the following actions are displayed to \"([^\"]*)\":$",
        (String userName, DataTable allowedActions) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);

          allowedActionsObjectScreenTestUtils.assertThatTheFollowingActionsExist(
              response, allowedActions);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of GoodsReceipt in home screen$",
        (String userName) -> {
          String readUrl = allowedActionsObjectScreenTestUtils.readRestUrl;
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              allowedActionsObjectScreenTestUtils.sendGETRequest(loginCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Goods Receipt")
        || contains(scenario.getName(), "Read Next/Previous GR")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          allowedActionsObjectScreenTestUtils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(allowedActionsObjectScreenTestUtils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Goods Receipt")) {
      databaseConnector.closeConnection();
    }
  }
}
