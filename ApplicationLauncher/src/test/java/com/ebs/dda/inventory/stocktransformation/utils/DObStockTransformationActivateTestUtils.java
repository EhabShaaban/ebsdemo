package com.ebs.dda.inventory.stocktransformation.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.stocktransformation.IDObStockTransformationRestURLS;
import com.ebs.dda.inventory.stocktransformation.IStockTransformationFileCommonKeys;
import com.ebs.dda.inventory.storetransaction.utils.IStoreTransactionFileCommonKeys;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObStockTransformationActivateTestUtils extends DObStockTransformationCommonTestUtils {
  public static final String TRANSACTION_TYPE_ADD = "ADD";

  public DObStockTransformationActivateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public Response activateStockTransformation(Cookie userCookie, String stockTransformationCode) {

    return sendPUTRequest(
        userCookie,
        IDObStockTransformationRestURLS.STOCK_TRANSFORMATION_COMMAND
            + "/"
            + stockTransformationCode
            + IDObStockTransformationRestURLS.STOCK_TRASFORMATION_ACTIVTE,
        new JsonObject());
  }

  public void assertThatGeneralStockTransformationsExits(DataTable stockTransformationsDataTable) {
    List<Map<String, String>> stockTransformationsList =
        stockTransformationsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> stockTransformation : stockTransformationsList) {
      DObStockTransformationGeneralModel dObstockTransformations =
          (DObStockTransformationGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGeneralStockTransformation",
                  constructStockTransformationParameters(stockTransformation));
      assertNotNull(dObstockTransformations);
      assertDateEquals(
          stockTransformation.get(IFeatureFileCommonKeys.CREATION_DATE),
          dObstockTransformations.getCreationDate());
    }
  }

  private Object[] constructStockTransformationParameters(
      Map<String, String> stockTransformationMap) {
    return new Object[] {
      stockTransformationMap.get(IFeatureFileCommonKeys.CODE),
      "%" + stockTransformationMap.get(IFeatureFileCommonKeys.STATE) + "%",
      stockTransformationMap.get(IFeatureFileCommonKeys.TYPE),
      stockTransformationMap.get(IFeatureFileCommonKeys.CREATED_BY),
      stockTransformationMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertThatStockTransformationDetailsExits(
      DataTable stockTransformationDetailsDataTable) {
    List<Map<String, String>> stockTransformationsList =
        stockTransformationDetailsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> stockTransformations : stockTransformationsList) {
      DObStockTransformationGeneralModel dObstockTransformations =
          (DObStockTransformationGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getstocktransformationdetails",
                  constructStockTransformationDetailsParameters(stockTransformations));
      assertNotNull(dObstockTransformations);
    }
  }

  private Object[] constructStockTransformationDetailsParameters(
      Map<String, String> stockTransformationMap) {
    return new Object[] {
      stockTransformationMap.get(IFeatureFileCommonKeys.CODE),
      stockTransformationMap.get(IStockTransformationFileCommonKeys.REF_STORE_TRANSACTION),
      Double.parseDouble(stockTransformationMap.get(IFeatureFileCommonKeys.QTY)),
      stockTransformationMap.get(IStockTransformationFileCommonKeys.NEW_STOCK_TYPE),
      stockTransformationMap.get(IStockTransformationFileCommonKeys.STOCK_TRASFORMATION_REASON),
    };
  }

  public void assertThatStoreTransactionDataExits(DataTable storeTransactionDatatable) {
    List<Map<String, String>> storeTransactionList =
        storeTransactionDatatable.asMaps(String.class, String.class);

    for (Map<String, String> storeTransaction : storeTransactionList) {
      TObStoreTransactionGeneralModel storeTransactionGeneralModel =
          (TObStoreTransactionGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getstoretransactiondata",
                  constructStoreTransactionDataParameters(storeTransaction));
      assertNotNull(storeTransactionGeneralModel);
    }
  }

  private Object[] constructStoreTransactionDataParameters(
      Map<String, String> storeTransactionMap) {
    return new Object[] {
      storeTransactionMap.get(IFeatureFileCommonKeys.CODE),
      Double.parseDouble(storeTransactionMap.get(IStoreTransactionFileCommonKeys.REMAINING_QTY)),
      storeTransactionMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.STOCK_TYPE)
    };
  }

  public void assertThatStockTransformationIsUpdated(
      String stockTransformationCode, DataTable expectedStockTransformationDataTable) {
    Map<String, String> expectedStockTransformation =
        expectedStockTransformationDataTable.asMaps(String.class, String.class).get(0);
    DObStockTransformationGeneralModel actualStockTransformation =
        (DObStockTransformationGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getStockTransformationUpdatedByUserAtDate",
                constructUpdatedStockTransformationParams(
                    stockTransformationCode, expectedStockTransformation));

    assertNotNull(actualStockTransformation);
    assertDateEquals(
            expectedStockTransformation.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            actualStockTransformation.getModifiedDate());
  }

  private Object[] constructUpdatedStockTransformationParams(
      String stockTransformationCode, Map<String, String> expectedStockTransformation) {
    return new Object[] {
      stockTransformationCode,
      expectedStockTransformation.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      "%" + expectedStockTransformation.get(IFeatureFileCommonKeys.STATE) + "%",
      expectedStockTransformation.get(IStockTransformationFileCommonKeys.NEW_STORE_TRANSACTION)
    };
  }

  public void assertThatStoreTransactionsUpdated(DataTable updatedStoreTransaction) {
    List<Map<String, String>> storeTransactionMaps =
        updatedStoreTransaction.asMaps(String.class, String.class);
    for (int i = 0; i < storeTransactionMaps.size(); i++) {
      Map<String, String> storeTransactionMap = storeTransactionMaps.get(i);
      TObStoreTransactionGeneralModel storeTransactionGeneralModel =
          super.getStoreTransactionGeneralModel(updatedStoreTransaction, i);
      Assert.assertNotNull(storeTransactionGeneralModel);

      assertDateEquals(
          storeTransactionMap.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE),
          storeTransactionGeneralModel.getOriginalAddingDate());
      assertDateEquals(
              storeTransactionMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
              storeTransactionGeneralModel.getModifiedDate());
    }
  }

  public void assertLastCreatedStoreTransaction(String storeTransactioncode) {
    TObStoreTransaction storeTransaction =
        (TObStoreTransaction)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLastStoreTransactionCode");
    assertEquals(storeTransactioncode, storeTransaction.getUserCode());
  }

  public void assertThatStoreTransactionsCreated(DataTable storeTransactionsDataTable) {
    List<Map<String, String>> storeTransactionMaps =
        storeTransactionsDataTable.asMaps(String.class, String.class);
    for (int i = 0; i < storeTransactionMaps.size(); i++) {
      Map<String, String> storeTransactionMap = storeTransactionMaps.get(i);
      TObStoreTransactionGeneralModel storeTransactionGeneralModel =
          super.getStoreTransactionGeneralModel(storeTransactionsDataTable, i);
      Assert.assertNotNull(storeTransactionGeneralModel);

      if (storeTransactionMap
          .get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE)
          .equals(TRANSACTION_TYPE_ADD)) {
        assertDateEquals(
            storeTransactionMap.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE),
            storeTransactionGeneralModel.getOriginalAddingDate());
      }
      assertDateEquals(
          storeTransactionMap.get(IFeatureFileCommonKeys.CREATION_DATE),
          storeTransactionGeneralModel.getCreationDate());
    }
  }
}
