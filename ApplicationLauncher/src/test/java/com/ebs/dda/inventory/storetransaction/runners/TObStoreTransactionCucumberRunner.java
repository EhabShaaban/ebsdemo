package com.ebs.dda.inventory.storetransaction.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {
                "classpath:features/inventory/store-transaction/ST_ViewAll.feature",
                "classpath:features/inventory/store-transaction/ST_Dropdown.feature",
                "classpath:features/inventory/stock-availability/SA_ViewAll.feature"
        },
        glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.inventory.storetransaction"},
        strict = true,
        tags = "not @Future")
public class TObStoreTransactionCucumberRunner {
}
