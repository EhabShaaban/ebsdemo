package com.ebs.dda.inventory.stocktransformation.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.stocktransformation.IDObStockTransformationRestURLS;
import com.ebs.dda.jpa.inventory.stocktransformation.ICObStockTransformationType;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CObStockTransformationTypeDropDownTestUtils extends DObStockTransformationCommonTestUtils {

  private ObjectMapper objectMapper;

  public CObStockTransformationTypeDropDownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/inventory/stock-transformation/CObStockTransformation.sql");

    return str.toString();
  }

  public String readAllStockTransformationTypes() {
    return IDObStockTransformationRestURLS.STOCK_TRANSFORMATION_TYPES;
  }

  public void assertResponseMatchesExpected(Response response, DataTable stockTransformationTypesDataTable)
          throws Exception {
    List<Map<String, String>> stockTransformationTypesExpectedList =
        stockTransformationTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> stockTransformationTypesActualList = getListOfMapsFromResponse(response);
    for (int i = 0; i < stockTransformationTypesActualList.size(); i++) {
      assertActualAccountMatchesExpected(
          stockTransformationTypesExpectedList.get(i), stockTransformationTypesActualList.get(i));
    }
  }

  private void assertActualAccountMatchesExpected(
      Map<String, String> expectedstockTransformationTypes, HashMap<String, Object> actualstockTransformationTypes)
          throws Exception {

    String actualTransformationReasonsEnglishLocale = getEnglishValue(actualstockTransformationTypes.get(ICObStockTransformationType.NAME));
    assertEquals(
        expectedstockTransformationTypes.get(IFeatureFileCommonKeys.TYPE).split(" - ")[0], actualTransformationReasonsEnglishLocale);
  }

  public void assertThatAllStockTransformationTypesExist(Integer recordsNumber) {
    Long typesCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getStockTransformationTypesCount");
    assertEquals(Long.valueOf(recordsNumber), typesCount);
  }
}
