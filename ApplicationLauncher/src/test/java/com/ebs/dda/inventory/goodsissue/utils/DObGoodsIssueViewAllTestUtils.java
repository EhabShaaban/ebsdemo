package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IDObGoodsIssueGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DObGoodsIssueViewAllTestUtils extends DObGoodsIssueCommonTestUtils {

  public static String REQUEST_LOCALE = "en";
  private ObjectMapper objectMapper;

  public DObGoodsIssueViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssue_ViewAll.sql");
    return str.toString();
  }

  public void assertGoodsIssuesExist(DataTable goodsIssuesTable) {
    List<Map<String, String>> goodsIssueListMap =
        goodsIssuesTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueMap : goodsIssueListMap) {
      DObGoodsIssueGeneralModel goodsIssueGeneralModel =
          (DObGoodsIssueGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsIssueViewAll", constructGoodsIssueQueryParams(goodsIssueMap));
      Assert.assertNotNull(
          goodsIssueMap.get(IFeatureFileCommonKeys.CODE) + " doesn't exist!",
          goodsIssueGeneralModel);
    }
  }

  private Object[] constructGoodsIssueQueryParams(Map<String, String> goodsIssueMap) {
    return new Object[] {
      goodsIssueMap.get(IFeatureFileCommonKeys.CODE),
      goodsIssueMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + goodsIssueMap.get(IFeatureFileCommonKeys.STATE) + "%",
      goodsIssueMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      goodsIssueMap.get(IFeatureFileCommonKeys.TYPE),
      goodsIssueMap.get(IFeatureFileCommonKeys.STOREHOUSE),
      goodsIssueMap.get(IFeatureFileCommonKeys.CUSTOMER)
    };
  }

  public void assertGoodsIssuesDataIsCorrect(DataTable goodsIssueDataTable, Response response)
      throws Exception {

    List<Map<String, String>> goodsIssueList =
        goodsIssueDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> goodsIssueResponse = response.body().jsonPath().getList("data");
    for (int i = 1; i < goodsIssueList.size(); i++) {
      assertCorrectShownData(goodsIssueResponse.get(i - 1), goodsIssueList.get(i - 1));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> responsedGoodsIssue, Map<String, String> goodsIssueRowMap)
      throws IOException, JSONException {

    String code = goodsIssueRowMap.get(IFeatureFileCommonKeys.CODE);
    String purchaseUnitName = goodsIssueRowMap.get(IFeatureFileCommonKeys.PURCHASE_UNIT);
    String state = goodsIssueRowMap.get(IFeatureFileCommonKeys.STATE);
    String creationDate = goodsIssueRowMap.get(IFeatureFileCommonKeys.CREATION_DATE);
    String type = goodsIssueRowMap.get(IFeatureFileCommonKeys.TYPE);
    String storehouseName = goodsIssueRowMap.get(IFeatureFileCommonKeys.STOREHOUSE);
    String customerName = goodsIssueRowMap.get(IFeatureFileCommonKeys.CUSTOMER);

    assertEquals(code, getAsString(responsedGoodsIssue.get(IDObGoodsIssueGeneralModel.USER_CODE)));
    String localizedPurchaseUnitName =
        getLocaleFromLocalizedResponse(
            getLocalizedValue(
                responsedGoodsIssue.get(IDObGoodsIssueGeneralModel.PURCHASING_UNIT_NAME)),
            REQUEST_LOCALE);
    assertEquals(purchaseUnitName, localizedPurchaseUnitName);
    assertTrue(
        responsedGoodsIssue
            .get(IDObGoodsIssueGeneralModel.CURRENT_STATES)
            .toString()
            .contains(state));
    assertCreationDateEquals(creationDate, responsedGoodsIssue);
    assertEquals(
        type, getAsString(responsedGoodsIssue.get(IDObGoodsIssueGeneralModel.TYPE_NAME_EN)));
    assertEquals(purchaseUnitName, localizedPurchaseUnitName);
    String localizedStorehouseName =
        getLocaleFromLocalizedResponse(
            getLocalizedValue(responsedGoodsIssue.get(IDObGoodsIssueGeneralModel.STORE_HOUSE_NAME)),
            REQUEST_LOCALE);
    assertEquals(storehouseName, localizedStorehouseName);
    String localizedCustomerName =
        getLocaleFromLocalizedResponse(
            getLocalizedValue(responsedGoodsIssue.get(IDObGoodsIssueGeneralModel.CUSTOMER_NAME)),
            REQUEST_LOCALE);
    assertEquals(customerName, localizedCustomerName);
  }

  private String getAsString(Object obj) {
    if (obj == null) return "";
    return obj.toString();
  }

  private String getLocalizedValue(Object obj) throws IOException {
    return objectMapper.writeValueAsString(obj);
  }

  public void assertCreationDateEquals(
      String creationDate, Map<String, Object> goodsIssueDatabaseVersion) {
    if (creationDate != null && !creationDate.isEmpty()) {
      assertThatDatesAreEqual(
          creationDate,
          String.valueOf(goodsIssueDatabaseVersion.get(IDObGoodsIssueGeneralModel.CREATION_DATE)));
    } else {
      assertNull(goodsIssueDatabaseVersion.get(IDObGoodsIssueGeneralModel.CREATION_DATE));
    }
  }
}
