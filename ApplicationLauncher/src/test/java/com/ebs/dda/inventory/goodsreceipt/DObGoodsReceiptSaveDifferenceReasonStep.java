package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptSaveDifferenceReasonTestUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DObGoodsReceiptSaveDifferenceReasonStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptSaveDifferenceReasonTestUtils utils;

  public DObGoodsReceiptSaveDifferenceReasonStep() {
    Given(
        "^GoodsReceipts has the following received items:$",
        (DataTable receivedItems) -> {
            utils.assertGoodsReceiptItemsExist(receivedItems);
        });
    When(
        "^\"([^\"]*)\" saves Difference Reason of Item with code \"([^\"]*)\" in goodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String itemCode, String grCode, String saveDate, DataTable itemData) -> {
          utils.freeze(saveDate);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveItemDifferenceReason(userCookie, grCode, itemCode, itemData);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^edit Difference Reason dialog is closed and the lock by \"([^\"]*)\" on Items section of goodsreceipt with code \"([^\"]*)\" is released$",
        (String userName, String grCode) -> {
          utils.assertThatLockIsReleased(
              userName, grCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObGoodsReceiptSaveDifferenceReasonTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save GR Item Difference Reason -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Save GR Item Difference Reason -")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
          new ArrayList<>(Collections.singletonList(IGoodsReceiptSectionNames.ITEMS_SECTION)));
      databaseConnector.closeConnection();
    }
  }
}
