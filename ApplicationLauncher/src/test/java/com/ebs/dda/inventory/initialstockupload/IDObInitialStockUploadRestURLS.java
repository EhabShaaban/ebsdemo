package com.ebs.dda.inventory.initialstockupload;

public interface IDObInitialStockUploadRestURLS {

  String BASE_URL = "/services/initialstockupload";
  String VIEW_ALL_URL = BASE_URL;
  String AUTHORIZED_ACTIONS = BASE_URL + "/authorizedactions";

  String INITIAL_STOCK_UPLOAD_COMMAND = "/command/initialstockupload";
  String REQUEST_CREATE_INITIAL_STOCK_UPLOAD = INITIAL_STOCK_UPLOAD_COMMAND + "/";
  String CREATE_INITIAL_STOCK_UPLOAD = INITIAL_STOCK_UPLOAD_COMMAND + "/";
  String ACTIVATE = "/activate";
}
