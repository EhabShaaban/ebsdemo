package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.*;
import com.ebs.dda.inventory.storetransaction.utils.TObStoreTransactionCreateTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObGoodsIssueActivateHappyPathStep extends SpringBootRunner implements En {

    private DObGoodsIssueActivateTestUtils utils;
    private IObGoodsIssueItemViewTestUtils goodsIssueItemViewTestUtils;
    private DObGoodsIssueViewGeneralDataTestUtils goodsIssueViewGeneralDataTestUtils;
    private IObGoodsIssueViewSalesInvoiceDataTestUtils goodsIssueViewConsigneeTestUtils;
    private IObGoodsIssueCompanyViewTestUtils goodsIssueStoreViewTestUtils;
    private TObStoreTransactionCreateTestUtils tObStoreTransactionCreateTestUtils;
    private IObSalesOrderViewGeneralDataTestUtils salesOrderViewGeneralDataTestUtils;

    public DObGoodsIssueActivateHappyPathStep() {

        Given(
                "^ONLY the following StoreTransaction records exist:$",
                (DataTable storeTransactionDataTable) -> {
                    utils.assertThatStoreTransactionHasOnlyTheFollowingRecords(storeTransactionDataTable);
                });

        Given(
                "^Last created store transaction was with code \"([^\"]*)\"$",
                (String storeTransactionCode) -> {
                    tObStoreTransactionCreateTestUtils.assertStoreTransactionExistsWithCode(
                            storeTransactionCode);
                });
        Given(
                "^ONLY the following StoreTransaction records for Item \"([^\"]*)\" with UOM \"([^\"]*)\" in company \"([^\"]*)\", in storehouse \"([^\"]*)\", for BusinessUnit \"([^\"]*)\" exist:$",
                (String itemCode,
                 String uomCode,
                 String companyCode,
                 String storehouseCode,
                 String businessUnitCode,
                 DataTable storeTransactionsDataTable) -> {
                    utils.assertThatStoreTransactionsExistWithData(
                            itemCode,
                            uomCode,
                            companyCode,
                            storehouseCode,
                            businessUnitCode,
                            storeTransactionsDataTable);
                });

        Given(
                "^the total number of StoreTransaction records for Item \"([^\"]*)\" with UOM \"([^\"]*)\" in company \"([^\"]*)\", in storehouse \"([^\"]*)\", for BusinessUnit \"([^\"]*)\" is (\\d+)$",
                (String itemCode,
                 String uomCode,
                 String companyCode,
                 String storehouseCode,
                 String businessUnitCode,
                 Integer recordsCount) -> {
                    utils.assertThatStoreTransactionsCountMatchesExpected(
                            itemCode, uomCode, companyCode, storehouseCode, businessUnitCode, recordsCount);
                });

        Given(
                "^the remaining \"([^\"]*)\" qty of for Item \"([^\"]*)\" with UOM \"([^\"]*)\" in company \"([^\"]*)\", in storehouse \"([^\"]*)\", for BusinessUnit \"([^\"]*)\" exist:$",
                (String stockType,
                 String itemCode,
                 String uomCode,
                 String companyCode,
                 String storehouseCode,
                 String businessUnitCode,
                 DataTable quantityDataTable) -> {
                    utils.assertRemainingAmountInStoreForItemWithDataIs(
                            stockType,
                            itemCode,
                            uomCode,
                            companyCode,
                            storehouseCode,
                            businessUnitCode,
                            quantityDataTable);
                });
        Given(
                "^the following GeneralData for SalesOrder exists:$",
                (DataTable generalDataTable) -> {
                    salesOrderViewGeneralDataTestUtils.checkSalesOrderGeneralDataExists(generalDataTable);
                });

        When(
                "^\"([^\"]*)\" Activates GoodsIssue with code \"([^\"]*)\" at \"([^\"]*)\"$",
                (String userName, String goodsIssueCode, String postingDate) -> {
                    utils.freeze(postingDate);
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response postResponse = utils.activateGoodsIssue(userCookie, goodsIssueCode);
                    userActionsTestUtils.setUserResponse(userName, postResponse);
                });

        Then(
                "^GoodsIssue with Code \"([^\"]*)\" is updated as follows:$",
                (String goodsIssueCode, DataTable goodsIssueDataTable) -> {
                    utils.assertThatGoodsIssueIsUpdatedByUserAtDate(goodsIssueCode, goodsIssueDataTable);
                });

        Then(
                "^ActivationDetails in GoodsIssue with Code \"([^\"]*)\" is updated as follows:$",
                (String goodsIssueCode, DataTable postingDetailsDataTable) -> {
                    utils.assertThatGoodsIssueActivationDetailAreUpdated(
                            goodsIssueCode, postingDetailsDataTable);
                });

        Then(
                "^the following new StoreTransaction records are added as follows:$",
                (DataTable storeTransactionDataTable) -> {
                    utils.assertThatNewStoreTransactionsAreAdded(storeTransactionDataTable);
                });

        Then(
                "^then the following StoreTransaction records are updated as follows:$",
                (DataTable storeTransactionDataTable) -> {
                    utils.assertThatStoreTransactionsUpdatedAsFollows(storeTransactionDataTable);
                });

        Then(
                "^then the following StoreTransaction records remains as follows:$",
                (DataTable storeTransactionDataTable) -> {
                    utils.assertThatStoreTransactionsRemainsAsFollows(storeTransactionDataTable);
                });
        Then(
                "^The State of SalesOrder with code \"([^\"]*)\" is changed to \"([^\"]*)\"$",
                (String salesOrderCode, String currentState) -> {
                    utils.assertThatSalesOrderStateIsUpdatedAsIssued(salesOrderCode, currentState);
                });
    }

  @Override
  public void afterPropertiesSet() {
      super.afterPropertiesSet();
      Map<String, Object> properties = getProperties();
      utils = new DObGoodsIssueActivateTestUtils(properties);
      utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
      goodsIssueItemViewTestUtils = new IObGoodsIssueItemViewTestUtils(properties);
      goodsIssueItemViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
      goodsIssueViewGeneralDataTestUtils = new DObGoodsIssueViewGeneralDataTestUtils(properties);
      goodsIssueViewGeneralDataTestUtils.setEntityManagerDatabaseConnector(
              entityManagerDatabaseConnector);
      goodsIssueViewConsigneeTestUtils = new IObGoodsIssueViewSalesInvoiceDataTestUtils(properties);
      goodsIssueViewConsigneeTestUtils.setEntityManagerDatabaseConnector(
              entityManagerDatabaseConnector);
      goodsIssueStoreViewTestUtils = new IObGoodsIssueCompanyViewTestUtils(properties);
      goodsIssueStoreViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
      tObStoreTransactionCreateTestUtils = new TObStoreTransactionCreateTestUtils(getProperties());
      tObStoreTransactionCreateTestUtils.setEntityManagerDatabaseConnector(
              entityManagerDatabaseConnector);
      salesOrderViewGeneralDataTestUtils = new IObSalesOrderViewGeneralDataTestUtils(getProperties());
      salesOrderViewGeneralDataTestUtils.setEntityManagerDatabaseConnector(
              entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Activate GoodsIssue HP -")
              || contains(scenario.getName(), "Activate GoodsIssue Auth -")) {
          databaseConnector.createConnection();
          databaseConnector.executeSQLScript(utils.getDbScripts());
      }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Activate GoodsIssue HP -")) {
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
