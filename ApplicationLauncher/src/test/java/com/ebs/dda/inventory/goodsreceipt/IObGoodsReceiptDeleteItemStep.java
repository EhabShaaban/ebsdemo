package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptDeleteItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class IObGoodsReceiptDeleteItemStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private IObGoodsReceiptDeleteItemTestUtils goodsReceiptDeleteItemTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodsReceiptDeleteItemTestUtils = new IObGoodsReceiptDeleteItemTestUtils(getProperties());
        goodsReceiptDeleteItemTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }

    public IObGoodsReceiptDeleteItemStep() {

        When(
                "^\"([^\"]*)\" requests to delete Item with code \"([^\"]*)\" in GR with code \"([^\"]*)\" at \"([^\"]*)\"$",
                (String userName, String itemCode, String goodsReceiptCode, String dateTime) -> {
                    goodsReceiptDeleteItemTestUtils.freeze(dateTime);
                    Response deleteGoodsReceiptResponse =
                            goodsReceiptDeleteItemTestUtils.deleteItem(
                                    itemCode, goodsReceiptCode, userActionsTestUtils.getUserCookie(userName));
                    userActionsTestUtils.setUserResponse(userName, deleteGoodsReceiptResponse);
                });

    Then(
        "^Item with code \"([^\"]*)\" from GR with code \"([^\"]*)\" is deleted$",
        (String itemCode, String goodsReceiptCode) -> {
          goodsReceiptDeleteItemTestUtils.assertNoItemExistsWithCode(itemCode, goodsReceiptCode);
        });

    Then(
        "^GoodsReceipt with Code \"([^\"]*)\" is updated as follows:$",
        (String goodsReceiptCode, DataTable goodsReceiptExpectedData) -> {
          goodsReceiptDeleteItemTestUtils.assertThatGoodsReceiptIsUpdated(
              goodsReceiptCode, goodsReceiptExpectedData);
        });

    Then(
        "^the lock by \"([^\"]*)\" on ReceivedItems in GR with code \"([^\"]*)\" section is released$",
        (String userName, String goodsReceiptCode) -> {
          goodsReceiptDeleteItemTestUtils.assertThatLockIsReleased(
              userName, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to delete Item with code \"([^\"]*)\" in GR with code \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode) -> {
            Response deleteGoodsReceiptResponse =
                    goodsReceiptDeleteItemTestUtils.deleteItem(
                            itemCode, goodsReceiptCode, userActionsTestUtils.getUserCookie(userName));

          userActionsTestUtils.setUserResponse(userName, deleteGoodsReceiptResponse);
        });

    Given(
        "^\"([^\"]*)\" first request to add item to ReceivedItems in GR with code \"([^\"]*)\" and new record is opened in edit mode successfully$",
        (String userName, String goodsReceiptCode) -> {
            Response response =
                    userActionsTestUtils.lockSection(
                            userName,
                            IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM + goodsReceiptCode,
                            goodsReceiptCode);
            goodsReceiptDeleteItemTestUtils.assertResponseSuccessStatus(response);
            userActionsTestUtils.setUserResponse(userName, response);
        });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete GR Item -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(
                        goodsReceiptDeleteItemTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(goodsReceiptDeleteItemTestUtils.getDbScriptPath());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete GR Item -")) {
            userActionsTestUtils.unlockSectionForAllUsers(IDObGoodsReceiptRestUrls.CANCEL_REQUEST_ADD_ITEM);
            databaseConnector.closeConnection();
        }
    }
}
