package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObCostingViewActivationDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObCostingViewActivationDetailsStep extends SpringBootRunner
    implements En {

  public static final String SCENARIO_NAME =
      "View Costing ActivationDetails";
  private IObCostingViewActivationDetailsTestUtils utils;

  public IObCostingViewActivationDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view ActivationDetails section for GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          String readUrl = utils.getCostingActivationDetailsViewDataUrl(goodsReceiptCode);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
                  utils.sendGETRequest(userCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of ActivationDetails section for GoodsReceipt with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
            (String grAccountingDocumentCode, String userName,
                         DataTable accountingDetailsDataTable) -> {
                          Response response = userActionsTestUtils.getUserResponse(userName);
                          utils.assertCorrectValuesAreDisplayed(response,
                                  accountingDetailsDataTable);
                        });


  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObCostingViewActivationDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewAll(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(utils.clearCostingInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
