package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class IObGoodsIssueViewSalesInvoiceDataTestUtils
        extends IObGoodsIssueViewRefDocumentDataTestUtils {
  public IObGoodsIssueViewSalesInvoiceDataTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertThatGoodsIssueHasRefDocumentData(DataTable consigneeDataTable) {
    List<Map<String, String>> giRefDocumentListMap =
            consigneeDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueMap : giRefDocumentListMap) {
      IObGoodsIssueRefDocumentDataGeneralModel iObGoodsIssueRefDocumentDataGeneralModel =
              (IObGoodsIssueRefDocumentDataGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getGoodsIssueRefDocumentData",
                              constructGoodsIssueRefDocumentQueryParams(goodsIssueMap));
      Assert.assertNotNull(
              "RefDocument Data for GI with code: "
                      + goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE)
                      + " doesn't exist",
              iObGoodsIssueRefDocumentDataGeneralModel);
    }
  }

  private Object[] constructGoodsIssueRefDocumentQueryParams(Map<String, String> goodsIssueMap) {
    return new Object[]{
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.CONTACT_PERSON),
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.COMMENTS),
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.CUSTOMER),
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.ADDRESS),
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.REF_DOCUMENT),
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.SALES_REPRESENTATIVE),
            goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.KAP)
    };
  }
}
