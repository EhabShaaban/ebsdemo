package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.CObGoodsIssueDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class CObGoodsIssueDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private CObGoodsIssueDropDownTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new CObGoodsIssueDropDownTestUtils(properties, entityManagerDatabaseConnector);
  }

  public CObGoodsIssueDropDownStep() {
    Given(
        "^the total number of existing GoodsIssue Types are (\\d+)$",
        (Integer recordsNumber) -> {
          utils.assertThatGoodsIssueTypesCountIsCorrect(recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read all GoodsIssueTypes$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.readAllGoodsIssueTypes());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following GoodsIssue Types values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable goodsIssueTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseMatchesExpected(response, goodsIssueTypesDataTable);
        });

    Then(
        "^total number of GoodsIssue Types returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read list of GoodsIsuueTypes")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Read list of GoodsIsuueTypes")) {

      databaseConnector.closeConnection();
    }
  }
}
