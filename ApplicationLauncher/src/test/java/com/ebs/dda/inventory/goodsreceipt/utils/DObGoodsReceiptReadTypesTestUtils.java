package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.CObGoodsReceipt;
import com.ebs.dda.jpa.inventory.goodsreceipt.ICObGoodsReceipt;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObGoodsReceiptReadTypesTestUtils extends DObGoodsReceiptCommonTestUtils {

  public DObGoodsReceiptReadTypesTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    return str.toString();
  }

  public void assertGoodsReceiptTypesExist(DataTable typesDataTable) {
    List<Map<String, String>> goodsReceiptsTypes =
            typesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptsType : goodsReceiptsTypes) {
      CObGoodsReceipt cObGoodsReceipt =
              (CObGoodsReceipt)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getGoodsReceiptTypes", constructTypesQueryParam(goodsReceiptsType));
      assertNotNull(
              "GoodsReceiptType "
                      + goodsReceiptsType.get(IFeatureFileCommonKeys.CODE)
                      + " doesn't exist",
              cObGoodsReceipt);
    }
  }

  private Object[] constructTypesQueryParam(Map<String, String> goodsReceiptsType) {
    return new Object[]{
            goodsReceiptsType.get(IFeatureFileCommonKeys.CODE),
            goodsReceiptsType.get(IFeatureFileCommonKeys.NAME),
    };
  }

  public void assertGoodsReceiptTypesCountMatchesExpected(Integer typesNumber) {
    Long goodsReceiptsTypeCount =
            (Long)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getGoodsReceiptTypesCount");
    assertEquals(Long.valueOf(typesNumber), goodsReceiptsTypeCount);
  }

  public void assertActualResponseMatchesExpected(DataTable typesDataTable, Response response)
          throws Exception {
    List<Map<String, String>> expectedGoodsReceiptTypes =
            typesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualGoodsReceiptTypes = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedGoodsReceiptTypes.size(); i++) {
      Assertions.assertEquals(
              expectedGoodsReceiptTypes.get(i).get(IFeatureFileCommonKeys.CODE),
              actualGoodsReceiptTypes.get(i).get(ICObGoodsReceipt.CODE));

      String typeEnglishName =
              getEnglishValue(actualGoodsReceiptTypes.get(i).get(ICObGoodsReceipt.NAME));
      Assertions.assertEquals(
              expectedGoodsReceiptTypes.get(i).get(IFeatureFileCommonKeys.NAME), typeEnglishName);
    }
  }
}
