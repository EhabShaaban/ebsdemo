package com.ebs.dda.inventory.goodsreceipt;

public interface IGoodsReceiptFeatureFileCommonKeys {

  String NOTES = "Notes";
  String DELIVERY_NOTE = "DeliveryNote/PLNumber";
  String PURCHASE_RESPONSIBLE = "PurchasingResponsible";
  String VENDOR_CODE = "Vendor";
  String PURCHASE_ORDER_CODE = "POCode";
  String ACTIVATION_DATE = "ActivationDate";
  String BUSINESS_PARTNER = "BusinessPartner";
  String SALES_RETURN = "SalesReturn";
  String SALES_REPRESENTATIVE = "SalesRepresentative";
  String SALES_RETURN_DOCUMENT_OWNER = "SalesReturnDocumentOwner";
  String SALES_RETURN_CUSTOMER = "SalesReturnCustomer";
  String SALES_RETURN_QTY_BASE_UNIT = "SalesReturnQty(BaseUnit)";
  String RECEIVED_QTY_BASE = "ReceivedQty(Base)";
  String CONVERSION_FACTOR = "ConversionFactor";
  String OLD_ITEM_NUMBER = "OldItemNumber";
  String ALTERNATIVE_UNIT = "AlternativeUnit";
  String COSTING_DOCUMENT_CODE = "CostingDocumentCode";

}
