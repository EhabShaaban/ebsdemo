package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObGoodsIssueDeleteStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private DObGoodsIssueDeleteTestUtils utils;
    private CommonTestUtils commonTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        commonTestUtils = new CommonTestUtils();
        commonTestUtils.setProperties(getProperties());
        utils = new DObGoodsIssueDeleteTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObGoodsIssueDeleteStep() {
        When(
                "^\"([^\"]*)\" requests to delete GoodsIssue with code \"([^\"]*)\"$",
                (String userName, String goodsIssueCode) -> {
                    Response response =
                            utils.deleteGoodsIssues(userActionsTestUtils.getUserCookie(userName), goodsIssueCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^GoodsIssue with code \"([^\"]*)\" is deleted from the system$",
                (String goodsIssueCode) -> {
                    utils.assertThatGoodsIssueDeleted(goodsIssueCode);
                });

        Given(
                "^\"([^\"]*)\" first deleted the GoodsIssue with code \"([^\"]*)\" successfully$",
                (String userName, String goodsIssueCode) -> {
          String deleteURL = IDObGoodsIssueRestURLS.DELETE_GOODS_ISSUE + goodsIssueCode;
          Response response =
              commonTestUtils.sendDeleteRequest(
                  userActionsTestUtils.getUserCookie(userName), deleteURL);
          userActionsTestUtils.setUserResponse(userName, response);
          commonTestUtils.assertResponseSuccessStatus(response);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" section of GoodsIssue with code \"([^\"]*)\" in edit mode$",
        (String userName, String sectionName, String goodsIssueCode) -> {
          Map<String, String> sectionsLockUrlsMap = utils.getSectionsLockUrlsMap();
          String lockSectionUrl = sectionsLockUrlsMap.get(sectionName) + goodsIssueCode;

            Response lockRepponse =
                    userActionsTestUtils.lockSection(userName, lockSectionUrl, goodsIssueCode);

            utils.assertResponseSuccessStatus(lockRepponse);

            userActionsTestUtils.setUserResponse(userName, lockRepponse);
        });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete GoodsIssue -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScriptsRepeatExecution());
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "Delete GoodsIssue -")) {
            databaseConnector.closeConnection();
        }
    }
}
