package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptSaveItemTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObGoodsReceiptSaveItemAuthorizationStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private IObGoodsReceiptSaveItemTestUtils goodRecieptSaveItemTestUtils;
    private JsonObject goodsReceiptItem;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodRecieptSaveItemTestUtils = new IObGoodsReceiptSaveItemTestUtils(getProperties());
        goodRecieptSaveItemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public IObGoodsReceiptSaveItemAuthorizationStep() {

        Given(
                "^GoodsReceipts has the following values:$",
                (DataTable goodsReceiptsDataTable) -> {
                    goodRecieptSaveItemTestUtils.assertThatGoodsReceiptHasGivenPOAndVendor(
                            goodsReceiptsDataTable);
                });
        And(
        "^the following ItemVendorRecords exist:$",
        (DataTable itemVendorRecords) -> {
          goodRecieptSaveItemTestUtils.assertThatItemVendorRecordsExist(itemVendorRecords);
        });

    Given(
        "^\"([^\"]*)\" entered the following data in GR with Code \"([^\"]*)\":$",
        (String userName, String goodsReceiptCode, DataTable itemHeaderDataTable) -> {
          goodsReceiptItem =
              goodRecieptSaveItemTestUtils.createGoodsReceiptItemHeaderJsonObject(
                  itemHeaderDataTable);
        });
    When(
        "^\"([^\"]*)\" saves ordinary item to GR with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode, String saveTime) -> {
          goodRecieptSaveItemTestUtils.freeze(saveTime);

          Response saveItemResponse =
              goodRecieptSaveItemTestUtils.saveGoodsReceiptOrdinaryItemSection(
                  userActionsTestUtils.getUserCookie(userName), goodsReceiptItem, goodsReceiptCode);

          userActionsTestUtils.setUserResponse(userName, saveItemResponse);
        });
  }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Save GR Item Auth -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(
                        goodRecieptSaveItemTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(goodRecieptSaveItemTestUtils.getDbScriptPath());
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "Save GR Item Auth -")) {
            userActionsTestUtils.unlockSectionForAllUsers(IDObGoodsReceiptRestUrls.CANCEL_REQUEST_ADD_ITEM);
            goodRecieptSaveItemTestUtils.unfreeze();
            databaseConnector.closeConnection();
        }
    }
}
