package com.ebs.dda.inventory.storetransaction;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptActivateTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptCompanyViewTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptHeaderViewTestUtils;
import com.ebs.dda.inventory.storetransaction.utils.TObStoreTransactionCreateTestUtils;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSaveConsigneeTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class TObStoreTransactionCreateStep extends SpringBootRunner
    implements En, InitializingBean {

  private TObStoreTransactionCreateTestUtils utils;
  private CObEnterpriseTestUtils cobEnterpriseTestUtils;
  private DObPurchaseOrderSaveConsigneeTestUtils purchaseOrderSaveConsigneeTestUtils;
  private DObGoodsReceiptActivateTestUtils goodsReceiptPostTestUtils;
  private IObGoodsReceiptHeaderViewTestUtils goodsReceiptHeaderViewTestUtils;
  private IObGoodsReceiptCompanyViewTestUtils goodsReceiptCompanyViewTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new TObStoreTransactionCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    goodsReceiptHeaderViewTestUtils = new IObGoodsReceiptHeaderViewTestUtils(getProperties());
    goodsReceiptHeaderViewTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    goodsReceiptCompanyViewTestUtils = new IObGoodsReceiptCompanyViewTestUtils(getProperties());
    goodsReceiptCompanyViewTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    cobEnterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    purchaseOrderSaveConsigneeTestUtils =
        new DObPurchaseOrderSaveConsigneeTestUtils(getProperties());
    purchaseOrderSaveConsigneeTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    goodsReceiptPostTestUtils = new DObGoodsReceiptActivateTestUtils(getProperties());
  }

  public TObStoreTransactionCreateStep() {

    Given(
        "^the following Plants exist:$",
        (DataTable plantsDataTable) -> {
          cobEnterpriseTestUtils.assertThatPlantsExist(plantsDataTable);
        });
    Given(
        "^the following CompanyData for GoodsReceipts exist:$",
        (DataTable sompanyDataTable) -> {
            goodsReceiptCompanyViewTestUtils.assertThatGoodsReceiptHasCompany(sompanyDataTable);
        });
    Given(
        "^the following Plants are related to Company with code \"([^\"]*)\":$",
        (String companyCode, DataTable plantsDataTable) -> {
          purchaseOrderSaveConsigneeTestUtils.assertThatPlantsRelateToCompany(
              plantsDataTable, companyCode);
        });
    Given(
            "^the following GRs exist with the following GeneralData:$",
            (DataTable goodsReceiptHeaderData) -> {
                goodsReceiptHeaderViewTestUtils.assertThatGoodsReceiptExistsWithHeaderData(
                        goodsReceiptHeaderData);
            });
    Given(
            "^GoodsReceipt with code \"([^\"]*)\" has the following received items:$",
            (String goodsReceiptCode, DataTable receivedItems) -> {
                utils.assertGoodsReceiptItemsExist(goodsReceiptCode, receivedItems);
            });

    Given(
            "^Item \"([^\"]*)\" in GoodsReceipt \"([^\"]*)\" has the following received quantities:$",
            (String itemCode, String goodsReceiptCode, DataTable ordinaryItemQuantities) -> {
                utils.assertGoodsReceiptItemQuantitiesExist(
                        ordinaryItemQuantities, itemCode, goodsReceiptCode);
            });

    Given(
            "^Item \"([^\"]*)\" in GoodsReceipt \"([^\"]*)\" has the following batches:$",
            (String batchedItemCode, String goodsReceiptCode, DataTable itemBatches) -> {
                utils.assertGoodsReceiptItemBatchesExist(goodsReceiptCode, batchedItemCode, itemBatches);
            });

    Given(
        "^Last created store transaction was with code \"([^\"]*)\"$",
        (String storeTransactionCode) -> {
          utils.assertStoreTransactionExistsWithCode(storeTransactionCode);
        });

    Given(
        "^Last created store transaction is with code \"([^\"]*)\"$",
        (String storeTransactionCode) -> {
          utils.assertStoreTransactionExistsWithCode(storeTransactionCode);
        });

    When(
        "^\"([^\"]*)\" requests to post GR with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String goodsReceiptCode, String postingDate) -> {
          goodsReceiptPostTestUtils.freeze(postingDate);
          Response response =
              goodsReceiptPostTestUtils.sendActivateGoodsReceiptRequest(
                  goodsReceiptCode, userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^new StoreTransactions created as follows:$",
        (DataTable storeTransactionDataTable) -> {
          utils.assertStoreTransactionsAreCreated(storeTransactionDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Store Transaction")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Store Transaction")) {
      databaseConnector.closeConnection();
      goodsReceiptPostTestUtils.unfreeze();
    }
  }
}
