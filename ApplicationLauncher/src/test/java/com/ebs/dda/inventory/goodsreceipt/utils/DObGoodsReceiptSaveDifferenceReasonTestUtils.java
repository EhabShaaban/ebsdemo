package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObGoodsReceiptSaveDifferenceReasonTestUtils extends DObGoodsReceiptCommonTestUtils {

  public DObGoodsReceiptSaveDifferenceReasonTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
            "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");

    return str.toString();
  }

  public void assertThatGoodsReceiptIsUpdated(
      String goodsReceiptCode, DataTable expectedGoodsReceipt) {
    Map<String, String> expectedGoodsReceiptMap =
        expectedGoodsReceipt.asMaps(String.class, String.class).get(0);
    Object[] values =
        (Object[])
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptByCodeAndStateAndModifiedByInfo",
                goodsReceiptCode,
                "%" + expectedGoodsReceiptMap.get(IFeatureFileCommonKeys.STATE) + "%",
                expectedGoodsReceiptMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY));
    assertDateEquals(
            expectedGoodsReceiptMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            new DateTime(values[0]).withZone(DateTimeZone.UTC));
  }

  public Response saveItemDifferenceReason(
      Cookie userCookie, String grCode, String itemCode, DataTable itemData) {
    String restURL = IDObGoodsReceiptRestUrls.SAVE_DIFFERENCE_REASON + grCode + '/' + itemCode;
    JsonObject itemObject = createVendorGeneralDataJsonObject(itemData);
    return sendPUTRequest(userCookie, restURL, itemObject);
  }

  private JsonObject createVendorGeneralDataJsonObject(DataTable itemData) {
    JsonObject itemDataDataObject = new JsonObject();
    Map<String, String> itemDataUpdateValue = itemData.asMaps(String.class, String.class).get(0);
    itemDataDataObject.addProperty("differenceReason", itemDataUpdateValue.get("DifferenceReason"));
    return itemDataDataObject;
  }
  public void assertGoodsReceiptItemsExist(DataTable items) {

    List<Map<String, String>> expectedItemsMaps = items.asMaps(String.class, String.class);
    for (Map<String, String> expectedItems : expectedItemsMaps) {
      expectedItems = convertEmptyStringsToNulls(expectedItems);
      IObGoodsReceiptReceivedItemsGeneralModel itemsGeneralModel =
              (IObGoodsReceiptReceivedItemsGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getGoodsReceiptItemsByGRCodeAndItemNameAndType",
                              constructGoodsReceiptItemsQueryParams(expectedItems));
      assertNotNull(
              itemsGeneralModel, "Goods Receipt Item Does not exist" + expectedItems.toString());
    }
  }
  private Object[] constructGoodsReceiptItemsQueryParams(Map<String, String> expectedItems) {
    return new Object[] {
            expectedItems.get(IFeatureFileCommonKeys.GOODS_RECEIPT_CODE),
            expectedItems.get(IFeatureFileCommonKeys.ITEM),
    };
  }
}
