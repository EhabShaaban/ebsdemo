package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class DObGoodsReceiptCreateTestUtils extends DObGoodsReceiptCommonTestUtils {

  public DObGoodsReceiptCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertThatLastInsertedGoodsReceiptCodeIs(String goodsReceiptCode) throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        (DObGoodsReceiptGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLastCreatedGoodsReceipt");
    assertEquals(goodsReceiptCode, goodsReceiptGeneralModel.getUserCode());
  }

  public void assertThatGoodsReceiptIsCreatedSuccessfully(DataTable goodsReceiptTable) {

    Map<String, String> goodsReceiptMap =
        convertEmptyStringsToNulls(goodsReceiptTable.asMaps(String.class, String.class).get(0));
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        (DObGoodsReceiptGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCreatedGoodsReceipt",
                goodsReceiptMap.get(IFeatureFileCommonKeys.CODE),
                goodsReceiptMap.get(IFeatureFileCommonKeys.TYPE),
                "%" + goodsReceiptMap.get(IFeatureFileCommonKeys.STATE) + "%",
                goodsReceiptMap.get(IFeatureFileCommonKeys.CREATED_BY),
                goodsReceiptMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                goodsReceiptMap.get(IFeatureFileCommonKeys.PLANT));

    assertDateEquals(
        goodsReceiptMap.get(IFeatureFileCommonKeys.CREATION_DATE),
        goodsReceiptGeneralModel.getCreationDate());
    assertDateEquals(
            goodsReceiptMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            goodsReceiptGeneralModel.getModifiedDate());
    assertEquals(
        goodsReceiptMap.get(IFeatureFileCommonKeys.STOREKEEPER),
        goodsReceiptGeneralModel.getStorekeeperCode());
  }

  public void assertThatSalesReturnOrderExistsWithData(DataTable salesReturnDataTable)
      throws Exception {

    List<Map<String, String>> salesReturnOrdersMaps =
        salesReturnDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesReturnOrderMap : salesReturnOrdersMaps) {
      salesReturnOrderMap = convertEmptyStringsToNulls(salesReturnOrderMap);
      Object salesReturnOrder =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesReturnOrderForGRCreation",
              constructSalesReturnQueryParam(salesReturnOrderMap));
      assertNotNull(
          salesReturnOrder,
          "salesReturnOrder with code: "
              + salesReturnOrderMap.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist");
    }
  }

  private Object[] constructSalesReturnQueryParam(Map<String, String> salesReturnOrderMap) {
    return new Object[] {
      salesReturnOrderMap.get(IFeatureFileCommonKeys.CODE),
      salesReturnOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      salesReturnOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.SALES_REPRESENTATIVE),
      salesReturnOrderMap.get(IFeatureFileCommonKeys.CUSTOMER),
      salesReturnOrderMap.get(IFeatureFileCommonKeys.COMPANY),
      "%" + salesReturnOrderMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatPurchaseOrderExistsWithData(DataTable purchaseOrderTable) throws Exception {

    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersMaps) {
      purchaseOrderMap = convertEmptyStringsToNulls(purchaseOrderMap);
      Object actualPlantCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPurchaseOrderForGRCreation",
              purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
              purchaseOrderMap.get(IFeatureFileCommonKeys.TYPE),
              "%" + purchaseOrderMap.get(IFeatureFileCommonKeys.STATE) + "%",
              purchaseOrderMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
              purchaseOrderMap.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE),
              purchaseOrderMap.get(IFeatureFileCommonKeys.VENDOR));

      assertEquals(purchaseOrderMap.get(IFeatureFileCommonKeys.PLANT), actualPlantCode);
    }
  }

  public void assertThatGoodsReceiptHasNoBatchManagedItems(String goodsReceiptCode)
      throws Exception {
    List<String> purchaseOrderItemsCodes =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getGoodsReceiptItemsByReceiptCode",
            goodsReceiptCode,
            IIObGoodsReceiptReceivedItemsData.BATCHED_ITEMS_OBJECT_TYPE_CODE);
    assertTrue(purchaseOrderItemsCodes.isEmpty());
  }

  public void assertThatGoodsReceiptHasOrdinaryItems(
      String goodsReceiptCode, String ordinaryItemsObjectTypeCode, DataTable itemsTable) {
    List<Map<String, String>> expectedItems = itemsTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedItem : expectedItems) {
      IObGoodsReceiptReceivedItemsGeneralModel receiptReceivedItemsGeneralModel =
          (IObGoodsReceiptReceivedItemsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptOrdinaryItemsByGRCodeAndItemData",
                  constructOrdinaryItemsQueryParams(
                      goodsReceiptCode, ordinaryItemsObjectTypeCode, expectedItem));

      assertNotNull(
          receiptReceivedItemsGeneralModel,
          "Item with code: "
              + expectedItem.get(IFeatureFileCommonKeys.ITEM).split(" - ")[0]
              + " doesn't exist in GR with code: "
              + goodsReceiptCode);
    }
  }

  private Object[] constructOrdinaryItemsQueryParams(
      String goodsReceiptCode,
      String ordinaryItemsObjectTypeCode,
      Map<String, String> expectedItem) {
    return new Object[] {
      goodsReceiptCode,
      ordinaryItemsObjectTypeCode,
      expectedItem.get(IFeatureFileCommonKeys.ITEM),
      expectedItem.get((IFeatureFileCommonKeys.BASE_UNIT))
    };
  }

  public void assertThatGoodsReceiptHasBatchManagedItems(
      String goodsReceiptCode, String objectTypeCode, DataTable itemsTable) {

    List<Map<String, String>> expectedItems = itemsTable.asMaps(String.class, String.class);
    List<String> purchaseOrderItemsCodes =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getGoodsReceiptItemsByReceiptCode", goodsReceiptCode, objectTypeCode);
    int matchedItems = 0;
    for (Map<String, String> expectedItem : expectedItems) {
      for (String actualItemCode : purchaseOrderItemsCodes) {
        if (expectedItem.get(IFeatureFileCommonKeys.CODE).equals(actualItemCode)) {
          matchedItems++;
          break;
        }
      }
    }
    assertTrue(
        expectedItems.size() == matchedItems
            && purchaseOrderItemsCodes.size() == expectedItems.size());
  }

  public void assertThatPurchaseOrderHasItems(String purchaseOrderCode, DataTable itemsTable) {

    List<Map<String, String>> expectedItems = itemsTable.asMaps(String.class, String.class);
    List<String> purchaseOrderItemsCodes =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderItemsByOrderCode", purchaseOrderCode);
    int matchedItems = 0;
    for (Map<String, String> expectedItem : expectedItems) {
      for (String actualItemCode : purchaseOrderItemsCodes) {
        if (expectedItem.get(IFeatureFileCommonKeys.CODE).equals(actualItemCode)) {
          matchedItems++;
          break;
        }
      }
    }
    assertTrue(
        expectedItems.size() == matchedItems
            && purchaseOrderItemsCodes.size() == expectedItems.size());
  }

  public JsonObject initializeCreateJsonObject(DataTable goodsReceiptTable) {
    String refDocumentCode;
    JsonObject goodsReceiptObject = new JsonObject();
    Map<String, String> goodsReceipt = goodsReceiptTable.asMaps(String.class, String.class).get(0);
    goodsReceipt = convertEmptyStringsToNulls(goodsReceipt);
    goodsReceiptObject.addProperty(
        IDObGoodsReceiptCreationValueObject.TYPE,
        getFeatureValueIfEmpty(goodsReceipt.get(IFeatureFileCommonKeys.TYPE)));
    if (null != goodsReceipt.get(IFeatureFileCommonKeys.TYPE)
        && goodsReceipt
            .get(IFeatureFileCommonKeys.TYPE)
            .equals(IDObGoodsReceiptCreationValueObject.SALES_RETURN)) {
      refDocumentCode =
          getFeatureValueIfEmpty(goodsReceipt.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN));
    } else {
      refDocumentCode =
          getFeatureValueIfEmpty(goodsReceipt.get(IFeatureFileCommonKeys.PURCHASE_ORDER));
    }
    goodsReceiptObject.addProperty(
        IDObGoodsReceiptCreationValueObject.REF_DOCUMENT_CODE, refDocumentCode);

    goodsReceiptObject.addProperty(
        IDObGoodsReceiptCreationValueObject.STOREHOUSE,
        goodsReceipt.get(IFeatureFileCommonKeys.STOREHOUSE));
    goodsReceiptObject.addProperty(
        IDObGoodsReceiptCreationValueObject.STOREKEEPER,
        goodsReceipt.get(IFeatureFileCommonKeys.STOREKEEPER));
    goodsReceiptObject.addProperty(
        IDObGoodsReceiptCreationValueObject.COMPANY,
        goodsReceipt.get(IFeatureFileCommonKeys.COMPANY));

    return goodsReceiptObject;
  }

  public void assertThatGoodsReceiptHasCompanyData(
      String goodsReceiptCode, DataTable companyDataTable) {

    List<Map<String, String>> goodsReceiptCompanyList =
        companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptRow : goodsReceiptCompanyList) {

      IObInventoryCompanyGeneralModel iObInventoryCompanyGeneralModel =
          (IObInventoryCompanyGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedInventoryCompany",
                  getGoodsReceiptCompanyQueryParameters(goodsReceiptCode, goodsReceiptRow));
      assertNotNull(
          iObInventoryCompanyGeneralModel,
          "Company section of GR with code " + goodsReceiptCode + " doesn't exist!");
    }
  }

  private Object[] getGoodsReceiptCompanyQueryParameters(
      String goodsReceiptCode, Map<String, String> goodsReceiptRow) {
    return new Object[] {
      goodsReceiptCode,
      goodsReceiptRow.get(IFeatureFileCommonKeys.COMPANY),
      goodsReceiptRow.get(IFeatureFileCommonKeys.STOREHOUSE),
      goodsReceiptRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertThatGoodsReceiptHasPurchaseOrderData(
      String goodsReceiptCode, DataTable poDataTable) {
    List<Map<String, String>> goodsReceiptPOList = poDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptPORow : goodsReceiptPOList) {

      IObGoodsReceiptPurchaseOrderDataGeneralModel iObGoodsReceiptPurchaseOrderDataGeneralModel =
          (IObGoodsReceiptPurchaseOrderDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getUpdatedGRPurchaseOrderSection",
                  getGoodsReceiptPurchaseOrderQueryParameters(goodsReceiptCode, goodsReceiptPORow));
      assertNotNull(
          iObGoodsReceiptPurchaseOrderDataGeneralModel,
          "PO section of GR with code " + goodsReceiptCode + " doesn't exist!");

      assertOptionalParameters(goodsReceiptPORow, iObGoodsReceiptPurchaseOrderDataGeneralModel);
    }
  }

  public void assertOptionalParameters(
      Map<String, String> goodsReceiptPORow,
      IObGoodsReceiptPurchaseOrderDataGeneralModel iObGoodsReceiptPurchaseOrderDataGeneralModel) {
    if (!goodsReceiptPORow.get(IGoodsReceiptFeatureFileCommonKeys.DELIVERY_NOTE).isEmpty()) {
      assertEquals(
          goodsReceiptPORow.get(IGoodsReceiptFeatureFileCommonKeys.DELIVERY_NOTE),
          iObGoodsReceiptPurchaseOrderDataGeneralModel.getDeliveryNote());
    } else {
      Assert.assertNull(iObGoodsReceiptPurchaseOrderDataGeneralModel.getDeliveryNote());
    }
    if (!goodsReceiptPORow.get(IGoodsReceiptFeatureFileCommonKeys.NOTES).isEmpty()) {
      assertEquals(
          goodsReceiptPORow.get(IGoodsReceiptFeatureFileCommonKeys.NOTES),
          iObGoodsReceiptPurchaseOrderDataGeneralModel.getComments());
    } else {
      Assert.assertNull(iObGoodsReceiptPurchaseOrderDataGeneralModel.getComments());
    }
  }

  private Object[] getGoodsReceiptPurchaseOrderQueryParameters(
      String goodsReceiptCode, Map<String, String> goodsReceiptRow) {
    return new Object[] {
      goodsReceiptCode,
      goodsReceiptRow.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
      goodsReceiptRow.get(IFeatureFileCommonKeys.VENDOR),
      goodsReceiptRow.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE)
    };
  }

  public void assertThatSalesReturnOrdersExist(DataTable salesReturnOrderDataTable) {
    List<Map<String, String>> salesReturnOrderListMap =
        salesReturnOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesReturnOrderMap : salesReturnOrderListMap) {
      Object[] salesReturnOrderGeneralModel =
          (Object[])
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesReturnOrderForGR",
                  constructSalesReturnOrderQueryParams(salesReturnOrderMap));
      assertNotNull(
          salesReturnOrderGeneralModel,
          "Sales Return Order with code "
              + salesReturnOrderMap.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist!");
    }
  }

  private Object[] constructSalesReturnOrderQueryParams(Map<String, String> salesReturnOrderMap) {
    return new Object[] {
      salesReturnOrderMap.get(IFeatureFileCommonKeys.CODE),
      salesReturnOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      salesReturnOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.SALES_REPRESENTATIVE),
      salesReturnOrderMap.get(IFeatureFileCommonKeys.CUSTOMER),
      "%" + salesReturnOrderMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatGoodsReceiptHasSalesReturnData(
      String goodsReceiptCode, DataTable salesReturnDataTable) {
    List<Map<String, String>> goodsReceiptSalesReturnList =
        salesReturnDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptSalesReturnRow : goodsReceiptSalesReturnList) {

      IObGoodsReceiptSalesReturnDataGeneralModel iObGoodsReceiptSalesReturnDataGeneralModel =
          (IObGoodsReceiptSalesReturnDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptSalesReturnData",
                  getGoodsReceiptSalesReturnQueryParameters(
                      goodsReceiptCode, goodsReceiptSalesReturnRow));
      assertNotNull(
          iObGoodsReceiptSalesReturnDataGeneralModel,
          "Sales Return section of GR with code " + goodsReceiptCode + " doesn't exist!");
    }
  }

  private Object[] getGoodsReceiptSalesReturnQueryParameters(
      String goodsReceiptCode, Map<String, String> goodsReceiptSalesReturnRow) {
    return new Object[] {
      goodsReceiptCode,
      goodsReceiptSalesReturnRow.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN),
      goodsReceiptSalesReturnRow.get(IGoodsReceiptFeatureFileCommonKeys.SALES_REPRESENTATIVE),
      goodsReceiptSalesReturnRow.get(IFeatureFileCommonKeys.CUSTOMER)
    };
  }
}
