package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.*;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

import java.sql.SQLException;
import java.util.Map;

public class DObGoodsIssueActivateValStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsIssueActivateTestUtils goodsIssueActivateTestUtils;
  private IObGoodsIssueItemViewTestUtils goodsIssueItemViewTestUtils;
  private DObGoodsIssueViewGeneralDataTestUtils goodsIssueViewGeneralDataTestUtils;
  private IObGoodsIssueViewSalesInvoiceDataTestUtils goodsIssueViewConsigneeTestUtils;
  private IObGoodsIssueCompanyViewTestUtils goodsIssueStoreViewTestUtils;

  public DObGoodsIssueActivateValStep() {
    Then(
            "^the following items which has quantity remaining in store less than quantity in GoodsIssue with code \"([^\"]*)\" are sent to \"([^\"]*)\"$",
            (String giCode, String userName, DataTable itemsDataTable) -> {
              goodsIssueActivateTestUtils.assertThatItemHasNotEnoughRemainingQuantityExistInResponse(
                      userActionsTestUtils.getUserResponse(userName), itemsDataTable);
            });
    Given(
            "^remaining amounts of the following Items in Storehouse \"([^\"]*)\" in Plant \"([^\"]*)\" in Company \"([^\"]*)\" are:$",
            (String storeHouse, String plant, String company, DataTable itemsDataTable) -> {
              goodsIssueActivateTestUtils.assertRemainingAmountInStoreForItemIs(
                      storeHouse, plant, company, itemsDataTable);
            });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    goodsIssueActivateTestUtils = new DObGoodsIssueActivateTestUtils(properties);
    goodsIssueActivateTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    goodsIssueItemViewTestUtils = new IObGoodsIssueItemViewTestUtils(properties);
    goodsIssueItemViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    goodsIssueViewGeneralDataTestUtils = new DObGoodsIssueViewGeneralDataTestUtils(properties);
    goodsIssueViewGeneralDataTestUtils.setEntityManagerDatabaseConnector(
            entityManagerDatabaseConnector);
    goodsIssueViewConsigneeTestUtils = new IObGoodsIssueViewSalesInvoiceDataTestUtils(properties);
    goodsIssueViewConsigneeTestUtils.setEntityManagerDatabaseConnector(
            entityManagerDatabaseConnector);
    goodsIssueStoreViewTestUtils = new IObGoodsIssueCompanyViewTestUtils(properties);
    goodsIssueStoreViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate GoodsIssue Val -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
                goodsIssueActivateTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(goodsIssueActivateTestUtils.getDbScriptsRepeatExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Activate GoodsIssue Val -")) {
      goodsIssueActivateTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
