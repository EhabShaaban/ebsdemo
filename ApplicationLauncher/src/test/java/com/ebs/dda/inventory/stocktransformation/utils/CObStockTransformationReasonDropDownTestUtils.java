package com.ebs.dda.inventory.stocktransformation.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.stocktransformation.IDObStockTransformationRestURLS;
import com.ebs.dda.jpa.inventory.stocktransformation.ICObStockTransformationReason;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class CObStockTransformationReasonDropDownTestUtils extends DObStockTransformationCommonTestUtils {

    private ObjectMapper objectMapper;

    public CObStockTransformationReasonDropDownTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        setProperties(properties);
        objectMapper = new ObjectMapper();
    }

    public String getDbScriptsOneTimeExecution() {
        StringBuilder str = new StringBuilder();
        str.append("db-scripts/clearDataSqlScript.sql");
        str.append("," + "db-scripts/inventory/stock-transformation/CObStockTransformation.sql");

        return str.toString();
    }

    public String readAllStockTransformationReasons(
            String stockTransformationTypeCode, String fromStockType, String toStockType) {
        String STOCK_TRANSFORMATION_REASONS =
                IDObStockTransformationRestURLS.GET_URL
                        + "/"
                        + stockTransformationTypeCode
                        + "/stocktransformationreasons/"
                        + fromStockType
                        + "/"
                        + toStockType;
        return STOCK_TRANSFORMATION_REASONS;
    }

    public void assertResponseMatchesExpected(
            Response response, DataTable stockTransformationReasonDataTable) throws Exception {
        List<Map<String, String>> stockTransformationReasonExpectedList =
                stockTransformationReasonDataTable.asMaps(String.class, String.class);
        List<HashMap<String, Object>> stockTransformationReasonActualList =
                getListOfMapsFromResponse(response);
        for (int i = 0; i < stockTransformationReasonActualList.size(); i++) {
            assertActualAccountMatchesExpected(
                    stockTransformationReasonExpectedList.get(i), stockTransformationReasonActualList.get(i));
        }
    }

    private void assertActualAccountMatchesExpected(
            Map<String, String> expectedstockTransformationReasons,
            HashMap<String, Object> actualstockTransformationReasons)
            throws Exception {
        assertEquals(
                expectedstockTransformationReasons.get(
                        IInventoryFeatureFileCommonKeys.STOCK_TRANSFORMATION_REASON)
                        .split(" - ")[0],
                actualstockTransformationReasons.get(ICObStockTransformationReason.USER_CODE));

        String actualTransformationReasonsEnglishLocale =
                getEnglishValue(actualstockTransformationReasons.get(ICObStockTransformationReason.NAME));
        assertEquals(
                expectedstockTransformationReasons.get(
                        IInventoryFeatureFileCommonKeys.STOCK_TRANSFORMATION_REASON)
                        .split(" - ")[1],
                actualTransformationReasonsEnglishLocale);
    }

    public void assertThatAllStockTransformationReasonsExist(Integer recordsNumber) {
        Long accountsCount =
                (Long)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getStockTransformationReasonsCount");
        assertEquals(Long.valueOf(recordsNumber), accountsCount);
    }
}
