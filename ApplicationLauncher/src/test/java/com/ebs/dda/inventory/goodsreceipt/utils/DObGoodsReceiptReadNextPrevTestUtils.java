package com.ebs.dda.inventory.goodsreceipt.utils;

import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class DObGoodsReceiptReadNextPrevTestUtils extends DObGoodsReceiptCommonTestUtils {

  public DObGoodsReceiptReadNextPrevTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptCompany.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptActivationDetailsScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    return str.toString();
  }

  public Response requestToViewPreviousGoodsReceipt(Cookie userCookie, String currentCode) {
    String restURL = "/services/goodsreceipt/previous/" + currentCode;
    return sendGETRequest(userCookie, restURL);
  }

  public Response requestToViewNextGoodsReceipt(Cookie userCookie, String currentCode) {
    String restURL = "/services/goodsreceipt/next/" + currentCode;
    return sendGETRequest(userCookie, restURL);
  }
}
