package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptReadNextPrevTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObGoodReceiptReadNextPrevStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptReadNextPrevTestUtils readNextPrevTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    readNextPrevTestUtils = new DObGoodsReceiptReadNextPrevTestUtils(getProperties());
    readNextPrevTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObGoodReceiptReadNextPrevStep() {
    When(
        "^\"([^\"]*)\" requests to view next GR of current GR with code \"([^\"]*)\"$",
        (String username, String currentInstanceCode) -> {
          Response response =
              readNextPrevTestUtils.requestToViewNextGoodsReceipt(
                  userActionsTestUtils.getUserCookie(username), currentInstanceCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the next GR code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String nextVendorCode, String username) -> {
          readNextPrevTestUtils.assertThatNextInstanceCodeIsCorrect(
              nextVendorCode, userActionsTestUtils.getUserResponse(username));
        });

    When(
        "^\"([^\"]*)\" requests to view previous GR of current GR with code \"([^\"]*)\"$",
        (String username, String currentInstanceCode) -> {
          Response response =
              readNextPrevTestUtils.requestToViewPreviousGoodsReceipt(
                  userActionsTestUtils.getUserCookie(username), currentInstanceCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the previous GR code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String previousInstanceCode, String username) -> {
          readNextPrevTestUtils.assertThatPreviousInstanceCodeIsCorrect(
              previousInstanceCode, userActionsTestUtils.getUserResponse(username));
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read Next/Previous GR -")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(readNextPrevTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(readNextPrevTestUtils.getDbScriptPath());
    }
  }
}
