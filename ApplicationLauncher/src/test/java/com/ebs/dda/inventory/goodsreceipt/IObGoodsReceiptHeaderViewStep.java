package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptHeaderViewTestUtils;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class IObGoodsReceiptHeaderViewStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObGoodsReceiptHeaderViewTestUtils goodsReceiptHeaderViewTestUtils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    goodsReceiptHeaderViewTestUtils = new IObGoodsReceiptHeaderViewTestUtils(properties);
    goodsReceiptHeaderViewTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
  }

  public IObGoodsReceiptHeaderViewStep() {
    Given(
            "^the following GRs exist with the following GeneralData:$",
            (DataTable goodsReceiptHeaderData) -> {
                goodsReceiptHeaderViewTestUtils.assertThatGoodsReceiptExistsWithHeaderData(
                        goodsReceiptHeaderData);
            });

    When(
        "^\"([^\"]*)\" requests to view Header section of GR with code \"([^\"]*)\"$",
        (String username, String goodsReceiptCode) -> {
          String readUrl = goodsReceiptHeaderViewTestUtils.getViewGeneralDataUrl(goodsReceiptCode);
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response goodsReceiptHeaderResponse =
              goodsReceiptHeaderViewTestUtils.sendGETRequest(userCookie, readUrl);
          userActionsTestUtils.setUserResponse(username, goodsReceiptHeaderResponse);
        });

    Then(
        "^the following values of Header section of GR with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String code, String username, DataTable expectedGoodsReceiptHeaderResponseData) -> {
          Response goodsReceiptHeaderResponse = userActionsTestUtils.getUserResponse(username);
          goodsReceiptHeaderViewTestUtils.assertThatResponseIsCorrect(
              goodsReceiptHeaderResponse, expectedGoodsReceiptHeaderResponseData);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GR HeaderData section -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            goodsReceiptHeaderViewTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(goodsReceiptHeaderViewTestUtils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GR HeaderData section -")) {
      goodsReceiptHeaderViewTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
