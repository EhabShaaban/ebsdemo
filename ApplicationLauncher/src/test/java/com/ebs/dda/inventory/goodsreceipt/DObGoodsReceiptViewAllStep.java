package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptViewAllTestUtils;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptGeneralModel;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Locale;

public class DObGoodsReceiptViewAllStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private String requestLocale = "en";
    private DObGoodsReceiptViewAllTestUtils goodsReceiptViewAllTestUtils;
    private CObEnterpriseTestUtils enterpriseTestUtils;
    private MObVendorCommonTestUtils vendorCommonTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodsReceiptViewAllTestUtils = new DObGoodsReceiptViewAllTestUtils(getProperties());
        goodsReceiptViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        vendorCommonTestUtils = new MObVendorCommonTestUtils(getProperties());
        vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    }

    public DObGoodsReceiptViewAllStep() {
        Given(
                "^the following GoodsReceipts exist with more data:$",
                (DataTable goodsReceiptsDataTable) -> {
                    goodsReceiptViewAllTestUtils.assertGoodsReceiptExist(goodsReceiptsDataTable);
                });

        Given(
                "^the following Vendors exist:$",
                (DataTable vendorsData) -> {
                    vendorCommonTestUtils.assertThatVendorExistsByCodeAndName(vendorsData);
                });
        Given(
                "^the following PurchaseUnits exist:$",
                (DataTable purchaseUnits) -> {
                    enterpriseTestUtils.assertThatPurchaseUnitsExist(purchaseUnits);
                });
        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with (\\d+) records per page$",
                (String userName, Integer pageNumber, Integer rowsPerPage) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                                    userCookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    rowsPerPage,
                                    "");
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with filter applied on GRCode which contains \"([^\"]*)\" with (\\d+) records per page$",
                (String username, Integer pageNumber, String filter, Integer recordsPerPage) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(username);
                    String filterString =
                            goodsReceiptViewAllTestUtils.getContainsFilterField(
                                    IDObGoodsReceiptGeneralModel.USER_CODE, filter);
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                                    cookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    recordsPerPage,
                                    filterString);
                    userActionsTestUtils.setUserResponse(username, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
                (String username, Integer pageNumber, String filter, Integer recordsPerPage) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(username);
                    String filterString =
                            goodsReceiptViewAllTestUtils.getContainsFilterField(
                                    IDObGoodsReceiptGeneralModel.STATE, filter);
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                                    cookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    recordsPerPage,
                                    filterString);
                    userActionsTestUtils.setUserResponse(username, response);
                });

        Then(
                "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
                (String userName, Integer totalRecordsNumber) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    goodsReceiptViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
                            response, totalRecordsNumber);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with filter applied on StorehouseName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
                (String username,
                 Integer pageNumber,
                 String filter,
                 Integer recordsPerPage,
                 String locale) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(username);
                    String filterString =
                            goodsReceiptViewAllTestUtils.getContainsFilterField(
                                    IDObGoodsReceiptGeneralModel.STOREHOUSE_NAME, filter);
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                                    cookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    recordsPerPage,
                                    filterString,
                                    locale);
                    userActionsTestUtils.setUserResponse(username, response);

                    Locale stepLocale = Locale.forLanguageTag(locale);
                    requestLocale = stepLocale.getLanguage();
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with filter applied on GRActivationDate which equals \"([^\"]*)\" with (\\d+) records per page$",
                (String username, Integer pageNumber, String dateTime, Integer recordsPerPage) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(username);
                    String filterString =
                            goodsReceiptViewAllTestUtils.getContainsFilterField(
                                    IDObGoodsReceiptGeneralModel.ACTIVATION_DATE,
                                    String.valueOf(
                                            goodsReceiptViewAllTestUtils.getDateTimeInMilliSecondsFormat(dateTime)));
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                                    cookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    recordsPerPage,
                                    filterString);
                    userActionsTestUtils.setUserResponse(username, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with (\\d+) records per page and the following filters applied on GR while current locale is \"([^\"]*)\":$",
                (String userName,
                 Integer pageNumber,
                 Integer pageSize,
                 String locale,
                 DataTable filters) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    String fieldFilter = goodsReceiptViewAllTestUtils.getFilters(filters);
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                                    cookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    pageSize,
                                    fieldFilter,
                                    locale);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with filter applied on BusinessPartner which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
                (String userName,
                 Integer pageNumber,
                 String businessPartnerCodeContains,
                 Integer pageSize,
                 String locale) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    String filter =
                            goodsReceiptViewAllTestUtils.getContainsFilterField(
                                    IDObGoodsReceiptGeneralModel.BUSINESS_PARTNER, businessPartnerCodeContains);
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                                    cookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    pageSize,
                                    filter,
                                    locale);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of GRs with filter applied on Type which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
                (String userName,
                 Integer pageNumber,
                 String typeNameContains,
                 Integer pageSize,
                 String locale) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    String filter =
                            goodsReceiptViewAllTestUtils.getContainsFilterField(
                                    IDObGoodsReceiptGeneralModel.TYPE_NAME, typeNameContains);
                    Response response =
                            goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                                    cookie,
                                    IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT,
                                    pageNumber,
                                    pageSize,
                                    filter,
                                    locale);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
        Then(
                "^the following values will be presented to \"([^\"]*)\":$",
                (String username, DataTable goodsReceiptsDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(username);
                    goodsReceiptViewAllTestUtils.withLocale(requestLocale);
                    goodsReceiptViewAllTestUtils.assertCorrectValueAreReturnedFromDatabase(
                            goodsReceiptsDataTable, response);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "ViewAll GoodsReceipt -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(
                        goodsReceiptViewAllTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(goodsReceiptViewAllTestUtils.getDbScriptPathViewAll());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "ViewAll GoodsReceipt -")) {
            databaseConnector.closeConnection();
        }
    }
}
