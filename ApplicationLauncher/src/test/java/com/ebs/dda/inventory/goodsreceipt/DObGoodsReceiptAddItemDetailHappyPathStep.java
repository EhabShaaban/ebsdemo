package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptAddItemDetailHappyPathTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DObGoodsReceiptAddItemDetailHappyPathStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptAddItemDetailHappyPathTestUtils utils;

  public DObGoodsReceiptAddItemDetailHappyPathStep() {

    Given(
            "^GoodsReceipts \"([^\"]*)\" has the following ReceivedItems:$",
            (String goodsReceiptCode, DataTable itemsDataTable) -> {
                utils.assertGoodReceiptHasItemCodes(goodsReceiptCode, itemsDataTable);
            });

    Given(
        "^\"([^\"]*)\" requests to add Item Detail for Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" in edit mode$",
        (String userName, String itemCode, String goodsReceiptCode) -> {
          String lockURL =
              IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" saves Item Detail of type \"([^\"]*)\" in Item with code \"([^\"]*)\" to ReceivedItems section in GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String detailType,
            String itemCode,
            String goodsReceiptCode,
            String dateTime,
            DataTable detailDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String postfixUrl =
              IDObGoodsReceiptRestUrls.ADD_ITEM_DETAIL + goodsReceiptCode + "/" + itemCode;
          Response response = utils.saveItemDetail(cookie, postfixUrl, detailDataTable, detailType);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
            "^Item Detail of type \"([^\"]*)\" is added to Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" of type \"([^\"]*)\" with the following values:$",
            (String detailType,
             String itemCode,
             String goodsReceiptCode,
             String goodsReceiptType,
             DataTable dataTable) -> {
                utils.assertThatItemDetailIsAddedToGoodsReceiptReceivedItems(
                        detailType, itemCode, goodsReceiptCode, goodsReceiptType, dataTable);
            });

    Then(
        "^the lock by \"([^\"]*)\" on ReceivedItems section in GoodsReceipt with Code \"([^\"]*)\" is released$",
        (String userName, String goodsReceiptCode) -> {
          utils.assertThatLockIsReleased(
              userName, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObGoodsReceiptAddItemDetailHappyPathTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Add GR Item Detail HP -") || contains(scenario.getName(), "Add GR Item Detail Auth -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Add GR Item Detail HP -") || contains(scenario.getName(), "Add GR Item Detail Auth -")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
          new ArrayList<>(Collections.singletonList(IGoodsReceiptSectionNames.ITEMS_SECTION)));

      databaseConnector.closeConnection();
    }
  }
}
