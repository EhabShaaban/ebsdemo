package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObGoodsReceiptViewAllTestUtils extends DObGoodsReceiptCommonTestUtils {

  public static final String ACTIVE_STATE = "Active";

  public DObGoodsReceiptViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    updateRestURLs();
  }

  public void withLocale(String locale) {
    this.requestLocale = locale;
  }

  public String getDbScriptPathViewAll() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/goods-receipt/DObGoodsReceiptPurchaseOrderScript_ViewAll.sql");
    str.append(",")
            .append("db-scripts/inventory/goods-receipt/DObGoodsReceiptSalesReturnScript_ViewAll.sql");
    return str.toString();
  }

  public void assertGoodsReceiptExist(DataTable goodsReceiptsDataTable) {
    List<Map<String, String>> goodsReceiptsMap =
            goodsReceiptsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptMap : goodsReceiptsMap) {
      DObGoodsReceiptGeneralModel dObGoodsReceiptGeneralModel =
              (DObGoodsReceiptGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getGoodsReceiptViewAllData", constructQueryParam(goodsReceiptMap));
      assertNotNull(
              "GoodsReceipt with code "
                      + goodsReceiptMap.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist",
          dObGoodsReceiptGeneralModel);
      if (goodsReceiptMap.get(IFeatureFileCommonKeys.STATE).equals(ACTIVE_STATE)) {
        assertDateEquals(
                goodsReceiptMap.get(IGoodsReceiptFeatureFileCommonKeys.ACTIVATION_DATE),
                dObGoodsReceiptGeneralModel.getActivationDate());
      }
    }
  }

  private Object[] constructQueryParam(Map<String, String> goodsReceiptMap) {
    return new Object[]{
            goodsReceiptMap.get(IFeatureFileCommonKeys.CODE),
            goodsReceiptMap.get(IFeatureFileCommonKeys.TYPE).split(" - ")[0],
            goodsReceiptMap.get(IGoodsReceiptFeatureFileCommonKeys.BUSINESS_PARTNER).split(" - ")[0],
            goodsReceiptMap.get(IFeatureFileCommonKeys.STOREHOUSE).split(" - ")[0],
            "%" + goodsReceiptMap.get(IFeatureFileCommonKeys.STATE) + "%",
            goodsReceiptMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(" - ")[0]
    };
  }
}
