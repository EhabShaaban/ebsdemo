package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.IIObInventoryCompanyGeneralModel;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.junit.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class IObGoodsIssueCompanyViewTestUtils extends DObGoodsIssueCommonTestUtils {

  private ObjectMapper objectMapper;

  public IObGoodsIssueCompanyViewTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getGoodsIssueCompanyViewDataUrl(String code) {
    return IDObGoodsIssueRestURLS.BASE_URL + "/" + code + IDObGoodsIssueRestURLS.VIEW_COMPANY;
  }

  public void assertThatGoodsIssueHasCompany(DataTable storeDataTable) {

    Map<String, String> goodsIssueStoreData =
        storeDataTable.asMaps(String.class, String.class).get(0);

    Object[] queryParameters = getGoodsIssueStoreQueryParameters(goodsIssueStoreData);
    IObInventoryCompanyGeneralModel goodsIssueStoreGeneralModel =
        getIObInventoryCompany(queryParameters);
    Assert.assertNotNull(goodsIssueStoreGeneralModel);
  }

  private Object[] getGoodsIssueStoreQueryParameters(Map<String, String> goodsIssueStoreData) {
    String goodsIssueCode = goodsIssueStoreData.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE);
      String companyCodeName = goodsIssueStoreData.get(IInventoryFeatureFileCommonKeys.COMPANY);
      String storehouseCodeName =
              goodsIssueStoreData.get(IInventoryFeatureFileCommonKeys.STOREHOUSE);
    String businessUnit = goodsIssueStoreData.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    return new Object[] {goodsIssueCode, companyCodeName, storehouseCodeName, businessUnit};
  }

  private IObInventoryCompanyGeneralModel getIObInventoryCompany(Object[] parameters) {
    return (IObInventoryCompanyGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getInventoryDocumentCompanyData", parameters);
  }

  public void assertThatGoodsIssueCompanyMatchesResponse(
      DataTable storeDataTable, Response response) throws IOException, JSONException {

    Map<String, String> expectedStoreData =
        storeDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualStoreData = getMapFromResponse(response);

    assertThatGoodsIssueStoreDataMatches(expectedStoreData, actualStoreData);
  }

  private void assertThatGoodsIssueStoreDataMatches(
      Map<String, String> expectedStoreData, HashMap<String, Object> actualStoreData)
      throws IOException, JSONException {

    assertEquals(
            expectedStoreData.get(IInventoryFeatureFileCommonKeys.COMPANY),
            getEnglishLocaleFromLocalizedResponse(
                    objectMapper.writeValueAsString(
                            actualStoreData.get(IIObInventoryCompanyGeneralModel.COMPANY_NAME))));

    assertEquals(
        expectedStoreData.get(IInventoryFeatureFileCommonKeys.STOREHOUSE),
        getEnglishLocaleFromLocalizedResponse(
            objectMapper.writeValueAsString(
                actualStoreData.get(IIObInventoryCompanyGeneralModel.STOREHOUSE_NAME))));
    assertEquals(
        expectedStoreData.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        getEnglishLocaleFromLocalizedResponse(
            objectMapper.writeValueAsString(
                actualStoreData.get(IIObInventoryCompanyGeneralModel.PURCHASING_UNIT_NAME))));
  }
}
