package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;

import java.util.Map;

import static org.junit.Assert.assertNull;

public class DObGoodsReceiptActivateValTestUtils extends DObGoodsReceiptActivateTestUtils {

  public DObGoodsReceiptActivateValTestUtils(Map<String, Object> properties) {
    super(properties);
  }


  public void assertGoodsReceiptSalesReturnItemHasNoQuantities(
          String goodsReceiptCode, String item) {

      IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel itemQuantities =
              (IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getGoodsReceiptSalesReturnItemsQuantities",
                              constructItemQuantitiesQueryParams(
                                      goodsReceiptCode, item));

      assertNull(itemQuantities);
    }

  private Object[] constructItemQuantitiesQueryParams(
          String goodsReceiptCode, String item) {
    return new Object[] {
            goodsReceiptCode,
            item.split(" - ")[0]
    };
  }
}
