package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNull;

public class IObGoodsReceiptDeleteItemTestUtils extends IObGoodsReceiptItemTestUtils {

  private static final String DELETE = "/delete/";

  public IObGoodsReceiptDeleteItemTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response deleteItem(String itemCode, String goodsReceiptCode, Cookie loginCookie) {
      String deleteURL =
              IDObGoodsReceiptRestUrls.COMMAND_GOODS_RECEIPT + goodsReceiptCode + DELETE + itemCode;
      return sendDeleteRequest(loginCookie, deleteURL);
  }

  public void assertNoItemExistsWithCode(String itemCode, String goodsReceiptCode)
      throws Exception {
      IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptReceivedItemsGeneralModel =
              (IObGoodsReceiptReceivedItemsGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getReceivedItemsByCodeAndGoodsReceiptCode", itemCode,goodsReceiptCode);
      assertNull(goodsReceiptReceivedItemsGeneralModel, itemCode + " exist!");
  }
}
