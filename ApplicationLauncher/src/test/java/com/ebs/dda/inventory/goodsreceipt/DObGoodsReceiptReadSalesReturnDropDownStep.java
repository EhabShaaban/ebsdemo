package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptCreateTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptReadSalesReturnDropDownTestUtils;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObGoodsReceiptReadSalesReturnDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptReadSalesReturnDropDownTestUtils utils;
  private DObGoodsReceiptCreateTestUtils goodsReceiptCreateTestUtils;

  public DObGoodsReceiptReadSalesReturnDropDownStep() {

    Given(
        "^the following are ALL existing SalesReturnOrders:$",
        (DataTable salesReturnOrderDataTable) -> {
          goodsReceiptCreateTestUtils.assertThatSalesReturnOrdersExist(salesReturnOrderDataTable);
        });

    Given(
        "^the total number of existing records of SalesReturnOrders is (\\d+)$",
        (Long totalRecordsNumber) -> {
            utils.assertThatTotalNumberOfSalesReturnOrdersEquals(totalRecordsNumber);
        });

    When(
            "^\"([^\"]*)\" requests to read drop-down of SalesReturnOrder for State \\(Shipped | CreditNoteActivated\\)$",
            (String userName) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                Response response =
                        utils.sendGETRequest(cookie, IDObSalesReturnOrderRestURLs.SALES_RETURN_ORDER_FOR_GR);
                userActionsTestUtils.setUserResponse(userName, response);
            });

      Then(
              "^the following SalesReturnOrders will be presented to \"([^\"]*)\":$",
              (String userName, DataTable salesReturnOrderDataTable) -> {
                  Response response = userActionsTestUtils.getUserResponse(userName);
                  utils.assertResponseSuccessStatus(response);
                  utils.assertSalesReturnOrderDataIsCorrect(salesReturnOrderDataTable, response);
              });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObGoodsReceiptReadSalesReturnDropDownTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        goodsReceiptCreateTestUtils = new DObGoodsReceiptCreateTestUtils(getProperties());
        goodsReceiptCreateTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of SalesReturnOrders dropdown for GR")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
        }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read list of SalesReturnOrders dropdown for GR")) {
      databaseConnector.closeConnection();
    }
  }
}
