package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObGoodsReceiptReadSalesReturnDropDownTestUtils
    extends DObGoodsReceiptCommonTestUtils {
  public DObGoodsReceiptReadSalesReturnDropDownTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }
  public DObGoodsReceiptReadSalesReturnDropDownTestUtils() {
  }

  public void assertThatTotalNumberOfSalesReturnOrdersEquals(Long totalRecordsNumber) {
    Object expectedTotalNumberOfSalesReturnOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getTotalNumberOfSalesReturnOrderViewAll");

    assertEquals(totalRecordsNumber, expectedTotalNumberOfSalesReturnOrder);
  }

  public void assertSalesReturnOrderDataIsCorrect(
      DataTable salesReturnOrderDataTable, Response response) {

    List<Map<String, String>> expectedSalesReturnOrderList =
        salesReturnOrderDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualSalesReturnOrderResponse =
        getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedSalesReturnOrderList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(
              expectedSalesReturnOrderList.get(i), actualSalesReturnOrderResponse.get(i));

      mapAssertion.assertValue(
          IFeatureFileCommonKeys.CODE, IDObSalesReturnOrderGeneralModel.USER_CODE);
    }
  }
}
