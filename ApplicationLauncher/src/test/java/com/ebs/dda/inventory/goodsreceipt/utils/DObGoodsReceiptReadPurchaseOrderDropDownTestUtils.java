package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObGoodsReceiptReadPurchaseOrderDropDownTestUtils
    extends DObGoodsReceiptCommonTestUtils {
  public DObGoodsReceiptReadPurchaseOrderDropDownTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    return str.toString();
  }

  public void assertPurchaseOrdersExist(DataTable purchaseOrderDataTable) {
    List<Map<String, String>> expectedPurchaseOrderList =
        purchaseOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : expectedPurchaseOrderList) {
      DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
          (DObPurchaseOrderGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrderData", constructPurchaseOrderQueryParams(purchaseOrder));
      assertNotNull(
          "PurchaseOrder with code "
              + purchaseOrder.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist",
          purchaseOrderGeneralModel);
    }
  }

  private Object[] constructPurchaseOrderQueryParams(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.TYPE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      "%" + purchaseOrderMap.get(IFeatureFileCommonKeys.STATE) + "%",
      purchaseOrderMap.get(IFeatureFileCommonKeys.CREATED_BY),
      purchaseOrderMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
    };
  }

  public void assertThatTotalNumberOfPurchaseOrdersEquals(Long totalRecordsNumber) {
    Long totalNumberOfPurchaseOrder =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getTotalNumberOfPurchaseOrderViewAll");

    assertEquals(totalRecordsNumber, totalNumberOfPurchaseOrder);
  }

  public void assertPurchaseOrderResponseIsCorrect(
      DataTable purchaseOrderDataTable, Response response) {

    List<Map<String, String>> expectedPurchaseOrderList =
        purchaseOrderDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualPurchaseOrderResponse = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedPurchaseOrderList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedPurchaseOrderList.get(i), actualPurchaseOrderResponse.get(i));

      mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObPurchaseOrderGeneralModel.CODE);
    }
  }
}
