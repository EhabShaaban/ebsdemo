package com.ebs.dda.inventory.storetransaction;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.storetransaction.utils.TObStoreTransactionViewAllTestUtils;
import com.ebs.dda.jpa.inventory.storetransaction.ITObStoreTransactionGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class TObStoreTransactionViewAllStep extends SpringBootRunner
        implements En, InitializingBean {

    private static boolean hasBeenExecuted = false;
    private TObStoreTransactionViewAllTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new TObStoreTransactionViewAllTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public TObStoreTransactionViewAllStep() {

        Given(
                "^the ONLY StoreTransactions with View All data exist:$",
                (DataTable dataTable) -> {
                    utils.assertThatOnlyTheFollowingStoreTransactionsExist(dataTable);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with no filter applied with (\\d+) records per page$",
                (String userName, Integer pageNumber, Integer recordsPerPage) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadStoreTransactionsWithFiler(
                  userCookie, pageNumber, recordsPerPage, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable storeTransactionsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatStoreTransactionsArePresentedToUser(response, storeTransactionsDataTable);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer numberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, numberOfRecords);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on TransactionCode which contains \"([^\"]*)\" with (\\d+) records per page$",
            (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getContainsFilterField(
                                ITObStoreTransactionGeneralModel.ST_CODE, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFiler(
                                cookie, pageNumber, recordsPerPage, filterString);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on TransactionType which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            String filterString =
                    utils.getEqualsFilterField(
                            ITObStoreTransactionGeneralModel.TRANSACTION_OPERATION, filteringValue);
            Response response =
                    utils.requestToReadStoreTransactionsWithFiler(
                            cookie, pageNumber, recordsPerPage, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on Company which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            String filterString =
                    utils.getEqualsFilterField(
                            ITObStoreTransactionGeneralModel.COMPANY_CODE, filteringValue);
            Response response =
                    utils.requestToReadStoreTransactionsWithFiler(
                            cookie, pageNumber, recordsPerPage, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
            "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on Plant which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
            (String userName,
             Integer pageNumber,
             String filteringValue,
             Integer recordsPerPage,
             String locale) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getContainsFilterField(
                                ITObStoreTransactionGeneralModel.PLANT_NAME, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFilerAndLocale(
                                cookie, pageNumber, recordsPerPage, filterString, locale);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
            "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on Storehouse which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
            (String userName,
             Integer pageNumber,
             String filteringValue,
             Integer recordsPerPage,
             String locale) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getContainsFilterField(
                                ITObStoreTransactionGeneralModel.STOREHOUSE_NAME, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFilerAndLocale(
                                cookie, pageNumber, recordsPerPage, filterString, locale);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            String filterString =
                    utils.getEqualsFilterField(
                            ITObStoreTransactionGeneralModel.PURCHASEUNIT_CODE, filteringValue);
            Response response =
                    utils.requestToReadStoreTransactionsWithFiler(
                            cookie, pageNumber, recordsPerPage, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on StockType which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            String filterString =
                    utils.getEqualsFilterField(
                            ITObStoreTransactionGeneralModel.STOCK_TYPE, filteringValue);
            Response response =
                    utils.requestToReadStoreTransactionsWithFiler(
                            cookie, pageNumber, recordsPerPage, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
            "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on Item which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
            (String userName,
             Integer pageNumber,
             String filteringValue,
             Integer recordsPerPage,
             String locale) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getContainsFilterField(ITObStoreTransactionGeneralModel.ITEM, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFilerAndLocale(
                                cookie, pageNumber, recordsPerPage, filterString, locale);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
            "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on UnitOfMeasureName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
            (String userName,
             Integer pageNumber,
             String filteringValue,
             Integer recordsPerPage,
             String locale) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getContainsFilterField(
                                ITObStoreTransactionGeneralModel.UOM_NAME, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFilerAndLocale(
                                cookie, pageNumber, recordsPerPage, filterString, locale);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on Quantity which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getEqualsFilterField(ITObStoreTransactionGeneralModel.QUANTITY, filteringValue);
          Response response =
              utils.requestToReadStoreTransactionsWithFiler(
                  cookie, pageNumber, recordsPerPage, filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
            "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on EstimateCost which equals \"([^\"]*)\" with (\\d+) records per page$",
            (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getEqualsFilterField(ITObStoreTransactionGeneralModel.ESTIMATE_COST, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFiler(
                                cookie, pageNumber, recordsPerPage, filterString);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
            "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on ActualCost which equals \"([^\"]*)\" with (\\d+) records per page$",
            (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getEqualsFilterField(
                                ITObStoreTransactionGeneralModel.ACTUAL_COST, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFiler(
                                cookie, pageNumber, recordsPerPage, filterString);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
            "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on RefDocument which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
            (String userName,
             Integer pageNumber,
             String filteringValue,
             Integer recordsPerPage,
             String locale) -> {
                Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                String filterString =
                        utils.getContainsFilterField(
                                ITObStoreTransactionGeneralModel.REF_DOCUMENT, filteringValue);
                Response response =
                        utils.requestToReadStoreTransactionsWithFilerAndLocale(
                                cookie, pageNumber, recordsPerPage, filterString, locale);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on RemainingQuantity which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsPerPage) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            String filterString =
                    utils.getEqualsFilterField(
                            ITObStoreTransactionGeneralModel.REMAINING_QUANTITY, filteringValue);
            Response response =
                    utils.requestToReadStoreTransactionsWithFiler(
                            cookie, pageNumber, recordsPerPage, filterString);
            userActionsTestUtils.setUserResponse(userName, response);
        });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of StoreTransactions with filter applied on OriginalAddingDate which equals \"([^\"]*)\" with (\\d+) records per page$",
                (String userName, Integer pageNumber, String dateTime, Integer recordsPerPage) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    String filterString =
                            utils.getContainsFilterField(
                                    ITObStoreTransactionGeneralModel.ORIGINAL_ADDING_DATE,
                                    String.valueOf(utils.getDateTimeInMilliSecondsFormat(dateTime)));
                    Response response =
                            utils.requestToReadStoreTransactionsWithFiler(
                                    cookie, pageNumber, recordsPerPage, filterString);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
    }

  @Before
  public void before(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "View All StoreTransactions")) {
          databaseConnector.createConnection();
          if (!hasBeenExecuted) {
              databaseConnector.executeSQLScript(utils.getViewAllDbScriptPath());
              hasBeenExecuted = true;
          }
      }
  }

  @After
  public void after(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "View All StoreTransactions")) {
          databaseConnector.closeConnection();
      }
  }
}
