package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
import cucumber.api.DataTable;

import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DObGoodsReceiptAddItemDetailHappyPathTestUtils extends IObGoodsReceiptItemTestUtils {

    public static final String BATCH = "Batch";
    private final String GR_PO = "PURCHASE_ORDER";

    public DObGoodsReceiptAddItemDetailHappyPathTestUtils(Map<String, Object> properties) {
        super(properties);
    }

    public void assertThatItemDetailIsAddedToGoodsReceiptReceivedItems(
            String detailType,
            String itemCode,
            String goodsReceiptCode,
            String goodsReceiptType,
            DataTable detailDataTable) {
        Map<String, String> detailMap = detailDataTable.asMaps(String.class, String.class).get(0);

        if (goodsReceiptType.equals(GR_PO)) {
            if (detailType.equals(BATCH)) {
                IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel batch =
                        (IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel)
                                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                        "getItemAddedBatch",
                                        constructItemBatchesQueryParams(itemCode, goodsReceiptCode, detailMap));
                assertNotNull(batch);
            } else {
                IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel quantity =
                        (IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel)
                                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                        "getGoodsReceiptPurchaseOrderItemAddedQuantity",
                                        constructItemQuantityQueryParams(itemCode, goodsReceiptCode, detailMap));
                assertNotNull(quantity);
            }
        } else {
            IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel quantity =
                    (IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "getGoodsReceiptSalesReturnItemAddedQuantity",
                                    constructItemQuantityQueryParams(itemCode, goodsReceiptCode, detailMap));
            assertNotNull(
                    "Quantity on item "
                            + itemCode
                            + " with UOM "
                            + detailMap.get(IFeatureFileCommonKeys.UOE)
                            + " and StockType "
                            + detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE)
                            + " doesn't exist!",
                    quantity);
        }
        assertTrue("Goods Receipt Item Detail has been added", true);
    }

    private Object[] constructItemQuantityQueryParams(
            String itemCode, String goodsReceiptCode, Map<String, String> detailMap) {
        return new Object[]{
                detailMap.get(IFeatureFileCommonKeys.CREATED_BY),
                detailMap.get(IFeatureFileCommonKeys.CREATION_DATE),
                detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
                detailMap.get(IFeatureFileCommonKeys.UOE),
                detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
                detailMap.get(IGoodsReceiptFeatureFileCommonKeys.NOTES),
                itemCode,
                goodsReceiptCode
        };
    }

    private Object[] constructItemBatchesQueryParams(
            String itemCode, String goodsReceiptCode, Map<String, String> detailMap) {
        return new Object[]{
                detailMap.get(IFeatureFileCommonKeys.CREATED_BY),
                detailMap.get(IFeatureFileCommonKeys.CREATION_DATE),
                detailMap.get(IFeatureFileCommonKeys.BATCH_NUMBER),
                detailMap.get(IFeatureFileCommonKeys.PRODUCTION_DATE),
                detailMap.get(IFeatureFileCommonKeys.EXPIRATION_DATE),
                detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
                detailMap.get(IFeatureFileCommonKeys.UOE),
                detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
                detailMap.get(IGoodsReceiptFeatureFileCommonKeys.NOTES),
                itemCode,
                goodsReceiptCode
        };
    }
}
