package com.ebs.dda.inventory.storetransaction.utils;

public interface IStoreTransactionFileCommonKeys {

    String UOM = "UOM";
    String ESTIMATE_COST = "EstimateCost";
    String TRANSACTION_TYPE = "TransactionType";
    String ACTUAL_COST = "ActualCost";
    String REFERENCE_DOCUMENT = "ReferenceDocument";
    String REFERENCE_DOCUMENT_TYPE = "ReferenceDocumentType";
    String REMAINING_QTY = "RemainingQty";
    String REF_TRANSACTION = "RefTransaction";
    String TOTAL_RECEIVED_QTY = "TotalReceivedQty(Base)";
    String RECEIVED_QTY_BASE = "ReceivedQty(Base)";
    String RECEIVED_QTY = "ReceivedQty(UoE)";
    String BATCHED_QTY = "BatchQty(UoE)";
    String ST_CODE = "STCode";
    String STOCK_TYPE = "StockType";
    String ORIGINAL_ADDING_DATE = "OriginalAddingDate";
}
