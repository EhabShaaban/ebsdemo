package com.ebs.dda.inventory.stocktransformation;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationAllowedActionsTestUtils;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class DObStockTransformationAllowedActionsStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private DObStockTransformationAllowedActionsTestUtils utils;
    private DObStockTransformationViewAllTestUtils stockTransformationViewAllTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new DObStockTransformationAllowedActionsTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

        stockTransformationViewAllTestUtils = new DObStockTransformationViewAllTestUtils(properties);
        stockTransformationViewAllTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }

    public DObStockTransformationAllowedActionsStep() {

        Given(
                "^the following StockTransformations exist:$",
                (DataTable stockTransformationsDataTable) -> {
                    stockTransformationViewAllTestUtils.assertThatStockTransformationsExits(
                            stockTransformationsDataTable);
                });

        When(
                "^\"([^\"]*)\" requests to read actions of StockTransformation in home screen$",
                (String userName) -> {
                    String url = IDObStockTransformationRestURLS.AUTHORIZED_ACTIONS;
                    Response response =
                            utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following actions are displayed to \"([^\"]*)\":$",
                (String userName, DataTable allowedactionsDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatTheFollowingActionsExist(response, allowedactionsDataTable);
                });

        When(
                "^\"([^\"]*)\" requests to read actions of StockTransformation with code \"([^\"]*)\"$",
                (String username, String stockTransformationCode) -> {
                    String url =
                            IDObStockTransformationRestURLS.AUTHORIZED_ACTIONS + '/' + stockTransformationCode;
                    Response response =
                            utils.sendGETRequest(userActionsTestUtils.getUserCookie(username), url);
                    userActionsTestUtils.setUserResponse(username, response);
                });

        Then(
                "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
                (String allowedActions, String username) -> {
                    Response response = userActionsTestUtils.getUserResponse(username);
                    utils.assertThatTheFollowingActionsExist(response, allowedActions);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read allowed actions in StockTransformation")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(utils.getDbScriptPath());
                hasBeenExecuted = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read allowed actions in StockTransformation")) {
            databaseConnector.closeConnection();
        }
    }
}
