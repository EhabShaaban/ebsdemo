package com.ebs.dda.inventory.stocktransformation.utils;

import java.util.Map;

public class DObStockTransformationAllowedActionsTestUtils
    extends DObStockTransformationCommonTestUtils {

  public DObStockTransformationAllowedActionsTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append("," + "db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
      str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
      str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append("," + "db-scripts/inventory/stock-transformation/CObStockTransformation.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");
    str.append("," + "db-scripts/inventory/stock-transformation/DObStockTransformationScript.sql");
    str.append(
        "," + "db-scripts/inventory/store-transaction/TObStockTransformationStoreTransaction.sql");
    str.append(
        "," + "db-scripts/inventory/stock-transformation/IObStockTransformationDetailsScript.sql");
    return str.toString();
  }
}
