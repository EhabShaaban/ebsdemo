package com.ebs.dda.inventory.initialstockupload.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/inventory/initial-stock-upload/ISU_ViewAll.feature",
      "classpath:features/inventory/initial-stock-upload/ISU_Create_HP.feature",
      "classpath:features/inventory/initial-stock-upload/ISU_Create_Auth.feature",
      "classpath:features/inventory/initial-stock-upload/ISU_AllowedActions_HomeScreen.feature",
      "classpath:features/inventory/initial-stock-upload/ISU_AllowedActions_ObjectScreen.feature",
      "classpath:features/inventory/initial-stock-upload/ISU_Activate_HP.feature",
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.inventory.initialstockupload"},
    strict = true,
    tags = "not @Future")
public class DObInitialStockUploadCucumberRunner {}
