package com.ebs.dda.inventory.initialstockupload.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.inventory.initialstockupload.IDObInitialStockUploadRestURLS;
import com.ebs.dda.inventory.storetransaction.utils.IStoreTransactionFileCommonKeys;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUploadGeneralModel;
import com.ebs.dda.jpa.inventory.initialstockupload.IDObInitialStockUploadCreateValueObject;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObInitialStockUploadCreateTestUtils extends DObInitialStockUploadCommonTestUtils {

  private final ObjectMapper objectMapper;

  public DObInitialStockUploadCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsPathOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    return str.toString();
  }

  public void assertLastCreatedInitialStockUpload(String initialStockUploadCode) {
    DObInitialStockUploadGeneralModel initialStockUploadGeneralModel =
        (DObInitialStockUploadGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLastCreatedInitialStockUpload", initialStockUploadCode);
    Assert.assertEquals(initialStockUploadGeneralModel.getUserCode(), initialStockUploadCode);
  }

  public Response createInitialStockUpload(DataTable initialStockUploadDataTable, Cookie userCookie)
      throws Exception {
    Map<String, String> initialStockUpload =
        initialStockUploadDataTable.asMaps(String.class, String.class).get(0);
    JsonObject initialStockUploadRequest = prepareInitialStockUploadJsonObject(initialStockUpload);
    String restURL = IDObInitialStockUploadRestURLS.CREATE_INITIAL_STOCK_UPLOAD;
    File attachment = getAttachment(initialStockUpload.get(IFeatureFileCommonKeys.ATTACHMENT));
    return sendPostRequestWithAttachment(
        userCookie, restURL, attachment, initialStockUploadRequest);
  }

  private File getAttachment(String fileName) {
    return new File("src/test/resources/files/" + fileName);
  }

  private JsonObject prepareInitialStockUploadJsonObject(Map<String, String> initialStockUpload) {
    initialStockUpload = convertDoubleQuotesToEmptyStrings(initialStockUpload);
    JsonObject initialStockUploadJsonObject = new JsonObject();

    addValueToJsonObject(
        initialStockUploadJsonObject,
        IDObInitialStockUploadCreateValueObject.PURCHASE_UNIT,
        checkIfNull(initialStockUpload.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT))
            ? null
            : initialStockUpload.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT)
                .split(" - ")[0]);

    addValueToJsonObject(
        initialStockUploadJsonObject,
        IDObInitialStockUploadCreateValueObject.STORE_KEEPER,
        checkIfNull(initialStockUpload.get(IFeatureFileCommonKeys.STOREKEEPER))
            ? null
            : initialStockUpload.get(IFeatureFileCommonKeys.STOREKEEPER).split(" - ")[0]);

    addValueToJsonObject(
        initialStockUploadJsonObject,
        IDObInitialStockUploadCreateValueObject.COMPANY,
        checkIfNull(initialStockUpload.get(IFeatureFileCommonKeys.COMPANY))
            ? null
            : initialStockUpload.get(IFeatureFileCommonKeys.COMPANY).split(" - ")[0]);
    addValueToJsonObject(
        initialStockUploadJsonObject,
        IDObInitialStockUploadCreateValueObject.STORE_HOUSE,
        checkIfNull(initialStockUpload.get(IFeatureFileCommonKeys.STOREHOUSE))
            ? null
            : initialStockUpload.get(IFeatureFileCommonKeys.STOREHOUSE).split(" - ")[0]);
    return initialStockUploadJsonObject;
  }

  public void assertInitialStockUploadIsCreatedSuccessfully(DataTable initialStockUploadDataTable) {

    List<Map<String, String>> initialStockUploadList =
        initialStockUploadDataTable.asMaps(String.class, String.class);
    for (Map<String, String> initialStockUploadRow : initialStockUploadList) {
      DObInitialStockUploadGeneralModel initialStockUploadGeneralModel =
          (DObInitialStockUploadGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedInitialStockUpload",
                  constructInitialStockUploadQueryParams(initialStockUploadRow));
      assertNotNull(initialStockUploadGeneralModel);

      assertDateEquals(
          initialStockUploadRow.get(IFeatureFileCommonKeys.CREATION_DATE),
          initialStockUploadGeneralModel.getCreationDate());
      assertDateEquals(
          initialStockUploadRow.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
          initialStockUploadGeneralModel.getModifiedDate());
    }
  }

  private Object[] constructInitialStockUploadQueryParams(
      Map<String, String> initialStockUploadRow) {
    return new Object[] {
      initialStockUploadRow.get(IFeatureFileCommonKeys.CODE),
      '%' + initialStockUploadRow.get(IFeatureFileCommonKeys.STATE) + '%',
      initialStockUploadRow.get(IFeatureFileCommonKeys.CREATED_BY),
      initialStockUploadRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      initialStockUploadRow.get(IFeatureFileCommonKeys.STOREKEEPER)
    };
  }

  public void assertInitialStockUploadCompanyIsCreatedSuccessfully(
      String initialStockUploadCode, DataTable initialStockUploadDataTable) {

    List<Map<String, String>> initialStockUploadList =
        initialStockUploadDataTable.asMaps(String.class, String.class);
    for (Map<String, String> initialStockUploadRow : initialStockUploadList) {

      IObInventoryCompanyGeneralModel iObInventoryCompanyGeneralModel =
          (IObInventoryCompanyGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedInventoryCompany",
                  constructInitialStockUploadCompanyQueryParams(
                      initialStockUploadCode, initialStockUploadRow));
      assertNotNull(iObInventoryCompanyGeneralModel);
    }
  }

  private Object[] constructInitialStockUploadCompanyQueryParams(
      String initialStockUploadCode, Map<String, String> initialStockUploadRow) {
    return new Object[] {
      initialStockUploadCode,
      initialStockUploadRow.get(IFeatureFileCommonKeys.COMPANY).split(" - ")[0],
      initialStockUploadRow.get(IFeatureFileCommonKeys.STOREHOUSE).split(" - ")[0],
      initialStockUploadRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(" - ")[0]
    };
  }

  public boolean checkIfNull(String str) {
    return (str == null);
  }

  public void insertLastCreatedInitialStockUploadCode(String initialStockUploadCode) {
    entityManagerDatabaseConnector.executeInsertQueryForObject(
        "insertLastCreatedInitialStockUploadCode", initialStockUploadCode);
  }

  public void assertThatInitialStockItemsAreCreatedSuccessfully(
      String userCode, DataTable itemsDataTable) {
    List<Map<String, String>> itemsMapList = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> itemMap : itemsMapList) {
      Object itemOriginalAddingDate =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCreatedISUItem", constructISUItemQueryParams(userCode, itemMap));
      assertNotNull(itemOriginalAddingDate);
      assertDateEqualsWithoutTime(
          itemMap.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE),
          new DateTime(itemOriginalAddingDate).withZone(DateTimeZone.UTC));
    }
  }

  private Object[] constructISUItemQueryParams(String userCode, Map<String, String> itemMap) {
    return new Object[] {
      userCode,
      itemMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
      itemMap.get(IFeatureFileCommonKeys.ITEM),
      itemMap.get(IFeatureFileCommonKeys.UOM),
      Double.parseDouble(itemMap.get(IFeatureFileCommonKeys.QTY)),
      getCostInDouble(itemMap.get(IFeatureFileCommonKeys.ESTIMATE_COST)),
      getCostInDouble(itemMap.get(IFeatureFileCommonKeys.ACTUAL_COST)),
      Double.parseDouble(itemMap.get(IFeatureFileCommonKeys.REMAINING_QTY)),
    };
  }
}
