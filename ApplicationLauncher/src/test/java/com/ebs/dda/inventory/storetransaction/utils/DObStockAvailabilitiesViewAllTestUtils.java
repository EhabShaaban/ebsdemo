package com.ebs.dda.inventory.storetransaction.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.stockavailabilities.ICObStockAvailabilitiyGeneralModel;
import cucumber.api.DataTable;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.path.json.config.JsonPathConfig.NumberReturnType.BIG_DECIMAL;

public class DObStockAvailabilitiesViewAllTestUtils extends CommonTestUtils {

  public DObStockAvailabilitiesViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String clearStockAvailabilities() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/stockavailability/stockavailabilityClear.sql");
    return str.toString();
  }

  public Response requestToReadFilteredData(
      Cookie cookie,
      String prefixUrl,
      Integer pageNumber,
      Integer totalRecordsNumber,
      String fieldFilter) {
    String filteredReadUrl =
        constructReadUrl(prefixUrl, pageNumber, totalRecordsNumber, fieldFilter);
    return sendGETRequest(cookie, filteredReadUrl);
  }

  private String constructReadUrl(
      String prefixUrl, Integer pageNumber, Integer recordsPerPage, String filterString) {
    return prefixUrl
        + "/page="
        + (pageNumber - 1)
        + "/rows="
        + recordsPerPage
        + "/filters="
        + filterString;
  }

  public Response requestToReadFilteredDataWithLocale(
      Cookie cookie,
      String prefixUrl,
      Integer pageNumber,
      Integer totalRecordsNumber,
      String fieldFilter,
      String locale) {
    String filteredReadUrl =
        constructReadUrl(prefixUrl, pageNumber, totalRecordsNumber, fieldFilter);
    return sendGETRequestWithLocale(cookie, locale, filteredReadUrl);
  }

  public Response sendGETRequest(Cookie loginCookie, String restURL) {
    return given()
        .cookie(loginCookie)
        .config(RestAssured.config().jsonConfig(jsonConfig().numberReturnType(BIG_DECIMAL)))
        .contentType(ContentType.JSON)
        .get(urlPrefix + restURL);
  }

  public Response sendGETRequestWithLocale(Cookie loginCookie, String locale, String restURL) {
    return given()
        .cookie(loginCookie)
        .config(RestAssured.config().jsonConfig(jsonConfig().numberReturnType(BIG_DECIMAL)))
        .contentType(ContentType.JSON)
        .header("Accept-Language", locale)
        .get(urlPrefix + restURL);
  }

  public void assertThatStockAvailabilitiesIsMatch(
      Response response, DataTable stockAvailabilitiesDataTable) throws Exception {
    List<HashMap<String, Object>> actualStockAvailability = getListOfMapsFromResponse(response);
    List<Map<String, String>> expectedStockAvailabilityMaps =
        stockAvailabilitiesDataTable.asMaps(String.class, String.class);
    for (int i = 0; i < actualStockAvailability.size(); i++) {
      assertThatStockAvailabilityMatchesTheExpected(
          expectedStockAvailabilityMaps.get(i), actualStockAvailability.get(i));
    }
  }

  private void assertThatStockAvailabilityMatchesTheExpected(
      Map<String, String> expectedStockAvailabilityMap,
      HashMap<String, Object> actualStockAvailabilityMap)
      throws Exception {
    MapAssertion mapAssertion =
        new MapAssertion(expectedStockAvailabilityMap, actualStockAvailabilityMap);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.ITEM,
        ICObStockAvailabilitiyGeneralModel.ITEM_CODE,
        ICObStockAvailabilitiyGeneralModel.ITEM_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IStoreTransactionFileCommonKeys.UOM,
        ICObStockAvailabilitiyGeneralModel.UOM_CODE,
        ICObStockAvailabilitiyGeneralModel.UOM_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PURCHASE_UNIT,
        ICObStockAvailabilitiyGeneralModel.PURCHASEUNIT_CODE,
        ICObStockAvailabilitiyGeneralModel.PURCHASEUNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.STOREHOUSE,
        ICObStockAvailabilitiyGeneralModel.STOREHOUSE_CODE,
        ICObStockAvailabilitiyGeneralModel.STOREHOUSE_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        ICObStockAvailabilitiyGeneralModel.COMPANY_CODE,
        ICObStockAvailabilitiyGeneralModel.COMPANY_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PLANT,
        ICObStockAvailabilitiyGeneralModel.PLANT_CODE,
        ICObStockAvailabilitiyGeneralModel.PLANT_NAME);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.STOCK_TYPE, ICObStockAvailabilitiyGeneralModel.STOCK_TYPE);

    mapAssertion.assertBigDecimalNumberValue(
        IInventoryFeatureFileCommonKeys.TOTAL_QTY,
        ICObStockAvailabilitiyGeneralModel.AVAILABLE_QUANTITY);
  }

  public void insertStockAvailabilities(DataTable stockAvailabilitiesDataTable) {
    List<Map<String, String>> stockAvailabilitiesMaps =
        stockAvailabilitiesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> stockAvailabilitiesMap : stockAvailabilitiesMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createStockAvailability",
          constructInsertStockAvailabilityDataQueryParams(stockAvailabilitiesMap));
    }
  }

  public void validateStockAvailabilitiesMatchDatabase(DataTable expectedStockAvailability) {
    List<Map<String, String>> expectedMaps =
        expectedStockAvailability.asMaps(String.class, String.class);
    for (Map<String, String> stockAvailabilitiesMap : expectedMaps) {
      Object result =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "selectSingleStockAvailability",
              constructInsertStockAvailabilityDataQueryParams(stockAvailabilitiesMap));
      Assert.assertNotNull("Stock availability doesn't match expected value", result);
    }
  }

  private Object[] constructInsertStockAvailabilityDataQueryParams(
      Map<String, String> stockAvailabilitiesMap) {
    return new Object[] {
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.CODE),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.COMPANY),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.PLANT),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.STOREHOUSE),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.ITEM),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.UOM),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
      stockAvailabilitiesMap.get(IFeatureFileCommonKeys.AVAILABLE_QUANTITY),
    };
  }
}
