package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.inventory.goodsreceipt.IGoodsReceiptItemValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class IObGoodsReceiptSaveItemTestUtils extends IObGoodsReceiptItemTestUtils {

    public IObGoodsReceiptSaveItemTestUtils(Map<String, Object> properties) {
        super(properties);
    }

    public void assertThatGoodsReceiptHasGivenPOAndVendor(DataTable goodsRecieptsDataTable) {
        List<Map<String, String>> goodsRecieptsMap =
                goodsRecieptsDataTable.asMaps(String.class, String.class);
        for (Map<String, String> goodsRecieptMap : goodsRecieptsMap) {
            List<Map<String, Object>> goodsReceiptWithPOAndVendorMapList =
                    constructGoodsReceiptWithPOAndVendorMap(
                            goodsRecieptMap.get(IFeatureFileCommonKeys.CODE),
                            goodsRecieptMap.get(IFeatureFileCommonKeys.PO_CODE),
                            goodsRecieptMap.get(IFeatureFileCommonKeys.VENDOR_CODE));
            Assert.assertEquals(
                    "Goods Receipt has a record exists", 1, goodsReceiptWithPOAndVendorMapList.size());
        }
    }

    public void assertSavedItemHeaderDataInGoodsReceiptIsCorrect(String goodsReceiptCode, DataTable itemHeaderTable) {

        List<Map<String, String>> goodsReceiptItems =
                itemHeaderTable.asMaps(String.class, String.class);

        for (Map<String, String> goodsReceiptItemMap : goodsReceiptItems) {

            IObGoodsReceiptReceivedItemsGeneralModel iObGoodsReceiptRecievedItemsGeneralModel =
                    (IObGoodsReceiptReceivedItemsGeneralModel)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "getGoodsReceiptRecievedItem",
                                    constructGoodsReceiptRecievedItemsDataMap(goodsReceiptCode, goodsReceiptItemMap));
            Assert.assertNotNull(iObGoodsReceiptRecievedItemsGeneralModel);
        }
    }

    private Object[] constructGoodsReceiptRecievedItemsDataMap(
            String goodsReceiptCode, Map<String, String> goodsReceiptItemMap) {
        return new Object[]{goodsReceiptCode, goodsReceiptItemMap.get(IFeatureFileCommonKeys.ITEM_CODE)};
    }

    private List<Map<String, Object>> constructItemVendorRecordMap(
            String itemCode, String vendorCode, String itemVendorCode) {
        return entityManagerDatabaseConnector.executeNativeNamedQuery(
                "getIVRByItemAndVendorAndIVRCode", itemCode, vendorCode, itemVendorCode);
    }

    public void assertThatItemVendorRecordsExist(DataTable itemVendorRecordsDataTable) {
        List<Map<String, String>> itemVendorRecords =
                itemVendorRecordsDataTable.asMaps(String.class, String.class);

        for (Map<String, String> itemVendorRecordMap : itemVendorRecords) {
            List<Map<String, Object>> itemVendorRecord =
                    constructItemVendorRecordMap(
                            itemVendorRecordMap.get(IFeatureFileCommonKeys.ITEM_CODE),
                            itemVendorRecordMap.get(IFeatureFileCommonKeys.VENDOR_CODE),
                            itemVendorRecordMap.get(IFeatureFileCommonKeys.ITEM_VENDOR_CODE));

            Assert.assertFalse(itemVendorRecord.isEmpty());
        }
    }

    private List<Map<String, Object>> constructGoodsReceiptWithPOAndVendorMap(
            String goodsRecieptCode, String purchaseOrderCode, String vendorCode) {
        return entityManagerDatabaseConnector.executeNativeNamedQuery(
                "getGoodsReceiptPurchaseOrderByGRAndPOAndVendorCode",
                goodsRecieptCode,
                purchaseOrderCode,
                vendorCode);
    }

    public JsonObject createGoodsReceiptItemHeaderJsonObject(DataTable itemHeaderDataTable) {
        List<Map<String, String>> itemHeaderMaps =
                itemHeaderDataTable.asMaps(String.class, String.class);

        JsonObject goodsReceiptItemObject = new JsonObject();
        for (Map<String, String> itemHeaderMap : itemHeaderMaps) {
            addGoodsReceiptItemHeaderJsonObjectValues(goodsReceiptItemObject, itemHeaderMap);
        }

        return goodsReceiptItemObject;
    }

    private void addGoodsReceiptItemHeaderJsonObjectValues(
            JsonObject goodsReceiptItemObject, Map<String, String> itemHeaderMap) {
        goodsReceiptItemObject.addProperty(
                IGoodsReceiptItemValueObject.ITEM_CODE,
                itemHeaderMap.get(IFeatureFileCommonKeys.ITEM_CODE));
    }

    public Response saveGoodsReceiptOrdinaryItemSection(
            Cookie userCookie, JsonObject goodsReceiptItem, String goodsReceiptCode) {
        String restURL = IDObGoodsReceiptRestUrls.REQUEST_SAVE_ITEM_URL + goodsReceiptCode;
        return sendPUTRequest(userCookie, restURL, goodsReceiptItem);
    }

    public void assertItemHasNoItemVendorReocrd(String itemCode) {
        List itemVendorRecord =
                entityManagerDatabaseConnector.executeNativeNamedQuery("getIVRByItemCode", itemCode);
        Assert.assertTrue(itemVendorRecord.isEmpty());
    }
}
