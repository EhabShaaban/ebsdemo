package com.ebs.dda.inventory.initialstockupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.initialstockupload.utils.DObInitialStockUploadCreateTestUtils;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObInitialStockUploadCreateStep extends SpringBootRunner implements En {
  private static boolean oneTimeExecution = false;
  private DObInitialStockUploadCreateTestUtils utils;
  private ItemVendorRecordCommonTestUtils ivrUtils;

  public DObInitialStockUploadCreateStep() {
    When(
        "^\"([^\"]*)\" requests to create InitialStockUpload",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(
                  cookie, IDObInitialStockUploadRestURLS.REQUEST_CREATE_INITIAL_STOCK_UPLOAD);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^Last created InitialStockUpload has code \"([^\"]*)\"$",
        (String initialStockUploadCode) -> {
          utils.insertLastCreatedInitialStockUploadCode(initialStockUploadCode);
        });

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    Given(
        "^Last created InitialStockUpload was with code \"([^\"]*)\"$",
        (String initialStockUploadCode) -> {
          utils.assertLastCreatedInitialStockUpload(initialStockUploadCode);
        });

    When(
        "^\"([^\"]*)\" creates InitialStockUpload with the following values:$",
        (String userName, DataTable initialStockUploadDataTable) -> {
          Response response =
              utils.createInitialStockUpload(
                  initialStockUploadDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new InitialStockUpload is created with the following values:$",
        (DataTable initialStockUploadDataTable) -> {
          utils.assertInitialStockUploadIsCreatedSuccessfully(initialStockUploadDataTable);
        });

    Then(
        "^the Company Section of InitialStockUpload with code \"([^\"]*)\" is created as follows:$",
        (String initialStockUploadCode, DataTable initialStockUploadDataTable) -> {
          utils.assertInitialStockUploadCompanyIsCreatedSuccessfully(
              initialStockUploadCode, initialStockUploadDataTable);
        });

    Given(
        "^the following IVRs exist:$",
        (DataTable ivrDataTable) -> {
          ivrUtils.assertThatItemVendorRecordsWithoutCreationDateExists(ivrDataTable);
        });

    Then(
        "^the Items Section of InitialStockUpload with code \"([^\"]*)\" is created as follows:$",
        (String userCode, DataTable itemsDataTable) -> {
          utils.assertThatInitialStockItemsAreCreatedSuccessfully(userCode, itemsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitialStockUploadCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    ivrUtils = new ItemVendorRecordCommonTestUtils(getProperties());
    ivrUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create InitialStockUpload -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearISUInsertions());
      if (!oneTimeExecution) {
        databaseConnector.executeSQLScript(utils.getDbScriptsPathOneTimeExecution());
        oneTimeExecution = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create InitialStockUpload -")) {
      databaseConnector.closeConnection();
    }
  }
}
