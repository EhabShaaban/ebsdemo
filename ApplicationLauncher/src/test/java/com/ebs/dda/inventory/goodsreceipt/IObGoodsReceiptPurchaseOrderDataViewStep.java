package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObCostingViewAccountingDetailsTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptPurchaseOrderDataViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;

public class IObGoodsReceiptPurchaseOrderDataViewStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private IObGoodsReceiptPurchaseOrderDataViewTestUtils goodsReceiptPurchaseOrderDataViewTestUtils;
    private IObCostingViewAccountingDetailsTestUtils accountingDocumentViewAccountingDetailsTestUtils;


    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodsReceiptPurchaseOrderDataViewTestUtils =
                new IObGoodsReceiptPurchaseOrderDataViewTestUtils(getProperties());
        goodsReceiptPurchaseOrderDataViewTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
        accountingDocumentViewAccountingDetailsTestUtils = new
                IObCostingViewAccountingDetailsTestUtils(getProperties(),
                entityManagerDatabaseConnector);
    }

    public IObGoodsReceiptPurchaseOrderDataViewStep() {

        Given(
                "^the following GRs exist with the following GR-POData:$",
                (DataTable goodsReceiptPurchaseOrderData) -> {
                    goodsReceiptPurchaseOrderDataViewTestUtils.assertThatGoodsReceiptsExist(
                            goodsReceiptPurchaseOrderData);
                });

    When(
        "^\"([^\"]*)\" requests to view POData section of GR with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response goodsReceiptPurchaseOrderDataResponse =
              goodsReceiptPurchaseOrderDataViewTestUtils.readGoodsReceiptPurchaseOrderData(
                  loginCookie, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, goodsReceiptPurchaseOrderDataResponse);
        });

    Then(
        "^the following values of POData section are displayed to \"([^\"]*)\":$",
        (String userName, DataTable expectedGoodsReceiptPurchaseOrderDataResponseData) -> {
          Response goodsReceiptPurchaseOrderDataResponse =
              userActionsTestUtils.getUserResponse(userName);
          goodsReceiptPurchaseOrderDataViewTestUtils
              .assertGoodsReceiptPurchaseOrderDataIsRepresentedToUser(
                  goodsReceiptPurchaseOrderDataResponse,
                  expectedGoodsReceiptPurchaseOrderDataResponseData);
        });

    When(
        "^\"([^\"]*)\" then requests to refresh GR with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response goodsReceiptPurchaseOrderDataResponse =
              goodsReceiptPurchaseOrderDataViewTestUtils.readGoodsReceiptPurchaseOrderData(
                  loginCookie, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, goodsReceiptPurchaseOrderDataResponse);
        });

    Given(
        "^the following PurchasingResponsibles exist:$",
        (DataTable purchaseResponsibleTable) -> {
          List<List<String>> purchaseResponsible = purchaseResponsibleTable.raw();
          for (int i = 1; i < purchaseResponsible.size(); i++) {
            goodsReceiptPurchaseOrderDataViewTestUtils.assertThatPurchaseResponsibleExist(
                purchaseResponsible.get(i));
          }
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GR POData section -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            goodsReceiptPurchaseOrderDataViewTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
        databaseConnector.executeSQLScript(
                accountingDocumentViewAccountingDetailsTestUtils
                        .clearCostingInsertions());
        databaseConnector.executeSQLScript(
          goodsReceiptPurchaseOrderDataViewTestUtils.getDbScriptPath());
    }
  }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View GR POData section -")) {
            databaseConnector.closeConnection();
        }
    }
}
