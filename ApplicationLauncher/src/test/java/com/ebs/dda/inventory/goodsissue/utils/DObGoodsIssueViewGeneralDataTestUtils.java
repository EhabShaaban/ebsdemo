package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IDObGoodsIssueGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.junit.Assert;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;

public class DObGoodsIssueViewGeneralDataTestUtils extends DObGoodsIssueCommonTestUtils {
    private ObjectMapper objectMapper;

    public DObGoodsIssueViewGeneralDataTestUtils(Map<String, Object> properties) {
        setProperties(properties);
        objectMapper = new ObjectMapper();
        this.urlPrefix = (String) getProperty(URL_PREFIX);
    }

    public void assertThatTheFollowingGIGeneralDataExist(DataTable generalDataTable) {
        List<Map<String, String>> giGeneralDataListMap =
                generalDataTable.asMaps(String.class, String.class);
        for (Map<String, String> giGeneralDataMap : giGeneralDataListMap) {
            DObGoodsIssueGeneralModel giGeneralData =
                    (DObGoodsIssueGeneralModel)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "getGIGeneralData", constructGoodsIssueQueryParams(giGeneralDataMap));
            Assert.assertNotNull(
                    "GI General Data doesn't exist for GI with code: "
                            + giGeneralDataMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
                    giGeneralData);
        }
  }

  private Object[] constructGoodsIssueQueryParams(Map<String, String> goodsIssueMap) {
      return new Object[]{
              goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
              goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.TYPE),
              goodsIssueMap.get(IFeatureFileCommonKeys.CREATED_BY),
              "%" + goodsIssueMap.get(IFeatureFileCommonKeys.STATE) + "%",
              goodsIssueMap.get(IFeatureFileCommonKeys.CREATION_DATE),
              goodsIssueMap.get(IInventoryFeatureFileCommonKeys.STOREKEEPER)
      };
  }

    public String getViewGeneralDataUrl(String code) {
        return IDObGoodsIssueRestURLS.BASE_URL + "/" + code + IDObGoodsIssueRestURLS.VIEW_GENERAL_DATA;
    }

    public void assertThatViewGeneralDataResponseIsCorrect(
            Response response, DataTable generalDataTable) throws IOException, JSONException {

        Map<String, String> giGeneralDataMap =
                generalDataTable.asMaps(String.class, String.class).get(0);
        Map<String, Object> giGeneralDataResponseMap =
                response.body().jsonPath().getMap(CommonKeys.DATA);

        Assert.assertEquals(
                giGeneralDataMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
                giGeneralDataResponseMap.get(IDObGoodsIssueGeneralModel.USER_CODE));
        Assert.assertEquals(
                giGeneralDataMap.get(IGoodsIssueFeatureFileCommonKeys.TYPE),
                giGeneralDataResponseMap.get(IDObGoodsIssueGeneralModel.TYPE_NAME_EN));
        Assert.assertEquals(
                giGeneralDataMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
                giGeneralDataResponseMap.get(IDObGoodsIssueGeneralModel.PURCHASING_UNIT_NAME_EN));
        Assert.assertEquals(
                giGeneralDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
                giGeneralDataResponseMap.get(IDObGoodsIssueGeneralModel.CREATION_INFO));
        Assert.assertTrue(
                (giGeneralDataResponseMap.get(IDObGoodsIssueGeneralModel.CURRENT_STATES).toString())
                        .contains(giGeneralDataMap.get(IFeatureFileCommonKeys.STATE)));

        this.assertDateEquals(
                giGeneralDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
                formatDate(
                        String.valueOf(
                                giGeneralDataResponseMap.get(IDObGoodsIssueGeneralModel.CREATION_DATE))));

        Assert.assertEquals(
                giGeneralDataMap.get(IInventoryFeatureFileCommonKeys.STOREKEEPER),
                getEnglishLocaleFromLocalizedResponse(
                        objectMapper.writeValueAsString(
                                giGeneralDataResponseMap.get(IDObGoodsIssueGeneralModel.STOREKEEPER_NAME))));
    }

  private DateTime formatDate(String date) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
    DateTime parsedDateTime = formatter.parseDateTime(date).withZone(DateTimeZone.UTC);
    return parsedDateTime.minusSeconds(parsedDateTime.getSecondOfMinute());
  }
}
