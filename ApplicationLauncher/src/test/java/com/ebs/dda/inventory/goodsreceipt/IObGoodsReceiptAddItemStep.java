package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptAddItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class IObGoodsReceiptAddItemStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private IObGoodsReceiptAddItemTestUtils goodsReceiptAddItemTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodsReceiptAddItemTestUtils = new IObGoodsReceiptAddItemTestUtils(getProperties());
        goodsReceiptAddItemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public IObGoodsReceiptAddItemStep() {
        When(
                "^\"([^\"]*)\" requests to add item to GoodsReceipt with code \"([^\"]*)\"$",
                (String userName, String goodsReceiptCode) -> {
                    Response requestAddItem =
                            userActionsTestUtils.lockSection(
                                    userName,
                                    IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM + goodsReceiptCode,
                                    goodsReceiptCode);
                    userActionsTestUtils.setUserResponse(userName, requestAddItem);
                });

    Then(
            "^a new add Item side nav is opened and Items section of GoodsReceipt with code \"([^\"]*)\" is locked by \"([^\"]*)\"$",
            (String goodsReceiptCode, String userName) -> {
                goodsReceiptAddItemTestUtils.assertResponseSuccessStatus(
                        userActionsTestUtils.getUserResponse(userName));
                goodsReceiptAddItemTestUtils.assertThatResourceIsLockedByUser(
                        userName, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
            });

    Given(
            "^\"([^\"]*)\" first requests to add item to GoodsReceipt with code \"([^\"]*)\" successfully$",
            (String userName, String goodsReceiptCode) -> {
                Response requestAddItem =
                        userActionsTestUtils.lockSection(
                                userName,
                                IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM + goodsReceiptCode,
                                goodsReceiptCode);
                goodsReceiptAddItemTestUtils.assertResponseSuccessStatus(requestAddItem);
            });

    Given(
            "^Items section of GoodsReceipt with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
            (String goodsReceiptCode, String userName, String lockTime) -> {
                goodsReceiptAddItemTestUtils.freeze(lockTime);
                Response requestAddItem =
                        userActionsTestUtils.lockSection(
                                userName,
                                IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM + goodsReceiptCode,
                                goodsReceiptCode);
                goodsReceiptAddItemTestUtils.assertResponseSuccessStatus(requestAddItem);
            });

    When(
            "^\"([^\"]*)\" cancels saving Items section of GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\"$",
            (String username, String goodsReceiptCode, String lockTime) -> {
                goodsReceiptAddItemTestUtils.freeze(lockTime);
                Response unlockResponse =
                        userActionsTestUtils.unlockSection(
                                username, IDObGoodsReceiptRestUrls.CANCEL_REQUEST_ADD_ITEM + goodsReceiptCode);
                userActionsTestUtils.setUserResponse(username, unlockResponse);
            });

    Then(
            "^the lock by \"([^\"]*)\" on Items section of GoodsReceipt with code \"([^\"]*)\" is released$",
            (String username, String goodsReceiptCode) -> {
                goodsReceiptAddItemTestUtils.assertThatLockIsReleased(
                        username, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
            });
  }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Request to Add/Cancel Item to a GR -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(
                        goodsReceiptAddItemTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(goodsReceiptAddItemTestUtils.getDbScriptPath());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Request to Add/Cancel Item to a GR -")) {
            goodsReceiptAddItemTestUtils.unfreeze();
            userActionsTestUtils.unlockSectionForAllUsers(IDObGoodsReceiptRestUrls.CANCEL_REQUEST_ADD_ITEM);
            databaseConnector.closeConnection();
        }
    }
}
