package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.costing.utils.DObCostingCommonTestUtils;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCommonTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptActivateTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptCommonTestUtils;
import com.ebs.dda.masterdata.item.utils.MObItemViewAccountingInfoTestUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

import java.util.Arrays;

public class DObGoodsReceiptActivateBasedOnPOStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;

  private DObCostingCommonTestUtils costingCommonTestUtils;
  private MObItemViewAccountingInfoTestUtils itemViewAccountingInfoTestUtils;

  public DObGoodsReceiptActivateBasedOnPOStep() {
    Then(
        "^a new Costing Document is Created As Follows:$",
        (DataTable costingDataTable) -> {
          costingCommonTestUtils.assertThatCostingDocumentCreatedSuccessfully(costingDataTable);
        });

    Then(
        "^the CompanyData of Costing Document is Created As Follows:$",
        (DataTable costingCompanyDataTable) -> {
          costingCommonTestUtils.assertThatCostingCompanyCreatedSuccessfully(
              costingCompanyDataTable);
        });

    Given(
        "^the following AccountingDetails for Items exist:$",
        (DataTable accountingInfoDataTable) -> {
          itemViewAccountingInfoTestUtils.assertThatItemHasAccountInfo(accountingInfoDataTable);
        });
    Then(
        "^the Costing Items of Costing Document is Created As Follows:$",
        (DataTable costingItemsDataTable) -> {
          costingCommonTestUtils.assertThatCostingItemsCreatedSuccessfully(costingItemsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    costingCommonTestUtils =
        new DObCostingCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
    itemViewAccountingInfoTestUtils = new MObItemViewAccountingInfoTestUtils(getProperties());
    itemViewAccountingInfoTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    databaseConnector.createConnection();
    if (contains(scenario.getName(), " Activate GR- LandedCost")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(costingCommonTestUtils.getDbScriptsOneTimeExecution());
      }
      databaseConnector.executeSQLScript(DObJournalEntryViewAllTestUtils.clearJournalEntries());
      databaseConnector.executeSQLScript(DObGoodsReceiptActivateTestUtils.clearStoreTransaction());
      databaseConnector.executeSQLScript(DObGoodsReceiptCommonTestUtils.clearGoodsReceipts());
      databaseConnector.executeSQLScript(DObPaymentRequestCommonTestUtils.clearPaymentRequests());
      databaseConnector.executeSQLScript(DObLandedCostCommonTestUtils.clearLandedCosts());
      databaseConnector.executeSQLScript(DObVendorInvoiceTestUtils.clearVendorInvoices());
      databaseConnector.executeSQLScript(costingCommonTestUtils.clearCostingInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    databaseConnector.closeConnection();
    costingCommonTestUtils.unfreeze();
    if (contains(scenario.getName(), "Activate GR- LandedCost")) {
      userActionsTestUtils.unlockAllSections(
          IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
          Arrays.asList(
              IPurchaseOrderSectionNames.HEADER_SECTION, IPurchaseOrderSectionNames.ITEMS_SECTION));
    }
  }
}
