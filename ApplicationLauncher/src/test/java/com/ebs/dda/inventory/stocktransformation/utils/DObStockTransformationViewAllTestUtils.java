package com.ebs.dda.inventory.stocktransformation.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.stocktransformation.IStockTransformationFileCommonKeys;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.stocktransformation.IDObStockTransformationGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObStockTransformationViewAllTestUtils extends DObStockTransformationCommonTestUtils {

  public DObStockTransformationViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getViewAllDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append("," + "db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
      str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
      str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append("," + "db-scripts/inventory/stock-transformation/CObStockTransformation.sql");
    str.append(
        "," + "db-scripts/inventory/stock-transformation/DObStockTransformation_ViewAll.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append(
        ","
            + "db-scripts/inventory/store-transaction/TObStockTransformationStoreTransaction_ViewAll.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");
    str.append(
        ","
            + "db-scripts/inventory/stock-transformation/IObStockTransformationDetails_ViewAll.sql");
    return str.toString();
  }

  public void assertThatStockTransformationsExits(DataTable stockTransformationsDataTable) {
    {
      List<Map<String, String>> stockTransformationsList =
          stockTransformationsDataTable.asMaps(String.class, String.class);

      for (Map<String, String> stockTransformations : stockTransformationsList) {
        DObStockTransformationGeneralModel dObstockTransformations =
            (DObStockTransformationGeneralModel)
                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getStockTransformation",
                    constructStockTransformationParameters(stockTransformations));
        assertNotNull(
            "StockTransformation with code "
                + stockTransformations.get(IFeatureFileCommonKeys.CODE)
                + " Doesn't exist!",
            dObstockTransformations);
        if (!stockTransformations
            .get(IStockTransformationFileCommonKeys.NEW_STORE_TRANSACTION)
            .isEmpty()) {
          assertEquals(
              stockTransformations.get(IStockTransformationFileCommonKeys.NEW_STORE_TRANSACTION),
              dObstockTransformations.getNewStoreTransactionCode());
        }
      }
    }
  }

  private Object[] constructStockTransformationParameters(
      Map<String, String> stockTransformationMap) {
    return new Object[] {
      stockTransformationMap.get(IFeatureFileCommonKeys.CODE),
      stockTransformationMap.get(IFeatureFileCommonKeys.TYPE),
      stockTransformationMap.get(IInventoryFeatureFileCommonKeys.REF_STORE_TRANSACTION),
      stockTransformationMap.get(IInventoryFeatureFileCommonKeys.STOCK_TRANSFORMATION_REASON),
      "%" + stockTransformationMap.get(IFeatureFileCommonKeys.STATE) + "%",
      stockTransformationMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertThatStockTransformationssIsMatch(
      Response response, DataTable stockTransformationsDataTable) throws Exception {
    List<HashMap<String, Object>> actualStockTransformation = getListOfMapsFromResponse(response);
    List<Map<String, String>> expectedStockTransformationMaps =
        stockTransformationsDataTable.asMaps(String.class, String.class);
    for (int i = 0; i < actualStockTransformation.size(); i++) {
      assertThatStockTransformationMatchesTheExpected(
          expectedStockTransformationMaps.get(i), actualStockTransformation.get(i));
    }
  }

  private void assertThatStockTransformationMatchesTheExpected(
      Map<String, String> expectedStockTransformationMap,
      HashMap<String, Object> actualStockTransformationMap)
      throws Exception {
    MapAssertion mapAssertion =
        new MapAssertion(expectedStockTransformationMap, actualStockTransformationMap);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObStockTransformationGeneralModel.STOCK_TRANSFORMATION_CODE);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE,
        IDObStockTransformationGeneralModel.STOCK_TRANSFORMATION_TYPE_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObStockTransformationGeneralModel.PURCHASEUNIT_CODE,
        IDObStockTransformationGeneralModel.PURCHASEUNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IInventoryFeatureFileCommonKeys.STOCK_TRANSFORMATION_REASON,
        IDObStockTransformationGeneralModel.TRANSFORMATION_REASON_CODE,
        IDObStockTransformationGeneralModel.TRANSFORMATION_REASON_NAME);

    mapAssertion.assertValue(
        IInventoryFeatureFileCommonKeys.REF_STORE_TRANSACTION,
        IDObStockTransformationGeneralModel.REF_STORE_TRANSACTION_CODE);

    mapAssertion.assertValue(
        IStockTransformationFileCommonKeys.NEW_STORE_TRANSACTION,
        IDObStockTransformationGeneralModel.NEW_STORE_TRANSACTION_CODE);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObStockTransformationGeneralModel.CURRENT_STATES);
  }

  public void assertTotalNumberOfStockTransformationsAreCorrect(Integer recordsNumber) {
    Long actualTotalRecords =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCountStockTransformation");
    Assert.assertEquals(
        "Total Number of Records in Response Not Equal ",
        recordsNumber.intValue(),
        actualTotalRecords.intValue());
  }
}
