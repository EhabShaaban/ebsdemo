package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.IObGoodsIssueCompanyViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObGoodsIssueCompanyViewStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private IObGoodsIssueCompanyViewTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new IObGoodsIssueCompanyViewTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public IObGoodsIssueCompanyViewStep() {
        Given(
                "^the following CompanyData for GoodsIssues exist:$",
                (DataTable sompanyDataTable) -> {
                    utils.assertThatGoodsIssueHasCompany(sompanyDataTable);
                });

        When(
                "^\"([^\"]*)\" requests to view Company section of GoodsIssue with code \"([^\"]*)\"$",
        (String userName, String goodsIssueCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getGoodsIssueCompanyViewDataUrl(goodsIssueCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of Company section for GoodsIssue with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String goodsIssueCode, String userName, DataTable sompanyDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatGoodsIssueCompanyMatchesResponse(sompanyDataTable, response);
        });
  }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View GI CompanyData section -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {

                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View GI CompanyData section -")) {
            databaseConnector.closeConnection();
        }
    }
}
