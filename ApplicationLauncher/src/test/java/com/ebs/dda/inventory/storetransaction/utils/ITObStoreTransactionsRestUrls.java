package com.ebs.dda.inventory.storetransaction.utils;

public interface ITObStoreTransactionsRestUrls {

  String GET_URL = "/services/storetransaction";
  String READ_FILTERED_STORE_TRANSACTIONS = GET_URL + "/readfilteredstoretransactions/";
}
