package com.ebs.dda.inventory;

public interface IInventoryFeatureFileCommonKeys {
    String COMPANY = "Company";
    String PLANT = "Plant";
    String STOREHOUSE = "Storehouse";
    String STOREKEEPER = "Storekeeper";
    String BUSINESS_PARTNER = "BusinessPartner";
    String REF_DOCUMENT = "RefDocument";
    String TOTAL_QTY = "TotalQty";
    String NEW_STOCK_TYPE = "NewStockType";
    String STOCK_TRANSFORMATION_REASON = "StockTransformationReason";
    String REF_STORE_TRANSACTION = "RefStoreTransaction";
    String ORIGINAL_ADDING_DATE = "OriginalAddingDate";
    String GOODS_RECEIPT="GoodsReceipt";
}
