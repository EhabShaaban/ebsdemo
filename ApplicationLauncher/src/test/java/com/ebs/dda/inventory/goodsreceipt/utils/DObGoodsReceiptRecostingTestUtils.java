package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionAllDataGeneralModel;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObGoodsReceiptRecostingTestUtils extends DObGoodsReceiptCommonTestUtils {
  public DObGoodsReceiptRecostingTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/LookupSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument_ViewAll.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder_ViewAll.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append(",").append("db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append(",").append("db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderDocumentRequiredDocuments.sql");
    str.append(",").append("db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts_ViewAll.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append("," + "db-scripts/sales/IObSISOItems.sql");
    str.append("," + "db-scripts/order/salesreturnorder/CObSalesReturnOrderType.sql");
    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder.sql");
    str.append(
        "," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptSalesReturnDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptCompany.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptActivationDetailsScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxes.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxesViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyData.sql");
    str.append(",")
        .append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyDataViewAll.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    str.append(",")
        .append(
            "db-scripts/accounting/payment-request/iob-payment-request-payment-details-view-all.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
    str.append(",")
        .append(
            "db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-postingdetails.sql");
    str.append(",")
        .append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetailsViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");
    str.append(",").append("db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");

    return str.toString();
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostFactorItem.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCostJournalEntry.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/DObLandedCostJournalEntry_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails_ViewAll.sql");
    str.append("," + "db-scripts/accounting/journal-entry/payment-request-journal-entry.sql");
    str.append("," + "db-scripts/accounting/journal-entry/payment-request-journal-entry-items.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/payment-request-journal-entry-viewall.sql");
    str.append(
        ","
            + "db-scripts/accounting/journal-entry/payment-request-journal-entry-items-viewall.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-viewall.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items-viewall.sql");
    str.append("," + "db-scripts/accounting/costing/costing_clear.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/recosting_clear.sql");
    return str.toString();
  }

  public void assertThatStoreTransactionsWereUpdated(
      String refDocument, DataTable storeTransactionsDataTable) {

    List<Map<String, String>> storeTransactions =
        storeTransactionsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> storeTransaction : storeTransactions) {
      TObStoreTransactionAllDataGeneralModel storeTransactionGeneralModel =
          (TObStoreTransactionAllDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getStoreTransactionAllDataRecordsByParameters",
                  constructUpdatedStoreTransactionsQueryParameters(refDocument, storeTransaction));
      assertNotNull(
          storeTransactionGeneralModel, "StoreTransaction " + storeTransaction + " doesn't exist!");
    }
  }

  private Object[] constructUpdatedStoreTransactionsQueryParameters(
      String refDocument, Map<String, String> storeTransactionRecord) {
    return new Object[] {
      storeTransactionRecord.get(IFeatureFileCommonKeys.BATCH_NUMBER),
      storeTransactionRecord.get(IFeatureFileCommonKeys.REF_TRANSACTION),
      storeTransactionRecord.get(IFeatureFileCommonKeys.ESTIMATE_COST),
      storeTransactionRecord.get(IFeatureFileCommonKeys.ACTUAL_COST),
      storeTransactionRecord.get(IFeatureFileCommonKeys.REMAINING_QTY),
      storeTransactionRecord.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE),
      storeTransactionRecord.get(IFeatureFileCommonKeys.STOCK_TYPE),
      storeTransactionRecord.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
      storeTransactionRecord.get(IFeatureFileCommonKeys.ITEM),
      storeTransactionRecord.get(IFeatureFileCommonKeys.UOM),
      storeTransactionRecord.get(IFeatureFileCommonKeys.QTY),
      storeTransactionRecord.get(IFeatureFileCommonKeys.TRANSACTION_TYPE),
      refDocument,
      storeTransactionRecord.get(IFeatureFileCommonKeys.COMPANY),
      storeTransactionRecord.get(IFeatureFileCommonKeys.PLANT),
      storeTransactionRecord.get(IFeatureFileCommonKeys.STOREHOUSE)
    };
  }

  public void assertupdatedCostingItemsExist(String costingCode, DataTable itemsDataTable) {
    List<Map<String, String>> costingItemsMapList =
        itemsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> costingItemMap : costingItemsMapList) {
      IObCostingItemGeneralModel costingItem =
          (IObCostingItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCostingItemByParameters",
                  constructupdatedCostingItemQueryParameters(costingCode, costingItemMap));
      assertNotNull(costingItem, "CostingItem " + costingCode + " doesn't exist!");
    }
  }

  private Object[] constructupdatedCostingItemQueryParameters(
      String costingCode, Map<String, String> costingItemMap) {
    return new Object[] {
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_PRICE),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_PRICE),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_LANDED_COST),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_LANDED_COST),
      costingCode,
      costingItemMap.get(IFeatureFileCommonKeys.ITEM),
      costingItemMap.get(IFeatureFileCommonKeys.UOM),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.WEIGHT_PERCENTAGE),
      costingItemMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
      costingItemMap.get(IFeatureFileCommonKeys.STOCK_TYPE)
    };
  }
}
