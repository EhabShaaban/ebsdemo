package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptReadTypesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObGoodsReceiptReadTypesStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private DObGoodsReceiptReadTypesTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObGoodsReceiptReadTypesTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObGoodsReceiptReadTypesStep() {
        Given(
                "^the following GoodsReceiptTypes exist:$",
                (DataTable typesDataTable) -> {
                    utils.assertGoodsReceiptTypesExist(typesDataTable);
                });

        Given(
                "^the total number of existing GoodsReceiptTypes is (\\d+)$",
                (Integer typesNumber) -> {
                    utils.assertGoodsReceiptTypesCountMatchesExpected(typesNumber);
                });

        When(
                "^\"([^\"]*)\" requests to read all GoodsReceiptTypes$",
                (String userName) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = utils.sendGETRequest(cookie, IDObGoodsReceiptRestUrls.READ_TYPES);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following GoodsReceiptTypes values will be presented to \"([^\"]*)\":$",
                (String userName, DataTable typesDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertActualResponseMatchesExpected(typesDataTable, response);
                });

        Then(
                "^total number of GoodsReceiptTypes returned to \"([^\"]*)\" is equal to (\\d+)$",
                (String userName, Integer typesCount) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertTotalNumberOfRecordsEqualResponseSize(response, typesCount);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of GoodsReceiptTypes -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptPath());
                hasBeenExecutedOnce = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of GoodsReceiptTypes -")) {
            databaseConnector.closeConnection();
        }
    }
}
