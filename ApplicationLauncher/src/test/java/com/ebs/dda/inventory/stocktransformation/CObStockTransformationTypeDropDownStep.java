package com.ebs.dda.inventory.stocktransformation;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.stocktransformation.utils.CObStockTransformationTypeDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class CObStockTransformationTypeDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private CObStockTransformationTypeDropDownTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils =
                new CObStockTransformationTypeDropDownTestUtils(properties, entityManagerDatabaseConnector);
    }

  public CObStockTransformationTypeDropDownStep() {
    Given(
        "^the total number of existing StockTransformation Types are (\\d+)$",
        (Integer recordsNumber) -> {
          utils.assertThatAllStockTransformationTypesExist(recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read all StockTransformationTypes$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.readAllStockTransformationTypes());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following StockTransformation Types values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable stockTransformationTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseMatchesExpected(response, stockTransformationTypesDataTable);
        });

    Then(
        "^total number of StockTransformation Types returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read list of StockTransformationTypes")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Read list of StockTransformationTypes")) {

      databaseConnector.closeConnection();
    }
  }
}
