package com.ebs.dda.inventory.goodsreceipt.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptSalesReturnDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

public class IObGoodsReceiptViewSalesReturnTestUtils extends DObGoodsReceiptCommonTestUtils {
  public IObGoodsReceiptViewSalesReturnTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertThatGoodsReceiptHasSalesReturn(DataTable goodsReceiptSalesReturnDataTable) {

    List<Map<String, String>> goodsReceiptSalesReturns =
        goodsReceiptSalesReturnDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsReceiptSalesReturn : goodsReceiptSalesReturns) {
      IObGoodsReceiptSalesReturnDataGeneralModel goodsReceiptSalesReturnDataGeneralModel =
          (IObGoodsReceiptSalesReturnDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptSalesReturnData",
                  constructGoodsReceiptQueryParameters(goodsReceiptSalesReturn));
      assertNotNull(
          goodsReceiptSalesReturnDataGeneralModel,
          goodsReceiptSalesReturn.get(IFeatureFileCommonKeys.GR_CODE) + " doesn't exist!");
      if (!goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.NOTES).isEmpty()) {
        assertEquals(
            goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.NOTES),
            goodsReceiptSalesReturnDataGeneralModel.getComments());
      } else {
        assertNull(goodsReceiptSalesReturnDataGeneralModel.getComments());
      }
    }
  }

  private Object[] constructGoodsReceiptQueryParameters(
      Map<String, String> goodsReceiptSalesReturn) {
    return new Object[] {
      goodsReceiptSalesReturn.get(IFeatureFileCommonKeys.GR_CODE),
      goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN),
      goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_DOCUMENT_OWNER),
      goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_CUSTOMER)
    };
  }

  public Response viewSalesReturnSection(Cookie userCookie, String goodsReceiptCode) {
    String restUrl =
        IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT
            + "/"
            + goodsReceiptCode
            + IDObGoodsReceiptRestUrls.VIEW_REF_DOCUMENT;
    return sendGETRequest(userCookie, restUrl);
  }

  public void assertThatViewSalesReturnDataResponseIsCorrect(
      Response response, DataTable salesReturnDataTable) throws Exception {
    Map<String, String> expectedSalesReturnData =
        salesReturnDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualSalesReturn = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedSalesReturnData, actualSalesReturn);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_CUSTOMER,
        IIObGoodsReceiptSalesReturnDataGeneralModel.CUSTOMER_CODE,
        IIObGoodsReceiptSalesReturnDataGeneralModel.CUSTOMER_NAME);

    mapAssertion.assertValue(
            IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN,
            IIObGoodsReceiptSalesReturnDataGeneralModel.SALES_RETURN_ORDER_CODE);

    mapAssertion.assertValue(
        IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_DOCUMENT_OWNER,
        IIObGoodsReceiptSalesReturnDataGeneralModel.SALES_REPRESENTATIVE_NAME);

    mapAssertion.assertValue(
        IGoodsReceiptFeatureFileCommonKeys.NOTES,
        IIObGoodsReceiptSalesReturnDataGeneralModel.COMMENTS);
  }
}
