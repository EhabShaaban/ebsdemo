package com.ebs.dda.inventory.storetransaction.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.inventory.storetransaction.ITObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import cucumber.api.DataTable;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.config.JsonConfig.jsonConfig;
import static io.restassured.path.json.config.JsonPathConfig.NumberReturnType.BIG_DECIMAL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TObStoreTransactionViewAllTestUtils extends TObStoreTransactionCommonTestUtils {

  public static final String TRANSACTION_TYPE_ADD = "ADD";

  public TObStoreTransactionViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getViewAllDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptPath());
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append(',')
        .append("db-scripts/inventory/store-transaction/TObStoreTransaction_ViewAll.sql");
    return str.toString();
  }

  public void assertThatOnlyTheFollowingStoreTransactionsExist(DataTable dataTable) {
    List<Map<String, String>> storeTransactionMaps = dataTable.asMaps(String.class, String.class);
    assertNumberOfStoreTransactionsThatExist(storeTransactionMaps);
    for (Map<String, String> storeTransactionMap : storeTransactionMaps) {
      assertThatStoreTransactionExists(storeTransactionMap);
    }
  }

  private void assertNumberOfStoreTransactionsThatExist(
      List<Map<String, String>> storeTransactionMaps) {
    long expectedNumberOfStoreTransactions = storeTransactionMaps.size();
    Object actualNumberOfStoreTransactions =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getNumberOfStoreTransactions");
    assertEquals(expectedNumberOfStoreTransactions, actualNumberOfStoreTransactions);
  }

  private void assertThatStoreTransactionExists(Map<String, String> storeTransactionMap) {
    TObStoreTransactionGeneralModel storeTransactionGeneralModel =
        (TObStoreTransactionGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getStoreTransactionGeneralModel",
                constructStoreTransactionParameters(storeTransactionMap));
    assertNotNull(
        storeTransactionMap.get(IStoreTransactionFileCommonKeys.ST_CODE) + " Doesn't exist!",
        storeTransactionGeneralModel);
    if (storeTransactionMap
        .get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE)
        .equals(TRANSACTION_TYPE_ADD)) {
      assertDateEquals(
          storeTransactionMap.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE),
          storeTransactionGeneralModel.getOriginalAddingDate());
    }
  }

  private Object[] constructStoreTransactionParameters(Map<String, String> storeTransactionMap) {
    return new Object[] {
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.REMAINING_QTY),
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.REF_TRANSACTION),
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.ESTIMATE_COST),
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.ACTUAL_COST),
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.ST_CODE),
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE),
      storeTransactionMap.get(IFeatureFileCommonKeys.COMPANY),
      storeTransactionMap.get(IFeatureFileCommonKeys.PLANT),
      storeTransactionMap.get(IFeatureFileCommonKeys.STOREHOUSE),
      storeTransactionMap.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
      storeTransactionMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
      storeTransactionMap.get(IFeatureFileCommonKeys.ITEM),
      storeTransactionMap.get(IStoreTransactionFileCommonKeys.UOM),
      storeTransactionMap.get(IFeatureFileCommonKeys.QUANTITY),
      getReferenceDocument(storeTransactionMap)
    };
  }

  public Response requestToReadStoreTransactionsWithFiler(
      Cookie userCookie, Integer pageNumber, Integer recordsPerPage, String fieldFilter) {
    return requestToReadFilteredData(
        userCookie, ITObStoreTransactionsRestUrls.GET_URL, pageNumber, recordsPerPage, fieldFilter);
  }

  public Response requestToReadStoreTransactionsWithFilerAndLocale(
      Cookie userCookie,
      Integer pageNumber,
      Integer recordsPerPage,
      String fieldFilter,
      String locale) {
    return requestToReadFilteredDataWithLocale(
        userCookie,
        ITObStoreTransactionsRestUrls.GET_URL,
        pageNumber,
        recordsPerPage,
        fieldFilter,
        locale);
  }

  public Response requestToReadFilteredData(
      Cookie cookie,
      String prefixUrl,
      Integer pageNumber,
      Integer totalRecordsNumber,
      String fieldFilter) {
    String filteredReadUrl =
        constructReadUrl(prefixUrl, pageNumber, totalRecordsNumber, fieldFilter);
    return sendGETRequest(cookie, filteredReadUrl);
  }

  private String constructReadUrl(
      String prefixUrl, Integer pageNumber, Integer recordsPerPage, String filterString) {
    return prefixUrl
        + "/page="
        + (pageNumber - 1)
        + "/rows="
        + recordsPerPage
        + "/filters="
        + filterString;
  }

  public Response requestToReadFilteredDataWithLocale(
      Cookie cookie,
      String prefixUrl,
      Integer pageNumber,
      Integer totalRecordsNumber,
      String fieldFilter,
      String locale) {
    String filteredReadUrl =
        constructReadUrl(prefixUrl, pageNumber, totalRecordsNumber, fieldFilter);
    return sendGETRequestWithLocale(cookie, locale, filteredReadUrl);
  }

  public Response sendGETRequest(Cookie loginCookie, String restURL) {
    return given()
        .cookie(loginCookie)
        .config(RestAssured.config().jsonConfig(jsonConfig().numberReturnType(BIG_DECIMAL)))
        .contentType(ContentType.JSON)
        .get(urlPrefix + restURL);
  }

  public Response sendGETRequestWithLocale(Cookie loginCookie, String locale, String restURL) {
    return given()
        .cookie(loginCookie)
        .config(RestAssured.config().jsonConfig(jsonConfig().numberReturnType(BIG_DECIMAL)))
        .contentType(ContentType.JSON)
        .header("Accept-Language", locale)
        .get(urlPrefix + restURL);
  }

  public void assertThatStoreTransactionsArePresentedToUser(
      Response response, DataTable storeTransactionsDataTable) throws Exception {
    List<HashMap<String, Object>> actualStoreTransactions = getListOfMapsFromResponse(response);
    List<Map<String, String>> expectedStoreTransactionsMaps =
        storeTransactionsDataTable.asMaps(String.class, String.class);
    for (int i = 0; i < actualStoreTransactions.size(); i++) {
      assertThatStoreTransactionMatchesTheExpected(
          expectedStoreTransactionsMaps.get(i), actualStoreTransactions.get(i));
    }
  }

  private void assertThatStoreTransactionMatchesTheExpected(
      Map<String, String> expectedStoreTransactionMap,
      HashMap<String, Object> actualStoreTransactionMap)
      throws Exception {
    MapAssertion mapAssertion =
        new MapAssertion(expectedStoreTransactionMap, actualStoreTransactionMap);
    mapAssertion.assertValue(
        IStoreTransactionFileCommonKeys.ST_CODE, ITObStoreTransactionGeneralModel.ST_CODE);
    mapAssertion.assertValue(
        IStoreTransactionFileCommonKeys.TRANSACTION_TYPE,
        ITObStoreTransactionGeneralModel.TRANSACTION_OPERATION);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        ITObStoreTransactionGeneralModel.COMPANY_CODE,
        ITObStoreTransactionGeneralModel.COMPANY_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PLANT,
        ITObStoreTransactionGeneralModel.PLANT_CODE,
        ITObStoreTransactionGeneralModel.PLANT_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.STOREHOUSE,
        ITObStoreTransactionGeneralModel.STOREHOUSE_CODE,
        ITObStoreTransactionGeneralModel.STOREHOUSE_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PURCHASE_UNIT,
        ITObStoreTransactionGeneralModel.PURCHASEUNIT_CODE,
        ITObStoreTransactionGeneralModel.PURCHASEUNIT_NAME);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.STOCK_TYPE, ITObStoreTransactionGeneralModel.STOCK_TYPE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.ITEM,
        ITObStoreTransactionGeneralModel.ITEM_CODE,
        ITObStoreTransactionGeneralModel.ITEM_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IStoreTransactionFileCommonKeys.UOM,
        ITObStoreTransactionGeneralModel.UOM_CODE,
        ITObStoreTransactionGeneralModel.UOM_NAME);
    mapAssertion.assertBigDecimalNumberValue(
        IFeatureFileCommonKeys.QUANTITY, ITObStoreTransactionGeneralModel.QUANTITY);
    mapAssertion.assertBigDecimalNumberValue(
        IStoreTransactionFileCommonKeys.ESTIMATE_COST,
        ITObStoreTransactionGeneralModel.ESTIMATE_COST);
    mapAssertion.assertBigDecimalNumberValue(
        IStoreTransactionFileCommonKeys.ACTUAL_COST, ITObStoreTransactionGeneralModel.ACTUAL_COST);
    if (isRefDocumentCodeLegacyDocument(expectedStoreTransactionMap)) {
      assertEquals(
          expectedStoreTransactionMap.get(IStoreTransactionFileCommonKeys.REFERENCE_DOCUMENT),
          actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.REF_DOCUMENT_CODE));
    } else {
      mapAssertion.assertCodeAndLocalizedNameValue(
          IStoreTransactionFileCommonKeys.REFERENCE_DOCUMENT,
          ITObStoreTransactionGeneralModel.REF_DOCUMENT_CODE,
          ITObStoreTransactionGeneralModel.REF_DOCUMENT_TYPE);
    }
    mapAssertion.assertBigDecimalNumberValue(
        IStoreTransactionFileCommonKeys.REMAINING_QTY,
        ITObStoreTransactionGeneralModel.REMAINING_QUANTITY);
  }

  private boolean isRefDocumentCodeLegacyDocument(Map<String, String> expectedStoreTransactionMap) {
    return expectedStoreTransactionMap
        .get(IStoreTransactionFileCommonKeys.REFERENCE_DOCUMENT)
        .contains("Legacy");
  }
}
