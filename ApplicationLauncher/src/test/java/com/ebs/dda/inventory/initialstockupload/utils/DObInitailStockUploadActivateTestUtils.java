package com.ebs.dda.inventory.initialstockupload.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.initialstockupload.IDObInitialStockUploadRestURLS;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Map;

public class DObInitailStockUploadActivateTestUtils extends DObInitialStockUploadCommonTestUtils {

  public DObInitailStockUploadActivateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public Response sendPutInitialStockUpload(Cookie cookie, String initialStockUploadCode) {
    JsonObject jsonObject = new JsonObject();
    return sendPUTRequest(cookie, getActivatetUrl(initialStockUploadCode), jsonObject);
  }

  private String getActivatetUrl(String collectionCode) {
    return IDObInitialStockUploadRestURLS.INITIAL_STOCK_UPLOAD_COMMAND
        + "/"
        + collectionCode
            + IDObInitialStockUploadRestURLS.ACTIVATE;
  }

  public void assertInitialStockUploadDetailsIsUpdatedAfterPost(String initialStockUploadCode, DataTable initialStockUploadDataTable) {
    {
      Map<String, String> expectedInitialStockUploadMap =
              initialStockUploadDataTable.asMaps(String.class, String.class).get(0);
      Object lastUpdatedDate =
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getInitialStockUploadByCodeAndStateAndModifiedByInfo",
                              initialStockUploadCode,
                              "%" + expectedInitialStockUploadMap.get(IFeatureFileCommonKeys.STATE) + "%",
                              expectedInitialStockUploadMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY));

      assertDateEquals(
              expectedInitialStockUploadMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
              new DateTime(lastUpdatedDate).withZone(DateTimeZone.UTC));
    }}
}
