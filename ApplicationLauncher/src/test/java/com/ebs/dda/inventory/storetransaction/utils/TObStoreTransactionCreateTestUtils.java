package com.ebs.dda.inventory.storetransaction.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransaction;
import cucumber.api.DataTable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TObStoreTransactionCreateTestUtils extends TObStoreTransactionCommonTestUtils {

  public TObStoreTransactionCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertStoreTransactionExistsWithCode(String storeTransactionCode) {
    TObStoreTransaction storeTransaction =
        (TObStoreTransaction)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLastStoreTransactionCode");

    assertEquals(storeTransactionCode, storeTransaction.getUserCode());
  }

  public void assertGoodsReceiptItemQuantitiesExist(
      DataTable goodsReceiptItemQuantities, String itemCode, String goodsReceiptCode) {
    List<Map<String, String>> goodsReceiptItemQuantitiesData =
        goodsReceiptItemQuantities.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptItemQuantitiesRow : goodsReceiptItemQuantitiesData) {
      Optional<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
          goodsReceiptItemQuantitiesRowOptional =
              retrieveGoodsReceiptItemQuantitiesOptionalData(
                  goodsReceiptItemQuantitiesRow, itemCode, goodsReceiptCode);
      Assert.assertTrue(goodsReceiptItemQuantitiesRowOptional.isPresent());
    }
  }

  public void assertGoodsReceiptItemsExist(
      String goodsReceiptCode, DataTable goodsReceiptDataTable) {
    List<Map<String, String>> goodsReceiptItemsData =
        goodsReceiptDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptItemsRow : goodsReceiptItemsData) {
      List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel> goodsReceiptList =
          retrieveGoodsReceiptItemData(goodsReceiptItemsRow, goodsReceiptCode);
      Assert.assertTrue(!goodsReceiptList.isEmpty());
    }
  }

  public void assertStoreTransactionsAreCreated(DataTable storeTransactionTable) {
    List<Map<String, String>> StoreTransactionData =
        storeTransactionTable.asMaps(String.class, String.class);
    for (Map<String, String> storeTransactionRow : StoreTransactionData) {
      Object[] goodsReceiptStoreTransaction = retrieveStoreTransactionData(storeTransactionRow);

      Assert.assertNotNull(
          "No "
              + storeTransactionRow.get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE)
              + " StoreTransaction was created on "
              + storeTransactionRow.get(IFeatureFileCommonKeys.CREATION_DATE)
              + " for item "
              + storeTransactionRow.get(IFeatureFileCommonKeys.ITEM)
              + " with qty "
              + storeTransactionRow.get(IFeatureFileCommonKeys.QUANTITY),
          goodsReceiptStoreTransaction);

      assertDateEquals(
          storeTransactionRow.get(IFeatureFileCommonKeys.CREATION_DATE),
          new DateTime(goodsReceiptStoreTransaction[0]).withZone(DateTimeZone.UTC));

      assertDateEquals(
          storeTransactionRow.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE),
          new DateTime(goodsReceiptStoreTransaction[1]).withZone(DateTimeZone.UTC));
    }
  }

  private Object[] retrieveStoreTransactionData(Map<String, String> storeTransactionRow) {
    return (Object[])
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getGoodsReceiptStoreTransactionByAllFields",
            prepareGoodsReceiptStoreTransactionQueryParameters(storeTransactionRow));
  }

  private Object[] prepareGoodsReceiptStoreTransactionQueryParameters(
      Map<String, String> storeTransactionRow) {
    return new Object[] {
      storeTransactionRow.get(IFeatureFileCommonKeys.BATCH_NUMBER),
      storeTransactionRow.get(IStoreTransactionFileCommonKeys.ESTIMATE_COST),
      storeTransactionRow.get(IStoreTransactionFileCommonKeys.ACTUAL_COST),
      storeTransactionRow.get(IStoreTransactionFileCommonKeys.REF_TRANSACTION),
      storeTransactionRow.get(IFeatureFileCommonKeys.STOCK_TYPE),
      storeTransactionRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      storeTransactionRow.get(IFeatureFileCommonKeys.ITEM),
      storeTransactionRow.get(IStoreTransactionFileCommonKeys.UOM),
      storeTransactionRow.get(IFeatureFileCommonKeys.QUANTITY),
      storeTransactionRow.get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE),
      storeTransactionRow.get(IStoreTransactionFileCommonKeys.REFERENCE_DOCUMENT),
      storeTransactionRow.get(IStoreTransactionFileCommonKeys.REMAINING_QTY),
      storeTransactionRow.get(IFeatureFileCommonKeys.COMPANY),
      storeTransactionRow.get(IFeatureFileCommonKeys.PLANT),
      storeTransactionRow.get(IFeatureFileCommonKeys.STOREHOUSE),
    };
  }

  public void assertGoodsReceiptItemBatchesExist(
      String goodsReceiptCode, String batchedItemCode, DataTable itemBatchesTable) {
    List<Map<String, String>> itemBatchesData = itemBatchesTable.asMaps(String.class, String.class);
    for (Map<String, String> itemBatchesRow : itemBatchesData) {
      Optional<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel> itemBatchesOptional =
          retrieveItemBatchesOptionalData(itemBatchesRow, goodsReceiptCode, batchedItemCode);
      Assert.assertTrue(itemBatchesOptional.isPresent());
    }
  }

  private List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
      retrieveGoodsReceiptItemData(Map<String, String> goodsReceiptRow, String goodsReceiptCode) {

    List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
        goodsReceiptOrdinaryReceivedItemGeneralModelList =
            entityManagerDatabaseConnector.executeNativeNamedQuery(
                "getGoodsReceiptItemsForStoreTransaction",
                prepareGoodsReceiptItemDataQueryParameters(goodsReceiptRow, goodsReceiptCode));
    return goodsReceiptOrdinaryReceivedItemGeneralModelList;
  }

  private Object[] prepareGoodsReceiptItemDataQueryParameters(
      Map<String, String> goodsReceiptRow, String goodsReceiptCode) {
    Object[] parameters =
        new Object[] {
          goodsReceiptRow.get(IStoreTransactionFileCommonKeys.TOTAL_RECEIVED_QTY),
          goodsReceiptCode,
          goodsReceiptRow.get(IFeatureFileCommonKeys.ITEM),
          goodsReceiptRow.get(IFeatureFileCommonKeys.STOCK_TYPE)
        };
    return parameters;
  }

  private Optional<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
      retrieveGoodsReceiptItemQuantitiesOptionalData(
          Map<String, String> goodsReceiptRow, String itemCode, String goodsReceiptCode) {

    return Optional.ofNullable(
        (IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptItemsQuantitiesForStoreTransaction",
                prepareGoodsReceiptItemQuantitiesOptionalDataQueryParameters(
                    goodsReceiptRow, itemCode, goodsReceiptCode)));
  }

  private Object[] prepareGoodsReceiptItemQuantitiesOptionalDataQueryParameters(
      Map<String, String> goodsReceiptRow, String itemCode, String goodsReceiptCode) {
    return new Object[] {
      goodsReceiptRow.get(IFeatureFileCommonKeys.STOCK_TYPE),
      goodsReceiptRow.get(IStoreTransactionFileCommonKeys.RECEIVED_QTY),
      goodsReceiptRow.get(IFeatureFileCommonKeys.UOE),
      itemCode,
      goodsReceiptCode
    };
  }

  private Optional<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel>
      retrieveItemBatchesOptionalData(
          Map<String, String> ItemBatchesRow, String goodsReceiptCode, String batchedItemCode) {

    return Optional.ofNullable(
        (IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptBatchedItemsForStoreTransaction",
                prepareItemBatchesOptionalDataQueryParameters(
                    ItemBatchesRow, goodsReceiptCode, batchedItemCode)));
  }

  private Object[] prepareItemBatchesOptionalDataQueryParameters(
      Map<String, String> ItemBatchesRow, String goodsReceiptCode, String batchedItemCode) {
    Object[] parameters =
        new Object[] {
          ItemBatchesRow.get(IFeatureFileCommonKeys.STOCK_TYPE),
          ItemBatchesRow.get(IFeatureFileCommonKeys.BATCH_NUMBER),
          ItemBatchesRow.get(IFeatureFileCommonKeys.PRODUCTION_DATE),
          ItemBatchesRow.get(IFeatureFileCommonKeys.EXPIRATION_DATE),
          Double.parseDouble(ItemBatchesRow.get(IStoreTransactionFileCommonKeys.BATCHED_QTY)),
          ItemBatchesRow.get(IFeatureFileCommonKeys.UOE),
          ItemBatchesRow.get(IStoreTransactionFileCommonKeys.RECEIVED_QTY_BASE),
          goodsReceiptCode,
          batchedItemCode
        };
    return parameters;
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptCompany.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    return str.toString();
  }

  public void insertStoreTransactions(DataTable storeTransactions) {
    List<Map<String, String>> storeTransactionListMap =
        storeTransactions.asMaps(String.class, String.class);
    for (Map<String, String> storeTransactionDataMap : storeTransactionListMap) {
      String documentType =
          storeTransactionDataMap.get(IStoreTransactionFileCommonKeys.REFERENCE_DOCUMENT_TYPE);
      String query =
          "GoodsReceipt".equals(documentType)
              ? "createStoreTransaction"
              : "createStoreTransactionRecordsByGoodsIssueRefDocument";
      entityManagerDatabaseConnector.executeInsertQuery(
          query, constructStoreTransactionDataInsertionParams(storeTransactionDataMap));
    }
  }

  private Object[] constructStoreTransactionDataInsertionParams(
      Map<String, String> storeTransactionDataMap) {
    Object[] parameters =
        new Object[] {
          storeTransactionDataMap.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.ITEM),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.UOM),
          new BigDecimal(storeTransactionDataMap.get(IFeatureFileCommonKeys.QUANTITY)),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.BATCH_NUMBER).isEmpty()
              ? null
              : storeTransactionDataMap.get(IFeatureFileCommonKeys.BATCH_NUMBER),
          !storeTransactionDataMap.get(IFeatureFileCommonKeys.REMAINING_QTY).isEmpty()
              ? new BigDecimal(storeTransactionDataMap.get(IFeatureFileCommonKeys.REMAINING_QTY))
              : null,
          storeTransactionDataMap.get(IFeatureFileCommonKeys.REF_TRANSACTION),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.TRANSACTION_TYPE),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.COMPANY),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.PLANT),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.STOREHOUSE),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
          storeTransactionDataMap.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE),
          !storeTransactionDataMap.get(IStoreTransactionFileCommonKeys.ACTUAL_COST).isEmpty()
              ? new BigDecimal(
                  storeTransactionDataMap.get(IStoreTransactionFileCommonKeys.ACTUAL_COST))
              : null,
          !storeTransactionDataMap.get(IStoreTransactionFileCommonKeys.ESTIMATE_COST).isEmpty()
              ? new BigDecimal(
                  storeTransactionDataMap.get(IStoreTransactionFileCommonKeys.ESTIMATE_COST))
              : null,
          storeTransactionDataMap.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE),
          storeTransactionDataMap.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT),
        };
    return parameters;
  }
}
