package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.MapAssertion;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.IIObGoodsIssueRefDocumentDataGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.Map;

public class IObGoodsIssueViewRefDocumentDataTestUtils extends DObGoodsIssueCommonTestUtils {

    public String getViewRefDocumentDataUrl(String code) {
        return IDObGoodsIssueRestURLS.BASE_URL + "/" + code + IDObGoodsIssueRestURLS.VIEW_REF_DOCUMENT_DATA;
    }

    public void assertThatViewRefDocumentDataResponseIsCorrect(
      Response response, DataTable refDocumentDataTable) throws Exception {
    Map<String, String> refDocumentDataExpected =
        refDocumentDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> refDocumentDataActual = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(refDocumentDataExpected, refDocumentDataActual);
    assertActualAndExpectedDataFromResponse(
        mapAssertion, isGoodsIssueSalesInvoice(refDocumentDataExpected));
  }

  private boolean isGoodsIssueSalesInvoice(Map<String, String> refDocumentDataExpected) {
    return (refDocumentDataExpected.get(IGoodsIssueFeatureFileCommonKeys.KAP) != null);
  }

  private void assertActualAndExpectedDataFromResponse(
      MapAssertion mapAssertion, boolean goodsIssueSalesInvoice) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IGoodsIssueFeatureFileCommonKeys.CUSTOMER,
        IIObGoodsIssueRefDocumentDataGeneralModel.CUSTOMER_CODE,
        IIObGoodsIssueRefDocumentDataGeneralModel.CUSTOMER_NAME);

    mapAssertion.assertLocalizedValue(
        IGoodsIssueFeatureFileCommonKeys.ADDRESS,
        IIObGoodsIssueRefDocumentDataGeneralModel.ADDRESS);

    mapAssertion.assertValue(
        IGoodsIssueFeatureFileCommonKeys.CONTACT_PERSON,
        IIObGoodsIssueRefDocumentDataGeneralModel.CONTACT_PERSON_NAME_EN);

    mapAssertion.assertValue(
        IGoodsIssueFeatureFileCommonKeys.REF_DOCUMENT,
        IIObGoodsIssueRefDocumentDataGeneralModel.REF_DOCUMENT);

    mapAssertion.assertLocalizedValue(
        IGoodsIssueFeatureFileCommonKeys.SALES_REPRESENTATIVE,
        IIObGoodsIssueRefDocumentDataGeneralModel.SALES_REPRESENTATIVE_NAME);

    if (goodsIssueSalesInvoice) {
      mapAssertion.assertCodeAndLocalizedNameValue(
          IGoodsIssueFeatureFileCommonKeys.KAP,
          IIObGoodsIssueRefDocumentDataGeneralModel.KAP_CODE,
          IIObGoodsIssueRefDocumentDataGeneralModel.KAP_NAME);
    }
    mapAssertion.assertValue(
        IGoodsIssueFeatureFileCommonKeys.COMMENTS,
        IIObGoodsIssueRefDocumentDataGeneralModel.COMMENTS);
  }
}
