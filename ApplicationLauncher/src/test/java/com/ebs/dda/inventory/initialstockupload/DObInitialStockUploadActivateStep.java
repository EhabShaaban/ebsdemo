package com.ebs.dda.inventory.initialstockupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.initialstockupload.utils.DObInitailStockUploadActivateTestUtils;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObInitialStockUploadActivateStep extends SpringBootRunner implements En {

  private DObInitailStockUploadActivateTestUtils utils;
  private DObStockTransformationActivateTestUtils stockTransformationActivateTestUtils;

  public DObInitialStockUploadActivateStep() {
    When(
        "^\"([^\"]*)\" activates InitialStockUpload with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String initialStockUploadCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);

          Response response = utils.sendPutInitialStockUpload(cookie, initialStockUploadCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^InitialStockUpload with code \"([^\"]*)\" is updated as follows:$",
        (String initialStockUploadCode, DataTable initialStockUploadDataTable) -> {
          utils.assertInitialStockUploadDetailsIsUpdatedAfterPost(
              initialStockUploadCode, initialStockUploadDataTable);
        });

    Given(
        "^the Last StoreTransaction code is \"([^\"]*)\"$",
        (String storeTransactionCode) -> {
          utils.assertThatLastEntityCodeIs("TObStoreTransaction", storeTransactionCode);
        });

    Then(
        "^the following StoreTransactions are created:$",
        (DataTable storeTransactionDataTable) -> {
          stockTransformationActivateTestUtils.assertThatStoreTransactionsCreated(
              storeTransactionDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitailStockUploadActivateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    stockTransformationActivateTestUtils =
        new DObStockTransformationActivateTestUtils(getProperties());
    stockTransformationActivateTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate InitialStockUpload -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearISUInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Activate InitialStockUpload -")) {
      databaseConnector.closeConnection();
    }
  }
}
