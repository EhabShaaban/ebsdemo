package com.ebs.dda.inventory.stocktransformation;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

import java.util.Map;

public class DObStockTransformationActivateAddStoreTransactionStep extends SpringBootRunner
    implements En {

  private DObStockTransformationActivateTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObStockTransformationActivateTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObStockTransformationActivateAddStoreTransactionStep() {
    Given(
        "^the Last StoreTransaction code is \"([^\"]*)\"$",
        (String storeTransactioncode) -> {
          utils.assertLastCreatedStoreTransaction(storeTransactioncode);
        });

    Then(
        "^the RemainingQty in the following StoreTransaction is updated as follows:$",
        (DataTable updatedStoreTransacrion) -> {
          utils.assertThatStoreTransactionsUpdated(updatedStoreTransacrion);
        });

    Then(
        "^the following StoreTransactions are created:$",
        (DataTable storeTransactionsDataTable) -> {
          utils.assertThatStoreTransactionsCreated(storeTransactionsDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate draft StockTransformation -")) {
      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate draft StockTransformation -")) {
      databaseConnector.closeConnection();
    }
  }
}
