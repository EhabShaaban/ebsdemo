package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptSaveItemDetailHappyPathTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DObGoodsReceiptSaveItemDetailHappyPathStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptSaveItemDetailHappyPathTestUtils utils;

  public DObGoodsReceiptSaveItemDetailHappyPathStep() {
    When(
        "^\"([^\"]*)\" saves Item Detail with id \"([^\"]*)\" of type \"([^\"]*)\" in Item with code \"([^\"]*)\" to ReceivedItems section in GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
         String detailId,
         String detailType,
         String itemCode,
         String goodsReceiptCode,
         String dateTime,
         DataTable detailDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String postfixUrl =
              IDObGoodsReceiptRestUrls.SAVE_ITEM_DETAIL
                  + goodsReceiptCode
                  + "/"
                  + itemCode
                  + "/"
                  + Long.parseLong(detailId);
          Response response = utils.saveItemDetail(cookie, postfixUrl, detailDataTable, detailType);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
            "^Item Detail with id \"([^\"]*)\" of type \"([^\"]*)\" in Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" of type \"([^\"]*)\" is updated as follows:$",
            (String detailId,
             String detailType,
             String itemCode,
             String goodsReceiptCode,
             String goodsReceiptType,
             DataTable detailDataTable) -> {
                utils.assertThatItemDetailIsUpdated(detailType, goodsReceiptType, detailDataTable);
            });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObGoodsReceiptSaveItemDetailHappyPathTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Save GR Item Detail")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScriptPath());
        }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
      if (contains(scenario.getName(), "Save GR Item Detail")) {
          utils.unfreeze();
          userActionsTestUtils.unlockAllSections(
                  IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
                  new ArrayList<>(Collections.singletonList(IGoodsReceiptSectionNames.ITEMS_SECTION)));

          databaseConnector.closeConnection();
      }
  }
}
