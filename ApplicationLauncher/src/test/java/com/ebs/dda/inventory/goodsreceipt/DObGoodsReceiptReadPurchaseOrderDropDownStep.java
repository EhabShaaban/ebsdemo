package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptReadPurchaseOrderDropDownTestUtils;
import com.ebs.dda.purchases.IDObPurchaseOrderRestUrls;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObGoodsReceiptReadPurchaseOrderDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptReadPurchaseOrderDropDownTestUtils utils;

  public DObGoodsReceiptReadPurchaseOrderDropDownStep() {

    Given(
        "^the following are ALL existing PurchaseOrders:$",
        (DataTable purchaseOrderDataTable) -> {
          utils.assertPurchaseOrdersExist(purchaseOrderDataTable);
        });

    Given(
        "^the total number of existing records of PurchaseOrders is (\\d+)$",
        (Long totalRecordsNumber) -> {
          utils.assertThatTotalNumberOfPurchaseOrdersEquals(totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read drop-down of PurchaseOrder for State \\(Shipped,NotReceived | Cleared,NotReceived\\)$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IDObPurchaseOrderRestUrls.VIEW_PURCHASE_ORDERS_FOR_GR);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following PurchaseOrders will be presented to \"([^\"]*)\":$",
        (String userName, DataTable purchaseOrderDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertPurchaseOrderResponseIsCorrect(purchaseOrderDataTable, response);
        });
    Then(
        "^the total number of PurchaseOrders returned to \"([^\"]*)\" is (\\d+)$",
        (String userName, Integer purchaseOrdersNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, purchaseOrdersNumber);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObGoodsReceiptReadPurchaseOrderDropDownTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of PurchaseOrders dropdown")) {
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }
}
