package com.ebs.dda.inventory.stocktransformation;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObStockTransformationActivateStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObStockTransformationActivateTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObStockTransformationActivateTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObStockTransformationActivateStep() {

    Given(
        "^the following GeneralData section for StockTransformations exist:$",
        (DataTable stockTransformationsDataTable) -> {
          utils.assertThatGeneralStockTransformationsExits(stockTransformationsDataTable);
        });

    Given(
        "^the following StockTransformationDetails section for StockTransformations exist:$",
        (DataTable stockTransformationDetailsDataTable) -> {
          utils.assertThatStockTransformationDetailsExits(stockTransformationDetailsDataTable);
        });

    Given(
        "^the RefStoreTransaction in above StockTransformations has the following data:$",
        (DataTable storeTransactionDatatable) -> {
          utils.assertThatStoreTransactionDataExits(storeTransactionDatatable);
        });

    When(
        "^\"([^\"]*)\" Activates StockTransformation with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String stockTransformationCode, String activationDate) -> {
          utils.freeze(activationDate);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.activateStockTransformation(userCookie, stockTransformationCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^StockTransformation with Code \"([^\"]*)\" is updated as follows:$",
        (String stockTransformationCode, DataTable stockTransformationDataTable) -> {
          utils.assertThatStockTransformationIsUpdated(
              stockTransformationCode, stockTransformationDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate StockTransformation -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate StockTransformation -")) {
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
