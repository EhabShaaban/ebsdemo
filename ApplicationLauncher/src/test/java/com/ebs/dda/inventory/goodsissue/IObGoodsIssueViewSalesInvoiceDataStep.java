package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.IObGoodsIssueViewSalesInvoiceDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObGoodsIssueViewSalesInvoiceDataStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private IObGoodsIssueViewSalesInvoiceDataTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new IObGoodsIssueViewSalesInvoiceDataTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public IObGoodsIssueViewSalesInvoiceDataStep() {

        Given(
                "^the following SalesInvoiceData for GoodsIssues exist:$",
                (DataTable consigneeDataTable) -> {
                    utils.assertThatGoodsIssueHasRefDocumentData(consigneeDataTable);
                });

        When(
                "^\"([^\"]*)\" requests to view SalesInvoice section of GoodsIssue with code \"([^\"]*)\"$",
                (String userName, String giCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.getViewRefDocumentDataUrl(giCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of SalesInvoice section for GoodsIssue with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String giCode, String userName, DataTable consigneeDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertThatViewRefDocumentDataResponseIsCorrect(response, consigneeDataTable);
        });
  }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View GI SalesInvoice section -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {

                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View GI SalesInvoice section -")) {
            databaseConnector.closeConnection();
        }
    }
}
