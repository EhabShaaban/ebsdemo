package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceItemsViewTestUtils;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueCreateTestUtils;
import com.ebs.dda.inventory.storekeeper.StorekeeperTestUtils;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderAllowedActionsObjectScreenTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewCompanyStoreDataTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewSalesOrderDataTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSaveConsigneeTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.sql.SQLException;
import java.util.Map;

public class DObGoodsIssueCreateStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private DObGoodsIssueCreateTestUtils utils;
    private CObEnterpriseTestUtils cObEnterpriseTestUtils;
    private StorekeeperTestUtils storekeeperTestUtils;
    private DObPurchaseOrderSaveConsigneeTestUtils purchaseOrderSaveConsigneeTestUtils;
    private DObSalesInvoiceTestUtils salesInvoiceTestUtils;
    private IObSalesInvoiceItemsViewTestUtils salesInvoiceViewItemsTestUtils;
    private IObSalesOrderViewCompanyStoreDataTestUtils iObSalesOrderViewCompanyStoreDataTestUtils;
    private DObSalesOrderAllowedActionsObjectScreenTestUtils
            dObSalesOrderAllowedActionsObjectScreenTestUtils;
    private IObSalesOrderViewSalesOrderDataTestUtils iObSalesOrderViewSalesOrderDataTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();

        utils = new DObGoodsIssueCreateTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        storekeeperTestUtils = new StorekeeperTestUtils(entityManagerDatabaseConnector);
        cObEnterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
        purchaseOrderSaveConsigneeTestUtils = new DObPurchaseOrderSaveConsigneeTestUtils(properties);
        purchaseOrderSaveConsigneeTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);

        salesInvoiceViewItemsTestUtils = new IObSalesInvoiceItemsViewTestUtils(properties);
        salesInvoiceViewItemsTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
        salesInvoiceTestUtils = new DObSalesInvoiceTestUtils(properties);
        salesInvoiceTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        dObSalesOrderAllowedActionsObjectScreenTestUtils =
                new DObSalesOrderAllowedActionsObjectScreenTestUtils(
                        properties, entityManagerDatabaseConnector);
        iObSalesOrderViewCompanyStoreDataTestUtils =
                new IObSalesOrderViewCompanyStoreDataTestUtils(properties, entityManagerDatabaseConnector);
        iObSalesOrderViewSalesOrderDataTestUtils =
                new IObSalesOrderViewSalesOrderDataTestUtils(properties, entityManagerDatabaseConnector);
    }

  public DObGoodsIssueCreateStep() {

      Given(
              "^the following GoodsIssue Types exist:$",
              (DataTable goodsIssueTypes) -> {
                  utils.assertThatGoodsIssueTypesExist(goodsIssueTypes);
              });

      Given(
              "^the following Plants exist:$",
              (DataTable plantsDataTable) -> {
                  cObEnterpriseTestUtils.assertThatPlantsExist(plantsDataTable);
              });

      Given(
              "^the following SalesOrders exist:$",
              (DataTable salesOrderDataTable) -> {
                  dObSalesOrderAllowedActionsObjectScreenTestUtils.assertThatSalesOrdersExist(
                          salesOrderDataTable);
              });

      Given(
              "^the following SalesOrderData for the following SalesOrders exist:$",
              (DataTable SalesOrderDataTable) -> {
                  iObSalesOrderViewSalesOrderDataTestUtils.assertThatTheFollowingSalesOrderDataExist(
                          SalesOrderDataTable);
              });

      Given(
              "^the following SalesOrders have the following Items:$",
              (DataTable salesOrderItemsDataTable) -> {
                  utils.assertThatTheFollowingSalesOrderItemsExist(salesOrderItemsDataTable);
              });

      Given(
              "^the following CompanyAndStoreData for the following SalesOrders exist:$",
              (DataTable companyStoreDataTable) -> {
                  iObSalesOrderViewCompanyStoreDataTestUtils.assertThatTheFollowingCompanyAndStoreDataExist(
                          companyStoreDataTable);
              });

      Then(
              "^the SalesOrder Section of GoodsIssue with code \"([^\"]*)\" is updated as follows:$",
              (String goodsIssueCode, DataTable goodsIssueSalesOrderDataTable) -> {
                  utils.assertThatSalesOrderSectionExist(goodsIssueCode, goodsIssueSalesOrderDataTable);
              });

      Given(
              "^the following Storehouses exist:$",
              (DataTable storehousesDataTable) -> {
                  cObEnterpriseTestUtils.assertThatStorehousesExist(storehousesDataTable);
              });

      Given(
              "^the following Storekeepers exist:$",
              (DataTable storekeepersDataTable) -> {
                  storekeeperTestUtils.assertThatStorekeepersExist(storekeepersDataTable);
              });
      And(
              "^the following SalesInvoices exist:$",
              (DataTable salesInvoicesDataTable) -> {
                  salesInvoiceTestUtils.assertSalesInvoiceExists(salesInvoicesDataTable);
              });

      Given(
              "^the following Plants are related to Company with code \"([^\"]*)\":$",
              (String companyCode, DataTable plantsDataTable) -> {
                  purchaseOrderSaveConsigneeTestUtils.assertThatPlantsRelateToCompany(
                          plantsDataTable, companyCode);
              });

      Given(
        "^the following Storehouses for Plant with code \"([^\"]*)\" exist:$",
        (String plantCode, DataTable storehousesDataTable) -> {
          purchaseOrderSaveConsigneeTestUtils.assertThatStorehousesRelateToPlant(
              storehousesDataTable, plantCode);
        });
    And(
        "^the following Customer for SalesInvoice with code \"([^\"]*)\" exist:$",
        (String salesInvoiceCode, DataTable customersDataTable) -> {
          utils.assertThatCustomerExistForSalesInvoice(salesInvoiceCode, customersDataTable);
        });
    And(
        "^the following Company for SalesInvoice with code \"([^\"]*)\" exist:$",
        (String salesInvoiceCode, DataTable salesInvoiceCompanyDataTable) -> {
          utils.assertThatCompanyExistForSalesInvoice(
              salesInvoiceCode, salesInvoiceCompanyDataTable);
        });

    And(
        "^SalesInvoice with code \"([^\"]*)\" has the following Items:$",
        (String salesInvoiceCode, DataTable itemsTable) -> {
          salesInvoiceViewItemsTestUtils.assertThatSalesInvoiceHasItems(
              salesInvoiceCode, itemsTable);
        });
    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String dateTime) -> {
          utils.freeze(dateTime);
        });

    Given(
        "^Last created GoodsIssue was with code \"([^\"]*)\"$",
        (String goodsIssueCode) -> {
          DObGoodsIssueGeneralModel goodsIssue =
              (DObGoodsIssueGeneralModel)
                  entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                      "getLastCreatedGoodsIssue", goodsIssueCode);
          Assert.assertEquals(goodsIssue.getUserCode(), goodsIssueCode);
        });

    When(
        "^\"([^\"]*)\" creates GoodsIssue with the following values:$",
        (String username, DataTable GoodsIssueTable) -> {
          Response response =
              utils.createGoodsIssue(GoodsIssueTable, userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^a new GoodsIssue is created with the following values:$",
        (DataTable goodsIssueDataTable) -> {
          utils.assertGoodsIssueIsCreatedSuccessfully(goodsIssueDataTable);
        });

    Then(
        "^the SalesInvoice Section of GoodsIssue with code \"([^\"]*)\" is updated as follows:$",
        (String goodsIssueCode, DataTable goodsIssueDataTable) -> {
            utils.assertGoodsIssueSalesInvoiceSectionIsCreatedSuccessfully(goodsIssueDataTable);
        });

    Then(
        "^the Company Section of GoodsIssue with code \"([^\"]*)\" is updated as follows:$",
        (String goodsIssueCode, DataTable goodsIssueDataTable) -> {
          utils.assertGoodsIssueStoreIsCreatedSuccessfully(goodsIssueCode, goodsIssueDataTable);
        });
    And(
        "^the Items Section of GoodsIssue with code \"([^\"]*)\" is updated as follows:$",
        (String goodsIssueCode, DataTable itemsDataTable) -> {
          utils.assertThatGoodsIssueHasItems(goodsIssueCode, itemsDataTable);
        });

      When(
              "^\"([^\"]*)\" requests to create GoodsIssue$",
              (String userName) -> {
                  Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                  Response response = utils.requestCreateGoodsIssue(userCookie);
                  userActionsTestUtils.setUserResponse(userName, response);
              });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create GoodsIssue -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
        databaseConnector.executeSQLScript(utils.getDBScript());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create GoodsIssue -")) {
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
