package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptCreateTestUtils;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptCreationValueObject;
import com.ebs.dda.masterdata.item.utils.MObItemCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObGoodsReceiptCreateValStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptCreateTestUtils utils;
  private MObItemCommonTestUtils itemTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObGoodsReceiptCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    itemTestUtils = new MObItemCommonTestUtils(getProperties());
    itemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObGoodsReceiptCreateValStep() {
    And(
        "^the following error message is attached to PurchaseOrder field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObGoodsReceiptCreationValueObject.REF_DOCUMENT_CODE);
        });
    And(
        "^the following error message is attached to SalesReturn field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObGoodsReceiptCreationValueObject.REF_DOCUMENT_CODE);
        });

    Then(
        "^the following error message is attached to Storehouse field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObGoodsReceiptCreationValueObject.STOREHOUSE);
        });

    Then(
        "^the following error message is attached to Storekeeper field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObGoodsReceiptCreationValueObject.STOREKEEPER);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create GoodsReceipt Val -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create GoodsReceipt Val -")) {
      databaseConnector.closeConnection();
    }
  }
}
