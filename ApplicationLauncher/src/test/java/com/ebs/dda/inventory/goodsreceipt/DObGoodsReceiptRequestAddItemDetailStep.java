package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObGoodsReceiptRequestAddItemDetailStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObGoodsReceiptItemTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new IObGoodsReceiptItemTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

  public DObGoodsReceiptRequestAddItemDetailStep() {

    When(
        "^\"([^\"]*)\" requests to add Item Quantity for Item with code \"([^\"]*)\" to Items section of GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode) -> {
          String lockURL =
              IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new add Item Quantity side nav is opened and Items section of GoodsReceipt with code \"([^\"]*)\" is locked by \"([^\"]*)\"$",
        (String goodsReceiptCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first requested to add Item Quantity for Item with code \"([^\"]*)\" to Items section of GoodsReceipt with code \"([^\"]*)\" successfully$",
        (String userName, String itemCode, String goodsReceiptCode) -> {
          String lockURL =
              IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, goodsReceiptCode);
          utils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" opened Item Quantity for Item with code \"([^\"]*)\" to Items section of GoodsReceipt with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving Item Quantity for Item with code \"([^\"]*)\" of GoodsReceipt with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockURL =
              IDObGoodsReceiptRestUrls.REQUEST_CANCEL_ADD_ITEM_DETAIL
                  + goodsReceiptCode
                  + '/'
                  + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on Items section of GoodsReceipt with Code \"([^\"]*)\" is released$",
        (String userName, String goodsReceiptCode) -> {
          utils.assertThatLockIsReleased(
              userName, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving Item Quantity for Item with code \"([^\"]*)\" of GoodsReceipt with Code \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode) -> {
          String unlockURL =
              IDObGoodsReceiptRestUrls.REQUEST_CANCEL_ADD_ITEM_DETAIL
                  + goodsReceiptCode
                  + '/'
                  + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Add/Cancel GR Item Detail -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to Add/Cancel GR Item Detail -")) {
      utils.unfreeze();
      userActionsTestUtils.unlockSectionForAllUsers(IDObGoodsReceiptRestUrls.CANCEL_REQUEST_ADD_ITEM);
      databaseConnector.closeConnection();
    }
  }
}
