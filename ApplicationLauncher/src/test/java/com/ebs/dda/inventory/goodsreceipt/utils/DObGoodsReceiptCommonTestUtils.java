package com.ebs.dda.inventory.goodsreceipt.utils;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;

public class DObGoodsReceiptCommonTestUtils extends CommonTestUtils {

  public static final String CREATION_DATE = "CreationDateTime";
  public static final String STATE = "State";
  public static final String CODE = "Code";
  public static final String USER_CODE = "userCode";
  public static final String TYPE = "Type";
  public static final String COMMENTS = "comments";
  public static final String GR_PURCHASERESPONSIBLE_NAME = "purchaseResponsibleName";
  public static final String GR_COSTING_DOCUMENT_CODE = "costingDocumentCode";
  public static final String GR_VENDOR_NAME = "vendorName";
  public static final String GR_PURCHASEORDER_CODE = "purchaseOrderCode";
  public static final String GR_DELIVERYNOTE = "deliveryNote";
  public String deleteRestUrl;
  protected String requestLocale = "en";
  private String GR_JMX_LOCK_BEAN_NAME = "GoodsReceiptEntityLockCommand";

  public DObGoodsReceiptCommonTestUtils() {
    deleteRestUrl = "/command/goodsreceipt/delete/";
  }

  public static String clearGoodsReceipts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/goods-receipt/ClearDObGoodsReceipt.sql");
    return str.toString();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/LookupSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts_ViewAll.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append("," + "db-scripts/sales/IObSISOItems.sql");
    str.append("," + "db-scripts/order/salesreturnorder/CObSalesReturnOrderType.sql");
    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder.sql");
    str.append(
        "," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    return str.toString();
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptSalesReturnDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptCompany.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptActivationDetailsScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    return str.toString();
  }

  public void createGoodsReceiptSalesReturnGeneralData(DataTable goodsReceiptHeaderData) {
    List<Map<String, String>> headers = goodsReceiptHeaderData.asMaps(String.class, String.class);
    for (Map<String, String> header : headers) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsReceiptSalesReturnGeneralData",
          constructInsertGoodsReceiptSalesReturnGeneralDataQueryParameters(header));
    }
  }

  private Object[] constructInsertGoodsReceiptSalesReturnGeneralDataQueryParameters(
      Map<String, String> header) {
    return new Object[] {
      header.get(IFeatureFileCommonKeys.CODE),
      header.get(IFeatureFileCommonKeys.CREATION_DATE),
      header.get(IFeatureFileCommonKeys.CREATION_DATE),
      header.get(IFeatureFileCommonKeys.CREATED_BY),
      header.get(IFeatureFileCommonKeys.CREATED_BY),
      header.get(IFeatureFileCommonKeys.STATE),
      header.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      header.get(IFeatureFileCommonKeys.CODE),
      header.get(IFeatureFileCommonKeys.TYPE)
    };
  }

  public void createGoodsReceiptSalesReturnCompany(DataTable storeDataTable) {

    List<Map<String, String>> goodsReceiptCompanyMapList =
        storeDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsReceiptCompanyMap : goodsReceiptCompanyMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsReceiptSalesReturnCompany",
          constructInsertGoodsReceiptSalesReturnCompanyQueryParameters(goodsReceiptCompanyMap));
    }
  }

  private Object[] constructInsertGoodsReceiptSalesReturnCompanyQueryParameters(
      Map<String, String> GoodsReceiptStoreData) {
    return new Object[] {
      GoodsReceiptStoreData.get(IFeatureFileCommonKeys.GR_CODE),
      GoodsReceiptStoreData.get(IInventoryFeatureFileCommonKeys.COMPANY),
      GoodsReceiptStoreData.get(IInventoryFeatureFileCommonKeys.PLANT),
      GoodsReceiptStoreData.get(IInventoryFeatureFileCommonKeys.STOREHOUSE),
      GoodsReceiptStoreData.get(IFeatureFileCommonKeys.STOREKEEPER)
    };
  }

  public void createGoodsReceiptSalesReturnData(DataTable goodsReceiptSalesReturnDataTable) {

    List<Map<String, String>> goodsReceiptSalesReturns =
        goodsReceiptSalesReturnDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsReceiptSalesReturn : goodsReceiptSalesReturns) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsReceiptSalesReturnData",
          constructInsertGoodsReceiptSalesReturnDataQueryParameters(goodsReceiptSalesReturn));
    }
  }

  private Object[] constructInsertGoodsReceiptSalesReturnDataQueryParameters(
      Map<String, String> goodsReceiptSalesReturn) {
    return new Object[] {
      goodsReceiptSalesReturn.get(IFeatureFileCommonKeys.GR_CODE),
      goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_CUSTOMER),
      goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN),
      goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_DOCUMENT_OWNER),
      goodsReceiptSalesReturn.get(IGoodsReceiptFeatureFileCommonKeys.NOTES)
    };
  }

  public void createGoodsReceiptSalesReturnItem(
      String goodsReceiptCode, DataTable goodsReceiptDataTable) {
    List<Map<String, String>> goodsReceiptItemsData =
        goodsReceiptDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptItemsRow : goodsReceiptItemsData) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsReceiptSalesReturnItem",
          constructInsertGoodsReceiptSalesReturnItemQueryParameters(
              goodsReceiptItemsRow, goodsReceiptCode));
    }
  }

  private Object[] constructInsertGoodsReceiptSalesReturnItemQueryParameters(
      Map<String, String> goodsReceiptItemRow, String goodsReceiptCode) {
    return new Object[] {
      goodsReceiptCode,
      goodsReceiptItemRow.get(IFeatureFileCommonKeys.ITEM),
      goodsReceiptItemRow.get(IFeatureFileCommonKeys.DIFFERENCE_REASON),
      Boolean.parseBoolean(goodsReceiptItemRow.get(IFeatureFileCommonKeys.IS_BATCH_MANAGED))
    };
  }

  public void createGoodsReceiptSalesReturnItemQuantity(
      String goodsReceiptCode, String item, DataTable itemQuantitiesDataTable) {

    List<Map<String, String>> expectedItemQuantitiesMapList =
        itemQuantitiesDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedItemQuantity : expectedItemQuantitiesMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsReceiptSalesReturnItemQuantity",
          constructInsertGoodsReceiptSalesReturnItemQuantityQueryParams(
              goodsReceiptCode, item, expectedItemQuantity));
    }
  }

  private Object[] constructInsertGoodsReceiptSalesReturnItemQuantityQueryParams(
      String goodsReceiptCode, String item, Map<String, String> expectedItemQuantity) {
    return new Object[] {
      goodsReceiptCode,
      item,
      expectedItemQuantity.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
      expectedItemQuantity.get(IFeatureFileCommonKeys.UOE),
      expectedItemQuantity.get(IFeatureFileCommonKeys.STOCK_TYPE),
      expectedItemQuantity.get(IGoodsReceiptFeatureFileCommonKeys.NOTES),
    };
  }

  public void assertThatResourceIsLockedByUser(String userName, String userCode, String sectionName)
      throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name = new ObjectName("com.ebs.dda.commands.general:name=" + GR_JMX_LOCK_BEAN_NAME);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, userCode};
    String[] parameterTypes =
        new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
        (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);
    assertNotNull(lockReleaseDetails);
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, GR_JMX_LOCK_BEAN_NAME, sectionName);
  }

  public void assertGoodsReceiptExistsWithData(DataTable goodsReceiptDataTable) {
    List<Map<String, String>> goodsReceipts =
        goodsReceiptDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsReceipt : goodsReceipts) {
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
          (DObGoodsReceiptGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptByCodeStateBusinessUnitBusinessPartnerTypeAndRefDocument",
                  constructGoodsReceiptQueryParameters(goodsReceipt));
      assertNotNull(
          goodsReceiptGeneralModel,
          goodsReceipt.get(IFeatureFileCommonKeys.CODE) + " doesn't exist!");
    }
  }

  private Object[] constructGoodsReceiptQueryParameters(Map<String, String> goodsReceipt) {
    return new Object[] {
      goodsReceipt.get(IFeatureFileCommonKeys.CODE),
      goodsReceipt.get(IFeatureFileCommonKeys.TYPE),
      goodsReceipt.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      goodsReceipt.get(IInventoryFeatureFileCommonKeys.BUSINESS_PARTNER),
      "%" + goodsReceipt.get(IFeatureFileCommonKeys.STATE) + "%",
      goodsReceipt.get(IInventoryFeatureFileCommonKeys.REF_DOCUMENT)
    };
  }

  public void assertGoodsReceiptExist(DataTable goodsReceiptDataTable) {
    List<Map<String, String>> goodsReceipts =
        goodsReceiptDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsReceipt : goodsReceipts) {
      assertGoodsReceiptExists(goodsReceipt);
    }
  }

  private void assertGoodsReceiptExists(Map<String, String> goodsReceipt) {
    String goodsReceiptCode = goodsReceipt.get(IFeatureFileCommonKeys.CODE);
    String state = goodsReceipt.get(IFeatureFileCommonKeys.STATE);
    String businessUnit = goodsReceipt.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    String type = goodsReceipt.get(IFeatureFileCommonKeys.TYPE);
    Object[] queryParameters =
        getGoodsReceiptQueryParameters(goodsReceiptCode, state, businessUnit, type);
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel = getDObGoodsReceipt(queryParameters);
    assertNotNull(goodsReceiptGeneralModel, goodsReceiptCode + " Doesn't exist!");
  }

  private DObGoodsReceiptGeneralModel getDObGoodsReceipt(Object[] queryParameters) {
    return (DObGoodsReceiptGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getGoodsReceiptByCodeStatePurchaseUnitAndType", queryParameters);
  }

  private Object[] getGoodsReceiptQueryParameters(
      String goodsReceiptCode, String state, String businessUnit, String type) {
    return new Object[] {goodsReceiptCode, '%' + state + '%', businessUnit, type};
  }

  public Response deleteGoodsReceipt(String goodsReceiptCode, Cookie loginCookie) {
    String deleteURL = deleteRestUrl + goodsReceiptCode;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  protected void updateRestURLs() {
    deleteRestUrl = "/command/goodsreceipt/delete/";
  }

  public void assertCorrectShownData(
      Response response, DataTable goodsReceiptsDataTable, int index) {
    HashMap<String, Object> responseGoodsReceipt = getListOfMapsFromResponse(response).get(index);
    Map<String, String> goodsReceiptsExpectedData =
        convertDataTableRowToMap(goodsReceiptsDataTable, index);
    assertEachRowData(responseGoodsReceipt, goodsReceiptsExpectedData);
  }

  private Map<String, String> convertDataTableRowToMap(DataTable dataTable, int index) {
    List<Map<String, String>> dataTableList = dataTable.asMaps(String.class, String.class);
    return dataTableList.get(index);
  }

  private void assertEachRowData(
      HashMap<String, Object> responseGoodsReceipt, Map<String, String> goodsReceiptsExpectedData) {
    boolean isRecordReturned = false;
    String shownUserCode = convertObjectToString(responseGoodsReceipt.get(USER_CODE));
    if (shownUserCode.equals(goodsReceiptsExpectedData.get(IFeatureFileCommonKeys.CODE))) {
      isRecordReturned = true;
      assertCorrectRowValues(goodsReceiptsExpectedData, responseGoodsReceipt);
    }
    assertTrue(isRecordReturned);
  }

  private void assertCorrectRowValues(
      Map<String, String> goodsReceiptsExpectedData,
      HashMap<String, Object> goodsReceiptActualData) {
    Map<String, Object> responseMap = fillMapWithActualRowValues(goodsReceiptActualData);
    assertEquals(
        goodsReceiptsExpectedData.get(IFeatureFileCommonKeys.TYPE),
        responseMap.get(IFeatureFileCommonKeys.TYPE));
    assertEquals(
        goodsReceiptsExpectedData.get(IGoodsReceiptFeatureFileCommonKeys.BUSINESS_PARTNER),
        responseMap.get(IGoodsReceiptFeatureFileCommonKeys.BUSINESS_PARTNER));
    assertEquals(
        goodsReceiptsExpectedData.get(IFeatureFileCommonKeys.STOREHOUSE),
        responseMap.get(IFeatureFileCommonKeys.STOREHOUSE));
    assertShownActivationDateEquals(
        goodsReceiptsExpectedData.get(IGoodsReceiptFeatureFileCommonKeys.ACTIVATION_DATE),
        responseMap.get(IGoodsReceiptFeatureFileCommonKeys.ACTIVATION_DATE));
    String currentStatesString =
        convertObjectToString(responseMap.get(IFeatureFileCommonKeys.STATE));
    assertTrue(
        currentStatesString.contains(goodsReceiptsExpectedData.get(IFeatureFileCommonKeys.STATE)));
  }

  private Map<String, Object> fillMapWithActualRowValues(
      HashMap<String, Object> goodsReceiptActualData) {
    Map<String, Object> actualData = new HashMap<>();
    actualData.put(
        IFeatureFileCommonKeys.TYPE,
        ((HashMap<String, String>)
                ((HashMap<String, Object>)
                        goodsReceiptActualData.get(IDObGoodsReceiptGeneralModel.TYPE_NAME))
                    .get("values"))
            .get(requestLocale));

    actualData.put(
        IGoodsReceiptFeatureFileCommonKeys.BUSINESS_PARTNER,
        getEnglishBusinessPartnerName(goodsReceiptActualData));
    actualData.put(
        IFeatureFileCommonKeys.STOREHOUSE,
        ((HashMap<String, String>)
                ((HashMap<String, Object>)
                        goodsReceiptActualData.get(IDObGoodsReceiptGeneralModel.STOREHOUSE_NAME))
                    .get("values"))
            .get(requestLocale));
    actualData.put(
        IGoodsReceiptFeatureFileCommonKeys.ACTIVATION_DATE,
        goodsReceiptActualData.get(IDObGoodsReceiptGeneralModel.ACTIVATION_DATE));
    actualData.put(
        IFeatureFileCommonKeys.STATE,
        goodsReceiptActualData.get(IDObGoodsReceiptGeneralModel.STATE));
    actualData.put(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        goodsReceiptActualData.get(IDObGoodsReceiptGeneralModel.PURCHASING_UNIT_NAME_EN));
    return actualData;
  }

  private String getEnglishBusinessPartnerName(HashMap<String, Object> goodsReceiptActualData) {
    String shownEnglishBusinessPartnerName = "";
    shownEnglishBusinessPartnerName =
        ((HashMap<String, String>)
                ((HashMap<String, Object>)
                        goodsReceiptActualData.get(
                            IDObGoodsReceiptGeneralModel.BUSINESS_PARTNER_NAME))
                    .get("values"))
            .get("en");
    return shownEnglishBusinessPartnerName;
  }

  private void assertShownActivationDateEquals(String activationDate, Object shownActivationDate) {
    if (activationDate != null && !activationDate.isEmpty()) {
      DateTimeFormatter dateTimeFormatter =
          DateTimeFormat.forPattern(DateFormats.DATE_TIME_WITH_TIMEZONE);
      DateTime dbParsingDate =
          dateTimeFormatter
              .parseDateTime(convertObjectToString(shownActivationDate))
              .withZone(DateTimeZone.UTC);
      assertDateEquals(activationDate, dbParsingDate);
    } else {
      Assert.assertNull(shownActivationDate);
    }
  }

  public void assertCorrectValueAreReturnedFromDatabase(
      DataTable goodsReceiptsDataTable, Response viewAllResponse) {
    assertResponseSuccessStatus(viewAllResponse);
    List<List<String>> goodsReceipts = goodsReceiptsDataTable.raw();
    assertCorrectNumberOfRows(viewAllResponse, goodsReceipts.size() - 1);
    for (int i = 1; i < goodsReceipts.size(); i++) {
      assertCorrectShownData(viewAllResponse, goodsReceiptsDataTable, i - 1);
    }
  }

  private String convertObjectToString(Object obj) {
    return obj.toString();
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IGoodsReceiptSectionNames.HEADER_SECTION, IGoodsReceiptSectionNames.ITEMS_SECTION));
  }

  public Map<String, String> getSectionsLockUrlsMap() {
    Map<String, String> sectionsLockUrls = new HashMap<>();
    sectionsLockUrls.put(
        IGoodsReceiptSectionNames.ITEMS_SECTION, IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM);
    return sectionsLockUrls;
  }

  public void assertThatGoodsReceiptIsUpdated(
      String goodsReceiptCode, DataTable modificationDataTable) {

    Map<String, String> expectedMap =
        modificationDataTable.asMaps(String.class, String.class).get(0);

    String lastUpdatedDate = expectedMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE);
    String lastUpdatedBy = expectedMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY);

    DObGoodsReceiptGeneralModel dObGoodsReceiptGeneralModel =
        (DObGoodsReceiptGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptModificationDetails", goodsReceiptCode, lastUpdatedBy);

    assertNotNull(dObGoodsReceiptGeneralModel);
    assertDateEquals(lastUpdatedDate, dObGoodsReceiptGeneralModel.getModifiedDate());
  }

  public void insertGoodsReceiptGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> grGeneralDataListMap =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> grGeneralDataMap : grGeneralDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertGoodsReceiptGeneralData", constructGRGeneralDataInsertionParams(grGeneralDataMap));
    }
  }

  public void insertGoodsReceiptCompanyData(DataTable grCompanyData) {
    List<Map<String, String>> grCompanyDataListMap =
        grCompanyData.asMaps(String.class, String.class);
    for (Map<String, String> grCompanyDataMap : grCompanyDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertGoodsReceiptCompanyData", constructGRCompanyDataInsertionParams(grCompanyDataMap));
    }
  }

  public void insertGoodsReceiptPurchaseOrderData(DataTable grPurchaseOrderData) {
    List<Map<String, String>> grPurchaseOrderDataListMap =
        grPurchaseOrderData.asMaps(String.class, String.class);
    for (Map<String, String> grPurchaseOrderDataMap : grPurchaseOrderDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertGoodsReceiptPurchaseOrderData",
          constructGRPurcaseOrderDataInsertionParams(grPurchaseOrderDataMap));
    }
  }

  private Object[] constructGRGeneralDataInsertionParams(Map<String, String> grGeneralDataMap) {
    String state = grGeneralDataMap.get(IFeatureFileCommonKeys.STATE);
    return new Object[] {
      grGeneralDataMap.get(IFeatureFileCommonKeys.CODE),
      grGeneralDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      grGeneralDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      grGeneralDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      grGeneralDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      state,
      grGeneralDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  private Object[] constructGRCompanyDataInsertionParams(Map<String, String> grCompanyDataMap) {
    return new Object[] {
      grCompanyDataMap.get(IFeatureFileCommonKeys.CODE),
      grCompanyDataMap.get(IInventoryFeatureFileCommonKeys.COMPANY),
      grCompanyDataMap.get(IInventoryFeatureFileCommonKeys.PLANT),
      grCompanyDataMap.get(IInventoryFeatureFileCommonKeys.STOREHOUSE),
      grCompanyDataMap.get(IFeatureFileCommonKeys.STOREKEEPER)
    };
  }

  private Object[] constructGRPurcaseOrderDataInsertionParams(
      Map<String, String> grPurchaseOrderDataMap) {
    String vendor = grPurchaseOrderDataMap.get(IFeatureFileCommonKeys.VENDOR);
    String purchaseResponsible =
        grPurchaseOrderDataMap.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE);

    return new Object[] {
      grPurchaseOrderDataMap.get(IFeatureFileCommonKeys.CODE),
      grPurchaseOrderDataMap.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
      vendor,
      vendor,
      grPurchaseOrderDataMap.get(IFeatureFileCommonKeys.DELIVERY_NOTE_PL),
      purchaseResponsible,
      purchaseResponsible
    };
  }

  public void insertGoodsReceiptItems(DataTable grItemsDataTable) {
    List<Map<String, String>> grItemListMap = grItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> grItemDataMap : grItemListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertGoodsReceiptPurchaseOrderReceivedItemsData",
          constructGRItemDataInsertionParams(grItemDataMap));
    }
  }

  public void insertGoodsReceiptItemQuantities(DataTable itemQuantitiesDataTable) {
    List<Map<String, String>> itemQuantitiesListMap =
        itemQuantitiesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> itemQuantitiesMap : itemQuantitiesListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertGoodsReceiptPurchaseOrderItemQuantities",
          constructGRItemQuantitiesInsertionParams(itemQuantitiesMap));
    }
  }

  private Object[] constructGRItemDataInsertionParams(Map<String, String> grItemDataMap) {
    String item = grItemDataMap.get(IFeatureFileCommonKeys.ITEM);

    return new Object[] {
      grItemDataMap.get(IFeatureFileCommonKeys.CODE),
      item,
      grItemDataMap.get(IFeatureFileCommonKeys.DIFFERENCE_REASON)
    };
  }

  private Object[] constructGRItemQuantitiesInsertionParams(Map<String, String> grItemDataMap) {
    String item = grItemDataMap.get(IFeatureFileCommonKeys.ITEM);

    return new Object[] {
      grItemDataMap.get(IFeatureFileCommonKeys.CODE),
      item,
      grItemDataMap.get(IFeatureFileCommonKeys.UOE),
      grItemDataMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
      grItemDataMap.get(IFeatureFileCommonKeys.STOCK_TYPE)
    };
  }
}
