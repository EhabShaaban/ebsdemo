package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.inventory.goodsissue.IIObInventoryCompanyGeneralModel;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.json.JSONException;
import org.junit.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class IObGoodsReceiptCompanyViewTestUtils extends DObGoodsReceiptCommonTestUtils {

  private ObjectMapper objectMapper;

  public IObGoodsReceiptCompanyViewTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getGoodsReceiptCompanyViewDataUrl(String code) {
   return IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT + "/" + code + IDObGoodsReceiptRestUrls.VIEW_COMPANY;
  }

  public void assertThatGoodsReceiptHasCompany(DataTable storeDataTable) {

    Map<String, String> GoodsReceiptStoreData =
        storeDataTable.asMaps(String.class, String.class).get(0);

    Object[] queryParameters = getGoodsReceiptStoreQueryParameters(GoodsReceiptStoreData);
    IObInventoryCompanyGeneralModel GoodsReceiptStoreGeneralModel =
        getIObInventoryCompany(queryParameters);
    Assert.assertNotNull(GoodsReceiptStoreGeneralModel);
  }

  private Object[] getGoodsReceiptStoreQueryParameters(Map<String, String> GoodsReceiptStoreData) {
    String GoodsReceiptCode = GoodsReceiptStoreData.get(IFeatureFileCommonKeys.GR_CODE);
      String companyCodeName = GoodsReceiptStoreData.get(IInventoryFeatureFileCommonKeys.COMPANY);
      String storehouseCodeName =
              GoodsReceiptStoreData.get(IInventoryFeatureFileCommonKeys.STOREHOUSE);
    String businessUnit = GoodsReceiptStoreData.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    return new Object[] {GoodsReceiptCode, companyCodeName, storehouseCodeName, businessUnit};
  }

  private IObInventoryCompanyGeneralModel getIObInventoryCompany(Object[] parameters) {
    return (IObInventoryCompanyGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getInventoryDocumentCompanyData", parameters);
  }

  public void assertThatGoodsReceiptCompanyMatchesResponse(
      DataTable storeDataTable, Response response) throws IOException, JSONException {

    Map<String, String> expectedStoreData =
        storeDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualStoreData = getMapFromResponse(response);

    assertThatGoodsReceiptStoreDataMatches(expectedStoreData, actualStoreData);
  }

  private void assertThatGoodsReceiptStoreDataMatches(
      Map<String, String> expectedStoreData, HashMap<String, Object> actualStoreData)
      throws IOException, JSONException {

    assertEquals(
            expectedStoreData.get(IInventoryFeatureFileCommonKeys.COMPANY),
            getEnglishLocaleFromLocalizedResponse(
                    objectMapper.writeValueAsString(
                            actualStoreData.get(IIObInventoryCompanyGeneralModel.COMPANY_NAME))));

    assertEquals(
        expectedStoreData.get(IInventoryFeatureFileCommonKeys.STOREHOUSE),
        getEnglishLocaleFromLocalizedResponse(
            objectMapper.writeValueAsString(
                actualStoreData.get(IIObInventoryCompanyGeneralModel.STOREHOUSE_NAME))));
    assertEquals(
        expectedStoreData.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        getEnglishLocaleFromLocalizedResponse(
            objectMapper.writeValueAsString(
                actualStoreData.get(IIObInventoryCompanyGeneralModel.PURCHASING_UNIT_NAME))));
  }

}
