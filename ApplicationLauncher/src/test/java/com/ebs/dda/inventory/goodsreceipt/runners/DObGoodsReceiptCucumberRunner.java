package com.ebs.dda.inventory.goodsreceipt.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/inventory/goods-receipt/GR_Create_HP.feature",
      "classpath:features/inventory/goods-receipt/GR_Create_Val.feature",
      "classpath:features/inventory/goods-receipt/GR_Create_Auth.feature",
      "classpath:features/inventory/goods-receipt/GR_Delete.feature",
      "classpath:features/inventory/goods-receipt/GR_AllowedActions_Home.feature",
      "classpath:features/inventory/goods-receipt/GR_AllowedActions_Object.feature",
      //      "classpath:features/inventory/goods-receipt/GR_Activate_HP.feature",
      "classpath:features/inventory/goods-receipt/GR_Activate_Val.feature",
      "classpath:features/inventory/goods-receipt/GR_View_POData.feature",
      "classpath:features/inventory/goods-receipt/GR_View_SalesReturn_Section.feature",
      "classpath:features/inventory/goods-receipt/GR_NextPrev.feature",
      "classpath:features/inventory/goods-receipt/GR_ViewAll.feature",
      "classpath:features/inventory/goods-receipt/GR_View_GeneralData.feature",
      "classpath:features/inventory/goods-receipt/GR_View_Company.feature",
      "classpath:features/inventory/goods-receipt/GR_Types_Dropdown.feature",
      "classpath:features/inventory/goods-receipt/GR_SRO_Dropdown.feature",
      "classpath:features/inventory/goods-receipt/GR_SRO_Activate.feature",
      "classpath:features/inventory/goods-receipt/GR_SRO_Activate_Val.feature",
      "classpath:features/inventory/goods-receipt/GR_Activate_Auth.feature",
      "classpath:features/inventory/goods-receipt/GR_PO_Dropdown.feature",
      "classpath:features/inventory/goods-receipt/GR_Recosting_HP.feature",
      "classpath:features/inventory/goods-receipt/GR_Activate_BasedOnPO_HP.feature",
      "classpath:features/accounting/costing/Costing_View_AccountingDetails.feature",
      "classpath:features/accounting/costing/Costing_View_ActivationDetails.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.inventory.goodsreceipt"},
    strict = true,
    tags = "not @Future")
public class DObGoodsReceiptCucumberRunner {}
