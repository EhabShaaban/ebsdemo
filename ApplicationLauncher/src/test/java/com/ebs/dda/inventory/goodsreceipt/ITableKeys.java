package com.ebs.dda.inventory.goodsreceipt;

public interface ITableKeys {

  String CREATION_DATE = "creationdate";
  String STATE = "currentstates";
  String CODE = "goodsreceiptcode";
  String TYPE = "type";
  String PURCHASEUNIT_CODE = "purchaseunitcode";
  String PURCHASEUNIT_ID = "purchaseunitid";
  String PURCHASEUNIT_NAME = "purchaseunitname";
  String VENDOR_ID = "vendorid";
  String STOREHOUSE_ID = "storehouseid";
  String POSTING_DATE = "postingdate";
  String CREATION_INFO = "creationinfo";
  String USER_CODE = "userCode";
  String VENDOR_NAME = "vendorName";
}
