package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class DObGoodsReceiptDeleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptDeleteTestUtils goodsReceiptDeleteTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodsReceiptDeleteTestUtils = new DObGoodsReceiptDeleteTestUtils(getProperties());
        goodsReceiptDeleteTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

  public DObGoodsReceiptDeleteStep() {

    When(
        "^\"([^\"]*)\" requests to delete GR with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          Response deleteGoodsReceiptResponse =
              goodsReceiptDeleteTestUtils.deleteGoodsReceipt(
                  goodsReceiptCode, userActionsTestUtils.getUserCookie(userName));

          userActionsTestUtils.setUserResponse(userName, deleteGoodsReceiptResponse);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" section of GR with code \"([^\"]*)\" in edit mode$",
        (String userName, String sectionName, String goodsReceiptCode) -> {
            Map<String, String> sectionsLockUrlsMap =
                    goodsReceiptDeleteTestUtils.getSectionsLockUrlsMap();
            String lockSectionUrl = sectionsLockUrlsMap.get(sectionName) + goodsReceiptCode;

            Response lockRepponse =
                    userActionsTestUtils.lockSection(userName, lockSectionUrl, goodsReceiptCode);

            goodsReceiptDeleteTestUtils.assertResponseSuccessStatus(lockRepponse);

            userActionsTestUtils.setUserResponse(userName, lockRepponse);
        });

    Then(
        "^GR with code \"([^\"]*)\" is deleted from the system$",
        (String goodsReceiptCode) -> {
          goodsReceiptDeleteTestUtils.assertNoEntityExistsWithCode(goodsReceiptCode);
        });
  }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete GoodsReceipt")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(
                        goodsReceiptDeleteTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(goodsReceiptDeleteTestUtils.getDbScriptPath());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete GoodsReceipt")) {
            userActionsTestUtils.unlockAllSections(
                    IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
                    goodsReceiptDeleteTestUtils.getSectionsNames());
            databaseConnector.closeConnection();
        }
    }
}
