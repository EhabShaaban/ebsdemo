package com.ebs.dda.inventory.initialstockupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.initialstockupload.utils.DObInitailStockUploadViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObInitialStockUploadViewAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObInitailStockUploadViewAllTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitailStockUploadViewAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObInitialStockUploadViewAllStep() {
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of InitialStockUploads with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IDObInitialStockUploadRestURLS.VIEW_ALL_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable goodsIssueDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertInitailStockUploadDataIsCorrect(
              goodsIssueDataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(
              response, totalRecordsNumber);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all InitialStockUploads -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearISUInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View all InitialStockUploads -")) {
      databaseConnector.closeConnection();
    }
  }
}
