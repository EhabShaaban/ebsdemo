package com.ebs.dda.inventory.storetransaction;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.storetransaction.utils.DObStockAvailabilitiesViewAllTestUtils;
import com.ebs.dda.jpa.inventory.stockavailabilities.ICObStockAvailabilitiyGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObStockAvailabilitiesViewAllStep extends SpringBootRunner implements En {

  private static final boolean hasBeenExecuted = false;

  private DObStockAvailabilitiesViewAllTestUtils utils;

  public DObStockAvailabilitiesViewAllStep() {

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IDObStockavailabilitiesRestUrls.GET_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following StockAvailabilities values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable stockAvailabilitiesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatStockAvailabilitiesIsMatch(response, stockAvailabilitiesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with filter applied on Item which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getContainsFilterField(ICObStockAvailabilitiyGeneralModel.ITEM, filteringValue);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObStockavailabilitiesRestUrls.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with filter applied on UOM which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getContainsFilterField(
                  ICObStockAvailabilitiyGeneralModel.UOM_NAME, filteringValue);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObStockavailabilitiesRestUrls.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with filter applied on TotalQty which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getEqualsFilterField(
                  ICObStockAvailabilitiyGeneralModel.AVAILABLE_QUANTITY, filteringValue);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IDObStockavailabilitiesRestUrls.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with filter applied on StockType which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getContainsFilterField(
                  ICObStockAvailabilitiyGeneralModel.STOCK_TYPE, filteringValue);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObStockavailabilitiesRestUrls.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with filter applied on PurchaseUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getEqualsFilterField(
                  ICObStockAvailabilitiyGeneralModel.PURCHASEUNIT_CODE, filteringValue);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IDObStockavailabilitiesRestUrls.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with filter applied on Storehouse which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getContainsFilterField(
                  ICObStockAvailabilitiyGeneralModel.STOREHOUSE_NAME, filteringValue);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObStockavailabilitiesRestUrls.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockAvailabilities with filter applied on Company which equal \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              utils.getEqualsFilterField(
                  ICObStockAvailabilitiyGeneralModel.COMPANY_CODE, filteringValue);
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IDObStockavailabilitiesRestUrls.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObStockAvailabilitiesViewAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all StockAvailabilities -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearStockAvailabilities());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all StockAvailabilities -")) {
      databaseConnector.closeConnection();
    }
  }
}
