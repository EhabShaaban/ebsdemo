package com.ebs.dda.inventory.stocktransformation;

public interface IStockTransformationFileCommonKeys {

  String NEW_STORE_TRANSACTION = "NewStoreTransaction";
  String RELATED_STOCFK_TRASFORMATION_TYPE = "RelatedStockTransformationType";
  String FROM_STOCK_TYPE = "FromStockType";
  String TO_STOCK_TYPE = "ToStockType";
  String REF_STORE_TRANSACTION = "RefStoreTransaction";
  String NEW_STOCK_TYPE = "NewStockType";
  String STOCK_TRASFORMATION_REASON = "StockTransformationReason";


}
