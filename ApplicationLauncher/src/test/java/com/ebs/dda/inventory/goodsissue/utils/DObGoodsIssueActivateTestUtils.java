package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.apis.IGoodsIssueSectionNames;
import com.ebs.dda.inventory.storetransaction.utils.IStoreTransactionFileCommonKeys;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryActivationDetailsGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionAllDataGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObGoodsIssueActivateTestUtils extends DObGoodsIssueCommonTestUtils {
  private List<TObStoreTransactionAllDataGeneralModel> oldStoreTransactionsDataList;

  public DObGoodsIssueActivateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append(",").append("db-scripts/CObBankSqlScript.sql");
    str.append(",").append("db-scripts/CObGeolocaleSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/LobPortSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesOrderData.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append(",")
        .append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(",")
        .append("db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(",")
        .append("db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append(",").append("db-scripts/inventory/stock-transformation/CObStockTransformation.sql");
    str.append(",")
        .append("db-scripts/inventory/stock-transformation/DObStockTransformationScript.sql");
    str.append(",").append("db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append(",")
        .append(
            "db-scripts/inventory/stock-transformation/IObStockTransformationDetailsScript.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");
    str.append(",")
        .append(
            "db-scripts/inventory/store-transaction/TObStockTransformationStoreTransaction.sql");
    return str.toString();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append(",").append("db-scripts/CObBankSqlScript.sql");
    str.append(",").append("db-scripts/CObGeolocaleSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/LobPortSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",")
        .append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    return str.toString();
  }

  public String getDbScriptsRepeatExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/inventory/stockavailability/stockavailabilityClear.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append(',').append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesOrderData.sql");
    str.append(",").append("db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append(",")
        .append("db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");

    return str.toString();
  }

  public Response activateGoodsIssue(Cookie userCookie, String goodsIssueCode) {
    oldStoreTransactionsDataList = new ArrayList<TObStoreTransactionAllDataGeneralModel>();
    oldStoreTransactionsDataList =
        entityManagerDatabaseConnector.executeNativeNamedQuery("getStoreTransactionAllDataRecords");

    return sendPUTRequest(
        userCookie,
        IDObGoodsIssueRestURLS.GOODS_ISSUE_COMMAND
            + "/"
            + goodsIssueCode
            + IDObGoodsIssueRestURLS.GOODS_ISSUE_ACTIVATE,
        new JsonObject());
  }

  public void assertThatGoodsIssueActivationDetailAreUpdated(
      String goodsIssueCode, DataTable expectedActivationDetailsDataTable) {
    Map<String, String> expectedActivationDetails =
        expectedActivationDetailsDataTable.asMaps(String.class, String.class).get(0);
    IObInventoryActivationDetailsGeneralModel activationDetails =
        (IObInventoryActivationDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsIssueActivationDetails",
                goodsIssueCode,
                expectedActivationDetails.get(IGoodsIssueFeatureFileCommonKeys.ACTIVATED_BY));
    assertNotNull(activationDetails);
    assertDateEquals(
        expectedActivationDetails.get(IGoodsIssueFeatureFileCommonKeys.ACTIVATION_DATE),
        activationDetails.getActivationDate());
  }

  public void assertRemainingAmountInStoreForItemIs(
      String storeHouse, String plant, String company, DataTable itemsDataTable) {
    List<Map<String, String>> goodsIssueItems = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueItem : goodsIssueItems) {
      assertGoodsIssueItemRemainingIs(storeHouse, plant, company, goodsIssueItem);
    }
  }

  private void assertGoodsIssueItemRemainingIs(
      String storeHouse, String plant, String company, Map<String, String> goodsIssueItem) {
    String itemCode = goodsIssueItem.get(IFeatureFileCommonKeys.ITEM_CODE);
    String unitOfMeasure = goodsIssueItem.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT);
    String purchaseUnit = goodsIssueItem.get(IFeatureFileCommonKeys.PURCHASE_UNIT);
    BigDecimal expectedRemainingQuantity =
        new BigDecimal(goodsIssueItem.get(IGoodsIssueFeatureFileCommonKeys.REMAINING_IN_STORE));
    Object[] queryParameters =
        getGoodsIssueItemRemainingQueryParameters(
            storeHouse, plant, company, itemCode, unitOfMeasure, purchaseUnit);
    BigDecimal actualRemainingQuantity = getDObGoodsIssueItemActualRemaining(queryParameters);
    Assert.assertEquals(expectedRemainingQuantity.compareTo(actualRemainingQuantity), 0);
  }

  private Object[] getGoodsIssueItemRemainingQueryParameters(
      String storeHouse,
      String plant,
      String company,
      String itemCode,
      String unitOfMeasure,
      String purchaseUnit) {
    return new Object[] {itemCode, storeHouse, plant, company, unitOfMeasure, purchaseUnit};
  }

  private BigDecimal getDObGoodsIssueItemActualRemaining(Object[] parameters) {
    return (BigDecimal)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getGoodsIssueItemRemainingInStore", parameters);
  }

  public void assertThatItemHasNotEnoughRemainingQuantityExistInResponse(
      Response userResponse, DataTable itemsDataTable) {
    List<Map<String, String>> items = itemsDataTable.asMaps(String.class, String.class);
    String expectedItemEn = items.get(0).get(IGoodsIssueFeatureFileCommonKeys.ITEM_EN);
    String expectedItemAr = items.get(0).get(IGoodsIssueFeatureFileCommonKeys.ITEM_AR);
    Map<String, Object> responseItemsListMap = getResponseItemsList(userResponse);
    List<Map<String, Map<String, String>>> itemsSection =
        (List) responseItemsListMap.get(IGoodsIssueSectionNames.ITEMS_SECTION);
    List<String> actualItemsListEn =
        itemsSection.stream()
            .map(item -> item.get("values").get("en"))
            .collect(Collectors.toList());
    List<String> actualItemsListAr =
        itemsSection.stream()
            .map(item -> item.get("values").get("ar"))
            .collect(Collectors.toList());
    Assert.assertTrue(actualItemsListEn.containsAll(Arrays.asList(expectedItemEn)));
    Assert.assertTrue(actualItemsListAr.containsAll(Arrays.asList(expectedItemAr)));
  }

  private Map<String, Object> getResponseItemsList(Response response) {
    return response.body().jsonPath().getMap("errorValues");
  }

  public void assertThatStoreTransactionHasOnlyTheFollowingRecords(
      DataTable storeTransactionDataTable) {

    List<Map<String, String>> expectedStoreTransactionRecords =
        storeTransactionDataTable.asMaps(String.class, String.class);
    oldStoreTransactionsDataList = new ArrayList<TObStoreTransactionAllDataGeneralModel>();
    for (Map<String, String> storeTransactionRecord : expectedStoreTransactionRecords) {
      TObStoreTransactionAllDataGeneralModel storeTransactionAllDataGeneralModelRecord =
          (TObStoreTransactionAllDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getStoreTransactionAllDataRecordsByParameters",
                  constructStoreTransactionRecordsQueryParams(storeTransactionRecord));
      oldStoreTransactionsDataList.add(storeTransactionAllDataGeneralModelRecord);
      assertNotNull(
          storeTransactionAllDataGeneralModelRecord,
          storeTransactionRecord.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE)
              + " Doesn't exist!");
      assertDateEquals(
          convertEmptyStringToNull(
              storeTransactionRecord.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE)),
          storeTransactionAllDataGeneralModelRecord.getOriginalAddingDate());
      assertDateEquals(
          storeTransactionRecord.get(IFeatureFileCommonKeys.CREATION_DATE),
          storeTransactionAllDataGeneralModelRecord.getCreationDate());
    }
    List<TObStoreTransactionAllDataGeneralModel> storeTransactionAllDataGeneralModelRecord =
        entityManagerDatabaseConnector.executeNativeNamedQuery("getStoreTransactionAllDataRecords");
    assertEquals(
        expectedStoreTransactionRecords.size(), storeTransactionAllDataGeneralModelRecord.size());
  }

  private Object[] constructStoreTransactionRecordsQueryParams(
      Map<String, String> storeTransactionRecord) {
    Object[] storeTransactionRecordObject =
        new Object[] {
          storeTransactionRecord.get(IFeatureFileCommonKeys.BATCH_NUMBER),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REF_TRANSACTION),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ESTIMATE_COST),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ACTUAL_COST),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REMAINING_QTY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STOCK_TYPE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ITEM),
          storeTransactionRecord.get(IFeatureFileCommonKeys.UOM),
          storeTransactionRecord.get(IFeatureFileCommonKeys.QTY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.TRANSACTION_TYPE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.COMPANY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.PLANT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STOREHOUSE)
        };
    return storeTransactionRecordObject;
  }

  public void assertThatNewStoreTransactionsAreAdded(DataTable storeTransactionDataTable) {
    List<Map<String, String>> expectedAddedStoreTransactionRecords =
        storeTransactionDataTable.asMaps(String.class, String.class);

    for (Map<String, String> storeTransactionRecord : expectedAddedStoreTransactionRecords) {
      TObStoreTransactionAllDataGeneralModel storeTransactionAllDataGeneralModelRecord =
          (TObStoreTransactionAllDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getAddedStoreTransactionAllDataRecordsByParameters",
                  constructAddedStoreTransactionRecordsQueryParams(storeTransactionRecord));
      assertNotNull(storeTransactionAllDataGeneralModelRecord);
      assertDateEquals(
          convertEmptyStringToNull(
              storeTransactionRecord.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE)),
          storeTransactionAllDataGeneralModelRecord.getOriginalAddingDate());
      assertDateEquals(
          storeTransactionRecord.get(IFeatureFileCommonKeys.CREATION_DATE),
          storeTransactionAllDataGeneralModelRecord.getCreationDate());
    }
    List<TObStoreTransactionAllDataGeneralModel> storeTransactionAllDataGeneralModelRecord =
        entityManagerDatabaseConnector.executeNativeNamedQuery("getStoreTransactionAllDataRecords");
    Assert.assertEquals(
        expectedAddedStoreTransactionRecords.size(),
        storeTransactionAllDataGeneralModelRecord.size() - oldStoreTransactionsDataList.size());
  }

  private Object[] constructAddedStoreTransactionRecordsQueryParams(
      Map<String, String> storeTransactionRecord) {
    Object[] storeTransactionRecordObject =
        new Object[] {
          storeTransactionRecord.get(IFeatureFileCommonKeys.BATCH_NUMBER),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REF_TRANSACTION),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ESTIMATE_COST),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ACTUAL_COST),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STOCK_TYPE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ITEM),
          storeTransactionRecord.get(IFeatureFileCommonKeys.UOM),
          storeTransactionRecord.get(IFeatureFileCommonKeys.QTY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.TRANSACTION_TYPE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REMAINING_QTY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.COMPANY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.PLANT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STOREHOUSE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.CREATED_BY)
        };
    return storeTransactionRecordObject;
  }

  public void assertThatStoreTransactionsRemainsAsFollows(DataTable storeTransactionDataTable) {
    List<Map<String, String>> expectedRemainsStoreTransactionRecords =
        storeTransactionDataTable.asMaps(String.class, String.class);

    for (Map<String, String> storeTransactionRecord : expectedRemainsStoreTransactionRecords) {
      TObStoreTransactionAllDataGeneralModel storeTransactionAllDataGeneralModelRecord =
          (TObStoreTransactionAllDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getStoreTransactionAllDataRecordsByParameters",
                  constructStoreTransactionRecordsQueryParams(storeTransactionRecord));

      assertDateEquals(
          convertEmptyStringToNull(
              storeTransactionRecord.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE)),
          storeTransactionAllDataGeneralModelRecord.getOriginalAddingDate());
      assertDateEquals(
          storeTransactionRecord.get(IFeatureFileCommonKeys.CREATION_DATE),
          storeTransactionAllDataGeneralModelRecord.getCreationDate());

      for (TObStoreTransactionAllDataGeneralModel oldStoreTransactionRecord :
          oldStoreTransactionsDataList) {
        if (oldStoreTransactionRecord
            .getUserCode()
            .equals(storeTransactionAllDataGeneralModelRecord.getUserCode())) {
          assertTransactionEquality(
              storeTransactionAllDataGeneralModelRecord, oldStoreTransactionRecord);
          break;
        }
      }
    }
  }

  public void assertThatStoreTransactionsUpdatedAsFollows(DataTable storeTransactionDataTable) {
    List<Map<String, String>> expectedRemainsStoreTransactionRecords =
        storeTransactionDataTable.asMaps(String.class, String.class);

    for (Map<String, String> storeTransactionRecord : expectedRemainsStoreTransactionRecords) {
      TObStoreTransactionAllDataGeneralModel storeTransactionAllDataGeneralModelRecord =
          (TObStoreTransactionAllDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getStoreTransactionAllDataRecordsByParameters",
                  constructStoreTransactionRecordsQueryParams(storeTransactionRecord));
      assertNotNull(storeTransactionAllDataGeneralModelRecord);
      assertDateEquals(
          storeTransactionRecord.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE),
          storeTransactionAllDataGeneralModelRecord.getOriginalAddingDate());
      assertDateEquals(
          storeTransactionRecord.get(IFeatureFileCommonKeys.CREATION_DATE),
          storeTransactionAllDataGeneralModelRecord.getCreationDate());
    }
  }

  public void assertTransactionEquality(
      TObStoreTransactionAllDataGeneralModel storeTransactionAllDataGeneralModelRecord,
      TObStoreTransactionAllDataGeneralModel oldStoreTransactionRecord) {
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getCreationDate(),
        oldStoreTransactionRecord.getCreationDate());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getStockType(),
        oldStoreTransactionRecord.getStockType());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getPurchaseUnitCode(),
        oldStoreTransactionRecord.getPurchaseUnitCode());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getItemCode(),
        oldStoreTransactionRecord.getItemCode());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getUnitOfMeasureCode(),
        oldStoreTransactionRecord.getUnitOfMeasureCode());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getBatchNo(),
        oldStoreTransactionRecord.getBatchNo());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getQuantity(),
        oldStoreTransactionRecord.getQuantity());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getEstimateCost(),
        oldStoreTransactionRecord.getEstimateCost());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getActualCost(),
        oldStoreTransactionRecord.getActualCost());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getDocumentCode(),
        oldStoreTransactionRecord.getDocumentCode());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getRemainingQuantity(),
        oldStoreTransactionRecord.getRemainingQuantity());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getRefTransactionCode(),
        oldStoreTransactionRecord.getRefTransactionCode());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getCompanyCode(),
        oldStoreTransactionRecord.getCompanyCode());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getPlantCode(),
        oldStoreTransactionRecord.getPlantCode());
    Assert.assertEquals(
        storeTransactionAllDataGeneralModelRecord.getStorehouseCode(),
        oldStoreTransactionRecord.getStorehouseCode());
  }

  public void assertThatStoreTransactionsExistWithData(
      String itemCode,
      String uomCode,
      String companyCode,
      String storehouseCode,
      String businessUnitCode,
      DataTable storeTransactionsDataTable) {
    List<Map<String, String>> expectedStoreTransactionRecords =
        storeTransactionsDataTable.asMaps(String.class, String.class);
    List<TObStoreTransactionGeneralModel> storeTransactionsList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getStoreTransactionRecordsWithData",
            constructStoreTransactionRecordsWithDataQueryParams(
                itemCode, uomCode, companyCode, storehouseCode, businessUnitCode));
    for (int i = 0; i < storeTransactionsList.size(); i++) {
      assertEquals(
          expectedStoreTransactionRecords.get(i).get(IStoreTransactionFileCommonKeys.ST_CODE),
          storeTransactionsList.get(i).getUserCode(),
          "StoreTransaction record with code: "
              + expectedStoreTransactionRecords.get(i).get(IStoreTransactionFileCommonKeys.ST_CODE)
              + " for Item "
              + itemCode
              + " with UOM "
              + uomCode
              + " in company "
              + companyCode
              + " in storehouse "
              + storehouseCode
              + " for BusinessUnit "
              + businessUnitCode
              + " doesn't exist!");
    }
  }

  private Object[] constructStoreTransactionRecordsWithDataQueryParams(
      String itemCode,
      String uomCode,
      String companyCode,
      String storehouseCode,
      String businessUnitCode) {
    return new Object[] {itemCode, uomCode, companyCode, storehouseCode, businessUnitCode};
  }

  public void assertThatStoreTransactionsCountMatchesExpected(
      String itemCode,
      String uomCode,
      String companyCode,
      String storehouseCode,
      String businessUnitCode,
      Integer recordsCount) {
    List<TObStoreTransactionGeneralModel> storeTransactionsList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getStoreTransactionRecordsWithData",
            constructStoreTransactionRecordsWithDataQueryParams(
                itemCode, uomCode, companyCode, storehouseCode, businessUnitCode));
    assertEquals(
        recordsCount,
        Integer.valueOf(storeTransactionsList.size()),
        "Total number of records returned for Item "
            + itemCode
            + " with UOM "
            + uomCode
            + " in company "
            + companyCode
            + " in storehouse "
            + storehouseCode
            + " for BusinessUnit "
            + businessUnitCode
            + " is "
            + storeTransactionsList.size());
  }

  public void assertRemainingAmountInStoreForItemWithDataIs(
      String stockType,
      String itemCode,
      String uomCode,
      String companyCode,
      String storehouseCode,
      String businessUnitCode,
      DataTable quantityDataTable) {
    Map<String, String> expectedItemQuantity =
        quantityDataTable.asMaps(String.class, String.class).get(0);

    BigDecimal remaining =
        (BigDecimal)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsIssueItemRemainingInStoreFromStockAvailability",
                constructItemRemainingFromStockAvailabilities(
                    stockType, itemCode, uomCode, companyCode, storehouseCode, businessUnitCode));
    BigDecimal expected =
        new BigDecimal(
            expectedItemQuantity.get(IGoodsIssueFeatureFileCommonKeys.REMAINING_IN_STORE));
    assertEquals(
        0,
        expected.compareTo(remaining),
        "Remaining Qty for Item "
            + itemCode
            + " with UOM "
            + uomCode
            + " in company "
            + companyCode
            + " in storehouse "
            + storehouseCode
            + " for BusinessUnit "
            + businessUnitCode
            + " is "
            + remaining);
  }

  private Object[] constructItemRemainingFromStockAvailabilities(
      String stockType,
      String itemCode,
      String uomCode,
      String companyCode,
      String storehouseCode,
      String businessUnitCode) {

    return new Object[] {
      stockType, itemCode, uomCode, companyCode, storehouseCode, businessUnitCode
    };
  }
}
