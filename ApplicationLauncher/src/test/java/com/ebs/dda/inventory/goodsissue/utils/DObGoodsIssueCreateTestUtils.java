package com.ebs.dda.inventory.goodsissue.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.CObGoodsIssue;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IDObGoodsIssueCreateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import com.ebs.dda.order.salesorder.ISalesOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderItemsCommonTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

public class DObGoodsIssueCreateTestUtils extends DObGoodsIssueCommonTestUtils {
  IObSalesOrderItemsCommonTestUtils iObSalesOrderItemsCommonTestUtils;

  public DObGoodsIssueCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    iObSalesOrderItemsCommonTestUtils = new IObSalesOrderItemsCommonTestUtils(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    return str.toString();
  }

  public String getDBScript() {
    StringBuilder str = new StringBuilder();

    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
      str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
      str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesOrderData.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    return str.toString();
  }

  private JsonObject prepareGoodsIssueJsonObject(DataTable goodsIssueDataTable) {
    Map<String, String> goodsIssue = goodsIssueDataTable.asMaps(String.class, String.class).get(0);
    goodsIssue = convertDoubleQuotesToEmptyStrings(goodsIssue);
    JsonObject goodsIssueJsonObject = new JsonObject();
    addValueToJsonObject(
        goodsIssueJsonObject,
        IDObGoodsIssueCreateValueObject.TYPE,
        goodsIssue.get(IFeatureFileCommonKeys.TYPE));
    addValueToJsonObject(
        goodsIssueJsonObject,
        IDObGoodsIssueCreateValueObject.PURCHASE_UNIT,
        checkIfNull(goodsIssue.get(IAccountingFeatureFileCommonKeys.PURCHASE_UNIT))
            ? null
            : goodsIssue.get(IAccountingFeatureFileCommonKeys.PURCHASE_UNIT).split(" - ")[0]);
    addValueToJsonObject(
        goodsIssueJsonObject,
        IDObGoodsIssueCreateValueObject.REF_DOCUMENT_CODE,
        goodsIssue.get(IGoodsIssueFeatureFileCommonKeys.REF_DOCUMENT));
    addValueToJsonObject(
        goodsIssueJsonObject,
        IDObGoodsIssueCreateValueObject.COMPANY,
        checkIfNull(goodsIssue.get(IFeatureFileCommonKeys.COMPANY))
            ? null
            : goodsIssue.get(IFeatureFileCommonKeys.COMPANY).split(" - ")[0]);
    addValueToJsonObject(
        goodsIssueJsonObject,
        IDObGoodsIssueCreateValueObject.STORE_HOUSE,
        checkIfNull(goodsIssue.get(IFeatureFileCommonKeys.STOREHOUSE))
            ? null
            : goodsIssue.get(IFeatureFileCommonKeys.STOREHOUSE).split(" - ")[0]);
    addValueToJsonObject(
        goodsIssueJsonObject,
        IDObGoodsIssueCreateValueObject.STORE_KEEPER,
        checkIfNull(goodsIssue.get(IFeatureFileCommonKeys.STOREKEEPER))
            ? null
            : goodsIssue.get(IFeatureFileCommonKeys.STOREKEEPER).split(" - ")[0]);

    return goodsIssueJsonObject;
  }

  public boolean checkIfNull(String str) {
    return (str == null);
  }

  public void assertThatGoodsIssueTypesExist(DataTable goodsIssueTypes) {
    List<Map<String, String>> goodsIssueTypesList =
        goodsIssueTypes.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueType : goodsIssueTypesList) {

      String goodsIssueTypeCode = goodsIssueType.get(IFeatureFileCommonKeys.CODE);
      String goodsIssueTypeName = goodsIssueType.get(IFeatureFileCommonKeys.NAME);

      CObGoodsIssue cObGoodsIssue =
          (CObGoodsIssue)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "goodsIssueTypeByCodeAndName", goodsIssueTypeCode, goodsIssueTypeName);

      assertNotNull(cObGoodsIssue);
    }
  }

  public void assertGoodsIssueIsCreatedSuccessfully(DataTable goodsIssueDataTable) {

    List<Map<String, String>> goodsIssueList =
        goodsIssueDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueRow : goodsIssueList) {
      DObGoodsIssueGeneralModel goodsIssueGeneralModel =
          (DObGoodsIssueGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedGoodsIssue", constructGoodsIssueQueryParams(goodsIssueRow));
      assertNotNull(goodsIssueGeneralModel);

      assertDateEquals(
          goodsIssueRow.get(IFeatureFileCommonKeys.CREATION_DATE),
          goodsIssueGeneralModel.getCreationDate());
      assertDateEquals(
              goodsIssueRow.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
              goodsIssueGeneralModel.getModifiedDate());
    }
  }

  private Object[] constructGoodsIssueQueryParams(Map<String, String> goodsIssueRow) {
    return new Object[] {
      goodsIssueRow.get(IFeatureFileCommonKeys.CODE),
      '%' + goodsIssueRow.get(IFeatureFileCommonKeys.STATE) + '%',
      goodsIssueRow.get(IFeatureFileCommonKeys.CREATED_BY),
      goodsIssueRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      goodsIssueRow.get(IFeatureFileCommonKeys.TYPE),
      goodsIssueRow.get(IInventoryFeatureFileCommonKeys.STOREKEEPER).split(" - ")[0]
    };
  }

  public void assertGoodsIssueSalesInvoiceSectionIsCreatedSuccessfully(
      DataTable goodsIssueDataTable) {

    List<Map<String, String>> goodsIssueList =
        goodsIssueDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueRow : goodsIssueList) {
      IObGoodsIssueRefDocumentDataGeneralModel goodsIssueSalesInvoiceDataGeneralModel =
          (IObGoodsIssueRefDocumentDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedGoodsIssueSalesInvoiceSection",
                  constructGoodsIssueSalesInvoiceQueryParams(goodsIssueRow));
      assertNotNull(goodsIssueSalesInvoiceDataGeneralModel);
    }
  }

  private Object[] constructGoodsIssueSalesInvoiceQueryParams(Map<String, String> goodsIssueRow) {
    return new Object[] {
      goodsIssueRow.get(IGoodsIssueFeatureFileCommonKeys.CUSTOMER),
      goodsIssueRow.get(IGoodsIssueFeatureFileCommonKeys.ADDRESS),
      goodsIssueRow.get(IGoodsIssueFeatureFileCommonKeys.CONTACT_PERSON),
      goodsIssueRow.get(IGoodsIssueFeatureFileCommonKeys.REF_DOCUMENT),
      goodsIssueRow.get(IGoodsIssueFeatureFileCommonKeys.KAP)
    };
  }

  public void assertGoodsIssueStoreIsCreatedSuccessfully(
      String goodsIssueCode, DataTable goodsIssueDataTable) {

    List<Map<String, String>> goodsIssueList =
        goodsIssueDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueRow : goodsIssueList) {

      IObInventoryCompanyGeneralModel iObInventoryCompanyGeneralModel =
          (IObInventoryCompanyGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedInventoryCompany",
                  constructGoodsIssueStoreQueryParams(goodsIssueCode, goodsIssueRow));
      assertNotNull(iObInventoryCompanyGeneralModel);
    }
  }

  private Object[] constructGoodsIssueStoreQueryParams(
      String goodsIssueCode, Map<String, String> goodsIssueRow) {
    return new Object[] {
      goodsIssueCode,
      goodsIssueRow.get(IFeatureFileCommonKeys.COMPANY).split(" - ")[0],
      goodsIssueRow.get(IFeatureFileCommonKeys.STOREHOUSE).split(" - ")[0],
      goodsIssueRow.get(IFeatureFileCommonKeys.PURCHASE_UNIT).split(" - ")[0]
    };
  }

  public Response createGoodsIssue(DataTable goodsIssueTable, Cookie cookie) {
    JsonObject goodsIssueRequest = prepareGoodsIssueJsonObject(goodsIssueTable);
    String restURL = IDObGoodsIssueRestURLS.CREATE_GOODS_ISSUE;
    return sendPostRequest(cookie, restURL, goodsIssueRequest);
  }

  public void assertThatCustomerExistForSalesInvoice(
      String salesInvoiceCode, DataTable customersDataTable) {
    List<Map<String, String>> customersList = customersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> customer : customersList) {

      IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartner =
          (IObSalesInvoiceBusinessPartnerGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getBySalesInvoiceCodeAndCustomerCode",
                  salesInvoiceCode,
                  customer.get(IFeatureFileCommonKeys.CUSTOMER));
      assertNotNull(salesInvoiceBusinessPartner);
    }
  }

  public void assertThatCompanyExistForSalesInvoice(
      String salesInvoiceCode, DataTable salesInvoiceCompanyDataTable) {
    List<Map<String, String>> companiesList =
        salesInvoiceCompanyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> customer : companiesList) {

      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getBySalesInvoiceCodeAndCompanyCode",
                  salesInvoiceCode,
                  customer.get(IFeatureFileCommonKeys.COMPANY));
      assertNotNull(salesInvoiceGeneralModel);
    }
  }

  public void assertThatGoodsIssueHasItems(String goodsIssueCode, DataTable itemsDataTable) {
    List<Map<String, String>> itemsList = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsList) {
      convertEmptyStringsToNulls(item);

      IObGoodsIssueItemGeneralModel goodsIssueItemGeneralModel =
          (IObGoodsIssueItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsIssueCreatedItems", constructItemsQueryParams(goodsIssueCode, item));
      assertNotNull(goodsIssueItemGeneralModel);
    }
  }

  private Object[] constructItemsQueryParams(String goodsIssueCode, Map<String, String> item) {
    return new Object[] {
      goodsIssueCode,
      item.get(IGoodsIssueFeatureFileCommonKeys.ITEM),
      item.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT).split(" - ")[1],
      item.get(IFeatureFileCommonKeys.QUANTITY),
      item.get(IFeatureFileCommonKeys.BATCH_NUMBER)
    };
  }

  public void assertThatSalesOrderSectionExist(
      String goodsIssueCode, DataTable goodsIssueSalesOrderDataTable) {
    List<Map<String, String>> goodsIssueSalesOrderList =
        goodsIssueSalesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueSalesOrder : goodsIssueSalesOrderList) {
      IObGoodsIssueRefDocumentDataGeneralModel iObGoodsIssueRefDocumentDataGeneralModel =
          (IObGoodsIssueRefDocumentDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedGoodsIssueSalesOrderSection",
                  constructGoodsIssueSalesOrderSectionQueryParams(
                      goodsIssueCode, goodsIssueSalesOrder));
      assertNotNull(
          iObGoodsIssueRefDocumentDataGeneralModel,
          "SalesOrder section for GI with code " + goodsIssueCode + " doesn't exist!");
    }
  }

  private Object[] constructGoodsIssueSalesOrderSectionQueryParams(
      String goodsIssueCode, Map<String, String> goodsIssueSalesOrder) {
    return new Object[] {
      goodsIssueCode,
      goodsIssueSalesOrder.get(IFeatureFileCommonKeys.CUSTOMER),
      goodsIssueSalesOrder.get(IGoodsIssueFeatureFileCommonKeys.ADDRESS),
      goodsIssueSalesOrder.get(IGoodsIssueFeatureFileCommonKeys.CONTACT_PERSON),
      goodsIssueSalesOrder.get(IGoodsIssueFeatureFileCommonKeys.REF_DOCUMENT),
      goodsIssueSalesOrder.get(IGoodsIssueFeatureFileCommonKeys.SALES_REPRESENTATIVE),
    };
  }

  public void assertThatTheFollowingSalesOrderItemsExist(DataTable salesOrderItemsDataTable) {
    List<Map<String, String>> goodsIssueItemsList =
        salesOrderItemsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsIssueItem : goodsIssueItemsList) {
      Object salesOrderCode =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObSalesOrderItem",
              iObSalesOrderItemsCommonTestUtils.constructSalesOrderItemQueryParams(goodsIssueItem));
      assertNotNull(
          salesOrderCode,
          "Item "
              + goodsIssueItem.get(IFeatureFileCommonKeys.ITEM)
              + " doesn't exist in SO with code "
              + goodsIssueItem.get(ISalesOrderFeatureFileCommonKeys.SO_CODE));
    }
  }

    public Response requestCreateGoodsIssue(Cookie userCookie) {
      String restURL = IDObGoodsIssueRestURLS.CREATE_GOODS_ISSUE;
      return sendGETRequest(userCookie, restURL);
    }
}
