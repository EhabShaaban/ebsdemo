package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptItemTestUtils;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptItemDetailValueObject;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DObGoodsReceiptAddItemDetailValidationStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObGoodsReceiptItemTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new IObGoodsReceiptItemTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }


    public DObGoodsReceiptAddItemDetailValidationStep() {

    Given(
        "^\"([^\"]*)\" opened Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" in edit mode$",
        (String userName, String itemCode, String goodsReceiptCode) -> {
          String lockURL =
              IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" opened Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode, String datetime) -> {
          utils.freeze(datetime);
          String lockURL =
              IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following error message is attached to unitOfEntryCode field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IIObGoodsReceiptItemDetailValueObject.UOE_CODE);
        });

    Then(
        "^the following error message \"([^\"]*)\" is returned and sent to \"([^\"]*)\"$",
        (String messageCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, messageCode, IIObGoodsReceiptItemDetailValueObject.ITEM_DETAIL);
        });
  }

    @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Add GR Item Detail Val -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Add GR Item Detail Val -")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
          new ArrayList<>(Collections.singletonList(IGoodsReceiptSectionNames.ITEMS_SECTION)));

      databaseConnector.closeConnection();
    }
  }
}
