package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.storetransaction.utils.IStoreTransactionFileCommonKeys;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionAllDataGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DObGoodsReceiptActivateTestUtils extends DObGoodsReceiptCommonTestUtils {

  public DObGoodsReceiptActivateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public static String getActivateUrl(String goodsReceiptCode) {
    return IDObGoodsReceiptRestUrls.COMMAND_GOODS_RECEIPT
        + goodsReceiptCode
        + IDObGoodsReceiptRestUrls.ACTIVATE;
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/LookupSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/master-data/item/MasterData.sql");
    str.append("," + "db-scripts/master-data/vendor/MasterData.sql");
    str.append("," + "db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append("," + "db-scripts/master-data/vendor/MObVendor.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append("," + "db-scripts/master-data/customer/MObCustomer.sql");
    str.append("," + "db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append("," + "db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append("," + "db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts_ViewAll.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append("," + "db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append("," + "db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append("," + "db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append("," + "db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append("," + "db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append("," + "db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append("," + "db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append("," + "db-scripts/sales/IObSISOBusinessPartner.sql");
    str.append("," + "db-scripts/sales/IObSISOItems.sql");
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesOrderData.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append("," + "db-scripts/order/salesreturnorder/CObSalesReturnOrderType.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxes.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxesViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyData.sql");
    str.append(",")
        .append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyDataViewAll.sql");
    str.append(",").append("db-scripts/accounting/costing-payment-request.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoicePostingDetails.sql");
    str.append("," + "db-scripts/accounting/landed-cost/CObLandedCostType.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-viewall.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items-viewall.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");

    return str.toString();
  }

  public static String clearStoreTransaction() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/store-transaction/ClearTObStoreTransaction.sql");
    return str.toString();
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/goods-receipt/goodsreceipt_clear.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost.sql");
    str.append(",").append("db-scripts/accounting/costing/costing_clear.sql");

    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder.sql");
    str.append(
        "," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append("," + "db-scripts/inventory/stockavailability/stockavailabilityClear.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptSalesReturnDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptCompany.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptActivationDetailsScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObStoreTransactionScript.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObGoodsReceiptStoreTransaction.sql");
    str.append("," + "db-scripts/inventory/store-transaction/TObGoodsIssueStoreTransaction.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append("," + "db-scripts/accounting/landed-cost/DObLandedCost_ViewAll.sql");
    str.append("," + "db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails.sql");
    str.append(
        "," + "db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails_ViewAll.sql");

    str.append("," + "db-scripts/accounting/landed-cost/IObLandedCostCompanyData.sql");
    str.append("," + "db-scripts/accounting/landed-cost/IObLandedCostDetails.sql");
    str.append("," + "db-scripts/accounting/landed-cost/IObLandedCostFactorItem.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/DObLandedCost_Costing_ConvertToActual.sql");

    return str.toString();
  }

  public Response sendActivateGoodsReceiptRequest(String url, Cookie userCookie) {
    return sendPUTRequest(userCookie, url, new JsonObject());
  }

  public void assertThatGoodsReceiptIsUpdatedAfterActivate(
      String goodsReceiptCode, DataTable expectedGoodsReceipt) {
    Map<String, String> expectedGoodsReceiptMap =
        expectedGoodsReceipt.asMaps(String.class, String.class).get(0);
    Object[] values =
        (Object[])
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptByCodeAndStateAndModifiedByInfo",
                goodsReceiptCode,
                "%" + expectedGoodsReceiptMap.get(IFeatureFileCommonKeys.STATE) + "%",
                expectedGoodsReceiptMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY));

    assertDateEquals(
        expectedGoodsReceiptMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(values[0]).withZone(DateTimeZone.UTC));
    assertDateEquals(
        expectedGoodsReceiptMap.get(IGoodsReceiptFeatureFileCommonKeys.ACTIVATION_DATE),
        new DateTime(values[1]).withZone(DateTimeZone.UTC));
  }

  public void assertThatpurchaseorderIsUpdatedAfterGoodsReceiptChangedToActive(
      String purchaseOrderCode) {
    DObPurchaseOrder dObPurchaseOrder =
        (DObPurchaseOrder)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "checkPurchaseOrderIsReceived", purchaseOrderCode);
    assertNotNull(dObPurchaseOrder);
  }

  public void assertThatSectionMissingFieldsExistInResponse(
      Response response, String sectionName, DataTable missingFields) {

    Map<String, Object> missingFieldsList = getResponseMissingFields(response);
    List<String> actualSectionMissingFields = (List<String>) missingFieldsList.get(sectionName);
    String[] expectedMissingFields =
        getStringAsArray(
            missingFields
                .asMaps(String.class, String.class)
                .get(0)
                .get(IFeatureFileCommonKeys.MISSING_FIELDS));
    Assert.assertTrue(actualSectionMissingFields.containsAll(Arrays.asList(expectedMissingFields)));
  }

  private Map<String, Object> getResponseMissingFields(Response response) {
    return response.body().jsonPath().getMap("missingFields");
  }

  private String[] getStringAsArray(String sectionMissingData) {
    return sectionMissingData.replaceAll("\\s", "").split(",");
  }

  public void assertThatItemsSectionMissingFieldsExistInResponse(
      Response response, String itemCode, DataTable missingFields) {
    Map<String, Object> missingFieldsList = getResponseMissingFields(response);
    Map<String, List<String>> allItemsMissingFields =
        (Map<String, List<String>>) missingFieldsList.get(IGoodsReceiptSectionNames.ITEMS_SECTION);
    String[] expectedMissingFields =
        getStringAsArray(
            missingFields
                .asMaps(String.class, String.class)
                .get(0)
                .get(IFeatureFileCommonKeys.MISSING_FIELDS));
    List<String> actualItemMissingFields = allItemsMissingFields.get(itemCode);
    Assert.assertTrue(actualItemMissingFields.containsAll(Arrays.asList(expectedMissingFields)));
  }

  public void assertThatItemsSectionMissingFieldsHasEmptyItemsList(
      Response response, DataTable missingFields) {
    Map<String, String> missingFieldsMap = missingFields.asMaps(String.class, String.class).get(0);

    Map<String, Object> missingFieldsList = getResponseMissingFields(response);
    assertTrue(missingFieldsList.containsKey(IGoodsReceiptSectionNames.ITEMS_SECTION));

    List<String> actualMissingItemsFields =
        (List<String>) missingFieldsList.get(IGoodsReceiptSectionNames.ITEMS_SECTION);
    assertTrue(actualMissingItemsFields.contains(missingFieldsMap.get("MissingFields")));
  }

  public void assertThatPurchaseOrdersExist(DataTable purchaseOrderTable) {
    List<Map<String, String>> purchaseOrders =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrders) {
      assertpurchaseOrdersExist(purchaseOrder);
    }
  }

  private void assertpurchaseOrdersExist(Map<String, String> purchaseOrder) {
    DObPurchaseOrderGeneralModel PurchaseOrderGeneralModel =
        (DObPurchaseOrderGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseOrderWithCurrentStateAndGoodsState",
                constructPurchaseOrder(purchaseOrder));
    assertNotNull(PurchaseOrderGeneralModel);
  }

  private Object[] constructPurchaseOrder(Map<String, String> purchaseOrder) {
    return new Object[] {
      purchaseOrder.get(IFeatureFileCommonKeys.CODE),
      "%" + purchaseOrder.get(IFeatureFileCommonKeys.STATE) + "%",
      "%" + purchaseOrder.get(IFeatureFileCommonKeys.GOODS_STATE) + "%",
      purchaseOrder.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
    };
  }

  public void assertThatPODataExistForGoodsReceipt(DataTable goodsReceiptPurchaseOrderDataTable) {
    List<Map<String, String>> goodsReceiptPurchaseOrders =
        goodsReceiptPurchaseOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptPurchaseOrder : goodsReceiptPurchaseOrders) {
      assertGoodsReceiptPurchaseOrderExist(goodsReceiptPurchaseOrder);
    }
  }

  private void assertGoodsReceiptPurchaseOrderExist(Map<String, String> goodsReceiptPurchaseOrder) {
    IObGoodsReceiptPurchaseOrderDataGeneralModel iObGoodsReceiptPurchaseOrderDataGeneralModel =
        (IObGoodsReceiptPurchaseOrderDataGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptPurchaseOrderData",
                constructGoodsReceiptPurchaseOrderData(goodsReceiptPurchaseOrder));
    assertNotNull(
        goodsReceiptPurchaseOrder.get(IFeatureFileCommonKeys.GR_CODE),
        iObGoodsReceiptPurchaseOrderDataGeneralModel);
  }

  private Object[] constructGoodsReceiptPurchaseOrderData(
      Map<String, String> goodsReceiptPurchaseOrder) {
    return new Object[] {
      goodsReceiptPurchaseOrder.get(IFeatureFileCommonKeys.DELIVERY_NOTE_PL),
      goodsReceiptPurchaseOrder.get(IFeatureFileCommonKeys.GR_CODE),
      goodsReceiptPurchaseOrder.get(IFeatureFileCommonKeys.VENDOR),
      goodsReceiptPurchaseOrder.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
      goodsReceiptPurchaseOrder.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE),
      goodsReceiptPurchaseOrder.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
    };
  }

  private Object[] constructGoodsReceiptHeaderData(Map<String, String> goodsReceiptHeader) {
    return new Object[] {
      goodsReceiptHeader.get(IFeatureFileCommonKeys.CODE),
      goodsReceiptHeader.get(IFeatureFileCommonKeys.CREATED_BY),
      goodsReceiptHeader.get(IFeatureFileCommonKeys.TYPE),
      goodsReceiptHeader.get(IFeatureFileCommonKeys.STOREKEEPER),
      "%" + goodsReceiptHeader.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatGoodsReceiptHasEmptyItemsList(String goodsReceiptCode) {
    IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptItems =
        (IObGoodsReceiptReceivedItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptItemsByCode", goodsReceiptCode);
    assertNull(goodsReceiptItems);
  }

  public void assertThatCostingCreatedAfterGoodsReceiptChangedToActive(DataTable costing) {
    List<Map<String, String>> costingMaps = costing.asMaps(String.class, String.class);
    for (Map<String, String> costingMap : costingMaps) {

      Object[] queryParams = constructcostingQueryParams(costingMap);

      DObCostingGeneralModel costingGeneralModel =
          (DObCostingGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCosting", queryParams);
      Assert.assertNotNull("Costing doesn't exist", costingGeneralModel);
    }
  }

  private Object[] constructcostingQueryParams(Map<String, String> costingMap) {

    return new Object[] {
      costingMap.get(IAccountingFeatureFileCommonKeys.COSTING_CODE),
      costingMap.get(IFeatureFileCommonKeys.CREATED_BY),
      costingMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      costingMap.get(IFeatureFileCommonKeys.COMPANY),
      costingMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertThatCostingDetailsCreatedAfterGoodsReceiptChangedToActive(
      String code, DataTable goodsReceiptAccountingDetails) {
    List<Map<String, String>> goodsReceiptAccountingDetailstMaps =
        goodsReceiptAccountingDetails.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptAccountingDetailsMap :
        goodsReceiptAccountingDetailstMaps) {

      Object[] queryParams =
          constructCostingAccountingdetailsQueryParams(code, goodsReceiptAccountingDetailsMap);

      IObCostingAccountingDetailsGeneralModel costingDetails =
          (IObCostingAccountingDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCostingAccountingDetails", queryParams);
      Assert.assertNotNull("CostingAccountingDetails doesn't exist", costingDetails);
    }
  }

  private Object[] constructCostingAccountingdetailsQueryParams(
      String code, Map<String, String> goodsReceiptAccountingDetailsMap) {
    return new Object[] {
      code,
      goodsReceiptAccountingDetailsMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
      goodsReceiptAccountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
      goodsReceiptAccountingDetailsMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT),
    };
  }

  public void assertThatCostingItemsCreated(String costingCode, DataTable costingItems) {
    List<Map<String, String>> costingItemstMaps = costingItems.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptAccountingItemMap : costingItemstMaps) {

      Object[] queryParams =
          constructGoodsReceiptAccountingItemsQueryParams(
              costingCode, goodsReceiptAccountingItemMap);

      IObCostingItemGeneralModel costingitem =
          (IObCostingItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCostingItem", queryParams);
      Assert.assertNotNull("CostingItem doesn't exist", costingitem);
    }
  }

  private Object[] constructGoodsReceiptAccountingItemsQueryParams(
      String costingCode, Map<String, String> goodsReceiptAccountingItemMap) {
    return new Object[] {
      costingCode,
      goodsReceiptAccountingItemMap.get(IFeatureFileCommonKeys.ITEM),
      goodsReceiptAccountingItemMap.get(IFeatureFileCommonKeys.UOM),
      goodsReceiptAccountingItemMap.get(IFeatureFileCommonKeys.QUANTITY),
      goodsReceiptAccountingItemMap.get(IFeatureFileCommonKeys.STOCK_TYPE)
    };
  }

  public void assertThatCostingDetailsCreated(String costingCode, DataTable costingDetails) {
    List<Map<String, String>> costingDetailstMaps =
        costingDetails.asMaps(String.class, String.class);
    for (Map<String, String> costingDetailsMap : costingDetailstMaps) {

      Object[] queryParams = constructcostingDetailsQueryParams(costingCode, costingDetailsMap);

      IObCostingDetailsGeneralModel getGoodsReceiptAccountingDocumntDetails =
          (IObCostingDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCostingDetails", queryParams);
      Assert.assertNotNull("costingDetails doesn't exist", getGoodsReceiptAccountingDocumntDetails);
    }
  }

  private Object[] constructcostingDetailsQueryParams(
      String code, Map<String, String> costingDetailsMap) {
    return new Object[] {code, costingDetailsMap.get(IFeatureFileCommonKeys.GR_CODE)};
  }

  public void assertThatStoreTransactionsExistWithRefDocument(
      String refDocument,
      String itemCode,
      String unitOfMeasureCode,
      String companyCode,
      String storeHouseCode,
      String businessUnitCode,
      DataTable storeTransactionsDataTable) {
    List<Map<String, String>> expectedStoreTransactionRecords =
        storeTransactionsDataTable.asMaps(String.class, String.class);
    List<TObStoreTransactionGeneralModel> storeTransactionsList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getStoreTransactionRecordsByGoodsIssueRefDocument",
            constructStoreTransactionRecordsWithDataQueryParams(
                refDocument,
                itemCode,
                unitOfMeasureCode,
                companyCode,
                storeHouseCode,
                businessUnitCode));
    for (int i = 0; i < storeTransactionsList.size(); i++) {
      assertEquals(
          expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE),
          storeTransactionsList.get(i).getUserCode(),
          "StoreTransaction record with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE)
              + " for Item "
              + itemCode
              + " with UOM "
              + unitOfMeasureCode
              + " in company "
              + companyCode
              + " in storehouse "
              + storeHouseCode
              + " for BusinessUnit "
              + businessUnitCode
              + " doesn't exist!");
      assertEquals(
          expectedStoreTransactionRecords.get(i).get(IStoreTransactionFileCommonKeys.STOCK_TYPE),
          storeTransactionsList.get(i).getStockType(),
          "StockType "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IStoreTransactionFileCommonKeys.STOCK_TYPE)
              + " doesn't exist in StoreTransaction with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE));
      assertEquals(
          expectedStoreTransactionRecords
              .get(i)
              .get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE),
          storeTransactionsList.get(i).getTransactionOperation(),
          "TransactionOperation "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE)
              + " doesn't exist in StoreTransaction with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE));
      assertEquals(
          new BigDecimal(expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.QTY)),
          storeTransactionsList.get(i).getQuantity(),
          "Quantity "
              + expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.QTY)
              + " doesn't exist in StoreTransaction with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE));
      assertEquals(
          new BigDecimal(
              expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.REMAINING_QTY)),
          storeTransactionsList.get(i).getRemainingQuantity(),
          "Remaining Quantity "
              + expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.REMAINING_QTY)
              + " doesn't exist in StoreTransaction with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE));
      assertNull(
          storeTransactionsList.get(i).getEstimateCost(),
          "Cost "
              + expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.ESTIMATE_COST)
              + " doesn't exist in StoreTransaction with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE));
      assertNull(
          storeTransactionsList.get(i).getActualCost(),
          "ActualCost "
              + expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.ACTUAL_COST)
              + " doesn't exist in StoreTransaction with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE));
      assertEquals(
          expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.REF_TRANSACTION),
          storeTransactionsList.get(i).getRefTransactionCode(),
          "RefTransaction "
              + expectedStoreTransactionRecords.get(i).get(IFeatureFileCommonKeys.REF_TRANSACTION)
              + " doesn't exist in StoreTransaction with code: "
              + expectedStoreTransactionRecords
                  .get(i)
                  .get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE));
    }
  }

  private Object[] constructStoreTransactionRecordsWithDataQueryParams(
      String refDocument,
      String itemCode,
      String uomCode,
      String companyCode,
      String storehouseCode,
      String businessUnitCode) {
    return new Object[] {
      refDocument.split(" - ")[1], itemCode, uomCode, companyCode, storehouseCode, businessUnitCode
    };
  }

  public void assertThatStoreTransactionExist(DataTable storeTransactionsDataTable) {

    List<Map<String, String>> expectedStoreTransactionRecords =
        storeTransactionsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> storeTransactionRecord : expectedStoreTransactionRecords) {
      TObStoreTransactionAllDataGeneralModel storeTransactionAllDataGeneralModelRecord =
          (TObStoreTransactionAllDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getStoreTransactionAllDataRecordsByParameters",
                  constructStoreTransactionRecordsQueryParams(storeTransactionRecord));

      assertNotNull(
          storeTransactionRecord.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE)
              + " Doesn't exist!",
          storeTransactionAllDataGeneralModelRecord);

      assertDateEquals(
          convertEmptyStringToNull(
              storeTransactionRecord.get(IStoreTransactionFileCommonKeys.ORIGINAL_ADDING_DATE)),
          storeTransactionAllDataGeneralModelRecord.getOriginalAddingDate());

      assertDateEquals(
          storeTransactionRecord.get(IFeatureFileCommonKeys.CREATION_DATE),
          storeTransactionAllDataGeneralModelRecord.getCreationDate());
    }
  }

  private Object[] constructStoreTransactionRecordsQueryParams(
      Map<String, String> storeTransactionRecord) {
    Object[] storeTransactionRecordObject =
        new Object[] {
          storeTransactionRecord.get(IFeatureFileCommonKeys.BATCH_NUMBER),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REF_TRANSACTION),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ESTIMATE_COST),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ACTUAL_COST),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REMAINING_QTY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STOCK_TYPE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.ITEM),
          storeTransactionRecord.get(IFeatureFileCommonKeys.UOM),
          storeTransactionRecord.get(IFeatureFileCommonKeys.QTY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.TRANSACTION_TYPE),
          storeTransactionRecord.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.COMPANY),
          storeTransactionRecord.get(IFeatureFileCommonKeys.PLANT),
          storeTransactionRecord.get(IFeatureFileCommonKeys.STOREHOUSE)
        };
    return storeTransactionRecordObject;
  }
}
