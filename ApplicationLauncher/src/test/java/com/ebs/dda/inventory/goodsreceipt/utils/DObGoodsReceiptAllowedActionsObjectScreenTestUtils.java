package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrder;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DObGoodsReceiptAllowedActionsObjectScreenTestUtils
    extends DObGoodsReceiptCommonTestUtils {
  public String readRestUrl;

  public DObGoodsReceiptAllowedActionsObjectScreenTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    updateRestURLs();
  }

  public void assertThatTheseActionsExistInThisState(String state, DataTable allowedActions)
      throws Exception {
    DObGoodsReceiptPurchaseOrder dObGoodsReceiptPurchaseOrder = new DObGoodsReceiptPurchaseOrder();
    dObGoodsReceiptPurchaseOrder.setCurrentStates(new HashSet<String>(Arrays.asList(state)));
    Set<String> stateMachineAllowedActionsActual =
        this.getStateMachineAllowedActions(dObGoodsReceiptPurchaseOrder);
    Set stateMachineAllowedActionsExpected = getExpectedAllowedActionsAsSet(allowedActions);
    Assert.assertEquals(stateMachineAllowedActionsExpected, stateMachineAllowedActionsActual);
  }

  protected void updateRestURLs() {
    readRestUrl = IDObGoodsReceiptRestUrls.AUTHORIZED_ACTIONS;
    super.updateRestURLs();
  }

  private <T extends IStatefullBusinessObject> Set<String> getStateMachineAllowedActions(
      BusinessObject entity) throws Exception {

    T statefulEntity = (T) entity;
    Set<String> allowedActionForState = new HashSet<>();
    DObGoodsReceiptStateMachine dObGoodsReceiptStateMachine = new DObGoodsReceiptStateMachine();
    dObGoodsReceiptStateMachine.initObjectState(statefulEntity);
    Set<String> stateMachineActions = dObGoodsReceiptStateMachine.getAllowedActions();
    for (String action : stateMachineActions) {
      if (dObGoodsReceiptStateMachine.canFireEvent(action)) allowedActionForState.add(action);
    }

    return allowedActionForState;
  }
}
