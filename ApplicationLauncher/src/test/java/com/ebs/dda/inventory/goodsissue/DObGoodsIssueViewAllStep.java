package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueViewAllTestUtils;
import com.ebs.dda.jpa.inventory.goodsissue.IDObGoodsIssueGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObGoodsIssueViewAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsIssueViewAllTestUtils goodsReceiptViewAllTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    goodsReceiptViewAllTestUtils = new DObGoodsIssueViewAllTestUtils(getProperties());
    goodsReceiptViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObGoodsIssueViewAllStep() {
    Given(
        "^the following GoodsIssues exist with the following data:$",
        (DataTable goodsIssuesTable) -> {
          goodsReceiptViewAllTestUtils.assertGoodsIssuesExist(goodsIssuesTable);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                  userCookie, IDObGoodsIssueRestURLS.VIEW_ALL_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable goodsIssueDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          goodsReceiptViewAllTestUtils.assertResponseSuccessStatus(response);
          goodsReceiptViewAllTestUtils.assertGoodsIssuesDataIsCorrect(
              goodsIssueDataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          goodsReceiptViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
              response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with filter applied on GICode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              goodsReceiptViewAllTestUtils.getContainsFilterField(
                  IDObGoodsIssueGeneralModel.USER_CODE, userCodeContains);
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                  cookie, IDObGoodsIssueRestURLS.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String stateContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              goodsReceiptViewAllTestUtils.getContainsFilterField(
                  IDObGoodsIssueGeneralModel.CURRENT_STATES, stateContains);
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                  cookie, IDObGoodsIssueRestURLS.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with filter applied on Creation date which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String dateTime, Integer recordsPerPage) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filterString =
              goodsReceiptViewAllTestUtils.getContainsFilterField(
                  IDObGoodsIssueGeneralModel.CREATION_DATE,
                  String.valueOf(
                      goodsReceiptViewAllTestUtils.getDateTimeInMilliSecondsFormat(dateTime)));
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObGoodsIssueRestURLS.VIEW_ALL_URL,
                  pageNumber,
                  recordsPerPage,
                  filterString);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with filter applied on Storehouse which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              goodsReceiptViewAllTestUtils.getContainsFilterField(
                  IDObGoodsIssueGeneralModel.STORE_HOUSE_NAME, filteringValue);
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObGoodsIssueRestURLS.VIEW_ALL_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  DObGoodsIssueViewAllTestUtils.REQUEST_LOCALE);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with filter applied on Customer which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              goodsReceiptViewAllTestUtils.getContainsFilterField(
                  IDObGoodsIssueGeneralModel.CUSTOMER, filteringValue);
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObGoodsIssueRestURLS.VIEW_ALL_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  DObGoodsIssueViewAllTestUtils.REQUEST_LOCALE);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with filter applied on PurchaseUnit which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              goodsReceiptViewAllTestUtils.getContainsFilterField(
                  IDObGoodsIssueGeneralModel.PURCHASING_UNIT_NAME, filteringValue);
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObGoodsIssueRestURLS.VIEW_ALL_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  DObGoodsIssueViewAllTestUtils.REQUEST_LOCALE);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of GoodsIssues with filter applied on GoodsIssueType which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              goodsReceiptViewAllTestUtils.getContainsFilterField(
                  IDObGoodsIssueGeneralModel.TYPE_NAME, filteringValue);
          Response response =
              goodsReceiptViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObGoodsIssueRestURLS.VIEW_ALL_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  DObGoodsIssueViewAllTestUtils.REQUEST_LOCALE);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all GoodsIssues -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            goodsReceiptViewAllTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View all GoodsIssues -")) {
      databaseConnector.closeConnection();
    }
  }
}
