package com.ebs.dda.inventory.storetransaction;

public interface IDObStockavailabilitiesRestUrls {

  String GET_URL = "/services/stockavailability";

}

