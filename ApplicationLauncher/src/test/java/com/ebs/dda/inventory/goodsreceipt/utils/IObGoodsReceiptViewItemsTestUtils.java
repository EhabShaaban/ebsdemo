package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class IObGoodsReceiptViewItemsTestUtils extends IObGoodsReceiptItemTestUtils {

  private static final String RESULT_KEY = "result";
  String QUANTITIES = "quantities";
  String BATCHES = "batches";

  private ObjectMapper mapperObj;

  public IObGoodsReceiptViewItemsTestUtils(Map<String, Object> properties) {
    super(properties);
    mapperObj = new ObjectMapper();
  }

  public Response viewReceivedItemsSection(Cookie cookie, String goodsReceiptCode) {
    String restUrl = "/services/goodsreceipt/" + goodsReceiptCode + "/items";
    return sendGETRequest(cookie, restUrl);
  }

  public void assertGoodsReceiptItemsExist(String goodsReceiptCode, DataTable items) {

    List<Map<String, String>> expectedItemsMaps = items.asMaps(String.class, String.class);
    for (Map<String, String> expectedItems : expectedItemsMaps) {
      expectedItems = convertEmptyStringsToNulls(expectedItems);
      IObGoodsReceiptReceivedItemsGeneralModel itemsGeneralModel =
          (IObGoodsReceiptReceivedItemsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptItemsByGRCodeAndItemData",
                  goodsReceiptCode,
                  expectedItems.get(IFeatureFileCommonKeys.ITEM).split(" - ")[0]);

      assertEquals(
          expectedItems.get(IFeatureFileCommonKeys.DIFFERENCE_REASON),
          itemsGeneralModel.getDifferenceReason());
    }
  }

  public void assertGoodsReceiptPurchaseOrderItemQuantitiesExist(
      String goodsReceiptCode, String item, DataTable itemQuantitiesDataTable) {

    List<Map<String, String>> expectedItemQuantitiesMaps =
        itemQuantitiesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedItemQuantities : expectedItemQuantitiesMaps) {
      Object[] queryParams =
          constructItemQuantitiesQueryParams(goodsReceiptCode, item, expectedItemQuantities);
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel itemQuantities =
          (IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptPurchaseOrderItemsQuantitiesByGRCodeAndItemData", queryParams);

      assertNotNull("item not exist" + Arrays.toString(queryParams), itemQuantities);
    }
  }

  public void assertGoodsReceiptSalesReturnItemQuantitiesExist(
      String goodsReceiptCode, String item, DataTable itemQuantitiesDataTable) {

    List<Map<String, String>> expectedItemQuantitiesMaps =
        itemQuantitiesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedItemQuantities : expectedItemQuantitiesMaps) {

      IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel itemQuantities =
          (IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptSalesReturnItemsQuantitiesByGRCodeAndItemData",
                  constructItemQuantitiesQueryParams(
                      goodsReceiptCode, item, expectedItemQuantities));

      assertNotNull(itemQuantities);
    }
  }

  private Object[] constructItemQuantitiesQueryParams(
      String goodsReceiptCode, String item, Map<String, String> expectedItemQuantities) {
    return new Object[] {
      expectedItemQuantities.get(IGoodsReceiptFeatureFileCommonKeys.NOTES),
      goodsReceiptCode,
      item.split(" - ")[0],
      expectedItemQuantities.get(IFeatureFileCommonKeys.STOCK_TYPE),
      expectedItemQuantities.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
      expectedItemQuantities.get(IFeatureFileCommonKeys.UOE).split(" - ")[0]
    };
  }

  public void assertGoodsReceiptItemBatchesExist(
      String goodsReceiptCode, String item, DataTable itemBatchesDataTable) {
    List<Map<String, String>> expectedItemBatchesMaps =
        itemBatchesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedItemBatches : expectedItemBatchesMaps) {
      expectedItemBatches = convertEmptyStringsToNulls(expectedItemBatches);
      IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel itemBatch =
          (IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptItemsBatchesByGRCodeAndItemData",
                  constructBatchesQueryParams(goodsReceiptCode, item, expectedItemBatches));

      assertNotNull(itemBatch);

      assertEquals(
          expectedItemBatches.get(IFeatureFileCommonKeys.STOCK_TYPE), itemBatch.getStockType());
      assertEquals(
          expectedItemBatches.get(IGoodsReceiptFeatureFileCommonKeys.NOTES), itemBatch.getNotes());
    }
  }

  private Object[] constructBatchesQueryParams(
      String goodsReceiptCode, String item, Map<String, String> expectedItemBatches) {
    return new Object[] {
      goodsReceiptCode,
      item.split(" - ")[0],
      expectedItemBatches.get(IFeatureFileCommonKeys.BATCH_NUMBER),
      expectedItemBatches.get(IFeatureFileCommonKeys.PRODUCTION_DATE),
      expectedItemBatches.get(IFeatureFileCommonKeys.EXPIRATION_DATE),
      expectedItemBatches.get(IFeatureFileCommonKeys.BATCH_QUANTITY_UOE),
      expectedItemBatches.get(IFeatureFileCommonKeys.UOE).split(" - ")[0]
    };
  }

  public void assertThatReceivedItemsAreRepresentedToUser(
      DataTable receivedItems, Response response) throws Exception {

    List<Map<String, String>> expectedReceivedItemsMaps =
        receivedItems.asMaps(String.class, String.class);
    List<Map<String, Object>> actualGoodsReceiptsReceivedItemsMaps =
        response.body().jsonPath().getList(RESULT_KEY);
    for (Map<String, String> expectedReceivedItemMap : expectedReceivedItemsMaps) {
      expectedReceivedItemMap = convertEmptyStringsToNulls(expectedReceivedItemMap);
      assertExpectedReceivedItemExistsInActualData(
          expectedReceivedItemMap, actualGoodsReceiptsReceivedItemsMaps);
    }
  }

  public void assertThatReceivedItemsQuantitiesAreRepresentedToUser(
      String itemCode, DataTable receivedItemsQuantities, Response response) throws Exception {

    List<Map<String, String>> expectedReceivedItemQuantitiesMaps =
        receivedItemsQuantities.asMaps(String.class, String.class);
    List<Map<String, Object>> actualGoodsReceiptsReceivedItemsMaps =
        response.body().jsonPath().getList(RESULT_KEY);
    for (Map<String, String> expectedReceivedItemQuantityMap : expectedReceivedItemQuantitiesMaps) {
      assertExpectedReceivedItemQuantitiesExistsInActualData(
          itemCode, expectedReceivedItemQuantityMap, actualGoodsReceiptsReceivedItemsMaps);
    }
  }

  public void assertThatReceivedItemsBatchesAreRepresentedToUser(
      String itemCode, DataTable receivedItemsBatches, Response response) throws Exception {

    List<Map<String, String>> expectedGoodsReceiptsReceivedItemsBatchesMaps =
        receivedItemsBatches.asMaps(String.class, String.class);
    List<Map<String, Object>> actualGoodsReceiptsReceivedItemsMaps =
        response.body().jsonPath().getList(RESULT_KEY);
    for (Map<String, String> expectedGoodsReceiptsReceivedItemsBatcheMap :
        expectedGoodsReceiptsReceivedItemsBatchesMaps) {
      assertExpectedReceivedItemBatchesExistsInActualData(
          itemCode,
          expectedGoodsReceiptsReceivedItemsBatcheMap,
          actualGoodsReceiptsReceivedItemsMaps);
    }
  }

  private void assertExpectedReceivedItemExistsInActualData(
      Map<String, String> expectedReceivedItem, List<Map<String, Object>> actualReceivedItemMaps)
      throws Exception {
    Boolean isExistingItem = false;
    String expectedItemCode = expectedReceivedItem.get(IFeatureFileCommonKeys.ITEM_CODE);

    for (Map<String, Object> actualItemMap : actualReceivedItemMaps) {
      String actualItemCode =
          actualItemMap.get(IIObGoodsReceiptReceivedItemsData.RECEIVED_ITEM_CODE).toString();
      if (expectedItemCode.equals(actualItemCode)) {
        isExistingItem = true;
        assertExpectedItemDataEqualsActualItemData(expectedReceivedItem, actualItemMap);
      }
    }
    assertTrue(isExistingItem);
  }

  private void assertExpectedItemDataEqualsActualItemData(
      Map<String, String> expectedReceivedItem, Map<String, Object> actualItemMap)
      throws Exception {
    String itemNameEnValue =
        getEnglishValue(actualItemMap.get(IIObGoodsReceiptReceivedItemsGeneralModel.ITEM_NAME));
    String baseUnitEnValue =
        getEnglishValue(actualItemMap.get(IIObGoodsReceiptReceivedItemsGeneralModel.BASE_UNIT));
    String actualBaseUnitCode =
        actualItemMap.get(IIObGoodsReceiptReceivedItemsGeneralModel.BASE_UNIT_CODE).toString();
    assertEquals(expectedReceivedItem.get(IFeatureFileCommonKeys.ITEM_NAME), itemNameEnValue);
    assertEquals(
        expectedReceivedItem.get(IFeatureFileCommonKeys.BASE_UNIT).split(" - ")[0],
        actualBaseUnitCode);
    assertEquals(
        expectedReceivedItem.get(IFeatureFileCommonKeys.BASE_UNIT).split(" - ")[1],
        baseUnitEnValue);

    if (expectedReceivedItem.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN) == null) {
      assertThatExpectedSalesReturnQtyIsEqualsActualIfExist(expectedReceivedItem, actualItemMap);
    } else {
      assertEquals(
          getBigDecimal(expectedReceivedItem.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN))
              .compareTo(
                  getBigDecimal(
                      actualItemMap.get(IIObGoodsReceiptReceivedItemsGeneralModel.QTY_IN_DN))),
          0);
    }
    assertEquals(
        expectedReceivedItem.get(IFeatureFileCommonKeys.DIFFERENCE_REASON),
        actualItemMap.get(IIObGoodsReceiptReceivedItemsGeneralModel.DIFFERENCE_REASON));
  }

  private void assertThatExpectedSalesReturnQtyIsEqualsActualIfExist(
      Map<String, String> expectedReceivedItem, Map<String, Object> actualItemMap) {
    if (expectedReceivedItem.get(IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_QTY_BASE_UNIT)
        == null) {
      assertNull(actualItemMap.get(IIObGoodsReceiptReceivedItemsGeneralModel.RETURNED_QTY_BASE));
    } else {
      assertEquals(
          getBigDecimal(
                  expectedReceivedItem.get(
                      IGoodsReceiptFeatureFileCommonKeys.SALES_RETURN_QTY_BASE_UNIT))
              .compareTo(
                  getBigDecimal(
                      actualItemMap.get(
                          IIObGoodsReceiptReceivedItemsGeneralModel.RETURNED_QTY_BASE))),
          0);
    }
  }

  private void assertExpectedReceivedItemQuantitiesExistsInActualData(
      String itemCode,
      Map<String, String> expectedReceivedItemQuantity,
      List<Map<String, Object>> actualReceivedItemMaps)
      throws Exception {
    Boolean isExistingItem = false;

    for (Map<String, Object> actualItemMap : actualReceivedItemMaps) {
      String actualItemCode =
          actualItemMap.get(IIObGoodsReceiptReceivedItemsData.RECEIVED_ITEM_CODE).toString();
      String actualItemType =
          actualItemMap.get(IIObGoodsReceiptReceivedItemsData.OBJECT_TYPE_CODE).toString();
      if (itemCode.equals(actualItemCode)
          && actualItemType.equals(
              IIObGoodsReceiptReceivedItemsData.ORDINARY_ITEMS_OBJECT_TYPE_CODE)) {
        isExistingItem = true;
        assertExpectedItemQuantityExistsInActualItemQuantities(
            expectedReceivedItemQuantity,
            (List<Map<String, Object>>) actualItemMap.get(QUANTITIES));
      }
    }
    assertTrue(isExistingItem);
  }

  private void assertExpectedItemQuantityExistsInActualItemQuantities(
      Map<String, String> expectedReceivedItemQuantity,
      List<Map<String, Object>> actualItemQuantitiesMap)
      throws Exception {
    Boolean isExistingQuantity = false;
    String expectedReceivedQuantity =
        expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE);
    for (Map<String, Object> actualItemQuantityMap : actualItemQuantitiesMap) {
      String actualReceivedQuantity =
          actualItemQuantityMap
              .get(IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.RECEIVED_QTY_UOE)
              .toString();
      if (expectedReceivedQuantity.equals(actualReceivedQuantity)) {
        isExistingQuantity = true;
        assertExpectedItemQuantityDataEqualsActualItemQuantityData(
            expectedReceivedItemQuantity, actualItemQuantityMap);
      }
    }
    assertTrue(isExistingQuantity);
  }

  private void assertExpectedItemQuantityDataEqualsActualItemQuantityData(
      Map<String, String> expectedReceivedItemQuantity, Map<String, Object> actualItemQuantityMap)
      throws Exception {
    expectedReceivedItemQuantity = convertEmptyStringsToNulls(expectedReceivedItemQuantity);

    assertEquals(
        expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.UOE).split(" - ")[0],
        actualItemQuantityMap.get(IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.UOE_CODE));
    assertEquals(
        expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.UOE).split(" - ")[1],
        getEnglishValue(
            actualItemQuantityMap.get(IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.UOE)));
    if (expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.DEFECTS_DESCRIPTION) != null) {
      assertEquals(
          expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.DEFECTS_DESCRIPTION),
          actualItemQuantityMap.get(
              IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.DEFECT_DESCRIPTION));
    }
    if (expectedReceivedItemQuantity.get(IGoodsReceiptFeatureFileCommonKeys.RECEIVED_QTY_BASE)
            != null
        && expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.BASE_UNIT) != null) {

      assertEquals(
          getBigDecimal(
                  expectedReceivedItemQuantity.get(
                      IGoodsReceiptFeatureFileCommonKeys.RECEIVED_QTY_BASE))
              .compareTo(
                  getBigDecimal(
                      actualItemQuantityMap.get(
                          IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.RECEIVED_QTY_BASE))),
          0);

      assertEquals(
          expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.BASE_UNIT).split(" - ")[1],
          getEnglishValue(
              actualItemQuantityMap.get(
                  IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.BASE_UNIT_SYMBOL)));
    }
    assertEquals(
        expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.STOCK_TYPE),
        actualItemQuantityMap.get(IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.STOCK_TYPE));

    if (expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.ITEM_CODE_AT_VENDOR) != null) {
      assertEquals(
          expectedReceivedItemQuantity.get(IFeatureFileCommonKeys.ITEM_CODE_AT_VENDOR),
          actualItemQuantityMap.get(
              IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.ITEM_CODE_AT_VENDOR));
    }
  }

  private void assertExpectedReceivedItemBatchesExistsInActualData(
      String itemCode,
      Map<String, String> expectedReceivedItemBatch,
      List<Map<String, Object>> actualReceivedItemMaps)
      throws Exception {
    Boolean isExistingItem = false;

    for (Map<String, Object> actualItemMap : actualReceivedItemMaps) {
      String actualItemCode =
          actualItemMap.get(IIObGoodsReceiptReceivedItemsData.RECEIVED_ITEM_CODE).toString();
      String actualItemType =
          actualItemMap.get(IIObGoodsReceiptReceivedItemsData.OBJECT_TYPE_CODE).toString();
      if (itemCode.equals(actualItemCode) && actualItemType.equals("2")) {
        isExistingItem = true;
        assertExpectedItemBatchExistsInActualItemBatches(
            expectedReceivedItemBatch, (List<Map<String, Object>>) actualItemMap.get(BATCHES));
      }
    }
    assertTrue(isExistingItem);
  }

  private void assertExpectedItemBatchExistsInActualItemBatches(
      Map<String, String> expectedReceivedItemBatch, List<Map<String, Object>> actualItemBatchesMap)
      throws Exception {
    Boolean isExistingBatch = false;
    String expectedBtachNumber = expectedReceivedItemBatch.get(IFeatureFileCommonKeys.BATCH_NUMBER);
    for (Map<String, Object> actualItemQuantityMap : actualItemBatchesMap) {
      String actualBatchNumber =
          actualItemQuantityMap
              .get(IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.BATCH_NUMBER)
              .toString();
      if (expectedBtachNumber.equals(actualBatchNumber)) {
        isExistingBatch = true;
        assertExpectedItemBatchDataEqualsActualItemBatchData(
            expectedReceivedItemBatch, actualItemQuantityMap);
      }
    }
    assertTrue(isExistingBatch);
  }

  private void assertExpectedItemBatchDataEqualsActualItemBatchData(
      Map<String, String> expectedReceivedItemBatch, Map<String, Object> actualItemBatchMap)
      throws Exception {

    String actualUnitOfEntry =
        mapperObj.writeValueAsString(
            actualItemBatchMap.get(
                IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.UNIT_OF_ENTRY));
    String actualUnitOfEntryCode =
        actualItemBatchMap
            .get(IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.UNIT_OF_ENTRY_CODE)
            .toString();

    assertEquals(
        expectedReceivedItemBatch.get(IFeatureFileCommonKeys.BATCH_NUMBER),
        actualItemBatchMap.get(
            IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.BATCH_NUMBER));
    assertExpectedDateEqualActualDate(
        expectedReceivedItemBatch.get(IFeatureFileCommonKeys.PRODUCTION_DATE),
        actualItemBatchMap
            .get(IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.PRODUCTION_DATE)
            .toString());
    assertExpectedDateEqualActualDate(
        expectedReceivedItemBatch.get(IFeatureFileCommonKeys.EXPIRATION_DATE),
        actualItemBatchMap
            .get(IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.EXPIRATION_DATE)
            .toString());
    assertBatchQuantityEquals(expectedReceivedItemBatch, actualItemBatchMap);
    assertEquals(
        expectedReceivedItemBatch.get(IFeatureFileCommonKeys.UOE).split(" - ")[0],
        actualUnitOfEntryCode);
    assertEquals(
        expectedReceivedItemBatch.get(IFeatureFileCommonKeys.UOE).split(" - ")[1],
        getEnglishLocaleFromLocalizedResponse(actualUnitOfEntry));
    if (!expectedReceivedItemBatch.get(IFeatureFileCommonKeys.STOCK_TYPE).isEmpty()) {
      assertEquals(
          expectedReceivedItemBatch.get(IFeatureFileCommonKeys.STOCK_TYPE),
          actualItemBatchMap.get(
              IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.STOCK_TYPE));
    }
    if (!expectedReceivedItemBatch.get(IFeatureFileCommonKeys.DEFECTS_REASON).isEmpty()) {

      assertEquals(
          expectedReceivedItemBatch.get(IFeatureFileCommonKeys.DEFECTS_REASON),
          actualItemBatchMap.get(IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.NOTES));
    }
  }

  private void assertBatchQuantityEquals(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    BigDecimal expected = getBigDecimal(expectedMap.get(IFeatureFileCommonKeys.BATCH_QUANTITY_UOE));
    BigDecimal actual =
        getBigDecimal(
            actualMap
                .get(IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.RECEIVED_QTY_UOE)
                .toString());
    assertEquals(expected.compareTo(actual), 0);
  }

  private void assertExpectedDateEqualActualDate(
      String expectedDateString, String actualDateString) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
    DateTimeFormatter responseFormatter =
        DateTimeFormat.forPattern(DateFormats.DATE_TIME_WITH_TIMEZONE);

    DateTime expectedDate = formatter.parseDateTime(expectedDateString).withZone(DateTimeZone.UTC);
    DateTime actualDate =
        responseFormatter.parseDateTime(actualDateString).withZone(DateTimeZone.UTC);
    assertEquals(expectedDate, actualDate);
  }

  public void assertThatPurchaseOrderHasItems(String purchaseOrderCode, DataTable items) {
    List<Map<String, String>> expectedItemMaps = items.asMaps(String.class, String.class);
    for (Map<String, String> expectedItem : expectedItemMaps) {
      expectedItem = convertEmptyStringsToNulls(expectedItem);
      Object quantityInDN =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getOrderLineDetailsGeneralModel",
              constructPOItemBaseQueryParams(purchaseOrderCode, expectedItem));

      if (expectedItem.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN) == null) {
        assertNull(quantityInDN);
      } else {
        assertEquals(
            getBigDecimal(expectedItem.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN))
                .compareTo(getBigDecimal(quantityInDN)),
            0);
      }
    }
  }

  public void assertThatPurchaseOrdersExistByCode(DataTable purchaseOrdersData) {
    List<Map<String, String>> expectedOrdersMaps =
        purchaseOrdersData.asMaps(String.class, String.class);
    for (Map<String, String> expectedOrder : expectedOrdersMaps) {
      String orderCode = expectedOrder.get(IFeatureFileCommonKeys.CODE);
      Object order =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPurchaseOrderByCode", orderCode);
      Assert.assertNotNull("Purchase Order " + orderCode + " does not exist", order);
    }
  }

  public void assertUOMsExistInItemWithCode(DataTable itemsData, String itemCode) {
    List<Map<String, String>> itemsList = itemsData.asMaps(String.class, String.class);
    for (int i = 1; i < itemsList.size(); i++) {
      Map<String, String> itemUOMData = itemsList.get(i);
      Object[] params = constructItemHasUnitOfMeasureQueryParams(itemCode, itemUOMData);
      if (params != null) {
        assertThatItemHasUnitOfMeasure(itemCode, params);
      }
    }
  }

  public void assertThatItemHasUnitOfMeasure(String itemCode, Object... params) {
    Object uom =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getItemUOMWithCodeAndName", params);
    Assert.assertNotNull("Item" + itemCode + " Item did not have UOM " + params[1], uom);
  }

  public Object[] constructItemHasUnitOfMeasureQueryParams(
      String itemCode, Map<String, String> itemUOMData) {
    return new Object[] {
      itemCode,
      itemUOMData.get(IGoodsReceiptFeatureFileCommonKeys.ALTERNATIVE_UNIT),
      Float.parseFloat(itemUOMData.get(IGoodsReceiptFeatureFileCommonKeys.CONVERSION_FACTOR)),
      itemUOMData.get(IGoodsReceiptFeatureFileCommonKeys.OLD_ITEM_NUMBER)
    };
  }

  public Object[] constructPOItemBaseQueryParams(
      String purchaseOrderCode, Map<String, String> expectedItem) {
    return new Object[] {
      purchaseOrderCode,
      expectedItem.get(IFeatureFileCommonKeys.ITEM_CODE),
      expectedItem.get(IFeatureFileCommonKeys.BASE_UNIT)
    };
  }

  public void assertSalesReturnHasOrderItems(String srCode, DataTable orderItemsDataTable) {
    List<Map<String, String>> expectedSalesReturnOrderItemsMaps =
        orderItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedOrderItem : expectedSalesReturnOrderItemsMaps) {
      Object order =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getSalesReturnOrderWithReturnQtyInBase",
              constructSalesOrderItemQueryParams(srCode, expectedOrderItem));
      Assert.assertNotNull(
          "returned item "
              + expectedOrderItem.get(IFeatureFileCommonKeys.ITEM).split(" - ")[0]
              + " does not exist in SROrder "
              + srCode,
          order);
    }
  }

  public Object[] constructSalesOrderItemQueryParams(
      String salesOrderCode, Map<String, String> expectedOrderItem) {
    return new Object[] {
      salesOrderCode,
      expectedOrderItem.get(IFeatureFileCommonKeys.ITEM),
      getDouble(expectedOrderItem.get(ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY)),
      expectedOrderItem.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      getDouble(
          expectedOrderItem.get(
              ISalesReturnOrderFeatureFileCommonKeys.RETURN_QUANTITY_IN_BASE_UNIT)),
      expectedOrderItem.get(IFeatureFileCommonKeys.BASE_UNIT)
    };
  }
}
