package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptCompanyViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObGoodsReceiptCompanyViewStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObGoodsReceiptCompanyViewTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new IObGoodsReceiptCompanyViewTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public IObGoodsReceiptCompanyViewStep() {
    Given(
            "^the following CompanyData for GoodsReceipts exist:$",
            (DataTable companyDataTable) -> {
                utils.assertThatGoodsReceiptHasCompany(companyDataTable);
            });

    When(
        "^\"([^\"]*)\" requests to view Company section of GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
            String readUrl = utils.getGoodsReceiptCompanyViewDataUrl(goodsReceiptCode);
            Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
            Response goodsReceiptHeaderResponse =
                    utils.sendGETRequest(userCookie, readUrl);
            userActionsTestUtils.setUserResponse(userName, goodsReceiptHeaderResponse);
        });

    Then(
        "^the following values of Company section for GoodsReceipt with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String GoodsReceiptCode, String userName, DataTable sompanyDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatGoodsReceiptCompanyMatchesResponse(sompanyDataTable, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GR CompanyData section -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View GR CompanyData section -")) {
      databaseConnector.closeConnection();
    }
  }
}
