package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObGoodsIssueAllowedActionsStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private DObGoodsIssueCommonTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new DObGoodsIssueCommonTestUtils();
        utils.setProperties(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObGoodsIssueAllowedActionsStep() {
        When(
                "^\"([^\"]*)\" requests to read Authorized actions and Reads of GoodsIssue in home screen$",
                (String userName) -> {
                    String url = IDObGoodsIssueRestURLS.AUTHORIZED_ACTIONS;
                    Response response =
                            utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

    Then(
        "^the following actions are displayed to \"([^\"]*)\":$",
        (String userName, DataTable allowedactionsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedactionsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of GoodsIssue with code \"([^\"]*)\"$",
        (String username, String goodsIssueCode) -> {
          String url = IDObGoodsIssueRestURLS.AUTHORIZED_ACTIONS + '/' + goodsIssueCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(username), url);
            userActionsTestUtils.setUserResponse(username, response);
        });

        Then(
                "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
                (String allowedActions, String username) -> {
                    Response response = userActionsTestUtils.getUserResponse(username);
                    utils.assertThatTheFollowingActionsExist(response, allowedActions);
                });

        Given(
                "^the following GoodsIssues exist:$",
                (DataTable goodsIssueDataTable) -> {
                    utils.assertGoodsIssuesExist(goodsIssueDataTable);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read allowed actions in Goods Issue")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "Read allowed actions in Goods Issue")) {
            databaseConnector.closeConnection();
        }
    }
}
