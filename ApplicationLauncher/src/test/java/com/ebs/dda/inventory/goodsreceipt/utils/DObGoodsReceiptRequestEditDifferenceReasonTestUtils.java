package com.ebs.dda.inventory.goodsreceipt.utils;

import java.util.Map;

public class DObGoodsReceiptRequestEditDifferenceReasonTestUtils
    extends DObGoodsReceiptCommonTestUtils {

  public DObGoodsReceiptRequestEditDifferenceReasonTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append(",")
        .append("db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");

    return str.toString();
  }
}
