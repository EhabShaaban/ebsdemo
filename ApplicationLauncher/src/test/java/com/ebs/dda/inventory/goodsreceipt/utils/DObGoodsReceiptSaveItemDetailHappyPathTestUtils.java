package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemBatches;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderItemQuantities;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnItemQuantities;
import cucumber.api.DataTable;

import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObGoodsReceiptSaveItemDetailHappyPathTestUtils extends IObGoodsReceiptItemTestUtils {

    public static final String BATCH = "Batch";
    public static final String GR_PURCHASE_ORDER_TYPE = "PURCHASE_ORDER";

    public DObGoodsReceiptSaveItemDetailHappyPathTestUtils(Map<String, Object> properties) {
        super(properties);
    }

    public void assertThatItemDetailIsUpdated(
            String detailType, String goodsReceiptType, DataTable detailDataTable) {

        Map<String, String> detailMap = detailDataTable.asMaps(String.class, String.class).get(0);

        if (goodsReceiptType.equals(GR_PURCHASE_ORDER_TYPE)) {
            if (detailType.equals(BATCH)) {
                IObGoodsReceiptItemBatches batch =
                        (IObGoodsReceiptItemBatches)
                                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                        "getItemBatchById", constructBatchedItemDetailQueryParams(detailMap));

                assertNotNull(
                        detailType
                                + " with UOE "
                                + detailMap.get(IFeatureFileCommonKeys.UOE)
                                + " and Qty of "
                                + detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE)
                                + " and StockType "
                                + detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE)
                                + " was not found!",
                        batch);
            } else {
                IObGoodsReceiptPurchaseOrderItemQuantities quantity =
                        (IObGoodsReceiptPurchaseOrderItemQuantities)
                                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                        "getGoodsReceiptPurchaseOrderItemQuantity",
                                        constructUpdatedItemDetailQueryParams(detailMap));

                assertNotNull(
                        detailType
                                + " with UOE "
                                + detailMap.get(IFeatureFileCommonKeys.UOE)
                                + " and Qty of "
                                + detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE)
                                + " and StockType "
                                + detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE)
                                + " was not found!",
                        quantity);
            }
        } else {
            IObGoodsReceiptSalesReturnItemQuantities quantity =
                    (IObGoodsReceiptSalesReturnItemQuantities)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "getGoodsReceiptSalesReturnItemQuantity",
                                    constructUpdatedItemDetailQueryParams(detailMap));
            assertNotNull(
                    detailType
                            + " with UOE "
                            + detailMap.get(IFeatureFileCommonKeys.UOE)
                            + " and Qty of "
                            + detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE)
                            + " and StockType "
                            + detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE)
                            + " was not found!",
                    quantity);
        }
    }

    private Object[] constructUpdatedItemDetailQueryParams(Map<String, String> detailMap) {
        return new Object[]{
                detailMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                detailMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
                detailMap.get(IFeatureFileCommonKeys.UOE),
                detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
                detailMap.get(IGoodsReceiptFeatureFileCommonKeys.NOTES)
        };
    }

    private Object[] constructBatchedItemDetailQueryParams(Map<String, String> detailMap) {
        return new Object[]{
                detailMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                detailMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                detailMap.get(IFeatureFileCommonKeys.BATCH_NUMBER),
                detailMap.get(IFeatureFileCommonKeys.PRODUCTION_DATE),
                detailMap.get(IFeatureFileCommonKeys.EXPIRATION_DATE),
                detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
                detailMap.get(IFeatureFileCommonKeys.UOE),
                detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
                detailMap.get(IGoodsReceiptFeatureFileCommonKeys.NOTES)
        };
    }
}
