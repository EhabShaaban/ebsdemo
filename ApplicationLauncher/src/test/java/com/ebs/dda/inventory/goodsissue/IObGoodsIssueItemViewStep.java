package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.IObGoodsIssueItemViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObGoodsIssueItemViewStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private IObGoodsIssueItemViewTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new IObGoodsIssueItemViewTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public IObGoodsIssueItemViewStep() {

        Given(
                "^the following ItemData for GoodsIssues exist:$",
                (DataTable itemsDataTable) -> {
                    utils.assertThatGoodsIssueHasItems(itemsDataTable);
                });

        When(
        "^\"([^\"]*)\" requests to view Item section of GoodsIssue with code \"([^\"]*)\"$",
        (String userName, String goodsIssueCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getGoodsIssueItemsViewDataUrl(goodsIssueCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

        Then(
                "^the following values of Item section for GoodsIssue with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
                (String goodsIssueCode, String userName, DataTable itemsDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatGoodsIssueItemsMatchesResponse(itemsDataTable, response);
                });
  }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View GI ItemData section -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View GI ItemData section -")) {
            databaseConnector.closeConnection();
        }
    }
}
