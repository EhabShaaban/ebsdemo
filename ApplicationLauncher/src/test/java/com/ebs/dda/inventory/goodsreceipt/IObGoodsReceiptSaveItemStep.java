package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptSaveItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObGoodsReceiptSaveItemStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private IObGoodsReceiptSaveItemTestUtils goodRecieptSaveEditItemTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodRecieptSaveEditItemTestUtils = new IObGoodsReceiptSaveItemTestUtils(getProperties());
        goodRecieptSaveEditItemTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }

    public IObGoodsReceiptSaveItemStep() {

        And(
                "^ReceivedItems of GR with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
                (String goodsReceiptCode, String userName, String lockTime) -> {
                    goodRecieptSaveEditItemTestUtils.freeze(lockTime);
                    Response response =
                            userActionsTestUtils.lockSection(
                                    userName,
                                    IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM + goodsReceiptCode,
                                    goodsReceiptCode);
                    userActionsTestUtils.setUserResponse(userName, response);

                    goodRecieptSaveEditItemTestUtils.assertResponseSuccessStatus(response);
                });

        Then(
                "^GR with code \"([^\"]*)\" is updated as follows:$",
                (String goodsReceiptCode, DataTable goodsReceiptExpectedData) -> {
                    goodRecieptSaveEditItemTestUtils.assertThatGoodsReceiptIsUpdated(
                            goodsReceiptCode, goodsReceiptExpectedData);
                });

        Then(
                "^a new item is added to ReceivedItems of GR with code \"([^\"]*)\" as follows:$",
                (String goodsReceiptCode, DataTable itemHeaderTable) -> {
                    goodRecieptSaveEditItemTestUtils.assertSavedItemHeaderDataInGoodsReceiptIsCorrect(goodsReceiptCode,
                            itemHeaderTable);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Save GR Item HP -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(
                        goodRecieptSaveEditItemTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(goodRecieptSaveEditItemTestUtils.getDbScriptPath());
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "Save GR Item HP -")) {
            userActionsTestUtils.unlockSectionForAllUsers(IDObGoodsReceiptRestUrls.CANCEL_REQUEST_ADD_ITEM);
            goodRecieptSaveEditItemTestUtils.unfreeze();
            databaseConnector.closeConnection();
        }
    }
}
