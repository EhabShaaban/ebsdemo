package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptItemDetailValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class IObGoodsReceiptItemTestUtils extends DObGoodsReceiptCommonTestUtils {
  public static final String BATCH = "Batch";

  public final String ITEM_CODE = "ItemCode";

  public IObGoodsReceiptItemTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    updateRestURLs();
  }

  public void assertGoodReceiptHasItemCodes(String goodReceiptCode, DataTable itemsDataTable)
      throws Exception {
    List<Map<String, String>> itemCodes = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> itemCodeMap : itemCodes) {
      List<Map<String, Object>> goodsReceiptItem =
          getGoodsReceiptItem(goodReceiptCode, itemCodeMap.get(ITEM_CODE));
      Assert.assertFalse(goodsReceiptItem.isEmpty());
    }
  }

  private List<Map<String, Object>> getGoodsReceiptItem(String goodsReceiptCode, String itemCode)
      throws Exception {
    String query =
        "SELECT * FROM iobgoodsreceiptreceiveditemsgeneralmodel  WHERE "
            + "goodsreceiptcode = ? and itemcode = ?";

    return getDatabaseRecords(query, goodsReceiptCode, itemCode);
  }

  public void assertThatGoodsReceiptHasItemsAndQuantities(
      DataTable goodsReceiptWithQuantitiesTable) {
    for (int i = 0; i < goodsReceiptWithQuantitiesTable.raw().size() - 1; i++) {
      Map<String, String> expectedMap =
          goodsReceiptWithQuantitiesTable.asMaps(String.class, String.class).get(i);
      Object goodsReceipt =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getGoodsReceiptItemQuantity", constructQtyQueryParams(expectedMap));
      assertNotNull(
          goodsReceipt,
          "GR With Code "
              + expectedMap.get(IFeatureFileCommonKeys.CODE)
              + " and Qty ID "
              + Long.parseLong(expectedMap.get(IFeatureFileCommonKeys.QUANTITY_ID))
              + " Doesn't exist");
    }
  }

  public void assertThatGoodsReceiptHasItemsAndBatches(DataTable goodsReceiptWithBatchesTable) {
    for (int i = 0; i < goodsReceiptWithBatchesTable.raw().size() - 1; i++) {
      Map<String, String> expectedMap =
          goodsReceiptWithBatchesTable.asMaps(String.class, String.class).get(i);

      String goodsReceiptCode = expectedMap.get(IFeatureFileCommonKeys.CODE);
      String itemCode = expectedMap.get(IFeatureFileCommonKeys.ITEM_CODE);
      Long detailId = Long.parseLong(expectedMap.get(IFeatureFileCommonKeys.BATCH_ID));

      Object goodsReceipt =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getGoodsReceiptItemBatch", goodsReceiptCode, itemCode, detailId);

      assertNotNull(goodsReceipt);
    }
  }

  public void assertThatGoodsReceiptItemIsUpdated(
      String goodsReceiptCode, String itemCode, DataTable modificationDataTable) {

    Map<String, String> expectedMap =
        modificationDataTable.asMaps(String.class, String.class).get(0);

    String lastUpdatedDate = expectedMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE);
    String lastUpdatedBy = expectedMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY);

    IObGoodsReceiptReceivedItemsGeneralModel receivedItem =
        (IObGoodsReceiptReceivedItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptItemModificationDetails",
                goodsReceiptCode,
                itemCode,
                lastUpdatedBy);
    assertNotNull(receivedItem);
    assertDateEquals(lastUpdatedDate, receivedItem.getModifiedDate());
  }

  public Response saveItemDetail(
      Cookie cookie, String postfixUrl, DataTable detailDataTable, String detailType) {
    JsonObject valueObject = constructDetailValueObject(detailDataTable, detailType);
    return sendPUTRequest(cookie, postfixUrl, valueObject);
  }

  private Object[] constructQtyQueryParams(Map<String, String> expectedMap) {
    String goodsReceiptCode = expectedMap.get(IFeatureFileCommonKeys.CODE);
    String item = expectedMap.get(IFeatureFileCommonKeys.ITEM);
    Long detailId = Long.parseLong(expectedMap.get(IFeatureFileCommonKeys.QUANTITY_ID));

    return new Object[] {goodsReceiptCode, item, detailId};
  }

  private JsonObject constructDetailValueObject(DataTable detailDataTable, String detailType) {
    Map<String, String> detailMap = detailDataTable.asMaps(String.class, String.class).get(0);
    JsonObject valueObject = new JsonObject();

    if (detailType.equals(BATCH)) {
      valueObject.addProperty(
          IIObGoodsReceiptItemDetailValueObject.BATCH_CODE,
          detailMap.get(IFeatureFileCommonKeys.BATCH_NUMBER));
      valueObject.addProperty(
          IIObGoodsReceiptItemDetailValueObject.PRODUCTION_DATE,
          detailMap.get(IFeatureFileCommonKeys.PRODUCTION_DATE));
      valueObject.addProperty(
          IIObGoodsReceiptItemDetailValueObject.EXPIRATION_DATE,
          detailMap.get(IFeatureFileCommonKeys.EXPIRATION_DATE));
    }

    try {
      valueObject.addProperty(
          IIObGoodsReceiptItemDetailValueObject.RECEIVED_QTY_UOE,
          detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE).isEmpty()
              ? null
              : getBigDecimal(detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE)));
    } catch (NumberFormatException exception) {
      valueObject.addProperty(
          IIObGoodsReceiptItemDetailValueObject.RECEIVED_QTY_UOE,
          detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE).isEmpty()
              ? null
              : detailMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE));
    }
    valueObject.addProperty(
        IIObGoodsReceiptItemDetailValueObject.UOE_CODE, detailMap.get(IFeatureFileCommonKeys.UOE));

    valueObject.addProperty(
        IIObGoodsReceiptItemDetailValueObject.STOCK_TYPE,
        detailMap.get(IFeatureFileCommonKeys.STOCK_TYPE));
    valueObject.addProperty(
        IIObGoodsReceiptItemDetailValueObject.NOTES,
        detailMap.get(IGoodsReceiptFeatureFileCommonKeys.NOTES));

    return valueObject;
  }
}
