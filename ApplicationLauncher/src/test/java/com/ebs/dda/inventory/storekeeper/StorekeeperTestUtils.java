package com.ebs.dda.inventory.storekeeper;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.StoreKeeper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StorekeeperTestUtils {

  private EntityManagerDatabaseConnector entityManagerDatabaseConnector;
  private String STOREKEEPER = "storekeeperCode";

  public StorekeeperTestUtils(EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    this.entityManagerDatabaseConnector = entityManagerDatabaseConnector;
  }

  public void assertThatStorekeepersExist(DataTable storekeepersData) {
    List<Map<String, String>> storekeepers = storekeepersData.asMaps(String.class, String.class);
    for (Map<String, String> storekeeper : storekeepers) {

      String storekeeperCode = storekeeper.get(IFeatureFileCommonKeys.CODE);
      String storekeeperEnglishName = storekeeper.get(IFeatureFileCommonKeys.NAME);

      StoreKeeper actualStorekeeper =
          (StoreKeeper)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getStorekeeperByCodeAndName", storekeeperCode, storekeeperEnglishName);

      assertNotNull(actualStorekeeper);
    }
  }

  public void assertThatStoreKeeperDoesNotExist(Response response, String errorCode) {

    HashMap<String, Object> errors =
        response.body().jsonPath().get(CommonKeys.ERRORS_VALIDATION_RESULT);
    String storekeeperCode = (String) errors.get(STOREKEEPER);
    assertEquals(errorCode, storekeeperCode);
  }
}
