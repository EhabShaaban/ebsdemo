package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostAllowedActionsObjectScreenTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostConvertToActualTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostDetailsViewTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostFactorItemsViewTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostGeneralDataViewTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceBusinessPartnerTestUtils;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueViewGeneralDataTestUtils;
import com.ebs.dda.inventory.goodsissue.utils.IObGoodsIssueItemViewTestUtils;
import com.ebs.dda.inventory.goodsissue.utils.IObGoodsIssueViewSalesOrderTestUtils;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptActivateTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptViewItemsTestUtils;
import com.ebs.dda.inventory.storetransaction.utils.TObStoreTransactionCreateTestUtils;
import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderCommonTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderCommonTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewReturnDetailsTestUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Arrays;
import java.util.Map;

public class DObGoodsReceiptActivateStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private final String GR_SO_FEATURE_FILE_NAME = "GR_SRO_Activate.feature";
  private DObGoodsReceiptActivateTestUtils utils;
  private IObGoodsReceiptViewItemsTestUtils goodsReceiptViewItemsTestUtils;
  private TObStoreTransactionCreateTestUtils tObStoreTransactionCreateTestUtils;
  private IObSalesReturnOrderViewReturnDetailsTestUtils salesReturnOrderViewReturnDetailsTestUtils;
  private IObSalesInvoiceBusinessPartnerTestUtils iObSalesInvoiceBusinessPartnerTestUtils;
  private DObSalesOrderCommonTestUtils salesOrderCommonTestUtils;
  private IObGoodsIssueViewSalesOrderTestUtils goodsIssueViewSalesOrderTestUtils;
  private DObGoodsIssueViewGeneralDataTestUtils goodsIssueViewGeneralDataTestUtils;
  private IObGoodsIssueItemViewTestUtils goodsIssueItemViewTestUtils;
  private DObSalesReturnOrderCommonTestUtils salesReturnOrderCommonTestUtils;
  private DObLandedCostAllowedActionsObjectScreenTestUtils
      landedCostAllowedActionsObjectScreenTestUtils;
  private DObLandedCostDetailsViewTestUtils landedCostDetailsUtils;
  private DObLandedCostConvertToActualTestUtils landedCostConvertToActualTestUtils;
  private DObLandedCostGeneralDataViewTestUtils landedCostUtils;
  private DObLandedCostFactorItemsViewTestUtils landedCostFactorItemsViewTestUtils;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
  private DObSalesInvoiceActivateTestUtils salesInvoicePostTestUtils;

  public DObGoodsReceiptActivateStep() {

    Given(
        "^the following ReturnDetails for SalesReturnOrder exists:$",
        (DataTable returnDetailsTable) -> {
          salesReturnOrderViewReturnDetailsTestUtils.assertThatSalesReturnOrderReturnDetailsExists(
              returnDetailsTable);
        });

    Given(
        "^the following businessPartnerData for salesInvoics exist:$",
        (DataTable businessPartnerTable) -> {
          iObSalesInvoiceBusinessPartnerTestUtils.assertBusinessPartnerExists(businessPartnerTable);
        });

    Given(
        "^the following SalesOrders exist:$",
        (DataTable salesOrderDataTable) -> {
          salesOrderCommonTestUtils.assertThatSalesOrdersExist(salesOrderDataTable);
        });

    Given(
        "^the following SalesOrderData for GoodsIssues exist:$",
        (DataTable salesOrderDataTable) -> {
          goodsIssueViewSalesOrderTestUtils.assertThatGoodsIssueHasSalesOrder(salesOrderDataTable);
        });

    Given(
        "^the following GeneralData for GoodsIssues exist:$",
        (DataTable generalDataTable) -> {
          goodsIssueViewGeneralDataTestUtils.assertThatTheFollowingGIGeneralDataExist(
              generalDataTable);
        });

    Given(
        "^the following ItemData for GoodsIssues exist:$",
        (DataTable itemsDataTable) -> {
          goodsIssueItemViewTestUtils.assertThatGoodsIssueHasItems(itemsDataTable);
        });

    Given(
        "^ONLY the following StoreTransaction records for RefDocument \"([^\"]*)\" And Item \"([^\"]*)\" with UOM \"([^\"]*)\" in company \"([^\"]*)\", in storehouse \"([^\"]*)\", for BusinessUnit \"([^\"]*)\" exist:$",
        (String refDocument,
            String itemCode,
            String unitOfMeasureCode,
            String companyCode,
            String storeHouseCode,
            String businessUnitCode,
            DataTable storeTransactionsDataTable) -> {
          utils.assertThatStoreTransactionsExistWithRefDocument(
              refDocument,
              itemCode,
              unitOfMeasureCode,
              companyCode,
              storeHouseCode,
              businessUnitCode,
              storeTransactionsDataTable);
        });

    Given(
        "^the following ADD \\(Reference\\) StoreTransactions Exist:$",
        (DataTable storeTransactionsDataTable) -> {
          utils.assertThatStoreTransactionExist(storeTransactionsDataTable);
        });

    Given(
        "^Item \"([^\"]*)\" in GoodsReceipt \\(Based On PurchaseOrder\\) \"([^\"]*)\" has the following received quantities:$",
        (String item, String goodsReceiptCode, DataTable ordinaryItemQuantities) -> {
          goodsReceiptViewItemsTestUtils.assertGoodsReceiptPurchaseOrderItemQuantitiesExist(
              goodsReceiptCode, item, ordinaryItemQuantities);
        });

    Given(
        "^Item \"([^\"]*)\" in GoodsReceipt \\(Based On Sales Return\\) \"([^\"]*)\" has the following received quantities:$",
        (String itemCode, String goodsReceiptCode, DataTable ordinaryItemQuantities) -> {
          goodsReceiptViewItemsTestUtils.assertGoodsReceiptSalesReturnItemQuantitiesExist(
              goodsReceiptCode, itemCode, ordinaryItemQuantities);
        });

    Given(
        "^Item \"([^\"]*)\" in GoodsReceipt \"([^\"]*)\" has the following batches:$",
        (String batchedItem, String goodsReceiptCode, DataTable itemBatches) -> {
          goodsReceiptViewItemsTestUtils.assertGoodsReceiptItemBatchesExist(
              goodsReceiptCode, batchedItem, itemBatches);
        });
    Given(
        "^GoodsReceipt with code \"([^\"]*)\" has empty received items list$",
        (String goodsReceiptCode) -> {
          utils.assertThatGoodsReceiptHasEmptyItemsList(goodsReceiptCode);
        });

    Given(
        "^first \"([^\"]*)\" activated GR with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String goodsReceiptCode, String activationDate) -> {
          utils.freeze(activationDate);
          String url = DObGoodsReceiptActivateTestUtils.getActivateUrl(goodsReceiptCode);
          Response response =
              utils.sendActivateGoodsReceiptRequest(
                  url, userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" of GR with Code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String username, String sectionName, String goodsReceiptCode, String editTime) -> {
          Map<String, String> sectionsLockUrlsMap = utils.getSectionsLockUrlsMap();
          String lockSectionUrl = sectionsLockUrlsMap.get(sectionName) + goodsReceiptCode;
          utils.freeze(editTime);
          Response lockRepponse =
              userActionsTestUtils.lockSection(username, lockSectionUrl, goodsReceiptCode);
          utils.assertResponseSuccessStatus(lockRepponse);
        });
    And(
        "^GoodsReceipt with code \"([^\"]*)\" has the following received items:$",
        (String goodsReceiptCode, DataTable receivedItems) -> {
          goodsReceiptViewItemsTestUtils.assertGoodsReceiptItemsExist(
              goodsReceiptCode, receivedItems);
        });
    When(
        "^\"([^\"]*)\" requests to activate GR with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String goodsReceiptCode, String activationDate) -> {
          utils.freeze(activationDate);
          String url = DObGoodsReceiptActivateTestUtils.getActivateUrl(goodsReceiptCode);
          Response response =
              utils.sendActivateGoodsReceiptRequest(
                  url, userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^GR with Code \"([^\"]*)\" is updated as follows:$",
        (String goodsReceiptCode, DataTable expectedGoodsReceipt) -> {
          utils.assertThatGoodsReceiptIsUpdatedAfterActivate(
              goodsReceiptCode, expectedGoodsReceipt);
        });

    Then(
        "^the following missing fields in section HeaderData are sent to \"([^\"]*)\":$",
        (String username, DataTable missingFields) -> {
          utils.assertThatSectionMissingFieldsExistInResponse(
              userActionsTestUtils.getUserResponse(username),
              IGoodsReceiptSectionNames.HEADER_SECTION,
              missingFields);
        });

    Then(
        "^the following missing fields in section ItemsData in Item with code \"([^\"]*)\" are sent to \"([^\"]*)\":$",
        (String itemCode, String username, DataTable missingFields) -> {
          utils.assertThatItemsSectionMissingFieldsExistInResponse(
              userActionsTestUtils.getUserResponse(username), itemCode, missingFields);
        });

    Then(
        "^the following missing fields in section ItemsData are sent to \"([^\"]*)\":$",
        (String username, DataTable missingFields) -> {
          utils.assertThatItemsSectionMissingFieldsHasEmptyItemsList(
              userActionsTestUtils.getUserResponse(username), missingFields);
        });

    Given(
        "^the following Import PurchaseOrders exist with values:$",
        (DataTable purchaseOrderTable) -> {
          utils.assertThatPurchaseOrdersExist(purchaseOrderTable);
        });

    Given(
        "^the following Local PurchaseOrders exist with values:$",
        (DataTable purchaseOrderTable) -> {
          utils.assertThatPurchaseOrdersExist(purchaseOrderTable);
        });

    Given(
        "^the following POData exists for GoodsReceipts:$",
        (DataTable goodsReceiptPurchaseOrderDataTable) -> {
          utils.assertThatPODataExistForGoodsReceipt(goodsReceiptPurchaseOrderDataTable);
        });

    Then(
        "^GoodsState of PurchaseOrder with Code \"([^\"]*)\" is changed to Received$",
        (String poCode) -> {
          utils.assertThatpurchaseorderIsUpdatedAfterGoodsReceiptChangedToActive(poCode);
        });

    Given(
        "^last created Costing is \"([^\"]*)\"$",
        (String costingCode) -> {
          utils.createLastEntityCode(DObCosting.class.getSimpleName(), costingCode);
        });

    Then(
        "^Costing is created as follows:$",
        (DataTable costingDataTable) -> {
          utils.assertThatCostingCreatedAfterGoodsReceiptChangedToActive(costingDataTable);
        });

    Then(
        "^CostingDetails section in Costing with code \"([^\"]*)\" is created as follows:$",
        (String grAccountingDocumentCode, DataTable GoodsReceiptAccountingDocumentDetails) -> {
          utils.assertThatCostingDetailsCreated(
              grAccountingDocumentCode, GoodsReceiptAccountingDocumentDetails);
        });

    Then(
        "^CostingItems section in Costing with code \"([^\"]*)\" is created as follows:$",
        (String costingCode, DataTable costingItems) -> {
          utils.assertThatCostingItemsCreated(costingCode, costingItems);
        });

    Then(
        "^the following values of AccountingDetails section for Costing with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String grAccountingDocumentCode,
            String userName,
            DataTable accountingDetailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatCostingDetailsCreatedAfterGoodsReceiptChangedToActive(
              grAccountingDocumentCode, accountingDetailsDataTable);
        });

    Given(
        "^Last created store transaction was with code \"([^\"]*)\"$",
        (String storeTransactionCode) -> {
          tObStoreTransactionCreateTestUtils.assertStoreTransactionExistsWithCode(
              storeTransactionCode);
        });
    Given(
        "^Last created store transaction is with code \"([^\"]*)\"$",
        (String storeTransactionCode) -> {
          tObStoreTransactionCreateTestUtils.assertStoreTransactionExistsWithCode(
              storeTransactionCode);
        });
    Then(
        "^new StoreTransactions created as follows:$",
        (DataTable storeTransactionDataTable) -> {
          tObStoreTransactionCreateTestUtils.assertStoreTransactionsAreCreated(
              storeTransactionDataTable);
        });

    Then(
        "^The State of SalesReturnOrder with code \"([^\"]*)\" is changed to \"([^\"]*)\"$",
        (String salesReturnOrderCode, String state) -> {
          salesReturnOrderCommonTestUtils.assertThatSalesOrderStateIsUpdated(
              salesReturnOrderCode, state);
        });

    Given(
        "^the following LandedCosts exist:$",
        (DataTable landedCostTable) -> {
          landedCostAllowedActionsObjectScreenTestUtils.assertLandedCostExists(landedCostTable);
        });

    Given(
        "^the following LandedCostDetails for LandedCost exist:$",
        (DataTable landedCostDetailsDataTable) -> {
          landedCostDetailsUtils.checkLandedCostDetailsExists(landedCostDetailsDataTable);
        });
    Then(
        "^the items single unit final cost with damaged items cost are exist as following:$",
        (DataTable itemsCostDT) -> {
          landedCostConvertToActualTestUtils.assertThatItemsTotalCostAreExist(itemsCostDT);
        });
    Given(
        "^the following GeneralData for LandedCosts exist:$",
        (DataTable generalDataTable) -> {
          landedCostUtils.checkLandedCostGeneralDataExists(generalDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    utils = new DObGoodsReceiptActivateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    goodsReceiptViewItemsTestUtils = new IObGoodsReceiptViewItemsTestUtils(getProperties());
    goodsReceiptViewItemsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    tObStoreTransactionCreateTestUtils = new TObStoreTransactionCreateTestUtils(getProperties());
    tObStoreTransactionCreateTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    salesReturnOrderViewReturnDetailsTestUtils =
        new IObSalesReturnOrderViewReturnDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    iObSalesInvoiceBusinessPartnerTestUtils =
        new IObSalesInvoiceBusinessPartnerTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    salesOrderCommonTestUtils = new DObSalesOrderCommonTestUtils(getProperties());
    salesOrderCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    goodsIssueViewSalesOrderTestUtils = new IObGoodsIssueViewSalesOrderTestUtils(getProperties());
    goodsIssueViewSalesOrderTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    goodsIssueViewGeneralDataTestUtils = new DObGoodsIssueViewGeneralDataTestUtils(getProperties());
    goodsIssueViewGeneralDataTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    goodsIssueItemViewTestUtils = new IObGoodsIssueItemViewTestUtils(getProperties());
    goodsIssueItemViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    salesReturnOrderCommonTestUtils =
        new DObSalesReturnOrderCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
    landedCostAllowedActionsObjectScreenTestUtils =
        new DObLandedCostAllowedActionsObjectScreenTestUtils(getProperties());
    landedCostAllowedActionsObjectScreenTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    landedCostDetailsUtils = new DObLandedCostDetailsViewTestUtils(getProperties());
    landedCostDetailsUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    landedCostConvertToActualTestUtils = new DObLandedCostConvertToActualTestUtils(getProperties());
    landedCostConvertToActualTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    landedCostUtils = new DObLandedCostGeneralDataViewTestUtils(getProperties());
    landedCostUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    landedCostFactorItemsViewTestUtils = new DObLandedCostFactorItemsViewTestUtils(getProperties());
    landedCostFactorItemsViewTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    journalEntryViewAllTestUtils =
        new DObJournalEntryViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
    salesInvoicePostTestUtils = new DObSalesInvoiceActivateTestUtils(getProperties());
    salesInvoicePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate GR -")
        || contains(scenario.getName(), "Activate GR Val -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());

        if (scenario.getId().contains(GR_SO_FEATURE_FILE_NAME)) {
          hasBeenExecutedOnce = true;
        }
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    databaseConnector.closeConnection();
    utils.unfreeze();
    if (contains(scenario.getName(), "Activate GR -")) {
      userActionsTestUtils.unlockAllSections(
          IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
          Arrays.asList(
              IPurchaseOrderSectionNames.HEADER_SECTION, IPurchaseOrderSectionNames.ITEMS_SECTION));
    }
  }
}
