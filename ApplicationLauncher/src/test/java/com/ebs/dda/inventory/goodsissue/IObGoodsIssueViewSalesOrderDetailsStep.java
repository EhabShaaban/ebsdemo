package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.IObGoodsIssueViewSalesOrderTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObGoodsIssueViewSalesOrderDetailsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObGoodsIssueViewSalesOrderTestUtils goodsIssueViewSalesOrderTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    goodsIssueViewSalesOrderTestUtils = new IObGoodsIssueViewSalesOrderTestUtils(properties);
    goodsIssueViewSalesOrderTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  public IObGoodsIssueViewSalesOrderDetailsStep() {

    Given(
        "^the following SalesOrderData for GoodsIssues exist:$",
        (DataTable salesOrderDataTable) -> {
          goodsIssueViewSalesOrderTestUtils.assertThatGoodsIssueHasSalesOrder(salesOrderDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view SalesOrder section of GoodsIssue with code \"([^\"]*)\"$",
        (String userName, String giCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              goodsIssueViewSalesOrderTestUtils.sendGETRequest(
                  cookie, goodsIssueViewSalesOrderTestUtils.getViewRefDocumentDataUrl(giCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of SalesOrder section for GoodsIssue with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String giCode, String userName, DataTable consigneeDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          goodsIssueViewSalesOrderTestUtils.assertThatViewRefDocumentDataResponseIsCorrect(
              response, consigneeDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GI SalesOrder section -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {

        databaseConnector.executeSQLScript(
            goodsIssueViewSalesOrderTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View GI SalesOrder section -")) {
      databaseConnector.closeConnection();
    }
  }
}
