package com.ebs.dda.inventory.goodsreceipt.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/inventory/goods-receipt/GR_RequestAdd_Item.feature",
      "classpath:features/inventory/goods-receipt/GR_Delete_Item.feature",
      "classpath:features/inventory/goods-receipt/GR_Delete_ItemDetail.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_NewItemDetail_Auth.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_NewItemDetail_HP.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_NewItemDetail_Val.feature",
      "classpath:features/inventory/goods-receipt/GR_View_Items.feature",
      "classpath:features/inventory/goods-receipt/GR_RequestAdd_NewItemDetail.feature",
      "classpath:features/inventory/goods-receipt/GR_RequestEdit_ItemDetail.feature",
      "classpath:features/inventory/goods-receipt/GR_RequestEdit_ItemDifferenceReason.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_ItemDifferenceReason.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_NewItem_Auth.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_NewItem_HP.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_NewItem_Val.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_ExistingItemDetail_HP.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_ExistingItemDetail_Val.feature",
      "classpath:features/inventory/goods-receipt/GR_Save_ExistingItemDetail_Auth.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.inventory.goodsreceipt"},
    strict = true,
    tags = "not @Future")
public class DObGoodsReceiptItemsCucumberRunner {}
