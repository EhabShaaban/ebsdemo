package com.ebs.dda.inventory.goodsissue.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
            "classpath:features/inventory/goods-issue/GI_Activate_HP.feature",
            "classpath:features/inventory/goods-issue/GI_Activate_Val.feature",
            "classpath:features/inventory/goods-issue/GI_Activate_Auth.feature",
            "classpath:features/inventory/goods-issue/GI_AllowedActions_Home.feature",
            "classpath:features/inventory/goods-issue/GI_AllowedActions_Object.feature",
            "classpath:features/inventory/goods-issue/GI_View_SalesInvoice_Section.feature",
            "classpath:features/inventory/goods-issue/GI_View_SalesOrder_Section.feature",
            "classpath:features/inventory/goods-issue/GI_Create_HP.feature",
            "classpath:features/inventory/goods-issue/GI_Create_Val.feature",
            "classpath:features/inventory/goods-issue/GI_Create_Auth.feature",
            "classpath:features/inventory/goods-issue/GI_Delete.feature",
            "classpath:features/inventory/goods-issue/GI_View_General_Data.feature",
            "classpath:features/inventory/goods-issue/GI_View_Items.feature",
            "classpath:features/inventory/goods-issue/GI_View_Company.feature",
            "classpath:features/inventory/goods-issue/GI_ViewAll.feature",
            "classpath:features/inventory/goods-issue/GI_Type_Dropdown.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.inventory.goodsissue"},
    strict = true,
    tags = "not @Future")
public class DObGoodsIssueCucumberRunner {}
