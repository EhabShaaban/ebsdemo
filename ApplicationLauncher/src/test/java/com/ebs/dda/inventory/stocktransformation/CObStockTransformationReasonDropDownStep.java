package com.ebs.dda.inventory.stocktransformation;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.stocktransformation.utils.CObStockTransformationReasonDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class CObStockTransformationReasonDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private CObStockTransformationReasonDropDownTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils =
                new CObStockTransformationReasonDropDownTestUtils(
                        properties, entityManagerDatabaseConnector);
    }

  public CObStockTransformationReasonDropDownStep() {
    Given(
        "^the total number of existing StockTransformationReasons are (\\d+)$",
        (Integer recordsNumber) -> {
          utils.assertThatAllStockTransformationReasonsExist(recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read all StockTransformationReason based on StockTransformationType with code \"([^\"]*)\" from \"([^\"]*)\" to \"([^\"]*)\"$",
        (String userName,
            String transformationTypeCode,
            String fromStockType,
            String toStockType) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(
                  cookie,
                  utils.readAllStockTransformationReasons(
                      transformationTypeCode, fromStockType, toStockType));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following StockTransformationReason values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable stockTransformationReasonsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseMatchesExpected(response, stockTransformationReasonsDataTable);
        });

    Then(
        "^total number of StockTransformationReason returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read list of StockTransformationReason")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Read list of StockTransformationReason")) {
      databaseConnector.closeConnection();
    }
  }
}
