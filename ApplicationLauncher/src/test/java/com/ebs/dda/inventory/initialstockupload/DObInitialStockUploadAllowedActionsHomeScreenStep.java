package com.ebs.dda.inventory.initialstockupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.initialstockupload.utils.DObInitialStockUploadCommonTestUtils;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObInitialStockUploadAllowedActionsHomeScreenStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObInitialStockUploadCommonTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitialStockUploadCommonTestUtils();
    utils.setProperties(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObInitialStockUploadAllowedActionsHomeScreenStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of InitialStockUpload home screen$",
        (String userName) -> {
          String readUrl = IDObInitialStockUploadRestURLS.AUTHORIZED_ACTIONS;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String allowedActionsDataTable, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActionsDataTable);
        });
  }
}
