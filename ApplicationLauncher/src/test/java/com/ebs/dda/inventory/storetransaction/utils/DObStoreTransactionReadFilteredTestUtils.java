package com.ebs.dda.inventory.storetransaction.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.inventory.storetransaction.ITObStoreTransactionGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DObStoreTransactionReadFilteredTestUtils extends CommonTestUtils {

  public DObStoreTransactionReadFilteredTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertThatStoreTransactionsArePresentedToUser(
      Response response, DataTable storeTransactionsDataTable) throws Exception {
    List<HashMap<String, Object>> actualStoreTransactions = getListOfMapsFromResponse(response);
    List<Map<String, String>> expectedStoreTransactionsMaps =
        storeTransactionsDataTable.asMaps(String.class, String.class);
    for (int i = 0; i < actualStoreTransactions.size(); i++) {
      assertThatStoreTransactionMatchesTheExpected(
          expectedStoreTransactionsMaps.get(i), actualStoreTransactions.get(i));
    }
  }

  private void assertThatStoreTransactionMatchesTheExpected(
      Map<String, String> expectedStoreTransactionMap,
      HashMap<String, Object> actualStoreTransactionMap) {
    MapAssertion mapAssertion =
        new MapAssertion(expectedStoreTransactionMap, actualStoreTransactionMap);
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.STORE_TRANSACTION_CODE),
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.ST_CODE));
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.STOCK_TYPE));
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.PURCHASE_UNIT).split(" - ")[0],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.PURCHASEUNIT_CODE));
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.PURCHASE_UNIT).split(" - ")[1],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.PURCHASEUNIT_NAME_EN));
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.ITEM).split(" - ")[0],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.ITEM_CODE));
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.UOM).split(" - ")[0],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.UOM_CODE));
    mapAssertion.assertBigDecimalNumberValue(
        IFeatureFileCommonKeys.QTY, ITObStoreTransactionGeneralModel.QUANTITY);
    mapAssertion.assertBigDecimalNumberValue(
        IFeatureFileCommonKeys.ESTIMATE_COST, ITObStoreTransactionGeneralModel.ESTIMATE_COST);
    assertEquals(
        expectedStoreTransactionMap.get(IStoreTransactionFileCommonKeys.TRANSACTION_TYPE),
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.TRANSACTION_OPERATION));
    String expectedActualCost =
        expectedStoreTransactionMap.get(IStoreTransactionFileCommonKeys.ACTUAL_COST);
    if (expectedActualCost.equals("null")) {
      assertNull(actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.ACTUAL_COST));
    } else {
      mapAssertion.assertBigDecimalNumberValue(
          IStoreTransactionFileCommonKeys.ACTUAL_COST,
          ITObStoreTransactionGeneralModel.ACTUAL_COST);
    }
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT).split(" - ")[0],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.REF_DOCUMENT_CODE));
    mapAssertion.assertBigDecimalNumberValue(
        IStoreTransactionFileCommonKeys.REMAINING_QTY,
        ITObStoreTransactionGeneralModel.REMAINING_QUANTITY);
    assertEquals(
        convertEmptyStringToNull(
            expectedStoreTransactionMap.get(IStoreTransactionFileCommonKeys.REF_TRANSACTION)),
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.REF_TRANSACTION_CODE));
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.COMPANY).split(" - ")[0],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.COMPANY_CODE));

    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.PLANT).split(" - ")[0],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.PLANT_CODE));
    assertEquals(
        expectedStoreTransactionMap.get(IFeatureFileCommonKeys.STOREHOUSE).split(" - ")[0],
        actualStoreTransactionMap.get(ITObStoreTransactionGeneralModel.STOREHOUSE_CODE));
  }
}
