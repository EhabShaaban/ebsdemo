package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObCostingViewAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObCostingViewAccountingDetailsStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "View Costing AccountingDetails";
  private IObCostingViewAccountingDetailsTestUtils utils;

  public IObCostingViewAccountingDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view AccountingDetails section for GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          String readUrl = utils.getCostingDetailsViewDataUrl(goodsReceiptCode);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(userCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of AccountingDetails section for GoodsReceipt with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String grAccountingDocumentCode,
            String userName,
            DataTable accountingDetailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayed(response, accountingDetailsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObCostingViewAccountingDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(utils.clearCostingInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
