package com.ebs.dda.inventory.goodsissue.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObGoodsIssueCommonTestUtils extends CommonTestUtils {

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
      str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
      str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    return str.toString();
  }


  public void createGoodsIssueSalesOrderGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> giGeneralDataListMap =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> giGeneralDataMap : giGeneralDataListMap) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsIssueSalesOrderGeneralData",
          constructInsertGoodsIssueSalesOrderGeneralDataQueryParams(giGeneralDataMap));
    }
  }

  private Object[] constructInsertGoodsIssueSalesOrderGeneralDataQueryParams(Map<String, String> goodsIssueMap) {
    return new Object[] {
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
        goodsIssueMap.get(IFeatureFileCommonKeys.CREATION_DATE),
        goodsIssueMap.get(IFeatureFileCommonKeys.CREATION_DATE),
        goodsIssueMap.get(IFeatureFileCommonKeys.CREATED_BY),
        goodsIssueMap.get(IFeatureFileCommonKeys.CREATED_BY),
        goodsIssueMap.get(IFeatureFileCommonKeys.STATE),
        goodsIssueMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.TYPE)
    };
  }

  public void createGoodsIssueSalesOrderCompany(DataTable storeDataTable) {

    List<Map<String, String>> goodsIssueCompanyMapList =
        storeDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsIssueCompanyMap : goodsIssueCompanyMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsIssueSalesOrderCompany",
          constructInsertGoodsIssueSalesOrderCompanyQueryParameters(goodsIssueCompanyMap));
    }
  }

  private Object[] constructInsertGoodsIssueSalesOrderCompanyQueryParameters(Map<String, String> goodsIssueStoreData) {
    return new Object[] {
        goodsIssueStoreData.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
        goodsIssueStoreData.get(IInventoryFeatureFileCommonKeys.COMPANY),
        goodsIssueStoreData.get(IInventoryFeatureFileCommonKeys.PLANT),
        goodsIssueStoreData.get(IInventoryFeatureFileCommonKeys.STOREHOUSE),
        goodsIssueStoreData.get(IFeatureFileCommonKeys.STOREKEEPER)
    };
  }

  public void createGoodsIssueSalesOrderData(DataTable salesOrderDataTable) {
    List<Map<String, String>> giSalesOrderListMap =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> giSalesOrderDataMap : giSalesOrderListMap) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsIssueSalesOrderData",
          constructInsertGoodsIssueSalesOrderDataQueryParams(giSalesOrderDataMap));
    }
  }

  private Object[] constructInsertGoodsIssueSalesOrderDataQueryParams(Map<String, String> goodsIssueMap) {
    return new Object[] {
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.REF_DOCUMENT),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.CUSTOMER),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.ADDRESS),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.CONTACT_PERSON),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.SALES_REPRESENTATIVE),
        goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.COMMENTS)
    };
  }

  public void createGoodsIssueSalesOrderItem(DataTable itemsDataTable) {
    List<Map<String, String>> goodsIssueItemsData =
        itemsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsIssueItemData : goodsIssueItemsData) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createGoodsIssueSalesOrderItem", constructInsertGoodsIssueSalesOrderItemQueryParameters(goodsIssueItemData));
    }
  }

  private Object[] constructInsertGoodsIssueSalesOrderItemQueryParameters(Map<String, String> goodsIssueItemData) {

    return new Object[] {
        goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
        goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.ITEM),
        goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.UOM),
        goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.QUANTITY),
        goodsIssueItemData.get(IFeatureFileCommonKeys.BATCH_NUMBER).isEmpty()
            ? null
            : goodsIssueItemData.get(IFeatureFileCommonKeys.BATCH_NUMBER),
        goodsIssueItemData.get(IFeatureFileCommonKeys.PRICE).isEmpty()
            ? null
            : goodsIssueItemData.get(IFeatureFileCommonKeys.PRICE)
    };
  }

  public void assertGoodsIssuesExist(DataTable goodsIssueDataTable) {
    List<Map<String, String>> goodsIssues = goodsIssueDataTable.asMaps(String.class, String.class);

    for (Map<String, String> goodsIssue : goodsIssues) {
      assertGoodsIssueExists(goodsIssue);
    }
  }

  private void assertGoodsIssueExists(Map<String, String> goodsIssue) {
    String goodsIssueCode = goodsIssue.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE);
    String state = goodsIssue.get(IFeatureFileCommonKeys.STATE);
    String purchaseUnit = goodsIssue.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    Object[] queryParameters = getGoodsIssueQueryParameters(goodsIssueCode, state, purchaseUnit);
    DObGoodsIssueGeneralModel goodsIssueGeneralModel = getDObGoodsIssue(queryParameters);
    Assert.assertNotNull(goodsIssueGeneralModel);
  }

  private Object[] getGoodsIssueQueryParameters(
      String goodsIssueCode, String state, String purchaseUnit) {
    return new Object[] {goodsIssueCode, '%' + state + '%', purchaseUnit};
  }

  private DObGoodsIssueGeneralModel getDObGoodsIssue(Object[] parameters) {
    return (DObGoodsIssueGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getGoodsIssueByCodeStatePurchaseUnit", parameters);
  }

  public void assertThatGoodsIssueIsUpdatedByUserAtDate(
      String goodsIssueCode, DataTable expectedGoodsIssueDataTable) {
    Map<String, String> expectedGoodsIssue =
        expectedGoodsIssueDataTable.asMaps(String.class, String.class).get(0);
    DObGoodsIssueGeneralModel actualGoodsIssue =
        (DObGoodsIssueGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsIssueUpdatedByUserAtDate",
                goodsIssueCode,
                expectedGoodsIssue.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                "%" + expectedGoodsIssue.get(IFeatureFileCommonKeys.STATE) + "%");

    assertNotNull(
        actualGoodsIssue,
        "The updates in GI with code: " + goodsIssueCode + " didn't match expected!");
    assertDateEquals(
            expectedGoodsIssue.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            actualGoodsIssue.getModifiedDate());
  }

  public void assertThatSalesOrderStateIsUpdatedAsIssued(
      String salesOrderCode, String currentState) {

    Object actualSalesOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderByCodeAndCurrentState", salesOrderCode, "%" + currentState + "%");

    assertNotNull(
        actualSalesOrder,
        "The updates in SO with code: " + salesOrderCode + " didn't match expected!");
  }
}
