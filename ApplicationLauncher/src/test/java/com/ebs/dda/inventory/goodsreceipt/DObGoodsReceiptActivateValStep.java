package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptActivateValTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Arrays;

public class DObGoodsReceiptActivateValStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptActivateValTestUtils utils;
  private final String GR_SO_FEATURE_FILE_NAME = "GR_SRO_Activate_Val.feature";

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    utils = new DObGoodsReceiptActivateValTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObGoodsReceiptActivateValStep() {

    Given(
        "^Item \"([^\"]*)\" in GoodsReceipt \\(Based On SalesReturn\\) \"([^\"]*)\" has no received quantities:$",
        (String item, String goodsReceiptCode) -> {
        utils.assertGoodsReceiptSalesReturnItemHasNoQuantities(goodsReceiptCode,item);
        });
    Given(
        "^first \"([^\"]*)\" requests to add Item Quantity for Item with code \"([^\"]*)\" to Items section of GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String goodsReceiptCode, String datetime) -> {
          utils.freeze(datetime);
          String lockURL =
              IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate GR Val -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());

        if (scenario.getId().contains(GR_SO_FEATURE_FILE_NAME)) {
          hasBeenExecutedOnce = true;
        }
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate GR Val -")) {
      databaseConnector.closeConnection();
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
          Arrays.asList(IGoodsReceiptSectionNames.ITEMS_SECTION));
    }
  }
}
