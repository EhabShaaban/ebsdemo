package com.ebs.dda.inventory.stocktransformation.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.stocktransformation.IDObStockTransformationCreateValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static com.ebs.dda.inventory.stocktransformation.IDObStockTransformationRestURLS.CREATE_STOCK_TRANSFORMATION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObStockTransformationCreateTestUtils extends DObStockTransformationCommonTestUtils {

    public DObStockTransformationCreateTestUtils(Map<String, Object> properties) {
        setProperties(properties);
    }

    public void assertLastCreatedStockTransformation(String stockTransformationCode) {

        DObStockTransformationGeneralModel dObStockTransformation =
                (DObStockTransformationGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getLastCreatedStockTransformationByCode");
        assertEquals(stockTransformationCode, dObStockTransformation.getUserCode());
    }

    public Response createStockTransformation(DataTable stockTransformationDataTable, Cookie cookie) {
        JsonObject stockTransformationRequest =
                prepareStockTransformationJsonObject(stockTransformationDataTable);
        String restURL = CREATE_STOCK_TRANSFORMATION;
        return sendPostRequest(cookie, restURL, stockTransformationRequest);
    }

    private JsonObject prepareStockTransformationJsonObject(DataTable stockTransformationDataTable) {
        Map<String, String> stockTransformation =
                stockTransformationDataTable.asMaps(String.class, String.class).get(0);
        JsonObject stockTransformationJsonObject = new JsonObject();
        addValueToJsonObject(
                stockTransformationJsonObject,
                IDObStockTransformationCreateValueObject.TRANSFORMATION_TYPE_CODE,
                stockTransformation.get(IFeatureFileCommonKeys.TYPE));

        String purchaseUnitCode =
                stockTransformation.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(" - ")[0];
        addValueToJsonObject(
                stockTransformationJsonObject,
                IDObStockTransformationCreateValueObject.PURCHASE_UNIT_CODE,
                purchaseUnitCode);

        addValueToJsonObject(
                stockTransformationJsonObject,
                IDObStockTransformationCreateValueObject.STORE_TRANSACTION_CODE,
                stockTransformation.get(IInventoryFeatureFileCommonKeys.REF_STORE_TRANSACTION));

        try {

            stockTransformationJsonObject.addProperty(
                    IDObStockTransformationCreateValueObject.TRANSFORMED_QTY,
                    stockTransformation.get(IFeatureFileCommonKeys.QUANTITY) != null
                            ? Double.valueOf(stockTransformation.get(IFeatureFileCommonKeys.QUANTITY))
                            : null);
        } catch (NumberFormatException ex) {
            stockTransformationJsonObject.addProperty(
                    IDObStockTransformationCreateValueObject.TRANSFORMED_QTY,
                    stockTransformation.get(IFeatureFileCommonKeys.QUANTITY));
        }

        addValueToJsonObject(
                stockTransformationJsonObject,
                IDObStockTransformationCreateValueObject.NEW_STOCK_TYPE,
                stockTransformation.get(IInventoryFeatureFileCommonKeys.NEW_STOCK_TYPE));

        String transformationReasonCode =
                stockTransformation.get(IInventoryFeatureFileCommonKeys.STOCK_TRANSFORMATION_REASON)
                        .split(" - ")[0];
        addValueToJsonObject(
                stockTransformationJsonObject,
                IDObStockTransformationCreateValueObject.TRANSFORMATION_REASON_CODE,
                transformationReasonCode);

        return stockTransformationJsonObject;
    }

    public void assertThatStockTransformationWasCreated(DataTable stockTransformationDataTable) {
        Map<String, String> stockTransformation =
                stockTransformationDataTable.asMaps(String.class, String.class).get(0);

        DObStockTransformationGeneralModel dObStockTransformationGeneralModel =
                (DObStockTransformationGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getCreatedStockTransformation",
                                constructStockTransformationQueryParams(stockTransformation));

        assertNotNull(dObStockTransformationGeneralModel);

        assertDateEquals(
                stockTransformation.get(IFeatureFileCommonKeys.CREATION_DATE),
                dObStockTransformationGeneralModel.getCreationDate());

        assertDateEquals(
                stockTransformation.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                dObStockTransformationGeneralModel.getModifiedDate());
    }

    private Object[] constructStockTransformationQueryParams(
            Map<String, String> stockTransformation) {
        return new Object[]{
                stockTransformation.get(IFeatureFileCommonKeys.CODE),
                "%" + stockTransformation.get(IFeatureFileCommonKeys.STATE) + "%",
                stockTransformation.get(IFeatureFileCommonKeys.CREATED_BY),
                stockTransformation.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                stockTransformation.get(IFeatureFileCommonKeys.TYPE),
                stockTransformation.get(IFeatureFileCommonKeys.BUSINESS_UNIT).split(" - ")[0],
        };
    }

    public void assertThatStockTransformationDetailsSectionWasCreated(
            String stockTransformationCode, DataTable stockTransformationDataTable) {
        Map<String, String> stockTransformationDetails =
                stockTransformationDataTable.asMaps(String.class, String.class).get(0);

        DObStockTransformationGeneralModel dObStockTransformationGeneralModel =
                (DObStockTransformationGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getCreatedStockTransformationDetails",
                                constructStockTransformationDetailsQueryParams(
                                        stockTransformationCode, stockTransformationDetails));

        assertNotNull(dObStockTransformationGeneralModel);
    }

    private Object[] constructStockTransformationDetailsQueryParams(
            String stockTransformationCode, Map<String, String> stockTransformationDetails) {
        return new Object[]{
                stockTransformationCode,
                stockTransformationDetails.get(IInventoryFeatureFileCommonKeys.REF_STORE_TRANSACTION),
                Double.valueOf(stockTransformationDetails.get(IFeatureFileCommonKeys.QUANTITY)),
                stockTransformationDetails.get(IInventoryFeatureFileCommonKeys.NEW_STOCK_TYPE),
                stockTransformationDetails.get(IInventoryFeatureFileCommonKeys.STOCK_TRANSFORMATION_REASON)
                        .split(" - ")[0]
        };
    }
}
