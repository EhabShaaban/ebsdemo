package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptViewSalesReturnTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObGoodsReceiptViewSalesReturnStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObGoodsReceiptViewSalesReturnTestUtils goodsReceiptViewSalesReturnTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    goodsReceiptViewSalesReturnTestUtils = new IObGoodsReceiptViewSalesReturnTestUtils(properties);
    goodsReceiptViewSalesReturnTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  public IObGoodsReceiptViewSalesReturnStep() {
    Given(
        "^the following SalesReturnData for GoodsReceipts exist:$",
        (DataTable goodsReceiptSalesReturnDataTable) -> {
          goodsReceiptViewSalesReturnTestUtils.assertThatGoodsReceiptHasSalesReturn(
              goodsReceiptSalesReturnDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view SalesReturn section of GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          Response response =
              goodsReceiptViewSalesReturnTestUtils.viewSalesReturnSection(
                  userActionsTestUtils.getUserCookie(userName), goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of SalesReturn section for GoodsReceipt with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String giCode, String userName, DataTable salesReturnDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          goodsReceiptViewSalesReturnTestUtils.assertThatViewSalesReturnDataResponseIsCorrect(
              response, salesReturnDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GR SalesReturn section -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            goodsReceiptViewSalesReturnTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(goodsReceiptViewSalesReturnTestUtils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View GR SalesReturn section -")) {
      databaseConnector.closeConnection();
    }
  }
}
