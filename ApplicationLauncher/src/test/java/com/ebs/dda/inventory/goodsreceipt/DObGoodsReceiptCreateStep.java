package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptCreateTestUtils;
import com.ebs.dda.inventory.storekeeper.StorekeeperTestUtils;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptReceivedItemsData;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.masterdata.item.utils.MObItemCommonTestUtils;
import com.ebs.dda.order.salesreturnorder.utils.IObSalesReturnOrderViewItemsDataTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObGoodsReceiptCreateStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptCreateTestUtils utils;
  private CObEnterpriseTestUtils enterpriseTestUtils;
  private StorekeeperTestUtils storekeeperTestUtils;
  private MObItemCommonTestUtils itemTestUtils;
  private IObSalesReturnOrderViewItemsDataTestUtils salesReturnOrderViewItemsDataTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObGoodsReceiptCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    salesReturnOrderViewItemsDataTestUtils =
        new IObSalesReturnOrderViewItemsDataTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    storekeeperTestUtils = new StorekeeperTestUtils(entityManagerDatabaseConnector);
    itemTestUtils = new MObItemCommonTestUtils(getProperties());
    itemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObGoodsReceiptCreateStep() {
    Given(
        "^the following SalesReturnOrders exist with the following values needed for GR creation:$",
        (DataTable salesReturnDataTable) -> {
          utils.assertThatSalesReturnOrderExistsWithData(salesReturnDataTable);
        });

    Given(
        "^the following PurchaseOrders exist with the following values needed for GR creation:$",
        (DataTable orderDataTable) -> {
          utils.assertThatPurchaseOrderExistsWithData(orderDataTable);
        });

    Given(
        "^the following items exist:$",
        (DataTable itemsData) -> {
          itemTestUtils.assertItemsExistByCodeNameBaseUnitAndBatchManaged(itemsData);
        });

    Given(
        "^ImportPurchaseOrder with code \"([^\"]*)\" has the following items:$",
        (String purchaseOrderCode, DataTable itemsTable) -> {
          utils.assertThatPurchaseOrderHasItems(purchaseOrderCode, itemsTable);
        });

    Given(
        "^the following Storehouses exist:$",
        (DataTable storehousesData) -> {
          enterpriseTestUtils.assertThatStorehousesExist(storehousesData);
        });

    Given(
        "^the following Storekeepers exist:$",
        (DataTable storekeepersData) -> {
          storekeeperTestUtils.assertThatStorekeepersExist(storekeepersData);
        });

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });
    When(
        "^\"([^\"]*)\" requests to create GoodsReceipt",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IDObGoodsReceiptRestUrls.REQUEST_CREATE_GOODS_RECEIPT);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Given(
        "^Last created GoodsReceipt was with code \"([^\"]*)\"$",
        (String goodsReceiptCode) -> {
          utils.assertThatLastInsertedGoodsReceiptCodeIs(goodsReceiptCode);
        });

    When(
        "^\"([^\"]*)\" creates GoodsReceipt with following values:$",
        (String userName, DataTable goodsReceiptTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          JsonObject goodsReceipt = utils.initializeCreateJsonObject(goodsReceiptTable);
          Response response =
              utils.sendPostRequest(cookie, IDObGoodsReceiptRestUrls.CREATE, goodsReceipt);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new GoodsReceipt is created with the following data:$",
        (DataTable goodsReceiptTable) -> {
          utils.assertThatGoodsReceiptIsCreatedSuccessfully(goodsReceiptTable);
        });

    Then(
        "^the Company Section of GoodsReceipt with code \"([^\"]*)\" is updated as follows:$",
        (String goodsReceiptCode, DataTable companyDataTable) -> {
          utils.assertThatGoodsReceiptHasCompanyData(goodsReceiptCode, companyDataTable);
        });
    And(
        "^the POData Section of GoodsReceipt with code \"([^\"]*)\" is updated as follows:$",
        (String goodsReceiptCode, DataTable poDataTable) -> {
          utils.assertThatGoodsReceiptHasPurchaseOrderData(goodsReceiptCode, poDataTable);
        });
    And(
        "^GoodsReceipt with code \"([^\"]*)\" has the following batch managed received items only:$",
        (String goodsReceiptCode, DataTable receivedItems) -> {
          utils.assertThatGoodsReceiptHasBatchManagedItems(
              goodsReceiptCode,
              IIObGoodsReceiptReceivedItemsData.BATCHED_ITEMS_OBJECT_TYPE_CODE,
              receivedItems);
        });

    And(
        "^GoodsReceipt with code \"([^\"]*)\" has the following ordinary received items only:$",
        (String goodsReceiptCode, DataTable receivedItems) -> {
          utils.assertThatGoodsReceiptHasOrdinaryItems(
              goodsReceiptCode,
              IIObGoodsReceiptReceivedItemsData.ORDINARY_ITEMS_OBJECT_TYPE_CODE,
              receivedItems);
        });

    And(
        "^GoodsReceipt with code \"([^\"]*)\" has no batch managed received items$",
        (String goodsReceiptCode) -> {
          utils.assertThatGoodsReceiptHasNoBatchManagedItems(goodsReceiptCode);
        });

    Given(
        "^the following SalesReturnOrders exist:$",
        (DataTable salesReturnOrderDataTable) -> {
          utils.assertThatSalesReturnOrdersExist(salesReturnOrderDataTable);
        });

    Given(
        "^the following SalesReturnOrders have the following Items:$",
        (DataTable salesReturnOrderItemsDataTable) -> {
          salesReturnOrderViewItemsDataTestUtils.assertThatSalesReturnOrderItemDataExists(
              salesReturnOrderItemsDataTable);
        });

    Then(
        "^the SalesReturnData Section of GoodsReceipt with code \"([^\"]*)\" is updated as follows:$",
        (String goodsReceiptCode, DataTable salesReturnDataTable) -> {
          utils.assertThatGoodsReceiptHasSalesReturnData(goodsReceiptCode, salesReturnDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create GoodsReceipt")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create GoodsReceipt")) {
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
