package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptRequestEditDifferenceReasonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DObGoodsReceiptRequestEditDifferenceReasonStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObGoodsReceiptRequestEditDifferenceReasonTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObGoodsReceiptRequestEditDifferenceReasonTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

  public DObGoodsReceiptRequestEditDifferenceReasonStep() {

    When(
        "^\"([^\"]*)\" requests to edit Item Difference Reason Item with code \"([^\"]*)\" in GR-ReceivedItems section of GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String itemCode, String grCode) -> {
          String requestEditURL =
              IDObGoodsReceiptRestUrls.REQUEST_EDIT_DIFFERENCE_REASON + grCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, requestEditURL, grCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^edit Item Difference dialog is opened and GR-ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String grCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, grCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to edit Item Difference Reason Item with code \"([^\"]*)\" in GR-ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String grCode, String editTime) -> {
          utils.freeze(editTime);
          String requestEditURL =
              IDObGoodsReceiptRestUrls.REQUEST_EDIT_DIFFERENCE_REASON + grCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, requestEditURL, grCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^edit Item Difference dialog is closed and the lock by \"([^\"]*)\" on GR-ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" is released$",
        (String userName, String grCode) -> {
          utils.assertThatLockIsReleased(userName, grCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        });
    When(
        "^\"([^\"]*)\" requests to cancel Item Difference Reason Item with code \"([^\"]*)\" in GR-ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String grCode, String cancelTime) -> {
          utils.freeze(cancelTime);
          String requestEditURL =
              IDObGoodsReceiptRestUrls.REQUEST_CANCEL_EDIT_DIFFERENCE_REASON + grCode + '/' + itemCode;
          Response response = userActionsTestUtils.lockSection(userName, requestEditURL, grCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

      And(
              "^\"([^\"]*)\" opened Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" in edit mode at \"([^\"]*)\" to edit Item DifferenceReason$",
              (String userName, String itemCode, String grCode, String editTime) -> {
                  utils.freeze(editTime);
                  String requestEditURL =
                          IDObGoodsReceiptRestUrls.REQUEST_EDIT_DIFFERENCE_REASON + grCode + '/' + itemCode;
                  Response response = userActionsTestUtils.lockSection(userName, requestEditURL, grCode);
                  utils.assertResponseSuccessStatus(response);
                  userActionsTestUtils.setUserResponse(userName, response);
              });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Edit/Cancel GR Item Difference Reason -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to Edit/Cancel GR Item Difference Reason -")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
          new ArrayList<>(Collections.singletonList(IGoodsReceiptSectionNames.ITEMS_SECTION)));
      databaseConnector.closeConnection();
    }
  }
}
