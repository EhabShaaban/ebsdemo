package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostConvertToActualTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPurchaseOrderReadAllTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceAllowedActionTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceSaveItemTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptRecostingTestUtils;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationCreateTestUtils;
import com.ebs.dda.purchases.utils.MarkAsPIRequestedTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObGoodsReceiptRecostingStep extends SpringBootRunner implements En {

  private DObGoodsReceiptRecostingTestUtils utils;
  private DObStockTransformationCreateTestUtils stockTransformationCreateTestUtils;
  private DObVendorInvoiceSaveItemTestUtils invoiceSaveItemTestUtils;
  private DObPaymentRequestPurchaseOrderReadAllTestUtils
      paymentRequestPurchaseOrderReadAllTestUtils;
  private MarkAsPIRequestedTestUtils markAsPIRequestedTestUtils;
  private DObLandedCostConvertToActualTestUtils landedCostTestUtils;
  private DObVendorInvoiceAllowedActionTestUtils vendorInvoiceAllowedActionTestUtils;

  public DObGoodsReceiptRecostingStep() {
    Given(
        "^the following StoreTransactions exist:$",
        (DataTable storeTransactionsDataTable) -> {
          stockTransformationCreateTestUtils.assertThatStoreTransactionsExist(
              storeTransactionsDataTable);
        });

    Given(
        "^VendorInvoice with code \"([^\"]*)\" has the following InvoiceItems:$",
        (String vendorInvoiceCode, DataTable vendorInvoicesItems) -> {
          invoiceSaveItemTestUtils.assertVendorInvoiceItemExists(
              vendorInvoiceCode, vendorInvoicesItems);
        });
    Given(
        "^the following PurchaseOrders exist:$",
        (DataTable purchaseOrdersDataTable) -> {
          paymentRequestPurchaseOrderReadAllTestUtils.assertPurchaseOrdersExist(
              purchaseOrdersDataTable);
        });
    And(
        "^Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" has the following Quantities:$",
        (String itemCode, String purchaseOrderCode, DataTable quantities) -> {
          markAsPIRequestedTestUtils.assertThatPOQuantitiesAreCorrect(
              itemCode, purchaseOrderCode, quantities);
        });
    When(
        "^\"([^\"]*)\" ConvertToActual LandedCost with code \"([^\"]*)\" at \"([^\"]*)\":$",
        (String userName, String landedCostCode, String dateTime) -> {
          landedCostTestUtils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

          Response response =
              landedCostTestUtils.sendPUTRequest(
                  userCookie,
                  IDObLandedCostRestUrls.COMMAND_URL
                      + "/"
                      + landedCostCode
                      + IDObLandedCostRestUrls.CONVERT_TO_ACTUAL_LANDED_COST,
                  new JsonObject());

          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^ActualCost for StoreTransactions with Goods Receipt RefDocumentCode \"([^\"]*)\" are Updated as Follows:$",
        (String refDocumentCode, DataTable storeTransactionsDataTable) -> {
          utils.assertThatStoreTransactionsWereUpdated(refDocumentCode, storeTransactionsDataTable);
        });
    And(
        "^the following Vendor Invoices exist:$",
        (DataTable invoicesDataTable) -> {
          vendorInvoiceAllowedActionTestUtils.assertThatInvoicesExist(invoicesDataTable);
        });

    Then(
        "^the following values of Items section for Costing with code \"([^\"]*)\" are updated as follows:$",
        (String costingCode, DataTable itemsDataTable) -> {
          utils.assertupdatedCostingItemsExist(costingCode, itemsDataTable);
        });
    Then(
        "^ActualCost for StoreTransactions with Goods Issue RefDocumentCode \"([^\"]*)\" is not changed as Follows:$",
        (String refDocumentCode, DataTable storeTransactionsDataTable) -> {
            utils.assertThatStoreTransactionsWereUpdated(refDocumentCode, storeTransactionsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObGoodsReceiptRecostingTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    stockTransformationCreateTestUtils = new DObStockTransformationCreateTestUtils(properties);
    stockTransformationCreateTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    invoiceSaveItemTestUtils = new DObVendorInvoiceSaveItemTestUtils(getProperties());
    invoiceSaveItemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    paymentRequestPurchaseOrderReadAllTestUtils =
        new DObPaymentRequestPurchaseOrderReadAllTestUtils(getProperties());
    paymentRequestPurchaseOrderReadAllTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    markAsPIRequestedTestUtils = new MarkAsPIRequestedTestUtils(getProperties());
    markAsPIRequestedTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    landedCostTestUtils = new DObLandedCostConvertToActualTestUtils(getProperties());
    landedCostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    vendorInvoiceAllowedActionTestUtils =
        new DObVendorInvoiceAllowedActionTestUtils(getProperties());
    vendorInvoiceAllowedActionTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "GR Recosting -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(utils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "GR Recosting -")) {
      landedCostTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
