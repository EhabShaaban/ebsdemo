package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.costing.utils.DObCostingCommonTestUtils;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentActivationDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class IObCostingViewActivationDetailsTestUtils
        extends DObCostingCommonTestUtils {
    public IObCostingViewActivationDetailsTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        super(properties,entityManagerDatabaseConnector);
    }

    public String getCostingActivationDetailsViewDataUrl(String goodsReceiptCode) {
        return IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT
                + "/"
                + goodsReceiptCode
                + IDObGoodsReceiptRestUrls.VIEW_COSTING_ACTIVATION_DETAILS;
    }

    public void assertCorrectValuesAreDisplayed(Response response, DataTable activationDetailsDataTable) {

        Map<String, String> expectedActivationDetails =
                activationDetailsDataTable.asMaps(String.class, String.class).get(0);
        HashMap<String, Object> actualActivationDetails = getMapFromResponse(response);
        MapAssertion mapAssertion =
                new MapAssertion(
                        expectedActivationDetails, actualActivationDetails);
        assertCodeMatches(mapAssertion);
        assertAccountantMatches(mapAssertion);
        assertActivationDateMatches(mapAssertion);
        assertJournalEntryCode(mapAssertion);
        assertFiscalPeriod(mapAssertion);
        assertCurrencyPrice(mapAssertion);
    }

    private void assertCodeMatches(MapAssertion mapAssertion) {
        mapAssertion.assertValue(
                IFeatureFileCommonKeys.CODE,
                IIObAccountingDocumentActivationDetailsGeneralModel.CODE);
    }

    private void assertAccountantMatches(MapAssertion mapAssertion) {
        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.ACCOUNTANT,
                IIObAccountingDocumentActivationDetailsGeneralModel.ACCOUNTANT);
    }

    private void assertActivationDateMatches(MapAssertion mapAssertion) {
        mapAssertion.assertDateTime(
                IFeatureFileCommonKeys.ACTIVATION_DATE,
                IIObAccountingDocumentActivationDetailsGeneralModel.ACTIVATION_DATE);
    }

    private void assertJournalEntryCode(MapAssertion mapAssertion) {
        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE,
                IIObAccountingDocumentActivationDetailsGeneralModel.JOURNAL_ENTRY_CODE);
    }


    private void assertFiscalPeriod(MapAssertion mapAssertion) {
        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.FISCAL_YEAR,
                IIObAccountingDocumentActivationDetailsGeneralModel.FISCAL_PERIOD);
    }

    private void assertCurrencyPrice(MapAssertion mapAssertion) {
        mapAssertion.assertEquationValues(
                IAccountingFeatureFileCommonKeys.CURRENCY_PRICE,
                constructLeftHandSide(), constructRightHandSide(), "=");
    }

    public List<String> constructLeftHandSide() {
        List<String> leftHandSide = new ArrayList<>();
        leftHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.FIRST_VALUE);
        leftHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.FIRST_CURRENCY_ISO);
        return leftHandSide;
    }

    public List<String> constructRightHandSide() {
        List<String> rightHandSide = new ArrayList<>();
        rightHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.CURRENCY_PRICE);
        rightHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.SECOND_CURRENCY_ISO);
        return rightHandSide;
    }

}
