package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptDeleteItemDetailTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

public class DObGoodsReceiptRequestEditItemDetailStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObGoodsReceiptItemTestUtils utils;
  private DObGoodsReceiptDeleteItemDetailTestUtils deleteItemDetailTestUtils;


    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new IObGoodsReceiptItemTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        deleteItemDetailTestUtils = new DObGoodsReceiptDeleteItemDetailTestUtils(getProperties());
        deleteItemDetailTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

  public DObGoodsReceiptRequestEditItemDetailStep() {

    When(
        "^\"([^\"]*)\" requests to Edit Item Detail with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in ReceivedItems section of GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String detailId, String itemCode, String goodsReceiptCode) -> {
            String postfixUrl = goodsReceiptCode + "/" + itemCode + "/" + Long.parseLong(detailId);
            Response response =
                    userActionsTestUtils.lockSection(
                            userName,
                            IDObGoodsReceiptRestUrls.REQUEST_EDIT_ITEM_DETAIL + postfixUrl,
                            goodsReceiptCode);

            userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^edit Item Detail dialoge is opened and ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String goodsReceiptCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first deleted Item Detail with id \"([^\"]*)\" with type \"([^\"]*)\" of Item with code \"([^\"]*)\" of GoodsReceipt with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName,
            String detailId,
            String detailType,
            String itemCode,
            String goodsReceiptCode, String dateTime) -> {
            utils.freeze(dateTime);
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          deleteItemDetailTestUtils.deleteItemDetail(cookie, goodsReceiptCode, itemCode, detailId);
          deleteItemDetailTestUtils.assertThatItemDetailIsDeleted(
              Long.parseLong(detailId), detailType);
        });

    Given(
        "^\"([^\"]*)\" opened Item Detail with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName,
            String detailId,
            String itemCode,
            String goodsReceiptCode,
            String dateTime) -> {
            utils.freeze(dateTime);
            String postfixUrl = goodsReceiptCode + "/" + itemCode + "/" + Long.parseLong(detailId);
            Response response =
                    userActionsTestUtils.lockSection(
                            userName,
                            IDObGoodsReceiptRestUrls.REQUEST_EDIT_ITEM_DETAIL + postfixUrl,
                            goodsReceiptCode);
            userActionsTestUtils.setUserResponse(userName, response);
            utils.assertThatResourceIsLockedByUser(
                    userName, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to Edit Item Detail with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName,
            String detailId,
            String itemCode,
            String goodsReceiptCode,
            String dateTime) -> {
            utils.freeze(dateTime);
            String postfixUrl = goodsReceiptCode + "/" + itemCode + "/" + Long.parseLong(detailId);
            Response response =
                    userActionsTestUtils.lockSection(
                            userName,
                            IDObGoodsReceiptRestUrls.REQUEST_EDIT_ITEM_DETAIL + postfixUrl,
                            goodsReceiptCode);

            userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to Cancel Item Detail with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in ReceivedItems section of GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String detailId, String itemCode, String goodsReceiptCode) -> {
          String url =
              IDObGoodsReceiptRestUrls.REQUEST_CANCEL_ITEM_DETAIL
                  + goodsReceiptCode
                  + "/"
                  + itemCode
                  + "/"
                  + Long.parseLong(detailId);
          Response response = userActionsTestUtils.unlockSection(userName, url);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to Cancel Item Detail with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in ReceivedItems section of GoodsReceipt with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName,
            String detailId,
            String itemCode,
            String goodsReceiptCode,
            String dateTime) -> {
          utils.freeze(dateTime);
          String url =
              IDObGoodsReceiptRestUrls.REQUEST_CANCEL_ITEM_DETAIL
                  + goodsReceiptCode
                  + "/"
                  + itemCode
                  + "/"
                  + Long.parseLong(detailId);
          Response response = userActionsTestUtils.unlockSection(userName, url);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

    @Before
  public void before(Scenario scenario) throws Exception {
      if (contains(scenario.getName(), "Request to Edit/Cancel GR Item Detail -")) {
    databaseConnector.createConnection();
    if (!hasBeenExecutedOnce) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      hasBeenExecutedOnce = true;
    }
    databaseConnector.executeSQLScript(utils.getDbScriptPath());
  }}

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "Request to Edit/Cancel GR Item Detail -")) {
    utils.unfreeze();
    userActionsTestUtils.unlockAllSections(
        IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
        new ArrayList<>(Collections.singletonList(IGoodsReceiptSectionNames.ITEMS_SECTION)));

    databaseConnector.closeConnection();
  }}
}
