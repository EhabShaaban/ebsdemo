package com.ebs.dda.inventory.goodsissue;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObGoodsIssueViewGeneralDataStep extends SpringBootRunner implements En {

    private DObGoodsIssueViewGeneralDataTestUtils utils;
    private boolean hasBeenExecutedOnce;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObGoodsIssueViewGeneralDataTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObGoodsIssueViewGeneralDataStep() {
        Given(
                "^the following GeneralData for GoodsIssues exist:$",
                (DataTable generalDataTable) -> {
                    utils.assertThatTheFollowingGIGeneralDataExist(generalDataTable);
                });

        When(
                "^\"([^\"]*)\" requests to view GeneralData section of GoodsIssue with code \"([^\"]*)\"$",
        (String userName, String giCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.getViewGeneralDataUrl(giCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of GeneralData section for GoodsIssue with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String giCode, String userName, DataTable generalDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatViewGeneralDataResponseIsCorrect(response, generalDataTable);
        });
  }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View GI GeneralData section -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View GI GeneralData section -")) {
            databaseConnector.closeConnection();
        }
    }
}
