package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class IObGoodsReceiptHeaderViewTestUtils extends DObGoodsReceiptCommonTestUtils {

  private ObjectMapper objectMapper;

  public IObGoodsReceiptHeaderViewTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public void assertThatResponseIsCorrect(Response response, DataTable expectedTable)
      throws Exception {

    Map<String, Object> actualMap = response.body().jsonPath().get(CommonKeys.DATA);
    Map<String, String> expectedMap = expectedTable.asMaps(String.class, String.class).get(0);

    assertEquals(
        expectedMap.get(IFeatureFileCommonKeys.GR_CODE),
        actualMap.get(IDObGoodsReceiptGeneralModel.USER_CODE));
    assertEquals(
        expectedMap.get(IFeatureFileCommonKeys.CREATED_BY),
        actualMap.get(IDObGoodsReceiptGeneralModel.CREATION_INFO));
    String localizedStorekeeper =
        objectMapper.writeValueAsString(
            actualMap.get(IDObGoodsReceiptGeneralModel.STOREKEEPER_NAME));
    assertEquals(
            expectedMap.get(IFeatureFileCommonKeys.STOREKEEPER),
            getEnglishLocaleFromLocalizedResponse(localizedStorekeeper));
    assertEquals(
            expectedMap.get(IFeatureFileCommonKeys.TYPE),
            actualMap.get(IDObGoodsReceiptGeneralModel.TYPE));
    assertEquals(
            expectedMap.get(IFeatureFileCommonKeys.STATE),
            getCurrentState(actualMap.get(IDObGoodsReceiptGeneralModel.STATE)));
    assertDateEquals(
        expectedMap.get(IFeatureFileCommonKeys.CREATION_DATE),
        formatResponseDate(actualMap.get(IDObGoodsReceiptGeneralModel.CREATION_DATE).toString()));
  }

  // TODO: to be removed when applied getstates array
  private Object getCurrentState(Object state) {
    return ((List<Object>) state).get(0);
  }

  private DateTime formatResponseDate(String date) {
    DateTimeFormatter format = DateTimeFormat.forPattern(DateFormats.DATE_TIME_WITH_TIMEZONE);
    DateTime responseDate = DateTime.parse(date, format).withZone(DateTimeZone.UTC);
    return responseDate;
  }

  public void assertThatGoodsReceiptExistsWithHeaderData(DataTable goodsReceiptHeaderData) {
    List<Map<String, String>> headers = goodsReceiptHeaderData.asMaps(String.class, String.class);
    for (Map<String, String> header : headers) {
      String creationDate = header.get(IFeatureFileCommonKeys.CREATION_DATE);

      DObGoodsReceiptGeneralModel headerGeneralModel =
          (DObGoodsReceiptGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptHeaderByGRCode", constructParameters(header));
      assertNotNull(
          "GoodsReceipt with code " + header.get(IFeatureFileCommonKeys.CODE) + " doesn't exist",
          headerGeneralModel);
      assertDateEquals(creationDate, headerGeneralModel.getCreationDate());
    }
  }

  private Object[] constructParameters(Map<String, String> header) {
    String grCode = header.get(IFeatureFileCommonKeys.CODE);
    String createdBy = header.get(IFeatureFileCommonKeys.CREATED_BY);
    String type = header.get(IFeatureFileCommonKeys.TYPE);
    String storekeeperCode = header.get(IFeatureFileCommonKeys.STOREKEEPER).split(" - ")[0];
    String state = header.get(IFeatureFileCommonKeys.STATE);
    return new Object[]{grCode, createdBy, type, storekeeperCode, "%" + state + "%"};
  }

  public String getViewGeneralDataUrl(String code) {
    return IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT
            + "/"
            + code
            + IDObGoodsReceiptRestUrls.VIEW_GENERAL_DATA;
  }
}
