package com.ebs.dda.inventory.storetransaction;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueActivateTestUtils;
import com.ebs.dda.inventory.storetransaction.utils.DObStoreTransactionReadFilteredTestUtils;
import com.ebs.dda.inventory.storetransaction.utils.ITObStoreTransactionsRestUrls;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObStoreTransactionReadFilteredStep extends SpringBootRunner
        implements En, InitializingBean {

    private static boolean hasBeenExecuted = false;
    private DObStoreTransactionReadFilteredTestUtils utils;
    private DObGoodsIssueActivateTestUtils dObGoodsIssuePostTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObStoreTransactionReadFilteredTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        dObGoodsIssuePostTestUtils = new DObGoodsIssueActivateTestUtils(getProperties());
        dObGoodsIssuePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObStoreTransactionReadFilteredStep() {

        Given(
                "^ONLY the following StoreTransaction records exist:$",
                (DataTable storeTransactionDataTable) -> {
                    dObGoodsIssuePostTestUtils.assertThatStoreTransactionHasOnlyTheFollowingRecords(
                            storeTransactionDataTable);
                });
        When(
                "^\"([^\"]*)\" requests to read filtered StoreTransactions with businessUnit \"([^\"]*)\"$",
                (String userName, String businessUnit) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    String businessUnitCode = businessUnit.split(" - ")[0];
                    String readUrl =
                            ITObStoreTransactionsRestUrls.READ_FILTERED_STORE_TRANSACTIONS + businessUnitCode;
                    Response response = utils.sendGETRequest(cookie, readUrl);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following StoreTransactions values will be presented to \"([^\"]*)\":$",
                (String userName, DataTable storeTransactionsDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatStoreTransactionsArePresentedToUser(response, storeTransactionsDataTable);
                });

        Then(
                "^total number of StoreTransactions returned to \"([^\"]*)\" is equal to (\\d+)$",
                (String userName, Integer recordsCount) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsCount);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of StoreTransactions")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(
                        dObGoodsIssuePostTestUtils.getDbScripts());
                hasBeenExecuted = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of StoreTransactions")) {
            databaseConnector.closeConnection();
        }
    }
}
