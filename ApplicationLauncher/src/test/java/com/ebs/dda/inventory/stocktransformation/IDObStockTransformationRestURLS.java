package com.ebs.dda.inventory.stocktransformation;

public interface IDObStockTransformationRestURLS {
    String GET_URL = "/services/stocktransformation";
    String STOCK_TRANSFORMATION_COMMAND = "/command/stocktransformation";

    String CREATE_STOCK_TRANSFORMATION = STOCK_TRANSFORMATION_COMMAND + "/";
    String STOCK_TRANSFORMATION_TYPES = GET_URL + "/stocktransformationtypes";
    String AUTHORIZED_ACTIONS = GET_URL + "/authorizedactions";

    String STOCK_TRASFORMATION_ACTIVTE = "/activate";
}
