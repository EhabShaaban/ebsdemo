package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.costing.utils.DObCostingCommonTestUtils;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.accounting.costing.IIObCostingAccountingDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class IObCostingViewAccountingDetailsTestUtils extends DObCostingCommonTestUtils {
  public IObCostingViewAccountingDetailsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties,entityManagerDatabaseConnector);
  }


  public String getCostingDetailsViewDataUrl(String goodsReceiptCode) {
    return IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT
        + "/"
        + goodsReceiptCode
        + IDObGoodsReceiptRestUrls.VIEW_ACCOUNTING_DETAILS;
  }

  public void assertCorrectValuesAreDisplayed(
      Response response, DataTable accountingDetailsDataTable) throws Exception {

    List<Map<String, String>> expectedAccountingDetails =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualAccountingDetails = getListOfMapsFromResponse(response);

    assertEquals(expectedAccountingDetails.size(), actualAccountingDetails.size());

    for (int index = 0; index < actualAccountingDetails.size(); index++) {
      MapAssertion mapAssertion =
          new MapAssertion(
              expectedAccountingDetails.get(index), actualAccountingDetails.get(index));

      assertAccountingEntryMatches(mapAssertion);
      assertGLAccountMatches(mapAssertion);
      assertSubGLAccountMatches(mapAssertion);
      assertThatAmountMatches(
          expectedAccountingDetails.get(index), actualAccountingDetails.get(index));
    }
  }

  private void assertAccountingEntryMatches(MapAssertion mapAssertion) {
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.CREDIT_DEBIT,
        IIObCostingAccountingDetailsGeneralModel.ACCOUNTING_ENTRY);
  }

  private void assertGLAccountMatches(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.ACCOUNT,
        IIObCostingAccountingDetailsGeneralModel.GL_ACCOUNT_CODE,
        IIObCostingAccountingDetailsGeneralModel.GL_ACCOUNT_NAME);
  }

  private void assertSubGLAccountMatches(MapAssertion mapAssertion) {
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.SUB_ACCOUNT,
        IIObCostingAccountingDetailsGeneralModel.GL_SUB_ACCOUNT_CODE);
  }

  private void assertThatAmountMatches(
      Map<String, String> expectedAccountingDetail,
      HashMap<String, Object> actualAccountingDetail) {
    String[] expectedAmount =
        expectedAccountingDetail.get(IAccountingFeatureFileCommonKeys.VALUE).split(" ");
    BigDecimal expected = new BigDecimal(expectedAmount[0]);
    BigDecimal actual =
        new BigDecimal(
            actualAccountingDetail.get(IIObCostingAccountingDetailsGeneralModel.AMOUNT).toString());
    assertEquals("Amount is not equal", 0, expected.compareTo(actual));
    assertEquals(
        "ISO is not equal",
        expectedAmount[1],
        actualAccountingDetail.get(IIObCostingAccountingDetailsGeneralModel.CURRENCY_ISO));
  }
}
