package com.ebs.dda.inventory.initialstockupload.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

public class DObInitialStockUploadCommonTestUtils extends CommonTestUtils {
  public String clearISUInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/inventory/initial-stock-upload/initial_stock_upload_Clear.sql");
    return str.toString();
  }

  public void createInitialStockUploadGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> isuGeneralDataListMap =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> isuGeneralDataMap : isuGeneralDataListMap) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createInitialStockUploadGeneralData",
          constructInsertInitialStockUploadGeneralDataQueryParams(isuGeneralDataMap));
    }
  }

  private Object[] constructInsertInitialStockUploadGeneralDataQueryParams(
      Map<String, String> initialStockUploadMap) {
    return new Object[] {
      initialStockUploadMap.get(IFeatureFileCommonKeys.CODE),
      initialStockUploadMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      initialStockUploadMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      initialStockUploadMap.get(IFeatureFileCommonKeys.CREATED_BY),
      initialStockUploadMap.get(IFeatureFileCommonKeys.CREATED_BY),
      initialStockUploadMap.get(IFeatureFileCommonKeys.STATE),
      initialStockUploadMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
    };
  }

  public void createInitialStockUploadCompany(DataTable companyDataTable) {

    List<Map<String, String>> isuCompanyMapList =
        companyDataTable.asMaps(String.class, String.class);

    for (Map<String, String> isuCompanyMap : isuCompanyMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createInitialStockUploadCompany",
          constructInsertInitialStockUploadCompanyQueryParameters(isuCompanyMap));
    }
  }

  private Object[] constructInsertInitialStockUploadCompanyQueryParameters(
      Map<String, String> isuCompanyData) {
    return new Object[] {
      isuCompanyData.get(IFeatureFileCommonKeys.CODE),
      isuCompanyData.get(IInventoryFeatureFileCommonKeys.COMPANY),
      isuCompanyData.get(IInventoryFeatureFileCommonKeys.COMPANY).split(" - ")[0],
      isuCompanyData.get(IInventoryFeatureFileCommonKeys.STOREHOUSE),
      isuCompanyData.get(IFeatureFileCommonKeys.STOREKEEPER)
    };
  }

  public void createInitialStockUploadItem(DataTable itemDataTable) {

    List<Map<String, String>> isuItemMapList = itemDataTable.asMaps(String.class, String.class);

    for (Map<String, String> isuItemMap : isuItemMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createInitialStockUploadItem",
          constructInsertInitialStockUploadItemQueryParameters(isuItemMap));
    }
  }

  private Object[] constructInsertInitialStockUploadItemQueryParameters(
      Map<String, String> isuItemData) {
    return new Object[] {
      isuItemData.get(IFeatureFileCommonKeys.CODE),
      isuItemData.get(IInventoryFeatureFileCommonKeys.ORIGINAL_ADDING_DATE),
      isuItemData.get(IFeatureFileCommonKeys.ITEM),
      isuItemData.get(IFeatureFileCommonKeys.UOM),
      isuItemData.get(IFeatureFileCommonKeys.STOCK_TYPE),
      isuItemData.get(IFeatureFileCommonKeys.QTY),
      getCostInDouble(isuItemData.get(IFeatureFileCommonKeys.ESTIMATE_COST)),
      getCostInDouble(isuItemData.get(IFeatureFileCommonKeys.ACTUAL_COST)),
      isuItemData.get(IFeatureFileCommonKeys.REMAINING_QTY)
    };
  }

  protected double getCostInDouble(String cost) {
    if (!cost.isEmpty()) {
      return Double.parseDouble(cost);
    }
    return 0.0;
  }
}
