package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptSaveItemTestUtils;
import com.ebs.dda.jpa.inventory.goodsreceipt.IGoodsReceiptItemValueObject;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObGoodsReceiptSaveItemValidationStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObGoodsReceiptSaveItemTestUtils goodRecieptSaveItemTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    goodRecieptSaveItemTestUtils = new IObGoodsReceiptSaveItemTestUtils(getProperties());
    goodRecieptSaveItemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public IObGoodsReceiptSaveItemValidationStep() {
    Given(
            "^Item with code \"([^\"]*)\" has no IVR record$",
            (String itemCode) -> {
              goodRecieptSaveItemTestUtils.assertItemHasNoItemVendorReocrd(itemCode);
            });

    Then(
            "^the following error message is attached to Item field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          goodRecieptSaveItemTestUtils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IGoodsReceiptItemValueObject.ITEM_CODE);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save GR Item Val -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
                goodRecieptSaveItemTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(goodRecieptSaveItemTestUtils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Save GR Item Val -")) {
      userActionsTestUtils.unlockSectionForAllUsers(IDObGoodsReceiptRestUrls.CANCEL_REQUEST_ADD_ITEM);
      goodRecieptSaveItemTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
