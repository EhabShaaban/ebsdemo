package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptDeleteItemDetailTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.Collections;

public class DObGoodsReceiptDeleteItemDetailStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private DObGoodsReceiptDeleteItemDetailTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObGoodsReceiptDeleteItemDetailTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObGoodsReceiptDeleteItemDetailStep() {

        Given(
                "^the following GoodsReceipts exist with the following Items and Quantities:$",
                (DataTable goodsReceiptWithQuantitiesTable) -> {
                    utils.assertThatGoodsReceiptHasItemsAndQuantities(goodsReceiptWithQuantitiesTable);
                });

        Given(
                "^the following GoodsReceipts exist with the following Items and Batches:$",
                (DataTable goodsReceiptWithBatchesTable) -> {
                    utils.assertThatGoodsReceiptHasItemsAndBatches(goodsReceiptWithBatchesTable);
                });

        When(
                "^\"([^\"]*)\" requests to delete detail with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" at \"([^\"]*)\"$",
                (String userName,
            String detailId,
            String itemCode,
            String goodsReceiptCode,
            String dateTime) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.deleteItemDetail(cookie, goodsReceiptCode, itemCode, detailId);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^detail with id \"([^\"]*)\" with type \"([^\"]*)\" is deleted successfully$",
        (String detailId, String detailType) -> {
          utils.assertThatItemDetailIsDeleted(Long.valueOf(detailId), detailType);
        });

    Then(
        "^Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" is updated as follows:$",
        (String itemCode, String goodsReceiptCode, DataTable modificationDataTable) -> {
          utils.assertThatGoodsReceiptItemIsUpdated(
              goodsReceiptCode, itemCode, modificationDataTable);
        });

    Then(
        "^GoodsReceipt with code \"([^\"]*)\" is updated as follows:$",
        (String goodsReceiptCode, DataTable modificationDataTable) -> {
          utils.assertThatGoodsReceiptIsUpdated(goodsReceiptCode, modificationDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to delete detail with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\"$",
        (String userName, String detailId, String itemCode, String goodsReceiptCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.deleteItemDetail(cookie, goodsReceiptCode, itemCode, detailId);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    And(
        "^\"([^\"]*)\" first deletes detail with id \"([^\"]*)\" with type \"([^\"]*)\" of Item with code \"([^\"]*)\" in GoodsReceipt with code \"([^\"]*)\" successfully$",
        (String userName,
            String detailId,
            String detailType,
            String itemCode,
            String goodsReceiptCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          utils.deleteItemDetail(cookie, goodsReceiptCode, itemCode, detailId);
          utils.assertThatItemDetailIsDeleted(Long.valueOf(detailId), detailType);
        });

    And(
            "^\"([^\"]*)\" requests to add detail to Item with code \"([^\"]*)\" at \"([^\"]*)\" So ReceivedItems section of GoodsReceipt with code \"([^\"]*)\" is locked$",
            (String userName, String itemCode, String dateTime, String goodsReceiptCode) -> {
                utils.freeze(dateTime);
                userActionsTestUtils.lockSection(
                        userName,
                        IDObGoodsReceiptRestUrls.REQUEST_ADD_ITEM_DETAIL + goodsReceiptCode + "/" + itemCode,
                        goodsReceiptCode);
                utils.assertThatResourceIsLockedByUser(
                        userName, goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
            });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete GR Item Detail -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScriptPath());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete GR Item Detail -")) {
            utils.unfreeze();
            userActionsTestUtils.unlockAllSections(
                    IDObGoodsReceiptRestUrls.UNLOCK_LOCKED_SECTIONS,
                    new ArrayList<>(Collections.singletonList(IGoodsReceiptSectionNames.ITEMS_SECTION)));

            databaseConnector.closeConnection();
        }
    }
}
