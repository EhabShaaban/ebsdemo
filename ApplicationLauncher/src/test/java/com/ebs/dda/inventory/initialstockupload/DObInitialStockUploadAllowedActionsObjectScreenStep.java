package com.ebs.dda.inventory.initialstockupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.initialstockupload.utils.DObInitialStockUploadCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObInitialStockUploadAllowedActionsObjectScreenStep extends SpringBootRunner implements En {
  private DObInitialStockUploadCommonTestUtils utils;

  public DObInitialStockUploadAllowedActionsObjectScreenStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of InitialStockUpload with code \"([^\"]*)\"$",
        (String userName, String initialStockUploadCode) -> {
          String url =
              IDObInitialStockUploadRestURLS.AUTHORIZED_ACTIONS + '/' + initialStockUploadCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitialStockUploadCommonTestUtils();
    utils.setProperties(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in InitialStockUpload Object Screen")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearISUInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read allowed actions in InitialStockUpload Object Screen")) {
      databaseConnector.closeConnection();
    }
  }
}
