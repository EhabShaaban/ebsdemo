package com.ebs.dda.inventory.stocktransformation;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationCreateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObStockTransformationCreateStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObStockTransformationCreateTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObStockTransformationCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObStockTransformationCreateStep() {
    Given(
        "^the following StockTransformationTypes exist:$",
        (DataTable stockTransformationTypesDataTable) -> {
          utils.assertThatStockTransformationTypesExist(stockTransformationTypesDataTable);
        });

    Given(
        "^the following StoreTransactions exist:$",
        (DataTable storeTransactionsDataTable) -> {
          utils.assertThatStoreTransactionsExist(storeTransactionsDataTable);
        });
    Given(
        "^the following StockTransformationReasons exist:$",
        (DataTable stockTransformationReasonsDataTable) -> {
          utils.assertThatTransformationReasonsExist(stockTransformationReasonsDataTable);
        });

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    Given(
        "^Last created StockTransformation was with code \"([^\"]*)\"$",
        (String stockTransformationCode) -> {
          utils.assertLastCreatedStockTransformation(stockTransformationCode);
        });

    When(
        "^\"([^\"]*)\" creates StockTransformation with the following values:$",
        (String userName, DataTable stockTransformationDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.createStockTransformation(stockTransformationDataTable, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new StockTransformation is created with the following values:$",
        (DataTable stockTransformationDataTable) -> {
          utils.assertThatStockTransformationWasCreated(stockTransformationDataTable);
        });
    Then(
        "^the StockTransformationDetails Section of StockTransformation with code \"([^\"]*)\" is created as follows:$",
        (String stockTransformationCode, DataTable stockTransformationDataTable) -> {
          utils.assertThatStockTransformationDetailsSectionWasCreated(
              stockTransformationCode, stockTransformationDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create StockTransformation -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create StockTransformation -")) {
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
