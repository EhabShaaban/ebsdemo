package com.ebs.dda.inventory.stocktransformation;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.stocktransformation.utils.DObStockTransformationViewAllTestUtils;
import com.ebs.dda.jpa.inventory.stocktransformation.IDObStockTransformationGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObStockTransformationViewAllStep extends SpringBootRunner implements En {

  private DObStockTransformationViewAllTestUtils stockTransformationViewAllTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        stockTransformationViewAllTestUtils =
                new DObStockTransformationViewAllTestUtils(getProperties());
        stockTransformationViewAllTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }

  public DObStockTransformationViewAllStep() {
    Given(
        "^total number of StockTransformations records is (\\d+)$",
        (Integer recordsNumber) -> {
          stockTransformationViewAllTestUtils.assertTotalNumberOfStockTransformationsAreCorrect(
              recordsNumber);
        });
    ;

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockTransformations with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNumber, Integer pageSize) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              stockTransformationViewAllTestUtils.requestToReadFilteredData(
                  userCookie, IDObStockTransformationRestURLS.GET_URL, pageNumber, pageSize, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable stockTransformationsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          stockTransformationViewAllTestUtils.assertResponseSuccessStatus(response);
          stockTransformationViewAllTestUtils.assertThatStockTransformationssIsMatch(
              response, stockTransformationsDataTable);
        });
    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer pageSize) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          stockTransformationViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
              response, pageSize);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockTransformations with filter applied on Type which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              stockTransformationViewAllTestUtils.getContainsFilterField(
                  IDObStockTransformationGeneralModel.STOCK_TRANSFORMATION_TYPE_CODE,
                  filteringValue);
          Response response =
              stockTransformationViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObStockTransformationRestURLS.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockTransformations with filter applied on RefStoreTransactionCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              stockTransformationViewAllTestUtils.getContainsFilterField(
                  IDObStockTransformationGeneralModel.REF_STORE_TRANSACTION_CODE, filteringValue);
          Response response =
              stockTransformationViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObStockTransformationRestURLS.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockTransformations with filter applied on NewStoreTransactionCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              stockTransformationViewAllTestUtils.getContainsFilterField(
                  IDObStockTransformationGeneralModel.NEW_STORE_TRANSACTION_CODE, filteringValue);
          Response response =
              stockTransformationViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObStockTransformationRestURLS.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockTransformations with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              stockTransformationViewAllTestUtils.getContainsFilterField(
                  IDObStockTransformationGeneralModel.CURRENT_STATES, filteringValue);
          Response response =
              stockTransformationViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObStockTransformationRestURLS.GET_URL,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockTransformations with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer numberOfRecords,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              stockTransformationViewAllTestUtils.getContainsFilterField(
                  IDObStockTransformationGeneralModel.STOCK_TRANSFORMATION_CODE, filteringValue);
          Response response =
              stockTransformationViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObStockTransformationRestURLS.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of StockTransformations with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              stockTransformationViewAllTestUtils.getContainsFilterField(
                  IDObStockTransformationGeneralModel.PURCHASEUNIT_CODE, filteringValue);
          Response response =
              stockTransformationViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObStockTransformationRestURLS.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View all StockTransformations -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          stockTransformationViewAllTestUtils.getViewAllDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View all StockTransformations -")) {
      databaseConnector.closeConnection();
    }
  }
}
