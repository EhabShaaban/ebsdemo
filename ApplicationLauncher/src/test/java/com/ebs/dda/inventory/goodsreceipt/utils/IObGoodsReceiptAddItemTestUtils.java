package com.ebs.dda.inventory.goodsreceipt.utils;


import java.util.Map;

public class IObGoodsReceiptAddItemTestUtils extends IObGoodsReceiptItemTestUtils {

    public IObGoodsReceiptAddItemTestUtils(Map<String, Object> properties) {
        super(properties);
    }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
      str.append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
      str.append(",").append("db-scripts/inventory/goods-receipt/DObGoodsReceiptPurchaseOrderScript_ViewAll.sql");
    return str.toString();
  }
}
