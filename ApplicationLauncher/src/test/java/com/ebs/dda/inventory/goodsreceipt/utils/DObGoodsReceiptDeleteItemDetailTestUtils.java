package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import javax.persistence.NoResultException;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DObGoodsReceiptDeleteItemDetailTestUtils extends IObGoodsReceiptItemTestUtils {

  public static final String BATCH = "Batch";

  public DObGoodsReceiptDeleteItemDetailTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response deleteItemDetail(
      Cookie userCookie, String goodsReceiptCode, String itemCode, String detailId) {
    String restURL =
        IDObGoodsReceiptRestUrls.DELETE
            + goodsReceiptCode
            + "/"
            + itemCode
            + "/"
            + Long.parseLong(detailId);
    return sendDeleteRequest(userCookie, restURL);
  }

  public void assertThatItemDetailIsDeleted(Long detailId, String detailType) {

    try {
      if (detailType.equals(BATCH)) {
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getGoodsReceiptItemBatchById", detailId);
      } else {
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getGoodsReceiptItemQuantityById", detailId);
      }
      assertFalse("Goods Receipt Item Detail has not been deleted", false);

    } catch (NoResultException noResultException) {
      assertTrue(true);
    }
  }
}
