package com.ebs.dda.inventory.goodsreceipt;

public interface IDObGoodsReceiptRestUrls {

  String COMMAND_GOODS_RECEIPT = "/command/goodsreceipt/";
  String QUERY_GOODS_RECEIPT = "/services/goodsreceipt";

  String CREATE = COMMAND_GOODS_RECEIPT + "create";
  String REQUEST_CREATE_GOODS_RECEIPT = COMMAND_GOODS_RECEIPT + "createconfiguration";

  String UNLOCK_LOCKED_SECTIONS = COMMAND_GOODS_RECEIPT + "unlock/lockedsections/";

  String ACTIVATE = "/activate";

  String REQUEST_ADD_ITEM = COMMAND_GOODS_RECEIPT + "requestadditem/";
  String CANCEL_REQUEST_ADD_ITEM = COMMAND_GOODS_RECEIPT + "cancelrequestadditem/";

  String REQUEST_ADD_ITEM_DETAIL = COMMAND_GOODS_RECEIPT + "lock/add/itemdetail/";
  String REQUEST_CANCEL_ADD_ITEM_DETAIL = COMMAND_GOODS_RECEIPT + "unlock/add/itemdetail/";

  String DELETE = COMMAND_GOODS_RECEIPT + "delete/";
  String REQUEST_EDIT_ITEM_DETAIL = COMMAND_GOODS_RECEIPT + "lock/edit/itemdetail/";
  String REQUEST_CANCEL_ITEM_DETAIL = COMMAND_GOODS_RECEIPT + "unlock/edit/itemdetail/";

  String REQUEST_EDIT_DIFFERENCE_REASON =
      COMMAND_GOODS_RECEIPT + "lock/itemssection/differencereason/";
  String REQUEST_CANCEL_EDIT_DIFFERENCE_REASON =
      COMMAND_GOODS_RECEIPT + "unlock/itemssection/differencereason/";
  String SAVE_ITEM_DETAIL = COMMAND_GOODS_RECEIPT + "save/itemdetail/";
  String ADD_ITEM_DETAIL = COMMAND_GOODS_RECEIPT + "add/itemdetail/";

  String REQUEST_SAVE_ITEM_URL = "/command/goodsreceipt/saveitem/";

  String SAVE_DIFFERENCE_REASON = COMMAND_GOODS_RECEIPT + "differencereason/";
  String AUTHORIZED_ACTIONS = QUERY_GOODS_RECEIPT + "/authorizedactions/";
  String VIEW_GENERAL_DATA = "/generaldata";
  String VIEW_COMPANY = "/company";
  String VIEW_REF_DOCUMENT = "/refdocumentdata";
  String VIEW_ACCOUNTING_DETAILS = "/accountingdetails";
  String VIEW_COSTING_ACTIVATION_DETAILS = "/costing/activationdetails";

  String READ_TYPES = QUERY_GOODS_RECEIPT + "/types";
}
