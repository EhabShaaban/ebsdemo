package com.ebs.dda.inventory.goodsreceipt;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptViewItemsTestUtils;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObGoodsReceiptViewItemsStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private IObGoodsReceiptViewItemsTestUtils goodsReceiptViewItemsTestUtils;
    private ItemVendorRecordViewAllTestUtils itemVendorRecordTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        goodsReceiptViewItemsTestUtils = new IObGoodsReceiptViewItemsTestUtils(getProperties());
        itemVendorRecordTestUtils = new ItemVendorRecordViewAllTestUtils(getProperties());
        itemVendorRecordTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        goodsReceiptViewItemsTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }

    public IObGoodsReceiptViewItemsStep() {

        Given(
                "^the following PurchaseOrder exits:$",
                (DataTable purchaseOrdersData) -> {
                    goodsReceiptViewItemsTestUtils.assertThatPurchaseOrdersExistByCode(purchaseOrdersData);
                });

        Given(
                "^PurchaseOrder with code \"([^\"]*)\" has the following OrderItems:$",
                (String purchaseOrderCode, DataTable items) -> {
                    goodsReceiptViewItemsTestUtils.assertThatPurchaseOrderHasItems(purchaseOrderCode, items);
                });

        Given(
                "^the following IVRs exist:$",
                (DataTable itemVendorRecordsList) -> {
                    itemVendorRecordTestUtils.assertThatItemVendorRecordsExists(itemVendorRecordsList);
                });
        Given(
                "^Item \"([^\"]*)\" in GoodsReceipt \\(Based On SalesReturn\\) \"([^\"]*)\" has the following received quantities:$",
                (String item, String goodsReceiptCode, DataTable ordinaryItemQuantities) -> {
                    goodsReceiptViewItemsTestUtils.assertGoodsReceiptSalesReturnItemQuantitiesExist(
                            goodsReceiptCode, item, ordinaryItemQuantities);
                });

        Then(
                "^the following values of ReceivedItems section are displayed to \"([^\"]*)\":$",
                (String userName, DataTable receivedItemTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    goodsReceiptViewItemsTestUtils.assertThatReceivedItemsAreRepresentedToUser(
                            receivedItemTable, response);
                });

        Then(
        "^the following quantity data for Item \"([^\"]*)\" is displayed to \"([^\"]*)\":$",
        (String itemCode, String userName, DataTable itemQuantities) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          goodsReceiptViewItemsTestUtils.assertThatReceivedItemsQuantitiesAreRepresentedToUser(
              itemCode, itemQuantities, response);
        });

    Then(
        "^the following batch data for Item \"([^\"]*)\" is displayed to \"([^\"]*)\":$",
        (String itemCode, String userName, DataTable batchesTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          goodsReceiptViewItemsTestUtils.assertThatReceivedItemsBatchesAreRepresentedToUser(
              itemCode, batchesTable, response);
        });

    When(
        "^\"([^\"]*)\" requests to view ReceivedItems section of GR with code \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode) -> {
          Response response =
              goodsReceiptViewItemsTestUtils.viewReceivedItemsSection(
                  userActionsTestUtils.getUserCookie(userName), goodsReceiptCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When("the following GoodsReceipts exist with the following BusinessPartner and ReferenceDoc data",
            (DataTable goodsReceiptsDataTable) -> {
                goodsReceiptViewItemsTestUtils.assertGoodsReceiptExistsWithData(goodsReceiptsDataTable);
            });
        Given("^the following UnitOfMeasures section exist in Item with code \"([^\"]*)\":$", (String itemCode, DataTable itemUOMDataTable) -> {
            goodsReceiptViewItemsTestUtils.assertUOMsExistInItemWithCode(itemUOMDataTable, itemCode);
        });

        Given("^SalesReturn with code \"([^\"]*)\" has the following OrderItems:$", (String SRCode, DataTable orderItemsDataTable) -> {
            goodsReceiptViewItemsTestUtils.assertSalesReturnHasOrderItems(SRCode, orderItemsDataTable);
        });
    }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View GR Received Items -")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            goodsReceiptViewItemsTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(goodsReceiptViewItemsTestUtils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View GR Received Items -")) {
      databaseConnector.closeConnection();
    }
  }
}
