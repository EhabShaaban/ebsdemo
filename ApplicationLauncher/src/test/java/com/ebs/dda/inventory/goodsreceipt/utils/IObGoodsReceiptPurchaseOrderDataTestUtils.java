package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class IObGoodsReceiptPurchaseOrderDataTestUtils extends DObGoodsReceiptCommonTestUtils {

  public void assertThatGoodsReceiptsExist(DataTable goodsReceiptPurchaseOrderData) {
    List<Map<String, String>> goodsReceiptsPurchaseOrders =
        goodsReceiptPurchaseOrderData.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptPurchaseOrder : goodsReceiptsPurchaseOrders) {
      assertThatGoodsReceiptExist(goodsReceiptPurchaseOrder);
      assertThatGoodsReceiptPurchaseOrderDataIsCorrect(goodsReceiptPurchaseOrder);
    }
  }

  private void assertThatGoodsReceiptPurchaseOrderDataIsCorrect(
      Map<String, String> goodsReceiptPurchaseOrderData) {
    List<IObGoodsReceiptPurchaseOrderDataGeneralModel> goodsReceiptPurchaseOrderDataList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getGoodsReceiptPOData",
            constructGoodsReceiptPurchaseOrderQueryParams(goodsReceiptPurchaseOrderData));
    Assert.assertEquals(1, goodsReceiptPurchaseOrderDataList.size());
    String actualDeliveryNote = goodsReceiptPurchaseOrderDataList.get(0).getDeliveryNote();
    String comments = goodsReceiptPurchaseOrderDataList.get(0).getComments();

    assetThatDeliveryNoteIsCorrect(goodsReceiptPurchaseOrderData, actualDeliveryNote);
    assertThatCommentsFeildIsCorrect(goodsReceiptPurchaseOrderData, comments);
  }

  private void assertThatCommentsFeildIsCorrect(
      Map<String, String> goodsReceiptPurchaseOrderData, String comments) {

    if (!goodsReceiptPurchaseOrderData.get(IGoodsReceiptFeatureFileCommonKeys.NOTES).isEmpty()) {
      Assert.assertEquals(
          goodsReceiptPurchaseOrderData.get(IGoodsReceiptFeatureFileCommonKeys.NOTES), comments);
    } else {
      Assert.assertNull(comments);
    }
  }

  private void assetThatDeliveryNoteIsCorrect(
      Map<String, String> goodsReceiptPurchaseOrderData, String actualDeliveryNote) {
    if (!goodsReceiptPurchaseOrderData
            .get(IGoodsReceiptFeatureFileCommonKeys.DELIVERY_NOTE)
        .isEmpty()) {
      Assert.assertEquals(
              goodsReceiptPurchaseOrderData.get(IGoodsReceiptFeatureFileCommonKeys.DELIVERY_NOTE),
              actualDeliveryNote);
    } else {
      Assert.assertNull(actualDeliveryNote);
    }
  }

  private Object[] constructGoodsReceiptPurchaseOrderQueryParams(
      Map<String, String> goodsReceiptPurchaseOrderData) {
    return new Object[]{
            goodsReceiptPurchaseOrderData.get(IFeatureFileCommonKeys.CODE),
            goodsReceiptPurchaseOrderData.get(IGoodsReceiptFeatureFileCommonKeys.PURCHASE_ORDER_CODE),
            goodsReceiptPurchaseOrderData.get(IGoodsReceiptFeatureFileCommonKeys.VENDOR_CODE),
            goodsReceiptPurchaseOrderData.get(
                    IGoodsReceiptFeatureFileCommonKeys.PURCHASE_RESPONSIBLE),
    };
  }

  private void assertThatGoodsReceiptExist(Map<String, String> goodsReceipt) {
    List<DObGoodsReceiptGeneralModel> goodsReceiptsList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getGoodsReceipts", constructGoodsReceiptQueryParams(goodsReceipt));
    Assert.assertEquals(1, goodsReceiptsList.size());
  }

  private Object[] constructGoodsReceiptQueryParams(Map<String, String> goodsReceipt) {
    String currentStateWithOperation = "%" + goodsReceipt.get(STATE) + "%";
    return new Object[] {goodsReceipt.get(CODE), currentStateWithOperation, goodsReceipt.get(TYPE)};
  }

}
