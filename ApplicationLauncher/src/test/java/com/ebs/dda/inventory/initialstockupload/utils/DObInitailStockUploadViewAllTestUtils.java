package com.ebs.dda.inventory.initialstockupload.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.initialstockupload.IDObInitialStockUploadGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DObInitailStockUploadViewAllTestUtils extends DObInitialStockUploadCommonTestUtils {

  public static String REQUEST_LOCALE = "en";
  private ObjectMapper objectMapper;

  public DObInitailStockUploadViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public void assertInitailStockUploadDataIsCorrect(DataTable initialStockUploadDataTable, Response response)
      throws Exception {

    List<Map<String, String>> initialStockUploadList =
        initialStockUploadDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> initialStockUploadResponse = response.body().jsonPath().getList("data");
    for (int i = 1; i < initialStockUploadList.size(); i++) {
      assertCorrectShownData(initialStockUploadResponse.get(i - 1), initialStockUploadList.get(i - 1));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> responsedInitialStockUpload, Map<String, String> initailStockUploadRowMap)
      throws IOException, JSONException {

    String code = initailStockUploadRowMap.get(IFeatureFileCommonKeys.CODE);
    String createdBy = initailStockUploadRowMap.get(IFeatureFileCommonKeys.CREATED_BY);
    String purchaseUnitName = initailStockUploadRowMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    String state = initailStockUploadRowMap.get(IFeatureFileCommonKeys.STATE);
    String creationDate = initailStockUploadRowMap.get(IFeatureFileCommonKeys.CREATION_DATE);
    String storehouseName = initailStockUploadRowMap.get(IFeatureFileCommonKeys.STOREHOUSE);

    assertEquals(code, getAsString(responsedInitialStockUpload.get(IDObInitialStockUploadGeneralModel.USER_CODE)));
    assertEquals(createdBy, getAsString(responsedInitialStockUpload.get(IDObInitialStockUploadGeneralModel.CREATION_INFO)));
    String localizedPurchaseUnitName =
        getLocaleFromLocalizedResponse(
            getLocalizedValue(
                responsedInitialStockUpload.get(IDObInitialStockUploadGeneralModel.PURCHASING_UNIT_NAME)),
            REQUEST_LOCALE);
    assertEquals(purchaseUnitName, localizedPurchaseUnitName);
    assertTrue(
        responsedInitialStockUpload
            .get(IDObInitialStockUploadGeneralModel.CURRENT_STATES)
            .toString()
            .contains(state));
    assertCreationDateEquals(creationDate, responsedInitialStockUpload);
    assertEquals(purchaseUnitName, localizedPurchaseUnitName);
    String localizedStorehouseName =
        getLocaleFromLocalizedResponse(
            getLocalizedValue(responsedInitialStockUpload.get(IDObInitialStockUploadGeneralModel.STORE_HOUSE_NAME)),
            REQUEST_LOCALE);
    assertEquals(storehouseName, localizedStorehouseName);
  }

  private String getAsString(Object obj) {
    if (obj == null) return "";
    return obj.toString();
  }

  private String getLocalizedValue(Object obj) throws IOException {
    return objectMapper.writeValueAsString(obj);
  }

  public void assertCreationDateEquals(
      String creationDate, Map<String, Object> goodsIssueDatabaseVersion) {
    if (creationDate != null && !creationDate.isEmpty()) {
      assertThatDatesAreEqual(
          creationDate,
          String.valueOf(goodsIssueDatabaseVersion.get(IDObInitialStockUploadGeneralModel.CREATION_DATE)));
    } else {
      assertNull(goodsIssueDatabaseVersion.get(IDObInitialStockUploadGeneralModel.CREATION_DATE));
    }
  }

}
