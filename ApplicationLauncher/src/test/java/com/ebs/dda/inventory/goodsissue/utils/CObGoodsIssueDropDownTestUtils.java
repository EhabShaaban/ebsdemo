package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.jpa.inventory.goodsissue.ICObGoodsIssue;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CObGoodsIssueDropDownTestUtils extends DObGoodsIssueCommonTestUtils {

  private ObjectMapper objectMapper;

  public CObGoodsIssueDropDownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");

    return str.toString();
  }

  public String readAllGoodsIssueTypes() {
    return IDObGoodsIssueRestURLS.BASE_URL + IDObGoodsIssueRestURLS.VIEW_GI_TYPES;
  }

  public void assertResponseMatchesExpected(Response response, DataTable goodsIssueTypesDataTable)
      throws Exception {
    List<Map<String, String>> goodsIssueTypesExpectedList =
        goodsIssueTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> goodsIssueTypesActualList = getListOfMapsFromResponse(response);

    for (int i = 0; i < goodsIssueTypesActualList.size(); i++) {
      String goodsIssueTypesActuaCode =
          (String) goodsIssueTypesActualList.get(i).get(ICObGoodsIssue.CODE);

      Map goodsIssueTypesActualMap = new HashMap<String, String>();
      goodsIssueTypesActualMap.put(IFeatureFileCommonKeys.CODE, goodsIssueTypesActuaCode);
      assertTrue(goodsIssueTypesExpectedList.contains(goodsIssueTypesActualMap));
    }
  }

  public void assertThatGoodsIssueTypesCountIsCorrect(Integer recordsNumber) {
    Long typesCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsIssueTypesCount");
    assertEquals(Long.valueOf(recordsNumber), typesCount);
  }
}
