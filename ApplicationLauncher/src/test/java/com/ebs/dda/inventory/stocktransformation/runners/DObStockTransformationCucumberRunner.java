package com.ebs.dda.inventory.stocktransformation.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
            "classpath:features/inventory/stock-transformation/StkTr_AllowedActions_Home.feature",
            "classpath:features/inventory/stock-transformation/StkTr_AllowedActions_Object.feature",
            "classpath:features/inventory/stock-transformation/StkTr_Create_HP.feature",
            "classpath:features/inventory/stock-transformation/StkTr_Create_Val.feature",
            "classpath:features/inventory/stock-transformation/StkTr_Activate_AddStoreTransaction.feature",
            "classpath:features/inventory/stock-transformation/StkTr_Activate_Val.feature",
            "classpath:features/inventory/stock-transformation/StkTr_Activate_Auth.feature",
            "classpath:features/inventory/stock-transformation/StkTr_Reasons_Dropdown.feature",
            "classpath:features/inventory/stock-transformation/StkTr_Types_Dropdown.feature",
            "classpath:features/inventory/stock-transformation/StkTr_ViewAll.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.inventory.stocktransformation"},
    strict = true,
    tags = "not @Future")
public class DObStockTransformationCucumberRunner {}
