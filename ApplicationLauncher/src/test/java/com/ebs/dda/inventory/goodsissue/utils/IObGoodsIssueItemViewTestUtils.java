package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.IIObGoodsIssueItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class IObGoodsIssueItemViewTestUtils extends DObGoodsIssueCommonTestUtils {
    private ObjectMapper objectMapper;

    public IObGoodsIssueItemViewTestUtils(Map<String, Object> properties) {
        setProperties(properties);
        objectMapper = new ObjectMapper();
    }

    public String getDbScriptsOneTimeExecution() {
        StringBuilder str = new StringBuilder();
        str.append("db-scripts/clearDataSqlScript.sql");
        str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
        str.append(",").append("db-scripts/CObMeasureScript.sql");
        str.append(",").append("db-scripts/master-data/item/MasterData.sql");
        str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
        str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
        str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
        str.append("," + "db-scripts/master-data/item/CObItem.sql");
        str.append(",").append("db-scripts/master-data/item/MObItem.sql");
        str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
        str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
        str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
        str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
        str.append(",").append("db-scripts/LookupSqlScript.sql");
        str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
        str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
        str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
        str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
        str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
        str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
        str.append(",").append("db-scripts/accounting/taxes.sql");
        str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
        str.append("," + "db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
        str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
        str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
        str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
        str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
        str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
        return str.toString();
    }

    public String getGoodsIssueItemsViewDataUrl(String code) {
        return IDObGoodsIssueRestURLS.BASE_URL + "/" + code + IDObGoodsIssueRestURLS.VIEW_ITEMS;
    }

    public void assertThatGoodsIssueHasItems(DataTable itemsDataTable) {

        List<Map<String, String>> goodsIssueItemsData =
                itemsDataTable.asMaps(String.class, String.class);

        for (Map<String, String> goodsIssueItemData : goodsIssueItemsData) {
            assertThatGoodsIssueHasItem(goodsIssueItemData);
        }
    }

    private void assertThatGoodsIssueHasItem(Map<String, String> goodsIssueItemData) {
        Object[] queryParameters = getGoodsIssueItemQueryParameters(goodsIssueItemData);
        String goodsIssueCode = goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE);
        String item = goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.ITEM);
        IObGoodsIssueItemGeneralModel goodsIssueItemGeneralModel =
                getIObGoodsIssueItem(queryParameters);
        Assert.assertNotNull(
                "goodsIssue: " + goodsIssueCode + " item:" + item, goodsIssueItemGeneralModel);
    }

    private Object[] getGoodsIssueItemQueryParameters(Map<String, String> goodsIssueItemData) {
        String price =
                goodsIssueItemData.get(IFeatureFileCommonKeys.PRICE).isEmpty()
                        ? null
                        : goodsIssueItemData.get(IFeatureFileCommonKeys.PRICE);
        String goodsIssueCode = goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE);
        String item = goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.ITEM);
        String uom = goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.UOM);
        String quantity = goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.QUANTITY);
        String baseUnit = goodsIssueItemData.get(IFeatureFileCommonKeys.BASE_UNIT);
        String quantityBase = goodsIssueItemData.get(IGoodsIssueFeatureFileCommonKeys.QTY_BASE);
        String batchNumber = goodsIssueItemData.get(IFeatureFileCommonKeys.BATCH_NUMBER);

        return new Object[]{
                price,
                goodsIssueCode,
                item,
                uom,
                quantity,
                baseUnit,
                quantityBase,
                batchNumber.isEmpty() ? null : batchNumber
        };
    }

    private IObGoodsIssueItemGeneralModel getIObGoodsIssueItem(Object[] parameters) {
        return (IObGoodsIssueItemGeneralModel)
                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                        "getGoodsIssueItemData", parameters);
    }

    public void assertThatGoodsIssueItemsMatchesResponse(DataTable itemsDataTable, Response response)
            throws Exception {
        List<Map<String, String>> expectedItemsData = itemsDataTable.asMaps(String.class, String.class);
        List<HashMap<String, Object>> actualItemsData = getListOfMapsFromResponse(response);
        for (int index = 0; index < expectedItemsData.size(); index++) {
            assertThatGoodsIssueStoreDataMatches(
                    expectedItemsData.get(index), actualItemsData.get(index));
        }
    }

    private void assertThatGoodsIssueStoreDataMatches(
            Map<String, String> expectedItemData, HashMap<String, Object> actualItemData)
            throws Exception {

        MapAssertion mapAssertion = new MapAssertion(expectedItemData, actualItemData);

        Object itemNameObject = actualItemData.get(IIObGoodsIssueItemGeneralModel.ITEM_NAME);
        String itemName = getLocalizedValue(itemNameObject);

        assertEquals(
                expectedItemData.get(IFeatureFileCommonKeys.ITEM_CODE),
                actualItemData.get(IIObGoodsIssueItemGeneralModel.ITEM_CODE));
        assertEquals(
                expectedItemData.get(IGoodsIssueFeatureFileCommonKeys.ITEM),
                getEnglishLocaleFromLocalizedResponse(itemName));
        assertEquals(
                expectedItemData.get(IGoodsIssueFeatureFileCommonKeys.UOM).split(" - ")[0],
                actualItemData.get(IIObGoodsIssueItemGeneralModel.UOM_CODE));
        mapAssertion.assertBigDecimalNumberValue(
                IGoodsIssueFeatureFileCommonKeys.QUANTITY,
                IIObGoodsIssueItemGeneralModel.QUANTITY);
        assertEquals(
                expectedItemData.get(IFeatureFileCommonKeys.BASE_UNIT).split(" - ")[0],
                actualItemData.get(IIObGoodsIssueItemGeneralModel.BASE_UNIT_CODE));
        mapAssertion.assertBigDecimalNumberValue(
                IGoodsIssueFeatureFileCommonKeys.QTY_BASE,
                IIObGoodsIssueItemGeneralModel.RECEIVED_QTY_BASE);
        assertEquals(
                convertEmptyStringToNull(expectedItemData.get(IFeatureFileCommonKeys.BATCH_NUMBER)),
                actualItemData.get(IIObGoodsIssueItemGeneralModel.BATCH_NO));
    }

    private String getLocalizedValue(Object obj) throws IOException {
        return objectMapper.writeValueAsString(obj);
    }
}
