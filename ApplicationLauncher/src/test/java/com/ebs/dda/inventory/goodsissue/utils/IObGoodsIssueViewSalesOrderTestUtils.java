package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.inventory.goodsissue.IGoodsIssueFeatureFileCommonKeys;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class IObGoodsIssueViewSalesOrderTestUtils
    extends IObGoodsIssueViewRefDocumentDataTestUtils {
  public IObGoodsIssueViewSalesOrderTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/CObGoodsIssue.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
      str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
      str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueSalesOrderData.sql");
    str.append(",").append("db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    return str.toString();
  }

  public void assertThatGoodsIssueHasSalesOrder(DataTable salesOrderDataTable) {
    List<Map<String, String>> giSalesOrderListMap =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> giSalesOrderDataMap : giSalesOrderListMap) {
      IObGoodsIssueRefDocumentDataGeneralModel giSalesOrderData =
          (IObGoodsIssueRefDocumentDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsIssueSalesOrderData",
                  constructGoodsIssueSalesOrderQueryParams(giSalesOrderDataMap));
      Assert.assertNotNull("GI SalesOrder Data doesn't exist", giSalesOrderData);
    }
  }

  private Object[] constructGoodsIssueSalesOrderQueryParams(Map<String, String> goodsIssueMap) {
    return new Object[] {
      goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.CONTACT_PERSON),
      goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.COMMENTS),
      goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.GI_CODE),
      goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.CUSTOMER),
      goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.ADDRESS),
      goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.REF_DOCUMENT),
      goodsIssueMap.get(IGoodsIssueFeatureFileCommonKeys.SALES_REPRESENTATIVE),
    };
  }
}
