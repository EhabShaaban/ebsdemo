package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.inventory.goodsreceipt.IGoodsReceiptFeatureFileCommonKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IObGoodsReceiptPurchaseOrderDataViewTestUtils
    extends IObGoodsReceiptPurchaseOrderDataTestUtils {

  public IObGoodsReceiptPurchaseOrderDataViewTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = getProperty(URL_PREFIX).toString();
    updateRestURLs();
  }

  public void assertGoodsReceiptPurchaseOrderDataIsRepresentedToUser(
      Response goodsReceiptPurchaseOrderDataResponse,
      DataTable expectedGoodsReceiptPurchaseOrderDataResponse)
      throws Exception {
    Map<String, String> expectedPurchaseOrderData =
        expectedGoodsReceiptPurchaseOrderDataResponse.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualPurchaseOrderData =
        goodsReceiptPurchaseOrderDataResponse.body().jsonPath().get("data");

    assertGoodsReceiptPurchaseOrderDataMapsAreEqual(
        expectedPurchaseOrderData, actualPurchaseOrderData);
  }

  public Response readGoodsReceiptPurchaseOrderData(Cookie cookie, String goodsReceiptCode) {
    String restURL =
        IDObGoodsReceiptRestUrls.QUERY_GOODS_RECEIPT
            + "/"
            + goodsReceiptCode
            + IDObGoodsReceiptRestUrls.VIEW_REF_DOCUMENT;
    return sendGETRequest(cookie, restURL);
  }

  private void assertGoodsReceiptPurchaseOrderDataMapsAreEqual(
      Map<String, String> expectedPurchaseOrderMap, Map<String, Object> actualPurchaseOrderMap)
      throws Exception {
    String grVendorName = getStringfiedJson(actualPurchaseOrderMap.get(GR_VENDOR_NAME));

    assertEquals(
        expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.PURCHASE_ORDER_CODE),
        actualPurchaseOrderMap.get(GR_PURCHASEORDER_CODE));
    assertEquals(
        expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.VENDOR_CODE),
        getEnglishLocaleFromLocalizedResponse(grVendorName));
    assertEquals(
        expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.PURCHASE_RESPONSIBLE),
            actualPurchaseOrderMap.get(GR_PURCHASERESPONSIBLE_NAME));
    if (!expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.COSTING_DOCUMENT_CODE).isEmpty()) {
      assertEquals(expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.COSTING_DOCUMENT_CODE),
              actualPurchaseOrderMap.get(GR_COSTING_DOCUMENT_CODE));
    } else {
      Assert.assertNull(actualPurchaseOrderMap.get(GR_COSTING_DOCUMENT_CODE));
    }
    if (!expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.DELIVERY_NOTE).isEmpty()) {
      assertEquals(
          expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.DELIVERY_NOTE),
          actualPurchaseOrderMap.get(GR_DELIVERYNOTE));
    } else {
      Assert.assertNull(actualPurchaseOrderMap.get(GR_DELIVERYNOTE));
    }
    if (!expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.NOTES).isEmpty()) {
      assertEquals(
          expectedPurchaseOrderMap.get(IGoodsReceiptFeatureFileCommonKeys.NOTES),
          actualPurchaseOrderMap.get(COMMENTS));
    } else {
      Assert.assertNull(actualPurchaseOrderMap.get(COMMENTS));
    }
  }

  private String getStringfiedJson(Object localizedStringObject) throws Exception {
    ObjectMapper mapperObj = new ObjectMapper();
    return mapperObj.writeValueAsString(localizedStringObject);
  }
}
