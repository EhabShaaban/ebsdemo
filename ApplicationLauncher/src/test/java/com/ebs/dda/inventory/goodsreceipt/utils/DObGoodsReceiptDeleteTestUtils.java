package com.ebs.dda.inventory.goodsreceipt.utils;

import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNull;

public class DObGoodsReceiptDeleteTestUtils extends DObGoodsReceiptCommonTestUtils {

  public DObGoodsReceiptDeleteTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertNoEntityExistsWithCode(String userCode) throws Exception {
    DObGoodsReceiptGeneralModel dObGoodsReceiptGeneralModel =
        (DObGoodsReceiptGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGoodsReceiptByCode", userCode);
    assertNull(dObGoodsReceiptGeneralModel, userCode + " exist!");
  }
}
