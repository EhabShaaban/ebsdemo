package com.ebs.dda.inventory.goodsissue.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.inventorydocument.DObInventoryDocument;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObGoodsIssueDeleteTestUtils extends DObGoodsIssueCommonTestUtils {

  public DObGoodsIssueDeleteTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsRepeatExecution() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/inventory/goods-issue/DObGoodsIssueScript.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueSalesInvoiceData.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueActivationDetails.sql");
    str.append(',' + "db-scripts/inventory/goods-issue/IObGoodsIssueItem.sql");
    str.append("," + "db-scripts/inventory/goods-issue/IObGoodsIssueCompany.sql");
    return str.toString();
  }

  public void assertGoodsIssuesExist(DataTable goodsIssuesTable) {
    List<Map<String, String>> goodsIssueListMap =
        goodsIssuesTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsIssueMap : goodsIssueListMap) {
      DObGoodsIssueGeneralModel goodsIssueGeneralModel =
          (DObGoodsIssueGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsIssueViewAll", constructGoodsIssueQueryParams(goodsIssueMap));
      Assert.assertNotNull("goods issue doesn't exists", goodsIssueGeneralModel);
    }
  }

  private Object[] constructGoodsIssueQueryParams(Map<String, String> goodsIssueMap) {
    return new Object[] {
      goodsIssueMap.get(IFeatureFileCommonKeys.CODE),
      goodsIssueMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + goodsIssueMap.get(IFeatureFileCommonKeys.STATE) + "%",
      goodsIssueMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      goodsIssueMap.get(IFeatureFileCommonKeys.TYPE),
      goodsIssueMap.get(IFeatureFileCommonKeys.STOREHOUSE),
      goodsIssueMap.get(IFeatureFileCommonKeys.CUSTOMER)
    };
  }

  public Response deleteGoodsIssues(Cookie userCookie, String goodsIssueCode) {
    String deleteURL = IDObGoodsIssueRestURLS.DELETE_GOODS_ISSUE + goodsIssueCode;
    return sendDeleteRequest(userCookie, deleteURL);
  }

  public void assertThatGoodsIssueDeleted(String goodsIssueCode) {
    List<DObInventoryDocument> goodsIssueRows =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getGoodsIssueByCode", goodsIssueCode);
    Assert.assertEquals(0, goodsIssueRows.size());
  }

  public Map<String, String> getSectionsLockUrlsMap() {
    Map<String, String> sectionsLockUrls = new HashMap<>();
    return sectionsLockUrls;
  }
}
