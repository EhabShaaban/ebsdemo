package com.ebs.dda;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

public class CobCurrenciesTestUtils {

  private EntityManagerDatabaseConnector entityManagerDatabaseConnector;

  public CobCurrenciesTestUtils(EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    this.entityManagerDatabaseConnector = entityManagerDatabaseConnector;
  }

  public void assertThatCurrenciesExist(DataTable currenciesData) {
    List<Map<String, String>> expectedRecordsMapsList =
        currenciesData.asMaps(String.class, String.class);
    for (Map<String, String> expectedRecordMap : expectedRecordsMapsList) {
      assertCurrencyExistsByCodeAndName(
          expectedRecordMap.get(IFeatureFileCommonKeys.CODE),
          expectedRecordMap.get(IFeatureFileCommonKeys.NAME));
    }
  }

  private void assertCurrencyExistsByCodeAndName(String code, String name) {
    CObCurrency actualCurrency =
        (CObCurrency)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCurrencyByCodeAndName", code, name);

    assertNotNull(actualCurrency);
  }

}
