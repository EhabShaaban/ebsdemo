package com.ebs.dda;

import io.restassured.http.Cookie;
import io.restassured.path.json.exception.JsonPathException;
import io.restassured.response.Response;

import java.util.*;
import java.util.Map.Entry;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class UserActionsTestUtils {
  private static UserActionsTestUtils instance;
  private final String PASSWORD = "madina";
  private String loginURL;
  private String cookieName;
  private Map<String, UserInfo> userInfoMap;
  private Map<String, String> userLockedInstances;
  private CommonTestUtils commonTestUtils;

  public UserActionsTestUtils(Map<String, Object> properties) {
    commonTestUtils = new CommonTestUtils();
    commonTestUtils.setProperties(properties);
    userInfoMap = new HashMap<>();
    userLockedInstances = new HashMap<>();
    this.loginURL = getLoginURL();
    this.cookieName = getCookieName();
  }

  public static synchronized UserActionsTestUtils getInstance(Map<String, Object> properties) {
    if (instance == null) instance = new UserActionsTestUtils(properties);
    return instance;
  }

  public Response login(String userName) {
    Response loginResponse = createLoginResponse(userName);
    Cookie loginCookie = loginResponse.getDetailedCookie(cookieName);
    assertNotNull(loginCookie);
    setUserCookie(userName, loginCookie);
    return loginResponse;
  }

  public void logout(String userName) {
    Cookie cookie = userInfoMap.get(userName).getLoginCookie();
    commonTestUtils.sendGETRequest(cookie, "/logout");
    assertUserLoggedOut(userName);
  }

  public void assertUserLoggedOut(String userName) {
    Cookie cookie = getUserCookie(userName);
    Response response = commonTestUtils.sendGETRequest(cookie, "/loggedin");
    String loggedInUser = response.getBody().asString();
    assertTrue(loggedInUser.isEmpty());
  }

  public Cookie getUserCookie(String username) {
    return userInfoMap.get(username).getLoginCookie();
  }

  public Map<String, Cookie> getLoggedInUsersCookies() {
    Map<String, Cookie> loggedInUserCookies = new HashMap<>();
    Set<String> userNames = userInfoMap.keySet();
    for (String userName : userNames) {
      loggedInUserCookies.put(userName, userInfoMap.get(userName).getLoginCookie());
    }
    return loggedInUserCookies;
  }

  public Response getUserResponse(String username) {
    return userInfoMap.get(username).getResponse();
  }

  public void setUserResponse(String username, Response response) {
    userInfoMap.get(username).setResponse(response);
  }

  public Response lockSection(String username, String url, String code) {
    Cookie cookie = getUserCookie(username);
    Response response = commonTestUtils.sendGETRequest(cookie, url);
    try {
      String responseStatus = getResponseStatus(response);
      if (isSuccessResponse(responseStatus)) {
        userLockedInstances.put(username, code);
      }
    } catch (JsonPathException e) {
      // this empty catch because in case of user is unauthorized then user will
      // logout without sending back response so when we try to get status from
      // response
      // it throw exception
    }
    return response;
  }

  public Response unlockSection(String username, String restURL) {
    Cookie cookie = getUserCookie(username);
    Response response = commonTestUtils.sendGETRequest(cookie, restURL);
    try {
      String responseStatus = getResponseStatus(response);
      if (isSuccessResponse(responseStatus)) {
        removeUserCookie(username);
      }
    } catch (JsonPathException e) {
      // this empty catch because in case of user is unauthorized then user will
      // logout without sending back response so when we try to get status from
      // response
      // it throw exception
    }
    return response;
  }

  public Response unlockMultipleSectionsWithIterator(
      String restURL, String username, List<String> lockedSections, Iterator iterator) {
    Cookie cookie = getUserCookie(username);
    Response response =
        commonTestUtils.sendPostRequestWithObjectBody(cookie, restURL, lockedSections);
    try {
      String responseStatus = getResponseStatus(response);
      if (isSuccessResponse(responseStatus)) {
        removeUserCookieWithIterator(iterator);
      }
    } catch (JsonPathException e) {
      // this empty catch because in case of user is unauthorized then user will
      // logout without sending back response so when we try to get status from
      // response
      // it throw exception
    }
    return response;
  }

  public void unlockAllSections(String restURL, List<String> lockedSections) {
    Iterator<Map.Entry<String, String>> userIterator = getIteratorForUserLockedPurchaseOrders();
    while (userIterator.hasNext()) {
      String username = getUserNameFromUserLocksIterator(userIterator);
      String code = userLockedInstances.get(username);
      unlockMultipleSectionsWithIterator(restURL + code, username, lockedSections, userIterator);
    }
  }

  public void unlockAllSectionsNewStandard(
      String baseURL, String restURL, List<String> lockedSections) {
    Iterator<Map.Entry<String, String>> userIterator = getIteratorForUserLockedPurchaseOrders();
    while (userIterator.hasNext()) {
      String username = getUserNameFromUserLocksIterator(userIterator);
      String code = userLockedInstances.get(username);
      unlockMultipleSectionsWithIterator(
          baseURL + code + restURL, username, lockedSections, userIterator);
    }
  }

  public void unlockSectionForAllUsers(String restURL) {
    Iterator<Map.Entry<String, String>> userIterator = getIteratorForUserLockedPurchaseOrders();
    while (userIterator.hasNext()) {
      String username = getUserNameFromUserLocksIterator(userIterator);
      String code = userLockedInstances.get(username);
      unlockSectionWithIterator(username, restURL + code, userIterator);
    }
  }

  private void unlockSectionWithIterator(
      String username, String restURL, Iterator<Map.Entry<String, String>> userIterator) {
    Cookie cookie = getUserCookie(username);
    Response response = commonTestUtils.sendGETRequest(cookie, restURL);
    try {
      String responseStatus = getResponseStatus(response);
      if (isSuccessResponse(responseStatus)) {
        removeUserCookieWithIterator(userIterator);
      }
    } catch (JsonPathException e) {
      // this empty catch because in case of user is unauthorized then user will
      // logout without sending back response so when we try to get status from
      // response
      // it throw exception
    }
  }

  private String getUserNameFromUserLocksIterator(
      Iterator<Map.Entry<String, String>> userIterator) {
    Map.Entry<String, String> userLocks = userIterator.next();
    return userLocks.getKey();
  }

  private Iterator<Map.Entry<String, String>> getIteratorForUserLockedPurchaseOrders() {
    Set<Entry<String, String>> entrySet = userLockedInstances.entrySet();
    return entrySet.iterator();
  }

  private boolean isSuccessResponse(String responseStatus) {
    return CommonKeys.SUCCESS.equals(responseStatus);
  }

  private String getResponseStatus(Response response) {
    return response.body().jsonPath().get(CommonKeys.STATUS);
  }

  private void removeUserCookie(String username) {
    userLockedInstances.remove(username);
  }

  private void removeUserCookieWithIterator(Iterator iterator) {
    iterator.remove();
  }

  private void setUserCookie(String userName, Cookie loginCookie) {
    userInfoMap.put(userName, new UserInfo(loginCookie));
  }

  private Response createLoginResponse(String userName) {
    Map<String, String> formParams = new HashMap<>();
    formParams.put(CommonKeys.USER_NAME, userName);
    formParams.put(CommonKeys.PASSWORD, PASSWORD);
    return given().formParams(formParams).redirects().follow(true).post(loginURL);
  }

  private String getCookieName() {
    return commonTestUtils.getProperty(CommonKeys.COOKIE_NAME).toString();
  }

  private String getLoginURL() {
    return commonTestUtils.getProperty(CommonKeys.URL_PREFIX).toString()
        + commonTestUtils.getProperty(CommonKeys.LOGIN_URL).toString();
  }
}
