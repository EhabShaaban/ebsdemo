package com.ebs.dda;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.jpa.entities.AuthorizationGeneralModel;
import com.ebs.dac.security.jpa.entities.UserPermissionGeneralModel;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.masterdata.bank.CObBank;
import com.ebs.dda.jpa.masterdata.company.CObTreasury;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetails;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rest.utils.Status;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.RestAssured;
import io.restassured.config.JsonConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.http.Cookie;
import io.restassured.path.json.JsonPath;
import io.restassured.path.json.config.JsonPathConfig;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rest.AuthenticationController;

import javax.management.MBeanInfo;
import javax.management.MBeanOperationInfo;
import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;
import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommonTestUtils extends CommonTestUtils_ {

  private static final String SECURITYERROR_JSP = "securityerror";
  private static final String MISSING_FIELDS_KEY = "missingFields";
  private static final Logger log = LoggerFactory.getLogger(CommonTestUtils.class);
  protected final String BDK_COMPANY_CODE = "BDKCompanyCode";
  private final String SUBROLE = "Subrole";
  private final String ADMIN_SUBRROLE = "AdminSubRoles";
  private final String PERMISSION = "Permission";
  private final String CONDITION = "Condition";
  private final String USER = "User";
  protected EntityManagerDatabaseConnector entityManagerDatabaseConnector;
  protected Cookie cookie;
  private Map<String, Object> properties;
  private ObjectMapper mapperObj;

  public CommonTestUtils() {
    properties = new HashMap<>();
    mapperObj = new ObjectMapper();
  }

  public static Response sendPostRequest(
          Cookie loginCookie, String restURL, JsonObject jsonData, String urlPrefix) {
    String fullRestURL = urlPrefix + restURL;
    return given()
            .cookie(loginCookie)
            .contentType(ContentType.JSON)
            .body(jsonData.toString())
            .post(fullRestURL);
  }

  public static void addValueToJsonObject(JsonObject jsonObject, String property, String value) {
    if (value == null || value.isEmpty()) {
      jsonObject.add(property, null);
    } else if (value.equals("\"\"")) {
      jsonObject.addProperty(property, "");
    } else if (value.equals("N/A")) {
      return;
    } else {
      jsonObject.addProperty(property, value);
    }
  }

  public static void addBigDecimalValueToJsonObject(
          JsonObject newItemValueObject,
          String property,
          String value) {
    if (value.equals("N/A")) {
      return;
    }
    try {
      newItemValueObject.addProperty(property, new BigDecimal(value));
    } catch (NumberFormatException ex) {
      newItemValueObject.addProperty(property, value);
    }
  }

  public Response sendPostRequestWithAttachment(
          Cookie loginCookie, String restURL, File attachment, JsonObject body) {
    String fullRestURL = urlPrefix + restURL;
    return given()
            .cookie(loginCookie)
            .multiPart("file", attachment)
            .formParam("valueObject", body.toString())
            .post(fullRestURL);
  }

  public Response sendPostRequestWithFormParam(
          Cookie loginCookie, String restURL, JsonObject body) {
    String fullRestURL = urlPrefix + restURL;
    return given().cookie(loginCookie).formParam("valueObject", body.toString()).post(fullRestURL);
  }

  public void setEntityManagerDatabaseConnector(
          EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    this.entityManagerDatabaseConnector = entityManagerDatabaseConnector;
  }

  public void assertThatUsersWithRolesExist(DataTable usersDataTable) throws Exception {
    List<List<String>> users = usersDataTable.raw();
    for (int i = 1; i < users.size(); i++) {
      String userName = users.get(i).get(0);
      String roleExpression = users.get(i).get(1);
      assertThatUserExistsWithUserName(userName);
      assertThatRoleExists(roleExpression);
      assertThatUserHasRole(userName, roleExpression);
    }
  }

  private void assertThatUserExistsWithUserName(String userName) throws Exception {
    String query =
            "SELECT * FROM useraccount WHERE userid = (SELECT id From ebsuser WHERE username = ?)";
    PreparedStatement preparedStatement = constructPreparedStatement(query, userName);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    Assert.assertTrue("User " + userName + " doesn't exist!", resultSet.next());
    userRecord.put(userName, resultSet.getLong("id"));
  }

  private void assertThatUserHasRole(String userName, String roleExpression) throws Exception {
    String query = "SELECT * From userassignment WHERE userid = ? AND roleid = ?";
    Long userId = userRecord.get(userName);
    Long roleId = roleRecord.get(roleExpression);
    PreparedStatement preparedStatement = constructPreparedStatement(query, userId, roleId);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    Assert.assertTrue(
            "user assignment is not found: " + userName + " | " + roleExpression, resultSet.next());
  }

  private void assertThatRoleExists(String roleExpression) throws Exception {
    String query = "SELECT * From role WHERE roleexpression = ?";
    PreparedStatement preparedStatement = constructPreparedStatement(query, roleExpression);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    Assert.assertTrue("this role is not found: " + roleExpression, resultSet.next());
    roleRecord.put(roleExpression, resultSet.getLong("id"));
  }

  public void assertThatRolesAndSubRolesExist(DataTable rolesAndSubRolesTable) throws Exception {
    List<List<String>> rolesAndSubRoles = rolesAndSubRolesTable.raw();
    for (int i = 1; i < rolesAndSubRoles.size(); i++) {
      String roleExpression = rolesAndSubRoles.get(i).get(0);
      assertThatRoleExists(roleExpression);
      String subRoleExpression = rolesAndSubRoles.get(i).get(1);
      if (!subRoleExpression.equals("*") && !subRoleExpression.isEmpty()) {
        assertThatRoleExists(subRoleExpression);
        assertThatParentRoleHasSubRole(roleExpression, subRoleExpression);
      }
    }
  }

  public void assertThatResponseIsClientBypassingResponse(Response errorResponse) {
    Assert.assertEquals(460, errorResponse.getStatusCode());
    assertThatSecurityErrorPageLocationHeaderExists(errorResponse);
  }

  public void assertThatRolesHavePermissionsAndConditions(DataTable permissionDataTable)
          throws Exception {
    List<Map<String, String>> permissionRoles =
            permissionDataTable.asMaps(String.class, String.class);
    for (Map<String, String> permissionRole : permissionRoles) {
      String subRole = permissionRole.get(SUBROLE);
      String permission = permissionRole.get(PERMISSION);
      String condition = permissionRole.get(CONDITION);
      assertThatRoleExists(subRole);
      assertThatPermissionExists(permission);
      if (condition != null && !condition.isEmpty()) {
        String objectName = permission.substring(0, permission.indexOf(':'));
        assertThatAuthorizationConditionExists(condition, objectName);
      }
      assertThatRoleHasPermissionAndPermissionHasCondition(subRole, permission, condition);
    }
  }

  public void assertThatUserHaveNoPermissions(DataTable permissionDataTable) throws Exception {
    List<Map<String, String>> usersPermission =
            permissionDataTable.asMaps(String.class, String.class);
    for (Map<String, String> userPermission : usersPermission) {
      String userName = userPermission.get(USER);
      String permission = userPermission.get(PERMISSION);
      assertThatUserExistsWithUserName(userName);
      assertThatPermissionExists(permission);
      assertThatUserRoleDoesNotHavePermissions(userName, permission);
    }
  }

  public void assertThatUserRoleDoesNotHavePermissions(String userName, String permission) {
    UserPermissionGeneralModel userPermission =
            (UserPermissionGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getUserAndPermission", userName, permission);
    Assert.assertNull(userName + " | " + permission + " Exists!", userPermission);
  }

  public void assertThatUserHavePermissionsWithoutCondition(DataTable permissionDataTable)
          throws Exception {
    List<Map<String, String>> usersPermission =
            permissionDataTable.asMaps(String.class, String.class);
    for (Map<String, String> userPermission : usersPermission) {
      String user = userPermission.get(USER);
      String permission = userPermission.get(PERMISSION);
      String condition = userPermission.get(CONDITION);
      assertThatUserExistsWithUserName(user);
      assertThatPermissionExists(permission);
      assertThatUserRoleHavePermissionsWithNoCondition(user, permission, condition);
    }
  }

  public void assertThatUserRoleHavePermissionsWithNoCondition(
          String userName, String permission, String condition) throws Exception {
    AuthorizationGeneralModel userAndPermissionAndCondition =
            (AuthorizationGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getUserAndPermissionAndCondition", userName, permission, condition);
    Assert.assertNull(userAndPermissionAndCondition);
  }

  public Response sendGETRequest(Cookie loginCookie, String restURL) {
    RestAssured.config =
            RestAssuredConfig.newConfig()
                    .jsonConfig(
                            JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.BIG_DECIMAL));
    return given().cookie(loginCookie).contentType(ContentType.JSON).get(urlPrefix + restURL);  }

  public Response sendGETRequestWithDoubleNumbersType(Cookie loginCookie, String restURL) {
    RestAssured.config =
            RestAssuredConfig.newConfig()
                    .jsonConfig(
                            JsonConfig.jsonConfig().numberReturnType(JsonPathConfig.NumberReturnType.DOUBLE));
    return given().cookie(loginCookie).contentType(ContentType.JSON).get(urlPrefix + restURL);
  }

  public Response sendGETRequestWithQueryParams(
          Cookie loginCookie, String restURL, Map<String, Object> queryParams) {
    return given()
            .queryParams(queryParams)
            .cookie(loginCookie)
            .contentType(ContentType.JSON)
            .get(urlPrefix + restURL);
  }

  public Response sendGETRequestWithLocale(Cookie loginCookie, String locale, String restURL) {
    return given()
            .cookie(loginCookie)
            .contentType(ContentType.JSON)
            .header("Accept-Language", locale)
            .get(urlPrefix + restURL);
  }

  public Response sendPostRequest(Cookie loginCookie, String restURL, JsonObject jsonData) {
    return sendPostRequest(loginCookie, restURL, jsonData, urlPrefix);
  }

  public Response sendPostRequestWithObjectBody(Cookie loginCookie, String restURL, Object data) {
    String fullRestURL = urlPrefix + restURL;
    return given().cookie(loginCookie).contentType(ContentType.JSON).body(data).post(fullRestURL);
  }

  public Response sendDeleteRequest(Cookie loginCookie, String restURL) {
    return given().cookie(loginCookie).delete(urlPrefix + restURL);
  }

  protected void assertCorrectNumberOfRows(Response response, int size) {
    List<HashMap<String, Object>> responseData =
            response.body().jsonPath().getList(CommonKeys.DATA);
    Assert.assertEquals(size, responseData.size());
  }

  protected List<HashMap<String, Object>> getListOfMapsFromResponse(Response response) {
    ResponseBody responseBody = response.body();
    JsonPath jsonPath = responseBody.jsonPath();
    return jsonPath.getList(CommonKeys.DATA);
  }

  protected HashMap<String, Object> getMapFromResponse(Response response) {
    ResponseBody responseBody = response.body();
    JsonPath jsonPath = responseBody.jsonPath();
    return jsonPath.get(CommonKeys.DATA);
  }

  public Response requestToReadFilteredData(
          Cookie cookie,
          String prefixUrl,
          Integer pageNumber,
          Integer totalRecordsNumber,
          String fieldFilter) {
    String encodedFieldFilter = null;
    try {
      encodedFieldFilter = URLEncoder.encode(fieldFilter, StandardCharsets.UTF_8.name());
    } catch (UnsupportedEncodingException ignored) {
      log.error("URL Encoding is Failed");
    }
    String filteredReadUrl =
            constructReadUrl(prefixUrl, pageNumber, totalRecordsNumber, encodedFieldFilter);
    return sendGETRequest(cookie, filteredReadUrl);
  }

  public Response requestToReadFilteredDataWithLocale(
          Cookie cookie,
          String prefixUrl,
          Integer pageNumber,
          Integer totalRecordsNumber,
          String fieldFilter,
          String locale) {
    String encodedFieldFilter = null;
    try {
      encodedFieldFilter = URLEncoder.encode(escapeSpecialCharacters(fieldFilter), StandardCharsets.UTF_8.name());
    } catch (UnsupportedEncodingException ignored) {
      log.error("URL Encoding is Failed");
    }
    String filteredReadUrl =
            constructReadUrl(prefixUrl, pageNumber, totalRecordsNumber, encodedFieldFilter);
    return sendGETRequestWithLocale(cookie, locale, filteredReadUrl);
  }

  private String escapeSpecialCharacters(String fieldFilter) {
    StringBuilder newFilter = new StringBuilder();
    for (int i = 0; i < fieldFilter.length(); i++) {
      if (fieldFilter.charAt(i) == ';') {
        newFilter.append("semicolon");
        continue;
      }
      if (fieldFilter.charAt(i) == '/') {
        newFilter.append("forwardslash");
        continue;
      }
      if (fieldFilter.charAt(i) == '\\') {
        newFilter.append("backslash");
        continue;
      }
      newFilter.append(fieldFilter.charAt(i));
    }
    return newFilter.toString();
  }

  private String constructReadUrl(
          String prefixUrl, Integer pageNumber, Integer recordsPerPage, String filterString) {
    return prefixUrl
            + "/page="
            + (pageNumber - 1)
            + "/rows="
            + recordsPerPage
            + "/filters="
            + filterString;
  }

  public void assertResponseSuccessStatus(Response response) {
    String responseStatus = response.body().jsonPath().get(CommonKeys.STATUS);
    Assert.assertEquals(CommonKeys.SUCCESS, responseStatus);
    Assert.assertEquals(200, response.getStatusCode());
  }

  public void assertThatSuccessResponseHasMessageCode(Response response, String messageCode) {
    String actualMessageCode = response.body().jsonPath().get(CommonKeys.CODE);
    assertResponseSuccessStatus(response);
    Assert.assertEquals(messageCode, actualMessageCode);
  }

  public void assertResponseErrorStatus(Response response) {
    String responseStatus = response.body().jsonPath().get(CommonKeys.STATUS);
    Assert.assertEquals(CommonKeys.ERROR, responseStatus);
  }

  public void assertThatResponseHasErrorStatusAndMsgCodeEquals(
          Response response, String expectedMessageCode) {
    assertResponseErrorStatus(response);
    String actualMessageCode = response.body().jsonPath().get(CommonKeys.CODE);
    Assert.assertEquals(expectedMessageCode, actualMessageCode);
  }

  public void assertThatResponseHasFailStatusAndMsgCodeEquals(
          Response response, String expectedMessageCode) {
    String status = response.body().jsonPath().get(CommonKeys.STATUS);
    String actualMessageCode = response.body().jsonPath().get(CommonKeys.CODE);
    Assert.assertEquals(CommonKeys.FAIL, status);
    Assert.assertEquals(expectedMessageCode, actualMessageCode);
  }

  public void assertThatLockIsReleased(
          String userName, String code, String entityName, String sectionName) throws Exception {
    BDKUser user = getUser(userName);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    for (MBeanOperationInfo operation : getMBeanInfo(mbs, entityName).getOperations()) {
      if (operation.getName().equals(CommonKeys.CURRENT_USER_LOCK)) {
        Object[] parameters = new Object[] {user, sectionName, code};
        String[] parameterTypes =
                new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
        LockDetails lockReleaseDetails =
                (LockDetails)
                        mbs.invoke(
                                getEntityLockObjectName(entityName),
                                operation.getName(),
                                parameters,
                                parameterTypes);
        Assert.assertNull("the Lock is already exists ", lockReleaseDetails);
        break;
      }
    }
  }

  public String getEntityCodeById(Long id, String tableName) throws Exception {
    String entityCode = null;
    String query = "SELECT code From " + tableName + " WHERE id = ?";
    PreparedStatement preparedStatement = databaseConnector.createPreparedStatement(query);
    preparedStatement.setLong(1, id);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    if (resultSet.next()) {
      entityCode = resultSet.getString(CommonKeys.CODE);
    }
    return entityCode;
  }

  public void freeze(String frozenDateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    DateTime dateTime = formatter.parseDateTime(frozenDateTime);
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  public void freezeWithTimeZone(String frozenDateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy HH:mm a Z");
    DateTime dateTime = formatter.parseDateTime(frozenDateTime);
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  public void freezeDate(String frozenDateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
    DateTime dateTime = formatter.parseDateTime(frozenDateTime);
    DateTimeUtils.setCurrentMillisFixed(dateTime.getMillis());
  }

  public void unfreeze() {
    DateTimeUtils.setCurrentMillisSystem();
  }

  public void assertThatEditSessionTimeIsCorrect(int expectedEditSessionInMinutes)
          throws Exception {
    ObjectName name =
            new ObjectName(
                    "com.ebs.dac.foundation.realization.concurrency.policies:type=TimeoutEditTokenValidityPolicy");
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String getTimeoutInSeconds = "retrieveEditSessionTime";
    boolean methodCalled = false;
    try {
      int editSessionTimeInSeconds = (int) mbs.invoke(name, getTimeoutInSeconds, null, null);
      Assert.assertEquals(expectedEditSessionInMinutes * 60, editSessionTimeInSeconds);
      methodCalled = true;

    } catch (Exception ex) {
      Assert.assertFalse(methodCalled);
    }
    Assert.assertTrue(methodCalled);
  }

  public String getEnglishLocaleFromLocalizedResponse(String localizedString) throws JSONException {
    JSONObject jsonObject = new JSONObject(localizedString);
    return jsonObject.getJSONObject("values").getString("en");
  }

  public String getEnglishLocale(String localizedString) throws JSONException {
    JSONObject jsonObject = new JSONObject(localizedString);
    return jsonObject.getString("en");
  }

  public String getLocaleFromLocalizedResponse(String localizedString, String locale)
          throws JSONException {
    JSONObject jsonObject = new JSONObject(localizedString);
    return jsonObject.getJSONObject("values").getString(locale);
  }

  public ResultSet executeQuery(PreparedStatement preparedStatement) throws Exception {
    return databaseConnector.executeQuery(preparedStatement);
  }

  public void assertThatRolesAreAdminRoles(DataTable adminDataTable) {
    List<Map<String, String>> adminRoles = adminDataTable.asMaps(String.class, String.class);
    for (Map<String, String> tableRow : adminRoles) {
      String expectedAdminRole = tableRow.get(ADMIN_SUBRROLE);
      assertThatRoleIsAdminRole(expectedAdminRole);
    }
  }

  public void assertThatUserIsLoggedOut(Cookie cookie) {

    Response response = given().cookie(cookie).get(urlPrefix + "/loggedin");
    String loggedInUser = response.getBody().asString();
    Assert.assertTrue(loggedInUser.isEmpty());
  }

  public void assertThatNextInstanceCodeIsCorrect(
          String expectedNextCode, Response nextInstanceResponse) {
    String actualNextCode = nextInstanceResponse.body().jsonPath().getString("nextCode");
    Assert.assertEquals(expectedNextCode, actualNextCode);
  }

  public void assertThatPreviousInstanceCodeIsCorrect(
          String expectedPreviousCode, Response previousInstanceResponse) {
    String actualPreviousCode =
            previousInstanceResponse.body().jsonPath().getString("previousCode");
    Assert.assertEquals(expectedPreviousCode, actualPreviousCode);
  }

  public void assertTotalNumberOfRecordsAreCorrect(Response response, Integer totalRecordsNumber) {
    Integer actualTotalNumberOfRecords =
            response.body().jsonPath().get(CommonKeys.TOTAL_NUMBER_OF_RECOREDS);
    Assert.assertEquals(
            "[COMMON] Total number of records in response is not correct ",
            totalRecordsNumber,
            actualTotalNumberOfRecords);
  }

  public void assertThatAllowedActionsResponseIsCorrect(
          Response response, String allowedActions, int noOfActions) {
    if ("".equals(allowedActions)) {
      Assert.assertNull(response.body().jsonPath().get(CommonKeys.DATA));
      return;
    }
    String[] allowedActionsArray = allowedActions.replaceAll("\\s", "").split(",");
    Set<String> expectedAllowedActions = new HashSet<>(Arrays.asList(allowedActionsArray));
    Set<String> actualAllowedActions = getActualAllowedActionsAsSet(response);
    Assert.assertEquals(
            "[COMMON] Actions in response is not correct",
            expectedAllowedActions,
            actualAllowedActions);
    Assert.assertEquals(
            "[COMMON] Number of actions in response is not correct",
            noOfActions,
            actualAllowedActions.size());
  }

  public void assertThatTheFollowingActionsExist(Response response, String allowedActions) {
    if ("".equals(allowedActions)) {
      Assert.assertNull(response.body().jsonPath().get(CommonKeys.DATA));
      return;
    }

    String[] allowedActionsArray = allowedActions.replaceAll("\\s", "").split(",");
    Set<String> expectedAllowedActions = new HashSet<>(Arrays.asList(allowedActionsArray));
    Set<String> actualAllowedActions = getActualAllowedActionsAsSet(response);
    Assert.assertEquals(expectedAllowedActions, actualAllowedActions);
  }

  public void assertThatTheFollowingActionsExist(Response response, DataTable allowedActions) {

    Set<String> expectedAllowedActions = getExpectedAllowedActionsAsSet(allowedActions);
    Set<String> actualAllowedActions = getActualAllowedActionsAsSet(response);
    Assert.assertEquals(
            "[COMMON] Actions in response is not correct",
            expectedAllowedActions,
            actualAllowedActions);
  }

  public Response sendPUTRequest(Cookie loginCookie, String restURL, JsonObject jsonData) {
    String fullRestURL = urlPrefix + restURL;
    Response response =
            given()
                    .cookie(loginCookie)
                    .contentType(ContentType.JSON)
                    .body(jsonData.toString())
                    .put(fullRestURL);
    return response;
  }

  public void assertThatResponseTimeIsLessThan(Response response, String expectedResponseTime) {
    System.out.println("response.time() -> " + response.time());
    assertTrue(
            response.time() < (Long.valueOf(expectedResponseTime) * 1000),
            "Response Time is " + response.time());
  }

  protected Object getProperty(String key) {
    return properties.get(key);
  }

  public void setProperties(Map<String, Object> properties) {
    this.properties = properties;
    this.urlPrefix = getProperty(URL_PREFIX).toString();
  }

  protected List<Map<String, Object>> getDatabaseRecords(String query, Object... params)
          throws Exception {
    PreparedStatement preparedStatement = constructPreparedStatement(query, params);
    ResultSet resultSet = executeQuery(preparedStatement);
    return convertResultSetToListOfMaps(resultSet);
  }

  protected PreparedStatement constructPreparedStatement(String query, Object... params)
          throws Exception {
    PreparedStatement preparedStatement = databaseConnector.createPreparedStatement(query);
    int preparedStatementIndex = 1;
    for (Object param : params) {
      if (!isEmptyParameter(param)) {
        preparedStatement.setObject(preparedStatementIndex, param);
        preparedStatementIndex++;
      }
    }
    return preparedStatement;
  }

  protected void assertThatStatesArrayContainsValue(List<String> states, String targetValue) {
    Assert.assertTrue(states.contains(targetValue));
  }

  protected List<String> getStatesArray(String stringfiedStatesArray) {
    String statesArray = stringfiedStatesArray.replaceAll("[\\[\\]\\s\"]", "");
    return Arrays.asList(statesArray.split(","));
  }

  protected boolean isNotEmptyResultSet(ResultSet resultSet) throws SQLException {
    return resultSet.next();
  }

  protected void assertDateEqualsWithoutTime(String expectedDateStr, DateTime actualDate) {
    DateTime expectedDate = null;
    if (expectedDateStr != null) {
      DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
      expectedDate = formatter.parseDateTime(expectedDateStr).withZone(DateTimeZone.UTC);
    }
    Assert.assertEquals("The Dates are not equal ", expectedDate, actualDate);
  }

  protected void assertDateEquals(String expectedDateStr, DateTime actualDate) {
    DateTime expectedDate = null;
    if (expectedDateStr != null) {
      DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
      expectedDate = formatter.parseDateTime(expectedDateStr).withZone(DateTimeZone.UTC);
    }
    Assert.assertEquals("The Dates are not equal ", expectedDate, actualDate);
  }

  protected void assertThatDatesAreEqual(String expectedDate, String actualDate) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy HH:mm a");
    DateTimeFormatter databaseFormatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
    DateTime expectedDateTime = formatter.parseDateTime(expectedDate);
    DateTime actualDateTime = databaseFormatter.parseDateTime(actualDate);
    Assert.assertEquals("The Dates are not equal ", expectedDateTime, actualDateTime);
  }

  protected boolean isEmptyParameter(Object param) {
    if (param == null) return true;
    else return param.toString().isEmpty();
  }

  protected void assertStringArrayEqualLocalizedStringArray(
          String message, String stringArray, String localizedStringArray) {
    String[] expectedPUNames = stringArray.split(", ");
    String[] englishPUNames = getStringArrayFromLocalizedString(localizedStringArray);
    Assert.assertTrue(
            message,
            CollectionUtils.isEqualCollection(
                    Arrays.asList(expectedPUNames), Arrays.asList(englishPUNames)));
  }

  private String[] getStringArrayFromLocalizedString(String localizedString) {
    String[] purchaseUnitesNames = localizedString.split(", \\{");
    String[] englishPUNames = new String[purchaseUnitesNames.length];
    for (int i = 0; i < purchaseUnitesNames.length; i++) {
      int startSubString = purchaseUnitesNames[i].indexOf("en=") + 3;
      int endSubString = purchaseUnitesNames[i].indexOf("}}");
      englishPUNames[i] = purchaseUnitesNames[i].substring(startSubString, endSubString);
    }
    return englishPUNames;
  }

  private void assertThatParentRoleHasSubRole(String roleExpression, String subRoleExpression)
          throws Exception {
    String query = "SELECT * From roleassignment WHERE parentroleid = ? AND roleid = ?";
    Long parentRoleId = roleRecord.get(roleExpression);
    Long roleId = roleRecord.get(subRoleExpression);
    PreparedStatement preparedStatement = constructPreparedStatement(query, parentRoleId, roleId);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    Assert.assertTrue(
            roleExpression + "|" + subRoleExpression + " Doesn't exist", resultSet.next());
  }

  private void assertThatAuthorizationConditionExists(String condition, String objectName)
          throws Exception {
    String query = "SELECT * From AuthorizationCondition WHERE condition = ? And objectname= ?";
    PreparedStatement preparedStatement = constructPreparedStatement(query, condition, objectName);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    Assertions.assertTrue(
            resultSet.next(),
            "objectName: " + objectName + " doesn't exist with condition: " + condition);
    authzConditionRecord.put(condition, resultSet.getLong("id"));
  }

  private void assertThatPermissionExists(String permisson) throws Exception {
    String query = "SELECT * From permission WHERE permissionexpression LIKE ?";
    PreparedStatement preparedStatement = constructPreparedStatement(query, permisson);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    Assert.assertTrue("Message " + permisson, resultSet.next());
    permissionRecord.put(permisson, resultSet.getLong("id"));
  }

  private List<Map<String, Object>> convertResultSetToListOfMaps(ResultSet resultSet)
          throws SQLException {
    ResultSetMetaData metaData = resultSet.getMetaData();
    int colCount = metaData.getColumnCount();
    List<Map<String, Object>> actualDataList = new ArrayList<>();
    while (resultSet.next()) {
      Map<String, Object> actualData = new HashMap<>();
      for (int i = 1; i <= colCount; i++) {
        actualData.put(metaData.getColumnLabel(i), resultSet.getObject(i));
      }
      actualDataList.add(actualData);
    }
    return actualDataList;
  }

  private MBeanInfo getMBeanInfo(MBeanServer mbs, String entityName) throws Exception {
    return mbs.getMBeanInfo(getEntityLockObjectName(entityName));
  }

  private ObjectName getEntityLockObjectName(String entityName) throws Exception {
    return new ObjectName("com.ebs.dda.commands.general:name=" + entityName);
  }

  private BDKUser getUser(String userName) {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    return user;
  }

  private void assertThatRoleHasPermissionAndPermissionHasCondition(
          String role, String permission, String condition) {
    Object actualPermissionAndConditionAssignment =
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getRolePermissionAndConditionAssignment",
                    getRoleHasPermissionAndPermissionHasConditionParameters(role, permission, condition));
    Assert.assertNotNull(
            role + " | " + permission + " | " + condition + " doesn't exist ",
            actualPermissionAndConditionAssignment);
  }

  private Object[] getRoleHasPermissionAndPermissionHasConditionParameters(
          String roleExpression, String permissionExpression, String condition) {
    String objectName = permissionExpression.split(":")[0];
    String conditionValue = condition.isEmpty() ? null : condition;
    return new Object[] {
            roleExpression, permissionExpression, conditionValue, objectName, condition
    };
  }

  private void assertThatRoleIsAdminRole(String expectedAdminRole) {
    AuthenticationController authenticationController = new AuthenticationController();
    List<String> actualAdminRoles = authenticationController.getAdminRoles();
    Assert.assertTrue(actualAdminRoles.indexOf(expectedAdminRole) != -1);
  }

  private Set<String> getActualAllowedActionsAsSet(Response response) {
    ArrayList<String> ActualAllowedActions = response.body().jsonPath().get(CommonKeys.DATA);
    return new HashSet<>(ActualAllowedActions);
  }

  protected Set<String> getExpectedAllowedActionsAsSet(DataTable allowedActions) {
    List<List<String>> allowedActionsList = allowedActions.raw();
    Set<String> actionsSet = new HashSet<>();
    for (int i = 1; i < allowedActionsList.size(); i++) {
      String actionName = allowedActionsList.get(i).get(0);
      actionsSet.add(actionName);
    }
    return actionsSet;
  }

  public void assertThatUserHasAuthorizedReads(
          DataTable authorizedReadDataTable, Response lockResponse) {

    Map<String, List<String>> configuration = lockResponse.body().jsonPath().get("configuration");
    List<String> authorizedReadsActual = configuration.get("authorizedReads");
    List<String> authorizedReadExpected = getAuthorizedRead(authorizedReadDataTable);
    Assertions.assertTrue(
            CollectionUtils.isEqualCollection(authorizedReadExpected, authorizedReadsActual));
  }

  public void assertThatUserHasAuthorizedReads(
          String authorizedRead, Response lockResponse) {

    Map<String, List<String>> configuration = lockResponse.body().jsonPath().get("configuration");
    List<String> authorizedReadsActual = configuration.get("authorizedReads");
    List<String> authorizedReadExpected = getAuthorizedRead(authorizedRead);
    Assertions.assertTrue(
            CollectionUtils.isEqualCollection(authorizedReadExpected, authorizedReadsActual));
  }

  public void assertThatUserHasNoAuthorizedReads(Response lockResponse) {

    Map<String, List<String>> configuration = lockResponse.body().jsonPath().get("configuration");
    List<String> authorizedReadsActual = configuration.get("authorizedReads");
    Assertions.assertTrue(authorizedReadsActual.isEmpty());
  }

  public List<String> getAuthorizedRead(DataTable authorizedReadsData) {
    List<String> authorizedReads = new ArrayList<>();

    for (int i = 1; i < authorizedReadsData.raw().size(); i++) {
      authorizedReads.add(authorizedReadsData.raw().get(i).get(0));
    }
    return authorizedReads;
  }

  public List<String> getAuthorizedRead(String authorizedReadsData) {
    String[] authorizedReads = authorizedReadsData.replaceAll("\\s", "").split(",");
    return Arrays.asList(authorizedReads);
  }

  public Map<String, String> convertEmptyStringsToNulls(Map<String, String> map) {

    Map<String, String> expectedDataMap = new HashMap<>();
    map.entrySet()
            .forEach(
                    entry -> {
                      if (!entry.getValue().equals(""))
                        expectedDataMap.put(entry.getKey(), entry.getValue());
                    });
    return expectedDataMap;
  }

  public Map<String, String> convertDoubleQuotesToEmptyStrings(Map<String, String> map) {

    map = convertEmptyStringsToNulls(map);
    Map<String, String> expectedDataMap = new HashMap<>();
    map.entrySet()
            .forEach(
                    entry -> {
                      if (entry.getValue().equals("\"\"")) {
                        expectedDataMap.put(entry.getKey(), "");
                      } else {
                        expectedDataMap.put(entry.getKey(), entry.getValue());
                      }
                    });
    return expectedDataMap;
  }

  public String convertEmptyStringToNull(String entry) {
    if (entry != null && entry.isEmpty()) {
      return null;
    }
    return entry;
  }

  public String getEnglishValue(Object object) throws Exception {
    return getEnglishLocaleFromLocalizedResponse(mapperObj.writeValueAsString(object));
  }

  public void assertThatFieldHasErrorMessageCode(
          Response response, String messageCode, String objectKey) {
    HashMap<String, Object> errors = response.body().jsonPath().get("errorsValidationResult");
    String objectCode = (String) errors.get(objectKey);
    Assert.assertEquals(messageCode, objectCode);
  }

  protected String getFeatureValueIfEmpty(String value) {
    return value != null && value.equals("\"\"") ? "" : value;
  }

  public String getFilters(DataTable filters) throws Exception {
    String filter = "";
    List<Map<String, String>> filterList = filters.asMaps(String.class, String.class);
    for (int i = 0; i < filterList.size(); i++) {
      String fieldName = filterList.get(i).get(IFeatureFileCommonKeys.FIELD_NAME);
      String operation = filterList.get(i).get(IFeatureFileCommonKeys.OPERATION);
      String value = filterList.get(i).get(IFeatureFileCommonKeys.VALUE);
      filter += fieldName + "-" + operation + "-" + translateFieldValue(fieldName, value);
      if (i != filterList.size() - 1) {
        filter += Status.FILTER_VALUE_SEPARATOR;
      }
    }
    return filter;
  }

  private String translateFieldValue(String fieldName, String value) throws ParseException {
    if (fieldName.toLowerCase().contains("date")) {
      return String.valueOf(getDateTimeInMilliSecondsFormat(value));
    }
    return value;
  }

  public Long getDateTimeInMilliSecondsFormat(String dateTime) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    Date date = dateFormat.parse(dateTime);
    return date.getTime();
  }

  public String getContainsFilterField(String fieldName, String filter) {
    return fieldName + Status.FILTER_PART_SEPARATOR + "contains" + Status.FILTER_PART_SEPARATOR + filter;
  }

  public String getBetweenFilterField(
          String firstFieldName, String firstFilter, String secondFieldName, String secondFilter) {
    return firstFieldName
            + "-after-"
            + firstFilter
            + Status.FILTER_VALUE_SEPARATOR
            + secondFieldName
            + "-before-"
            + secondFilter;
  }

  public String getEqualsFilterField(String fieldName, String filter) {
    return fieldName + Status.FILTER_PART_SEPARATOR + "equals" + Status.FILTER_PART_SEPARATOR + filter;
  }

  public void assertThatMandatoriesFieldsReturned(
          Response lockResponse, DataTable mandatoriesDataTable) {
    Map<String, List<String>> configuration = lockResponse.body().jsonPath().get("configuration");
    List<String> actualMandatoriesFields = configuration.get("mandatories");
    List<String> expectedMandatoriesFields = getAuthorizedRead(mandatoriesDataTable);
    for (String mandatory : expectedMandatoriesFields) {
      assertTrue(
              actualMandatoriesFields.contains(mandatory),
              "MandatoryField: " + mandatory + " didn't return in actual mandatories!");
    }
  }

  public void assertThatResponseMandatoriesListIsEmpty(Response lockResponse) {
    Map<String, List<String>> configuration = lockResponse.body().jsonPath().get("configuration");
    List<String> actualMandatoriesFields = configuration.get("mandatories");
    Assert.assertTrue("Mandatories is not empty", actualMandatoriesFields.isEmpty());
  }

  public void assertThatEditableFieldsReturned(Response lockResponse, DataTable editableDataTable) {
    Map<String, List<String>> configuration = lockResponse.body().jsonPath().get("configuration");
    List<String> actualEditablesFields = configuration.get("editables");
    List<String> expectedEditablesFields = getAuthorizedRead(editableDataTable);
    Assert.assertTrue(
            "Editables not equal ",
            CollectionUtils.isEqualCollection(expectedEditablesFields, actualEditablesFields));
  }

  public void assertThatSecurityErrorPageLocationHeaderExists(Response response) {
    String errorPageLocation = response.getHeader("Location");
    Assert.assertNotNull(errorPageLocation);
    Assert.assertEquals(SECURITYERROR_JSP, errorPageLocation);
  }

  public void assertThatPurchaseResponsibleExist(List<String> list) {
    String code = list.get(0);
    String name = list.get(1);
    String purchaseResponsibleName = getPurchaseResponsibleNameByCode(code);
    Assert.assertEquals(purchaseResponsibleName, name);
  }

  private String getPurchaseResponsibleNameByCode(String code) {
    return (String)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getPurchaseResponsibleNameByCode", code);
  }

  public void assertThatPurchaseUnitsExist(DataTable purchaseUnits) {
    List<List<String>> purchaseUnitsList = purchaseUnits.raw();
    for (int i = 1; i < purchaseUnitsList.size(); i++) {
      String code = purchaseUnitsList.get(i).get(0);
      String name = purchaseUnitsList.get(i).get(1);
      String purchaseUnitName = getPurchaseUnitNameByCode(code);
      Assert.assertEquals(purchaseUnitName, name);
    }
  }

  public String getPurchaseUnitNameByCode(String code) {
    return (String)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getPurchaseUnitNameByCode", code);
  }

  public void assertThatBanksExist(DataTable bankTable) {
    List<Map<String, String>> banks = bankTable.asMaps(String.class, String.class);
    for (Map<String, String> bank : banks) {
      String code = bank.get(IFeatureFileCommonKeys.CODE);
      String name = bank.get(IFeatureFileCommonKeys.NAME);
      assertThatBankExistWithCode(code, name);
    }
  }

  private void assertThatBankExistWithCode(String bankCode, String bankName) {
    CObBank bank =
            (CObBank)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getBanks", bankCode, bankName);
    Assert.assertNotNull("Bank with code " + bankCode + " doesn't exist!", bank);
  }

  public void assertThatTreasuriesExist(DataTable treasuriesTable) {
    List<Map<String, String>> treasuries = treasuriesTable.asMaps(String.class, String.class);
    for (Map<String, String> treasury : treasuries) {
      String code = treasury.get(IFeatureFileCommonKeys.CODE);
      String name = treasury.get(IFeatureFileCommonKeys.NAME);
      assertThatTreasuriesExistWithCode(code, name);
    }
  }

  private void assertThatTreasuriesExistWithCode(String treasuryCode, String treasuryName) {
    CObTreasury treasury =
            (CObTreasury)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getTreasuries", treasuryCode, treasuryName);
    Assert.assertNotNull("Treasure with code " + treasuryCode + " doesn't exist!", treasury);
  }

  public void assertThatCompaniesHaveBankAccounts(DataTable companyBanks) {
    List<Map<String, String>> companiesBanks = companyBanks.asMaps(String.class, String.class);
    for (Map<String, String> companiesBank : companiesBanks) {
      assertThatCompanyHasThisBankAccount(companiesBank);
    }
  }

  public void assertThatCompaniesHaveBankAccountsWithIDs(DataTable companyBanks) {
    List<Map<String, String>> companiesBanks = companyBanks.asMaps(String.class, String.class);
    for (Map<String, String> companiesBank : companiesBanks) {
      assertThatCompanyHasThisBankAccountWithIDs(companiesBank);
    }
  }

  private void assertThatCompanyHasThisBankAccount(Map<String, String> companyBank) {
    IObCompanyBankDetails companyBankDetails =
            (IObCompanyBankDetails)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getCompanyBanks", constructCompanyBanksData(companyBank));
    Assert.assertNotNull(companyBankDetails);
  }

  private void assertThatCompanyHasThisBankAccountWithIDs(Map<String, String> companyBank) {
    IObCompanyBankDetails companyBankDetails =
            (IObCompanyBankDetails)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getCompanyBanksWithIDs", constructCompanyBanksDataWithIDs(companyBank));
    Assert.assertNotNull(
            "bank account number "
                    + companyBank.get(IFeatureFileCommonKeys.ACCOUNT_NUMBER)
                    + " Doesn't exist!",
            companyBankDetails);
  }

  private Object[] constructCompanyBanksData(Map<String, String> companyBank) {
    return new Object[] {
            companyBank.get(IFeatureFileCommonKeys.ACCOUNT_NUMBER),
            companyBank.get(IFeatureFileCommonKeys.BANK_CODE),
            companyBank.get(IFeatureFileCommonKeys.COMPANY_CODE)
    };
  }

  private Object[] constructCompanyBanksDataWithIDs(Map<String, String> companyBank) {
    return new Object[] {
            Integer.parseInt(companyBank.get(IFeatureFileCommonKeys.ID)),
            companyBank.get(IFeatureFileCommonKeys.ACCOUNT_NUMBER),
            companyBank.get(IFeatureFileCommonKeys.BANK),
            companyBank.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public CObStorehouseGeneralModel getStorehouseByCode(String code) throws Exception {
    return (CObStorehouseGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getStorehouseByCode", code);
  }

  public MObVendorGeneralModel getVendorGeneralModelCode(String code) throws Exception {
    return (MObVendorGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("getVendorByCode", code);
  }

  public void assertThatPurchaseUnitExist(String purchaseUnitCode) throws Exception {
    String purchaseUnitName = getPurchaseUnitNameByCode(purchaseUnitCode);
    Assert.assertNotNull(purchaseUnitName);
  }

  public Double getDouble(Object value) {
    return value == null || String.valueOf(value).isEmpty() || String.valueOf(value).equals("null")
            ? null
            : Double.valueOf(value.toString());
  }

  public BigDecimal getBigDecimal(Object value) {
    return value == null || String.valueOf(value).isEmpty() || String.valueOf(value).equals("null")
            ? null
            : new BigDecimal(value.toString());
  }

  public String getDateTimeInMachineTimeZone(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a Z");
    return formatter.parseDateTime(dateTime).toString("dd-MMM-yyyy hh:mm a");
  }

  public void assertThatResourceIsLockedByUser(
          String userName, String objectCode, String sectionName, String jmxLockBeanName)
          throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name = new ObjectName("com.ebs.dda.commands.general:name=" + jmxLockBeanName);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, objectCode};
    String[] parameterTypes =
            new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
            (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);
    Assert.assertNotNull("[COMMON][Lock/Unlock] LockDetails is null", lockReleaseDetails);
  }

  public void assertThatLastEntityCodeIs(String entityName, String entityCode) {
    Object lastEntityCode =
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getLastInsertedEntityCodeByEntityType", entityName);
    Assert.assertEquals(entityCode, lastEntityCode);
  }

  public void assertTotalNumberOfRecordsEqualResponseSize(
          Response response, int totalRecordsNumber) {
    List<HashMap<String, Object>> responseData = getListOfMapsFromResponse(response);
    Assert.assertEquals(responseData.size(), totalRecordsNumber);
  }

  public List<HashMap<String, Object>> readListOfMapFromMap(
          HashMap<String, Object> map, String objectName) throws Exception {
    com.fasterxml.jackson.databind.ObjectMapper mapper =
            new com.fasterxml.jackson.databind.ObjectMapper();
    return mapper.readValue(
            String.valueOf(map.get(objectName)), new TypeReference<List<HashMap<String, Object>>>() {});
  }

  public HashMap<String, Object> readMapFromString(HashMap<String, Object> map, String objectName)
          throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(String.valueOf(map.get(objectName)), HashMap.class);
  }

  public void assertTotalNumberOfRecordsReturned(Response response, Integer expectedRecordsNumber) {
    List<HashMap<String, Object>> actualItemsList = getListOfMapsFromResponse(response);
    Assert.assertEquals(
            "Records number doesn't match", expectedRecordsNumber.intValue(), actualItemsList.size());
  }

  protected void assertThatBusinessUnitsAreEqual(
          String objectCode, List<String> expectedBusinessUnitCodes, String actualBusinessUnitCodes) {
    for (String expectedBusinessUnitCode : expectedBusinessUnitCodes) {
      Assert.assertTrue(
              "expectedBusinessUnitCode "
                      + expectedBusinessUnitCode
                      + " Doesn't exist for code: "
                      + objectCode,
              actualBusinessUnitCodes.contains(expectedBusinessUnitCode.trim()));
    }
  }

  protected List<String> getBusinessUnitCodes(String[] businessUnits) {
    List<String> businessUnitsCodes = new ArrayList<>();
    for (String businessUnit : businessUnits) {
      businessUnitsCodes.add(businessUnit.split(" - ")[0]);
    }
    return businessUnitsCodes;
  }

  public String getCodeFromCodeName(String codeName) {
    return codeName.split(" - ")[0];
  }

  public String getNameFromCodeName(String codeName) {
    return codeName.split(" - ")[1];
  }

  public void assertThatResponseHasMissingFields(Response response, String missingFields) {
    Map<String, List<String>> missingFieldsList =
            response.body().jsonPath().get(MISSING_FIELDS_KEY);

    Set<String> actualMissingFieldsSet = new HashSet<>();
    String[] expectedList = missingFields.split(",");

    for (int i = 0; i < expectedList.length; i++) expectedList[i] = expectedList[i].trim();

    Set<String> expectedMissingFieldsSet = new HashSet<>(Arrays.asList(expectedList));

    for (Map.Entry<String, List<String>> entry : missingFieldsList.entrySet()) {
      actualMissingFieldsSet.addAll(entry.getValue());
    }

    Assert.assertEquals(expectedMissingFieldsSet, actualMissingFieldsSet);
    assertEquals(expectedMissingFieldsSet.size(), actualMissingFieldsSet.size());
  }

  public void assertThatDocumentOwnersAreExist(DataTable documentOwnersDT) {
    List<Map<String, String>> documentOwnersList =
            documentOwnersDT.asMaps(String.class, String.class);

    for (Map<String, String> documentOwner : documentOwnersList) {
      Object[] params = constructQueryParams(documentOwner);

      DocumentOwnerGeneralModel actualDocumentOwner =
              (DocumentOwnerGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getDocumentOwnerByAllParams", params);

      assertNotNull(
              "Document Owner does not exist " + Arrays.toString(params), actualDocumentOwner);
    }
  }

  private Object[] constructQueryParams(Map<String, String> documentOwner) {
    Long ownerId = Long.parseLong(documentOwner.get(IFeatureFileCommonKeys.ID));
    String ownerName = documentOwner.get(IFeatureFileCommonKeys.NAME);
    String objectName = documentOwner.get(IFeatureFileCommonKeys.BUSINESS_OBJECT);
    String permissionExpression =
            objectName + ":" + documentOwner.get(IFeatureFileCommonKeys.PERMISSION);
    String condition = documentOwner.get(IFeatureFileCommonKeys.CONDITION);

    Object[] params =
            new Object[] {condition, ownerId, ownerName, objectName, permissionExpression};
    return params;
  }

  public boolean isMapValueEmptyOrNull(String mapValue) {
    return (mapValue == null || mapValue.isEmpty());
  }

  public void createLastEntityCode(String simpleName, String journalEntryCode) {
    entityManagerDatabaseConnector.executeInsertQueryForObject(
            "updateLastInsertedEntityCodeByEntityType", simpleName, journalEntryCode, simpleName);
  }

  public void assertThatDocumentOwnersAreRetrieved(
          Response userResponse, DataTable documentOwnersDT) {
    List<Map<String, String>> expectedDocOwners =
            documentOwnersDT.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualDocOwners = getListOfMapsFromResponse(userResponse);
    int index = 0;
    for (Map<String, String> expectedDocOwner : expectedDocOwners) {
      HashMap<String, Object> actualDocOwner = actualDocOwners.get(index++);
      MapAssertion mapAssertion = new MapAssertion(expectedDocOwner, actualDocOwner);
      mapAssertion.assertValue(IFeatureFileCommonKeys.NAME, IDocumentOwnerGeneralModel.USER_NAME);
    }
  }

  public void assertThatTotalNumberOfDocumentOwnersIsCorrect(
          Integer expectedDocumentOwnersCount, String objectSysName) {
    Object actualDocumentOwnersCount =
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getDocumentOwnersTotalCount", objectSysName);

    Assert.assertEquals(Long.valueOf(expectedDocumentOwnersCount), actualDocumentOwnersCount);
  }

  public void assertLastJournalEntry(String journalEntryCode) {
    createLastEntityCode(DObJournalEntry.class.getSimpleName(), journalEntryCode);
  }

  public void assertThatResponseHasJournalEntryCode(Response response, String journalEntryCode) {
    String responseCode = response.body().jsonPath().get(CommonControllerUtils.USER_CODE_KEY);
    Assert.assertEquals("Journal Entry Code is not generated correctly", journalEntryCode, responseCode);
  }
}

