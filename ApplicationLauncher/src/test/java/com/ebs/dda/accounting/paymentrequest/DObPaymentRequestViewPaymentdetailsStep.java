package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import org.springframework.beans.factory.InitializingBean;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestDeleteTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPostCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewPaymentDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPaymentRequestViewPaymentdetailsStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObPaymentRequestViewPaymentDetailsTestUtils utils;
  private DObPaymentRequestDeleteTestUtils paymentRequestDeleteTestUtils;
  private DObPaymentRequestPostCommonTestUtils paymentRequestPostCommonTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestViewPaymentDetailsTestUtils(getProperties());
    paymentRequestDeleteTestUtils = new DObPaymentRequestDeleteTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    paymentRequestDeleteTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    paymentRequestPostCommonTestUtils = new DObPaymentRequestPostCommonTestUtils(getProperties());
    paymentRequestPostCommonTestUtils
        .setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObPaymentRequestViewPaymentdetailsStep() {

    Given("^the following PaymentDetails for PaymentRequests exist:$",
        (DataTable paymentDetailsTable) -> {
          utils.assertThatTheFollowingPRPaymentDetailsExist(paymentDetailsTable);
        });

    When(
        "^\"([^\"]*)\" requests to view PaymentDetails section of PaymentRequest with code \"([^\"]*)\"$",
        (String userName, String paymentRequestCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getViewPaymentDetailsUrl(paymentRequestCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of PaymentDetails section for PaymentRequest with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String paymentRequestCode, String userName, DataTable paymentDetailsTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayedInPaymentDetails(response, paymentDetailsTable);
        });

  }

  @Before
  public void beforeViewPaymentDetails(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View PaymentDetails")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(DObPaymentRequestViewPaymentDetailsTestUtils
          .getPaymentRequestDbScriptsOneTimeExecution());
      databaseConnector
      .executeSQLScript(DObPaymentRequestViewPaymentDetailsTestUtils.clearPaymentRequests());
      databaseConnector
      .executeSQLScript(DObVendorInvoiceTestUtils.clearVendorInvoices());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View PaymentDetails")) {
      databaseConnector.closeConnection();
    }
  }

}
