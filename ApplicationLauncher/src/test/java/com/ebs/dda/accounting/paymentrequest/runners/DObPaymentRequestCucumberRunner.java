package com.ebs.dda.accounting.paymentrequest.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/accounting/payment-request/PR_Create_HP.feature",
      "classpath:features/accounting/payment-request/PR_Create_Val.feature",
      "classpath:features/accounting/payment-request/PR_Create_Auth.feature",
      "classpath:features/accounting/payment-request/PR_ViewAll.feature",
      "classpath:features/accounting/payment-request/PR_View_GeneralData.feature",
      "classpath:features/accounting/payment-request/PR_View_Company.feature",
      "classpath:features/accounting/payment-request/PR_View_PaymentDetails.feature",
      "classpath:features/accounting/payment-request/PR_CostFactorItems_Dropdown.feature",
      "classpath:features/accounting/payment-request/PR_Treasuries_Dropdown.feature",
      "classpath:features/accounting/payment-request/PR_View_AccountingDetails.feature",
      "classpath:features/accounting/payment-request/PR_View_PostingDetails.feature",
      "classpath:features/accounting/payment-request/PR_RequestEdit_PaymentDetails.feature",
      "classpath:features/accounting/payment-request/PR_Save_PaymentDetails_HP.feature",
      "classpath:features/accounting/payment-request/PR_Save_PaymentDetails_Auth.feature",
      "classpath:features/accounting/payment-request/PR_Save_PaymentDetails_Val.feature",
      "classpath:features/accounting/payment-request/PR_Delete.feature",
      "classpath:features/accounting/payment-request/PR_AllowedActions.feature",
      "classpath:features/accounting/payment-request/PR_Activate_PaymentForVendorAgainstPO_HP.feature",
      "classpath:features/accounting/payment-request/PR_Activate_PaymentForVendorAgainstVI_HP.feature",
      "classpath:features/accounting/payment-request/PR_Activate_GeneralPaymentForPO_HP.feature",
      "classpath:features/accounting/payment-request/PR_Activate_CreditNotePayment_HP.feature",
      "classpath:features/accounting/payment-request/PR_Activate_Val.feature",
      "classpath:features/accounting/payment-request/PR_Activate_Auth.feature",
      "classpath:features/accounting/payment-request/PR_Activate_Concurrent.feature",
      "classpath:features/accounting/vendor-invoice/VI_Dropdowns.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Dropdowns.feature",
      "classpath:features/accounting/payment-request/PR_Employees_Dropdown.feature",
      "classpath:features/accounting/payment-request/PaymentRequest_Dropdown.feature",
      "classpath:features/accounting/payment-request/PR_DueDocumentType_Dropdown.feature",
      "classpath:features/accounting/payment-request/PR_DocumentOwner_Dropdown.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.paymentrequest"},
    strict = true,
    tags = "not @Future")
public class DObPaymentRequestCucumberRunner {}
