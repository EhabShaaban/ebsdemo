package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostSectionNames;
import com.ebs.dda.accounting.landedcost.utils.IObLandedCostSaveCostFactorItemHappyPathTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class IObLandedCostSaveCostFactorItemStep extends SpringBootRunner implements En {

	private static boolean hasBeenExecuted = false;
	private IObLandedCostSaveCostFactorItemHappyPathTestUtils utils;

	public IObLandedCostSaveCostFactorItemStep() {

		Given("^the following CostFactorItems exist with the following details:$",
						(DataTable costFactorItemsDataTable) -> {
							utils.assertThatThoseItemsExist(costFactorItemsDataTable);
						});

		Given("^CostFactorItems section of LandedCost with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
						(String landedCostCode, String userName, String lockTime) -> {
							utils.freeze(lockTime);
							String lockUrl = utils.getLockUrl(landedCostCode);
							Response response = userActionsTestUtils.lockSection(userName, lockUrl,
											landedCostCode);
							userActionsTestUtils.setUserResponse(userName, response);

						});

		When("^\"([^\"]*)\" saves the following new costFactorItem to CostFactorItems section of LandedCost with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
						(String userName, String landedCostCode, String savingTime,
										DataTable itemsDataTable) -> {
							utils.freeze(savingTime);
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							Response response = utils.saveItemToLandedCost(cookie, landedCostCode,
											itemsDataTable);
							userActionsTestUtils.setUserResponse(userName, response);

						});
		Then("^LandedCost with Code \"([^\"]*)\" is updated as follows:$",
						(String landedCostCode, DataTable updatedDetailsDataTable) -> {
							utils.assertThatLandedCostIsUpdated(landedCostCode,
											updatedDetailsDataTable);

						});

		Then("^CostFactorItems section of LandedCot with code \"([^\"]*)\" is created as follows:$",
						(String landedCostCode, DataTable costFactorItemsDataTable) -> {
							utils.assertThatLandedCostHasThoseCostFactorItem(landedCostCode,
											costFactorItemsDataTable);
						});
		Then("^the lock by \"([^\"]*)\" on CostFactorItems section of LandedCost with Code \"([^\"]*)\" is released$",
						(String userName, String landedCostCode) -> {
							utils.assertThatLockIsReleased(userName, landedCostCode,
											IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);
						});
        And("^CostFactorItemsSummary section is updated as follows:$",
                (DataTable itemsSummaryDataTable) -> {
                    utils.assertThatLandedCostHasItemsSummary(itemsSummaryDataTable);

                });
		Given("^the following Items are not Cost Factor Items:$",
						(DataTable costFactorItemsDataTable) -> {
							utils.assertThatThoseItemsAreNotCostFactorItems(
											costFactorItemsDataTable);
						});

	}

	@Before
	public void setup(Scenario scenario) throws Exception {
		Map<String, Object> properties = getProperties();
		utils = new IObLandedCostSaveCostFactorItemHappyPathTestUtils(properties,
						entityManagerDatabaseConnector);
		if (contains(scenario.getName(), "Save new LandedCost CostFactorItem")) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
				hasBeenExecuted = true;
			}
			databaseConnector.executeSQLScript(utils.getDbScripts());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), "Save new LandedCost CostFactorItem")) {
			utils.unfreeze();
			userActionsTestUtils.unlockAllSections(IDObLandedCostRestUrls.UNLOCK_LOCKED_SECTIONS,
							utils.getSectionsNames());
			databaseConnector.closeConnection();
		}
	}
}
