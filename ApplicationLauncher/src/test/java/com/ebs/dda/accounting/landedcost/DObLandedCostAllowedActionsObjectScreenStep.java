package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostAllowedActionsObjectScreenTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObLandedCostAllowedActionsObjectScreenStep extends SpringBootRunner
    implements En, InitializingBean {

  private static boolean hasBeenExecutedOnce = false;
  private DObLandedCostAllowedActionsObjectScreenTestUtils utils;

  public DObLandedCostAllowedActionsObjectScreenStep() {
    Given(
        "^the following LandedCosts exist:$",
        (DataTable landedCostDataTable) -> {
          utils.assertLandedCostExists(landedCostDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of LandedCost with code \"([^\"]*)\"$",
        (String userName, String landedCostCode) -> {
          String url = IDObLandedCostRestUrls.AUTHORIZED_ACTIONS + '/' + landedCostCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObLandedCostAllowedActionsObjectScreenTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Landed Cost Object Screen")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScripts());
    }
  }
}
