package com.ebs.dda.accounting.settlement.utils;

import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Map;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.settlement.IDObSettlementRestURLs;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;

public class DObSettlementDeleteAccountingDetailsTestUtils extends DObSettlementsCommonTestUtils {

	public DObSettlementDeleteAccountingDetailsTestUtils(Map<String, Object> properties,
					EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
		super(properties);
		setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	public String getDeleteAccountingDetailRestUrl(String accountingDetailId,
					String settlementCode) {
		StringBuilder deleteUrl = new StringBuilder();
		deleteUrl.append(String.format(IDObSettlementRestURLs.DELETE_ACCOUNTING_DETAILS,
						settlementCode));
		deleteUrl.append(Long.parseLong(accountingDetailId));
		return deleteUrl.toString();
	}

	public void assertThatSettlementAccountingDetailDoesNotExist(String detailId,
					String settlementCode) {
		IObSettlementAccountDetailsGeneralModel accountDetailsGeneralModel = (IObSettlementAccountDetailsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult(
										"getSettlementAccountingDetailByIdAndSettlementCode",
										settlementCode, detailId);
		assertNull(accountDetailsGeneralModel);

	}
}
