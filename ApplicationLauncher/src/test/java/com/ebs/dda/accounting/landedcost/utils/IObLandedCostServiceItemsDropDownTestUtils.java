package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueCommonTestUtils;
import com.ebs.dda.jpa.accounting.landedcost.CObLandedCostType;
import com.ebs.dda.jpa.accounting.landedcost.ICObLandedCost;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import com.ebs.dda.masterdata.item.IMObItemRestUrls;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class IObLandedCostServiceItemsDropDownTestUtils extends DObLandedCostTestUtils {

    private ObjectMapper objectMapper;

    public IObLandedCostServiceItemsDropDownTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        super(properties);
        setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }


    public String readAllLandedCostServiceItems(String landedCostCode) {
        return IDObLandedCostRestUrls.GET_URL + IDObLandedCostRestUrls.READ_SERVICE_ITEMS + "/"+ landedCostCode;
    }

    public void assertResponseMatchesExpected(Response response, DataTable landedCostServiceItemsDataTable)
            throws Exception {
        List<Map<String, String>> landedCostServiceItemsExpectedList =
                landedCostServiceItemsDataTable.asMaps(String.class, String.class);
        List<HashMap<String, Object>> landedCostServiceActualList = getListOfMapsFromResponse(response);

        for (int i = 0; i < landedCostServiceActualList.size(); i++) {
            String landedCostServiceItemsActualCodeName =
                    (String) landedCostServiceActualList.get(i).get(IMObItemGeneralModel.CODE)
                           + " - "
                           + getEnglishValue(landedCostServiceActualList.get(i).get(IMObItemGeneralModel.ITEM_NAME));

            Map landedCostServiceActualMap = new HashMap<String, String>();
            landedCostServiceActualMap.put(IAccountingFeatureFileCommonKeys.ITEM, landedCostServiceItemsActualCodeName);
            assertTrue(landedCostServiceItemsExpectedList.contains(landedCostServiceActualMap));
        }
    }



}
