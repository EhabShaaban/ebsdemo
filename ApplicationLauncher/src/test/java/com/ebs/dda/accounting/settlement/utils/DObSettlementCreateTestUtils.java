package com.ebs.dda.accounting.settlement.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.EnumUtils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.dbo.jpa.entities.businessobject.enums.SettlementTypeEnum;
import com.ebs.dda.accounting.settlement.IDObSettlementRestURLs;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlementCreationValueObject;
import com.google.gson.JsonObject;

import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementCreateTestUtils extends DObSettlementsCommonTestUtils {
	public DObSettlementCreateTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public void assertThatLastSettlementCode(String expectedLastSettlementCode) {
		String actualSettlementCodeExist = (String) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getLastSettlementCode");

		assertEquals(expectedLastSettlementCode, actualSettlementCodeExist);

	}

	public void assertThatSettlementTypesExist(DataTable settlementTypesDataTable) {
		List<Map<String, String>> settlementTypesDataTableList = settlementTypesDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> settlementTypes : settlementTypesDataTableList) {
			String expectedSettlementType = settlementTypes.get(IFeatureFileCommonKeys.CODE);
			boolean isTypeValid = EnumUtils.isValidEnum(SettlementTypeEnum.SettlementType.class,
							expectedSettlementType);
			assertTrue(isTypeValid);
		}

	}

	public Response createSettlement(DataTable settlementDataTable, Cookie userCookie) {
		JsonObject valueObject = prepareValueObject(settlementDataTable);
		return sendPostRequest(userCookie, IDObSettlementRestURLs.CREATE_URL, valueObject);
	}

	private JsonObject prepareValueObject(DataTable settlementDataTable) {
		Map<String, String> settlement = settlementDataTable.asMaps(String.class, String.class)
						.get(0);
		settlement = convertEmptyStringsToNulls(settlement);

		JsonObject parsedData = new JsonObject();

		addValueToJsonObject(parsedData, IDObSettlementCreationValueObject.TYPE,
						settlement.get(IFeatureFileCommonKeys.TYPE));

		addValueToJsonObject(parsedData, IDObSettlementCreationValueObject.BUSINESS_UNIT_CODE,
						settlement.get(IFeatureFileCommonKeys.BUSINESS_UNIT));

		addValueToJsonObject(parsedData, IDObSettlementCreationValueObject.COMPANY_CODE,
						settlement.get(IFeatureFileCommonKeys.COMPANY));

		addValueToJsonObject(parsedData, IDObSettlementCreationValueObject.CURRENCY_CODE,
						settlement.get(IFeatureFileCommonKeys.CURRENCY));

		parsedData.addProperty(IDObSettlementCreationValueObject.DOCUMENT_OWNER_ID,
						Long.parseLong(settlement.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)));

		return parsedData;
	}

	public void assertThatSettlementGeneralDataExist(DataTable settlementGeneralDataTable) {
		List<Map<String, String>> settlementGeneralDataTableList = settlementGeneralDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> expectedSettlementGeneralData : settlementGeneralDataTableList) {

			String settlementCode = (String) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getSettlementGeneralData",
											constructSettlementGeneralDataQueryParams(
															expectedSettlementGeneralData));

			assertNotNull(settlementCode,
							"Settlement with code "
											+ expectedSettlementGeneralData
															.get(IFeatureFileCommonKeys.CODE)
											+ " General Data is not Created");
		}
	}

	private Object[] constructSettlementGeneralDataQueryParams(
					Map<String, String> expectedSettlementGeneralData) {

		return new Object[] { expectedSettlementGeneralData.get(IFeatureFileCommonKeys.CODE),
						"%" + expectedSettlementGeneralData.get(IFeatureFileCommonKeys.STATE) + "%",
						expectedSettlementGeneralData.get(IFeatureFileCommonKeys.TYPE),
						expectedSettlementGeneralData.get(IFeatureFileCommonKeys.CREATED_BY),
						expectedSettlementGeneralData.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
						expectedSettlementGeneralData.get(IFeatureFileCommonKeys.CREATION_DATE),
						expectedSettlementGeneralData.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
						expectedSettlementGeneralData.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),

		};
	}

	public void assertThatSettlementCompanyDataExist(String settlementCode,
					DataTable settlementCompanyDataTable) {
		List<Map<String, String>> settlementCompanyDataTableList = settlementCompanyDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> expectedSettlementCompanyData : settlementCompanyDataTableList) {

			Long settlementId = (Long) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getSettlementCompanyData",
											constructSettlementCompanyDataQueryParams(
															settlementCode,
															expectedSettlementCompanyData));

			assertNotNull(settlementId, "Settlement with code " + settlementCode
							+ " Company Data is not Created");
		}
	}

	private Object[] constructSettlementCompanyDataQueryParams(String settlementCode,
					Map<String, String> expectedSettlementCompanyData) {

		return new Object[] { settlementCode,
						expectedSettlementCompanyData.get(IFeatureFileCommonKeys.COMPANY),
						expectedSettlementCompanyData.get(IFeatureFileCommonKeys.BUSINESS_UNIT),

		};
	}

	public void assertThatSettlementDetailsExist(String settlementCode,
					DataTable settlementDetailsTable) {
		List<Map<String, String>> settlementDetailsTableList = settlementDetailsTable
						.asMaps(String.class, String.class);
		for (Map<String, String> expectedSettlementDetails : settlementDetailsTableList) {

			Long settlementId = (Long) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getSettlementDetails",
											constructSettlementDetailsQueryParams(settlementCode,
															expectedSettlementDetails));

			assertNotNull(settlementId,
							"Settlement with code " + settlementCode + " Details is not Created");
		}
	}

	private Object[] constructSettlementDetailsQueryParams(String settlementCode,
					Map<String, String> expectedSettlementDetails) {

		return new Object[] { settlementCode,
						expectedSettlementDetails.get(IFeatureFileCommonKeys.CURRENCY),

		};
	}

}
