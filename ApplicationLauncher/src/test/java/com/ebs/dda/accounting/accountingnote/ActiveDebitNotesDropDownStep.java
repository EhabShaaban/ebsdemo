package com.ebs.dda.accounting.accountingnote;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingnote.utils.ActiveDebitNotesDropDownTestUtil;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class ActiveDebitNotesDropDownStep extends SpringBootRunner implements En {

  private static final String DEBIT_SCENARIO_NAME =
      "Read All DebitNotes Dropdown in Collection creation";
  private static final String CREDIT_SCENARIO_NAME =
      "Read All CreditNotes Dropdown in PaymentRequest Details";
  private ActiveDebitNotesDropDownTestUtil utils;

  public ActiveDebitNotesDropDownStep() {
    Given(
        "^the total number of records of AccountingNotes is (\\d+)$",
        (Long recordsNumber) -> {
          utils.assertThatNumberOfAccountingNotesIS(recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read all DebitNotes with state Posted remaining is greater than Zero according to user's BusinessUnit$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = IDObCollectionRestURLs.VIEW_DEBIT_NOTES;
          Response response = utils.sendGETRequest(cookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following DebitNotes for Collections values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable debitNotesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatNotesActualResponseMatchesExpected(response, debitNotesDataTable);
        });

    Then(
        "^total number of DebitNotes returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read all CreditNotes with state Posted remaining is greater than Zero according to user's BusinessUnit$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = IAccountingNotesRestUrls.VIEW_CREDIT_NOTES;
          Response response = utils.sendGETRequest(cookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following CreditNotes for PaymentRequest values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable creditNotesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatNotesActualResponseMatchesExpected(response, creditNotesDataTable);
        });

    And(
        "^total number of CreditNotes returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer numberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, numberOfRecords);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new ActiveDebitNotesDropDownTestUtil(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), DEBIT_SCENARIO_NAME)
        || contains(scenario.getName(), CREDIT_SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.clearAccountingNoteInsertions());
    }
  }
}
