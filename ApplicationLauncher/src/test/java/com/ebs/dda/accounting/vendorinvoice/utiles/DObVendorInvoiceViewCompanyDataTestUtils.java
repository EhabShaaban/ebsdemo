package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceCompanyDataGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DObVendorInvoiceViewCompanyDataTestUtils extends DObVendorInvoiceCommonTestUtils {

  public DObVendorInvoiceViewCompanyDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatCompanyDataIsExist(DataTable companyDataTable) {
    List<Map<String, String>> companyDataList = companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> companyDataMap : companyDataList) {

      Object[] queryParams = constructCompanyQueryParams(companyDataMap);
      IObVendorInvoiceCompanyDataGeneralModel companyDataGeneralModel =
          (IObVendorInvoiceCompanyDataGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorInvoiceCompanyData", queryParams);

      Assert.assertNotNull(
          "Vendor invoice company doesn't exist, given " + Arrays.toString(queryParams),
          companyDataGeneralModel);
    }
  }

  public Object[] constructCompanyQueryParams(Map<String, String> companyDataMap) {
    return new Object[] {
      companyDataMap.get(IFeatureFileCommonKeys.CODE),
      companyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      companyDataMap.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void assertThatViewComanyDataResponseIsCorrect(
      Response response, DataTable companyDataTable) throws Exception {

    Map<String, String> expected = companyDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> actual = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(expected, actual);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {

    mapAssertion.assertLocalizedValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_UNIT,
        IIObAccountingDocumentCompanyDataGeneralModel.PURCHASING_UNIT_NAME);

    mapAssertion.assertLocalizedValue(
        IAccountingFeatureFileCommonKeys.COMPANY,
        IIObAccountingDocumentCompanyDataGeneralModel.COMPANY_NAME);
  }
}
