package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObCollectionDeleteStep extends SpringBootRunner implements En {

    private DObCollectionDeleteTestUtils utils;

    public DObCollectionDeleteStep() {
        When(
                "^\"([^\"]*)\" requests to delete Collection with code \"([^\"]*)\"$",
                (String userName, String collectionCode) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = utils.deleteCollection(userCookie, collectionCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^Collection with code \"([^\"]*)\" is deleted from the system$",
                (String collectionCode) -> {
                    utils.assertThatCollectionIsDeleted(collectionCode);
                });

        Given(
                "^\"([^\"]*)\" first deleted the Collection with code \"([^\"]*)\" successfully$",
                (String userName, String collectionCode) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = utils.deleteCollection(userCookie, collectionCode);
                    utils.assertResponseSuccessStatus(response);
                });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObCollectionDeleteTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete Collection -")) {
            databaseConnector.createConnection();
            databaseConnector.executeSQLScript(utils.getConciseDbScripts());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete Collection -")) {
            databaseConnector.closeConnection();
            utils.unfreeze();
        }
    }
}
