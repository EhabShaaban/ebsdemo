package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestDropdownTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewPostingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPaymentRequestDropdownStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "Read all PaymentRequests for create Collection";
  private DObPaymentRequestDropdownTestUtils utils;
  private static boolean hasBeenExecuted = false;

  public DObPaymentRequestDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all PaymentDone PaymentRequests with remaining greater than zero and according to user’s BusinessUnit$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readPaymentRequestsForCreateCollection(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read all PaymentRequests for create Collection$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readPaymentRequestsForCreateCollection(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following PaymentRequests will be presented to \"([^\"]*)\" in a dropdown:$",
        (String userName, DataTable paymentRequestDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertPaymentRequestDropDownResultIsCorrect(paymentRequestDataTable, response);
        });

    Then(
        "^the total number of records returned to \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer numOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatRecordsNumberInResponseIsCorrect(numOfRecords, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestDropdownTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
          DObPaymentRequestCommonTestUtils.getPaymentRequestDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }

      databaseConnector.executeSQLScript(DObPaymentRequestDropdownTestUtils.clearData());
    }
  }
}
