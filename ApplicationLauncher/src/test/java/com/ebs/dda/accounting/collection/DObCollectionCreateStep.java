package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionCreateTestUtils;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCommonTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceAllowedActionTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceViewAllTestUtils;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionCreateStep extends SpringBootRunner implements En {

  private static final String CREATE_SCENARIO_HP = "Create Collection";
  private static final String CREATE_SCENARIO_VAL = "Create Collection Validation";
  private static boolean hasExcutedOnce = false;
  private DObCollectionCreateTestUtils collectionCreateTestUtils;
  private DObSalesInvoiceViewAllTestUtils salesInvoiceViewAllTestUtils;
  private DObSalesInvoiceAllowedActionTestUtils salesInvoiceAllowedActionTestUtils;
  private DObMonetaryNotesCommonTestUtils notesReceivablesTestUtils;
  private HObCommonChartOfAccountTestUtils chartOfAccountTestUtils;
  private DObSalesInvoiceActivateTestUtils salesInvoicePostTestUtils;

  public DObCollectionCreateStep() {

    Given(
        "^the following SalesInvoices exist:$",
        (DataTable salesInvoiceDataTable) -> {
          salesInvoiceAllowedActionTestUtils.assertSalesInvoicesExist(salesInvoiceDataTable);
        });

    Given(
        "^the following Company and Currency and Customer and RemainingAmount exist in the following SalesInvoices:$",
        (DataTable salesInvoiceDataTable) -> {
          salesInvoiceViewAllTestUtils
              .assertSalesInvoicesWithCompanyAndCurrencyAndCustomerAndRemainingAmountExist(
                  salesInvoiceDataTable);
        });

    Given(
        "^the following Company and Currency and Customer and RemainingAmount exist in the following NotesReceivables:$",
        (DataTable notesReceivablesData) -> {
          notesReceivablesTestUtils
              .assertNotesReceivablesWithCompanyAndCurrencyAndCustomerAndRemainingAmountExist(
                  notesReceivablesData);
        });

    Given(
        "^the following Company and Currency and Customer exist in the following SalesOrder:$",
        (DataTable salesOrderDataTable) -> {
          collectionCreateTestUtils.assertSalesOrdersWithCompanyAndCurrencyAndCustomerExist(
              salesOrderDataTable);
        });

    Given(
        "^the following NotesReceivables exist:$",
        (DataTable notesReceivablesDataTable) -> {
          collectionCreateTestUtils.assertNotesReceivablesWithCodeAndStateAndBusinessUnitExist(
              notesReceivablesDataTable);
        });

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String dateTime) -> {
          collectionCreateTestUtils.freeze(dateTime);
        });

    Given(
        "^Last created Collection was with code \"([^\"]*)\"$",
        (String collectionCode) -> {
          collectionCreateTestUtils.createLastEntityCode(
              DObCollection.class.getSimpleName(), collectionCode);
        });

    When(
        "^\"([^\"]*)\" creates Collection with the following values:$",
        (String userName, DataTable collectionDataTable) -> {
          Response response =
              collectionCreateTestUtils.createCollection(
                  collectionDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new Collection is created as follows:$",
        (DataTable CollectionDataTable) -> {
          collectionCreateTestUtils.assertCollectionIsCreated(CollectionDataTable);
        });

    Then(
        "^the CollectionDetails Section of Collection with code \"([^\"]*)\" is updated as follows:$",
        (String collectionCode, DataTable collectionDetailsDataTable) -> {
          collectionCreateTestUtils.assertCollectionDetailsIsUpdated(
              collectionCode, collectionDetailsDataTable);
        });

    Given(
        "^the GLAccounts configuration exist:$",
        (DataTable glAccountConfigDataTable) -> {
          chartOfAccountTestUtils.assetGLAccountConfigurationsExist(glAccountConfigDataTable);
        });

    Given(
        "^the following GLAccounts has the following Subledgers:$",
        (DataTable subledgerDataTable) -> {
          salesInvoicePostTestUtils.assertThatSubLedgerExist(subledgerDataTable);
        });

    Then(
        "^the CompanyDetails Section of Collection with code \"([^\"]*)\" is updated as follows:$",
        (String collectionCode, DataTable dataTable) -> {
          collectionCreateTestUtils.assertThatCollectionCompanyDetailsIsUpdated(
              collectionCode, dataTable);
        });
    And(
        "^the following SalesOrders have the following remainings:$",
        (DataTable salesOrderRemainingsTable) -> {
          collectionCreateTestUtils.assertThatSalesOrderRemainingsAreCorrect(
              salesOrderRemainingsTable);
        });

    When(
        "^\"([^\"]*)\" requests to create Collection$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              collectionCreateTestUtils.sendGETRequest(
                  cookie, IDObCollectionRestURLs.REQUEST_CREATE_URL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    collectionCreateTestUtils =
        new DObCollectionCreateTestUtils(properties, entityManagerDatabaseConnector);
    salesInvoiceViewAllTestUtils = new DObSalesInvoiceViewAllTestUtils(properties);
    salesInvoiceViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    salesInvoiceAllowedActionTestUtils = new DObSalesInvoiceAllowedActionTestUtils(properties);
    salesInvoiceAllowedActionTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);

    notesReceivablesTestUtils =
        new DObMonetaryNotesCommonTestUtils(properties, entityManagerDatabaseConnector);

    chartOfAccountTestUtils = new HObCommonChartOfAccountTestUtils(getProperties());
    chartOfAccountTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    salesInvoicePostTestUtils = new DObSalesInvoiceActivateTestUtils(getProperties());
    salesInvoicePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), CREATE_SCENARIO_VAL)) {
      databaseConnector.createConnection();
      if (!hasExcutedOnce) {
        hasExcutedOnce = true;
        databaseConnector.executeSQLScript(collectionCreateTestUtils.getDbScriptsExecution());
      }
    } else if (contains(scenario.getName(), CREATE_SCENARIO_HP)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          collectionCreateTestUtils.clearIsertedDataFromFeatureFile());
      databaseConnector.executeSQLScript(collectionCreateTestUtils.clearCollectionInsertions());
      databaseConnector.executeSQLScript(collectionCreateTestUtils.getDbScriptsExecution());
      databaseConnector.executeSQLScript(collectionCreateTestUtils.getCreateDbScriptsExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), CREATE_SCENARIO_HP)) {
      collectionCreateTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
