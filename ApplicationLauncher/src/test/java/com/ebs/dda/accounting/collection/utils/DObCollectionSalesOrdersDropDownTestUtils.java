package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObCollectionSalesOrdersDropDownTestUtils extends DObCollectionTestUtils {

  public DObCollectionSalesOrdersDropDownTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    return str.toString();
  }

  public void assertThatSalesOrdersAreExist(DataTable salesOrders) {
    List<Map<String, String>> salesOrdersList = salesOrders.asMaps(String.class, String.class);

    for (Map<String, String> salesOrder : salesOrdersList) {
      Object[] params = constructQueryParams(salesOrder);

      DObSalesOrderGeneralModel actualSalesOrder =
          (DObSalesOrderGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesOrderByCodeAndStateAndBusinessUnit", params);

      assertNotNull("Sales Order does not exist " + Arrays.toString(params), actualSalesOrder);
    }
  }

  private Object[] constructQueryParams(Map<String, String> salesOrder) {
    String code = salesOrder.get(IFeatureFileCommonKeys.CODE);
    String businessUnit = salesOrder.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    String state = salesOrder.get(IFeatureFileCommonKeys.STATE);

    Object[] params = new Object[] {code, businessUnit, "%" + state + "%"};
    return params;
  }

  public void assertThatSalesOrdersAreRetrieved(Response userResponse, DataTable salesOrdersDT) {
    List<Map<String, String>> expectedSalesOrders =
        salesOrdersDT.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualSalesOrders = getListOfMapsFromResponse(userResponse);
    for (int i = 0; i < expectedSalesOrders.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedSalesOrders.get(i), actualSalesOrders.get(i));
      mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObSalesOrderGeneralModel.USER_CODE);
    }
  }
}
