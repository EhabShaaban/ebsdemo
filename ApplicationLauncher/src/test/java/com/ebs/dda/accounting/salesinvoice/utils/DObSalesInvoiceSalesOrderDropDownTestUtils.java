package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;

import cucumber.api.DataTable;
import io.restassured.response.Response;

public class DObSalesInvoiceSalesOrderDropDownTestUtils extends DObSalesInvoiceTestUtils {

	public DObSalesInvoiceSalesOrderDropDownTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public String getDbScriptsOneTimeExecution(){
		StringBuilder str = new StringBuilder();
		str.append(super.getDbScriptsOneTimeExecution());
		str.append(",").append(
				"db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
		str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");

		str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
		str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
		return str.toString();
	}
	public void assertThatSalesOrdersActualResponseMatchesExpected(Response response,
					DataTable salesOrdersDataTable) throws Exception {
		List<Map<String, String>> expectedSalesOrders = salesOrdersDataTable.asMaps(String.class,
						String.class);
		List<HashMap<String, Object>> actualSalesOrders = getListOfMapsFromResponse(response);
		int index = 0;
		for (Map<String, String> expectedSalesOrder : expectedSalesOrders) {
			HashMap<String, Object> actualSalesOrder = actualSalesOrders.get(index);
			MapAssertion mapAssertion = new MapAssertion(expectedSalesOrder, actualSalesOrder);
			mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObSalesOrderGeneralModel.USER_CODE);
			mapAssertion.assertCodeAndLocalizedNameValue(IFeatureFileCommonKeys.TYPE,
							IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_CODE,
							IDObSalesOrderGeneralModel.SALES_ORDER_TYPE_NAME);
			mapAssertion.assertCodeAndLocalizedNameValue(IFeatureFileCommonKeys.BUSINESS_UNIT,
							IDObSalesOrderGeneralModel.PURCHASE_UNIT_CODE,
							IDObSalesOrderGeneralModel.PURCHASE_UNIT_NAME_ATTR_NAME);
			mapAssertion.assertValueContains(IFeatureFileCommonKeys.STATE,
							IDObSalesOrderGeneralModel.CURRENT_STATES);
			index++;
		}
	}

	public void assertThatNumberOfSalesOrdersIS(Long recordsNumber) {
		Object actualTotalNumberOfSalesOrder = entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getTotalNumberOfSalesOrderViewAll");

		assertEquals(recordsNumber, actualTotalNumberOfSalesOrder);
	}
}
