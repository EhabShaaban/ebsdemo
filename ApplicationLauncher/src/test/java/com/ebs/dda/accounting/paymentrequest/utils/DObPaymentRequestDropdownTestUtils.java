package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestDropdownTestUtils extends DObPaymentRequestCommonTestUtils {
  public DObPaymentRequestDropdownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public static String clearData() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/payment-request/PaymentRequest_Clear.sql");
    return str.toString();
  }

  public Response readPaymentRequestsForCreateCollection(Cookie cookie) {
    return sendGETRequest(cookie, IDObCollectionRestURLs.READ_ALL_PAYMENT_REQUESTS);
  }

  public void assertPaymentRequestDropDownResultIsCorrect(
      DataTable paymentRequestDataTable, Response response) throws Exception {
    List<Map<String, String>> paymentRequestMapList =
        paymentRequestDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> paymentRequestResponse =
        response.body().jsonPath().getList("data");
    for (int i = 0; i < paymentRequestMapList.size(); i++) {
      assertThatPaymentRequestIsCorrect(
          paymentRequestResponse.get(i), paymentRequestMapList.get(i));
    }
  }

  public void assertThatPaymentRequestIsCorrect(
      HashMap<String, Object> actualPaymentRequest, Map<String, String> expectedPaymentRequest)
      throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedPaymentRequest, actualPaymentRequest);
    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObPaymentRequestGeneralModel.USER_CODE);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObPaymentRequestGeneralModel.PAYMENT_TYPE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObPaymentRequestGeneralModel.PURCHASE_UNIT_CODE,
        IDObPaymentRequestGeneralModel.PURCHASING_UNIT_NAME);
    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObPaymentRequestGeneralModel.STATE);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.REMAINING,
      IDObPaymentRequestGeneralModel.REMAINING);
  }

  public void assertThatRecordsNumberInResponseIsCorrect(Integer numOfRecords, Response response) {
    List<HashMap<String, Object>> paymentRequestResponse =
        response.body().jsonPath().getList("data");
    Integer actual = paymentRequestResponse.size();
    String assertionMessage =
        "[PaymentRequest][Dropdown] Number of record in response is not correct";
    Assert.assertEquals(assertionMessage, numOfRecords, actual);
  }
}
