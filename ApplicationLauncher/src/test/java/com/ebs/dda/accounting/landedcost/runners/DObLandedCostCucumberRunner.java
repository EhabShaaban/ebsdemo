package com.ebs.dda.accounting.landedcost.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/accounting/landed-cost/LC_PurchaseOrders_Dropdown.feature",
      "classpath:features/accounting/landed-cost/LC_AllowedActions_ObjectScreen.feature",
      "classpath:features/accounting/landed-cost/LC_ViewAll.feature",
      "classpath:features/accounting/landed-cost/LC_View_GeneralData.feature",
      "classpath:features/accounting/landed-cost/LC_Type_dropdown.feature",
      "classpath:features/accounting/landed-cost/LC_View_LandedCostDetails.feature",
      "classpath:features/accounting/landed-cost/LC_View_CostFactorItems.feature",
      "classpath:features/accounting/landed-cost/LC_View_CompanyData.feature",
      "classpath:features/accounting/landed-cost/LC_AllowedActions_HS.feature",
      "classpath:features/accounting/landed-cost/LC_Create_HP.feature",
      "classpath:features/accounting/landed-cost/LC_Create_Auth.feature",
      "classpath:features/accounting/landed-cost/LC_Create_Val.feature",
      "classpath:features/accounting/landed-cost/LC_RequestAdd_CostFactorItem.feature",
      "classpath:features/accounting/landed-cost/LC_Delete.feature",
      "classpath:features/accounting/landed-cost/LC_Delete_CostFactorItems.feature",
      "classpath:features/accounting/landed-cost/LC_Save_NewCostFactorItem_HP.feature",
      "classpath:features/accounting/landed-cost/LC_Save_NewCostFactorItem_Val.feature",
      "classpath:features/accounting/landed-cost/LC_Save_NewCostFactorItem_Auth.feature",
      "classpath:features/accounting/landed-cost/LC_Service_Items_Dropdowns.feature",
      "classpath:features/accounting/landed-cost/LC_ConvertToEstimate_HP.feature",
      "classpath:features/accounting/landed-cost/LC_ConvertToEstimate_Val.feature",
      "classpath:features/accounting/landed-cost/LC_ConvertToActual_HP.feature",
      "classpath:features/accounting/landed-cost/LC_ConvertToActual_Val.feature",
      "classpath:features/accounting/landed-cost/LC_ConvertToActual_Auth.feature",
      "classpath:features/accounting/landed-cost/LC_ConvertToEstimate_Auth.feature",
      "classpath:features/accounting/landed-cost/LC_DocumentOwner_Dropdown.feature"

    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.landedcost"},
    strict = true,
    tags = "not @Future")
public class DObLandedCostCucumberRunner {}
