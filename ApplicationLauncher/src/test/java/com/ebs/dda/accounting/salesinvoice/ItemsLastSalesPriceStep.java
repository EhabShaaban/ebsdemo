package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCreationTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceItemsViewTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.ItemsLastSalesPriceTestUtils;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.masterdata.item.utils.MObItemUOMViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class ItemsLastSalesPriceStep extends SpringBootRunner implements En {
    public static final String SCENARIO_NAME = "View ItemLastSalesPrice";
    private DObMonetaryNotesCreationTestUtils notesReceivablesCreationTestUtils;
  private IObSalesInvoiceItemsViewTestUtils salesInvoiceViewItemsTestUtils;
  private CObEnterpriseTestUtils enterpriseTestUtils;
  private MObItemUOMViewTestUtils uomViewTestUtils;
  private ItemsLastSalesPriceTestUtils itemsLastSalesPriceTestUtils;

    private static boolean hasBeenExecuted = false;

  public ItemsLastSalesPriceStep() {

    Given(
        "^the following UnitOfMeasures section exist in Item with code \"([^\"]*)\":$",
        (String itemCode, DataTable itemsData) -> {
          List<List<String>> itemsList = itemsData.raw();
          for (int i = 1; i < itemsList.size(); i++) {
            List<String> itemUOMData = itemsList.get(i);
            Object[] params =
                uomViewTestUtils.constructItemHasUnitOfMeasureQueryParams(itemCode, itemUOMData);
            uomViewTestUtils.assertThatItemHasUnitOfMeasure(params);
          }
        });

    When(
        "^\"([^\"]*)\" requests to view ItemLastSalesPrice with itemCode \"([^\"]*)\" and CustomerCode \"([^\"]*)\" and UOM \"([^\"]*)\" and PurchaseUnit \"([^\"]*)\"$",
        (String userName,
            String itemCode,
            String CustomerCode,
            String UOM,
            String PurchaseUnit) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              itemsLastSalesPriceTestUtils.sendGETRequest(
                  cookie,
                  itemsLastSalesPriceTestUtils.getItemsLastSalePriceUrl(
                      itemCode, UOM, PurchaseUnit, CustomerCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesInvoicesItem values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable itemsLastSalesPriceDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          itemsLastSalesPriceTestUtils.assertResponseSuccessStatus(response);
          itemsLastSalesPriceTestUtils.assertThatItemsLastSalesPriceAreCorrect(
              response, itemsLastSalesPriceDataTable);
        });

      And("^the total number of existing posting SalesInvoice are \"([^\"]*)\"$", (String NumOfPostedSalesInvoice) -> {
          itemsLastSalesPriceTestUtils.assertPostedSalesInvoice(NumOfPostedSalesInvoice);
      });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    itemsLastSalesPriceTestUtils = new ItemsLastSalesPriceTestUtils(properties);
    itemsLastSalesPriceTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    notesReceivablesCreationTestUtils = new DObMonetaryNotesCreationTestUtils(properties,entityManagerDatabaseConnector);
    salesInvoiceViewItemsTestUtils = new IObSalesInvoiceItemsViewTestUtils(properties);
    salesInvoiceViewItemsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    uomViewTestUtils = new MObItemUOMViewTestUtils(properties);
    uomViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            itemsLastSalesPriceTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(ItemsLastSalesPriceTestUtils.clearSalesInvoices());
}
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
      if (contains(scenario.getName(), SCENARIO_NAME)) {
        databaseConnector.closeConnection();
      }
  }
}
