package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostSectionNames;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostDeleteTestUtils;
import com.ebs.dda.accounting.landedcost.utils.IObLandedCoatRequestAddCostFactorItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class IObLandedCostRequestAddCostFactorItemStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private IObLandedCoatRequestAddCostFactorItemTestUtils landedCostRequestAddCostFactorItemTestUtils;
    private DObLandedCostDeleteTestUtils landedCostDeleteTestUtils;


    public IObLandedCostRequestAddCostFactorItemStep() {


        When("^\"([^\"]*)\" requests to add Item to LandedCostFactorItems section of LandedCost with code \"([^\"]*)\"$", (String userName, String landedCostCode) -> {
            String lockUrl = landedCostRequestAddCostFactorItemTestUtils.getLockUrl(landedCostCode);
            Response response = userActionsTestUtils.lockSection(userName, lockUrl, landedCostCode);
            userActionsTestUtils.setUserResponse(userName, response);
        });


        Then("^a new add Item dialoge is opened and LandedCostFactorItems section of LandedCost with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$", (String landedCostCode, String userName) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            landedCostRequestAddCostFactorItemTestUtils.assertResponseSuccessStatus(response);
            landedCostRequestAddCostFactorItemTestUtils.assertThatResourceIsLockedByUser(
                    userName, landedCostCode, IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);
        });


        Given(
                "^\"([^\"]*)\" first requested to add Item to LandedCostFactorItems section of LandedCost with code \"([^\"]*)\" successfully$",
                (String userName, String landedCostCode) -> {
                    String lockUrl = landedCostRequestAddCostFactorItemTestUtils.getLockUrl(landedCostCode);
                    Response response = userActionsTestUtils.lockSection(userName, lockUrl, landedCostCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^LandedCostFactorItems section of LandedCost with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
                (String landedCostCode, String userName) -> {
                    landedCostRequestAddCostFactorItemTestUtils.assertThatLockIsReleased(
                            userName, landedCostCode, IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);
                });


        Given(
                "^LandedCostFactorItems section of LandedCost with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
                (String landedCostCode, String userName, String lockTime) -> {
                    landedCostRequestAddCostFactorItemTestUtils.freeze(lockTime);
                    String lockUrl = landedCostRequestAddCostFactorItemTestUtils.getLockUrl(landedCostCode);
                    Response response = userActionsTestUtils.lockSection(userName, lockUrl, landedCostCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        When(
                "^\"([^\"]*)\" cancels saving LandedCostFactorItems section of LandedCost with code \"([^\"]*)\" at \"([^\"]*)\"$",
                (String userName, String landedCostCode, String unlockTime) -> {
                    landedCostRequestAddCostFactorItemTestUtils.freeze(unlockTime);
                    String unLockUrl = landedCostRequestAddCostFactorItemTestUtils.getUnLockUrl(landedCostCode);
                    Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the lock by \"([^\"]*)\" on LandedCostFactorItems section of LandedCost with code \"([^\"]*)\" is released$",
                (String userName, String landedCostCode) -> {
                    landedCostRequestAddCostFactorItemTestUtils.assertThatLockIsReleased(
                            userName, landedCostCode, IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);
                });

        When(
                "^\"([^\"]*)\" cancels saving LandedCostFactorItems section of LandedCost with code \"([^\"]*)\"$",
                (String userName, String landedCostCode) -> {
                    String unLockUrl = landedCostRequestAddCostFactorItemTestUtils.getUnLockUrl(landedCostCode);
                    Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
        Given(
                "^\"([^\"]*)\" first deleted the LandedCost with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
                (String userName, String landedCostCode, String deletionTime) -> {
                    landedCostRequestAddCostFactorItemTestUtils.freeze(deletionTime);
                    Response landedCostDeleteResponse =
                            landedCostDeleteTestUtils.deleteLandedCost(userActionsTestUtils.getUserCookie(userName), landedCostCode);
                    userActionsTestUtils.setUserResponse(userName, landedCostDeleteResponse);
                });
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        Map<String, Object> properties = getProperties();
        landedCostRequestAddCostFactorItemTestUtils =
                new IObLandedCoatRequestAddCostFactorItemTestUtils(properties, entityManagerDatabaseConnector);
        landedCostDeleteTestUtils = new DObLandedCostDeleteTestUtils(properties);
        landedCostDeleteTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        if (contains(scenario.getName(), "Request to Add/Cancel Item to LandedCost")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(
                        landedCostRequestAddCostFactorItemTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
            databaseConnector.executeSQLScript(landedCostRequestAddCostFactorItemTestUtils.getDbScripts());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Request to Add/Cancel Item to LandedCost")) {

            userActionsTestUtils.unlockAllSections(
                    IDObLandedCostRestUrls.UNLOCK_LOCKED_SECTIONS, landedCostRequestAddCostFactorItemTestUtils.getSectionsNames());


            databaseConnector.closeConnection();
        }
    }
}
