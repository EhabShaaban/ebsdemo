package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionActivateHappyPathTestUtils;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewAllTestUtils;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCreateJournalEntryTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceDeleteTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceViewAllTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObCollectionActivateStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "Activate Collection";

  private HObCommonChartOfAccountTestUtils chartOfAccountTestUtils;
  private DObCollectionActivateHappyPathTestUtils utils;
  private DObCollectionViewAllTestUtils collectionViewAllTestUtils;
  private DObSalesInvoiceViewAllTestUtils salesInvoiceViewAllTestUtils;
  private DObPaymentRequestCreateJournalEntryTestUtils paymentRequestCreateJournalEntryTestUtils;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
  private DObSalesInvoiceActivateTestUtils salesInvoicePostTestUtils;
  private DObMonetaryNotesCommonTestUtils notesReceivablesTestUtils;
  private DObSalesInvoiceDeleteTestUtils salesInvoiceDeleteTestUtils;
  private DObSalesInvoiceTestUtils salesInvoiceTestUtils;
  private static boolean hasExcutedOnce = false;

  public DObCollectionActivateStep() {
    When(
        "^\"([^\"]*)\" Activates Collection based on \"([^\"]*)\" with code \"([^\"]*)\" and DueDate \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName,
            String collectionType,
            String collectionCode,
            String dueDateTime,
            String dateTime) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);

          Response response =
              utils.sendPutCollection(cookie, collectionCode, collectionType, dueDateTime);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^Collection with Code \"([^\"]*)\" is updated as follows:$",
        (String collectionCode, DataTable collectionDetailsDataTable) -> {
          utils.assertCollectionDetailsIsUpdatedAfterPost(
              collectionCode, collectionDetailsDataTable);
        });

    Then(
        "^ActivationDetails in Collection with Code \"([^\"]*)\" is updated as follows:$",
        (String collectionCode, DataTable postingDetailsDataTable) -> {
          utils.assertThatCollectionActivationDetailAreUpdated(
              collectionCode, postingDetailsDataTable);
        });

    Given(
        "^Last Journal Entry Code is \"([^\"]*)\"$",
        (String journalEntryCode) -> {
          paymentRequestCreateJournalEntryTestUtils.assertLastJournalEntry(journalEntryCode);
        });

    Then(
        "^a new JournalEntry is created as follows:$",
        (DataTable journalEntryDataTable) -> {
          journalEntryViewAllTestUtils
              .assertThatJournalEntriesExistWithCompanyAndRefDocumentCodeName(
                  journalEntryDataTable);
        });

    Then(
        "^And the following new JournalEntryDetails are created for JournalEntry with code \"([^\"]*)\":$",
        (String journalEntryCode, DataTable journalItemsDataTable) -> {
          salesInvoicePostTestUtils.assertThatJournalEntryItemsCreatedSuccessfully(
              journalEntryCode, journalItemsDataTable);
        });

    Given(
        "^the following AccountingDetails for the following Collections exist:$",
        (DataTable accountingDetailsDataTable) -> {
          utils.assertAccountingDetailsExistForCollections(accountingDetailsDataTable);
        });

    Then(
        "^State of SalesInvoice with code \"([^\"]*)\" remains \"([^\"]*)\"$",
        (String salesInvoiceCode, String salesInvoiceState) -> {
          salesInvoiceTestUtils.assertThatSalesInvoiceStateUpdated(
              salesInvoiceCode, salesInvoiceState);
        });
    Then(
        "^State of SalesInvoice with code \"([^\"]*)\" becomes \"([^\"]*)\"$",
        (String salesInvoiceCode, String salesInvoiceState) -> {
          salesInvoiceTestUtils.assertThatSalesInvoiceStateUpdated(
              salesInvoiceCode, salesInvoiceState);
        });

    Then(
        "^Remaining of SalesInvoice with code \"([^\"]*)\" is updated as follows \"([^\"]*)\"$",
        (String salesInvoiceCode, String remainingAmount) -> {
          salesInvoiceTestUtils.assertThatSalesInvoiceRemainingAmountUpdated(
              salesInvoiceCode, remainingAmount);
        });
    Then(
        "^Remaining amount of SalesOrder with Code \"([^\"]*)\" becomes \"([^\"]*)\"$",
        (String salesOrderCode, String remainingAmount) -> {
          utils.assertThatSalesOrderRemainingAmountUpdated(salesOrderCode, remainingAmount);
        });
    And(
        "^Remaining amount of NotesReceivable with Code \"([^\"]*)\" becomes \"([^\"]*)\"$",
        (String notesReceivableCode, String remainingAmount) -> {
          utils.assertThatRemainingAmountMatchesExpected(notesReceivableCode, remainingAmount);
        });

    And(
        "^State of NotesReceivable with Code \"([^\"]*)\" becomes \"([^\"]*)\"$",
        (String notesReceivableCode, String notesReceivableState) -> {
          utils.assertThatNotesReceivableStateMatchesExpected(
              notesReceivableCode, notesReceivableState);
        });
    And(
        "^State of SalesOrder with Code \"([^\"]*)\" remains \"([^\"]*)\"$",
        (String salesOrderCode, String salesOrderState) -> {
          utils.assertThatSalesOrderStateMatchesExpected(salesOrderCode, salesOrderState);
        });
    And(
        "^State of NotesReceivable with Code \"([^\"]*)\" remains \"([^\"]*)\"$",
        (String notesReceivableCode, String notesReceivableState) -> {
          utils.assertThatNotesReceivableStateMatchesExpected(
              notesReceivableCode, notesReceivableState);
        });
    Given(
        "^the GLAccounts configuration exit:$",
        (DataTable glAccountConfigDataTable) -> {
          chartOfAccountTestUtils.assetGLAccountConfigurationsExist(glAccountConfigDataTable);
        });
    Then(
        "^AccountingDetails in Collection with Code \"([^\"]*)\" is updated as follows:$",
        (String collectionCode, DataTable accountingDetailsDataTable) -> {
          utils.assertAccountingDetailsAreUpdated(collectionCode, accountingDetailsDataTable);
        });

    Then(
        "^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
        (String missingFields, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatResponseHasMissingFields(response, missingFields);
        });

    And(
        "^Remaining amount of DebitNote with Code \"([^\"]*)\" becomes \"([^\"]*)\"$",
        (String debitNoteCode, String remaining) -> {
          utils.assertThatDebitNoteRemainingIsUpdated(debitNoteCode, remaining);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObCollectionActivateHappyPathTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    collectionViewAllTestUtils = new DObCollectionViewAllTestUtils(properties);
    collectionViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    notesReceivablesTestUtils =
        new DObMonetaryNotesCommonTestUtils(properties, entityManagerDatabaseConnector);

    salesInvoiceTestUtils = new DObSalesInvoiceTestUtils(properties);
    salesInvoiceTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    salesInvoiceViewAllTestUtils = new DObSalesInvoiceViewAllTestUtils(properties);
    salesInvoiceViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    paymentRequestCreateJournalEntryTestUtils =
        new DObPaymentRequestCreateJournalEntryTestUtils(properties);
    paymentRequestCreateJournalEntryTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);

    journalEntryViewAllTestUtils =
        new DObJournalEntryViewAllTestUtils(properties, entityManagerDatabaseConnector);

    salesInvoicePostTestUtils = new DObSalesInvoiceActivateTestUtils(properties);
    salesInvoicePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    salesInvoiceDeleteTestUtils = new DObSalesInvoiceDeleteTestUtils(properties);
    salesInvoiceDeleteTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    chartOfAccountTestUtils = new HObCommonChartOfAccountTestUtils(getProperties());
    chartOfAccountTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.resetDataDependanciesBeforeInsertion());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
      utils.unfreeze();
    }
  }
}
