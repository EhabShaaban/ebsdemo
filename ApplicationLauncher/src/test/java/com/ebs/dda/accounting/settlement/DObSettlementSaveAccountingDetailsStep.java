package com.ebs.dda.accounting.settlement;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.settlement.utils.IObSettlementSaveAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementSaveAccountingDetailsStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "Save new Settlement AccountingDetails";
  private static boolean hasBeenExecuted = false;

  private IObSettlementSaveAccountingDetailsTestUtils utils;
  private AccountingDocumentCommonTestUtils commonAccountingDocumentUtils;

  public DObSettlementSaveAccountingDetailsStep() {

    When(
        "^\"([^\"]*)\" saves the following new AccountingDetails record in Settlement with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String settlementCode, String savingTime, DataTable itemsDataTable) -> {
          utils.freeze(savingTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.saveAccountingDetails(cookie, settlementCode, itemsDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then("^AccountingDetails section of Settlement with code \"([^\"]*)\" is Updated as follows:$",
        (String settlementCode, DataTable updatedDetailsDT) -> {
          utils.assertThatAccountingDetailsIsUpdated(settlementCode, updatedDetailsDT);
        });
    Then("^Settlement with code \"([^\"]*)\" is updated as follows:$",
        (String settlementCode, DataTable updatedSettlementDT) -> {
          utils.assertThatSettlementIsUpdated(settlementCode, updatedSettlementDT);
        });



  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObSettlementSaveAccountingDetailsTestUtils(getProperties());

    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    commonAccountingDocumentUtils = new AccountingDocumentCommonTestUtils(getProperties());
    commonAccountingDocumentUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewAll(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
        utils.executeAccountDeterminationFunction();
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(clearSettlement());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
      utils.unfreeze();
    }
  }
}
