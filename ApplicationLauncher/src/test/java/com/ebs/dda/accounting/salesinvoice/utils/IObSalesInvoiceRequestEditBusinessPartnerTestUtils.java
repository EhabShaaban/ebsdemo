package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceBusinessPartnerTestUtils;
import java.util.Map;

public class IObSalesInvoiceRequestEditBusinessPartnerTestUtils extends IObSalesInvoiceBusinessPartnerTestUtils {

  public IObSalesInvoiceRequestEditBusinessPartnerTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/DObSalesInvoice.sql");
    return str.toString();
  }


  public StringBuilder getLockUrl(String salesInvoiceCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObSalesInvoiceRestUrls.COMMAND_URL);
    lockUrl.append(salesInvoiceCode);
    lockUrl.append(IDObSalesInvoiceRestUrls.LOCK_BUSINESS_PARTNER);
    return lockUrl;
  }

  public String getUnLockUrl(String salesInvoiceCode) {
    String unLockUrl =
            IDObSalesInvoiceRestUrls.COMMAND_URL
                    + salesInvoiceCode
                    + IDObSalesInvoiceRestUrls.UNLOCK_BUSINESS_PARTNER;
    return unLockUrl;
  }

}
