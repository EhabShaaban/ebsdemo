package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceCreateTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceTaxesViewTestUtils;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.order.salesorder.utils.IObSalesOrderViewItemsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceCreateStep extends SpringBootRunner implements En {

	private static final String SCENARIO_NAME = "Create SalesInvoice";
	private static boolean hasBeenExecuted = false;
	private DObSalesInvoiceCreateTestUtils dObSalesInvoiceCreateTestUtils;
	private CObEnterpriseTestUtils enterpriseTestUtils;
	private IObSalesInvoiceTaxesViewTestUtils taxesViewTestUtils;
	private IObSalesOrderViewItemsTestUtils salesOrderViewItemsTestUtils;

	public DObSalesInvoiceCreateStep() {

		Given("^the following SalesInvoiceTypes exist:$", (DataTable invoiceTypeDataTable) -> {
			dObSalesInvoiceCreateTestUtils.assertThatSalesInvoiceTypesExist(invoiceTypeDataTable);
		});

		Given("^the following companys exist:$", (DataTable companyDataTable) -> {
			enterpriseTestUtils.assertThatComapnyExist(companyDataTable);
		});

		Given("^the following SalesOrders have the following TaxDetails:$",
						(DataTable taxesDataTable) -> {
							salesOrderViewItemsTestUtils.assertThatSalesOrderHasTheFollowingTaxes(
											taxesDataTable);
						});

		Given("^CurrentDateTime = \"([^\"]*)\"$", (String dateTime) -> {
			dObSalesInvoiceCreateTestUtils.freeze(dateTime);
		});

		Given("^Last created SalesInvoice was with code \"([^\"]*)\"$",
						(String salesInvoiceCode) -> {
							dObSalesInvoiceCreateTestUtils
											.assertSalesInvoiceExistsWithCode(salesInvoiceCode);
						});

		When("^\"([^\"]*)\" creates SalesInvoice with the following values:$",
						(String userName, DataTable salesInvoiceDataTable) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							Response response = dObSalesInvoiceCreateTestUtils
											.createSalesInvoice(userCookie, salesInvoiceDataTable);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		When("^\"([^\"]*)\" creates SalesInvoice without Reference with the following values:$",
						(String userName, DataTable salesInvoiceDataTable) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							Response response = dObSalesInvoiceCreateTestUtils
											.createSalesInvoice(userCookie, salesInvoiceDataTable);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^a new SalesInvoice of type Without_Reference is created with the following values:$",
						(DataTable invoiceDataTable) -> {
							dObSalesInvoiceCreateTestUtils.assertSalesInvoiceIsCreatedSuccessfully(
											invoiceDataTable);
						});

		And("^the BusinessPartner Section of SalesInvoice without Reference with code \"([^\"]*)\" is created as follows:$",
						(String salesInvoiceCode, DataTable businessPartnerDataTable) -> {
							dObSalesInvoiceCreateTestUtils
											.assertThatBusinessPartnerForSIWithoutRefIsCreatedSuccessfully(
															salesInvoiceCode,
															businessPartnerDataTable);
						});
		Then("^the Taxes Section of SalesInvoice with code \"([^\"]*)\" is created as follows:$",
						(String salesInvoiceCode, DataTable companyTaxesDataTable) -> {
							dObSalesInvoiceCreateTestUtils
											.assertThatTheFollowingTaxDetailsExistForSalesInvoice(
															salesInvoiceCode,
															companyTaxesDataTable);
						});
		And("^the Company Section of SalesInvoice with code \"([^\"]*)\" is created as follows:$",
						(String salesInvoiceCode, DataTable companyTaxesDataTable) -> {
							dObSalesInvoiceCreateTestUtils
											.assertThatCompanySectionExistForSalesInvoice(
															salesInvoiceCode,
															companyTaxesDataTable);
						});

		Given("^the following SalesOrders with the following General, Company, and Details data exist:$",
						(DataTable salesOrdersDT) -> {
							dObSalesInvoiceCreateTestUtils
											.assertThatSalesOrderForSalesInvoiceIsExist(
															salesOrdersDT);
						});

		Given("^the following SalesOrders have the following Items with the following Qty, OrderUnit, SalesPrice and TotalAmount exist:$",
						(DataTable salesOrderItemsDT) -> {
							dObSalesInvoiceCreateTestUtils
											.assertThatSalesOrderItemsForSalesInvoiceIsExist(
															salesOrderItemsDT);
						});

		Given("^Last created SalesInvoice for SalesOrder was with code \"([^\"]*)\"$",
						(String salesInvoiceCode) -> {
							dObSalesInvoiceCreateTestUtils
											.assertSalesInvoiceExistsWithCode(salesInvoiceCode);
						});

		When("^\"([^\"]*)\" creates SalesInvoice for SalesOrder with the following values:$",
						(String userName, DataTable salesInvoiceValuesDT) -> {
							Response response = dObSalesInvoiceCreateTestUtils
											.createSalesInvoiceForSalesOrder(salesInvoiceValuesDT,
															userActionsTestUtils.getUserCookie(
																			userName));
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^a new SalesInvoice of type SalesOrder_Based is created with the following values:$",
						(DataTable invoiceDataTable) -> {
							dObSalesInvoiceCreateTestUtils.assertSalesInvoiceIsCreatedSuccessfully(
											invoiceDataTable);
						});

		Then("^the BusinessPartner Section of SalesInvoice for SalesOrder with code \"([^\"]*)\" is created as follows:$",
						(String salesInvoiceCode, DataTable businessPartnerValuesDT) -> {
							dObSalesInvoiceCreateTestUtils
											.assertThatBusinessPartnerIsCopiedFromSalesOrder(
															salesInvoiceCode,
															businessPartnerValuesDT);
						});

		When("^\"([^\"]*)\" requests to create SalesInvoice$", (String userName) -> {
			Cookie cookie = userActionsTestUtils.getUserCookie(userName);
			Response response = dObSalesInvoiceCreateTestUtils.sendGETRequest(cookie,
							IDObSalesInvoiceRestUrls.REQUEST_CREATE_URL);
			userActionsTestUtils.setUserResponse(userName, response);
		});

	}

	@Before
	public void setup(Scenario scenario) throws Exception {
		Map<String, Object> properties = getProperties();
		dObSalesInvoiceCreateTestUtils = new DObSalesInvoiceCreateTestUtils(properties,
						entityManagerDatabaseConnector);
		enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
		taxesViewTestUtils = new IObSalesInvoiceTaxesViewTestUtils(properties);
		taxesViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		salesOrderViewItemsTestUtils = new IObSalesOrderViewItemsTestUtils(properties,
        entityManagerDatabaseConnector);
		salesOrderViewItemsTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				databaseConnector.executeSQLScript(
								dObSalesInvoiceCreateTestUtils.getDbScriptsOneTimeExecution());
				hasBeenExecuted = true;
			}
			databaseConnector.executeSQLScript(dObSalesInvoiceCreateTestUtils.getDbScriptPath());
		}
	}

	@After
	public void afterEachScenario(Scenario scenario) throws SQLException {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
