package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.IIObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.masterdata.paymentterms.ICObPaymentTerms;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IObSalesInvoiceBusinessPartnerTestUtils extends DObSalesInvoiceTestUtils {

  public IObSalesInvoiceBusinessPartnerTestUtils(
          Map<String, Object> properties,
          EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    return str.toString();
  }

  public String getLockBusinessPartnerUrl(String salesInvoiceCode) {
    return IDObSalesInvoiceRestUrls.COMMAND_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.LOCK_BUSINESS_PARTNER;
  }

  public void assertBusinessPartnerExists(DataTable businessPartnerTable) {
    List<Map<String, String>> businessPartners =
            businessPartnerTable.asMaps(String.class, String.class);

    for (Map<String, String> businesssPartner : businessPartners) {
      IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartner =
              (IObSalesInvoiceBusinessPartnerGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getSalesInvoiceBusinessPartnerData",
                              constructSalesInvoiceBusinessPartner(businesssPartner));
      Assert.assertNotNull(salesInvoiceBusinessPartner);
    }
  }

  private Object[] constructSalesInvoiceBusinessPartner(Map<String, String> businesssPartner) {
    return new Object[]{
        businesssPartner.get(ISalesFeatureFileCommonKeys.SALES_ORDER).isEmpty() ? null
            : businesssPartner.get(ISalesFeatureFileCommonKeys.SALES_ORDER),
        businesssPartner.get(ISalesFeatureFileCommonKeys.DOWN_PAYMENT).isEmpty() ? null
            : new BigDecimal(businesssPartner.get(ISalesFeatureFileCommonKeys.DOWN_PAYMENT)),
        businesssPartner.get(ISalesFeatureFileCommonKeys.SI_CODE),
        businesssPartner.get(IFeatureFileCommonKeys.CUSTOMER),
        businesssPartner.get(ISalesFeatureFileCommonKeys.PAYMENT_TERM),
        businesssPartner.get(ISalesFeatureFileCommonKeys.CURRENCY)
    };
  }

  public void assertCorrectValuesAreDisplayedInBusinessPartner(
          Response response, DataTable businessPartnerTable) throws Exception {
    Map<String, String> businessPartnerExpected =
            businessPartnerTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> businessPartnerActual = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(businessPartnerExpected, businessPartnerActual);

    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {

    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.CUSTOMER, IIObSalesInvoiceBusinessPartnerGeneralModel.CUSTOMER);
    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.SALES_ORDER,
            IIObSalesInvoiceBusinessPartnerGeneralModel.SALES_ORDER);
    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.PAYMENT_TERM,
            IIObSalesInvoiceBusinessPartnerGeneralModel.PAYMENT_TERM);
    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.CURRENCY, IIObSalesInvoiceBusinessPartnerGeneralModel.CURRENCY);
  }

  public void assertThatResponsePaymentTermsDataIsCorrect(
          Response response, DataTable paymentTermsDataTable) throws Exception {
    List<Map<String, String>> expectedPaymentTermList =
            paymentTermsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualPaymentTermData = getListOfMapsFromResponse(response);

    for (int i = 0; i < actualPaymentTermData.size(); i++) {
      MapAssertion mapAssertion =
              new MapAssertion(expectedPaymentTermList.get(i), actualPaymentTermData.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
              IFeatureFileCommonKeys.PAYMENT_TERMS, ICObPaymentTerms.CODE, ICObPaymentTerms.NAME);
    }
  }

  public void assertTotalNumberOfRecordsEqualResponseSize(
          Response response, int totalRecordsNumber) {
    List<HashMap<String, Object>> responseData = getListOfMapsFromResponse(response);
    assertEquals(responseData.size(), totalRecordsNumber);
  }
}
