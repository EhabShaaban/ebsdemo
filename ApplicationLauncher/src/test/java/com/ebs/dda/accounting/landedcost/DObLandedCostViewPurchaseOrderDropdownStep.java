package com.ebs.dda.accounting.landedcost;

import java.sql.SQLException;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostViewPOsDropdownTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObLandedCostViewPurchaseOrderDropdownStep extends SpringBootRunner implements En {

  private static final String IMPORT_POS_SCENARIO_NAME = "Read all Import PurchaseOrders";
  private static final String LOCAL_POS_SCENARIO_NAME = "Read all Local PurchaseOrders";

  private static boolean hasBeenExecuted = false;

  private DObLandedCostViewPOsDropdownTestUtils utils;
  private DObPurchaseOrderViewAllTestUtils viewAllPurchaseOrderTestUtils;

  public DObLandedCostViewPurchaseOrderDropdownStep() {
    When("^\"([^\"]*)\" request to read all \"([^\"]*)\" PurchaseOrders for LandedCost creation$",
        (String username, String type) -> {
          utils.requestToreadPurchaseOrders(username, type, userActionsTestUtils);
        });

    Then("^the following PurchaseOrders will be presented to \"([^\"]*)\":$",
        (String username, DataTable purchaseOrdersDT) -> {
          utils.assertThatPurchaseOrdersAreRetrieved(userActionsTestUtils.getUserResponse(username),
              purchaseOrdersDT);
        });

    Then(
        "^total number of PurchaseOrders for LandedCost creation returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String username, Integer numOfRecords) -> {
          utils.assetThatTotalNumberOfRetrievedPOsIs(
              userActionsTestUtils.getUserResponse(username), numOfRecords);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObLandedCostViewPOsDropdownTestUtils(properties, entityManagerDatabaseConnector);
    viewAllPurchaseOrderTestUtils = new DObPurchaseOrderViewAllTestUtils(properties);
    viewAllPurchaseOrderTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains(IMPORT_POS_SCENARIO_NAME)
        || scenario.getName().contains(LOCAL_POS_SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector
            .executeSQLScript(viewAllPurchaseOrderTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(viewAllPurchaseOrderTestUtils.getDbScriptPath());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains(IMPORT_POS_SCENARIO_NAME)
        || scenario.getName().contains(LOCAL_POS_SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
