package com.ebs.dda.accounting.vendorinvoice;

import java.util.Map;

import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceItemsDropDownTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObVendorInvoiceItemsDropDownStep extends SpringBootRunner
				implements En, InitializingBean {

	public static final String ITEMS_SCENARIO_NAME = "Read All VendorItems for VendorInvoice";
	public static final String ITEMS_UNITS_SCENARIO_NAME = "Read All VendorItems Units for VendorInvoice";
	private DObVendorInvoiceItemsDropDownTestUtils utils;

	private boolean hasBeenExecutedOnce = false;

	public DObVendorInvoiceItemsDropDownStep() {
		When("^\"([^\"]*)\" requests to read all VendorItems for VendorInvoice with Code \"([^\"]*)\" and Vendor with Code \"([^\"]*)\" and BusinessUnit with Code \"([^\"]*)\"$",
						(String userName, String vendorInvoiceCode, String vendorCode,
										String businessUnitCode) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							Response response = utils.readAllVendorItems(cookie, vendorInvoiceCode, vendorCode, businessUnitCode);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		Then("^the following VendorItems are displayed to \"([^\"]*)\":$",
						(String userName, DataTable vendorItemsDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertThatVendorItemsResponseIs(response, vendorItemsDataTable);
						});
		When("^\"([^\"]*)\" requests to read all OrderUnits for VendorItem with Code \"([^\"]*)\" for VendorInvoice with Code \"([^\"]*)\" and Vendor with Code \"([^\"]*)\" and BusinessUnit with Code \"([^\"]*)\"$",
				(String userName, String itemCode, String vendorInvoiceCode, String vendorCode,
				 String businessUnitCode) -> {
					Cookie cookie = userActionsTestUtils.getUserCookie(userName);
					Response response = utils.readAllVendorItemUnits(cookie, itemCode, vendorInvoiceCode, vendorCode, businessUnitCode);
					userActionsTestUtils.setUserResponse(userName, response);
		});
		Then("^the following VendorItem OrderUnits are displayed to \"([^\"]*)\":$", (String userName, DataTable vendorItemUnitsDataTable) -> {
			Response response = userActionsTestUtils.getUserResponse(userName);
			utils.assertThatVendorItemUnitsResponseIs(response, vendorItemUnitsDataTable);
		});

	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		utils = new DObVendorInvoiceItemsDropDownTestUtils(properties);
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	@Before
	public void before(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), ITEMS_SCENARIO_NAME) || contains(scenario.getName(), ITEMS_UNITS_SCENARIO_NAME) ) {
			databaseConnector.createConnection();
			if (!hasBeenExecutedOnce) {
				databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
				hasBeenExecutedOnce = true;
			}
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), ITEMS_SCENARIO_NAME) || contains(scenario.getName(), ITEMS_UNITS_SCENARIO_NAME) ) {
			databaseConnector.closeConnection();
		}
	}
}
