package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObCollectionNotesReceivablesDropDownTestUtils extends DObCollectionTestUtils {

  public DObCollectionNotesReceivablesDropDownTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    return str.toString();
  }

  public void assertThatNotesReceivablesAreRetrieved(
      Response userResponse, DataTable notesReceivablesDT) {
    List<Map<String, String>> expectedNotesReceivables =
        notesReceivablesDT.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualNotesReceivables = getListOfMapsFromResponse(userResponse);
    for (int i = 0; i < expectedNotesReceivables.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedNotesReceivables.get(i), actualNotesReceivables.get(i));

      mapAssertion.assertValue(
          IFeatureFileCommonKeys.CODE, IDObNotesReceivablesGeneralModel.USER_CODE);
    }
  }
}
