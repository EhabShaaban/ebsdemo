package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import cucumber.api.DataTable;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class DObVendorInvoiceCommonTestUtils extends DObVendorInvoiceTestUtils {

  public DObVendorInvoiceCommonTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatCompanyExistWithCurrency(DataTable companyDataTable) {
    List<Map<String, String>> companies = companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> company : companies) {

      CObCompanyGeneralModel cObCompanyGeneralModel =
          (CObCompanyGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCompanyDataWithCurrency",
                  company.get(IFeatureFileCommonKeys.CODE),
                  company.get(IFeatureFileCommonKeys.NAME),
                  company.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO));
      Assertions.assertNotNull(cObCompanyGeneralModel);
    }
  }

  public void insertVendorInvoiceGeneralData(DataTable vendorInvoiceDataTable) {
    List<Map<String, String>> vendorInvoiceList =
        vendorInvoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendorInvoice : vendorInvoiceList) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertVendorInvoiceGeneralData", constructVIGeneralDataInsertionParams(vendorInvoice));
    }
  }

  private Object[] constructVIGeneralDataInsertionParams(Map<String, String> vendorInvoice) {
    return new Object[] {
      vendorInvoice.get(IFeatureFileCommonKeys.CODE),
      vendorInvoice.get(IFeatureFileCommonKeys.CREATION_DATE),
      vendorInvoice.get(IFeatureFileCommonKeys.CREATION_DATE),
      vendorInvoice.get(IFeatureFileCommonKeys.CREATED_BY),
      vendorInvoice.get(IFeatureFileCommonKeys.CREATED_BY),
      vendorInvoice.get(IFeatureFileCommonKeys.STATE),
      vendorInvoice.get(IFeatureFileCommonKeys.TYPE),
      vendorInvoice.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  public void insertVendorInvoiceCompanyData(DataTable vendorInvoiceDataTable) {
    List<Map<String, String>> vendorInvoiceList =
        vendorInvoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendorInvoice : vendorInvoiceList) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertVendorInvoiceCompanyData", constructVICompanyDataInsertionParams(vendorInvoice));
    }
  }

  private Object[] constructVICompanyDataInsertionParams(Map<String, String> vendorInvoice) {
    return new Object[] {
      vendorInvoice.get(IFeatureFileCommonKeys.CODE),
      vendorInvoice.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      vendorInvoice.get(IFeatureFileCommonKeys.COMPANY),
    };
  }

  public void insertVendorInvoiceDetails(DataTable invoicesDetailsDataTable) {
    List<Map<String, String>> invoicesDetailsDataList =
        invoicesDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoicesDetails : invoicesDetailsDataList) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertVendorInvoiceDetails", constructVIDetailsInsertionParams(invoicesDetails));
      if (invoicesDetails.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT) != null) {
        entityManagerDatabaseConnector.executeInsertQuery(
            "insertVendorInvoiceDownPayment",
            constructVIDownPaymentInsertionParams(invoicesDetails));
      }
    }
  }

  private Object[] constructVIDetailsInsertionParams(Map<String, String> invoicesDetails) {
    invoicesDetails = convertEmptyStringsToNulls(invoicesDetails);
    return new Object[] {
      invoicesDetails.get(IFeatureFileCommonKeys.CODE),
      invoicesDetails.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER),
      invoicesDetails.get(IAccountingFeatureFileCommonKeys.INVOICE_DATE),
      invoicesDetails.get(IAccountingFeatureFileCommonKeys.VENDOR),
      invoicesDetails.get(IFeatureFileCommonKeys.CODE),
      invoicesDetails.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
      invoicesDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      invoicesDetails.get(IFeatureFileCommonKeys.PAYMENT_TERMS)
    };
  }

  private Object[] constructVIDownPaymentInsertionParams(Map<String, String> invoicesDetails) {
    String[] downPayment =
        invoicesDetails.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT).split("-");
    String paymentRequestCode = downPayment[0];
    String downPaymentAmount = downPayment[1];
    return new Object[] {
      invoicesDetails.get(IFeatureFileCommonKeys.CODE), paymentRequestCode, downPaymentAmount
    };
  }

  public void insertVendorInvoiceItems(DataTable vendorInvoiceItemsDataTable) {
    List<Map<String, String>> vendorInvoiceItemsList =
        vendorInvoiceItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendorInvoiceItem : vendorInvoiceItemsList) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertVendorInvoiceItem", constructVIItemInsertionParams(vendorInvoiceItem));
    }
  }

  private Object[] constructVIItemInsertionParams(Map<String, String> vendorInvoiceItem) {
    String[] qtyOrderUnit =
        vendorInvoiceItem.get(IFeatureFileCommonKeys.QTY_ORDER_UNIT).split(" ", 2);
    String qtyOrderUnitValue = qtyOrderUnit[0];
    String orderUnit = qtyOrderUnit[1];
    String[] qtyBase = vendorInvoiceItem.get(IFeatureFileCommonKeys.QTY_BASE_UNIT).split(" ", 2);
    String qtyBaseUnit = qtyBase[1];

    return new Object[] {
      vendorInvoiceItem.get(IFeatureFileCommonKeys.CODE),
      vendorInvoiceItem.get(IFeatureFileCommonKeys.ITEM),
      qtyBaseUnit,
      orderUnit,
      qtyOrderUnitValue,
      vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)
    };
  }

  public void insertVendorInvoiceSummary(DataTable vendorInvoiceSummaryDataTable) {
    List<Map<String, String>> vendorInvoiceSummaries =
        vendorInvoiceSummaryDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendorInvoiceSummary : vendorInvoiceSummaries) {
      entityManagerDatabaseConnector.executeInsertQuery(
          "insertVendorInvoiceSummary", constructVISummaryInsertionParams(vendorInvoiceSummary));
    }
  }

  private Object[] constructVISummaryInsertionParams(Map<String, String> vendorInvoiceSummary) {
    return new Object[] {
      vendorInvoiceSummary.get(IFeatureFileCommonKeys.CODE),
      new BigDecimal(vendorInvoiceSummary.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT)),
      new BigDecimal(vendorInvoiceSummary.get(IAccountingFeatureFileCommonKeys.TOTAL_REMAINING)),
      new BigDecimal(
          vendorInvoiceSummary.get(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT) == null
              ? "0"
              : vendorInvoiceSummary.get(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT))
    };
  }

  public void insertVendorInvoicePostingDetails(DataTable postingDetailsDataTable) {
    List<Map<String, String>> postingDetailsMaps =
            postingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> postingDetailsMap : postingDetailsMaps) {
      entityManagerDatabaseConnector.executeInsertQuery(
              "insertVendorInvoicePostingDetails",
              constructVendorInvoicePostingDetailsInsertionParams(postingDetailsMap));
    }
  }

  private Object[] constructVendorInvoicePostingDetailsInsertionParams(
          Map<String, String> postingDetailsMap) {
    String[] currencyPrice =
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE).split(" = ");
    String[] leftHandSide = currencyPrice[0].split(" ");
    String[] rightHandSide = currencyPrice[1].split(" ");
    String companyCurrencyIso = leftHandSide[1];
    String value = rightHandSide[0];
    String documentCurrencyIso = rightHandSide[1];

    return new Object[]{
            postingDetailsMap.get(IFeatureFileCommonKeys.CODE),
            postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATED_BY),
            postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            companyCurrencyIso,
            documentCurrencyIso,
            Double.parseDouble(value),
            Double.parseDouble(value),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
            postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            postingDetailsMap.get(IFeatureFileCommonKeys.CODE),
            postingDetailsMap.get(IFeatureFileCommonKeys.CODE),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
            postingDetailsMap.get(IFeatureFileCommonKeys.CODE),
    };
  }
}
