package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestPostValueObject;
import com.google.gson.JsonObject;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class DObPaymentRequestMultithreadingPostTestUtils extends Thread {

  private final static Logger LOGGER = LoggerFactory
      .getLogger(DObPaymentRequestMultithreadingPostTestUtils.class);

  private DObPaymentRequestConcurrencyPostTestUtils utils;
  private String paymentRequestCode;
  private Map<String, String> postDataMap;
  private Cookie userCookie;
  private Response response;
  private String paymentType;
  private String paymentForm;
  private String dueDocumentType;

  public DObPaymentRequestMultithreadingPostTestUtils(
      DObPaymentRequestConcurrencyPostTestUtils utils) {
    this.utils = utils;
  }

  public void setPaymentRequestCode(String paymentRequestCode) {
    this.paymentRequestCode = paymentRequestCode;
  }

  public void setPostDataMap(Map<String, String> postDataMap) {
    this.postDataMap = postDataMap;
  }

  public void setUserCookie(Cookie userCookie) {
    this.userCookie = userCookie;
  }

  public void setPaymentType(String paymentType) {
    this.paymentType = paymentType;
  }

  public void setPaymentForm(String paymentForm) {
    this.paymentForm = paymentForm;
  }

  public void setDueDocumentType(String dueDocumentType) {
    this.dueDocumentType = dueDocumentType;
  }

  public Response getResponse() {
    return response;
  }

  public void run() {
    try {
      this.response =
          utils.sendPUTRequest(
              userCookie,
              this.utils.getActivationUrl(paymentRequestCode, paymentType, paymentForm, dueDocumentType),
              getPostValueObject(postDataMap));
      LOGGER.info("Thread " +
          Thread.currentThread().getId() +
          " is running for payment request " + paymentRequestCode);
      LOGGER.info("response.body() " + response.body().prettyPrint());
    } catch (Exception e) {
      // Throwing an exception
      LOGGER.error("Thread " +
          Thread.currentThread().getId() + " Didn't Create journal entry", e);
    }
  }

  public JsonObject getPostValueObject(Map<String, String> postDataMap) {

    JsonObject valueObject = new JsonObject();
    valueObject.addProperty(
        IDObPaymentRequestPostValueObject.DUE_DATE,
        postDataMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE));

    return valueObject;
  }
}
