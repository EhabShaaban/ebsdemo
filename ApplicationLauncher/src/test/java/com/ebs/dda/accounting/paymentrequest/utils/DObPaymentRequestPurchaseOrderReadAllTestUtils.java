package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderTotalAmountGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObPaymentRequestPurchaseOrderReadAllTestUtils
    extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestPurchaseOrderReadAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    return str.toString();
  }
  public static String clearOrderDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    return str.toString();
  }
	public String getReadAllPurchaseOrdersUrl(String businessUnitCode, String businessPartnerCode,
					String paymentType) {
		return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_QUERY + "/" + businessUnitCode + "/"
						+ businessPartnerCode + "/" + paymentType
						+ IDObPaymentRequestRestURLs.READ_ALL_ORDERS;
	}

  public void assertThatInvoicesDropDownResponseIsCorrect(
      Response response, DataTable purchaseOrdersDataTable) throws Exception {
    List<HashMap<String, Object>> actualPurchaseOrders = getListOfMapsFromResponse(response);
    List<Map<String, String>> expectedPurchaseOrdersMap =
        purchaseOrdersDataTable.asMaps(String.class, String.class);

    for (int i = 0; i < actualPurchaseOrders.size(); i++) {
      assertThatExpectedPurchaseOrdersMatcheActualOnes(
          expectedPurchaseOrdersMap.get(i), actualPurchaseOrders.get(i));
    }
  }

  public void assertThatResponseGoodsPODataIsCorrect(
      Response response, DataTable goodsPODataTable) {

    List<HashMap<String, Object>> actualGoodsPOData = getListOfMapsFromResponse(response);
    List<Map<String, String>> expectedGoodsPOsMap =
        goodsPODataTable.asMaps(String.class, String.class);

    for (int i = 0; i < actualGoodsPOData.size(); i++) {
      assertThatExpectedGoodsPOsMatcheActualOnes(
          expectedGoodsPOsMap.get(i), actualGoodsPOData.get(i));
    }
  }

  private void assertThatExpectedGoodsPOsMatcheActualOnes(
      Map<String, String> expectedGoodsPOsMap, HashMap<String, Object> actualGoodsPOData) {

    MapAssertion mapAssertion = new MapAssertion(expectedGoodsPOsMap, actualGoodsPOData);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObPurchaseOrderGeneralModel.CODE);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObSalesInvoiceGeneralModel.CURRENT_STATES);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObPurchaseOrderGeneralModel.OBJECT_TYPE_CODE);
  }

  private void assertThatExpectedPurchaseOrdersMatcheActualOnes(
      Map<String, String> expectedPurchaseOrderMap, HashMap<String, Object> actualPurchaseOrderMap)
      throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedPurchaseOrderMap, actualPurchaseOrderMap);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObPurchaseOrderGeneralModel.CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.COMPANY,
        IDObPurchaseOrderGeneralModel.COMPANY_CODE,
        IDObPurchaseOrderGeneralModel.COMPANY_NAME);

  }

  public void assertThatTotalNumberOfReturnedPurchaseOrdersIsCorrect(
      Response response, Integer expectedNumberOfPurchaseOrders) {
    List<HashMap<String, Object>> actualPurchaseOrders = getListOfMapsFromResponse(response);
    Integer actualNumberOfPurchaseOrders = actualPurchaseOrders.size();
    Assert.assertEquals(expectedNumberOfPurchaseOrders, actualNumberOfPurchaseOrders);
  }

  public void assertPurchaseOrdersExist(DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrdersListMap =
        purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersListMap) {
      DObPurchaseOrderTotalAmountGeneralModel orderTotalAmountGeneralModel =
          (DObPurchaseOrderTotalAmountGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrderTotalAmount",
                  getPurchaseOrderTotalAmountQueryParams(purchaseOrderMap));

      assertNotNull(
          "Purchase Order does not exist " + purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
          orderTotalAmountGeneralModel);
    }
  }

  private Object[] getPurchaseOrderTotalAmountQueryParams(Map<String, String> purchaseOrderMap) {
    return new Object[] {
            purchaseOrderMap.get(IAccountingFeatureFileCommonKeys.CURRENCY),
            purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
            "%" + purchaseOrderMap.get(IFeatureFileCommonKeys.STATE) + "%",
            purchaseOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
            purchaseOrderMap.get(IFeatureFileCommonKeys.VENDOR),
      new BigDecimal(purchaseOrderMap.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
    };
  }
}
