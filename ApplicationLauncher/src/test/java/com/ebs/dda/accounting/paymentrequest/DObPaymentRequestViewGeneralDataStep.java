package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestViewGeneralDataStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObPaymentRequestViewGeneralDataTestUtils utils;

  public DObPaymentRequestViewGeneralDataStep() {

    Given("^the following GeneralData for PaymentRequests exist:$",
        (DataTable generalDataTable) -> {
          utils.assertThatTheFollowingPRGeneralDataExist(generalDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of PaymentRequest with code \"([^\"]*)\"$",
        (String userName, String prCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.getViewGeneralDataUrl(prCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of GeneralData section for PaymentRequest with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String prCode, String userName, DataTable generalDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatViewGeneralDataResponseIsCorrect(response, generalDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestViewGeneralDataTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewGeneralData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View PR GeneralData,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          DObPaymentRequestViewGeneralDataTestUtils.getPaymentRequestDbScriptsOneTimeExecution());
        databaseConnector
                .executeSQLScript(DObPaymentRequestViewGeneralDataTestUtils.clearPaymentRequests());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View PR GeneralData,")) {
      databaseConnector.closeConnection();
    }
  }
}
