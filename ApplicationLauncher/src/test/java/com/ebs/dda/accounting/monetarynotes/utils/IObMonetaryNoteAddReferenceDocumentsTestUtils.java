package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;

import java.util.Map;

public class IObMonetaryNoteAddReferenceDocumentsTestUtils extends DObMonetaryNotesCommonTestUtils {

  public IObMonetaryNoteAddReferenceDocumentsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getLockReferenceDocumentsUrl(String code) {
    return IDObNotesReceivableRestURLs.BASE_COMMAND_URL
        + "/"
        + code
        + IDObNotesReceivableRestURLs.LOCK_REFERENCE_DOCUMENTS_SECTION;
  }

  public String getUnLockReferenceDocumentsUrl(String code) {
    return IDObNotesReceivableRestURLs.BASE_COMMAND_URL
        + "/"
        + code
        + IDObNotesReceivableRestURLs.UNLOCK_REFERENCE_DOCUMENTS_SECTION;
  }
}
