package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.collection.CObCollectionType;
import com.ebs.dda.jpa.accounting.collection.ICObCollectionType;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObCollectionTypesDropDownTestUtils extends DObCollectionTestUtils {
  public DObCollectionTypesDropDownTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    return str.toString();
  }

  public void assertCollectionTypesCountMatchesExpected(Integer typesNumber) {
    Long collectionTypeCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCollectionTypesCount");
    assertEquals(Long.valueOf(typesNumber), collectionTypeCount);
  }

  public void assertThatCollectionTypesExist(DataTable collectionTypesDataTable) {
    List<Map<String, String>> collectionTypesList =
        collectionTypesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionTypes : collectionTypesList) {

      String collectionTypesCode = collectionTypes.get(IFeatureFileCommonKeys.CODE);
      String collectionTypesName = collectionTypes.get(IFeatureFileCommonKeys.NAME);

      CObCollectionType cObCollectionType =
          (CObCollectionType)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCollectionTypesByCodeAndName", collectionTypesCode, collectionTypesName);

      assertNotNull(cObCollectionType);
    }
  }


  public void assertThatReadAllCollectionTypesResponseIsCorrect(
          Response response, DataTable collectionTypeDataTable) throws Exception {

    List<Map<String, String>> expectedCollectionTypes =
            collectionTypeDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualCollectionTypes = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedCollectionTypes.size(); i++) {

      MapAssertion mapAssertion =
              new MapAssertion(expectedCollectionTypes.get(i), actualCollectionTypes.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
              IAccountingFeatureFileCommonKeys.COLLECTION_TYPE,
              ICObCollectionType.USER_CODE,
              ICObCollectionType.NAME);
    }
  }
}
