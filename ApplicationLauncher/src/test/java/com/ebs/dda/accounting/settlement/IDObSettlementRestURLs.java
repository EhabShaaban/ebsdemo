package com.ebs.dda.accounting.settlement;

public interface IDObSettlementRestURLs {
  String COMMAND_URL = "/command/settlement";
  String BASE_URL = "/services/accounting/settlement";
  String VIEW_ALL_URL = BASE_URL;
  String VIEW_GENERAL_DATA = BASE_URL + "/%s/generaldata";
  String VIEW_DETAILS = BASE_URL + "/%s/details";
  String VIEW_COMPANY = BASE_URL + "/%s/companydata";
  String VIEW_ACTIVATION_DETAILS = BASE_URL + "/%s/postingdetails";
  String AUTHORIZED_ACTIONS = BASE_URL + "/authorizedactions";
  String CREATE_URL = COMMAND_URL;
  String VIEW_ACCOUNTING_DETAILS = "/accountingdetails";
  String LOCK_ACCOUNTING_DETAILS = COMMAND_URL + "/%s/lock/accountingdetails";
  String UNLOCK_ACCOUNTING_DETAILS = COMMAND_URL + "/%s/unlock/accountingdetails";
  String UNLOCK_LOCKED_SECTIONS = COMMAND_URL + "/unlock/lockedsections/";
  String REQUEST_CREATE_URL = COMMAND_URL;
  String DELETE_ACCOUNTING_DETAILS = COMMAND_URL + "/%s/accountingdetail/";
  String UPDATE_ACCOUNTING_DETAILS = COMMAND_URL + "/%s/update/accountingdetails";
  String VIEW_SUMMARY = "/summary";
  String POST = "/post";
}
