package com.ebs.dda.accounting.costing.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.inventory.IInventoryFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObCostingCommonTestUtils extends CommonTestUtils {

  ObjectMapper objectMapper;

  public DObCostingCommonTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/LookupSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts_ViewAll.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    return str.toString();
  }

  public String clearCostingInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/costing/costing_clear.sql");
    return str.toString();
  }

  public void insertCosting(DataTable costing) {
    List<Map<String, String>> costingListMap = costing.asMaps(String.class, String.class);
    for (Map<String, String> costingMap : costingListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertCosting", constructCostingDataInsertionParams(costingMap));
    }
  }

  public void insertCostingCompanyData(DataTable costingCompanyData) {
    List<Map<String, String>> costingCompanyDataListMap =
        costingCompanyData.asMaps(String.class, String.class);
    for (Map<String, String> costingCompanyDataMap : costingCompanyDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertCostingDocumentCompanyData",
          constructCostingCompanyDataInsertionParams(costingCompanyDataMap));
    }
  }

  public void insertCostingDetails(DataTable costingDetails) {

    List<Map<String, String>> costingDetailsDataListMap =
        costingDetails.asMaps(String.class, String.class);
    for (Map<String, String> costingDetailsDataMap : costingDetailsDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertCostingDetailsData",
          constructCostingDetailsDataInsertionParams(costingDetailsDataMap));
    }
  }

  public void insertCostingAccountingDetails(DataTable costingAccountingDetails) {

    List<Map<String, String>> costingAccountingDetailsDataListMap =
        costingAccountingDetails.asMaps(String.class, String.class);
    for (Map<String, String> costingAccountingDetailsDataMap :
        costingAccountingDetailsDataListMap) {
      insertAccountingDetails(costingAccountingDetailsDataMap);
      insertOrderAccountingDetails(costingAccountingDetailsDataMap);
    }
  }

  public void insertCostingItems(DataTable costingItemsDataTable) {
    List<Map<String, String>> costingItemListMap =
        costingItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> costingItemDataMap : costingItemListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertCostingItemsData", constructCostingItemDataInsertionParams(costingItemDataMap));
    }
  }

  private Object[] constructCostingItemDataInsertionParams(Map<String, String> costingItemDataMap) {
    String item = costingItemDataMap.get(IFeatureFileCommonKeys.ITEM);

    return new Object[] {
      costingItemDataMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_PRICE).split(" ")[0],
      !costingItemDataMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_PRICE).isEmpty()
          ? costingItemDataMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_PRICE).split(" ")[0]
          : null,
      costingItemDataMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_LANDED_COST)
          .split(" ")[0],
      !costingItemDataMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_LANDED_COST).isEmpty()
          ? costingItemDataMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_LANDED_COST)
              .split(" ")[0]
          : null,
      costingItemDataMap.get(IFeatureFileCommonKeys.CODE),
      item,
      costingItemDataMap.get(IFeatureFileCommonKeys.UOM),
      costingItemDataMap.get(IAccountingFeatureFileCommonKeys.WEIGHT_PERCENTAGE),
      costingItemDataMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
      costingItemDataMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
    };
  }

  private void insertAccountingDetails(Map<String, String> costingAccountingDetailsDataMap) {
    entityManagerDatabaseConnector.executeInsertQuery(
        "insertAccountingDocumentAccountingDetails",
        constructAccountingDetailsQueryParams(costingAccountingDetailsDataMap));
  }

  private void insertOrderAccountingDetails(Map<String, String> costingAccountingDetailsDataMap) {
    if ("PO"
        .equals(costingAccountingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.SUBLEDGER))) {
      entityManagerDatabaseConnector.executeInsertQuery(
          "insertOrderDocumentAccountingDetails",
          costingAccountingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT));
    }
  }

  private Object[] constructAccountingDetailsQueryParams(
      Map<String, String> costingAccountingDetailsDataMap) {
    return new Object[] {
      costingAccountingDetailsDataMap.get(IFeatureFileCommonKeys.CODE),
      costingAccountingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.SUBLEDGER),
      costingAccountingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
      costingAccountingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
      new BigDecimal(costingAccountingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.VALUE))
    };
  }

  public void insertCostingActivationDetails(DataTable costingActivationDetails) {

    List<Map<String, String>> costingActivationDetailsDataListMap =
        costingActivationDetails.asMaps(String.class, String.class);
    for (Map<String, String> costingActivationDetailsDataMap :
        costingActivationDetailsDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertCostingActivationDetailsData",
          constructCostingActivationDetailsDataInsertionParams(costingActivationDetailsDataMap));
    }
  }

  private Object[] constructCostingDataInsertionParams(Map<String, String> costingMap) {
    return new Object[] {
      costingMap.get(IFeatureFileCommonKeys.CODE),
      costingMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      costingMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      costingMap.get(IFeatureFileCommonKeys.CREATED_BY),
      costingMap.get(IFeatureFileCommonKeys.CREATED_BY),
      costingMap.get(IFeatureFileCommonKeys.STATE),
      costingMap.get(IFeatureFileCommonKeys.CODE),
      costingMap.get(IFeatureFileCommonKeys.CODE)
    };
  }

  private Object[] constructCostingCompanyDataInsertionParams(
      Map<String, String> costingCompanyDataMap) {
    return new Object[] {
      costingCompanyDataMap.get(IFeatureFileCommonKeys.CODE),
      costingCompanyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      costingCompanyDataMap.get(IFeatureFileCommonKeys.COMPANY),
    };
  }

  private Object[] constructCostingDetailsDataInsertionParams(
      Map<String, String> costingDetailsDataMap) {
    return new Object[] {
      costingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE_ESTIMATE_TIME)
          .split(" ")[0],
      costingDetailsDataMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE_ACTUAL_TIME)
          .split(" ")[0],
      costingDetailsDataMap.get(IFeatureFileCommonKeys.CODE),
      costingDetailsDataMap.get(IInventoryFeatureFileCommonKeys.GOODS_RECEIPT)
    };
  }

  private Object[] constructCostingActivationDetailsDataInsertionParams(
      Map<String, String> costingActivationDetailsDataMap) {
    String[] currencyPrice =
        costingActivationDetailsDataMap
            .get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE)
            .split(" = ");
    String[] leftHandSide = currencyPrice[0].split(" ");
    String[] rightHandSide = currencyPrice[1].split(" ");
    String companyCurrencyIso = leftHandSide[1];
    String value = rightHandSide[0];
    String documentCurrencyIso = rightHandSide[1];
    return new Object[] {
      costingActivationDetailsDataMap.get(IFeatureFileCommonKeys.CODE),
      costingActivationDetailsDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
      costingActivationDetailsDataMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
      costingActivationDetailsDataMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
      companyCurrencyIso,
      documentCurrencyIso,
      value,
      value,
      costingActivationDetailsDataMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR),
    };
  }

  public void assertThatCostingDocumentCreatedSuccessfully(DataTable costingDataTable) {
    List<Map<String, String>> costingDataListMap =
        costingDataTable.asMaps(String.class, String.class);
    for (Map<String, String> costingDataMap : costingDataListMap) {

      Object creationDate =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCostingDocument", constructCostingDocumentDataParams(costingDataMap));

      assertDateEquals(
          costingDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
          new DateTime(creationDate).withZone(DateTimeZone.UTC));
    }
  }

  private Object[] constructCostingDocumentDataParams(Map<String, String> costingDataMap) {
    return new Object[] {
      costingDataMap.get(IFeatureFileCommonKeys.CODE),
      costingDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
      "%\"" + costingDataMap.get(IFeatureFileCommonKeys.STATE) + "\"%",
      costingDataMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  private Object[] constructCostingDocumentDataUpdateParams(Map<String, String> costingDataMap) {
    return new Object[] {
      costingDataMap.get(IFeatureFileCommonKeys.CODE),
      costingDataMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      "%\"" + costingDataMap.get(IFeatureFileCommonKeys.STATE) + "\"%"
    };
  }

  public void assertThatCostingCompanyCreatedSuccessfully(DataTable costingCompanyDataTable) {
    List<Map<String, String>> costingCompanyDataListMap =
        costingCompanyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> costingCompanyDataMap : costingCompanyDataListMap) {

      Long costingId =
          (Long)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCostingCompany", constructCostingComapnyDataParams(costingCompanyDataMap));

      assertNotNull(
          costingId,
          "Costing with code "
              + costingCompanyDataMap.get(IFeatureFileCommonKeys.CODE)
              + " Company Data is not Created");
    }
  }

  private Object[] constructCostingComapnyDataParams(Map<String, String> costingCompanyDataMap) {
    return new Object[] {
      costingCompanyDataMap.get(IFeatureFileCommonKeys.CODE),
      costingCompanyDataMap.get(IFeatureFileCommonKeys.COMPANY),
      costingCompanyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertCostingDetails(String operationType, DataTable costingDetailsTable) {
    List<Map<String, String>> costingDetailsListMap =
        costingDetailsTable.asMaps(String.class, String.class);
    for (Map<String, String> costingDetailsMap : costingDetailsListMap) {

      IObCostingDetailsGeneralModel iObCostingDetailsGeneralModel =
          (IObCostingDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCostingDetails", constructCostingDetailsDataParams(costingDetailsMap));
      assertNotNull(
          iObCostingDetailsGeneralModel,
          "Costing with code "
              + costingDetailsMap.get(IFeatureFileCommonKeys.CODE)
              + " Details is not Created");
      if ("Created".equals(operationType)) {
        assertDateEquals(
            new DateTime().toString("dd-MMM-yyyy hh:mm a"),
            iObCostingDetailsGeneralModel.getCreationDate());
      }
      assertDateEquals(
          new DateTime().toString("dd-MMM-yyyy hh:mm a"),
          iObCostingDetailsGeneralModel.getModifiedDate());
    }
  }

  private Object[] constructCostingDetailsDataParams(Map<String, String> costingCompanyDataMap) {
    String exchangeRateEstimateTime =
        costingCompanyDataMap.get(
            IAccountingFeatureFileCommonKeys.COSTING_EXCHANGE_RATE_ESTIMATE_TIME);
    String exchangeRateActualTime =
        costingCompanyDataMap.get(
            IAccountingFeatureFileCommonKeys.COSTING_EXCHANGE_RATE_ACTUAL_TIME);
    return new Object[] {
      exchangeRateEstimateTime.split(" ")[0],
      exchangeRateEstimateTime.isEmpty() ? null : exchangeRateEstimateTime.split(" ")[1],
      exchangeRateActualTime.split(" ")[0],
      exchangeRateActualTime.isEmpty() ? null : exchangeRateActualTime.split(" ")[1],
      costingCompanyDataMap.get(IFeatureFileCommonKeys.CODE),
      costingCompanyDataMap.get(IFeatureFileCommonKeys.GOODS_RECEIPT_CODE)
    };
  }

  public void assertThatAccountingDetailsIsCreatedSuccessfully(DataTable accountingDetailsTable) {
    List<Map<String, String>> accountingDetailsMapList =
        accountingDetailsTable.asMaps(String.class, String.class);

    accountingDetailsMapList.forEach(
        accountingDetailsMap -> {
          Object accountingDetail =
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCostingAccountingDetails",
                  constructGetAccountingDetailQueryParams(accountingDetailsMap));

          Assert.assertNotNull("Costing AccountingDetail is not created", accountingDetail);
        });
  }

  private Object[] constructGetAccountingDetailQueryParams(
      Map<String, String> accountingDetailsMap) {
    return new Object[] {
      accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT),
      accountingDetailsMap.get(IFeatureFileCommonKeys.CODE),
      accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
      accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
      accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.VALUE)
    };
  }

  public void assertThatCostingItemsCreatedSuccessfully(DataTable costingItemsDataTable) {
    List<Map<String, String>> costingItemsMapList =
        costingItemsDataTable.asMaps(String.class, String.class);

    costingItemsMapList.forEach(
        costingItemMap -> {
          IObCostingItemGeneralModel costingItem =
              (IObCostingItemGeneralModel)
                  entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                      "getCostingItem", constructGetCostingItemQueryParams(costingItemMap));

          Assert.assertNotNull("Costing Item is not created ", costingItem);
        });
  }

  private Object[] constructGetCostingItemQueryParams(Map<String, String> costingItemMap) {
    return new Object[] {
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_PRICE_IN_LOCAL_CURRENCY),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_PRICE_IN_LOCAL_CURRENCY),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_LANDED_COST),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_LANDED_COST),
      costingItemMap.get(IFeatureFileCommonKeys.CODE),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.ITEM),
      costingItemMap.get(IFeatureFileCommonKeys.UOM),
      new BigDecimal(costingItemMap.get(IAccountingFeatureFileCommonKeys.WEIGHT_PERCENTAGE))
          .divide(new BigDecimal("100")),
      costingItemMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE),
      costingItemMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
      costingItemMap.get(IAccountingFeatureFileCommonKeys.UNIT_PRICE_IN_LOCAL_CURRENCY)
    };
  }

  public void assertThatCostingDocumentUpdatedSuccessfully(DataTable costingDataTable) {
    List<Map<String, String>> costingDataListMap =
        costingDataTable.asMaps(String.class, String.class);
    for (Map<String, String> costingDataMap : costingDataListMap) {

      Object creationDate =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getUpdatedCostingDocument",
              constructCostingDocumentDataUpdateParams(costingDataMap));

      assertDateEquals(
          costingDataMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
          new DateTime(creationDate).withZone(DateTimeZone.UTC));
    }
  }
}
