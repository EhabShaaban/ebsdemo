package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DOMonetaryNotesViewAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObMonetaryNotesViewAccountingDetailsStep extends SpringBootRunner implements En {

  private DOMonetaryNotesViewAccountingDetailsTestUtils utils;

  public DObMonetaryNotesViewAccountingDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view AccountingDetails section of MonetaryNotes with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAccountingDetailsResponse(monetaryNoteCode, userCookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of AccountingDetails section for MonetaryNotes with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String monetaryNoteCode, String userName, DataTable accountingDetailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayed(response, accountingDetailsDataTable);
        });

    Then(
        "^an empty AccountingDetails section for MonetaryNotes with code \\\"([^\\\"]*)\\\" is displayed to \"([^\"]*)\"$",
        (String monetaryNoteCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatAccountingDetailsAreEmpty(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DOMonetaryNotesViewAccountingDetailsTestUtils(properties,entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View AccountingDetails section in MonetaryNotes,")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }
}
