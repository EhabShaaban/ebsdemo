package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceSaveEditItemTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceSaveEditItemStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObVendorInvoiceSaveEditItemTestUtils invoiceSaveEditItemTestUtils;

  public DObVendorInvoiceSaveEditItemStep() {

    Given(
            "^InvoiceItems section of Vendor Invoice with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
            (String vendorInvoiceCode, String userName, String dateTime) -> {
              invoiceSaveEditItemTestUtils.freeze(dateTime);
              String lockURL =
                      IDObVendorInvoiceRestUrls.COMMAND_URL
                              + vendorInvoiceCode
                              + IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION;
              Response response =
                      userActionsTestUtils.lockSection(userName, lockURL, vendorInvoiceCode);
              invoiceSaveEditItemTestUtils.assertResponseSuccessStatus(response);
            });

    When(
            "^\"([^\"]*)\" saves the following edited item with id \"([^\"]*)\" to the InvoiceItems of Vendor Invoice with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
            (String userName,
             String itemId,
             String vendorInvoiceCode,
             String dateTime,
             DataTable itemData) -> {
              invoiceSaveEditItemTestUtils.freeze(dateTime);
              Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
              Response response =
                      invoiceSaveEditItemTestUtils.saveItemToInvoiceSection(
                              userCookie, vendorInvoiceCode, itemId, itemData);
              userActionsTestUtils.setUserResponse(userName, response);
            });

    Then(
            "^Vendor Invoice with Code \"([^\"]*)\" is updated as follows:$",
            (String invoiceCode, DataTable invoiceItemDataTable) -> {
              invoiceSaveEditItemTestUtils.assertThatInvoiceIsUpdatedByUserAtDate(
                      invoiceCode, invoiceItemDataTable);
            });

    Then(
            "^InvoiceItems section of Vendor Invoice with Code \"([^\"]*)\" and ItemId \"([^\"]*)\" is updated as follows:$",
            (String invoiceCode, String itemId, DataTable invoiceItemDataTable) -> {
              invoiceSaveEditItemTestUtils.assertVendorInvoiceItemExists(
                      invoiceCode, invoiceItemDataTable);
            });

    Then(
            "^the lock by \"([^\"]*)\" on InvoiceItems section of Vendor Invoice with Code \"([^\"]*)\" is released$",
            (String userName, String invoiceCode) -> {
              invoiceSaveEditItemTestUtils.assertResponseSuccessStatus(
                      userActionsTestUtils.getUserResponse(userName));
              invoiceSaveEditItemTestUtils.assertThatLockIsReleased(
                      userName, invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
            });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    invoiceSaveEditItemTestUtils = new DObVendorInvoiceSaveEditItemTestUtils(properties);
    invoiceSaveEditItemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(
        scenario.getName(), "Save \"editable\" fields in \"editable states\" for VendorInvoice")) {

      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
          databaseConnector.executeSQLScript(
                  DObVendorInvoiceTestUtils.clearVendorInvoices());
        databaseConnector.executeSQLScript(
            invoiceSaveEditItemTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(invoiceSaveEditItemTestUtils.getDbScriptPath());

        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(
        scenario.getName(), "Save \"editable\" fields in \"editable states\" for VendorInvoice")) {
      invoiceSaveEditItemTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSectionsNewStandard(
          IDObVendorInvoiceRestUrls.COMMAND_URL,
          IDObVendorInvoiceRestUrls.UNLOCK_ALL_SECTION,
          invoiceSaveEditItemTestUtils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
