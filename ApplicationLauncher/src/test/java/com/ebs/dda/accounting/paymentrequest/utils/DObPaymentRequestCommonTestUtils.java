package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestSectionNames;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestAccountDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import cucumber.api.DataTable;
import org.junit.Assert;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.util.*;

import static com.ebs.dac.foundation.realization.statemachine.BasicStateMachine.DRAFT_STATE;
import static com.ebs.dac.foundation.realization.statemachine.PrimitiveStateMachine.ACTIVE_STATE;
import static org.junit.Assert.assertNotNull;

public class DObPaymentRequestCommonTestUtils extends AccountingDocumentCommonTestUtils {

  private String PAYMENT_REQUEST_JMX_LOCK_BEAN_NAME = "PaymentRequestEntityLockCommand";

  public DObPaymentRequestCommonTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static String getPaymentRequestDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append("," + "db-scripts/accounting/payment-request/payment-request.sql");
    str.append("," + "db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
    str.append(
        ","
            + "db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    return str.toString();
  }

  public static String clearPaymentRequests() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/payment-request/PaymentRequest_Clear.sql");
    return str.toString();
  }

  public void assertThatTheFollowingPRGeneralDataExist(DataTable generalDataTable) {
    List<Map<String, String>> prGeneralDataListMap =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prGeneralDataMap : prGeneralDataListMap) {
      DObPaymentRequestGeneralModel prGeneralData =
          (DObPaymentRequestGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPRGeneralData", constructPaymentRequestQueryParams(prGeneralDataMap));
      Assert.assertNotNull(
          "PR General Data for PR with code "
              + prGeneralDataMap.get(IAccountingFeatureFileCommonKeys.PR_CODE)
              + " doesn't exist",
          prGeneralData);
    }
  }

  private Object[] constructPaymentRequestQueryParams(Map<String, String> paymentRequestMap) {
    return new Object[] {
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PAYMENT_TYPE),
      "%" + paymentRequestMap.get(IFeatureFileCommonKeys.STATE) + "%",
      paymentRequestMap.get(IFeatureFileCommonKeys.CREATED_BY),
      paymentRequestMap.get(IFeatureFileCommonKeys.CREATION_DATE),
    };
  }

  protected void assertThatPRCompanyDataExists(
      String paymentRequestCode, Map<String, String> prCompanyDataMap) {
    Object[] queryParams =
        constructPaymentRequestCompanyDataQueryParams(paymentRequestCode, prCompanyDataMap);
    Object prCompanyData =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPRCompanyData", queryParams);
    Assert.assertNotNull(
        "PR Company Data for PR doesn't exist " + Arrays.toString(queryParams), prCompanyData);
  }

  private Object[] constructPaymentRequestCompanyDataQueryParams(
      String paymentRequestCode, Map<String, String> paymentRequestMap) {
    return new Object[] {
      paymentRequestCode,
      paymentRequestMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      paymentRequestMap.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void assertThatResourceIsLockedByUser(String userName, String prCode, String sectionName)
      throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=" + PAYMENT_REQUEST_JMX_LOCK_BEAN_NAME);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, prCode};
    String[] parameterTypes =
        new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
        (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);
    assertNotNull(lockReleaseDetails);
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, PAYMENT_REQUEST_JMX_LOCK_BEAN_NAME, sectionName);
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION,
            IDObPaymentRequestSectionNames.COMPANY_DATA_SECTION));
  }

  protected void retrieveAccountingDetailsData(
      String paymentRequestCode, Map<String, String> accountDetailsMap) {
    if (accountDetailsMap
            .get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE)
            .equals(AccountingEntry.CREDIT)
        && !accountDetailsMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT).isEmpty()) {
      Optional<IObPaymentRequestAccountDetailsGeneralModel> prAccountingDetails =
          Optional.ofNullable(
              (IObPaymentRequestAccountDetailsGeneralModel)
                  entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                      "getPRAccountingDetailsByPRCode",
                      getExpectedAccountingDetailsData(paymentRequestCode, accountDetailsMap)));
      Assert.assertTrue(
          "PR Accounting Details with code " + accountDetailsMap.toString() + " doesn't exist",
          prAccountingDetails.isPresent());
    }
  }

  private Object[] getExpectedAccountingDetailsData(
      String paymentRequestCode, Map<String, String> accountDetailsMap) {
    return new Object[] {
      accountDetailsMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
      accountDetailsMap.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT),
      getBigDecimal(accountDetailsMap.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
      paymentRequestCode,
      accountDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE)
    };
  }

  public void insertPaymentRequestGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> prGeneralDataListMap =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prGeneralDataMap : prGeneralDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertPaymentRequestGeneralData",
          constructPRGeneralDataInsertionParams(prGeneralDataMap));
    }
  }

  private Object[] constructPRGeneralDataInsertionParams(Map<String, String> paymentRequestMap) {
    String state = paymentRequestMap.get(IFeatureFileCommonKeys.STATE);

    if (state.equals(DRAFT_STATE)) {
      state = "[\"" + state + "\"]";
    } else {
      state = "[\"" + ACTIVE_STATE + "\",\"" + state + "\"]";
    }

    return new Object[] {
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
      paymentRequestMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      paymentRequestMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      paymentRequestMap.get(IFeatureFileCommonKeys.CREATED_BY),
      paymentRequestMap.get(IFeatureFileCommonKeys.CREATED_BY),
      state,
      paymentRequestMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PAYMENT_TYPE),
      getPaymentRequestRemaining(paymentRequestMap.get(IAccountingFeatureFileCommonKeys.REMAINING)),
            paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
            paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_CODE)
    };
  }

  private Object getPaymentRequestRemaining(String remaining) {
    if (remaining == null || remaining.isEmpty()) {
      return null;
    }
    return new BigDecimal(remaining);
  }

  public void insertPaymentRequestCompanyData(DataTable companyDataTable) {
    List<Map<String, String>> prCompanyDataListMap =
        companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prComapanyDataMap : prCompanyDataListMap) {

      Object[] params =
          new Object[] {
            prComapanyDataMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
            prComapanyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
            prComapanyDataMap.get(IFeatureFileCommonKeys.COMPANY)
          };

      entityManagerDatabaseConnector.executeInsertQuery("insertPaymentRequestCompanyData", params);
    }
  }

  public void insertPaymentRequestDetailsData(DataTable detailsDataTable) {
    List<Map<String, String>> prDetailsDataListMap =
        detailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prDetailsDataMap : prDetailsDataListMap) {

      Object[] params = constructPRDetailsInsertionParams(prDetailsDataMap);

      entityManagerDatabaseConnector.executeInsertQuery("insertPaymentRequestDetailsData", params);
      insertDueDocRecord(prDetailsDataMap);
    }
  }

  private void insertDueDocRecord(Map<String, String> prDetailsDataMap) {

    String prCode = prDetailsDataMap.get(IAccountingFeatureFileCommonKeys.PR_CODE);
    String dueDocType = prDetailsDataMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE);

    String dueDoc = prDetailsDataMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT);
    dueDoc = (dueDoc.isEmpty()) ? null : dueDoc;

    if (dueDocType.equals(DueDocumentTypeEnum.PURCHASEORDER)) {
      entityManagerDatabaseConnector.executeInsertQuery(
          "insertIObPaymentRequestPurchaseOrderPaymentDetails", new Object[] {prCode, dueDoc});
    } else if (dueDocType.equals(DueDocumentTypeEnum.INVOICE)) {
      entityManagerDatabaseConnector.executeInsertQuery(
          "insertIObPaymentRequestInvoicePaymentDetails", new Object[] {prCode, dueDoc});
    } else if (dueDocType.equals(DueDocumentTypeEnum.CREDITNOTE)) {
      entityManagerDatabaseConnector.executeInsertQuery(
          "insertIObPaymentRequestCreditNotePaymentDetails", new Object[] {prCode, dueDoc});
    }
  }

  private Object[] constructPRDetailsInsertionParams(Map<String, String> paymentRequestMap) {

    paymentRequestMap = convertEmptyStringsToNulls(paymentRequestMap);

    return new Object[] {
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE),
      getBigDecimal(paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT)),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_DESCRIPTION),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PAYMENT_FORM),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR_ITEM),
      paymentRequestMap.get(IFeatureFileCommonKeys.TREASURY),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.BANK_TRANS_REF),
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.EXPECTED_DUE_DATE),
    };
  }

  public void executeAccountDeterminationFunction() {
    entityManagerDatabaseConnector.executeInsertQuery(
        "createAccountingDetailsDeterminationFunction");
  }

  public void insertPaymentRequestPostingDetails(DataTable postingDetailsDataTable) {
    List<Map<String, String>> postingDetailsMaps =
        postingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> postingDetailsMap : postingDetailsMaps) {
      entityManagerDatabaseConnector.executeInsertQuery(
          "insertPaymentRequestPostingDetails",
          constructPaymentRequestPostingDetailsInsertionParams(postingDetailsMap));
    }
  }

  private Object[] constructPaymentRequestPostingDetailsInsertionParams(
      Map<String, String> postingDetailsMap) {
    String[] currencyPrice =
        postingDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE).split(" = ");
    String[] leftHandSide = currencyPrice[0].split(" ");
    String[] rightHandSide = currencyPrice[1].split(" ");
    String companyCurrencyIso = leftHandSide[1];
    String value = rightHandSide[0];
    String documentCurrencyIso = rightHandSide[1];

    return new Object[] {
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
      companyCurrencyIso,
      documentCurrencyIso,
      new BigDecimal(value),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
    };
  }
}
