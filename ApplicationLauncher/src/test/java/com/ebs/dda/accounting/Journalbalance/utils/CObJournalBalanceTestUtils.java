package com.ebs.dda.accounting.Journalbalance.utils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;
import cucumber.api.DataTable;

public class CObJournalBalanceTestUtils extends CommonTestUtils {

  public CObJournalBalanceTestUtils(Map<String, Object> properties) {
    super();
    setProperties(properties);
  }

  public void insertBankJournalBalance(DataTable bankJournalDataTable) {
    List<Map<String, String>> bankJournalBalanceList =
        bankJournalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> bankJournalBalanceMap : bankJournalBalanceList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject("insertBankJournalBalance",
          getInsertBankJournalBalanceParams(bankJournalBalanceMap));
    }
  }

  private Object[] getInsertBankJournalBalanceParams(Map<String, String> bankJournalBalanceMap) {
    return new Object[] {bankJournalBalanceMap.get(IFeatureFileCommonKeys.CODE),
        bankJournalBalanceMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        bankJournalBalanceMap.get(IFeatureFileCommonKeys.COMPANY),
        bankJournalBalanceMap.get(IFeatureFileCommonKeys.CURRENCY),
        bankJournalBalanceMap.get(IFeatureFileCommonKeys.BALANCE),
        bankJournalBalanceMap.get(IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER)};
  }

  public void insertTreasuryJournalBalance(DataTable treasuryJournalDataTable) {
    List<Map<String, String>> treasuryJournalBalanceList =
        treasuryJournalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> treasuryJournalBalanceMap : treasuryJournalBalanceList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject("insertTreasuryJournalBalance",
          getInsertTreasuryJournalBalanceParams(treasuryJournalBalanceMap));
    }
  }

  private Object[] getInsertTreasuryJournalBalanceParams(
      Map<String, String> treasuryJournalBalanceMap) {
    return new Object[] {treasuryJournalBalanceMap.get(IFeatureFileCommonKeys.CODE),
        treasuryJournalBalanceMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        treasuryJournalBalanceMap.get(IFeatureFileCommonKeys.COMPANY),
        treasuryJournalBalanceMap.get(IFeatureFileCommonKeys.CURRENCY),
        treasuryJournalBalanceMap.get(IFeatureFileCommonKeys.BALANCE),
        treasuryJournalBalanceMap.get(IFeatureFileCommonKeys.TREASURY)};
  }

  public void assertThatJournalBalanceIsUpdated(String journalBalanceCode, String updatedBalance) {

    CObJournalBalanceGeneralModel updatedJournalBalance =
        (CObJournalBalanceGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getUpdatedJournalBalance", journalBalanceCode,
                new BigDecimal(updatedBalance));

    Assert.assertNotNull(String.format("Failed to update JournalBalance, given {%s, %s}",
        journalBalanceCode, updatedBalance), updatedJournalBalance);


  }
}
