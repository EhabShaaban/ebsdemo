package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewPostingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestViewPostingDetailsStep extends SpringBootRunner
  implements En, InitializingBean {

  public static final String SCENARIO_NAME = "View Payment PostingDetails";
  private DObPaymentRequestViewPostingDetailsTestUtils utils;
  private static boolean hasBeenExecuted = false;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestViewPostingDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObPaymentRequestViewPostingDetailsStep() {
    Given("^the following PostingDetails for PaymentRequests exist:$",
      (DataTable postingDetailsTable) -> {
        utils.assertThatTheFollowingPRActivationDetailsExist(postingDetailsTable);
      });

    When(
      "^\"([^\"]*)\" requests to view PostingDetails section of PaymentRequest with code \"([^\"]*)\"$",
      (String userName, String paymentRequestCode) -> {
        Cookie cookie = userActionsTestUtils.getUserCookie(userName);
        Response response =
          utils.sendGETRequest(cookie, utils.getViewPostingDetailsUrl(paymentRequestCode));
        userActionsTestUtils.setUserResponse(userName, response);
      });

    Then(
      "^the following values of PostingDetails section for PaymentRequest with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
      (String paymentRequestCode, String userName, DataTable postingDetailsTable) -> {
        Response response = userActionsTestUtils.getUserResponse(userName);
        utils.assertCorrectValuesAreDisplayedInPostingDetails(response, postingDetailsTable);
      });
  }

  @Before
  public void beforeViewPostingDetails(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
          DObPaymentRequestViewPostingDetailsTestUtils.getPaymentRequestDbScripts());
        hasBeenExecuted = true;
      }
      databaseConnector
        .executeSQLScript(DObPaymentRequestViewPostingDetailsTestUtils.clearPaymentRequests());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }

}
