package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestSectionNames;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;

public class DObPaymentRequestDeleteTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatPaymentRequestNotExist(String paymentrequestCode) {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        (DObPaymentRequestGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getPaymentRequestByCode", paymentrequestCode);
    Assert.assertNull("Payment Request Exist with code " + paymentrequestCode,
        paymentRequestGeneralModel);
  }

  public Response deletePaymentRequest(Cookie loginCookie, String PaymentRequestCode) {
    String deleteURL = IDObPaymentRequestRestURLs.DELETE + PaymentRequestCode;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  public Map<String, String> getSectionsLockUrlsMap() {
    Map<String, String> sectionsLockUrls = new HashMap<>();
    sectionsLockUrls.put(IDObPaymentRequestSectionNames.ACCOUNTING_DETAILS_SECTION,
        IDObPaymentRequestRestURLs.LOCK_ACCOUNTING_DETAILS_SECTION);
    sectionsLockUrls.put(IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION,
        IDObPaymentRequestRestURLs.LOCK_PAYMENT_DETAILS_SECTION);
    return sectionsLockUrls;
  }

  public StringBuilder getLockUrl(String paymentRequestCode, String sectionUrl) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND + "/");
    lockUrl.append(paymentRequestCode);
    lockUrl.append(sectionUrl);
    return lockUrl;
  }

  public String getDeletePaymentRequestDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    return str.toString();
  }
}
