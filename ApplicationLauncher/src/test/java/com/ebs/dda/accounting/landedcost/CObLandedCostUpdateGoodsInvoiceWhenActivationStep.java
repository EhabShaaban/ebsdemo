package com.ebs.dda.accounting.landedcost;

import java.sql.SQLException;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.CObLandedCostUpdateGoodsInvoiceWhenActivationTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class CObLandedCostUpdateGoodsInvoiceWhenActivationStep extends SpringBootRunner
    implements En {

  private static final String SCENARIO_NAME =
      "Update LandedCost GoodsInvoice and ExchangeRate fields";
  private static boolean hasBeenExecuted = false;
  private DObVendorInvoiceActivateTestUtils dObInvoicePostTestUtils;
  private CObLandedCostUpdateGoodsInvoiceWhenActivationTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new CObLandedCostUpdateGoodsInvoiceWhenActivationTestUtils(properties,
        entityManagerDatabaseConnector);
    dObInvoicePostTestUtils = new DObVendorInvoiceActivateTestUtils(properties);
    dObInvoicePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public CObLandedCostUpdateGoodsInvoiceWhenActivationStep() {
    When("^\"([^\"]*)\" Activates VendorInvoice of type \"([^\"]*)\" with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
            (String username, String invoiceType, String invoiceCode, String date, DataTable invoiceDT) -> {
          dObInvoicePostTestUtils.freeze(date);
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response response =
              dObInvoicePostTestUtils.activateVendorInvoice(userCookie, invoiceCode,invoiceType, date, invoiceDT);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the GoodsInvoice and ExchangeRate of LandedCost with code \"([^\"]*)\" are updated as follows:$",
        (String landedCostCode, DataTable detailsDT) -> {
          utils.assertThatGoodsInvoiceAndExangeRateAreUpdated(landedCostCode, detailsDT);
        });

  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains(SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(dObInvoicePostTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDBScripts());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains(SCENARIO_NAME)) {
      dObInvoicePostTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
