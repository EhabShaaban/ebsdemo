package com.ebs.dda.accounting.accountingnote.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/accounting/accounting-notes/AN_ViewAll.feature",
      "classpath:features/accounting/accounting-notes/AN_Create_HP.feature",
      "classpath:features/accounting/accounting-notes/AN_DocumentOwner_Dropdown.feature",
      "classpath:features/accounting/accounting-notes/AN_ActiveNotes_Dropdown.feature",
      "classpath:features/accounting/accounting-notes/AN_Type_Dropdown.feature",
      "classpath:features/accounting/accounting-notes/AN_SRO_Dropdown.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.accountingnote"},
    strict = true,
    tags = "not @Future")
public class DObAccountingNoteCucumberRunner {}
