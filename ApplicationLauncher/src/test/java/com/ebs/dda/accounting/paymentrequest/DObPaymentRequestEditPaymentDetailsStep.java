package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestSectionNames;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestEditPaymentDetailsTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPostCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestSavePaymentDetailsTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewPaymentDetailsTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.HashMap;
import java.util.Map;

public class DObPaymentRequestEditPaymentDetailsStep extends SpringBootRunner
				implements En, InitializingBean {

	public static final String EDIT_SCENARIO_NAME = "Request to edit Payment details";
	public static final String CANCEL_SCENARIO_NAME = "Request to Cancel saving Payment details";
	private DObPaymentRequestSavePaymentDetailsTestUtils savePaymentDetailsTestUtils;
	private DObPaymentRequestEditPaymentDetailsTestUtils utils;
	private DObPaymentRequestPostCommonTestUtils postTestUtils;
	private Map<String, String> currentLocks;
	private static boolean hasBeenExecutedOnce = false;

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		utils = new DObPaymentRequestEditPaymentDetailsTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		savePaymentDetailsTestUtils = new DObPaymentRequestSavePaymentDetailsTestUtils(
						getProperties());
		savePaymentDetailsTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		postTestUtils = new DObPaymentRequestPostCommonTestUtils(getProperties());
		currentLocks = new HashMap<>();
	}

	@After
	public void clearLocks(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), "Request to edit Payment details") || contains(
						scenario.getName(), "Request to Cancel saving Payment details")) {
			for (String userName : currentLocks.keySet()) {
				String prCode = currentLocks.get(userName);
				String unLockURL = utils.getUnlockPaymentDetailsUrl(prCode);
				userActionsTestUtils.unlockSection(userName, unLockURL);
			}
		}
	}

	public DObPaymentRequestEditPaymentDetailsStep() {

		When("^\"([^\"]*)\" requests to edit PaymentDetails section of PaymentRequest with code \"([^\"]*)\"$",
						(String userName, String prCode) -> {
							String lock_url = utils.getLockPaymentDetailsUrl(prCode);
							Response response = userActionsTestUtils.lockSection(userName, lock_url,
											prCode);
							userActionsTestUtils.setUserResponse(userName, response);
							currentLocks.put(userName, prCode);
						});

		Then("^PaymentDetails section of PaymentRequest with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
						(String prCode, String userName) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertResponseSuccessStatus(response);
							utils.assertThatResourceIsLockedByUser(userName, prCode,
											IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION);
						});

		Given("^\"([^\"]*)\" first opened the PaymentRequest with code \"([^\"]*)\" in the edit mode successfully$",
						(String userName, String prCode) -> {
							String lock_url = utils.getLockPaymentDetailsUrl(prCode);
							Response response = userActionsTestUtils.lockSection(userName, lock_url,
											prCode);
							utils.assertResponseSuccessStatus(response);
							userActionsTestUtils.setUserResponse(userName, response);
							currentLocks.put(userName, prCode);
						});

		Given("^PaymentDetails section of PaymentRequest with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
						(String prCode, String userName, String dateTime) -> {
							utils.freeze(dateTime);
							String lockUrl = utils.getLockPaymentDetailsUrl(prCode);
							Response response = userActionsTestUtils.lockSection(userName, lockUrl,
											prCode);
							userActionsTestUtils.setUserResponse(userName, response);
							currentLocks.put(userName, prCode);
						});

		When("^\"([^\"]*)\" cancels saving PaymentDetails section of PaymentRequest with code \"([^\"]*)\" at \"([^\"]*)\"$",
						(String userName, String prCode, String dateTime) -> {
							utils.freeze(dateTime);
							String unlockUrl = utils.getUnlockPaymentDetailsUrl(prCode);
							Response response = userActionsTestUtils.unlockSection(userName,
											unlockUrl);
							userActionsTestUtils.setUserResponse(userName, response);
							currentLocks.put(userName, prCode);
						});

		Then("^the lock by \"([^\"]*)\" on PaymentDetails section of PaymentRequest with code \"([^\"]*)\" is released$",
						(String userName, String prCode) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertResponseSuccessStatus(response);
							utils.assertThatLockIsReleased(userName, prCode,
											IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION);
						});

		When("^\"([^\"]*)\" cancels saving PaymentDetails section of PaymentRequest with code \"([^\"]*)\"$",
						(String userName, String prCode) -> {
							String unLockURL = utils.getUnlockPaymentDetailsUrl(prCode);
							Response response = userActionsTestUtils.unlockSection(userName,
											unLockURL);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		And("^\"([^\"]*)\" requests to edit PaymentDetails section of PaymentRequest with code \"([^\"]*)\" at \"([^\"]*)\"$",
						(String userName, String paymentRequestCode, String dateTime) -> {
							utils.freeze(dateTime);
							String lockUrl = utils.getLockPaymentDetailsUrl(paymentRequestCode);
							Response response = userActionsTestUtils.lockSection(userName, lockUrl,
											paymentRequestCode);
							userActionsTestUtils.setUserResponse(userName, response);
							currentLocks.put(userName, paymentRequestCode);
						});

		And("^PaymentDetails section of PaymentRequest with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
						(String paymentRequestCode, String userName) -> {
							utils.assertThatLockIsReleased(userName, paymentRequestCode,
											IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION);
						});
	}

	@Before
	public void beforeRequestEditPaymentDetails(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), EDIT_SCENARIO_NAME)
						|| contains(scenario.getName(), CANCEL_SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecutedOnce) {
				databaseConnector.executeSQLScript(DObPaymentRequestViewPaymentDetailsTestUtils
						.getPaymentRequestDbScriptsOneTimeExecutionForEditPaymentDetails());
				hasBeenExecutedOnce = true;
			}
			databaseConnector.executeSQLScript(DObPaymentRequestViewPaymentDetailsTestUtils.clearPaymentRequests());
			databaseConnector.executeSQLScript(DObVendorInvoiceTestUtils.clearVendorInvoices());

		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), EDIT_SCENARIO_NAME)
						|| contains(scenario.getName(), CANCEL_SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
