package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.accounting.IObInvoiceSummaryTestUtils;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceSummaryViewTestUtils;
import java.sql.SQLException;
import java.util.Map;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceTaxesViewTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObVendorInvoiceSummaryViewStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObVendorInvoiceSummaryViewTestUtils utils;
  private IObInvoiceSummaryTestUtils summaryUtils;

  public DObVendorInvoiceSummaryViewStep() {
    When("^\"([^\"]*)\" requests to view Summary section of VendorInvoice with code \"([^\"]*)\"$", (String userName, String invoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      StringBuilder readUrl = new StringBuilder();
      readUrl.append(IDObVendorInvoiceRestUrls.GET_URL);
      readUrl.append(invoiceCode);
      readUrl.append(IDObVendorInvoiceRestUrls.SUMMARY);
      Response response = utils.sendGETRequest(cookie, readUrl.toString());
      userActionsTestUtils.setUserResponse(userName, response);
    });

    Then("^the following summary values for VendorInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$", (String invoiceCode, String userName, DataTable invoiceSummaryDataTable) -> {
      Response response = userActionsTestUtils.getUserResponse(userName);
      utils.assertCorrectValuesAreDisplayedInSummarySection(response, invoiceSummaryDataTable);
    });
    Then("^Summary section of VendorInvoice with Code \"([^\"]*)\" is updated as follows:$", (String invoiceCode, DataTable invoiceSummaryDataTable) -> {
      summaryUtils.assertThatTheFollowingInvoiceSummaryDetailsExist(invoiceCode, "VendorInvoice",invoiceSummaryDataTable);

    });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new IObVendorInvoiceSummaryViewTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    summaryUtils = new IObInvoiceSummaryTestUtils(properties);
    summaryUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "View VendorInvoiceSummary")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View VendorInvoiceSummary")) {
      databaseConnector.closeConnection();
    }
  }
}
