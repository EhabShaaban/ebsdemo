package com.ebs.dda.accounting.journalentry;

public interface IDObJournalEntryRestUrls {

  String GET_URL = "/services/accounting/journalentry";
  String ITEMS_GET_URL = "/services/accounting/journalentry/items";
}
