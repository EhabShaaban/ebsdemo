package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.CommonKeys;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObLandedCostViewAllTestUtils extends CommonTestUtils {

  public DObLandedCostViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObControlPointSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");

    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/CObLandedCostType.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails_ViewAll.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData_ViewAll.sql");

    return str.toString();
  }

  public void assertThatLandedCostsAreExist(DataTable landedCostsDT) {
    List<Map<String, String>> landedCosts = landedCostsDT.asMaps(String.class, String.class);

    for (Map<String, String> landedCost : landedCosts) {
      Object[] params = constructLandedCost(landedCost);
      DObLandedCostGeneralModel landedCostGeneralModel =
          (DObLandedCostGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getViewAllLandedCosts", params);
      Assert.assertNotNull(
          "Landed Cost doesn't exist: " + Arrays.deepToString(params), landedCostGeneralModel);
    }
  }

  private Object[] constructLandedCost(Map<String, String> landedCost) {
    Object arr[] =
        new Object[] {
          landedCost.get(IFeatureFileCommonKeys.CODE),
          landedCost.get(IFeatureFileCommonKeys.TYPE),
          '%' + landedCost.get(IFeatureFileCommonKeys.STATE) + '%',
          landedCost.get(IFeatureFileCommonKeys.CREATION_DATE),
          landedCost.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
          landedCost.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
        };
    return arr;
  }

  public void assertThatNumberOfSavedLandedCostsEqual(Integer numberOfLandedCosts) {
    Long savedLandedCostsLength =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("countLandedCosts");
    Assert.assertEquals(numberOfLandedCosts.intValue(), savedLandedCostsLength.intValue());
  }

  public void assertThatLandedCostMatches(Response response, DataTable landedCostsDT)
      throws Exception {
    List<Map<String, String>> expectedLandedCost = landedCostsDT.asMaps(String.class, String.class);

    assertResponseSuccessStatus(response);
    assertCorrectNumberOfRows(response, expectedLandedCost.size());

    List<HashMap<String, Object>> listOfLandedCosts = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedLandedCost.size(); i++) {
      assertThatLandedCostsMatchesTheExpected(expectedLandedCost.get(i), listOfLandedCosts.get(i));
    }
  }

  private void assertThatLandedCostsMatchesTheExpected(
      Map<String, String> expectedLandedCostMap, HashMap<String, Object> actualListOfLandedCostsMap)
      throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedLandedCostMap, actualListOfLandedCostsMap);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObLandedCostGeneralModel.CODE);

    assertThatStatesArrayContainsValue(
        getStatesArray(
            String.valueOf(
                actualListOfLandedCostsMap.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME))),
        expectedLandedCostMap.get(IFeatureFileCommonKeys.STATE));

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObLandedCostGeneralModel.CREATION_DATE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TYPE,
        IDObLandedCostGeneralModel.TYPE_CODE,
        IDObLandedCostGeneralModel.TYPE_NAME);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.PURCHASE_ORDER, IDObLandedCostGeneralModel.PURCHASE_ORDER_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObLandedCostGeneralModel.PURCHASE_UNIT_CODE,
        IDObLandedCostGeneralModel.PURCHASING_UNIT_NAME);
  }

  public void assertThatNumberOfRecordDisplayIs(Response response, Integer numberOfRecords) {
    List<Object> data = response.body().jsonPath().get(CommonKeys.DATA);
    Assert.assertEquals(numberOfRecords.intValue(), data.size());
  }
}
