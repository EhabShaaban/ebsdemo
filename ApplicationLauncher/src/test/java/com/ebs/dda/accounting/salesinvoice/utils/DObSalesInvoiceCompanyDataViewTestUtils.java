package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObSalesInvoiceCompanyDataViewTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceCompanyDataViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatTheFollowingCompanyExistsForSalesInvoice(DataTable dataTable) {
    Map<String, String> companySalesInvoiceMap =
        dataTable.asMaps(String.class, String.class).get(0);
    Object[] queryParams = constructCompanySalesInvoiceQueryParams(companySalesInvoiceMap);
    DObSalesInvoiceGeneralModel salesInvoiceCompanyTaxesDetailsGeneralModel =
        (DObSalesInvoiceGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getCompanySalesInvoice", queryParams);
    Assert.assertNotNull(
        "Company doesn't exist in SalesInvoice "
            + companySalesInvoiceMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
        salesInvoiceCompanyTaxesDetailsGeneralModel);
  }

  private Object[] constructCompanySalesInvoiceQueryParams(
      Map<String, String> companySalesInvoiceMap) {
    return new Object[] {companySalesInvoiceMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
        companySalesInvoiceMap.get(ISalesFeatureFileCommonKeys.COMPANY)};
  }

  public void assertCorrectValuesAreDisplayedInCompanyData(Response response,
      DataTable taxValuesDataTable) {
    List<Map<String, String>> companyDataExpected =
        taxValuesDataTable.asMaps(String.class, String.class);
    HashMap<String, Object> companyDataActual = getMapFromResponse(response);

    for (int i = 0; i < companyDataExpected.size(); i++) {
      assertActualAndExpectedDataFromResponse(companyDataExpected.get(i), companyDataActual);
    }
  }

  private void assertActualAndExpectedDataFromResponse(Map<String, String> expectedCompanyTaxDetail,
      HashMap<String, Object> actualCompanyTaxDetail) {
    MapAssertion mapAssertion = new MapAssertion(expectedCompanyTaxDetail, actualCompanyTaxDetail);
    mapAssertion.assertLocalizedValue(IFeatureFileCommonKeys.COMPANY,
        IDObSalesInvoiceGeneralModel.COMPANY_NAME);
  }
}
