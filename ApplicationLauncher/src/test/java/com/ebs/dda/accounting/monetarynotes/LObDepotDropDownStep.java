package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.LObDepotDropDownTestUtil;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class LObDepotDropDownStep extends SpringBootRunner implements En {

  private LObDepotDropDownTestUtil utils;

  public LObDepotDropDownStep() {

    Given("^the following Depots exist:$", (DataTable depotDataTable) -> {
      utils.assertThatLObDepotsAreExist(depotDataTable);
    });

    Given("^the total number of existing Depots is (\\d+)$", (Integer depotsNumber) -> {
      utils.assertDepotsCountMatchesExpected(depotsNumber);

    });

    When("^\"([^\"]*)\" requests to read all Depots$", (String userName) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response response = utils.sendGETRequest(cookie, IDObNotesReceivableRestURLs.DEPOT_QUERY);
      userActionsTestUtils.setUserResponse(userName, response);
    });

    Then("^the following Depots values will be presented to \"([^\"]*)\":$", (String userName, DataTable depotDataTable) -> {
      Response response = userActionsTestUtils.getUserResponse(userName);
      utils.assertThatReadAllDepotsIsCorrect(response, depotDataTable);
    });

    Then("^total number of Depots returned to \"([^\"]*)\" is equal to (\\d+)$", (String userName, Integer totalNumberOfRecords) -> {
      Response response = userActionsTestUtils.getUserResponse(userName);
      utils.assertThatTotalNumberOfRecordsIsCorrect(response, totalNumberOfRecords);
    });

  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new LObDepotDropDownTestUtil(properties, entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of Depots dropdown")) {
      databaseConnector.executeSQLScript(utils.getClearLObDepotsDbScripts());
    }
  }
}
