package com.ebs.dda.accounting.landedcost.utils;

import java.util.List;
import java.util.Map;

import org.junit.Assert;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsGeneralModel;

import cucumber.api.DataTable;

public class DObLandedCostFactorItemsDeleteTestUtils extends DObLandedCostTestUtils {
	public DObLandedCostFactorItemsDeleteTestUtils(Map<String, Object> properties,
					EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
		super(properties);
		setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	public void assertThatLandedCostFactorItemsExistsWithIds(
					DataTable landedCostFactorItemsDataTable) {
		List<Map<String, String>> costFactorItemsMaps = landedCostFactorItemsDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> costFactorItemMap : costFactorItemsMaps) {
			assertThatLandedCostHasFactorItemsWithIds(costFactorItemMap);
		}
	}

	private void assertThatLandedCostHasFactorItemsWithIds(Map<String, String> costFactorItemMap) {
		IObLandedCostFactorItemsGeneralModel landedCostFactorItem = (IObLandedCostFactorItemsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getLandedCostFactorItemsWithIds",
										constructLandedCostFactorItemQueryParams(
														costFactorItemMap));
		Assert.assertNotNull("Landed cost Factor Item with ids doesn't exist "
						+ costFactorItemMap.toString(), landedCostFactorItem);
	}

	private Object[] constructLandedCostFactorItemQueryParams(Map<String, String> generalDataMap) {
		return new Object[] {
				generalDataMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_VALUE),
				generalDataMap.get(IAccountingFeatureFileCommonKeys.DIFFERENCE),
				generalDataMap.get(IAccountingFeatureFileCommonKeys.LC_CODE),
				Long.parseLong(generalDataMap
								.get(IAccountingFeatureFileCommonKeys.LC_ITEM_ID)),
				generalDataMap.get(IAccountingFeatureFileCommonKeys.ITEM),
				generalDataMap.get(IAccountingFeatureFileCommonKeys.ESTIMATED_VALUE)
						 };
	}

	public String getDeleteCostFactorItemRestUrl(String costFactorItemId, String landedCostCode) {
		StringBuilder deleteUrl = new StringBuilder();
		deleteUrl.append(IDObLandedCostRestUrls.COMMAND_URL);
		deleteUrl.append("/");
		deleteUrl.append(landedCostCode);
		deleteUrl.append(IDObLandedCostRestUrls.COST_FACTOR_ITEMS);
		deleteUrl.append("/");
		deleteUrl.append(Long.parseLong(costFactorItemId));
		return deleteUrl.toString();
	}

	public void assertThatLandedCostFactorItemIsDeleted(String costFactorItemId,
					String landedCostCode) {
		IObLandedCostFactorItemsGeneralModel landedCostFactorItem = (IObLandedCostFactorItemsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getLandedCostFactorItemsByCodeAndId",
										landedCostCode, Long.parseLong(costFactorItemId));
		Assert.assertNull("Landed cost Factor Item with id " + costFactorItemId,
						landedCostFactorItem);
	}

	public void assertThatLandedCostFactorItemsOnlyRemaining(String landedCostCode,
					DataTable landedCostFactorItemsDataTable) {
		List<Map<String, String>> costFactorItemsMaps = landedCostFactorItemsDataTable
						.asMaps(String.class, String.class);
		assertThatLandedCostHasOnlyFactorItems(landedCostCode, costFactorItemsMaps);
	}

	private void assertThatLandedCostHasOnlyFactorItems(String landedCostCode,
					List<Map<String, String>> costFactorItemsMaps) {
		List<IObLandedCostFactorItemsGeneralModel> landedCostFactorItems = (List<IObLandedCostFactorItemsGeneralModel>) entityManagerDatabaseConnector
						.executeNativeNamedQuery("getLandedCostFactorItemsByCode", landedCostCode);
		Assert.assertEquals(costFactorItemsMaps.size(), landedCostFactorItems.size());
	}
}
