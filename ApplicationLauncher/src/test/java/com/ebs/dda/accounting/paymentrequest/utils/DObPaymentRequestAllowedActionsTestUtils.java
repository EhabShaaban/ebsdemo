package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObPaymentRequestAllowedActionsTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestAllowedActionsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    return str.toString();
  }

  public void assertThatTheFollowingPaymentRequestExist(DataTable paymentRequestDataTable) {
    List<Map<String, String>> paymentRequestListMap =
        paymentRequestDataTable.asMaps(String.class, String.class);
    for (Map<String, String> paymentRequestMap : paymentRequestListMap) {
      DObPaymentRequest paymentRequest =
          (DObPaymentRequest)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPaymentRequest", constructPaymentRequestQueryParams(paymentRequestMap));
      Assert.assertNotNull(
          "Payment Request with code "
              + paymentRequestMap.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exists",
          paymentRequest);
    }
  }

  private Object[] constructPaymentRequestQueryParams(Map<String, String> paymentRequestMap) {
    return new Object[] {
      paymentRequestMap.get(IFeatureFileCommonKeys.CODE),
      paymentRequestMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + paymentRequestMap.get(IFeatureFileCommonKeys.STATE) + "%",
      paymentRequestMap.get(IFeatureFileCommonKeys.TYPE)
    };
  }
}
