package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.IObMonetaryNotesSaveNoteDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObMonetaryNotesSaveNoteDetailsStep extends SpringBootRunner implements En {
  private IObMonetaryNotesSaveNoteDetailsTestUtils utils;

  public IObMonetaryNotesSaveNoteDetailsStep() {
    When(
        "^\"([^\"]*)\" saves NotesDetails section of NotesReceivable with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String code, String dateTime, DataTable noteDetailsDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.saveNoteDetailsData(code, noteDetailsDataTable, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^NotesDetails section of NotesReceivable with code \"([^\"]*)\" is updated as follows:$",
        (String code, DataTable noteDetailsDataTable) -> {
          utils.assertThatNotesDetailsIsUpdatedAsFollows(code, noteDetailsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObMonetaryNotesSaveNoteDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save NotesReceivable NotesDetails section,")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }

  @After
  public void afterSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save NotesReceivable NotesDetails section,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObNotesReceivableRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
