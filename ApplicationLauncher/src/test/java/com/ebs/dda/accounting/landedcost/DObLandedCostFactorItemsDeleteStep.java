package com.ebs.dda.accounting.landedcost;

import java.sql.SQLException;
import java.util.Map;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostFactorItemsDeleteTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObLandedCostFactorItemsDeleteStep extends SpringBootRunner implements En {
	private static final String SCENARIO_NAME = "Delete CostFactorItem from LandedCost";
	private static boolean hasBeenExecuted = false;
	private DObLandedCostFactorItemsDeleteTestUtils utils;

	public DObLandedCostFactorItemsDeleteStep() {

		Given("^the following LandedCosts have the following CostFactorItems with ids:$",
						(DataTable landedCostFactorItemsDataTable) -> {
							utils.assertThatLandedCostFactorItemsExistsWithIds(
											landedCostFactorItemsDataTable);

						});

		When("^\"([^\"]*)\" requests to delete CostFactorItem with id \"([^\"]*)\" in LandedCost with code \"([^\"]*)\"$",
						(String userName, String costFactorItemId, String landedCostCode) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							String restUrl = utils.getDeleteCostFactorItemRestUrl(costFactorItemId,
											landedCostCode);
							Response response = utils.sendDeleteRequest(userCookie, restUrl);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^CostFactorItem with id \"([^\"]*)\" from LandedCost with code \"([^\"]*)\" is deleted$",
						(String costFactorItemId, String landedCostCode) -> {
							utils.assertThatLandedCostFactorItemIsDeleted(costFactorItemId,
											landedCostCode);
						});

		Then("^Only the following CostFactorItems remaining in LandedCost with code \"([^\"]*)\":$",
						(String landedCostCode, DataTable landedCostFactorItemsDataTable) -> {
							utils.assertThatLandedCostFactorItemsExistsWithIds(
											landedCostFactorItemsDataTable);
							utils.assertThatLandedCostFactorItemsOnlyRemaining(landedCostCode,
											landedCostFactorItemsDataTable);
						});

		Given("^\"([^\"]*)\" first deleted CostFactorItem with id \"([^\"]*)\" of LandedCost with code \"([^\"]*)\" successfully$",
						(String userName, String costFactorItemId, String landedCostCode) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							String restUrl = utils.getDeleteCostFactorItemRestUrl(costFactorItemId,
											landedCostCode);
							Response response = utils.sendDeleteRequest(userCookie, restUrl);
							userActionsTestUtils.setUserResponse(userName, response);
						});
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		utils = new DObLandedCostFactorItemsDeleteTestUtils(properties,
						entityManagerDatabaseConnector);
	}

	@Before
	public void setup(Scenario scenario) throws Exception {
		if (scenario.getName().contains(SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
				hasBeenExecuted = true;
			}
			databaseConnector.executeSQLScript(utils.getDbScripts());
		}
	}

	@After
	public void afterEachScenario(Scenario scenario) throws SQLException {
		if (scenario.getName().contains(SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}

}
