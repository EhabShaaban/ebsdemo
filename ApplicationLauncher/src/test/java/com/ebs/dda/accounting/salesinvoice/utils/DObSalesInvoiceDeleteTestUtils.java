package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

public class DObSalesInvoiceDeleteTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatSalesInvoiceNotExist(String salesInvoiceCode) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceByCode", salesInvoiceCode);
    Assert.assertNull(salesInvoiceGeneralModel);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObControlPointSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");

    return str.toString();
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    return str.toString();
  }

  public Map<String, String> getSectionsLockUrlsMap() {
    Map<String, String> sectionsLockUrls = new HashMap<>();
    sectionsLockUrls.put(
        IDObSalesInvoiceSectionNames.COMPANY_DATA_SECTION,
        IDObSalesInvoiceRestUrls.LOCK_SALES_INVOICE_COMPANY_DATA_SECTION);
    sectionsLockUrls.put(
        IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION,
        IDObSalesInvoiceRestUrls.LOCK_BUSINESS_PARTNER);
    sectionsLockUrls.put(
        IDObSalesInvoiceSectionNames.ITEM_SECTION,
        IDObSalesInvoiceRestUrls.LOCK_SALES_INVOICE_ITEMS_SECTION);
    return sectionsLockUrls;
  }

  public StringBuilder getLockUrl(String salesInvoiceCode, String sectionUrl) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObSalesInvoiceRestUrls.COMMAND_URL);
    lockUrl.append(salesInvoiceCode);
    lockUrl.append(sectionUrl);
    return lockUrl;
  }
}
