package com.ebs.dda.accounting.vendorinvoice.utiles;

import java.util.List;
import java.util.Map;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorItemsGeneralModel;

import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObVendorInvoiceItemsDropDownTestUtils extends DObVendorInvoiceCommonTestUtils {

	public DObVendorInvoiceItemsDropDownTestUtils(Map<String, Object> properties) {
		super(properties);
	}
	public String getDbScriptsOneTimeExecution() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/clearDataSqlScript.sql");
		str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
		str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
		str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
		str.append("," + "db-scripts/CObBankSqlScript.sql");
		str.append("," + "db-scripts/CObCurrencySqlScript.sql");
		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
		str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
		str.append(",").append("db-scripts/master-data/item/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
		str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
		str.append("," + "db-scripts/CObMeasureScript.sql");
		str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
		str.append("," + "db-scripts/master-data/item/CObItem.sql");
		str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");

		return str.toString();
	}

    public Response readAllVendorItems(Cookie cookie, String vendorInvoiceCode, String vendorCode, String businessUnitCode) {
		String restURL =  IDObVendorInvoiceRestUrls.READ_ALL_VENDOR_ITEMS +  vendorInvoiceCode + '/' + vendorCode + '/' + businessUnitCode;
		return sendGETRequest(cookie, restURL);
    }


	public void assertThatVendorItemsResponseIs(Response response, DataTable vendorItemsDataTable) throws Exception {
		List<Map<String, Object>> actualMapList =
				response.body().jsonPath().get(CommonKeys.DATA);
		for (int index = 0 ; index < actualMapList.size(); index++) {
		Map<String, String> expectedMap = vendorItemsDataTable.asMaps(String.class, String.class).get(index);
		MapAssertion mapAssertion = new MapAssertion(expectedMap, actualMapList.get(index));
		mapAssertion.assertCodeAndLocalizedNameValue(IFeatureFileCommonKeys.ITEM, IMObVendorItemsGeneralModel.ITEM_CODE, IMObVendorItemsGeneralModel.ITEM_NAME);
	}
	}

	public Response readAllVendorItemUnits(Cookie cookie, String itemCode, String vendorInvoiceCode,
					String vendorCode, String businessUnitCode) {
		String restURL = IDObVendorInvoiceRestUrls.READ_ALL_VENDOR_ITEM_UNITS + vendorInvoiceCode + '/'
						+ itemCode + '/' + vendorCode + '/' + businessUnitCode;
		return sendGETRequest(cookie, restURL);
	}

	public void assertThatVendorItemUnitsResponseIs(Response response,
					DataTable vendorItemUnitsDataTable) {
		List<Map<String, Object>> actualMapList = response.body().jsonPath().get(CommonKeys.DATA);
		for (int index = 0; index < actualMapList.size(); index++) {
			Map<String, String> expectedMap = vendorItemUnitsDataTable
							.asMaps(String.class, String.class).get(index);
			MapAssertion mapAssertion = new MapAssertion(expectedMap, actualMapList.get(index));
			mapAssertion.assertLocalizedValue(IFeatureFileCommonKeys.ITEM_ORDER_UNIT,
							IMObVendorItemsGeneralModel.IVR_MEASURE_SYMBOL);
			mapAssertion.assertBigDecimalNumberValue(IMasterDataFeatureFileCommonKeys.CONVERSION_FACTOR, IMObVendorItemsGeneralModel.CONVERSION_FACTOR);
			mapAssertion.assertLocalizedValue(IFeatureFileCommonKeys.BASE_UNIT,
					IMObVendorItemsGeneralModel.BASE_UNIT_SYMBOL);
		}
	}
}
