package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import cucumber.api.DataTable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class IObSalesInvoiceItemsTestUtils extends DObSalesInvoiceTestUtils {

  public IObSalesInvoiceItemsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
      str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
      str.append("," + "db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
      str.append("," + "db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
      str.append("," + "db-scripts/sales/IObSalesInvoiceItems.sql");
      str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
      str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
      str.append("," + "db-scripts/notes-receivables/DObNotesReceivables_ViewAll.sql");
      str.append("," + "db-scripts/notes-receivables/DObNotesReceivables.sql");
      str.append(
              ","
                      + "db-scripts/accounting/collection/CObCollectionType.sql");
      str.append("," + "db-scripts/accounting/collection/DObCollection_ViewAll.sql");
      str.append(
              "," + "db-scripts/accounting/collection/IObCollectionDetails_ViewAll.sql");
      str.append("," + "db-scripts/accounting/collection/DObCollection.sql");
      str.append("," + "db-scripts/accounting/collection/IObCollectionCompanyDetails.sql");
      str.append("," + "db-scripts/accounting/collection/IObCollectionDetails.sql");

      return str.toString();
  }

  public void assertThatSalesInvoiceHasItems(
      String salesInvoiceCode, DataTable salesInvoiceItemsTable) {
    List<Map<String, String>> salesInvoiceItems =
        salesInvoiceItemsTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceItem : salesInvoiceItems) {
      assertThatSalesInvoiceHasItem(salesInvoiceCode, salesInvoiceItem);
    }
  }

  public void assertThatSalesInvoiceHasItem(
      String salesInvoiceCode, Map<String, String> salesInvoiceItem) {
    IObSalesInvoiceItemsGeneralModel salesInvoiceItemsGeneralModel =
        (IObSalesInvoiceItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesItems",
                constructSalesInvoiceItems(salesInvoiceCode, salesInvoiceItem));
    assertNotNull(
        "Sales Invoice Item does not exist " + salesInvoiceItem, salesInvoiceItemsGeneralModel);
  }

  public Object[] constructSalesInvoiceItems(
      String invoiceCode, Map<String, String> salesInvoiceItems) {
    return new Object[] {
      invoiceCode,
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.ITEM),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.ORDER_UNIT_CODE_NAME),
      new BigDecimal(salesInvoiceItems.get(ISalesFeatureFileCommonKeys.QUANTITY)),
      new BigDecimal(
          salesInvoiceItems.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT)),
      new BigDecimal(
          salesInvoiceItems.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)),
      new BigDecimal(salesInvoiceItems.get(ISalesFeatureFileCommonKeys.TOTAL_AMOUNT))
    };
  }

  public Object[] constructItemQueryParams(Map<String, String> item) {
    return new Object[] {
      item.get(ISalesFeatureFileCommonKeys.ITEM), item.get(ISalesFeatureFileCommonKeys.UOM)
    };
  }
}
