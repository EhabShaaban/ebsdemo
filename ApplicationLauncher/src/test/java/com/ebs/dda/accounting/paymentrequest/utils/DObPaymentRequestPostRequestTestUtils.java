package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class DObPaymentRequestPostRequestTestUtils extends DObPaymentRequestPostCommonTestUtils {

  public DObPaymentRequestPostRequestTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public void assertThatResponseHasLatestExchangeRate(Response response, String exchangeRateCode) {
    HashMap<String, Object> responseExchangeRate = response.body().jsonPath().get("data");
    String responseExchangeRateCode = responseExchangeRate.get("userCode").toString();
    assertEquals(responseExchangeRateCode, exchangeRateCode);
  }

  public void assertPurchaseOrderDownPayment(DataTable PODownPayment) {
    List<Map<String, String>> PODownPaymentListMap =
        PODownPayment.asMaps(String.class, String.class);
    class purchaseOrderDownPaymentData {
      private BigDecimal amount;
      private BigDecimal remaining;
      private BigDecimal totalPaid;

      public BigDecimal getTotalPaid() {
        return totalPaid;
      }

      public BigDecimal getAmount() {
        return amount;
      }

      public BigDecimal getRemaining() {
        return remaining;
      }
    }

    for (Map<String, String> purchaseOrderMap : PODownPaymentListMap) {
      purchaseOrderDownPaymentData purchaseOrderDownPayment =
          (purchaseOrderDownPaymentData)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrderDownPaymentDetails",
                  purchaseOrderMap.get(IFeatureFileCommonKeys.CODE));

      if (new BigDecimal(purchaseOrderMap.get(IAccountingFeatureFileCommonKeys.TOTAL_PAID))
        .compareTo(BigDecimal.ZERO) == 0) {
        Assert.assertNull(purchaseOrderDownPayment);
      } else {
        Assert.assertNotNull(purchaseOrderDownPayment);
        Assert.assertEquals(
            purchaseOrderMap.get(IAccountingFeatureFileCommonKeys.TOTAL_PAID),
            purchaseOrderDownPayment.getTotalPaid());
        Assert.assertEquals(
            new BigDecimal(purchaseOrderMap.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
            purchaseOrderDownPayment.getAmount());

        Assert.assertEquals(
            new BigDecimal(purchaseOrderMap.get(IAccountingFeatureFileCommonKeys.REMAINING)),
            purchaseOrderDownPayment.getRemaining());
      }
    }
  }

  public void assertThatNoLandedCostsExistForPurchaseOrders(
      String landedCostState, DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrdersListMap =
        purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersListMap) {
      String purchaseOrderCode = purchaseOrderMap.get(IFeatureFileCommonKeys.PURCHASE_ORDER);
      DObLandedCostGeneralModel landedCost =
          (DObLandedCostGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrderLandedCost", '%' + landedCostState + '%', purchaseOrderCode);
      assertNull(landedCost, "a LandedCost exits for PurchaseOrder with code " + purchaseOrderCode);
    }
  }

  public void assertThatVendorInvoiceRemainingIsUpdatedWithValue(
      String vendorInvoiceCode, String vendorInvoiceRemaining) {
    BigDecimal remaining =
            (BigDecimal) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorInvoiceRemainingByCode", vendorInvoiceCode);
    Assert.assertEquals(new BigDecimal(vendorInvoiceRemaining).compareTo(remaining), 0);
  }

  public void assertThatPORemainingMatchesExpected(String purchaseOrderCode, String remaining) {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
            (DObPurchaseOrderGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseOrderObjectByCode", purchaseOrderCode);
    assertNotNull(
            purchaseOrderGeneralModel, " There is no PurchaseOrder with code " + purchaseOrderCode);
    BigDecimal expectedRemaining = new BigDecimal(remaining);
    BigDecimal actualRemaining = purchaseOrderGeneralModel.getRemaining();
    Assert.assertEquals(
        " Remaining value of PurchaseOrder with code "
            + purchaseOrderCode
            + " is not updated as expected",
        0,
        expectedRemaining.compareTo(actualRemaining));
  }
}
