package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestPostValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestPostCommonTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestPostCommonTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");

    str.append(",").append("db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorAccountingInfo.sql");

    return str.toString();
  }

  public static String getDbScriptsEveryTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(
            "db-scripts/accounting/payment-request/iob-payment-request-payment-details-view-all.sql");
    str.append(
            "," + "db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    str.append(
            "," + "db-scripts/accounting/payment-request/iob-payment-request-accounting-details.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetailsViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoicePostingDetails.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append(
            "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-viewall.sql");
    str.append(
            "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items-viewall.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items.sql");
    return str.toString();
  }
  public static String clearAccountingNoteInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/accountingnote/Accounting_Notes_Clear.sql");
    return str.toString();
  }

  public static String clearCollectionInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/collection/collection_clear.sql");
    return str.toString();
  }

  public String getActivationUrl(String paymentCode, String paymentType, String method) {
    return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND + "/" + paymentCode
            + IDObPaymentRequestRestURLs.ACTIVATE + "?type=" + paymentType + "&method=" + method;
  }
  public String getActivationUrl(String paymentCode, String paymentType, String method, String dueDocument) {
    return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND + "/" + paymentCode
            + IDObPaymentRequestRestURLs.ACTIVATE + "?type=" + paymentType + "&method=" + method + "&dueDocument=" + dueDocument;
  }

  public JsonObject getPostValueObject(DataTable postDataTable) {

    Map<String, String> postDataMap = postDataTable.asMaps(String.class, String.class).get(0);

    JsonObject valueObject = new JsonObject();
    valueObject.addProperty(
        IDObPaymentRequestPostValueObject.DUE_DATE,
        postDataMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE));
    return valueObject;
  }

  public void assertThatPaymentRequestIsUpdatedSuccessfully(
      String paymentRequestCode, DataTable paymentRequestDataTable) {
    Map<String, String> paymentRequestMap =
        paymentRequestDataTable.asMaps(String.class, String.class).get(0);

    DObPaymentRequest dObPaymentRequest =
        (DObPaymentRequest)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedPaymentRequest",
                constructPaymentRequestQueryParams(paymentRequestCode, paymentRequestMap));
    Assert.assertNotNull(
        "Payment Request is not updated successfully for " + paymentRequestCode, dObPaymentRequest);
  }

  public Object[] constructPaymentRequestQueryParams(
          String paymentRequestCode, Map<String, String> paymentRequestMap) {
    return new Object[]{
            paymentRequestCode,
            paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            "%" + paymentRequestMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatPaymentRequestActivationDetailsIsCreatedSuccessfully(
          String paymentRequestCode, DataTable activationDetailsDataTable) {
    Map<String, String> activationDetailsMap =
            activationDetailsDataTable.asMaps(String.class, String.class).get(0);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetails =
            (IObAccountingDocumentActivationDetailsGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getPaymentRequestActivationDetails",
                            constructPaymentRequestActivationDetailsQueryParams(
                                    paymentRequestCode, activationDetailsMap));
    Assert.assertNotNull(
            "Payment Request Activation Details is not created successfully for " + paymentRequestCode,
            activationDetails);
  }

  public Object[] constructPaymentRequestActivationDetailsQueryParams(
          String paymentRequestCode, Map<String, String> postingDetailsMap) {
    return new Object[]{
            paymentRequestCode,
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_LATEST_EXCHANGE_RATE_CODE),
            new BigDecimal(postingDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE))
    };
  }

  public void assertThatTheFollowingPaymentRequestExist(DataTable paymentRequestDataTable) {
    List<Map<String, String>> paymentRequestListMap =
        paymentRequestDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prPaymentDetailsMap : paymentRequestListMap) {
      IObPaymentRequestPaymentDetailsGeneralModel prPaymentDetails =
          (IObPaymentRequestPaymentDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPaymentRequestPaymentDetails",
                  constructPaymentRequestPaymentDetailsQueryParams(prPaymentDetailsMap));
      Assert.assertNotNull(
          "Payment Request Payment Details doesn't exist for "
              + prPaymentDetailsMap.get(IFeatureFileCommonKeys.CODE),
          prPaymentDetails);
    }
  }

  private Object[] constructPaymentRequestPaymentDetailsQueryParams(
      Map<String, String> prPaymentDetailsMap) {
    return new Object[] {
      prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE),
      prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT),
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.CODE),
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + prPaymentDetailsMap.get(IFeatureFileCommonKeys.STATE) + "%",
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.TYPE),
      new BigDecimal(prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT))
    };
  }
}
