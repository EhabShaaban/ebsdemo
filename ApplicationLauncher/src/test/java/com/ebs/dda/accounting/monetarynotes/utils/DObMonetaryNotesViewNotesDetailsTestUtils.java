package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObMonetaryNotesDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

public class DObMonetaryNotesViewNotesDetailsTestUtils extends DObMonetaryNotesCommonTestUtils {
  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Notes Details]";

  public DObMonetaryNotesViewNotesDetailsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readNotesDetails(Cookie cookie, String monetaryNoteCode) {
    return sendGETRequest(cookie, getViewNotesDetailsUrl(monetaryNoteCode));
  }

  private String getViewNotesDetailsUrl(String monetaryNoteCode) {
    return IDObNotesReceivableRestURLs.QUERY
        + "/"
        + monetaryNoteCode
        + IDObNotesReceivableRestURLs.VIEW_NOTES_DETAILS;
  }

  public void assertMonetaryNoteNotesDetailsIsCorrect(Response response, DataTable companyDataTable)
      throws Exception {
    Map<String, String> expectedNotesDetails =
        companyDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualNotesDetails = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedNotesDetails, actualNotesDetails);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IIObMonetaryNotesDetailsGeneralModel.USER_CODE);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.NOTE_FORM, IIObMonetaryNotesDetailsGeneralModel.NOTE_FORM);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER,
        IIObMonetaryNotesDetailsGeneralModel.BUSINESS_PARTNER_CODE,
        IIObMonetaryNotesDetailsGeneralModel.BUSINESS_PARTNER_NAME);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.NOTE_NUMBER,
        IIObMonetaryNotesDetailsGeneralModel.NOTE_NUMBER);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.NOTE_BANK, IIObMonetaryNotesDetailsGeneralModel.NOTE_BANK);

    mapAssertion.assertDateTime(
        IAccountingFeatureFileCommonKeys.DUE_DATE, IIObMonetaryNotesDetailsGeneralModel.DUE_DATE);

    mapAssertion.assertConcatenatingValue(
        IAccountingFeatureFileCommonKeys.CURRENCY,
        IIObMonetaryNotesDetailsGeneralModel.CURRENCY_CODE,
        IIObMonetaryNotesDetailsGeneralModel.CURRENCY_ISO,
        " - ");

    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.AMOUNT, IIObMonetaryNotesDetailsGeneralModel.AMOUNT);

    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.REMAINING, IIObMonetaryNotesDetailsGeneralModel.REMAINING);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.DEPOT,
        IIObMonetaryNotesDetailsGeneralModel.DEPOT_CODE,
        IIObMonetaryNotesDetailsGeneralModel.DEPOT_NAME);
  }
}
