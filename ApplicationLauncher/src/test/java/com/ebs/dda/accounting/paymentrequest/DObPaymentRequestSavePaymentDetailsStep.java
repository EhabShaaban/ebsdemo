package com.ebs.dda.accounting.paymentrequest;

import java.util.Map;

import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import org.springframework.beans.factory.InitializingBean;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestSavePaymentDetailsTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.IObPaymentRequestSaveCompanyDataTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceCreateTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewCompanyDataTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPaymentRequestSavePaymentDetailsStep extends SpringBootRunner
    implements En, InitializingBean {

  private static final String SCENARIO_NAME = "Save PaymentDetails section";

  private DObPaymentRequestSavePaymentDetailsTestUtils utils;
  private DObVendorInvoiceTestUtils invoiceTestUtils;
  private MObVendorViewAllTestUtils vendorViewAllTestUtils;
  private DObVendorInvoiceViewCompanyDataTestUtils vendorInvoiceViewCompanyDataTestUtils;
  private DObVendorInvoiceCreateTestUtils vendorInvoiceCreateTestUtils;
  private IObPaymentRequestSaveCompanyDataTestUtils companyDataTestUtils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  public DObPaymentRequestSavePaymentDetailsStep() {
    Given(
        "^the following PurchaseOrders exist with the company data:$",
        (DataTable purchaseOrderDataTable) -> {
          vendorInvoiceCreateTestUtils.assertPurchaseOrderExists(purchaseOrderDataTable);
        });
    Given(
        "^the following Vendors exits:$",
        (DataTable vendorsDataTable) -> {
          vendorViewAllTestUtils.assertThatVendorsExistByCodeAndNameAndPurchaseUnits(
              vendorsDataTable);
        });

    Given(
        "^the following Vendor Invoices exist:$",
        (DataTable invoicesDataTable) -> {
          invoiceTestUtils.assertThatInvoicesExistWithStateAndVendorAndPurchaseUnitAndCurrency(
              invoicesDataTable);
        });

    Given(
        "^the following CompanyData for VendorInvoice exist:$",
        (DataTable companyDataDT) -> {
          vendorInvoiceViewCompanyDataTestUtils.assertThatCompanyDataIsExist(companyDataDT);
        });

    Given(
        "^the following Service Items exist:$",
        (DataTable itemsDT) -> {
          utils.assertThatServiceItemsExist(itemsDT);
        });

    When(
        "^\"([^\"]*)\" saves PaymentDetails section of PaymentRequest with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String prCode, String dateTime, DataTable paymentDetailDataTable) -> {
          utils.freeze(dateTime);
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePaymentDetailsSection(loginCookie, prCode, paymentDetailDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PaymentRequest of type \"([^\"]*)\" with Code \"([^\"]*)\" is updated as follows:$",
        (String paymentRequestType,
            String paymentRequestCode,
            DataTable paymentDetailDataTable) -> {
          utils.checkUpdatedDataInVendorPaymentDetails(paymentRequestCode, paymentDetailDataTable);
        });

    Then(
        "^CompanyData section in PaymentRequest with Code \"([^\"]*)\" is updated as follows:$",
        (String paymentRequestCode, DataTable companyDataTable) -> {
          companyDataTestUtils.assertThatCompanyDataIsUpdated(paymentRequestCode, companyDataTable);
        });
    And(
        "^the Company \"([^\"]*)\" has the following Treasuries:$",
        (String company, DataTable companyTreasuriesDataTable) -> {
          enterpriseTestUtils.assertThatCompanyTreasuriesExist(company, companyTreasuriesDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();

    utils = new DObPaymentRequestSavePaymentDetailsTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    invoiceTestUtils = new DObVendorInvoiceTestUtils(properties);
    invoiceTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    vendorViewAllTestUtils = new MObVendorViewAllTestUtils(properties);
    vendorViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    vendorInvoiceViewCompanyDataTestUtils =
        new DObVendorInvoiceViewCompanyDataTestUtils(properties);
    vendorInvoiceViewCompanyDataTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    vendorInvoiceCreateTestUtils =
        new DObVendorInvoiceCreateTestUtils(properties, entityManagerDatabaseConnector);

    companyDataTestUtils = new IObPaymentRequestSaveCompanyDataTestUtils(properties);
    companyDataTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    enterpriseTestUtils.setProperties(properties);
  }

  @Before
  public void beforeSavePaymentDetails(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          DObPaymentRequestSavePaymentDetailsTestUtils
              .getPaymentRequestDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(
                DObPaymentRequestSavePaymentDetailsTestUtils
                        .clearAccountingDocuments());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObPaymentRequestRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
