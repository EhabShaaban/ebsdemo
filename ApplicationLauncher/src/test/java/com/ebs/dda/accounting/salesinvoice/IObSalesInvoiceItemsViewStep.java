package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceItemsViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObSalesInvoiceItemsViewStep extends SpringBootRunner implements En {

    public static final String SCENARIO_NAME = "View Items in SalesInvoice";
    private static boolean hasBeenExecutedOnce = false;
  private IObSalesInvoiceItemsViewTestUtils salesInvoiceViewItemsTestUtils;

  IObSalesInvoiceItemsViewStep() {

    Given(
        "^the following SalesInvoices have empty Items secion:$",
        (DataTable itemsTable) -> {
          salesInvoiceViewItemsTestUtils.assertThatSalesInvoiceHasNoItems(itemsTable);
        });

    Given(
        "^the following are ALL the Collections posted against the SalesInvoice with code \"([^\"]*)\"$",
        (String salesInvoiceCode, DataTable collectionsTable) -> {
          salesInvoiceViewItemsTestUtils.assertCollectionsExistWithAmount(
              salesInvoiceCode, collectionsTable);
        });

    Given(
        "^the following are ALL the NotesRecievables posted against the SalesInvoice with code \"([^\"]*)\"$",
        (String salesInvoiceCode, DataTable notesRecievables) -> {
          salesInvoiceViewItemsTestUtils.assertNotesRecievablesExistWithAmount(
              salesInvoiceCode, notesRecievables);
        });

    When(
        "^\"([^\"]*)\" requests to view Items section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              salesInvoiceViewItemsTestUtils.requestToReadSalesInvoiceItems(
                  cookie, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of Items section for SalesInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesInvoiceCode, String userName, DataTable salesInvoicesItems) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceViewItemsTestUtils.assertThatInvoiceItemsMatch(
              salesInvoiceCode, salesInvoicesItems, response);
        });

    Then(
        "^an empty Items section in SalesInvoice is displayed to \"([^\"]*)\"$",
        (String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceViewItemsTestUtils.assertThatInvoiceItemsAreEmpty(response);
        });

    Given(
        "^the following SalesInvoices exist:$",
        (DataTable salesInvoicesDataTable) -> {
          salesInvoiceViewItemsTestUtils.assertSalesInvoiceExists(salesInvoicesDataTable);
        });

}

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    salesInvoiceViewItemsTestUtils = new IObSalesInvoiceItemsViewTestUtils(properties);
    salesInvoiceViewItemsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            salesInvoiceViewItemsTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(IObSalesInvoiceItemsViewTestUtils.clearSalesInvoices());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
