package com.ebs.dda.accounting.costing;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.costing.utils.IObCostingViewDetailsTestUtils;
import com.ebs.dda.accounting.costing.utils.IObCostingViewItemsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObCostingViewItemsStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "View Costing Items section";
  private IObCostingViewItemsTestUtils utils;

  public IObCostingViewItemsStep() {

    When(
        "^\"([^\"]*)\" requests to view Items section for Costing with code \"([^\"]*)\"$",
        (String userName, String costingCode) -> {
          String readUrl = utils.getCostingItemsViewDataUrl(costingCode);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(userCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^the following values of Items section for Costing with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
            (String costingCode,
             String userName,
             DataTable itemsDataTable) -> {
                Response response = userActionsTestUtils.getUserResponse(userName);
                utils.assertCorrectValuesAreDisplayed(response, itemsDataTable);
            });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObCostingViewItemsTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewAll(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(utils.clearCostingDocumentInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
