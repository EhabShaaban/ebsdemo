package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.ISalesResponseKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesInvoiceReadAllCurrencyUtils extends CommonTestUtils {

  public DObSalesInvoiceReadAllCurrencyUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    this.urlPrefix = (String) getProperty(URL_PREFIX);

    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    return str.toString();
  }

  public void assertThatResponseCurrencyDataIsCorrect(
          Response response, DataTable customerDataTable) throws Exception {
    List<Map<String, String>> expectedCustomerList =
            customerDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualCusomerData = getListOfMapsFromResponse(response);

    for (int i = 0; i < actualCusomerData.size(); i++) {
      MapAssertion mapAssertion =
              new MapAssertion(expectedCustomerList.get(i), actualCusomerData.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          ISalesFeatureFileCommonKeys.CURRENCY, ISalesResponseKeys.CODE, ISalesResponseKeys.NAME);
    }
  }

  public void assertTotalNumberOfRecordsEqualResponseSize(
      Response response, int totalRecordsNumber) {
    List<HashMap<String, Object>> responseData = getListOfMapsFromResponse(response);
    assertEquals(responseData.size(), totalRecordsNumber);
  }
}
