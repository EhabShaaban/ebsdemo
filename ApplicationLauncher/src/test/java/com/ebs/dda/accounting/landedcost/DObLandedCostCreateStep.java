package com.ebs.dda.accounting.landedcost;

import java.sql.SQLException;
import java.util.Map;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCreateHPTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewCompanyDataTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObLandedCostCreateStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "Create LandedCost";

  private static boolean hasBeenExecuted = false;

  private DObVendorInvoiceViewCompanyDataTestUtils invoiceTestUtils;
  private DObVendorInvoiceViewDetailsTestUtils dObInvoiceViewDetailsTestUtils;
  private DObLandedCostCreateHPTestUtils lcCreateUtils;

  public DObLandedCostCreateStep() {
    Given(
        "^the following Vendor Invoices exist:$",
        (DataTable invoicesDataTable) -> {
          invoiceTestUtils.assertThatInvoicesExist(invoicesDataTable);
        });

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String dateTime) -> {
          lcCreateUtils.freeze(dateTime);
        });

    Given(
        "^Last created LandedCost was with code \"([^\"]*)\"$",
        (String lastLcCode) -> {
          lcCreateUtils.assertLastLandedCostCodeIsExist(lastLcCode);
        });

    When(
        "^\"([^\"]*)\" creates LandedCost with the following values:$",
        (String username, DataTable landedCostDT) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response response = lcCreateUtils.createLandedCost(userCookie, landedCostDT);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^a new LandedCost is created with the following values:$",
        (DataTable newLandedCostDT) -> {
          lcCreateUtils.assertLandedCostIsCreatedSuccessfully(newLandedCostDT);
        });

    Then(
        "^the Company Section of LandedCost with code \"([^\"]*)\" is created as follows:$",
        (String code, DataTable companyDT) -> {
          lcCreateUtils.assertCompanySectionIsCreatedSuccessfully(code, companyDT);
        });

    Then(
        "^the Details Section of LandedCost with code \"([^\"]*)\" is created as follows:$",
        (String code, DataTable detailsDT) -> {
          lcCreateUtils.assertDetailsSectionIsCreatedSuccessfully(code, detailsDT);
        });

    When(
        "^\"([^\"]*)\" requests to create LandedCost$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              lcCreateUtils.sendGETRequest(cookie, IDObLandedCostRestUrls.REQUEST_CREATE_URL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    lcCreateUtils = new DObLandedCostCreateHPTestUtils(properties, entityManagerDatabaseConnector);

    invoiceTestUtils = new DObVendorInvoiceViewCompanyDataTestUtils(properties);
    invoiceTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    dObInvoiceViewDetailsTestUtils = new DObVendorInvoiceViewDetailsTestUtils(properties);
    dObInvoiceViewDetailsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains(SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(lcCreateUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(
          AccountingDocumentCommonTestUtils.clearAccountingDocuments());
      databaseConnector.executeSQLScript(DObLandedCostCommonTestUtils.clearLandedCosts());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains(SCENARIO_NAME)) {
      databaseConnector.closeConnection();
      lcCreateUtils.unfreeze();
    }
  }
}
