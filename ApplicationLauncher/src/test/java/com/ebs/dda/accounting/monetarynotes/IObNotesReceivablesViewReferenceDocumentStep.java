package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.IObNotesReceivableViewReferenceDocumentTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class IObNotesReceivablesViewReferenceDocumentStep extends SpringBootRunner
    implements En, InitializingBean {

  private IObNotesReceivableViewReferenceDocumentTestUtils utils;
  private DObSalesInvoiceActivateTestUtils salesInvoiceUtils;

  public IObNotesReceivablesViewReferenceDocumentStep() {
    Given(
        "^the following Summaries for SalesInvoices exist:$",
        (DataTable SalesInvoicesSummariesDataTable) -> {
          salesInvoiceUtils.assertThatSalesInvoiceInformationExist(SalesInvoicesSummariesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view ReferenceDocuments section of MonetaryNotes with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readReferenceDocuments(cookie, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^the following values of ReferenceDocuments section for MonetaryNotes with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String monetaryNoteCode, String userName, DataTable referenceDocumentsTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertMonetaryNoteReferenceDocumentsIsCorrect(
              monetaryNoteCode, response, referenceDocumentsTable);
        });
    Then(
        "^ReferenceDocuments section for MonetaryNotes with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String monetaryNoteCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReferenceDocumentsResponseIsEmpty(response);
        });
    }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils =
        new IObNotesReceivableViewReferenceDocumentTestUtils(
            properties, entityManagerDatabaseConnector);

    salesInvoiceUtils = new DObSalesInvoiceActivateTestUtils(properties);
    salesInvoiceUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View NotesReceivable ReferenceDocuments")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }
}
