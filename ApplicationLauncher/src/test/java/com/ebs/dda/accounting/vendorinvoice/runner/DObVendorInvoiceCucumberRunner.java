package com.ebs.dda.accounting.vendorinvoice.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = {

    "classpath:features/accounting/vendor-invoice/VI_Create_HP.feature",
    "classpath:features/accounting/vendor-invoice/VI_Create_Auth.feature",
    "classpath:features/accounting/vendor-invoice/VI_Create_Val.feature",
    "classpath:features/accounting/vendor-invoice/VI_PO_Dropdowns.feature",
    "classpath:features/accounting/vendor-invoice/VI_AllowedActions.feature",
    "classpath:features/accounting/vendor-invoice/VI_Activate_ImportVendorInvoice_HP.feature",
    "classpath:features/accounting/vendor-invoice/VI_Activate_LocalVendorInvoice_HP.feature",
    "classpath:features/accounting/vendor-invoice/VI_Activate_ServiceInvoice_HP.feature",
    "classpath:features/accounting/vendor-invoice/VI_Activate_Val.feature",
    "classpath:features/accounting/vendor-invoice/VI_Activate_Auth.feature",
    "classpath:features/accounting/vendor-invoice/VI_ViewAll.feature",
    "classpath:features/accounting/vendor-invoice/VI_RequestAddItem_Items.feature",
    "classpath:features/accounting/vendor-invoice/VI_RequestEdit_Items.feature",
    "classpath:features/accounting/vendor-invoice/VI_View_Items.feature",
    "classpath:features/accounting/vendor-invoice/VI_DeleteItem_Items.feature",
    "classpath:features/accounting/vendor-invoice/VI_SaveEditItem_Items_HP.feature",
    "classpath:features/accounting/vendor-invoice/VI_SaveNewItem_Items_HP.feature",
    "classpath:features/accounting/vendor-invoice/VI_SaveNewItem_Items_Val.feature",
    "classpath:features/accounting/vendor-invoice/VI_SaveNewItem_Items_Auth.feature",
    "classpath:features/accounting/vendor-invoice/VI_View_GeneralData.feature",
    "classpath:features/accounting/vendor-invoice/VI_RequestEdit_InvoiceDetails.feature",
    "classpath:features/accounting/vendor-invoice/VI_Save_InvoiceDetails_HP.feature",
    "classpath:features/accounting/vendor-invoice/VI_Save_InvoiceDetails_Val.feature",
    "classpath:features/accounting/vendor-invoice/VI_Save_InvoiceDetails_Auth.feature",
    "classpath:features/accounting/vendor-invoice/VI_View_InvoiceDetails.feature",
    "classpath:features/accounting/vendor-invoice/VI_View_Taxes.feature",
    "classpath:features/accounting/vendor-invoice/VI_View_Summary.feature",
    "classpath:features/accounting/vendor-invoice/VI_Delete_Tax.feature",
    "classpath:features/accounting/vendor-invoice/VI_View_CompanyData.feature",
    "classpath:features/accounting/vendor-invoice/VI_Delete.feature",
    "classpath:features/accounting/vendor-invoice/VI_View_ActivationDetails.feature",
    "classpath:features/accounting/vendor-invoice/VI_DocumentOwner_Dropdown.feature",
    "classpath:features/accounting/vendor-invoice/VI_Items_Dropdowns.feature",
    "classpath:features/accounting/vendor-invoice/VI_ItemsUnits_Dropdowns.feature"

}, glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.vendorinvoice"}, strict = true,
    tags = "not @Future")
public class DObVendorInvoiceCucumberRunner {
}
