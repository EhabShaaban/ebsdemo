package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import cucumber.api.DataTable;
import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class IObVendorInvoiceItemsTestUtils extends DObVendorInvoiceCommonTestUtils {

  public IObVendorInvoiceItemsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertEmptyInvoiceItemsInVendorInvoice(DataTable vendorInvoiceDataTable) {
    List<Map<String, String>> vendorInvoices =
            vendorInvoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendorInvoice : vendorInvoices) {
      String vendorInvoiceCode = vendorInvoice.get(IFeatureFileCommonKeys.CODE);
      List<Object[]> actualData =
              entityManagerDatabaseConnector.executeNativeNamedQuery(
                      "getInvoicesWithNoItemsByCode", vendorInvoiceCode);
      assertTrue(actualData.isEmpty());
    }
  }
  public void assertThatInvoiceHasItems(String invoiceCode, DataTable invoiceData) {
    List<Map<String, String>> invoiceItems = invoiceData.asMaps(String.class, String.class);
    for (Map<String, String> invoiceItem : invoiceItems) {
      String itemCode = invoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_CODE);
      List<IObVendorInvoiceItemsGeneralModel> actualInvoiceItems =
              (List<IObVendorInvoiceItemsGeneralModel>) entityManagerDatabaseConnector.executeNativeNamedQuery(
                      "getVendorInvoicesItemsByCode", invoiceCode, itemCode);
      assertNotNull(actualInvoiceItems);
      assertFalse(actualInvoiceItems.isEmpty());
    }
  }

  public void assertThatInvoicesExist(DataTable invoicesDataTable) {
    List<Map<String, String>> invoicesList = invoicesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {

      String invoiceCode = invoice.get(IFeatureFileCommonKeys.CODE);
      String invoiceType = invoice.get(IFeatureFileCommonKeys.TYPE);
      String invoicePurchaseUnit = invoice.get(IAccountingFeatureFileCommonKeys.PURCHASE_UNIT);
      String invoiceVendorCode = invoice.get(IFeatureFileCommonKeys.VENDOR_CODE);
      String invoicePurchaseOrder = invoice.get(IAccountingFeatureFileCommonKeys.PURCHESE_ORDER);
      String invoiceNumber = invoice.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER);
      String invoiceState = "%" + invoice.get(IFeatureFileCommonKeys.STATE) + "%";

      DObVendorInvoiceGeneralModel dObInvoice =
              (DObVendorInvoiceGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getVendorInvoices",
                              invoiceCode,
                              invoiceType,
                              invoicePurchaseUnit,
                              invoiceVendorCode,
                              invoicePurchaseOrder,
                              invoiceNumber,
                              invoiceState);

      assertNotNull(dObInvoice);
    }
  }

  public void assertVendorInvoiceItemExists(String vendorInvoiceCode,
      DataTable vendorInvoicesItems) {
    List<Map<String, String>> itemList = vendorInvoicesItems.asMaps(String.class, String.class);
    for (Map<String, String> itemData : itemList) {
      Object item =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorInvoiceItem",
                  constractQueryParam(vendorInvoiceCode, itemData));
      Assertions.assertNotNull(item);
    }
  }

  private Object[] constractQueryParam(String vendorInvoiceCode, Map<String, String> itemData) {
    String itemId = itemData.get(IAccountingFeatureFileCommonKeys.ITEM_ID);
    String item = itemData.get(IAccountingFeatureFileCommonKeys.ITEM);
    String qtyOrder = itemData.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER);
    String orderUnit = itemData.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT);
    String unitPriceBase = itemData.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE);
    String unitOrderPrice = itemData.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT);

    return new Object[]{Long.parseLong(itemId), vendorInvoiceCode, item,
        Double.parseDouble(qtyOrder), orderUnit, Double.parseDouble(unitPriceBase), Double.parseDouble(unitOrderPrice)};
  }

}
