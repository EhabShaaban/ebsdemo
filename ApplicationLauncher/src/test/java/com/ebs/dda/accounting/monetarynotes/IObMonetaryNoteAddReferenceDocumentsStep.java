package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.IObMonetaryNoteAddReferenceDocumentsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class IObMonetaryNoteAddReferenceDocumentsStep extends SpringBootRunner implements En {
  private IObMonetaryNoteAddReferenceDocumentsTestUtils utils;

  public IObMonetaryNoteAddReferenceDocumentsStep() {

    When(
        "^\"([^\"]*)\" requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          String lockUrl = utils.getLockReferenceDocumentsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^ReferenceDocument section of NotesReceivable with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String monetaryNoteCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName,
              monetaryNoteCode,
              IDObMonetaryNotesSectionNames.REFERENCE_DOCUMENTS_SECTION,
              utils.NOTES_RECEIVABLE_JMX_LOCK_BEAN_NAME);
        });

    Given(
        "^\"([^\"]*)\" first requests to add ReferenceDocument to ReferenceDocument section of NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          String lockUrl = utils.getLockReferenceDocumentsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, monetaryNoteCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^ReferenceDocument section of NotesReceivable with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String monetaryNoteCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = utils.getLockReferenceDocumentsUrl(monetaryNoteCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^ReferenceDocument section of NotesReceivable with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String monetaryNoteCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName,
              monetaryNoteCode,
              utils.NOTES_RECEIVABLE_JMX_LOCK_BEAN_NAME,
              IDObMonetaryNotesSectionNames.REFERENCE_DOCUMENTS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to cancel add ReferenceDocument to ReferenceDocument section of NotesReceivable with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unLockUrl = utils.getUnLockReferenceDocumentsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^the lock by \"([^\"]*)\" on ReferenceDocument section of NotesReceivable with code \"([^\"]*)\" is released$",
        (String userName, String monetaryNoteCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              monetaryNoteCode,
              utils.NOTES_RECEIVABLE_JMX_LOCK_BEAN_NAME,
              IDObMonetaryNotesSectionNames.REFERENCE_DOCUMENTS_SECTION);
        });
    When(
        "^\"([^\"]*)\" requests to cancel add ReferenceDocument to ReferenceDocument section of NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          String unLockUrl = utils.getUnLockReferenceDocumentsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObMonetaryNoteAddReferenceDocumentsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to add ReferenceDocument in NotesReceivable,")
        || contains(
            scenario.getName(), "Request to Cancel add ReferenceDocument in NotesReceivable,")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to add ReferenceDocument in NotesReceivable,")
        || contains(
            scenario.getName(), "Request to Cancel add ReferenceDocument in NotesReceivable,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObNotesReceivableRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
