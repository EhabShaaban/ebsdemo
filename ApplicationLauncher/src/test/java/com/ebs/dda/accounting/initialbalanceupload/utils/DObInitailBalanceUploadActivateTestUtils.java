package com.ebs.dda.accounting.initialbalanceupload.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.initialbalanceupload.IDObInitialBalanceUploadRestURLS;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Map;

public class DObInitailBalanceUploadActivateTestUtils extends DObInitialBalanceUploadCommonTestUtils{
    public DObInitailBalanceUploadActivateTestUtils(Map<String, Object> properties) {
        setProperties(properties);
    }

    public Response activateInitailBalanceUplod(Cookie userCookie, String initialBalanceUploadCode) {

        return sendPUTRequest(
                userCookie,
                IDObInitialBalanceUploadRestURLS.INITIAL_BALANCE_UPLOAD_COMMAND
                        + "/"
                        + initialBalanceUploadCode
                        + IDObInitialBalanceUploadRestURLS.ACTIVATE,
                new JsonObject());
    }

    public void assertInitialBalanceUploadDetailsIsUpdatedAfterPost(String initialBalanceUploadCode, DataTable initialBalanceUploadDataTable) {
        {
            Map<String, String> expectedInitialBalanceUploadMap =
                    initialBalanceUploadDataTable.asMaps(String.class, String.class).get(0);
            Object lastUpdatedDate =
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getInitialStockUploadByCodeAndStateAndModifiedByInfo",
                            initialBalanceUploadCode,
                            "%" + expectedInitialBalanceUploadMap.get(IFeatureFileCommonKeys.STATE) + "%",
                            expectedInitialBalanceUploadMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY));

            assertDateEquals(
                    expectedInitialBalanceUploadMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                    new DateTime(lastUpdatedDate).withZone(DateTimeZone.UTC));
        }}
}
