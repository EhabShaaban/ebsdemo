package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.IObLandedCostServiceItemsDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class IObLandedCostServiceItemsDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObLandedCostServiceItemsDropDownTestUtils utils;
   private static final String SCENARIO_HEADER  = "Read All Items for LandedCost ServiceItems";

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new IObLandedCostServiceItemsDropDownTestUtils(properties, entityManagerDatabaseConnector);
  }

  public IObLandedCostServiceItemsDropDownStep() {

      Then("^the following Items should be returned to \"([^\"]*)\":$", (String userName, DataTable servieItemsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseMatchesExpected(response, servieItemsDataTable);
      });

      Then("^total number of service items returned to \"([^\"]*)\" is equal to (\\d+)$", (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
      });
      When("^\"([^\"]*)\" requests to read all Items That marked as Service items for landed Cost With Code \"([^\"]*)\"$", (String userName, String landedCostCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.readAllLandedCostServiceItems(landedCostCode));
          userActionsTestUtils.setUserResponse(userName, response);

      });

  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains(SCENARIO_HEADER)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
        databaseConnector.executeSQLScript(utils.getDbScripts());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains(SCENARIO_HEADER)) {

      databaseConnector.closeConnection();
    }
  }
}
