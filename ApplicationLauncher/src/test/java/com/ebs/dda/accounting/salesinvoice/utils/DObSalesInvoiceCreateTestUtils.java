package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.CObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceCreationValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObSalesInvoiceCreateTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceCreateTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/DObSalesInvoice_Create_HP.sql");

    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");

    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");

    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderItems.sql");
    return str.toString();
  }

  public void assertThatSalesInvoiceTypesExist(DataTable customerDataTable) {

    List<Map<String, String>> customerList = customerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> customer : customerList) {

      String invoiceCode = customer.get(IFeatureFileCommonKeys.CODE);
      String invoiceName = customer.get(IFeatureFileCommonKeys.NAME);

      CObSalesInvoice cObSalesInvoice =
          (CObSalesInvoice)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceTypesByCodeAndName", invoiceCode, invoiceName);

      assertNotNull(cObSalesInvoice);
    }
  }

  public void assertSalesInvoiceExistsWithCode(String salesInvoiceCode) {
    Object dObSalesInvoice =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getLastSalesInvoiceByCode");

    assertEquals(salesInvoiceCode, dObSalesInvoice);
  }

  public Response createSalesInvoice(Cookie userCookie, DataTable salesInvoiceDataTable) {
    JsonObject valueObject = prepareValueObject(salesInvoiceDataTable);
    return sendPostRequest(userCookie, IDObSalesInvoiceRestUrls.CREATE_URL, valueObject);
  }

  private JsonObject prepareValueObject(DataTable salesInvoiceDataTable) {
    Map<String, String> salesInvoiceRecord =
        salesInvoiceDataTable.asMaps(String.class, String.class).get(0);
    salesInvoiceRecord = convertEmptyStringsToNulls(salesInvoiceRecord);

    JsonObject parsedData = new JsonObject();
    parsedData.addProperty(
        IDObSalesInvoiceCreationValueObject.INVOICE_TYPE_CODE,
        salesInvoiceRecord.get(ISalesFeatureFileCommonKeys.TYPE));

    parsedData.addProperty(
        IDObSalesInvoiceCreationValueObject.SALES_ORDER_CODE,
        salesInvoiceRecord.get(ISalesFeatureFileCommonKeys.SALES_ORDER));

    parsedData.addProperty(
        IDObSalesInvoiceCreationValueObject.BUSINESS_UNIT_CODE,
        salesInvoiceRecord.get(ISalesFeatureFileCommonKeys.BUSINESS_UNIT));

    parsedData.addProperty(
        IDObSalesInvoiceCreationValueObject.COMPANY_CODE,
        salesInvoiceRecord.get(ISalesFeatureFileCommonKeys.COMPANY));

    parsedData.addProperty(
        IDObSalesInvoiceCreationValueObject.CUSTOMER_CODE,
        salesInvoiceRecord.get(ISalesFeatureFileCommonKeys.CUSTOMER));

    parsedData.addProperty(
        IDObSalesInvoiceCreationValueObject.DOCUMENT_OWNER_ID,
        Long.parseLong(salesInvoiceRecord.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID)));

    return parsedData;
  }

  public void assertSalesInvoiceIsCreatedSuccessfully(DataTable invoiceDataTable) {
    List<Map<String, String>> invoicesList = invoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {
      Object[] params = constructGetCreatedInvoiceQueryParams(invoice);
      DObSalesInvoiceGeneralModel cObInvoice =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedSalesInvoice", params);

      assertNotNull("Sales invoice is not exist, given " + Arrays.toString(params), cObInvoice);
    }
  }

  private Object[] constructGetCreatedInvoiceQueryParams(Map<String, String> invoice) {
    return new Object[] {
      invoice.get(IFeatureFileCommonKeys.CODE),
      '%' + invoice.get(IFeatureFileCommonKeys.STATE) + '%',
      invoice.get(IFeatureFileCommonKeys.CREATED_BY),
      invoice.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      invoice.get(IFeatureFileCommonKeys.CREATION_DATE),
      invoice.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      invoice.get(ISalesFeatureFileCommonKeys.TYPE),
      invoice.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  public void assertThatBusinessPartnerForSIWithoutRefIsCreatedSuccessfully(
      String salesInvoiceCode, DataTable businessPartnerDataTable) {
    List<Map<String, String>> businessPartnerSections =
        businessPartnerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> businessPartnerSection : businessPartnerSections) {

      Object[] queryParams =
          new Object[] {
            salesInvoiceCode, businessPartnerSection.get(ISalesFeatureFileCommonKeys.CUSTOMER)
          };

      IObSalesInvoiceBusinessPartnerGeneralModel businessPatnerGeneralModel =
          (IObSalesInvoiceBusinessPartnerGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedBusinessPartnerForSIWithoutRef", queryParams);

      assertNotNull(businessPatnerGeneralModel);
    }
  }

  public void assertThatCompanySectionExistForSalesInvoice(
      String salesInvoiceCode, DataTable dataTable) {
    Map<String, String> companySalesInvoiceMap =
        dataTable.asMaps(String.class, String.class).get(0);
    Object[] queryParams =
        constructCompanySalesInvoiceQueryParams(salesInvoiceCode, companySalesInvoiceMap);
    DObSalesInvoiceGeneralModel salesInvoiceCompanyTaxesDetailsGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCompanySalesInvoice", queryParams);
    Assert.assertNotNull(
        "Company doesn't exist in SalesInvoice " + salesInvoiceCode,
        salesInvoiceCompanyTaxesDetailsGeneralModel);
  }

  private Object[] constructCompanySalesInvoiceQueryParams(
      String salesInvoiceCode, Map<String, String> companySalesInvoiceMap) {
    return new Object[] {
      salesInvoiceCode, companySalesInvoiceMap.get(ISalesFeatureFileCommonKeys.COMPANY)
    };
  }

  public void assertThatSalesOrderForSalesInvoiceIsExist(DataTable salesOrdersDT) {

    List<Map<String, String>> salesOrderMap = salesOrdersDT.asMaps(String.class, String.class);
    for (Map<String, String> order : salesOrderMap) {

      Object[] queryParams = constructGetSalesOrderForSalesInvoiceParams(order);

      DObSalesOrderGeneralModel salesOrder =
          (DObSalesOrderGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesOrderForSalesInvoice", queryParams);

      assertNotNull("Sales Order Does Not Exist " + order, salesOrder);
    }
  }

  private Object[] constructGetSalesOrderForSalesInvoiceParams(Map<String, String> order) {
    String orderCode = order.get(IFeatureFileCommonKeys.CODE);
    String orderType = order.get(IFeatureFileCommonKeys.TYPE);
    String orderBusinnesUnit = order.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    String orderState = "%" + order.get(IFeatureFileCommonKeys.STATE) + "%";
    String orderCurrencyISO = order.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO);
    String orderCompany = order.get(IFeatureFileCommonKeys.COMPANY);
    String orderCustomer = order.get(IFeatureFileCommonKeys.CUSTOMER);
    String orderPaymentTerm = order.get(IAccountingFeatureFileCommonKeys.PAYMENT_TERM);

    Object[] arr =
        new Object[] {
          orderCurrencyISO,
          orderCompany,
          orderPaymentTerm,
          orderCode,
          orderType,
          orderBusinnesUnit,
          orderState,
          orderCustomer
        };

    return arr;
  }

  public void assertThatSalesOrderItemsForSalesInvoiceIsExist(DataTable salesOrderItemsDT) {

    List<Map<String, String>> itemsMap = salesOrderItemsDT.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsMap) {

      Object[] queryParams = constructGetSalesOrderItemsForSalesInvoiceParams(item);

      Object salesOrder =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObSalesOrderItem", queryParams);

      assertNotNull(salesOrder);
    }
  }

  private Object[] constructGetSalesOrderItemsForSalesInvoiceParams(Map<String, String> itemMap) {
    String orderCode = itemMap.get(IAccountingFeatureFileCommonKeys.SO_CODE);
    String ItemId = itemMap.get(IAccountingFeatureFileCommonKeys.ITEM_ID);
    String ItemType = itemMap.get(IAccountingFeatureFileCommonKeys.ITEM_TYPE);
    String ItemCodeAndName = itemMap.get(IAccountingFeatureFileCommonKeys.ITEM);
    String orderUnit = itemMap.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT);
    String quantity = itemMap.get(IAccountingFeatureFileCommonKeys.QTY);
    String salesPrice = itemMap.get(IAccountingFeatureFileCommonKeys.ITEM_SALES_PRICE);
    String totalAmount = itemMap.get(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT);

    Object[] arr =
        new Object[] {
          orderCode,
          Long.parseLong(ItemId),
          ItemType,
          ItemCodeAndName,
          orderUnit,
          Double.parseDouble(quantity),
          Double.parseDouble(salesPrice),
          Double.parseDouble(totalAmount)
        };

    return arr;
  }

  public Response createSalesInvoiceForSalesOrder(
      DataTable salesInvoiceValuesDT, Cookie userCookie) {
    JsonObject salesOrder = prepareSalesInvoiceJsonObject(salesInvoiceValuesDT);
    String restURL = IDObSalesInvoiceRestUrls.CREATE_URL;
    return sendPostRequest(userCookie, restURL, salesOrder);
  }

  private JsonObject prepareSalesInvoiceJsonObject(DataTable salesInvoiceDT) {
    Map<String, String> salesInvoiceMap = salesInvoiceDT.asMaps(String.class, String.class).get(0);

    JsonObject salesInvoiceJsonObject = new JsonObject();

    addValueToJsonObject(
        salesInvoiceJsonObject,
        IDObSalesInvoiceCreationValueObject.INVOICE_TYPE_CODE,
        salesInvoiceMap.get(IOrderFeatureFileCommonKeys.TYPE));

    addValueToJsonObject(
        salesInvoiceJsonObject,
        IDObSalesInvoiceCreationValueObject.SALES_ORDER_CODE,
        salesInvoiceMap.get(IAccountingFeatureFileCommonKeys.SO_CODE));

    addValueToJsonObject(
        salesInvoiceJsonObject,
        IDObSalesInvoiceCreationValueObject.BUSINESS_UNIT_CODE,
        salesInvoiceMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT));

    addDocOwnerId(salesInvoiceMap, salesInvoiceJsonObject);

    return salesInvoiceJsonObject;
  }

  private void addDocOwnerId(
      Map<String, String> salesInvoiceMap, JsonObject salesInvoiceJsonObject) {
    String docOwnerId = salesInvoiceMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID);
    try {
      if (hasFloatingPoint(docOwnerId)) {
        salesInvoiceJsonObject.addProperty(
            IDObSalesInvoiceCreationValueObject.DOCUMENT_OWNER_ID, Double.parseDouble(docOwnerId));
      } else {
        salesInvoiceJsonObject.addProperty(
            IDObSalesInvoiceCreationValueObject.DOCUMENT_OWNER_ID, Long.parseLong(docOwnerId));
      }
    } catch (NumberFormatException e) {
      addValueToJsonObject(
          salesInvoiceJsonObject,
          IDObSalesInvoiceCreationValueObject.DOCUMENT_OWNER_ID,
          docOwnerId);
    }
  }

  private boolean hasFloatingPoint(String docOwnerId) {
    return docOwnerId.indexOf(".") >= 0;
  }

  public void assertThatBusinessPartnerIsCopiedFromSalesOrder(
      String salesInvoiceCode, DataTable businessPartnerValuesDT) {

    List<Map<String, String>> businessPartnerMap =
        businessPartnerValuesDT.asMaps(String.class, String.class);
    for (Map<String, String> businessPartner : businessPartnerMap) {

      Object[] queryParams =
          constructGetCreatedBusinessPartnerForSalesInvoiceOrders(
              salesInvoiceCode, businessPartner);

      IObSalesInvoiceBusinessPartnerGeneralModel businessPartnerGM =
          (IObSalesInvoiceBusinessPartnerGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedBusinessPartnerForSalesInvoiceOrder", queryParams);

      assertNotNull(businessPartnerGM);
    }
  }

  private Object[] constructGetCreatedBusinessPartnerForSalesInvoiceOrders(
      String salesInvoiceCode, Map<String, String> businessPartner) {

    String orderCode = businessPartner.get(IAccountingFeatureFileCommonKeys.SO_CODE);
    String currancyISO = businessPartner.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO);
    String paymentTerm = businessPartner.get(IAccountingFeatureFileCommonKeys.PAYMENT_TERM);
    String customer = businessPartner.get(IAccountingFeatureFileCommonKeys.CUSTOMER);

    return new Object[] {salesInvoiceCode, orderCode, customer, currancyISO, paymentTerm};
  }

  public void assertThatTheFollowingTaxDetailsExistForSalesInvoice(
      String salesInvoiceCode, DataTable taxDetailsDataTable) {
    List<Map<String, String>> taxDetailsMaps =
        taxDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> taxDetailsMap : taxDetailsMaps) {

      Object[] queryParams =
          constructCompanyTaxesForSalesInvoiceQueryParams(salesInvoiceCode, taxDetailsMap);

      Object salesInvoiceCompanyTaxesDetails =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getTaxesForCompanyInSalesInvoice", queryParams);
      Assert.assertNotNull(
          "Tax detail doesn't exist, given:" + Arrays.toString(queryParams),
          salesInvoiceCompanyTaxesDetails);
    }
  }

  public Object[] constructCompanyTaxesForSalesInvoiceQueryParams(
      String salesInvoiceCode, Map<String, String> companyTaxesValues) {
    return new Object[] {
      salesInvoiceCode,
      companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX),
      companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }
}
