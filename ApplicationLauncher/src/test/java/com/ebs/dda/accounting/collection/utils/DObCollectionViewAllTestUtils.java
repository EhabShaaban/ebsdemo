package com.ebs.dda.accounting.collection.utils;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObCollectionViewAllTestUtils extends DObCollectionTestUtils {
  public static String REQUEST_LOCALE = "en";
  private ObjectMapper objectMapper;

  public DObCollectionViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");

    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");

    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables_ViewAll.sql");
    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables.sql");
    str.append(",").append("db-scripts/accounting/collection/CObCollectionType.sql");

    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append("," + "db-scripts/accounting/payment-request/payment-request.sql");
    str.append("," + "db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
    str.append(
        "," + "db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    str.append(
        "," + "db-scripts/accounting/payment-request/iob-payment-request-accounting-details.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoicePostingDetails.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append(",").append("db-scripts/accounting/journal-entry-items.sql");
    return str.toString();
  }

  public String resetDataDependanciesBeforeInsertion(){
    String str = super.resetDataDependanciesBeforeInsertion();
    str += ","+"db-scripts/master-data/customer/clear_customer.sql";
    return str;
  }

  public void assertThatTheFollowingCollectionGeneralDataExist(DataTable generalDataTable) {

    List<Map<String, String>> collectionGeneralDataList =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionGeneralData : collectionGeneralDataList) {

      DObCollectionGeneralModel collectionGeneralModel =
          (DObCollectionGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCollectionGeneralData",
                  constructCollectionGeneralDataQueryParams(collectionGeneralData));

      Assertions.assertNotNull(
          collectionGeneralModel,
          "Collection "
              + collectionGeneralData.get(IAccountingFeatureFileCommonKeys.C_CODE)
              + " Doesn't exist...");
    }
  }

  private Object[] constructCollectionGeneralDataQueryParams(
      Map<String, String> collectionGeneralData) {
    return new Object[] {
      collectionGeneralData.get(IAccountingFeatureFileCommonKeys.C_CODE),
      "%" + collectionGeneralData.get(IFeatureFileCommonKeys.STATE) + "%",
      collectionGeneralData.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      collectionGeneralData.get(IFeatureFileCommonKeys.CREATED_BY),
      collectionGeneralData.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      collectionGeneralData.get(IAccountingFeatureFileCommonKeys.COLLECTION_TYPE),
      collectionGeneralData.get(IAccountingFeatureFileCommonKeys.COMPANY),
      collectionGeneralData.get(IAccountingFeatureFileCommonKeys.COMPANY_CURRENCY_ISO),
    };
  }

  public void assertThatCollectionDetailsExist(DataTable collectionDetailsDataTable) {

    List<Map<String, String>> collectionDetailsList =
        collectionDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionDetail : collectionDetailsList) {

      IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
          (IObCollectionDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCollectionDetails", constructCollectionDetailsQueryParams(collectionDetail));

      Assertions.assertNotNull(
          collectionDetailsGeneralModel,
          "Details for Collection with code "
              + collectionDetail.get(IAccountingFeatureFileCommonKeys.C_CODE)
              + " don't exist!");
      if (!collectionDetail.get(IAccountingFeatureFileCommonKeys.AMOUNT).isEmpty()) {
        BigDecimal expectedAmount =
               new BigDecimal(collectionDetail.get(IAccountingFeatureFileCommonKeys.AMOUNT));
        BigDecimal actualAmount = collectionDetailsGeneralModel.getAmount();
        Assertions.assertEquals(expectedAmount.compareTo(actualAmount), 0);
      } else {
        Assertions.assertNull(collectionDetailsGeneralModel.getAmount());
      }
    }
  }

  private Object[] constructCollectionDetailsQueryParams(Map<String, String> CollectionDetails) {
    return new Object[] {
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT),
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.TREASURY),
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.C_CODE),
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.COLLECTION_METHOD),
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_CODE),
      CollectionDetails.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_TYPE)
    };
  }

  public void assertThatViewAllResponseIsCorrect(DataTable collectionDataTable, Response response)
      throws Exception {

    List<HashMap<String, Object>> collection = getListOfMapsFromResponse(response);

    List<Map<String, String>> expectedCollection =
        collectionDataTable.asMaps(String.class, String.class);

    for (int i = 0; i < collection.size(); i++) {
      assertThatExpectedCollectionMathcesAcutal(expectedCollection.get(i), collection.get(i));
    }
  }

  private void assertThatExpectedCollectionMathcesAcutal(
      Map<String, String> expectedCollection, HashMap<String, Object> actualCollection)
      throws Exception {
    assertEquals(
        expectedCollection.get(IFeatureFileCommonKeys.CODE),
        actualCollection.get(IDObCollectionGeneralModel.USER_CODE));

    assertEquals(
        expectedCollection.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
        actualCollection.get(IDObCollectionGeneralModel.DOCUMENT_OWNER_ID)
            + " - "
            + actualCollection.get(IDObCollectionGeneralModel.DOCUMENT_OWNER));

    assertEquals(
        expectedCollection.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
        getEnglishLocaleFromLocalizedResponse(
                getLocalizedValue(
                    actualCollection.get(IDObCollectionGeneralModel.BUSINESS_PARTNER_TYPE)))
            + " - "
            + actualCollection.get(IDObCollectionGeneralModel.BUSINESS_PARTNER_CODE)
            + " - "
            + getEnglishLocaleFromLocalizedResponse(
                getLocalizedValue(
                    actualCollection.get(IDObCollectionGeneralModel.BUSINESS_PARTNER_NAME))));

    assertEquals(
        expectedCollection.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        actualCollection.get(IDObCollectionGeneralModel.PURCHASE_UNIT_CODE)
            + " - "
            + getEnglishLocaleFromLocalizedResponse(
                getLocalizedValue(
                    actualCollection.get(IDObCollectionGeneralModel.PURCHASE_UNIT_NAME_ATTR))));

    assertEquals(
        expectedCollection.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT),
        getEnglishLocaleFromLocalizedResponse(
                getLocalizedValue(
                    actualCollection.get(IDObCollectionGeneralModel.REF_DOCUMENT_TYPE_NAME)))
            + " - "
            + actualCollection.get(IDObCollectionGeneralModel.REF_DOCUMENT_CODE));

    assertThatExpectedAmountEqualsActual(expectedCollection.get(IAccountingFeatureFileCommonKeys.AMOUNT), actualCollection,
            expectedCollection.get(IFeatureFileCommonKeys.CODE));

    assertThatStatesArrayContainsValue(
        getStatesArray(
            String.valueOf(
                actualCollection.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME))),
        expectedCollection.get(IFeatureFileCommonKeys.STATE));
  }

  private void assertThatExpectedAmountEqualsActual(String expectedAmountWithCurrency, Map<String, Object> actualCollection, Object collectionCode) {
    BigDecimal actualAmount = new BigDecimal(actualCollection.get(IDObCollectionGeneralModel.AMOUNT).toString());
    String actualCurrency = String.valueOf(actualCollection.get(IDObCollectionGeneralModel.COMPANY_CURRENCY_ISO));
    String[] expectedAmountWithCurrencyArray = expectedAmountWithCurrency.split(" ");
    BigDecimal expectedAmount = new BigDecimal(expectedAmountWithCurrencyArray[0]);
    String expectedCurrency = expectedAmountWithCurrencyArray[1];
    assertEquals(expectedCurrency, actualCurrency);
    assertEquals(actualAmount.compareTo(expectedAmount), 0, "Actual Amount of Collection " + collectionCode + "is not equal to the Expected ");
  }

  public void assertThatTotalNumberOfRecordsIsCorrect(Response response, Integer numOfRecords) {

    List<HashMap<String, Object>> collection = getListOfMapsFromResponse(response);

    assertEquals(numOfRecords.intValue(), collection.size());
  }

  private String getLocalizedValue(Object obj) throws IOException {
    return objectMapper.writeValueAsString(obj);
  }

  public void assertThatAllCollectionsExist(Integer recordsNumber) {
    Long collectionsCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCollectionsCount");
    Assert.assertEquals(Long.valueOf(recordsNumber), collectionsCount);
  }
}
