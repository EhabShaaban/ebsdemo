package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryItemsViewAllTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostGeneralDataViewTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObLandedCostGeneralDataViewStep extends SpringBootRunner implements En {

    public static final String SCENARIO_NAME = "View GeneralData section in LandedCost";
    private static boolean hasBeenExecuted = false;
  private DObLandedCostGeneralDataViewTestUtils landedCostUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    landedCostUtils = new DObLandedCostGeneralDataViewTestUtils(properties);
    landedCostUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }


  public DObLandedCostGeneralDataViewStep() {
    Given("^the following GeneralData for LandedCosts exist:$", (DataTable generalDataTable) -> {
      landedCostUtils.checkLandedCostGeneralDataExists(generalDataTable);
    });

    When("^\"([^\"]*)\" requests to view GeneralData section of LandedCost with code \"([^\"]*)\"$",
        (String username, String LandedCostCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          StringBuilder readUrl = new StringBuilder();
          readUrl.append(IDObLandedCostRestUrls.GET_URL);
          readUrl.append("/" + LandedCostCode);
          readUrl.append(IDObLandedCostRestUrls.GENERAL_DATA);
          Response response = landedCostUtils.sendGETRequest(userCookie, String.valueOf(readUrl));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of GeneralData section for LandedCost with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesInvoiceCode, String username, DataTable generalDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          landedCostUtils.assertCorrectValuesAreDisplayedInGeneralData(response, generalDataTable);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(landedCostUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(DObLandedCostCommonTestUtils.clearLandedCosts());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
