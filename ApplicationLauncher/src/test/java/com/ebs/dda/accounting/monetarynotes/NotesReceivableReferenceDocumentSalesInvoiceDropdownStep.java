package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.NotesReceivableReferenceDocumentSalesInvoiceDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class NotesReceivableReferenceDocumentSalesInvoiceDropdownStep extends SpringBootRunner
    implements En {

  public static final String SCENARIO_NAME = "Read list of SalesInvoice - ReferenceDocument";
  private NotesReceivableReferenceDocumentSalesInvoiceDropdownTestUtils utils;

  public NotesReceivableReferenceDocumentSalesInvoiceDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all SalesInvoice - ReferenceDocument for NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String notesReceivableCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.readAllReferenceDocumentSalesInvoices(cookie, notesReceivableCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesInvoice - ReferenceDocument values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesInvoicesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReferenceDocumentSalesInvoicesResponseIsCorrect(
              salesInvoicesDataTable, response);
        });

    Then(
        "^total number of SalesInvoice - ReferenceDocument returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer salesInvoiceCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfReferenceDocumentSalesInvoicesEquals(
              salesInvoiceCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new NotesReceivableReferenceDocumentSalesInvoiceDropdownTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
