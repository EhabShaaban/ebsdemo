package com.ebs.dda.accounting.paymentrequest.utils;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;
import static org.junit.Assert.assertEquals;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPostingDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;

public class DObPaymentRequestViewPostingDetailsTestUtils
				extends DObPaymentRequestPaymentDetailsTestUtils {
	public DObPaymentRequestViewPostingDetailsTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public static String getPaymentRequestDbScripts() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/clearDataSqlScript.sql");
		str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
		str.append("," + "db-scripts/CObBankSqlScript.sql");
		str.append("," + "db-scripts/CObCurrencySqlScript.sql");
		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
		str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
		str.append(",").append("db-scripts/master-data/item/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
		str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
		str.append("," + "db-scripts/CObMeasureScript.sql");
		str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
		str.append("," + "db-scripts/master-data/item/CObItem.sql");
		str.append("," + "db-scripts/master-data/item/MObItem.sql");
		str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
		str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
		str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
		str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
		str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
		str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
		str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
		str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
		str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
		return str.toString();
	}

	public void assertThatTheFollowingPRActivationDetailsExist(DataTable activationDetailsTable) {
		List<Map<String, String>> prActivationDetailsListMap = activationDetailsTable
						.asMaps(String.class, String.class);
		for (Map<String, String> prActivationDetailsMap : prActivationDetailsListMap) {

			IObAccountingDocumentActivationDetailsGeneralModel prActivationDetails = getIObActivationDetailsGeneralModel(
							prActivationDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE));

			assertAccountantEquals(prActivationDetailsMap, prActivationDetails);
			assertActivationDateEquals(prActivationDetailsMap, prActivationDetails);
			assertCurrencyPriceEquals(prActivationDetailsMap, prActivationDetails);
			assertJournalEntryCodeEquals(prActivationDetailsMap, prActivationDetails);

			Assert.assertNotNull(
							"PR Activation Details doesn't exist for" + prActivationDetailsMap
											.get(IAccountingFeatureFileCommonKeys.PR_CODE),
							prActivationDetails);
		}
	}

	private void assertJournalEntryCodeEquals(Map<String, String> prpostingDetailsMap,
					IObAccountingDocumentActivationDetailsGeneralModel prpostingDetails) {
		assertEquals(prpostingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
						prpostingDetails.getJournalEntryCode());
	}

	private void assertCurrencyPriceEquals(Map<String, String> prPostingDetailsMap,
					IObAccountingDocumentActivationDetailsGeneralModel prPostingDetails) {
		assertEquals(prPostingDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
						prPostingDetails.getCurrencyPrice().toString());
	}

	private void assertActivationDateEquals(Map<String, String> prPostingDetailsMap,
					IObAccountingDocumentActivationDetailsGeneralModel prPostingDetails) {
		assertDateEquals(prPostingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
						prPostingDetails.getActivationDate().withZone(DateTimeZone.UTC));
	}

	private void assertAccountantEquals(Map<String, String> prpostingDetailsMap,
					IObAccountingDocumentActivationDetailsGeneralModel prpostingDetails) {
		assertEquals(prpostingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
						prpostingDetails.getAccountant());
	}

	private IObAccountingDocumentActivationDetailsGeneralModel getIObActivationDetailsGeneralModel(
					String prCode) {
		return (IObAccountingDocumentActivationDetailsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getPRPostingDetails", prCode);
	}

	public String getViewPostingDetailsUrl(String paymentRequestCode) {
		return IDObPaymentRequestRestURLs.BASE_URL + "/" + paymentRequestCode
						+ IDObPaymentRequestRestURLs.VIEW_POSTING_DETAILS;
	}

	public void assertCorrectValuesAreDisplayedInPostingDetails(Response response,
					DataTable postingDetailsTable) {
		Map<String, String> postDetailsExpected = postingDetailsTable
						.asMaps(String.class, String.class).get(0);
		Map<String, Object> postDetailsActual = response.body().jsonPath().getMap(CommonKeys.DATA);

		assertPostingDetailsDataEquals(postDetailsExpected, postDetailsActual);
	}

	private void assertPostingDetailsDataEquals(Map<String, String> postDetailsExpected,
					Map<String, Object> postDetailsActual) {
		Assert.assertEquals(postDetailsExpected.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
						postDetailsActual.get(
										IIObPaymentRequestPostingDetailsGeneralModel.ACCOUNTANT));

		assertDateEquals(postDetailsExpected.get(IFeatureFileCommonKeys.POSTING_DATE),
						formatDate(String.valueOf(postDetailsActual.get(
										IIObPaymentRequestPostingDetailsGeneralModel.ACTIVATION_DATE))));

		assertEquals(postDetailsExpected.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
						postDetailsActual.get(
										IIObPaymentRequestPostingDetailsGeneralModel.CURRENCY_PRICE)
										.toString());

		assertEquals(postDetailsExpected.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
						postDetailsActual.get(
										IIObPaymentRequestPostingDetailsGeneralModel.JOURNAL_ENTRY_CODE));
	}

	public void assertThatTheFollowingPaymentRequestExist(DataTable paymentRequestDataTable) {
		List<Map<String, String>> paymentRequestListMap = paymentRequestDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> paymentRequestMap : paymentRequestListMap) {
			DObPaymentRequest paymentRequest = (DObPaymentRequest) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getPaymentRequest",
											constructPaymentRequestQueryParams(paymentRequestMap));
			Assert.assertNotNull(
							"Payment Request doesn't exists for "
											+ paymentRequestMap.get(IFeatureFileCommonKeys.CODE),
							paymentRequest);
		}
	}

	private Object[] constructPaymentRequestQueryParams(Map<String, String> paymentRequestMap) {
		return new Object[] { paymentRequestMap.get(IFeatureFileCommonKeys.CODE),
						paymentRequestMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
						"%" + paymentRequestMap.get(IFeatureFileCommonKeys.STATE) + "%",
						paymentRequestMap.get(IFeatureFileCommonKeys.TYPE) };
	}

	private DateTime formatDate(String date) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
		return formatter.parseDateTime(date).withZone(DateTimeZone.UTC);
	}

	public void insertPaymentRequestPostingDetails(DataTable postingDetailsDataTable) {
		List<Map<String, String>> postingDetailsMaps = postingDetailsDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> postingDetailsMap : postingDetailsMaps) {
			entityManagerDatabaseConnector.executeInsertQuery("insertPaymentRequestPostingDetails",
							constructPaymentRequestPostingDetailsInsertionParams(
											postingDetailsMap));
		}
	}

	private Object[] constructPaymentRequestPostingDetailsInsertionParams(
					Map<String, String> postingDetailsMap) {
		String[] currencyPrice = postingDetailsMap
						.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE).split(" = ");
		String[] leftHandSide = currencyPrice[0].split(" ");
		String[] rightHandSide = currencyPrice[1].split(" ");
		String companyCurrencyIso = leftHandSide[1];
		String value = rightHandSide[0];
		String documentCurrencyIso = rightHandSide[1];

		return new Object[] { postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
						companyCurrencyIso, documentCurrencyIso, new BigDecimal(value),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),

		};
	}
}
