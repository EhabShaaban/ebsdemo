package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceDeleteItemTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceRequestAddItemTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceItemsViewTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class IObSalesInvoiceDeleteItemStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private IObSalesInvoiceDeleteItemTestUtils iObSalesInvoiceDeleteItemTestUtils;
    private IObSalesInvoiceRequestAddItemTestUtils iObSalesInvoiceRequestAddItemTestUtils;
    private IObSalesInvoiceItemsViewTestUtils salesInvoiceViewItemsTestUtils;

    public IObSalesInvoiceDeleteItemStep() {

        When(
                "^\"([^\"]*)\" requests to delete Item with id \"([^\"]*)\" in SalesInvoice with code \"([^\"]*)\"$",
                (String userName, String itemId, String salesInvoiceCode) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response itemDeleteResponse =
                            iObSalesInvoiceDeleteItemTestUtils.sendDeleteRequest(
                                    cookie,
                                    iObSalesInvoiceDeleteItemTestUtils.getDeleteItemUrl(salesInvoiceCode, itemId));
                    userActionsTestUtils.setUserResponse(userName, itemDeleteResponse);
                });

        Then(
                "^Item with id \"([^\"]*)\" from SalesInvoice with code \"([^\"]*)\" is deleted$",
                (String itemId, String salesInvoiceCode) -> {
                    iObSalesInvoiceDeleteItemTestUtils.assertNoItemExistsById(itemId, salesInvoiceCode);
                });
        Given(
                "^\"([^\"]*)\" first deleted Item with id \"([^\"]*)\" of SalesInvoice with code \"([^\"]*)\" successfully$",
                (String userName, String itemId, String salesInvoiceCode) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response itemDeleteResponse =
                            iObSalesInvoiceDeleteItemTestUtils.sendDeleteRequest(
                                    cookie,
                                    iObSalesInvoiceDeleteItemTestUtils.getDeleteItemUrl(salesInvoiceCode, itemId));
                    userActionsTestUtils.setUserResponse(userName, itemDeleteResponse);
                });

        Given(
                "^first \"([^\"]*)\" opens SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" in edit mode$",
                (String userName, String salesInvoiceCode) -> {
                    String lockUrl = iObSalesInvoiceRequestAddItemTestUtils.getLockUrl(salesInvoiceCode);
                    Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesInvoiceCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        Map<String, Object> properties = getProperties();
        iObSalesInvoiceDeleteItemTestUtils =
                new IObSalesInvoiceDeleteItemTestUtils(properties, entityManagerDatabaseConnector);
        iObSalesInvoiceRequestAddItemTestUtils =
                new IObSalesInvoiceRequestAddItemTestUtils(properties, entityManagerDatabaseConnector);
        salesInvoiceViewItemsTestUtils = new IObSalesInvoiceItemsViewTestUtils(properties);
        salesInvoiceViewItemsTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
        if (contains(scenario.getName(), "Delete Item from SalesInvoice")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(
                        iObSalesInvoiceDeleteItemTestUtils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
            databaseConnector.executeSQLScript(iObSalesInvoiceDeleteItemTestUtils.getDbScripts());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Delete Item from SalesInvoice")) {
            userActionsTestUtils.unlockAllSections(
               IDObSalesInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS, iObSalesInvoiceDeleteItemTestUtils.getSectionsNames());
            databaseConnector.closeConnection();
        }
    }
}
