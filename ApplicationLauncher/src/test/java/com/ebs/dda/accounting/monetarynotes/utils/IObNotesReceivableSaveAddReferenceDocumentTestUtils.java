package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObMonetaryNotesReferenceDocumentsValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class IObNotesReceivableSaveAddReferenceDocumentTestUtils
    extends DObMonetaryNotesCommonTestUtils {

  public IObNotesReceivableSaveAddReferenceDocumentTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/SalesInvoice_Clear.sql");
    return str.toString();
  }

  public Response saveAddReferenceDocument(
      Cookie cookie, String notesReceivableCode, DataTable newReferenceDocumentDataTable) {
    JsonObject newReferenceDocumentJsonObject =
        constructAddReferenceDocumentValueObject(newReferenceDocumentDataTable);
    return sendPUTRequest(
        cookie,
        getSaveNewReferenceDocumentUrl(notesReceivableCode),
        newReferenceDocumentJsonObject);
  }

  private JsonObject constructAddReferenceDocumentValueObject(
      DataTable newReferenceDocumentDataTable) {
    JsonObject newReferenceDocumentValueObject = new JsonObject();

    Map<String, String> newReferenceDocumentMap =
        newReferenceDocumentDataTable.asMaps(String.class, String.class).get(0);

    addValueToJsonObject(
        newReferenceDocumentValueObject,
        IIObMonetaryNotesReferenceDocumentsValueObject.DOCUMENT_TYPE,
        newReferenceDocumentMap.get(IAccountingFeatureFileCommonKeys.DOCUMENT_TYPE));
    addValueToJsonObject(
        newReferenceDocumentValueObject,
        IIObMonetaryNotesReferenceDocumentsValueObject.REF_DOCUMENT_CODE,
        newReferenceDocumentMap.get(IAccountingFeatureFileCommonKeys.DOCUMENT_CODE));
    addBigDecimalValueToJsonObject(
        newReferenceDocumentValueObject,
        IIObMonetaryNotesReferenceDocumentsValueObject.AMOUNT_TO_COLLECT,
        newReferenceDocumentMap.get(IAccountingFeatureFileCommonKeys.AMOUNT_TO_COLLECT));

    return newReferenceDocumentValueObject;
  }

  private String getSaveNewReferenceDocumentUrl(String notesReceivableCode) {
    StringBuilder url = new StringBuilder();
    url.append(IDObNotesReceivableRestURLs.BASE_COMMAND_URL)
        .append("/")
        .append(notesReceivableCode)
        .append(IDObNotesReceivableRestURLs.UPDATE_REFERENCE_DOCUMENTS);
    return url.toString();
  }
}
