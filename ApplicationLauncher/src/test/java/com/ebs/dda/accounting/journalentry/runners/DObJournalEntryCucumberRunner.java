package com.ebs.dda.accounting.journalentry.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {
                "classpath:features/accounting/journal-entry/JE_ViewAll.feature",
                "classpath:features/accounting/journal-entry/JE_Items_ViewAll.feature"
        },
        glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.journalentry"},
        strict = true,
        tags = "not @Future")
public class DObJournalEntryCucumberRunner {
}
