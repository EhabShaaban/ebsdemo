package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewAllTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestViewAllStep extends SpringBootRunner implements En, InitializingBean {

  public static final String SCENARIO_NAME = "View All PaymentRequests";
  private DObPaymentRequestViewAllTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestViewAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObPaymentRequestViewAllStep() {

    Given(
        "^ONLY the following ViewAll data for PaymentRequests exist:$",
        (DataTable paymentRequestDataTable) -> {
          this.utils.assertThatTheFollowingPaymentRequestExist(paymentRequestDataTable);
        });
    Given(
        "^the total number of existing records are (\\d+)$",
        (Integer recordsCount) -> {
          this.utils.assertTotalNumberOfItemsExist(recordsCount);
        });
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              this.utils.requestToReadFilteredData(
                  userCookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following PaymentRequests will be presented to \"([^\"]*)\":$",
        (String userName, DataTable paymentRequestDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          this.utils.assertResponseSuccessStatus(response);
          this.utils.assertPaymentRequestDataIsCorrect(paymentRequestDataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          this.utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getContainsFilterField(
                  IDObPaymentRequestGeneralModel.USER_CODE, userCodeContains);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String stateContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getContainsFilterField(
                  IDObPaymentRequestGeneralModel.STATE, stateContains);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String type, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getEqualsFilterField(IDObPaymentRequestGeneralModel.PAYMENT_TYPE, type);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on BusinessPartner which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String businessPartner, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getEqualsFilterField(
                  IDObPaymentRequestGeneralModel.BUSINESS_PARTNER_CODE, businessPartner);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on CreatedBy which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String createdBy, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getContainsFilterField(
                  IDObPaymentRequestGeneralModel.CREATION_INFO, createdBy);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on ReferenceDocument which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String username,
            Integer pageNumber,
            String referenceDocument,
            Integer pageSize,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getContainsFilterField(
                  IDObPaymentRequestGeneralModel.REFERENCE_DOCUMENT, referenceDocument);
          Response response =
              this.utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObPaymentRequestRestURLs.VIEW_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on Amount which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String amount, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getEqualsFilterField(IDObPaymentRequestGeneralModel.AMOUNT, amount);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on Currency which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String currency, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getEqualsFilterField(IDObPaymentRequestGeneralModel.AMOUNT, currency);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String businessUnit, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getEqualsFilterField(
                  IDObPaymentRequestGeneralModel.PURCHASE_UNIT_CODE, businessUnit);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });
    Then(
        "^no values are displayed to \"([^\"]*)\" and empty grid state is viewed in Home Screen$",
        (String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsReturned(response, 0);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on BankTransRef which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String bankTransRef, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              this.utils.getContainsFilterField(
                  IDObPaymentRequestGeneralModel.BANK_TRANS_REF, bankTransRef);
          Response response =
              this.utils.requestToReadFilteredData(
                  cookie, IDObPaymentRequestRestURLs.VIEW_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PaymentRequests with filter applied on ExpectedDueDate from \"([^\"]*)\" to \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String fromDate, String toDate, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String expectedDueDateFilter =
              utils.getBetweenFilterField(
                  IDObPaymentRequestGeneralModel.FROM_EXPECTED_DUE_DATE,
                  String.valueOf(utils.getDateTimeInMilliSecondsFormat(fromDate)),
                      IDObPaymentRequestGeneralModel.TO_EXPECTED_DUE_DATE,
                  String.valueOf(utils.getDateTimeInMilliSecondsFormat(toDate)));
          Response response =
              utils.requestToReadFilteredData(
                  cookie,
                  IDObPaymentRequestRestURLs.VIEW_ALL_URL,
                  pageNumber,
                  pageSize,
                  expectedDueDateFilter);

          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Before
  public void beforeViewAll(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          DObPaymentRequestViewAllTestUtils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(DObPaymentRequestViewAllTestUtils.clearPaymentRequests());
      databaseConnector.executeSQLScript(DObVendorInvoiceTestUtils.clearVendorInvoices());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
