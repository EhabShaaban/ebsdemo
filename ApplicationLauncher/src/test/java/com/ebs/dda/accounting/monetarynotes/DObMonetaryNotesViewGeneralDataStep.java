package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObMonetaryNotesViewGeneralDataStep extends SpringBootRunner implements En {

  DObMonetaryNotesViewGeneralDataTestUtils utils;

  public DObMonetaryNotesViewGeneralDataStep() {

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of MonetaryNotes with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readGeneralData(cookie, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of GeneralData section for MonetaryNotes with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String monetaryNoteCode, String userName, DataTable generalDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertMonetaryNoteGeneralDataIsCorrect(
              monetaryNoteCode, response, generalDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObMonetaryNotesViewGeneralDataTestUtils(getProperties(),entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View MonetaryNotes GeneralData section")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }
}
