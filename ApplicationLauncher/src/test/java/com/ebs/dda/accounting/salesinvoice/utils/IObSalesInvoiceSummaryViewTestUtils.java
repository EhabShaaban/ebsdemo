package com.ebs.dda.accounting.salesinvoice.utils;

import java.util.Map;

public class IObSalesInvoiceSummaryViewTestUtils extends DObSalesInvoiceTestUtils {

  public IObSalesInvoiceSummaryViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsEveryTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    return str.toString();
  }
}
