package com.ebs.dda.accounting.paymentrequest.utils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

public class IObPaymentRequestViewAccountingDetailsTestUtils
    extends DObPaymentRequestAccountingDetailsTestUtils {

  public IObPaymentRequestViewAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
    this.urlPrefix = (String) getProperty(URL_PREFIX);
  }

  public static String getPaymentRequestDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    return str.toString();
  }

    public void assertThatAccountingDetailsDataMatches(DataTable AccountingDetailsTable,
      Response response) throws Exception {
    List<Map<String, String>> expectedAccountingDetailsList =
        AccountingDetailsTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualAccountingDetailsList = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedAccountingDetailsList.size(); i++) {
      MapAssertion mapAssertion = new MapAssertion(expectedAccountingDetailsList.get(i),
          actualAccountingDetailsList.get(i));
      assertThatViewAccountingDetailsResponseIsCorrect(mapAssertion);
    }
  }

  private void assertThatViewAccountingDetailsResponseIsCorrect(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(IAccountingFeatureFileCommonKeys.GL_ACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_NAME);

    mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNTING_ENTRY);

    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.AMOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.AMOUNT);
  }

  public String getViewAccountingDetailsUrl(String code) {
    return IDObPaymentRequestRestURLs.BASE_URL + "/" + code
        + IDObPaymentRequestRestURLs.VIEW_ACCOUNTING_DETAILS;
  }

  public void insertPaymentRequestAccountingDetails(DataTable accountingDetailsTable) {
    List<Map<String, String>> accountingDetailsMaps =
            accountingDetailsTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingDetailsMap : accountingDetailsMaps) {

      entityManagerDatabaseConnector.executeInsertQuery("insertPaymentRequestAccountingDetails",
              constructPRAccountingDetailsInsertionParams(accountingDetailsMap));

      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("insertPaymentRequestAccountingDetailsSubAccounts",
              constructPRAccountingDetailsSubAccountsInsertionParams(accountingDetailsMap));

    }
  }

  private Object[] constructPRAccountingDetailsInsertionParams(Map<String, String> accountingDetailsMap) {
    return new Object[]{
            accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
            accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
            accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
            accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
            new BigDecimal(accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.VALUE))
    };
  }
  private Object[] constructPRAccountingDetailsSubAccountsInsertionParams(Map<String, String> accountingDetailsMap) {
    return new Object[]{
            accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
            accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT)
    };
  }
}
