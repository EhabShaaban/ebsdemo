package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.IObMonetaryNoteEditNoteDetailsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class IObMonetaryNoteEditNoteDetailsStep extends SpringBootRunner implements En {
  private IObMonetaryNoteEditNoteDetailsTestUtils utils;

  public IObMonetaryNoteEditNoteDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to edit NotesDetails section of NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          String lockUrl = utils.getLockNoteDetailsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^NotesDetails section of NotesReceivable with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String monetaryNoteCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName,
              monetaryNoteCode,
              IDObMonetaryNotesSectionNames.NOTES_DETAILS_SECTION,
              utils.NOTES_RECEIVABLE_JMX_LOCK_BEAN_NAME);
        });

    Given(
        "^\"([^\"]*)\" first opened the NotesDetails section of NotesReceivable with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String monetaryNoteCode) -> {
          String lockUrl = utils.getLockNoteDetailsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, monetaryNoteCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^NotesDetails section of NotesReceivable with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String monetaryNoteCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = utils.getLockNoteDetailsUrl(monetaryNoteCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving NotesDetails section of NotesReceivable with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unLockUrl = utils.getUnLockNoteDetailsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^the lock by \"([^\"]*)\" on NotesDetails section of NotesReceivable with code \"([^\"]*)\" is released$",
        (String userName, String monetaryNoteCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              monetaryNoteCode,
              utils.NOTES_RECEIVABLE_JMX_LOCK_BEAN_NAME,
              IDObMonetaryNotesSectionNames.NOTES_DETAILS_SECTION);
        });
    When(
        "^\"([^\"]*)\" cancels saving NotesDetails section of NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          String unLockUrl = utils.getUnLockNoteDetailsUrl(monetaryNoteCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObMonetaryNoteEditNoteDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit NotesDetails section")
        || contains(scenario.getName(), "Request to cancel saving NotesDetails section")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit NotesDetails section")
        || contains(scenario.getName(), "Request to cancel saving NotesDetails section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObNotesReceivableRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
