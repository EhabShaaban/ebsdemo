package com.ebs.dda.accounting;

public interface IAccountingFeatureFileCommonKeys {

  String VENDOR = "Vendor";
  String PURCHESE_ORDER = "PurchaseOrder";
  String INVOICE_NUMBER = "InvoiceNumber";
  String COMPANY = "Company";
  String COMPANY_CODE = "CompanyCode";
  String COMPANY_NAME = "CompanyName";
  String CURRENCY = "Currency";
  String DOCUMENT_CURRENCY = "Currency(Document)";
  String COMPANY_CURRENCY = "Currency(Company)";
  String COMPANY_GROUP_CURRENCY = "Currency(Group)";
  String PR_CODE = "PRCode";
  String PAYMENT_TERMS = "PaymentTerms";
  String PAYMENT_TERM = "PaymentTerm";
  String PAYMENT_TYPE = "PaymentType";
  String PAYMENT_FORM = "PaymentForm";
  String PURCHASE_UNIT = "PurchaseUnit";
  String AMOUNT = "Amount";
  String AMOUNT_VALUE = "amount";
  String METHOD = "Method";

  String NOTE_TYPE = "NoteType";
  String NOTE_FORM = "NoteForm";
  String PARTNER_TYPE = "PartnerType";
  String BUSINESS_PARTNER_CODE = "BusinessPartnerCode";
  String CURRENCY_CODE = "CurrencyCode";
  String BUSINESS_PARTNER_NAME = "BusinessPartnerName";
  String CURRENCY_ISO = "CurrencyISO";
  String DOCUMENT_CURRENCY_ISO = "DocumentCurrencyISO";

  String ACCOUNT_CODE = "AccountCode";
  String ACCOUNT_NAME = "AccountName";
  String ACCOUNT_PARENT = "AccountParent";
  String ACCOUNT_TYPE = "AccountType";
  String OLD_ACCOUNT_CODE = "OldAccountCode";
  String MAPPED_ACCOUNT = "MappedAccount";
  String LEVEL = "Level";
  String CREDIT_DEBIT = "Credit/Debit";
  String PARENT_CODE_NAME = "ParentCodeName";
  String PARENT_CODE = "ParentCode";
  String Leadger = "Leadger";

  String PR_PARTY = "Party";
  String PR_DUE_DOCUMENT_TYPE = "DueDocumentType";
  String PR_DUE_DOCUMENT = "DueDocument";
  String PR_NET_AMOUNT = "NetAmount";
  String CURRENCY_NAME = "CurrencyName";
  String PR_DESCRIPTION = "Description";
  String DOWN_PAYMENT = "DownPayment";
  String INVOICE_DATE = "InvoiceDate";
  String COMPANY_LOCAL_CURRENCY_ISO = "CompanyLocalCurrencyISO";
  String COST_FACTOR_ITEM = "CostFactorItem";

  // VendorInvoiceItemsKeys

  String ITEM = "Item";
  String ITEM_CODE = "ItemCode";
  String ITEM_ID = "ItemId";
  String ITEM_TYPE = "ItemType";
  String ITEM_NAME = "ItemName";
  String ITEM_QUANTITY_ORDER = "QtyOrder";
  String ITEM_ORDER_UNIT = "OrderUnit";
  String ITEM_ORDER_UNIT_Symbol = "OrderUnitSymbol";
  String ITEM_BASE_UNIT = "BaseUnit";
  String ITEM_BASE_UNIT_Symbol = "BaseUnitSymbol";
  String ITEM_UNIT_PRICE_BASE = "UnitPrice(Base)";
  String ITEM_UNIT_PRICE_ORDER_UNIT = "UnitPrice(OrderUnit)";
  String ITEM_SALES_PRICE = "SalesPrice";
  String QTY = "Qty";

  String SUB_ACCOUNT_CODE = "SubAccountCode";
  String SUB_ACCOUNT_NAME = "SubAccountName";
  String BANK_ACCOUNT = "BankAccount";
  String BANK_ACCOUNT_NUMBER = "BankAccountNumber";
  String SECOND_CURRENCY_CODE = "SecondCurrencyCode";
  String PRICE = "Price";

  String ACCOUNTANT = "Accountant";
  String POSTING_DATE = "PostingDate";
  String DUE_DATE = "DueDate";
  String JOURNAL_DATE = "JournalDate";
  String PR_LATEST_EXCHANGE_RATE_CODE = "LatestExchangeRateCode";
  String CURRENCY_PRICE = "CurrencyPrice";
  String EX_RATE_TO_CURRENCY_COMPANY = "ExRateToCurrency(Company)";
  String EX_RATE_TO_CURRENCY_COMPANY_GROUP = "ExRateToCurrency(Group)";
  String CURRENCY_PRICE_ESTIMATE_TIME = "CurrencyPriceEstimateTime";
  String CURRENCY_PRICE_ACTUAL_TIME = "CurrencyPriceActualTime";
  String EXCHANGE_RATE_CODE = "ExchangeRateCode";
  String JOURNAL_ENTRY_CODE = "JournalEntryCode";
  String ACCOUNT = "Account";
  String SUB_ACCOUNT = "SubAccount";
  String DEBIT = "Debit";
  String CREDIT = "Credit";
  String SUBLEDGER = "Subledger";
  String CREDIT_ACCOUNT = "CreditAccount";
  String CREDIT_SUBACCOUNT = "CreditSubAccount";
  String DEBIT_ACCOUNT = "DebitAccount";
  String DEBIT_SUBACCOUNT = "DebitSubAccount";
  String TOTAL_DEBIT = "TotalDebit";
  String TOTAL_CREDIT = "TotalCredit";

  String REFERENCE_DOCUMENT_COMPANY = "ReferenceDocumentCompany";
  String REFERENCE_DOCUMENT_NAME = "ReferenceDocumentName";
  String REFERENCE_DOCUMENT_CODE = "ReferenceDocumentCode";
  String DOCUMENT_CODE = "DocumentCode";
  String DOCUMENT_TYPE = "DocumentType";
  String MONETARY_NOTE_REF_DOCUMENT_ID = "MonetaryNoteRefDocumentId";
  String AMOUNT_TO_COLLECT = "AmountToCollect";
  String REFERENCE_DOCUMENT_TYPE = "ReferenceDocumentType";
  String REFERENCE_DOCUMENT = "ReferenceDocument";
  String PURCHASE_ORDER = "PurchaseOrder";
  String AMOUNT_REMAINING = "AmountRemaining";
  String AMOUNT_CURRENCY_ISO = "AmountCurrencyISO";
  String COMPANY_CURRENCY_ISO = "CompanyLocalCurrencyISO";
  String GL_ACCOUNT = "GLAccount";
  String GL_SUBACCOUNT = "GLSubAccount";
  String GL_ACCOUNT_CONFIG = "GLAccountConfig";
  String REFERENCE = "Reference";

  String NR_TYPE = "NotesReceivableType";
  String CUSTOMER = "Customer";
  String REF_DOCUMENT_TYPE = "RefDocumentType";
  String REF_DOCUMENT = "RefDocument";
  String NOTES_INFORMATION = "NotesInformation";
  String NOTE_NUMBER = "NoteNumber";
  String NOTE_BANK = "NoteBank";
  String DEPOT = "Depot";
  String ISSUING_BANK = "IssuingBank";
  String REMAINING_AMOUNT = "RemainingAmount";
  String NR_CODE = "NRCode";

  String C_CODE = "CCode";
  String COLLECTION_TYPE = "CollectionType";
  String BUSINESS_UNIT = "BusinessUnit";
  String CR_AMOUNT = "Amount";
  String COLLECTION_METHOD = "Method";
  String CR_REF_DOCUMENT_TYPE = "RefDocumentType";
  String CR_REF_DOCUMENT = "RefDocument";
  String SOURCE = "Source";
  String JOURNAL_DUE_DATE = "JournalDueDate";
  String JOURNAL_ENTRY_DATE = "JournalEntryDate";
  String POSTED_BY = "PostedBy";
  String BANK = "Bank";
  String COSTING_CODE = "CostingCode";
  String ITEM_TOTAL_AMOUNT = "ItemTotalAmount";
  String TOTAL_AMOUNT = "TotalAmount";

  String FIRST_CURRENCY = "FirstCurrency";
  String SECOND_CURRENCY = "SecondCurrency";
  String SECOND_VALUE = "SecondValue";
  String VALID_FROM = "ValidFrom";
  String GR_CODE = "GRCode";
  String TO_BE_CONSIDERED_IN_COSTING = "ToBeConsideredinCosting";
  String ITEM_NET_COST_PER_BASE_UNIT = "ItemNetCostPerBaseUnit";
  String PO_CODE = "POCode";

  String START_DATE = "StartDate";
  String END_DATE = "EndDate";
  String CONTROL_POINT = "ControlPoint";
  String NOTES = "Notes";
  String TOTAL_PAID = "TotalPaid";
  String REMAINING = "Remaining";
  String COMPANY_BANK_ID = "CompanyBankId";
  String TAX = "Tax";
  String TAX_NAME = "TaxName";
  String TAX_PERCENTAGE = "TaxPercentage";
  String TAX_AMOUNT = "TaxAmount";
  String TOTAL_AMOUNT_BEFORE_TAXES = "TotalAmountBeforeTaxes";
  String NET_AMOUNT = "NetAmount";
  String TOTAL_REMAINING = "TotalRemaining";
  String COST_FACTOR = "CostFactor";
  String COST_FACTOR_FIELD = "costFactor";
  String SO_CODE = "SOCode";

  String LC_CODE = "LCCode";
  String LC_TYPE = "LCType";
  String LC_ITEM_ID = "LCItemId";
  String ESTIMATE_COST_CURRENCY_PRICE = "EstimateCostCurrencyPrice";
  String ESTIMATE_COST_INVOICE_TOTAL_VALUE = "EstimateCostTotalValue";
  String ACTUAL_COST_CURRENCY_PRICE = "ActualCostCurrencyPrice";
  String ACTUAL_COST_INVOICE_TOTAL_VALUE = "ActualCostTotalValue";
  String GOODS_INVOICE_CURRENCY = "GoodsInvoiceCurrency";
  String GOODS_INVOICE = "GoodsInvoice";
  String DAMAGE_STOCK = "DamageStock";

  String SALES_INVOICE_TYPES = "SalesInvoiceTypes";
  String LANDED_COST_TYPES = "LandedCostTypes";

  String ESTIMATED_VALUE = "EstimatedValue";
  String ACTUAL_VALUE = "ActualValue";
  String DIFFERENCE = "Difference";

  String ESTIMATE_TOTAL_AMOUNT = "EstimateTotalAmount";
  String ACTUAL_TOTAL_AMOUNT = "ActualTotalAmount";
  String DIFFERENCE_TOTAL_AMOUNT = "DifferenceTotalAmount";

  String DAMAGE_STOCK_INCLUDED_IN_COST = "DamageStockIncludedInCost";
  String VALUE = "Value";
  String BUSINESS_PARTNER = "BusinessPartner";
  String COLLECTION_RESPONSIBLE = "CollectionResponsible";
  String TREASURY = "Treasury";
  String ITEM_WEIGHT = "ItemWeight";
  String LANDED_COST_TOTAL = "LandedCostTotal";

  String SERVICE_PURCHASE_ORDER = "ServicePurchaseOrder";
  String GOODS_PURCHASE_ORDER = "GoodsPurchaseOrder";
  String ITEM_FINAL_COST = "ItemFinalCost";

  String BUSINESS_PARTNER_TYPE = "BusinessPartnerType";
  String SETTLEMENT_CODE = "SettlementCode";
  String SETTLEMENT_TYPE = "SettlementType";
  String FISCAL_PERIOD = "FiscalPeriod";
  String FISCAL_YEAR = "FiscalYear";

  String GL_CONFIGURATION_CODE = "GLConfigurationCode";
  String GL_CONFIGURATION_ACCOUNT = "GLConfigurationAccount";
  String DETAIL_ID = "DetailId";

  String EXPECTED_DUE_DATE = "ExpectedDueDate";
  String BANK_TRANS_REF = "BankTransRef";
  String DOCUMENT_INFORMATION = "DocumentInformation";
  String COLLECTION_DUE_DATE = "CollectionDueDate";
  String COSTING_EXCHANGE_RATE_ESTIMATE_TIME = "CostingExchangeRateEstimateTime";
  String COSTING_EXCHANGE_RATE_ACTUAL_TIME = "CostingExchangeRateActualTime";
  String WEIGHT_PERCENTAGE = "WeightPercentage";
  String ESTIMATE_UNIT_LANDED_COST= "UnitLandedCost(Estimate)";
  String ACTUAL_UNIT_LANDED_COST= "UnitLandedCost(Actual)";
  String ESTIMATE_ITEM_TOTAL_COST = "ItemTotalCost(Estimate)";
  String ACTUAL_ITEM_TOTAL_COST= "ItemTotalCost(Actual)";
  String UNIT_PRICE_IN_LOCAL_CURRENCY="UnitPriceInLocalCurrency";
  String ESTIMATE_UNIT_PRICE_IN_LOCAL_CURRENCY = "UnitPriceInLocalCurrency(Estimate)";
  String ACTUAL_UNIT_PRICE_IN_LOCAL_CURRENCY = "UnitPriceInLocalCurrency(Actual)";
  String ESTIMATE_UNIT_PRICE = "UnitPrice(Estimate)";
  String ACTUAL_UNIT_PRICE = "UnitPrice(Actual)";
}
