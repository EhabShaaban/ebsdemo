package com.ebs.dda.accounting.settlement;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.settlement.utils.DObSettlementViewAllTestUtils;
import com.ebs.dda.inventory.goodsreceipt.IDObGoodsReceiptRestUrls;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlementGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementViewAllStep extends SpringBootRunner implements En {

	public static final String SCENARIO_NAME = "View All Settlements";
	private DObSettlementViewAllTestUtils utils;
	private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
	private static boolean hasBeenExecuted = false;

	public DObSettlementViewAllStep() {

		Given("^the total number of existing records are (\\d+)$", (Integer recordsCount) -> {
			this.utils.assertTotalNumberOfItemsExist(recordsCount);
		});
		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with no filter applied with (\\d+) records per page$",
						(String userName, Integer pageNum, Integer recordsNum) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							Response response = this.utils.requestToReadFilteredData(userCookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNum,
											recordsNum, "");
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^the following Settlements will be presented to \"([^\"]*)\":$",
						(String userName, DataTable settlementDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							this.utils.assertResponseSuccessStatus(response);
							this.utils.assertSettlementDataIsCorrect(settlementDataTable, response);
						});

		Then("^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
						(String userName, Integer totalRecordsNumber) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							this.utils.assertTotalNumberOfRecordsAreCorrect(response,
											totalRecordsNumber);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String userCodeContains,
										Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filter = this.utils.getContainsFilterField(
											IDObSettlementGeneralModel.USER_CODE, userCodeContains);
							Response response = this.utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filter);
							userActionsTestUtils.setUserResponse(username, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String stateContains,
										Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filter = this.utils.getContainsFilterField(
											IDObSettlementGeneralModel.CURRENT_STATES,
											stateContains);
							Response response = this.utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filter);
							userActionsTestUtils.setUserResponse(username, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String type, Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filter = this.utils.getEqualsFilterField(
											IDObSettlementGeneralModel.SETTLEMENT_TYPE, type);
							Response response = this.utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filter);
							userActionsTestUtils.setUserResponse(username, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on Company which contains \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String company, Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filter = this.utils.getContainsFilterField(
											IDObSettlementGeneralModel.COMPANY_NAME_EN, company);
							Response response = this.utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filter);
							userActionsTestUtils.setUserResponse(username, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on ActivatedBy which contains \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String createdBy,
										Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filter = this.utils.getContainsFilterField(
											IDObSettlementGeneralModel.ACTIVATED_BY, createdBy);
							Response response = this.utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filter);
							userActionsTestUtils.setUserResponse(username, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on Currency which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String currency,
										Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filter = this.utils.getEqualsFilterField(
											IDObSettlementGeneralModel.CURRENCY_ISO, currency);
							Response response = this.utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filter);
							userActionsTestUtils.setUserResponse(username, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String businessUnit,
										Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filter = this.utils.getEqualsFilterField(
											IDObSettlementGeneralModel.PURCHASE_UNIT_CODE,
											businessUnit);
							Response response = this.utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filter);
							userActionsTestUtils.setUserResponse(username, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of Settlements with filter applied on CreationDate which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String username, Integer pageNumber, String creationDate,
										Integer pageSize) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							String filterString = utils.getContainsFilterField(
											IDObSettlementGeneralModel.CREATION_DATE,
											String.valueOf(utils.getDateTimeInMilliSecondsFormat(
															creationDate)));
							Response response = utils.requestToReadFilteredData(cookie,
											IDObSettlementRestURLs.VIEW_ALL_URL, pageNumber,
											pageSize, filterString);
							userActionsTestUtils.setUserResponse(username, response);
						});
		Then("^no values are displayed to \"([^\"]*)\" and empty grid state is viewed in Home Screen$",
						(String userName) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertTotalNumberOfRecordsReturned(response, 0);
						});
	}

	@Before
	public void beforeViewAll(Scenario scenario) throws Exception {
		utils = new DObSettlementViewAllTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		journalEntryViewAllTestUtils = new DObJournalEntryViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				journalEntryViewAllTestUtils.executeJournalEntryReferenceDocumentFunction();
				utils.executeAccountDeterminationFunction();
			}
			databaseConnector.executeSQLScript(
							DObJournalEntryViewAllTestUtils.clearJournalEntries());
			databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
			databaseConnector.executeSQLScript(clearSettlement());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
