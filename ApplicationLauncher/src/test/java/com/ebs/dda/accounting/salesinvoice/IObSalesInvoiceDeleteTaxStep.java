package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceDeleteTaxTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesInvoiceDeleteTaxStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private final String scenarioName = "Delete Tax from SalesInvoice";
  private IObSalesInvoiceDeleteTaxTestUtils utils;

  public IObSalesInvoiceDeleteTaxStep() {
    When("^\"([^\"]*)\" requests to delete Tax with code \"([^\"]*)\" in SalesInvoice with code \"([^\"]*)\"$", (String userName, String taxCode, String salesInvoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response taxDeleteResponse =
          utils.sendDeleteRequest(cookie, utils.getDeleteTaxUrl(salesInvoiceCode, taxCode));
      userActionsTestUtils.setUserResponse(userName, taxDeleteResponse);
    });

    Then("^Tax with code \"([^\"]*)\" from SalesInvoice with code \"([^\"]*)\" is deleted$", (String taxCode, String salesInvoiceCode) -> {
      utils.assertNoTaxExistsByCode(taxCode, salesInvoiceCode);
    });

    Then("^InvoiceSummaries section of SalesInvoice with Code \"([^\"]*)\" is updated as follows:$", (String salesInvoiceCode, DataTable salesInvoiceSummary) -> {
      utils.assertThatSalesInvoiceSummaryValuesUpdated(salesInvoiceCode, salesInvoiceSummary);
    });

    Given("^\"([^\"]*)\" first deleted Tax with code \"([^\"]*)\" of SalesInvoice with code \"([^\"]*)\" successfully$", (String userName, String taxCode, String salesInvoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response taxDeleteResponse =
          utils.sendDeleteRequest(cookie, utils.getDeleteTaxUrl(salesInvoiceCode, taxCode));
      userActionsTestUtils.setUserResponse(userName, taxDeleteResponse);
    });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new IObSalesInvoiceDeleteTaxTestUtils(properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), scenarioName)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), scenarioName)) {
      userActionsTestUtils.unlockAllSections(IDObSalesInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS,
          utils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
