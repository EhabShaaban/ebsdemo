package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ebs.dda.CommonKeys.DATA;
import static org.junit.Assert.assertTrue;

public class DOMonetaryNotesViewAccountingDetailsTestUtils extends DObMonetaryNotesCommonTestUtils {

  public DOMonetaryNotesViewAccountingDetailsTestUtils(Map<String, Object> properties, EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties,entityManagerDatabaseConnector);
  }

  public Response readAccountingDetailsResponse(String monetaryNoteCode, Cookie userCookie) {
    String restURL =
        IDObNotesReceivableRestURLs.QUERY
            + "/"
            + monetaryNoteCode
            + IDObNotesReceivableRestURLs.ACCOUNTING_DETAILS;
    return sendGETRequest(userCookie, restURL);
  }

  public void assertCorrectValuesAreDisplayed(
      Response response, DataTable accountingDetailsDataTable) throws Exception {
    List<Map<String, String>> expectedAccountingDetails =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualAccountingDetails = getListOfMapsFromResponse(response);
    for (int index = 0; index < actualAccountingDetails.size(); index++) {
      MapAssertion mapAssertion =
          new MapAssertion(
              expectedAccountingDetails.get(index), actualAccountingDetails.get(index));
      assertThatAccountingDetailsMatches(mapAssertion);
    }
  }

  private void assertThatAccountingDetailsMatches(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.DOCUMENT_CODE);
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.CREDIT_DEBIT,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNTING_ENTRY);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.ACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.SUB_ACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_NAME);
    mapAssertion.assertDoubleValue(
        IAccountingFeatureFileCommonKeys.VALUE, IAccountingFeatureFileCommonKeys.AMOUNT_VALUE);
  }

  public void assertThatAccountingDetailsAreEmpty(Response response) {
    List<HashMap<String, Object>> items = response.body().jsonPath().getList(DATA);
    assertTrue(items.isEmpty());
  }
}
