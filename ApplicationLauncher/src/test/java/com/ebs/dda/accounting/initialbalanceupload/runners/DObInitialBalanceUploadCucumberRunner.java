package com.ebs.dda.accounting.initialbalanceupload.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/accounting/initial-balance-upload/IBU_DocumentOwner_Dropdown.feature",
      "classpath:features/accounting/initial-balance-upload/IBU_ViewAll.feature",
      "classpath:features/accounting/initial-balance-upload/IBU_Create_HP.feature",
      "classpath:features/accounting/initial-balance-upload/IBU_AllowedActions_HomeScreen.feature",
      "classpath:features/accounting/initial-balance-upload/IBU_Create_Auth.feature",
      "classpath:features/accounting/initial-balance-upload/IBU_Activate_HP.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.initialbalanceupload"},
    strict = true,
    tags = "not @Future")
public class DObInitialBalanceUploadCucumberRunner {}
