package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceAllowedActionTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceAllowedActionsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceAllowedActionTestUtils salesInvoiceAllowedActionTestUtils;

  public DObSalesInvoiceAllowedActionsStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of Sales Invoice home screen$",
        (String userName) -> {
          String readUrl = IDObSalesInvoiceRestUrls.AUTHORIZED_ACTIONS;
          Response response =
              salesInvoiceAllowedActionTestUtils.sendGETRequest(
                  userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String allowedActionsDataTable, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceAllowedActionTestUtils.assertThatTheFollowingActionsExist(
              response, allowedActionsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of SalesInvoice with code \"([^\"]*)\"$",
        (String username, String salesInvoiceCode) -> {
          String url = IDObSalesInvoiceRestUrls.AUTHORIZED_ACTIONS + '/' + salesInvoiceCode;
          Response response =
              salesInvoiceAllowedActionTestUtils.sendGETRequest(
                  userActionsTestUtils.getUserCookie(username), url);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    salesInvoiceAllowedActionTestUtils = new DObSalesInvoiceAllowedActionTestUtils(properties);
    salesInvoiceAllowedActionTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "Read allowed actions in Sales Invoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {

        databaseConnector.executeSQLScript(
            salesInvoiceAllowedActionTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(
          salesInvoiceAllowedActionTestUtils.getDbScriptsEveryTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read allowed actions in Sales Invoice")) {
      databaseConnector.closeConnection();
    }
  }
}
