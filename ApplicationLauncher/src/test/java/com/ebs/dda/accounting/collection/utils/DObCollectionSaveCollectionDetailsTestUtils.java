package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.collection.IIObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObCollectionSaveCollectionDetailsTestUtils extends DObCollectionTestUtils {

  public DObCollectionSaveCollectionDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getSaveCollectionDetailsUrl(String collectionCode) {
    return IDObCollectionRestURLs.COLLECTION_COMMAND
        + "/"
        + collectionCode
        + IDObCollectionRestURLs.UPDATE_COLLECTION_DETAILS_SECTION;
  }

  public Response saveCollectionDetails(
      String collectionCode,
      String collectionMethod,
      DataTable collectionDetailsDataTable,
      Cookie cookie) {
    Map<String, String> collectionDetailsMap =
        collectionDetailsDataTable.asMaps(String.class, String.class).get(0);
    JsonObject object =
        constructCollectionDetailsValueObject(collectionMethod, collectionDetailsMap);
    return sendPUTRequest(cookie, getSaveCollectionDetailsUrl(collectionCode), object);
  }

  private JsonObject constructCollectionDetailsValueObject(
      String collectionMethod, Map<String, String> collectionDetailsMap) {
    JsonObject object = new JsonObject();
    try {
      Double amountDoubleValue =
          Double.valueOf(collectionDetailsMap.get(IAccountingFeatureFileCommonKeys.AMOUNT));
      object.addProperty(IIObCollectionDetailsGeneralModel.AMOUNT, amountDoubleValue);
    } catch (NumberFormatException e) {
      object.addProperty(
          IIObCollectionDetailsGeneralModel.AMOUNT,
          collectionDetailsMap.get(IAccountingFeatureFileCommonKeys.AMOUNT));
    }
    if (collectionMethod.equals("CASH")) {
      String treasuryCode =
          collectionDetailsMap.get(IAccountingFeatureFileCommonKeys.TREASURY).split(" - ")[0];
      addValueToJsonObject(object, IIObCollectionDetailsGeneralModel.TREASURY_CODE, treasuryCode);
    } else {
      String bankAccountNumber =
          collectionDetailsMap.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT).split(" - ")[0];
      if (collectionDetailsMap.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT).contains(" - ")) {
        bankAccountNumber = bankAccountNumber + " - " +
                collectionDetailsMap.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT).split(" - ")[1];
      }
      addValueToJsonObject(
          object, IIObCollectionDetailsGeneralModel.BANK_ACCOUNT_NUMBER, bankAccountNumber);
    }
    return object;
  }

  public void assertCollectionDetailsWasUpdated(
      String collectionCode, DataTable collectionDetailsDataTable) {
    Map<String, String> updatedCollectionDetailsMap =
        collectionDetailsDataTable.asMaps(String.class, String.class).get(0);

    IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
        (IObCollectionDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedCollectionDetails",
                constructCollectionDetailsQueryParams(collectionCode, updatedCollectionDetailsMap));

    Assertions.assertNotNull(
        collectionDetailsGeneralModel,
        "Details for Collection with code "
            + updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.C_CODE)
            + " don't exist!");
    BigDecimal expectedAmount =
            new BigDecimal(
            updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.AMOUNT));
    BigDecimal actualAmount = collectionDetailsGeneralModel.getAmount();
    assertEquals(expectedAmount.compareTo(actualAmount), 0);
    assertDateEquals(
        updatedCollectionDetailsMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        collectionDetailsGeneralModel.getModifiedDate());
  }

  private Object[] constructCollectionDetailsQueryParams(
      String collectionCode, Map<String, String> updatedCollectionDetailsMap) {
    return new Object[] {
      updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT),
      updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.TREASURY),
      collectionCode,
      updatedCollectionDetailsMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.COLLECTION_METHOD),
      updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_CODE),
      updatedCollectionDetailsMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_TYPE)
    };
  }
}
