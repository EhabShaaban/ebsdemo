package com.ebs.dda.accounting.landedcost.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import cucumber.api.DataTable;

public class CObLandedCostUpdateGoodsInvoiceWhenActivationTestUtils extends CommonTestUtils {

  public CObLandedCostUpdateGoodsInvoiceWhenActivationTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDBScripts() {
    StringBuilder str = new StringBuilder();
        str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
        str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");

    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData.sql");

    return str.toString();
  }

  public void assertThatGoodsInvoiceAndExangeRateAreUpdated(String landedCostCode,
      DataTable detailsDT) {
    List<Map<String, String>> detailsDataMaps = detailsDT.asMaps(String.class, String.class);
    for (Map<String, String> detailsDataMap : detailsDataMaps) {

      Object[] queryParams = constuctQueryParams(landedCostCode, detailsDataMap);

      IObLandedCostDetailsGeneralModel landedCostDetailsGeneralModel =
          (IObLandedCostDetailsGeneralModel) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("getLandedCostGoodsInvoiceAndExchangeRate",
                  queryParams);

      Assert.assertNotNull(
          "Landed cost details doesn't exist given: " + Arrays.toString(queryParams),
          landedCostDetailsGeneralModel);
    }
  }

  private Object[] constuctQueryParams(String landedCostCode, Map<String, String> detailsDataMap) {
    return new Object[] {landedCostCode, detailsDataMap.get(IFeatureFileCommonKeys.VENDOR_INVOICE)
       };
  }

}
