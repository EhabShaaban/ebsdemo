package com.ebs.dda.accounting.settlement;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementActivateStepsUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;

public class DObSettlementActivateSteps extends SpringBootRunner implements En, InitializingBean {

	public static final String SCENARIO_NAME = "Activate Settlement";
	private DObSettlementActivateStepsUtils utils;
	private HObCommonChartOfAccountTestUtils chartOfAccountTestUtils;
	private boolean hasBeenExecutedOnce = false;

	public DObSettlementActivateSteps() {

		When("^\"([^\"]*)\" activates the Settlement with Code \"([^\"]*)\" and JournalEntryDate \"([^\"]*)\" at \"([^\"]*)\"$",
						(String userName, String settlementCode, String journalEntryDate, String postingDateTime) -> {
							utils.freeze(postingDateTime);
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							Response response = utils.sendPutSettlement(cookie, settlementCode, journalEntryDate);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		Then("^Settlement with Code \"([^\"]*)\" is updated as follows:$",
						(String settlementCode, DataTable settlementDataTable) -> {
							utils.assertThatSettlementIsUpdatedSuccessfully(settlementCode,
											settlementDataTable);
						});
		Then("^Activation Details in Settlement with Code \"([^\"]*)\" is updated as follows:$",
						(String settlementCode, DataTable activationDetailsDataTable) -> {
							utils.assertThatSettlementActivationDetailsIsCreatedSuccessfully(
											settlementCode, activationDetailsDataTable);
						});
		Given("^the following GLAccounts has the following Subledgers:$",
						(DataTable subLeadersDataTable) -> {
							chartOfAccountTestUtils
											.assertThatAccountsHasSubledger(subLeadersDataTable);

						});

		Then("^a new JournalEntry is created as follows:$", (DataTable journalEntryDataTable) -> {
			utils.assertThatJournalEntryIsCreated(journalEntryDataTable);
		});

		Then("^the following new JournalEntryDetails are created for JournalEntry with code \"([^\"]*)\":$",
						(String journalEntryCode, DataTable journalEntryItemsDataTable) -> {
							utils.assertThatJournalEntryItemsIsCreated(journalEntryCode,
											journalEntryItemsDataTable);
						});

	}

	@Before
	public void before(Scenario scenario) throws Exception {
		utils = new DObSettlementActivateStepsUtils(getProperties());
		chartOfAccountTestUtils = new HObCommonChartOfAccountTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		chartOfAccountTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecutedOnce) {
				databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
				utils.executeAccountDeterminationFunction();
				hasBeenExecutedOnce = true;
			}
			databaseConnector.executeSQLScript(clearSettlement());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
