package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.ICObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DObSalesInvoiceTypeDropDownTestUtils extends CommonTestUtils {

  private ObjectMapper objectMapper;

  public DObSalesInvoiceTypeDropDownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    return str.toString();
  }

  public void assertThatSalesInvoiceTypesCountIsCorrect(Integer recordsNumber) {
    Long typesCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceTypesCount");
    assertEquals(Long.valueOf(recordsNumber), typesCount);
  }

  public void assertResponseMatchesExpected(Response response, DataTable typesDataTable)
      throws Exception {
    List<Map<String, String>> salesInvoiceTypesExpectedList = typesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> salesInvoiceTypesActualList = getListOfMapsFromResponse(response);

    for (int i = 0; i < salesInvoiceTypesActualList.size(); i++) {
      String salesInvoiceTypesActuaCodeName =
          (String) salesInvoiceTypesActualList.get(i).get(ICObSalesInvoice.CODE)
              + " - "
              + getEnglishValue(salesInvoiceTypesActualList.get(i).get(ICObSalesInvoice.NAME));

      Map salesInvoiceTypesActualMap = new HashMap<String, String>();
      salesInvoiceTypesActualMap.put(IAccountingFeatureFileCommonKeys.SALES_INVOICE_TYPES, salesInvoiceTypesActuaCodeName);
      assertTrue(salesInvoiceTypesExpectedList.contains(salesInvoiceTypesActualMap));
    }
  }
}
