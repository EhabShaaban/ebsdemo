package com.ebs.dda.accounting.paymentrequest.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.UserActionsTestUtils;
import com.ebs.dda.jpa.masterdata.customer.IMObEmployee;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.ebs.dda.masterdata.employee.IMObEmployeeRestUrls;

import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPaymentRequestEmployeeDropDownTestUtils extends CommonTestUtils {

	public DObPaymentRequestEmployeeDropDownTestUtils(Map<String, Object> properties) {
		setProperties(properties);
	}

	public String getDbScripts() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/clearDataSqlScript.sql");
		str.append(",").append("db-scripts/master-data/employee/MObEmployee_Clear.sql");
		return str.toString();
	}

	public void requestToReadEmployees(String userName, UserActionsTestUtils userActionsTestUtils) {
		Cookie cookie = userActionsTestUtils.getUserCookie(userName);
		String restUrl = IMObEmployeeRestUrls.READ_ALL;

		Response response = sendGETRequest(cookie, restUrl);
		userActionsTestUtils.setUserResponse(userName, response);
	}

	public void assertThatEmployeesAreDisplayedToUser(Response response,
					DataTable employeesDataTable) throws Exception {
		List<Map<String, String>> expectedEmployees = employeesDataTable.asMaps(String.class,
						String.class);
		List<HashMap<String, Object>> actualEmployees = getListOfMapsFromResponse(response);
		for (int index = 0; index < actualEmployees.size(); index++) {
			Map<String, String> expectedEmployee = expectedEmployees.get(index);
			HashMap<String, Object> actualEmployee = actualEmployees.get(index);
			MapAssertion mapAssertion = new MapAssertion(expectedEmployee, actualEmployee);
			mapAssertion.assertCodeAndLocalizedNameValue(IMasterDataFeatureFileCommonKeys.EMPLOYEE,
							IMObEmployee.CODE, IMObEmployee.NAME);
		}
	}
}
