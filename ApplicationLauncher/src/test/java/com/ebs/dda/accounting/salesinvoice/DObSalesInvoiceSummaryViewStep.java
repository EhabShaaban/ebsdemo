package com.ebs.dda.accounting.salesinvoice;

import java.sql.SQLException;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.IObInvoiceSummaryTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesInvoiceSummaryViewStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "View SalesInvoice Summary section";

  private IObInvoiceSummaryTestUtils utils;

  public DObSalesInvoiceSummaryViewStep() {

    Given("^the following InvoiceSummary exist in summary section for SalesInvoice \"([^\"]*)\"$",
        (String invoiceCode, DataTable invoiceSummaryDataTable) -> {
          utils.assertThatTheFollowingInvoiceSummaryDetailsExist(invoiceCode, "SalesInvoice",
              invoiceSummaryDataTable);
        });

    When("^\"([^\"]*)\" requests to view Summary section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {

          Cookie cookie = userActionsTestUtils.getUserCookie(userName);

          String readUrl =
              IDObSalesInvoiceRestUrls.GET_URL + invoiceCode + IDObSalesInvoiceRestUrls.SUMMARY;

          Response response = utils.sendGETRequest(cookie, readUrl.toString());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values for InvoiceSummary for SalesInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesInvoiceCode, String userName, DataTable salesInvoiceSummary) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatSummaryDisplayedToUser(response, salesInvoiceCode, salesInvoiceSummary);
        });

  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();

    utils = new IObInvoiceSummaryTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsToClearInsertionDependancies());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
