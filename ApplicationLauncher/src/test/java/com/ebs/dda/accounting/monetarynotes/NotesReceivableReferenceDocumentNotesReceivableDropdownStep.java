package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.NotesReceivableReferenceDocumentNotesReceivableDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class NotesReceivableReferenceDocumentNotesReceivableDropdownStep extends SpringBootRunner
    implements En {

    public static final String SCENARIO_NAME = "Read list of NotesReceivable - ReferenceDocument";
  private NotesReceivableReferenceDocumentNotesReceivableDropdownTestUtils utils;

  public NotesReceivableReferenceDocumentNotesReceivableDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all NotesReceivable - ReferenceDocument for NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String notesReceivableCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.readAllReferenceDocumentNotesReceivables(cookie, notesReceivableCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following NotesReceivable - ReferenceDocument values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable notesReceivablesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReferenceDocumentNotesReceivablesResponseIsCorrect(
              notesReceivablesDataTable, response);
        });

    Then(
        "^total number of NotesReceivable - ReferenceDocument returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer notesReceivableCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfReferenceDocumentNotesReceivablesEquals(
              notesReceivableCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new NotesReceivableReferenceDocumentNotesReceivableDropdownTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
