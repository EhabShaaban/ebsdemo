package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class DObLandedCostDetailsViewTestUtils extends DObLandedCostTestUtils {

    public DObLandedCostDetailsViewTestUtils(Map<String, Object> properties) {
        super(properties);
    }

    public void checkLandedCostDetailsExists(DataTable LCDetailsDataTable) {
        List<Map<String, String>> detailsMaps = LCDetailsDataTable.asMaps(String.class, String.class);
        for (Map<String, String> detailMap : detailsMaps) {
            IObLandedCostDetailsGeneralModel landedCostDetails =
                    (IObLandedCostDetailsGeneralModel)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "getLandedCostDetails",
                                    constructLandedCostDetailsQueryParams(detailMap));
            Assert.assertNotNull("Landed cost doesn't exist " + detailMap, landedCostDetails);
        }
    }

    public String getViewLandedCostDetailsDataUrl(String lcCode) {
        return IDObLandedCostRestUrls.GET_URL
                + "/"
                + lcCode
                + IDObLandedCostRestUrls.LANDED_COST_DETAILS;
    }

    private Object[] constructLandedCostDetailsQueryParams(
            Map<String, String> generalDataMap) {
     String [] totalAmountStr =   generalDataMap.get(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT).split(" ");
        return new Object[]{
                generalDataMap.get(IAccountingFeatureFileCommonKeys.GOODS_INVOICE),
                totalAmountStr.length == 2 ? totalAmountStr[0]: null ,
                totalAmountStr.length == 2 ? totalAmountStr[1]: null,
                generalDataMap.get(IAccountingFeatureFileCommonKeys.LC_CODE),
                generalDataMap.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
                generalDataMap.get(IAccountingFeatureFileCommonKeys.DAMAGE_STOCK)
        };
    }
    public void assertCorrectValuesAreDisplayedInGeneralData(
            Response response, DataTable generalDataTable) throws Exception {
        Map<String, String> generalDataExpected =
                generalDataTable.asMaps(String.class, String.class).get(0);
        Map<String, Object> generalDataActual = getMapFromResponse(response);

        MapAssertion mapAssertion = new MapAssertion(generalDataExpected, generalDataActual);
        assertActualAndExpectedDataFromResponse(mapAssertion);
    }

    private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
        mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.LC_CODE, IDObLandedCostGeneralModel.CODE);

        mapAssertion.assertCodeAndLocalizedNameValue(
                IFeatureFileCommonKeys.TYPE,
                IDObLandedCostGeneralModel.TYPE_CODE,
                IDObLandedCostGeneralModel.TYPE_NAME);

        mapAssertion.assertValue(
                IFeatureFileCommonKeys.CREATED_BY, IDObLandedCostGeneralModel.CREATION_INFO);

        mapAssertion.assertValueContains(
                IFeatureFileCommonKeys.STATE, IDObLandedCostGeneralModel.CURRENT_STATES);

        mapAssertion.assertDateTime(
                IFeatureFileCommonKeys.CREATION_DATE, IDObLandedCostGeneralModel.CREATION_DATE);


    }

    public void assertThatViewLandedCostDetailsResponseIsCorrect(Response response, DataTable landedCostDetailsDataTable) throws Exception {
        Map<String, String> expectedLandedCostDetailsDataMap =
                landedCostDetailsDataTable.asMaps(String.class, String.class).get(0);
        Map<String, Object> actualCompanyDataMap = getMapFromResponse(response);

        MapAssertion mapAssertion = new MapAssertion(expectedLandedCostDetailsDataMap, actualCompanyDataMap);

        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.LC_CODE,
                IIObLandedCostDetailsGeneralModel.LANDED_COST_CODE);
        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.GOODS_INVOICE,
                IIObLandedCostDetailsGeneralModel.VENDOR_INVOICE_CODE);
        mapAssertion.assertConcatenatingValue(
                IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT,
                IIObLandedCostDetailsGeneralModel.INVOICE_AMOUNT,
                IIObLandedCostDetailsGeneralModel.INVOICE_CURRENCY_ISO, " ");
        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.PURCHASE_ORDER,
                IIObLandedCostDetailsGeneralModel.PURCHASE_ORDER_CODE);
        mapAssertion.assertBooleanValue(
                IAccountingFeatureFileCommonKeys.DAMAGE_STOCK,
                IIObLandedCostDetailsGeneralModel.DAMAGE_STOCK);
    }
}
