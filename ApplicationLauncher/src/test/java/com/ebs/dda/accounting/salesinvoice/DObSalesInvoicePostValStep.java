package com.ebs.dda.accounting.salesinvoice;

import java.sql.SQLException;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesInvoicePostValStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "Validate SalesInvoice Activate";

  private DObSalesInvoiceActivateTestUtils utils;

  public DObSalesInvoicePostValStep() {
    And("^first \"([^\"]*)\" activates SalesInvoice of type \"([^\"]*)\" with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String salesInvoiceType, String salesInvoiceCode, String dateTime,
            DataTable salesInvoiceDataTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.activateSalesInvoice(userCookie, salesInvoiceCode,
              salesInvoiceType, salesInvoiceDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();

    utils = new DObSalesInvoiceActivateTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsToClearInsertionDependancies());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
