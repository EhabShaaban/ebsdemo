package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObInvoiceSummaryGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IObVendorInvoiceSummaryViewTestUtils extends DObVendorInvoiceTestUtils {

  public IObVendorInvoiceSummaryViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertCorrectValuesAreDisplayedInSummarySection(Response response,
      DataTable summaryValuesDataTable) {
    List<Map<String, String>> expectedSummaries = summaryValuesDataTable.asMaps(String.class,
        String.class);
    HashMap<String, Object> actualSummaries = getMapFromResponse(response);

    for (int i = 0; i < expectedSummaries.size(); i++) {
      assertActualAndExpectedDataFromResponse(expectedSummaries.get(i), actualSummaries);
    }
  }

  private void assertActualAndExpectedDataFromResponse(Map<String, String> expectedSummary, HashMap<String, Object> actualSummary) {
    MapAssertion mapAssertion = new MapAssertion(expectedSummary, actualSummary);

    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES, IIObInvoiceSummaryGeneralModel.AMOUNT_BEFORE_TAXES);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TAX_AMOUNT, IIObInvoiceSummaryGeneralModel.TAX_AMOUNT);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.NET_AMOUNT, IIObInvoiceSummaryGeneralModel.NET_AMOUNT);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT, IIObInvoiceSummaryGeneralModel.DOWN_PAYMENT);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TOTAL_REMAINING, IIObInvoiceSummaryGeneralModel.REMAINING_AMOUNT);
  }
}
