package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCreationTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObMonetaryNotesCreationStep extends SpringBootRunner implements En {

  private DObMonetaryNotesCreationTestUtils utils;

  public DObMonetaryNotesCreationStep() {

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String frozenDateTime) -> {
          utils.freeze(frozenDateTime);
        });
    Given(
        "^Last created NotesReceivable was with code \"([^\"]*)\"$",
        (String notesReceivableCode) -> {
          utils.assertNotesReceivablesExistsWithCode(notesReceivableCode);
        });

    When(
        "^\"([^\"]*)\" creates NotesReceivable with the following values:$",
        (String userName, DataTable notesReceivableDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          JsonObject notesReceivable =
              utils.preparNotesReceivableJsonObject(notesReceivableDataTable);
          Response response =
              utils.sendPostRequest(cookie, IDObNotesReceivableRestURLs.CREATE_URL, notesReceivable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new NotesReceivable is created with the following values:$",
        (DataTable notesReceivableDataTable) -> {
          utils.assertThatNotesReceivableIsCreatedSuccessfully(notesReceivableDataTable);
        });
    Then(
        "^the NotesReceivableCompany Section of NotesReceivable with code \"([^\"]*)\" is updated as follows:$",
        (String notesReceivableCode, DataTable notesReceivableCompanyDataTable) -> {
          utils.assertThatNotesReceivableCompanyIsCreatedSuccessfully(
              notesReceivableCode, notesReceivableCompanyDataTable);
        });
    Then(
        "^the NotesReceivableDetails Section of NotesReceivable with code \"([^\"]*)\" is updated as follows:$",
        (String notesReceivableCode, DataTable notesReceivableDetailsDataTable) -> {
          utils.assertThatNotesReceivableDetailsIsCreatedSuccessfully(
              notesReceivableCode, notesReceivableDetailsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to create NotesReceivables",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, IDObNotesReceivableRestURLs.CREATE_URL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObMonetaryNotesCreationTestUtils(getProperties(),entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create NotesReceivable")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create NotesReceivable")) {
      utils.unfreeze();
    }
  }
}
