package com.ebs.dda.accounting.settlement.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

public class DObSettlementViewDetailsTestUtils extends DObSettlementsCommonTestUtils {

  public DObSettlementViewDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public void assertThatViewSettlementDetailsResponseIsCorrect(Response response,
      DataTable detailsDT) {

    List<Map<String, String>> detailsMaps = detailsDT.asMaps(String.class, String.class);
    for (Map<String, String> detailsMap : detailsMaps) {

      Object[] params = constructSettlementDetailsSelectionQueryParams(detailsMap);

      IObSettlementDetailsGeneralModel detailsGM =
          (IObSettlementDetailsGeneralModel) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("selectSettlementDetailsGM", params);

      Assert.assertNotNull("Settlement Details is NULL, given: " + Arrays.toString(params),
          detailsGM);
    }
  }

  private Object[] constructSettlementDetailsSelectionQueryParams(Map<String, String> detailsMap) {
    return new Object[] {

        detailsMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE),
        detailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY)

    };
  }

}
