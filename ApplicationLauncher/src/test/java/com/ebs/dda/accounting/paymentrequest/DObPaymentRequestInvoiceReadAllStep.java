package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCreateJournalEntryTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestInvoiceReadAllTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPostCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestInvoiceReadAllStep extends SpringBootRunner
    implements En, InitializingBean {

    public static final String SCENARIO_NAME = "Read All Invoices for DueDocument Dropdown in PaymentRequest";
    private static boolean hasBeenExecutedOnce = false;
  private DObPaymentRequestInvoiceReadAllTestUtils utils;
  private DObPaymentRequestCreateJournalEntryTestUtils createJournalEntryUtils;
  private DObPaymentRequestPostCommonTestUtils paymentRequestPostCommonTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestInvoiceReadAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    createJournalEntryUtils = new DObPaymentRequestCreateJournalEntryTestUtils(getProperties());
    createJournalEntryUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    paymentRequestPostCommonTestUtils = new DObPaymentRequestPostCommonTestUtils(getProperties());
    paymentRequestPostCommonTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  public DObPaymentRequestInvoiceReadAllStep() {
    Given(
        "^the following VendorInvoices exist:$",
        (DataTable vendorInvoicesDataTable) -> {
          createJournalEntryUtils.assertVendorInvoiceExists(vendorInvoicesDataTable);
        });
    Then(
        "^the following invoices should be returned to \"([^\"]*)\":$",
        (String userName, DataTable vendorInvoicesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatInvoicesDropDownResponseIsCorrect(response, vendorInvoicesDataTable);
        });

    And(
        "^total number of invoices returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer numberOfInvoices) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfReturnedInvoicesIsCorrect(response, numberOfInvoices);
        });
      When("^\"([^\"]*)\" requests to read all activated VendorInvoices for Payment with Remaining greater than zero and BusinessUnit with Code \"([^\"]*)\" and  Vendor with Code \"([^\"]*)\"$", (String userName, String businessUnitCode, String vendorCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
                  utils.sendGETRequest(cookie, utils.getReadAllInvoicesUrl(businessUnitCode, vendorCode));
          userActionsTestUtils.setUserResponse(userName, response);
      });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(
        scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      } else {
          databaseConnector.executeSQLScript(utils.getDbScriptsEveryTime());
      }
        databaseConnector.executeSQLScript(AccountingDocumentCommonTestUtils.clearAccountingDocuments());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(
        scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
