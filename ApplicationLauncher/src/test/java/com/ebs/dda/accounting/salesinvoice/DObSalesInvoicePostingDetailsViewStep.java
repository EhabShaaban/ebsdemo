package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceViewPostingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoicePostingDetailsViewStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private DObSalesInvoiceViewPostingDetailsTestUtils utils;

    public DObSalesInvoicePostingDetailsViewStep() {

        Given("^the following PostingDetails for AccountingDocument with DocumentType \"([^\"]*)\" exist:$", (String documentType, DataTable postingDetailsDataTable) -> {
            utils.assertThatSalesInvoiceHasThoseActivationDetails(documentType, postingDetailsDataTable);
        });

        When("^\"([^\"]*)\" requests to view PostingDetails section of SalesInvoice with code \"([^\"]*)\"$", (String userName, String salesInvoiceCode) -> {
            Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
            StringBuilder readUrl = new StringBuilder();
            readUrl.append(IDObSalesInvoiceRestUrls.GET_URL);
            readUrl.append(salesInvoiceCode);
            readUrl.append(IDObSalesInvoiceRestUrls.VIEW_POSTING_DETAILS);
            Response response = utils.sendGETRequest(userCookie, String.valueOf(readUrl));
            userActionsTestUtils.setUserResponse(userName, response);
        });

        Then("^the following values of PostingDetails section for SalesInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$", (String salesInvoiceCode, String userName, DataTable postingDetailsDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertCorrectValuesAreDisplayedInActivationDetails(response, postingDetailsDataTable);
        });


    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        Map<String, Object> properties = getProperties();
        utils = new DObSalesInvoiceViewPostingDetailsTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        if (contains(scenario.getName(), "View PostingDetails section")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScriptsEveryTimeExecution());
        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View PostingDetails section")) {
            databaseConnector.closeConnection();
        }
    }
}
