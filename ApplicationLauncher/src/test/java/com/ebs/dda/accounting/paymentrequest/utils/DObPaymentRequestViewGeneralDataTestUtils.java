package com.ebs.dda.accounting.paymentrequest.utils;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.Arrays;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Assert;

public class DObPaymentRequestViewGeneralDataTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestViewGeneralDataTestUtils(Map<String, Object> properties) {
    super(properties);
    this.urlPrefix = (String) getProperty(URL_PREFIX);
  }

  public String getViewGeneralDataUrl(String code) {
    return IDObPaymentRequestRestURLs.BASE_URL
        + "/"
        + code
        + IDObPaymentRequestRestURLs.VIEW_GENERAL_DATA;
  }
  public static String getPaymentRequestDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    return str.toString();
  }

  public void assertThatViewGeneralDataResponseIsCorrect(
      Response response, DataTable generalDataTable) throws Exception {

    Map<String, String> prGeneralDataMap =
        generalDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> prGeneralDataResponseMap =
        response.body().jsonPath().getMap(CommonKeys.DATA);

    MapAssertion mapAssertion = new MapAssertion(prGeneralDataMap, prGeneralDataResponseMap);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.PR_CODE, IDObPaymentRequestGeneralModel.USER_CODE);
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.PAYMENT_TYPE, IDObPaymentRequestGeneralModel.PAYMENT_TYPE);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObPaymentRequestGeneralModel.CREATION_INFO);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObPaymentRequestGeneralModel.CREATION_INFO);
    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObPaymentRequestGeneralModel.CURRENT_STATES);
    mapAssertion.assertValueContains(
            IFeatureFileCommonKeys.DOCUMENT_OWNER, IDObPaymentRequestGeneralModel.DOCUMENT_OWNER);
    this.assertDateEquals(
        prGeneralDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
        formatDate(
            String.valueOf(
                prGeneralDataResponseMap.get(IDObPaymentRequestGeneralModel.CREATION_DATE))));
  }

  private DateTime formatDate(String date) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
    return formatter.parseDateTime(date).withZone(DateTimeZone.UTC);
  }

}





