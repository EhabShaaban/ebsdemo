package com.ebs.dda.accounting.landedcost;

import java.sql.SQLException;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.IObLandedCostCompanyDataViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObLandedCostCompanyDataViewStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "View CompanyData section in LandedCost";
  private static boolean hasBeenExecuted = false;
  private IObLandedCostCompanyDataViewTestUtils landedCostCompanyUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    landedCostCompanyUtils = new IObLandedCostCompanyDataViewTestUtils(properties);
    landedCostCompanyUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }


  public DObLandedCostCompanyDataViewStep() {

    Given("^the following ComapnyData for LandedCosts exist:$", (DataTable companyDataTable) -> {
      landedCostCompanyUtils.assertThatCompanyDataIsExist(companyDataTable);
    });

    When("^\"([^\"]*)\" requests to view CompanyData section of LandedCost with code \"([^\"]*)\"$",
        (String username, String landedCostCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          String readUrl = IDObLandedCostRestUrls.GET_URL + "/" + landedCostCode
              + IDObLandedCostRestUrls.COMPANY_DATA;
          Response response =
              landedCostCompanyUtils.sendGETRequest(userCookie, String.valueOf(readUrl));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of CompanyData section for LandedCost with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String landedCostCode, String username, DataTable companyDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          landedCostCompanyUtils.assertThatViewComanyDataResponseIsCorrect(response,
              companyDataTable);
        });

  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(landedCostCompanyUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(landedCostCompanyUtils.getDbScripts());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
