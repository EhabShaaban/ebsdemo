package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewActivationDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObCollectionViewActivationDetailsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObCollectionViewActivationDetailsTestUtils utils;

  public DObCollectionViewActivationDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view ActivationDetails section of Collection with code \"([^\"]*)\"$",
        (String userName, String collectionCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          StringBuilder readUrl = new StringBuilder();
          readUrl.append(IDObCollectionRestURLs.GET_URL);
          readUrl.append(collectionCode);
          readUrl.append(IDObCollectionRestURLs.ACTIVATION_DETAILS);
          Response response = utils.sendGETRequest(userCookie, String.valueOf(readUrl));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of ActivationDetails section for Collection with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String collectionCode, String userName, DataTable activationDetailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayedInActivationDetails(
              response, collectionCode, activationDetailsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObCollectionViewActivationDetailsTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), "View ActivationDetails section -")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getConciseDbScripts());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
