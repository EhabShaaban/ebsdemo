package com.ebs.dda.accounting.paymentrequest.utils;

import java.util.Map;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPaymentDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

public class DObPaymentRequestViewPaymentDetailsTestUtils
    extends DObPaymentRequestPaymentDetailsTestUtils {

  public DObPaymentRequestViewPaymentDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
    this.urlPrefix = (String) getProperty(URL_PREFIX);
  }

  public static String getPaymentRequestDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    return str.toString();
  }

  public static String getPaymentRequestDbScriptsOneTimeExecutionForEditPaymentDetails() {
    StringBuilder str = new StringBuilder();
    str.append(getPaymentRequestDbScriptsOneTimeExecution());
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    return str.toString();
  }

  public static String getPaymentRequestDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append("," + "db-scripts/accounting/payment-request/payment-request.sql");
    str.append(
        ","
            + "db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
    str.append("," + "db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
    str.append(
        ","
            + "db-scripts/accounting/payment-request/iob-payment-request-payment-details-view-all.sql");
    str.append(
        "," + "db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    return str.toString();
  }

  public String getViewPaymentDetailsUrl(String paymentRequestCode) {
    return IDObPaymentRequestRestURLs.BASE_URL
        + "/"
        + paymentRequestCode
        + IDObPaymentRequestRestURLs.VIEW_PAYMENT_DETAILS;
  }

  public void assertCorrectValuesAreDisplayedInPaymentDetails(
      Response response, DataTable paymentDetailsTable) throws Exception {
    Map<String, String> paymentDetailsExpected =
        paymentDetailsTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> paymentDetailsActual = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(paymentDetailsExpected, paymentDetailsActual);

    assertParty(mapAssertion);
    assertDueDocument(mapAssertion);
    assertAmountAndCurrency(mapAssertion);
    assertDescription(mapAssertion);
    assertPaymentStore(mapAssertion);
    assertExpectedDueDate(mapAssertion);
    assertBankRefNumber(mapAssertion);
  }

  private void assertPaymentStore(MapAssertion mapAssertion) throws Exception {
    assertBankAccount(mapAssertion);
    assertTreasury(mapAssertion);
  }

  private void assertTreasury(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER,
        IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_NUMBER,
        IIObPaymentRequestPaymentDetailsGeneralModel.BANK_ACCOUNT_NAME);
  }

  private void assertBankAccount(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TREASURY,
        IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE,
        IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_Name);
  }

  private void assertDescription(MapAssertion mapAssertion) {
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.PR_DESCRIPTION,
        IIObPaymentRequestPaymentDetailsGeneralModel.DESCRIPTION);
  }

  private void assertAmountAndCurrency(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT,
        IIObPaymentRequestPaymentDetailsGeneralModel.NET_AMOUNT);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.CURRENCY,
        IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_ISO,
        IIObPaymentRequestPaymentDetailsGeneralModel.CURRENCY_NAME);
  }

  private void assertDueDocument(MapAssertion mapAssertion) {
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE,
        IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_TYPE);
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT,
        IIObPaymentRequestPaymentDetailsGeneralModel.DUE_DOCUMENT_CODE);
  }

  private void assertParty(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER,
        IIObPaymentRequestPaymentDetailsGeneralModel.BUSINESS_PARTNER_CODE,
        IIObPaymentRequestPaymentDetailsGeneralModel.BUSINESS_PARTNER_NAME);
  }

  private void assertExpectedDueDate(MapAssertion mapAssertion) {
    mapAssertion.assertDateTime(
        IAccountingFeatureFileCommonKeys.EXPECTED_DUE_DATE,
        IIObPaymentRequestPaymentDetailsGeneralModel.EXPECTED_DUE_DATE);
  }

  private void assertBankRefNumber(MapAssertion mapAssertion) {
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.BANK_TRANS_REF,
        IIObPaymentRequestPaymentDetailsGeneralModel.BANK_TRANS_REF);
  }
}
