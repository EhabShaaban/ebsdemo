package com.ebs.dda.accounting.collection.utils;

import java.util.Map;

public class DObCollectionAllowedActionsObjectScreenTestUtils extends DObCollectionTestUtils {

  public DObCollectionAllowedActionsObjectScreenTestUtils(Map<String, Object> properties) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
