package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceGeneralDataViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceGeneralDataViewStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceGeneralDataViewTestUtils utils;

  public DObSalesInvoiceGeneralDataViewStep() {
    Given(
        "^the following GeneralData for SalesInvoice exists:$",
        (DataTable generalDataTable) -> {
          utils.checkSalesInvoiceGeneralDataExists(generalDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of SalesInvoice with code \"([^\"]*)\"$",
        (String username, String salesInvoiceCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          StringBuilder readUrl = new StringBuilder();
          readUrl.append(IDObSalesInvoiceRestUrls.GET_URL);
          readUrl.append(salesInvoiceCode);
          readUrl.append(IDObSalesInvoiceRestUrls.GENERAL_DATA);
          Response response = utils.sendGETRequest(userCookie, String.valueOf(readUrl));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of GeneralData section for SalesInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesInvoiceCode, String username, DataTable generalDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertCorrectValuesAreDisplayedInGeneralData(response, generalDataTable);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObSalesInvoiceGeneralDataViewTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "View GeneralData section")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View GeneralData section")) {
      databaseConnector.closeConnection();
      }
  }
}
