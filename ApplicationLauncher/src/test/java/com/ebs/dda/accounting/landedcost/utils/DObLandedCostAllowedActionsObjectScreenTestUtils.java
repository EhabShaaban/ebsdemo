package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObLandedCostAllowedActionsObjectScreenTestUtils extends DObLandedCostTestUtils {

  public DObLandedCostAllowedActionsObjectScreenTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertLandedCostExists(DataTable landedCostDataTable) {
    List<Map<String, String>> landedCosts = landedCostDataTable.asMaps(String.class, String.class);

    for (Map<String, String> landedCost : landedCosts) {
      DObLandedCostGeneralModel landedCostGeneralModel =
          (DObLandedCostGeneralModel) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getExistsLandedCost", constructLandedCost(landedCost));
      Assert.assertNotNull("Landed Cost doesn't exist: "+ landedCost, landedCostGeneralModel);
    }
  }

  private Object[] constructLandedCost(Map<String, String> landedCost) {
    return new Object[]{
        landedCost.get(IFeatureFileCommonKeys.CODE),
        landedCost.get(IFeatureFileCommonKeys.TYPE),
        '%' + landedCost.get(IFeatureFileCommonKeys.STATE) + '%',
        landedCost.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertThatTotalNumberOfAllowedActionsIsZero(Response response) {
    ArrayList actualAllowedActions = response.body().jsonPath().get(CommonKeys.DATA);
    Assert.assertNull(actualAllowedActions);
  }
}
