package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.IObPaymentRequestViewAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class IObPaymentRequestViewAccountingDetailsStep extends SpringBootRunner
    implements En, InitializingBean {

    public static final String SCENARIO_NAME = "View AccountingDetails";
    private IObPaymentRequestViewAccountingDetailsTestUtils utils;
    private static boolean hasBeenExecutedOnce = false;


    public IObPaymentRequestViewAccountingDetailsStep() {

    Given(
        "^the following AccountingDetails for PaymentRequests exist:$",
        (DataTable AccountingDetailsTable) -> {
          utils.assertThatTheFollowingPRAccountingDetailsExist(AccountingDetailsTable);
        });

    When(
        "^\"([^\"]*)\" requests to view AccountingDetails section of PaymentRequest with code \"([^\"]*)\"$",
        (String userName, String prCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, utils.getViewAccountingDetailsUrl(prCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of AccountingDetails section for PaymentRequest with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String PRCode, String userName, DataTable AccountingDetailsData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatAccountingDetailsDataMatches(AccountingDetailsData, response);
        });
        And("^the following PaymentRequests AccountingDetails exist:$",  (DataTable AccountingDetailsTable) -> {
            utils.insertPaymentRequestAccountingDetails(AccountingDetailsTable);
        });
    }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObPaymentRequestViewAccountingDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewAccountingDetails(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if(!hasBeenExecutedOnce){
          databaseConnector.executeSQLScript(
                  IObPaymentRequestViewAccountingDetailsTestUtils
                          .getPaymentRequestDbScriptsOneTimeExecution());
          utils.executeAccountDeterminationFunction();
          hasBeenExecutedOnce = true;
      }
        databaseConnector.executeSQLScript(IObPaymentRequestViewAccountingDetailsTestUtils.clearPaymentRequests());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
