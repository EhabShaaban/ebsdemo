package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObNotesReceivablesReferenceDocumentGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class IObNotesReceivableViewReferenceDocumentTestUtils
    extends DObMonetaryNotesCommonTestUtils {
  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Reference Document]";

  public IObNotesReceivableViewReferenceDocumentTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getClearMonetaryNotesDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/accountingnote/Accounting_Notes_Clear.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/SalesInvoice_Clear.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");

    return str.toString();
  }

  public Response readReferenceDocuments(Cookie cookie, String monetaryNoteCode) {
    return sendGETRequest(cookie, getViewReferenceDocumentsUrl(monetaryNoteCode));
  }

  public String getViewReferenceDocumentsUrl(String monetaryNoteCode) {
    return IDObNotesReceivableRestURLs.QUERY
        + "/"
        + monetaryNoteCode
        + IDObNotesReceivableRestURLs.VIEW_REFERENCE_DOCUMENTS;
  }

  public void assertMonetaryNoteReferenceDocumentsIsCorrect(
      String monetaryNoteCode, Response response, DataTable referenceDocumentsTable)
      throws Exception {
    List<Map<String, String>> expectedReferenceDocumentsList =
        referenceDocumentsTable.asMaps(String.class, String.class);

    HashMap<String, Object> actualReferenceDocumentsResponse = getMapFromResponse(response);
    List<HashMap<String, Object>> actualReferenceDocumentsList =
        readListOfMapFromMap(actualReferenceDocumentsResponse, "ReferenceDocuments");

    for (int i = 0; i < actualReferenceDocumentsList.size(); i++) {

      MapAssertion mapAssertion =
          new MapAssertion(
              expectedReferenceDocumentsList.get(i), actualReferenceDocumentsList.get(i));
      assertActualAndExpectedItemsFromResponse(mapAssertion);
    }
  }

  private void assertActualAndExpectedItemsFromResponse(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE,
        IIObNotesReceivablesReferenceDocumentGeneralModel.NOTES_RECEIVABLE_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT,
        IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE,
        IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_NAME);

    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.AMOUNT_TO_COLLECT,
        IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT);
  }

  public void assertThatReferenceDocumentsResponseIsEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualReferenceDocumentsList =
        getReferenceDocumentsFromResponse(response);
    assertEquals(0, actualReferenceDocumentsList.size());
  }

  private List<HashMap<String, Object>> getReferenceDocumentsFromResponse(Response response)
      throws Exception {
    HashMap<String, Object> actualReferenceDocumentsResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualReferenceDocumentsResponse, "ReferenceDocuments");
  }
}
