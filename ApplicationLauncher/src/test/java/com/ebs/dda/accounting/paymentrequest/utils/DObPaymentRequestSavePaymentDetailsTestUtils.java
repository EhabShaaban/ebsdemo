package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPaymentDetailsValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.masterdata.IMasterDataFeatureFileCommonKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObPaymentRequestSavePaymentDetailsTestUtils
    extends DObPaymentRequestPaymentDetailsTestUtils {

  public DObPaymentRequestSavePaymentDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static String getPaymentRequestDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/master-data/treasury/CObTreasury.sql");
    str.append("," + "db-scripts/master-data/treasury/IObCompanyTreasuries.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");

    return str.toString();
  }

  public Response savePaymentDetailsSection(Cookie loginCookie, String paymentRequestCode,
      DataTable paymentDetailsDataTable) {
    JsonObject paymentDetailsJsonData = getPaymentDetailsJsonObject(paymentDetailsDataTable);
    StringBuilder restURL = new StringBuilder();
    restURL.append(IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND).append('/');
    restURL.append(paymentRequestCode);
    restURL.append(IDObPaymentRequestRestURLs.UPDATE_PAYMENT_DETAILS_SECTION);
    return sendPUTRequest(loginCookie, String.valueOf(restURL), paymentDetailsJsonData);
  }

  private JsonObject getPaymentDetailsJsonObject(DataTable paymentDetailsDataTable) {
    Map<String, String> paymentDetails =
        paymentDetailsDataTable.asMaps(String.class, String.class).get(0);
    JsonObject paymentDetailsJsonData = new JsonObject();

    addAmountToJsonData(paymentDetailsJsonData, paymentDetails);

    String currencyCode = paymentDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY_CODE);
    if (currencyCode != null) {
      paymentDetailsJsonData.addProperty(IIObPaymentRequestPaymentDetailsValueObject.PR_CURRENCY_CODE,
              (!currencyCode.isEmpty()) ? currencyCode : null);
    }

    String description = paymentDetails.get(IAccountingFeatureFileCommonKeys.PR_DESCRIPTION);
    paymentDetailsJsonData.addProperty(IIObPaymentRequestPaymentDetailsValueObject.PR_DESCRIPTION,
        (!description.isEmpty()) ? description : null);
    Long bankId =
        !NumberUtils.isNumber(paymentDetails.get(IAccountingFeatureFileCommonKeys.COMPANY_BANK_ID))
            ? null
            : Long.parseLong(paymentDetails.get(IAccountingFeatureFileCommonKeys.COMPANY_BANK_ID));
    paymentDetailsJsonData.addProperty(IIObPaymentRequestPaymentDetailsValueObject.COMPANY_BANK_ID, bankId);

    String treasuryCode = paymentDetails.get(IFeatureFileCommonKeys.TREASURY) != null &&!paymentDetails.get(IFeatureFileCommonKeys.TREASURY).isEmpty() ? paymentDetails.get(IFeatureFileCommonKeys.TREASURY) : null;
    paymentDetailsJsonData.addProperty(IIObPaymentRequestPaymentDetailsValueObject.TREASURY_CODE, treasuryCode);

    addValueToJsonObject(paymentDetailsJsonData,
        IIObPaymentRequestPaymentDetailsValueObject.COST_FACTOR_ITEM_CODE,
        paymentDetails.get(IAccountingFeatureFileCommonKeys.COST_FACTOR_ITEM));
    return paymentDetailsJsonData;
  }

  private void addAmountToJsonData(JsonObject paymentDetailsJsonData,
      Map<String, String> paymentDetails) {
    String amount = paymentDetails.get(IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT);
    if (!amount.isEmpty() && amount.matches("^(\\d*\\.)?\\d+$")) {
      paymentDetailsJsonData.addProperty(IIObPaymentRequestPaymentDetailsValueObject.PR_NET_AMOUNT,
          new BigDecimal(amount));
    } else if (!amount.isEmpty()) {
      paymentDetailsJsonData.addProperty(IIObPaymentRequestPaymentDetailsValueObject.PR_NET_AMOUNT,
          amount);
    } else {
      paymentDetailsJsonData.add(IIObPaymentRequestPaymentDetailsValueObject.PR_NET_AMOUNT, null);
    }
  }

  public void checkUpdatedDataInVendorPaymentDetails(String paymentRequestCode,
      DataTable paymentDetailsDataTable) {

    Map<String, String> paymentDetailsMap =
        paymentDetailsDataTable.asMaps(String.class, String.class).get(0);
    Object[] params = getPartyPaymentDetailsParameters(paymentRequestCode, paymentDetailsMap);

    IObPaymentRequestPaymentDetailsGeneralModel paymentRequestPaymentDetailsGeneralModel =
        (IObPaymentRequestPaymentDetailsGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getPartyPaymentRequestPaymentDetailsUpdatedData",
                params);

    assertNotNull("payment Details Section was not updated for " + Arrays.toString(params),
        paymentRequestPaymentDetailsGeneralModel);
  }

  private Object[] getPartyPaymentDetailsParameters(String paymentRequestCode,
      Map<String, String> paymentDetailsMaps) {
    String costFactorItem =
        paymentDetailsMaps.get(IAccountingFeatureFileCommonKeys.COST_FACTOR_ITEM);
    String treasury = paymentDetailsMaps.get(IFeatureFileCommonKeys.TREASURY);
    String bank = paymentDetailsMaps.get(IFeatureFileCommonKeys.BANK);
    String dueDocument = paymentDetailsMaps.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT);
    return new Object[]{

            dueDocument == null ? "":dueDocument,
            getBigDecimal(paymentDetailsMaps.get(IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT)),
            paymentDetailsMaps.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO),
            paymentDetailsMaps.get(IAccountingFeatureFileCommonKeys.PR_DESCRIPTION),
            costFactorItem == null ? "" : costFactorItem,
            treasury == null ? "" : treasury,
            bank == null ? "" : bank,
            paymentRequestCode,
            paymentDetailsMaps.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            paymentDetailsMaps.get(IFeatureFileCommonKeys.LAST_UPDATED_BY)

    };
  }

  public void assertThatServiceItemsExist(DataTable itemsDT) {
    Map<String, String> itmesMap = itemsDT.asMaps(String.class, String.class).get(0);

    Object[] queryParams = getItemsQueryParams(itmesMap);
    MObItemGeneralModel item = (MObItemGeneralModel) entityManagerDatabaseConnector
        .executeNativeNamedQuerySingleResult("getServiceItems", queryParams);

    assertNotNull("Service Item not exists given: " + Arrays.toString(queryParams), item);
  }

  private Object[] getItemsQueryParams(Map<String, String> itmesMap) {
    String costFactor = itmesMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR);
    return new Object[] {itmesMap.get(IFeatureFileCommonKeys.CODE),
        itmesMap.get(IFeatureFileCommonKeys.TYPE),
        itmesMap.get(IMasterDataFeatureFileCommonKeys.MARKET_NAME),
        itmesMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        costFactor.equalsIgnoreCase("true") ? true : false};
  }
}
