package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import java.util.Map;

public class IObSalesInvoiceRequestAddItemTestUtils extends DObSalesInvoiceTestUtils {

  public IObSalesInvoiceRequestAddItemTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/DObSalesInvoice.sql");
    return str.toString();
  }

  public String getLockUrl(String salesInvoiceCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObSalesInvoiceRestUrls.COMMAND_URL);
    lockUrl.append(salesInvoiceCode);
    lockUrl.append(IDObSalesInvoiceRestUrls.LOCK_SALES_INVOICE_ITEMS_SECTION);
    return lockUrl.toString();
  }

  public String getUnLockUrl(String salesInvoiceCode) {
    String unLockUrl =
            IDObSalesInvoiceRestUrls.COMMAND_URL
                    + salesInvoiceCode
                    + IDObSalesInvoiceRestUrls.UNLOCK_SALES_INVOICE_ITEMS_SECTION;
    return unLockUrl;
  }

}
