package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class DObPaymentRequestDeleteStep extends SpringBootRunner implements En, InitializingBean {

    public static final String SCENARIO_NAME = "Delete PaymentRequests";
    private DObPaymentRequestDeleteTestUtils utils;
    private static boolean hasBeenExecutedOnce = false;


    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObPaymentRequestDeleteTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObPaymentRequestDeleteStep() {

        When(
                "^\"([^\"]*)\" requests to delete PaymentRequests with code \"([^\"]*)\"$",
                (String userName, String paymentRequestCode) -> {
                    Response paymentRequestDeleteResponse =
                            utils.deletePaymentRequest(
                                    userActionsTestUtils.getUserCookie(userName), paymentRequestCode);

                    userActionsTestUtils.setUserResponse(userName, paymentRequestDeleteResponse);
                });

        Then(
                "^PaymentRequests with code \"([^\"]*)\" is deleted from the system$",
                (String paymentRequestCode) -> {
                    utils.assertThatPaymentRequestNotExist(paymentRequestCode);
                });

        Given(
                "^\"([^\"]*)\" first deleted the PaymentRequests with code \"([^\"]*)\" successfully$",
                (String userName, String paymentRequestCode) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = utils.deletePaymentRequest(cookie, paymentRequestCode);
                    utils.assertResponseSuccessStatus(response);
                });

        Given(
                "^first \"([^\"]*)\" opens \"([^\"]*)\" section of PaymentRequests with code \"([^\"]*)\" in edit mode$",
                (String userName, String sectionName, String paymentRequestCode) -> {
                    Map<String, String> sectionsLockUrlsMap = utils.getSectionsLockUrlsMap();
                    String lockSectionUrl =
                            utils.getLockUrl(paymentRequestCode, sectionsLockUrlsMap.get(sectionName)).toString();

                    Response lockRepponse =
                            userActionsTestUtils.lockSection(userName, lockSectionUrl, paymentRequestCode);

                    utils.assertResponseSuccessStatus(lockRepponse);

                    userActionsTestUtils.setUserResponse(userName, lockRepponse);
                });
        Given(
                "^\"([^\"]*)\" first deleted the PaymentRequest with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
                (String userName, String paymentRequestCode, String deletionTime) -> {
                    utils.freeze(deletionTime);
                    Response paymentRquestDeleteResponse =
                            utils.deletePaymentRequest(
                                    userActionsTestUtils.getUserCookie(userName), paymentRequestCode);
                    userActionsTestUtils.setUserResponse(userName, paymentRquestDeleteResponse);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce){
                databaseConnector.executeSQLScript(utils.getDeletePaymentRequestDbScripts());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(DObPaymentRequestDeleteTestUtils.clearPaymentRequests());

        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            userActionsTestUtils.unlockAllSections(
                    IDObPaymentRequestRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
            databaseConnector.closeConnection();
        }
    }
}
