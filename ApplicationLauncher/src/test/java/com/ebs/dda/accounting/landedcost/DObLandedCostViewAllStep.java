package com.ebs.dda.accounting.landedcost;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostViewAllTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewGeneralDataTestUtils;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

import static com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils.getLandedCostDbScriptsOneTimeExecution;

public class DObLandedCostViewAllStep extends SpringBootRunner implements En {

    private static final String SCENARIO_NAEM = "View All LandedCosts";
    private static boolean hasBeenExecuted = false;

    private DObLandedCostViewAllTestUtils landedCostViewAllTestUtils;

    public DObLandedCostViewAllStep() {
        Given(
                "^the all exisitng LandedCosts are as follows:$",
                (DataTable landedCostsDT) -> {
                    landedCostViewAllTestUtils.assertThatLandedCostsAreExist(landedCostsDT);
                });

        Given(
                "^the total number of all exisitng LandedCosts records are (\\d+)$",
                (Integer numberOfLandedCosts) -> {
                    landedCostViewAllTestUtils.assertThatNumberOfSavedLandedCostsEqual(numberOfLandedCosts);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of LandedCosts with no filter applied (\\d+) records per page$",
                (String userName, Integer pageNumer, Integer recordsNumber) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response response =
                            landedCostViewAllTestUtils.requestToReadFilteredData(
                                    userCookie,
                                    IDObLandedCostRestUrls.VIEW_ALL_LANDED_COSTS,
                                    pageNumer,
                                    recordsNumber,
                                    "");
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following LandedCosts will be presented to \"([^\"]*)\":$",
                (String userName, DataTable landedCostsDT) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    landedCostViewAllTestUtils.assertResponseSuccessStatus(response);
                    landedCostViewAllTestUtils.assertThatLandedCostMatches(response, landedCostsDT);
                });
        Then(
                "^the number of records in search results by \"([^\"]*)\" on page \"([^\"]*)\" are (\\d+)$",
                (String userName, String pageNumber, Integer numberOfRecords) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    landedCostViewAllTestUtils.assertThatNumberOfRecordDisplayIs(response, numberOfRecords);
                });

        Then(
                "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
                (String userName, Integer recordsTotalNumber) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    landedCostViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
                            response, recordsTotalNumber);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of LandedCosts with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page$",
                (String userName, Integer pageNumber, String code, Integer pageSize) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

                    String filterString =
                            landedCostViewAllTestUtils.getContainsFilterField(
                                    IDObLandedCostGeneralModel.CODE, code);

                    Response response =
                            landedCostViewAllTestUtils.requestToReadFilteredData(
                                    userCookie,
                                    IDObLandedCostRestUrls.VIEW_ALL_LANDED_COSTS,
                                    pageNumber,
                                    pageSize,
                                    filterString);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of LandedCosts with filter applied on State which equals \"([^\"]*)\" with (\\d+) records per page$",
                (String userName, Integer pageNumber, String state, Integer pageSize) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

                    String filterString =
                            landedCostViewAllTestUtils.getContainsFilterField(
                                    IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME, state);

                    Response response =
                            landedCostViewAllTestUtils.requestToReadFilteredData(
                                    userCookie,
                                    IDObLandedCostRestUrls.VIEW_ALL_LANDED_COSTS,
                                    pageNumber,
                                    pageSize,
                                    filterString);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of LandedCosts with filter applied on CreationDate which equals \"([^\"]*)\" with (\\d+) records per page$",
                (String userName, Integer pageNumber, String creationDate, Integer pageSize) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

                    String filterString =
                            landedCostViewAllTestUtils.getContainsFilterField(
                                    IDObLandedCostGeneralModel.CREATION_DATE,
                                    String.valueOf(
                                            landedCostViewAllTestUtils.getDateTimeInMilliSecondsFormat(creationDate)));

                    Response response =
                            landedCostViewAllTestUtils.requestToReadFilteredData(
                                    userCookie,
                                    IDObLandedCostRestUrls.VIEW_ALL_LANDED_COSTS,
                                    pageNumber,
                                    pageSize,
                                    filterString);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of LandedCosts with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
                (String userName, Integer pageNumber, String type, Integer pageSize, String locale) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

                    String filterString =
                            landedCostViewAllTestUtils.getEqualsFilterField(
                                    IDObLandedCostGeneralModel.TYPE_CODE, type);

                    Response response =
                            landedCostViewAllTestUtils.requestToReadFilteredDataWithLocale(
                                    userCookie,
                                    IDObLandedCostRestUrls.VIEW_ALL_LANDED_COSTS,
                                    pageNumber,
                                    pageSize,
                                    filterString,
                                    locale);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of LandedCosts with filter applied on PurchaseOrder which contains \"([^\"]*)\" with (\\d+) records per page$",
                (String userName, Integer pageNumber, String purcahseOrder, Integer pageSize) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

                    String filterString =
                            landedCostViewAllTestUtils.getContainsFilterField(
                                    IDObLandedCostGeneralModel.PURCHASE_ORDER_CODE, purcahseOrder);

                    Response response =
                            landedCostViewAllTestUtils.requestToReadFilteredData(
                                    userCookie,
                                    IDObLandedCostRestUrls.VIEW_ALL_LANDED_COSTS,
                                    pageNumber,
                                    pageSize,
                                    filterString);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        When(
                "^\"([^\"]*)\" requests to read page (\\d+) of LandedCosts with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
                (String userName,
                 Integer pageNumber,
                 String businessUnit,
                 Integer pageSize,
                 String locale) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

                    String filterString =
                            landedCostViewAllTestUtils.getEqualsFilterField(
                                    IDObLandedCostGeneralModel.PURCHASE_UNIT_CODE, businessUnit);

                    Response response =
                            landedCostViewAllTestUtils.requestToReadFilteredDataWithLocale(
                                    userCookie,
                                    IDObLandedCostRestUrls.VIEW_ALL_LANDED_COSTS,
                                    pageNumber,
                                    pageSize,
                                    filterString,
                                    locale);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^no values are displayed to \"([^\"]*)\" and empty grid state is viewed in Home Screen$",
                (String userName) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    landedCostViewAllTestUtils.assertThatNumberOfRecordDisplayIs(response, 0);
                });

    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAEM)) {
            databaseConnector.createConnection();
            databaseConnector.executeSQLScript(AccountingDocumentCommonTestUtils.clearAccountingDocuments());
            databaseConnector.executeSQLScript(getLandedCostDbScriptsOneTimeExecution());

        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), SCENARIO_NAEM)) {
            databaseConnector.closeConnection();
        }
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        landedCostViewAllTestUtils = new DObLandedCostViewAllTestUtils(properties);
        landedCostViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
 }
}
