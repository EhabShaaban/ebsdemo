package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.vendorinvoice.IIObVendorInvoiceDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObVendorInvoiceViewDetailsTestUtils extends DObVendorInvoiceCommonTestUtils {

  public DObVendorInvoiceViewDetailsTestUtils(
      Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    return str.toString();
  }

  public Response requestToReadInvoiceDetails(Cookie cookie, String invoiceCode) {
    String restURL =
        IDObVendorInvoiceRestUrls.GET_URL
            + invoiceCode
            + IDObVendorInvoiceRestUrls.INVOICE_DETAILS_SECTION;
    return sendGETRequest(cookie, restURL);
  }

  public void assertInvoicesExistWithDetails(DataTable invoicesDetailsDataTable) {

    List<Map<String, String>> invoiceDetails =
        invoicesDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoiceDetails) {
      IObVendorInvoiceDetailsGeneralModel invoiceDetailsGeneralModel = getInvoiceDetails(invoice);
      assertNotNull(invoiceDetailsGeneralModel);
      assertThatDownPaymentsAreEquals(
          convertEmptyStringToNull(invoice.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT)),
          invoiceDetailsGeneralModel.getDownPayment());
    }
  }

  private IObVendorInvoiceDetailsGeneralModel getInvoiceDetails(Map<String, String> invoice) {

    IObVendorInvoiceDetailsGeneralModel iObInvoiceDetailsGeneralModel =
        (IObVendorInvoiceDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getInvoiceDetails",
                invoice.get(IFeatureFileCommonKeys.CODE),
                invoice.get(IFeatureFileCommonKeys.VENDOR_CODE),
                invoice.get(IFeatureFileCommonKeys.VENDOR_NAME),
                invoice.get(IAccountingFeatureFileCommonKeys.PAYMENT_TERMS),
                invoice.get(IAccountingFeatureFileCommonKeys.CURRENCY),
                invoice.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER),
                invoice.get(IAccountingFeatureFileCommonKeys.INVOICE_DATE),
                invoice.get(IFeatureFileCommonKeys.PO_CODE),
                invoice.get(IFeatureFileCommonKeys.PURCHASE_UNIT));
    return iObInvoiceDetailsGeneralModel;
  }

  public void assertThatInvoiceDetailsMatches(
      DataTable invoicesDetailsDataTable, Response response) {
    Map<String, String> expectedInvoice =
        invoicesDetailsDataTable.asMaps(String.class, String.class).get(0);
    assertResponseSuccessStatus(response);
    HashMap<String, Object> actualInvoice = getMapFromResponse(response);

    assertThatActualInvoiceMatchesExpected(expectedInvoice, actualInvoice);
  }

  private void assertThatActualInvoiceMatchesExpected(
      Map<String, String> expectedInvoice, HashMap<String, Object> actualInvoice) {
    MapAssertion mapAssertion = new MapAssertion(expectedInvoice, actualInvoice);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.PO_CODE, IIObVendorInvoiceDetailsGeneralModel.PURCHASE_ORDER_CODE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.VENDOR_CODE, IIObVendorInvoiceDetailsGeneralModel.VENDOR_CODE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.VENDOR_NAME, IIObVendorInvoiceDetailsGeneralModel.VENDOR_NAME);

    mapAssertion.assertLocalizedValue(
        IAccountingFeatureFileCommonKeys.PAYMENT_TERMS,
        IIObVendorInvoiceDetailsGeneralModel.PAYMENT_TERM);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.INVOICE_NUMBER,
        IIObVendorInvoiceDetailsGeneralModel.INVOICE_NUMBER);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.CURRENCY,
        IIObVendorInvoiceDetailsGeneralModel.CURRENCY_ISO);

    assertThatDownPaymentsAreEquals(
        expectedInvoice.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT),
        (String) actualInvoice.get(IIObVendorInvoiceDetailsGeneralModel.DOWN_PAYMENT));

    mapAssertion.assertDateTime(
        IAccountingFeatureFileCommonKeys.INVOICE_DATE,
        IIObVendorInvoiceDetailsGeneralModel.INVOICE_DATE);
  }
}
