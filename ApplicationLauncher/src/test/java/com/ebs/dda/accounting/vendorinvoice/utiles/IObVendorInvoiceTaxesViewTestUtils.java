package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.accounting.taxes.ILObCompanyTaxesGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IObVendorInvoiceTaxesViewTestUtils extends DObVendorInvoiceTestUtils {

	public IObVendorInvoiceTaxesViewTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public void assertCorrectValuesAreDisplayedInTaxesSection(Response response,
					DataTable taxValuesDataTable) throws Exception {
		List<Map<String, String>> expectedTaxes = taxValuesDataTable.asMaps(String.class,
						String.class);
		List<HashMap<String, Object>> actualTaxes = getListOfMapsFromResponse(response);

		for (int i = 0; i < expectedTaxes.size(); i++) {
			assertActualAndExpectedDataFromResponse(expectedTaxes.get(i), actualTaxes.get(i));
		}
	}

	private void assertActualAndExpectedDataFromResponse(
					Map<String, String> expectedCompanyTaxDetail,
					HashMap<String, Object> actualCompanyTaxDetail) throws Exception {
		MapAssertion mapAssertion = new MapAssertion(expectedCompanyTaxDetail,
						actualCompanyTaxDetail);
		mapAssertion.assertCodeAndLocalizedNameValue(
				IAccountingFeatureFileCommonKeys.TAX_NAME,
				ILObCompanyTaxesGeneralModel.TAX_CODE,
				ILObCompanyTaxesGeneralModel.TAX_NAME);

		mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE,
						IIObInvoiceTaxesGeneralModel.TAX_Percentage);

		mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TAX_AMOUNT,
						IIObInvoiceTaxesGeneralModel.TAX_AMOUNT);
	}

    public void insertVendorInvoiceTaxes(DataTable vendorInvoiceTaxesDataTable) {
		List<Map<String, String>> vendorInvoiceTaxesList =
				vendorInvoiceTaxesDataTable.asMaps(String.class, String.class);
		for (Map<String, String> vendorInvoiceTax : vendorInvoiceTaxesList) {

			entityManagerDatabaseConnector.executeInsertQuery("insertVendorInvoiceTax",
					constructVITaxInsertionParams(vendorInvoiceTax));

		}
    }

	private Object[] constructVITaxInsertionParams(Map<String, String> vendorInvoiceTax) {
		String [] tax = vendorInvoiceTax.get(IAccountingFeatureFileCommonKeys.TAX).split(" - ");
		String taxCode = tax[0];
		String taxName = tax[1];
		return new Object[]{
				vendorInvoiceTax.get(IFeatureFileCommonKeys.CODE),
				taxCode,
				taxName,
				taxName,
				vendorInvoiceTax.get(IFeatureFileCommonKeys.CODE),
				vendorInvoiceTax.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE),
				vendorInvoiceTax.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT),
		};
	}
}
