package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObPaymentRequestPostValidationTestUtils extends DObPaymentRequestPostCommonTestUtils {
  private final String MISSING_FIELDS_KEY = "missingFields";
  private final String INVOICE_TYPE = "VendorInvoice";

  public DObPaymentRequestPostValidationTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public void assertThatTheFollowingPaymentRequestExist(DataTable paymentRequestDataTable) {
    List<Map<String, String>> paymentRequestListMap =
        paymentRequestDataTable.asMaps(String.class, String.class);
    for (Map<String, String> paymentRequestMap : paymentRequestListMap) {
      DObPaymentRequest paymentRequest =
          (DObPaymentRequest)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPaymentRequest", constructPaymentRequestQueryParams(paymentRequestMap));
      Assert.assertNotNull(
          "Payment Request doesn't exists " + paymentRequestMap.get(IFeatureFileCommonKeys.CODE),
          paymentRequest);
    }
  }

  private Object[] constructPaymentRequestQueryParams(Map<String, String> prPaymentDetailsMap) {
    return new Object[] {
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.CODE),
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + prPaymentDetailsMap.get(IFeatureFileCommonKeys.STATE) + "%",
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.TYPE)
    };
  }

  private Object[] getSubAccountParameters(Map<String, String> subAccount) {
    return new Object[] {
      subAccount.get(IAccountingFeatureFileCommonKeys.Leadger),
      subAccount.get(IAccountingFeatureFileCommonKeys.ACCOUNT_CODE)
    };
  }

  // TODO: Remove this method and use assertThatResponseHasMissingFields in CommonTestUtils
  public void assertThatResponseHasMissingFields(Response response, String missingFields) {
    Map<String, List<String>> missingFieldsList =
        response.body().jsonPath().get(MISSING_FIELDS_KEY);

    Set<String> actualList = new HashSet<>();
    String[] expectedList = missingFields.split(",");

    for (int i = 0; i < expectedList.length; i++) expectedList[i] = expectedList[i].trim();

    for (Map.Entry<String, List<String>> entry : missingFieldsList.entrySet()) {
      actualList.addAll(entry.getValue());
    }
    assertEquals(expectedList.length, actualList.size());
    Assert.assertTrue(actualList.containsAll(Arrays.asList(expectedList)));
  }

  public Response openPaymentRequestSection(
      Cookie cookie, String sectionName, String paymentRequestCode) {
    String url = null;
    switch (sectionName) {
      case "accountingDetails":
        url = getLockAccountingDetailsUrl(paymentRequestCode);
        break;
      case "paymentDetails":
        url = getLockPaymentDetailsUrl(paymentRequestCode);
        break;
    }

    return sendGETRequest(cookie, url);
  }

  private String getLockPaymentDetailsUrl(String paymentRequestCode) {
    return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND
        + "/"
        + paymentRequestCode
        + IDObPaymentRequestRestURLs.LOCK_PAYMENT_DETAILS_SECTION;
  }

  private String getLockAccountingDetailsUrl(String paymentRequestCode) {
    return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND
        + "/"
        + paymentRequestCode
        + IDObPaymentRequestRestURLs.LOCK_ACCOUNTING_DETAILS_SECTION;
  }

  public void assertThatInvoiceRemainingIsCorrect(String invoiceCode, BigDecimal remainingAmount) {
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        (IObInvoiceSummaryGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getInvoiceSummaryByCodeAndRemaining", invoiceCode, INVOICE_TYPE);
    BigDecimal invoiceRemaining =
        new BigDecimal(invoiceSummaryGeneralModel.getTotalRemaining());
    assertEquals(
        remainingAmount.compareTo(invoiceRemaining),
        0,
        "Vendor Invoice Remaining is not Correct for " + invoiceCode);
  }

}
