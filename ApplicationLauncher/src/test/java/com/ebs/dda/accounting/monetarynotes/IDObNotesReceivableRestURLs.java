package com.ebs.dda.accounting.monetarynotes;

public interface IDObNotesReceivableRestURLs {

  String BASE_COMMAND_URL = "/command/accounting/notesreceivables";
  String QUERY = "/services/accounting/notesreceivables";
  String DEPOT_QUERY = "/services/masterdataobjects/depot/";


  String CREATE_URL = BASE_COMMAND_URL + "/";

  String AUTHORIZED_ACTIONS = QUERY + "/authorizedactions";
  String POST = "/activate";

  String VIEW_GENERAL_DATA = "/generaldata";
  String VIEW_COMPANY_DATA = "/companydata";
  String VIEW_REFERENCE_DOCUMENTS = "/referencedocuments";
  String READ_ALL_REFERENCE_DOCUMENT_TYPES = QUERY + "/referencedocumenttypes";
  String READ_ALL_REFERENCE_DOCUMENT_SALES_ORDERS = "/salesorders";
  String READ_ALL_REFERENCE_DOCUMENT_SALES_INVOICES = "/salesinvoices";
  String READ_ALL_REFERENCE_DOCUMENT_NOTES_RECEIVABLES = "/notesreceivables";
  String VIEW_NOTES_DETAILS = "/notesdetails";
  String ACCOUNTING_DETAILS = "/accountingdetails";
  String VIEW_ACTIVATION_DETAILS = QUERY + "/%s/postingdetails";

  String UPDATE_NOTES_DETAILS_SECTION = "/update/notesdetails";


  String READ_ALL_TYPES = QUERY + "/types";
  String READ_ALL_FORMS = QUERY + "/forms";

  String VIEW_ALL = QUERY;
  String LOCK_NOTES_DETAILS_SECTION = "/lock/notesdetails";
  String UNLOCK_NOTES_DETAILS_SECTION = "/unlock/notesdetails";
  String LOCK_REFERENCE_DOCUMENTS_SECTION = "/lock/referencedocuments";
  String UNLOCK_REFERENCE_DOCUMENTS_SECTION = "/unlock/referencedocuments";
  String UPDATE_REFERENCE_DOCUMENTS = "/update/referencedocuments";
  String DELETE_REFERENCE_DOCUMENTS = "/delete/referencedocuments";
  String UNLOCK_LOCKED_SECTIONS = BASE_COMMAND_URL + "/unlock/lockedsections/";



}
