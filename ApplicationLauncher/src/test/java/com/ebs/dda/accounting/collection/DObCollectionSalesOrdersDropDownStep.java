package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionSalesOrdersDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionSalesOrdersDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObCollectionSalesOrdersDropDownTestUtils utils;

  public DObCollectionSalesOrdersDropDownStep() {
    Given(
        "^the following SalesOrders exist:$",
        (DataTable salesOrders) -> {
          utils.assertThatSalesOrdersAreExist(salesOrders);
        });

    When(
            "^\"([^\"]*)\" requests to read all SalesOrders with state GoodsIssueActivated or Approved where remaining is greater than Zero according to user's BusinessUnit$",
            (String userName) -> {
                String url = IDObCollectionRestURLs.VIEW_SALES_ORDERS;
                Response response =
                        utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
                userActionsTestUtils.setUserResponse(userName, response);
            });

    Then(
        "^the following SalesOrders for Collections values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable SalesOrdersDT) -> {
          utils.assertThatSalesOrdersAreRetrieved(
              userActionsTestUtils.getUserResponse(userName), SalesOrdersDT);
        });

    Then(
        "^total number of SalesOrders for Collections returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer numberOfReturnedSalesOrders) -> {
          utils.assertTotalNumberOfRecordsEqualResponseSize(
              userActionsTestUtils.getUserResponse(userName), numberOfReturnedSalesOrders);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObCollectionSalesOrdersDropDownTestUtils(properties);
    utils.setProperties(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read All SalesOrders Dropdown in Collection")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read All SalesOrders Dropdown in Collection")) {
      databaseConnector.closeConnection();
    }
  }
}
