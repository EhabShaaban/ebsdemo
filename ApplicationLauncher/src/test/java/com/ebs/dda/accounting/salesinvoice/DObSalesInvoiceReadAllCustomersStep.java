package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceReadAllCustomersUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceReadAllCustomersStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceReadAllCustomersUtils salesInvoiceReadAllCustomersUtils;

  public DObSalesInvoiceReadAllCustomersStep() {

    When(
        "^\"([^\"]*)\" requests to read all Customers for selected BU \"([^\"]*)\"$",
        (String userName, String businessUnitCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String readCustomerUrl = IDObSalesInvoiceRestUrls.READ_CUSTOMER_URL + businessUnitCode;
          Response response =
              salesInvoiceReadAllCustomersUtils.sendGETRequest(cookie, readCustomerUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Customers values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable customerDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceReadAllCustomersUtils.assertThatResponseCustomerDataIsCorrect(
              response, customerDataTable);
        });
      Then("^total number of Customers returned to \"([^\"]*)\" is equal to (\\d+)$",  (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceReadAllCustomersUtils.assertTotalNumberOfRecordsEqualResponseSize(response, totalRecordsNumber);
      });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    salesInvoiceReadAllCustomersUtils =
        new DObSalesInvoiceReadAllCustomersUtils(properties, entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "Read list of Customers dropdown")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            salesInvoiceReadAllCustomersUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(salesInvoiceReadAllCustomersUtils.getDbScriptPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read list of Customers dropdown")) {
      databaseConnector.closeConnection();
    }
  }
}
