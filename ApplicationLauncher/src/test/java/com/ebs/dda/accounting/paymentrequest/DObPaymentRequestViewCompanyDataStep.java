package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewCompanyDataTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestViewCompanyDataStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObPaymentRequestViewCompanyDataTestUtils utils;

  public DObPaymentRequestViewCompanyDataStep() {

    Given(
        "^the following CompanyData for PaymentRequests exist:$",
        (DataTable companyDataTable) -> {
          utils.assertThatTheFollowingPRCompanyDataExists(companyDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view CompanyData section of PaymentRequest with code \"([^\"]*)\"$",
        (String userName, String prCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.getViewCompanyDataUrl(prCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of CompanyData section for PaymentRequest with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String prCode, String userName, DataTable generalDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatViewCompanyDataResponseIsCorrect(response, generalDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestViewCompanyDataTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewGeneralData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View PR CompanyData,")) {
      databaseConnector.executeSQLScript(
          DObPaymentRequestViewGeneralDataTestUtils.getPaymentRequestDbScriptsOneTimeExecution());
        databaseConnector
                .executeSQLScript(DObPaymentRequestViewGeneralDataTestUtils.clearPaymentRequests());
    }
  }
}
