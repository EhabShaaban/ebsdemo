package com.ebs.dda.accounting.landedcost;

public interface IDObLandedCostRestUrls {

  String GET_URL = "/services/accounting/landedcost";
  String COMMAND_URL = "/command/landedcost";
  String UNLOCK_LOCKED_SECTIONS = COMMAND_URL + "/unlock/lockedsections/";
  String AUTHORIZED_ACTIONS = GET_URL + "/authorizedactions";
  String GENERAL_DATA = "/generaldata";
  String COMPANY_DATA = "/companydata";
  String LANDED_COST_DETAILS = "/landedcostdetails";
  String VIEW_LANDED_CALCULATION_COST_STATUS = "/calculation-cost-status";
  String COST_FACTOR_ITEMS = "/costfactoritems";
  String COST_FACTOR_ITEMS_SUMMARY = "/costfactoritemssummary";
  String VIEW_ALL_LANDED_COSTS = GET_URL;
  String VIEW_LC_TYPES = "/types";
  String LOCK_LANDED_COST_DETAILS_SECTION = "/lock/landedcostdetails/";
  String UNLOCK_LANDED_COST_DETAILS_SECTION = "/unlock/landedcostdetails/";
  String LOCK_LANDED_COST_FACTOR_ITEMS_SECTION = "/lock/items/";
  String UNLOCK_LANDED_COST_FACTOR_ITEMS_SECTION = "/unlock/items/";
  String UPDATE_LANDED_COST_FACTOR_ITEMS_SECTION = "/update/items/";
  String READ_SERVICE_ITEMS = "/serviceitems";

  String CREATE_URL = COMMAND_URL;
  String VIEW_LC_POS = GET_URL + "/purchase-orders";
  String REQUEST_CREATE_URL = COMMAND_URL;

  String CONVERT_TO_ESTIMATE_LANDED_COST = "/convert-to-estimate";
  String CONVERT_TO_ACTUAL_LANDED_COST = "/convert-to-actual";
}
