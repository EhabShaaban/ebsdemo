package com.ebs.dda.accounting.initialbalanceupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.initialbalanceupload.utils.DObInitialBalanceUploadCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObInitialBalanceUploadCommonSteps extends SpringBootRunner implements En {

    private DObInitialBalanceUploadCommonTestUtils utils;
    public DObInitialBalanceUploadCommonSteps(){
        Given("^the following GeneralData for InitialBalanceUploads exist:$", (DataTable generalDataTable) -> {
            utils.createInitialBalanceUploadGeneralData(generalDataTable);
        });

        Given("^the following CompanyData for InitialBalanceUploads exist:$", (DataTable companyDataTable) -> {
            utils.createInitialBalanceUploadCompany(companyDataTable);
        });

        Given("^the following ItemsData for InitialBalanceUploads exist:$", (DataTable ItemsDataTable) -> {
           utils.createInitialBalanceUploadItem(ItemsDataTable);
        });
    }
    @Before
    public void setup() throws Exception {
        utils = new DObInitialBalanceUploadCommonTestUtils();
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }
}
