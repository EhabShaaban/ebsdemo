package com.ebs.dda.accounting.settlement;

import static com.ebs.dda.accounting.settlement.IDObSettlementRestURLs.LOCK_ACCOUNTING_DETAILS;
import static com.ebs.dda.accounting.settlement.IDObSettlementRestURLs.UNLOCK_LOCKED_SECTIONS;
import static com.ebs.dda.accounting.settlement.apis.IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION;

import java.util.Arrays;
import java.util.Map;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementDeleteAccountingDetailsTestUtils;
import com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementDeleteAccountingDetailsStep extends SpringBootRunner implements En {

	private static final String SCENARIO_NAME = "Delete Settlement AccountingDetails";

	private static boolean hasBeenExecuted = false;
	private DObSettlementDeleteAccountingDetailsTestUtils utils;

	public DObSettlementDeleteAccountingDetailsStep() {
		When("^\"([^\"]*)\" requests to delete AccountingDetail with id \"([^\"]*)\" in Settlement with code \"([^\"]*)\"$",
						(String userName, String accountingDetailId, String settlementCode) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							String restUrl = utils.getDeleteAccountingDetailRestUrl(
											accountingDetailId, settlementCode);
							Response response = utils.sendDeleteRequest(userCookie, restUrl);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^AccountingDetail with id \"([^\"]*)\" from Settlement with code \"([^\"]*)\" is deleted$",
						(String detailId, String settlementCode) -> {
							utils.assertThatSettlementAccountingDetailDoesNotExist(detailId,
											settlementCode);
						});

		Given("^\"([^\"]*)\" first deleted AccountingDetail with id \"([^\"]*)\" of Settlement with code \"([^\"]*)\" successfully$",
						(String userName, String accountingDetailId, String settlementCode) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							String restUrl = utils.getDeleteAccountingDetailRestUrl(
											accountingDetailId, settlementCode);
							Response response = utils.sendDeleteRequest(userCookie, restUrl);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		When("^\"([^\"]*)\" first requested to add a record to AccountingDetails section of Settlement with code \"([^\"]*)\"$",
						(String username, String settlementCode) -> {
							String lockUrl = String.format(LOCK_ACCOUNTING_DETAILS, settlementCode);
							Response response = userActionsTestUtils.lockSection(username, lockUrl,
											settlementCode);
							userActionsTestUtils.setUserResponse(username, response);
						});
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		utils = new DObSettlementDeleteAccountingDetailsTestUtils(properties,
						entityManagerDatabaseConnector);
	}

	@Before
	public void setup(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {

			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				databaseConnector.executeSQLScript(DObSettlementsCommonTestUtils
								.getSettlementDbScriptsOneTimeExecution());
				utils.executeAccountDeterminationFunction();
				hasBeenExecuted = true;
			}
			databaseConnector.executeSQLScript(DObSettlementsCommonTestUtils.clearSettlement());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			utils.unfreeze();

			userActionsTestUtils.unlockAllSections(UNLOCK_LOCKED_SECTIONS,
							Arrays.asList(ACCOUNTING_DETAILS_SECTION));

			databaseConnector.closeConnection();
		}
	}
}
