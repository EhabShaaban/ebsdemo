package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceRequestAddItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesInvoiceRequestAddItemStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObSalesInvoiceRequestAddItemTestUtils iObSalesInvoiceRequestAddItemTestUtils;

  public IObSalesInvoiceRequestAddItemStep() {

    When(
        "^\"([^\"]*)\" requests to add Item to SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          String lockUrl = iObSalesInvoiceRequestAddItemTestUtils.getLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new add Item dialoge is opened and SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          iObSalesInvoiceRequestAddItemTestUtils.assertResponseSuccessStatus(response);
          iObSalesInvoiceRequestAddItemTestUtils.assertThatResourceIsLockedByUser(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first requested to add Item to SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" successfully$",
        (String userName, String salesInvoiceCode) -> {
          String lockUrl = iObSalesInvoiceRequestAddItemTestUtils.getLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName) -> {
          iObSalesInvoiceRequestAddItemTestUtils.assertThatLockIsReleased(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);
        });

    Given(
        "^SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName, String lockTime) -> {
          iObSalesInvoiceRequestAddItemTestUtils.freeze(lockTime);
          String lockUrl = iObSalesInvoiceRequestAddItemTestUtils.getLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode, String unlockTime) -> {
          iObSalesInvoiceRequestAddItemTestUtils.freeze(unlockTime);
          String unLockUrl = iObSalesInvoiceRequestAddItemTestUtils.getUnLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\" is released$",
        (String userName, String salesInvoiceCode) -> {
          iObSalesInvoiceRequestAddItemTestUtils.assertThatLockIsReleased(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving SalesInvoiceItems section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          String unLockUrl = iObSalesInvoiceRequestAddItemTestUtils.getUnLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    iObSalesInvoiceRequestAddItemTestUtils =
        new IObSalesInvoiceRequestAddItemTestUtils(properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Request to Add Item to SalesInvoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            iObSalesInvoiceRequestAddItemTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(iObSalesInvoiceRequestAddItemTestUtils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Add Item to SalesInvoice")) {

        userActionsTestUtils.unlockAllSections(
                IDObSalesInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS, iObSalesInvoiceRequestAddItemTestUtils.getSectionsNames());


        databaseConnector.closeConnection();
    }
  }
}
