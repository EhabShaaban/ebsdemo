package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class DObVendorInvoiceViewGeneralDataStep extends SpringBootRunner
    implements En, InitializingBean {

    public static final String SCENARIO_NAME = "View GeneralData section of VendorInvoice";
  private DObVendorInvoiceViewGeneralDataTestUtils invoiceGeneralDataViewTestUtils;

  public DObVendorInvoiceViewGeneralDataStep() {
    Given(
        "^the following GeneralData for Invoices exist:$",
        (DataTable itemGeneralDataTable) -> {
          invoiceGeneralDataViewTestUtils.assertInvoicesExistWithGeneralData(itemGeneralDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of Invoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              invoiceGeneralDataViewTestUtils.requestToReadGeneralData(cookie, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of GeneralData section for VendorInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String invoiceCode, String userName, DataTable invoicesGeneralData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          invoiceGeneralDataViewTestUtils.assertThatInvoiceGeneralDataMatches(
              invoicesGeneralData, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    invoiceGeneralDataViewTestUtils = new DObVendorInvoiceViewGeneralDataTestUtils(properties);
    invoiceGeneralDataViewTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          invoiceGeneralDataViewTestUtils.getDbScriptsOneTimeExecution());

        databaseConnector.executeSQLScript( DObVendorInvoiceTestUtils.clearVendorInvoices());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
