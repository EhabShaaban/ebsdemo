package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.IIObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.IIObInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Assert;

public class IObSalesInvoiceItemsViewTestUtils extends IObSalesInvoiceItemsTestUtils {

  public IObSalesInvoiceItemsViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder(getMasterDataScripts());

    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");

    return str.toString();
  }
  public void assertThatSalesInvoiceHasNoItems(DataTable itemsTable) {
    List<Map<String, String>> salesInvoiceItems = itemsTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceItem : salesInvoiceItems) {
      assertThatSalesInvoiceHasNoItem(salesInvoiceItem.get(ISalesFeatureFileCommonKeys.SI_CODE));
    }
  }

  private void assertThatSalesInvoiceHasNoItem(String salesInvoiceCode) {
    IObSalesInvoiceItemsGeneralModel salesInvoicesItems =
        (IObSalesInvoiceItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesItemsBySICode", new Object[] {salesInvoiceCode});
    assertNull(salesInvoicesItems);
  }

  public void assertThatSalesInvoicesHaveItems(DataTable salesInvoiceItemsTable) {
    List<Map<String, String>> salesInvoiceItems =
        salesInvoiceItemsTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceItem : salesInvoiceItems) {
      assertThatSalesInvoiceHasItem(salesInvoiceItem);
    }
  }

  public void assertThatSalesInvoiceHasItem(Map<String, String> salesInvoiceItem) {
    IObSalesInvoiceItemsGeneralModel getSalesInvoicesItems =
        (IObSalesInvoiceItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesItems", constructcollectionParameters(salesInvoiceItem));
    assertNotNull(getSalesInvoicesItems);
  }

  public Object[] constructcollectionParameters(Map<String, String> salesInvoiceItems) {
    return new Object[] {
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.SI_CODE),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.ITEM),
      salesInvoiceItems.get(ISalesFeatureFileCommonKeys.ORDER_UNIT_CODE_NAME),
      new BigDecimal(salesInvoiceItems.get(ISalesFeatureFileCommonKeys.QUANTITY)),
      new BigDecimal(salesInvoiceItems.get(ISalesFeatureFileCommonKeys.SALES_PRICE)),
      new BigDecimal(salesInvoiceItems.get(ISalesFeatureFileCommonKeys.TOTAL_AMOUNT))
    };
  }

  public Response requestToReadSalesInvoiceItems(Cookie cookie, String salesInvoiceCode) {
    String restURL =
        IDObSalesInvoiceRestUrls.GET_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.ITEMS_SECTION;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatInvoiceItemsMatch(
      String salesInvoiceCode, DataTable salesInvoicesItems, Response response) throws Exception {
    List<Map<String, String>> expectedInvoiceItems =
        salesInvoicesItems.asMaps(String.class, String.class);
    assertResponseSuccessStatus(response);
    List<HashMap<String, Object>> actualInvoiceItems = getListOfMapsFromResponse(response);
    int index = 0;
    for (HashMap<String, Object> actualInvoiceItem : actualInvoiceItems) {
      assertExpectedAndActualItemValues(
          salesInvoiceCode, expectedInvoiceItems.get(index++), actualInvoiceItem);
    }
  }

  private void assertExpectedAndActualItemValues(
      String salesInvoiceCode,
      Map<String, String> expectedInvoiceItem,
      HashMap<String, Object> actualInvoiceItem)
      throws Exception {
    assertEquals(
        salesInvoiceCode, actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.INVOICE_CODE));
    MapAssertion mapAssertion = new MapAssertion(expectedInvoiceItem, actualInvoiceItem);
    assertThatActualInvoiceItemMatchesExpected(mapAssertion);
  }
  public void assertThatActualInvoiceItemMatchesExpected(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        ISalesFeatureFileCommonKeys.ITEM,
        IIObInvoiceItemsGeneralModel.ITEM_CODE,
        IIObInvoiceItemsGeneralModel.ITEM_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(
        ISalesFeatureFileCommonKeys.ORDER_UNIT_CODE_NAME,
        IIObInvoiceItemsGeneralModel.ORDER_UNIT_CODE,
        IIObInvoiceItemsGeneralModel.ORDER_UNIT_NAME);
    mapAssertion.assertBigDecimalNumberValue(
        ISalesFeatureFileCommonKeys.QUANTITY, IIObInvoiceItemsGeneralModel.QUANTITY_IN_ORDER_UNIT);
    mapAssertion.assertBigDecimalNumberValue(
            IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT, IIObInvoiceItemsGeneralModel.ITEM_ORDER_UNIT_PRICE);
    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE, IIObInvoiceItemsGeneralModel.ITEM_PRICE);
    mapAssertion.assertBigDecimalNumberValue(
        ISalesFeatureFileCommonKeys.TOTAL_AMOUNT, IIObInvoiceItemsGeneralModel.TOTAL_AMOUNT);
  }

  public void assertThatInvoiceItemsAreEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualInvoiceItems =getListOfMapsFromResponse(response);
    assertTrue(actualInvoiceItems.isEmpty());
  }

  public void assertThatInvoiceTaxesMatch(
      String salesInvoiceCode, DataTable salesInvoiceTaxesSummary, Response response)
      throws Exception {
    List<Map<String, String>> expectedInvoiceTaxes =
        salesInvoiceTaxesSummary.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualInvoiceTaxes = getActualInvoiceTaxes(salesInvoiceCode, response);
    Assert.assertThat(expectedInvoiceTaxes,
        IsIterableContainingInAnyOrder.containsInAnyOrder(actualInvoiceTaxes.toArray()));
  }

  private List<HashMap<String, Object>> getActualInvoiceTaxes(String salesInvoiceCode, Response response) throws Exception {
    List<HashMap<String, Object>> actualInvoiceTaxes = getTaxesListFromResponse(response);
    List<HashMap<String, Object>> actualInvoiceTaxesList = new LinkedList<>();
    for (HashMap<String, Object> invoiceTaxMap : actualInvoiceTaxes) {
      LinkedHashMap taxName = (LinkedHashMap) invoiceTaxMap.get(IIObInvoiceTaxesGeneralModel.TAX_NAME);
      LinkedHashMap taxNameValues = (LinkedHashMap) taxName.get("values");
      String enTaxName = taxNameValues.get("en").toString();
      String tax = invoiceTaxMap.get(IIObInvoiceTaxesGeneralModel.TAX_CODE) +
          " - " +
          enTaxName;

      BigDecimal taxAmount=new BigDecimal(invoiceTaxMap.get(IIObInvoiceTaxesGeneralModel.TAX_AMOUNT).toString());

      HashMap<String, Object> modifiedInvoiceTaxMap = new HashMap<>();
      modifiedInvoiceTaxMap.put(IAccountingFeatureFileCommonKeys.TAX, tax);
      modifiedInvoiceTaxMap.put(IAccountingFeatureFileCommonKeys.TAX_AMOUNT, taxAmount);

      assertEquals(
          salesInvoiceCode,
          invoiceTaxMap.get(IIObInvoiceTaxesGeneralModel.SI_CODE));

      actualInvoiceTaxesList.add(modifiedInvoiceTaxMap);
    }
    return actualInvoiceTaxesList;
  }

  private List<HashMap<String, Object>> getTaxesListFromResponse(Response response)
      throws Exception {
    HashMap<String, Object> actualInvoiceTaxResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualInvoiceTaxResponse, "Taxes");
  }

  public void assertThatInvoiceSummaryMatches(
      String salesInvoiceCode, DataTable salesInvoiceSummary, Response response) throws Exception {
    Map<String, String> expectedInvoiceSummary =
        salesInvoiceSummary.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualInvoiceSummary = getItemSummaryMapFromResponse(response);
    assertExpectedAndActualInvoiceSummaryValues(
        salesInvoiceCode, expectedInvoiceSummary, actualInvoiceSummary);
  }

  private void assertExpectedAndActualInvoiceSummaryValues(
      String salesInvoiceCode,
      Map<String, String> expectedInvoiceSummary,
      HashMap<String, Object> actualInvoiceSummary) {
    assertEquals(
        salesInvoiceCode,
        actualInvoiceSummary.get(IIObInvoiceSummaryGeneralModel.SI_CODE));
    MapAssertion mapAssertion = new MapAssertion(expectedInvoiceSummary, actualInvoiceSummary);
    assertThatActualInvoiceSummaryMatchesExpected(mapAssertion);
  }

  private HashMap<String, Object> getItemSummaryMapFromResponse(Response response)
      throws Exception {
    HashMap<String, Object> actualInvoiceResponse = getMapFromResponse(response);
    return readMapFromString(actualInvoiceResponse, "ItemSummary");
  }

  public void assertThatActualInvoiceSummaryMatchesExpected(MapAssertion mapAssertion) {
    mapAssertion.assertBigDecimalNumberValue(
        ISalesFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES,
        IIObInvoiceSummaryGeneralModel.AMOUNT_BEFORE_TAXES);
    mapAssertion.assertBigDecimalNumberValue(
        ISalesFeatureFileCommonKeys.TOTAL_TAXES,
        IIObInvoiceSummaryGeneralModel.TAX_AMOUNT);
    mapAssertion.assertBigDecimalNumberValue(
        ISalesFeatureFileCommonKeys.TOTAL_AMOUNT_AFTER_TAXES,
        IIObInvoiceSummaryGeneralModel.NET_AMOUNT);
    mapAssertion.assertBigDecimalNumberValue(
        ISalesFeatureFileCommonKeys.REMAINING_AMOUNT,
        IIObInvoiceSummaryGeneralModel.REMAINING_AMOUNT);
  }

  public void assertCollectionsExistWithAmount(
      String salesInvoiceCode, DataTable collectionsTable) {
    List<Map<String, String>> collectionsMaps =
        collectionsTable.asMaps(String.class, String.class);
    for (Map<String, String> collection : collectionsMaps) {
      assertThatcollectionExistWithAmountAndSalesInvoice(
          salesInvoiceCode, collection);
    }
  }

  private void assertThatcollectionExistWithAmountAndSalesInvoice(
      String salesInvoiceCode, Map<String, String> collection) {
    DObCollectionGeneralModel collectionGeneralModel =
        (DObCollectionGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCollectionsByCodeAmountAndSalesInvoiceCode",
                constructcollectionParameters(salesInvoiceCode, collection));
    assertNotNull(collectionGeneralModel);
  }

  public Object[] constructcollectionParameters(
      String salesInvoiceCode, Map<String, String> colllection) {
    return new Object[] {
      colllection.get(IAccountingFeatureFileCommonKeys.C_CODE),
      new BigDecimal(colllection.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
      salesInvoiceCode
    };
  }

  public void assertNotesRecievablesExistWithAmount(
      String salesInvoiceCode, DataTable notesRecievablesMapTable) {
    List<Map<String, String>> notesRecievablesMaps =
        notesRecievablesMapTable.asMaps(String.class, String.class);
    for (Map<String, String> notesRecievable : notesRecievablesMaps) {
      assertThatNotesRecievableExistWithAmountAndSalesInvoice(salesInvoiceCode, notesRecievable);
    }
  }

  public void assertThatNotesRecievableExistWithAmountAndSalesInvoice(
      String salesInvoiceCode, Map<String, String> notesRecievables) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        (DObNotesReceivablesGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getNotesRecievablesByCodeAmountAndSalesInvoiceCode",
                constructNotesRecievableParameters(salesInvoiceCode, notesRecievables));
    assertNotNull(notesReceivablesGeneralModel);
  }

  private Object[] constructNotesRecievableParameters(
      String salesInvoiceCode, Map<String, String> notesRecievables) {
    return new Object[] {
      notesRecievables.get(IAccountingFeatureFileCommonKeys.NR_CODE),
      new BigDecimal(notesRecievables.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
      salesInvoiceCode
    };
  }

  public void assertThatSalesInvoiceHasNoTaxes(Response response) throws Exception {
    List<HashMap<String, Object>> actualInvoiceTaxes = getTaxesListFromResponse(response);
    assertTrue(actualInvoiceTaxes.isEmpty());
  }
}
