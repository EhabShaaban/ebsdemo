package com.ebs.dda.accounting.accountingnote.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class AccountingNoteCommonTestUtil extends CommonTestUtils {

  protected final String ASSERTION_MSG = "[Accounting][Note]";

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/order/salesreturnorder/Sales_Return_Clear.sql");
    str.append(",").append("db-scripts/accounting/accountingnote/Accounting_Notes_Clear.sql");
    return str.toString();
  }

  public void createAccountingNoteGeneralData(DataTable accountingNoteTable) {
    List<Map<String, String>> accountingNoteMaps =
        accountingNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingNoteMap : accountingNoteMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createAccountingNoteGeneralData", getGeneralDataCreateParameters(accountingNoteMap));
    }
  }

  private Object[] getGeneralDataCreateParameters(Map<String, String> accountingNoteMap) {
    String remainingMapValue = accountingNoteMap.get(IAccountingFeatureFileCommonKeys.REMAINING);
    return new Object[] {
      accountingNoteMap.get(IFeatureFileCommonKeys.CODE),
      accountingNoteMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      accountingNoteMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      accountingNoteMap.get(IFeatureFileCommonKeys.CREATED_BY),
      accountingNoteMap.get(IFeatureFileCommonKeys.CREATED_BY),
      accountingNoteMap.get(IFeatureFileCommonKeys.STATE),
      accountingNoteMap.get(IFeatureFileCommonKeys.TYPE),
            accountingNoteMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
            isMapValueEmptyOrNull(remainingMapValue) ? null : remainingMapValue,
      accountingNoteMap.get(IAccountingFeatureFileCommonKeys.NOTE_TYPE)
    };
  }

  public void createAccountingNoteCompanyData(DataTable accountingNoteTable) {
    List<Map<String, String>> accountingNoteMaps =
        accountingNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingNoteMap : accountingNoteMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createAccountingNoteCompanyData", getCompanyDataCreateParameters(accountingNoteMap));
    }
  }

  private Object[] getCompanyDataCreateParameters(Map<String, String> accountingNoteMap) {
    return new Object[] {
      accountingNoteMap.get(IFeatureFileCommonKeys.CODE),
      accountingNoteMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      accountingNoteMap.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void createAccountingNoteDetails(DataTable accountingNoteTable) {
    List<Map<String, String>> accountingNoteMaps =
        accountingNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingNoteMap : accountingNoteMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createAccountingNoteDetails",
          getAccountingNoteDetailsCreateParameters(accountingNoteMap));
    }
  }

  private Object[] getAccountingNoteDetailsCreateParameters(Map<String, String> accountingNoteMap) {
    String referenceDocumentParam =
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT);
    return new Object[] {
      isMapValueEmptyOrNull(referenceDocumentParam) ? null : referenceDocumentParam.split(" - ")[1],
      accountingNoteMap.get(IFeatureFileCommonKeys.CODE),
      accountingNoteMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      accountingNoteMap.get(IAccountingFeatureFileCommonKeys.AMOUNT),
      accountingNoteMap.get(IAccountingFeatureFileCommonKeys.CURRENCY),
    };
  }
  public void assertThatTotalNumberOfDocumentOwnersEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualDocumentOwners = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        "Total Number of Document Owners doesn't equal",
        expectedTotalNumber.intValue(),
        actualDocumentOwners.size());
  }
}
