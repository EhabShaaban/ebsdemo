package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.NotesReceivableReferenceDocumentTypeDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class NotesReceivableReferenceDocumentTypeDropdownStep extends SpringBootRunner
    implements En {

  private NotesReceivableReferenceDocumentTypeDropdownTestUtils utils;

  public NotesReceivableReferenceDocumentTypeDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all NotesReceivable ReferenceDocument Types$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllReferenceDocumentTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following NotesReceivable ReferenceDocument Types values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable referenceDocumentTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReferenceDocumentTypesResponseIsCorrect(referenceDocumentTypesDataTable, response);
        });

    Then(
        "^total number of NotesReceivable ReferenceDocument Types returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer referenceDocumentTypesCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfReferenceDocumentTypesEquals(referenceDocumentTypesCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new NotesReceivableReferenceDocumentTypeDropdownTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
