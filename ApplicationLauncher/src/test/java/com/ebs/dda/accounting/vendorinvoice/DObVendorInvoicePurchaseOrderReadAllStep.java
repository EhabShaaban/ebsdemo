package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPurchaseOrderReadAllTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewPurchaseOrderDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObVendorInvoicePurchaseOrderReadAllStep extends SpringBootRunner implements En, InitializingBean {

    private static boolean hasBeenExecutedOnce = false;
    private DObVendorInvoiceViewPurchaseOrderDropDownTestUtils utils;
    private DObPaymentRequestPurchaseOrderReadAllTestUtils paymentRequestPurchaseOrderUtils;


    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObVendorInvoiceViewPurchaseOrderDropDownTestUtils(getProperties(), entityManagerDatabaseConnector);
        paymentRequestPurchaseOrderUtils = new DObPaymentRequestPurchaseOrderReadAllTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObVendorInvoicePurchaseOrderReadAllStep() {

        When("^\"([^\"]*)\" requests to read all purchase orders After Selecting Vendor Invoice Type With Code \"([^\"]*)\"  & Vendor With Code \"([^\"]*)\"$",
                (String userName, String invoiceTypeCode, String vendorCode) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response response =
                            utils.sendGETRequest(cookie, IDObVendorInvoiceRestUrls.GET_PURCASE_ORDER_BASED_ON_TYPE
                                    + "/" + invoiceTypeCode
                                    + "/" + vendorCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then("^the following purchase orders should be returned in the DropDown to \"([^\"]*)\":$",
                (String userName, DataTable purchaseOrdersDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertThatPurchaseOrdersDataResponseAreCorrect(response, purchaseOrdersDataTable);
        });

        Then("^total number of purchase orders returned to \"([^\"]*)\" is equal to (\\d+)$", (String userName, Integer numberOfPurchaseOrders) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            paymentRequestPurchaseOrderUtils.assertThatTotalNumberOfReturnedPurchaseOrdersIsCorrect(response, numberOfPurchaseOrders);
        });



    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read All Vendor Invoice PurchaseOrders")) {
            databaseConnector.createConnection();

            databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
            hasBeenExecutedOnce = true;

        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read All Vendor Invoice PurchaseOrders")) {
            databaseConnector.closeConnection();
        }
    }
}
