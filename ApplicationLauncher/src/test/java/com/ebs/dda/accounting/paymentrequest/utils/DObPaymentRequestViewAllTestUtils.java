package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObPaymentRequestViewAllTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");

    return str.toString();
  }

  public static String clearPaymentRequests() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/payment-request/PaymentRequest_Clear.sql");
    return str.toString();
  }

  public void assertThatTheFollowingPaymentRequestExist(DataTable paymentRequestDataTable) {
    List<Map<String, String>> paymentRequestListMap =
        paymentRequestDataTable.asMaps(String.class, String.class);
    for (Map<String, String> paymentRequestMap : paymentRequestListMap) {
      DObPaymentRequestGeneralModel paymentRequest =
          (DObPaymentRequestGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPaymentRequestViewAll",
                  constructPaymentRequestQueryParams(paymentRequestMap));
      Assert.assertNotNull("Payment Request doesn't exists " + paymentRequestMap, paymentRequest);
    }
  }

  private Object[] constructPaymentRequestQueryParams(Map<String, String> paymentRequestMap) {
    String amountWithCurrency = paymentRequestMap.get(IAccountingFeatureFileCommonKeys.AMOUNT);
    String[] amountArray = amountWithCurrency.split(" ");

    return new Object[] {
      paymentRequestMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      new BigDecimal(amountArray[0]),
      amountArray[1],
      paymentRequestMap.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT),
      paymentRequestMap.get(IFeatureFileCommonKeys.CODE),
      paymentRequestMap.get(IFeatureFileCommonKeys.TYPE),
      paymentRequestMap.get(IFeatureFileCommonKeys.CREATED_BY),
      paymentRequestMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      "%" + paymentRequestMap.get(IFeatureFileCommonKeys.STATE) + "%",
    };
  }

  public void assertPaymentRequestDataIsCorrect(
      DataTable paymentRequestDataTable, Response response) throws Exception {
    List<Map<String, String>> paymentRequestList =
        paymentRequestDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> paymentRequestResponse =
        response.body().jsonPath().getList("data");
    for (int i = 0; i < paymentRequestList.size(); i++) {
      assertCorrectShownData(paymentRequestResponse.get(i), paymentRequestList.get(i));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> responsedPaymentRequest, Map<String, String> paymentRequestRowMap)
      throws Exception {
    MapAssertion mapAssertion = new MapAssertion(paymentRequestRowMap, responsedPaymentRequest);
    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObPaymentRequestGeneralModel.USER_CODE);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObPaymentRequestGeneralModel.PAYMENT_TYPE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER,
        IDObPaymentRequestGeneralModel.BUSINESS_PARTNER_CODE,
        IDObPaymentRequestGeneralModel.BUSINESS_PARTNER_NAME);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObPaymentRequestGeneralModel.CREATION_INFO);
    mapAssertion.assertLocalizedValueAndCodeValue(
        IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT,
        IDObPaymentRequestGeneralModel.REFERENCE_DOCUMENT_TYPE,
        IDObPaymentRequestGeneralModel.REFERENCE_DOCUMENT_CODE);
    assertAmountAndCurrency(paymentRequestRowMap, responsedPaymentRequest);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObPaymentRequestGeneralModel.PURCHASE_UNIT_CODE,
        IDObPaymentRequestGeneralModel.PURCHASING_UNIT_NAME);
    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObPaymentRequestGeneralModel.STATE);
    mapAssertion.assertDateTime(
        IAccountingFeatureFileCommonKeys.EXPECTED_DUE_DATE,
        IDObPaymentRequestGeneralModel.EXPECTED_DUE_DATE);
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.BANK_TRANS_REF,
        IDObPaymentRequestGeneralModel.BANK_TRANS_REF);
  }

  private void assertAmountAndCurrency(
      Map<String, String> paymentRequestRowMap, HashMap<String, Object> responsedPaymentRequest) {
    String expectedAmountAndCurrency =
        paymentRequestRowMap.get(IAccountingFeatureFileCommonKeys.AMOUNT);
    String[] expectedAmountAndCurrencyArray = expectedAmountAndCurrency.split(" ");
    assertEquals(new BigDecimal(expectedAmountAndCurrencyArray[0]).compareTo(new BigDecimal(
        responsedPaymentRequest.get(IDObPaymentRequestGeneralModel.AMOUNT_VALUE).toString())), 0,
      1e-15);
    assertEquals(
        expectedAmountAndCurrencyArray[1],
        responsedPaymentRequest.get(IDObPaymentRequestGeneralModel.AMOUNT_CURRENCY));
  }

  public void assertTotalNumberOfItemsExist(Integer expectedPRCount) {
    Long actualItemsCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("getViewAllPRCount");
    Assert.assertEquals(Long.valueOf(expectedPRCount), actualItemsCount);
  }
}
