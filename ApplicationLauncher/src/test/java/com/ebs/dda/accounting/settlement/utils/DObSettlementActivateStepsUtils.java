package com.ebs.dda.accounting.settlement.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.settlement.IDObSettlementRestURLs;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivableActivateValueObject;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Map;

public class DObSettlementActivateStepsUtils extends DObSettlementsCommonTestUtils {
  public DObSettlementActivateStepsUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getPostUrl(String settlementCode) {
    return IDObSettlementRestURLs.COMMAND_URL + "/" + settlementCode + IDObSettlementRestURLs.POST;
  }

  public Response sendPutSettlement(Cookie cookie, String settlementCode, String journalEntryDate) {
    JsonObject jsonObject = new JsonObject();
    addValueToJsonObject(
        jsonObject, IDObNotesReceivableActivateValueObject.JOURNAL_ENTRY_DATE, journalEntryDate);
    return sendPUTRequest(cookie, getPostUrl(settlementCode), jsonObject);
  }

  public void assertThatSettlementIsUpdatedSuccessfully(
      String settlementCode, DataTable settlementDataTable) {
    Map<String, String> settlementMap =
        settlementDataTable.asMaps(String.class, String.class).get(0);

    DObSettlement settlement =
        (DObSettlement)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedSettlement",
                constructPaymentRequestQueryParams(settlementCode, settlementMap));
    Assert.assertNotNull(
        "Settlement is not updated successfully for " + settlementCode, settlement);
  }

  public Object[] constructPaymentRequestQueryParams(
      String settlementCode, Map<String, String> settlementMap) {
    return new Object[] {
      settlementCode,
      settlementMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      settlementMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      "%" + settlementMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatSettlementActivationDetailsIsCreatedSuccessfully(
      String settlementCode, DataTable activationDetailsDataTable) {
    Map<String, String> activationDetailsMap =
        activationDetailsDataTable.asMaps(String.class, String.class).get(0);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetails =
        (IObAccountingDocumentActivationDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSettlementPostingDetails",
                constructSettlementActivationDetailsQueryParams(
                    settlementCode, activationDetailsMap));
    Assert.assertNotNull(
        "Settlement Activation Details is not created successfully " + settlementCode,
        activationDetails);
  }

  public Object[] constructSettlementActivationDetailsQueryParams(
      String settlementCode, Map<String, String> postingDetailsMap) {
    return new Object[] {
      settlementCode,
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
      postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_LATEST_EXCHANGE_RATE_CODE),
      Double.parseDouble(postingDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE))
    };
  }
}
