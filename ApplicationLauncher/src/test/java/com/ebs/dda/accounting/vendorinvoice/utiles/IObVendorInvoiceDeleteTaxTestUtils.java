package com.ebs.dda.accounting.vendorinvoice.utiles;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotNull;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

public class IObVendorInvoiceDeleteTaxTestUtils extends DObVendorInvoiceTestUtils {

  public IObVendorInvoiceDeleteTaxTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxesViewAll.sql");
    return str.toString();
  }

  public String getDeleteTaxUrl(String vendorInvoiceCode, String taxCode) {
    String deleteTaxUrl =
        IDObVendorInvoiceRestUrls.COMMAND_URL
            + vendorInvoiceCode
            + IDObVendorInvoiceRestUrls.TAXES + "/"
            + taxCode;
    return deleteTaxUrl;
  }

  public String getDeleteVendorInvoiceUrl(String vendorInvoiceCode) {
    String deleteInvoiceUrl =
        IDObVendorInvoiceRestUrls.COMMAND_URL
            + IDObVendorInvoiceRestUrls.REQUEST_DELETE_ITEM_URL
            + vendorInvoiceCode;
    return deleteInvoiceUrl;
  }

  public void assertNoTaxExistsByCode(String taxCode, String vendorInvoiceCode) {
    IObVendorInvoiceTaxesGeneralModel actualInvoiceTax =
        (IObVendorInvoiceTaxesGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorInvoiceTaxByCodeNotExist", vendorInvoiceCode, taxCode);
    assertNull(actualInvoiceTax);
  }

  public void assertThatVendorInvoiceSummaryValuesUpdated(String vendorInvoiceCode,
      DataTable vendorInvoiceDataTable) {
    List<Map<String, String>> invoiceSummaryMaps =
        vendorInvoiceDataTable.asMaps(String.class, String.class);

    for (Map<String, String> invoiceSummaryMap : invoiceSummaryMaps) {
      IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
          (IObInvoiceSummaryGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorInvoiceSummary",
                  constructSummaryQueryParams(vendorInvoiceCode, invoiceSummaryMap));
      assertNotNull(invoiceSummaryGeneralModel);
    }
  }

  private Object[] constructSummaryQueryParams(String vendorInvoiceCode,
      Map<String, String> invoiceSummaryMap) {
    return new Object[]{
        vendorInvoiceCode,
        Double.valueOf(invoiceSummaryMap.get(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES)),
        Double.valueOf(invoiceSummaryMap.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT)),
        Double.valueOf(invoiceSummaryMap.get(IAccountingFeatureFileCommonKeys.NET_AMOUNT)),
        Double.valueOf(invoiceSummaryMap.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT)),
        Double.valueOf(invoiceSummaryMap.get(IAccountingFeatureFileCommonKeys.TOTAL_REMAINING))
    };
  }

  public void assertThatVendorInvoiceTaxesValuesUpdated(String vendorInvoiceCode,
      DataTable vendorInvoiceDataTable) {
    List<Map<String, String>> invoiceTaxesMaps =
        vendorInvoiceDataTable.asMaps(String.class, String.class);

    for (Map<String, String> invoiceTaxMap : invoiceTaxesMaps) {
      IObVendorInvoiceTaxesGeneralModel invoiceTaxGeneralModel =
          (IObVendorInvoiceTaxesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getTaxesDetailsForCompanyInVendorInvoice",
                  constructTaxesQueryParams(vendorInvoiceCode, invoiceTaxMap));
      assertNotNull(invoiceTaxGeneralModel);
    }
  }

  private Object[] constructTaxesQueryParams(String vendorInvoiceCode,
      Map<String, String> invoiceTaxMap) {
    return new Object[]{
        vendorInvoiceCode,
        invoiceTaxMap.get(IAccountingFeatureFileCommonKeys.TAX),
        Double.valueOf(invoiceTaxMap.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)),
        Double.valueOf(invoiceTaxMap.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT))
    };
  }
}
