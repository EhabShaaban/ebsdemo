package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewAllTestUtils;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceGeneralModel;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceViewAllStep extends SpringBootRunner implements En, InitializingBean {

  private static boolean hasBeenExecuted = false;
  private DObVendorInvoiceViewAllTestUtils dObInvoiceViewAllTestUtils;
  private MObVendorCommonTestUtils vendorCommonTestUtils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  public DObVendorInvoiceViewAllStep() {
    Given(
        "^the following PurchaseUnits exist:$",
        (DataTable purchaseUnits) -> {
          enterpriseTestUtils.assertThatPurchaseUnitsExist(purchaseUnits);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendor Invoices with filter applied on InvoiceCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              dObInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObVendorInvoiceGeneralModel.INVOICE_CODE, filteringValue);
          Response response =
              dObInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObVendorInvoiceRestUrls.GET_ALL_INVOICES,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable invoicesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          dObInvoiceViewAllTestUtils.assertResponseSuccessStatus(response);
          dObInvoiceViewAllTestUtils.assertThatInvoicesMatches(response, invoicesDataTable);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer numberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          dObInvoiceViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
              response, numberOfRecords);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendor Invoices with filter applied on Vendor which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              dObInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObVendorInvoiceGeneralModel.VENDOR_CODE_NAME, filteringValue);
          Response response =
              dObInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObVendorInvoiceRestUrls.GET_ALL_INVOICES,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendor Invoices with filter applied on PurchaseUnitName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer numberOfRecords,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              dObInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObVendorInvoiceGeneralModel.PURCHASE_UNIT_NAME, filteringValue);
          Response response =
              dObInvoiceViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObVendorInvoiceRestUrls.GET_ALL_INVOICES,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendor Invoices with filter applied on InvoiceType which equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer numberOfRecords,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              dObInvoiceViewAllTestUtils.getEqualsFilterField(
                  IDObVendorInvoiceGeneralModel.INVOICE_TYPE, filteringValue);
          Response response =
              dObInvoiceViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObVendorInvoiceRestUrls.GET_ALL_INVOICES,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendor Invoices with filter applied on PurchaseOrder which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              dObInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObVendorInvoiceGeneralModel.PURCHASE_ORDER_CODE, filteringValue);
          Response response =
              dObInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObVendorInvoiceRestUrls.GET_ALL_INVOICES,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendor Invoices with filter applied on InvoiceNumber which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              dObInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObVendorInvoiceGeneralModel.INVOICE_NUMBER, filteringValue);
          Response response =
              dObInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObVendorInvoiceRestUrls.GET_ALL_INVOICES,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Vendor Invoices with filter applied on State which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              dObInvoiceViewAllTestUtils.getContainsFilterField(
                  IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME, filteringValue);
          Response response =
              dObInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObVendorInvoiceRestUrls.GET_ALL_INVOICES,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    dObInvoiceViewAllTestUtils =
        new DObVendorInvoiceViewAllTestUtils(properties, entityManagerDatabaseConnector);
    vendorCommonTestUtils = new MObVendorCommonTestUtils(properties);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);

    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Filter View All Vendor Invoices")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            dObInvoiceViewAllTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
        databaseConnector.executeSQLScript(DObVendorInvoiceTestUtils.clearVendorInvoices());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Filter View All Vendor Invoices")) {
      databaseConnector.closeConnection();
    }
  }
}
