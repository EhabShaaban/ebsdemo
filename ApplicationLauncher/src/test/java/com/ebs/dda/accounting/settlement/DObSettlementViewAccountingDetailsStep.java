package com.ebs.dda.accounting.settlement;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.settlement.utils.IObSettlementViewAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;

public class DObSettlementViewAccountingDetailsStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "View Settlement AccountingDetails";
  private static boolean hasBeenExecuted = false;

  private IObSettlementViewAccountingDetailsTestUtils utils;
  private AccountingDocumentCommonTestUtils commonAccountingDocumentUtils;

  public DObSettlementViewAccountingDetailsStep() {

    Then(
        "^the following values of AccountingDetails section for Settlement with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String settlementCode, String userName, DataTable AccountingDetailsData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
            commonAccountingDocumentUtils.assertThatAccountingDetailsDataMatches(AccountingDetailsData, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObSettlementViewAccountingDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    commonAccountingDocumentUtils = new AccountingDocumentCommonTestUtils(getProperties());
    commonAccountingDocumentUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewAll(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
      databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
      utils.executeAccountDeterminationFunction();
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(clearSettlement());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
