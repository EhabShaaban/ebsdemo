package com.ebs.dda.accounting.salesinvoice;

import java.sql.SQLException;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceMarkAsReadyForDeliveringTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesInvoiceMarkAsReadyForDeliveringStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceMarkAsReadyForDeliveringTestUtils utils;

  public DObSalesInvoiceMarkAsReadyForDeliveringStep() {

    When(
        "^\"([^\"]*)\" Marks SalesInvoice with code \"([^\"]*)\" Ready for Delivering at \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.markSalesInvoiceAsReadyForDelivering(userCookie, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObSalesInvoiceMarkAsReadyForDeliveringTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "Post Sales Invoice Ready for Delivering")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScripts());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Post Sales Invoice Ready for Delivering")) {
      databaseConnector.closeConnection();
    }
  }
}
