package com.ebs.dda.accounting.salesinvoice;

import java.sql.SQLException;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewAllTestUtils;
import com.ebs.dda.accounting.fiscalyear.utils.CObFiscalYearTestUtils;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCreateJournalEntryTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceCompanyDataViewTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceGeneralDataViewTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceBusinessPartnerTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceItemsViewTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import com.ebs.dda.order.salesorder.utils.DObSalesOrderCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesInvoiceActivateStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "Activate SalesInvoice";
  private static final String ACTIVATE_AUTH_SCENARIO_NAME = "Activate SalesInvoice Auth";

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceActivateTestUtils utils;

  private DObSalesInvoiceGeneralDataViewTestUtils generalDataViewTestUtils;
  private DObSalesInvoiceCompanyDataViewTestUtils companyDataViewTestUtils;
  private IObSalesInvoiceBusinessPartnerTestUtils businessPartnerTestUtils;
  private DObPaymentRequestCreateJournalEntryTestUtils paymentRequestJournalEntryTestUtils;
  private IObSalesInvoiceItemsViewTestUtils itemsViewTestUtils;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
  private HObCommonChartOfAccountTestUtils chartOfAccountTestUtils;
  private DObSalesOrderCommonTestUtils salesOrderCommonTestUtils;
  private DObCollectionViewAllTestUtils collectionUtils;

  public DObSalesInvoiceActivateStep() {

    Given("^the following Summaries for SalesInvoices exist:$",
        (DataTable SalesInvoicesSummariesDataTable) -> {
          utils.assertThatSalesInvoiceInformationExist(SalesInvoicesSummariesDataTable);
        });

    Given("^the following SalesInvoices have the following Items:$",
        (DataTable salesInvoiceDataTable) -> {
          utils.assertThatSalesInvoiceExist(salesInvoiceDataTable);
        });

    Given("^the GLAccounts configuration exist:$", (DataTable glAccountConfigDataTable) -> {
      chartOfAccountTestUtils.assetGLAccountConfigurationsExist(glAccountConfigDataTable);
    });

    Given("^the following GLAccounts has the following Subledgers:$",
        (DataTable subledgerDataTable) -> {
          utils.assertThatSubLedgerExist(subledgerDataTable);
        });

    Given("^the following GeneralData for SalesInvoices exists:$", (DataTable generalDataTable) -> {
      generalDataViewTestUtils.checkSalesInvoiceGeneralDataExists(generalDataTable);
    });

    Given("^the following BusinessPartnerData for SalesInvoices exist:$",
        (DataTable businessPartnerTable) -> {
          businessPartnerTestUtils.assertBusinessPartnerExists(businessPartnerTable);
        });

    When(
        "^\"([^\"]*)\" activates SalesInvoice of type \"([^\"]*)\" with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String salesInvoiceType, String salesInvoiceCode, String dateTime,
            DataTable dataTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.activateSalesInvoice(userCookie, salesInvoiceCode, salesInvoiceType, dataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    And("^Last JournalEntry Code is \"([^\"]*)\"$", (String journalEntryCode) -> {
      paymentRequestJournalEntryTestUtils.assertLastJournalEntry(journalEntryCode);
    });

    Then("^SalesInvoice with Code \"([^\"]*)\" is updated as follows:$",
        (String salesInvoiceCode, DataTable dataTable) -> {
          utils.assertThatSalesInvoiceIsUpdated(salesInvoiceCode, dataTable);
        });

    Then("^ActivationDetails in SalesInvoice with Code \"([^\"]*)\" is updated as follows:$",
        (String salesInvoiceCode, DataTable dataTable) -> {
          utils.assertThatPostingDetailsIsUpdated(salesInvoiceCode, dataTable);
        });

    Then("^the following JournalEntry is Created:$", (DataTable journalEntriesDataTable) -> {
      journalEntryViewAllTestUtils
          .assertThatJournalEntriesExistWithCompanyAndRefDocumentCodeName(journalEntriesDataTable);
    });

    Then("^The following JournalItems for JournalEntry with code \"([^\"]*)\":$",
        (String journalEntryCode, DataTable journalEntryItemsDateTable) -> {
          utils.assertThatJournalEntryItemsCreatedSuccessfully(journalEntryCode,
              journalEntryItemsDateTable);
        });

    Given("^the following SalesOrders exist:$", (DataTable salesOrderDataTable) -> {
      salesOrderCommonTestUtils.assertThatSalesOrdersExist(salesOrderDataTable);
    });

    Then("^The State of SalesOrder with code \"([^\"]*)\" is changed to \"([^\"]*)\"$",
        (String salesOrderCode, String currentState) -> {
          utils.assertThatSalesOrderStateIsUpdatedAsIssued(salesOrderCode, currentState);
        });

    Given("^the following GeneralData for the following Collections exists:$",
        (DataTable generalDataTable) -> {
          collectionUtils.assertThatTheFollowingCollectionGeneralDataExist(generalDataTable);
        });

    Given("^the following CollectionDetails for the following Collections exists:$",
        (DataTable collectionDetailsDataTable) -> {
          collectionUtils.assertThatCollectionDetailsExist(collectionDetailsDataTable);
        });

      Then("^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
              (String missingFields, String username) -> {
                  Response response = userActionsTestUtils.getUserResponse(username);
                  utils.assertThatResponseHasMissingFields(response, missingFields);
              });
      And("^the following SalesInvoices ActivationDetails exist:$",
              (DataTable activationDetailsDataTable) -> {
                  utils.insertSalesInvoiceActivationDetails(activationDetailsDataTable);
              });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();

    utils = new DObSalesInvoiceActivateTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    generalDataViewTestUtils = new DObSalesInvoiceGeneralDataViewTestUtils(properties);
    generalDataViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    companyDataViewTestUtils = new DObSalesInvoiceCompanyDataViewTestUtils(properties);
    companyDataViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    businessPartnerTestUtils =
        new IObSalesInvoiceBusinessPartnerTestUtils(properties, entityManagerDatabaseConnector);

    itemsViewTestUtils = new IObSalesInvoiceItemsViewTestUtils(properties);
    itemsViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    paymentRequestJournalEntryTestUtils =
        new DObPaymentRequestCreateJournalEntryTestUtils(getProperties());
    paymentRequestJournalEntryTestUtils
        .setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    journalEntryViewAllTestUtils =
        new DObJournalEntryViewAllTestUtils(properties, entityManagerDatabaseConnector);

    chartOfAccountTestUtils = new HObCommonChartOfAccountTestUtils(getProperties());
    chartOfAccountTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    salesOrderCommonTestUtils = new DObSalesOrderCommonTestUtils(properties);
    salesOrderCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    collectionUtils = new DObCollectionViewAllTestUtils(properties);
    collectionUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), ACTIVATE_AUTH_SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsToClearInsertionDependancies());
    }

    else if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getNotesReceivablesDbScriptsForSalesInvoice());
      databaseConnector.executeSQLScript(utils.getDbScriptsEveryTimeExecution());
      databaseConnector.executeSQLScript(DObSalesInvoiceActivateTestUtils.clearSalesInvoices());
      databaseConnector.executeSQLScript(CObFiscalYearTestUtils.clearFiscalYearInsertions());

      databaseConnector.executeSQLScript(utils.getCollectionDbScriptsForSalesInvoice());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
