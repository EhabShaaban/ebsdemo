package com.ebs.dda.accounting.salesinvoice;

public interface IDObSalesInvoiceRestUrls {

  String GET_URL = "/services/accounting/salesinvoice/";
  String COMMAND_URL = "/command/salesinvoice/";
  String AUTHORIZED_ACTIONS = GET_URL + "authorizedactions";
  String VIEW_ALL_INVOICES = "/services/accounting/salesinvoice";
  String VIEW_ITEMS_LAST_SALES_PRICE = "/services/accounting/itemslastsalesprice";
  String GENERAL_DATA = "/generaldata";
  String CREATE_URL = COMMAND_URL;
  String REQUEST_CREATE_URL = COMMAND_URL;
  String DELETE_URL = COMMAND_URL + "delete/";
  String BUSINESS_PARTNER_DATA = "/businesspartner";
  String COMPANY_DATA = "/companydata";
  String TAXES = "/taxes";
  String SUMMARY = "/summary";
  String READ_CUSTOMER_URL = "/services/masterdataobjects/customers/bybusinessunitcode/";
  String ITEMS_SECTION = "/items";
  String VIEW_SI_TYPES = "/services/enterprise/salesInvoiceTypes/getall";

  String LOCK_SALES_INVOICE_COMPANY_DATA_SECTION = "/lock/companydata/";
  String UNLOCK_SALES_INVOICE_COMPANY_DATA_SECTION = "/unlock/companydata/";
  String UNLOCK_SALES_INVOICE_SECTIONS = "/unlock/lockedsections/";
  String UNLOCK_LOCKED_SECTIONS = COMMAND_URL + "unlock/lockedsections/";
  String VIEW_POSTING_DETAILS = "/postingdetails";

  String LOCK_BUSINESS_PARTNER = "/lock/businesspartner/";
  String UNLOCK_BUSINESS_PARTNER = "/unlock/businesspartner/";
  String UPDATE_BUSINESS_PARTNER = "/update/businesspartner/";
  String LOCK_SALES_INVOICE_ITEMS_SECTION = "/lock/items/";
  String UNLOCK_SALES_INVOICE_ITEMS_SECTION = "/unlock/items/";
  String POST_SALES_INVOICE = "/activate";

  String READ_CURRENCY_URL = "/services/purchase/orders/currencies/";
  String DELETE_ITEM = "/items/";
  String DELETE_TAX = "/taxes/";
  String READY_FOR_DELIVERING_SALES_INVOICE = "/readyfordelivering";
  String UPDATE_SALES_INVOICE_ITEMS_SECTION = "/update/items/";
  String SALES_ORDERS = "sales-orders";
  
  String DOCUMENT_OWNERS = "document-owners";
}
