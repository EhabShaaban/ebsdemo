package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionAllowedActionsObjectScreenTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPostValidationTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

import static com.ebs.dda.accounting.collection.IDObCollectionRestURLs.AUTHORIZED_ACTIONS;

public class DObCollectionAllowedActionsObjectScreenStep extends SpringBootRunner
        implements En {

    private static boolean hasBeenExecuted = false;
    private DObCollectionAllowedActionsObjectScreenTestUtils utils;

    public DObCollectionAllowedActionsObjectScreenStep() {
        Given(
                "^the following Collections exist:$",
                (DataTable collectionsDataTable) -> {
                    utils.assertThatCollectionsExistByCodeAndStateAndBU(collectionsDataTable);
                });

        When(
                "^\"([^\"]*)\" requests to read actions of Collection with code \"([^\"]*)\"$",
                (String userName, String collectionCode) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, AUTHORIZED_ACTIONS + collectionCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObCollectionAllowedActionsObjectScreenTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Read allowed actions in Collection Object Screen")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {

        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
        databaseConnector.executeSQLScript(
                utils.resetDataDependanciesBeforeInsertion());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read allowed actions in Collection Object Screen")) {
        databaseConnector.closeConnection();
      }
  }
}
