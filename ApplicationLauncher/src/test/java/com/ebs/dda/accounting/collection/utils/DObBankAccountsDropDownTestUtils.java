package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.masterdata.company.IIObCompanyBankDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Assert;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DObBankAccountsDropDownTestUtils extends CommonTestUtils {

  public DObBankAccountsDropDownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObBankSqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");

    return str.toString();
  }

  public void assertThatBankAccountsResponseIsCorrect(
      DataTable bankAccountsDataTable, Response response) throws Exception {

    List<String> bankAccountList = bankAccountsDataTable.asList(String.class).stream()
        .filter(e -> !e.equals(IFeatureFileCommonKeys.ACCOUNT_NUMBER))
        .collect(Collectors.toList());

    List<HashMap<String, Object>> responseAccountsMap = getListOfMapsFromResponse(response);
    List<String> responseBankAccountList = new LinkedList<>();
    for (HashMap<String, Object> responseAccounts : responseAccountsMap) {
      responseBankAccountList.add(
          responseAccounts.get(IIObCompanyBankDetailsGeneralModel.BANK_ACCOUNT_NUMBER).toString());
    }

    Assert.assertThat(bankAccountList,
        IsIterableContainingInAnyOrder.containsInAnyOrder(responseBankAccountList.toArray()));
  }

  public String getReadAllBankAccountsUrl(String companyCode) {
    return IDObCollectionRestURLs.READ_ALL_BANK_ACCOUNTS + "/" + companyCode;
  }
}
