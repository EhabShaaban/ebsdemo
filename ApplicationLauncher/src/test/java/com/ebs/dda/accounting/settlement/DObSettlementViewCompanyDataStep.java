package com.ebs.dda.accounting.settlement;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementViewCompanyTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementViewCompanyDataStep extends SpringBootRunner implements En {

	public static final String SCENARIO_NAME = "View Company section in Settlement";
	private DObSettlementViewCompanyTestUtils utils;

	public DObSettlementViewCompanyDataStep() {
		When("^\"([^\"]*)\" requests to view Company section of Settlement with code \"([^\"]*)\"$",
						(String username, String settlementCode) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							Response response = utils.sendGETRequest(cookie, String.format(
											IDObSettlementRestURLs.VIEW_COMPANY, settlementCode));
							userActionsTestUtils.setUserResponse(username, response);
						});
		Then("^the following values of CompanyData section for Settlement with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
						(String settlementCode, String username, DataTable companyDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(username);
							utils.assertThatViewSettlementCompanyDataResponseIsCorrect(response,
									companyDataTable);
						});

	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		utils = new DObSettlementViewCompanyTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	@Before
	public void beforeViewAll(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
			databaseConnector.executeSQLScript(clearSettlement());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
