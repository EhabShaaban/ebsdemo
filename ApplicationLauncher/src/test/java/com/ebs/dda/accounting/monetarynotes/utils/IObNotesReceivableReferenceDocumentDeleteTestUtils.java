package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;

import java.util.Map;

import static org.junit.Assert.assertNull;

public class IObNotesReceivableReferenceDocumentDeleteTestUtils
    extends DObMonetaryNotesCommonTestUtils {

  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[Delete] [ReferenceDocument]";

  public IObNotesReceivableReferenceDocumentDeleteTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    return str.toString();
  }

  public void assertThatNotesReceivableReferenceDocumentIsDeletedSuccessfully(
      String notesReceivableCode, Integer referenceDocumentId) {
    Object actualNotesReceivableReferenceDocument =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getNotesReceivableReferenceDocumentByNRCodeAndRefDocumentId",
            notesReceivableCode,
            referenceDocumentId);
    assertNull(
        ASSERTION_MSG + "NotesReceivable ReferenceDocument does not deleted successfully",
        actualNotesReceivableReferenceDocument);
  }

  public String getDeleteNotesReceivableReferenceDocumentRestUrl(
      String notesReceivableCode, Integer referenceDocumentId, String referenceDocumentType) {
    StringBuilder deleteUrl = new StringBuilder();
    deleteUrl.append(IDObNotesReceivableRestURLs.BASE_COMMAND_URL);
    deleteUrl.append("/");
    deleteUrl.append(notesReceivableCode);
    deleteUrl.append(IDObNotesReceivableRestURLs.DELETE_REFERENCE_DOCUMENTS);
    deleteUrl.append("/");
    deleteUrl.append(referenceDocumentId);
    deleteUrl.append("/");
    deleteUrl.append(referenceDocumentType);
    return deleteUrl.toString();
  }
}
