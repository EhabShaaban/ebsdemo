package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentActivationDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DObCollectionViewActivationDetailsTestUtils extends DObCollectionTestUtils {

  public DObCollectionViewActivationDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertCorrectValuesAreDisplayedInActivationDetails(
      Response response, String CollectionCode, DataTable activationDetailsDataTable)
      throws Exception {
    Map<String, String> activationDetailsDataExpected =
        activationDetailsDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> activationDetailsDataActual = getMapFromResponse(response);
    String collectionActivationDetailsCode =
        activationDetailsDataExpected.get(IFeatureFileCommonKeys.CODE);
    MapAssertion mapAssertion =
        new MapAssertion(activationDetailsDataExpected, activationDetailsDataActual);
    if (collectionActivationDetailsCode.isEmpty()) {
      assertThereIsNoactivationDetails(CollectionCode);
    } else {
      assertActualAndExpectedDataFromResponse(mapAssertion);
    }
  }

  private void assertThereIsNoactivationDetails(String collectionCode) {
    IIObAccountingDocumentActivationDetailsGeneralModel collectionActivationDetails =
        (IIObAccountingDocumentActivationDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCollectionDocumentActivationDetailsByCodeAndObjectType", collectionCode);
    Assert.assertNull(collectionActivationDetails);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IIObAccountingDocumentActivationDetailsGeneralModel.CODE);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.ACCOUNTANT,
        IIObAccountingDocumentActivationDetailsGeneralModel.ACCOUNTANT);

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.ACTIVATION_DATE,
        IIObAccountingDocumentActivationDetailsGeneralModel.ACTIVATION_DATE);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.POSTED_BY,
        IIObAccountingDocumentActivationDetailsGeneralModel.MODIFIED_BY);
    ;

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE,
        IIObAccountingDocumentActivationDetailsGeneralModel.JOURNAL_ENTRY_CODE);

    mapAssertion.assertEquationValues(
        IAccountingFeatureFileCommonKeys.CURRENCY_PRICE,
        constructLeftHandSide(),
        constructRightHandSide(),
        "=");

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.EXCHANGE_RATE_CODE,
        IIObAccountingDocumentActivationDetailsGeneralModel.EXCHANGE_RATE_CODE);
  }

  public List<String> constructLeftHandSide() {
    List<String> leftHandSide = new ArrayList<>();
    leftHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.FIRST_VALUE);
    leftHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.FIRST_CURRENCY_ISO);
    return leftHandSide;
  }

  public List<String> constructRightHandSide() {
    List<String> rightHandSide = new ArrayList<>();
    rightHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.CURRENCY_PRICE);
    rightHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.SECOND_CURRENCY_ISO);
    return rightHandSide;
  }
}
