package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.actualcost.ActualCostTestUtils;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostConvertToActualTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPurchaseOrderReadAllTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewPaymentDetailsTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptViewItemsTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsDeliveryCompleteTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class DObLandedCostConvertToActualStep extends SpringBootRunner
    implements En, InitializingBean {

  private static final String SCENARIO_NAME = "LandedCost ConvertToActual";
  private static final String HAPPY_PATH = "Happy Path";
  private boolean hasBeenExecutedOnce = false;
  private DObLandedCostConvertToActualTestUtils landedCostTestUtils;
  private DObPurchaseOrderMarkAsDeliveryCompleteTestUtils markAsDeliveryCompleteTestUtils;
  private IObGoodsReceiptViewItemsTestUtils goodsReceiptViewItemsTestUtils;
  private DObLandedCostConvertToActualTestUtils landedCostConvertToActualTestUtils;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
  private DObPaymentRequestPurchaseOrderReadAllTestUtils
      paymentRequestPurchaseOrderReadAllTestUtils;
  private DObPaymentRequestViewPaymentDetailsTestUtils paymentRequestViewPaymentDetailsTestUtils;
  private ActualCostTestUtils actualCostTestUtils;
  private DObSalesInvoiceActivateTestUtils salesInvoicePostTestUtils;

  public DObLandedCostConvertToActualStep() {

    When(
        "^\"([^\"]*)\" ConvertToActual LandedCost with code \"([^\"]*)\" at \"([^\"]*)\":$",
        (String userName, String landedCostCode, String dateTime) -> {
          landedCostTestUtils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);

          Response response =
              landedCostTestUtils.sendPUTRequest(
                  userCookie,
                  IDObLandedCostRestUrls.COMMAND_URL
                      + "/"
                      + landedCostCode
                      + IDObLandedCostRestUrls.CONVERT_TO_ACTUAL_LANDED_COST,
                  new JsonObject());

          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^the following sections with missing fields are sent to \"([^\"]*)\":$",
        (String userName, DataTable sectionsWithMissingFieldsDataTable) -> {
          landedCostTestUtils.assertThatSectionsWithMissingFieldsExistInResponse(
              userActionsTestUtils.getUserResponse(userName), sectionsWithMissingFieldsDataTable);
        });
    Given(
        "^the following GRs exist with the following GR-POData:$",
        (DataTable goodsReceiptPurchaseOrderData) -> {
          markAsDeliveryCompleteTestUtils.assertThatGoodsReceiptsExist(
              goodsReceiptPurchaseOrderData);
        });

    Given(
        "^GoodsReceipt with code \"([^\"]*)\" has the following received items:$",
        (String goodsReceiptCode, DataTable receivedItems) -> {
          goodsReceiptViewItemsTestUtils.assertGoodsReceiptItemsExist(
              goodsReceiptCode, receivedItems);
        });

    Given(
        "^Item \"([^\"]*)\" in GoodsReceipt \\(Based On PurchaseOrder\\) \"([^\"]*)\" has the following received quantities:$",
        (String item, String goodsReceiptCode, DataTable ordinaryItemQuantities) -> {
          goodsReceiptViewItemsTestUtils.assertGoodsReceiptPurchaseOrderItemQuantitiesExist(
              goodsReceiptCode, item, ordinaryItemQuantities);
        });

    Then(
        "^the items single unit final cost with damaged items cost are exist as following:$",
        (DataTable itemsCostDT) -> {
          landedCostConvertToActualTestUtils.assertThatItemsTotalCostAreExist(itemsCostDT);
        });

    Given(
        "^the following PurchaseOrders exist:$",
        (DataTable purchaseOrdersDataTable) -> {
          paymentRequestPurchaseOrderReadAllTestUtils.assertPurchaseOrdersExist(
              purchaseOrdersDataTable);
          ;
        });
    Given(
        "^the following PaymentRequests exist:$",
        (DataTable paymentRequestsDataTable) -> {
          actualCostTestUtils.assertThatPaymentRequestExists(paymentRequestsDataTable);
        });
    Given(
        "^the following PaymentDetails for PaymentRequests exist:$",
        (DataTable paymentDetailsTable) -> {
          paymentRequestViewPaymentDetailsTestUtils.assertThatTheFollowingPRPaymentDetailsExist(
              paymentDetailsTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    landedCostTestUtils = new DObLandedCostConvertToActualTestUtils(properties);
    landedCostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    markAsDeliveryCompleteTestUtils =
        new DObPurchaseOrderMarkAsDeliveryCompleteTestUtils(properties);
    markAsDeliveryCompleteTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    goodsReceiptViewItemsTestUtils = new IObGoodsReceiptViewItemsTestUtils(properties);
    goodsReceiptViewItemsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    landedCostConvertToActualTestUtils = new DObLandedCostConvertToActualTestUtils(properties);
    landedCostConvertToActualTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    journalEntryViewAllTestUtils =
        new DObJournalEntryViewAllTestUtils(properties, entityManagerDatabaseConnector);
    salesInvoicePostTestUtils = new DObSalesInvoiceActivateTestUtils(properties);
    salesInvoicePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    paymentRequestPurchaseOrderReadAllTestUtils =
        new DObPaymentRequestPurchaseOrderReadAllTestUtils(properties);
    paymentRequestPurchaseOrderReadAllTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    paymentRequestViewPaymentDetailsTestUtils =
        new DObPaymentRequestViewPaymentDetailsTestUtils(properties);
    paymentRequestViewPaymentDetailsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    actualCostTestUtils = new ActualCostTestUtils(properties);
    actualCostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            AccountingDocumentCommonTestUtils.clearAccountingDocuments());
        databaseConnector.executeSQLScript(DObLandedCostCommonTestUtils.clearLandedCosts());
        databaseConnector.executeSQLScript(landedCostTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(landedCostTestUtils.getDbScripts());
      if (contains(scenario.getName(), HAPPY_PATH)) {
        databaseConnector.executeSQLScript(landedCostTestUtils.costingDBSenarios());
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      landedCostTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
