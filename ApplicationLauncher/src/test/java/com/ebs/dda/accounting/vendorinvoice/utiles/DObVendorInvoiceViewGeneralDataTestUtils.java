package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObVendorInvoiceViewGeneralDataTestUtils extends DObVendorInvoiceCommonTestUtils {

  public DObVendorInvoiceViewGeneralDataTestUtils(
      Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");

    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    return str.toString();
  }

  public void assertInvoicesExistWithGeneralData(DataTable invoicesGeneralData) {
    List<Map<String, String>> invoices = invoicesGeneralData.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoices) {
      assertThatInvoiceExists(invoice);
    }
  }

  public void assertThatInvoiceExists(Map<String, String> invoice) {
    Object[] params =
        new Object[] {
          invoice.get(IFeatureFileCommonKeys.CODE),
          invoice.get(IFeatureFileCommonKeys.TYPE),
          invoice.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
          invoice.get(IAccountingFeatureFileCommonKeys.COMPANY),
          invoice.get(IFeatureFileCommonKeys.CREATED_BY),
          invoice.get(IFeatureFileCommonKeys.CREATION_DATE),
          "%" + invoice.get(IFeatureFileCommonKeys.STATE) + "%",
          invoice.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
          invoice.get(IFeatureFileCommonKeys.VENDOR_CODE)
        };
    DObVendorInvoiceGeneralModel dObInvoice =
        (DObVendorInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getInvoiceForGeneralData", params);
    assertNotNull("Vendor Invoice is not exist, given " + Arrays.toString(params), dObInvoice);
  }

  public Response requestToReadGeneralData(Cookie cookie, String invoiceCode) {
    String restURL =
        IDObVendorInvoiceRestUrls.INVOICE_GENERAL_DATA_URL
            + invoiceCode
            + IDObVendorInvoiceRestUrls.GENERAL_DATA_SECTION;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatInvoiceGeneralDataMatches(DataTable invoicesGeneralData, Response response)
      throws Exception {
    Map<String, String> expectedInvoice =
        invoicesGeneralData.asMaps(String.class, String.class).get(0);
    assertResponseSuccessStatus(response);
    HashMap<String, Object> actualInvoice = getMapFromResponse(response);

    assertThatActualInvoiceMatchesExpected(expectedInvoice, actualInvoice);
  }

  public void assertThatActualInvoiceMatchesExpected(
      Map<String, String> expectedInvoice, HashMap<String, Object> actualInvoice) throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedInvoice, actualInvoice);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObVendorInvoiceGeneralModel.INVOICE_TYPE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObVendorInvoiceGeneralModel.PURCHASE_UNIT_CODE,
        IDObVendorInvoiceGeneralModel.PURCHASE_UNIT_NAME);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObVendorInvoiceGeneralModel.CREATION_INFO);
    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObVendorInvoiceGeneralModel.CREATION_DATE);
    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);
    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.DOCUMENT_OWNER, IDObVendorInvoiceGeneralModel.DOCUMENT_OWNER);
  }
}
