package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPostCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPurchaseOrderReadAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestPurchaseOrderReadAllStep extends SpringBootRunner implements En, InitializingBean {

    public static final String READ_PO_SCENARIO_NAME = "Read All PurchaseOrders for DueDocument Dropdown in PaymentRequest";
    public static final String READ_GOODS_PO_SCENARIO_NAME = "Read All Goods PurchaseOrders [Import / Local]";
    private static boolean hasBeenExecutedOnce = false;
  private DObPaymentRequestPurchaseOrderReadAllTestUtils utils;
  private DObPaymentRequestPostCommonTestUtils paymentRequestPostCommonTestUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObPaymentRequestPurchaseOrderReadAllTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        paymentRequestPostCommonTestUtils = new DObPaymentRequestPostCommonTestUtils(getProperties());
        paymentRequestPostCommonTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }

    public DObPaymentRequestPurchaseOrderReadAllStep() {

    Given(
        "^the following PurchaseOrders exist:$",
        (DataTable purchaseOrdersDataTable) -> {
          utils.assertPurchaseOrdersExist(purchaseOrdersDataTable);
        });
    Then(
        "^the following purchase orders should be returned to \"([^\"]*)\":$",
        (String userName, DataTable vendorInvoicesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatInvoicesDropDownResponseIsCorrect(response, vendorInvoicesDataTable);
        });

    And(
        "^total number of purchase orders returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer numberOfPurchaseOrders) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfReturnedPurchaseOrdersIsCorrect(response, numberOfPurchaseOrders);
        });
		When("^\"([^\"]*)\" requests to read all PurchaseOrders from Approved to Delivery Complete And not Cancelled for Payment with BusinessUnitCode \"([^\"]*)\" and BusinessPartnerCode \"([^\"]*)\" and PaymentType \"([^\"]*)\"$",
						(String userName, String businessUnitCode, String businessPartnerCode,
										String paymentType) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							Response response = utils.sendGETRequest(cookie,
											utils.getReadAllPurchaseOrdersUrl(businessUnitCode,
															businessPartnerCode, paymentType));
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read all PurchaseOrders from Approved to Delivery Complete And not Cancelled for Payment with BusinessUnitCode \"([^\"]*)\" and PaymentType \"([^\"]*)\"$",
						(String userName, String businessUnitCode, String paymentType) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							Response response = utils.sendGETRequest(cookie,
											utils.getReadAllPurchaseOrdersUrl(businessUnitCode,
															null, paymentType));
							userActionsTestUtils.setUserResponse(userName, response);
						});
    }

  @Before
  public void before(Scenario scenario) throws Exception {
    if(contains(scenario.getName(), READ_PO_SCENARIO_NAME) || contains(scenario.getName(), READ_GOODS_PO_SCENARIO_NAME)){
        databaseConnector.createConnection();
        if (!hasBeenExecutedOnce) {
            databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
            hasBeenExecutedOnce = true;
        }
        databaseConnector.executeSQLScript(DObPaymentRequestPurchaseOrderReadAllTestUtils.clearOrderDbScriptPath());

    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
      if(contains(scenario.getName(), READ_PO_SCENARIO_NAME) || contains(scenario.getName(), READ_GOODS_PO_SCENARIO_NAME)){
          databaseConnector.closeConnection();
      }
  }
}
