package com.ebs.dda.accounting.salesinvoice;

import java.sql.SQLException;
import java.util.Map;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceTaxesViewTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSalesInvoiceTaxesViewStep extends SpringBootRunner implements En {

	public static final String SCENARIO_NAME = "View SalesInvoiceTaxes";
	private static boolean hasBeenExecuted = false;
	private IObSalesInvoiceTaxesViewTestUtils utils;

	public DObSalesInvoiceTaxesViewStep() {

		Given(
				"^the following TaxDetails exist in taxes section for SalesInvoice \"([^\"]*)\"$",
				(String salesInvoiceCode, DataTable taxDetailsDataTable) -> {
					utils.assertThatTheFollowingTaxDetailsExistForSalesInvoice(
							salesInvoiceCode, taxDetailsDataTable);
				});
		Given("^the following TaxDetails for SalesInvoices exist:$",
						(DataTable taxDetailsDataTable) -> {
							utils.assertThatTaxDetailsExist(taxDetailsDataTable);
						});
		When("^\"([^\"]*)\" requests to view Taxes section of SalesInvoice with code \"([^\"]*)\"$",
						(String userName, String salesInvoiceCode) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							StringBuilder readUrl = new StringBuilder();
							readUrl.append(IDObSalesInvoiceRestUrls.GET_URL);
							readUrl.append(salesInvoiceCode);
							readUrl.append(IDObSalesInvoiceRestUrls.TAXES);
							Response response = utils.sendGETRequest(cookie, readUrl.toString());
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^the following tax values for SalesInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
						(String salesInvoiceCode, String userName,
										DataTable taxValuesDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertCorrectValuesAreDisplayedInTaxesSection(response,
											taxValuesDataTable);
						});
	}

	@Before
	public void setup(Scenario scenario) throws Exception {
		Map<String, Object> properties = getProperties();
		utils = new IObSalesInvoiceTaxesViewTestUtils(properties);
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
				hasBeenExecuted = true;
			}
			databaseConnector.executeSQLScript(DObSalesInvoiceTestUtils.clearSalesInvoices());
		}
	}

	@After
	public void afterEachScenario(Scenario scenario) throws SQLException {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
