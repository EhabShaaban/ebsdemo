package com.ebs.dda.accounting.settlement;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.settlement.utils.DObSettlementViewActivationDetailsTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementViewActivationDetailsStep extends SpringBootRunner implements En {

	public static final String SCENARIO_NAME = "View ActivationDetails section in Settlement";
	private DObSettlementViewActivationDetailsTestUtils utils;
	private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
	private static boolean hasBeenExecuted = false;

	public DObSettlementViewActivationDetailsStep() {
		When("^\"([^\"]*)\" requests to view ActivationDetails section of Settlement with code \"([^\"]*)\"$",
						(String username, String settlementCode) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(username);
							Response response = utils.sendGETRequest(cookie, String.format(
											IDObSettlementRestURLs.VIEW_ACTIVATION_DETAILS,
											settlementCode));
							userActionsTestUtils.setUserResponse(username, response);
						});

		Then("^the following values of ActivationDetails section of Settlement with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
						(String settlementCode, String username, DataTable activationDetailsTable) -> {
							Response response = userActionsTestUtils.getUserResponse(username);
							utils.assertThatViewSettlementActivationDetailsResponseIsCorrect(
											response, activationDetailsTable);
						});

	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		utils = new DObSettlementViewActivationDetailsTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		journalEntryViewAllTestUtils = new DObJournalEntryViewAllTestUtils(getProperties(),
						entityManagerDatabaseConnector);
	}

	@Before
	public void beforeViewAll(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				utils.executeAccountDeterminationFunction();
			}
			databaseConnector.executeSQLScript(
							DObJournalEntryViewAllTestUtils.clearJournalEntries());
			databaseConnector.executeSQLScript(clearSettlement());
			databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
