package com.ebs.dda.accounting.salesinvoice.utils;

import static junit.framework.TestCase.assertNull;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItem;
import java.util.Map;

public class IObSalesInvoiceDeleteItemTestUtils extends DObSalesInvoiceTestUtils {

  public IObSalesInvoiceDeleteItemTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceItems.sql");
    return str.toString();
  }

  public String getDeleteItemUrl(String salesInvoiceCode, String itemId) {
    String deleteItemUrl =
        IDObSalesInvoiceRestUrls.COMMAND_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.DELETE_ITEM
            + itemId;
    return deleteItemUrl;
  }

  public void assertNoItemExistsById(String itemId, String salesInvoiceCode) {
    IObSalesInvoiceItem actualInvoiceItem =
        (IObSalesInvoiceItem)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceItemByIdNotExist", salesInvoiceCode, Long.parseLong(itemId));
    assertNull(actualInvoiceItem);
  }
}
