package com.ebs.dda.accounting.salesinvoice.runners;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {
    "classpath:features/accounting/sales-invoice/SI_AllowedActions_Home.feature",
    "classpath:features/accounting/sales-invoice/SI_AllowedActions_Object.feature",
    "classpath:features/accounting/sales-invoice/SI_Delete.feature",
    "classpath:features/accounting/sales-invoice/SI_View_GeneralData.feature",
    "classpath:features/accounting/sales-invoice/SI_ItemLastSalesPrice.feature",
    "classpath:features/accounting/sales-invoice/SI_Activate_SalesOrder_HP.feature",
    "classpath:features/accounting/sales-invoice/SI_Activate_WithoutReference_HP.feature",
    "classpath:features/accounting/sales-invoice/SI_ViewAll.feature",
    "classpath:features/accounting/sales-invoice/SI_MarkAsReadyForDelivering_HP.feature",
    "classpath:features/accounting/sales-invoice/SI_RequestEdit_CompanyData.feature",
    "classpath:features/accounting/sales-invoice/SI_View_CompanyData.feature",
    "classpath:features/accounting/sales-invoice/SI_RequestAdd_Item.feature",
    "classpath:features/accounting/sales-invoice/SI_Delete_Item.feature",
    "classpath:features/accounting/sales-invoice/SI_Save_NewItem_HP.feature",
    "classpath:features/accounting/sales-invoice/SI_View_Items.feature",
    "classpath:features/accounting/sales-invoice/SI_Create_HP.feature",
    "classpath:features/accounting/sales-invoice/SI_Create_Auth.feature",
    "classpath:features/accounting/sales-invoice/SI_Create_Val.feature",
    "classpath:features/masterdata/business-unit/BusinessUnit_Dropdowns.feature",
    "classpath:features/masterdata/collection-responsible/CollectionResponsible_Dropdowns.feature",
    "classpath:features/masterdata/company/Company_Dropdowns.feature",
    "classpath:features/masterdata/customer/Customer_Dropdowns.feature",
    "classpath:features/accounting/sales-invoice/SI_RequestEdit_BusinessPartner.feature",
    "classpath:features/masterdata/currency/Currency_Dropdowns.feature",
    "classpath:features/masterdata/payment-terms/PaymentTerm_Dropdowns.feature",
    "classpath:features/accounting/sales-invoice/SI_Save_BusinessPartner_HP.feature",
    "classpath:features/accounting/sales-invoice/SI_View_BusinessPartner.feature",
    "classpath:features/accounting/sales-invoice/SI_View_Taxes.feature",
    "classpath:features/accounting/sales-invoice/SI_View_Summary.feature",
    "classpath:features/accounting/sales-invoice/SI_Type_Dropdown.feature",
    "classpath:features/accounting/sales-invoice/SI_SalesOrders_Dropdown.feature",
    "classpath:features/accounting/sales-invoice/SI_DocumentOwner_Dropdown.feature",
    "classpath:features/accounting/sales-invoice/SI_View_PostingDetails.feature",
    "classpath:features/accounting/sales-invoice/SI_Activate_SO_Auth.feature",
    "classpath:features/accounting/sales-invoice/SI_Post_Val.feature",
    "classpath:features/accounting/sales-invoice/SI_Delete_Tax.feature"

}, glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.salesinvoice"}, strict = true,
    tags = "not @Future")
public class DObSalesInvoiceCucumberRunner {
}
