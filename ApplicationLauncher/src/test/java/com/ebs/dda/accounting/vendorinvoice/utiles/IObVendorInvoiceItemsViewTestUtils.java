package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.IIObInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ebs.dda.CommonKeys.DATA;
import static org.junit.Assert.*;

public class IObVendorInvoiceItemsViewTestUtils extends IObVendorInvoiceItemsTestUtils {

  public IObVendorInvoiceItemsViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertVendorInvoiceItemExists(
      String vendorInvoiceCode, DataTable vendorInvoiceItemsDataTable) {
    Map<String, String> vendorInvoiceItem =
        vendorInvoiceItemsDataTable.asMaps(String.class, String.class).get(0);
    IObVendorInvoiceItemsGeneralModel invoiceItemsGeneralModel =
        (IObVendorInvoiceItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorInvoicesItems",
                vendorInvoiceCode,
                vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_CODE),
                "%" + vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_NAME) + "%",
                getDouble(
                    vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER)),
                vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT),
                "%"
                    + vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT_Symbol)
                    + "%",
                vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_BASE_UNIT),
                "%"
                    + vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_BASE_UNIT_Symbol)
                    + "%",
                getDouble(
                    vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)));
    assertNotNull(invoiceItemsGeneralModel);
  }

  public Response requestToReadVendorInvoiceItems(Cookie cookie, String invoiceCode) {
    String restURL =
        IDObVendorInvoiceRestUrls.GET_URL + invoiceCode + IDObVendorInvoiceRestUrls.ITEMS_SECTION;
    return sendGETRequestWithDoubleNumbersType(cookie, restURL);
  }

  public void assertThatInvoiceItemsMatch(DataTable invoicesItems, Response response)
      throws Exception {
    List<Map<String, String>> expectedInvoiceItem =
        invoicesItems.asMaps(String.class, String.class);
    assertResponseSuccessStatus(response);
    List<HashMap<String, Object>> actualInvoiceItems = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedInvoiceItem.size(); i++) {
      assertThatActualInvoiceItemMatchesExpected(
          expectedInvoiceItem.get(i), actualInvoiceItems.get(i));
    }
  }

  private void assertThatActualInvoiceItemMatchesExpected(
      Map<String, String> expectedInvoiceItem, HashMap<String, Object> actualInvoiceItem)
      throws Exception {
    assertEquals(
        expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_CODE),
        actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.ITEM_CODE));
    assertEquals(
        expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_NAME),
        getEnglishValue(actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.ITEM_NAME)));
    assertEquals(
        new BigDecimal(
            expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER)).stripTrailingZeros(),
            new BigDecimal(actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.ITEM_QUANTITY_IN_ORDER_UNIT).toString()).stripTrailingZeros());
    assertEquals(
        expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT),
        actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.ORDER_UNIT_CODE));
    assertEquals(
        expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT_Symbol),
        getEnglishValue(actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.ORDER_UNIT_SYMBOL)));
    assertEquals(
        expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_BASE_UNIT),
        actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.BASE_UNIT_CODE));
    assertEquals(
        expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_BASE_UNIT_Symbol),
        getEnglishValue(actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.BASE_UNIT_SYMBOL)));
    assertEquals(
            new BigDecimal(
            expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)).stripTrailingZeros(),
            new BigDecimal(actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.ITEM_PRICE).toString()).stripTrailingZeros());
    assertEquals(
            new BigDecimal(
            expectedInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT)).stripTrailingZeros(),
            new BigDecimal(actualInvoiceItem.get(IIObInvoiceItemsGeneralModel.ITEM_ORDER_UNIT_PRICE).toString()).stripTrailingZeros());
  }

  public void assertThatInvoiceItemsAreEmpty(Response response) {
    List<HashMap<String, Object>> items = response.body().jsonPath().getList(DATA);
    assertTrue(items.isEmpty());
  }

  public void assertThatInvoiceTotalAmountEquals(
      String vendorInvoiceCode, DataTable invoiceTotalAmountDataTable) {
    Map<String, String> invoiceTotalAmount =
        invoiceTotalAmountDataTable.asMaps(String.class, String.class).get(0);
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        (IObInvoiceSummaryGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getInvoiceTotalAmount",
                vendorInvoiceCode,
                Double.valueOf(
                    invoiceTotalAmount.get(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT)));
    assertNotNull(
        "The Invoice Total Amount Is Wrong Or Doesn't Exists ", invoiceSummaryGeneralModel);
  }
}
