package com.ebs.dda.accounting.costing;

public interface IDObCostingRestUrls {

  String QUERY_COSTING = "/services/accounting/costing";
  String AUTHORIZED_ACTIONS = QUERY_COSTING + "/authorizedactions/";

  String VIEW_DETAILS = "/details";
  String VIEW_ITEMS = "/items";


  String COMMAND_COSTING = "/command/accounting/costing";
  String ACTIVATE = "/activate";
}
