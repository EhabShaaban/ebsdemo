package com.ebs.dda.accounting.journalentry;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntryGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObJournalEntryViewAllStep extends SpringBootRunner implements En, InitializingBean {

  private static boolean hasBeenExecuted = false;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    journalEntryViewAllTestUtils =
        new DObJournalEntryViewAllTestUtils(properties, entityManagerDatabaseConnector);
  }

  public DObJournalEntryViewAllStep() {

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredData(
                  userCookie, IDObJournalEntryRestUrls.GET_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following JournalEntries will be presented to \"([^\"]*)\":$",
        (String userName, DataTable journalEntriesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          journalEntryViewAllTestUtils.assertResponseSuccessStatus(response);
          journalEntryViewAllTestUtils.assertThatJournalEntriesArePresentedToUser(
              response, journalEntriesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on JournalEntryCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.CODE, filteringValue);
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on CreationDate which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.CREATION_DATE,
                  String.valueOf(
                      journalEntryViewAllTestUtils.getDateTimeInMilliSecondsFormat(
                          filteringValue)));
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on CreatedBy which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.CREATED_BY, filteringValue);
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on JournalDate which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.DUE_DATE,
                  String.valueOf(
                      journalEntryViewAllTestUtils.getDateTimeInMilliSecondsFormat(
                          filteringValue)));
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on ReferenceDocument which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer numberOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.DOCUMENT_REFERENCE, filteringValue);
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on Company which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer numberOfRecords,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.COMPANY_NAME, filteringValue);
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on BusinessUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer numberOfRecords,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.PURCHASE_UNIT_NAME, filteringValue);
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of JournalEntries with filter applied on FiscalPeriod which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer numberOfRecords,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              journalEntryViewAllTestUtils.getContainsFilterField(
                  IDObJournalEntryGeneralModel.FISCAL_PERIOD, filteringValue);
          Response response =
              journalEntryViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObJournalEntryRestUrls.GET_URL,
                  pageNumber,
                  numberOfRecords,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Filter View All JournalEntries")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            journalEntryViewAllTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }

      databaseConnector.executeSQLScript(DObJournalEntryViewAllTestUtils.clearJournalEntries());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Filter View All JournalEntriesItems")) {

      databaseConnector.closeConnection();
    }
  }
}
