package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoicePostValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoicePostingDetails;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObSalesInvoiceActivateTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceActivateTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObControlPointSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAccountingInfo.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");

    return str.toString();
  }
  public String getDbScriptsEveryTimeExecution() {
    StringBuilder str = new StringBuilder();
       str.append(",").append("db-scripts/accounting/collection/Clear_Collection_SalesInvoice.sql");
       str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes.sql");

    return str.toString();
  }

  public String getCollectionDbScriptsForSalesInvoice() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/accounting/collection/Collection_SalesInvoice.sql");
    return str.toString();
  }

  public String getNotesReceivablesDbScriptsForSalesInvoice() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");    return str.toString();
  }

  public Response activateSalesInvoice(
      Cookie userCookie, String salesInvoiceCode, String salesInvoiceType, DataTable postingDetailsDataTable) {
    Map<String, String> postingDetails =
        postingDetailsDataTable.asMaps(String.class, String.class).get(0);
    JsonObject salesInvoiceData = new JsonObject();
    salesInvoiceData.addProperty(
        IDObSalesInvoicePostValueObject.JOURNAL_ENTRY_DATE,
        postingDetails.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_DATE));
    return sendPUTRequest(
        userCookie,
        IDObSalesInvoiceRestUrls.COMMAND_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.POST_SALES_INVOICE  + "?type=" + salesInvoiceType,
        salesInvoiceData);
  }

  public void assertThatSalesInvoiceIsUpdated(String salesInvoiceCode, DataTable dataTable) {
    Map<String, String> salesInvoiceMap = dataTable.asMaps(String.class, String.class).get(0);
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesForPostedCheck",
                constructSalesInvoiceQueryParams(salesInvoiceCode, salesInvoiceMap));
    assertNotNull("Sales Invoice is not updated successfully", salesInvoiceGeneralModel);
  }

  private Object[] constructSalesInvoiceQueryParams(
      String salesInvoiceCode, Map<String, String> salesInvoiceMap) {
    return new Object[] {
            salesInvoiceCode,
            salesInvoiceMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            salesInvoiceMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            '%' + salesInvoiceMap.get(IFeatureFileCommonKeys.STATE) + '%'
    };
  }

  public void assertThatPostingDetailsIsUpdated(String salesInvoiceCode, DataTable dataTable) {
    Map<String, String> postingDetailsMap = dataTable.asMaps(String.class, String.class).get(0);
    IObSalesInvoicePostingDetails salesInvoicesPostingDetails =
        (IObSalesInvoicePostingDetails)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesPostingDetails",
                constructPostingDetailsQueryParams(salesInvoiceCode, postingDetailsMap));
    assertNotNull(
        "Sales Invoice Posting Details is not updated successfully", salesInvoicesPostingDetails);
  }

  private Object[] constructPostingDetailsQueryParams(
      String salesInvoiceCode, Map<String, String> postingDetailsMap) {
    return new Object[] {
      salesInvoiceCode,
      postingDetailsMap.get(IFeatureFileCommonKeys.STOREKEEPER) != null
          ? postingDetailsMap.get(IFeatureFileCommonKeys.STOREKEEPER)
          : postingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
      postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
      postingDetailsMap.get(ISalesFeatureFileCommonKeys.DUE_DATE)
    };
  }

  public void assertThatSalesInvoiceInformationExist(DataTable salesInvoiceDataTable) {
    List<Map<String, String>> invoiceItemsSummaryMaps =
        salesInvoiceDataTable.asMaps(String.class, String.class);

    for (Map<String, String> invoiceItemsSummaryMap : invoiceItemsSummaryMaps) {
      IObInvoiceSummaryGeneralModel invoiceItemSummaryGeneralModel =
          (IObInvoiceSummaryGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceItemsSummary",
                  constructItemsSummaryQueryParams(invoiceItemsSummaryMap));
      assertNotNull(
          "SalesInvoice Summary with code "
              + invoiceItemsSummaryMap.get(ISalesFeatureFileCommonKeys.SI_CODE)
              + " doesn't exist!",
              invoiceItemSummaryGeneralModel);
    }
  }

  private Object[] constructItemsSummaryQueryParams(Map<String, String> invoiceItemsSummaryMap) {
    return new Object[] {
      invoiceItemsSummaryMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      Double.valueOf(invoiceItemsSummaryMap.get(ISalesFeatureFileCommonKeys.TOTAL_BEFORE_TAX)),
      Double.valueOf(invoiceItemsSummaryMap.get(ISalesFeatureFileCommonKeys.TOTAL_AFTER_TAX)),
      Double.valueOf(invoiceItemsSummaryMap.get(ISalesFeatureFileCommonKeys.REMAINING))
    };
  }

  public void assertThatSalesInvoiceExist(DataTable salesInvoiceDataTable) {
    List<Map<String, String>> salesInvoicMaps =
        salesInvoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoicMap : salesInvoicMaps) {
      IObSalesInvoiceItemsGeneralModel salesInvoiceItemsGeneralModel =
          (IObSalesInvoiceItemsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoicesItems", constructSalesInvoiceItemsQueryParams(salesInvoicMap));
      assertNotNull(
          "Sales Invoice Item does not exist " + salesInvoicMap, salesInvoiceItemsGeneralModel);
    }
  }

  private Object[] constructSalesInvoiceItemsQueryParams(Map<String, String> salesInvoicMap) {
    return new Object[] {
      salesInvoicMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      salesInvoicMap.get(ISalesFeatureFileCommonKeys.ITEM),
      salesInvoicMap.get(ISalesFeatureFileCommonKeys.ORDER_UNIT_CODE_NAME),
      new BigDecimal(salesInvoicMap.get(ISalesFeatureFileCommonKeys.QUANTITY)),
            new BigDecimal(
          salesInvoicMap.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT)),
            new BigDecimal(salesInvoicMap.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)),
            new BigDecimal(salesInvoicMap.get(ISalesFeatureFileCommonKeys.TOTAL_AMOUNT))
    };
  }

  public void assertThatSubLedgerExist(DataTable subledgerDataTable) {
    List<Map<String, String>> subledgerList = subledgerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> subledger : subledgerList) {
      HObChartOfAccountsGeneralModel chartOfAccountsGeneralModel =
          (HObChartOfAccountsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGLAccountsAndSubLedgers", constructGLAccountQueryParams(subledger));
      assertNotNull(chartOfAccountsGeneralModel);
    }
  }

  private Object[] constructGLAccountQueryParams(Map<String, String> subledger) {
    return new Object[] {
      subledger.get(ISalesFeatureFileCommonKeys.SUB_LEDGER),
      subledger.get(ISalesFeatureFileCommonKeys.GL_ACCOUNT)
    };
  }

  public void assertThatJournalEntryItemsCreatedSuccessfully(
      String journalEntryCode, DataTable journalEntryItemsDataTable) {

    List<Map<String, String>> journalEntryItemsMap =
        journalEntryItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> journalEntryItem : journalEntryItemsMap) {

      IObJournalEntryItemGeneralModel journalEntryItemGeneralModel =
          (IObJournalEntryItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getJournalEntryItemsWithDueDate",
                  constructJournalEntryItemParams(journalEntryCode, journalEntryItem));
      assertNotNull(
          "Journal Item was not created! " + journalEntryItem.toString(),
          journalEntryItemGeneralModel);
      assertDateEquals(
          journalEntryItem.get(IAccountingFeatureFileCommonKeys.JOURNAL_DATE),
          journalEntryItemGeneralModel.getDueDate());
    }
  }

  private Object[] constructJournalEntryItemParams(
      String journalEntyCode, Map<String, String> journalEntryItem) {
    return new Object[] {
      journalEntryItem.get(ISalesFeatureFileCommonKeys.SUB_ACCOUNT),
      journalEntryItem.get(ISalesFeatureFileCommonKeys.EXCHANGE_RATE_CODE),
      journalEntyCode,
      journalEntryItem.get(ISalesFeatureFileCommonKeys.GL_ACCOUNT),
      new BigDecimal(journalEntryItem.get(ISalesFeatureFileCommonKeys.CREDIT)),
      new BigDecimal(journalEntryItem.get(ISalesFeatureFileCommonKeys.DEPIT)),
      journalEntryItem.get(IAccountingFeatureFileCommonKeys.DOCUMENT_CURRENCY),
      journalEntryItem.get(IAccountingFeatureFileCommonKeys.COMPANY_CURRENCY),
      new BigDecimal(journalEntryItem.get(IAccountingFeatureFileCommonKeys.EX_RATE_TO_CURRENCY_COMPANY)),
    };
  }

  public void assertThatSalesOrderStateIsUpdatedAsIssued(
      String salesOrderCode, String currentState) {

    Object actualSalesOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderByCodeAndCurrentState", salesOrderCode, "%" + currentState + "%");

    Assertions.assertNotNull(
        actualSalesOrder,
        "The updates in SO with code: " + salesOrderCode + " didn't match expected!");
  }
  public void insertSalesInvoiceActivationDetails(DataTable activationDetailsDataTable){

    List<Map<String, String>> activationDetailsMaps = activationDetailsDataTable.asMaps(String.class,
            String.class);
    for (Map<String, String> activationDetailsMap : activationDetailsMaps) {
      entityManagerDatabaseConnector.executeInsertQuery("createSalesInvoiceActivationDetails",
              constructSalesInvoiceActivationDetailsInsertionParams(activationDetailsMap));
    }
  }

  private Object[] constructSalesInvoiceActivationDetailsInsertionParams(
          Map<String, String> activationDetailsMap) {
    String[] currencyPrice = activationDetailsMap
            .get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE).split(" = ");
    String[] leftHandSide = currencyPrice[0].split(" ");
    String[] rightHandSide = currencyPrice[1].split(" ");
    String companyCurrencyIso = leftHandSide[1];
    String value = rightHandSide[0];
    String documentCurrencyIso = rightHandSide[1];

    return new Object[] { activationDetailsMap.get(IFeatureFileCommonKeys.CODE),
            activationDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            activationDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            activationDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
            activationDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            activationDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            companyCurrencyIso, documentCurrencyIso, Double.parseDouble(value),
            Double.parseDouble(value),
            activationDetailsMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR)
    };
  }
}
