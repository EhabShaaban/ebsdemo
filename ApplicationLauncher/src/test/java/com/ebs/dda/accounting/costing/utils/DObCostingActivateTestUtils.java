package com.ebs.dda.accounting.costing.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.costing.IDObCostingRestUrls;
import com.google.gson.JsonObject;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObCostingActivateTestUtils extends DObCostingCommonTestUtils {
  public DObCostingActivateTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String clearCostingDocumentInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/costing/costing_clear.sql");
    return str.toString();
  }

  public Response sendCostingPutRequest(Cookie cookie, String userCode, String dueDateTime) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("dueDate", dueDateTime);
    return sendPUTRequest(cookie, getActivateUrl(userCode), jsonObject);
  }

  private String getActivateUrl(String userCode) {
    return IDObCostingRestUrls.COMMAND_COSTING + "/" + userCode + IDObCostingRestUrls.ACTIVATE;
  }
}
