package com.ebs.dda.accounting.journalentry.utils;

import static org.junit.Assert.assertEquals;

import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.journalentry.IIObJournalEntryItemGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

public class DObJournalEntryItemsViewAllTestUtils extends DObJournalEntryTestUtils {

	public DObJournalEntryItemsViewAllTestUtils(Map<String, Object> properties,
					EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
		super(properties);
		setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	public void assertThatJournalEntryItemsExist(DataTable journalEntryItemsDataTable) {
		List<Map<String, String>> journalEntryItemsMap = journalEntryItemsDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> journalEntryItem : journalEntryItemsMap) {
			journalEntryItem = convertEmptyStringsToNulls(journalEntryItem);
			IObJournalEntryItemGeneralModel journalEntryItemGeneralModel = (IObJournalEntryItemGeneralModel) entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getJournalEntryItems",
											constructJournalEntryItemParams(journalEntryItem));
			Assert.assertNotNull("Journal Entry Item Exits " + journalEntryItem.toString(),
							journalEntryItemGeneralModel);
			assertThatJournalItemSubAccountExist(journalEntryItem, journalEntryItemGeneralModel);
			assertThatJournalItemSubLedgerExist(journalEntryItem, journalEntryItemGeneralModel);
		}
	}

	private void assertThatJournalItemSubLedgerExist(Map<String, String> journalEntryItem,
					IObJournalEntryItemGeneralModel journalEntryItemGeneralModel) {
		assertEquals("Journal Item Not exist" + journalEntryItem,
						journalEntryItem.get(IAccountingFeatureFileCommonKeys.SUBLEDGER),
						journalEntryItemGeneralModel.getSubLedger());
	}

	private void assertThatJournalItemSubAccountExist(Map<String, String> journalEntryItem,
					IObJournalEntryItemGeneralModel journalEntryItemGeneralModel) {
		if (journalEntryItem.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT) != null) {
			String[] expectedSubAccountList = journalEntryItem
							.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ");
			Assertions.assertEquals(expectedSubAccountList[0],
							journalEntryItemGeneralModel.getSubAccountCode());
			if (expectedSubAccountList.length == 2) {
				Assertions.assertEquals(expectedSubAccountList[1], journalEntryItemGeneralModel
								.getSubAccountName().getValue(ISysDefaultLocales.ENGLISH_LANG_KEY));
			}
		}
	}

	private Object[] constructJournalEntryItemParams(Map<String, String> journalEntryItem) {

		String[] accountList = journalEntryItem.get(IAccountingFeatureFileCommonKeys.ACCOUNT)
						.split(" - ");

		return new Object[] { journalEntryItem.get(IFeatureFileCommonKeys.CODE),
						journalEntryItem.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
						accountList[0], accountList[1],
						Double.parseDouble(journalEntryItem
										.get(IAccountingFeatureFileCommonKeys.DEBIT)),
						Double.parseDouble(journalEntryItem
										.get(IAccountingFeatureFileCommonKeys.CREDIT)),
						journalEntryItem.get(IAccountingFeatureFileCommonKeys.CURRENCY),
						Double.parseDouble(journalEntryItem
										.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE)) };
	}

	public void assertThatJournalEntryItemsArePresentedToUser(Response response,
					DataTable journalEntryItemsDataTable) throws Exception {
		List<HashMap<String, Object>> actualJournalEntryItems = getListOfMapsFromResponse(response);

		List<Map<String, String>> expectedJournalEntriesMap = journalEntryItemsDataTable
						.asMaps(String.class, String.class);
		assertEquals("Number of presented records doesn't match expected records",
				expectedJournalEntriesMap.size(), actualJournalEntryItems.size());
		for (int i = 0; i < actualJournalEntryItems.size(); i++) {
			assertThatJournalEntryItemsMatchesTheExpected(expectedJournalEntriesMap.get(i),
							actualJournalEntryItems.get(i));
		}
	}

	private void assertThatJournalEntryItemsMatchesTheExpected(
					Map<String, String> expectedJournalEntryItemsMap,
					HashMap<String, Object> actualJournalEntryItemsMap) throws Exception {
		MapAssertion mapAssertion = new MapAssertion(expectedJournalEntryItemsMap, actualJournalEntryItemsMap);
		expectedJournalEntryItemsMap = convertEmptyStringsToNulls(expectedJournalEntryItemsMap);
		assertThatJournalEntryMatchesTheExpected(mapAssertion);
		assertThatCompanyMatchesTheExpected(mapAssertion);
		assertThatBusinessUnitMatchesTheExpected(mapAssertion);
		assertThatAccountMatchesTheExpected(mapAssertion);

		assertThatSubAccountMatchesTheExpected(expectedJournalEntryItemsMap,
						actualJournalEntryItemsMap);

		assertThatDebitMatchesTheExpected(mapAssertion);
		assertThatCreditMatchesTheExpected(mapAssertion);

		assertThatDocumentCurrencyMatchesTheExpected(mapAssertion);
		assertThatCompanyCurrencyMatchesTheExpected(mapAssertion);
		assertThatCompanyGroupCurrencyMatchesTheExpected(mapAssertion);

		assertThatCompanyExchangeRateMatchesTheExpected(mapAssertion);
		assertThatCompanyGroupExchangeRateMatchesTheExpected(mapAssertion);
	}

	private void assertThatCompanyMatchesTheExpected(MapAssertion mapAssertion) throws Exception {
		mapAssertion.assertCodeAndLocalizedNameValue(IAccountingFeatureFileCommonKeys.COMPANY, IIObJournalEntryItemGeneralModel.COMPANY_CODE,  IIObJournalEntryItemGeneralModel.COMPANY_NAME);
	}

	private void assertThatBusinessUnitMatchesTheExpected(MapAssertion mapAssertion) throws Exception {
		mapAssertion.assertCodeAndLocalizedNameValue(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT, IIObJournalEntryItemGeneralModel.PURCHASE_UNIT_CODE,  IIObJournalEntryItemGeneralModel.PURCHASE_UNIT_NAME);
	}

	private void assertThatDocumentCurrencyMatchesTheExpected(MapAssertion mapAssertion) {
		mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.DOCUMENT_CURRENCY,
						IIObJournalEntryItemGeneralModel.DOCUMENT_CURRENCY_NAME);
	}
	private void assertThatCompanyCurrencyMatchesTheExpected(MapAssertion mapAssertion) {
		mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.COMPANY_CURRENCY,
						IIObJournalEntryItemGeneralModel.COMPANY_CURRENCY_NAME);
	}
	private void assertThatCompanyGroupCurrencyMatchesTheExpected(MapAssertion mapAssertion) {
		mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.COMPANY_GROUP_CURRENCY,
						IIObJournalEntryItemGeneralModel.COMPANY_GROUP_CURRENCY_NAME);
	}

	private void assertThatCreditMatchesTheExpected(MapAssertion mapAssertion) {
		mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.CREDIT, IIObJournalEntryItemGeneralModel.CREDIT);
	}

	private void assertThatDebitMatchesTheExpected(MapAssertion mapAssertion) {
		mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.DEBIT, IIObJournalEntryItemGeneralModel.DEBIT);
	}

	private void assertThatCompanyExchangeRateMatchesTheExpected(MapAssertion mapAssertion) {
		mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.EX_RATE_TO_CURRENCY_COMPANY,
		IIObJournalEntryItemGeneralModel.COMPANY_CURRENCY_PRICE);
	}

	private void assertThatCompanyGroupExchangeRateMatchesTheExpected(MapAssertion mapAssertion) {
		mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.EX_RATE_TO_CURRENCY_COMPANY_GROUP,
		IIObJournalEntryItemGeneralModel.COMPANY_GROUP_CURRENCY_PRICE);
	}

	private void assertThatSubAccountMatchesTheExpected(
					Map<String, String> expectedJournalEntryItemsMap,
					HashMap<String, Object> actualJournalEntryItemsMap) throws Exception {
		String actualSubAccount = null;
		String expectedSubAccount = expectedJournalEntryItemsMap
						.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT);
		actualSubAccount = assertSubAccountMatch(actualJournalEntryItemsMap, actualSubAccount);
		assertEquals(expectedSubAccount, actualSubAccount);
	}

	private void assertThatAccountMatchesTheExpected(
					MapAssertion mapAssertion) throws Exception {
		mapAssertion.assertCodeAndLocalizedNameValue(IAccountingFeatureFileCommonKeys.GL_ACCOUNT,
						IIObJournalEntryItemGeneralModel.ACCOUNT_CODE,
										IIObJournalEntryItemGeneralModel.ACCOUNT_NAME);
	}

	private String assertSubAccountMatch(HashMap<String, Object> actualJournalEntryItemsMap,
					String actualSubAccount) throws Exception {
		if (actualJournalEntryItemsMap
						.get(IIObJournalEntryItemGeneralModel.SUB_ACCOUNT_CODE) != null) {

			String actualSubAccountCode = actualJournalEntryItemsMap
							.get(IIObJournalEntryItemGeneralModel.SUB_ACCOUNT_CODE).toString();
			actualSubAccount = actualSubAccountCode;
			if (actualJournalEntryItemsMap
							.get(IIObJournalEntryItemGeneralModel.SUB_ACCOUNT_CODE) != null) {

				if (actualJournalEntryItemsMap
								.get(IIObJournalEntryItemGeneralModel.SUB_ACCOUNT_NAME) != null) {
					String actualSubAccountName = getEnglishValue(actualJournalEntryItemsMap
									.get(IIObJournalEntryItemGeneralModel.SUB_ACCOUNT_NAME));
					actualSubAccount += " - " + actualSubAccountName;
				}
			}
		}
		return actualSubAccount;
	}

	private void assertThatJournalEntryMatchesTheExpected(
					MapAssertion mapAssertion) {

		mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IIObJournalEntryItemGeneralModel.CODE);
		mapAssertion.assertDateTime(IAccountingFeatureFileCommonKeys.JOURNAL_DATE, IIObJournalEntryItemGeneralModel.DUE_DATE);
	}

	public void insertJournalEntriesDetails(DataTable journalEntriesDetailsDataTable) {
		List<Map<String, String>> detailsMaps = journalEntriesDetailsDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> detail : detailsMaps) {
			entityManagerDatabaseConnector.executeInsertQueryForObject("createJournalEntryDetails",
							constructInsertJournalEntryDetailsQueryParams(detail));
      if (!detail.get(IAccountingFeatureFileCommonKeys.SUBLEDGER).isEmpty()) {
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "createJournalEntryDetailsSubAccounts",
            constructJournalEntryDetailsSubAccountsInsertionParams(detail));
			}
		}
	}

	private Object[] constructJournalEntryDetailsSubAccountsInsertionParams(Map<String, String> detail) {
		return new Object[]{
				detail.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
				detail.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT)
		};
	}

	private Object[] constructInsertJournalEntryDetailsQueryParams(Map<String, String> detail) {
		return new Object[] { detail.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
						detail.get(IAccountingFeatureFileCommonKeys.SUBLEDGER),
						detail.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
						detail.get(IAccountingFeatureFileCommonKeys.CREDIT),
						detail.get(IAccountingFeatureFileCommonKeys.DEBIT),
						detail.get(IAccountingFeatureFileCommonKeys.DOCUMENT_CURRENCY),
						detail.get(IAccountingFeatureFileCommonKeys.COMPANY_CURRENCY),
						detail.get(IAccountingFeatureFileCommonKeys.EX_RATE_TO_CURRENCY_COMPANY),
						detail.get(IAccountingFeatureFileCommonKeys.COMPANY_GROUP_CURRENCY),
						detail.get(IAccountingFeatureFileCommonKeys.EX_RATE_TO_CURRENCY_COMPANY_GROUP)

		};
	}
}
