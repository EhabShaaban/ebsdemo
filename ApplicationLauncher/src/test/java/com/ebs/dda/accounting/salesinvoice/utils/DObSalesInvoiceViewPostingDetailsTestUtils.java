package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IIObSalesInvoicePostingDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObSalesInvoiceViewPostingDetailsTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceViewPostingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatSalesInvoiceHasThoseActivationDetails(
          String documentType, DataTable activationDetailsDataTable) {
    List<Map<String, String>> activationDetailsList =
            activationDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> activationDetails : activationDetailsList) {

      IObAccountingDocumentActivationDetailsGeneralModel accountingDocumentActivationDetails =
              (IObAccountingDocumentActivationDetailsGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getPostingDetailsByDocumentTypeAndPostingInfo",
                              constructSalesInvoiceActivationDetailsArguments(activationDetails, documentType));

      assertNotNull(accountingDocumentActivationDetails);
    }
  }

  private Object[] constructSalesInvoiceActivationDetailsArguments(
          Map<String, String> salesInvoice, String documentType) {
    return new Object[]{
            salesInvoice.get(IFeatureFileCommonKeys.CODE),
            salesInvoice.get(IFeatureFileCommonKeys.CREATED_BY),
            salesInvoice.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
            salesInvoice.get(IAccountingFeatureFileCommonKeys.EXCHANGE_RATE_CODE),
            salesInvoice.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
            salesInvoice.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
            documentType
    };
  }

  public void assertCorrectValuesAreDisplayedInActivationDetails(
          Response response, DataTable activationDetailsDataTable) throws Exception {
    Map<String, String> activationDetailsDataExpected =
            activationDetailsDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> activationDetailsDataActual = getMapFromResponse(response);

    MapAssertion mapAssertion =
            new MapAssertion(activationDetailsDataExpected, activationDetailsDataActual);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertValue(
            IFeatureFileCommonKeys.CODE, IIObSalesInvoicePostingDetailsGeneralModel.CODE);

    mapAssertion.assertDateTime(
            IFeatureFileCommonKeys.POSTING_DATE,
            IIObSalesInvoicePostingDetailsGeneralModel.ACTIVATION_DATE);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.POSTED_BY,
        IIObSalesInvoicePostingDetailsGeneralModel.MODIFIED_BY);
    ;

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE,
        IIObSalesInvoicePostingDetailsGeneralModel.JOURNAL_ENTRY_CODE);

    mapAssertion.assertEquationValues(
        IAccountingFeatureFileCommonKeys.CURRENCY_PRICE,
        constructLeftHandSide(),
        constructRightHandSide(),
        "=");

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.EXCHANGE_RATE_CODE,
        IIObSalesInvoicePostingDetailsGeneralModel.EXCHANGE_RATE_CODE);
  }

  public List<String> constructLeftHandSide() {
    List<String> leftHandSide = new ArrayList<>();
    leftHandSide.add(IIObSalesInvoicePostingDetailsGeneralModel.FIRST_VALUE);
    leftHandSide.add(IIObSalesInvoicePostingDetailsGeneralModel.FIRST_CURRENCY_ISO);
    return leftHandSide;
  }

  public List<String> constructRightHandSide() {
    List<String> rightHandSide = new ArrayList<>();
    rightHandSide.add(IIObSalesInvoicePostingDetailsGeneralModel.CURRENCY_PRICE);
    rightHandSide.add(IIObSalesInvoicePostingDetailsGeneralModel.SECOND_CURRENCY_ISO);
    return rightHandSide;
  }

  public String getDbScriptsEveryTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");

    str.append(",").append("db-scripts/sales/DObSalesInvoiceJournalEntry.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoicePostingDetails.sql");
    return str.toString();
  }
}
