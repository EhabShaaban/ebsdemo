package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostAllowedActionsHomeScreenTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostAllowedActionsObjectScreenTestUtils;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObLandedCostAllowedActionsHomeScreenStep extends SpringBootRunner implements En, InitializingBean {

    private static boolean hasBeenExecutedOnce = false;
    private DObLandedCostAllowedActionsHomeScreenTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObLandedCostAllowedActionsHomeScreenTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObLandedCostAllowedActionsHomeScreenStep() {

        When("^\"([^\"]*)\" requests to read actions of Landed Cost home screen$", (String userName) -> {
            String readUrl = IDObLandedCostRestUrls.AUTHORIZED_ACTIONS;
            Response response =
                    utils.sendGETRequest(
                            userActionsTestUtils.getUserCookie(userName), readUrl);
            userActionsTestUtils.setUserResponse(userName, response);

        });

        Then("^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$", (String allowedActionsDataTable, String userName) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertThatTheFollowingActionsExist(
                    response, allowedActionsDataTable);
        });

    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read allowed actions in Landed Cost Home Screen")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScripts());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read allowed actions in Landed Cost Home Screen")) {
            databaseConnector.closeConnection();
        }
    }

}
