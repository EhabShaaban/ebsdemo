package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import cucumber.api.DataTable;
import org.apache.commons.collections.CollectionUtils;
import org.junit.jupiter.api.Assertions;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObVendorInvoiceTestUtils extends AccountingDocumentCommonTestUtils {

  private String INVOICE_JMX_LOCK_BEAN_NAME = "InvoiceEntityLockCommand";

  public DObVendorInvoiceTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IDObVendorInvoiceSectionNames.INVOICE_DETAILS,
            IDObVendorInvoiceSectionNames.ITEMS_SECTION));
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyData.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyDataViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append("," + "db-scripts/accounting/payment-request/payment-request.sql");
    str.append("," + "db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
    str.append("," + "db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
    str.append(
        "," + "db-scripts/accounting/payment-request/iob-payment-request-payment-details-view-all.sql");
    str.append(
        "," + "db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
    str.append( "," + "db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetailsViewAll.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxesViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxes.sql");

    return str.toString();
  }
  public static String clearVendorInvoices() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/vendorinvoice/DObVendorInvoice_Clear.sql");
    return str.toString();
  }
  public void assertThatInvoicesExistWithCodeAndState(DataTable invoicesDataTable) {
    List<Map<String, String>> invoicesList = invoicesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {

      String invoiceCode = invoice.get(IFeatureFileCommonKeys.CODE);
      String invoiceState = invoice.get(IFeatureFileCommonKeys.STATE);

      DObVendorInvoiceGeneralModel invoiceGeneralModel =
          (DObVendorInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getInvoiceByCodeAndState", invoiceCode, '%' + invoiceState + '%');

      assertNotNull(invoiceGeneralModel, "Vendor Invoice does not exist " + invoice.toString());
    }
  }

  public void assertThatInvoiceIsUpdatedByUserAtDate(
      String invoiceCode, DataTable expectedInvoiceDataTable) {
    Map<String, String> expectedInvoice =
        expectedInvoiceDataTable.asMaps(String.class, String.class).get(0);
    DObVendorInvoiceGeneralModel actualInvoice =
        (DObVendorInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getInvoiceUpdatedByUserAtDate",
                    invoiceCode,
                    expectedInvoice.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                    expectedInvoice.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                    "%" + expectedInvoice.get(IFeatureFileCommonKeys.STATE) + "%");

    assertNotNull(actualInvoice);
  }

  public void assertThatInvoicesExistWithStateAndVendorAndPurchaseUnitAndCurrency(
      DataTable invoicesDataTable) {
    List<Map<String, String>> invoicesList = invoicesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {

      DObVendorInvoiceGeneralModel invoiceGeneralModel =
          (DObVendorInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getInvoiceByCodeAndStateAndPurchaseUnitAndVendorAndCurrency",
                  getInvoiceParametersByStateAndVendorAndPurchaseUnitAndCurrency(invoice));

      assertNotNull(invoiceGeneralModel, "Vendor Invoice does not exist " + invoice.toString());
    }
  }

  private Object[] getInvoiceParametersByStateAndVendorAndPurchaseUnitAndCurrency(
      Map<String, String> invoice) {

    String invoiceCode = invoice.get(IFeatureFileCommonKeys.CODE);
    String invoiceState = invoice.get(IFeatureFileCommonKeys.STATE);
    String vendorCode = invoice.get(IFeatureFileCommonKeys.VENDOR_CODE);
    String invoicePurchaseUnitCode = invoice.get(IFeatureFileCommonKeys.PURCHASE_UNIT);
    String currencyCode = invoice.get(IFeatureFileCommonKeys.CURRENCY_CODE);

    return new Object[] {
      invoiceCode, '%' + invoiceState + '%', invoicePurchaseUnitCode, vendorCode, currencyCode
    };
  }

  protected List<String> convertDownPaymentsStringToList(String downPaymentsString) {
    List<String> downPayments = Arrays.asList(downPaymentsString.replaceAll("\\s", "").split(","));

    return downPayments;
  }

  protected void assertThatDownPaymentsAreEquals(
      String expectedDownPayments, String actualDownPayments) {
    if (actualDownPayments == null) {
      assertEquals(expectedDownPayments, "");
      return;
    }

    List<String> expectedDownPaymentsList = convertDownPaymentsStringToList(expectedDownPayments);
    List<String> actualDownPaymentsList = convertDownPaymentsStringToList(actualDownPayments);

    assertTrue(CollectionUtils.isEqualCollection(expectedDownPaymentsList, actualDownPaymentsList));
  }

  public void assertThatResourceIsLockedByUser(
      String userName, String invoiceCode, String sectionName) throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=" + INVOICE_JMX_LOCK_BEAN_NAME);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, invoiceCode};
    String[] parameterTypes =
        new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
        (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);
    assertNotNull(lockReleaseDetails);
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, INVOICE_JMX_LOCK_BEAN_NAME, sectionName);
  }

  public void assertThatInvoicesExist(DataTable invoicesDataTable) {
    List<Map<String, String>> invoicesList = invoicesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {
      DObVendorInvoiceGeneralModel invoiceGeneralModel =
          (DObVendorInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getInvoiceData", constructInvoiceQueryParams(invoice));

      Assertions.assertNotNull(
          "Vendor Invoice does not exist " + invoice.toString() + invoiceGeneralModel);
    }
  }

  private Object[] constructInvoiceQueryParams(Map<String, String> invoice) {
    return new Object[] {
      invoice.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER),
      invoice.get(IFeatureFileCommonKeys.CODE),
      invoice.get(IFeatureFileCommonKeys.TYPE),
      invoice.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
      invoice.get(IFeatureFileCommonKeys.VENDOR),
      invoice.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
      '%' + invoice.get(IFeatureFileCommonKeys.STATE) + '%'
    };
  }
}
