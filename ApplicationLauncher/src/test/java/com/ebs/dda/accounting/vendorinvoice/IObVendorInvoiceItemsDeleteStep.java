package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceItemsDeleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class IObVendorInvoiceItemsDeleteStep extends SpringBootRunner implements En, InitializingBean {

  private static boolean hasBeenExecutedOnce = false;
  private IObVendorInvoiceItemsDeleteTestUtils invoiceItemsDeleteTestUtils;

  IObVendorInvoiceItemsDeleteStep() {

    Given(
        "^the following Items in Invoice with code \"([^\"]*)\" exist:$",
        (String invoiceCode, DataTable invoiceData) -> {
          invoiceItemsDeleteTestUtils.assertItemExistsById(invoiceCode, invoiceData);
        });

    When(
        "^\"([^\"]*)\" requests to delete Item with code \"([^\"]*)\" and id \"([^\"]*)\" in Invoice with code \"([^\"]*)\"$",
        (String userName, String itemCode, String itemId, String invoiceCode) -> {
          Response itemDeleteResponse =
              invoiceItemsDeleteTestUtils.deleteItemById(
                  userActionsTestUtils.getUserCookie(userName), invoiceCode, itemId);
          userActionsTestUtils.setUserResponse(userName, itemDeleteResponse);
        });

    Then(
        "^Item with code \"([^\"]*)\" and id \"([^\"]*)\" from Invoice with code \"([^\"]*)\" is deleted$",
        (String itemCode, String itemId, String invoiceCode) -> {
          invoiceItemsDeleteTestUtils.assertItemIsDeletedById(itemId);
        });

    Given(
        "^\"([^\"]*)\" first deleted Item with code \"([^\"]*)\" and id \"([^\"]*)\" of Invoice with code \"([^\"]*)\" successfully$",
        (String userName, String itemCode, String itemId, String invoiceCode) -> {
          invoiceItemsDeleteTestUtils.deleteItemById(
              userActionsTestUtils.getUserCookie(userName), invoiceCode, itemId);
          invoiceItemsDeleteTestUtils.assertItemIsDeletedById(itemId);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    invoiceItemsDeleteTestUtils = new IObVendorInvoiceItemsDeleteTestUtils(properties);
    invoiceItemsDeleteTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete Item from Invoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            invoiceItemsDeleteTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(invoiceItemsDeleteTestUtils.getDbScriptPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Delete Item from Invoice")) {
        userActionsTestUtils.unlockAllSectionsNewStandard(
                IDObVendorInvoiceRestUrls.COMMAND_URL,
                IDObVendorInvoiceRestUrls.UNLOCK_ALL_SECTION,
                invoiceItemsDeleteTestUtils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
