package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestAllowedActionsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestAllowedActionsStep extends SpringBootRunner implements En, InitializingBean {

    public static final String SCENARIO_NAME = "Read allowed actions in Payment Request";
    private static boolean hasBeenExecutedOnce = false;
  private DObPaymentRequestAllowedActionsTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObPaymentRequestAllowedActionsTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObPaymentRequestAllowedActionsStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of PaymentRequest with code \"([^\"]*)\"$",
        (String userName, String paymentRequestCode) -> {
            String url = IDObPaymentRequestRestURLs.AUTHORIZED_ACTIONS + "/" + paymentRequestCode;
          Response response =
                  utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of PaymentRequest in home screen$",
        (String userName) -> {
            String url = IDObPaymentRequestRestURLs.AUTHORIZED_ACTIONS;
          Response response =
                  utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });

    Then(
        "^the following actions are displayed to \"([^\"]*)\":$",
        (String userName, DataTable actionsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, actionsDataTable);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
        if(contains(scenario.getName(),SCENARIO_NAME)){
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(DObPaymentRequestAllowedActionsTestUtils.clearPaymentRequests());
        }
  }

  @After
  public void after(Scenario scenario) throws Exception {
      if(contains(scenario.getName(), SCENARIO_NAME)){
          databaseConnector.closeConnection();
      }
  }

}
