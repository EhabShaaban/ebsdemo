package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.CObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObSalesInvoiceTestUtils extends CommonTestUtils {

  private String SALES_INVOICE_JMX_LOCK_BEAN_NAME = "SalesInvoiceEntityLockCommand";

  public DObSalesInvoiceTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder(getMasterDataScripts());
    
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesOrderSalesInvoiceBusinessPartner_viewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");

    return str.toString();
  }

  public String getDbScriptsToClearInsertionDependancies() {
    StringBuilder str = new StringBuilder(getMasterDataScripts());
    str.append(",").append("db-scripts/sales/DObSalesInvoice_Clear_Dependancies_Before_Insertion.sql");
    return str.toString();
  }

  protected String getMasterDataScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObControlPointSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    return str.toString();
  }

  
  public void createSalesInvoiceGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesInvoiceGeneralData",
          constructInsertSalesInvoiceGeneralDataQueryParams(generalDataMap));
    }
  }

  private Object[] constructInsertSalesInvoiceGeneralDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      generalDataMap.get(IFeatureFileCommonKeys.STATE),
            generalDataMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
            generalDataMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
            generalDataMap.get(IFeatureFileCommonKeys.TYPE)
    };
  }

  public void createSalesInvoiceCompany(DataTable dataTable) {
    List<Map<String, String>> companySalesInvoiceMapList =
        dataTable.asMaps(String.class, String.class);
    for (Map<String, String> companySalesInvoiceMap : companySalesInvoiceMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesInvoiceCompany",
          constructInsertCompanySalesInvoiceQueryParams(companySalesInvoiceMap));
    }
  }

  private Object[] constructInsertCompanySalesInvoiceQueryParams(
      Map<String, String> companySalesInvoiceMap) {
    return new Object[] {
      companySalesInvoiceMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      companySalesInvoiceMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      companySalesInvoiceMap.get(ISalesFeatureFileCommonKeys.COMPANY)
    };
  }

  public void createSalesInvoiceCustomerBusinessPartner(DataTable businessPartnerTable) {
    List<Map<String, String>> businessPartnerMapList =
        businessPartnerTable.asMaps(String.class, String.class);

    for (Map<String, String> businessPartnerMap : businessPartnerMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesInvoiceCustomerBusinessPartner",
          constructInsertSalesInvoiceBusinessPartner(businessPartnerMap));
    }
  }

  private Object[] constructInsertSalesInvoiceBusinessPartner(
      Map<String, String> businessPartnerMap) {
    return new Object[] {
      businessPartnerMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      businessPartnerMap.get(ISalesFeatureFileCommonKeys.PAYMENT_TERM),
      businessPartnerMap.get(ISalesFeatureFileCommonKeys.CURRENCY),
      businessPartnerMap.get(ISalesFeatureFileCommonKeys.DOWN_PAYMENT).isEmpty()
          ? null
          : Double.parseDouble(businessPartnerMap.get(ISalesFeatureFileCommonKeys.DOWN_PAYMENT)),
      businessPartnerMap.get(ISalesFeatureFileCommonKeys.SALES_ORDER).isEmpty()
          ? null
          : businessPartnerMap.get(ISalesFeatureFileCommonKeys.SALES_ORDER),
      businessPartnerMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      businessPartnerMap.get(IFeatureFileCommonKeys.CUSTOMER)
    };
  }

  public void createSalesInvoiceItem(DataTable itemTable) {
    List<Map<String, String>> itemMapList = itemTable.asMaps(String.class, String.class);

    for (Map<String, String> itemMap : itemMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createSalesInvoiceItems", constructInsertSalesInvoiceItem(itemMap));
    }
  }

  private Object[] constructInsertSalesInvoiceItem(Map<String, String> itemMap) {
    return new Object[] {
      itemMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      itemMap.get(ISalesFeatureFileCommonKeys.ITEM),
      itemMap.get(ISalesFeatureFileCommonKeys.ORDER_UNIT_CODE_NAME),
      Double.valueOf(itemMap.get(IFeatureFileCommonKeys.QTY_ORDER_UNIT)),
      Double.valueOf(itemMap.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)),
      itemMap.get(IFeatureFileCommonKeys.BASE_UNIT)
    };
  }

  public void assertThatSalesInvoiceStateIs(String salesInvoiceCode, String salesInvoiceState) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceByState", salesInvoiceCode, "%" + salesInvoiceState + "%");
    assertNotNull(salesInvoiceGeneralModel);
  }

  public void assertThatSalesInvoiceRemainingIs(
      String salesInvoiceCode, String salesInvoiceRemaining) {
    IObInvoiceSummaryGeneralModel salesInvoiceGeneralModel =
        (IObInvoiceSummaryGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceRemaining",
                salesInvoiceCode,
                Double.parseDouble(salesInvoiceRemaining));
    assertNotNull(salesInvoiceGeneralModel);
  }

  public void assertThatSalesInvoiceTypesExist(DataTable customerDataTable) {

    List<Map<String, String>> invoiceTypeList =
        customerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoiceTYpe : invoiceTypeList) {
      CObSalesInvoice cObSalesInvoice =
          (CObSalesInvoice)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceType", constructInvoiceTypes(invoiceTYpe));
      ;
      assertNotNull(cObSalesInvoice);
    }
  }

  private Object[] constructInvoiceTypes(Map<String, String> invoiceTYpe) {
    return new Object[] {
      invoiceTYpe.get(IFeatureFileCommonKeys.CODE), invoiceTYpe.get(IFeatureFileCommonKeys.NAME)
    };
  }

  public void assertSalesInvoiceExists(DataTable salesInvoiceTable) {
    List<Map<String, String>> salesInvoices = salesInvoiceTable.asMaps(String.class, String.class);

    for (Map<String, String> salesInvoice : salesInvoices) {
      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceByCodeStateTypePurchaseUnit",
                  constructSalesInvoice(salesInvoice));
      Assert.assertNotNull(salesInvoiceGeneralModel);
    }
  }

  private Object[] constructSalesInvoice(Map<String, String> salesInvoice) {
    return new Object[] {
      salesInvoice.get(IFeatureFileCommonKeys.CODE),
      '%' + salesInvoice.get(IFeatureFileCommonKeys.STATE) + '%',
      salesInvoice.get(IFeatureFileCommonKeys.TYPE),
      salesInvoice.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertThatCustomersExist(DataTable customerDataTable) {

    List<Map<String, String>> customerList = customerDataTable.asMaps(String.class, String.class);
    for (Map<String, String> customer : customerList) {

      String PurchaseUnits = customer.get(IFeatureFileCommonKeys.PURCHASE_UNIT);

      List<String> purchaseUnitList = Arrays.asList(PurchaseUnits.split("\\s*,\\s*"));
      for (String purchaseUnit : purchaseUnitList) {

        IObCustomerBusinessUnitGeneralModel iObCustomerBusinessUnitGeneralModel =
            (IObCustomerBusinessUnitGeneralModel)
                entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getCustomerByCodeNameAndPurchaseUnit",
                    constructBusinessPartner(customer, purchaseUnit));
        assertNotNull(iObCustomerBusinessUnitGeneralModel);
      }
    }
  }

  public void assertThatResourceIsLockedByUser(
      String userName, String salesInvoiceCode, String sectionName) throws Exception {
    assertThatResourceIsLockedByUser(
        userName, salesInvoiceCode, sectionName, SALES_INVOICE_JMX_LOCK_BEAN_NAME);
  }

  private Object[] constructBusinessPartner(Map<String, String> customer, String purchaseUnit) {
    return new Object[] {
      customer.get(IFeatureFileCommonKeys.CODE),
      customer.get(IFeatureFileCommonKeys.NAME),
      purchaseUnit
    };
  }

  public void assertThatSalesInvoicesExist(DataTable salesInvoicesDataTable) {
    Map<String, String> salesInvoiceMap =
        salesInvoicesDataTable.asMaps(String.class, String.class).get(0);
    Object[] queryParams = constructSalesInvoiceQueryParams(salesInvoiceMap);

    DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesByCodeAndPurchaseUnit", queryParams);
    Assertions.assertNotNull(dObSalesInvoiceGeneralModel);
  }

  public Object[] constructSalesInvoiceQueryParams(Map<String, String> salesInvoiceMap) {
    return new Object[] {
      salesInvoiceMap.get(IFeatureFileCommonKeys.CODE),
      salesInvoiceMap.get(IFeatureFileCommonKeys.PURCHASE_UNIT_NAME)
    };
  }

  public Response deleteSalesInvoice(Cookie userCookie, String salesInvoiceCode) {
    String deleteURL = IDObSalesInvoiceRestUrls.DELETE_URL + salesInvoiceCode;
    return sendDeleteRequest(userCookie, deleteURL);
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IDObSalesInvoiceSectionNames.COMPANY_DATA_SECTION,
            IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION,
            IDObSalesInvoiceSectionNames.ITEM_SECTION));
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, SALES_INVOICE_JMX_LOCK_BEAN_NAME, sectionName);
  }

  public void assertThatSalesInvoiceIsUpdated(
      String salesInvoiceCode, DataTable salesInvoiceDataTable) {
    List<Map<String, String>> salesInvoiceUpdatedList =
        salesInvoiceDataTable.asMaps(String.class, String.class);

    for (Map<String, String> salesInvoice : salesInvoiceUpdatedList) {
      Object[] queryParams =
          constructUpdatedSalesInvoiceQueryParams(salesInvoiceCode, salesInvoice);

      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getModifiedSalesInvoice", queryParams);

      assertNotNull(salesInvoiceGeneralModel);
    }
  }

  private Object[] constructUpdatedSalesInvoiceQueryParams(
      String salesInvoiceCode, Map<String, String> salesInvoice) {
    return new Object[] {
      salesInvoiceCode,
      salesInvoice.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      salesInvoice.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      "%" + salesInvoice.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertSalesInvoicesWithCompanyAndCurrencyAndCustomerAndRemainingAmountExist(
      DataTable salesInvoiceDataTable) {
    List<Map<String, String>> salesInvoiceMaps =
        salesInvoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceMap : salesInvoiceMaps) {
      Object[] queryParams = constructSalesInvoiceDataQueryParams(salesInvoiceMap);

      DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoicesByCodeAndCompanyAndCurrencyAndCustomer", queryParams);
      assertNotNull(
          "Sales Invoice does not exist " + salesInvoiceMap.toString(),
          dObSalesInvoiceGeneralModel);
    }
  }

  private Object[] constructSalesInvoiceDataQueryParams(Map<String, String> salesInvoiceMap) {
    return new Object[] {
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.COMPANY),
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.CURRENCY),
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.CUSTOMER),
      Double.parseDouble(salesInvoiceMap.get(ISalesFeatureFileCommonKeys.REMAINING_AMOUNT))
    };
  }

  public void assertThatSalesInvoiceStateUpdated(
      String salesInvoiceCode, String salesInvoiceState) {

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceByCodeAndState", salesInvoiceCode, "%" + salesInvoiceState + "%");

    assertNotNull(salesInvoiceGeneralModel);
  }

  public void assertThatSalesInvoiceRemainingAmountUpdated(
      String salesInvoiceCode, String remainingAmount) {

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoicesByCodeAndRemainingAmount",
                salesInvoiceCode,
                Double.parseDouble(remainingAmount));

    assertNotNull(
        "Remaining amount of SalesInvoice with code "
            + salesInvoiceCode
            + " wasn't updated correctly!",
        salesInvoiceGeneralModel);
  }

  public void createSalesInvoiceTaxes(DataTable taxesDataTable) {
    List<Map<String, String>> taxesList =
            taxesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> tax : taxesList) {

      entityManagerDatabaseConnector.executeInsertQuery("createSalesInvoiceTax",
              constructSalesInvoiceTaxInsertionParams(tax));

    }
  }

  private Object[] constructSalesInvoiceTaxInsertionParams(Map<String, String> salesInvoiceTax) {
    String [] tax = salesInvoiceTax.get(IAccountingFeatureFileCommonKeys.TAX).split(" - ");
    String taxCode = tax[0];
    String taxName = tax[1];
    return new Object[]{
            salesInvoiceTax.get(ISalesFeatureFileCommonKeys.SI_CODE),
            taxCode,
            taxName,
            taxName,
            salesInvoiceTax.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE),
            salesInvoiceTax.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT),
    };
  }

  public void createSalesInvoiceSummaryRemaining(DataTable summaryDataTable) {
    List<Map<String, String>> summaryList =
            summaryDataTable.asMaps(String.class, String.class);
    for (Map<String, String> summary : summaryList) {

      entityManagerDatabaseConnector.executeInsertQuery("createSalesInvoiceSummaryIfNotExist",
              constructSalesInvoiceSummaryInsertionParams(summary));

    }
  }

  private Object[] constructSalesInvoiceSummaryInsertionParams(Map<String, String> salesInvoiceSummary) {
    return new Object[]{
            salesInvoiceSummary.get(ISalesFeatureFileCommonKeys.SI_CODE),
            Double.valueOf(salesInvoiceSummary.get(ISalesFeatureFileCommonKeys.REMAINING))
    };
  }

  public static String clearSalesInvoices() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/Sales_Invoice_Clear.sql");
    return str.toString();
  }
}
