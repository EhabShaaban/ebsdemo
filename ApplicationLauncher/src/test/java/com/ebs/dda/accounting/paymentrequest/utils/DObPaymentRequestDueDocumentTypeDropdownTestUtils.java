package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.*;

public class DObPaymentRequestDueDocumentTypeDropdownTestUtils extends DObPaymentRequestCommonTestUtils {

    public DObPaymentRequestDueDocumentTypeDropdownTestUtils(Map<String, Object> properties, EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        super(properties);
    }

    public Response readDueDocumentTypesForPaymentRequest(Cookie cookie, String paymentType) {
        return sendGETRequest(cookie, IDObPaymentRequestRestURLs.PAYMENT_REQUEST_QUERY
        +"/" + paymentType + IDObPaymentRequestRestURLs.DUE_DOCUMENT_TYPES);
    }

    public void assertDueDocumentTypesDropDownResultIsCorrect(String dueDocumentTypes, Response response){
        if ("".equals(dueDocumentTypes)) {
            Assert.assertNull(response.body().jsonPath().get(CommonKeys.DATA));
            return;
        }

        String[] dueDocumentTypesArray = dueDocumentTypes.replaceAll("\\s", "").split(",");
        Set<String> expectedDueDocumentTypes = new HashSet<>(Arrays.asList(dueDocumentTypesArray));
        Set<String> actualDueDocumentTypes = new HashSet<>(response.body().jsonPath().get(CommonKeys.DATA));
        Assert.assertEquals(expectedDueDocumentTypes, actualDueDocumentTypes);
    }
}
