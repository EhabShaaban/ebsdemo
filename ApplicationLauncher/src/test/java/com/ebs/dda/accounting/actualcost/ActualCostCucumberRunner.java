package com.ebs.dda.accounting.actualcost;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {
                "classpath:features/accounting/actual-cost/ActualCost_DownPayment.feature",
                "classpath:features/accounting/actual-cost/ActualCost_DownPayment_And_Installments.feature",
                "classpath:features/accounting/actual-cost/ActualCost_Calculate_Validation.feature"
        },
        glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.actualcost"},
        strict = true,
        tags = "not @Future")
public class ActualCostCucumberRunner {
}
