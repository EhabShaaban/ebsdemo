package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.taxes.LObVendorTaxesGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceCreationValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObVendorInvoiceCreateTestUtils extends DObVendorInvoiceTestUtils {


  public DObVendorInvoiceCreateTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");

    return str.toString();
  }

  public void assertGoodsInvoiceExistsWithCode(String invoiceCode) {
    Object dObInvoiceCode =
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLastInvoiceByCode");

    assertEquals(invoiceCode, dObInvoiceCode);
  }

  public Response createInvoice(Cookie userCookie, DataTable invoiceDataTable) {
    JsonObject valueObject = prepareValueObject(invoiceDataTable);
    return sendPostRequest(userCookie, IDObVendorInvoiceRestUrls.CREATE_URL, valueObject);
  }

  private JsonObject prepareValueObject(DataTable invoiceDataTable) {
    Map<String, String> invoiceRecord = invoiceDataTable.asMaps(String.class, String.class).get(0);
    invoiceRecord = convertEmptyStringsToNulls(invoiceRecord);

    JsonObject parsedData = new JsonObject();
    parsedData.addProperty(
        IDObVendorInvoiceCreationValueObject.PURCHESE_ORDER_CODE,
        invoiceRecord.get(IAccountingFeatureFileCommonKeys.PURCHASE_ORDER));

    parsedData.addProperty(
        IDObVendorInvoiceCreationValueObject.VENDOR_CODE,
        invoiceRecord.get(IAccountingFeatureFileCommonKeys.VENDOR));

    parsedData.addProperty(
        IDObVendorInvoiceCreationValueObject.INVOICE_TYPE,
        invoiceRecord.get(IFeatureFileCommonKeys.TYPE));

    try {
      parsedData.addProperty(
              IDObVendorInvoiceCreationValueObject.DOCUMENT_OWNER,
              Long.parseLong(invoiceRecord.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)));
    }catch (NumberFormatException exception){
      parsedData.addProperty(
              IDObVendorInvoiceCreationValueObject.DOCUMENT_OWNER,
              invoiceRecord.get(IFeatureFileCommonKeys.DOCUMENT_OWNER));
    }

    return parsedData;
  }

  public void assertInvoiceIsCreatedSuccessfully(DataTable invoiceDataTable) {

    List<Map<String, String>> invoicesTypesList =
        invoiceDataTable.asMaps(String.class, String.class);
    for (Map<String, String> invoicesType : invoicesTypesList) {
      DObVendorInvoiceGeneralModel cObInvoice =
          (DObVendorInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedVendorInvoice", constructGetCreatedInvoiceQueryParams(invoicesType));

      assertNotNull(cObInvoice);
    }
  }

  private Object[] constructGetCreatedInvoiceQueryParams(Map<String, String> invoicesType) {
    return new Object[]{
            invoicesType.get(IFeatureFileCommonKeys.CREATED_BY),
            invoicesType.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            invoicesType.get(IFeatureFileCommonKeys.CREATION_DATE),
            invoicesType.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            invoicesType.get(IFeatureFileCommonKeys.CODE),
            '%' + invoicesType.get(IFeatureFileCommonKeys.STATE) + '%',
            invoicesType.get(IFeatureFileCommonKeys.TYPE),
    };
  }

  public void assertPurchaseOrderExists(DataTable purchaseOrderDataTable) {
    List<Map<String, String>> purchaseOrderListMap =
        purchaseOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrderListMap) {

        IObOrderCompanyGeneralModel purchaseOrderGeneralModel =
                (IObOrderCompanyGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrder", constructPurchaseOrderQueryParams(purchaseOrder));

      assertNotNull(purchaseOrderGeneralModel);
      LocalizedString localizedCompanyName = purchaseOrderGeneralModel.getCompanyName();
      assertEquals(
          convertEmptyStringToNull(purchaseOrder.get(IAccountingFeatureFileCommonKeys.COMPANY)),
              checkIfCompanyIsNull(
                      purchaseOrderGeneralModel.getCompanyCode(), localizedCompanyName != null ? localizedCompanyName.getValue("en") : null), purchaseOrder.get(IFeatureFileCommonKeys.CODE));
    }
  }

  private String checkIfCompanyIsNull(String companyCode, String companyName) {
    if (companyCode == null
            || companyName == null
            || companyCode.isEmpty()
            || companyName.isEmpty()) {
      return null;
    }
    return (companyCode + " - " + companyName);
  }

  private Object[] constructPurchaseOrderQueryParams(Map<String, String> purchaseOrder) {
    return new Object[]{
            purchaseOrder.get(IFeatureFileCommonKeys.CODE),
            '%' + purchaseOrder.get(IFeatureFileCommonKeys.STATE) + '%',
            purchaseOrder.get(IAccountingFeatureFileCommonKeys.CURRENCY),
            purchaseOrder.get(IAccountingFeatureFileCommonKeys.PAYMENT_TERMS),
            purchaseOrder.get(IAccountingFeatureFileCommonKeys.PURCHASE_UNIT)
    };
  }

  public void assertPurchaseOrderItemsWithCode(String poCode, DataTable itemDataTable) {
    List<Map<String, String>> itemsListMap =
        itemDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsListMap) {

      Object orderLineDetailsQuantitiesGeneralModel =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrderItemsWithQuantities",
                  constructPurchaseOrderItemQueryParams(poCode, item));

      assertNotNull(orderLineDetailsQuantitiesGeneralModel);
    }
  }

  private Object[] constructPurchaseOrderItemQueryParams(String poCode,
      Map<String, String> poItemDataTable) {
    return new Object[]{
        poCode,
        poItemDataTable.get(IAccountingFeatureFileCommonKeys.ITEM_CODE),
        Double.parseDouble(
            poItemDataTable.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER)),
        poItemDataTable.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT),
        Double.parseDouble(poItemDataTable.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE))
    };
  }

  public void assertVendorInvoiceItemsWithCode(String invoiceCode,
      DataTable invoiceItemsDataTable) {
    List<Map<String, String>> itemsListMap =
        invoiceItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsListMap) {

      Object orderLineDetailsQuantitiesGeneralModel =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorInvoicesItems",
                  constructVendorInvoiceItemQueryParams(invoiceCode, item));

      assertNotNull(orderLineDetailsQuantitiesGeneralModel);
    }
  }

  private Object[] constructVendorInvoiceItemQueryParams(String invoiceCode,
      Map<String, String> item) {
    return new Object[]{
        invoiceCode,
        item.get(IAccountingFeatureFileCommonKeys.ITEM_CODE),
        item.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER),
        item.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT),
        item.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)
    };
  }

  public void assertThatTheFollowingTaxDetailsExistForVendorInvoice(
          String vendorInvoiceCode, DataTable taxDetailsDataTable) {
    List<Map<String, String>> taxDetailsMaps =
            taxDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> taxDetailsMap : taxDetailsMaps) {

      Object[] queryParams =
              constructVendorTaxesForVendorInvoiceQueryParams(vendorInvoiceCode, taxDetailsMap);

      IObVendorInvoiceTaxesGeneralModel vendorInvoiceTaxesGeneralModel =
              (IObVendorInvoiceTaxesGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getVendorInvoiceTaxes", queryParams);
      Assert.assertNotNull("Tax detail doesn't exist "+ taxDetailsMap.toString(), vendorInvoiceTaxesGeneralModel);
    }
  }

  public Object[] constructVendorTaxesForVendorInvoiceQueryParams(
          String vendorInvoiceCode, Map<String, String> vendorTaxesValues) {
    return new Object[] {
            vendorInvoiceCode,
            vendorTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX),
            Double.valueOf(vendorTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)),
            Double.valueOf(vendorTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT))
    };
  }

    public void assertThatVendorInvoiceCompanyDataIs(String vendorInvoiceCode, DataTable companyDataTable) {
      List<Map<String, String>> vendorInvoiceCompanyDataTableList = companyDataTable
              .asMaps(String.class, String.class);
      for (Map<String, String> expectedCompanyData : vendorInvoiceCompanyDataTableList) {

        Long companyDataId = (Long) entityManagerDatabaseConnector
                .executeNativeNamedQuerySingleResult("getCreatedVendorInvoiceCompanyData",
                        constructVendorInvoiceCompanyDataQueryParams(
                                vendorInvoiceCode,
                                expectedCompanyData));

        Assertions.assertNotNull(companyDataId, "VendorInvoice with code " + vendorInvoiceCode
                + " Company Data is not Created");
      }
    }
  private Object[] constructVendorInvoiceCompanyDataQueryParams(String vendorInvoiceCode,
                                                             Map<String, String> expectedCompanyData) {

    return new Object[] { vendorInvoiceCode,
            expectedCompanyData.get(IFeatureFileCommonKeys.COMPANY),
            expectedCompanyData.get(IFeatureFileCommonKeys.BUSINESS_UNIT),

    };
  }
  public void assertThatVendorInvoiceItemsAre(String vendorInvoiceCode, DataTable itemsDataTable) {
    List<Map<String, String>> vendorInvoiceItemsDataTableList = itemsDataTable
            .asMaps(String.class, String.class);
    for (Map<String, String> expectedItems : vendorInvoiceItemsDataTableList) {

      Long vendorInvoiceItemId = (Long) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("getCreatedVendorInvoiceItems",
                      constructVendorInvoiceItemsQueryParams(
                              vendorInvoiceCode,
                              expectedItems));

      Assertions.assertNotNull(vendorInvoiceItemId, "VendorInvoice with code " + vendorInvoiceCode
              + " Item is not Created "+ expectedItems);
    }
  }
  private Object[] constructVendorInvoiceItemsQueryParams(String vendorInvoiceCode,
                                                                Map<String, String> expectedItem) {
    String[] qtyOrderUnitArray = expectedItem.get(IFeatureFileCommonKeys.QTY_ORDER_UNIT).split(" ", 2);
    String[] qtyBaseUnitArray = expectedItem.get(IFeatureFileCommonKeys.QTY_BASE_UNIT).split(" ", 2);

    return new Object[] { vendorInvoiceCode,
            expectedItem.get(IFeatureFileCommonKeys.ITEM),
            qtyOrderUnitArray[0],
            qtyOrderUnitArray[1],
            qtyBaseUnitArray[0],
            qtyBaseUnitArray[1],
            expectedItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT),
            expectedItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)

    };
  }

  public void assertThatTheFollowingDetailsExistForVendorInvoice(String vendorInvoiceCode, DataTable detailsDataTable) {
    List<Map<String, String>> vendorInvoiceDetailsTableList = detailsDataTable
            .asMaps(String.class, String.class);
    for (Map<String, String> expectedDetails : vendorInvoiceDetailsTableList) {

      Long detailsId = (Long) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("getCreatedVendorInvoiceDetails",
                      constructVendorInvoiceDetailsQueryParams(
                              vendorInvoiceCode,
                              expectedDetails));

      Assertions.assertNotNull(detailsId, "VendorInvoice with code " + vendorInvoiceCode
              + " Details are not Created");
    }
  }
  private Object[] constructVendorInvoiceDetailsQueryParams(String vendorInvoiceCode,
                                                          Map<String, String> expectedDetails) {
    return new Object[] {

            expectedDetails.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER),
            expectedDetails.get(IAccountingFeatureFileCommonKeys.INVOICE_DATE),
            expectedDetails.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT),
            vendorInvoiceCode,
            expectedDetails.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
            expectedDetails.get(IFeatureFileCommonKeys.VENDOR),
            expectedDetails.get(IFeatureFileCommonKeys.PAYMENT_TERMS),
            expectedDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY),

    };
  }

  public void assertThatVendorInvoiceHasNoTaxes(String vendorInvoiceCode) {
    Long taxesCount = (Long) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getVendorInvoicesTaxesCount", vendorInvoiceCode);
    assertEquals(Long.valueOf(0), taxesCount);
  }
}
