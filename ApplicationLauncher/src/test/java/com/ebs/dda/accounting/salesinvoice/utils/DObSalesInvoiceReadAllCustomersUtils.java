package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceCreateTestUtils;
import com.ebs.dda.jpa.masterdata.customer.IIObCustomerBusinessUnitGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesInvoiceReadAllCustomersUtils extends DObSalesInvoiceCreateTestUtils {

  public DObSalesInvoiceReadAllCustomersUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public void assertThatResponseCustomerDataIsCorrect(
      Response response, DataTable customerDataTable) throws Exception {
    List<Map<String, String>> expectedCustomerList =
        customerDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualCusomerData = getListOfMapsFromResponse(response);

    for (int i = 0; i < actualCusomerData.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedCustomerList.get(i), actualCusomerData.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          ISalesFeatureFileCommonKeys.CUSTOMER,
          IIObCustomerBusinessUnitGeneralModel.CUSTOMER_CODE,
          IIObCustomerBusinessUnitGeneralModel.CUSTOMER_NAME);
    }
  }

  public void assertTotalNumberOfRecordsEqualResponseSize(
      Response response, int totalRecordsNumber) {
    List<HashMap<String, Object>> responseData = getListOfMapsFromResponse(response);
    assertEquals(responseData.size(), totalRecordsNumber);
  }
}
