package com.ebs.dda.accounting.accountingnote.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNoteGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ActiveDebitNotesDropDownTestUtil extends AccountingNoteCommonTestUtil {


    public ActiveDebitNotesDropDownTestUtil(Map<String, Object> properties) {
        setProperties(properties);
    }

    public void assertThatNumberOfAccountingNotesIS(Long recordsNumber) {
        Object actualTotalNumberOfAccountingNotes = entityManagerDatabaseConnector
                .executeNativeNamedQuerySingleResult("getTotalNumberOfAccountingNotes");
        assertEquals(recordsNumber, actualTotalNumberOfAccountingNotes);
    }


    public void assertThatNotesActualResponseMatchesExpected(
            Response response, DataTable notesDataTable) throws Exception {
        List<Map<String, String>> expectedNotes =
                notesDataTable.asMaps(String.class, String.class);
        List<HashMap<String, Object>> actualNotes = getListOfMapsFromResponse(response);

        for (int i = 0; i < expectedNotes.size(); i++) {
            MapAssertion mapAssertion =
                    new MapAssertion(expectedNotes.get(i), actualNotes.get(i));

            mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObAccountingNoteGeneralModel.USER_CODE);

            mapAssertion.assertValue(
                    IAccountingFeatureFileCommonKeys.NOTE_TYPE,
                    IDObAccountingNoteGeneralModel.TYPE);

            mapAssertion.assertCodeAndLocalizedNameValue(
                    IFeatureFileCommonKeys.BUSINESS_UNIT,
                    IDObAccountingNoteGeneralModel.PURCHASE_UNIT_CODE,
                    IDObAccountingNoteGeneralModel.PURCHASE_UNIT_NAME);

            mapAssertion.assertValueContains(
                    IFeatureFileCommonKeys.STATE, IDObAccountingNoteGeneralModel.CURRENT_STATES);
            mapAssertion.assertDoubleValue(
                    IAccountingFeatureFileCommonKeys.REMAINING, IDObAccountingNoteGeneralModel.REMAINING);
        }
    }

    public String clearAccountingNoteInsertions() {
        StringBuilder str = new StringBuilder();
        str.append("db-scripts/accounting/accountingnote/accounting_note_clear_drop_down.sql");
        return str.toString();
    }
}
