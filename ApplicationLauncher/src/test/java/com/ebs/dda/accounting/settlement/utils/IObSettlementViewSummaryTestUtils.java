package com.ebs.dda.accounting.settlement.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.settlement.IDObSettlementRestURLs;
import com.ebs.dda.jpa.accounting.settlements.IIObSettlementDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IIObSettlementSummaryGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class IObSettlementViewSummaryTestUtils extends DObSettlementsCommonTestUtils{

    public IObSettlementViewSummaryTestUtils(Map<String, Object> properties) {
        super(properties);
    }

    public String getViewSummaryUrl(String code) {
        return IDObSettlementRestURLs.BASE_URL + "/" + code
                + IDObSettlementRestURLs.VIEW_SUMMARY;
    }

    public void assertThatViewSettlementSummaryResponseIsCorrect(Response response, DataTable summaryDataTable) {
        Map<String, String> expectedSummaryMap = summaryDataTable.asMaps(String.class, String.class).get(0);
        Map<String, Object> actualSummaryMap = getMapFromResponse(response);
        assertActualAndExpectedDataFromResponse(expectedSummaryMap, actualSummaryMap);
    }

    private void assertActualAndExpectedDataFromResponse(Map<String, String> expectedSummaryMap,
                                                         Map<String, Object>actualSummaryMap) {
        MapAssertion mapAssertion = new MapAssertion(expectedSummaryMap, actualSummaryMap);

        mapAssertion.assertValue(IFeatureFileCommonKeys.CODE,
                IIObSettlementSummaryGeneralModel.SETTLEMENT_CODE);

        assertAmountAndCurrency(expectedSummaryMap.get(IAccountingFeatureFileCommonKeys.TOTAL_DEBIT),
                actualSummaryMap.get(IIObSettlementSummaryGeneralModel.TOTAL_DEBIT).toString(),
                actualSummaryMap.get(IIObSettlementDetailsGeneralModel.CURRENCY_ISO).toString());

        assertAmountAndCurrency(expectedSummaryMap.get(IAccountingFeatureFileCommonKeys.TOTAL_CREDIT),
                actualSummaryMap.get(IIObSettlementSummaryGeneralModel.TOTAL_CREDIT).toString(),
                actualSummaryMap.get(IIObSettlementDetailsGeneralModel.CURRENCY_ISO).toString());
    }

    private void assertAmountAndCurrency(String expectedAmountCurrency, String actualAmount, String actualCurrency) {
        String expectedAmount = expectedAmountCurrency.split(" ")[0];
        String expectedCurrency = expectedAmountCurrency.split(" ")[1];
        assertEquals(Double.parseDouble(expectedAmount), Double.parseDouble(actualAmount), 1e-15);
        assertEquals(expectedCurrency, actualCurrency);
    }
}
