package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotesReceivableReferenceDocumentSalesInvoiceDropdownTestUtils
    extends DObMonetaryNotesCommonTestUtils {

  protected String ASSERTION_MSG = "[NotesReceivable][ReferenceDocument][SalesInvoice][Dropdown]";

  public NotesReceivableReferenceDocumentSalesInvoiceDropdownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/SalesInvoice_Clear.sql");
    return str.toString();
  }

  public Response readAllReferenceDocumentSalesInvoices(Cookie cookie, String notesReceivableCode) {
    String restURL =
        IDObNotesReceivableRestURLs.QUERY
            + "/"
            + notesReceivableCode
            + IDObNotesReceivableRestURLs.READ_ALL_REFERENCE_DOCUMENT_SALES_INVOICES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReferenceDocumentSalesInvoicesResponseIsCorrect(
      DataTable salesInvoicesDataTable, Response response) {
    List<Map<String, String>> expectedSalesInvoicesMapList =
        salesInvoicesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualSalesInvoicesList = getListOfMapsFromResponse(response);
    Assert.assertEquals(expectedSalesInvoicesMapList.size(), actualSalesInvoicesList.size());
    for (int i = 0; i < expectedSalesInvoicesMapList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(expectedSalesInvoicesMapList.get(i), actualSalesInvoicesList.get(i));
      mapAssertion.assertValue(
          IFeatureFileCommonKeys.CODE, IDObSalesInvoiceGeneralModel.INVOICE_CODE);
    }
  }

  public void assertThatTotalNumberOfReferenceDocumentSalesInvoicesEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualSalesInvoices = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of ReferenceDocument SalesInvoices is not correct",
        expectedTotalNumber.intValue(),
        actualSalesInvoices.size());
  }
}
