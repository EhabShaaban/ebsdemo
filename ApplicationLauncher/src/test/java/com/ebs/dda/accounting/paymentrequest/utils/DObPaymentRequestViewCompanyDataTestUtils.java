package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentCompanyDataGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestViewCompanyDataTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestViewCompanyDataTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public void assertThatTheFollowingPRCompanyDataExists(DataTable companyDataTable) {
    List<Map<String, String>> prGeneralDataListMap =
        companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prGeneralDataMap : prGeneralDataListMap) {
      String paymentRequestCode = prGeneralDataMap.get(IAccountingFeatureFileCommonKeys.PR_CODE);
      assertThatPRCompanyDataExists(paymentRequestCode,prGeneralDataMap);
    }
  }

  public String getViewCompanyDataUrl(String prCode) {
    return IDObPaymentRequestRestURLs.BASE_URL
        + "/"
        + prCode
        + IDObPaymentRequestRestURLs.VIEW_COMPANY_DATA;
  }

  public void assertThatViewCompanyDataResponseIsCorrect(
      Response response, DataTable companyDataTable) throws Exception {

    Map<String, String> expectedCompanyDataMap =
        companyDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualCompanyDataMap = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(expectedCompanyDataMap, actualCompanyDataMap);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.PR_CODE,
        IIObAccountingDocumentCompanyDataGeneralModel.DOCUMENT_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IIObAccountingDocumentCompanyDataGeneralModel.PURCHASE_UNIT_CODE,
        IIObAccountingDocumentCompanyDataGeneralModel.PURCHASING_UNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        IIObAccountingDocumentCompanyDataGeneralModel.COMPANY_CODE,
        IIObAccountingDocumentCompanyDataGeneralModel.COMPANY_NAME);
  }
}
