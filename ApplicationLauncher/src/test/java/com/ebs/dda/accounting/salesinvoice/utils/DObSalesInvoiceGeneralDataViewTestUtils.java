package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObSalesInvoiceGeneralDataViewTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceGeneralDataViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void checkSalesInvoiceGeneralDataExists(DataTable generalDataTable) {
    List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> generalDataMap : generalDataMaps) {
      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceGeneralData",
                  constructSalesInvoiceGeneralDataQueryParams(generalDataMap));
      Assert.assertNotNull("Sales Invoice doesn't exist", salesInvoiceGeneralModel);
    }
  }

  private Object[] constructSalesInvoiceGeneralDataQueryParams(
      Map<String, String> generalDataMap) {
    return new Object[]{
        generalDataMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
        generalDataMap.get(IFeatureFileCommonKeys.TYPE),
        generalDataMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
        generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
        '%' + generalDataMap.get(IFeatureFileCommonKeys.STATE) + '%',
        generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE)
    };
  }

  public void assertCorrectValuesAreDisplayedInGeneralData(
      Response response, DataTable generalDataTable) throws Exception {
    Map<String, String> generalDataExpected =
        generalDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> generalDataActual = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(generalDataExpected, generalDataActual);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TYPE,
        IDObSalesInvoiceGeneralModel.INVOICE_TYPE_CODE,
        IDObSalesInvoiceGeneralModel.INVOICE_TYPE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PURCHASING_UNIT,
        IDObSalesInvoiceGeneralModel.PURCHASE_UNIT_CODE,
        IDObSalesInvoiceGeneralModel.PURCHASE_UNIT);

    mapAssertion.assertValue(
            IFeatureFileCommonKeys.DOCUMENT_OWNER,
        IDObSalesInvoiceGeneralModel.DOCUMENT_OWNER);
    ;

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObSalesInvoiceGeneralModel.CREATION_INFO_NAME);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObSalesInvoiceGeneralModel.CURRENT_STATES);

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObSalesInvoiceGeneralModel.CREATION_DATE);
  }
}
