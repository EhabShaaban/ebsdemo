package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import java.util.Map;

public class DObSalesInvoiceCompanyDataRequestEditTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceCompanyDataRequestEditTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    return str.toString();
  }

  public StringBuilder getLockUrl(String salesInvoiceCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObSalesInvoiceRestUrls.COMMAND_URL);
    lockUrl.append(salesInvoiceCode);
    lockUrl.append(IDObSalesInvoiceRestUrls.LOCK_SALES_INVOICE_COMPANY_DATA_SECTION);
    return lockUrl;
  }

  public String getUnLockUrl(String salesInvoiceCode) {
    String unLockUrl =
        IDObSalesInvoiceRestUrls.COMMAND_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.UNLOCK_SALES_INVOICE_COMPANY_DATA_SECTION;
    return unLockUrl;
  }
}
