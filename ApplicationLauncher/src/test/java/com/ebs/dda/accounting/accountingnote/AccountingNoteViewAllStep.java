package com.ebs.dda.accounting.accountingnote;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingnote.utils.AccountingNoteViewAllTestUtil;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNoteGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class AccountingNoteViewAllStep extends SpringBootRunner implements En {

  private AccountingNoteViewAllTestUtil utils;

  public AccountingNoteViewAllStep() {

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IAccountingNotesRestUrls.VIEW_ALL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable creditDebitNoteData) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertCreditDebitNoteDataIsCorrect(creditDebitNoteData, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(
                  IDObAccountingNoteGeneralModel.USER_CODE, userCodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IAccountingNotesRestUrls.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String type, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(IDObAccountingNoteGeneralModel.OBJECT_TYPE_CODE, type);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IAccountingNotesRestUrls.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with filter applied on BusinessPartner which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String businessPartner,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObAccountingNoteGeneralModel.BUSINESS_PARTNER, businessPartner);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IAccountingNotesRestUrls.VIEW_ALL,
                  pageNumber,
                  rowsNumber,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with filter applied on DocumentOwner which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String documentOwner, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObAccountingNoteGeneralModel.DOCUMENT_OWNER_NAME, documentOwner);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IAccountingNotesRestUrls.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit with filter applied on ReferenceDocument which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String referenceDocumentValue,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObAccountingNoteGeneralModel.REFERENCE_DOCUMENT, referenceDocumentValue);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IAccountingNotesRestUrls.VIEW_ALL,
                  pageNumber,
                  rowsNumber,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with filter applied on Amount which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String amount, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getEqualsFilterField(IDObAccountingNoteGeneralModel.AMOUNT, amount);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IAccountingNotesRestUrls.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String businessUnitCode, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(
                  IDObAccountingNoteGeneralModel.PURCHASE_UNIT_CODE, businessUnitCode);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IAccountingNotesRestUrls.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Credit/Debit Notes with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String stateName, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObAccountingNoteGeneralModel.CURRENT_STATES, stateName);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IAccountingNotesRestUrls.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new AccountingNoteViewAllTestUtil(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all Credit/Debit Notes,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
