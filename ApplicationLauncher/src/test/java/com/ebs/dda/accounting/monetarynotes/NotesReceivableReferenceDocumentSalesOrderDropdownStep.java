package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.NotesReceivableReferenceDocumentSalesOrderDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class NotesReceivableReferenceDocumentSalesOrderDropdownStep extends SpringBootRunner
    implements En {

    public static final String SCENARIO_NAME = "Read list of SalesOrder - ReferenceDocument";
  private NotesReceivableReferenceDocumentSalesOrderDropdownTestUtils utils;

  public NotesReceivableReferenceDocumentSalesOrderDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all SalesOrder - ReferenceDocument for NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String notesReceivableCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.readAllReferenceDocumentSalesOrders(cookie, notesReceivableCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesOrder - ReferenceDocument values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesOrdersDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReferenceDocumentSalesOrdersResponseIsCorrect(
              salesOrdersDataTable, response);
        });

    Then(
        "^total number of SalesOrder - ReferenceDocument returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer salesOrderCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfReferenceDocumentSalesOrdersEquals(
              salesOrderCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new NotesReceivableReferenceDocumentSalesOrderDropdownTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
