package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceRequestEditBusinessPartnerTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesInvoiceRequestEditBusinessPartnerStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObSalesInvoiceRequestEditBusinessPartnerTestUtils
      iObSalesInvoiceRequestEditBusinessPartnerTestUtils;

  public IObSalesInvoiceRequestEditBusinessPartnerStep() {

    When(
        "^\"([^\"]*)\" requests to edit BusinessPartner section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          String lockUrl =
              iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getLockBusinessPartnerUrl(
                  salesInvoiceCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^BusinessPartner section of SalesInvoice with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          iObSalesInvoiceRequestEditBusinessPartnerTestUtils.assertResponseSuccessStatus(response);
          iObSalesInvoiceRequestEditBusinessPartnerTestUtils.assertThatResourceIsLockedByUser(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened BusinessPartner section of SalesInvoice with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String salesInvoiceCode) -> {
          StringBuilder lockUrl =
              iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getLockUrl(salesInvoiceCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockUrl.toString(), salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesInvoice section of SalesInvoice with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName) -> {
          iObSalesInvoiceRequestEditBusinessPartnerTestUtils.assertThatLockIsReleased(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION);
        });

    Given(
        "^BusinessPartner section of SalesInvoice with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName, String lockTime) -> {
          iObSalesInvoiceRequestEditBusinessPartnerTestUtils.freeze(lockTime);
          StringBuilder lockUrl =
              iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getLockUrl(salesInvoiceCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockUrl.toString(), salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving BusinessPartner section of SalesInvoice with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode, String unlockTime) -> {
          iObSalesInvoiceRequestEditBusinessPartnerTestUtils.freeze(unlockTime);
          String unLockUrl =
              iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getUnLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on BusinessPartner section of SalesInvoice with Code \"([^\"]*)\" is released$",
        (String userName, String salesInvoiceCode) -> {
          iObSalesInvoiceRequestEditBusinessPartnerTestUtils.assertThatLockIsReleased(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving BusinessPartner section of SalesInvoice with Code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          String unLockUrl = iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getUnLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    iObSalesInvoiceRequestEditBusinessPartnerTestUtils =
        new IObSalesInvoiceRequestEditBusinessPartnerTestUtils(
            properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Request to edit BusinessPartner section")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(
          iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit BusinessPartner section")) {


        userActionsTestUtils.unlockAllSections(
                IDObSalesInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS, iObSalesInvoiceRequestEditBusinessPartnerTestUtils.getSectionsNames());

        databaseConnector.closeConnection();
    }
  }
}
