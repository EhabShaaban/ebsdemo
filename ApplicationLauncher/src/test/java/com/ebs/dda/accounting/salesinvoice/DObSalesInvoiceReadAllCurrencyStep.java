package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceReadAllCurrencyUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceReadAllCurrencyStep extends SpringBootRunner implements En {

  private DObSalesInvoiceReadAllCurrencyUtils salesInvoiceReadAllCurrencyUtils;

  public DObSalesInvoiceReadAllCurrencyStep() {
      When("^\"([^\"]*)\" requests to read all Currency$", (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String readCurrencyUrl = IDObSalesInvoiceRestUrls.READ_CURRENCY_URL;
          Response response =
              salesInvoiceReadAllCurrencyUtils.sendGETRequest(cookie, readCurrencyUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Currency values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable currencyDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceReadAllCurrencyUtils.assertThatResponseCurrencyDataIsCorrect(
              response, currencyDataTable);
        });
    Then(
        "^total number of Currency returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceReadAllCurrencyUtils.assertTotalNumberOfRecordsEqualResponseSize(
              response, totalRecordsNumber);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    salesInvoiceReadAllCurrencyUtils =
        new DObSalesInvoiceReadAllCurrencyUtils(properties, entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "Read list of Currency dropdown")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(salesInvoiceReadAllCurrencyUtils.getDbScripts());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read list of Currency dropdown")) {
      databaseConnector.closeConnection();
      }
  }
}
