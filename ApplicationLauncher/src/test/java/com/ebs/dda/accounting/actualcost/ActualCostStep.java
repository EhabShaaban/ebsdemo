package com.ebs.dda.accounting.actualcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCreateJournalEntryTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class ActualCostStep extends SpringBootRunner implements En {

  private ActualCostTestUtils actualCostTestUtils;
  private DObPaymentRequestCreateJournalEntryTestUtils paymentRequestCreateJournalEntryTestUtils;

  public ActualCostStep() {
    Given(
        "^the following PurchaseOrders for actual cost exist:$",
        (DataTable purchaseOrdersDataTable) -> {
          actualCostTestUtils.assertPurchaseOrdersExistWithAdvancedData(purchaseOrdersDataTable);
        });

    Given(
        "^the following Goods Receipts for actual cost exist:$",
        (DataTable goodsRecieptsDataTable) -> {
          actualCostTestUtils.assertGoodsReceiptPurchaseOrderExist(goodsRecieptsDataTable);
        });

    Given(
        "^the following Goods VendorInvoice for actual cost exists:$",
        (DataTable goodsVendorInvoicesTable) -> {
          actualCostTestUtils.assertThatGoodsVendorInvoicesExist(goodsVendorInvoicesTable);
        });

    Given(
        "^the following Custom Trustee VendorInvoice for actual cost exists:$",
        (DataTable customTrusteeVendorInvoicesDataTable) -> {
          actualCostTestUtils.assertThatCustomTrusteeVendorInvoicesExist(
              customTrusteeVendorInvoicesDataTable);
        });

    Given(
        "^VendorInvoice with code \"([^\"]*)\" has the following InvoiceItems:$",
        (String vendorInvoiceCode, DataTable VendorInvoicesItems) -> {
          actualCostTestUtils.assertVendorInvoiceItemExistsForAcutalCost(
              vendorInvoiceCode, VendorInvoicesItems);
        });

    Given(
        "^the following PaymentRequests exist:$",
        (DataTable paymentRequestsDataTable) -> {
          actualCostTestUtils.assertThatPaymentRequestExists(paymentRequestsDataTable);
        });

    Given(
        "^the Last ExchangeRate was as following:$",
        (DataTable exchangeRateDataTable) -> {
          actualCostTestUtils.assertThatLastExchangeRateIs(exchangeRateDataTable);
        });

    Given(
        "^the following JournalEntriesItems for actual cost exist:$",
        (DataTable journalEntriesItemsDataTable) -> {
          actualCostTestUtils.assertThatJournalEntriesItemsForActualCostExist(
              journalEntriesItemsDataTable);
        });

    Given(
        "^the following GoodsReceiptAccountingDocument exist:$",
        (DataTable grAccountingDocumentDataTable) -> {
          actualCostTestUtils.assertThatGoodsReceiptAccountingDocumentExist(
              grAccountingDocumentDataTable);
        });

    Given(
        "^GoodsReceiptAccountingDocument has the following Details:$",
        (DataTable grAccountingDocumentDetailsDataTable) -> {
          actualCostTestUtils.assertThatGoodsReceiptAccountingDocumentDetailsExist(
              grAccountingDocumentDetailsDataTable);
        });

    Given(
        "^GoodsReceiptAccountingDocument has the following ItemDetails:$",
        (DataTable grAccountingDocumentItemDetailsDataTable) -> {
          actualCostTestUtils.assertThatGoodsReceiptAccountingDocumentItemDetails(
              grAccountingDocumentItemDetailsDataTable);
        });

    Given(
        "^Last Journal Entry Code is \"([^\"]*)\"$",
        (String journalEntryCode) -> {
          paymentRequestCreateJournalEntryTestUtils.assertLastJournalEntry(journalEntryCode);
        });

    When(
        "^\"([^\"]*)\" requests to calculate ActualCost to GoodsReceipts with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String goodsReceiptsCode, String dateTime) -> {
          actualCostTestUtils.freeze(dateTime);
          Response response =
              actualCostTestUtils.calculateActualCost(
                  goodsReceiptsCode, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^ItemDetails in GoodsReceiptAccountingDocument with Code \"([^\"]*)\" is updated as follows:$",
        (String grAccDocCode, DataTable itemDetailsDataTable) -> {
          actualCostTestUtils.assertThatGoodsReceiptAccountingDocumentItemDetailsIsUpdated(
              grAccDocCode, itemDetailsDataTable);
        });

    Given(
        "^the PO \"([^\"]*)\" related to GRAccountingDocument \"([^\"]*)\" has no posted Custom Trustee Invoice$",
        (String poCode, String grAccountingDocumentCode) -> {
            actualCostTestUtils.assertThatPORelatedToGRAccountingDocument(poCode,grAccountingDocumentCode);
          actualCostTestUtils.assertThatPurchaseOrderDoeNotHasPostedCustomTrusteeIvoice(poCode);
        });

    Given(
        "^the PO \"([^\"]*)\" related to GRAccountingDocument \"([^\"]*)\" has no posted CustomTrustee VendorInvoice$",
        (String poCode, String grAccountingDocumentCode) -> {
            actualCostTestUtils.assertThatPORelatedToGRAccountingDocument(poCode,grAccountingDocumentCode);
          actualCostTestUtils.assertThatPurchaseOrderDoeNotHasPostedCustomTrusteeIvoice(poCode);
        });

    When(
        "^\"([^\"]*)\" requests to calculate Actual Cost for GoodsReceipts with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String grCode, String dateTime) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = actualCostTestUtils.calculateActualCost(grCode, userCookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^the PO \"([^\"]*)\" related to GRAccountingDocument \"([^\"]*)\" has posted Goods VendorInvoice with code \"([^\"]*)\"$",
        (String poCode, String grAccountingDocumentCode, String viCode) -> {
            actualCostTestUtils.assertThatPORelatedToGRAccountingDocument(poCode,grAccountingDocumentCode);
          actualCostTestUtils.assertThatPurchaseOrderHasPostedGoodsIvoice(poCode, viCode);
        });

    Given(
        "^the PO \"([^\"]*)\" related to GRAccountingDocument \"([^\"]*)\" has posted CustomTrustee VendorInvoice with code \"([^\"]*)\"$",
        (String poCode, String grAccountingDocumentCode, String viCode) -> {
            actualCostTestUtils.assertThatPORelatedToGRAccountingDocument(poCode,grAccountingDocumentCode);
          actualCostTestUtils.assertThatPurchaseOrderDoeHasPostedCustomTrusteeIvoice(
              poCode, viCode);
        });

    Given(
        "^the PO \"([^\"]*)\" related to GRAccountingDocument \"([^\"]*)\" has no posted Goods VendorInvoice$",
        (String poCode, String grAccountingDocumentCode) -> {
            actualCostTestUtils.assertThatPORelatedToGRAccountingDocument(poCode,grAccountingDocumentCode);
            actualCostTestUtils.assertThatPurchaseOrderDoesNotHasPostedGoodsIvoice(poCode);
        });

    When(
        "^\"([^\"]*)\" requests to calculate Actual Cost for Goods Receipts with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String goodsReceiptCode, String date) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = actualCostTestUtils.calculateActualCost(goodsReceiptCode, userCookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup() throws Exception {
    Map<String, Object> properties = getProperties();
    actualCostTestUtils = new ActualCostTestUtils(properties);
    actualCostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    paymentRequestCreateJournalEntryTestUtils =
        new DObPaymentRequestCreateJournalEntryTestUtils(getProperties());
    paymentRequestCreateJournalEntryTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);

    databaseConnector.createConnection();
    databaseConnector.executeSQLScript(actualCostTestUtils.getDbScriptsOneTimeExecution());
  }

  @After
  public void afterEachScenario() throws SQLException {
    actualCostTestUtils.unfreeze();
    databaseConnector.closeConnection();
  }
}
