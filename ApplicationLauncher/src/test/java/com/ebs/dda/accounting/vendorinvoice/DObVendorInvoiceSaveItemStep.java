package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceSaveItemTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceItemValueObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceSaveItemStep extends SpringBootRunner implements En, InitializingBean {

    public static final String SCENARIO_NAME = "Save new Item in VendorInvoice";
    private DObVendorInvoiceSaveItemTestUtils invoiceSaveItemTestUtils;

  public DObVendorInvoiceSaveItemStep() {
    Given(
        "^the following VendorInvoice exists:$",
        (DataTable invoiceItemDataTable) -> {
          invoiceSaveItemTestUtils.assertThatInvoicesExist(invoiceItemDataTable);
        });
    Given(
        "^VendorInvoice with code \"([^\"]*)\" has the following InvoiceItems:$",
        (String vendorInvoiceCode, DataTable VendorInvoicesItems) -> {
          invoiceSaveItemTestUtils.assertVendorInvoiceItemExists(
              vendorInvoiceCode, VendorInvoicesItems);
        });

    Given(
        "^the following IVRs exit:$",
        (DataTable itemVendorRecordsList) -> {
          invoiceSaveItemTestUtils.assertThatItemVendorRecordsExists(itemVendorRecordsList);
        });

    When(
        "^\"([^\"]*)\" saves the following new item to the InvoiceItems of Vendor Invoice with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String invoiceCode, String saveTime, DataTable itemData) -> {
          invoiceSaveItemTestUtils.freeze(saveTime);
          saveItemToItemsInInvoice(userName, invoiceCode, itemData);
        });
    When(
        "^Item with code \"([^\"]*)\" has no IVR record$",
        (String itemCode) -> {
          invoiceSaveItemTestUtils.assertItemHasNoItemVendorReocrd(itemCode);
        });
      Then(
              "^InvoiceItems section of Vendor Invoice with Code \"([^\"]*)\" is updated as follows:$",
              (String invoiceCode, DataTable invoiceItemsDataTable) -> {
                  invoiceSaveItemTestUtils.assertThatInvoiceItemsUpdated(
                          invoiceCode, invoiceItemsDataTable);
              });
    Then(
        "^the following error message is attached to Item field and orderUnit field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          invoiceSaveItemTestUtils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObVendorInvoiceItemValueObject.ITEM_CODE);
          invoiceSaveItemTestUtils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObVendorInvoiceItemValueObject.ORDER_UNIT_CODE);
        });
      And("^the following TaxDetails exist in taxes section for VendorInvoice \"([^\"]*)\"$",
              (String vendorInvoiceCode, DataTable taxDetailsDataTable) -> {
                  invoiceSaveItemTestUtils.assertThatTheFollowingTaxDetailsExistForVendorInvoice(
                  vendorInvoiceCode, taxDetailsDataTable);
      });
      And("^InvoiceTaxes section of Vendor Invoice with Code \"([^\"]*)\" is updated as follows:$", 	(String vendorInvoiceCode, DataTable taxDetailsDataTable) -> {
          invoiceSaveItemTestUtils.assertThatTheFollowingTaxDetailsExistForVendorInvoice(
                  vendorInvoiceCode, taxDetailsDataTable);
      });
  }

  private void saveItemToItemsInInvoice(String userName, String invoiceCode, DataTable itemData) {
    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
    Response response =
        invoiceSaveItemTestUtils.saveitemToInvoiceSection(userCookie, invoiceCode, itemData);
    userActionsTestUtils.setUserResponse(userName, response);
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    invoiceSaveItemTestUtils = new DObVendorInvoiceSaveItemTestUtils(properties);
    invoiceSaveItemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {

      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(invoiceSaveItemTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(DObVendorInvoiceTestUtils.clearVendorInvoices());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {

      invoiceSaveItemTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSectionsNewStandard(
          IDObVendorInvoiceRestUrls.COMMAND_URL,
          IDObVendorInvoiceRestUrls.UNLOCK_ALL_SECTION,
          invoiceSaveItemTestUtils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
