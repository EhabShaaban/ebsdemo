package com.ebs.dda.accounting.settlement;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementViewDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementViewDetailsStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "View SettlementDetails";
  private DObSettlementViewDetailsTestUtils utils;

  public DObSettlementViewDetailsStep() {
    When(
        "^\"([^\"]*)\" requests to view SettlementDetails section of Settlement with code \"([^\"]*)\"$",
        (String username, String settlementCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          Response response = utils.sendGETRequest(cookie,
              String.format(IDObSettlementRestURLs.VIEW_DETAILS, settlementCode));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of SettlementDetails for Settlement with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String settlementCode, String username, DataTable detailsDT) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatViewSettlementDetailsResponseIsCorrect(response, detailsDT);
        });

  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSettlementViewDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewAll(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(clearSettlement());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
