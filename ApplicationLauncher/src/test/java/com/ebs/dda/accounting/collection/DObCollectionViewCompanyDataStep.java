package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewCompanyDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionViewCompanyDataStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private DObCollectionViewCompanyDataTestUtils utils;

    public DObCollectionViewCompanyDataStep() {
        Given(
                "^the following Company Data for Collections exist:$",
                (DataTable collectionCompanyData) -> {
                    utils.assertThatCollectionsCompanyDataExist(collectionCompanyData);
                });

        When(
                "^\"([^\"]*)\" requests to view Company section of Collection with code \"([^\"]*)\"$",
                (String username, String collectionCode) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(username);
                    Response response = utils.getCompanyDataForCollection(userCookie, collectionCode);
                    userActionsTestUtils.setUserResponse(username, response);
                });

        Then(
                "^the following values of Company section for Collection with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
                (String collectionCode, String username, DataTable collectionCompanyData) -> {
                    Response response = userActionsTestUtils.getUserResponse(username);
                    utils.assertResponseSuccessStatus(response);
                    utils.assertCorrectValuesAreDisplayedInCompanyData(response, collectionCompanyData);
                });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new DObCollectionViewCompanyDataTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {

        if (contains(scenario.getName(), "View Collection Company section -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getConciseDbScripts());
                hasBeenExecutedOnce = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View Collection Company section -")) {
            databaseConnector.closeConnection();
        }
    }
}
