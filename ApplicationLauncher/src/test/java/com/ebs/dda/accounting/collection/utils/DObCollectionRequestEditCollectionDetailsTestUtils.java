package com.ebs.dda.accounting.collection.utils;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObCollectionRequestEditCollectionDetailsTestUtils extends DObCollectionTestUtils {
  protected String COLLECTION_JMX_LOCK_BEAN_NAME = "CollectionEntityLockCommand";

  public DObCollectionRequestEditCollectionDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getUnlockCollectionDetailsUrl(String collectionCode) {
    return IDObCollectionRestURLs.COLLECTION_COMMAND
        + "/"
        + collectionCode
        + IDObCollectionRestURLs.UNLOCK_COLLECTION_DETAILS_SECTION;
  }

  public String getLockCollectionDetailsUrl(String collectionCode) {
    return IDObCollectionRestURLs.COLLECTION_COMMAND
        + "/"
        + collectionCode
        + IDObCollectionRestURLs.LOCK_COLLECTION_DETAILS_SECTION;
  }

  public void assertThatResourceIsLockedByUser(String userName, String prCode, String sectionName)
      throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=" + COLLECTION_JMX_LOCK_BEAN_NAME);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, prCode};
    String[] parameterTypes =
        new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
        (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);
    assertNotNull(lockReleaseDetails);
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, COLLECTION_JMX_LOCK_BEAN_NAME, sectionName);
  }
}
