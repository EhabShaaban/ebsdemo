package com.ebs.dda.accounting.accountingnote.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNoteGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class AccountingNoteViewAllTestUtil extends AccountingNoteCommonTestUtil {

  public AccountingNoteViewAllTestUtil(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertCreditDebitNoteDataIsCorrect(DataTable creditDebitNoteData, Response response)
      throws Exception {
    List<Map<String, String>> expectedCreditDebitNoteList =
        creditDebitNoteData.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualCreditDebitNoteList =
        response.body().jsonPath().getList(CommonKeys.DATA);
    for (int i = 0; i < expectedCreditDebitNoteList.size(); i++) {
      assertCorrectShownData(expectedCreditDebitNoteList.get(i), actualCreditDebitNoteList.get(i));
    }
  }

  private void assertCorrectShownData(
      Map<String, String> expectedCreditDebitNote, Map<String, Object> actualCreditDebitNote)
      throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedCreditDebitNote, actualCreditDebitNote);

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObAccountingNoteGeneralModel.USER_CODE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObAccountingNoteGeneralModel.OBJECT_TYPE_CODE);

    mapAssertion.assertLocalizedTypeAndCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER,
        IDObAccountingNoteGeneralModel.BUSINESS_PARTNER_TYPE,
        IDObAccountingNoteGeneralModel.BUSINESS_PARTNER_CODE,
        IDObAccountingNoteGeneralModel.BUSINESS_PARTNER_NAME);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.DOCUMENT_OWNER, IDObAccountingNoteGeneralModel.DOCUMENT_OWNER_NAME);

    mapAssertion.assertLocalizedValueAndCodeValue(
        IFeatureFileCommonKeys.REFERENCE_DOCUMENT,
        IDObAccountingNoteGeneralModel.REFERENCE_DOCUMENT_TYPE,
        IDObAccountingNoteGeneralModel.REFERENCE_DOCUMENT_CODE);

    assertThatExpectedAmountValueEqualsActualValue(expectedCreditDebitNote, actualCreditDebitNote);

    assertThatExpectedCurrencyIsoEqualsActualValue(expectedCreditDebitNote, actualCreditDebitNote);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObAccountingNoteGeneralModel.PURCHASE_UNIT_CODE,
        IDObAccountingNoteGeneralModel.PURCHASE_UNIT_NAME);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObAccountingNoteGeneralModel.CURRENT_STATES);
  }

  private void assertThatExpectedAmountValueEqualsActualValue(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    String expectedAmountValue =
        expectedMap.get(IAccountingFeatureFileCommonKeys.AMOUNT).split(" - ")[0];
    Assert.assertEquals(
        ASSERTION_MSG + " Expected " + expectedAmountValue + " doesn't equal actual value",
        Double.valueOf(expectedAmountValue),
        Double.valueOf(actualMap.get(IDObAccountingNoteGeneralModel.AMOUNT).toString()));
  }

  private void assertThatExpectedCurrencyIsoEqualsActualValue(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    String expectedCurrencyIsoValue =
        expectedMap.get(IAccountingFeatureFileCommonKeys.AMOUNT).split(" - ")[1];
    Assert.assertEquals(
        ASSERTION_MSG + " Expected " + expectedCurrencyIsoValue + " doesn't equal actual value",
        expectedCurrencyIsoValue,
        actualMap.get(IDObAccountingNoteGeneralModel.CURRENCY_ISO));
  }
}
