package com.ebs.dda.accounting.accountingnote.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.accountingnote.IAccountingNotesRestUrls;
import com.ebs.dda.jpa.accounting.accountingnotes.IAccountingNoteCreateValueObject;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

public class AccountingNoteCreateTestUtils extends AccountingNoteCommonTestUtil {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Create]";

  public AccountingNoteCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/accountingnote/Accounting_Notes_Clear.sql");
    return str.toString();
  }

  public void assertThatLastCreatedCodeIsCorrect(String lastCreatedCode) {
    assertThatLastEntityCodeIs("DObAccountingNote", lastCreatedCode);
  }

  public Response createAccountingNote(DataTable accountingNoteDataTable, Cookie cookie) {
    JsonObject accountingNote = prepareAccountingNoteJsonObject(accountingNoteDataTable);
    String restURL = IAccountingNotesRestUrls.CREATE;
    return sendPostRequest(cookie, restURL, accountingNote);
  }

  private JsonObject prepareAccountingNoteJsonObject(DataTable accountingNoteDataTable) {
    Map<String, String> accountingNoteMap =
        accountingNoteDataTable.asMaps(String.class, String.class).get(0);
    JsonObject accountingNoteJsonObject = new JsonObject();

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.NOTE_TYPE,
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.NOTE_TYPE));

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.TYPE,
        accountingNoteMap.get(IFeatureFileCommonKeys.TYPE));

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.BUSINESS_PARTNER_CODE,
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER));

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.COMPANY_CODE,
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.COMPANY));

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.BUSINESS_UNIT_CODE,
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT));

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.AMOUNT,
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.AMOUNT));

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.CURRENCY_ISO,
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.CURRENCY));

    addValueToJsonObject(
        accountingNoteJsonObject,
        IAccountingNoteCreateValueObject.REFERENCE_DOCUMENT_CODE,
        accountingNoteMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT));

    try {
      accountingNoteJsonObject.addProperty(
          IAccountingNoteCreateValueObject.DOCUMENT_OWNER_ID,
          Long.valueOf(accountingNoteMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID)));
    } catch (NumberFormatException exception) {
      accountingNoteJsonObject.addProperty(
          IAccountingNoteCreateValueObject.DOCUMENT_OWNER_ID,
          accountingNoteMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID));
    }
    return accountingNoteJsonObject;
  }

  public void assertThatAccountingNoteIsCreatedSuccessfully(DataTable accountingNoteDataTable) {
    Map<String, String> accountingNoteData =
        accountingNoteDataTable.asMaps(String.class, String.class).get(0);
    Object[] accountingNote = (Object[]) retrieveAccountingNoteData(accountingNoteData);

    Assert.assertNotNull(accountingNote);
    assertDateEquals(
        accountingNoteData.get(IFeatureFileCommonKeys.CREATION_DATE),
        new DateTime(accountingNote[0]).withZone(DateTimeZone.UTC));

    assertDateEquals(
        accountingNoteData.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(accountingNote[1]).withZone(DateTimeZone.UTC));
  }

  private Object retrieveAccountingNoteData(Map<String, String> accountingNoteRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedAccountingNote", constructCreatedAccountingNoteQueryParams(accountingNoteRow));
  }

  private Object[] constructCreatedAccountingNoteQueryParams(
      Map<String, String> accountingNoteRow) {
    return new Object[] {
      accountingNoteRow.get(IFeatureFileCommonKeys.CODE),
      '%' + accountingNoteRow.get(IFeatureFileCommonKeys.STATE) + '%',
      accountingNoteRow.get(IFeatureFileCommonKeys.CREATED_BY),
      accountingNoteRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      accountingNoteRow.get(IOrderFeatureFileCommonKeys.TYPE),
      accountingNoteRow.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  public void assertThatDebitNoteCompanyIsCreatedSuccessfully(
      String debitNoteCode, DataTable debitNoteDataTable) {
    Map<String, String> debitNoteData =
        debitNoteDataTable.asMaps(String.class, String.class).get(0);
    Object debitNote = retrieveDebitNoteCompanyData(debitNoteCode, debitNoteData);
    Assert.assertNotNull(ASSERTION_MSG + " Company data doesn't exists", debitNote);
  }

  private Object retrieveDebitNoteCompanyData(
      String debitNoteCode, Map<String, String> debitNoteRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedDebitNoteCompanyData",
        constructCreatedDebitNoteCompanyDataQueryParams(debitNoteCode, debitNoteRow));
  }

  private Object[] constructCreatedDebitNoteCompanyDataQueryParams(
      String debitNoteCode, Map<String, String> debitNoteRow) {
    return new Object[] {
      debitNoteCode,
      debitNoteRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      debitNoteRow.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void assertThatDebitNoteDetailsIsCreatedSuccessfully(
      String debitNoteCode, DataTable debitNoteDataTable) {
    Map<String, String> debitNoteData =
        debitNoteDataTable.asMaps(String.class, String.class).get(0);
    Object debitNote = retrieveDebitNoteDetailsData(debitNoteCode, debitNoteData);
    Assert.assertNotNull(ASSERTION_MSG + " AccountingDetails doesn't exists", debitNote);
  }

  private Object retrieveDebitNoteDetailsData(
      String debitNoteCode, Map<String, String> debitNoteRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedDebitNoteDetailsData",
        constructCreatedDebitNoteDetailsDataQueryParams(debitNoteCode, debitNoteRow));
  }

  private Object[] constructCreatedDebitNoteDetailsDataQueryParams(
      String debitNoteCode, Map<String, String> debitNoteRow) {
    return new Object[] {
      debitNoteRow.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT),
      debitNoteCode,
      debitNoteRow.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      Double.parseDouble(debitNoteRow.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
      debitNoteRow.get(IAccountingFeatureFileCommonKeys.CURRENCY)
    };
  }
}
