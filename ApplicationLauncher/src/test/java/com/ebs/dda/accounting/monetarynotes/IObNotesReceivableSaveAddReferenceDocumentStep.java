package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.IObNotesReceivableSaveAddReferenceDocumentTestUtils;
import com.ebs.dda.accounting.monetarynotes.utils.IObNotesReceivableViewReferenceDocumentTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObNotesReceivableSaveAddReferenceDocumentStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "Save Add ReferenceDocument in NotesReceivable,";

  private IObNotesReceivableSaveAddReferenceDocumentTestUtils utils;
  private IObNotesReceivableViewReferenceDocumentTestUtils viewReferenceDocumentTestUtils;

  public IObNotesReceivableSaveAddReferenceDocumentStep() {

    When(
        "^\"([^\"]*)\" saves the following new ReferenceDocument to ReferenceDocuments section of NotesReceivable with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String notesReceivableCode,
            String dateTime,
            DataTable newReferenceDocumentDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveAddReferenceDocument(
                  cookie, notesReceivableCode, newReferenceDocumentDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^ReferenceDocuments section of NotesReceivable with Code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String notesReceivableCode, String userName, DataTable referenceDocumentsDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(
                  cookie,
                  viewReferenceDocumentTestUtils.getViewReferenceDocumentsUrl(notesReceivableCode));
          userActionsTestUtils.setUserResponse(userName, response);
          viewReferenceDocumentTestUtils.assertMonetaryNoteReferenceDocumentsIsCorrect(
              notesReceivableCode, response, referenceDocumentsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObNotesReceivableSaveAddReferenceDocumentTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    viewReferenceDocumentTestUtils =
        new IObNotesReceivableViewReferenceDocumentTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      utils.unfreeze();
    }
  }
}
