package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCommonTestUtils;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObMonetaryNotesHomeScreenAllowedActionsStep extends SpringBootRunner
    implements En, InitializingBean {

  private static boolean hasBeenExecutedOnce = false;
  private DObMonetaryNotesCommonTestUtils utils;

  public DObMonetaryNotesHomeScreenAllowedActionsStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of NotesReceivable home screen$",
        (String userName) -> {
          String url = IDObNotesReceivableRestURLs.AUTHORIZED_ACTIONS;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObMonetaryNotesCommonTestUtils(getProperties(),entityManagerDatabaseConnector);
    }

}
