package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceActivateValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class DObVendorInvoiceActivateTestUtils extends DObVendorInvoiceTestUtils {

  public DObVendorInvoiceActivateTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/invoiceActivationPreparation.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");

    return str.toString();
  }


  public Response postInvoice(
          Cookie userCookie, String invoiceCode, String date, DataTable activationDetailsDataTable) {
    Map<String, String> activationDetails =
            activationDetailsDataTable.asMaps(String.class, String.class).get(0);
    JsonObject invoiceData = new JsonObject();

    invoiceData.addProperty(
            IDObVendorInvoiceActivateValueObject.JOURNAL_DATE,
            activationDetails.get(IAccountingFeatureFileCommonKeys.DUE_DATE));
    return sendPUTRequest(
            userCookie,
            IDObVendorInvoiceRestUrls.COMMAND_URL
                    + invoiceCode
                    + IDObVendorInvoiceRestUrls.POST_INVOICE_URL,
            invoiceData);
  }


  public Response activateVendorInvoice(
          Cookie userCookie, String invoiceCode, String invoiceType, String date, DataTable activationDetailsDataTable) {
    Map<String, String> activationDetails =
            activationDetailsDataTable.asMaps(String.class, String.class).get(0);
    JsonObject invoiceData = constructActivateBody(date, activationDetails);
    return sendPUTRequest(
            userCookie,
            IDObVendorInvoiceRestUrls.COMMAND_URL
                    + invoiceCode
                    + IDObVendorInvoiceRestUrls.ACTIVATE_URL + "?type=" + invoiceType,
            invoiceData);
  }

  private JsonObject constructActivateBody(String date, Map<String, String> activationDetails) {
    JsonObject invoiceData = new JsonObject();

    invoiceData.addProperty(
            IDObVendorInvoiceActivateValueObject.JOURNAL_DATE,
            activationDetails.get(IAccountingFeatureFileCommonKeys.DUE_DATE));
    return invoiceData;
  }


  public void assertThatInvoicePostingDetailAreUpdated(
          String invoiceCode, DataTable expectedActivationDetailsDataTable) {
    Map<String, String> expectedActivationDetails =
            expectedActivationDetailsDataTable.asMaps(String.class, String.class).get(0);
    IObAccountingDocumentActivationDetailsGeneralModel activationDetails =
            (IObAccountingDocumentActivationDetailsGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getInvoicePostingDetails",
                            invoiceCode,
                            expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
                            Double.valueOf(
                                    expectedActivationDetails.get(
                                            IAccountingFeatureFileCommonKeys.CURRENCY_PRICE)));

    assertNotNull(activationDetails);
  }

  public void assertThatInvoicesExistWithDetails(DataTable invoicesDataTable) {
    List<Map<String, String>> invoices = invoicesDataTable.asMaps(String.class, String.class);

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
    for (Map<String, String> expectedInvoice : invoices) {

      String vendor = expectedInvoice.get(IAccountingFeatureFileCommonKeys.VENDOR);
      String vendorCode = vendor.split("-")[0];
      String vendorName = vendor.split("-")[1];
      String invoiceCode = expectedInvoice.get(IFeatureFileCommonKeys.CODE);
      IObVendorInvoiceDetailsGeneralModel actualInvoice =
              (IObVendorInvoiceDetailsGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getInvoiceDetailsForPosting",
                              invoiceCode,
                              vendorCode,
                              vendorName,
                              expectedInvoice.get(IAccountingFeatureFileCommonKeys.PAYMENT_TERMS),
                              expectedInvoice.get(IAccountingFeatureFileCommonKeys.PURCHASE_ORDER),
                              expectedInvoice.get(IAccountingFeatureFileCommonKeys.PURCHASE_UNIT));

      assertNotNull(actualInvoice, "Invoice Details does not exist " + expectedInvoice.toString());
      assertEquals(
              convertEmptyStringToNull(
                      expectedInvoice.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO)),
              actualInvoice.getCurrencyISO());
      assertEquals(
              convertEmptyStringToNull(
                      expectedInvoice.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER)),
              actualInvoice.getInvoiceNumber());
      assertDateEquals(
              convertEmptyStringToNull(
                      expectedInvoice.get(IAccountingFeatureFileCommonKeys.INVOICE_DATE)),
              actualInvoice.getInvoiceDate());
    }
  }

public void assertThatLandedCostDetailsIsUpdated(String landedCostCode, DataTable landedCostDetailsDataTable) {
    List<Map<String, String>> landedCostDetails = landedCostDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> details : landedCostDetails) {

      IObLandedCostDetailsGeneralModel actualLandedCostDetails =
              (IObLandedCostDetailsGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getLandedCostDetailsByCode",
                              constructGetLandedCostDetailsByCodeParams(landedCostCode, details));

      assertNotNull(actualLandedCostDetails);
    }
  }

  private Object[] constructGetLandedCostDetailsByCodeParams(String landedCostCode, Map<String, String> details) {
    return new Object[]{
            details.get(IAccountingFeatureFileCommonKeys.GOODS_INVOICE),
            landedCostCode,
            details.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            details.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            details.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
            details.get(IAccountingFeatureFileCommonKeys.DAMAGE_STOCK)
    };
  }

  public void assertThatNoLandedCostsExistForPurchaseOrders(String landedCostState, DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrdersListMap =
            purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersListMap) {
      String purchaseOrderCode = purchaseOrderMap.get(IFeatureFileCommonKeys.PURCHASE_ORDER);
      DObLandedCostGeneralModel landedCost = (DObLandedCostGeneralModel) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPurchaseOrderLandedCost",
              '%'+landedCostState+'%',
              purchaseOrderCode
      );
      assertNull(landedCost, "a LandedCost exits for PurchaseOrder with code " + purchaseOrderCode);
    }
  }
}
