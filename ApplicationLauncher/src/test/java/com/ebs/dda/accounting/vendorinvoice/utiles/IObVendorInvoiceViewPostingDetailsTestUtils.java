package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IIObVendorInvoicePostingDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class IObVendorInvoiceViewPostingDetailsTestUtils extends DObVendorInvoiceTestUtils {

    public IObVendorInvoiceViewPostingDetailsTestUtils(Map<String, Object> properties) {
        super(properties);
    }

    public String getDbScripts() {
        StringBuilder str = new StringBuilder();
        str.append("db-scripts/accounting/vendorinvoice/IObVendorInvoicePostingDetails.sql");
        str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry.sql");
        str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-viewall.sql");
        return str.toString();
    }

    public void assertThatVendorInvoiceHasThoseActivationDetails(
            String documentType, DataTable activationDetailsDataTable) {
        List<Map<String, String>> activationDetailsList =
                activationDetailsDataTable.asMaps(String.class, String.class);
        for (Map<String, String> activationDetails : activationDetailsList) {

            IObAccountingDocumentActivationDetailsGeneralModel accountingDocumentActivationDetails =
                    (IObAccountingDocumentActivationDetailsGeneralModel)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "getPostingDetailsByDocumentTypeAndPostingInfo",
                                    constructVendorInvoiceActivationDetailsArguments(activationDetails, documentType));

            assertNotNull(accountingDocumentActivationDetails);
        }
    }

    private Object[] constructVendorInvoiceActivationDetailsArguments(
            Map<String, String> vendorInvoice, String documentType) {
        return new Object[]{
                vendorInvoice.get(IFeatureFileCommonKeys.CODE),
                vendorInvoice.get(IFeatureFileCommonKeys.CREATED_BY),
                vendorInvoice.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
                vendorInvoice.get(IAccountingFeatureFileCommonKeys.EXCHANGE_RATE_CODE),
                vendorInvoice.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
                vendorInvoice.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
                documentType
        };
    }

    public void assertCorrectValuesAreDisplayedInActivationDetails(
            Response response, DataTable activationDetailsDataTable) throws Exception {
        Map<String, String> activationDetailsDataExpected =
                activationDetailsDataTable.asMaps(String.class, String.class).get(0);
        Map<String, Object> activationDetailsDataActual = getMapFromResponse(response);

        MapAssertion mapAssertion =
                new MapAssertion(activationDetailsDataExpected, activationDetailsDataActual);
        assertActualAndExpectedDataFromResponse(mapAssertion);
    }

    private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
        mapAssertion.assertValue(
                IFeatureFileCommonKeys.CODE, IIObVendorInvoicePostingDetailsGeneralModel.CODE);

        mapAssertion.assertDateTime(
                IFeatureFileCommonKeys.POSTING_DATE,
                IIObVendorInvoicePostingDetailsGeneralModel.ACTIVATION_DATE);

        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.POSTED_BY,
                IIObVendorInvoicePostingDetailsGeneralModel.MODIFIED_BY);
        ;

        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE,
                IIObVendorInvoicePostingDetailsGeneralModel.JOURNAL_ENTRY_CODE);

        mapAssertion.assertEquationValues(
                IAccountingFeatureFileCommonKeys.CURRENCY_PRICE,
                constructLeftHandSide(),
                constructRightHandSide(),
                "=");

        mapAssertion.assertValue(
                IAccountingFeatureFileCommonKeys.EXCHANGE_RATE_CODE,
                IIObVendorInvoicePostingDetailsGeneralModel.EXCHANGE_RATE_CODE);
    }

    public List<String> constructLeftHandSide() {
        List<String> leftHandSide = new ArrayList<>();
        leftHandSide.add(IIObVendorInvoicePostingDetailsGeneralModel.FIRST_VALUE);
        leftHandSide.add(IIObVendorInvoicePostingDetailsGeneralModel.FIRST_CURRENCY_ISO);
        return leftHandSide;
    }

    public List<String> constructRightHandSide() {
        List<String> rightHandSide = new ArrayList<>();
        rightHandSide.add(IIObVendorInvoicePostingDetailsGeneralModel.CURRENCY_PRICE);
        rightHandSide.add(IIObVendorInvoicePostingDetailsGeneralModel.SECOND_CURRENCY_ISO);
        return rightHandSide;
    }
}
