package com.ebs.dda.accounting.salesinvoice;

public interface ISalesFeatureFileCommonKeys {

  String SI_CODE = "SICode";
  String COLLECTION_RESPONSIBLE = "CollectionResponsible";
  String TYPE = "Type";
  String SALES_ORDER = "SalesOrder";
  String BUSINESS_UNIT = "BusinessUnit";
  String COMPANY = "Company";
  String CUSTOMER = "Customer";
  String PAYMENT_TERM = "PaymentTerm";
  String CURRENCY = "Currency";
  String DOWN_PAYMENT = "DownPayment";
  String ITEM = "Item";
  String ORDER_UNIT_CODE_NAME = "OrderUnit";
  String QUANTITY = "Qty";
  String RETURN_QUANTITY = "ReturnQuantity";
  String SALES_PRICE = "SalesPrice";
  String TOTAL_AMOUNT = "TotalAmount";

  String TOTAL_AMOUNT_BEFORE_TAXES = "TotalAmountBeforeTaxes";
  String TOTAL_TAXES = "TotalTaxes";
  String TOTAL_AMOUNT_AFTER_TAXES = "TotalAmountAfterTaxes";
  String REMAINING_AMOUNT = "RemainingAmount";
  String REMAINING = "Remaining";

  String ITEM_NAME = "ItemName";
  String ITEM_ID = "ItemId";
  String ORDER_UNIT_NAME = "OrderUnitName";
  String UOM = "UOM";
  String DUE_DATE = "DueDate";
  String TOTAL_BEFORE_TAX = "TotalBeforeTax";
  String TOTAL_AFTER_TAX = "TotalAfterTax";
  String SUB_LEDGER = "SubLedger";
  String SUB_ACCOUNT = "SubAccount";
  String GL_ACCOUNT = "GLAccount";
  String CREDIT = "Credit";
  String DEPIT = "Debit";
  String CURRENCY_PRICE = "CurrencyPrice";
  String EXCHANGE_RATE_CODE = "ExchangeRateCode";
  String BUSINESS_PARTNER_CURRENCY = "BusinessPartnerCurrency";
  String SALES_INVOICE_POSTING_DATE = "SIPostingDate";
  String SALES_INVOICE = "SalesInvoice";

}
