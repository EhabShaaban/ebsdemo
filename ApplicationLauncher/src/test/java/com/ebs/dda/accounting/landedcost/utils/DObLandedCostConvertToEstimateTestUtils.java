package com.ebs.dda.accounting.landedcost.utils;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.jpa.accounting.PurchasedItemsTotalFinalCost;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObLandedCostConvertToEstimateTestUtils extends CommonTestUtils {

  public DObLandedCostConvertToEstimateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/CObLandedCostType.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    return str.toString();
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();

    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptActivationDetailsScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptCompany.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append(",").append("db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
    str.append(",").append("db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxes.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyData.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxesViewAll.sql");
    str.append(",")
        .append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyDataViewAll.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    str.append(",")
        .append(
            "db-scripts/accounting/payment-request/iob-payment-request-payment-details-view-all.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
    str.append(",")
        .append(
            "db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-postingdetails.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");
    str.append(",")
        .append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetailsViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostFactorItem.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCostJournalEntry.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/DObLandedCostJournalEntry_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails_ViewAll.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/DObLandedCost_Costing_ConvertToEstimate.sql");
    return str.toString();
  }

  public Response convertToEstimateLandedCost(Cookie userCookie, String landedCostCode) {

    return sendPUTRequest(
        userCookie,
        IDObLandedCostRestUrls.COMMAND_URL
            + "/"
            + landedCostCode
            + IDObLandedCostRestUrls.CONVERT_TO_ESTIMATE_LANDED_COST,
        new JsonObject());
  }
}
