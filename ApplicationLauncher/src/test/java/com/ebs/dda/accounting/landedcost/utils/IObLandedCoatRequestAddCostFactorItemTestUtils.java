package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;

import java.util.Map;

public class IObLandedCoatRequestAddCostFactorItemTestUtils extends DObLandedCostTestUtils {

  public IObLandedCoatRequestAddCostFactorItemTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }


  public String getLockUrl(String landedCostCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObLandedCostRestUrls.COMMAND_URL);
    lockUrl.append("/");
    lockUrl.append(landedCostCode);
    lockUrl.append(IDObLandedCostRestUrls.LOCK_LANDED_COST_FACTOR_ITEMS_SECTION);
    return lockUrl.toString();
  }

  public String getUnLockUrl(String landedCostCode) {
    StringBuilder unLockUrl = new StringBuilder();
    unLockUrl.append(IDObLandedCostRestUrls.COMMAND_URL);
    unLockUrl.append("/");
    unLockUrl.append(landedCostCode);
    unLockUrl.append(IDObLandedCostRestUrls.UNLOCK_LANDED_COST_FACTOR_ITEMS_SECTION);
    return unLockUrl.toString();
  }

}
