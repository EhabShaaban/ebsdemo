package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.inventory.goodsissue.IDObGoodsIssueRestURLS;
import com.ebs.dda.inventory.goodsissue.utils.DObGoodsIssueCommonTestUtils;
import com.ebs.dda.jpa.accounting.landedcost.CObLandedCostType;
import com.ebs.dda.jpa.accounting.landedcost.ICObLandedCost;
import com.ebs.dda.jpa.inventory.goodsissue.CObGoodsIssue;
import com.ebs.dda.jpa.inventory.goodsissue.ICObGoodsIssue;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CObLandedCostTypesDropDownTestUtils extends DObGoodsIssueCommonTestUtils {

    private ObjectMapper objectMapper;

    public CObLandedCostTypesDropDownTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        setProperties(properties);
        objectMapper = new ObjectMapper();
    }

    public String getDbScriptsOneTimeExecution() {
        StringBuilder str = new StringBuilder();
        str.append("db-scripts/clearDataSqlScript.sql");
        str.append("," + "db-scripts/accounting/landed-cost/CObLandedCostType.sql");

        return str.toString();
    }

    public String readAllLandedCostTypes() {
        return IDObLandedCostRestUrls.GET_URL + IDObLandedCostRestUrls.VIEW_LC_TYPES;
    }

    public void assertResponseMatchesExpected(Response response, DataTable landedCostTypesDataTable)
            throws Exception {
        List<Map<String, String>> landedCostTypesExpectedList =
                landedCostTypesDataTable.asMaps(String.class, String.class);
        List<HashMap<String, Object>> landedCostTypesActualList = getListOfMapsFromResponse(response);

        for (int i = 0; i < landedCostTypesActualList.size(); i++) {
            String landedCostTypesActualCodeName =
                    (String) landedCostTypesActualList.get(i).get(ICObLandedCost.CODE)
                           + " - "
                           + getEnglishValue(landedCostTypesActualList.get(i).get(ICObLandedCost.NAME));

            Map landedCostTypesActualMap = new HashMap<String, String>();
            landedCostTypesActualMap.put(IAccountingFeatureFileCommonKeys.LANDED_COST_TYPES, landedCostTypesActualCodeName);
            assertTrue(landedCostTypesExpectedList.contains(landedCostTypesActualMap));
        }
    }

    public void assertThatLandedCostTypesCountIsCorrect(Integer recordsNumber) {
        Long typesCount =
                (Long)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getLandedCostTypesCount");
        assertEquals(Long.valueOf(recordsNumber), typesCount);
    }

    public void assertThatLandedCostTypesExist(DataTable landedCoatTypes) {
        List<Map<String, String>> landedCoatTypesList =
                landedCoatTypes.asMaps(String.class, String.class);
        for (Map<String, String> landedCostType : landedCoatTypesList) {

            String landedCostTypeCode = landedCostType.get(IFeatureFileCommonKeys.CODE);
            String landedCostTypeName = landedCostType.get(IFeatureFileCommonKeys.NAME);

            CObLandedCostType cObLandedCostType =
                    (CObLandedCostType)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "landedCostTypeByCodeAndName", landedCostTypeCode, landedCostTypeName);

            assertNotNull(cObLandedCostType);
        }
    }
}
