package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceItemValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.purchases.IExpectedKeys;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObVendorInvoiceSaveItemTestUtils extends IObVendorInvoiceItemsTestUtils {

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    return str.toString();
  }
  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");

    return str.toString();
  }
  public DObVendorInvoiceSaveItemTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response saveitemToInvoiceSection(
      Cookie userCookie, String invoiceCode, DataTable itemData) {
    JsonObject invoiceItem = createInvoiceItemJsonObject(itemData);
    String restURL =
        IDObVendorInvoiceRestUrls.COMMAND_URL + invoiceCode + IDObVendorInvoiceRestUrls.UPDATE_ITEM_SECTION;
    return sendPostRequest(userCookie, restURL, invoiceItem);
  }

  private JsonObject createInvoiceItemJsonObject(DataTable itemDataTable) {
    List<Map<String, String>> itemsData = itemDataTable.asMaps(String.class, String.class);

    JsonObject invoiceItemObject = new JsonObject();
    for (Map<String, String> itemData : itemsData) {
      if (!itemData.get(IExpectedKeys.ITEM_CODE).equals("")) {
        invoiceItemObject.addProperty(
            IDObVendorInvoiceItemValueObject.ITEM_CODE, itemData.get(IExpectedKeys.ITEM_CODE));
      } else {
        invoiceItemObject.add(IDObVendorInvoiceItemValueObject.ITEM_CODE, JsonNull.INSTANCE);
      }
      invoiceItemObject.addProperty(
              IDObVendorInvoiceItemValueObject.ORDER_UNIT_CODE, itemData.get(IExpectedKeys.ORDER_UNIT).isEmpty() ? null : itemData.get(IExpectedKeys.ORDER_UNIT));
      try {
        invoiceItemObject.addProperty(IDObVendorInvoiceItemValueObject.QTY, Double.parseDouble(itemData.get(IExpectedKeys.QTY)));
      } catch (NumberFormatException ex) {
        invoiceItemObject.addProperty(IDObVendorInvoiceItemValueObject.QTY, itemData.get(IExpectedKeys.QTY));
      }

      try {
        invoiceItemObject.addProperty(IDObVendorInvoiceItemValueObject.UNIT_PRICE, Double.parseDouble(itemData.get(IExpectedKeys.UNIT_PRICE)));
      } catch (NumberFormatException ex) {
        invoiceItemObject.addProperty(
                IDObVendorInvoiceItemValueObject.UNIT_PRICE, itemData.get(IExpectedKeys.UNIT_PRICE));
      }
    }
    return invoiceItemObject;
  }

  public void assertThatInvoiceItemsUpdated(String invoiceCode, DataTable invoiceItemDataTable) {
    List<Map<String, String>> expectedInvoiceItems =
        invoiceItemDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedInvoiceItem : expectedInvoiceItems) {
      assertThatInvoiceItemExists(invoiceCode, expectedInvoiceItem);
    }
  }

  private void assertThatInvoiceItemExists(
      String invoiceCode, Map<String, String> expectedInvoice) {
    List<IObVendorInvoiceItemsGeneralModel> invoiceItemsGeneralModelList =
        (List<IObVendorInvoiceItemsGeneralModel>)
            entityManagerDatabaseConnector.executeNativeNamedQuery(
                "getVendorInvoicesItemsByCode",
                invoiceCode,
                expectedInvoice.get(IAccountingFeatureFileCommonKeys.ITEM_CODE));
    IObVendorInvoiceItemsGeneralModel itemQuantityInOrderUnit = invoiceItemsGeneralModelList
        .stream()
        .filter(model -> new BigDecimal(expectedInvoice.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER))
                .compareTo(model.getQuantityInOrderUnit()) == 0)
        .findAny()
        .orElse(null);
    IObVendorInvoiceItemsGeneralModel itemOrderUnitCode = invoiceItemsGeneralModelList.stream()
        .filter(model -> expectedInvoice.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT)
            .equals(model.getOrderUnitCode()))
        .findAny()
        .orElse(null);
    IObVendorInvoiceItemsGeneralModel itemPrice = invoiceItemsGeneralModelList.stream()
        .filter(model -> new BigDecimal(expectedInvoice.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE))
                .compareTo(model.getPrice()) == 0)
        .findAny()
        .orElse(null);
    IObVendorInvoiceItemsGeneralModel itemOrderPrice = invoiceItemsGeneralModelList.stream()
            .filter(model -> new BigDecimal(expectedInvoice.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT))
                    .compareTo(model.getOrderUnitPrice()) == 0)
            .findAny()
            .orElse(null);
    assertNotNull(itemQuantityInOrderUnit);
    assertNotNull(itemOrderUnitCode);
    assertNotNull(itemPrice);
    assertNotNull(itemOrderPrice);
  }

  public void assertVendorInvoiceItemExists(
      String vendorInvoiceCode, DataTable vendorInvoiceItemsDataTable) {
    Map<String, String> vendorInvoiceItem =
        vendorInvoiceItemsDataTable.asMaps(String.class, String.class).get(0);
    IObVendorInvoiceItemsGeneralModel invoiceItemsGeneralModel =
        (IObVendorInvoiceItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorInvoicesItems",
                vendorInvoiceCode,
                vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_CODE),
                "%" + vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_NAME) + "%",
                getDouble(
                    vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER)),
                vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT),
                "%"
                    + vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_ORDER_UNIT_Symbol)
                    + "%",
                vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_BASE_UNIT),
                "%"
                    + vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_BASE_UNIT_Symbol)
                    + "%",
                getDouble(vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)),
                getDouble(vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_ORDER_UNIT))
                    );
    assertNotNull("Vendor Invoice Item with code "+ vendorInvoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_CODE) + " does not exist",invoiceItemsGeneralModel);
  }

  public void assertThatItemVendorRecordsExists(DataTable itemVendorRecordsList) {
    List<Map<String, String>> itemVendorRecords =
        itemVendorRecordsList.asMaps(String.class, String.class);
    for (Map<String, String> itemVendorRecord : itemVendorRecords) {
      ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
          (ItemVendorRecordGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getItemVendorRecord", constructItemVendorRecordQueryParams(itemVendorRecord));

      Assert.assertNotNull(itemVendorRecordGeneralModel);
    }
  }

  private Object[] constructItemVendorRecordQueryParams(Map<String, String> itemVendorRecord) {
    return new Object[] {
      itemVendorRecord.get(com.ebs.dda.masterdata.ivr.IExpectedKeys.IVR_CODE_KEY),
      itemVendorRecord.get(
          com.ebs.dda.masterdata.ivr.IExpectedKeys.ITEM_CODE_KEY),
      itemVendorRecord.get(
          com.ebs.dda.masterdata.ivr.IExpectedKeys.VENDOR_CODE_KEY),
      itemVendorRecord.get(com.ebs.dda.masterdata.ivr.IExpectedKeys.UOM_CODE_KEY),
      itemVendorRecord.get(
          com.ebs.dda.masterdata.ivr.IExpectedKeys.ITEM_CODE_AT_VENDOR_KEY),
      Double.parseDouble(
          itemVendorRecord.get(
              com.ebs.dda.masterdata.ivr.IExpectedKeys.PRICE_KEY)),
      itemVendorRecord.get(
          com.ebs.dda.masterdata.ivr.IExpectedKeys.CURRENCY_CODE_KEY),
      itemVendorRecord.get(
          com.ebs.dda.masterdata.ivr.IExpectedKeys.PURCHASE_UNIT_CODE_KEY),
      itemVendorRecord.get(com.ebs.dda.masterdata.ivr.IExpectedKeys.CREATION_DATE)
    };
  }

  public void assertItemHasNoItemVendorReocrd(String itemCode) {
    List itemVendorRecord =
            entityManagerDatabaseConnector.executeNativeNamedQuery(
                    "getIVRByItemCode", itemCode);
    Assert.assertTrue(itemVendorRecord.isEmpty());
  }

    public void assertThatTheFollowingTaxDetailsExistForVendorInvoice(
            String vendorInvoiceCode, DataTable taxDetailsDataTable) {
      List<Map<String, String>> taxDetailsMaps =
              taxDetailsDataTable.asMaps(String.class, String.class);
      for (Map<String, String> taxDetailsMap : taxDetailsMaps) {

        Object[] queryParams =
                constructCompanyTaxesForVendorInvoiceQueryParams(vendorInvoiceCode, taxDetailsMap);

        IObVendorInvoiceTaxesGeneralModel vendorInvoiceTaxesGeneralModel =
                (IObVendorInvoiceTaxesGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getTaxesDetailsForCompanyInVendorInvoice", queryParams);
        Assert.assertNotNull("Tax detail doesn't exist "+ taxDetailsMap.toString(), vendorInvoiceTaxesGeneralModel);
      }
    }

  public Object[] constructCompanyTaxesForVendorInvoiceQueryParams(
          String salesInvoiceCode, Map<String, String> companyTaxesValues) {
    return new Object[] {
            salesInvoiceCode,
            companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX),
            Double.valueOf(companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)),
            Double.valueOf(companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT))
    };
  }

}
