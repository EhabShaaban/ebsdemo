package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceCreateJournalEntryTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewDetailsTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewGeneralDataTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceItemsViewTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCommonTestUtils;
import com.ebs.dda.masterdata.item.utils.MObItemCommonTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class DObVendorInvoiceCreateJournalEntryStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObVendorInvoiceCreateJournalEntryTestUtils invoiceCreateJournalEntryTestUtils;
  private DObVendorInvoiceViewGeneralDataTestUtils invoiceGeneralDataViewTestUtils;
  private DObVendorInvoiceViewDetailsTestUtils invoiceViewDetailsTestUtils;
  private IObVendorInvoiceItemsViewTestUtils invoiceItemsViewTestUtils;
  private CObExchangeRateCommonTestUtils exchangeRateCommonTestUtils;
  private MObItemCommonTestUtils itemCommonTestUtils;
  private MObVendorCommonTestUtils vendorCommonTestUtils;
  private HObCommonChartOfAccountTestUtils chartOfAccountTestUtils;
  private boolean hasBeenExecutedOnce = false;

  public DObVendorInvoiceCreateJournalEntryStep() {

    Given(
        "^VendorInvoice with code \"([^\"]*)\" has the following Summary:$",
        (String vendorInvoiceCode, DataTable totalAmountDataTable) -> {
          invoiceItemsViewTestUtils.assertThatInvoiceTotalAmountEquals(
              vendorInvoiceCode, totalAmountDataTable);
        });

    Given(
        "^the following AccountingInfo for Items exist:$",
        (DataTable accountingInfoDataTable) -> {
          itemCommonTestUtils.assertThatItemHasAccountInfo(accountingInfoDataTable);
        });

    Given(
        "^the following AccountingInfo for Vendors exist:$",
        (DataTable accountingInfoDataTable) -> {
          vendorCommonTestUtils.assertThatVendorHasAccountInfo(accountingInfoDataTable);
        });

    Given(
        "^the following GLAccounts has the following Subledgers:$",
        (DataTable accountsSubledgerDataTable) -> {
          chartOfAccountTestUtils.assertThatAccountsHasSubledger(accountsSubledgerDataTable);
        });

    Given(
        "^the following ServicePurchaseOrder exist:$",
        (DataTable spoDT) -> {
          invoiceCreateJournalEntryTestUtils.assertThatServicePOsExist(spoDT);
        });

    Given(
        "^the ServicePurchaseOrder has the following ItemsData:$",
        (DataTable itemsDT) -> {
          invoiceCreateJournalEntryTestUtils.assertThatSPOItemsExist(itemsDT);
        });

    Then(
        "^a new JournalEntry is created as follows:$",
        (DataTable journalEntryDataTable) -> {
          invoiceCreateJournalEntryTestUtils.assertThatJournalEntryIsCreated(journalEntryDataTable);
        });

    Then(
        "^the following new JournalEntryDetails are created for JournalEntry with code \"([^\"]*)\":$",
        (String journalEntryCode, DataTable journalEntryItemsDataTable) -> {
          invoiceCreateJournalEntryTestUtils.assertThatJournalEntryItemsIsCreated(
              journalEntryCode, journalEntryItemsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    invoiceCreateJournalEntryTestUtils =
        new DObVendorInvoiceCreateJournalEntryTestUtils(properties);
    itemCommonTestUtils = new MObItemCommonTestUtils(properties);
    vendorCommonTestUtils = new MObVendorCommonTestUtils(properties);
    invoiceGeneralDataViewTestUtils = new DObVendorInvoiceViewGeneralDataTestUtils(properties);
    invoiceViewDetailsTestUtils = new DObVendorInvoiceViewDetailsTestUtils(properties);
    invoiceItemsViewTestUtils = new IObVendorInvoiceItemsViewTestUtils(properties);
    exchangeRateCommonTestUtils = new CObExchangeRateCommonTestUtils(properties);

    invoiceCreateJournalEntryTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    itemCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    invoiceGeneralDataViewTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    invoiceViewDetailsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    invoiceItemsViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    exchangeRateCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    chartOfAccountTestUtils = new HObCommonChartOfAccountTestUtils(properties);
    chartOfAccountTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Journal Entry After Posting Invoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            invoiceCreateJournalEntryTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(invoiceCreateJournalEntryTestUtils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Journal Entry After Posting Invoice")) {
      invoiceCreateJournalEntryTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
