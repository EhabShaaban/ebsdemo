package com.ebs.dda.accounting.collection.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class) @CucumberOptions(features = {

  "classpath:features/accounting/collection/C_AllowedActions_HomeScreen.feature",
  "classpath:features/accounting/collection/C_AllowedActions_ObjectScreen.feature",
  "classpath:features/accounting/collection/C_Create_HP.feature",
  "classpath:features/accounting/collection/C_Create_Val.feature",
  "classpath:features/accounting/collection/C_Create_Auth.feature",
  "classpath:features/accounting/collection/C_Activate_SI_HP.feature",
  "classpath:features/accounting/collection/C_Activate_SO_HP.feature",
  "classpath:features/accounting/collection/C_Activate_NR_HP.feature",
  "classpath:features/accounting/collection/C_Activate_DN_HP.feature",
  "classpath:features/accounting/collection/C_Activate_SI_Val.feature",
  "classpath:features/accounting/collection/C_Activate_SO_Val.feature",
  "classpath:features/accounting/collection/C_BankAccountDropdown.feature",
  "classpath:features/accounting/collection/C_ViewAll.feature",
  "classpath:features/accounting/collection/C_DocumentOwner_Dropdown.feature",
  "classpath:features/accounting/collection/C_NotesReceivable_Dropdown.feature",
  "classpath:features/accounting/collection/C_Types_Dropdown.feature",
  "classpath:features/accounting/collection/C_SalesOrder_Dropdown.feature",
  "classpath:features/accounting/collection/C_View_GeneralData.feature",
  "classpath:features/accounting/collection/C_Delete.feature",
  "classpath:features/accounting/collection/C_View_CompanyData.feature",
  "classpath:features/accounting/collection/C_View_AccountingDetails.feature",
  "classpath:features/accounting/collection/C_View_CollectionDetails.feature",
  "classpath:features/accounting/collection/C_View_ActivationDetails.feature",
  "classpath:features/accounting/collection/C_RequestEdit_CollectionDetails.feature",
  "classpath:features/accounting/collection/C_Save_CollectionDetails_HP.feature",
  "classpath:features/accounting/collection/C_Save_CollectionDetails_Val.feature",
  "classpath:features/accounting/collection/C_Save_CollectionDetails_Auth.feature",
  "classpath:features/accounting/collection/C_Activate_Auth.feature",
  "classpath:features/accounting/collection/C_SalesInvoice_Dropdown.feature"

}, glue = {"com.ebs.dda.generaldefinitions",
  "com.ebs.dda.accounting.collection"}, strict = true, tags = "not @Future")
public class DObCollectionCucumberRunner {
}
