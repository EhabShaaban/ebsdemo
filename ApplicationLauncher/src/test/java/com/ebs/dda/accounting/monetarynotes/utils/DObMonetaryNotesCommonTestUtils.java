package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.monetarynotes.IDObMonetaryNotesSectionNames;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableReferenceDocumentsTypeEnum;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertNull;

public class DObMonetaryNotesCommonTestUtils extends AccountingDocumentCommonTestUtils {

  private static String NOTES_RECEIVABLES = "Notes receivables";
  private static String CUSTOMERS = "Customers";
  protected final String ASSERTION_MSG = "[MonetaryNote]";
  public String NOTES_RECEIVABLE_JMX_LOCK_BEAN_NAME = "NotesReceivableEntityLockCommand";

  public DObMonetaryNotesCommonTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getClearMonetaryNotesDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/accounting/monetarynote/SalesInvoice_Clear.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");

    return str.toString();
  }

  public void assertNotesReceivablesExist(DataTable notesReceivablesTable) {
    List<Map<String, String>> notesReceivables =
        notesReceivablesTable.asMaps(String.class, String.class);

    for (Map<String, String> notesReceivable : notesReceivables) {
      assertNotesReceivableExists(notesReceivable);
    }
  }

  private void assertNotesReceivableExists(Map<String, String> notesReceivable) {

    String notesReceivableCode = notesReceivable.get(IAccountingFeatureFileCommonKeys.NR_CODE);
    String state = notesReceivable.get(IFeatureFileCommonKeys.STATE);
    String purchaseUnit = notesReceivable.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    Object[] queryParameters =
        getnotesReceivableQueryParameters(notesReceivableCode, state, purchaseUnit);
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        getDObNotesReceivable(queryParameters);
    Assert.assertNotNull(notesReceivablesGeneralModel);
  }

  private DObNotesReceivablesGeneralModel getDObNotesReceivable(Object[] queryParameters) {
    return (DObNotesReceivablesGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getNotesReceivableByCodeStatePurchaseUnit", queryParameters);
  }

  private Object[] getnotesReceivableQueryParameters(
      String notesReceivableCode, String state, String purchaseUnit) {
    return new Object[] {notesReceivableCode, '%' + state + '%', purchaseUnit};
  }

  public void assertNotesReceivablesWithCompanyAndCurrencyAndCustomerAndRemainingAmountExist(
      DataTable notesReceivablesData) {
    List<Map<String, String>> notesReceivableMaps =
        notesReceivablesData.asMaps(String.class, String.class);

    for (Map<String, String> notesReceivableMap : notesReceivableMaps) {
      Object[] queryParams = constructSalesInvoiceDataQueryParams(notesReceivableMap);

      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
          (DObNotesReceivablesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getNotesReceivableByCodeAndCompanyAndCurrencyAndCustomerAndRemainingAmount",
                  queryParams);
      Assertions.assertNotNull(notesReceivablesGeneralModel);
    }
  }

  private Object[] constructSalesInvoiceDataQueryParams(Map<String, String> salesInvoiceMap) {
    return new Object[] {
      salesInvoiceMap.get(IAccountingFeatureFileCommonKeys.NR_CODE),
      salesInvoiceMap.get(IAccountingFeatureFileCommonKeys.COMPANY),
      salesInvoiceMap.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      salesInvoiceMap.get(IAccountingFeatureFileCommonKeys.CUSTOMER),
      Double.parseDouble(salesInvoiceMap.get(IAccountingFeatureFileCommonKeys.REMAINING_AMOUNT))
    };
  }

  public void assertThatNotesReceivableMasterObjectExists(DataTable notesReceivableDataTable) {
    List<Map<String, String>> notesReceivableData =
        notesReceivableDataTable.asMaps(String.class, String.class);
    for (Map<String, String> notesReceivableMap : notesReceivableData) {
      assertThatNotesReceivableExists(notesReceivableMap);
    }
  }

  private void assertThatNotesReceivableExists(Map<String, String> notesReceivableMap) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        (DObNotesReceivablesGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getDObNotesReceivablesByCodeAndStateAndBusinessUnit",
                constructNotesReceivableMasterObjectQueryParams(notesReceivableMap));
    Assertions.assertNotNull(notesReceivablesGeneralModel);
  }

  private Object[] constructNotesReceivableMasterObjectQueryParams(
      Map<String, String> notesReceivableMap) {
    return new Object[] {
      notesReceivableMap.get(IFeatureFileCommonKeys.CODE),
      "%" + notesReceivableMap.get(IFeatureFileCommonKeys.STATE) + "%",
      notesReceivableMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void createMonetaryNoteGeneralData(DataTable monetaryNoteTable) {
    List<Map<String, String>> monetaryNoteMaps =
        monetaryNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> monetaryNoteMap : monetaryNoteMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createMonetaryNoteGeneralData", getGeneralDataCreateParameters(monetaryNoteMap));
    }
  }

  private Object[] getGeneralDataCreateParameters(Map<String, String> monetaryNoteMap) {
    return new Object[] {
      monetaryNoteMap.get(IFeatureFileCommonKeys.CODE),
      monetaryNoteMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      monetaryNoteMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      monetaryNoteMap.get(IFeatureFileCommonKeys.CREATED_BY),
      monetaryNoteMap.get(IFeatureFileCommonKeys.CREATED_BY),
      monetaryNoteMap.get(IFeatureFileCommonKeys.STATE),
      monetaryNoteMap.get(IFeatureFileCommonKeys.TYPE),
      monetaryNoteMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  public void createMonetaryNoteCompanyData(DataTable monetaryNoteTable) {
    List<Map<String, String>> monetaryNoteMaps =
        monetaryNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> monetaryNoteMap : monetaryNoteMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createMonetaryNoteCompanyData", getCompanyDataCreateParameters(monetaryNoteMap));
    }
  }

  private Object[] getCompanyDataCreateParameters(Map<String, String> monetaryNoteMap) {
    return new Object[] {
      monetaryNoteMap.get(IFeatureFileCommonKeys.CODE),
      monetaryNoteMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      monetaryNoteMap.get(IFeatureFileCommonKeys.COMPANY),
      monetaryNoteMap.get(IFeatureFileCommonKeys.CODE)
    };
  }

  private void createIObNotesReceivableAccountingDetails(Map<String, String> monetaryNoteMap) {
    if (isNotesReceivable(monetaryNoteMap)) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createIObNotesReceivableAccountingDetails",
          getMonetaryNoteAccountingDetailsCreateParameters(monetaryNoteMap));
    }
  }

  private boolean isNotesReceivable(Map<String, String> monetaryNoteMap) {
    return monetaryNoteMap
        .get(IAccountingFeatureFileCommonKeys.ACCOUNT)
        .split(" - ")[1]
        .equals(NOTES_RECEIVABLES);
  }

  private void createIObCustomerAccountingDetails(Map<String, String> monetaryNoteMap) {
    if (isCustomer(monetaryNoteMap)) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createIObCustomerAccountingDetails",
          getMonetaryNoteAccountingDetailsCreateParameters(monetaryNoteMap));
    }
  }

  private boolean isCustomer(Map<String, String> monetaryNoteMap) {
    return monetaryNoteMap
        .get(IAccountingFeatureFileCommonKeys.ACCOUNT)
        .split(" - ")[1]
        .equals(CUSTOMERS);
  }

  public void createMonetaryNoteAccountingDetails(DataTable monetaryNoteTable) {
    List<Map<String, String>> monetaryNoteMaps =
        monetaryNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> monetaryNoteMap : monetaryNoteMaps) {
      createIObNotesReceivableAccountingDetails(monetaryNoteMap);
      createIObCustomerAccountingDetails(monetaryNoteMap);
    }
  }

  private Object[] getMonetaryNoteAccountingDetailsCreateParameters(
      Map<String, String> monetaryNoteMap) {
    return new Object[] {
      monetaryNoteMap.get(IFeatureFileCommonKeys.CODE),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
      Double.parseDouble(monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.VALUE)),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT),
      monetaryNoteMap.get(IFeatureFileCommonKeys.CODE)
    };
  }

  public void createMonetaryNoteDetails(DataTable monetaryNoteTable) {
    List<Map<String, String>> monetaryNoteMaps =
        monetaryNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> monetaryNoteMap : monetaryNoteMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createMonetaryNoteDetails", getMonetaryNoteDetailsCreateParameters(monetaryNoteMap));
    }
  }

  private Object[] getMonetaryNoteDetailsCreateParameters(Map<String, String> monetaryNoteMap) {
    return new Object[] {
      monetaryNoteMap.get(IFeatureFileCommonKeys.CODE),
      new BigDecimal(monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.NOTE_NUMBER),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.NOTE_BANK),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.DEPOT),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      new BigDecimal(monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.REMAINING)),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.NOTE_FORM)
    };
  }

  public void assertThatMonetaryNotesDoesNotHaveAccountingDetails(DataTable monetaryNoteTable) {
    List<Map<String, String>> monetaryNoteMaps =
        monetaryNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> monetaryNoteMap : monetaryNoteMaps) {
      Object accountingDetails =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getMonetaryNoteAccountingDetailsByCode",
              monetaryNoteMap.get(IFeatureFileCommonKeys.CODE));
      Assert.assertNull(
          "MonetaryNote has accounting detail " + monetaryNoteMap.toString(), accountingDetails);
    }
  }

  public void assertThatMonetaryNotesMatch(DataTable dataTable, Response response)
      throws Exception {
    List<HashMap<String, Object>> actualMonetaryNotes = getListOfMapsFromResponse(response);

    List<Map<String, String>> expectedMonetaryNotes = dataTable.asMaps(String.class, String.class);

    for (int i = 0; i < actualMonetaryNotes.size(); i++) {
      assertThatMonetaryNotesMatchesTheExpected(
          expectedMonetaryNotes.get(i), actualMonetaryNotes.get(i));
    }
  }

  private void assertThatMonetaryNotesMatchesTheExpected(
      Map<String, String> expectedMonetaryNotesMap, HashMap<String, Object> actualMonetaryNotesMap)
      throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedMonetaryNotesMap, actualMonetaryNotesMap);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObNotesReceivablesGeneralModel.USER_CODE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObNotesReceivablesGeneralModel.OBJECT_TYPE_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER,
        IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER_CODE,
        IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER_NAME);

    assertThatExpectedDepotEqualsActualValueIfNotesReceivable(
        expectedMonetaryNotesMap.get(IFeatureFileCommonKeys.TYPE), mapAssertion);

    mapAssertion.assertDateTime(
        IAccountingFeatureFileCommonKeys.DUE_DATE, IDObNotesReceivablesGeneralModel.DUE_DATE);

    assertThatExpectedAmountValueEqualsActualValue(
        expectedMonetaryNotesMap, actualMonetaryNotesMap);

    assertThatExpectedRemainingValueEqualsActualValue(
        expectedMonetaryNotesMap, actualMonetaryNotesMap);

    assertThatExpectedCurrencyIsoEqualsActualValue(
        expectedMonetaryNotesMap, actualMonetaryNotesMap);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_CODE,
        IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_NAME_ATTR);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObNotesReceivablesGeneralModel.CURRENT_STATES);
  }

  private void assertThatExpectedDepotEqualsActualValueIfNotesReceivable(
      String expectedTypeKey, MapAssertion mapAssertion) throws Exception {
    if (isNotesReceivable(expectedTypeKey)) {
      mapAssertion.assertCodeAndLocalizedNameValue(
          IAccountingFeatureFileCommonKeys.DEPOT,
          IDObNotesReceivablesGeneralModel.DEPOT_CODE,
          IDObNotesReceivablesGeneralModel.DEPOT_NAME);
    }
  }

  private boolean isNotesReceivable(String expectedTypeKey) {
    return expectedTypeKey.contains(IDObNotesReceivablesGeneralModel.NOTES_RECEIVABLE);
  }

  private void assertThatExpectedAmountValueEqualsActualValue(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {

    BigDecimal expectedAmountValue =
        new BigDecimal(expectedMap.get(IAccountingFeatureFileCommonKeys.AMOUNT).split(" - ")[0]);

    BigDecimal actualAmountValue =
        new BigDecimal(actualMap.get(IDObNotesReceivablesGeneralModel.AMOUNT).toString());

    String message =
        ASSERTION_MSG
            + " Expected Amount"
            + expectedAmountValue
            + " doesn't equal actual value "
            + actualAmountValue;

    Assert.assertEquals(message, 0, expectedAmountValue.compareTo(actualAmountValue));
  }

  private void assertThatExpectedRemainingValueEqualsActualValue(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {

    BigDecimal expectedRemainingValue =
        new BigDecimal(expectedMap.get(IAccountingFeatureFileCommonKeys.REMAINING).split(" - ")[0]);

    BigDecimal actualRemainingValue =
        new BigDecimal(actualMap.get(IDObNotesReceivablesGeneralModel.REMAINING).toString());

    String message =
        ASSERTION_MSG
            + " Expected Remaining"
            + expectedRemainingValue
            + " doesn't equal actual value "
            + actualRemainingValue;

    Assert.assertEquals(message, 0, expectedRemainingValue.compareTo(actualRemainingValue));
  }

  private void assertThatExpectedCurrencyIsoEqualsActualValue(
      Map<String, String> expectedMap, Map<String, Object> actualMap) {
    String expectedCurrencyIsoValue =
        expectedMap.get(IAccountingFeatureFileCommonKeys.AMOUNT).split(" - ")[1];
    Assert.assertEquals(
        ASSERTION_MSG + " Expected " + expectedCurrencyIsoValue + " doesn't equal actual value",
        expectedCurrencyIsoValue,
        actualMap.get(IDObNotesReceivablesGeneralModel.CURRENCY_ISO));
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IDObMonetaryNotesSectionNames.NOTES_DETAILS_SECTION,
            IDObMonetaryNotesSectionNames.REFERENCE_DOCUMENTS_SECTION));
  }

  public void createMonetaryNoteReferenceDocuments(DataTable monetaryNoteTable) {
    List<Map<String, String>> monetaryNoteMaps =
        monetaryNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> monetaryNoteMap : monetaryNoteMaps) {
      createIObNotesReceivableReferenceDocumentSalesOrder(monetaryNoteMap);
      createIObNotesReceivableReferenceDocumentSalesInvoice(monetaryNoteMap);
      createIObNotesReceivableReferenceDocumentNotesReceivable(monetaryNoteMap);
    }
  }

  private Object[] getMonetaryNoteReferenceDocumentsCreateParameters(
      Map<String, String> monetaryNoteMap) {
    return new Object[] {
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.MONETARY_NOTE_REF_DOCUMENT_ID),
      monetaryNoteMap.get(IFeatureFileCommonKeys.CODE),
      monetaryNoteMap.get(IFeatureFileCommonKeys.TYPE),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.DOCUMENT_TYPE),
      new BigDecimal(monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.AMOUNT_TO_COLLECT)),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.MONETARY_NOTE_REF_DOCUMENT_ID),
      monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.DOCUMENT_CODE)
    };
  }

  private void createIObNotesReceivableReferenceDocumentSalesOrder(
      Map<String, String> monetaryNoteMap) {
    if (isSalesOrderReferenceDocument(monetaryNoteMap)) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createIObNotesReceivableReferenceDocumentSalesOrder",
          getMonetaryNoteReferenceDocumentsCreateParameters(monetaryNoteMap));
    }
  }

  private boolean isSalesOrderReferenceDocument(Map<String, String> monetaryNoteMap) {
    return monetaryNoteMap
        .get(IAccountingFeatureFileCommonKeys.DOCUMENT_TYPE)
        .equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_ORDER.name());
  }

  private void createIObNotesReceivableReferenceDocumentSalesInvoice(
      Map<String, String> monetaryNoteMap) {
    if (isSalesInvoiceReferenceDocument(monetaryNoteMap)) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createIObNotesReceivableReferenceDocumentSalesInvoice",
          getMonetaryNoteReferenceDocumentsCreateParameters(monetaryNoteMap));
    }
  }

  private boolean isSalesInvoiceReferenceDocument(Map<String, String> monetaryNoteMap) {
    return monetaryNoteMap
        .get(IAccountingFeatureFileCommonKeys.DOCUMENT_TYPE)
        .equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_INVOICE.name());
  }

  private void createIObNotesReceivableReferenceDocumentNotesReceivable(
      Map<String, String> monetaryNoteMap) {
    if (isNotesReceivableReferenceDocument(monetaryNoteMap)) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createIObNotesReceivableReferenceDocumentNotesReceivable",
          getMonetaryNoteReferenceDocumentsCreateParameters(monetaryNoteMap));
    }
  }

  private boolean isNotesReceivableReferenceDocument(Map<String, String> monetaryNoteMap) {
    return monetaryNoteMap
        .get(IAccountingFeatureFileCommonKeys.DOCUMENT_TYPE)
        .equals(NotesReceivableReferenceDocumentsTypeEnum.NOTES_RECEIVABLE.name());
  }

  public void assertThatMonetaryNoteHasNoReferenceDocuments(DataTable monetaryNoteTable) {
    List<Map<String, String>> MonetaryNoteList =
        monetaryNoteTable.asMaps(String.class, String.class);
    for (Map<String, String> MonetaryNoteRow : MonetaryNoteList) {
      String monetaryNoteCode = MonetaryNoteRow.get(IFeatureFileCommonKeys.CODE);
      Object referenceDocument =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObMonetaryNoteReferenceDocumentsByCode", monetaryNoteCode);
      Assert.assertNull(
          ASSERTION_MSG + " This MonetaryNote " + monetaryNoteCode + " has ReferenceDocuments. ",
          referenceDocument);
    }
  }

  protected String getFirstValue(Map<String, String> noteDetailsDataMap, String key) {
    return noteDetailsDataMap.get(key).split(" - ")[0].trim();
  }

  protected void assertThatActualDateEqualsExpectedAndIfEmptyAssertNull(
      String expectedDateStr, Object actualDateObject) {
    if (expectedDateStr.equals("")) {
      assertNull(actualDateObject, "The actual date must be null");
    } else {
      assertDateEquals(expectedDateStr, new DateTime(actualDateObject).withZone(DateTimeZone.UTC));
    }
  }

  public void createMonetaryNoteActivationDetails(DataTable activationDetails) {
    List<Map<String, String>> activationDetailsList = activationDetails.asMaps(String.class, String.class);
    for (Map<String, String> activationDetail : activationDetailsList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject("createIObNotesReceivableActivationDetails",
              getMonetaryNoteActivationDetailsCreateParameters(activationDetail));
    }
  }

  private Object[] getMonetaryNoteActivationDetailsCreateParameters(Map<String, String> monetaryNoteMap) {
    String[] currencyPrice = monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE).split(" = ");
    String[] leftHandSide = currencyPrice[0].split(" ");
    String[] rightHandSide = currencyPrice[1].split(" ");
    String companyCurrencyIso = leftHandSide[1];
    String value = rightHandSide[0];
    String documentCurrencyIso = rightHandSide[1];

    return new Object[] { monetaryNoteMap.get(IFeatureFileCommonKeys.CODE),
            monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
            monetaryNoteMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            monetaryNoteMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
            companyCurrencyIso, documentCurrencyIso, Double.parseDouble(value),
            Double.parseDouble(value),
            monetaryNoteMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR)
    };
  }
}
