package com.ebs.dda.accounting.settlement.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentActivationDetailsGeneralModel;

import cucumber.api.DataTable;
import io.restassured.response.Response;

public class DObSettlementViewActivationDetailsTestUtils extends DObSettlementsCommonTestUtils {

	public DObSettlementViewActivationDetailsTestUtils(Map<String, Object> properties) {
		super(properties);
		setProperties(properties);
	}

	public void assertThatViewSettlementActivationDetailsResponseIsCorrect(Response response,
					DataTable activationDetailsTable) {
		Map<String, String> expectedActivationDetailsMap = activationDetailsTable
						.asMaps(String.class, String.class).get(0);
		Map<String, Object> actualActivationDetailsMap = getMapFromResponse(response);

		MapAssertion mapAssertion = new MapAssertion(expectedActivationDetailsMap,
						actualActivationDetailsMap);

		assertActualAndExpectedDataFromResponse(mapAssertion);
	}

	private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) {
		mapAssertion.assertValue(IFeatureFileCommonKeys.CODE,
						IIObAccountingDocumentActivationDetailsGeneralModel.CODE);

		mapAssertion.assertDateTime(IFeatureFileCommonKeys.ACTIVATION_DATE,
						IIObAccountingDocumentActivationDetailsGeneralModel.ACTIVATION_DATE);

		mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE,
						IIObAccountingDocumentActivationDetailsGeneralModel.JOURNAL_ENTRY_CODE);

		mapAssertion.assertEquationValues(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE,
						constructLeftHandSide(), constructRightHandSide(), "=");

		mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.FISCAL_YEAR,
						IIObAccountingDocumentActivationDetailsGeneralModel.FISCAL_PERIOD);
	}

	public List<String> constructLeftHandSide() {
		List<String> leftHandSide = new ArrayList<>();
		leftHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.FIRST_VALUE);
		leftHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.FIRST_CURRENCY_ISO);
		return leftHandSide;
	}

	public List<String> constructRightHandSide() {
		List<String> rightHandSide = new ArrayList<>();
		rightHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.CURRENCY_PRICE);
		rightHandSide.add(IIObAccountingDocumentActivationDetailsGeneralModel.SECOND_CURRENCY_ISO);
		return rightHandSide;
	}

}
