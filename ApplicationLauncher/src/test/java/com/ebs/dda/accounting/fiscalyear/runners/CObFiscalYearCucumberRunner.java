package com.ebs.dda.accounting.fiscalyear.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/accounting/fiscalyear/FiscalYear_Dropdown.feature",
      "classpath:features/accounting/fiscalyear/Active_FiscalYears.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.fiscalyear"},
    strict = true,
    tags = "not @Future")
public class CObFiscalYearCucumberRunner {}
