package com.ebs.dda.accounting.actualcost;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class ActualCostTestUtils extends CommonTestUtils {

  public static final String GOODS = "Goods";
  public static final String CUSTOM_TRUSTEE = "CustomTrustee";

  public ActualCostTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(",")
        .append("db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(",")
        .append("db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/accounting/actual-cost/actual-cost-val.sql");
    str.append("," + "db-scripts/accounting/actual-cost/actual-cost-downpayment.sql");
    str.append(
        "," + "db-scripts/accounting/actual-cost/actual-cost-downpayment-and-installments.sql");
    return str.toString();
  }

  public void assertPurchaseOrdersExistWithAdvancedData(DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersMaps) {
      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
          "getPurchaseOrderByAdvancedData", constructPurchaseOrderParams(purchaseOrderMap));
    }
  }

  private Object[] constructPurchaseOrderParams(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      '%' + purchaseOrderMap.get(IFeatureFileCommonKeys.STATE) + '%',
      purchaseOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      purchaseOrderMap.get(IFeatureFileCommonKeys.TYPE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.COMPANY),
      purchaseOrderMap.get(IFeatureFileCommonKeys.VENDOR),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CURRENCY)
    };
  }

  public void assertGoodsReceiptPurchaseOrderExist(DataTable goodsReceiptsDataTable) {
    List<Map<String, String>> goodsReceiptMaps =
        goodsReceiptsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptMap : goodsReceiptMaps) {
      Object goodsReceiptPurchaseOrderData =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsReceiptPODataByState",
                  constructGoodsReceiptPurchaseOrderQueryParams(goodsReceiptMap));
      Assert.assertNotNull(goodsReceiptPurchaseOrderData);
    }
  }

  private Object[] constructGoodsReceiptPurchaseOrderQueryParams(
      Map<String, String> goodsReceiptPurchaseOrderData) {
    return new Object[] {
      goodsReceiptPurchaseOrderData.get(IFeatureFileCommonKeys.CODE),
      goodsReceiptPurchaseOrderData.get(IFeatureFileCommonKeys.PO_CODE),
      '%' + goodsReceiptPurchaseOrderData.get(IFeatureFileCommonKeys.STATE) + '%'
    };
  }

  public void assertThatGoodsVendorInvoicesExist(DataTable goodsVendorInvoicesTable) {
    List<Map<String, String>> invoicesList =
        goodsVendorInvoicesTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {

      Object invoiceGeneralModel =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGoodsVendorInvoiceByCodeAndStateAndPOAndCurrency",
                  getInvoiceParametersByStateAndVendorAndPurchaseUnitAndCurrency(invoice, GOODS));

      assertNotNull(invoiceGeneralModel);
    }
  }

  public void assertThatCustomTrusteeVendorInvoicesExist(
      DataTable customTrusteeVendorInvoicesTable) {
    List<Map<String, String>> invoicesList =
        customTrusteeVendorInvoicesTable.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {

      Object invoiceGeneralModel =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCustomTrusteeVendorInvoiceByCodeAndStateAndPOAndCurrency",
                  getInvoiceParametersByStateAndVendorAndPurchaseUnitAndCurrency(
                      invoice, CUSTOM_TRUSTEE));

      assertNotNull(invoiceGeneralModel);
    }
  }

  private Object[] getInvoiceParametersByStateAndVendorAndPurchaseUnitAndCurrency(
      Map<String, String> invoice, String invoiceType) {

    String invoiceCode = invoice.get(IFeatureFileCommonKeys.CODE);
    String businessUnit = invoice.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    String vendorCode = invoice.get(IFeatureFileCommonKeys.VENDOR);
    String purchaseOrder = invoice.get(IFeatureFileCommonKeys.PURCHASE_ORDER);
    String invoiceState = invoice.get(IFeatureFileCommonKeys.STATE);
    if (invoiceType.equals(GOODS)) {
      String currencyCode = invoice.get(IFeatureFileCommonKeys.CURRENCY);
      Double currencyPrice =
          Double.parseDouble(invoice.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE));
      Double totalAmount =
          Double.parseDouble(invoice.get(ISalesFeatureFileCommonKeys.TOTAL_AMOUNT));
      Double totalRemaining =
          Double.parseDouble(invoice.get(IFeatureFileCommonKeys.TOTAL_REMAINING));
      return new Object[] {
        invoiceCode,
        businessUnit,
        vendorCode,
        purchaseOrder,
        '%' + invoiceState + '%',
        currencyCode,
        currencyPrice,
        totalAmount,
        totalRemaining
      };
    } else {
      return new Object[] {
        invoiceCode, businessUnit, vendorCode, purchaseOrder, '%' + invoiceState + '%'
      };
    }
  }

  public void assertVendorInvoiceItemExistsForAcutalCost(
      String vendorInvoiceCode, DataTable vendorInvoicesItems) {
    List<Map<String, String>> itemsList = vendorInvoicesItems.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsList) {

      Object invoiceGeneralModel =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getVendorInvoiceItemsForActualCost",
              constructVendorInvoiceItemsForActualCostParams(vendorInvoiceCode, item));

      assertNotNull(invoiceGeneralModel);
    }
  }

  private Object[] constructVendorInvoiceItemsForActualCostParams(
      String vendorInvoiceCode, Map<String, String> item) {
    return new Object[] {
      vendorInvoiceCode,
      item.get(IFeatureFileCommonKeys.ITEM),
      item.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER),
      Double.parseDouble(item.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)),
      Double.parseDouble(item.get(IAccountingFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT))
    };
  }

  public void assertThatPaymentRequestExists(DataTable paymentRequestsDataTable) {
    List<Map<String, String>> paymentRequestsList =
        paymentRequestsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> paymentRequest : paymentRequestsList) {

      Object paymentRequestForActualCost =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPaymentRequestForActualCost",
              constructPaymentRequestForActualCostParams(paymentRequest));

      assertNotNull(paymentRequestForActualCost);
    }
  }

  private Object[] constructPaymentRequestForActualCostParams(Map<String, String> paymentRequest) {
    return new Object[] {
      paymentRequest.get(IFeatureFileCommonKeys.CODE),
      paymentRequest.get(IAccountingFeatureFileCommonKeys.PAYMENT_TYPE),
      paymentRequest.get(IAccountingFeatureFileCommonKeys.PAYMENT_FORM),
      paymentRequest.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT),
      '%' + paymentRequest.get(IFeatureFileCommonKeys.STATE) + '%',
      paymentRequest.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT),
      paymentRequest.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT_TYPE),
      Double.parseDouble(paymentRequest.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE)),
    };
  }

  public void assertThatLastExchangeRateIs(DataTable exchangeRateDataTable) {
    List<Map<String, String>> exchangeRateList =
        exchangeRateDataTable.asMaps(String.class, String.class);
    for (Map<String, String> exchangeRate : exchangeRateList) {

      Object lastExchangeRate =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getLastExchangeRate", constructLastExchangeRateParams(exchangeRate));

      assertNotNull(lastExchangeRate);
    }
  }

  private Object[] constructLastExchangeRateParams(Map<String, String> exchangeRate) {
    return new Object[] {
      exchangeRate.get(IFeatureFileCommonKeys.CODE),
      exchangeRate.get(IAccountingFeatureFileCommonKeys.FIRST_CURRENCY),
      exchangeRate.get(IAccountingFeatureFileCommonKeys.SECOND_CURRENCY),
      Double.parseDouble(exchangeRate.get(IAccountingFeatureFileCommonKeys.SECOND_VALUE)),
      exchangeRate.get(IAccountingFeatureFileCommonKeys.VALID_FROM),
    };
  }

  public void assertThatJournalEntriesItemsForActualCostExist(
      DataTable journalEntriesItemsDataTable) {
    List<Map<String, String>> journalEntriesItemsList =
        journalEntriesItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : journalEntriesItemsList) {

      Object journalEntriesItem =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getJournalEntriesItemsForActualCost", constructJournalEntriesItemsParams(item));

      assertNotNull(journalEntriesItem);
    }
  }

  private Object[] constructJournalEntriesItemsParams(Map<String, String> item) {
    return new Object[] {
      item.get(IFeatureFileCommonKeys.CODE),
      item.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT),
      item.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
      item.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT),
      item.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT),
      item.get(IAccountingFeatureFileCommonKeys.SUBLEDGER),
      Double.parseDouble(item.get(IAccountingFeatureFileCommonKeys.DEBIT)),
      Double.parseDouble(item.get(IAccountingFeatureFileCommonKeys.CREDIT)),
      item.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      Double.parseDouble(item.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE))
    };
  }

  public Response calculateActualCost(String goodsReciptsCode, Cookie cookie) {
    String restURL =
        IActualCostRestURLs.ACTUAL_COST_COMMAND
            + "/"
            + goodsReciptsCode
            + IActualCostRestURLs.CALCULATE_ACTUAL_COST;
    return sendPostRequest(cookie, restURL, new JsonObject());
  }

  public void assertThatGoodsReceiptAccountingDocumentExist(
      DataTable grAccountingDocumentDataTable) {
    List<Map<String, String>> grAccountingDocuments =
        grAccountingDocumentDataTable.asMaps(String.class, String.class);
    for (Map<String, String> grAccountingDocument : grAccountingDocuments) {

      Object actualGRAccountingDocument =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGRAccountingDocument",
                  constructGRAccountingDocumentParams(grAccountingDocument));

      assertNotNull(actualGRAccountingDocument);
    }
  }

  private Object[] constructGRAccountingDocumentParams(
      Map<String, String> actualGRAccountingDocument) {
    return new Object[] {
      actualGRAccountingDocument.get(IFeatureFileCommonKeys.CODE),
      '%' + actualGRAccountingDocument.get(IFeatureFileCommonKeys.STATE) + '%',
      actualGRAccountingDocument.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
    };
  }

  public void assertThatGoodsReceiptAccountingDocumentDetailsExist(
      DataTable grAccountingDocumentDetailsDataTable) {
    List<Map<String, String>> grAccountingDocuments =
        grAccountingDocumentDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> grAccountingDocument : grAccountingDocuments) {

      Object actualGRAccountingDocument =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGRAccountingDocumentDetails",
                  constructGRAccountingDocumentDetailsParams(grAccountingDocument));

      assertNotNull(actualGRAccountingDocument);
    }
  }

  private Object[] constructGRAccountingDocumentDetailsParams(
      Map<String, String> actualGRAccountingDocumentDetails) {
    return new Object[] {
      actualGRAccountingDocumentDetails.get(IFeatureFileCommonKeys.CODE),
      actualGRAccountingDocumentDetails.get(IAccountingFeatureFileCommonKeys.GR_CODE),
      actualGRAccountingDocumentDetails.get(IAccountingFeatureFileCommonKeys.PO_CODE)
    };
  }

  public void assertThatGoodsReceiptAccountingDocumentItemDetails(
      DataTable grAccountingDocumentItemDetailsDataTable) {
    List<Map<String, String>> grAccountingItems =
        grAccountingDocumentItemDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> grAccountingItem : grAccountingItems) {

      Object actualGRAccountingItem =
          (Object)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getGRAccountingDocumentItems",
                  constructGRAccountingDocumentItemsParams(grAccountingItem));

      assertNotNull(actualGRAccountingItem);
    }
  }

  private Object[] constructGRAccountingDocumentItemsParams(Map<String, String> grAccountingItem) {
    return new Object[] {
      grAccountingItem.get(IFeatureFileCommonKeys.CODE),
      grAccountingItem.get(IFeatureFileCommonKeys.ITEM),
      grAccountingItem.get(IFeatureFileCommonKeys.UOM),
      Double.parseDouble(grAccountingItem.get(IFeatureFileCommonKeys.QTY)),
      grAccountingItem.get(IFeatureFileCommonKeys.STOCK_TYPE),
      Double.parseDouble(
          grAccountingItem.get(IAccountingFeatureFileCommonKeys.TO_BE_CONSIDERED_IN_COSTING))
    };
  }

  public void assertThatPurchaseOrderDoeNotHasPostedCustomTrusteeIvoice(String poCode) {
    Object vendorInvoice =
        (Object)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPostedVendorInvoiceOfTypeCustomTrusteeInvoiceAndPO", poCode);

    assertNull(vendorInvoice);
  }

  public void assertThatGoodsReceiptAccountingDocumentItemDetailsIsUpdated(
      String grAccDocCode, DataTable itemDetailsDataTable) {
    List<Map<String, String>> items = itemDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : items) {

      Object[] actualGRAccountingItem =
          (Object[])
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getUpdatedGRAccountingDocumentItems",
                  constructUpdatedGRAccountingDocumentItemsParams(grAccDocCode, item));

      assertNotNull(actualGRAccountingItem);

      assertThatItemTotalCostEqualsToExpected(item, actualGRAccountingItem);
    }
  }

  private void assertThatItemTotalCostEqualsToExpected(
      Map<String, String> item, Object[] actualGRAccountingItem) {
    double quantity = calculateActualTotalCost(actualGRAccountingItem);

    assertEquals(item.get(IAccountingFeatureFileCommonKeys.ITEM_NET_COST_PER_BASE_UNIT), quantity);
  }

  private double calculateActualTotalCost(Object[] actualGRAccountingItem) {
    double itemTotalCost =
        getDouble(actualGRAccountingItem[7]) + getDouble(actualGRAccountingItem[8]);

    return itemTotalCost * getDouble(actualGRAccountingItem[4]);
  }

  private Object[] constructUpdatedGRAccountingDocumentItemsParams(
      String grAccDocCode, Map<String, String> grAccountingItem) {
    return new Object[] {
      grAccDocCode,
      grAccountingItem.get(IFeatureFileCommonKeys.ITEM),
      grAccountingItem.get(IFeatureFileCommonKeys.UOM),
      Double.parseDouble(grAccountingItem.get(IFeatureFileCommonKeys.QTY)),
      grAccountingItem.get(IFeatureFileCommonKeys.STOCK_TYPE),
      Double.parseDouble(
          grAccountingItem.get(IAccountingFeatureFileCommonKeys.TO_BE_CONSIDERED_IN_COSTING))
    };
  }

  public void assertThatPurchaseOrderHasPostedGoodsIvoice(String poCode, String viCode) {
    DObVendorInvoiceGeneralModel vendorInvoice =
        (DObVendorInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPostedVendorInvoiceOfTypeGoodsInvoiceAndPO", poCode);

    assertNotNull(vendorInvoice);
    assertEquals(vendorInvoice.getUserCode(), viCode);
  }

  public void assertThatPurchaseOrderDoesNotHasPostedGoodsIvoice(String poCode) {
    DObVendorInvoiceGeneralModel vendorInvoice =
        (DObVendorInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPostedVendorInvoiceOfTypeGoodsInvoiceAndPO", poCode);

    assertNull(vendorInvoice);
  }

  public void assertThatPurchaseOrderDoeHasPostedCustomTrusteeIvoice(String poCode, String viCode) {
    DObVendorInvoiceGeneralModel vendorInvoice =
        (DObVendorInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPostedVendorInvoiceOfTypeCustomTrusteeInvoiceAndPO", poCode);

    assertNotNull(vendorInvoice);
    assertEquals(vendorInvoice.getUserCode(), viCode);
  }

  public void assertThatPORelatedToGRAccountingDocument(
      String poCode, String grAccountingDocumentCode) {
    Object GRAccountingDocument =
        (Object)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getGRAccountingDocumentByUserCodeAndPOCode", grAccountingDocumentCode, poCode);

    assertNotNull(GRAccountingDocument);
  }
}
