package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesViewNotesDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObMonetaryNotesViewNotesDetailsStep extends SpringBootRunner implements En {

  DObMonetaryNotesViewNotesDetailsTestUtils utils;

  public DObMonetaryNotesViewNotesDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view NotesReceivableDetails section of MonetaryNotes with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readNotesDetails(cookie, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of NotesReceivableDetails section for MonetaryNotes with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String monetaryNoteCode, String userName, DataTable companyDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertMonetaryNoteNotesDetailsIsCorrect(response, companyDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObMonetaryNotesViewNotesDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View MonetaryNotes NotesReceivableDetails section,")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }
}
