package com.ebs.dda.accounting.initialbalanceupload.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUploadGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class DObInitailBalanceUploadViewAllTestUtils
    extends DObInitialBalanceUploadCommonTestUtils {

  public static String REQUEST_LOCALE = "en";

  public DObInitailBalanceUploadViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertInitailBalanceUploadDataIsCorrect(
      DataTable initialBalanceUploadDataTable, Response response)
      throws IOException, JSONException {
    List<Map<String, String>> initialBalanceUploadList =
        initialBalanceUploadDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> initialBalanceUploadResponse =
        response.body().jsonPath().getList("data");
    for (int i = 1; i < initialBalanceUploadList.size(); i++) {
      assertCorrectShownData(
          initialBalanceUploadResponse.get(i - 1), initialBalanceUploadList.get(i - 1));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> responsedInitialBalanceUpload,
      Map<String, String> initailBalanceUploadRowMap)
      throws IOException, JSONException {

    String code = initailBalanceUploadRowMap.get(IFeatureFileCommonKeys.CODE);
    String purchaseUnitName = initailBalanceUploadRowMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    String state = initailBalanceUploadRowMap.get(IFeatureFileCommonKeys.STATE);
    String creationDate = initailBalanceUploadRowMap.get(IFeatureFileCommonKeys.CREATION_DATE);
    String documentOwner = initailBalanceUploadRowMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER);
    String company = initailBalanceUploadRowMap.get(IFeatureFileCommonKeys.COMPANY);
    String fiscalYear =
        initailBalanceUploadRowMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR);

    assertEquals(
        code,
        getAsString(
            responsedInitialBalanceUpload.get(IDObInitialBalanceUploadGeneralModel.USER_CODE)));

    String localizedPurchaseUnitName =
        getLocaleFromLocalizedResponse(
            getLocalizedValue(
                responsedInitialBalanceUpload.get(
                    IDObInitialBalanceUploadGeneralModel.PURCHASING_UNIT_NAME)),
            REQUEST_LOCALE);
    assertEquals(purchaseUnitName, localizedPurchaseUnitName);
    assertTrue(
        responsedInitialBalanceUpload
            .get(IDObInitialBalanceUploadGeneralModel.CURRENT_STATES)
            .toString()
            .contains(state));
    assertCreationDateEquals(creationDate, responsedInitialBalanceUpload);

    assertEquals(
        documentOwner,
        responsedInitialBalanceUpload.get(IDObInitialBalanceUploadGeneralModel.DOCUMENT_OWNER));

    assertEquals(
            company,
            responsedInitialBalanceUpload.get(IDObInitialBalanceUploadGeneralModel.COMAPNY_CODE)
                    + " - "
                    + getEnglishLocaleFromLocalizedResponse(
                    getLocalizedValue(
                            responsedInitialBalanceUpload.get(IDObInitialBalanceUploadGeneralModel.COMPANY_NAME))));

    assertEquals(
        fiscalYear,
        responsedInitialBalanceUpload.get(IDObInitialBalanceUploadGeneralModel.FISCAL_YEAR));
  }

  private void assertCreationDateEquals(
      String creationDate, Map<String, Object> initialBalanceUploadDatabaseVersion) {
    if (creationDate != null && !creationDate.isEmpty()) {
      assertThatDatesAreEqual(
          creationDate,
          String.valueOf(
              initialBalanceUploadDatabaseVersion.get(
                  IDObInitialBalanceUploadGeneralModel.CREATION_DATE)));
    } else {
      assertNull(
          initialBalanceUploadDatabaseVersion.get(
              IDObInitialBalanceUploadGeneralModel.CREATION_DATE));
    }
  }
}
