package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestDueDocumentTypeDropdownTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPaymentRequestDueDocumentTypeDropDownStep extends SpringBootRunner implements En {

    public static final String SCENARIO_NAME = "Read all DueDocumentTypes per payment type";
    private DObPaymentRequestDueDocumentTypeDropdownTestUtils utils;

    DObPaymentRequestDueDocumentTypeDropDownStep() {
        When("^\"([^\"]*)\" requests to read all DueDocumentTypes for PaymentRequest with type \"([^\"]*)\"$",
                (String userName, String paymentType) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            Response response = utils.readDueDocumentTypesForPaymentRequest(cookie, paymentType);
            userActionsTestUtils.setUserResponse(userName, response);
        });
        Then("^the \"([^\"]*)\" values are displayed to \"([^\"]*)\":$",
                (String dueDocumentTypes, String userName) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertResponseSuccessStatus(response);
            utils.assertDueDocumentTypesDropDownResultIsCorrect(dueDocumentTypes, response);
        });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new DObPaymentRequestDueDocumentTypeDropdownTestUtils(getProperties(), entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {}
}
