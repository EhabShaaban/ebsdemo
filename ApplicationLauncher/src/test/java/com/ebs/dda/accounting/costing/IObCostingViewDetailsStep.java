package com.ebs.dda.accounting.costing;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.costing.utils.IObCostingViewDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObCostingViewDetailsStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "View CostingDocument Details section";
  private IObCostingViewDetailsTestUtils utils;

  public IObCostingViewDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view Details section for Costing with code \"([^\"]*)\"$",
        (String userName, String costingCode) -> {
          String readUrl = utils.getCostingDetailsViewDataUrl(costingCode);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(userCookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of Details section for Costing with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String costingCode,
            String userName,
            DataTable detailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayed(response, detailsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObCostingViewDetailsTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeViewAll(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(utils.clearCostingDocumentInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
