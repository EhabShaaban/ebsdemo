package com.ebs.dda.accounting.monetarynotes.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/accounting/monetary-notes/NotesReceivable_Delete_ReferenceDocument.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_Save_Add_ReferenceDocument_HP.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Activate_HP.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_SalesInvoice_Dropdown.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_NotesReceivable_DropDown.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_SalesOrder_Dropdown.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_ReferenceDocument_Type_Dropdown.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_RequestAdd_ReferenceDocument.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Save_NotesDetails_HP.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_View_ReferenceDocument.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_RequestEdit_Notes_Details.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Depot_Dropdown.feature",
      "classpath:features/accounting/monetary-notes/NR_Type_Dropdown.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_DocumentOwner_Dropdown.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Forms_Dropdown.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_HomeScreen_AllowedActions.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_ObjectScreen_AllowedActions.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Create_HP.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_ViewAll.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_View_GeneralData.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_View_CompanyData.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_View_NotesDetails.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_View_AccountingDetails.feature",
      "classpath:features/accounting/monetary-notes/NotesPayable_ViewAll.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Create_Auth.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Create_Val.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Activate_Val.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivables_View_ActivationDetails.feature",
      "classpath:features/accounting/monetary-notes/NotesReceivable_Save_Add_ReferenceDocument_Val.feature",
      "classpath:features/accounting/monetary-notes/MonetaryNotes_Save_NotesDetails_Val.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.monetarynotes"},
    strict = true,
    tags = "not @Future")
public class DObMonetaryNotesCucumberRunner {}
