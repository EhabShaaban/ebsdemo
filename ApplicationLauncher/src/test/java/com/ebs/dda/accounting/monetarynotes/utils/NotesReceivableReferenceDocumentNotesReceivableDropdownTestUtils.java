package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotesReceivableReferenceDocumentNotesReceivableDropdownTestUtils
    extends DObMonetaryNotesCommonTestUtils {

  protected String ASSERTION_MSG =
      "[NotesReceivable][ReferenceDocument][NotesReceivable][Dropdown]";

  public NotesReceivableReferenceDocumentNotesReceivableDropdownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    return str.toString();
  }

  public Response readAllReferenceDocumentNotesReceivables(
      Cookie cookie, String notesReceivableCode) {
    String restURL =
        IDObNotesReceivableRestURLs.QUERY
            + "/"
            + notesReceivableCode
            + IDObNotesReceivableRestURLs.READ_ALL_REFERENCE_DOCUMENT_NOTES_RECEIVABLES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReferenceDocumentNotesReceivablesResponseIsCorrect(
      DataTable notesReceivablesDataTable, Response response) {
    List<Map<String, String>> expectedNotesReceivablesMapList =
        notesReceivablesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualNotesReceivablesList = getListOfMapsFromResponse(response);
    Assert.assertEquals(expectedNotesReceivablesMapList.size(), actualNotesReceivablesList.size());
    for (int i = 0; i < expectedNotesReceivablesMapList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(
              expectedNotesReceivablesMapList.get(i), actualNotesReceivablesList.get(i));
      mapAssertion.assertValue(
          IFeatureFileCommonKeys.CODE, IDObNotesReceivablesGeneralModel.USER_CODE);
    }
  }

  public void assertThatTotalNumberOfReferenceDocumentNotesReceivablesEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualNotesReceivables = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of ReferenceDocument NotesReceivables is not correct",
        expectedTotalNumber.intValue(),
        actualNotesReceivables.size());
  }
}
