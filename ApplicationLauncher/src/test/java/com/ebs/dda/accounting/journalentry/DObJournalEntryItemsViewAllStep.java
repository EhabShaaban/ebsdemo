package com.ebs.dda.accounting.journalentry;

import java.sql.SQLException;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryItemsViewAllTestUtils;
import com.ebs.dda.jpa.accounting.journalentry.IIObJournalEntryItemGeneralModel;
import com.ebs.dda.purchases.utils.DObPurchaseOrderCommonTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObJournalEntryItemsViewAllStep extends SpringBootRunner
				implements En, InitializingBean {

	public static final String SCENARIO_NAME = "Filter View All JournalEntries";
	private static boolean hasBeenExecuted = false;
	private DObJournalEntryItemsViewAllTestUtils journalEntryItemsViewAllTestUtils;

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		journalEntryItemsViewAllTestUtils = new DObJournalEntryItemsViewAllTestUtils(properties,
						entityManagerDatabaseConnector);
	}

	public DObJournalEntryItemsViewAllStep() {
		Given("^the following JournalEntriesItems exist:$",
						(DataTable journalEntryItemsDataTable) -> {
							journalEntryItemsViewAllTestUtils.assertThatJournalEntryItemsExist(
											journalEntryItemsDataTable);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with no filter applied with (\\d+) records per page$",
						(String userName, Integer pageNum, Integer recordsNum) -> {
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(userCookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNum, recordsNum, "");
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^the following values will be presented to \"([^\"]*)\":$",
						(String userName, DataTable journalEntryItemsDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							journalEntryItemsViewAllTestUtils.assertResponseSuccessStatus(response);
							journalEntryItemsViewAllTestUtils
											.assertThatJournalEntryItemsArePresentedToUser(response,
															journalEntryItemsDataTable);
						});

		Then("^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
						(String userName, Integer numberOfRecords) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							journalEntryItemsViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
											response, numberOfRecords);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on JournalEntryCode which contains \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getContainsFilterField(
															IIObJournalEntryItemGeneralModel.CODE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on JournalDate which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getContainsFilterField(
															IIObJournalEntryItemGeneralModel.DUE_DATE,
															String.valueOf(journalEntryItemsViewAllTestUtils
																			.getDateTimeInMilliSecondsFormat(
																							filteringValue)));
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on GLAccount which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords, String locale) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getContainsFilterField(
															IIObJournalEntryItemGeneralModel.ACCOUNT,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredDataWithLocale(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString, locale);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on GLSubAccount which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords, String locale) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getContainsFilterField(
															IIObJournalEntryItemGeneralModel.SUB_ACCOUNT,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredDataWithLocale(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString, locale);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on Debit which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.DEBIT,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on Credit which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.CREDIT,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on Currency\\(Document\\) which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.DOCUMENT_CURRENCY_CODE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on ExRateToCurrency\\(Company\\) which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.COMPANY_CURRENCY_PRICE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on Currency\\(Company\\) which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.COMPANY_CURRENCY_CODE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on Currency\\(CompanyGroup\\) which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.COMPANY_GROUP_CURRENCY_CODE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on ExRateToCurrency\\(CompanyGroup\\) which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.COMPANY_GROUP_CURRENCY_PRICE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on Company which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.COMPANY_CODE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" requests to read page (\\d+) of JournalEntriesItems with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
						(String userName, Integer pageNumber, String filteringValue,
										Integer numberOfRecords) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String filterString = journalEntryItemsViewAllTestUtils
											.getEqualsFilterField(
															IIObJournalEntryItemGeneralModel.PURCHASE_UNIT_CODE,
															filteringValue);
							Response response = journalEntryItemsViewAllTestUtils
											.requestToReadFilteredData(cookie,
															IDObJournalEntryRestUrls.ITEMS_GET_URL,
															pageNumber, numberOfRecords,
															filterString);
							userActionsTestUtils.setUserResponse(userName, response);
						});
	}

	@Before
	public void before(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				databaseConnector.executeSQLScript(
								journalEntryItemsViewAllTestUtils.getDbScriptsOneTimeExecution());
				journalEntryItemsViewAllTestUtils.executeJournalEntryReferenceDocumentFunction();
				journalEntryItemsViewAllTestUtils.executeJournalEntryDetailTypeFunction();
				journalEntryItemsViewAllTestUtils.executeJournalEntryDetailSubAccountDeterminationFunction();
				journalEntryItemsViewAllTestUtils.executeJournalEntryReferenceDocumentFunction();
				hasBeenExecuted = true;
			}
			databaseConnector.executeSQLScript(
					DObJournalEntryItemsViewAllTestUtils.clearJournalEntries());
			databaseConnector.executeSQLScript(
					AccountingDocumentCommonTestUtils.clearAccountingDocuments());
			databaseConnector.executeSQLScript(
					DObPurchaseOrderCommonTestUtils.clearOrderDbScriptPath());
		}
	}

	@After
	public void after(Scenario scenario) throws SQLException {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
