package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.monetarynotes.utils.IObNotesReceivableViewActivationDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class IObNotesReceivablesViewActivationDetailsStep extends SpringBootRunner implements En {

    private IObNotesReceivableViewActivationDetailsTestUtils utils;
    private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
    public static final String SCENARIO_NAME = "View ActivationDetails section in NotesReceivables";


    public IObNotesReceivablesViewActivationDetailsStep() {
        When("^\"([^\"]*)\" requests to view ActivationDetails section of NotesReceivables with code \"([^\"]*)\"$", (String username, String nrCode) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(username);
            Response response = utils.sendGETRequest(cookie, String.format(
                    IDObNotesReceivableRestURLs.VIEW_ACTIVATION_DETAILS, nrCode));
            userActionsTestUtils.setUserResponse(username, response);
        });

        Then("^the following values of ActivationDetails section of NotesReceivables with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$", (String nrCode, String username, DataTable activationDetailsTable) -> {
            Response response = userActionsTestUtils.getUserResponse(username);
            utils.assertThatViewActivationDetailsResponseIsCorrect(response, activationDetailsTable);
        });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new IObNotesReceivableViewActivationDetailsTestUtils(properties, entityManagerDatabaseConnector);
        journalEntryViewAllTestUtils = new DObJournalEntryViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
    }

    @Before
    public void beforeViewAll(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.executeSQLScript(DObJournalEntryViewAllTestUtils.clearJournalEntries());
            databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.closeConnection();
        }
    }
}
