package com.ebs.dda.accounting.collection.utils;

import static com.ebs.dda.IFeatureFileCommonKeys.BUSINESS_UNIT;
import static com.ebs.dda.IFeatureFileCommonKeys.CODE;
import static com.ebs.dda.IFeatureFileCommonKeys.COMPANY;
import static com.ebs.dda.IFeatureFileCommonKeys.CREATED_BY;
import static com.ebs.dda.IFeatureFileCommonKeys.CREATION_DATE;
import static com.ebs.dda.IFeatureFileCommonKeys.CURRENCY;
import static com.ebs.dda.IFeatureFileCommonKeys.DOCUMENT_OWNER;
import static com.ebs.dda.IFeatureFileCommonKeys.REFERENCE_DOCUMENT;
import static com.ebs.dda.IFeatureFileCommonKeys.STATE;
import static com.ebs.dda.IFeatureFileCommonKeys.TOTAL_REMAINING;
import static com.ebs.dda.IFeatureFileCommonKeys.TREASURY;
import static com.ebs.dda.IFeatureFileCommonKeys.TYPE;
import static com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum.DEBIT_NOTE;
import static com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum.NOTES_RECEIVABLE;
import static com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum.PAYMENT_REQUEST;
import static com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum.SALES_INVOICE;
import static com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum.SALES_ORDER;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import cucumber.api.DataTable;

public class DObCollectionTestUtils extends CommonTestUtils {

  private static final String VENDOR_OBJECT_TYPE_CODE = "2";
  private static final String CUSTOMER_OBJECT_TYPE_CODE = "3";
  private static final String EMPLOYEE_OBJECT_TYPE_CODE = "4";

  protected final String ASSERTION_MSG = "[DObCollection] ";

  public DObCollectionTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/CObBankSqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceSummary.sql");

    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");

    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables_ViewAll.sql");
    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables.sql");
    str.append(",").append("db-scripts/accounting/collection/CObCollectionType.sql");

    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append(",").append("db-scripts/purchase-order/ServicePurchaseOrder.sql");

    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append(",").append("db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append(",").append("db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    str.append(",")
        .append("db-scripts/accounting/payment-request/iob-payment-request-accounting-details.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");
    str.append(",")
        .append("db-scripts/accounting/vendorinvoice/IObVendorInvoicePostingDetails.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorAccountingInfo.sql");
    str.append(",").append("db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append(",").append("db-scripts/accounting/journal-entry-items.sql");
    return str.toString();
  }

  public String getConciseDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");

    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/CObBankSqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/treasury/CObTreasury.sql");
    str.append(",").append("db-scripts/master-data/treasury/IObCompanyTreasuries.sql");
    str.append(",").append("db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables_ViewAll.sql");
    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables.sql");
    str.append(",").append("db-scripts/accounting/collection/CObCollectionType.sql");
    str.append(",").append("db-scripts/accounting/collection/DObCollection_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/DObCollection.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionCompanyDetails.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionDetails_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionDetails.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionAccountingDetails.sql");
    str.append(",")
        .append("db-scripts/accounting/collection/IObCollectionAccountingDetails_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/DObCollectionJournalEntry.sql");
    str.append(",")
        .append("db-scripts/accounting/collection/DObCollectionJournalEntry_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionActivationDetails.sql");
    str.append(",")
        .append("db-scripts/accounting/collection/IObCollectionActivationDetails_viewAll.sql");
    return str.toString();
  }

  public String resetDataDependanciesBeforeInsertion() {
    StringBuilder str = new StringBuilder();

    str.append(",")
        .append("db-scripts/accounting/collection/Clear_Collection_Activation_Dependancies.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorAccountingInfo.sql");

    return str.toString();

  }

  public String getCreateDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/accounting/collection/DObCollection_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/DObCollection.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionCompanyDetails.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionDetails_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionDetails.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionAccountingDetails.sql");
    str.append(",")
            .append("db-scripts/accounting/collection/IObCollectionAccountingDetails_ViewAll.sql");
    return str.toString();

  }

  public void assertThatCollectionsExistByCodeAndStateAndBU(DataTable collectionsDataTable) {
    List<Map<String, String>> collectionsList =
        collectionsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collection : collectionsList) {
      Object[] collectionsParams = constructCollectionsParams(collection);
      DObCollectionGeneralModel dObCollectionGeneralModel =
          (DObCollectionGeneralModel) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("getCollectionByCodeAndStateAndBusinessUnit",
                  collectionsParams);
      Assert.assertNotNull("Collection with code: " + collection.get(CODE) + " doesn't exist!",
          dObCollectionGeneralModel);
    }
  }

  private Object[] constructCollectionsParams(Map<String, String> collection) {
    return new Object[] {collection.get(CODE), "%" + collection.get(STATE) + "%",
        collection.get(BUSINESS_UNIT)};
  }

  protected void retrieveAccountingDetailsData(String collectionCode,
      Map<String, String> accountDetailsMap) {
    Object crAccountingDetails = entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCollectionAccountingDetailsGeneralModel",
        getExpectedAccountingDetailsData(collectionCode, accountDetailsMap));
    Assert.assertNotNull(ASSERTION_MSG + "Accounting details for Collection with code "
        + collectionCode + " not exist", crAccountingDetails);
  }

  private Object[] getExpectedAccountingDetailsData(String collectionCode,
      Map<String, String> accountDetailsMap) {
    return new Object[] {accountDetailsMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
        accountDetailsMap.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT),
        getDouble(accountDetailsMap.get(IAccountingFeatureFileCommonKeys.AMOUNT)), collectionCode,
        accountDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE)};
  }

  public void assertThatSalesOrderRemainingsAreCorrect(DataTable salesOrderRemainingsTable) {
    List<Map<String, String>> salesOrders =
        salesOrderRemainingsTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrder : salesOrders) {
      assertSalesOrderRemainingIsCorrect(salesOrder);
    }
  }

  private void assertSalesOrderRemainingIsCorrect(Map<String, String> salesOrderMap) {
    DObSalesOrder salesOrder =
        (DObSalesOrder) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderByCodeAndRemaining", getExpectedSalesOrderData(salesOrderMap));
    Assert.assertNotNull(
        ASSERTION_MSG + "Sales Order with code " + salesOrderMap.get(CODE) + " Doesn't exist",
        salesOrder);
  }

  private Object[] getExpectedSalesOrderData(Map<String, String> salesOrder) {
    return new Object[] {salesOrder.get(CODE), getDouble(salesOrder.get(TOTAL_REMAINING))};
  }

  public void createCollectionGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> collectionGeneralDataMaps =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionGeneralDataMap : collectionGeneralDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject("createCollectionGeneralData",
          getGeneralDataCreateParameters(collectionGeneralDataMap));
    }
  }

  private Object[] getGeneralDataCreateParameters(Map<String, String> collectionGeneralDataMap) {
    return new Object[] {collectionGeneralDataMap.get(CODE),
        collectionGeneralDataMap.get(CREATION_DATE), collectionGeneralDataMap.get(CREATION_DATE),
        collectionGeneralDataMap.get(CREATED_BY), collectionGeneralDataMap.get(CREATED_BY),
        collectionGeneralDataMap.get(STATE), collectionGeneralDataMap.get(TYPE),
        collectionGeneralDataMap.get(DOCUMENT_OWNER),
        collectionGeneralDataMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER_TYPE),};
  }

  public void createCollectionCompanyData(DataTable companyDataTable) {
    List<Map<String, String>> collectionCompanyDataMaps =
        companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionCompanyDataMap : collectionCompanyDataMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject("createCollectionCompanyData",
          getCompanyDataCreateParameters(collectionCompanyDataMap));
    }
  }

  private Object[] getCompanyDataCreateParameters(Map<String, String> collectionCompanyDataMap) {
    return new Object[] {collectionCompanyDataMap.get(CODE),
        collectionCompanyDataMap.get(BUSINESS_UNIT), collectionCompanyDataMap.get(COMPANY)};
  }

  public void createCollectionDetailsData(DataTable collectionDetailsDataTable) {
    List<Map<String, String>> collectionDetailsDataMaps =
        collectionDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionDetailsDataMap : collectionDetailsDataMaps) {

      Object[] queryParams = getCollectionDetailsDataCreateParameters(collectionDetailsDataMap);
      String detailsCreationQueryName = getDetailsCreationQueryName(collectionDetailsDataMap);

      entityManagerDatabaseConnector.executeInsertQueryForObject(detailsCreationQueryName,
          queryParams);

    }
  }

  private String getDetailsCreationQueryName(Map<String, String> details) {
    if (details.get(TYPE).equals(DEBIT_NOTE)) {
      return "createCollectionDetailsDataForDebitNote";
    }
    if (details.get(TYPE).equals(SALES_INVOICE)) {
      return "createCollectionDetailsDataForSalesInvoice";
    }
    if (details.get(TYPE).equals(SALES_ORDER)) {
      return "createCollectionDetailsDataForSalesOrder";
    }
    if (details.get(TYPE).equals(NOTES_RECEIVABLE)) {
      return "createCollectionDetailsDataForNotesReceivable";
    }
    if (details.get(TYPE).equals(PAYMENT_REQUEST)) {
      return "createCollectionDetailsDataForPyamentRequest";
    }
    throw new IllegalArgumentException("Ref Doc Type is not supported");
  }

  private String getBusinessPartnerObjectTypeCode(String refDocumentType) {
    if (refDocumentType.equals(DEBIT_NOTE)) {
      return VENDOR_OBJECT_TYPE_CODE;
    }
    if (refDocumentType.equals(SALES_INVOICE)) {
      return CUSTOMER_OBJECT_TYPE_CODE;
    }
    if (refDocumentType.equals(SALES_ORDER)) {
      return CUSTOMER_OBJECT_TYPE_CODE;
    }
    if (refDocumentType.equals(NOTES_RECEIVABLE)) {
      return CUSTOMER_OBJECT_TYPE_CODE;
    }
    if (refDocumentType.equals(PAYMENT_REQUEST)) {
      return EMPLOYEE_OBJECT_TYPE_CODE;
    }
    throw new IllegalArgumentException("Ref Doc Type is not supported");
  }

  private Object[] getCollectionDetailsDataCreateParameters(
      Map<String, String> collectionDetailsDataMap) {
    String amountStr = collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.AMOUNT);
    return new Object[] {collectionDetailsDataMap.get(CODE),
        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
        getBusinessPartnerObjectTypeCode(collectionDetailsDataMap.get(TYPE)),
        collectionDetailsDataMap.get(TYPE),
        (amountStr.isEmpty())? null : new BigDecimal(amountStr),
        collectionDetailsDataMap.get(CURRENCY),
        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.COLLECTION_METHOD),
        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT),
        collectionDetailsDataMap.get(TREASURY), collectionDetailsDataMap.get(REFERENCE_DOCUMENT)};
  }

  public String clearIsertedDataFromFeatureFile() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/collection/ClearInsertedDataFromFeatureFile.sql");
    return str.toString();
  }

  public String clearAccountingNoteInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/accountingnote/accounting_note_clear_drop_down.sql");
    return str.toString();
  }

  public static String clearCollectionInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/collection/collection_clear.sql");
    return str.toString();
  }

  public void createCollectionAccountingDetails(DataTable collectionAccountingDetails) {

    List<Map<String, String>> accountingDetailsMaps =
        collectionAccountingDetails.asMaps(String.class, String.class);

    for (Map<String, String> accountingDetailsMap : accountingDetailsMaps) {

      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createCollectionAccountingDetails",
          getAccountingDetailsDataCreateParameters(accountingDetailsMap));

      entityManagerDatabaseConnector
          .executeInsertQuery("createAccountingDetailsDeterminationFunction");

      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
          "insertCollectionAccountingDetailsSubAccounts",
          getAccountingDetailsSubAccountDataCreateParameters(accountingDetailsMap));

    }
  }

  private Object[] getAccountingDetailsDataCreateParameters(
      Map<String, String> collectionDetailsDataMap) {
    String valueAndCurency = collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.VALUE);
    return new Object[] {

        collectionDetailsDataMap.get(CODE),
        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
        new BigDecimal(valueAndCurency.split(" ")[0])

    };
  }

  private Object[] getAccountingDetailsSubAccountDataCreateParameters(
      Map<String, String> collectionDetailsDataMap) {
    return new Object[] {

        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
        collectionDetailsDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT)

    };
  }
}
