package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.CObLandedCostTypesDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class CObLandedCostTypesDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private CObLandedCostTypesDropDownTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new CObLandedCostTypesDropDownTestUtils(properties, entityManagerDatabaseConnector);
  }

  public CObLandedCostTypesDropDownStep() {
      Given(
              "^the following are ALL exisitng LandedCostTypes:$",
              (DataTable goodsIssueTypes) -> {
                  utils.assertThatLandedCostTypesExist(goodsIssueTypes);
              });

      Given(
        "^the total number of existing LandedCostTypes are (\\d+)$",
        (Integer recordsNumber) -> {
          utils.assertThatLandedCostTypesCountIsCorrect(recordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read all LandedCostTypes$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, utils.readAllLandedCostTypes());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following LandedCost Types values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable LandedCostTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseMatchesExpected(response, LandedCostTypesDataTable);
        });

    Then(
        "^total number of LandedCost Types returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer recordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, recordsNumber);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read list of LandedCostTypes")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Read list of LandedCostTypes")) {

      databaseConnector.closeConnection();
    }
  }
}
