package com.ebs.dda.accounting.settlement.utils;

import static com.ebs.dac.foundation.realization.statemachine.BasicStateMachine.DRAFT_STATE;
import static com.ebs.dac.foundation.realization.statemachine.PrimitiveStateMachine.ACTIVE_STATE;

import java.util.List;
import java.util.Map;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;

import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import cucumber.api.DataTable;

public class DObSettlementsCommonTestUtils extends AccountingDocumentCommonTestUtils {

	private static final String SETTLEMENT_JMX_LOCK_BEAN_NAME = "SettlementEntityLockCommand";

	public DObSettlementsCommonTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public static String getSettlementDbScriptsOneTimeExecution() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/clearDataSqlScript.sql");
		str.append("," + "db-scripts/CObBankSqlScript.sql");
		str.append("," + "db-scripts/CObCurrencySqlScript.sql");
		str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
		str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
		str.append("," + "db-scripts/CObMeasureScript.sql");
		str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
		str.append(",").append("db-scripts/master-data/item/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
		str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
		str.append("," + "db-scripts/master-data/item/CObItem.sql");
		str.append("," + "db-scripts/master-data/item/MObItem.sql");
		str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
		str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
		str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
		str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
		str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
		str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
		str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
		str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
		str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
		str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
		str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
		return str.toString();
	}

	public static String clearSettlement() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/accounting/settlement/settlement_Clear.sql");
		return str.toString();
	}

	public void insertSettlementsGeneralData(DataTable generalDataTable) {
		List<Map<String, String>> settlementGeneralDataListMap = generalDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> settlementGeneralDataMap : settlementGeneralDataListMap) {

			entityManagerDatabaseConnector.executeInsertQuery("insertSettlementsGeneralData",
							constructSettlementGeneralDataInsertionParams(
											settlementGeneralDataMap));
		}
	}

	private Object[] constructSettlementGeneralDataInsertionParams(
					Map<String, String> settlementMap) {
		String state = settlementMap.get(IFeatureFileCommonKeys.STATE);

		if (state.equals(DRAFT_STATE)) {
			state = "[\"" + state + "\"]";
		} else {
			state = "[\"" + ACTIVE_STATE + "\",\"" + state + "\"]";
		}

		return new Object[] { settlementMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE),
						settlementMap.get(IFeatureFileCommonKeys.CREATION_DATE),
						settlementMap.get(IFeatureFileCommonKeys.CREATION_DATE),
						settlementMap.get(IFeatureFileCommonKeys.CREATED_BY),
						settlementMap.get(IFeatureFileCommonKeys.CREATED_BY), state,
				        settlementMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
						settlementMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_TYPE),
						settlementMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE),
						settlementMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE), };
	}

	public void insertSettlementCompanyData(DataTable companyDataTable) {
		List<Map<String, String>> settlementCompanyDataListMap = companyDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> settlementComapanyDataMap : settlementCompanyDataListMap) {

			Object[] params = new Object[] {
							settlementComapanyDataMap
											.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE),
							settlementComapanyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
							settlementComapanyDataMap.get(IFeatureFileCommonKeys.COMPANY) };

			entityManagerDatabaseConnector.executeInsertQuery("insertSettlementCompanyData",
							params);
		}
	}

	public void insertSettlementDetailsData(DataTable detailsDataTable) {
		List<Map<String, String>> settlementDetailsDataListMap = detailsDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> settlementDetailsDataMap : settlementDetailsDataListMap) {

			Object[] params = constructSettlementDetailsInsertionParams(settlementDetailsDataMap);

			entityManagerDatabaseConnector.executeInsertQuery("insertSettlementDetailsData",
							params);
		}
	}

	private Object[] constructSettlementDetailsInsertionParams(Map<String, String> settlementMap) {

		settlementMap = convertEmptyStringsToNulls(settlementMap);

		return new Object[] { settlementMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE),
						settlementMap.get(IFeatureFileCommonKeys.CURRENCY), };
	}

	public void insertSettlementActivationDetails(DataTable postingDetailsDataTable) {
		List<Map<String, String>> postingDetailsMaps = postingDetailsDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> postingDetailsMap : postingDetailsMaps) {
			entityManagerDatabaseConnector.executeInsertQuery("insertSettlementActivationDetails",
							constructSettlementActivationDetailsInsertionParams(postingDetailsMap));
		}
	}

	private Object[] constructSettlementActivationDetailsInsertionParams(
					Map<String, String> postingDetailsMap) {
		String[] currencyPrice = postingDetailsMap
						.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE).split(" = ");
		String[] leftHandSide = currencyPrice[0].split(" ");
		String[] rightHandSide = currencyPrice[1].split(" ");
		String companyCurrencyIso = leftHandSide[1];
		String value = rightHandSide[0];
		String documentCurrencyIso = rightHandSide[1];

		return new Object[] { postingDetailsMap.get(IFeatureFileCommonKeys.CODE),
						postingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
						postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
						postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
						companyCurrencyIso, documentCurrencyIso, Double.parseDouble(value),
						Double.parseDouble(value),
				postingDetailsMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR)
		};
	}

	public void insertSettlementAccountingDetailsData(DataTable accountingDetailsTable) {
		List<Map<String, String>> accountingDetailsMaps = accountingDetailsTable
						.asMaps(String.class, String.class);
		for (Map<String, String> accountingDetailsMap : accountingDetailsMaps) {
			String detailId = accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.DETAIL_ID);
			if (detailId != null) {
				entityManagerDatabaseConnector.executeInsertQuery(
								"insertSettlementAccountingDetailsWithId",
								constructSettlementAccountingDetailsWithIdInsertionParams(detailId,
												accountingDetailsMap));
			} else {
				entityManagerDatabaseConnector.executeInsertQuery(
								"insertSettlementAccountingDetails",
								constructSettlementAccountingDetailsInsertionParams(
												accountingDetailsMap));
			}
			entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
							"insertSettlementAccountingDetailsSubAccounts",
							constructSettlementAccountingDetailsSubAccountsInsertionParams(
											accountingDetailsMap));
		}
	}

	private Object[] constructSettlementAccountingDetailsInsertionParams(
					Map<String, String> accountingDetailsMap) {
		return new Object[] {
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE),
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
						Double.parseDouble(accountingDetailsMap
										.get(IAccountingFeatureFileCommonKeys.VALUE)) };
	}

	private Object[] constructSettlementAccountingDetailsWithIdInsertionParams(String detailId,
					Map<String, String> accountingDetailsMap) {
		return new Object[] { detailId,
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE),
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
						Double.parseDouble(accountingDetailsMap
										.get(IAccountingFeatureFileCommonKeys.VALUE)) };
	}

	private Object[] constructSettlementAccountingDetailsSubAccountsInsertionParams(
					Map<String, String> accountingDetailsMap) {
		return new Object[] { accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
						accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT) };
	}

	public void assertThatLockIsReleased(String userName, String settlementCode, String sectionName)
					throws Exception {
		super.assertThatLockIsReleased(userName, settlementCode, SETTLEMENT_JMX_LOCK_BEAN_NAME,
						sectionName);
	}

	public void assertThatResourceIsLockedByUser(String userName, String settlementCode,
					String sectionName) throws Exception {
		assertThatResourceIsLockedByUser(userName, settlementCode, sectionName,
						SETTLEMENT_JMX_LOCK_BEAN_NAME);
	}

	public void executeAccountDeterminationFunction() {
		entityManagerDatabaseConnector.executeInsertQuery(
				"createAccountingDetailsDeterminationFunction");
	}
}
