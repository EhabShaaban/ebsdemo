package com.ebs.dda.accounting.initialbalanceupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.initialbalanceupload.utils.DObInitialBalanceUploadCreateTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObChartOfAccountsCreateHPTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObInitialBalanceUploadCreateStep extends SpringBootRunner implements En {
  private static boolean oneTimeExecution = false;
  private DObInitialBalanceUploadCreateTestUtils utils;
  private HObChartOfAccountsCreateHPTestUtils chartOfAccountsCreateHPTestUtils;
  private MObVendorCommonTestUtils vendorCommonTestUtils;

  public DObInitialBalanceUploadCreateStep() {
    Given(
        "^Last created InitialBalanceUpload has code \"([^\"]*)\"$",
        (String IBUCode) -> {
          utils.assertLastCreatedInitialBalanceUpload(IBUCode);
        });

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    When(
        "^\"([^\"]*)\" creates InitialBalanceUpload with the following values:$",
        (String userName, DataTable initialBalanceUploadDataTable) -> {
          Response response =
              utils.createInitialBalanceUpload(
                  initialBalanceUploadDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new InitialBalanceUpload is created with the following values:$",
        (DataTable initialBalanceUploadDataTable) -> {
          utils.assertInitialBalanceUploadIsCreatedSuccessfully(initialBalanceUploadDataTable);
        });

    Then(
        "^the Company Section of InitialBalanceUpload with code \"([^\"]*)\" is created as follows:$",
        (String initialBalanceUploadCode,
            DataTable initialBalanceUploadCompanyDetailsDataTable) -> {
          utils.assertThatInitialCompanyDetailsIsUpdated(
              initialBalanceUploadCode, initialBalanceUploadCompanyDetailsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to create InitialBalanceUpload$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(
                  cookie, IDObInitialBalanceUploadRestURLS.REQUEST_CREATE_INITIAL_BALANCE_UPLOAD);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    And(
        "^the Items Section of InitialBalanceUpload with code \"([^\"]*)\" is created as follows:$",
        (String userCode, DataTable itemsDataTable) -> {
          utils.assertThatInitialBalanceItemsAreCreatedSuccessfully(userCode, itemsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitialBalanceUploadCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    chartOfAccountsCreateHPTestUtils = new HObChartOfAccountsCreateHPTestUtils(getProperties());
    chartOfAccountsCreateHPTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    vendorCommonTestUtils = new MObVendorCommonTestUtils(getProperties());
    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create InitialBalanceUpload -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearIBUInsertions());
      if (!oneTimeExecution) {
        databaseConnector.executeSQLScript(utils.getDbScriptsPathOneTimeExecution());
        oneTimeExecution = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create InitialBalanceUpload -")) {
      databaseConnector.closeConnection();
    }
  }
}
