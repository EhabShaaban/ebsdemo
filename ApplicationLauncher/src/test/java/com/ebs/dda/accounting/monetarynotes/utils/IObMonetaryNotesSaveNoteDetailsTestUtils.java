package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObMonetaryNotesDetailsValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class IObMonetaryNotesSaveNoteDetailsTestUtils extends DObMonetaryNotesCommonTestUtils {
  public IObMonetaryNotesSaveNoteDetailsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response saveNoteDetailsData(String code, DataTable noteDetailsDataTable, Cookie cookie) {
    JsonObject noteDetailsJsonObject = getNoteDetailsJsonObject(noteDetailsDataTable);
    String restURL = getSaveNoteDetailsURL(code);
    return sendPUTRequest(cookie, restURL, noteDetailsJsonObject);
  }

  private String getSaveNoteDetailsURL(String code) {
    return IDObNotesReceivableRestURLs.BASE_COMMAND_URL
        + '/'
        + code
        + IDObNotesReceivableRestURLs.UPDATE_NOTES_DETAILS_SECTION;
  }

  private JsonObject getNoteDetailsJsonObject(DataTable noteDetailsDataTable) {
    JsonObject noteDetailsJsonObject = new JsonObject();
    Map<String, String> noteDetailsMap =
        noteDetailsDataTable.asMaps(String.class, String.class).get(0);

    addValueToJsonObject(
        noteDetailsJsonObject,
        IIObMonetaryNotesDetailsValueObject.NOTE_NUMBER,
        noteDetailsMap.get(IAccountingFeatureFileCommonKeys.NOTE_NUMBER));

    addValueToJsonObject(
        noteDetailsJsonObject,
        IIObMonetaryNotesDetailsValueObject.NOTE_BANK,
        noteDetailsMap.get(IAccountingFeatureFileCommonKeys.NOTE_BANK));

    addValueToJsonObject(
        noteDetailsJsonObject,
        IIObMonetaryNotesDetailsValueObject.DUE_DATE,
        noteDetailsMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE));

    addValueToJsonObject(
        noteDetailsJsonObject,
        IIObMonetaryNotesDetailsValueObject.DEPOT_CODE,
        getFirstValue(noteDetailsMap, IAccountingFeatureFileCommonKeys.DEPOT));

    return noteDetailsJsonObject;
  }

  public void assertThatNotesDetailsIsUpdatedAsFollows(
      String code, DataTable noteDetailsDataTable) {
    Map<String, String> noteDetailsDataMap =
        noteDetailsDataTable.asMaps(String.class, String.class).get(0);

    Object[] noteDetailsDataObject =
        (Object[])
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedIObNotesReceivableNoteDetails",
                constructNoteDetailsDataQueryParams(code, noteDetailsDataMap));

    assertNotNull("noteDetailsDataObject does not exist", noteDetailsDataObject);
    assertThatActualDateEqualsExpectedAndIfEmptyAssertNull(
        noteDetailsDataMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
        noteDetailsDataObject[1]);
  }

  private Object[] constructNoteDetailsDataQueryParams(
      String code, Map<String, String> noteDetailsDataMap) {
    return new Object[] {
      noteDetailsDataMap.get(IAccountingFeatureFileCommonKeys.NOTE_NUMBER),
      noteDetailsDataMap.get(IAccountingFeatureFileCommonKeys.NOTE_BANK),
      noteDetailsDataMap.get(IAccountingFeatureFileCommonKeys.DEPOT),
      code
    };
  }
}
