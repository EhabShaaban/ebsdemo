package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesActivateTestUtils;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class DObMonetaryNotesActivateHPStep extends SpringBootRunner
    implements En, InitializingBean {

  public static final String SCENARIO_NAME = "Activate NotesReceivable,";

  private DObMonetaryNotesActivateTestUtils utils;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;

  public DObMonetaryNotesActivateHPStep() {

    Then(
        "^NotesReceivable with Code \"([^\"]*)\" is updated as follows:$",
        (String NRCode, DataTable postedNRDataTable) -> {
          utils.assertThatNRUpdated(NRCode, postedNRDataTable);
        });

    Given(
        "^Last JournalEntry Code is \"([^\"]*)\"$",
        (String lastJournalEntryCode) -> {
          utils.createLastEntityCode(DObJournalEntry.class.getSimpleName(), lastJournalEntryCode);
        });

    When(
        "^\"([^\"]*)\" Activates NotesReceivable with code \"([^\"]*)\" and JournalEntryDate \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String NRCode, String journalEntryDate, String PostingDateTime) -> {
          utils.freeze(PostingDateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendPutNotesReceivable(cookie, NRCode, journalEntryDate);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^ActivationDetails in NotesReceivable with Code \"([^\"]*)\" is updated as follows:$",
        (String NRCode, DataTable postedNRDataTable) -> {
          utils.assertThatTheFollowingNRActivationDetailsDataExist(NRCode, postedNRDataTable);
        });

    Then(
        "^AccountingDetails in NotesReceivable with Code \"([^\"]*)\" is updated as follows:$",
        (String notesReceivableCode, DataTable accountingDetailsDataTable) -> {
          utils.assertAccountingDetailsAreUpdated(notesReceivableCode, accountingDetailsDataTable);
        });
    And(
        "^Remaining of NotesReceivable with code \"([^\"]*)\" is updated as follows \"([^\"]*)\"$",
        (String notesReceivableCode, String remaining) -> {
          utils.assertThatNRRemainingMatchesExpected(notesReceivableCode, remaining);
        });
    And(
        "^Remaining of SalesOrder with code \"([^\"]*)\" is updated as follows \"([^\"]*)\"$",
        (String salesOrderCode, String remaining) -> {
          utils.assertThatSalesOrderRemainingMatchesExpected(salesOrderCode, remaining);
        });
    And(
        "^Remaining of SalesInvoice with code \"([^\"]*)\" is updated as follows \"([^\"]*)\"$",
        (String salesInvoiceCode, String remaining) -> {
          utils.assertThatSalesInvoiceRemainingMatchesExpected(salesInvoiceCode, remaining);
        });

    Then("^a new JournalEntry is created as follows:$", (DataTable journalEntryDataTable) -> {
      journalEntryViewAllTestUtils
        .assertThatJournalEntriesExistWithCompanyAndRefDocumentCodeName(journalEntryDataTable);
    });

    Then("^And the following new JournalEntryDetails are created for JournalEntry with code \"([^\"]*)\":$",
      (String journalEntryCode, DataTable journalEntryItemsDataTable) -> {
        utils.assertThatJournalEntryItemsIsCreated(journalEntryCode,
          journalEntryItemsDataTable);
      });

      Then(
              "^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
              (String missingFields, String username) -> {
                  Response response = userActionsTestUtils.getUserResponse(username);
                  utils.assertThatResponseHasMissingFields(response, missingFields);
              });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObMonetaryNotesActivateTestUtils(properties, entityManagerDatabaseConnector);
    journalEntryViewAllTestUtils = new DObJournalEntryViewAllTestUtils(properties, entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      utils.unfreeze();
    }
  }
}
