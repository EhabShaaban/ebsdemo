package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import cucumber.api.DataTable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObPaymentRequestCreateJournalEntryTestUtils extends DObPaymentRequestCommonTestUtils {

	public DObPaymentRequestCreateJournalEntryTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public static String getDbScriptsOneTimeExecution() {
		StringBuilder str = new StringBuilder();

		str.append("db-scripts/clearDataSqlScript.sql");
		str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
		str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
		str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
		str.append("," + "db-scripts/CObBankSqlScript.sql");
		str.append("," + "db-scripts/CObCurrencySqlScript.sql");
		str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
		str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
		str.append("," + "db-scripts/CObMeasureScript.sql");
		str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
		str.append(",").append("db-scripts/master-data/item/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
		str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/item/CObItem.sql");
		str.append("," + "db-scripts/master-data/item/MObItem.sql");
		str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
		str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
		str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
		str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
		str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
		str.append("," + "db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
		str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
		str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
		str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
		str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");

		return str.toString();
	}

	public void assertThatTheFollowingPaymentRequestsHasTheFollowingAmountAndCurrency(
					String paymentRequestCode, DataTable prCurrenciesDataTable) {
		Map<String, String> paymentDetailsMap = prCurrenciesDataTable
						.asMaps(String.class, String.class).get(0);
		IObPaymentRequestPaymentDetailsGeneralModel paymentDetailsGeneralModel = (IObPaymentRequestPaymentDetailsGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult(
										"getPaymentDetailsByCodeAndAmountAndCurrency",
										constructPaymentDetailsQueryParams(paymentRequestCode,
														paymentDetailsMap));
		Assert.assertNotNull(
						"Payment Details for PR with code " + paymentRequestCode + " doesn't exist",
						paymentDetailsGeneralModel);
	}

	private Object[] constructPaymentDetailsQueryParams(String paymentRequestCode,
					Map<String, String> paymentDetailsMap) {
		return new Object[] { paymentRequestCode,
						new BigDecimal(paymentDetailsMap
										.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
						paymentDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY) };
	}

	public void assertLastJournalEntry(String journalEntryCode) {
		createLastEntityCode(DObJournalEntry.class.getSimpleName(),journalEntryCode);
	}
	public void assertVendorInvoiceExists(DataTable invoicesDataTable) {
		List<Map<String, String>> invoicesMaps = invoicesDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> invoicesMap : invoicesMaps) {
			assertVendorInvoiceExist(invoicesMap);
		}
	}

	private void assertVendorInvoiceExist(Map<String, String> invoicesMap) {
		DObVendorInvoiceGeneralModel invoiceGeneralModel = (DObVendorInvoiceGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getPostedVendorInvoices",
										constructVendorInvoiceQueryParams(invoicesMap));
		Assert.assertNotNull("Vendor Invoice with code "
						+ invoicesMap.get(IFeatureFileCommonKeys.CODE) + " does not exist!",
						invoiceGeneralModel);
	}

	private Object[] constructVendorInvoiceQueryParams(Map<String, String> invoicesMap) {
		return new Object[] { invoicesMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO),
						invoicesMap.get(IFeatureFileCommonKeys.CODE),
						'%' + invoicesMap.get(IFeatureFileCommonKeys.STATE) + '%',
						invoicesMap.get(IFeatureFileCommonKeys.VENDOR),
						new BigDecimal(invoicesMap
										.get(IAccountingFeatureFileCommonKeys.AMOUNT_REMAINING)),
						invoicesMap.get(IAccountingFeatureFileCommonKeys.COMPANY) };
	}

	public void assertJournalEntriesExist(DataTable journalEntryDataTable) {
		List<Map<String, String>> journalEntriesMaps = journalEntryDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> journalEntryMap : journalEntriesMaps) {
			assertJournalEntryExists(journalEntryMap);
		}
	}

	private void assertJournalEntryExists(Map<String, String> journalEntryMap) {
		DObJournalEntryGeneralModel journalEntryGeneralModel = (DObJournalEntryGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getJournalEntryByCodeAndReference",
										constructJournalEntryCodeAndRefQueryParams(
														journalEntryMap));
		Assert.assertNotNull("Journal Entry with code "
						+ journalEntryMap.get(IFeatureFileCommonKeys.CODE) + " does not exist!",
						journalEntryGeneralModel);
	}

	private Object[] constructJournalEntryCodeAndRefQueryParams(
					Map<String, String> journalEntryMap) {
		return new Object[] { journalEntryMap.get(IFeatureFileCommonKeys.CODE),
						journalEntryMap.get(IAccountingFeatureFileCommonKeys.REFERENCE) };
	}

	public void assertJournalEntryDetailsExist(DataTable journalDetailDataTable) {
		List<Map<String, String>> journalDetailsMaps = journalDetailDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> journalDetailMap : journalDetailsMaps) {
			assertJournalEntryDetailExists(journalDetailMap);
		}
	}

	private void assertJournalEntryDetailExists(Map<String, String> journalDetailMap) {
		IObJournalEntryItemGeneralModel journalEntryItemGeneralModel = (IObJournalEntryItemGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getJournalEntryDetail",
										constructJournalDetailQueryParams(journalDetailMap));
		Assert.assertNotNull("Journal Detail for JE with code "
						+ journalDetailMap.get(IFeatureFileCommonKeys.CODE) + " does not exist!",
						journalEntryItemGeneralModel);
	}

	private Object[] constructJournalDetailQueryParams(Map<String, String> journalDetailMap) {
		return new Object[] { journalDetailMap.get(IFeatureFileCommonKeys.CODE),
						new BigDecimal(journalDetailMap
										.get(IAccountingFeatureFileCommonKeys.CREDIT)),
						new BigDecimal(journalDetailMap
										.get(IAccountingFeatureFileCommonKeys.DEBIT)),
						new BigDecimal(journalDetailMap
										.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE)),
						journalDetailMap.get(
										IAccountingFeatureFileCommonKeys.PR_LATEST_EXCHANGE_RATE_CODE) };
	}

	public void assertThereIsNoRealizedDifferencesOnExchangeInJournalEntry(
					String journalEntryCode) {
		IObJournalEntryItemGeneralModel journalEntryItemGeneralModel = (IObJournalEntryItemGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult(
										"checkThereIsNoDifferencesOnExchangeInJournalDetail",
										journalEntryCode);
		Assert.assertNull("There is Differences on Exchange in Journal Detail where JE code is "
						+ journalEntryCode, journalEntryItemGeneralModel);
	}
}
