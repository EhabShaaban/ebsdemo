package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.Map;

public class DObCollectionViewGeneralDataTestUtils extends DObCollectionTestUtils{
    public DObCollectionViewGeneralDataTestUtils(Map<String, Object> properties) {
        super(properties);
    }
    public void assertCorrectValuesAreDisplayedInGeneralData(
            Response response, DataTable generalDataTable) throws Exception {
        Map<String, String> generalDataExpected =
                generalDataTable.asMaps(String.class, String.class).get(0);
        Map<String, Object> generalDataActual = getMapFromResponse(response);

        MapAssertion mapAssertion = new MapAssertion(generalDataExpected, generalDataActual);
        assertActualAndExpectedDataFromResponse(mapAssertion);
    }

    private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
        mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.C_CODE, IDObCollectionGeneralModel.USER_CODE);

        mapAssertion.assertCodeAndLocalizedNameValue(
                IFeatureFileCommonKeys.TYPE,
                IDObCollectionGeneralModel.REF_DOCUMENT_TYPE_CODE,
                IDObCollectionGeneralModel.REF_DOCUMENT_TYPE_NAME);

        mapAssertion.assertValue(
                IFeatureFileCommonKeys.CREATED_BY, IDObCollectionGeneralModel.CREATION_INFO);

        mapAssertion.assertValueContains(
                IFeatureFileCommonKeys.STATE, IDObCollectionGeneralModel.CURRENT_STATES);

        mapAssertion.assertDateTime(
                IFeatureFileCommonKeys.CREATION_DATE, IDObCollectionGeneralModel.CREATION_DATE);

        mapAssertion.assertValue(IFeatureFileCommonKeys.DOCUMENT_OWNER, IDObCollectionGeneralModel.DOCUMENT_OWNER);

    }

}
