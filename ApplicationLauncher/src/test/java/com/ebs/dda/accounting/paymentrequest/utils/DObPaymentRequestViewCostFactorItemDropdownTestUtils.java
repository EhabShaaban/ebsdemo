package com.ebs.dda.accounting.paymentrequest.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.UserActionsTestUtils;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPaymentRequestViewCostFactorItemDropdownTestUtils extends DObPaymentRequestCommonTestUtils {


  public DObPaymentRequestViewCostFactorItemDropdownTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }


  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    return str.toString();
  }

  public void requestToreadCostFactorItems(String username, String paymentRequestCode,
      UserActionsTestUtils userActionsTestUtils) {
    Cookie cookie = userActionsTestUtils.getUserCookie(username);
    String restUrl = IDObPaymentRequestRestURLs.PAYMENT_REQUEST_QUERY + "/" + paymentRequestCode
        + IDObPaymentRequestRestURLs.COST_FACTOR_ITEMS_URL;
    Response response = sendGETRequest(cookie, restUrl);
    userActionsTestUtils.setUserResponse(username, response);
  }

  public void assertThatCostFactorItemsAreRetrieved(Response userResponse, DataTable itemsDT) {
    List<Map<String, String>> expectedItemsList = itemsDT.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItemsList = getListOfMapsFromResponse(userResponse);
    int index = 0;
    for (Map<String, String> expectedItem : expectedItemsList) {
      HashMap<String, Object> actualItem = actualItemsList.get(index++);

      String expectedMarketName =
          expectedItem.get(IAccountingFeatureFileCommonKeys.COST_FACTOR_ITEM);
      Assert.assertEquals(expectedMarketName, actualItem.get(IMObItem.MARKET_NAME));
    }
  }

  public void assetThatTotalNumberOfRetrievedItemsIs(Response response, Integer numOfRecords) {
    List<HashMap<String, Object>> actualItems = getListOfMapsFromResponse(response);
    Assert.assertEquals(numOfRecords.intValue(), actualItems.size());
  }


}
