package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceViewAllTestUtils;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceViewAllStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceViewAllTestUtils salesInvoiceViewAllTestUtils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  public DObSalesInvoiceViewAllStep() {
    Given(
        "^the following PurchaseUnits exist:$",
        (DataTable purchaseUnitsDataTable) -> {
          enterpriseTestUtils.assertThatPurchaseUnitsExist(purchaseUnitsDataTable);
        });

    Given(
        "^the following CollectionResponsibles exist:$",
        (DataTable collectionResponsiblesDataTable) -> {
          salesInvoiceViewAllTestUtils.assertThatCollectionResponsiblesExist(
              collectionResponsiblesDataTable);
        });

    Given(
        "^the following are ALL existing SalesInvoiceTypes:$",
        (DataTable salesInvoiceTypesDataTable) -> {
          salesInvoiceViewAllTestUtils.assertThatSalesInvoiceTypesExist(salesInvoiceTypesDataTable);
        });

    Given(
        "^the following View All SalesInvoices exist:$",
        (DataTable salesInvoicesDataTable) -> {
          salesInvoiceViewAllTestUtils.assertThatSalesInvoicesExist(salesInvoicesDataTable);
        });


      Given("^the following ViewAll data for SalesInvoices exist And the total number of existing SalesInvoices is (\\d+)$", (Integer recordsNumber, DataTable salesInvoicesDataTable) -> {
          salesInvoiceViewAllTestUtils.assertThatSalesInvoicesExistWithTheFollowingNumberOfRecords(salesInvoicesDataTable,recordsNumber);
      });
      When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with no filter applied (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredData(
                  userCookie, IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesInvoices will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesInvoicesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceViewAllTestUtils.assertResponseSuccessStatus(response);
          salesInvoiceViewAllTestUtils.assertThatSalesInvoiceMatches(
              response, salesInvoicesDataTable);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          salesInvoiceViewAllTestUtils.assertTotalNumberOfRecordsAreCorrect(
              response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with filter applied on InvoiceCode which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObSalesInvoiceGeneralModel.INVOICE_CODE, filteringValue);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with filter applied on State which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getContainsFilterField(
                  IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME, filteringValue);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with filter applied on CreationDate which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObSalesInvoiceGeneralModel.CREATION_DATE,
                  String.valueOf(
                      salesInvoiceViewAllTestUtils.getDateTimeInMilliSecondsFormat(
                          filteringValue)));
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with filter applied on SalesOrder which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String filteringValue, Integer recordsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObSalesInvoiceGeneralModel.SALES_ORDER, filteringValue);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredData(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) " +
                "of SalesInvoices with filter applied on InvoiceTypeCode which equals \"([^\"]*)\" with " +
                "(\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getEqualsFilterField(
                  IDObSalesInvoiceGeneralModel.INVOICE_TYPE, filteringValue);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with filter applied on BusinessUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObSalesInvoiceGeneralModel.PURCHASE_UNIT, filteringValue);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with filter applied on Company which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObSalesInvoiceGeneralModel.COMPANY, filteringValue);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of SalesInvoices with filter applied on Customer which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String filteringValue,
            Integer recordsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filterString =
              salesInvoiceViewAllTestUtils.getContainsFilterField(
                  IDObSalesInvoiceGeneralModel.CUSTOMER_CODE_NAME, filteringValue);
          Response response =
              salesInvoiceViewAllTestUtils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObSalesInvoiceRestUrls.VIEW_ALL_INVOICES,
                  pageNumber,
                  recordsNumber,
                  filterString,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    salesInvoiceViewAllTestUtils = new DObSalesInvoiceViewAllTestUtils(properties);
    salesInvoiceViewAllTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "View All SalesInvoices")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {

        databaseConnector.executeSQLScript(
            salesInvoiceViewAllTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View All SalesInvoices")) {
      databaseConnector.closeConnection();
    }
  }
}
