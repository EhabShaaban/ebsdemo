package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.google.gson.JsonObject;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class DObSalesInvoiceMarkAsReadyForDeliveringTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceMarkAsReadyForDeliveringTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append(super.getDbScriptsOneTimeExecution());
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceItems.sql");
    return str.toString();
  }
  
  public Response markSalesInvoiceAsReadyForDelivering(Cookie userCookie, String salesInvoiceCode) {
    return sendPUTRequest(
        userCookie,
        IDObSalesInvoiceRestUrls.COMMAND_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.READY_FOR_DELIVERING_SALES_INVOICE,
        new JsonObject());
  }
}
