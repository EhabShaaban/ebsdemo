package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewCollectionDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionViewCollectionDetailsStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecutedOnce = false;
    private DObCollectionViewCollectionDetailsTestUtils utils;

    public DObCollectionViewCollectionDetailsStep() {
        When(
                "^\"([^\"]*)\" requests to view Collection Details section of Collection with code \"([^\"]*)\"$",
                (String userName, String collectionCode) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = utils.getCollectionDetailsForCollection(userCookie, collectionCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following values of Collection Details section for Collection with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
                (String collectionCode, String userName, DataTable collectionDetailsDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertResponseSuccessStatus(response);
                    utils.assertCorrectValuesAreDisplayedInCollectionDetails(
                            response, collectionDetailsDataTable);
                });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new DObCollectionViewCollectionDetailsTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {

        if (contains(scenario.getName(), "View Collection Details section -")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getConciseDbScripts());
                hasBeenExecutedOnce = true;
            }
        }
    }

    @After
    public void after(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View Collection Details section -")) {
            databaseConnector.closeConnection();
        }
    }
    @After("@ResetData")
    public void resetData(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View Collection Details section -")) {
            databaseConnector.createConnection();
            databaseConnector.executeSQLScript(utils.clearIsertedDataFromFeatureFile());
            databaseConnector.closeConnection();
        }
    }
}
