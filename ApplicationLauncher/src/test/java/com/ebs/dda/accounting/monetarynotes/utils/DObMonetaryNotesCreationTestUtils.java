package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.accountingnotes.IAccountingNoteCreateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesCreateValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Map;

public class DObMonetaryNotesCreationTestUtils extends DObMonetaryNotesCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Create]";

  public DObMonetaryNotesCreationTestUtils(Map<String, Object> properties , EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties,entityManagerDatabaseConnector);
  }

  public void assertNotesReceivablesExistsWithCode(String notesReceivableCode) {
    assertThatLastEntityCodeIs("DObNotesReceivable", notesReceivableCode);
  }

  public JsonObject preparNotesReceivableJsonObject(DataTable notesReceivableDataTable) {
    Map<String, String> notesReceivableMap =
        notesReceivableDataTable.asMaps(String.class, String.class).get(0);
    JsonObject notesReceivableJsonObject = new JsonObject();

    addValueToJsonObject(
        notesReceivableJsonObject,
        IDObNotesReceivablesCreateValueObject.NOTE_FORM,
        notesReceivableMap.get(IAccountingFeatureFileCommonKeys.NOTE_FORM));
    addValueToJsonObject(
        notesReceivableJsonObject,
        IDObNotesReceivablesCreateValueObject.TYPE,
        notesReceivableMap.get(IFeatureFileCommonKeys.TYPE));
    addValueToJsonObject(
        notesReceivableJsonObject,
        IDObNotesReceivablesCreateValueObject.BUSINESS_UNIT_CODE,
        notesReceivableMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT));
    addValueToJsonObject(
        notesReceivableJsonObject,
        IDObNotesReceivablesCreateValueObject.COMPANY_CODE,
        notesReceivableMap.get(IAccountingFeatureFileCommonKeys.COMPANY));
    addValueToJsonObject(
        notesReceivableJsonObject,
        IDObNotesReceivablesCreateValueObject.BUSINESS_PARTNER_CODE,
        notesReceivableMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER));
    addAmountJsonObject(
      notesReceivableJsonObject,
      notesReceivableMap.get(IAccountingFeatureFileCommonKeys.AMOUNT));
    addValueToJsonObject(
        notesReceivableJsonObject,
        IDObNotesReceivablesCreateValueObject.CURRENCY_ISO,
        notesReceivableMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO));
    try {
      notesReceivableJsonObject.addProperty(
          IDObNotesReceivablesCreateValueObject.DOCUMENT_OWNER_ID,
          Long.valueOf(notesReceivableMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID)));
    } catch (NumberFormatException exception) {
      notesReceivableJsonObject.addProperty(
          IAccountingNoteCreateValueObject.DOCUMENT_OWNER_ID,
          notesReceivableMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID));
    }
    return notesReceivableJsonObject;
  }

  private void addAmountJsonObject(JsonObject jsonObject, String amountAsStr) {
    BigDecimal amountAsNumber = null;
    if (amountAsStr != null && !amountAsStr.isEmpty()){
      amountAsNumber = new BigDecimal(amountAsStr);
    }
    jsonObject
      .addProperty(IDObNotesReceivablesCreateValueObject.AMOUNT, amountAsNumber);
  }

  public void assertThatNotesReceivableIsCreatedSuccessfully(DataTable NotesReceivableDataTable) {
    Map<String, String> notesReceivableMap =
        NotesReceivableDataTable.asMaps(String.class, String.class).get(0);
    Object notesReceivable =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getDObNotesReceivableGeneralData",
            constructNotesReceivableQueryParams(notesReceivableMap));
    Assert.assertNotNull(ASSERTION_MSG + " General data doesn't exists", notesReceivable);
  }

  private Object[] constructNotesReceivableQueryParams(Map<String, String> notesReceivableMap) {
    return new Object[] {
      notesReceivableMap.get(IFeatureFileCommonKeys.CODE),
      '%' + notesReceivableMap.get(IFeatureFileCommonKeys.STATE) + '%',
      notesReceivableMap.get(IFeatureFileCommonKeys.CREATED_BY),
      notesReceivableMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      notesReceivableMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      notesReceivableMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      notesReceivableMap.get(IFeatureFileCommonKeys.TYPE),
      notesReceivableMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  public void assertThatNotesReceivableCompanyIsCreatedSuccessfully(
      String notesReceivableCode, DataTable notesReceivableCompanyDataTable) {

    Map<String, String> notesReceivableCompanyMap =
        notesReceivableCompanyDataTable.asMaps(String.class, String.class).get(0);
    Object notesReceivable =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getDObNotesReceivableCompanyData",
            constructCreatedNotesReceivableCompanyDataQueryParams(
                notesReceivableCode, notesReceivableCompanyMap));
    Assert.assertNotNull(ASSERTION_MSG + " Company data doesn't exists", notesReceivable);
  }

  private Object[] constructCreatedNotesReceivableCompanyDataQueryParams(
      String notesReceivableCode, Map<String, String> notesReceivableCompanyMap) {
    return new Object[] {
      notesReceivableCode,
      notesReceivableCompanyMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      notesReceivableCompanyMap.get(IFeatureFileCommonKeys.COMPANY)
    };
  }

  public void assertThatNotesReceivableDetailsIsCreatedSuccessfully(
      String notesReceivableCode, DataTable notesReceivableDetailsDataTable) {

    Map<String, String> notesReceivableDetailsMap =
        notesReceivableDetailsDataTable.asMaps(String.class, String.class).get(0);

    Object[] notesReceivable =
        (Object[])
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getDObNotesReceivableDetailsData",
                constructCreatedNotesReceivableDetailsDataQueryParams(
                    notesReceivableCode, notesReceivableDetailsMap));
    Assert.assertNotNull(ASSERTION_MSG + " Details data doesn't exists", notesReceivable);
    Assert.assertNull("DueDate is not correct", notesReceivable[1]);
  }

  private Object[] constructCreatedNotesReceivableDetailsDataQueryParams(
      String notesReceivableCode, Map<String, String> notesReceivableDetailsMap) {
    return new Object[] {
      notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.NOTE_NUMBER),
      notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.NOTE_BANK),
      notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.DEPOT),
      notesReceivableCode,
      notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.NOTE_FORM),
      notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
      notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO),
      Double.parseDouble(notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.AMOUNT)),
      Double.parseDouble(notesReceivableDetailsMap.get(IAccountingFeatureFileCommonKeys.REMAINING))
    };
  }
}
