package com.ebs.dda.accounting.paymentrequest;

import org.springframework.beans.factory.InitializingBean;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestEmployeeDropDownTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPaymentRequestEmployeeDropDownStep extends SpringBootRunner
				implements En, InitializingBean {

	public static final String SCENARIO_NAME = "Read all Employees";
	private DObPaymentRequestEmployeeDropDownTestUtils utils;

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		utils = new DObPaymentRequestEmployeeDropDownTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
	}

	public DObPaymentRequestEmployeeDropDownStep() {
		When("^\"([^\"]*)\" requests to read all Employees$", (String userName) -> {
			utils.requestToReadEmployees(userName, userActionsTestUtils);
		});

		Then("^the following values of Employees are displayed to \"([^\"]*)\":$",
						(String userName, DataTable employeesDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertThatEmployeesAreDisplayedToUser(response,
											employeesDataTable);
						});

		Then("^total number of Employees returned to \"([^\"]*)\" is equal to (\\d+)$",
						(String userName, Integer employeesNumbers) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertTotalNumberOfRecordsEqualResponseSize(response,
											employeesNumbers);
						});

	}

	@Before
	public void before(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			databaseConnector.executeSQLScript(utils.getDbScripts());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
