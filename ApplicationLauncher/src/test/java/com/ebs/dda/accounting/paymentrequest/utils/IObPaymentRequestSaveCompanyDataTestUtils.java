package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class IObPaymentRequestSaveCompanyDataTestUtils
				extends DObPaymentRequestPaymentDetailsTestUtils {

	public IObPaymentRequestSaveCompanyDataTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public static String getPaymentRequestDbScripts() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
		str.append(",").append("db-scripts/accounting/payment-request/payment-request.sql");
		str.append(",").append(
						"db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
		str.append(",").append(
						"db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
		str.append(",").append(
						"db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
		str.append(",").append(
						"db-scripts/accounting/payment-request/iob-payment-request-payment-details-view-all.sql");
		str.append(",").append(
						"db-scripts/accounting/payment-request/iob-payment-request-accounting-details.sql");
		return str.toString();
	}

	public void assertThatCompanyDataIsUpdated(String paymentRequestCode,
					DataTable companyDataTable) {
		List<Map<String, String>> prCompanyDataMaps = companyDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> prCompanyDataMap : prCompanyDataMaps) {
			Object prCompanyData = entityManagerDatabaseConnector
							.executeNativeNamedQuerySingleResult("getPRUpdatedCompanyData",
											constructPaymentDetailsQueryParams(paymentRequestCode,
															prCompanyDataMap));
			Assert.assertNotNull(
							"PR Company Data with code " + paymentRequestCode + " doesn't exist",
							prCompanyData);
		}
	}

	private Object[] constructPaymentDetailsQueryParams(String paymentRequestCode,
					Map<String, String> paymentRequestMap) {
		return new Object[]{paymentRequestMap.get(IAccountingFeatureFileCommonKeys.COMPANY_CODE),
				paymentRequestCode,
				paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
				paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE)};
	}
}
