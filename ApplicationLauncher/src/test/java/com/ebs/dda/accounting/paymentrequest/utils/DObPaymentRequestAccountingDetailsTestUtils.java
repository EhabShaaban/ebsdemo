package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestAccountingDetailsTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatTheFollowingPRAccountingDetailsExist(DataTable AccountingDetailsTable) {
    List<Map<String, String>> prAccountingDetailsListMap =
        AccountingDetailsTable.asMaps(String.class, String.class);
    for (Map<String, String> prAccountingDetailsMap : prAccountingDetailsListMap) {
      String paymentRequestCode =
          prAccountingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_CODE);
      retrieveAccountingDetailsData(paymentRequestCode, prAccountingDetailsMap);
    }
  }
}
