package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesFormsDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObMonetaryNotesFormsDropdownStep extends SpringBootRunner implements En {

  private DObMonetaryNotesFormsDropdownTestUtils utils;

  public DObMonetaryNotesFormsDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all MonetaryNote Forms$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllMonetaryNoteForms(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following MonetaryNote Forms values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable formsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatMonetaryNoteFormsResponseIsCorrect(formsDataTable, response);
        });

    Then(
        "^total number of MonetaryNote Forms returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer formsCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfMonetaryNoteFormsEquals(formsCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObMonetaryNotesFormsDropdownTestUtils(getProperties(),entityManagerDatabaseConnector);
  }
}
