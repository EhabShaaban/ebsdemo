package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.UserActionsTestUtils;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.masterdata.company.IIObCompanyTreasuriesGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestTreasuriesTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestTreasuriesTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append("," + "db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/master-data/treasury/CObTreasury.sql");
    str.append("," + "db-scripts/master-data/treasury/IObCompanyTreasuries.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
    str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo.sql");
    return str.toString();
  }

  public void requestToReadTreasuries(
      String userName,
      String documentCode,
      String documentType,
      UserActionsTestUtils userActionsTestUtils) {
    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
    String restUrl =
        IDObPaymentRequestRestURLs.PAYMENT_REQUEST_QUERY
            + "/"
            + documentCode
            + "/"
            + documentType
            + IDObPaymentRequestRestURLs.TREASURIES_URL;
    Response response = sendGETRequest(cookie, restUrl);
    userActionsTestUtils.setUserResponse(userName, response);
  }

  public void assertThatTreasuriesAreDisplayedToUser(Response response, DataTable treasuries)
      throws Exception {
    List<Map<String, String>> expectedTreasuries = treasuries.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualTreasuries = getListOfMapsFromResponse(response);
    for (int index = 0; index < actualTreasuries.size(); index++) {
      Map<String, String> expectedTreasury = expectedTreasuries.get(index);
      HashMap<String, Object> actualTreasury = actualTreasuries.get(index);
      MapAssertion mapAssertion = new MapAssertion(expectedTreasury, actualTreasury);
      mapAssertion.assertCodeAndLocalizedNameValue(
          IFeatureFileCommonKeys.TREASURY,
          IIObCompanyTreasuriesGeneralModel.TREASURY_CODE,
          IIObCompanyTreasuriesGeneralModel.TREASURY_NAME);
    }
  }

  public void requestToReadTreasuriesForCompany(
      String userName, String companyCode, UserActionsTestUtils userActionsTestUtils) {
    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
    String restUrl =
        IDObCollectionRestURLs.GET_URL + companyCode + IDObCollectionRestURLs.TREASURIES_URL;
    Response response = sendGETRequest(cookie, restUrl);
    userActionsTestUtils.setUserResponse(userName, response);
  }
}
