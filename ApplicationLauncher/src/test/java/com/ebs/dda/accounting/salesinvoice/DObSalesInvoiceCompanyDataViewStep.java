package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceCompanyDataViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceCompanyDataViewStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceCompanyDataViewTestUtils utils;

  public DObSalesInvoiceCompanyDataViewStep() {
    Given(
        "^the following CompanyData SalesInvoices exist:$",
        (DataTable salesInvoicesDataTable) -> {
          utils.assertThatSalesInvoicesExist(salesInvoicesDataTable);
        });

    Given(
        "^the following CompanyData for SalesInvoices exists:$",
        (DataTable dataTable) -> {
          utils.assertThatTheFollowingCompanyExistsForSalesInvoice(dataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view CompanyData section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          StringBuilder readUrl = new StringBuilder();
          readUrl.append(IDObSalesInvoiceRestUrls.GET_URL);
          readUrl.append(salesInvoiceCode);
          readUrl.append(IDObSalesInvoiceRestUrls.COMPANY_DATA);
          Response response = utils.sendGETRequest(cookie, readUrl.toString());
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of CompanyData section for SalesInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String salesInvoiceCode, String userName, DataTable taxValuesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayedInCompanyData(response, taxValuesDataTable);
        });

  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObSalesInvoiceCompanyDataViewTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "View CompanyData")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View CompanyData")) {
      databaseConnector.closeConnection();
    }
  }
}
