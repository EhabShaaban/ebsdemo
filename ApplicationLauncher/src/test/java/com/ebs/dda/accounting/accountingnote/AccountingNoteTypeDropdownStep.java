package com.ebs.dda.accounting.accountingnote;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingnote.utils.AccountingNoteTypeDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class AccountingNoteTypeDropdownStep extends SpringBootRunner implements En {

  private AccountingNoteTypeDropdownTestUtils utils;

  public AccountingNoteTypeDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all AccountingNote NoteTypes$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllNoteTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following AccountingNote NoteTypes values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable noteTypesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatNoteTypesResponseIsCorrect(noteTypesDataTable, response);
        });

    Then(
        "^total number of AccountingNote NoteTypes returned to \"([^\"]*)\" is equal to (\\d+)$",
            (String userName, Integer noteTypesCount) -> {
                Response response = userActionsTestUtils.getUserResponse(userName);
                utils.assertThatTotalNumberOfNoteTypesEquals(noteTypesCount, response);
            });

    When(
        "^\"([^\"]*)\" requests to read all AccountingNote Types for \"([^\"]*)\" NoteType in a dropdown$",
        (String userName, String noteType) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllTypesForInputNoteType(cookie, noteType);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following AccountingNote Types values will be presented to \"([^\"]*)\" in a dropdown:$",
        (String userName, DataTable typesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTypesResponseIsCorrect(typesDataTable, response);
        });

    Then(
        "^total number of AccountingNote Types returned to \"([^\"]*)\" is equal to (\\d+)$",
            (String userName, Integer typesCount) -> {
                Response response = userActionsTestUtils.getUserResponse(userName);
                utils.assertThatTotalNumberOfTypesEquals(typesCount, response);
            });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new AccountingNoteTypeDropdownTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }
}
