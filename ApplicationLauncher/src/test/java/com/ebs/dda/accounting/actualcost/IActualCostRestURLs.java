package com.ebs.dda.accounting.actualcost;

public interface IActualCostRestURLs {

	String ACTUAL_COST_COMMAND = "/command/accounting/actualcost";
	String CALCULATE_ACTUAL_COST = "/calculate";
}
