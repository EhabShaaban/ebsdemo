package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionSaveCollectionDetailsTestUtils;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObCollectionSaveCollectionDetailsStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObCollectionSaveCollectionDetailsTestUtils utils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  public DObCollectionSaveCollectionDetailsStep() {
    Given(
        "^the Company \"([^\"]*)\" has the following Treasuries:$",
        (String company, DataTable companyTreasuriesDataTable) -> {
          enterpriseTestUtils.assertThatCompanyTreasuriesExist(company, companyTreasuriesDataTable);
        });
    When(
        "^\"([^\"]*)\" saves CollectionDetails section of Collection with Code \"([^\"]*)\" with CollectionMethod \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String collectionCode,
            String collectionMethod,
            String savingDate,
            DataTable collectionDetailsDataTable) -> {
          utils.freeze(savingDate);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveCollectionDetails(
                  collectionCode, collectionMethod, collectionDetailsDataTable, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^CollectionDetails section of Collection with Code \"([^\"]*)\" is updated as follows:$",
        (String collectionCode, DataTable collectionDetailsDataTable) -> {
          utils.assertCollectionDetailsWasUpdated(collectionCode, collectionDetailsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObCollectionSaveCollectionDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    enterpriseTestUtils.setProperties(getProperties());
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save CollectionDetails section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getConciseDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save CollectionDetails section")) {
      databaseConnector.closeConnection();
    }
  }
}
