package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObVendorInvoiceViewPurchaseOrderDropDownTestUtils
        extends DObVendorInvoiceTestUtils {

    public DObVendorInvoiceViewPurchaseOrderDropDownTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        super(properties);
        setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public String getDbScriptsOneTimeExecution() {
        StringBuilder str = new StringBuilder();
        str.append("db-scripts/PO_View_VendorData_Clear.sql");
        str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
        return str.toString();
    }

    public Response readPurchaseOrderVendor(Cookie userCookie, String purchaseOrderCode) {
        return sendGETRequest(userCookie, getViewVendorUrl(purchaseOrderCode));
    }

    private String getViewVendorUrl(String purchaseOrderCode) {
        return IPurchaseOrderRestUrls.QUERY_PURCHASE_ORDER
                + purchaseOrderCode
                + IPurchaseOrderRestUrls.VIEW_VENDOR;
    }

    public void assertThatPurchaseOrdersDataResponseAreCorrect(
             Response response, DataTable purchaseOrdersDataTable) throws Exception {
        List<HashMap<String, Object>> actualPurchaseOrderMap = getListOfMapsFromResponse(response);
        List<Map<String, String>> expectedPurchaseOrderMap = purchaseOrdersDataTable.asMaps(String.class, String.class);
        for (int i = 0; i < actualPurchaseOrderMap.size(); i++) {
            assertThatExpectedPOsMatcheActualOnes(
                    expectedPurchaseOrderMap.get(i), actualPurchaseOrderMap.get(i));
        }
    }

    private void assertThatExpectedPOsMatcheActualOnes
            (Map<String, String> expectedPurchaseOrderMap, HashMap<String, Object> actualPurchaseOrderMap) {

        MapAssertion mapAssertion = new MapAssertion(expectedPurchaseOrderMap, actualPurchaseOrderMap);
        mapAssertion.assertValue(
                IFeatureFileCommonKeys.CODE, IDObPurchaseOrderGeneralModel.CODE);
        mapAssertion.assertValueContains(
                IFeatureFileCommonKeys.STATE, IDObPurchaseOrderGeneralModel.CURRENT_STATES);
        mapAssertion.assertValueContains(
                IFeatureFileCommonKeys.TYPE, IDObPurchaseOrderGeneralModel.OBJECT_TYPE_CODE);
    }

    public void assertThatTotalNumberOfReturnedPurchaseOrdersIsCorrect(
            Response response, Integer expectedNumberOfPurchaseOrders) {
        List<HashMap<String, Object>> actualPurchaseOrders = getListOfMapsFromResponse(response);
        Integer actualNumberOfPurchaseOrders = actualPurchaseOrders.size();
        Assert.assertEquals(expectedNumberOfPurchaseOrders, actualNumberOfPurchaseOrders);
    }

}
