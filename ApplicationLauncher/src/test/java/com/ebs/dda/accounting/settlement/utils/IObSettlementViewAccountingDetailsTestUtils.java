package com.ebs.dda.accounting.settlement.utils;

import java.util.Map;
import com.ebs.dda.accounting.settlement.IDObSettlementRestURLs;

public class IObSettlementViewAccountingDetailsTestUtils extends DObSettlementsCommonTestUtils {

  public IObSettlementViewAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getViewAccountingDetailsUrl(String code) {
    return IDObSettlementRestURLs.BASE_URL + "/" + code
        + IDObSettlementRestURLs.VIEW_ACCOUNTING_DETAILS;
  }

}
