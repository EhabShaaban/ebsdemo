package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;

import java.util.Map;

public class IObMonetaryNoteEditNoteDetailsTestUtils extends DObMonetaryNotesCommonTestUtils {

  public IObMonetaryNoteEditNoteDetailsTestUtils(Map<String, Object> properties, EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties,entityManagerDatabaseConnector);
  }


  public String getLockNoteDetailsUrl(String code) {
    return IDObNotesReceivableRestURLs.BASE_COMMAND_URL
        + "/"
        + code
        + IDObNotesReceivableRestURLs.LOCK_NOTES_DETAILS_SECTION;
  }

  public String getUnLockNoteDetailsUrl(String code) {
    return IDObNotesReceivableRestURLs.BASE_COMMAND_URL
        + "/"
        + code
        + IDObNotesReceivableRestURLs.UNLOCK_NOTES_DETAILS_SECTION;
  }

}
