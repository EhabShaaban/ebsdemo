package com.ebs.dda.accounting.settlement;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.SpringBootRunner;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;

public class DObSettlementAllowedActionsObjectScreenStep extends SpringBootRunner
    implements En, InitializingBean {

  private static boolean hasBeenExecutedOnce = false;
  private CommonTestUtils utils;

  public DObSettlementAllowedActionsObjectScreenStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of Settlement with code \"([^\"]*)\"$",
        (String userName, String settlementCode) -> {
          String url = IDObSettlementRestURLs.AUTHORIZED_ACTIONS + '/' + settlementCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new CommonTestUtils();
    utils.setProperties(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Settlement Object Screen")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(clearSettlement());
    }
  }
}
