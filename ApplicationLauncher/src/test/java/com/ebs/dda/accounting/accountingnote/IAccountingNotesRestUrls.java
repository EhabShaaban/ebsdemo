package com.ebs.dda.accounting.accountingnote;

public interface IAccountingNotesRestUrls {
  String COMMAND = "/command/accounting/accountingnotes";
  String QUERY = "/services/accounting/accountingnotes";

  String VIEW_ALL = QUERY;
  String CREATE = COMMAND + "/";

  String READ_ALL_NOTE_TYPES = QUERY + "/notetypes";
  String READ_ALL_TYPES = QUERY + "/types/";
  String VIEW_CREDIT_NOTES = QUERY + "/creditNotes";
}
