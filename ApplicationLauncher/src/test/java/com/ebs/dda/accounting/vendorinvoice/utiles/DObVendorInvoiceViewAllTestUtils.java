package com.ebs.dda.accounting.vendorinvoice.utiles;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObVendorInvoiceViewAllTestUtils extends DObVendorInvoiceTestUtils {

  public DObVendorInvoiceViewAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialGroup.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");

    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    return str.toString();
  }

  public void assertThatInvoicesExist(DataTable invoices) {

    List<Map<String, String>> invoicesList = invoices.asMaps(String.class, String.class);
    for (Map<String, String> invoice : invoicesList) {

      String invoiceCode = invoice.get(IFeatureFileCommonKeys.CODE);
      String invoiceType = invoice.get(IFeatureFileCommonKeys.TYPE);
      String invoicePurchaseUnit = invoice.get(IAccountingFeatureFileCommonKeys.PURCHASE_UNIT);
      String invoiceVendor = invoice.get(IAccountingFeatureFileCommonKeys.VENDOR);
      String invoicePurchaseOrder = invoice.get(IAccountingFeatureFileCommonKeys.PURCHASE_ORDER);
      String invoiceNumber = invoice.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER);
      String invoiceState = "%" + invoice.get(IFeatureFileCommonKeys.STATE) + "%";

      DObVendorInvoiceGeneralModel dObInvoice =
          (DObVendorInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorInvoices",
                  invoiceCode,
                  invoiceType,
                  invoicePurchaseUnit,
                  invoiceVendor,
                  invoicePurchaseOrder,
                  invoiceNumber,
                  invoiceState);

      assertNotNull(dObInvoice);
    }
  }

  public void assertThatInvoicesMatches(Response userResponse, DataTable invoicesDataTable) {
    List<Map<String, String>> expectedInvoices =
        invoicesDataTable.asMaps(String.class, String.class);

    assertResponseSuccessStatus(userResponse);
    assertCorrectNumberOfRows(userResponse, expectedInvoices.size());

    List<HashMap<String, Object>> listOfInvoics = getListOfMapsFromResponse(userResponse);

    for (int i = 0; i < expectedInvoices.size(); i++) {
      assertThatInvoicesMatchesTheExpected(expectedInvoices.get(i), listOfInvoics.get(i));
    }
  }

  private void assertThatInvoicesMatchesTheExpected(
      Map<String, String> expectedInvoice, HashMap<String, Object> actualInvoice) {
    expectedInvoice = convertEmptyStringsToNulls(expectedInvoice);
    assertEquals(
        expectedInvoice.get(IFeatureFileCommonKeys.CODE),
        actualInvoice.get(IDObVendorInvoiceGeneralModel.INVOICE_CODE));
    assertEquals(
        expectedInvoice.get(IFeatureFileCommonKeys.PURCHASE_UNIT_NAME),
        actualInvoice.get(IDObVendorInvoiceGeneralModel.PRUCHASE_UNIT_NAME_EN));
    assertEquals(
        expectedInvoice.get(IFeatureFileCommonKeys.VENDOR),
        actualInvoice.get(IDObVendorInvoiceGeneralModel.VENDOR_CODE_NAME));
    assertEquals(
        expectedInvoice.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
        actualInvoice.get(IDObVendorInvoiceGeneralModel.PURCHASE_ORDER_CODE));
    assertEquals(
        expectedInvoice.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER),
        actualInvoice.get(IDObVendorInvoiceGeneralModel.INVOICE_NUMBER));
    assertThatStatesArrayContainsValue(
        getStatesArray(
            String.valueOf(actualInvoice.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME))),
        expectedInvoice.get(IFeatureFileCommonKeys.STATE));
  }
}
