package com.ebs.dda.accounting.paymentrequest;

import org.springframework.beans.factory.InitializingBean;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewCostFactorItemDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;

public class DObPaymentRequestViewCostFactorItemDropdownStep extends SpringBootRunner
    implements En, InitializingBean {

  private static final String SCENARIO_NAME = "Read all CostFactorItems";
  private static boolean hasBeenExcutedOnce = false;
  private DObPaymentRequestViewCostFactorItemDropdownTestUtils utils;


  public DObPaymentRequestViewCostFactorItemDropdownStep() {

    When(
        "^\"([^\"]*)\" request to read all CostFactorItems for PaymentRequest of code \"([^\"]*)\"$",
        (String username, String paymentRequestCode) -> {
          utils.requestToreadCostFactorItems(username, paymentRequestCode, userActionsTestUtils);
        });

    Then("^the following values of CostFactorItems are displayed to \"([^\"]*)\":$",
        (String username, DataTable itemsDT) -> {
          utils.assertThatCostFactorItemsAreRetrieved(
              userActionsTestUtils.getUserResponse(username), itemsDT);
        });

    Then("^total number of CostFactorItems returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String username, Integer numOfRecords) -> {
          utils.assetThatTotalNumberOfRetrievedItemsIs(
              userActionsTestUtils.getUserResponse(username), numOfRecords);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestViewCostFactorItemDropdownTestUtils(getProperties(),
        entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSavePaymentDetails(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      if (!hasBeenExcutedOnce) {
        databaseConnector.createConnection();
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExcutedOnce = true;
      }
      databaseConnector.executeSQLScript(DObPaymentRequestViewCostFactorItemDropdownTestUtils.clearPaymentRequests());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
