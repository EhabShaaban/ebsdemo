package com.ebs.dda.accounting.vendorinvoice;

public interface IDObVendorInvoiceRestUrls {

  String COMMAND_URL = "/command/accounting/vendorinvoice/";
  String GET_URL = "/services/accounting/vendorinvoice/";
  String CREATE_URL = COMMAND_URL;
  String AUTHORIZED_ACTIONS = GET_URL + "authorizedactions";
  String INVOICE_GENERAL_DATA_URL = "/services/accounting/vendorinvoice/";
  String GENERAL_DATA_SECTION = "/generaldata";
  String COMPANY_DATA_SECTION = "/companydata";
  String POSTING_DETAILS_SECTION = "/postingdetails";
  String ITEMS_SECTION = "/items";
  String GET_ALL_INVOICES = "/services/accounting/vendorinvoice";
  String GET_PURCASE_ORDER_BASED_ON_TYPE = GET_URL + "purchaseorders";

  String INVOICE_DETAILS_SECTION = "/invoicedetails";
  String LOCK_INVOICE_DETAILS_SECTION = "/lock/invoicedetails/";
  String UNLOCK_INVOICE_DETAILS_SECTION = "/unlock/invoicedetails/";
  String UNLOCK_LOCKED_SECTIONS = COMMAND_URL + "unlock/lockedsections/";
  String SAVE_INVOICE_DETAILS_SECTION = "/update/invoicedetails/";
  String REQUEST_DELETE_ITEM_URL = "/items/";
  String POST_INVOICE_URL = "/post";
  String ACTIVATE_URL = "/activate";

  String REQUEST_CREATE_VENDOR_INVOICE = COMMAND_URL;

  String LOCK_ITEM_SECTION = "/lock/items";
  String UPDATE_ITEM_SECTION = "/update/items";
  String SAVE_EDIT_ITEM_SECTION = "/update/item/";
  String UNLOCK_ITEM_SECTION = "/unlock/items";
  String UNLOCK_ALL_SECTION = "/unlock/lockedsections";
  String TAXES = "/taxes";
  String SUMMARY = "/summary";
  String READ_ALL_VENDOR_ITEMS = GET_URL + "businesspartneritems/";
  String READ_ALL_VENDOR_ITEM_UNITS = GET_URL + "businesspartneritemsunits/";
}
