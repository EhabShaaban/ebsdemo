package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceAllowedActionTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceAllowedActionsStep extends SpringBootRunner implements En, InitializingBean {

  private static boolean hasBeenExecuted = false;
  private DObVendorInvoiceAllowedActionTestUtils utils;

  public DObVendorInvoiceAllowedActionsStep() {
    When(
        "^\"([^\"]*)\" requests to read actions of Vendor Invoice home screen$",
        (String userName) -> {
          String readUrl = IDObVendorInvoiceRestUrls.AUTHORIZED_ACTIONS;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String actionsDataTable, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, actionsDataTable);
        });
    When(
        "^\"([^\"]*)\" requests to read actions of Vendor Invoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
          String readUrl = IDObVendorInvoiceRestUrls.AUTHORIZED_ACTIONS + '/' + invoiceCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    And(
        "^the following Vendor Invoices exist:$",
        (DataTable invoicesDataTable) -> {
          utils.assertThatInvoicesExist(invoicesDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObVendorInvoiceAllowedActionTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in Vendor Invoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read allowed actions in Vendor Invoice")) {
      databaseConnector.closeConnection();
    }
  }
}
