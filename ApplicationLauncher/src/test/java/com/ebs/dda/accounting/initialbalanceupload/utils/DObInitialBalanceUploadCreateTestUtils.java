package com.ebs.dda.accounting.initialbalanceupload.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.initialbalanceupload.IDObInitialBalanceUploadRestURLS;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUploadGeneralModel;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUploadCreateValueObject;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IObInitialBalanceUploadCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IObInitialBalanceUploadItemGeneralModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObInitialBalanceUploadCreateTestUtils extends CommonTestUtils {

  private final ObjectMapper objectMapper;

  public DObInitialBalanceUploadCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public String clearIBUInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/initial-balance-upload/initial_balance_upload_Clear.sql");
    return str.toString();
  }

  public String getDbScriptsPathOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    return str.toString();
  }

  public void assertLastCreatedInitialBalanceUpload(String initialBalanceUploadCode) {
    Object initialBalanceUpload =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getLastCreatedInitialBalanceUpload", initialBalanceUploadCode);
    Assert.assertNotNull(initialBalanceUpload);
  }

  public Response createInitialBalanceUpload(
      DataTable initialBalanceUploadDataTable, Cookie userCookie) {
    Map<String, String> initialBalanceUpload =
        initialBalanceUploadDataTable.asMaps(String.class, String.class).get(0);
    JsonObject initialBalanceUploadRequest =
        prepareInitialBalanceUploadJsonObject(initialBalanceUpload);
    String restURL = IDObInitialBalanceUploadRestURLS.CREATE_INITIAL_BALANCE_UPLOAD;
    File attachment = getAttachment(initialBalanceUpload.get(IFeatureFileCommonKeys.ATTACHMENT));
    return sendPostRequestWithAttachment(
        userCookie, restURL, attachment, initialBalanceUploadRequest);
  }

  private File getAttachment(String fileName) {
    return new File("src/test/resources/files/" + fileName);
  }

  private JsonObject prepareInitialBalanceUploadJsonObject(
      Map<String, String> initialBalanceUpload) {
    JsonObject initialBalanceUploadJsonObject = new JsonObject();

    addValueToJsonObject(
        initialBalanceUploadJsonObject,
        IDObInitialBalanceUploadCreateValueObject.PURCHASE_UNIT,
        checkIfNull(initialBalanceUpload.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT))
            ? null
            : initialBalanceUpload.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT)
                .split(" - ")[0]);

    addValueToJsonObject(
        initialBalanceUploadJsonObject,
        IDObInitialBalanceUploadCreateValueObject.COMPANY,
        checkIfNull(initialBalanceUpload.get(IFeatureFileCommonKeys.COMPANY))
            ? null
            : initialBalanceUpload.get(IFeatureFileCommonKeys.COMPANY).split(" - ")[0]);

    addValueToJsonObject(
        initialBalanceUploadJsonObject,
        IDObInitialBalanceUploadCreateValueObject.FISCALPERIODCODE,
        checkIfNull(initialBalanceUpload.get(IAccountingFeatureFileCommonKeys.FISCAL_PERIOD))
            ? null
            : initialBalanceUpload.get(IAccountingFeatureFileCommonKeys.FISCAL_PERIOD)
                .split(" - ")[0]);

    initialBalanceUploadJsonObject.addProperty(
        IDObInitialBalanceUploadCreateValueObject.DOCUMENT_OWNER_ID,
        Long.parseLong(initialBalanceUpload.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID)));

    return initialBalanceUploadJsonObject;
  }

  public void assertThatInitialCompanyDetailsIsUpdated(
      String initialBalanceUploadCode, DataTable initialBalanceUploadCompanyDetailsDataTable) {
    List<Map<String, String>> initialBalanceUploadCompanyData =
        initialBalanceUploadCompanyDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> initialBalanceUploadDetailsRow : initialBalanceUploadCompanyData) {
      IObInitialBalanceUploadCompanyDataGeneralModel intialBalanceUploadCompanyDataGeneralModel =
          retrieveIntialBalanceUploadCompanyDetailsData(
              initialBalanceUploadCode, initialBalanceUploadDetailsRow);
      assertNotNull(
          intialBalanceUploadCompanyDataGeneralModel,
          "Company Details Data for IBU with code " + initialBalanceUploadCode + " doesn't exist!");
    }
  }

  private IObInitialBalanceUploadCompanyDataGeneralModel
      retrieveIntialBalanceUploadCompanyDetailsData(
          String initialBalanceUploadCode, Map<String, String> initialBalanceUploadDetailsRow) {
    return (IObInitialBalanceUploadCompanyDataGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getInitialBalanceUploadCompanyDetails",
            constructCreatedInitialBalanceUploadCompanyDetailsQueryParams(
                initialBalanceUploadCode, initialBalanceUploadDetailsRow));
  }

  private Object[] constructCreatedInitialBalanceUploadCompanyDetailsQueryParams(
      String initialBalanceUploadCode, Map<String, String> initialBalanceUploadDetailsRow) {
    String company = initialBalanceUploadDetailsRow.get(IAccountingFeatureFileCommonKeys.COMPANY);
    String businessUnit = initialBalanceUploadDetailsRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    return new Object[] {initialBalanceUploadCode, company, businessUnit};
  }

  public void assertInitialBalanceUploadIsCreatedSuccessfully(
      DataTable initialBalanceUploadDataTable) {

    List<Map<String, String>> initialBalanceUploadList =
        initialBalanceUploadDataTable.asMaps(String.class, String.class);
    for (Map<String, String> initialBalanceUploadRow : initialBalanceUploadList) {
      DObInitialBalanceUploadGeneralModel initialBalanceUploadGeneralModel =
          (DObInitialBalanceUploadGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedInitialBalanceUpload",
                  constructInitialBalanceUploadQueryParams(initialBalanceUploadRow));
      assertNotNull(initialBalanceUploadGeneralModel);

      assertDateEquals(
          initialBalanceUploadRow.get(IFeatureFileCommonKeys.CREATION_DATE),
          initialBalanceUploadGeneralModel.getCreationDate());
      assertDateEquals(
          initialBalanceUploadRow.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
          initialBalanceUploadGeneralModel.getModifiedDate());
    }
  }

  private Object[] constructInitialBalanceUploadQueryParams(
      Map<String, String> initialBalanceUploadRow) {
    return new Object[] {
      initialBalanceUploadRow.get(IFeatureFileCommonKeys.CODE),
      '%' + initialBalanceUploadRow.get(IFeatureFileCommonKeys.STATE) + '%',
      initialBalanceUploadRow.get(IFeatureFileCommonKeys.CREATED_BY),
      initialBalanceUploadRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      initialBalanceUploadRow.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      initialBalanceUploadRow.get(IAccountingFeatureFileCommonKeys.FISCAL_PERIOD)
    };
  }

  public void assertThatInitialBalanceItemsAreCreatedSuccessfully(
      String userCode, DataTable itemsDataTable) {
    List<Map<String, String>> itemsMapList = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> itemMap : itemsMapList) {
      IObInitialBalanceUploadItemGeneralModel createdIBUItem =
          (IObInitialBalanceUploadItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedIBUItem", constructISUItemQueryParams(userCode, itemMap));
      assertNotNull(createdIBUItem);
    }
  }

  private Object[] constructISUItemQueryParams(String userCode, Map<String, String> itemMap) {
    return new Object[] {
      itemMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).isEmpty()?null:itemMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT),
      getDouble(itemMap.get(IAccountingFeatureFileCommonKeys.DEBIT)),
      getDouble(itemMap.get(IAccountingFeatureFileCommonKeys.CREDIT)),
      userCode,
      itemMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
      itemMap.get(IAccountingFeatureFileCommonKeys.SUBLEDGER),
      itemMap.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      getDouble(itemMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE)),
    };
  }

  public boolean checkIfNull(String str) {
    return (str == null);
  }
}
