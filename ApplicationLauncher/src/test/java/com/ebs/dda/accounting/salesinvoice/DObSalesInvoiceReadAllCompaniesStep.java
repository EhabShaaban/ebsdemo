package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceCreateTestUtils;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.masterdata.enterprise.ICObEnterpriseRestUrls;
import com.ebs.dda.masterdata.enterprise.IResponseKeys;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceReadAllCompaniesStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceCreateTestUtils dObSalesInvoiceCreateTestUtils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  public DObSalesInvoiceReadAllCompaniesStep() {
    When(
        "^\"([^\"]*)\" requests to read all Companies$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              enterpriseTestUtils.sendGETRequest(cookie, ICObEnterpriseRestUrls.READ_COMPANIES_URL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Company values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable companies) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          enterpriseTestUtils.assertThatResponseDataIsCorrect(
              response, companies, IResponseKeys.COMPANY_NAME);
        });

      Then("^total number of Companies returned to \"([^\"]*)\" is equal to (\\d+)$",  (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          enterpriseTestUtils.assertTotalNumberOfRecordsEqualResponseSize(response, totalRecordsNumber);
      });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    dObSalesInvoiceCreateTestUtils =
        new DObSalesInvoiceCreateTestUtils(properties, entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    enterpriseTestUtils.setProperties(properties);

    if (contains(scenario.getName(), "Read list of Companies dropdown")) {
      databaseConnector.createConnection();
    if (!hasBeenExecuted) {
      databaseConnector.executeSQLScript(
          dObSalesInvoiceCreateTestUtils.getDbScriptsOneTimeExecution());
      hasBeenExecuted = true;
    }
    databaseConnector.executeSQLScript(dObSalesInvoiceCreateTestUtils.getDbScriptPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read list of Companies dropdown")) {
      databaseConnector.closeConnection();
    }
  }
}
