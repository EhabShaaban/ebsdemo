package com.ebs.dda.accounting.accountingnote;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingnote.utils.CreditNoteReadSalesReturnDropDownTestUtils;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderRestURLs;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObCreditNoteReadSalesReturnDropDownStep extends SpringBootRunner implements En {

  private CreditNoteReadSalesReturnDropDownTestUtils utils;

  public DObCreditNoteReadSalesReturnDropDownStep() {

    Given(
        "^the total number of existing records of SalesReturnOrders is (\\d+)$",
        (Long totalRecordsNumber) -> {
          utils.assertThatTotalNumberOfSalesReturnOrdersEquals(totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read drop-down of SalesReturnOrder for State \\(Shipped | GoodsReceiptActivated\\)$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, IDObSalesReturnOrderRestURLs.SALES_RETURN_ORDER_FOR_AN);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following SalesReturnOrders will be presented to \"([^\"]*)\":$",
        (String userName, DataTable salesReturnOrderDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertSalesReturnOrderDataIsCorrect(salesReturnOrderDataTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new CreditNoteReadSalesReturnDropDownTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of SalesReturnOrders dropdown for AN")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
