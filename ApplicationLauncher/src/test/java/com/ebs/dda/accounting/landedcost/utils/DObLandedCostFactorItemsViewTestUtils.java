package com.ebs.dda.accounting.landedcost.utils;

import static com.ebs.dda.CommonKeys.DATA;
import static com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys.*;
import static com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsDetailsGeneralModel.DOCUMENT_INFO_CODE;
import static com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsDetailsGeneralModel.DOCUMENT_INFO_TYPE;
import static com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsGeneralModel.DETAILS;
import static com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsGeneralModel.ITEM_CODE;
import static com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsGeneralModel.ITEM_NAME;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemsSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObLandedCostFactorItemsViewTestUtils extends DObLandedCostTestUtils {

  public DObLandedCostFactorItemsViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static String resetDataDependancies() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemAccountingInfo.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemAccountingInfo_ViewAll.sql");
    return str.toString();

  }

  public Response readCostFactorItemResponse(String landedCostCode, Cookie userCookie) {
    StringBuilder restURL = new StringBuilder();
    restURL.append(IDObLandedCostRestUrls.GET_URL);
    restURL.append("/");
    restURL.append(landedCostCode);
    restURL.append(IDObLandedCostRestUrls.COST_FACTOR_ITEMS);
    return sendGETRequestWithDoubleNumbersType(userCookie, restURL.toString());
  }

  public Response readCostFactorItemsSummarResponse(String landedCostCode, Cookie userCookie) {
    StringBuilder restURL = new StringBuilder();
    restURL.append(IDObLandedCostRestUrls.GET_URL);
    restURL.append("/");
    restURL.append(landedCostCode);
    restURL.append(IDObLandedCostRestUrls.COST_FACTOR_ITEMS_SUMMARY);
    return sendGETRequestWithDoubleNumbersType(userCookie, restURL.toString());
  }

  public void assertCorrectValuesAreDisplayed(Response response, DataTable costFactorItemsDataTable)
      throws Exception {
    List<Map<String, String>> expectedCostFactorItems =
        costFactorItemsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualCostFactorItems = getListOfMapsFromResponse(response);
    for (int index = 0; index < expectedCostFactorItems.size(); index++) {
      String expectedItemFactor = expectedCostFactorItems.get(index).get(ITEM);
      if (expectedItemFactor.isEmpty()) {
        Assert.assertTrue(actualCostFactorItems.isEmpty());
      } else {
        MapAssertion mapAssertion =
            new MapAssertion(expectedCostFactorItems.get(index), actualCostFactorItems.get(index));
        assertThatCostFactorItemDataMatches(mapAssertion);
      }
    }
  }

  public void assertItemsActualDetailsAreExist(Response response, DataTable actualDetailsDT)
      throws Exception {

    List<Map<String, String>> expectedDetailsList =
        actualDetailsDT.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualCostFactorItems = getListOfMapsFromResponse(response);
    for (HashMap<String, Object> actualItemsMap : actualCostFactorItems) {

      List<Map<String, String>> expectedDetailsToCompare = new ArrayList<>();
      for (Map<String, String> expectedDetails : expectedDetailsList) {

        String actualItemCodeName = constructItemActualCodeName(actualItemsMap);
        if (actualItemCodeName.equals(expectedDetails.get(ITEM))) {
          expectedDetailsToCompare.add(expectedDetails);
        }
      }

      List<Map<String, Object>> actualDetailsList =
          (List<Map<String, Object>>) actualItemsMap.get(DETAILS);

      compareItemsDetailsResult(expectedDetailsToCompare, actualDetailsList);

    }
  }

  private String constructItemActualCodeName(HashMap<String, Object> actualItemsMap) {
    String actualItemCodeName = (String) actualItemsMap.get(ITEM_CODE);
    actualItemCodeName += " - " + ((Map<String, Map<String, String>>) actualItemsMap.get(ITEM_NAME))
        .get("values").get("en");
    return actualItemCodeName;
  }

  private void compareItemsDetailsResult(List<Map<String, String>> expectedDetailsList,
      List<Map<String, Object>> actualDetailsList) {

    assertEquals(expectedDetailsList.size(), actualDetailsList.size());
    for (int i = 0; i < expectedDetailsList.size(); i++) {

      Map<String, Object> actualDetails = actualDetailsList.get(i);
      Map<String, String> expectedDetails = expectedDetailsList.get(i);
      MapAssertion mapAssertion = new MapAssertion(expectedDetails, actualDetails);
      String actualDocInfoCodeType =
          actualDetails.get(DOCUMENT_INFO_CODE) + " - " + actualDetails.get(DOCUMENT_INFO_TYPE);
      assertEquals(expectedDetails.get(DOCUMENT_INFORMATION).replace("Payment",
          IDObPaymentRequest.SYS_NAME), actualDocInfoCodeType);
      mapAssertion.assertBigDecimalNumberValue(ACTUAL_VALUE, IIObLandedCostFactorItemsDetailsGeneralModel.ACTUAL_VALUE);
    }
  }

  public void assertTotalNumberOfCostFactorItemsDetailsExist(String itemCodeName,
      String landedCosCode, String numOfRecords) {
    Long actualNumber = (Long) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getLandedCostFactorItemsDetailsNumberByCode", landedCosCode, itemCodeName);
    Assert.assertEquals(Long.valueOf(numOfRecords), actualNumber);
  }

  public void assertCorrectValuesAreDisplayedInSummarySection(Response response,
      String landedCostCode, DataTable summaryDataTable) throws Exception {
    Map<String, String> summaryDataExpected =
        summaryDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> summaryDataActual = getMapFromResponse(response);
    String factorItemSummary = summaryDataExpected.get(ESTIMATE_TOTAL_AMOUNT);
    MapAssertion mapAssertion = new MapAssertion(summaryDataExpected, summaryDataActual);
    if (factorItemSummary.isEmpty()) {
      assertThereIsNoCostFactorItemsSummary(landedCostCode);
    } else {
      assertActualAndExpectedAreTheSameInSummarySection(mapAssertion);
    }
  }

  public void assertThereIsNoCostFactorItemsSummary(String landedCostCode) {

    IIObLandedCostFactorItemsSummaryGeneralModel landedCostFactorItemsSummary =
        (IIObLandedCostFactorItemsSummaryGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getLandedCostFactorItemsSummary", landedCostCode);
    Assert.assertNull(landedCostFactorItemsSummary);
  }

  private void assertActualAndExpectedAreTheSameInSummarySection(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertConcatenatingValue(ESTIMATE_TOTAL_AMOUNT,
        IIObLandedCostFactorItemsSummaryGeneralModel.ESTIMATE_TOTAL_AMOUNT,
        IIObLandedCostFactorItemsSummaryGeneralModel.COMPANY_CURRENCY, " ");

    mapAssertion.assertConcatenatingValue(ACTUAL_TOTAL_AMOUNT,
        IIObLandedCostFactorItemsSummaryGeneralModel.ACTUAL_TOTAL_AMOUNT,
        IIObLandedCostFactorItemsSummaryGeneralModel.COMPANY_CURRENCY, " ");

    mapAssertion.assertConcatenatingValue(DIFFERENCE_TOTAL_AMOUNT,
        IIObLandedCostFactorItemsSummaryGeneralModel.DIFFERENCE_TOTAL_AMOUNT,
        IIObLandedCostFactorItemsSummaryGeneralModel.COMPANY_CURRENCY, " ");
  }

  private void assertThatCostFactorItemDataMatches(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertValue(LC_CODE, IIObLandedCostFactorItemsGeneralModel.CODE);
    mapAssertion.assertCodeAndLocalizedNameValue(ITEM,
        IIObLandedCostFactorItemsGeneralModel.ITEM_CODE,
        IIObLandedCostFactorItemsGeneralModel.ITEM_NAME);
    mapAssertion.assertConcatenatingValue(ESTIMATED_VALUE,
        IIObLandedCostFactorItemsGeneralModel.ESTIMATED_VALUE,
        IIObLandedCostFactorItemsGeneralModel.COMPANY_CURRENCY, " ");
    mapAssertion.assertConcatenatingValue(ACTUAL_VALUE,
        IIObLandedCostFactorItemsGeneralModel.ACTUAL_VALUE,
        IIObLandedCostFactorItemsGeneralModel.COMPANY_CURRENCY, " ");
    mapAssertion.assertConcatenatingValue(DIFFERENCE,
        IIObLandedCostFactorItemsGeneralModel.DIFFERENE,
        IIObLandedCostFactorItemsGeneralModel.COMPANY_CURRENCY, " ");
  }

  public void assertTotalNumberOfCostFactorItemsExist(String code, String expectedNumOfRecords) {
    Long actualNumber = (Long) entityManagerDatabaseConnector
        .executeNativeNamedQuerySingleResult("getLandedCostFactorItemsNumberByCode", code);
    Assert.assertEquals(Long.valueOf(expectedNumOfRecords), actualNumber);
  }

  public void assertThatLandedCostItemsFactorAreEmpty(Response response) {
    List<HashMap<String, Object>> items = response.body().jsonPath().getList(DATA);
    assertTrue(items.isEmpty());
  }

  public void insertCostFactorItemsDetails(DataTable costFactorItemsDetailsDT) {
    List<Map<String, String>> costFactorItemsDetailsMaps =
        costFactorItemsDetailsDT.asMaps(String.class, String.class);

    for (Map<String, String> itemDetailsMap : costFactorItemsDetailsMaps) {
      Object[] params = constructCostFactorItemsDetailsInsertParams(itemDetailsMap);
      entityManagerDatabaseConnector
          .executeInsertQueryForObject("insertLandedCostFactorItemsDetails", params);
    }
  }

  private Object[] constructCostFactorItemsDetailsInsertParams(Map<String, String> itemDetailsMap) {
    return new Object[] {

        itemDetailsMap.get(LC_CODE), itemDetailsMap.get(ITEM),
        itemDetailsMap.get(DOCUMENT_INFORMATION).replace("Payment", IDObPaymentRequest.SYS_NAME),
        Double.parseDouble(itemDetailsMap.get(ACTUAL_VALUE))

    };
  }

  public void assertCostFactorItemsWithDetailsExist(String costFactorItem, String landedCostCode,
                                                    DataTable costFactorItemDataTable) {
    List<Map<String, String>> costFactorItemsWithDetailsMaps =
            costFactorItemDataTable.asMaps(String.class, String.class);

    for (Map<String, String> itemDetailsMap : costFactorItemsWithDetailsMaps) {
      Object[] params = constructCostFactorItemsWithDetailsParams(costFactorItem, landedCostCode, itemDetailsMap);

      IObLandedCostFactorItemsGeneralModel landedCostFactorItemsGeneralModel =
              (IObLandedCostFactorItemsGeneralModel) entityManagerDatabaseConnector
                      .executeNativeNamedQuerySingleResult("getLandedCostFactorItemsWithReferenceDocument", params);

      Assert.assertNotNull("CostFactorItem is null, given: " + Arrays.toString(params),
          landedCostFactorItemsGeneralModel);
    }
  }

  private Object[] constructCostFactorItemsWithDetailsParams(String costFactorItem,
      String landedCostCode, Map<String, String> itemDetailsMap) {
    return new Object[] {landedCostCode, costFactorItem,
        new BigDecimal(itemDetailsMap.get(ESTIMATED_VALUE)),
        new BigDecimal(itemDetailsMap.get(ACTUAL_VALUE)),
        itemDetailsMap.get(REFERENCE_DOCUMENT).replace("Payment", IDObPaymentRequest.SYS_NAME)};
  }

}
