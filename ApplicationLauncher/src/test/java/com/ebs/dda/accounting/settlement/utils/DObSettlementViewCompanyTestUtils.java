package com.ebs.dda.accounting.settlement.utils;

import java.util.Map;

import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentCompanyDataGeneralModel;

import cucumber.api.DataTable;
import io.restassured.response.Response;

public class DObSettlementViewCompanyTestUtils extends DObSettlementsCommonTestUtils {

	public DObSettlementViewCompanyTestUtils(Map<String, Object> properties) {
		super(properties);
		setProperties(properties);
	}

	public void assertThatViewSettlementCompanyDataResponseIsCorrect(Response response,
					DataTable companyDataTable){
		Map<String, String> expectedCompanyDataMap = companyDataTable
						.asMaps(String.class, String.class).get(0);
		Map<String, Object> actualCompanyDataMap = getMapFromResponse(response);

		MapAssertion mapAssertion = new MapAssertion(expectedCompanyDataMap, actualCompanyDataMap);

		mapAssertion.assertLocalizedValue(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT,
				IIObAccountingDocumentCompanyDataGeneralModel.PURCHASING_UNIT_NAME);

		mapAssertion.assertLocalizedValue(IAccountingFeatureFileCommonKeys.COMPANY,
				IIObAccountingDocumentCompanyDataGeneralModel.COMPANY_NAME);
	}

}
