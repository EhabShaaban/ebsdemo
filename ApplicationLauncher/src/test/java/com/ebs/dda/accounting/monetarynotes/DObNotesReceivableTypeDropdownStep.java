package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObNotesReceivableTypeDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObNotesReceivableTypeDropdownStep extends SpringBootRunner implements En {

  private DObNotesReceivableTypeDropdownTestUtils utils;

  public DObNotesReceivableTypeDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all NotesReceivable Types$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllNotesReceivableTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following NotesReceivable Types values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable typesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatNotesReceivableTypesResponseIsCorrect(typesDataTable, response);
        });

    Then(
        "^total number of NotesReceivable Types returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer typesCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfNotesReceivableTypesEquals(typesCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObNotesReceivableTypeDropdownTestUtils(getProperties(),entityManagerDatabaseConnector);
  }
}
