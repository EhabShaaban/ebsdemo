package com.ebs.dda.accounting.collection;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewAllTestUtils;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionGeneralModel;
import com.ebs.dda.rest.utils.Status;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObCollectionViewAllStep extends SpringBootRunner implements En {

  private DObCollectionViewAllTestUtils utils;

  public DObCollectionViewAllStep() {

    Given(
        "^the following GeneralData for the following Collections exists:$",
        (DataTable generalDataTable) -> {
          utils.assertThatTheFollowingCollectionGeneralDataExist(generalDataTable);
        });

    Given(
        "^the following CollectionDetails for the following Collections exists:$",
        (DataTable collectionDetailsDataTable) -> {
          utils.assertThatCollectionDetailsExist(collectionDetailsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with no filter applied (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable collectionDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatViewAllResponseIsCorrect(collectionDataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer numOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);

          utils.assertThatTotalNumberOfRecordsIsCorrect(response, numOfRecords);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on CollectionCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String collectionCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getContainsFilterField("userCode", collectionCodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on CreatedBy which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String collectionContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObCollectionGeneralModel.CREATION_INFO, collectionContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on BusinessPartnerName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String collectionContains,
            Integer pageSize,
            String local) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObCollectionGeneralModel.BUSINESS_PARTNER, collectionContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObCollectionRestURLs.VIEW_ALL_COLLECTION,
                  pageNumber,
                  pageSize,
                  filter,
                  local);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on BusinessPartnerCode which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String collectionContains,
            Integer pageSize,
            String local) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObCollectionGeneralModel.BUSINESS_PARTNER, collectionContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObCollectionRestURLs.VIEW_ALL_COLLECTION,
                  pageNumber,
                  pageSize,
                  filter,
                  local);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page  while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String collectionContains,
            Integer pageSize,
            String local) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(
                  IDObCollectionGeneralModel.PURCHASE_UNIT_CODE, collectionContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObCollectionRestURLs.VIEW_ALL_COLLECTION,
                  pageNumber,
                  pageSize,
                  filter,
                  local);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on ReferenceDocumentCode which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String collectionContains,
            Integer pageSize,
            String local) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObCollectionGeneralModel.REF_DOCUMENT, collectionContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObCollectionRestURLs.VIEW_ALL_COLLECTION,
                  pageNumber,
                  pageSize,
                  filter,
                  local);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on ReferenceDocumentName which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String collectionContains,
            Integer pageSize,
            String local) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObCollectionGeneralModel.REF_DOCUMENT, collectionContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObCollectionRestURLs.VIEW_ALL_COLLECTION,
                  pageNumber,
                  pageSize,
                  filter,
                  local);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on Amount which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String collectionContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(IDObCollectionGeneralModel.AMOUNT, collectionContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on AmountCurrency which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String collectionContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObCollectionGeneralModel.COMPANY_CURRENCY_ISO, collectionContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on State which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String collectionContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME, collectionContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String collectionContains, Integer pageSize) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            String filter =
                    utils.getContainsFilterField(
                            IDObCollectionGeneralModel.REF_DOCUMENT_TYPE_NAME, collectionContains);
            Response response =
                    utils.requestToReadFilteredDataWithLocale(
                            cookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNumber, pageSize, filter, utils.REQUEST_LOCALE);
            userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on DocumentOwner which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String collectionContains, Integer pageSize) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            String filter =
                    utils.getContainsFilterField(
                            IDObCollectionGeneralModel.DOCUMENT_OWNER, collectionContains);
            Response response =
                    utils.requestToReadFilteredData(
                            cookie, IDObCollectionRestURLs.VIEW_ALL_COLLECTION, pageNumber, pageSize, filter);
            userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
          "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on BusinessPartnerType which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
          (String userName,
              Integer pageNumber,
              String collectionContains,
              Integer pageSize,
              String local) -> {
              Cookie cookie = userActionsTestUtils.getUserCookie(userName);
              String filter =
                  utils.getContainsFilterField(
                      IDObCollectionGeneralModel.BUSINESS_PARTNER, collectionContains);
              Response response =
                  utils.requestToReadFilteredDataWithLocale(
                      cookie,
                      IDObCollectionRestURLs.VIEW_ALL_COLLECTION,
                      pageNumber,
                      pageSize,
                      filter,
                      local);
              userActionsTestUtils.setUserResponse(userName, response);
          });

      When(
          "^\"([^\"]*)\" requests to read page (\\d+) of Collections with filter applied on BusinessPartnerType which contains \"([^\"]*)\" and State equals \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
          (String userName,
              Integer pageNumber,
              String businessPartner,
              String state,
              Integer pageSize,
              String local) -> {

              Cookie cookie = userActionsTestUtils.getUserCookie(userName);
              String filter =
                  utils.getContainsFilterField(
                      IDObCollectionGeneralModel.BUSINESS_PARTNER,
                      businessPartner
                  )
                  + Status.FILTER_VALUE_SEPARATOR +
                  utils.getContainsFilterField(
                      IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME,
                      state
                  );
              Response response =
                  utils.requestToReadFilteredDataWithLocale(
                      cookie,
                      IDObCollectionRestURLs.VIEW_ALL_COLLECTION,
                      pageNumber,
                      pageSize,
                      filter,
                      local);
              userActionsTestUtils.setUserResponse(userName, response);
          });

      Given("^the total number of all existing Collection records are (\\d+)$", (Integer recordsNumber) -> {
              utils.assertThatAllCollectionsExist(recordsNumber);
          });

  }

  @Before
  public void before(Scenario scenario) throws Exception {
    utils = new DObCollectionViewAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "View All Collections")) {
        databaseConnector.createConnection();
        databaseConnector.executeSQLScript(utils.resetDataDependanciesBeforeInsertion());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View All Collections")) {
      databaseConnector.closeConnection();
    }
  }
}
