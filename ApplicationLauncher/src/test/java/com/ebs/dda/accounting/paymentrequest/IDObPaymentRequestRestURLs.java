package com.ebs.dda.accounting.paymentrequest;

public interface IDObPaymentRequestRestURLs {

  String PAYMENT_REQUEST_COMMAND = "/command/accounting/paymentrequest";
  String PAYMENT_REQUEST_QUERY = "/services/accounting/paymentrequest";
  String PURCHASE_ORDER_QUERY = "/services/purchase/orders";
  String CREATE_PAYMENT_REQUEST = PAYMENT_REQUEST_COMMAND + "/";
  String UNLOCK_LOCKED_SECTIONS = PAYMENT_REQUEST_COMMAND + "/unlock/lockedsections/";
  String SAVE_COMPANY_DATA_COMMAND = "/update/companydata";
  String LOCK_COMPANY_DATA_SECTION = "/lock/companydata";
  String UNLOCK_COMPANY_DATA_SECTION = "/unlock/companydata";

  String BASE_URL = "/services/accounting/paymentrequest";
  String VIEW_ALL_URL = BASE_URL;
  String AUTHORIZED_ACTIONS = BASE_URL + "/authorizedactions";
  String VIEW_GENERAL_DATA = "/generaldata";
  String VIEW_COMPANY_DATA = "/companydata";

  String POST = "/post";
  String ACTIVATE = "/activate";
  String EXCHANGE_RATE = "/exchangerate";

  String VIEW_PAYMENT_DETAILS = "/paymentdetails";
  String LOCK_PAYMENT_DETAILS_SECTION = "/lock/paymentdetails";
  String UNLOCK_PAYMENT_DETAILS_SECTION = "/unlock/paymentdetails";
  String UPDATE_PAYMENT_DETAILS_SECTION = "/update/paymentdetails";

  String VIEW_ACCOUNTING_DETAILS = "/accountingdetails";
  String LOCK_ACCOUNTING_DETAILS_SECTION = "/lock/accountingdetails";

  String VIEW_POSTING_DETAILS = "/postingdetails";
  String READ_ALL_INVOICES = "/invoices";
  String READ_ALL_ORDERS = "/orders";
  String DELETE = "/command/accounting/paymentrequest/";
  String VIEW_APPROVAL_CYCLES_SECTION = PAYMENT_REQUEST_QUERY + "/approvalcycles/";

  String COST_FACTOR_ITEMS_URL = "/cost-factor-items";
  String TREASURIES_URL = "/treasuries";
  String DUE_DOCUMENT_TYPES = "/documenttypes";
}
