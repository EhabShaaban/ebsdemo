package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivableActivateValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObMonetaryNotesActivateTestUtils extends DObMonetaryNotesCommonTestUtils {
  private static String NOTES_RECEIVABLES = "Notes receivables";
  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[Activate]";

  public DObMonetaryNotesActivateTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/order/salesorder/sales_order_Clear.sql");
    str.append(",").append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    return str.toString();
  }

  public void assertThatNRUpdated(String NRCode, DataTable NotesReceivablesDetailsDataTable) {
    List<Map<String, String>> nrDetailsDataListMap =
        NotesReceivablesDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> nrDetailsDataMap : nrDetailsDataListMap) {
      Object nrDetailslData =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getNRUpdatedData",
              constructNotesReceivableUpdatedDateQueryParams(NRCode, nrDetailsDataMap));
      Assert.assertNotNull(nrDetailslData);
    }
  }

  private Object[] constructNotesReceivableUpdatedDateQueryParams(
      String NRCode, Map<String, String> nrGeneralDataMap) {
    return new Object[] {
      NRCode,
      nrGeneralDataMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      nrGeneralDataMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      '%' + nrGeneralDataMap.get(IFeatureFileCommonKeys.STATE) + '%'
    };
  }

  public void assertThatTheFollowingNRActivationDetailsDataExist(
      String NRCode, DataTable NotesReceivableActivationDetailsDataTable) {
    List<Map<String, String>> nrActivationDetailsDataListMap =
        NotesReceivableActivationDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> nrActivationDetailsDataMap : nrActivationDetailsDataListMap) {
      IObAccountingDocumentActivationDetailsGeneralModel activationDetails =
          (IObAccountingDocumentActivationDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getNotesReceivableActivationDetails",
                  constructNotesReceivableActivationDetailsDateQueryParams(
                      NRCode, nrActivationDetailsDataMap));
      Assert.assertNotNull(
          ASSERTION_MSG
              + " ActivationDetails for NotesReceivable with code "
              + NRCode
              + " wasn't updated!",
          activationDetails);
      assertDateEquals(
          nrActivationDetailsDataMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
          activationDetails.getActivationDate());
      assertDateEquals(
          nrActivationDetailsDataMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_DATE),
          activationDetails.getDueDate());
    }
  }

  private Object[] constructNotesReceivableActivationDetailsDateQueryParams(
      String NRCode, Map<String, String> expectedActivationDetails) {
    return new Object[] {
      expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
      expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
      expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.FISCAL_PERIOD),
      NRCode,
      expectedActivationDetails.get(IFeatureFileCommonKeys.ACTIVATED_BY)
    };
  }

  public Response sendPutNotesReceivable(Cookie cookie, String NRCode, String journalEntryDate) {
    JsonObject jsonObject = new JsonObject();
    addValueToJsonObject(
        jsonObject, IDObNotesReceivableActivateValueObject.JOURNAL_ENTRY_DATE, journalEntryDate);
    return sendPUTRequest(cookie, getPostUrl(NRCode), jsonObject);
  }

  public String getPostUrl(String NRCode) {
    return IDObNotesReceivableRestURLs.BASE_COMMAND_URL
        + "/"
        + NRCode
        + IDObNotesReceivableRestURLs.POST;
  }

  public void assertAccountingDetailsAreUpdated(
      String notesReceivableCode, DataTable accountingDetailsDataTable) {
    List<Map<String, String>> accountingDetailsMaps =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingDetailsMap : accountingDetailsMaps) {
      assertThatNotesReceivableHasAccountingDetails(notesReceivableCode, accountingDetailsMap);
    }
  }

  private void assertThatNotesReceivableHasAccountingDetails(
      String notesReceivableCode, Map<String, String> accountingDetailsMap) {

    Object accountingDetails =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getNotesReceivableAccountingDetails",
            constructNotesReceivableAccountingDetailsQueryParams(
                notesReceivableCode, accountingDetailsMap));

    Assert.assertNotNull(
        "Accounting details for NotesReceivable " + notesReceivableCode + " doesn't exist ",
        accountingDetails);
  }

  private Object[] constructNotesReceivableAccountingDetailsQueryParams(
      String notesReceivableCode, Map<String, String> generalDataMap) {
    String glSubAccountName = "";
    if (!generalDataMap
        .get(IAccountingFeatureFileCommonKeys.ACCOUNT)
        .split(" - ")[1]
        .equals(NOTES_RECEIVABLES)) {
      glSubAccountName =
          generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ")[1];
    }
    String glSubAccount =
        generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ")[0];

    return new Object[] {
      glSubAccount,
      glSubAccountName,
      notesReceivableCode,
      generalDataMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
      generalDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
      generalDataMap.get(IAccountingFeatureFileCommonKeys.VALUE)
    };
  }

  public void assertThatNRRemainingMatchesExpected(String notesReceivableCode, String remaining) {
    DObNotesReceivablesGeneralModel dObNotesReceivablesGeneralModel =
        (DObNotesReceivablesGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getDObNotesReceivablesByCode", notesReceivableCode);
    assertNotNull(
        dObNotesReceivablesGeneralModel,
        ASSERTION_MSG + " There is no NotesReceivable with code " + notesReceivableCode);
    BigDecimal expectedRemaining = new BigDecimal(remaining);
    BigDecimal actualRemaining = dObNotesReceivablesGeneralModel.getRemaining();
    Assert.assertEquals(
        ASSERTION_MSG
            + " Remaining value of RefDocument with code "
            + notesReceivableCode
            + " is not updated as expected",
        0,
        expectedRemaining.compareTo(actualRemaining));
  }

  public void assertThatSalesOrderRemainingMatchesExpected(
      String salesOrderCode, String remaining) {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        (DObSalesOrderGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getDObSalesOrderByCode", salesOrderCode);
    assertNotNull(
        salesOrderGeneralModel,
        ASSERTION_MSG + " There is no SalesOrder with code " + salesOrderCode);
    BigDecimal expectedRemaining = new BigDecimal(remaining);
    BigDecimal actualRemaining = salesOrderGeneralModel.getRemaining();
    Assert.assertEquals(
        ASSERTION_MSG
            + " Remaining value of RefDocument with code "
            + salesOrderCode
            + " is not updated as expected",
        0,
        expectedRemaining.compareTo(actualRemaining));
  }

  public void assertThatSalesInvoiceRemainingMatchesExpected(
      String salesInvoiceCode, String remaining) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        (DObSalesInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceByCode", salesInvoiceCode);
    assertNotNull(
        salesInvoiceGeneralModel,
        ASSERTION_MSG + " There is no SalesInvoic with code " + salesInvoiceCode);
    BigDecimal expectedRemaining = new BigDecimal(remaining);
    BigDecimal actualRemaining = salesInvoiceGeneralModel.getRemaining();
    Assert.assertEquals(
        ASSERTION_MSG
            + " Remaining value of RefDocument with code "
            + salesInvoiceCode
            + " is not updated as expected",
        0,
        expectedRemaining.compareTo(actualRemaining));
  }
}
