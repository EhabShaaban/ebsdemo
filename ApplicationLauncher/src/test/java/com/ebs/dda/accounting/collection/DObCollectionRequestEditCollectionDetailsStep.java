package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.apis.IDObCollectionSectionNames;
import com.ebs.dda.accounting.collection.utils.DObCollectionDeleteTestUtils;
import com.ebs.dda.accounting.collection.utils.DObCollectionRequestEditCollectionDetailsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.HashMap;
import java.util.Map;

public class DObCollectionRequestEditCollectionDetailsStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObCollectionRequestEditCollectionDetailsTestUtils utils;
  private DObCollectionDeleteTestUtils colelctionDeleteTestUtils;
  private Map<String, String> currentLocks;

  public DObCollectionRequestEditCollectionDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to edit CollectionDetails section of Collection with code \"([^\"]*)\"$",
        (String userName, String collectionCode) -> {
          String lockUrl = utils.getLockCollectionDetailsUrl(collectionCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, collectionCode);
          userActionsTestUtils.setUserResponse(userName, response);
          currentLocks.put(userName, collectionCode);
        });

    Then(
        "^CollectionDetails section of Collection with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String collectionCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, collectionCode, IDObCollectionSectionNames.COLLECTION_DETAILS);
        });

    Given(
        "^\"([^\"]*)\" first opened the Collection with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String collectionCode) -> {
          String lockUrl = utils.getLockCollectionDetailsUrl(collectionCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, collectionCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
          currentLocks.put(userName, collectionCode);
        });

    Given(
        "^CollectionDetails section of Collection with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String collectionCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockUrl = utils.getLockCollectionDetailsUrl(collectionCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, collectionCode);
          userActionsTestUtils.setUserResponse(userName, response);
          currentLocks.put(userName, collectionCode);
        });

    When(
        "^\"([^\"]*)\" cancels saving CollectionDetails section of Collection with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String collectionCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockUrl = utils.getUnlockCollectionDetailsUrl(collectionCode);
          Response response = userActionsTestUtils.unlockSection(userName, unlockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
          currentLocks.put(userName, collectionCode);
        });

    Then(
        "^the lock by \"([^\"]*)\" on CollectionDetails section of Collection with code \"([^\"]*)\" is released$",
        (String userName, String collectionCode) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatLockIsReleased(
              userName, collectionCode, IDObCollectionSectionNames.COLLECTION_DETAILS);
        });

    When(
        "^\"([^\"]*)\" cancels saving CollectionDetails section of Collection with code \"([^\"]*)\"$",
        (String userName, String collectionCode) -> {
          String unLockURL = utils.getUnlockCollectionDetailsUrl(collectionCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    And(
        "^\"([^\"]*)\" requests to edit CollectionDetails section of Collection with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String collectionCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lockUrl = utils.getLockCollectionDetailsUrl(collectionCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, collectionCode);
          userActionsTestUtils.setUserResponse(userName, response);
          currentLocks.put(userName, collectionCode);
        });

    And(
        "^CollectionDetails section of Collection with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String collectionCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName, collectionCode, IDObCollectionSectionNames.COLLECTION_DETAILS);
        });

    Given(
        "^\"([^\"]*)\" first deleted the Collection with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String collectionCode, String deletionTime) -> {
          utils.freeze(deletionTime);
          Response collectionDeleteResponse =
              colelctionDeleteTestUtils.deleteCollection(
                  userActionsTestUtils.getUserCookie(userName), collectionCode);
          userActionsTestUtils.setUserResponse(userName, collectionDeleteResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObCollectionRequestEditCollectionDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    colelctionDeleteTestUtils = new DObCollectionDeleteTestUtils(getProperties());
    colelctionDeleteTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    currentLocks = new HashMap<>();
  }

  private void clearLocks(Scenario scenario) throws Exception {
        for (String userName : currentLocks.keySet()) {
        String collectionCode = currentLocks.get(userName);
        String unLockURL = utils.getUnlockCollectionDetailsUrl(collectionCode);
        userActionsTestUtils.unlockSection(userName, unLockURL);
    }
  }

  @Before
  public void beforeRequestEditCollectionDetails(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Collection details")
        || contains(scenario.getName(), "Request to Cancel saving Collection details")) {
      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(utils.getConciseDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Collection details")
        || contains(scenario.getName(), "Request to Cancel saving Collection details")
        || contains(scenario.getName(), "Save CollectionDetails section")) {
      databaseConnector.closeConnection();
      utils.unfreeze();
      clearLocks(scenario);
    }
  }
}
