package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceItemValueObject;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;

public class IObSalesInvoiceSaveItemHappyPathTestUtils extends IObSalesInvoiceItemsTestUtils {

  public IObSalesInvoiceSaveItemHappyPathTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertSalesInvoiceExists(DataTable salesInvoiceTable) {
    List<Map<String, String>> salesInvoices = salesInvoiceTable.asMaps(String.class, String.class);

    for (Map<String, String> salesInvoice : salesInvoices) {
      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceByCurrencyCodeStatePurchaseUnit",
                  constructSalesInvoice(salesInvoice));
      Assert.assertNotNull(salesInvoiceGeneralModel);
    }
  }

  private Object[] constructSalesInvoice(Map<String, String> salesInvoice) {
    return new Object[] {
      salesInvoice.get(IFeatureFileCommonKeys.CODE),
      '%' + salesInvoice.get(IFeatureFileCommonKeys.STATE) + '%',
      salesInvoice.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public String getLockUrl(String salesInvoiceCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObSalesInvoiceRestUrls.COMMAND_URL);
    lockUrl.append(salesInvoiceCode);
    lockUrl.append(IDObSalesInvoiceRestUrls.LOCK_SALES_INVOICE_ITEMS_SECTION);
    return lockUrl.toString();
  }

  public void assertItemsExistByCodeTypeUomAndPurchaseUnits(DataTable itemsDataTable) {
    List<Map<String, String>> itemsList = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsList) {
      assertThatItemExists(item);
    }
  }

  private void assertThatItemExists(Map<String, String> item) {
    Object[] queryParams = constructItemQueryParams(item);

    MObItemGeneralModel mObItemGeneralModel =
        (MObItemGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getItemByCodeNameTypeUomAndPurchaseUnit", queryParams);
    Assertions.assertNotNull(mObItemGeneralModel);
    assertThatBusinessUnitsAreEqual(
        item.get(IFeatureFileCommonKeys.ITEM).split(" - ")[0],
        getBusinessUnitCodes(item.get(IFeatureFileCommonKeys.PURCHASE_UNIT).split(", ")),
        mObItemGeneralModel.getPurchasingUnitCodes());
  }

  public Object[] constructItemQueryParams(Map<String, String> item) {
    return new Object[] {
      item.get(ISalesFeatureFileCommonKeys.ITEM),
      item.get(ISalesFeatureFileCommonKeys.TYPE),
      item.get(ISalesFeatureFileCommonKeys.UOM),
    };
  }

  public Response saveItemToSalesInvoice(
      Cookie cookie, String salesInvoiceCode, DataTable invoiceItemsDataTable) {
    JsonObject salesInvoiceItem = createSalesInvoiceItemJsonObject(invoiceItemsDataTable);
    String restURL =
        IDObSalesInvoiceRestUrls.COMMAND_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.UPDATE_SALES_INVOICE_ITEMS_SECTION;
    return sendPostRequest(cookie, restURL, salesInvoiceItem);
  }

  private JsonObject createSalesInvoiceItemJsonObject(DataTable invoiceItemsDataTable) {
    Map<String, String> itemData = invoiceItemsDataTable.asMaps(String.class, String.class).get(0);

    JsonObject invoiceItemObject = new JsonObject();
    invoiceItemObject.addProperty(
        IDObVendorInvoiceItemValueObject.ITEM_CODE,
        itemData.get(ISalesFeatureFileCommonKeys.ITEM).split(" - ")[0]);
    invoiceItemObject.addProperty(
        IDObVendorInvoiceItemValueObject.QTY,
        Float.valueOf(itemData.get(ISalesFeatureFileCommonKeys.QUANTITY)));
    invoiceItemObject.addProperty(
        IDObVendorInvoiceItemValueObject.ORDER_UNIT_CODE,
        itemData.get(ISalesFeatureFileCommonKeys.ORDER_UNIT_CODE_NAME).split(" - ")[0]);
    invoiceItemObject.addProperty(
        IDObVendorInvoiceItemValueObject.UNIT_PRICE,
        Float.valueOf(itemData.get(ISalesFeatureFileCommonKeys.SALES_PRICE)));

    return invoiceItemObject;
  }
}
