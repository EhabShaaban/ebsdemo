package com.ebs.dda.accounting.paymentrequest;

import org.springframework.beans.factory.InitializingBean;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestTreasuriesTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPaymentRequestTreasuriesDropDownStep extends SpringBootRunner
    implements En, InitializingBean {

  public static final String SCENARIO_NAME = "Read all Treasuries";
  private DObPaymentRequestTreasuriesTestUtils utils;
  private static boolean hasBeenExecutedOnce = false;

    @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestTreasuriesTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObPaymentRequestTreasuriesDropDownStep() {
    When(
        "^\"([^\"]*)\" requests to read all Treasuries of DueDocument with code \"([^\"]*)\" and dueDocumentType \"([^\"]*)\"$",
        (String userName, String paymentRequestCode, String dueDocumentType) -> {
          utils.requestToReadTreasuries(
              userName, paymentRequestCode, dueDocumentType, userActionsTestUtils);
        });
    Then(
        "^the following values of Treasuries are displayed to \"([^\"]*)\":$",
        (String userName, DataTable treasuries) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTreasuriesAreDisplayedToUser(response, treasuries);
        });

    Then(
        "^total number of Treasuries returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer treasuryNumbers) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, treasuryNumbers);
        });
    When(
        "^\"([^\"]*)\" requests to read all Treasuries of Company with code \"([^\"]*)\"$",
        (String userName, String companyCode) -> {
          utils.requestToReadTreasuriesForCompany(userName, companyCode, userActionsTestUtils);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce){
          databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
          hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(DObPaymentRequestTreasuriesTestUtils.clearPaymentRequests());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
