package com.ebs.dda.accounting.landedcost.utils;

import static com.ebs.dac.foundation.realization.statemachine.BasicStateMachine.DRAFT_STATE;
import static com.ebs.dac.foundation.realization.statemachine.PrimitiveStateMachine.ACTIVE_STATE;
import static com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys.*;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import cucumber.api.DataTable;
import org.junit.Assert;

public class DObLandedCostCommonTestUtils extends CommonTestUtils {

  public DObLandedCostCommonTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public static String getLandedCostDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/master-data/exchange-rate/exchange-rate.sql");
    return str.toString();
  }

  public static String clearLandedCosts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/landed-cost/landed_cost_Clear.sql");
    return str.toString();
  }

  public void insertLandedCostGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> landedCostGeneralDataListMap =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> landedCostGeneralDataMap : landedCostGeneralDataListMap) {

      entityManagerDatabaseConnector.executeInsertQuery(
          "insertLandedCostGeneralData",
          constructLandedCostGeneralDataInsertionParams(landedCostGeneralDataMap));
    }
  }

  private Object[] constructLandedCostGeneralDataInsertionParams(
      Map<String, String> landedCostMap) {
    String state = landedCostMap.get(IFeatureFileCommonKeys.STATE);

    if (state.equals(DRAFT_STATE)) {
      state = "[\"" + state + "\"]";
    } else {
      state = "[\"" + state + "\"]";
    }

    return new Object[] {
      landedCostMap.get(IAccountingFeatureFileCommonKeys.LC_CODE),
      landedCostMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      landedCostMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      landedCostMap.get(IFeatureFileCommonKeys.CREATED_BY),
      landedCostMap.get(IFeatureFileCommonKeys.CREATED_BY),
      state,
      landedCostMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      landedCostMap.get(IAccountingFeatureFileCommonKeys.LC_TYPE),
      landedCostMap.get(IAccountingFeatureFileCommonKeys.LC_CODE),
      landedCostMap.get(IAccountingFeatureFileCommonKeys.LC_CODE)
    };
  }

  public void insertLandedCostCompanyData(DataTable companyDataTable) {
    List<Map<String, String>> landedCostCompanyDataListMap =
        companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> landedCostComapanyDataMap : landedCostCompanyDataListMap) {

      Object[] params =
          new Object[] {
            landedCostComapanyDataMap.get(IAccountingFeatureFileCommonKeys.LC_CODE),
            landedCostComapanyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
            landedCostComapanyDataMap.get(IFeatureFileCommonKeys.COMPANY)
          };

      entityManagerDatabaseConnector.executeInsertQuery("insertLandedCostCompanyData", params);
    }
  }

  public void insertLandedCostDetailsData(DataTable detailsDataTable) {
    List<Map<String, String>> landedCostDetailsDataListMap =
        detailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> landedCostDetailsDataMap : landedCostDetailsDataListMap) {

      Object[] params = constructLandedCostDetailsInsertionParams(landedCostDetailsDataMap);

      entityManagerDatabaseConnector.executeInsertQuery("insertLandedCostDetailsData", params);
    }
  }

  private Object[] constructLandedCostDetailsInsertionParams(Map<String, String> landedCostMap) {

    landedCostMap = convertEmptyStringsToNulls(landedCostMap);

    return new Object[] {
      landedCostMap.get(IAccountingFeatureFileCommonKeys.LC_CODE),
      landedCostMap.get(IFeatureFileCommonKeys.PO_CODE),
      landedCostMap.get(IFeatureFileCommonKeys.VENDOR_INVOICE_CODE),
      landedCostMap.get(IFeatureFileCommonKeys.OBJECT_TYPE_CODE),
      Boolean.parseBoolean(landedCostMap.get(IAccountingFeatureFileCommonKeys.DAMAGE_STOCK))
    };
  }

  public void assertThatLandedCostFactorItemsExists(DataTable costFactorItemsDataTable) {
    List<Map<String, String>> costFactorItemsMaps =
        costFactorItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> costFactorItemMap : costFactorItemsMaps) {
      String factorItem = costFactorItemMap.get(ITEM);
      if (factorItem.isEmpty()) {
        assertThatLandedCostDoesNotHasFactorItems(costFactorItemMap);
      } else {
        assertThatLandedCostHasFactorItems(costFactorItemMap);
      }
    }
  }

  private void assertThatLandedCostDoesNotHasFactorItems(Map<String, String> costFactorItemMap) {
    IObLandedCostFactorItemsGeneralModel landedCostFactorItem =
        (IObLandedCostFactorItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLandedCostFactorItemsByCode", costFactorItemMap.get(LC_CODE));
    Assert.assertNull(landedCostFactorItem);
  }

  private void assertThatLandedCostHasFactorItems(Map<String, String> costFactorItemMap) {
    IObLandedCostFactorItemsGeneralModel landedCostFactorItem =
        (IObLandedCostFactorItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLandedCostFactorItems",
                constructLandedCostFactorItemQueryParams(costFactorItemMap));
    Assert.assertNotNull(
        "Landed cost Factor Item doesn't exist " + costFactorItemMap.toString(),
        landedCostFactorItem);
  }

  private Object[] constructLandedCostFactorItemQueryParams(Map<String, String> generalDataMap) {
    return new Object[] {
      generalDataMap.get(ACTUAL_VALUE),
      generalDataMap.get(DIFFERENCE),
      generalDataMap.get(LC_CODE),
      generalDataMap.get(ITEM),
      generalDataMap.get(ESTIMATED_VALUE),
    };
  }

  public void insertCostFactorItems(DataTable costFactorItemsDT) {
    List<Map<String, String>> costFactorItemsMaps =
        costFactorItemsDT.asMaps(String.class, String.class);

    for (Map<String, String> itemMap : costFactorItemsMaps) {
      Object[] params = constructCostFactorItemInsertParams(itemMap);
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "insertLandedCostFactorItems", params);
    }
  }

  private Object[] constructCostFactorItemInsertParams(Map<String, String> itemMap) {
    return new Object[] {
      itemMap.get(LC_CODE),
      itemMap.get(ITEM),
      Double.parseDouble(itemMap.get(ESTIMATED_VALUE).split(" ")[0]),
      Double.parseDouble(itemMap.get(ACTUAL_VALUE).split(" ")[0])
    };
  }

  public void assertThatLandedCostHasItemsSummary(DataTable itemsSummaryDataTable) {
    List<Map<String, String>> costFactorItemsSummary =
        itemsSummaryDataTable.asMaps(String.class, String.class);
    for (Map<String, String> summary : costFactorItemsSummary) {
      Object[] queryParams = constructLandedCostItemsSummaryParemeters(summary);
      IObLandedCostFactorItemsSummary landedCostFactorItemsSummaryGeneralModel =
          (IObLandedCostFactorItemsSummary)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getLandedCostFactorItemsSummaryByCodeAndEstimateActualAndDifference",
                  queryParams);
      assertNotNull(
          "LandedCost CostFactorItemsSummary  does not exist " + Arrays.toString(queryParams),
          landedCostFactorItemsSummaryGeneralModel);
    }
  }

  private Object[] constructLandedCostItemsSummaryParemeters(
          Map<String, String> costFactorItemsSummary) {
    return new Object[] {
            costFactorItemsSummary.get(IAccountingFeatureFileCommonKeys.ACTUAL_VALUE),
            costFactorItemsSummary.get(IAccountingFeatureFileCommonKeys.DIFFERENCE),
            costFactorItemsSummary.get(IAccountingFeatureFileCommonKeys.LC_CODE),
            costFactorItemsSummary
                    .get(IAccountingFeatureFileCommonKeys.ESTIMATED_VALUE)
            , };
  }
}
