package com.ebs.dda.accounting.accountingcommonutils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AccountingDocumentCommonTestUtils extends CommonTestUtils {

  public AccountingDocumentCommonTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertThatAccountingDetailsDataMatches(
      DataTable AccountingDetailsTable, Response response) throws Exception {
    List<Map<String, String>> expectedAccountingDetailsList =
        AccountingDetailsTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualAccountingDetailsList = getListOfMapsFromResponse(response);
    for (int i = 0; i < expectedAccountingDetailsList.size(); i++) {
      MapAssertion mapAssertion =
          new MapAssertion(
              expectedAccountingDetailsList.get(i), actualAccountingDetailsList.get(i));
      assertThatViewAccountingDetailsResponseIsCorrect(mapAssertion);
    }
  }

  private void assertThatViewAccountingDetailsResponseIsCorrect(MapAssertion mapAssertion)
      throws Exception {
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.GL_ACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_NAME);

    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNTING_ENTRY);

    mapAssertion.assertDoubleValue(
        IAccountingFeatureFileCommonKeys.AMOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.AMOUNT);
  }

  public void assertThatJournalEntryIsCreated(DataTable journalEntryDataTable) {
    Map<String, String> journalEntryMap =
        journalEntryDataTable.asMaps(String.class, String.class).get(0);
    DObJournalEntryGeneralModel journalEntry =
        (DObJournalEntryGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getCreatedJournalEntry", constructJournalEntryQueryParams(journalEntryMap));
    Assert.assertNotNull("Journal Entry was not created!", journalEntry);
  }

  public void assertThatJournalEntryItemsIsCreated(
      String journalEntryCode, DataTable journalEntryItemsDataTable) {
    List<Map<String, String>> journalEntryItemsMap =
        journalEntryItemsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> journalEntryItemMap : journalEntryItemsMap) {
      IObJournalEntryItemGeneralModel journalEntryItem =
          (IObJournalEntryItemGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCreatedJournalEntryItem",
                  constructJournalEntryItemQueryParams(journalEntryCode, journalEntryItemMap));
      Assert.assertNotNull(
          "Journal Entry Item was not created! " + journalEntryItemMap, journalEntryItem);
    }
  }

  private Object[] constructJournalEntryQueryParams(Map<String, String> journalEntryMap) {
    return new Object[] {
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR),
      journalEntryMap.get(IFeatureFileCommonKeys.CODE),
      journalEntryMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      journalEntryMap.get(IFeatureFileCommonKeys.CREATED_BY),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_DATE),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.COMPANY),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  private Object[] constructJournalEntryItemQueryParams(
      String journalEntryCode, Map<String, String> journalEntryMap) {
    return new Object[] {
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.EXCHANGE_RATE_CODE),
      journalEntryCode,
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
      new BigDecimal(journalEntryMap.get(IAccountingFeatureFileCommonKeys.CREDIT)),
      new BigDecimal(journalEntryMap.get(IAccountingFeatureFileCommonKeys.DEBIT)),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.DOCUMENT_CURRENCY),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.COMPANY_CURRENCY),
      new BigDecimal(journalEntryMap.get(IAccountingFeatureFileCommonKeys.EX_RATE_TO_CURRENCY_COMPANY))
    };
  }

  public static String clearAccountingDocuments() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/accounting_documents_Clear.sql");
    return str.toString();
  }

  public void assertThatAccountingDetailsAreUpdated(
      String objectType, String accountingDocumentCode, DataTable accountingDetailsDataTable) {
    List<Map<String, String>> accountingDetailsMap =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingDetails : accountingDetailsMap) {
      Long accountingDetailsId =
          (Long)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getUpdatedAccountingDetails",
                  constructAccountingDetailsQueryParams(
                      objectType, accountingDocumentCode, accountingDetails));
      Assert.assertNotNull(
          "Account "
              + accountingDetails.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT)
              + " Does Not Exist",
          accountingDetailsId);
    }
  }

  private Object[] constructAccountingDetailsQueryParams(
      String objectType, String accountingDocumentCode, Map<String, String> accountingDetails) {
    return new Object[] {
      accountingDetails.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
      accountingDetails.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT),
      accountingDetails.get(IAccountingFeatureFileCommonKeys.AMOUNT),
      accountingDocumentCode,
      objectType,
      accountingDetails.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT)
    };
  }

  public void assertThatAccountingDocumentHasThoseActivationDetails(
      String documentType, DataTable activationDetailsDataTable) {
    List<Map<String, String>> activatingDetailsList =
        activationDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> activatingDetails : activatingDetailsList) {

      IObAccountingDocumentActivationDetailsGeneralModel accountingDocumentActivatingDetail =
          (IObAccountingDocumentActivationDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPostingDetailsByDocumentTypeAndPostingInfo",
                  constructAccountingDocumentActivationDetailsArguments(
                      activatingDetails, documentType));

      assertNotNull(
          accountingDocumentActivatingDetail,
          "Activation details for "
              + documentType
              + " with code "
              + activatingDetails.get(IFeatureFileCommonKeys.CODE)
              + " Doesn't Exist!");
    }
  }

  private Object[] constructAccountingDocumentActivationDetailsArguments(
      Map<String, String> collection, String documentType) {
    return new Object[] {
      collection.get(IFeatureFileCommonKeys.CODE),
      collection.get(IFeatureFileCommonKeys.CREATED_BY),
      collection.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
      collection.get(IAccountingFeatureFileCommonKeys.EXCHANGE_RATE_CODE),
      collection.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
      collection.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
      documentType
    };
  }
}
