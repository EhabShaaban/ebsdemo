package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceCreateTestUtils;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceCreationValueObject;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceCreateStep extends SpringBootRunner implements En, InitializingBean {

    public static final String SCENARIO_NAME = "Create VendorInvoice";
    private DObVendorInvoiceCreateTestUtils dObInvoiceCreateTestUtils;
    private MObVendorCommonTestUtils vendorCommonTestUtils;

    public DObVendorInvoiceCreateStep() {

        Given(
                "^the following PurchaseOrders exist with the company data:$",
                (DataTable purchaseOrderDataTable) -> {
                    dObInvoiceCreateTestUtils.assertPurchaseOrderExists(purchaseOrderDataTable);
                });

        Given(
                "^the following Vendors exist:$",
                (DataTable vendorsDataTable) -> {
                    vendorCommonTestUtils.assertThatVendorsExistByCodeAndNameAndPurchaseUnitsCodeNameConcat(
                            vendorsDataTable);
                });

        Given(
                "^CurrentDateTime = \"([^\"]*)\"$",
                (String dateTime) -> {
                    dObInvoiceCreateTestUtils.freeze(dateTime);
                });

        Given(
                "^Last created VendorInvoice was with code \"([^\"]*)\"$",
                (String invoiceCode) -> {
                    dObInvoiceCreateTestUtils.assertGoodsInvoiceExistsWithCode(invoiceCode);
                });

        When(
                "^\"([^\"]*)\" creates VendorInvoice with the following values:$",
                (String userName, DataTable invoiceDataTable) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = dObInvoiceCreateTestUtils.createInvoice(userCookie, invoiceDataTable);
                    userActionsTestUtils.setUserResponse(userName, response);
                });
        When(
                "^\"([^\"]*)\" requests to create VendorInvoice",
                (String userName) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response response =
                            dObInvoiceCreateTestUtils.sendGETRequest(
                                    cookie, IDObVendorInvoiceRestUrls.REQUEST_CREATE_VENDOR_INVOICE);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^a new VendorInvoice is created with the following values:$",
                (DataTable invoiceDataTable) -> {
                    dObInvoiceCreateTestUtils.assertInvoiceIsCreatedSuccessfully(invoiceDataTable);
                });
        Then(
                "^the following error message is attached to Vendor field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
                (String messageCode, String username) -> {
                    dObInvoiceCreateTestUtils.assertThatFieldHasErrorMessageCode(
                            userActionsTestUtils.getUserResponse(username),
                            messageCode,
                            IDObVendorInvoiceCreationValueObject.VENDOR_CODE);
                });
        Given(
                "^PurchaseOrder with code \"([^\"]*)\" has the following OrderItems:$",
                (String poCode, DataTable itemDataTable) -> {
                    dObInvoiceCreateTestUtils.assertPurchaseOrderItemsWithCode(poCode, itemDataTable);
                });
        And("^the Company Section of VendorInvoice with code \"([^\"]*)\" is created as follows:$", (String vendorInvoiceCode, DataTable companyDataTable) -> {
            dObInvoiceCreateTestUtils.assertThatVendorInvoiceCompanyDataIs(vendorInvoiceCode, companyDataTable);
        });
        And("^the Items Section of VendorInvoice with code \"([^\"]*)\" is created as follows:$", (String vendorInvoiceCode, DataTable itemsDataTable) -> {
            dObInvoiceCreateTestUtils.assertThatVendorInvoiceItemsAre(vendorInvoiceCode, itemsDataTable);
        });
        And("^the Taxes Section of VendorInvoice with code \"([^\"]*)\" is created as follows:$", (String vendorInvoiceCode, DataTable taxesDataTable) -> {
            dObInvoiceCreateTestUtils.assertThatTheFollowingTaxDetailsExistForVendorInvoice(
                    vendorInvoiceCode, taxesDataTable);
        });
        Then("^the Details Section of VendorInvoice with Code \"([^\"]*)\" is created as follows:$", (String vendorInvoiceCode, DataTable detailsDataTable) -> {
            dObInvoiceCreateTestUtils.assertThatTheFollowingDetailsExistForVendorInvoice(
                    vendorInvoiceCode, detailsDataTable);
        });
        Then("^the Taxes Section of VendorInvoice with code \"([^\"]*)\" is displayed empty$", (String vendorInvoiceCode) -> {
            dObInvoiceCreateTestUtils.assertThatVendorInvoiceHasNoTaxes(vendorInvoiceCode);
        });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        dObInvoiceCreateTestUtils =
                new DObVendorInvoiceCreateTestUtils(properties, entityManagerDatabaseConnector);
        vendorCommonTestUtils = new MObVendorCommonTestUtils(properties);

        vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.createConnection();
                databaseConnector.executeSQLScript(
                        dObInvoiceCreateTestUtils.getDbScriptsOneTimeExecution());
        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            dObInvoiceCreateTestUtils.unfreeze();
            databaseConnector.closeConnection();
        }
    }
}
