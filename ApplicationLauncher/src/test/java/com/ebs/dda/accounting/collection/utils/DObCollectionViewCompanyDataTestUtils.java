package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.collection.IIObCollectionCompanyDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionCompanyDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObCollectionViewCompanyDataTestUtils extends DObCollectionTestUtils {
  public DObCollectionViewCompanyDataTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatCollectionsCompanyDataExist(DataTable collectionCompanyData) {
    List<Map<String, String>> collectionCompanyExpectedList =
            collectionCompanyData.asMaps(String.class, String.class);
    for (Map<String, String> collectionCompany : collectionCompanyExpectedList) {
      IObCollectionCompanyDetailsGeneralModel collectionCompanyDetailsGeneralModel =
              (IObCollectionCompanyDetailsGeneralModel)
                      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                              "getCollectionCompanyDetails",
                              constructCollectionCompanyDataQueryParams(collectionCompany));
      assertNotNull(
              collectionCompanyDetailsGeneralModel,
              "CompanyData for Collection with code "
                      + collectionCompany.get(IAccountingFeatureFileCommonKeys.C_CODE)
                      + " doesn't exist!");
    }
  }

  private Object[] constructCollectionCompanyDataQueryParams(
          Map<String, String> collectionCompany) {
    return new Object[]{
            collectionCompany.get(IAccountingFeatureFileCommonKeys.C_CODE),
            collectionCompany.get(IAccountingFeatureFileCommonKeys.COMPANY),
            collectionCompany.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public Response getCompanyDataForCollection(Cookie userCookie, String collectionCode) {
    String companyUrl =
            IDObCollectionRestURLs.GET_URL + collectionCode + IDObCollectionRestURLs.COMPANY_DATA;
    return sendGETRequest(userCookie, companyUrl);
  }

  public void assertCorrectValuesAreDisplayedInCompanyData(
          Response response, DataTable collectionCompanyData) throws Exception {

    Map<String, String> expectedCompanyData =
            collectionCompanyData.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualCompanyData = getMapFromResponse(response);

    MapAssertion mapAssertion = new MapAssertion(expectedCompanyData, actualCompanyData);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertLocalizedValue(
            IFeatureFileCommonKeys.COMPANY, IIObCollectionCompanyDetailsGeneralModel.COMPANY_NAME);

    mapAssertion.assertValue(
            IAccountingFeatureFileCommonKeys.BUSINESS_UNIT,
            IIObCollectionCompanyDetailsGeneralModel.PURCHASING_UNIT_NAME_EN);
  }
}
