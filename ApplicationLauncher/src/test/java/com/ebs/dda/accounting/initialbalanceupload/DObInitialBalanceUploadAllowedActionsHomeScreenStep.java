package com.ebs.dda.accounting.initialbalanceupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.inventory.initialstockupload.utils.DObInitialStockUploadCommonTestUtils;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObInitialBalanceUploadAllowedActionsHomeScreenStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObInitialStockUploadCommonTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitialStockUploadCommonTestUtils();
    utils.setProperties(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObInitialBalanceUploadAllowedActionsHomeScreenStep() {
      When("^\"([^\"]*)\" requests to read actions of InitialBalanceUpload home screen$", (String userName) -> {
          String readUrl = IDObInitialBalanceUploadRestURLS.AUTHORIZED_ACTIONS;
          Response response =
                  utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
      });
  }
}
