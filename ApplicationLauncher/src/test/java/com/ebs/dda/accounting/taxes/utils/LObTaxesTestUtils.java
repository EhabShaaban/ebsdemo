package com.ebs.dda.accounting.taxes.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.taxes.LObVendorTaxesGeneralModel;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class LObTaxesTestUtils extends CommonTestUtils {

  public LObTaxesTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertThatTheFollowingVendorTaxesExist(
      String vendorCodeName, DataTable vendortaxesDataTable) {
    List<Map<String, String>> vendorTaxesMaps =
        vendortaxesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> vendortaxesMap : vendorTaxesMaps) {

      Object[] queryParams = constructVendorTaxesQueryParams(vendorCodeName, vendortaxesMap);

      LObVendorTaxesGeneralModel vendorTaxesGeneralModel =
          (LObVendorTaxesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getTaxesForVendor", queryParams);
      Assert.assertNotNull(
          "Tax doesn't exist" + vendortaxesMap.toString(), vendorTaxesGeneralModel);
    }
  }

  private Object[] constructVendorTaxesQueryParams(
      String vendorCodeName, Map<String, String> vendorTaxesValues) {
    return new Object[] {
      vendorCodeName.split(" - ")[0],
      vendorCodeName.split(" - ")[1],
      vendorTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX).split(" - ")[0],
      vendorTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX).split(" - ")[1],
      vendorTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }

  public void assertThatTheFollowingCompanyTaxDetailsExist(
      String companyCodeName, DataTable taxDetailsDataTable) {
    List<Map<String, String>> taxDetailsMaps =
        taxDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> taxDetailsMap : taxDetailsMaps) {
      assertThatCompanyTaxDetailsExists(companyCodeName, taxDetailsMap);
    }
  }

  private void assertThatCompanyTaxDetailsExists(
      String companyCodeName, Map<String, String> taxDetailsMap) {
    Object companyTaxesDetailsGeneralModel =
        retrieveCompanyTaxDetail(companyCodeName, taxDetailsMap);
    Assert.assertNotNull("Tax detail doesn't exist", companyTaxesDetailsGeneralModel);
  }

  private Object retrieveCompanyTaxDetail(
      String companyCodeName, Map<String, String> taxDetailsMap) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCompanyTaxesDetails", constructCompanyTaxesQueryParams(companyCodeName, taxDetailsMap));
  }

  private Object[] constructCompanyTaxesQueryParams(
      String companyCodeName, Map<String, String> companyTaxesValues) {
    return new Object[] {
      companyCodeName.split(" - ")[0],
      companyCodeName.split(" - ")[1],
      companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX).split(" - ")[0],
      companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX).split(" - ")[1],
      companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }

  public void assertThatTheFollowingCompanyHasNoTaxDetails(String companyCodeName) {
    Object companyTaxesDetailsGeneralModel = retrieveCompanyTaxDetailByCodeName(companyCodeName);
    Assert.assertNull("Tax detail exist", companyTaxesDetailsGeneralModel);
  }

  private Object retrieveCompanyTaxDetailByCodeName(String companyCodeName) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCompanyTaxesDetailsByCodeName",
        constructCompanyTaxesCodeNameQueryParams(companyCodeName));
  }

  private Object[] constructCompanyTaxesCodeNameQueryParams(String companyCodeName) {
    return new Object[] {
      companyCodeName.split(" - ")[0], companyCodeName.split(" - ")[1],
    };
  }
}
