package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionViewGeneralDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObCollectionViewGeneralDataTestUtils utils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        utils = new DObCollectionViewGeneralDataTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    public DObCollectionViewGeneralDataStep() {

    When(
        "^\"([^\"]*)\" requests to view GeneralData section of Collection with code \"([^\"]*)\"$",
        (String username, String collectionCode) -> {
            Cookie userCookie = userActionsTestUtils.getUserCookie(username);
            StringBuilder readUrl = new StringBuilder();
            readUrl.append(IDObCollectionRestURLs.GET_URL+collectionCode+IDObCollectionRestURLs.GENERAL_DATA);
            Response response = utils.sendGETRequest(userCookie, String.valueOf(readUrl));
            userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of GeneralData section for Collection with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String collectionCode, String username, DataTable collectionGeneralDataDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(username);
            utils.assertResponseSuccessStatus(response);
            utils.assertCorrectValuesAreDisplayedInGeneralData(response, collectionGeneralDataDataTable);
        });
  }

    @Before
    public void setup(Scenario scenario) throws Exception {

        if (contains(scenario.getName(), "View GeneralData section")) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                databaseConnector.executeSQLScript(utils.getCreateDbScriptsExecution());
                hasBeenExecutedOnce = true;
            }
        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View GeneralData section")) {
            databaseConnector.closeConnection();
        }
    }

}
