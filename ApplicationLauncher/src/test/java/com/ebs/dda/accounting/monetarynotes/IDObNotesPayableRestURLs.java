package com.ebs.dda.accounting.monetarynotes;

public interface IDObNotesPayableRestURLs {

  String QUERY = "/services/accounting/notespayable";

  String VIEW_ALL = QUERY;
}
