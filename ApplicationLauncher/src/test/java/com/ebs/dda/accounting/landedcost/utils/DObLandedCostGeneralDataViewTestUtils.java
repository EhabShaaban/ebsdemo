package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

public class DObLandedCostGeneralDataViewTestUtils extends DObLandedCostTestUtils {

    public DObLandedCostGeneralDataViewTestUtils(Map<String, Object> properties) {
        super(properties);
    }

    public void checkLandedCostGeneralDataExists(DataTable generalDataTable) {
        List<Map<String, String>> generalDataMaps = generalDataTable.asMaps(String.class, String.class);
        for (Map<String, String> generalDataMap : generalDataMaps) {
            DObLandedCostGeneralModel landedCostGeneralModel =
                    (DObLandedCostGeneralModel)
                            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                    "getLandedCostGeneralData",
                                    constructLandedCostGeneralDataQueryParams(generalDataMap));
            Assert.assertNotNull("Landed cost doesn't exist " + generalDataMap , landedCostGeneralModel);
        }
    }

    private Object[] constructLandedCostGeneralDataQueryParams(
            Map<String, String> generalDataMap) {
        return new Object[]{
                generalDataMap.get(IAccountingFeatureFileCommonKeys.LC_CODE),
                generalDataMap.get(IFeatureFileCommonKeys.TYPE),
                generalDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
                generalDataMap.get(IFeatureFileCommonKeys.CREATED_BY),
                '%' + generalDataMap.get(IFeatureFileCommonKeys.STATE) + '%',
                generalDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
                generalDataMap.get(IFeatureFileCommonKeys.PURCHASE_ORDER),
                generalDataMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
        };
    }

    public void assertCorrectValuesAreDisplayedInGeneralData(
            Response response, DataTable generalDataTable) throws Exception {
        Map<String, String> generalDataExpected =
                generalDataTable.asMaps(String.class, String.class).get(0);
        Map<String, Object> generalDataActual = getMapFromResponse(response);

        MapAssertion mapAssertion = new MapAssertion(generalDataExpected, generalDataActual);
        assertActualAndExpectedDataFromResponse(mapAssertion);
    }

    private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
        mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.LC_CODE, IDObLandedCostGeneralModel.CODE);

        mapAssertion.assertCodeAndLocalizedNameValue(
                IFeatureFileCommonKeys.TYPE,
                IDObLandedCostGeneralModel.TYPE_CODE,
                IDObLandedCostGeneralModel.TYPE_NAME);

        mapAssertion.assertValue(
                IFeatureFileCommonKeys.CREATED_BY, IDObLandedCostGeneralModel.CREATION_INFO);

        mapAssertion.assertValueContains(
                IFeatureFileCommonKeys.STATE, IDObLandedCostGeneralModel.CURRENT_STATES);

        mapAssertion.assertDateTime(
                IFeatureFileCommonKeys.CREATION_DATE, IDObLandedCostGeneralModel.CREATION_DATE);

        mapAssertion.assertValue(
                IFeatureFileCommonKeys.DOCUMENT_OWNER, IDObLandedCostGeneralModel.DOCUMENT_OWNER);


    }
}
