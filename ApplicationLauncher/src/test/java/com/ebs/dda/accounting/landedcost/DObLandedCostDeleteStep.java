package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObLandedCostDeleteStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "Delete LandedCost";
  private static boolean hasBeenExecuted = false;
  private DObLandedCostDeleteTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObLandedCostDeleteTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }


  public DObLandedCostDeleteStep() {
    When("^\"([^\"]*)\" requests to delete LandedCost with code \"([^\"]*)\"$",
        (String userName, String landedCostCode) -> {
          Response landedCostDeleteResponse =
              utils.deleteLandedCost(userActionsTestUtils.getUserCookie(userName), landedCostCode);
          userActionsTestUtils.setUserResponse(userName, landedCostDeleteResponse);
    });

    Then("^LandedCost with code \"([^\"]*)\" is deleted from the system$",
        (String landedCostCode) -> {
          utils.assertThatLandedCostNotExist(landedCostCode);
    });

    Given("^\"([^\"]*)\" first deleted the LandedCost with code \"([^\"]*)\" successfully$",
        (String userName, String landedCostCode) -> {
          Response landedCostDeleteResponse =
              utils.deleteLandedCost(userActionsTestUtils.getUserCookie(userName), landedCostCode);
          userActionsTestUtils.setUserResponse(userName, landedCostDeleteResponse);
    });

    Given("^first \"([^\"]*)\" opens \"([^\"]*)\" section of LandedCost with code \"([^\"]*)\" in edit mode$",
        (String userName, String sectionName, String landedCostCode) -> {
          Map<String, String> sectionsLockUrlsMap = utils.getSectionsLockUrlsMap();
          String lockSectionUrl =
              utils.getLockUrl(landedCostCode, sectionsLockUrlsMap.get(sectionName)).toString();
          Response lockRepponse =
              userActionsTestUtils.lockSection(userName, lockSectionUrl, landedCostCode);

          utils.assertResponseSuccessStatus(lockRepponse);
          userActionsTestUtils.setUserResponse(userName, lockRepponse);
    });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScripts());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
