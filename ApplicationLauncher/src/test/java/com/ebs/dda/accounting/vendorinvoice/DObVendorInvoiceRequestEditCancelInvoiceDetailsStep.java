package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceDetailsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceRequestEditCancelInvoiceDetailsStep extends SpringBootRunner implements En, InitializingBean {

  private DObVendorInvoiceDetailsTestUtils dObInvoiceRequestEditCancelInvoiceDetailsTestUtils;

  public DObVendorInvoiceRequestEditCancelInvoiceDetailsStep() {


    When(
        "^\"([^\"]*)\" requests to edit InvoiceDetails section of Invoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
          String lockUrl =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_INVOICE_DETAILS_SECTION;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^InvoiceDetails section of Invoice with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String invoiceCode, String userName) -> {
          dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.assertThatResourceIsLockedByUser(
              userName, invoiceCode, IDObVendorInvoiceSectionNames.INVOICE_DETAILS);
        });
    Given(
        "^\"([^\"]*)\" first opened the Invoice with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String invoiceCode) -> {
          String lockUrl =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_INVOICE_DETAILS_SECTION;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^InvoiceDetails section of Invoice with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String invoiceCode, String userName, String lockTime) -> {
          dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.freeze(lockTime);
          String lockUrl =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_INVOICE_DETAILS_SECTION;
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving InvoiceDetails section of Invoice with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String invoiceCode, String unLockTime) -> {
          dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.freeze(unLockTime);

          String unLockUrl =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.UNLOCK_INVOICE_DETAILS_SECTION;
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on InvoiceDetails section of Invoice with code \"([^\"]*)\" is released$",
        (String userName, String invoiceCode) -> {
          dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.assertThatLockIsReleased(
              userName, invoiceCode, IDObVendorInvoiceSectionNames.INVOICE_DETAILS);
        });

    When(
        "^\"([^\"]*)\" cancels saving InvoiceDetails section of Invoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
          String unLockUrl =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.UNLOCK_INVOICE_DETAILS_SECTION;
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }
    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        dObInvoiceRequestEditCancelInvoiceDetailsTestUtils = new DObVendorInvoiceDetailsTestUtils(properties);
        dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.setEntityManagerDatabaseConnector(
                entityManagerDatabaseConnector);
    }
  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit/Cancel Vendor Invoice")) {

      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(
            dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to edit/Cancel Vendor Invoice")) {

      userActionsTestUtils.unlockAllSections(
          IDObVendorInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS,
          dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.getSectionsNames());
      dObInvoiceRequestEditCancelInvoiceDetailsTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
      }
}
