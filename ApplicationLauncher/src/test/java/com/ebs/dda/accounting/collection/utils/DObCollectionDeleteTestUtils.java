package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Map;

public class DObCollectionDeleteTestUtils extends DObCollectionTestUtils {

  public DObCollectionDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }


  public Response deleteCollection(Cookie userCookie, String collectionCode) {
    String deleteURL = IDObCollectionRestURLs.COLLECTION_COMMAND + "/" + collectionCode;
    return sendDeleteRequest(userCookie, deleteURL);
  }

  public void assertThatCollectionIsDeleted(String collectionCode) {
    DObCollectionGeneralModel dObCollectionGeneralModel =
            (DObCollectionGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getCollectionByCode", collectionCode);
    Assert.assertNull(
            "Collection with code: " + collectionCode + " still exists!", dObCollectionGeneralModel);
  }
}
