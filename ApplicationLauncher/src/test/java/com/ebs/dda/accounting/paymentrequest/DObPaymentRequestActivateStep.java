package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.*;
import com.ebs.dda.accounting.vendorinvoice.utiles.*;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.math.BigDecimal;
import java.util.Map;

public class DObPaymentRequestActivateStep extends SpringBootRunner
    implements En, InitializingBean {

  public static final String ACTIVATE_SCENARIO_NAME = "Activate Payment Request";
  public static final String REQUEST_ACTIVATE_SCENARIO_NAME = "Request Activate Payment Request";
  private DObPaymentRequestPostRequestTestUtils utils;
  private DObPaymentRequestPostHappyPathTestUtils utilshp;
  private DObPaymentRequestCreateJournalEntryTestUtils createJournalEntryUtils;
  private DObPaymentRequestPostValidationTestUtils utilsval;
  private HObCommonChartOfAccountTestUtils chartOfAccountTestUtils;
  private DObPaymentRequestConcurrencyPostTestUtils utilscoc;
  private DObVendorInvoiceViewCompanyDataTestUtils vendorInvoiceViewCompanyDataTestUtils;
  private DObVendorInvoiceViewGeneralDataTestUtils vendorInvoiceViewGeneralDataTestUtils;
  private IObVendorInvoiceItemsViewTestUtils vendorInvoiceViewItemsTestUtils;
  private IObVendorInvoiceTaxesViewTestUtils vendorInvoiceViewTaxesTestUtils;
  private IObPaymentRequestViewAccountingDetailsTestUtils
      paymentRequestViewAccountingDetailsTestUtils;
  private DObVendorInvoiceViewDetailsTestUtils vendorInvoiceViewDetailsTestUtils;

  private boolean hasBeenExecutedOnce = false;

  public DObPaymentRequestActivateStep() {
    Then(
        "^PaymentRequest with Code \"([^\"]*)\" is updated as follows:$",
        (String paymentRequestCode, DataTable paymentRequestDataTable) -> {
          utils.assertThatPaymentRequestIsUpdatedSuccessfully(
              paymentRequestCode, paymentRequestDataTable);
        });

    Given(
        "^the following PaymentRequests exist:$",
        (DataTable paymentRequestDataTable) -> {
          utilshp.assertThatTheFollowingPaymentRequestExist(paymentRequestDataTable);
        });

    Given(
        "^the following PurchaseOrders have been paid by down-payments as follows:$",
        (DataTable PODownPayment) -> {
          utils.assertPurchaseOrderDownPayment(PODownPayment);
        });

    Given(
        "^the following VendorInvoices Taxes exist:$",
        (DataTable vendorInvoiceDataTable) -> {
          vendorInvoiceViewTaxesTestUtils.insertVendorInvoiceTaxes(vendorInvoiceDataTable);
        });

    When(
        "^\"([^\"]*)\" activates the PaymentRequest of type \"([^\"]*)\" of method \"([^\"]*)\" with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String paymentType,
            String method,
            String paymentCode,
            String dateTime,
            DataTable postDataTable) -> {
          utilshp.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utilshp.sendPUTRequest(
                  cookie,
                  utilshp.getActivationUrl(paymentCode, paymentType, method),
                  utilshp.getPostValueObject(postDataTable));
          userActionsTestUtils.setUserResponse(userName, response);
        });
    When(
        "^\"([^\"]*)\" activates the PaymentRequest of type \"([^\"]*)\" of method \"([^\"]*)\" Against \"([^\"]*)\" with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String paymentType,
            String method,
            String dueDocument,
            String paymentCode,
            String dateTime,
            DataTable postDataTable) -> {
          utilshp.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utilshp.sendPUTRequest(
                  cookie,
                  utilshp.getActivationUrl(paymentCode, paymentType, method, dueDocument),
                  utilshp.getPostValueObject(postDataTable));
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^ActivationDetails in PaymentRequest with Code \"([^\"]*)\" is updated as follows:$",
        (String paymentRequestCode, DataTable activationDetailsDataTable) -> {
          utilshp.assertThatPaymentRequestActivationDetailsIsCreatedSuccessfully(
              paymentRequestCode, activationDetailsDataTable);
        });

    Given(
        "^the following GLAccounts has the following Subledgers:$",
        (DataTable subAccountsDataTable) -> {
          chartOfAccountTestUtils.assertThatAccountsHasSubledger(subAccountsDataTable);
        });

    Then(
        "^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
        (String missingFields, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utilsval.assertThatResponseHasMissingFields(response, missingFields);
        });

    And(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" section of PaymentRequest with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName, String sectionName, String paymentRequestCode, String dateTime) -> {
          utilsval.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utilsval.openPaymentRequestSection(cookie, sectionName, paymentRequestCode);
          utilsval.assertResponseSuccessStatus(response);
        });

    And(
        "^Remaining amount of Invoice with code \"([^\"]*)\" is \"([^\"]*)\"$",
        (String invoiceCode, String remainingAmount) -> {
          utilsval.assertThatInvoiceRemainingIsCorrect(
              invoiceCode, new BigDecimal(remainingAmount));
        });

    Given(
        "^the GLAccounts configuration exit:$",
        (DataTable glAccountConfigDataTable) -> {
          chartOfAccountTestUtils.assetGLAccountConfigurationsExist(glAccountConfigDataTable);
        });

    Given(
        "^the following JournalEntry exist:$",
        (DataTable journalEntryDataTable) -> {
          createJournalEntryUtils.assertJournalEntriesExist(journalEntryDataTable);
        });

    Given(
        "^the following JournalEntryDetail exist:$",
        (DataTable journalDetailDataTable) -> {
          createJournalEntryUtils.assertJournalEntryDetailsExist(journalDetailDataTable);
        });

    When(
        "^the users post PaymentRequests at \"([^\"]*)\" with the following values:$",
        (String postngDate, DataTable postingDataTable) -> {
          utilscoc.freeze(postngDate);
          Map<String, Cookie> loggedInUsersCookies = userActionsTestUtils.getLoggedInUsersCookies();
          Map<String, Response> userResponses =
              utilscoc.concurrentlyPostPaymentRequests(postingDataTable, loggedInUsersCookies);
          for (String userName : userResponses.keySet()) {
            userActionsTestUtils.setUserResponse(userName, userResponses.get(userName));
          }
        });

    Then(
        "^the following JournalEntries are created as follows:$",
        (DataTable journalEntryDataTable) -> {
          utilscoc.assertJournalEntriesDataAreCreated(journalEntryDataTable);
        });

    Then(
        "^the following JournalEntries Codes are generated as follows:$",
        (DataTable journalEntryCodesDataTable) -> {
          utilscoc.assertJournalEntryCodesGeneratedSuccessfully(journalEntryCodesDataTable);
        });

    Then(
        "^a success notification is sent to \"([^\"]*)\" with the following message \"([^\"]*)\" with one of the following JournalEntry codes:$",
        (String userName, String msgCode, DataTable journalEntryCodeDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utilscoc.assertThatSuccessResponseHasMessageCode(response, msgCode);
          utilscoc.assertThatResponseHasOneJournalEntryCodeOf(response, journalEntryCodeDataTable);
        });
    And(
        "^there is no LandedCost in state \"([^\"]*)\" for the following PurchaseOrders:$",
        (String landedCostState, DataTable purchaseOrdersDataTable) -> {
          utils.assertThatNoLandedCostsExistForPurchaseOrders(
              landedCostState, purchaseOrdersDataTable);
        });
    Then(
        "^Remaining of VendorInvoice with code \"([^\"]*)\" is updated as follows \"([^\"]*)\"$",
        (String vendorInvoiceCode, String vendorInvoiceRemaining) -> {
          utils.assertThatVendorInvoiceRemainingIsUpdatedWithValue(
              vendorInvoiceCode, vendorInvoiceRemaining);
        });
    And(
        "^Remaining of PurchaseOrder with code \"([^\"]*)\" is updated as follows \"([^\"]*)\"$",
        (String purchaseOrderCode, String remaining) -> {
          utils.assertThatPORemainingMatchesExpected(purchaseOrderCode, remaining);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestPostRequestTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    utilshp = new DObPaymentRequestPostHappyPathTestUtils(getProperties());
    utilshp.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    createJournalEntryUtils = new DObPaymentRequestCreateJournalEntryTestUtils(getProperties());
    createJournalEntryUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    utilsval = new DObPaymentRequestPostValidationTestUtils(getProperties());
    chartOfAccountTestUtils = new HObCommonChartOfAccountTestUtils(getProperties());
    chartOfAccountTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    utilsval.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    utilscoc = new DObPaymentRequestConcurrencyPostTestUtils(getProperties());
    utilscoc.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    vendorInvoiceViewCompanyDataTestUtils =
        new DObVendorInvoiceViewCompanyDataTestUtils(getProperties());
    vendorInvoiceViewCompanyDataTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    vendorInvoiceViewGeneralDataTestUtils =
        new DObVendorInvoiceViewGeneralDataTestUtils(getProperties());
    vendorInvoiceViewGeneralDataTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    vendorInvoiceViewItemsTestUtils = new IObVendorInvoiceItemsViewTestUtils(getProperties());
    vendorInvoiceViewItemsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    vendorInvoiceViewTaxesTestUtils = new IObVendorInvoiceTaxesViewTestUtils(getProperties());
    vendorInvoiceViewTaxesTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    paymentRequestViewAccountingDetailsTestUtils =
        new IObPaymentRequestViewAccountingDetailsTestUtils(getProperties());
    paymentRequestViewAccountingDetailsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    vendorInvoiceViewDetailsTestUtils = new DObVendorInvoiceViewDetailsTestUtils(getProperties());
    vendorInvoiceViewDetailsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), REQUEST_ACTIVATE_SCENARIO_NAME)
        || contains(scenario.getName(), ACTIVATE_SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            DObPaymentRequestPostValidationTestUtils.getDbScriptsOneTimeExecution());
        paymentRequestViewAccountingDetailsTestUtils.executeAccountDeterminationFunction();

        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(DObLandedCostCommonTestUtils.clearLandedCosts());
      databaseConnector.executeSQLScript(
          DObVendorInvoiceViewGeneralDataTestUtils.clearVendorInvoices());
      databaseConnector.executeSQLScript(
          DObPaymentRequestPostValidationTestUtils.clearPaymentRequests());
      databaseConnector.executeSQLScript(
          DObPaymentRequestPostValidationTestUtils.clearAccountingNoteInsertions());
      databaseConnector.executeSQLScript(
          DObPaymentRequestPostValidationTestUtils.clearCollectionInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), REQUEST_ACTIVATE_SCENARIO_NAME)
        || contains(scenario.getName(), ACTIVATE_SCENARIO_NAME)) {
      userActionsTestUtils.unlockAllSections(
          IDObPaymentRequestRestURLs.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
