package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.dbo.jpa.valueobjects.IIObBusinessPartnerValueObject;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Map;

public class IObSalesInvoiceBusinessPartnerSaveHappyPathTestUtils
        extends IObSalesInvoiceBusinessPartnerTestUtils {

    public IObSalesInvoiceBusinessPartnerSaveHappyPathTestUtils(
            Map<String, Object> properties,
            EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
        super(properties, entityManagerDatabaseConnector);
    }

    public Response saveSalesInvoiceBusinessPartner(
            Cookie userCookie, String salesInvoiceCode, DataTable businessPartnerDataTable) {
        JsonObject businessPartnerObject =
                constructSalesInvoiceBusinessPartnerJsonObject(businessPartnerDataTable);
        return sendSaveRequest(userCookie, businessPartnerObject, salesInvoiceCode);
    }

    private JsonObject constructSalesInvoiceBusinessPartnerJsonObject(
            DataTable businessPartnerDataTable) {
        Map<String, String> businessPartner =
                businessPartnerDataTable.asMaps(String.class, String.class).get(0);
        JsonObject businessPartnerObject = new JsonObject();
        businessPartnerObject.addProperty(
                IIObBusinessPartnerValueObject.PAYMENT_TERM_CODE,
                businessPartner.get(ISalesFeatureFileCommonKeys.PAYMENT_TERM) != null && !businessPartner.get(ISalesFeatureFileCommonKeys.PAYMENT_TERM).isEmpty() ?
                        businessPartner.get(ISalesFeatureFileCommonKeys.PAYMENT_TERM) : null
                );
        businessPartnerObject.addProperty(
                IIObBusinessPartnerValueObject.CURRENY_CODE,
                businessPartner.get(ISalesFeatureFileCommonKeys.CURRENCY));
        return businessPartnerObject;
    }

    protected Response sendSaveRequest(
            Cookie userCookie, JsonObject businessPartnerObject, String salesInvoiceCode) {
        String restURL =
                IDObSalesInvoiceRestUrls.COMMAND_URL
                        + salesInvoiceCode
                        + IDObSalesInvoiceRestUrls.UPDATE_BUSINESS_PARTNER;

        return sendPUTRequest(userCookie, restURL, businessPartnerObject);
    }

    public void assertThatSalesInvoiceUpdated(String salesInvoiceCode, DataTable salesInvoiceData) {
        Map<String, String> salesInvoiceMap =
                salesInvoiceData.asMaps(String.class, String.class).get(0);
        DObSalesInvoiceGeneralModel salesInvoice =
                (DObSalesInvoiceGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getUpdatedSalesInvoice",
                                constructSalesInvoiceParameters(salesInvoiceCode, salesInvoiceMap));
        Assert.assertNotNull(salesInvoice);
    }

    private Object[] constructSalesInvoiceParameters(
            String salesInvoiceCode, Map<String, String> salesInvoiceMap) {
        return new Object[]{
                salesInvoiceCode,
                salesInvoiceMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                salesInvoiceMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE)
        };
    }

    public void assertThatSalesInvoiceBusinessPartnerUpdated(DataTable businessPartnerDataTable) {

        Map<String, String> businessPartnerMap =
                businessPartnerDataTable.asMaps(String.class, String.class).get(0);
        businessPartnerMap = convertEmptyStringsToNulls(businessPartnerMap);
        IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartnerGeneralModel =
                (IObSalesInvoiceBusinessPartnerGeneralModel)
                        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                                "getUpdatedBusinessPartner",
                                constructSalesInvoiceBusinessPartner(businessPartnerMap));

        Assert.assertNotNull(salesInvoiceBusinessPartnerGeneralModel);
    }

    private Object[] constructSalesInvoiceBusinessPartner(Map<String, String> businessPartnerMap) {
        return new Object[]{
                businessPartnerMap.get(ISalesFeatureFileCommonKeys.SALES_ORDER),
                businessPartnerMap.get(ISalesFeatureFileCommonKeys.PAYMENT_TERM),
                businessPartnerMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
                businessPartnerMap.get(IFeatureFileCommonKeys.CUSTOMER),
                businessPartnerMap.get(ISalesFeatureFileCommonKeys.CURRENCY),
                Double.parseDouble(businessPartnerMap.get(ISalesFeatureFileCommonKeys.DOWN_PAYMENT)),
                businessPartnerMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
                businessPartnerMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE)
        };
    }
}
