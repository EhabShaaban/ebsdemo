package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

public class DObSalesInvoiceDropDownTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceDropDownTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertThatTheFollowingSalesInvoicesExist(DataTable salesInvoicesDataTable) {
    List<Map<String, String>> salesInvoicesMap =
        salesInvoicesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceMap : salesInvoicesMap) {

      Object[] queryParams = constructSalesInvoiceQueryParams(salesInvoiceMap);
      DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult(
                  "getSalesInvoicesByCodeAndStateAndPurchaseUnitAndCurrency", queryParams);
      assertNotNull(dObSalesInvoiceGeneralModel);
    }
  }

  public Object[] constructSalesInvoiceQueryParams(Map<String, String> salesInvoiceMap) {
    return new Object[] {salesInvoiceMap.get(IFeatureFileCommonKeys.CODE),
        "%" + salesInvoiceMap.get(IFeatureFileCommonKeys.STATE) + "%",
        salesInvoiceMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        salesInvoiceMap.get(ISalesFeatureFileCommonKeys.CUSTOMER)};
  }

  public void assertThatSalesInvoicesResponseIsCorrect(DataTable salesInvoicesDataTable,
      Response response) throws Exception {

    List<Map<String, String>> salesInvoicesMap =
        salesInvoicesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> responseInvoicesMap = getListOfMapsFromResponse(response);

    for (int i = 0; i < salesInvoicesMap.size(); i++) {

      assertThatExpectedSalesInvoiceMathcesActualOne(salesInvoicesMap, responseInvoicesMap, i);
    }
  }

  private void assertThatExpectedSalesInvoiceMathcesActualOne(
      List<Map<String, String>> salesInvoicesMap, List<HashMap<String, Object>> responseInvoicesMap,
      int i) throws Exception {
    MapAssertion mapAssertion =
        new MapAssertion(salesInvoicesMap.get(i), responseInvoicesMap.get(i));

    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE,
        IDObSalesInvoiceGeneralModel.INVOICE_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObSalesInvoiceGeneralModel.PURCHASE_UNIT_CODE,
        IDObSalesInvoiceGeneralModel.PURCHASE_UNIT_NAME);
  }

  public String getReadAllSalesInvoicesUrl() {
    return IDObCollectionRestURLs.READ_ALL_SALES_INVOICES;
  }

  public String getReadAllSalesInvoicesUrl(String businessUnitCode, String customerCode) {
    return IDObCollectionRestURLs.READ_ALL_SALES_INVOICES + "/" + businessUnitCode + "/"
        + customerCode;
  }

  public void assertThatTotalNumberOfRecordsInResponseIsCorrect(Response response,
      Integer numberOfRecords) {
    List<HashMap<String, Object>> listOfMapsFromResponse = getListOfMapsFromResponse(response);
    assertEquals(numberOfRecords.intValue(), listOfMapsFromResponse.size());
  }
}
