package com.ebs.dda.accounting.costing.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.collection.utils.DObCollectionTestUtils;
import com.ebs.dda.accounting.costing.utils.DObCostingCommonTestUtils;

import java.util.Map;

public class DObCostingAllowedActionsObjectScreenTestUtils extends DObCostingCommonTestUtils {

  public DObCostingAllowedActionsObjectScreenTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

    public String getDbScriptPath() {

      StringBuilder str = new StringBuilder();
      str.append("db-scripts/inventory/goods-receipt/goodsreceipt_clear.sql");
      str.append(",")
              .append("db-scripts/accounting/costing/costing_clear.sql");

      return str.toString();
    }
}
