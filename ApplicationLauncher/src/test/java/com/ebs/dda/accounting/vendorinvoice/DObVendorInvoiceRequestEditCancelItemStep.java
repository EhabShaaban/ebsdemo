package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceRequestEditCancelItemTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceItemsDeleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceRequestEditCancelItemStep extends SpringBootRunner
    implements En, InitializingBean {

  private static boolean hasBeenExecutedOnce = false;
  private DObVendorInvoiceRequestEditCancelItemTestUtils invoiceRequestEditCancelItemTestUtils;
  private IObVendorInvoiceItemsDeleteTestUtils itemsDeleteUtils;

  public DObVendorInvoiceRequestEditCancelItemStep() {

    Given(
        "^VendorInvoice with code \"([^\"]*)\" has the following OrderItems:$",
        (String vendorInvoiceCode, DataTable vendorInvoicesItems) -> {
          invoiceRequestEditCancelItemTestUtils.assertVendorInvoiceItemExists(
              vendorInvoiceCode, vendorInvoicesItems);
        });

    When(
        "^\"([^\"]*)\" requests to edit Item with id \"([^\"]*)\" in VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\"$",
        (String userName, String itemId, String invoiceCode) -> {
          String lockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^edit Item dialoge is opened and VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String invoiceCode, String userName) -> {
          invoiceRequestEditCancelItemTestUtils.assertThatResourceIsLockedByUser(
              userName, invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
        });


    Given(
        "^\"([^\"]*)\" first requested to edit Item with id \"([^\"]*)\" in VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\" successfully$",
        (String userName, String itemId, String invoiceCode) -> {
          String lockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String invoiceCode, String userName, String dateTime) -> {
          invoiceRequestEditCancelItemTestUtils.freeze(dateTime);
          String lockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, invoiceCode);
          invoiceRequestEditCancelItemTestUtils.assertResponseSuccessStatus(response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\" is released$",
        (String userName, String invoiceCode) -> {
          invoiceRequestEditCancelItemTestUtils.assertThatLockIsReleased(
              userName, invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving VendorInvoiceItems section of Vendor Invoice Item with id \"([^\"]*)\" in VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemId, String invoiceCode, String dateTime) -> {
          invoiceRequestEditCancelItemTestUtils.freeze(dateTime);
          String unlockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.UNLOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving VendorInvoiceItems section of Vendor Invoice Item with id \"([^\"]*)\" in VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\"$",
        (String userName, String itemId, String invoiceCode) -> {
          String unlockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.UNLOCK_ITEM_SECTION
                  + "/"
                  + itemId;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" first deleted the Vendor Invoice Item with id \"([^\"]*)\" in VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String itemId, String invoiceCode, String dateTime) -> {
          invoiceRequestEditCancelItemTestUtils.freeze(dateTime);
          itemsDeleteUtils.deleteItemById(
              userActionsTestUtils.getUserCookie(userName), invoiceCode, itemId);
          itemsDeleteUtils.assertItemIsDeletedById(itemId);
        });
    And(
        "^\"([^\"]*)\" first deleted the Vendor Invoice Item with id \"([^\"]*)\" in VendorInvoiceItems section of Vendor Invoice with code \"([^\"]*)\"$",
        (String userName, String itemId, String invoiceCode) -> {
          itemsDeleteUtils.deleteItemById(
              userActionsTestUtils.getUserCookie(userName), invoiceCode, itemId);
          itemsDeleteUtils.assertItemIsDeletedById(itemId);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    invoiceRequestEditCancelItemTestUtils =
        new DObVendorInvoiceRequestEditCancelItemTestUtils(properties);
    itemsDeleteUtils = new IObVendorInvoiceItemsDeleteTestUtils(properties);
    itemsDeleteUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    invoiceRequestEditCancelItemTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Edit/Cancel Item to Vendor Invoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            invoiceRequestEditCancelItemTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(DObVendorInvoiceTestUtils.clearVendorInvoices());
      databaseConnector.executeSQLScript(
          invoiceRequestEditCancelItemTestUtils.getDbScriptsEveryTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to Edit/Cancel Item to Vendor Invoice")) {
      invoiceRequestEditCancelItemTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSectionsNewStandard(
          IDObVendorInvoiceRestUrls.COMMAND_URL,
          IDObVendorInvoiceRestUrls.UNLOCK_ALL_SECTION,
          invoiceRequestEditCancelItemTestUtils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
