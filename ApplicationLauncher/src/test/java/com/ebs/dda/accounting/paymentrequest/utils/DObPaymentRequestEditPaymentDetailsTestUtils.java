package com.ebs.dda.accounting.paymentrequest.utils;

import java.util.Map;

import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;

public class DObPaymentRequestEditPaymentDetailsTestUtils
				extends DObPaymentRequestPaymentDetailsTestUtils {

	public DObPaymentRequestEditPaymentDetailsTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public String getUnlockPaymentDetailsUrl(String code) {
		return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND + "/" + code
						+ IDObPaymentRequestRestURLs.UNLOCK_PAYMENT_DETAILS_SECTION;
	}
}
