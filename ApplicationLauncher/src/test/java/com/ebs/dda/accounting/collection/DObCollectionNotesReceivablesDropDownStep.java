package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionNotesReceivablesDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionNotesReceivablesDropDownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObCollectionNotesReceivablesDropDownTestUtils utils;

  public DObCollectionNotesReceivablesDropDownStep() {

    When(
        "^\"([^\"]*)\" requests to read all NotesReceivables with state Active according to user's BusinessUnit$",
        (String userName) -> {
          String url = IDObCollectionRestURLs.NOTES_RECEIVABLES;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following NotesReceivables for Collections values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable notesReceivablesDT) -> {
          utils.assertThatNotesReceivablesAreRetrieved(
              userActionsTestUtils.getUserResponse(userName), notesReceivablesDT);
        });

    Then(
        "^total number of NotesReceivables for Collections returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer numberOfReturnedNotesReceivables) -> {
          utils.assertTotalNumberOfRecordsEqualResponseSize(
              userActionsTestUtils.getUserResponse(userName), numberOfReturnedNotesReceivables);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObCollectionNotesReceivablesDropDownTestUtils(properties);
    utils.setProperties(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read All NotesReceivables Dropdown in Collection")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read All NotesReceivables Dropdown in Collection")) {
      databaseConnector.closeConnection();
    }
  }
}
