package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotesReceivableReferenceDocumentTypeDropdownTestUtils
    extends DObMonetaryNotesCommonTestUtils {

  protected String ASSERTION_MSG = "[NotesReceivable][ReferenceDocument][Type][Dropdown]";

  public NotesReceivableReferenceDocumentTypeDropdownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readAllReferenceDocumentTypes(Cookie cookie) {
    String restURL = IDObNotesReceivableRestURLs.READ_ALL_REFERENCE_DOCUMENT_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatReferenceDocumentTypesResponseIsCorrect(
      DataTable referenceDocumentTypesDataTable, Response response) {
    List<Map<String, String>> expectedReferenceDocumentTypeMapList =
        referenceDocumentTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualReferenceDocumentTypesList =
        getListOfMapsFromResponse(response);
    Assert.assertEquals(
        expectedReferenceDocumentTypeMapList.size(), actualReferenceDocumentTypesList.size());
    for (int i = 0; i < expectedReferenceDocumentTypeMapList.size(); i++) {
      assertThatExpectedReferenceDocumentTypeMatchesActual(
          expectedReferenceDocumentTypeMapList.get(i), actualReferenceDocumentTypesList.get(i));
    }
  }

  private void assertThatExpectedReferenceDocumentTypeMatchesActual(
      Map<String, String> expectedReferenceDocumentTypeMap, Object actualReferenceDocumentType) {
    String expectedReferenceDocumentType = expectedReferenceDocumentTypeMap.get(IAccountingFeatureFileCommonKeys.DOCUMENT_TYPE);
    Assert.assertEquals(
            ASSERTION_MSG + " expectedReferenceDocumentType is " + expectedReferenceDocumentType + " but actual is " + actualReferenceDocumentType, expectedReferenceDocumentType,
        actualReferenceDocumentType);
  }

  public void assertThatTotalNumberOfReferenceDocumentTypesEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualReferenceDocumentTypes =
        getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of ReferenceDocument Types is not correct",
        expectedTotalNumber.intValue(),
        actualReferenceDocumentTypes.size());
  }
}
