package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.rest.CommonControllerUtils;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DObPaymentRequestConcurrencyPostTestUtils
    extends DObPaymentRequestPostCommonTestUtils {

  public DObPaymentRequestConcurrencyPostTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public static String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/journal-entry-items.sql");
    return str.toString();
  }

  public void assertThatPaymentRequestIsUpdatedSuccessfully(
      String paymentRequestCode, DataTable paymentRequestDataTable) {
    Map<String, String> paymentRequestMap =
        paymentRequestDataTable.asMaps(String.class, String.class).get(0);

    DObPaymentRequest dObPaymentRequest =
        (DObPaymentRequest)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedPaymentRequest",
                constructPaymentRequestQueryParams(paymentRequestCode, paymentRequestMap));
    Assert.assertNotNull(
        "Payment Request with code " + paymentRequestCode + " is not updated successfully",
        dObPaymentRequest);
  }

  public Object[] constructPaymentRequestQueryParams(
          String paymentRequestCode, Map<String, String> paymentRequestMap) {
    return new Object[]{
            paymentRequestCode,
            paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            "%" + paymentRequestMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatPaymentRequestActivationDetailsIsCreatedSuccessfully(
          String paymentRequestCode, DataTable activationDetailsDataTable) {
    Map<String, String> postingDetailsMap =
            activationDetailsDataTable.asMaps(String.class, String.class).get(0);

    IObAccountingDocumentActivationDetailsGeneralModel postingDetails =
            (IObAccountingDocumentActivationDetailsGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getPaymentRequestActivationDetails",
                            constructPaymentRequestActivationDetailsQueryParams(
                                    paymentRequestCode, postingDetailsMap));
    Assert.assertNotNull(
              "Payment Request Posting Details for PR with code "
                      + paymentRequestCode
                      + " is not created successfully",
              postingDetails);
  }

  public Object[] constructPaymentRequestActivationDetailsQueryParams(
          String paymentRequestCode, Map<String, String> postingDetailsMap) {
    return new Object[]{
            paymentRequestCode,
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.ACCOUNTANT),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.POSTING_DATE),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
            postingDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_LATEST_EXCHANGE_RATE_CODE),
            new BigDecimal(postingDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE))
    };
  }

  public void assertThatTheFollowingPaymentRequestExist(DataTable paymentRequestDataTable) {
    List<Map<String, String>> paymentRequestListMap =
        paymentRequestDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prPaymentDetailsMap : paymentRequestListMap) {
      IObPaymentRequestPaymentDetailsGeneralModel prPaymentDetails =
          (IObPaymentRequestPaymentDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPaymentRequestPaymentDetails",
                  constructPaymentRequestPaymentDetailsQueryParams(prPaymentDetailsMap));
      Assert.assertNotNull(
          "Payment Request Payment Details for PR with code "
              + prPaymentDetailsMap.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist",
          prPaymentDetails);
    }
  }

  private Object[] constructPaymentRequestPaymentDetailsQueryParams(
      Map<String, String> prPaymentDetailsMap) {
    return new Object[] {
      prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE),
      prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT),
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.CODE),
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + prPaymentDetailsMap.get(IFeatureFileCommonKeys.STATE) + "%",
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.TYPE),
      new BigDecimal(prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT))
    };
  }

  public void assertThatResponseHasLatestExchangeRate(Response response, String exchangeRateCode) {
    HashMap<String, Object> responseExchangeRate = response.body().jsonPath().get("data");
    String responseExchangeRateCode = responseExchangeRate.get("userCode").toString();
    assertEquals(responseExchangeRateCode, exchangeRateCode);
  }

  public void assertRemainingOfInvoiceIsCorrect(
      String invoiceCode, DataTable invoiceRemainingTable) {
    List<Map<String, String>> invoiceRemainingListMap =
        invoiceRemainingTable.asMaps(String.class, String.class);
    Map<String, String> invoiceMap = invoiceRemainingListMap.get(0);
    IObInvoiceSummaryGeneralModel invoiceRemainingGenereModel =
        (IObInvoiceSummaryGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getInvoiceSummaryByCodeAndRemaining", invoiceCode);
    BigDecimal invoiceActualRemaining = new BigDecimal(invoiceRemainingGenereModel.getTotalRemaining());
    BigDecimal invoiceExpectedRemaining =
        new BigDecimal(invoiceMap.get(IAccountingFeatureFileCommonKeys.AMOUNT_REMAINING));

    Assert.assertEquals(
        "invoice remaining not updated for invoice with code " + invoiceCode,
        invoiceExpectedRemaining,
        invoiceActualRemaining);
  }

  public void assertJournalEntriesDataAreCreated(DataTable journalEntryDataTable) {
    List<Map<String, String>> journalEntryMaps =
        journalEntryDataTable.asMaps(String.class, String.class);
    for (Map<String, String> journalEntry : journalEntryMaps) {
      assertJournalEntryDataIsCreated(journalEntry);
    }
  }

  private void assertJournalEntryDataIsCreated(Map<String, String> journalEntryMap) {
    DObJournalEntry journalEntry =
        (DObJournalEntry)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getConcurrentlyCreatedPaymentRequestJournalEntry",
                constructJournalEntryQueryParams(journalEntryMap));
    Assert.assertNotNull(
        "Journal Entry for document with code "
            + journalEntryMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_CODE)
            + " was not created!",
        journalEntry);
  }

  private Object[] constructJournalEntryQueryParams(Map<String, String> journalEntryMap) {
    return new Object[] {
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_NAME),
      journalEntryMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      journalEntryMap.get(IFeatureFileCommonKeys.CREATED_BY),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_CODE),
      journalEntryMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_COMPANY)
    };
  }

  public void assertJournalEntryCodesGeneratedSuccessfully(DataTable journalEntryCodesDataTable) {
    List<Map<String, String>> journalEntryCodesMaps =
        journalEntryCodesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> journalEntryCodeMap : journalEntryCodesMaps) {
      assertJournalEntryCodeIsGenerated(journalEntryCodeMap.get(IFeatureFileCommonKeys.CODE));
    }
  }

  private void assertJournalEntryCodeIsGenerated(String journalEntryCode) {
    DObJournalEntry journalEntry =
        (DObJournalEntry)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getConcurrentlyGeneratedPaymentRequestJournalEntryByCode", journalEntryCode);
    Assert.assertNotNull("Journal Entry with code "+journalEntryCode+" was not created!", journalEntry);
  }

  public Map<String, Response> concurrentlyPostPaymentRequests(
      DataTable postingDataTable, Map<String, Cookie> loggedInUsersCookies)
      throws InterruptedException {
    List<Map<String, String>> postingDataMaps = postingDataTable.asMaps(String.class, String.class);
    Map<String, DObPaymentRequestMultithreadingPostTestUtils> multithreadingPostTestUtilsMap =
        new HashMap<>();
    for (Map<String, String> postingDataMap : postingDataMaps) {
      String userName = postingDataMap.get(IFeatureFileCommonKeys.USER);
      String prCode = postingDataMap.get(IAccountingFeatureFileCommonKeys.PR_CODE);
      String paymentType = postingDataMap.get(IAccountingFeatureFileCommonKeys.PAYMENT_TYPE);
      String paymentForm = postingDataMap.get(IAccountingFeatureFileCommonKeys.PAYMENT_FORM);
      String dueDocumentType = postingDataMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE);
      Cookie userCookie = loggedInUsersCookies.get(userName);
      DObPaymentRequestMultithreadingPostTestUtils paymentRequestMultithreadingPostTestUtils =
          new DObPaymentRequestMultithreadingPostTestUtils(this);
      paymentRequestMultithreadingPostTestUtils.setUserCookie(userCookie);
      paymentRequestMultithreadingPostTestUtils.setPaymentRequestCode(prCode);
      paymentRequestMultithreadingPostTestUtils.setPaymentType(paymentType);
      paymentRequestMultithreadingPostTestUtils.setPaymentForm(paymentForm);
      paymentRequestMultithreadingPostTestUtils.setDueDocumentType(dueDocumentType);
      paymentRequestMultithreadingPostTestUtils.setPostDataMap(postingDataMap);
      multithreadingPostTestUtilsMap.put(userName, paymentRequestMultithreadingPostTestUtils);
    }
    return startAndJoinPostingThreads(multithreadingPostTestUtilsMap);
  }

  private Map<String, Response> startAndJoinPostingThreads(
      Map<String, DObPaymentRequestMultithreadingPostTestUtils> multithreadingPostTestUtilsMap)
      throws InterruptedException {
    Map<String, Response> postingResponses = new HashMap<>();
    for (DObPaymentRequestMultithreadingPostTestUtils postingThread :
        multithreadingPostTestUtilsMap.values()) {
      postingThread.start();
    }

    for (DObPaymentRequestMultithreadingPostTestUtils postingThread :
        multithreadingPostTestUtilsMap.values()) {
      postingThread.join();
    }

    for (Map.Entry<String, DObPaymentRequestMultithreadingPostTestUtils> postingThreadEntry :
        multithreadingPostTestUtilsMap.entrySet()) {
      postingResponses.put(
          postingThreadEntry.getKey(), postingThreadEntry.getValue().getResponse());
    }

    return postingResponses;
  }

  public void assertThatResponseHasOneJournalEntryCodeOf(
      Response response, DataTable journalEntryCodeDataTable) {
    List<Map<String, String>> journalEntryCodeMaps =
        journalEntryCodeDataTable.asMaps(String.class, String.class);
    Boolean isCorrectCode = false;
    for (Map<String, String> codeMap : journalEntryCodeMaps) {
      String code = codeMap.get(IFeatureFileCommonKeys.CODE);
      String responseCode = response.body().jsonPath().get(CommonControllerUtils.USER_CODE_KEY);
      isCorrectCode = isCorrectCode || code.equals(responseCode);
    }
    assertTrue(isCorrectCode, "Journal Entry Code is not generated correctly");
  }
}
