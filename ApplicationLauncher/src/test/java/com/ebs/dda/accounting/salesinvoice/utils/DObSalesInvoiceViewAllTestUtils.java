package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.CObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceBasicGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerGeneralModel;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesInvoiceViewAllTestUtils extends DObSalesInvoiceTestUtils {

  public DObSalesInvoiceViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/CObControlPointSqlScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");

    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesOrderSalesInvoiceBusinessPartner_viewAll.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");

    return str.toString();
  }

  public void assertThatCustomerExist(DataTable customersDataTable) {

    List<Map<String, String>> customersList = customersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> customer : customersList) {

      String customerCode = customer.get(IFeatureFileCommonKeys.CODE);
      String customerName = customer.get(IFeatureFileCommonKeys.NAME);

      MObCustomerGeneralModel mObCustomerGeneralModel =
          (MObCustomerGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getCustomerByCodeAndName", customerCode, customerName);

      assertNotNull(mObCustomerGeneralModel);
    }
  }

  public void assertThatCollectionResponsiblesExist(DataTable collectionResponsiblesDataTable) {
    List<Map<String, String>> collectionResponsiblesList =
        collectionResponsiblesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionResponsibles : collectionResponsiblesList) {

      String collectionResponsiblesCode = collectionResponsibles.get(IFeatureFileCommonKeys.CODE);
      String collectionResponsiblesName = collectionResponsibles.get(IFeatureFileCommonKeys.NAME);

      CObEnterprise cObEnterprise =
          (CObEnterprise)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getcollectionResponsiblesByCodeAndName",
                  collectionResponsiblesCode,
                  collectionResponsiblesName);

      assertNotNull(cObEnterprise);
    }
  }

  public void assertThatSalesInvoiceTypesExist(DataTable salesInvoiceTypesDataTable) {
    List<Map<String, String>> salesInvoiceTypesList =
        salesInvoiceTypesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceTypes : salesInvoiceTypesList) {

      String salesInvoiceTypesCode = salesInvoiceTypes.get(IFeatureFileCommonKeys.CODE);
      String salesInvoiceTypesName = salesInvoiceTypes.get(IFeatureFileCommonKeys.NAME);

      CObSalesInvoice cobSalesInvoice =
          (CObSalesInvoice)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceTypesByCodeAndName",
                  salesInvoiceTypesCode,
                  salesInvoiceTypesName);

      assertNotNull(cobSalesInvoice);
    }
  }

  public void assertThatSalesInvoicesExist(DataTable salesInvoicesDataTable) {

    List<Map<String, String>> salesInvoicesList =
        salesInvoicesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceMap : salesInvoicesList) {

      String salesInvoiceCreationDate = salesInvoiceMap.get(IFeatureFileCommonKeys.CREATION_DATE);

      DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
          (DObSalesInvoiceGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoices", getViewAllQueryParams(salesInvoiceMap));
      assertDateEquals(salesInvoiceCreationDate, dObSalesInvoiceGeneralModel.getCreationDate());
      assertNotNull(dObSalesInvoiceGeneralModel);
    }
  }

  public void assertThatSalesInvoicesExistWithTheFollowingNumberOfRecords(
      DataTable salesInvoicesDataTable, Integer recordNumber) {

    List<Map<String, String>> salesInvoicesList =
        salesInvoicesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesInvoiceMap : salesInvoicesList) {

      String salesInvoiceCreationDate = salesInvoiceMap.get(IFeatureFileCommonKeys.CREATION_DATE);

      DObSalesInvoiceBasicGeneralModel dObSalesInvoiceGeneralModel =
          (DObSalesInvoiceBasicGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getBasicSalesInvoices", getViewAllQueryParams(salesInvoiceMap));
      assertDateEquals(salesInvoiceCreationDate, dObSalesInvoiceGeneralModel.getCreationDate());
      assertNotNull(dObSalesInvoiceGeneralModel);
      assertEquals(salesInvoicesList.size(), recordNumber.intValue());
    }
  }

  private Object[] getViewAllQueryParams(Map<String, String> salesInvoiceMap) {
    return new Object[] {
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.SALES_ORDER).isEmpty()
          ? null
          : salesInvoiceMap.get(ISalesFeatureFileCommonKeys.SALES_ORDER),
      salesInvoiceMap.get(IFeatureFileCommonKeys.CODE),
      "%" + salesInvoiceMap.get(IFeatureFileCommonKeys.STATE) + "%",
      salesInvoiceMap.get(IFeatureFileCommonKeys.TYPE),
      salesInvoiceMap.get(IFeatureFileCommonKeys.PURCHASE_UNIT),
      salesInvoiceMap.get(IFeatureFileCommonKeys.COMPANY),
      salesInvoiceMap.get(ISalesFeatureFileCommonKeys.CUSTOMER),
      salesInvoiceMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)
    };
  }

  public void assertThatSalesInvoiceMatches(Response response, DataTable salesInvoicesDataTable)
      throws Exception {
    List<Map<String, String>> expectedSalesInvoices =
        salesInvoicesDataTable.asMaps(String.class, String.class);

    assertResponseSuccessStatus(response);
    assertCorrectNumberOfRows(response, expectedSalesInvoices.size());

    List<HashMap<String, Object>> listOfSalesInvoices = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedSalesInvoices.size(); i++) {
      assertThatSalesInvoicesMatchesTheExpected(
          expectedSalesInvoices.get(i), listOfSalesInvoices.get(i));
    }
  }

  private void assertThatSalesInvoicesMatchesTheExpected(
      Map<String, String> expectedSalesInvoice, HashMap<String, Object> actualSalesInvoice)
      throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedSalesInvoice, actualSalesInvoice);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObSalesInvoiceGeneralModel.INVOICE_CODE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.TYPE,
        IDObSalesInvoiceGeneralModel.INVOICE_TYPE_CODE,
        IDObSalesInvoiceGeneralModel.INVOICE_TYPE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.PURCHASE_UNIT,
        IDObSalesInvoiceGeneralModel.PURCHASE_UNIT_CODE,
        IDObSalesInvoiceGeneralModel.PURCHASE_UNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        IDObSalesInvoiceGeneralModel.COMPANY_CODE,
        IDObSalesInvoiceGeneralModel.COMPANY);

    mapAssertion.assertCodeAndLocalizedNameValue(
        ISalesFeatureFileCommonKeys.CUSTOMER,
        IDObSalesInvoiceGeneralModel.CUSTOMER_CODE,
        IDObSalesInvoiceGeneralModel.CUSTOMER_NAME);

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObSalesInvoiceGeneralModel.CREATION_DATE);

    assertEquals(
        expectedSalesInvoice.get(ISalesFeatureFileCommonKeys.SALES_ORDER).isEmpty()
            ? null
            : expectedSalesInvoice.get(ISalesFeatureFileCommonKeys.SALES_ORDER),
        actualSalesInvoice.get(IDObSalesInvoiceGeneralModel.SALES_ORDER));

    assertThatStatesArrayContainsValue(
        getStatesArray(
            String.valueOf(
                actualSalesInvoice.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME))),
        expectedSalesInvoice.get(IFeatureFileCommonKeys.STATE));
  }
}
