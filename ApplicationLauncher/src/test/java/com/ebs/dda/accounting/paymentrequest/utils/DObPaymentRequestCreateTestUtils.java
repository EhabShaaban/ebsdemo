package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetails;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class DObPaymentRequestCreateTestUtils extends DObPaymentRequestCommonTestUtils {

	private DObPaymentRequestViewCompanyDataTestUtils companyDataTestUtils;

	public static String getPaymentRequestDbScriptsOneTimeExecution() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/clearDataSqlScript.sql");
		str.append("," + "db-scripts/master-data/employee/MObEmployee_Clear.sql");
		str.append("," + "db-scripts/CObBankSqlScript.sql");
		str.append("," + "db-scripts/CObCurrencySqlScript.sql");
		str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
		str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
		str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
		str.append(",").append("db-scripts/master-data/item/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
		str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
		str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
		str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
		str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
		str.append("," + "db-scripts/master-data/vendor/IObVendorAccountingInfo_ViewAll.sql");
		str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
		str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
		return str.toString();
	}

	public DObPaymentRequestCreateTestUtils(Map<String, Object> properties) {
		super(properties);
		this.urlPrefix = (String) getProperty(URL_PREFIX);
	}

	private static JsonObject preparePaymentRequestJsonObject(DataTable paymentRequestDataTable) {
		Map<String, String> paymentRequest = paymentRequestDataTable
						.asMaps(String.class, String.class).get(0);
		JsonObject paymentRequestJsonObject = new JsonObject();
		setValueObjectBusinessUnit(paymentRequest, paymentRequestJsonObject);
		setValueObjectPaymentForm(paymentRequest, paymentRequestJsonObject);
		setValueObjectPaymentType(paymentRequest, paymentRequestJsonObject);
		setValueObjectBusinessPartner(paymentRequest, paymentRequestJsonObject);
		setValueObjectRefDocumentType(paymentRequest, paymentRequestJsonObject);
		setValueObjectRefDocumentCode(paymentRequest, paymentRequestJsonObject);
		setValueObjectCompany(paymentRequest, paymentRequestJsonObject);
		setValueObjectDocumentOwner(paymentRequest, paymentRequestJsonObject);
		return paymentRequestJsonObject;
	}

	private static void setValueObjectPaymentForm(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		addValueToJsonObject(paymentRequestJsonObject,
						IDObPaymentRequestCreateValueObject.PAYMENT_FORM,
						paymentRequest.get(IAccountingFeatureFileCommonKeys.PAYMENT_FORM));
	}

	private static void setValueObjectRefDocumentCode(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		addValueToJsonObject(paymentRequestJsonObject,
						IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_CODE,
						paymentRequest.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT));
	}

	private static void setValueObjectPaymentType(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		addValueToJsonObject(paymentRequestJsonObject,
						IDObPaymentRequestCreateValueObject.PAYMENT_TYPE,
						paymentRequest.get(IAccountingFeatureFileCommonKeys.PAYMENT_TYPE));
	}

	private static void setValueObjectBusinessUnit(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		String businessUnit = paymentRequest.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT);
		String businessUnitCode = businessUnit.split(" - ")[0];
		addValueToJsonObject(paymentRequestJsonObject,
						IDObPaymentRequestCreateValueObject.BUSINESSUNIT_CODE, businessUnitCode);
	}

	private static void setValueObjectBusinessPartner(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		String businessPartner = paymentRequest.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER);
		String businessPartnerCode = businessPartner.split(" - ")[0];
		if (businessPartnerCode != null  && !businessPartner.trim().isEmpty()) {
			addValueToJsonObject(paymentRequestJsonObject,
							IDObPaymentRequestCreateValueObject.BUSINESSPARTNER_CODE,
					businessPartnerCode);
		}
	}

	private static void setValueObjectDocumentOwner(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		try {
			paymentRequestJsonObject.addProperty(
					IDObPaymentRequestCreateValueObject.DOCUMENT_OWNER,
					Long.parseLong(paymentRequest.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)));
		}catch (NumberFormatException exception){
			addValueToJsonObject(paymentRequestJsonObject, IDObPaymentRequestCreateValueObject.DOCUMENT_OWNER,
					paymentRequest.get(IFeatureFileCommonKeys.DOCUMENT_OWNER));
		}
	}

	private static void setValueObjectRefDocumentType(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		String refDocumentType = paymentRequest
						.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT_TYPE);
		if (!refDocumentType.trim().isEmpty()) {
			addValueToJsonObject(paymentRequestJsonObject,
							IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_TYPE,
					refDocumentType);
		}
	}

	private static void setValueObjectCompany(Map<String, String> paymentRequest, JsonObject paymentRequestJsonObject) {
		String company = paymentRequest.get(IAccountingFeatureFileCommonKeys.COMPANY);
		String companyCode = company != null ? company.split(" - ")[0] : null;
		if (companyCode != null && !company.trim().isEmpty()) {
			addValueToJsonObject(paymentRequestJsonObject,
							IDObPaymentRequestCreateValueObject.COMPANY_CODE, companyCode);
		}
	}

	public void assertLastCreatedPaymentRequestByCode(String expectedPaymentRequestCode) {
		String actualPaymentRequestCode = (String) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getLastPaymentRequestByCode",
										expectedPaymentRequestCode);
		Assert.assertEquals(expectedPaymentRequestCode, actualPaymentRequestCode);
	}

	public Response createPaymentRequest(DataTable pymentRequestData, Cookie cookie) {
		JsonObject pymentRequest = preparePaymentRequestJsonObject(pymentRequestData);
		String restURL = IDObPaymentRequestRestURLs.CREATE_PAYMENT_REQUEST;
		return sendPostRequest(cookie, restURL, pymentRequest);
	}

	public void assertPaymentRequestIsCreated(DataTable paymentRequestTable) {
		List<Map<String, String>> PaymentRequestData = paymentRequestTable.asMaps(String.class,
						String.class);
		for (Map<String, String> paymentRequestRow : PaymentRequestData) {
			Optional<DObPaymentRequestGeneralModel> paymentRequestOptional = retrievePaymentRequestData(
							paymentRequestRow);
			Assert.assertTrue("PR with code " + paymentRequestRow.toString() + " is not presented!",
							paymentRequestOptional.isPresent());
		}
	}

	private Optional<DObPaymentRequestGeneralModel> retrievePaymentRequestData(
					Map<String, String> paymentRequestRow) {
		String paymentForm = paymentRequestRow.get(IAccountingFeatureFileCommonKeys.PAYMENT_FORM);
		String paymentType = paymentRequestRow.get(IAccountingFeatureFileCommonKeys.PAYMENT_TYPE);
		String code = paymentRequestRow.get(IFeatureFileCommonKeys.CODE);
		String state = '%' + paymentRequestRow.get(IFeatureFileCommonKeys.STATE) + '%';
		String createdBy = paymentRequestRow.get(IFeatureFileCommonKeys.CREATED_BY);
		String updatedBy = paymentRequestRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY);
		String creationDate = paymentRequestRow.get(IFeatureFileCommonKeys.CREATION_DATE);
		String updatedDate = paymentRequestRow.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE);
		String documentOwner = paymentRequestRow.get(IFeatureFileCommonKeys.DOCUMENT_OWNER);
		return Optional.ofNullable((DObPaymentRequestGeneralModel) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getPaymentRequestByAllFields", code,
										creationDate, updatedDate, createdBy, updatedBy,
										paymentType, paymentForm, state, documentOwner));
	}

	private Optional<IObPaymentRequestPaymentDetails> retrievePaymentDetailsData(
					String paymentRequestCode, Map<String, String> paymentRequestRow) {
		return Optional.ofNullable((IObPaymentRequestPaymentDetails) entityManagerDatabaseConnector
						.executeNativeNamedQuerySingleResult("getPaymentDetailsByPRCode",
										getExpectedPaymentDetailsData(paymentRequestCode,
														paymentRequestRow)));
	}

	private Object[] getExpectedPaymentDetailsData(String paymentRequestCode,
					Map<String, String> paymentRequestRow) {
		String businessPartnerCode = paymentRequestRow
						.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER);
		String refDocumentType = paymentRequestRow
						.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT_TYPE);
		String refDocumentCode = paymentRequestRow
						.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT);
		String currency = paymentRequestRow
						.get(IAccountingFeatureFileCommonKeys.CURRENCY);
		return new Object[] { businessPartnerCode, refDocumentType, refDocumentCode,currency, paymentRequestCode};
	}

	public void assertPaymentDetailsIsUpdated(String paymentRequestCode,
					DataTable paymentDetailsDataTable) {
		List<Map<String, String>> PaymentDetailsData = paymentDetailsDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> paymentDetailsRow : PaymentDetailsData) {
			Optional<IObPaymentRequestPaymentDetails> paymentDetailsOptional = retrievePaymentDetailsData(
							paymentRequestCode, paymentDetailsRow);
			Assert.assertTrue("PR with code " + paymentRequestCode + " is not presented!",
							paymentDetailsOptional.isPresent());
		}
	}

	public void assertAccountingDetailsIsUpdated(String paymentRequestCode,
					DataTable accountingDetailsDataTable) {
		List<Map<String, String>> accountingDetailsData = accountingDetailsDataTable
						.asMaps(String.class, String.class);
		for (Map<String, String> accountingDetailsRow : accountingDetailsData) {
			retrieveAccountingDetailsData(paymentRequestCode, accountingDetailsRow);
		}
	}

	public void assertThatPRCompanyDataUpdatedAsFollows(String paymentRequestCode,
					DataTable prCompanyDataTable) {
		Map<String, String> prCompanyDataMap = prCompanyDataTable.asMaps(String.class, String.class)
						.get(0);
		companyDataTestUtils.assertThatPRCompanyDataExists(paymentRequestCode, prCompanyDataMap);
	}

	public void setCompanyDataTestUtils(
					DObPaymentRequestViewCompanyDataTestUtils companyDataTestUtils) {
		this.companyDataTestUtils = companyDataTestUtils;
	}

	public void insertEmployeesData(DataTable employeesDataTable) {
		List<Map<String, String>> employeesMaps = employeesDataTable.asMaps(String.class,
						String.class);
		for (Map<String, String> employee : employeesMaps) {
			entityManagerDatabaseConnector.executeInsertQueryForObject("insertNewEmployee",
							constructInsertEmployeeParameters(employee));
		}
	}

	private Object[] constructInsertEmployeeParameters(Map<String, String> employee) {
		return new Object[] { employee.get(IFeatureFileCommonKeys.CODE),
						employee.get(IFeatureFileCommonKeys.NAME),
						employee.get(IFeatureFileCommonKeys.NAME)
		};
	}
}
