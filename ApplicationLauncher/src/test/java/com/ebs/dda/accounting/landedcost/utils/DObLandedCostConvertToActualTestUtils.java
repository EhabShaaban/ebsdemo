package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostSectionNames;
import com.ebs.dda.jpa.accounting.PurchasedItemsTotalFinalCost;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObLandedCostConvertToActualTestUtils extends CommonTestUtils {

  public DObLandedCostConvertToActualTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  static String LANDED_COST_JMX_LOCK_BEAN_NAME = "LandedCostEntityLockCommand";

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee_Clear.sql");
    str.append(",").append("db-scripts/master-data/employee/MObEmployee.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/LookupSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument_ViewAll.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder_ViewAll.sql");
    str.append(",").append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append(",").append("db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append(",").append("db-scripts/purchase-order/purchase-order-items-add-quantity.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append(",").append("db-scripts/purchase-order/IObOrderDocumentRequiredDocuments.sql");
    str.append(",").append("db-scripts/purchase-order/ServicePurchaseOrder.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts_ViewAll.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append("," + "db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append("," + "db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append("," + "db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append("," + "db-scripts/sales/IObSISOItems.sql");
    str.append("," + "db-scripts/order/salesreturnorder/CObSalesReturnOrderType.sql");
    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/DObSalesReturnOrder.sql");
    str.append(
        "," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderCompanyData.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderItems.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails_ViewAll.sql");
    str.append("," + "db-scripts/order/salesreturnorder/IObSalesReturnOrderDetails.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");

    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");

    str.append(
        "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptSalesReturnDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptCompany.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptActivationDetailsScript.sql");
    str.append("," + "db-scripts/accounting/Costing.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoice.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/DObVendorInvoiceViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxes.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxesViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyData.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyDataViewAll.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request-viewall.sql");
    str.append(",").append("db-scripts/accounting/payment-request/payment-request.sql");
    str.append(",").append("db-scripts/accounting/payment-request/iob-payment-request-payment-details.sql");
    str.append(",").append("db-scripts/accounting/payment-request/iob-payment-request-payment-details-view-all.sql");
    str.append(",").append("db-scripts/accounting/payment-request/iob-payment-request-company-data.sql");
    str.append(",").append("db-scripts/accounting/payment-request/iob-payment-request-company-data-view-all.sql");
    str.append(",").append("db-scripts/accounting/payment-request/iob-payment-request-postingdetails.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetailsViewAll.sql");
    str.append(",").append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceDetails.sql");

    return str.toString();
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostFactorItem.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCostJournalEntry.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/DObLandedCostJournalEntry_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails.sql");
    str.append(",")
        .append("db-scripts/accounting/landed-cost/IObLandedCostAccountingDetails_ViewAll.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/payment-request-journal-entry-viewall.sql");
    str.append("," + "db-scripts/accounting/journal-entry/payment-request-journal-entry.sql");
    str.append(
        ","
            + "db-scripts/accounting/journal-entry/payment-request-journal-entry-items-viewall.sql");
    str.append("," + "db-scripts/accounting/journal-entry/payment-request-journal-entry-items.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry.sql");
    str.append("," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-viewall.sql");
    str.append(
        "," + "db-scripts/accounting/journal-entry/vendor-invoice-journal-entry-items-viewall.sql");
    return str.toString();
  }

  public String costingDBSenarios() {
    return "db-scripts/accounting/landed-cost/DObLandedCost_Costing_ConvertToActual.sql";
  }

  public void assertThatLockIsReleased(String userName, String landedCostCode, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(
        userName, landedCostCode, LANDED_COST_JMX_LOCK_BEAN_NAME, sectionName);
  }

  public void assertThatResourceIsLockedByUser(
      String userName, String landedCostCode, String sectionName) throws Exception {
    assertThatResourceIsLockedByUser(
        userName, landedCostCode, sectionName, LANDED_COST_JMX_LOCK_BEAN_NAME);
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION,
            IDObLandedCostSectionNames.LANDED_COST_SECTION));
  }

  public void assertThatSectionsWithMissingFieldsExistInResponse(
      Response userResponse, DataTable sectionsWithMissingFieldsDataTable) {
    List<Map<String, String>> sections =
        sectionsWithMissingFieldsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> section : sections) {
      assertThatMissingFieldsExistInResponse(
          userResponse,
          section.get(IFeatureFileCommonKeys.SECTION_NAME),
          section.get(IFeatureFileCommonKeys.MISSING_FIELDS));
    }
  }

  public void assertThatItemsTotalCostAreExist(DataTable itemsCostDT) {
    List<Map<String, String>> itemsCost = itemsCostDT.asMaps(String.class, String.class);
    for (Map<String, String> itemCost : itemsCost) {
      Object[] quesryParams = constructQuesryParams(itemCost);
      PurchasedItemsTotalFinalCost itemCosts =
          (PurchasedItemsTotalFinalCost)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getItemTotalFinalCost", quesryParams);
      assertNotNull("Item Costs is not exist, given: " + Arrays.toString(quesryParams), itemCosts);
    }
  }

  private Object[] constructQuesryParams(Map<String, String> itemCostsMap) {
    return new Object[] {
      itemCostsMap.get(IAccountingFeatureFileCommonKeys.ITEM_CODE),
      Double.parseDouble(itemCostsMap.get(IAccountingFeatureFileCommonKeys.ITEM_FINAL_COST))
    };
  }

  public void assertThatMissingFieldsExistInResponse(
      Response response, String sectionName, String missingFields) {

    Map<String, Object> missingFieldsList = getResponseMissingFields(response);
    List<String> actualSectionMissingFields = (List<String>) missingFieldsList.get(sectionName);
    String[] expectedMissingFields = getStringAsArray(missingFields);
    Assert.assertTrue(actualSectionMissingFields.containsAll(Arrays.asList(expectedMissingFields)));
  }

  private Map<String, Object> getResponseMissingFields(Response response) {
    return response.body().jsonPath().getMap("missingFields");
  }

  private String[] getStringAsArray(String sectionMissingData) {
    return sectionMissingData.replaceAll("\\s", "").split(",");
  }
}
