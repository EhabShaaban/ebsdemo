package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCostCreationValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObLandedCostCreateHPTestUtils extends DObLandedCostTestUtils {

  public DObLandedCostCreateHPTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector dbManager) {
    super(properties);
    setEntityManagerDatabaseConnector(dbManager);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append(",").append("db-scripts/accounting/landed-cost/DObLandedCost_Create.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostDetails_Create.sql");
    str.append(",").append("db-scripts/accounting/landed-cost/IObLandedCostCompanyData_Create.sql");

    return str.toString();
  }

  public void assertLastLandedCostCodeIsExist(String code) {
    String lastCode = (String) entityManagerDatabaseConnector
        .executeNativeNamedQuerySingleResult("getLastLandedCostCode");

    assertEquals(code, lastCode);
  }

  public Response createLandedCost(Cookie userCookie, DataTable landedCostDT) {
    JsonObject valueObject = prepareValueObject(landedCostDT);
    return sendPostRequest(userCookie, IDObLandedCostRestUrls.CREATE_URL, valueObject);
  }

  private JsonObject prepareValueObject(DataTable landedCostDT) {

    Map<String, String> landedCostRecord = landedCostDT.asMaps(String.class, String.class).get(0);
    landedCostRecord = convertEmptyStringsToNulls(landedCostRecord);

    JsonObject parsedData = new JsonObject();
    addValueToJsonObject(parsedData, IDObLandedCostCreationValueObject.LANDED_COST_TYPE,
        landedCostRecord.get(IFeatureFileCommonKeys.TYPE));
    addValueToJsonObject(parsedData, IDObLandedCostCreationValueObject.PURCHASE_ORDER_CODE,
        landedCostRecord.get(IFeatureFileCommonKeys.PURCHASE_ORDER));
    try {
      parsedData.addProperty(
              IDObLandedCostCreationValueObject.DOCUMENT_OWNER,
              Long.parseLong(landedCostRecord.get(IFeatureFileCommonKeys.DOCUMENT_OWNER)));
    }catch (NumberFormatException exception){
      addValueToJsonObject( parsedData, IDObLandedCostCreationValueObject.DOCUMENT_OWNER,
              landedCostRecord.get(IFeatureFileCommonKeys.DOCUMENT_OWNER));
    }
    return parsedData;
  }

  public void assertLandedCostIsCreatedSuccessfully(DataTable newLandedCostDT) {

    List<Map<String, String>> landedCostsList = newLandedCostDT.asMaps(String.class, String.class);
    for (Map<String, String> landedCost : landedCostsList) {

      Object[] params = constructGetCreatedLandedCostQueryParams(landedCost);

      DObLandedCostGeneralModel createdLandedCost =
          (DObLandedCostGeneralModel) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("getCreatedLandedCost", params);

      assertNotNull("Landed Cost is not exist, given " + Arrays.toString(params),
          createdLandedCost);
    }
  }

  private Object[] constructGetCreatedLandedCostQueryParams(Map<String, String> landedCost) {

      return new Object[]{landedCost.get(IFeatureFileCommonKeys.CODE),
              '%' + landedCost.get(IFeatureFileCommonKeys.STATE) + '%',
              landedCost.get(IFeatureFileCommonKeys.TYPE),
              landedCost.get(IFeatureFileCommonKeys.CREATED_BY),
              landedCost.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
              landedCost.get(IFeatureFileCommonKeys.CREATION_DATE),
              landedCost.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE)};
  }

  public void assertCompanySectionIsCreatedSuccessfully(String landedCostCode,
      DataTable companyDT) {
    Map<String, String> companyMap = companyDT.asMaps(String.class, String.class).get(0);
    Object[] queryParams = constructCompanyLandedCostQueryParams(landedCostCode, companyMap);
    IObLandedCostCompanyDataGeneralModel createdLCCompanyData =
        (IObLandedCostCompanyDataGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getLandedCostCompanyData", queryParams);
    Assert.assertNotNull("Company doesn't exist in LandedCost " + Arrays.toString(queryParams),
        createdLCCompanyData);
  }

  private Object[] constructCompanyLandedCostQueryParams(String landedCostCode,
      Map<String, String> companyMap) {
    return new Object[] {landedCostCode, companyMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
        companyMap.get(IFeatureFileCommonKeys.COMPANY)};
  }

  public void assertDetailsSectionIsCreatedSuccessfully(String landedCostCode,
      DataTable detailsDT) {
    Map<String, String> detailsMap = detailsDT.asMaps(String.class, String.class).get(0);
    Object[] queryParams = constructDetailsLandedCostQueryParams(landedCostCode, detailsMap);
    IObLandedCostDetailsGeneralModel createdLCDetails =
        (IObLandedCostDetailsGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getCreatedLandedCostDetails", queryParams);
    Assert.assertNotNull(
        "Details Section doesn't exist in LandedCost " + Arrays.toString(queryParams),
        createdLCDetails);
  }

  private Object[] constructDetailsLandedCostQueryParams(String landedCostCode,
      Map<String, String> detailsMap) {
    String vendorInvoice = detailsMap.get(IFeatureFileCommonKeys.VENDOR_INVOICE);
    vendorInvoice = vendorInvoice.isEmpty() ? null : vendorInvoice;
    String dmgStkStr =
        detailsMap.get(IAccountingFeatureFileCommonKeys.DAMAGE_STOCK_INCLUDED_IN_COST);
    boolean dmgStk = dmgStkStr.equals("unchecked") ? false : true;
    return new Object[] {vendorInvoice, landedCostCode,
        detailsMap.get(IFeatureFileCommonKeys.PURCHASE_ORDER), dmgStk};
  }


}


