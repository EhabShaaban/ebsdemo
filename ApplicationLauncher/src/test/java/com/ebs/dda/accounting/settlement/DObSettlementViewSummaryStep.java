package com.ebs.dda.accounting.settlement;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.IObSettlementViewSummaryTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.clearSettlement;
import static com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution;

public class DObSettlementViewSummaryStep extends SpringBootRunner implements En {

    public static final String SCENARIO_NAME = "View Summary section in Settlement";
    private IObSettlementViewSummaryTestUtils utils;

    public DObSettlementViewSummaryStep() {
        When("^\"([^\"]*)\" requests to view Summary section of Settlement with code \"([^\"]*)\"$",
                (String userName, String settlementCode) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            Response response = utils.sendGETRequest(cookie, utils.getViewSummaryUrl(settlementCode));
            userActionsTestUtils.setUserResponse(userName, response);
        });
        Then("^the following values of Summary section of Settlement with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
                (String settlementCode, String userName, DataTable summaryDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertThatViewSettlementSummaryResponseIsCorrect(response, summaryDataTable);
        });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new IObSettlementViewSummaryTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void beforeViewAll(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.createConnection();
            databaseConnector.executeSQLScript(clearSettlement());
            utils.executeAccountDeterminationFunction();
            databaseConnector.executeSQLScript(getSettlementDbScriptsOneTimeExecution());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.closeConnection();
        }
    }
}
