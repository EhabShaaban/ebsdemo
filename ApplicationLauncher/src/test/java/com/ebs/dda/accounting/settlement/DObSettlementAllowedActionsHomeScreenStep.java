package com.ebs.dda.accounting.settlement;

import org.springframework.beans.factory.InitializingBean;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObSettlementAllowedActionsHomeScreenStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObSettlementsCommonTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObSettlementsCommonTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObSettlementAllowedActionsHomeScreenStep() {

    When("^\"([^\"]*)\" requests to read actions of Settlement home screen$", (String userName) -> {
      String readUrl = IDObSettlementRestURLs.AUTHORIZED_ACTIONS;
      Response response =
          utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
      userActionsTestUtils.setUserResponse(userName, response);

    });

    Then("^the \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String allowedActionsDataTable, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActionsDataTable);
        });

  }

}
