package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceSaveInvoiceDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceSaveInvoiceDetailsStep extends SpringBootRunner
		implements
			En, InitializingBean {

	private static boolean hasBeenExecuted = false;
	private DObVendorInvoiceSaveInvoiceDetailsTestUtils dObInvoiceSaveInvoiceDetailsTestUtils;

	public DObVendorInvoiceSaveInvoiceDetailsStep() {

		Given("^InvoiceDetails section of Invoice with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
				(String invoiceCode, String userName, String lockTime) -> {
					dObInvoiceSaveInvoiceDetailsTestUtils.freeze(lockTime);
					String lockUrl = IDObVendorInvoiceRestUrls.COMMAND_URL
							+ invoiceCode
							+ IDObVendorInvoiceRestUrls.LOCK_INVOICE_DETAILS_SECTION;
					Response response = userActionsTestUtils
							.lockSection(userName, lockUrl, invoiceCode);
					userActionsTestUtils.setUserResponse(userName, response);
				});

		When("^\"([^\"]*)\" saves InvoiceDetails section of Invoice with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
				(String userName, String invoiceCode, String saveDateTime,
						DataTable invoiceData) -> {
					dObInvoiceSaveInvoiceDetailsTestUtils.freeze(saveDateTime);
					Cookie userCookie = userActionsTestUtils
							.getUserCookie(userName);
					Response response = dObInvoiceSaveInvoiceDetailsTestUtils
							.saveInvoiceDetailsSection(userCookie, invoiceCode,
									invoiceData);
					userActionsTestUtils.setUserResponse(userName, response);
				});

		Then("^InvoiceDetails of VendorInvoice with Code \"([^\"]*)\" is updated as follows:$",
				(String invoiceCode, DataTable invoiceDataTable) -> {
					dObInvoiceSaveInvoiceDetailsTestUtils.checkUpdatedDataInInvoiceDetails(
							invoiceCode, invoiceDataTable);
				});
		Then("^the lock by \"([^\"]*)\" on InvoiceDetails section of Invoice with Code \"([^\"]*)\" is released$",
				(String userName, String invoiceCode) -> {
					dObInvoiceSaveInvoiceDetailsTestUtils
							.assertThatLockIsReleased(userName, invoiceCode,
									IDObVendorInvoiceSectionNames.INVOICE_DETAILS);
				});
	}
	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		dObInvoiceSaveInvoiceDetailsTestUtils = new DObVendorInvoiceSaveInvoiceDetailsTestUtils(
				properties);
		dObInvoiceSaveInvoiceDetailsTestUtils.setEntityManagerDatabaseConnector(
				entityManagerDatabaseConnector);

	}
	@Before
	public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save VendorInvoiceDetails section")) {

      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(
            dObInvoiceSaveInvoiceDetailsTestUtils.getDbScriptsOneTimeExecution());
        }
	}

	@After
	public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Save VendorInvoiceDetails section")) {

      dObInvoiceSaveInvoiceDetailsTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IDObVendorInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS,
          dObInvoiceSaveInvoiceDetailsTestUtils.getSectionsNames());
      databaseConnector.closeConnection();
		}
	}
}
