package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCommonTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceDeleteTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewDetailsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class DObVendorInvoiceDeleteStep  extends SpringBootRunner implements En {

  private final String SCENARIO_NAME = "Delete VendorInvoice";
  private DObVendorInvoiceDeleteTestUtils utils;
  private static boolean hasBeenExecuted = false;


  public DObVendorInvoiceDeleteStep() {
    When("^\"([^\"]*)\" requests to delete VendorInvoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      String deleteUrl = IDObVendorInvoiceRestUrls.COMMAND_URL + invoiceCode;
      Response deleteResponse =utils.sendDeleteRequest(cookie, deleteUrl);
      userActionsTestUtils.setUserResponse(userName, deleteResponse);
    });

    Then("^VendorInvoice with code \"([^\"]*)\" is deleted from the system$",
        (String invoiceCode) -> {
      utils.assertNoInvoiceExistsByCode(invoiceCode);
    });

    Given("^first \"([^\"]*)\" opens InvoiceDetails section of VendorInvoice with code \"([^\"]*)\" in edit mode$",
        (String userName, String invoiceCode) -> {
      String lockUrl = IDObVendorInvoiceRestUrls.COMMAND_URL
          + invoiceCode
          + IDObVendorInvoiceRestUrls.LOCK_INVOICE_DETAILS_SECTION;
      Response response = userActionsTestUtils.lockSection(userName, lockUrl, invoiceCode);
      userActionsTestUtils.setUserResponse(userName, response);
    });

    Given("^\"([^\"]*)\" first deleted the Vendor Invoice with code \"([^\"]*)\" successfully$",
        (String userName, String vendorInvoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      String deleteUrl = IDObVendorInvoiceRestUrls.COMMAND_URL + vendorInvoiceCode;
      Response deleteResponse =utils.sendDeleteRequest(cookie, deleteUrl);
      userActionsTestUtils.setUserResponse(userName, deleteResponse);
    });

    Given("^\"([^\"]*)\" first deleted the Vendor Invoice with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String vendorInvoiceCode, String dateTime) -> {
      utils.freeze(dateTime);
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      String deleteUrl = IDObVendorInvoiceRestUrls.COMMAND_URL + vendorInvoiceCode;
      Response deleteResponse =utils.sendDeleteRequest(cookie, deleteUrl);
      userActionsTestUtils.setUserResponse(userName, deleteResponse);
    });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObVendorInvoiceDeleteTestUtils(properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(DObVendorInvoiceViewDetailsTestUtils.clearAccountingDocuments());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      userActionsTestUtils.unlockAllSections(IDObVendorInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS,
          utils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}