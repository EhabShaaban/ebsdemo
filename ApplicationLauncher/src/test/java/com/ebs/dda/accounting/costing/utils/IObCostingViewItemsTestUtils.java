package com.ebs.dda.accounting.costing.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.costing.IDObCostingRestUrls;
import com.ebs.dda.jpa.accounting.costing.IIObCostingItemGeneralModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.json.JSONException;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class IObCostingViewItemsTestUtils extends DObCostingCommonTestUtils {

  public IObCostingViewItemsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String clearCostingDocumentInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/costing/costing_clear.sql");
    return str.toString();
  }

  public String getCostingItemsViewDataUrl(String costingCode) {
    return IDObCostingRestUrls.QUERY_COSTING + "/" + costingCode + IDObCostingRestUrls.VIEW_ITEMS;
  }

  public void assertCorrectValuesAreDisplayed(Response response, DataTable itemsDataTable)
      throws JsonProcessingException, JSONException {
    List<HashMap<String, Object>> actualCostingItems = getListOfMapsFromResponse(response);

    List<Map<String, String>> expectedCostingItemsMap =
        itemsDataTable.asMaps(String.class, String.class);

    for (int i = 0; i < actualCostingItems.size(); i++) {
      assertThatCostItemsMatchesTheExpected(
          expectedCostingItemsMap.get(i), actualCostingItems.get(i));
    }
  }

  private void assertThatCostItemsMatchesTheExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap)
      throws JsonProcessingException, JSONException {

    assertThatItemMatchesTheExpected(expectedCostingItemMap, actualCostingItemMap);
    assertThatItemUOMMatchesTheExpected(expectedCostingItemMap, actualCostingItemMap);
    assertThatWeightPercentageEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
    assertThatReceivedQuantityEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
    assertThatStockTypeMatchesTheExpected(expectedCostingItemMap, actualCostingItemMap);

    if (!expectedCostingItemMap
        .get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_PRICE)
        .isEmpty()) {
      assertThatEstimateUnitPriceEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
      assertThatEstimateUnitLandedCostEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
      assertThatEstimateItemTotalCostEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
    } else {
      assertThatActualUnitPriceEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
      assertThatActualUnitLandedCostEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
      assertThatActualItemTotalCostEqualsExpected(expectedCostingItemMap, actualCostingItemMap);
    }
  }

  private void assertThatEstimateItemTotalCostEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    String expectedTotalCost =
        expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_ITEM_TOTAL_COST)
            .split(" ")[0];
    BigDecimal expected = new BigDecimal(expectedTotalCost);
    BigDecimal actualTotalCost =
        new BigDecimal(
            actualCostingItemMap
                .get(IIObCostingItemGeneralModel.ESTIMATE_ITEM_TOTAL_COST)
                .toString());
    assertEquals(
        "Estimate Item Total Cost doesn't match\nExpected: "
            + expectedTotalCost
            + "\nActual: "
            + actualTotalCost,
        0,
        expected.compareTo(actualTotalCost));
  }

  private void assertThatActualItemTotalCostEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    String expectedTotalCost =
        expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_ITEM_TOTAL_COST)
            .split(" ")[0];
    BigDecimal expected = new BigDecimal(expectedTotalCost);
    BigDecimal actualTotalCost =
        new BigDecimal(
            actualCostingItemMap
                .get(IIObCostingItemGeneralModel.ACTUAL_ITEM_TOTAL_COST)
                .toString());
    assertEquals(
        "Actual Item Total Cost doesn't match\nExpected: "
            + expectedTotalCost
            + "\nActual: "
            + actualTotalCost,
        0,
        expected.compareTo(actualTotalCost));
  }

  private void assertThatEstimateUnitLandedCostEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    assertEquals(
        getBigDecimal(
            expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_LANDED_COST)
                .split(" ")[0]),
        getBigDecimal(
            actualCostingItemMap.get(IIObCostingItemGeneralModel.ESTIMATE_UNIT_LANDED_COST)));
  }

  private void assertThatActualUnitLandedCostEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    assertEquals(
        getBigDecimal(
            expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_LANDED_COST)
                .split(" ")[0]),
        getBigDecimal(
            actualCostingItemMap.get(IIObCostingItemGeneralModel.ACTUAL_UNIT_LANDED_COST)));
  }

  private void assertThatEstimateUnitPriceEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    assertEquals(
        getBigDecimal(
            expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.ESTIMATE_UNIT_PRICE)
                .split(" ")[0]),
        getBigDecimal(actualCostingItemMap.get(IIObCostingItemGeneralModel.ESTIMATE_UNIT_PRICE)));
  }

  private void assertThatActualUnitPriceEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    assertEquals(
        getBigDecimal(
            expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.ACTUAL_UNIT_PRICE)
                .split(" ")[0]),
        getBigDecimal(actualCostingItemMap.get(IIObCostingItemGeneralModel.ACTUAL_UNIT_PRICE)));
  }

  private void assertThatStockTypeMatchesTheExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    assertEquals(
        expectedCostingItemMap.get(IFeatureFileCommonKeys.STOCK_TYPE),
        actualCostingItemMap.get(IIObCostingItemGeneralModel.STOCK_TYPE));
  }

  private void assertThatReceivedQuantityEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    assertEquals(
        getBigDecimal(expectedCostingItemMap.get(IFeatureFileCommonKeys.RECEIVED_QTY_UOE)),
        getBigDecimal(actualCostingItemMap.get(IIObCostingItemGeneralModel.QUANTITY)));
  }

  private void assertThatWeightPercentageEqualsExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap) {
    assertEquals(
        getBigDecimal(
            expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.WEIGHT_PERCENTAGE)),
        getBigDecimal(
                actualCostingItemMap.get(
                    IIObCostingItemGeneralModel.TO_BE_CONSIDERED_IN_COSTING_PER))
            .multiply(new BigDecimal(100.0)));
  }

  private void assertThatItemUOMMatchesTheExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap)
      throws JSONException, JsonProcessingException {
    String localizedIUOMSymbol =
        getEnglishLocaleFromLocalizedResponse(
            objectMapper.writeValueAsString(
                actualCostingItemMap.get(IIObCostingItemGeneralModel.UOM_SYMBOL)));

    assertEquals(
        expectedCostingItemMap.get(IFeatureFileCommonKeys.UOM),
        actualCostingItemMap.get(IIObCostingItemGeneralModel.UOM_CODE)
            + " - "
            + localizedIUOMSymbol);
  }

  private void assertThatItemMatchesTheExpected(
      Map<String, String> expectedCostingItemMap, HashMap<String, Object> actualCostingItemMap)
      throws JsonProcessingException, JSONException {
    String localizedItemName =
        getEnglishLocaleFromLocalizedResponse(
            objectMapper.writeValueAsString(
                actualCostingItemMap.get(IIObCostingItemGeneralModel.ITEM_NAME)));

    assertEquals(
        expectedCostingItemMap.get(IAccountingFeatureFileCommonKeys.ITEM),
        actualCostingItemMap.get(IIObCostingItemGeneralModel.ITEM_CODE)
            + " - "
            + localizedItemName);
  }
}
