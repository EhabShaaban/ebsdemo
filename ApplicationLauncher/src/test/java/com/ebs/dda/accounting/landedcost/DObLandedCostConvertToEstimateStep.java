package com.ebs.dda.accounting.landedcost;

import java.util.Map;

import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptViewItemsTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsDeliveryCompleteTestUtils;
import org.springframework.beans.factory.InitializingBean;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostConvertToEstimateTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceSaveItemTestUtils;
import com.ebs.dda.inventory.goodsreceipt.utils.IObGoodsReceiptPurchaseOrderDataViewTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderCancelTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObLandedCostConvertToEstimateStep extends SpringBootRunner
    implements En, InitializingBean {


  private static final String SCENARIO_NAME = "LandedCost ConvertToEstimate";
  private boolean hasBeenExecutedOnce = false;
  private DObLandedCostConvertToEstimateTestUtils landedCostConvertToEstimateTestUtils;
  private DObPurchaseOrderCancelTestUtils purchaseOrderCancelTestUtils;
    private DObPurchaseOrderMarkAsDeliveryCompleteTestUtils markAsDeliveryCompleteTestUtils;
  private DObVendorInvoiceSaveItemTestUtils invoiceSaveItemTestUtils;
    private IObGoodsReceiptViewItemsTestUtils goodsReceiptViewItemsTestUtils;


    public DObLandedCostConvertToEstimateStep() {

    Given(
        "^\"([^\"]*)\" requests to cancel the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String purchaseOrderCode, String cancelTime) -> {
          purchaseOrderCancelTestUtils.freeze(cancelTime);
          Response response = purchaseOrderCancelTestUtils.cancelPurchaseOrder(purchaseOrderCode,
              userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    When("^\"([^\"]*)\" ConvertToEstimate LandedCost with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String landedCostCode, String dateTime) -> {
          landedCostConvertToEstimateTestUtils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = landedCostConvertToEstimateTestUtils
              .convertToEstimateLandedCost(userCookie, landedCostCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });




        Given("^VendorInvoice with code \"([^\"]*)\" has the following InvoiceItems:$",
        (String vendorInvoiceCode, DataTable vendorInvoicesItems) -> {
          invoiceSaveItemTestUtils.assertVendorInvoiceItemExists(vendorInvoiceCode,
              vendorInvoicesItems);
        });


  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();

    landedCostConvertToEstimateTestUtils = new DObLandedCostConvertToEstimateTestUtils(properties);
    landedCostConvertToEstimateTestUtils
        .setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
      goodsReceiptViewItemsTestUtils = new IObGoodsReceiptViewItemsTestUtils(properties);
      goodsReceiptViewItemsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    purchaseOrderCancelTestUtils = new DObPurchaseOrderCancelTestUtils(properties);
    purchaseOrderCancelTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

      markAsDeliveryCompleteTestUtils =
        new DObPurchaseOrderMarkAsDeliveryCompleteTestUtils(properties);
      markAsDeliveryCompleteTestUtils
        .setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    invoiceSaveItemTestUtils = new DObVendorInvoiceSaveItemTestUtils(properties);
    invoiceSaveItemTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector
            .executeSQLScript(landedCostConvertToEstimateTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(landedCostConvertToEstimateTestUtils.getDbScripts());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      landedCostConvertToEstimateTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
