package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObMonetaryNotesObjectScreenAllowedActionsStep extends SpringBootRunner
    implements En, InitializingBean {

  private DObMonetaryNotesCommonTestUtils utils;

  public DObMonetaryNotesObjectScreenAllowedActionsStep() {

    When(
        "^\"([^\"]*)\" requests to read actions of NotesReceivable with code \"([^\"]*)\"$",
        (String userName, String notesReceivableCode) -> {
          String readUrl =
              IDObNotesReceivableRestURLs.AUTHORIZED_ACTIONS + '/' + notesReceivableCode;
          Response response =
              utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObMonetaryNotesCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read allowed actions in NotesReceivable Object Screen")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }
}
