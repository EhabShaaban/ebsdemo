package com.ebs.dda.accounting.costing;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.costing.utils.DObCostingActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObCostingActivateSteps extends SpringBootRunner implements En {
  public static final String SCENARIO_NAME = "Activate Costing";
  private DObCostingActivateTestUtils utils;

  public DObCostingActivateSteps() {

    When(
        "^\"([^\"]*)\" activates Costing Document with code \"([^\"]*)\" and DueDate \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String userCode, String dueDateTime, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendCostingPutRequest(cookie, userCode, dueDateTime);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Costing Document with Code \"([^\"]*)\" is updated as follows:$",
        (String userCode, DataTable data) -> {
          utils.assertThatCostingDocumentUpdatedSuccessfully(data);
        });

    Then(
        "^the Costing Accounting Details of Costing Document is Created as Follows:$",
        (DataTable accountingDetailsTable) -> {
          utils.assertThatAccountingDetailsIsCreatedSuccessfully(accountingDetailsTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObCostingActivateTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeActivate(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
      databaseConnector.executeSQLScript(utils.clearCostingDocumentInsertions());
    }
  }
}
