package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostFactorItemsViewTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceItemsViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObLandedCostFactorItemsViewStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME = "View CostFactorItems section";
  private DObLandedCostFactorItemsViewTestUtils utils;
  private IObVendorInvoiceItemsViewTestUtils vendorInvoiceViewItemsTestUtils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObLandedCostFactorItemsViewTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    vendorInvoiceViewItemsTestUtils = new IObVendorInvoiceItemsViewTestUtils(properties);
    vendorInvoiceViewItemsTestUtils
        .setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }


  public DObLandedCostFactorItemsViewStep() {



    Given("^the following LandedCosts Items ActualDetails exist:$", (DataTable actualDetailsDT) -> {
      utils.insertCostFactorItemsDetails(actualDetailsDT);
    });

    When(
        "^\"([^\"]*)\" requests to view CostFactorItems section of LandedCost with code \"([^\"]*)\"$",
        (String userName, String landedCostCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readCostFactorItemResponse(landedCostCode, userCookie);
          userActionsTestUtils.setUserResponse(userName, response);

        });

    Then(
        "^the following values of CostFactorItems section for LandedCost with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String landedCostCode, String userName, DataTable ItemsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayed(response, ItemsDataTable);
        });

    Then(
        "^the following values of ActualDetails in CostFactorItems section for LandedCost with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String landedCostCode, String username, DataTable actualDetailsDT) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertItemsActualDetailsAreExist(response, actualDetailsDT);
        });

    Then(
        "^the total number of existing records in CostFactorItems section for LandedCost with code \"([^\"]*)\" is \"([^\"]*)\"$",
        (String landedCosCode, String numOfRecords) -> {
          utils.assertTotalNumberOfCostFactorItemsExist(landedCosCode, numOfRecords);
        });

    Then(
        "^the total number of existing ActualDetails in CostFactorItem \"([^\"]*)\" in LandedCost with code \"([^\"]*)\" is \"([^\"]*)\"$",
        (String itemCodeName, String landedCosCode, String numOfRecords) -> {
          utils.assertTotalNumberOfCostFactorItemsDetailsExist(itemCodeName, landedCosCode,
              numOfRecords);
        });


    Then(
        "^the following values for CostFactorItems Summary section for LandedCost with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String landedCostCode, String userName,
            DataTable landedCostFactorItemsSummaryDataTable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readCostFactorItemsSummarResponse(landedCostCode, userCookie);
          utils.assertCorrectValuesAreDisplayedInSummarySection(response, landedCostCode,
              landedCostFactorItemsSummaryDataTable);

        });

    Then(
        "^empty values for CostFactorItems Summary section for LandedCost with code \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String landedCostCode, String userName) -> {
          utils.assertThereIsNoCostFactorItemsSummary(landedCostCode);
        });


    Then(
        "^an empty CostFactorItems section for LandedCost with code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String landedCostCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatLandedCostItemsFactorAreEmpty(response);
        });



  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), SCENARIO_NAME)) {

      databaseConnector.createConnection();
      databaseConnector
          .executeSQLScript(DObLandedCostFactorItemsViewTestUtils.resetDataDependancies());
        databaseConnector
                .executeSQLScript(DObLandedCostCommonTestUtils.clearLandedCosts());
    }

  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
