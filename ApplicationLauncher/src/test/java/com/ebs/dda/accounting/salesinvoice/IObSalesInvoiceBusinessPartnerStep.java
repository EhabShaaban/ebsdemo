package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceBusinessPartnerTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class IObSalesInvoiceBusinessPartnerStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObSalesInvoiceBusinessPartnerTestUtils iObSalesInvoiceBusinessPartnerTestUtils;

  public IObSalesInvoiceBusinessPartnerStep() {
    Given("^the following businessPartnerData for salesInvoics exist:$",
        (DataTable businessPartnerTable) -> {
          iObSalesInvoiceBusinessPartnerTestUtils.assertBusinessPartnerExists(businessPartnerTable);
        });

    When(
        "^\"([^\"]*)\" requests to view BusinessPartner section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          StringBuilder readUrl = new StringBuilder();
          readUrl.append(IDObSalesInvoiceRestUrls.GET_URL);
          readUrl.append(salesInvoiceCode);
          readUrl.append(IDObSalesInvoiceRestUrls.BUSINESS_PARTNER_DATA);
          Response response = iObSalesInvoiceBusinessPartnerTestUtils.sendGETRequest(userCookie,
              String.valueOf(readUrl));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then("^the following values are displayed to \"([^\"]*)\":$",
        (String username, DataTable businessPartnerTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          iObSalesInvoiceBusinessPartnerTestUtils
              .assertCorrectValuesAreDisplayedInBusinessPartner(response, businessPartnerTable);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    iObSalesInvoiceBusinessPartnerTestUtils =
        new IObSalesInvoiceBusinessPartnerTestUtils(properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "View businessPartner section in SalesInvoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            iObSalesInvoiceBusinessPartnerTestUtils.getDbScriptsOneTimeExecution());

        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View businessPartner section in SalesInvoice")) {
      databaseConnector.closeConnection();
    }
  }
}
