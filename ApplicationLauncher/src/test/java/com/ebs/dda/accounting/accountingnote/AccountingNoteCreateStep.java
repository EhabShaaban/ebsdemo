package com.ebs.dda.accounting.accountingnote;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingnote.utils.AccountingNoteCreateTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class AccountingNoteCreateStep extends SpringBootRunner implements En {

  private AccountingNoteCreateTestUtils utils;
  private MObVendorCommonTestUtils vendorCommonTestUtils;

  public AccountingNoteCreateStep() {
    Given(
        "^Last created Credit/Debit Note was with code \"([^\"]*)\"$",
        (String lastCreatedCode) -> {
          utils.assertThatLastCreatedCodeIsCorrect(lastCreatedCode);
        });
    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });
    When(
        "^\"([^\"]*)\" creates Credit/Debit Note with the following values:$",
        (String userName, DataTable accountingNoteDataTable) -> {
          Response response =
              utils.createAccountingNote(
                  accountingNoteDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new Credit/Debit Note is created as follows:$",
        (DataTable accountingNoteDataTable) -> {
          utils.assertThatAccountingNoteIsCreatedSuccessfully(accountingNoteDataTable);
        });
    Given(
        "^the following Vendors exist:$",
        (DataTable vendorDataTable) -> {
          vendorCommonTestUtils.assertThatVendorsExistByCodeAndNameAndPurchaseUnits(
              vendorDataTable);
        });
    Then(
        "^the CompanyDetails Section of Credit/Debit Note with code \"([^\"]*)\" is updated as follows:$",
        (String debitNoteCode, DataTable debitNoteDataTable) -> {
          utils.assertThatDebitNoteCompanyIsCreatedSuccessfully(debitNoteCode, debitNoteDataTable);
        });
    Then(
        "^the NoteDetails Section of Credit/Debit Note with code \"([^\"]*)\" is updated as follows:$",
        (String debitNoteCode, DataTable debitNoteDataTable) -> {
          utils.assertThatDebitNoteDetailsIsCreatedSuccessfully(debitNoteCode, debitNoteDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new AccountingNoteCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    vendorCommonTestUtils = new MObVendorCommonTestUtils(getProperties());
    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create Credit/Debit Note")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create Credit/Debit Note")) {
      utils.unfreeze();
    }
  }
}
