package com.ebs.dda.accounting.settlement;

import static com.ebs.dda.accounting.settlement.IDObSettlementRestURLs.LOCK_ACCOUNTING_DETAILS;
import static com.ebs.dda.accounting.settlement.IDObSettlementRestURLs.UNLOCK_ACCOUNTING_DETAILS;
import static com.ebs.dda.accounting.settlement.IDObSettlementRestURLs.UNLOCK_LOCKED_SECTIONS;
import static com.ebs.dda.accounting.settlement.apis.IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION;
import java.util.Arrays;
import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementRequestAddAccountingDetailsTestUtils;
import com.ebs.dda.accounting.settlement.utils.DObSettlementsCommonTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObSettlementRequestAddAccountingDetailsStep extends SpringBootRunner implements En {

  private static final String SCENARIO_NAME =
      "Request to Add/Cancel AccountingDetails in Settlement";

  private static boolean hasBeenExecuted = false;
  private DObSettlementRequestAddAccountingDetailsTestUtils utils;

  public DObSettlementRequestAddAccountingDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to add a record to AccountingDetails section of Settlement with code \"([^\"]*)\"$",
        (String username, String settlementCode) -> {

          String lockUrl = String.format(LOCK_ACCOUNTING_DETAILS, settlementCode);
          Response response = userActionsTestUtils.lockSection(username, lockUrl, settlementCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^a new add AccountingDetails dialoge is opened and AccountingDetails section of Settlement with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String settlementCode, String username) -> {

          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(username, settlementCode, ACCOUNTING_DETAILS_SECTION);
        });

    Then(
        "^AccountingDetails section of Settlement with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String settlementCode, String username) -> {

          utils.assertThatLockIsReleased(username, settlementCode, ACCOUNTING_DETAILS_SECTION);
        });

    Given(
        "^AccountingDetails section of Settlement with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String settlementCode, String username, String lockTime) -> {

          utils.freeze(lockTime);
          String lockUrl = String.format(LOCK_ACCOUNTING_DETAILS, settlementCode);
          Response response = userActionsTestUtils.lockSection(username, lockUrl, settlementCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving AccountingDetails section of Settlement with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String settlementCode, String unlockTime) -> {

          utils.freeze(unlockTime);
          String unLockUrl = String.format(UNLOCK_ACCOUNTING_DETAILS, settlementCode);
          Response response = userActionsTestUtils.unlockSection(username, unLockUrl);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on AccountingDetails section of Settlement with code \"([^\"]*)\" is released$",
        (String username, String settlementCode) -> {
          utils.assertThatLockIsReleased(username, settlementCode, ACCOUNTING_DETAILS_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObSettlementRequestAddAccountingDetailsTestUtils(properties,
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {

      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            DObSettlementsCommonTestUtils.getSettlementDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(DObSettlementsCommonTestUtils.clearSettlement());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      utils.unfreeze();

      userActionsTestUtils.unlockAllSections(UNLOCK_LOCKED_SECTIONS,
          Arrays.asList(ACCOUNTING_DETAILS_SECTION));

      databaseConnector.closeConnection();
    }
  }
}
