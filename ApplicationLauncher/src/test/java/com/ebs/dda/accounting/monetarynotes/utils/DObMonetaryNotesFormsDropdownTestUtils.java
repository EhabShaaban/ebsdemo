package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObMonetaryNotesFormsDropdownTestUtils extends DObMonetaryNotesCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Form][Dropdown]";

  public DObMonetaryNotesFormsDropdownTestUtils(Map<String, Object> properties, EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties,entityManagerDatabaseConnector);
  }

  public Response readAllMonetaryNoteForms(Cookie cookie) {
    String restURL = IDObNotesReceivableRestURLs.READ_ALL_FORMS;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatMonetaryNoteFormsResponseIsCorrect(
      DataTable monetaryNoteFormsDataTable, Response response) {
    List<Map<String, String>> expectedMonetaryNoteFormsMapList =
        monetaryNoteFormsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualMonetaryNoteFormsList =
        getListOfMapsFromResponse(response);
    Assert.assertEquals(
        expectedMonetaryNoteFormsMapList.size(), actualMonetaryNoteFormsList.size());
    for (int i = 0; i < expectedMonetaryNoteFormsMapList.size(); i++) {
      assertThatExpectedMonetaryNoteFormMatchesActual(
          expectedMonetaryNoteFormsMapList.get(i), actualMonetaryNoteFormsList.get(i));
    }
  }

  private void assertThatExpectedMonetaryNoteFormMatchesActual(
      Map<String, String> expectedMonetaryNoteForm, Object actualMonetaryNoteForm) {
    Assert.assertEquals(
        expectedMonetaryNoteForm.get(IFeatureFileCommonKeys.FORM), actualMonetaryNoteForm);
  }

  public void assertThatTotalNumberOfMonetaryNoteFormsEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualMonetaryNoteForms = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of MonetaryNote Forms is not correct",
        expectedTotalNumber.intValue(),
        actualMonetaryNoteForms.size());
  }
}
