package com.ebs.dda.accounting.accountingnote.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.accountingnote.IAccountingNotesRestUrls;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountingNoteTypeDropdownTestUtils extends AccountingNoteCommonTestUtil {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Type][Dropdown]";

  public AccountingNoteTypeDropdownTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public Response readAllNoteTypes(Cookie cookie) {
    String restURL = IAccountingNotesRestUrls.READ_ALL_NOTE_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public Response readAllTypesForInputNoteType(Cookie cookie, String noteType) {
    String restURL = IAccountingNotesRestUrls.READ_ALL_TYPES + noteType;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatNoteTypesResponseIsCorrect(
      DataTable noteTypesDataTable, Response response) {
    List<Map<String, String>> expectedNoteTypeMapList =
        noteTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualNoteTypesList = getListOfMapsFromResponse(response);
    Assert.assertEquals(expectedNoteTypeMapList.size(), actualNoteTypesList.size());
    for (int i = 0; i < expectedNoteTypeMapList.size(); i++) {
      assertThatExpectedNoteTypeMatchesActual(
          expectedNoteTypeMapList.get(i), actualNoteTypesList.get(i));
    }
  }

  private void assertThatExpectedNoteTypeMatchesActual(
      Map<String, String> expectedNoteTypeMap, Object actualNoteType) {
    String expectedNoteType = expectedNoteTypeMap.get(IAccountingFeatureFileCommonKeys.NOTE_TYPE);
    Assert.assertEquals(
            ASSERTION_MSG + " expectedNoteType is " + expectedNoteType + " but actual is " + actualNoteType, expectedNoteType, actualNoteType);
  }

  public void assertThatTypesResponseIsCorrect(DataTable typesDataTable, Response response) {
    List<Map<String, String>> expectedTypeMapList =
        typesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualTypesList = getListOfMapsFromResponse(response);
    Assert.assertEquals(expectedTypeMapList.size(), actualTypesList.size());
    for (int i = 0; i < expectedTypeMapList.size(); i++) {
      assertThatExpectedTypeMatchesActual(expectedTypeMapList.get(i), actualTypesList.get(i));
    }
  }

  private void assertThatExpectedTypeMatchesActual(
      Map<String, String> expectedTypeMap, Object actualType) {
    String expectedType = expectedTypeMap.get(IFeatureFileCommonKeys.TYPE);
    Assert.assertEquals(ASSERTION_MSG + " expectedType is " + expectedType + " but actual is " + actualType, expectedType, actualType);
  }

  public void assertThatTotalNumberOfNoteTypesEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualNoteTypes = getListOfMapsFromResponse(response);
    Assert.assertEquals(ASSERTION_MSG +
        "Total number of AccountingNote noteTypes is not correct",
        expectedTotalNumber.intValue(),
        actualNoteTypes.size());
  }

  public void assertThatTotalNumberOfTypesEquals(Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualTypes = getListOfMapsFromResponse(response);
    Assert.assertEquals(ASSERTION_MSG +
        "Total number of AccountingNote types is not correct",
        expectedTotalNumber.intValue(),
        actualTypes.size());
  }
}
