package com.ebs.dda.accounting.vendorinvoice.utiles;

import static junit.framework.TestCase.assertNull;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import java.util.Map;

public class DObVendorInvoiceDeleteTestUtils  extends DObVendorInvoiceTestUtils {

  public DObVendorInvoiceDeleteTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertNoInvoiceExistsByCode(String invoiceCode) {
    DObVendorInvoiceGeneralModel actualInvoice =
        (DObVendorInvoiceGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getInvoiceByCode", invoiceCode);
    assertNull(actualInvoice);
  }
}
