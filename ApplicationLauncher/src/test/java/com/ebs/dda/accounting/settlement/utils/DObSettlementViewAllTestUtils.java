package com.ebs.dda.accounting.settlement.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlementGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSettlementViewAllTestUtils extends DObSettlementsCommonTestUtils {

  public DObSettlementViewAllTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public void assertSettlementDataIsCorrect(DataTable settlementDataTable, Response response)
      throws Exception {
    List<Map<String, String>> settlementList =
        settlementDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> settlementResponse = response.body().jsonPath().getList("data");
    for (int i = 0; i < settlementList.size(); i++) {
      assertCorrectShownData(settlementResponse.get(i), settlementList.get(i));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> responsedSettlement, Map<String, String> settlementRowMap)
      throws Exception {
    MapAssertion mapAssertion = new MapAssertion(settlementRowMap, responsedSettlement);
    mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, IDObSettlementGeneralModel.USER_CODE);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObSettlementGeneralModel.SETTLEMENT_TYPE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObSettlementGeneralModel.PURCHASE_UNIT_CODE,
        IDObSettlementGeneralModel.PURCHASING_UNIT_NAME);
    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObSettlementGeneralModel.CURRENT_STATES);

  }

  public void assertTotalNumberOfItemsExist(Integer expectedSettlementCount) {
    Long actualItemsCount =
        (Long)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getViewAllSettlementCount");
    Assert.assertEquals(Long.valueOf(expectedSettlementCount), actualItemsCount);
  }
}
