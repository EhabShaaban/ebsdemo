package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.accountingcommonutils.AccountingDocumentCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCreateTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestViewCompanyDataTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorUpdateAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestCreateSteps extends SpringBootRunner implements En, InitializingBean {

	public static final String SCENARIO_NAME = "Create Payment";
	private DObPaymentRequestCreateTestUtils paymentRequestCreationTestUtils;
	private MObVendorUpdateAccountingDetailsTestUtils vendorUpdateAccountingDetailsTestUtils;
	private MObVendorCommonTestUtils vendorCommonTestUtils;
	private DObPaymentRequestViewCompanyDataTestUtils companyDataTestUtils;

	public DObPaymentRequestCreateSteps() {

		When("^\"([^\"]*)\" requests to create PaymentRequest$", (String userName) -> {
			Cookie cookie = userActionsTestUtils.getUserCookie(userName);
			Response response = paymentRequestCreationTestUtils.sendGETRequest(cookie,
							IDObPaymentRequestRestURLs.CREATE_PAYMENT_REQUEST);
			userActionsTestUtils.setUserResponse(userName, response);
		});

		Given("^CurrentDateTime = \"([^\"]*)\"$", (String currentDateTime) -> {
			paymentRequestCreationTestUtils.freeze(currentDateTime);
		});

		Given("^Last created PaymentRequest was with code \"([^\"]*)\"$",
						(String paymentRequestCode) -> {
							paymentRequestCreationTestUtils.assertLastCreatedPaymentRequestByCode(
											paymentRequestCode);
						});

		When("^\"([^\"]*)\" creates PaymentRequest with the following values:$",
						(String username, DataTable paymentRequestTable) -> {
							Response response = paymentRequestCreationTestUtils
											.createPaymentRequest(paymentRequestTable,
															userActionsTestUtils.getUserCookie(
																			username));
							userActionsTestUtils.setUserResponse(username, response);
						});

		Then("^a new PaymentRequest is created as follows:$",
						(DataTable paymentRequestDataTable) -> {
							paymentRequestCreationTestUtils
											.assertPaymentRequestIsCreated(paymentRequestDataTable);
						});
		Given("^the following Vendors exist:$", (DataTable vendorDataTable) -> {
			vendorCommonTestUtils.assertThatVendorsExistByCodeAndNameAndPurchaseUnitsCodeNameConcat(
							vendorDataTable);
		});
		Given("^the following AccountingInfo for Vendors exist:$",
						(DataTable accountingInfoData) -> {
							vendorUpdateAccountingDetailsTestUtils
											.assertThatVendorHasAccountInfo(accountingInfoData);
						});

		Then("^the CompanyData Section of PaymentRequest with code \"([^\"]*)\" is updated as follows:$",
						(String paymentRequestCode, DataTable companyDataDataTable) -> {
							paymentRequestCreationTestUtils.assertThatPRCompanyDataUpdatedAsFollows(
											paymentRequestCode, companyDataDataTable);
						});

		Then("^the PaymentDetails Section of PaymentRequest with code \"([^\"]*)\" is updated as follows:$",
						(String paymentRequestCode, DataTable paymentDetailsDataTable) -> {
							paymentRequestCreationTestUtils.assertPaymentDetailsIsUpdated(
											paymentRequestCode, paymentDetailsDataTable);
						});

		Then("^the AccountingDetails Section of PaymentRequest with code \"([^\"]*)\" is updated as follows:$",
						(String paymentRequestCode, DataTable accountingDetailsDataTable) -> {
							paymentRequestCreationTestUtils.assertAccountingDetailsIsUpdated(
											paymentRequestCode, accountingDetailsDataTable);
						});
		And("^the following Employees exist:$", (DataTable employeesDataTable) -> {
			paymentRequestCreationTestUtils.insertEmployeesData(employeesDataTable);
		});
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		paymentRequestCreationTestUtils = new DObPaymentRequestCreateTestUtils(getProperties());
		paymentRequestCreationTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		vendorUpdateAccountingDetailsTestUtils = new MObVendorUpdateAccountingDetailsTestUtils(
						getProperties());
		vendorUpdateAccountingDetailsTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		vendorCommonTestUtils = new MObVendorCommonTestUtils(getProperties());
		vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		companyDataTestUtils = new DObPaymentRequestViewCompanyDataTestUtils(getProperties());
		companyDataTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		paymentRequestCreationTestUtils.setCompanyDataTestUtils(companyDataTestUtils);
	}

	@Before
	public void setup(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			databaseConnector.executeSQLScript(DObPaymentRequestCreateTestUtils
							.getPaymentRequestDbScriptsOneTimeExecution());
			databaseConnector.executeSQLScript(AccountingDocumentCommonTestUtils
					.clearAccountingDocuments());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
			paymentRequestCreationTestUtils.unfreeze();
		}
	}
}
