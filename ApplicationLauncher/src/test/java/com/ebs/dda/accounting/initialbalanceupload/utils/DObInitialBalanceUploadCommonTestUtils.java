package com.ebs.dda.accounting.initialbalanceupload.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.DataTable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.ebs.dac.foundation.realization.statemachine.BasicStateMachine.DRAFT_STATE;
import static com.ebs.dac.foundation.realization.statemachine.PrimitiveStateMachine.ACTIVE_STATE;

public class DObInitialBalanceUploadCommonTestUtils extends CommonTestUtils {
  private final ObjectMapper objectMapper;

  public DObInitialBalanceUploadCommonTestUtils() {
    objectMapper = new ObjectMapper();
  }

  public String clearIBUInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/initial-balance-upload/initial_balance_upload_Clear.sql");
    return str.toString();
  }

  public void createInitialBalanceUploadGeneralData(DataTable generalDataTable) {
    List<Map<String, String>> ibuGeneralDataListMap =
        generalDataTable.asMaps(String.class, String.class);
    for (Map<String, String> ibuGeneralDataMap : ibuGeneralDataListMap) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createInitialBalanceUploadGeneralData",
          constructInsertInitialBalanceUploadGeneralDataQueryParams(ibuGeneralDataMap));
    }
  }

  private Object[] constructInsertInitialBalanceUploadGeneralDataQueryParams(
      Map<String, String> initialBlanceUploadMap) {
    String state = initialBlanceUploadMap.get(IFeatureFileCommonKeys.STATE);

    if (state.equals(DRAFT_STATE)) {
      state = "[\"" + state + "\"]";
    } else {
      state = "[\"" + ACTIVE_STATE + "\",\"" + state + "\"]";
    }

    return new Object[] {
      initialBlanceUploadMap.get(IFeatureFileCommonKeys.CODE),
      initialBlanceUploadMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      initialBlanceUploadMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      initialBlanceUploadMap.get(IFeatureFileCommonKeys.CREATED_BY),
      initialBlanceUploadMap.get(IFeatureFileCommonKeys.CREATED_BY),
      state,
      initialBlanceUploadMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      initialBlanceUploadMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR)
    };
  }

  public void createInitialBalanceUploadCompany(DataTable companyDataTable) {
    List<Map<String, String>> initialBalanceUploadCompanyDataListMap =
        companyDataTable.asMaps(String.class, String.class);
    for (Map<String, String> initialBalanceUploadComapanyDataMap :
        initialBalanceUploadCompanyDataListMap) {

      Object[] params =
          new Object[] {
            initialBalanceUploadComapanyDataMap.get(IFeatureFileCommonKeys.CODE),
            initialBalanceUploadComapanyDataMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
            initialBalanceUploadComapanyDataMap.get(IFeatureFileCommonKeys.COMPANY)
          };

      entityManagerDatabaseConnector.executeInsertQuery(
          "createInitialBalanceUploadCompany", params);
    }
  }

  protected String getAsString(Object obj) {
    if (obj == null) return "";
    return obj.toString();
  }

  protected String getLocalizedValue(Object obj) throws IOException {
    return objectMapper.writeValueAsString(obj);
  }

  public void createInitialBalanceUploadItem(DataTable itemsDataTable) {
    List<Map<String, String>> ibuItemMapList = itemsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> ibuItemMap : ibuItemMapList) {
      if (ibuItemMap.get(IAccountingFeatureFileCommonKeys.SUBLEDGER).equals("Local_Vendors")) {
        entityManagerDatabaseConnector.executeInsertQueryForObject(
            "createInitialBalanceUploadVendorItem",
            constructInsertInitialBalanceUploadItemQueryParameters(ibuItemMap));
      } else {
        entityManagerDatabaseConnector.executeInsertQueryForObject(
            "createInitialBalanceUploadPOItem",
            constructInsertInitialBalanceUploadItemQueryParameters(ibuItemMap));
      }
    }
  }

  private Object[] constructInsertInitialBalanceUploadItemQueryParameters(
      Map<String, String> ibuItemData) {
    return new Object[] {
      ibuItemData.get(IAccountingFeatureFileCommonKeys.CREDIT),
      ibuItemData.get(IAccountingFeatureFileCommonKeys.DEBIT),
      ibuItemData.get(IFeatureFileCommonKeys.CODE),
      ibuItemData.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
      ibuItemData.get(IAccountingFeatureFileCommonKeys.CURRENCY),
      ibuItemData.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
      ibuItemData.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT)
    };
  }
}
