package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.sales.dbo.jpa.entities.GeneralModels.IItemsLastSalesPriceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ItemsLastSalesPriceTestUtils extends DObSalesInvoiceTestUtils {

  public ItemsLastSalesPriceTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder(getMasterDataScripts());

    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderTaxes.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");

    return str.toString();
  }

  public String getItemsLastSalePriceUrl(
      String itemCode, String UOM, String PurchaseUnit, String CustomerCode) {
    return IDObSalesInvoiceRestUrls.VIEW_ITEMS_LAST_SALES_PRICE
        + "/"
        + itemCode
        + "/"
        + UOM
        + "/"
        + PurchaseUnit
        + "/"
        + CustomerCode;
  }

  public void assertThatItemsLastSalesPriceAreCorrect(
      Response response, DataTable itemsLastSalesPriceDataTable) throws Exception {
    Map<String, Object> actualItemsLastSalesPrice =  getMapFromResponse(response);

    Map<String, String> expectedJournalEntriesMap =
            itemsLastSalesPriceDataTable.asMaps(String.class, String.class).get(0);
    MapAssertion mapAssertion = new MapAssertion(expectedJournalEntriesMap, actualItemsLastSalesPrice);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/master-data/exchange-rate/exchange-rate.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoicePostingDetails.sql");
    return str.toString();
  }

  public void assertPostedSalesInvoice(String NumOfPostedSalesInvoice) {
    List<DObSalesInvoiceGeneralModel> salesInvoiceGeneralModels =
        entityManagerDatabaseConnector.executeNativeNamedQuery("getPostedSalesInvoice");
    assertEquals(Integer.parseInt(NumOfPostedSalesInvoice), salesInvoiceGeneralModels.size());
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {


    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.SI_CODE, IItemsLastSalesPriceGeneralModel.SALES_INVOICE_CODE);
    mapAssertion.assertDateTime(
            ISalesFeatureFileCommonKeys.SALES_INVOICE_POSTING_DATE, IItemsLastSalesPriceGeneralModel.SALES_INVOICE_POSTING_DATE);
    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.UOM,  IItemsLastSalesPriceGeneralModel.UOM_CODE_NAME);
    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.ITEM,  IItemsLastSalesPriceGeneralModel.ITEM_CODE_NAME);
    mapAssertion.assertValue(
            ISalesFeatureFileCommonKeys.CUSTOMER,  IItemsLastSalesPriceGeneralModel.CUSTOMER_CODE_NAME);
    mapAssertion.assertValue(
            IFeatureFileCommonKeys.PURCHASE_UNIT,  IItemsLastSalesPriceGeneralModel.PURCHASE_UNIT_CODE_NAME);
    mapAssertion.assertBigDecimalNumberValue(
            ISalesFeatureFileCommonKeys.SALES_PRICE, IItemsLastSalesPriceGeneralModel.SALES_PRICE);
  }
}
