package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceBusinessPartnerSaveHappyPathTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesInvoiceBusinessPartnerSaveHappyPathStep extends SpringBootRunner implements En {

  private IObSalesInvoiceBusinessPartnerSaveHappyPathTestUtils salesInvoiceSaveBusinessPartnerTestUtils;

  public IObSalesInvoiceBusinessPartnerSaveHappyPathStep() {
    Given(
        "^the following BusinessPartner for the following SalesInvoice exist:$",
        (DataTable businessPartnerTable) -> {
           salesInvoiceSaveBusinessPartnerTestUtils.assertBusinessPartnerExists(businessPartnerTable);
        });

    Given(
        "^BusinessPartner section of SalesInvoice with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName, String lockDate) -> {
          salesInvoiceSaveBusinessPartnerTestUtils.freeze(lockDate);
          String lockUrl =
              salesInvoiceSaveBusinessPartnerTestUtils.getLockBusinessPartnerUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" saves BusinessPartner section of SalesInvoice with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String salesInvoiceCode,
            String saveDate,
            DataTable businessPartnerDataTable) -> {
          salesInvoiceSaveBusinessPartnerTestUtils.freeze(saveDate);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              salesInvoiceSaveBusinessPartnerTestUtils.saveSalesInvoiceBusinessPartner(
                  userCookie, salesInvoiceCode, businessPartnerDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SalesInvoice with code \"([^\"]*)\" is updated as follows:$",
        (String salesInvoiceCode, DataTable salesInvoiceData) -> {
          salesInvoiceSaveBusinessPartnerTestUtils.assertThatSalesInvoiceUpdated(
              salesInvoiceCode, salesInvoiceData);
        });

    Then(
        "^BusinessPartner section of SalesInvoice with code \"([^\"]*)\" is updated as follows:$",
        (String salesInvoiceCode, DataTable businessPartnerDataTable) -> {
          salesInvoiceSaveBusinessPartnerTestUtils.assertThatSalesInvoiceBusinessPartnerUpdated(
              businessPartnerDataTable);
        });

  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    salesInvoiceSaveBusinessPartnerTestUtils =
        new IObSalesInvoiceBusinessPartnerSaveHappyPathTestUtils(properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Save BusinessPartner section")) {
      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(
                salesInvoiceSaveBusinessPartnerTestUtils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save BusinessPartner section")) {

        userActionsTestUtils.unlockAllSections(
                IDObSalesInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS, salesInvoiceSaveBusinessPartnerTestUtils.getSectionsNames());

        databaseConnector.closeConnection();
    }
  }
}
