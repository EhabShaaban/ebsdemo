package com.ebs.dda.accounting.collection;

import java.util.Map;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionTestUtils;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObCollectionAllowedActionsHomeScreenStep extends SpringBootRunner implements En {

  private DObCollectionTestUtils utils;

  public DObCollectionAllowedActionsHomeScreenStep() {

    When("^\"([^\"]*)\" requests to read actions of Collection home screen$", (String userName) -> {
      String url = IDObCollectionRestURLs.AUTHORIZED_ACTIONS;
      Response response = utils.sendGETRequest(userActionsTestUtils.getUserCookie(userName), url);
      userActionsTestUtils.setUserResponse(userName, response);
    });

    Then("^the \"([^\"]*)\" are displayed to \"([^\"]*)\"$",
        (String allowedActions, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTheFollowingActionsExist(response, allowedActions);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();

    utils = new DObCollectionTestUtils(properties);
    utils.setProperties(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

}
