package com.ebs.dda.accounting.vendorinvoice;

import java.util.Map;
import org.springframework.beans.factory.InitializingBean;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewCompanyDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObVendorInvoiceViewCompanyDataStep extends SpringBootRunner
    implements En, InitializingBean {

  private static final String SCENARIO_NAME = "View CompanyData section in VendorInvoice";
  private static boolean hasBeenExecutedOnce = false;
  private DObVendorInvoiceViewCompanyDataTestUtils companyDataViewTestUtils;

  public DObVendorInvoiceViewCompanyDataStep() {
    Given(
        "^the following CompanyData for VendorInvoice exist:$",
        (DataTable companyDataDT) -> {
          companyDataViewTestUtils.assertThatCompanyDataIsExist(companyDataDT);
        });

    When(
        "^\"([^\"]*)\" requests to view CompanyData section of VendorInvoice with code \"([^\"]*)\"$",
        (String username, String vendorInvoiceCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          String readUrl =
              IDObVendorInvoiceRestUrls.GET_URL
                  + vendorInvoiceCode
                  + IDObVendorInvoiceRestUrls.COMPANY_DATA_SECTION;
          Response response =
              companyDataViewTestUtils.sendGETRequest(userCookie, String.valueOf(readUrl));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of CompanyData section for VendorInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String vendorInvoiceCode, String username, DataTable companyDataDT) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          companyDataViewTestUtils.assertThatViewComanyDataResponseIsCorrect(
              response, companyDataDT);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    companyDataViewTestUtils = new DObVendorInvoiceViewCompanyDataTestUtils(properties);
    companyDataViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(companyDataViewTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
