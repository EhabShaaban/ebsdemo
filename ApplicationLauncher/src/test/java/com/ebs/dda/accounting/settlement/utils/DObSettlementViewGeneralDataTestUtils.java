package com.ebs.dda.accounting.settlement.utils;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;

import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlementGeneralModel;

import cucumber.api.DataTable;
import io.restassured.response.Response;

public class DObSettlementViewGeneralDataTestUtils extends DObSettlementsCommonTestUtils {

	public DObSettlementViewGeneralDataTestUtils(Map<String, Object> properties) {
		super(properties);
		setProperties(properties);
	}

	public void assertThatViewSettlementGeneralDataResponseIsCorrect(Response response,
					DataTable generalDataDT) {
		Map<String, String> settlementGeneralDataMap = generalDataDT
						.asMaps(String.class, String.class).get(0);
		Map<String, Object> settlementGeneralDataResponseMap = response.body().jsonPath()
						.getMap(CommonKeys.DATA);

		MapAssertion mapAssertion = new MapAssertion(settlementGeneralDataMap,
						settlementGeneralDataResponseMap);

		mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.SETTLEMENT_CODE,
						IDObSettlementGeneralModel.USER_CODE);
		mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.SETTLEMENT_TYPE,
						IDObSettlementGeneralModel.SETTLEMENT_TYPE);
		mapAssertion.assertValue(IFeatureFileCommonKeys.CREATED_BY,
						IDObSettlementGeneralModel.CREATION_INFO);

		mapAssertion.assertValueContains(IFeatureFileCommonKeys.STATE,
						IDObSettlementGeneralModel.CURRENT_STATES);
		this.assertDateEquals(settlementGeneralDataMap.get(IFeatureFileCommonKeys.CREATION_DATE),
						formatDate(String.valueOf(settlementGeneralDataResponseMap
										.get(IDObSettlementGeneralModel.CREATION_DATE))));
		mapAssertion.assertValue(IFeatureFileCommonKeys.DOCUMENT_OWNER,
						IDObSettlementGeneralModel.DOCUMENT_OWNER);
	}

	private DateTime formatDate(String date) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
		return formatter.parseDateTime(date).withZone(DateTimeZone.UTC);
	}

}
