package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesCommonTestUtils;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

public class DObNotesReceivablesViewAllStep extends SpringBootRunner implements En, InitializingBean {

  private DObMonetaryNotesCommonTestUtils utils;

  public DObNotesReceivablesViewAllStep() {

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IDObNotesReceivableRestURLs.QUERY, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable dataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatMonetaryNotesMatch(dataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on Code which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(
                  IDObNotesReceivablesGeneralModel.USER_CODE, userCodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObNotesReceivableRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on Type which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String typeEquals, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(
                  IDObNotesReceivablesGeneralModel.OBJECT_TYPE_CODE, typeEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObNotesReceivableRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on BusinessPartner which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String businessPartnerContains,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER, businessPartnerContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObNotesReceivableRestURLs.VIEW_ALL,
                  pageNumber,
                  rowsNumber,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on Depot which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String depotContains,
            Integer rowsNumber,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(IDObNotesReceivablesGeneralModel.DEPOT, depotContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IDObNotesReceivableRestURLs.VIEW_ALL,
                  pageNumber,
                  rowsNumber,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on DueDate which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String dateContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter =
              utils.getContainsFilterField(
                  IDObNotesReceivablesGeneralModel.DUE_DATE,
                  String.valueOf(utils.getDateTimeInMilliSecondsFormat(dateContains)));
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObNotesReceivableRestURLs.VIEW_ALL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on Amount which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String amountEquals, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(IDObNotesReceivablesGeneralModel.AMOUNT, amountEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObNotesReceivableRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on Remaining which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String remainingEquals, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(IDObNotesReceivablesGeneralModel.AMOUNT, remainingEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObNotesReceivableRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on BusinessUnit which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String userName,
            Integer pageNumber,
            String businessUnitCodeEquals,
            Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getEqualsFilterField(
                  IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_CODE, businessUnitCodeEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObNotesReceivableRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of NotesReceivables with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String userName, Integer pageNumber, String stateNameContains, Integer rowsNumber) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObNotesReceivablesGeneralModel.CURRENT_STATES, stateNameContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IDObNotesReceivableRestURLs.VIEW_ALL, pageNumber, rowsNumber, filter);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObMonetaryNotesCommonTestUtils(getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all NotesReceivables,")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }
}
