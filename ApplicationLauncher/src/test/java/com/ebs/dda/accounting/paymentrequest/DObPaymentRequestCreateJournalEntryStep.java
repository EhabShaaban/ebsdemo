package com.ebs.dda.accounting.paymentrequest;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.*;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewGeneralDataTestUtils;
import com.ebs.dda.masterdata.chartofaccounts.utils.HObCommonChartOfAccountTestUtils;
import com.ebs.dda.masterdata.exchangerate.utils.CObExchangeRateCommonTestUtils;
import com.ebs.dda.masterdata.vendor.utils.MObVendorViewAccountingInfoTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import org.springframework.beans.factory.InitializingBean;

public class DObPaymentRequestCreateJournalEntryStep extends SpringBootRunner
    implements En, InitializingBean {

  public static final String SCENARIO_NAME = "Create Journal Entry when PaymentRequest";
  private DObPaymentRequestCreateJournalEntryTestUtils utils;
  private DObPaymentRequestPostHappyPathTestUtils postTestUtils;
  private DObPaymentRequestViewGeneralDataTestUtils generalDataViewTestUtils;
  private DObPaymentRequestSavePaymentDetailsTestUtils savePaymentDetailsTestUtils;
  private MObVendorViewAccountingInfoTestUtils vendorViewAccountingInfoUtils;
  private CObExchangeRateCommonTestUtils exchangeRateCommonTestUtils;
  private HObCommonChartOfAccountTestUtils chartOfAccountTestUtils;
  private boolean hasBeenExecutedOnce = false;


  public DObPaymentRequestCreateJournalEntryStep() {

    Given(
        "^the following AccoutingInfo for Vendors exist:$",
        (DataTable accountingInfoDataTable) -> {
          vendorViewAccountingInfoUtils.assertThatVendorHasAccountInfo(accountingInfoDataTable);
        });

    Given(
        "^the PaymentDetails Section of the PaymentRequest with code \"([^\"]*)\" has the following values:$",
        (String paymentRequestCode, DataTable paymentDetailsTable) -> {
          utils.assertThatTheFollowingPaymentRequestsHasTheFollowingAmountAndCurrency(
              paymentRequestCode, paymentDetailsTable);
        });

    Then(
        "^a new JournalEntry is created as follows:$",
        (DataTable journalEntryDataTable) -> {
          utils.assertThatJournalEntryIsCreated(journalEntryDataTable);
        });

    Then(
        "^the following new JournalEntryDetails are created for JournalEntry with code \"([^\"]*)\":$",
        (String journalEntryCode, DataTable journalDetailsDataTable) -> {
          utils.assertThatJournalEntryItemsIsCreated(journalEntryCode, journalDetailsDataTable);
        });

    Then(
        "^there is No Realized Differences on Exchange in the JournalEntryDetails of JournalEntry with code \"([^\"]*)\"$",
        (String journalEntryCode) -> {
          utils.assertThereIsNoRealizedDifferencesOnExchangeInJournalEntry(journalEntryCode);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPaymentRequestCreateJournalEntryTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    exchangeRateCommonTestUtils = new CObExchangeRateCommonTestUtils(getProperties());
    exchangeRateCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    postTestUtils = new DObPaymentRequestPostHappyPathTestUtils(getProperties());
    postTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    generalDataViewTestUtils = new DObPaymentRequestViewGeneralDataTestUtils(getProperties());
    generalDataViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    savePaymentDetailsTestUtils = new DObPaymentRequestSavePaymentDetailsTestUtils(getProperties());
    savePaymentDetailsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    vendorViewAccountingInfoUtils = new MObVendorViewAccountingInfoTestUtils(getProperties());
    vendorViewAccountingInfoUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    chartOfAccountTestUtils = new HObCommonChartOfAccountTestUtils(getProperties());
    chartOfAccountTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce){
        databaseConnector.executeSQLScript(
                DObPaymentRequestCreateJournalEntryTestUtils.getDbScriptsOneTimeExecution());
                utils.executeAccountDeterminationFunction();
        hasBeenExecutedOnce = true;
      }
      databaseConnector.executeSQLScript(
              DObVendorInvoiceViewGeneralDataTestUtils.clearVendorInvoices());
      databaseConnector.executeSQLScript(
              DObPaymentRequestPostValidationTestUtils.clearPaymentRequests());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
      utils.unfreeze();
    }
  }
}
