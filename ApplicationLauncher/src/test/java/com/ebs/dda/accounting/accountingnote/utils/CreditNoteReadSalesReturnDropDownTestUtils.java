package com.ebs.dda.accounting.accountingnote.utils;

import com.ebs.dda.inventory.goodsreceipt.utils.DObGoodsReceiptReadSalesReturnDropDownTestUtils;
import java.util.Map;

public class CreditNoteReadSalesReturnDropDownTestUtils
    extends DObGoodsReceiptReadSalesReturnDropDownTestUtils {
  protected String ASSERTION_MSG = "[Accounting][Note]" + "[SRO] [dropDown]";

  public CreditNoteReadSalesReturnDropDownTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/order/salesreturnorder/Sales_Return_Clear.sql");
    return str.toString();
  }
}
