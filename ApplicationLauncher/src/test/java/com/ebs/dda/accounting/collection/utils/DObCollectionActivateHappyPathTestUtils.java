package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class DObCollectionActivateHappyPathTestUtils extends DObCollectionTestUtils {

  private static String NOTES_RECEIVABLES = "Notes receivables";
  private static String DEBIT = "DEBIT";
  private static String BANK = "Bank";
  private static String DASH = " - ";

  public DObCollectionActivateHappyPathTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getPostUrl(String collectionCode, String collectionType) {
    return IDObCollectionRestURLs.COLLECTION_COMMAND + "/" + collectionCode
        + IDObCollectionRestURLs.POST + "?type=" + collectionType;
  }

  public void assertCollectionDetailsIsUpdatedAfterPost(String collectionCode,
      DataTable collectionDetailsDataTable) {
    List<Map<String, String>> collectionData =
        collectionDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionDetailsRow : collectionData) {
      IObCollectionDetailsGeneralModel collectionDetails =
          retrieveCollectionDetailsDataAfterPost(collectionCode, collectionDetailsRow);
      Assert.assertNotNull(collectionDetails);

      assertDateEquals(collectionDetailsRow.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
          collectionDetails.getModifiedDate());
    }
  }

  private IObCollectionDetailsGeneralModel retrieveCollectionDetailsDataAfterPost(
      String collectionCode, Map<String, String> collectionDetailsRow) {
    return (IObCollectionDetailsGeneralModel) entityManagerDatabaseConnector
        .executeNativeNamedQuerySingleResult("getCollectionDetailsAfterPost",
            constructCreatedCollectionDetailsQueryParams(collectionCode, collectionDetailsRow));
  }

  private Object[] constructCreatedCollectionDetailsQueryParams(String collectionCode,
      Map<String, String> collectionDetailsRow) {
    String lastUpdatedBy = collectionDetailsRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY);
    String state = '%' + collectionDetailsRow.get(IFeatureFileCommonKeys.STATE) + '%';

    return new Object[] {collectionCode, state, lastUpdatedBy};
  }

  public void assertThatCollectionActivationDetailAreUpdated(String collectionCode,
      DataTable activationDetailsDataTable) {
    Map<String, String> expectedActivationDetails =
        activationDetailsDataTable.asMaps(String.class, String.class).get(0);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetails =
        (IObAccountingDocumentActivationDetailsGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getCollectionActivationDetails",
                constructCollectionActivationDetailsParams(collectionCode,
                    expectedActivationDetails));

    assertNotNull(activationDetails,
        "ActivationDetails for collection with code " + collectionCode + " wasn't updated!");
    assertDateEquals(expectedActivationDetails.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
        activationDetails.getActivationDate());
    assertDateEquals(expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
        activationDetails.getDueDate());
  }

  public Response sendPutCollection(Cookie cookie, String collectionCode, String coolectionType,
      String dueDateTime) {
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("journalEntryDate", dueDateTime);
    return sendPUTRequest(cookie, getPostUrl(collectionCode, coolectionType), jsonObject);
  }

  public void assertAccountingDetailsExistForCollections(DataTable accountingDetailsDataTable) {
    List<Map<String, String>> accountDetailsMaps =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> accountDetailsMap : accountDetailsMaps) {
      String CRCode = accountDetailsMap.get(IAccountingFeatureFileCommonKeys.C_CODE);
      retrieveAccountingDetailsData(CRCode, accountDetailsMap);
    }
  }

  public void assertThatRemainingAmountMatchesExpected(String notesReceivableCode,
      String remainingAmount) {
    DObNotesReceivablesGeneralModel dObNotesReceivablesGeneralModel =
        (DObNotesReceivablesGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getDObNotesReceivablesByCode",
                notesReceivableCode);
    assertNotNull(dObNotesReceivablesGeneralModel);
    assertEquals(new BigDecimal(remainingAmount).compareTo(dObNotesReceivablesGeneralModel.getRemaining()),0);
  }

  public void assertThatNotesReceivableStateMatchesExpected(String notesReceivableCode,
      String notesReceivableState) {
    DObNotesReceivablesGeneralModel dObNotesReceivablesGeneralModel =
        (DObNotesReceivablesGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult(
                "getDObNotesReceivablesByCodeAndParallelStateAndDeliveredToTreasuryState",
                constructNotesReceivableStateQueryParams(notesReceivableCode,
                    notesReceivableState));
    assertNotNull(dObNotesReceivablesGeneralModel);
  }

  private Object[] constructNotesReceivableStateQueryParams(String notesReceivableCode,
      String notesReceivableState) {
    return new Object[] {notesReceivableCode, "%" + notesReceivableState + "%"};
  }

  private Object[] constructCollectionActivationDetailsParams(String collectionCode,
      Map<String, String> expectedActivationDetails) {
    return new Object[] {
        expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE),
        expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
        expectedActivationDetails.get(IAccountingFeatureFileCommonKeys.FISCAL_PERIOD),
        collectionCode, expectedActivationDetails.get(IFeatureFileCommonKeys.ACTIVATED_BY)};
  }

  public void assertThatSalesOrderRemainingAmountUpdated(String salesOrderCode,
      String remainingAmount) {

    DObSalesOrder salesOrder =
        (DObSalesOrder) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getSalesOrderByCodeAndRemaining", salesOrderCode, new BigDecimal(remainingAmount));

    Assert.assertNotNull(
        "Remaining amount of SalesOrder with code " + salesOrderCode + " wasn't updated correctly!",
        salesOrder);
  }

  public void assertThatSalesOrderStateMatchesExpected(String salesOrderCode, String currentState) {

    Object actualSalesOrder = entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getSalesOrderByCodeAndCurrentState", salesOrderCode, "%" + currentState + "%");

    assertNotNull(actualSalesOrder,
        "State of SO with code: " + salesOrderCode + " didn't match expected!");
  }

  public void assertAccountingDetailsAreUpdated(String collectionCode,
      DataTable accountingDetailsDataTable) {
    List<Map<String, String>> accountingDetailsMaps =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingDetailsMap : accountingDetailsMaps) {
      assertThatCollectionHasAccountingDetails(collectionCode, accountingDetailsMap);
    }
  }

  private void assertThatCollectionHasAccountingDetails(String collectionCode,
      Map<String, String> accountingDetailsMap) {

    Object[] queryParams =
        constructCollectionAccountingDetailsQueryParams(collectionCode, accountingDetailsMap);

    IObCollectionAccountingDetailsGeneralModel accountingDetails =
        (IObCollectionAccountingDetailsGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getCollectionAccountingDetails", queryParams);

    Assert.assertNotNull("Accounting details for Collection " + collectionCode + " doesn't exist ",
        accountingDetails);
  }

  private Object[] constructCollectionAccountingDetailsQueryParams(String collectionCode,
      Map<String, String> generalDataMap) {
    String glSubAccountName = "";
    String glSubAccount = null;
    if (!generalDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT).split(DASH)[1]
        .equals(NOTES_RECEIVABLES)) {
      if (generalDataMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT).equals(DEBIT)
          && generalDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT).contains(BANK)) {
        glSubAccountName =
            generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(DASH)[2];
        glSubAccount =
            generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(DASH)[0] + DASH
                + generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(DASH)[1];
      } else {
        glSubAccountName =
            generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(DASH)[1];
      }
    }
    if (glSubAccount == null) {
      glSubAccount =
          generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(DASH)[0];
    }

    return new Object[] {glSubAccount, glSubAccountName, collectionCode,
        generalDataMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
        generalDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
        generalDataMap.get(IAccountingFeatureFileCommonKeys.VALUE)};
  }

  public void assertThatDebitNoteRemainingIsUpdated(String debitNoteCode, String remaining) {
    Double remainingAmount = Double.parseDouble(remaining);
    Object debitNote = entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getUpdatedDebitNoteRemaining", debitNoteCode, remainingAmount);

    String message =
        "[Collection][Activate] DebitNote " + debitNoteCode + " remaining is not updated ";
    Assert.assertNotNull(message, debitNote);
  }
}
