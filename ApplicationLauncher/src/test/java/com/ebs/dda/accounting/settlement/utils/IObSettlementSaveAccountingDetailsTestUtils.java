package com.ebs.dda.accounting.settlement.utils;

import static org.junit.Assert.assertNotNull;
import java.util.Arrays;
import java.util.Map;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.settlement.IDObSettlementRestURLs;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IIObSettlementAddAccountingDetailsValueObject;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObSettlementSaveAccountingDetailsTestUtils extends DObSettlementsCommonTestUtils {

  private static final String CODE_NAME_SEPERATOR = " - ";

  public IObSettlementSaveAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response saveAccountingDetails(Cookie cookie, String settlementCode,
      DataTable itemsDataTable) {
    JsonObject accountingDetails = createAddAccountingDetailsRequestBody(itemsDataTable);
    String restURL =
        String.format(IDObSettlementRestURLs.UPDATE_ACCOUNTING_DETAILS, settlementCode);
    return sendPostRequest(cookie, restURL, accountingDetails);
  }

  private JsonObject createAddAccountingDetailsRequestBody(DataTable accDetailsDT) {
    Map<String, String> accDetailsData = accDetailsDT.asMaps(String.class, String.class).get(0);

    JsonObject accDetailsObject = new JsonObject();
    accDetailsObject.addProperty(IIObSettlementAddAccountingDetailsValueObject.ACCOUNT_TYPE,
        accDetailsData.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE));

    accDetailsObject.addProperty(IIObSettlementAddAccountingDetailsValueObject.GL_ACCOUNT,
        accDetailsData.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT)
            .split(CODE_NAME_SEPERATOR)[0]);

    String subAccount = null;
    if (!accDetailsData.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT).isEmpty()) {
      String[] glSubAccountParts = accDetailsData
          .get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT).split(CODE_NAME_SEPERATOR);
      if (glSubAccountParts.length == 2) {
        subAccount = glSubAccountParts[0];
      } else {
        subAccount = glSubAccountParts[0] + CODE_NAME_SEPERATOR + glSubAccountParts[1];
      }
    }
    accDetailsObject.addProperty(IIObSettlementAddAccountingDetailsValueObject.GL_SUB_ACCOUNT,
        subAccount);
    try {
      accDetailsObject.addProperty(IIObSettlementAddAccountingDetailsValueObject.AMOUNT,
          Double.parseDouble(accDetailsData.get(IAccountingFeatureFileCommonKeys.AMOUNT)));
    } catch (NumberFormatException exception) {
      accDetailsObject.addProperty(IIObSettlementAddAccountingDetailsValueObject.AMOUNT,
          Double.parseDouble(accDetailsData.get(IAccountingFeatureFileCommonKeys.AMOUNT)));
    }
    return accDetailsObject;
  }

  public void assertThatAccountingDetailsIsUpdated(String settlementCode,
      DataTable updatedDetailsDT) {

    Map<String, String> accDetailsMap = updatedDetailsDT.asMaps(String.class, String.class).get(0);
    Object[] queryParams = constructAccountingDetailsQueryParams(settlementCode, accDetailsMap);

    IObSettlementAccountDetailsGeneralModel accountingDetailsGM =
        (IObSettlementAccountDetailsGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getAddedSettlementAccountingDetails",
                queryParams);

    assertNotNull("Settlement AccountingDetails is not updated successfully, given "
        + Arrays.toString(queryParams), accountingDetailsGM);
  }


  private Object[] constructAccountingDetailsQueryParams(String settlementCode,
      Map<String, String> settlementMap) {
    return new Object[] {

        settlementMap.get(IAccountingFeatureFileCommonKeys.GL_SUBACCOUNT), settlementCode,
        settlementMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT_TYPE),
        settlementMap.get(IAccountingFeatureFileCommonKeys.GL_ACCOUNT),
        Double.parseDouble(settlementMap.get(IAccountingFeatureFileCommonKeys.AMOUNT))

    };
  }

  public void assertThatSettlementIsUpdated(String settlementCode, DataTable updatedSettlementDT) {
    Map<String, String> settlementMap =
        updatedSettlementDT.asMaps(String.class, String.class).get(0);
    Object[] queryParams = constructSettlementQueryParams(settlementCode, settlementMap);

    DObSettlementGeneralModel settlementGM =
        (DObSettlementGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getSettlementUpdateCheck", queryParams);

    assertNotNull("Settlement is not updated successfully, given " + Arrays.toString(queryParams),
        settlementGM);
  }

  private Object[] constructSettlementQueryParams(String settlementCode,
      Map<String, String> settlementMap) {
    return new Object[] {

        settlementCode, settlementMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
        settlementMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        '%' + settlementMap.get(IFeatureFileCommonKeys.STATE) + '%'

    };
  }



}
