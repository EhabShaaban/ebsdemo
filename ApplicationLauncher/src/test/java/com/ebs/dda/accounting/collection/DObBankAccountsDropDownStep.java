package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObBankAccountsDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObBankAccountsDropDownStep extends SpringBootRunner implements En {
  private DObBankAccountsDropDownTestUtils bankAccountsDropDownTestUtils;

  public DObBankAccountsDropDownStep() {
    When(
        "^\"([^\"]*)\" requests to read all BankAccounts with CompanyCode \"([^\"]*)\"$",
        (String userName, String companyCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
                  bankAccountsDropDownTestUtils.sendGETRequest(
                  cookie, bankAccountsDropDownTestUtils.getReadAllBankAccountsUrl(companyCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following BankAccounts values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable bankAccountsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
            bankAccountsDropDownTestUtils.assertThatBankAccountsResponseIsCorrect(bankAccountsDataTable, response);
        });

    Then(
        "^total number of BankAccounts returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer numberOfRecords) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
      bankAccountsDropDownTestUtils =
        new DObBankAccountsDropDownTestUtils(properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Read All BankAccounts Dropdown in Create Collection")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          bankAccountsDropDownTestUtils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read All BankAccounts Dropdown in Create Collection")) {
      databaseConnector.closeConnection();
    }
  }
}
