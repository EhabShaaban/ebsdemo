package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestPaymentDetailsTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestPaymentDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatTheFollowingPRPaymentDetailsExist(DataTable paymentDetailsTable) {
    List<Map<String, String>> prPaymentDetailsListMap =
        paymentDetailsTable.asMaps(String.class, String.class);

    for (Map<String, String> prPaymentDetailsMap : prPaymentDetailsListMap) {
      Object[] queryParams = constructPaymentRequestPaymentDetailsQueryParams(prPaymentDetailsMap);

      IObPaymentRequestPaymentDetailsGeneralModel prPaymentDetails =
          (IObPaymentRequestPaymentDetailsGeneralModel) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("getPRPaymentDetails", queryParams);

      Assert.assertNotNull("PR Payment Details doesn't exist for " + Arrays.toString(queryParams),
          prPaymentDetails);
    }
  }

  private Object[] constructPaymentRequestPaymentDetailsQueryParams(
      Map<String, String> paymentRequestMap) {
    String costFactor = paymentRequestMap.get(IAccountingFeatureFileCommonKeys.COST_FACTOR_ITEM);
    return new Object[] {

        paymentRequestMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER),
        paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE),
        paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT),
        costFactor == null ? "" : costFactor,
        paymentRequestMap.get(IAccountingFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER),
        paymentRequestMap.get(IFeatureFileCommonKeys.TREASURY),
        getBigDecimal(paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT)),
        paymentRequestMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO),
        paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_CODE),
        paymentRequestMap.get(IAccountingFeatureFileCommonKeys.PR_DESCRIPTION)};
  }

  public String getLockPaymentDetailsUrl(String code) {
    return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_COMMAND + "/" + code
        + IDObPaymentRequestRestURLs.LOCK_PAYMENT_DETAILS_SECTION;
  }
}
