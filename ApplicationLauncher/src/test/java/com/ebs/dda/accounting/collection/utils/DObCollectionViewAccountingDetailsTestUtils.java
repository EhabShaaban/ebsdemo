package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionAccountingDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ebs.dda.CommonKeys.DATA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DObCollectionViewAccountingDetailsTestUtils extends DObCollectionTestUtils {

  public DObCollectionViewAccountingDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String resetCollectionDataDependancies() {
    StringBuilder str = new StringBuilder();

    str.append(",")
        .append("db-scripts/accounting/collection/Clear_Collection_Activation_Dependancies.sql");

    return str.toString();

  }

  public void assertThatCollectionAccountingDetailsExists(DataTable accountingDetailsDataTable) {
    List<Map<String, String>> accountingDetailsMaps =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingDetailsMap : accountingDetailsMaps) {
      assertThatCollectionHasAccountingDetails(accountingDetailsMap);
    }
  }

  private void assertThatCollectionHasAccountingDetails(Map<String, String> accountingDetailsMap) {
    IObCollectionAccountingDetailsGeneralModel accountingDetails =
        (IObCollectionAccountingDetailsGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getCollectionAccountingDetails",
                constructCollectionAccountingDetailsQueryParams(accountingDetailsMap));
    Assert.assertNotNull(
        "Collection accounting detail doesn't exist " + accountingDetailsMap.toString(),
        accountingDetails);
  }

  private Object[] constructCollectionAccountingDetailsQueryParams(
      Map<String, String> generalDataMap) {
    String glSubAccountName = "";
    String glSubAccount = null;
    if (!generalDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT).split(" - ")[1]
        .equals("Notes receivables")) {
      if (generalDataMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT).equals("DEBIT")
          && generalDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT).contains("Bank")) {
        glSubAccountName =
            generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ")[2];
        glSubAccount =
            generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ")[0] + " - "
                + generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ")[1];
      } else {
        glSubAccountName =
            generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ")[1];
      }
    }
    if (glSubAccount == null) {
      glSubAccount =
          generalDataMap.get(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT).split(" - ")[0];
    }
    return new Object[] {glSubAccount, glSubAccountName,
        generalDataMap.get(IAccountingFeatureFileCommonKeys.C_CODE),
        generalDataMap.get(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT),
        generalDataMap.get(IAccountingFeatureFileCommonKeys.ACCOUNT),
        generalDataMap.get(IAccountingFeatureFileCommonKeys.VALUE)};
  }

  public void assertThatCollectionDoesNotHaveAccountingDetailsExists(
      DataTable accountingDetailsDataTable) {
    List<Map<String, String>> accountingDetailsMaps =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> accountingDetailsMap : accountingDetailsMaps) {
      assertThatCollectionDoesNotHaveAccountingDetails(accountingDetailsMap);
    }
  }

  private void assertThatCollectionDoesNotHaveAccountingDetails(
      Map<String, String> accountingDetailsMap) {
    IObCollectionAccountingDetailsGeneralModel accountingDetails =
        (IObCollectionAccountingDetailsGeneralModel) entityManagerDatabaseConnector
            .executeNativeNamedQuerySingleResult("getCollectionAccountingDetailsByCode",
                accountingDetailsMap.get(IAccountingFeatureFileCommonKeys.C_CODE));
    Assert.assertNull("Collection has accounting detail " + accountingDetailsMap.toString(),
        accountingDetails);
  }

  public Response readAccountingDetailsResponse(String collectionCode, Cookie userCookie) {
    String restURL =
        IDObCollectionRestURLs.GET_URL + collectionCode + IDObCollectionRestURLs.ACCOUNTING_DETAILS;
    return sendGETRequest(userCookie, restURL);
  }

  public void assertCorrectValuesAreDisplayed(Response response,
      DataTable accountingDetailsDataTable) throws Exception {
    List<Map<String, String>> expectedAccountingDetails =
        accountingDetailsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualAccountingDetails = getListOfMapsFromResponse(response);
    for (int index = 0; index < actualAccountingDetails.size(); index++) {
      MapAssertion mapAssertion = new MapAssertion(expectedAccountingDetails.get(index),
          actualAccountingDetails.get(index));
      assertThatAccountingDetailsMatches(mapAssertion);
      assertThatAmountMatches(expectedAccountingDetails.get(index),
          actualAccountingDetails.get(index));
    }
  }

  private void assertThatAmountMatches(Map<String, String> expectedAccountingDetail,
      HashMap<String, Object> actualAccountingDetail) {
    String[] expectedAmount =
        expectedAccountingDetail.get(IAccountingFeatureFileCommonKeys.VALUE).split(" ");
    assertEquals("Amount is not equal", Double.parseDouble(expectedAmount[0]),
        Double.parseDouble(actualAccountingDetail
            .get(IIObAccountingDocumentAccountDetailsGeneralModel.AMOUNT).toString()),
        1e-15);
  }

  private void assertThatAccountingDetailsMatches(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.C_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.DOCUMENT_CODE);
    mapAssertion.assertValue(IAccountingFeatureFileCommonKeys.CREDIT_DEBIT,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNTING_ENTRY);
    mapAssertion.assertCodeAndLocalizedNameValue(IAccountingFeatureFileCommonKeys.ACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.ACCOUNT_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(IAccountingFeatureFileCommonKeys.SUB_ACCOUNT,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_CODE,
        IIObAccountingDocumentAccountDetailsGeneralModel.SUB_ACCOUNT_NAME);
  }

  public void assertThatAccountingDetailsAreEmpty(Response response) {
    List<HashMap<String, Object>> items = response.body().jsonPath().getList(DATA);
    assertTrue(items.isEmpty());
  }
}
