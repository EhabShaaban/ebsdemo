package com.ebs.dda.accounting.landedcost.utils;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IIObLandedCostFactorItemValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.google.gson.JsonObject;

import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObLandedCostSaveCostFactorItemHappyPathTestUtils extends DObLandedCostTestUtils {

  public IObLandedCostSaveCostFactorItemHappyPathTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void assertThatLandedCostHasThoseCostFactorItem(
      String landedCostCode, DataTable itemsDataTable) {
    List<Map<String, String>> costFactorItems = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> costFactorItemItem : costFactorItems) {
      assertThatLandedCostHasCostFactorItem(landedCostCode, costFactorItemItem);
    }
  }

  public void assertThatLandedCostHasCostFactorItem(
      String landedCostCode, Map<String, String> costFactorItem) {
    IObLandedCostFactorItemsGeneralModel landedCostFactorItemsGeneralModel =
        (IObLandedCostFactorItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLandedCostFactorItems",
                constructLandedCostItems(landedCostCode, costFactorItem));
    assertNotNull(
        "LandedCost CostFactorItem  does not exist " + costFactorItem,
        landedCostFactorItemsGeneralModel);
  }

  public Object[] constructLandedCostItems(
      String landedCostCode, Map<String, String> landedCostItems) {
    return new Object[] {
      landedCostItems.get(IAccountingFeatureFileCommonKeys.ACTUAL_VALUE),
      landedCostItems.get(IAccountingFeatureFileCommonKeys.DIFFERENCE),
      landedCostCode,
      landedCostItems.get(IAccountingFeatureFileCommonKeys.ITEM),
      landedCostItems.get(IAccountingFeatureFileCommonKeys.ESTIMATED_VALUE),
    };
  }

  public void assertThatThoseItemsExist(DataTable costFactorItemsDataTable) {
    List<Map<String, String>> costFactorItems =
        costFactorItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> costFactorItem : costFactorItems) {
      assertThatItemExists(costFactorItem);
    }
  }

  private void assertThatItemExists(Map<String, String> item) {
    Object[] queryParams = constructItemQueryParams(item);

    MObItemGeneralModel mObItemGeneralModel =
        (MObItemGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getServiceItemByCodeNameAndPurchaseUnit", queryParams);
    Assertions.assertNotNull(mObItemGeneralModel);
    assertThatBusinessUnitsAreEqual(
        item.get(IFeatureFileCommonKeys.ITEM).split(" - ")[0],
        getBusinessUnitCodes(item.get(IFeatureFileCommonKeys.PURCHASE_UNIT).split(", ")),
        mObItemGeneralModel.getPurchasingUnitCodes());
  }

  public Object[] constructItemQueryParams(Map<String, String> item) {
    return new Object[] {item.get(IAccountingFeatureFileCommonKeys.ITEM)};
  }

  public String getLockUrl(String landedCostCode) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObLandedCostRestUrls.COMMAND_URL);
    lockUrl.append("/");
    lockUrl.append(landedCostCode);
    lockUrl.append(IDObLandedCostRestUrls.LOCK_LANDED_COST_FACTOR_ITEMS_SECTION);
    return lockUrl.toString();
  }

  public Response saveItemToLandedCost(
      Cookie cookie, String landedCostCode, DataTable costFactorItemItemsDataTable) {
    JsonObject landedCostItem = createLandedCostItemJsonObject(costFactorItemItemsDataTable);
    String restURL =
        IDObLandedCostRestUrls.COMMAND_URL
            + "/"
            + landedCostCode
            + IDObLandedCostRestUrls.UPDATE_LANDED_COST_FACTOR_ITEMS_SECTION;
    return sendPostRequest(cookie, restURL, landedCostItem);
  }

  private JsonObject createLandedCostItemJsonObject(DataTable landedCostItemsDataTable) {
    Map<String, String> itemData =
        landedCostItemsDataTable.asMaps(String.class, String.class).get(0);

    JsonObject landedCostItemObject = new JsonObject();
    landedCostItemObject.addProperty(
        IIObLandedCostFactorItemValueObject.ITEM_CODE,
        itemData.get(IAccountingFeatureFileCommonKeys.ITEM).split(" - ")[0]);
    try {
      landedCostItemObject.addProperty(
          IIObLandedCostFactorItemValueObject.ESTIMATED_VALUE,
          Double.parseDouble(itemData.get(IAccountingFeatureFileCommonKeys.VALUE)));
    } catch (NumberFormatException exception) {
      landedCostItemObject.addProperty(
          IIObLandedCostFactorItemValueObject.ESTIMATED_VALUE,
          itemData.get(IAccountingFeatureFileCommonKeys.VALUE));
    }
    return landedCostItemObject;
  }

  public void assertThatLandedCostIsUpdated(
      String landedCostCode, DataTable updatedDetailsDataTable) {
    Map<String, String> landedCostMap =
        updatedDetailsDataTable.asMaps(String.class, String.class).get(0);
    Object[] queryParams = constructLandedCostQueryParams(landedCostCode, landedCostMap);

    DObLandedCostGeneralModel landedCostGeneralModel =
        (DObLandedCostGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLandedCostUpdateCheck", queryParams);

    assertNotNull(
        "Landed Cost is not updated successfully, given " + Arrays.toString(queryParams),
        landedCostGeneralModel);
  }

  private Object[] constructLandedCostQueryParams(
      String landedCostCode, Map<String, String> landedCostMap) {
    return new Object[] {
      landedCostCode,
      landedCostMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      landedCostMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      '%' + landedCostMap.get(IFeatureFileCommonKeys.STATE) + '%'
    };
  }

  public void assertThatThoseItemsAreNotCostFactorItems(DataTable costFactorItemsDataTable) {
    List<Map<String, String>> itemsList =
        costFactorItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsList) {
      assertThatItemIsNotCostFactorItem(item);
    }
  }

  private void assertThatItemIsNotCostFactorItem(Map<String, String> item) {
    Object[] queryParams = constructCheckItemIsNotCostFactorItemQueryParams(item);

    MObItemGeneralModel mObItemGeneralModel =
        (MObItemGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "checkItemIsNotCostFactorItem", queryParams);
    Assertions.assertNotNull(mObItemGeneralModel);
  }

  public Object[] constructCheckItemIsNotCostFactorItemQueryParams(Map<String, String> item) {
    return new Object[] {item.get(IAccountingFeatureFileCommonKeys.ITEM)};
  }

  public void assertThatLandedCostHasItemsSummary(DataTable itemsSummaryDataTable) {
    List<Map<String, String>> costFactorItemsSummary =
        itemsSummaryDataTable.asMaps(String.class, String.class);
    for (Map<String, String> summary : costFactorItemsSummary) {
      Object[] queryParams = constructLandedCostItemsSummaryParemeters(summary);
      IObLandedCostFactorItemsSummary landedCostFactorItemsSummaryGeneralModel =
          (IObLandedCostFactorItemsSummary)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getLandedCostFactorItemsSummaryByCodeAndEstimateActualAndDifference",
                  queryParams);
      assertNotNull(
          "LandedCost CostFactorItemsSummary  does not exist " + Arrays.toString(queryParams),
          landedCostFactorItemsSummaryGeneralModel);
    }
  }

  private Object[] constructLandedCostItemsSummaryParemeters(
      Map<String, String> costFactorItemsSummary) {
    return new Object[] {
      costFactorItemsSummary.get(IAccountingFeatureFileCommonKeys.ACTUAL_VALUE),
      costFactorItemsSummary.get(IAccountingFeatureFileCommonKeys.DIFFERENCE),
      costFactorItemsSummary.get(IAccountingFeatureFileCommonKeys.LC_CODE),
      costFactorItemsSummary.get(IAccountingFeatureFileCommonKeys.ESTIMATED_VALUE),
    };
  }
}
