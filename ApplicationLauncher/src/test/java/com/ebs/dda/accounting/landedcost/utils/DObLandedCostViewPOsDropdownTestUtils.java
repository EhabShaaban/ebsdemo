package com.ebs.dda.accounting.landedcost.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.CommonKeys;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.UserActionsTestUtils;
import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObLandedCostViewPOsDropdownTestUtils extends DObLandedCostTestUtils {

  public DObLandedCostViewPOsDropdownTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector dbManager) {
    super(properties);
    setEntityManagerDatabaseConnector(dbManager);
  }

  public void requestToreadPurchaseOrders(String username, String type,
      UserActionsTestUtils userActionsTestUtils) {
    Cookie cookie = userActionsTestUtils.getUserCookie(username);
    String restUrl = IDObLandedCostRestUrls.VIEW_LC_POS + "?landedCostType=" + type;
    Response response = sendGETRequest(cookie, restUrl);
    userActionsTestUtils.setUserResponse(username, response);
  }

  public void assertThatPurchaseOrdersAreRetrieved(Response response, DataTable purchaseOrdersDT) {
    List<Map<String, String>> expectedPOList = purchaseOrdersDT.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualPOList = getListOfMapsFromResponse(response);
    int index = 0;
    for (Map<String, String> expectedPO : expectedPOList) {
      HashMap<String, Object> actualPO = actualPOList.get(index++);
      assertThatPoIsRetrived(expectedPO, actualPO);
    }
  }

  private void assertThatPoIsRetrived(Map<String, String> expected,
      HashMap<String, Object> actual) {

    String expectedValue = expected.get(IFeatureFileCommonKeys.PURCHASE_ORDER);
    String[] parts = expectedValue.split(" - ");

    String expectedCode = parts[0];
    Assert.assertEquals(expectedCode, actual.get(CommonKeys.USER_CODE));

    String expectedState = parts[1];
    List<String> currentStates =
        (List<String>) actual.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME);
    Assert.assertTrue(currentStates.contains(expectedState));

  }

  public void assetThatTotalNumberOfRetrievedPOsIs(Response response, Integer numOfRecords) {
    List<HashMap<String, Object>> actualDocOwners = getListOfMapsFromResponse(response);
    Assert.assertEquals(numOfRecords.intValue(), actualDocOwners.size());
  }

}


