package com.ebs.dda.accounting.collection.utils;

import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.collection.IIObCollectionDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObCollectionViewCollectionDetailsTestUtils extends DObCollectionTestUtils {
  private final String BANK = "BANK";
  private final String CASH = "CASH";

  public DObCollectionViewCollectionDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response getCollectionDetailsForCollection(Cookie userCookie, String collectionCode) {
    String collectionDetailsUrl =
        IDObCollectionRestURLs.GET_URL + collectionCode + IDObCollectionRestURLs.COLLECTION_DETAILS;
    return sendGETRequest(userCookie, collectionDetailsUrl);
  }

  public void assertCorrectValuesAreDisplayedInCollectionDetails(
      Response response, DataTable collectionCompanyData) throws Exception {

    Map<String, String> expectedCompanyData =
        collectionCompanyData.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualCompanyData = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedCompanyData, actualCompanyData);
    assertActualAndExpectedDataFromResponse(mapAssertion);
  }

  private void assertActualAndExpectedDataFromResponse(MapAssertion mapAssertion) throws Exception {
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_TYPE,
        IIObCollectionDetailsGeneralModel.REF_DOCUMENT_TYPE);
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT_CODE,
        IIObCollectionDetailsGeneralModel.REF_DOCUMENT_CODE);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER,
        IIObCollectionDetailsGeneralModel.BUSINESS_PARTNER_CODE,
        IIObCollectionDetailsGeneralModel.BUSINESS_PARTNER_NAME);
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.CURRENCY, IIObCollectionDetailsGeneralModel.CURRENCY_ISO);
    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.AMOUNT, IIObCollectionDetailsGeneralModel.AMOUNT);
    mapAssertion.assertValue(
        IAccountingFeatureFileCommonKeys.COLLECTION_METHOD,
        IIObCollectionDetailsGeneralModel.COLLECTION_METHOD);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.BANK_ACCOUNT,
        IIObCollectionDetailsGeneralModel.BANK_ACCOUNT_NUMBER,
        IIObCollectionDetailsGeneralModel.BANK_ACCOUNT_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.TREASURY,
        IIObCollectionDetailsGeneralModel.TREASURY_CODE,
        IIObCollectionDetailsGeneralModel.TREASURY_NAME);
  }
}
