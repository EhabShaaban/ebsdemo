package com.ebs.dda.accounting.salesinvoice.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IIObInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.accounting.taxes.ILObCompanyTaxesGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class IObSalesInvoiceTaxesViewTestUtils extends DObSalesInvoiceTestUtils {

  public IObSalesInvoiceTaxesViewTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder(getMasterDataScripts());
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");

    return str.toString();
  }
  public void assertThatTheFollowingTaxDetailsExistForSalesInvoice(
      String salesInvoiceCode, DataTable taxDetailsDataTable) {
    List<Map<String, String>> taxDetailsMaps =
        taxDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> taxDetailsMap : taxDetailsMaps) {

      Object[] queryParams =
          constructCompanyTaxesForSalesInvoiceQueryParams(salesInvoiceCode, taxDetailsMap);

      IObSalesInvoiceTaxesGeneralModel salesInvoiceCompanyTaxesDetailsGeneralModel =
          (IObSalesInvoiceTaxesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getTaxesDetailsForCompanyInSalesInvoice", queryParams);
      Assert.assertNotNull(
          "Tax detail doesn't exist " + Arrays.toString(queryParams),
          salesInvoiceCompanyTaxesDetailsGeneralModel);
    }
  }

  public Object[] constructCompanyTaxesForSalesInvoiceQueryParams(
      String salesInvoiceCode, Map<String, String> companyTaxesValues) {
    String taxAmountField = companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT);
    return new Object[] {
      salesInvoiceCode,
      companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX),
      new BigDecimal(companyTaxesValues.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)),
      (taxAmountField != null) ? new BigDecimal(taxAmountField) : null
    };
  }

  public void assertCorrectValuesAreDisplayedInTaxesSection(
      Response response, DataTable taxValuesDataTable) throws Exception {
    List<Map<String, String>> expectedTaxes = taxValuesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualTaxes = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedTaxes.size(); i++) {
      assertActualAndExpectedDataFromResponse(expectedTaxes.get(i), actualTaxes.get(i));
    }
  }

  private void assertActualAndExpectedDataFromResponse(
      Map<String, String> expectedCompanyTaxDetail, HashMap<String, Object> actualCompanyTaxDetail)
      throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedCompanyTaxDetail, actualCompanyTaxDetail);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.TAX_NAME,
        ILObCompanyTaxesGeneralModel.TAX_CODE,
        ILObCompanyTaxesGeneralModel.TAX_NAME);

    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE,
        IIObInvoiceTaxesGeneralModel.TAX_Percentage);

    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.TAX_AMOUNT, IIObInvoiceTaxesGeneralModel.TAX_AMOUNT);
  }

  public void assertThatTaxDetailsExist(DataTable taxDetailsDataTable) {

    List<Map<String, String>> taxDetailsMaps =
        taxDetailsDataTable.asMaps(String.class, String.class);

    for (Map<String, String> taxDetailsMap : taxDetailsMaps) {
      IObSalesInvoiceTaxesGeneralModel taxesDetailsForCompanyInSalesInvoice =
          (IObSalesInvoiceTaxesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getTaxesDetailsForCompanyInSalesInvoice",
                  constructTaxesDetailQueryParams(taxDetailsMap));
      assertNotNull(taxDetailsMap.toString(), taxesDetailsForCompanyInSalesInvoice);
    }
  }

  private Object[] constructTaxesDetailQueryParams(Map<String, String> taxDetailsMap) {
    return new Object[] {
      taxDetailsMap.get(ISalesFeatureFileCommonKeys.SI_CODE),
      taxDetailsMap.get(IAccountingFeatureFileCommonKeys.TAX),
      new BigDecimal(taxDetailsMap.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE)),
      new BigDecimal(taxDetailsMap.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT))
    };
  }
}
