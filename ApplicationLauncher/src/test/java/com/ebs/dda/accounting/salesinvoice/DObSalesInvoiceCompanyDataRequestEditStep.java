package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceCompanyDataRequestEditTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceCompanyDataRequestEditStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceCompanyDataRequestEditTestUtils utils;

  public DObSalesInvoiceCompanyDataRequestEditStep() {
    When(
        "^\"([^\"]*)\" requests to edit CompanyData section of SalesInvoice with code \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode) -> {
          StringBuilder lockUrl = utils.getLockUrl(salesInvoiceCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockUrl.toString(), salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^CompanyData section of SalesInvoice with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.COMPANY_DATA_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened CompanyData section of SalesInvoice with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String salesInvoiceCode) -> {
          StringBuilder lockUrl = utils.getLockUrl(salesInvoiceCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockUrl.toString(), salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Given(
        "^\"([^\"]*)\" first deleted SalesInvoice with code \"([^\"]*)\" successfully$",
        (String userName, String salesInvoiceCode) -> {
          Response salesInvoiceDeleteResponse =
              utils.deleteSalesInvoice(
                  userActionsTestUtils.getUserCookie(userName), salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, salesInvoiceDeleteResponse);
        });
    Given(
        "^CompanyData section of SalesInvoice with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName, String lockTime) -> {
          utils.freeze(lockTime);
          StringBuilder lockUrl = utils.getLockUrl(salesInvoiceCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockUrl.toString(), salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving CompanyData section of SalesInvoice with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode, String unlockTime) -> {
          utils.freeze(unlockTime);
          String unLockUrl = utils.getUnLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on CompanyData section of SalesInvoice with code \"([^\"]*)\" is released$",
        (String userName, String salesInvoiceCode) -> {
          utils.assertThatLockIsReleased(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.COMPANY_DATA_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first deleted the SalesInvoice with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String salesInvoiceCode, String deletionTime) -> {
          utils.freeze(deletionTime);
          Response salesInvoiceDeleteResponse =
              utils.deleteSalesInvoice(
                  userActionsTestUtils.getUserCookie(userName), salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, salesInvoiceDeleteResponse);
        });
    Then(
        "^CompanyData section of SalesInvoice with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.COMPANY_DATA_SECTION);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObSalesInvoiceCompanyDataRequestEditTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "Request to edit CompanyData section")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to edit CompanyData section")) {
      utils.unfreeze();

        userActionsTestUtils.unlockAllSections(
                IDObSalesInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());


        databaseConnector.closeConnection();
    }
  }
}
