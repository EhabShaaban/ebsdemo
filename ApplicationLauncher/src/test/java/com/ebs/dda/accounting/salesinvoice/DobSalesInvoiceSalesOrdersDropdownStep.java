package com.ebs.dda.accounting.salesinvoice;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceSalesOrderDropDownTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class DobSalesInvoiceSalesOrdersDropdownStep extends SpringBootRunner implements En {

	private static boolean hasBeenExecuted = false;
	private DObSalesInvoiceSalesOrderDropDownTestUtils utils;

	public DobSalesInvoiceSalesOrdersDropdownStep() {

		Given("^the total number of records of SalesOrders is (\\d+)$", (Long recordsNumber) -> {
			utils.assertThatNumberOfSalesOrdersIS(recordsNumber);
		});
		When("^\"([^\"]*)\" requests to read list of Approved and GoodsIssuedActivated SalesOrders with Business Unit \"([^\"]*)\"$",
						(String userName, String businessUnit) -> {
							Map<String, Object> queryParams = new HashMap<>();
							queryParams.put("business-unit-code", businessUnit.split(" - ")[0]);
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							String restUrl = IDObSalesInvoiceRestUrls.GET_URL
											+ IDObSalesInvoiceRestUrls.SALES_ORDERS;
							Response response = utils.sendGETRequestWithQueryParams(cookie,restUrl, queryParams);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		Then("^the following SalesOrders values will be presented to \"([^\"]*)\":$",
						(String userName, DataTable salesOrdersDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertThatSalesOrdersActualResponseMatchesExpected(response,
											salesOrdersDataTable);
						});

		Then("^total number of SalesOrders returned to \"([^\"]*)\" is equal to (\\d+)$",
						(String userName, Integer recordsNumber) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertTotalNumberOfRecordsEqualResponseSize(response,
											recordsNumber);
						});

	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		utils = new DObSalesInvoiceSalesOrderDropDownTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

	}

	@Before
	public void setup(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), "Read list of SalesOrders")) {
			databaseConnector.createConnection();
			if (!hasBeenExecuted) {
				databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
				hasBeenExecuted = true;
			}
		}
	}

	@After
	public void afterEachScenario(Scenario scenario) throws SQLException {
		if (contains(scenario.getName(), "Read list of SalesOrders")) {
			databaseConnector.closeConnection();
		}
	}
}
