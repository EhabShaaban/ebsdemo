package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

public class DObMonetaryNotesViewGeneralDataTestUtils extends DObMonetaryNotesCommonTestUtils {
  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[View][General Data]";

  public DObMonetaryNotesViewGeneralDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readGeneralData(Cookie cookie, String monetaryNoteCode) {
    return sendGETRequest(cookie, getViewGeneralDataUrl(monetaryNoteCode));
  }

  private String getViewGeneralDataUrl(String monetaryNoteCode) {
    return IDObNotesReceivableRestURLs.QUERY
        + "/"
        + monetaryNoteCode
        + IDObNotesReceivableRestURLs.VIEW_GENERAL_DATA;
  }

  public void assertMonetaryNoteGeneralDataIsCorrect(
      String monetaryNoteCode, Response response, DataTable generalDataTable) {
    Map<String, String> expectedGeneralData =
        generalDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualGeneralData = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedGeneralData, actualGeneralData);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObNotesReceivablesGeneralModel.USER_CODE);

    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE, IDObNotesReceivablesGeneralModel.CREATION_DATE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.TYPE, IDObNotesReceivablesGeneralModel.OBJECT_TYPE_CODE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY, IDObNotesReceivablesGeneralModel.CREATION_INFO);

    mapAssertion.assertValue(
        IOrderFeatureFileCommonKeys.DOCUMENT_OWNER,
        IDObNotesReceivablesGeneralModel.DOCUMENT_OWNER_NAME);

    mapAssertion.assertValueContains(
        IFeatureFileCommonKeys.STATE, IDObNotesReceivablesGeneralModel.CURRENT_STATES);
  }
}
