package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObNotesReceivableTypeDropdownTestUtils extends DObMonetaryNotesCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Type][Dropdown]";

  public DObNotesReceivableTypeDropdownTestUtils(Map<String, Object> properties, EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties,entityManagerDatabaseConnector);
  }

  public Response readAllNotesReceivableTypes(Cookie cookie) {
    String restURL = IDObNotesReceivableRestURLs.READ_ALL_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatNotesReceivableTypesResponseIsCorrect(
      DataTable notesReceivableTypesDataTable, Response response) {
    List<Map<String, String>> expectedNotesReceivableTypeMapList =
        notesReceivableTypesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualNotesReceivableTypesList =
        getListOfMapsFromResponse(response);
    Assert.assertEquals(
        expectedNotesReceivableTypeMapList.size(), actualNotesReceivableTypesList.size());
    for (int i = 0; i < expectedNotesReceivableTypeMapList.size(); i++) {
      assertThatExpectedNotesReceivableTypeMatchesActual(
          expectedNotesReceivableTypeMapList.get(i), actualNotesReceivableTypesList.get(i));
    }
  }

  private void assertThatExpectedNotesReceivableTypeMatchesActual(
      Map<String, String> expectedNotesReceivableType, Object actualNotesReceivableType) {
    Assert.assertEquals(
        expectedNotesReceivableType.get(IFeatureFileCommonKeys.TYPE), actualNotesReceivableType);
  }

  public void assertThatTotalNumberOfNotesReceivableTypesEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualNotesReceivableTypes = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + "Total number of NotesReceivable Types is not correct",
        expectedTotalNumber.intValue(),
        actualNotesReceivableTypes.size());
  }
}
