package com.ebs.dda.accounting.initialbalanceupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.initialbalanceupload.utils.DObInitailBalanceUploadViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObInitialBalanceUploadViewAllStep extends SpringBootRunner implements En {

  private DObInitailBalanceUploadViewAllTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitailBalanceUploadViewAllTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObInitialBalanceUploadViewAllStep() {
    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of InitialBalanceUploads with no filter applied with (\\d+) records per page$",
        (String userName, Integer pageNum, Integer recordsNum) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.requestToReadFilteredData(
                  userCookie, IDObInitialBalanceUploadRestURLS.VIEW_ALL_URL, pageNum, recordsNum, "");
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable initialBalanceUploadDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
                    utils.assertInitailBalanceUploadDataIsCorrect(
                        initialBalanceUploadDataTable, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(
              response, totalRecordsNumber);
        });
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all InitialBalanceUploads -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearIBUInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View all InitialBalanceUploads -")) {
      databaseConnector.closeConnection();
    }
  }
}
