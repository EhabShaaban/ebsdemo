package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.DObMonetaryNotesViewCompanyDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObMonetaryNotesViewCompanyDataStep extends SpringBootRunner implements En {

    DObMonetaryNotesViewCompanyDataTestUtils utils;

  public DObMonetaryNotesViewCompanyDataStep() {

    When(
        "^\"([^\"]*)\" requests to view CompanyData section of MonetaryNotes with code \"([^\"]*)\"$",
        (String userName, String monetaryNoteCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readCompanyData(cookie, monetaryNoteCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of CompanyData section for MonetaryNotes with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String monetaryNoteCode, String userName, DataTable companyDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertMonetaryNoteCompanyDataIsCorrect(
              monetaryNoteCode, response, companyDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObMonetaryNotesViewCompanyDataTestUtils(getProperties(),entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View MonetaryNotes CompanyData section")) {
      databaseConnector.executeSQLScript(utils.getClearMonetaryNotesDbScripts());
    }
  }
}
