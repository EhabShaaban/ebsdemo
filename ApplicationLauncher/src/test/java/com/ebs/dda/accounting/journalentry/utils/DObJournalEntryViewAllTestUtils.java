package com.ebs.dda.accounting.journalentry.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import com.ebs.dda.jpa.accounting.journalentry.IDObJournalEntryGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class DObJournalEntryViewAllTestUtils extends DObJournalEntryTestUtils {

  public DObJournalEntryViewAllTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public void insertJournalEntries(DataTable journalEntriesDataTable) {

    List<Map<String, String>> journalEntriesMapList =
        journalEntriesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> journalEntriesMap : journalEntriesMapList) {

      Object[] journalEntryParams = constructJournalEntryInsertionParams(journalEntriesMap);
      Object[] journalEntryRefDocumentParams =
          constructJournalEntryRefDocumentInsertionParams(journalEntriesMap);

      entityManagerDatabaseConnector.executeInsertQuery("createJournalEntry", journalEntryParams);
      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
          "createJournalEntryReferenceDocument", journalEntryRefDocumentParams);
    }
  }

  private Object[] constructJournalEntryInsertionParams(Map<String, String> journalEntriesMap) {
    String[] referenceDocument =
        journalEntriesMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT).split(" - ");
    String documentType = referenceDocument[1];
    return new Object[] {
      journalEntriesMap.get(IFeatureFileCommonKeys.CODE),
      journalEntriesMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      journalEntriesMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      journalEntriesMap.get(IFeatureFileCommonKeys.CREATED_BY),
      journalEntriesMap.get(IFeatureFileCommonKeys.CREATED_BY),
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_DATE),
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.COMPANY),
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT),
      documentType,
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.FISCAL_PERIOD)
    };
  }

  private Object[] constructJournalEntryRefDocumentInsertionParams(
      Map<String, String> journalEntriesMap) {
    String[] referenceDocument =
        journalEntriesMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT).split(" - ");
    String documentCode = referenceDocument[0];
    String documentType = referenceDocument[1];
    return new Object[] {
      documentType, documentCode, journalEntriesMap.get(IFeatureFileCommonKeys.CODE)
    };
  }

  public void assertThatJournalEntriesExistWithCompanyAndRefDocumentCodeName(
      DataTable journalEntriesDataTable) {

    List<Map<String, String>> journalEntriesMap =
        journalEntriesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> journalEntry : journalEntriesMap) {

      DObJournalEntryGeneralModel journalEntryGeneralModel =
          (DObJournalEntryGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getJournalEntryWithConcatenatedCompanyCodeName",
                  constructJournalEntriesWhenCompanyConcatinatedParams(journalEntry));
      assertDateEquals(
          journalEntry.get(IFeatureFileCommonKeys.CREATION_DATE),
          journalEntryGeneralModel.getCreationDate());
      assertDateEquals(
          journalEntry.get(IAccountingFeatureFileCommonKeys.JOURNAL_DATE),
          journalEntryGeneralModel.getDueDate());
      assertNotNull("Journal Entry Exists" + journalEntriesMap, journalEntryGeneralModel);
    }
  }

  private Object[] constructJournalEntriesWhenCompanyConcatinatedParams(
      Map<String, String> journalEntriesMap) {
    return new Object[] {
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR),
      journalEntriesMap.get(IFeatureFileCommonKeys.CODE),
      journalEntriesMap.get(IFeatureFileCommonKeys.CREATED_BY),
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.BUSINESS_UNIT),
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT),
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.COMPANY),
      journalEntriesMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR)
    };
  }

  public void assertThatJournalEntriesArePresentedToUser(
      Response response, DataTable journalEntriesDataTable) throws Exception {
    List<HashMap<String, Object>> actualJournalEntries = getListOfMapsFromResponse(response);

    List<Map<String, String>> expectedJournalEntriesMap =
        journalEntriesDataTable.asMaps(String.class, String.class);

    for (int i = 0; i < actualJournalEntries.size(); i++) {
      assertThatJournalEntriesMatchesTheExpected(
          expectedJournalEntriesMap.get(i), actualJournalEntries.get(i));
    }
  }

  private void assertThatJournalEntriesMatchesTheExpected(
      Map<String, String> expectedJournalEntriesMap,
      HashMap<String, Object> actualJournalEntriesMap) throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedJournalEntriesMap, actualJournalEntriesMap);
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE,
        IDObJournalEntryGeneralModel.CODE);
    mapAssertion.assertDateTime(
        IFeatureFileCommonKeys.CREATION_DATE,
        IDObJournalEntryGeneralModel.CREATION_DATE);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CREATED_BY,
        IDObJournalEntryGeneralModel.CREATED_BY);
    mapAssertion.assertDateTime(
        IAccountingFeatureFileCommonKeys.JOURNAL_DATE,
        IDObJournalEntryGeneralModel.DUE_DATE);
    mapAssertion.assertConcatenatingValue(
        IAccountingFeatureFileCommonKeys.REFERENCE_DOCUMENT,
        IDObJournalEntryGeneralModel.DOCUMENT_REFERENCE_CODE
            ,
           IDObJournalEntryGeneralModel.DOCUMENT_REFERENCE_TYPE, " - ");
    mapAssertion.assertLocalizedValue( IAccountingFeatureFileCommonKeys.COMPANY, IDObJournalEntryGeneralModel.COMPANY_NAME);
    mapAssertion.assertCodeAndLocalizedNameValue( IAccountingFeatureFileCommonKeys.BUSINESS_UNIT, IDObJournalEntryGeneralModel.PURCHASE_UNIT_CODE, IDObJournalEntryGeneralModel.PURCHASE_UNIT_NAME);
  }
}
