package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostCommonTestUtils;
import com.ebs.dda.accounting.landedcost.utils.IObLandedCostSaveCostFactorItemHappyPathTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPostHappyPathTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.*;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class DObVendorInvoiceActivateStep extends SpringBootRunner implements En, InitializingBean {

	private DObVendorInvoiceActivateTestUtils dObInvoiceActivateTestUtils;
	private DObVendorInvoiceViewGeneralDataTestUtils dObInvoiceGeneralDataViewTestUtils;
	private DObVendorInvoiceViewDetailsTestUtils dObInvoiceViewDetailsTestUtils;
	private IObVendorInvoiceItemsViewTestUtils iObInvoiceItemsViewTestUtils;
	private DObPaymentRequestPostHappyPathTestUtils dObPaymentRequestPostHappyPathTestUtils;
	private IObLandedCostSaveCostFactorItemHappyPathTestUtils landedCostUtils;
	private IObVendorInvoiceItemsTestUtils iObInvoiceItemsTestUtils;

	private boolean hasBeenExecutedOnce = false;

	public DObVendorInvoiceActivateStep() {
		Given("^the following Invoice exists:$", (DataTable invoicesDataTable) -> {
			dObInvoiceActivateTestUtils.assertThatInvoicesExistWithCodeAndState(invoicesDataTable);
		});
		Given("^Invoices have the following Details:$", (DataTable invoicesDataTable) -> {
			dObInvoiceActivateTestUtils.assertThatInvoicesExistWithDetails(invoicesDataTable);
		});



		Given("^the following details for Vendor Invoices exist:$",
						(DataTable invoicesDetailsDataTable) -> {
							dObInvoiceViewDetailsTestUtils.assertInvoicesExistWithDetails(
											invoicesDetailsDataTable);
						});
		Given("^Invoice with code \"([^\"]*)\" has the following items:$",
						(String invoiceCode, DataTable invoiceItemsDataTable) -> {
							iObInvoiceItemsTestUtils.assertThatInvoiceHasItems(invoiceCode,
											invoiceItemsDataTable);
						});

		Given("^first \"([^\"]*)\" opens \"([^\"]*)\" of Invoice with Code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
						(String userName, String invoiceSectionName, String invoiceCode,
										String date) -> {
							dObInvoiceActivateTestUtils.freeze(date);
							String lockUrl = null;
							if (invoiceSectionName.matches("InvoiceDetails")) {
								lockUrl = IDObVendorInvoiceRestUrls.COMMAND_URL + invoiceCode
												+ IDObVendorInvoiceRestUrls.LOCK_INVOICE_DETAILS_SECTION;
							} else if (invoiceSectionName.matches("InvoiceItems")) {

								lockUrl = IDObVendorInvoiceRestUrls.COMMAND_URL + invoiceCode
												+ IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION;
							}
							Response response = userActionsTestUtils.lockSection(userName, lockUrl,
											invoiceCode);
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Given("^the following VendorInvoice has an empty InvoiceItems section:$",
						(DataTable vendorInvoiceDataTable) -> {
							iObInvoiceItemsTestUtils.assertEmptyInvoiceItemsInVendorInvoice(
											vendorInvoiceDataTable);
						});

		Then("^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
				(String missingFields, String userName) -> {
					dObInvoiceActivateTestUtils
							.assertThatResponseHasMissingFields(
									userActionsTestUtils.getUserResponse(
											userName),
									missingFields);
				});

		When("^\"([^\"]*)\" Activates VendorInvoice with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
						(String userName, String invoiceCode, String date,
										DataTable postingDetailsDataTable) -> {
							dObInvoiceActivateTestUtils.freeze(date);
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							Response response = dObInvoiceActivateTestUtils.postInvoice(userCookie,
											invoiceCode, date, postingDetailsDataTable);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		When("^\"([^\"]*)\" Activates VendorInvoice of type \"([^\"]*)\" with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
						(String userName,String invoiceType, String invoiceCode, String date,
										DataTable postingDetailsDataTable) -> {
							dObInvoiceActivateTestUtils.freeze(date);
							Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
							Response response = dObInvoiceActivateTestUtils.activateVendorInvoice(userCookie,
											invoiceCode, invoiceType, date, postingDetailsDataTable);
							userActionsTestUtils.setUserResponse(userName, response);
						});
		Then("^VendorInvoice with Code \"([^\"]*)\" is updated as follows:$",
						(String invoiceCode, DataTable invoiceDataTable) -> {
							dObInvoiceActivateTestUtils.assertThatInvoiceIsUpdatedByUserAtDate(
											invoiceCode, invoiceDataTable);
						});
		Then("^ActivationDetails in VendorInvoice with Code \"([^\"]*)\" is updated as follows:$",
						(String invoiceCode, DataTable postingDetailsDataTable) -> {
							dObInvoiceActivateTestUtils.assertThatInvoicePostingDetailAreUpdated(
											invoiceCode, postingDetailsDataTable);
						});
		Then("^LandedCost with Code \"([^\"]*)\" is updated as follows:$", (String landedCostCode, DataTable landedCostDataTable) -> {
			landedCostUtils.assertThatLandedCostIsUpdated(landedCostCode,
					landedCostDataTable);
		});
		Then("^the LandedCostDetails section of LandedCost with code \"([^\"]*)\" is updated as follows:$",
				(String landedCostCode, DataTable landedCostDetailsDataTable) -> {
					dObInvoiceActivateTestUtils.assertThatLandedCostDetailsIsUpdated(landedCostCode, landedCostDetailsDataTable);
		});
		And("^there is no LandedCost in state \"([^\"]*)\" for the following PurchaseOrders:$",
				(String landedCostState, DataTable purchaseOrdersDataTable) -> {
					dObInvoiceActivateTestUtils.assertThatNoLandedCostsExistForPurchaseOrders(landedCostState, purchaseOrdersDataTable);
		});
	}

	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		Map<String, Object> properties = getProperties();
		dObInvoiceActivateTestUtils = new DObVendorInvoiceActivateTestUtils(properties);
		dObInvoiceActivateTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		dObInvoiceViewDetailsTestUtils = new DObVendorInvoiceViewDetailsTestUtils(properties);
		dObInvoiceViewDetailsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		dObInvoiceGeneralDataViewTestUtils = new DObVendorInvoiceViewGeneralDataTestUtils(
						properties);
		dObInvoiceGeneralDataViewTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		iObInvoiceItemsViewTestUtils = new IObVendorInvoiceItemsViewTestUtils(properties);
		iObInvoiceItemsViewTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		dObPaymentRequestPostHappyPathTestUtils = new DObPaymentRequestPostHappyPathTestUtils(
						properties);
		dObPaymentRequestPostHappyPathTestUtils
						.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		iObInvoiceItemsTestUtils = new IObVendorInvoiceItemsTestUtils(properties);
		iObInvoiceItemsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		landedCostUtils = new IObLandedCostSaveCostFactorItemHappyPathTestUtils(properties, entityManagerDatabaseConnector);
	}

	@Before
	public void before(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), "Activate Vendor Invoice")) {
			databaseConnector.createConnection();
			if (!hasBeenExecutedOnce) {
				databaseConnector.executeSQLScript(
								dObInvoiceActivateTestUtils.getDbScriptsOneTimeExecution());
				hasBeenExecutedOnce = true;
			}
			databaseConnector.executeSQLScript(DObLandedCostCommonTestUtils.clearLandedCosts());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), "Activate Vendor Invoice")) {
			dObInvoiceActivateTestUtils.unfreeze();
			userActionsTestUtils.unlockAllSectionsNewStandard(
					IDObVendorInvoiceRestUrls.COMMAND_URL,
					IDObVendorInvoiceRestUrls.UNLOCK_ALL_SECTION,
					dObInvoiceActivateTestUtils.getSectionsNames());
			databaseConnector.closeConnection();
		}
	}
}
