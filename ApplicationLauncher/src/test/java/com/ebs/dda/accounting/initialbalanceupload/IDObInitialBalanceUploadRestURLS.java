package com.ebs.dda.accounting.initialbalanceupload;

public interface IDObInitialBalanceUploadRestURLS {

  String BASE_URL = "/services/initialbalanceupload";
  String VIEW_ALL_URL = BASE_URL;
  String INITIAL_BALANCE_UPLOAD_COMMAND = "/command/initialbalanceupload";
  String CREATE_INITIAL_BALANCE_UPLOAD = INITIAL_BALANCE_UPLOAD_COMMAND + "/";
  String AUTHORIZED_ACTIONS = BASE_URL + "/authorizedactions";
  String REQUEST_CREATE_INITIAL_BALANCE_UPLOAD = INITIAL_BALANCE_UPLOAD_COMMAND + "/";
  String ACTIVATE = "/activate";

}
