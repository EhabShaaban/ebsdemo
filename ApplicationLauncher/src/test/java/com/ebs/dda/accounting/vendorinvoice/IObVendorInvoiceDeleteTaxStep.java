package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceDeleteTaxTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class IObVendorInvoiceDeleteTaxStep extends SpringBootRunner implements En {

  private final String SCENARIO_NAME = "Delete Tax from VendorInvoice";
  private IObVendorInvoiceDeleteTaxTestUtils utils;

  public IObVendorInvoiceDeleteTaxStep() {
    When("^\"([^\"]*)\" requests to delete Tax with code \"([^\"]*)\" in VendorInvoice with code \"([^\"]*)\"$", (String userName, String taxCode, String vendorInvoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response taxDeleteResponse =
          utils.sendDeleteRequest(cookie, utils.getDeleteTaxUrl(vendorInvoiceCode, taxCode));
      userActionsTestUtils.setUserResponse(userName, taxDeleteResponse);
    });

    Then("^Tax with code \"([^\"]*)\" from VendorInvoice with code \"([^\"]*)\" is deleted$", (String taxCode, String vendorInvoiceCode) -> {
      utils.assertNoTaxExistsByCode(taxCode, vendorInvoiceCode);
    });

    Then("^InvoiceTaxes section of VendorInvoice with Code \"([^\"]*)\" is updated as follows:$", (String vendorInvoiceCode, DataTable invoiceTaxDataTable) -> {
      utils.assertThatVendorInvoiceTaxesValuesUpdated(vendorInvoiceCode, invoiceTaxDataTable);
    });

    Then("^InvoiceSummaries section of VendorInvoice with Code \"([^\"]*)\" is updated as follows:$", (String vendorInvoiceCode, DataTable invoiceSummariesDataTable) -> {
      utils.assertThatVendorInvoiceSummaryValuesUpdated(vendorInvoiceCode, invoiceSummariesDataTable);
    });

    Given("^\"([^\"]*)\" first deleted Tax with code \"([^\"]*)\" of VendorInvoice with code \"([^\"]*)\" successfully$", (String userName, String taxCode, String vendorInvoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response taxDeleteResponse =
          utils.sendDeleteRequest(cookie, utils.getDeleteTaxUrl(vendorInvoiceCode, taxCode));
      userActionsTestUtils.setUserResponse(userName, taxDeleteResponse);
    });

    Given("^\"([^\"]*)\" first deleted the VendorInvoice with code \"([^\"]*)\" successfully$", (String userName, String vendorInvoiceCode) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response invoiceResponse =
          utils.sendDeleteRequest(cookie, utils.getDeleteVendorInvoiceUrl(vendorInvoiceCode));
      userActionsTestUtils.setUserResponse(userName, invoiceResponse);
    });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new IObVendorInvoiceDeleteTaxTestUtils(properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      userActionsTestUtils.unlockAllSections(IDObVendorInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS,
          utils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
