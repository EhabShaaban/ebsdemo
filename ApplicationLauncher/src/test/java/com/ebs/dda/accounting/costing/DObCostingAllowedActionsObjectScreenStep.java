package com.ebs.dda.accounting.costing;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.costing.utils.DObCostingAllowedActionsObjectScreenTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

import static com.ebs.dda.accounting.costing.IDObCostingRestUrls.AUTHORIZED_ACTIONS;

public class DObCostingAllowedActionsObjectScreenStep extends SpringBootRunner implements En {

  private DObCostingAllowedActionsObjectScreenTestUtils utils;

  public DObCostingAllowedActionsObjectScreenStep() {
    When(
        "^\"([^\"]*)\" requests to read actions of Costing with code \"([^\"]*)\"$",
        (String userName, String CostingCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, AUTHORIZED_ACTIONS + CostingCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils =
        new DObCostingAllowedActionsObjectScreenTestUtils(
            properties, entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Read allowed actions in Costing Object Screen")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read allowed actions in Costing Object Screen")) {
      databaseConnector.closeConnection();
    }
  }
}
