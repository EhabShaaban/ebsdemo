package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.paymentrequest.IDObPaymentRequestRestURLs;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObPaymentRequestInvoiceReadAllTestUtils extends DObPaymentRequestCommonTestUtils {

  public DObPaymentRequestInvoiceReadAllTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/ServicePurchaseOrder.sql");
    return str.toString();
  }

  public String getDbScriptsEveryTime() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/vendorinvoice/clearSummerySection.sql");

    return str.toString();
  }

	public String getReadAllInvoicesUrl(String businessUnitCode, String vendorCode) {
		return IDObPaymentRequestRestURLs.PAYMENT_REQUEST_QUERY + "/" + businessUnitCode + "/"
						+ vendorCode + IDObPaymentRequestRestURLs.READ_ALL_INVOICES;
	}

  public void assertThatInvoicesDropDownResponseIsCorrect(
      Response response, DataTable vendorInvoicesDataTable) throws Exception {
    List<HashMap<String, Object>> actualInvoices = getListOfMapsFromResponse(response);
    List<Map<String, String>> expectedInvoicesMap =
        vendorInvoicesDataTable.asMaps(String.class, String.class);

    for (int i = 0; i < actualInvoices.size(); i++) {
      assertThatExpectedInvoicesMatcheActualOnes(expectedInvoicesMap.get(i), actualInvoices.get(i));
    }
  }

  private void assertThatExpectedInvoicesMatcheActualOnes(
      Map<String, String> expectedInvoiceMap, HashMap<String, Object> actualInvoiceMap)
      throws Exception {

    MapAssertion mapAssertion = new MapAssertion(expectedInvoiceMap, actualInvoiceMap);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObVendorInvoiceGeneralModel.INVOICE_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.COMPANY,
        IDObVendorInvoiceGeneralModel.COMPANY_CODE,
        IDObVendorInvoiceGeneralModel.COMPANY_NAME);
  }

  public void assertThatTotalNumberOfReturnedInvoicesIsCorrect(
      Response response, Integer expectedNumberOfInvoices) {
    List<HashMap<String, Object>> actualInvoices = getListOfMapsFromResponse(response);
    Integer actualNumberOfInvoices = actualInvoices.size();
    Assert.assertEquals(expectedNumberOfInvoices, actualNumberOfInvoices);
  }
}
