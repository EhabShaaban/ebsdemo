package com.ebs.dda.accounting.costing.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
//      "classpath:features/accounting/costing/Costing_View_Details.feature",
//      "classpath:features/accounting/costing/Costing_View_Items.feature",
      "classpath:features/accounting/costing/Costing_Activate_HP.feature",
//      "classpath:features/accounting/costing/Costing_Activate_Val.feature",
//      "classpath:features/accounting/costing/Costing_Activate_Auth.feature",
//      "classpath:features/accounting/costing/Costing_AllowedActions_OS.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.costing"},
    strict = true,
    tags = "not @Future")
public class DObCostingCucumberRunner {}
