package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls.COMMAND_URL;
import static com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls.REQUEST_DELETE_ITEM_URL;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class IObVendorInvoiceItemsDeleteTestUtils extends IObVendorInvoiceItemsTestUtils {

  public IObVendorInvoiceItemsDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/vendorinvoice/IObVendorInvoiceItemsViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceItems.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxes.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceTaxesViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyData.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/IObVendorInvoiceCompanyDataViewAll.sql");
    str.append("," + "db-scripts/accounting/vendorinvoice/clearSummerySection.sql");
    return str.toString();
  }

  public Response deleteItemById(Cookie userCookie, String invoiceCode, String invoiceItemId) {
    String deleteURL = COMMAND_URL + invoiceCode + REQUEST_DELETE_ITEM_URL + invoiceItemId;
    return sendDeleteRequest(userCookie, deleteURL);
  }

  public void assertItemExistsById(String invoiceCode, DataTable invoiceData) {
    List<Map<String, String>> invoiceItems = invoiceData.asMaps(String.class, String.class);
    for (Map<String, String> invoiceItem : invoiceItems) {
      String invoiceItemId = invoiceItem.get(IAccountingFeatureFileCommonKeys.ITEM_ID);
      IObVendorInvoiceItemsGeneralModel actualInvoiceItem =
          (IObVendorInvoiceItemsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getVendorInvoicesItemById", Long.valueOf(invoiceItemId));
      assertNotNull(actualInvoiceItem);
    }
  }

  public void assertItemIsDeletedById(String invoiceItemId) {
    IObVendorInvoiceItemsGeneralModel actualInvoiceItem =
        (IObVendorInvoiceItemsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getVendorInvoicesItemById", Long.valueOf(invoiceItemId));
    assertNull(actualInvoiceItem);
  }
}
