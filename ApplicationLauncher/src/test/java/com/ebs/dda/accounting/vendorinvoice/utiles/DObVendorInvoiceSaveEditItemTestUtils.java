package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceItemValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

public class DObVendorInvoiceSaveEditItemTestUtils extends IObVendorInvoiceItemsTestUtils {

	public DObVendorInvoiceSaveEditItemTestUtils(Map<String, Object> properties) {
		super(properties);
	}

	public String getDbScriptPath() {
		StringBuilder str = new StringBuilder();
		str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
		return str.toString();
	}

	public Response saveItemToInvoiceSection(
			Cookie userCookie, String invoiceCode, String itemId, DataTable itemData) {
		JsonObject invoiceItem = createInvoiceItemJsonObject(itemData);
		String restURL =
				IDObVendorInvoiceRestUrls.COMMAND_URL
						+ invoiceCode
						+ IDObVendorInvoiceRestUrls.SAVE_EDIT_ITEM_SECTION
						+ itemId;
		return sendPUTRequest(userCookie, restURL, invoiceItem);
	}

	private JsonObject createInvoiceItemJsonObject(DataTable itemDataTable) {
		List<Map<String, String>> itemsData = itemDataTable.asMaps(String.class, String.class);

		JsonObject invoiceItemObject = new JsonObject();
		for (Map<String, String> itemData : itemsData) {
			try {
				invoiceItemObject.addProperty(
						IDObVendorInvoiceItemValueObject.QTY,
						Double.parseDouble(itemData.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER)));
			} catch (NumberFormatException ex) {
				invoiceItemObject.addProperty(
						IDObVendorInvoiceItemValueObject.QTY,
						itemData.get(IAccountingFeatureFileCommonKeys.ITEM_QUANTITY_ORDER));
			}

			try {
				invoiceItemObject.addProperty(
						IDObVendorInvoiceItemValueObject.UNIT_PRICE,
						Double.parseDouble(itemData.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE)));
			} catch (NumberFormatException ex) {
				invoiceItemObject.addProperty(
						IDObVendorInvoiceItemValueObject.UNIT_PRICE,
						itemData.get(IAccountingFeatureFileCommonKeys.ITEM_UNIT_PRICE_BASE));
			}
		}
		return invoiceItemObject;
	}
}
