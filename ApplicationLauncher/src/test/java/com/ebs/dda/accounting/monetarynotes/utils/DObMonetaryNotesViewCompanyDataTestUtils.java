package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.monetarynotes.IDObNotesReceivableRestURLs;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

public class DObMonetaryNotesViewCompanyDataTestUtils extends DObMonetaryNotesCommonTestUtils {
  protected final String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Company Data]";

  public DObMonetaryNotesViewCompanyDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readCompanyData(Cookie cookie, String monetaryNoteCode) {
    return sendGETRequest(cookie, getViewCompanyDataUrl(monetaryNoteCode));
  }

  private String getViewCompanyDataUrl(String monetaryNoteCode) {
    return IDObNotesReceivableRestURLs.QUERY
        + "/"
        + monetaryNoteCode
        + IDObNotesReceivableRestURLs.VIEW_COMPANY_DATA;
  }

  public void assertMonetaryNoteCompanyDataIsCorrect(
      String monetaryNoteCode, Response response, DataTable companyDataTable) throws Exception {
    Map<String, String> expectedCompanyData =
        companyDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualCompanyData = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedCompanyData, actualCompanyData);

    mapAssertion.assertValue(
        IFeatureFileCommonKeys.CODE, IDObNotesReceivablesGeneralModel.USER_CODE);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IIObAccountingDocumentCompanyDataGeneralModel.PURCHASE_UNIT_CODE,
        IIObAccountingDocumentCompanyDataGeneralModel.PURCHASING_UNIT_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        IIObAccountingDocumentCompanyDataGeneralModel.COMPANY_CODE,
        IIObAccountingDocumentCompanyDataGeneralModel.COMPANY_NAME);
  }
}
