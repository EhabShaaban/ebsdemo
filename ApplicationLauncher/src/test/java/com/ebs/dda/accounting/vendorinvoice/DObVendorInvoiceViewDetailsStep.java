package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestCommonTestUtils;
import com.ebs.dda.accounting.paymentrequest.utils.DObPaymentRequestPaymentDetailsTestUtils;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceViewDetailsTestUtils;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceViewDetailsStep extends SpringBootRunner
    implements En, InitializingBean {

  private static boolean hasBeenExecuted = false;
  private DObVendorInvoiceViewDetailsTestUtils dObInvoiceViewDetailsTestUtils;
  private CObEnterpriseTestUtils enterpriseTestUtils;
  private DObPaymentRequestCommonTestUtils paymentRequestCommonTestUtils;

  public DObVendorInvoiceViewDetailsStep() {

    Given(
        "^the following details for Invoice exist:$",
        (DataTable invoicesDetailsDataTable) -> {
          dObInvoiceViewDetailsTestUtils.assertInvoicesExistWithDetails(invoicesDetailsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view InvoiceDetails section of Invoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              dObInvoiceViewDetailsTestUtils.requestToReadInvoiceDetails(cookie, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of InvoiceDetails section for Invoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String invoiceCode, String userName, DataTable invoicesDetailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          dObInvoiceViewDetailsTestUtils.assertThatInvoiceDetailsMatches(
              invoicesDetailsDataTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    dObInvoiceViewDetailsTestUtils = new DObVendorInvoiceViewDetailsTestUtils(properties);
    dObInvoiceViewDetailsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
    paymentRequestCommonTestUtils = new DObPaymentRequestPaymentDetailsTestUtils(properties);
    paymentRequestCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View InvoiceDetails")) {

      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            dObInvoiceViewDetailsTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(
          DObVendorInvoiceViewDetailsTestUtils.clearVendorInvoices());
      databaseConnector.executeSQLScript(DObPaymentRequestCommonTestUtils.clearPaymentRequests());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View InvoiceDetails")) {

      dObInvoiceViewDetailsTestUtils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
