package com.ebs.dda.accounting.salesinvoice;

public interface ISalesResponseKeys {
  String CODE = "userCode";
  String NAME = "name";
}
