package com.ebs.dda.accounting.landedcost;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostDetailsViewTestUtils;
import com.ebs.dda.accounting.landedcost.utils.DObLandedCostGeneralDataViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObLandedCostDetailsViewStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private DObLandedCostDetailsViewTestUtils landedCostDetailsUtils;

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        Map<String, Object> properties = getProperties();
        landedCostDetailsUtils = new DObLandedCostDetailsViewTestUtils(properties);
        landedCostDetailsUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }


    public DObLandedCostDetailsViewStep() {
        Given("^the following LandedCostDetails for LandedCost exist:$", (DataTable landedCostDetailsDataTable) -> {
            landedCostDetailsUtils.checkLandedCostDetailsExists(landedCostDetailsDataTable);
        });

        When("^\"([^\"]*)\" requests to view landedCostDetails section of LandedCost with code \"([^\"]*)\"$", (String userName, String lcCode) -> {
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            Response response = landedCostDetailsUtils.sendGETRequest(cookie, landedCostDetailsUtils.getViewLandedCostDetailsDataUrl(lcCode));
            userActionsTestUtils.setUserResponse(userName, response);
        });

        Then("^the following values of LandedCostSection for LandedCost with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$", (String lcCode, String userName, DataTable landedCostDetailsDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            landedCostDetailsUtils.assertThatViewLandedCostDetailsResponseIsCorrect(response, landedCostDetailsDataTable);
        });

    }

    @Before
    public void setup(Scenario scenario) throws Exception {

        if (contains(scenario.getName(), "View landedCostDetails section")) {
            databaseConnector.createConnection();
            if (!hasBeenExecuted) {
                databaseConnector.executeSQLScript(landedCostDetailsUtils.getDbScriptsOneTimeExecution());
                hasBeenExecuted = true;
            }
            databaseConnector.executeSQLScript(landedCostDetailsUtils.getDbScripts());
        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
        if (contains(scenario.getName(), "View landedCostDetails section")) {
            databaseConnector.closeConnection();
        }
    }
}
