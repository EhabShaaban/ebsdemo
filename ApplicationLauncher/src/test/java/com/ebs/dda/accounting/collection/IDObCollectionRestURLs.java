package com.ebs.dda.accounting.collection;

public interface IDObCollectionRestURLs {

  String COLLECTION_COMMAND = "/command/accounting/collection";
  String GET_URL = "/services/accounting/collection/";

  String CREATE_COLLECTION = COLLECTION_COMMAND + "/";
  String READ_ALL_SALES_INVOICES = GET_URL + "salesinvoices";
  String AUTHORIZED_ACTIONS = GET_URL + "authorizedactions/";
  String POST = "/activate";

  String VIEW_ALL_COLLECTION = "/services/accounting/collection";

  String READ_ALL_BANK_ACCOUNTS = "/services/enterprise/banks/banksAndAccounts";
  String TREASURIES_URL = "/treasuries";
  String DOCUMENT_OWNERS = GET_URL + "document-owners";
  String NOTES_RECEIVABLES = GET_URL + "notesreceivables";
  String VIEW_TYPES = GET_URL + "collectiontypes";
  String VIEW_SALES_ORDERS = GET_URL + "salesorders";
  String VIEW_DEBIT_NOTES = GET_URL + "debitNotes";
  String GENERAL_DATA = "/generaldata";
  String COMPANY_DATA = "/companydata";
  String COLLECTION_DETAILS = "/collectiondetails";
  String REQUEST_CREATE_URL = COLLECTION_COMMAND;
  String ACCOUNTING_DETAILS = "/accountingdetails";
  String ACTIVATION_DETAILS = "/postingdetails";
  String READ_ALL_PAYMENT_REQUESTS = GET_URL + "paymentrequests";

  String LOCK_COLLECTION_DETAILS_SECTION = "/lock/collectiondetails/";
  String UNLOCK_COLLECTION_DETAILS_SECTION = "/unlock/collectiondetails/";
  String UPDATE_COLLECTION_DETAILS_SECTION = "/update/collectiondetails/";
}
