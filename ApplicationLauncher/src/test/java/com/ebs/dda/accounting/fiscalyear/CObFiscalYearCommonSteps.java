package com.ebs.dda.accounting.fiscalyear;

import com.ebs.dda.IGeneralRestURLs;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.fiscalyear.utils.CObFiscalYearTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class CObFiscalYearCommonSteps extends SpringBootRunner implements En {
    private CObFiscalYearTestUtils utils;
    public CObFiscalYearCommonSteps(){

        When(
                "^\"([^\"]*)\" requests to read all FiscalYears for \"([^\"]*)\"$",
                (String userName, String objectSysName) -> {
                    String url = IGeneralRestURLs.GET_ALL_FISCAL_YEARS;
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = utils.sendGETRequest(cookie, url);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following FiscalYears values will be presented to \"([^\"]*)\":$",
                (String userName, DataTable fiscalYearDT) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatFiscalYearsAreRetrieved(response, fiscalYearDT);
                });

        Then(
                "^total number of FiscalYears returned to \"([^\"]*)\" in a dropdown is equal to (\\d+)$",
                (String userName, Integer totalNumber) -> {
                    utils.assertThatTotalNumberOfFiscalYearsIsCorrect(totalNumber);
                });
        When("^\"([^\"]*)\" requests to read Active FiscalYears$", (String userName) -> {
            String url = IGeneralRestURLs.GET_ALL_ACTIVE_FISCAL_YEARS;
            Cookie cookie = userActionsTestUtils.getUserCookie(userName);
            Response response = utils.sendGETRequest(cookie, url);
            userActionsTestUtils.setUserResponse(userName, response);
        });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils = new CObFiscalYearTestUtils(getProperties());
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "Read list of FiscalYears dropdown")) {
            databaseConnector.createConnection();
            databaseConnector.executeSQLScript(utils.clearFiscalYearInsertions());
        }
    }

    @After
    public void after() throws Exception {
        databaseConnector.closeConnection();
    }
}
