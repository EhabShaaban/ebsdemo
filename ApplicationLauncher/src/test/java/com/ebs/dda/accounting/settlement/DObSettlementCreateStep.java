package com.ebs.dda.accounting.settlement;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.settlement.utils.DObSettlementCreateTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObSettlementCreateStep extends SpringBootRunner implements En {

	public static final String SCENARIO_NAME = "Create Settlement";
	private DObSettlementCreateTestUtils utils;

	public DObSettlementCreateStep() {

		Given("^the following Settlement Types exist:$", (DataTable settlementTypesDataTable) -> {
			utils.assertThatSettlementTypesExist(settlementTypesDataTable);
		});

		Given("^CurrentDateTime = \"([^\"]*)\"$", (String currentDateTime) -> {
			utils.freeze(currentDateTime);
		});

		Given("^Last created Settlement was with code \"([^\"]*)\"$",
						(String lastSettlementCode) -> {
							utils.assertThatLastSettlementCode(lastSettlementCode);
						});

		When("^\"([^\"]*)\" creates Settlement with the following values:$",
						(String userName, DataTable settlementDataTable) -> {
							Response response = utils.createSettlement(settlementDataTable,
											userActionsTestUtils.getUserCookie(userName));
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^a new Settlement is created with the following values:$",
						(DataTable settlementGeneralDataTable) -> {
							utils.assertThatSettlementGeneralDataExist(settlementGeneralDataTable);
						});

		Then("^the Company Section of Settlement with code \"([^\"]*)\" is created as follows:$",
						(String settlementCode, DataTable settlementCompanyDataTable) -> {
							utils.assertThatSettlementCompanyDataExist(settlementCode,
											settlementCompanyDataTable);
						});

		Then("^the Details Section of Settlement with code \"([^\"]*)\" is created as follows:$",
				(String settlementCode, DataTable settlementDetailsTable) -> {
					utils.assertThatSettlementDetailsExist(settlementCode,
							settlementDetailsTable);
				});

		When("^\"([^\"]*)\" requests to Create Settlement$", (String userName) -> {
			Cookie cookie = userActionsTestUtils.getUserCookie(userName);
			Response response = utils.sendGETRequest(cookie, IDObSettlementRestURLs.REQUEST_CREATE_URL);
			userActionsTestUtils.setUserResponse(userName, response);
		});
	}

	@Before
	public void beforeViewAll(Scenario scenario) throws Exception {
		utils = new DObSettlementCreateTestUtils(getProperties());
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.createConnection();
			databaseConnector.executeSQLScript(DObSettlementCreateTestUtils.clearSettlement());
			databaseConnector.executeSQLScript(
							DObSettlementCreateTestUtils.getSettlementDbScriptsOneTimeExecution());
		}
	}

	@After
	public void after(Scenario scenario) throws Exception {
		if (contains(scenario.getName(), SCENARIO_NAME)) {
			databaseConnector.closeConnection();
		}
	}
}
