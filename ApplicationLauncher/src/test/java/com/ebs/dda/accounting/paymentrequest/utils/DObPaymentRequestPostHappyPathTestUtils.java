package com.ebs.dda.accounting.paymentrequest.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DObPaymentRequestPostHappyPathTestUtils extends DObPaymentRequestPostCommonTestUtils {

  public DObPaymentRequestPostHappyPathTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public void assertThatPaymentRequestIsUpdatedSuccessfully(
      String paymentRequestCode, DataTable paymentRequestDataTable) {
    Map<String, String> paymentRequestMap =
        paymentRequestDataTable.asMaps(String.class, String.class).get(0);

    DObPaymentRequest dObPaymentRequest =
        (DObPaymentRequest)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedPaymentRequest",
                constructPaymentRequestQueryParams(paymentRequestCode, paymentRequestMap));
    Assert.assertNotNull(
        "Payment Request is not updated successfully for " + paymentRequestCode, dObPaymentRequest);
  }

  public Object[] constructPaymentRequestQueryParams(
      String paymentRequestCode, Map<String, String> paymentRequestMap) {
    return new Object[] {
      paymentRequestCode,
      paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      paymentRequestMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
      "%" + paymentRequestMap.get(IFeatureFileCommonKeys.STATE) + "%"
    };
  }

  public void assertThatPaymentRequestActivationDetailsIsCreatedSuccessfully(
      String paymentRequestCode, DataTable activationDetailsDataTable) {
    Map<String, String> activationDetailsMap =
        activationDetailsDataTable.asMaps(String.class, String.class).get(0);

    IObAccountingDocumentActivationDetailsGeneralModel activationDetails =
        (IObAccountingDocumentActivationDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPaymentRequestActivationDetails",
                constructPaymentRequestActivationDetailsQueryParams(
                    paymentRequestCode, activationDetailsMap));
    Assert.assertNotNull(
        "Payment Request Activation Details is not created successfully " + paymentRequestCode,
        activationDetails);
  }

  public Object[] constructPaymentRequestActivationDetailsQueryParams(
      String paymentRequestCode, Map<String, String> postingDetailsMap) {
    return new Object[] {
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.JOURNAL_ENTRY_CODE),
      paymentRequestCode,
      postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATED_BY),
      postingDetailsMap.get(IFeatureFileCommonKeys.ACTIVATION_DATE),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.DUE_DATE),
      new BigDecimal(postingDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE)),
      postingDetailsMap.get(IAccountingFeatureFileCommonKeys.FISCAL_YEAR)
    };
  }

  public void assertThatTheFollowingPaymentRequestExist(DataTable paymentRequestDataTable) {
    List<Map<String, String>> paymentRequestListMap =
        paymentRequestDataTable.asMaps(String.class, String.class);
    for (Map<String, String> prPaymentDetailsMap : paymentRequestListMap) {
      IObPaymentRequestPaymentDetailsGeneralModel prPaymentDetails =
          (IObPaymentRequestPaymentDetailsGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPaymentRequestPaymentDetails",
                  constructPaymentRequestPaymentDetailsQueryParams(prPaymentDetailsMap));
      Assert.assertNotNull(
          "Payment Request Payment Details doesn't exist for "
              + prPaymentDetailsMap.get(IFeatureFileCommonKeys.CODE),
          prPaymentDetails);
    }
  }

  private Object[] constructPaymentRequestPaymentDetailsQueryParams(
      Map<String, String> prPaymentDetailsMap) {
    return new Object[] {
      prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT_TYPE),
      prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_DUE_DOCUMENT),
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.CODE),
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + prPaymentDetailsMap.get(IFeatureFileCommonKeys.STATE) + "%",
      prPaymentDetailsMap.get(IFeatureFileCommonKeys.TYPE),
      new BigDecimal(prPaymentDetailsMap.get(IAccountingFeatureFileCommonKeys.PR_NET_AMOUNT))
    };
  }

  public void assertThatResponseHasLatestExchangeRate(Response response, String exchangeRateCode) {
    HashMap<String, Object> responseExchangeRate = response.body().jsonPath().get("data");
    String responseExchangeRateCode = responseExchangeRate.get("userCode").toString();
    assertEquals(responseExchangeRateCode, exchangeRateCode);
  }
}
