package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.vendorinvoice.IDObVendorInvoiceRestUrls;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceDetailsValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObVendorInvoiceSaveInvoiceDetailsTestUtils extends DObVendorInvoiceDetailsTestUtils {
  public DObVendorInvoiceSaveInvoiceDetailsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response saveInvoiceDetailsSection(
          Cookie userCookie, String invoiceCode, DataTable invoiceData) {
    JsonObject invoiceDetailsSectionJsonObject =
            prepareInvoiceDetailsSectionJsonObject(invoiceData);
    String saveRestUrl =
            IDObVendorInvoiceRestUrls.COMMAND_URL
                    + invoiceCode
                    + IDObVendorInvoiceRestUrls.SAVE_INVOICE_DETAILS_SECTION;
    return sendPUTRequest(userCookie, saveRestUrl, invoiceDetailsSectionJsonObject);
  }

  private JsonObject prepareInvoiceDetailsSectionJsonObject(DataTable invoiceData) {
    Map<String, String> invoiceDetails = invoiceData.asMaps(String.class, String.class).get(0);
    invoiceDetails = convertEmptyStringsToNulls(invoiceDetails);

    JsonObject parsedData = new JsonObject();

    parsedData.addProperty(
            IDObVendorInvoiceDetailsValueObject.INVOICE_NUMBER,
            invoiceDetails.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER));

    parsedData.addProperty(
            IDObVendorInvoiceDetailsValueObject.INVOICE_DATE,
            invoiceDetails.get(IAccountingFeatureFileCommonKeys.INVOICE_DATE));

    parsedData.addProperty(
            IDObVendorInvoiceDetailsValueObject.PAYMENT_TERM_CODE,
            invoiceDetails.get(IAccountingFeatureFileCommonKeys.PAYMENT_TERMS));

    parsedData.addProperty(
            IDObVendorInvoiceDetailsValueObject.CURRENCY_CODE,
            invoiceDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY_CODE));

    if (invoiceDetails.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT) != null) {
      parsedData.add(IDObVendorInvoiceDetailsValueObject.DOWN_PAYMENT, getDownPayments(invoiceDetails));
    } else {
      parsedData.add(IDObVendorInvoiceDetailsValueObject.DOWN_PAYMENT, null);
    }

    return parsedData;
  }

  private JsonArray getDownPayments(Map<String, String> invoiceDetailsData) {
    List<String> downPayments =
            Arrays.asList(
                    invoiceDetailsData.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT).split(","));
    downPayments.replaceAll(String::trim);
    return createJsonArray(downPayments);
  }

  private JsonArray createJsonArray(List<String> downPayments) {
    JsonArray downPaymentArray = new JsonArray();
    for (String downPayment : downPayments) {
      downPaymentArray.add(downPayment);
    }
    return downPaymentArray;
  }

  public void checkUpdatedDataInInvoiceDetails(String invoiceCode, DataTable invoiceDataTable) {
    Map<String, String> invoiceDetailsMap =
            invoiceDataTable.asMaps(String.class, String.class).get(0);
    IObVendorInvoiceDetailsGeneralModel invoivceDetailsUpdatedData =
            (IObVendorInvoiceDetailsGeneralModel)
                    entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                            "getInvoiceDetailsUpdatedData",
                            getInvoiceDetailsParameters(invoiceCode, invoiceDetailsMap));
    assertNotNull("Invoice Details Section was not updated!", invoivceDetailsUpdatedData);
    assertThatDownPaymentsAreEquals(
            invoiceDetailsMap.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT),
            invoivceDetailsUpdatedData.getDownPayment());
  }

  private Object[] getInvoiceDetailsParameters(
          String invoiceCode, Map<String, String> invoiceDetailsMap) {
    return new Object[]{
            invoiceCode,
            invoiceDetailsMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            invoiceDetailsMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            invoiceDetailsMap.get(IAccountingFeatureFileCommonKeys.PAYMENT_TERMS),
            invoiceDetailsMap.get(IAccountingFeatureFileCommonKeys.CURRENCY_ISO),
            invoiceDetailsMap.get(IAccountingFeatureFileCommonKeys.INVOICE_NUMBER),
            invoiceDetailsMap.get(IAccountingFeatureFileCommonKeys.INVOICE_DATE)
    };
  }
}
