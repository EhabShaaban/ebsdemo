package com.ebs.dda.accounting.salesinvoice.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.ISalesFeatureFileCommonKeys;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotNull;

public class IObSalesInvoiceDeleteTaxTestUtils extends DObSalesInvoiceTestUtils {

  public IObSalesInvoiceDeleteTaxTestUtils(Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesOrderSalesInvoice_ViewAll.sql");
    str.append("," + "db-scripts/sales/DObSalesInvoice.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append("," + "db-scripts/sales/IObSISOItems.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append("," + "db-scripts/sales/IObSalesOrderSalesInvoiceBusinessPartner_viewAll.sql");
    str.append("," + "db-scripts/sales/IObSISOBusinessPartner.sql");
    str.append("," + "db-scripts/sales/IObSalesInvoiceSummary.sql");
    return str.toString();
  }

  public String getDeleteTaxUrl(String salesInvoiceCode, String taxCode) {
    String deleteTaxUrl =
        IDObSalesInvoiceRestUrls.COMMAND_URL
            + salesInvoiceCode
            + IDObSalesInvoiceRestUrls.DELETE_TAX
            + taxCode;
    return deleteTaxUrl;
  }

  public void assertNoTaxExistsByCode(String taxCode, String salesInvoiceCode) {
    IObSalesInvoiceTaxesGeneralModel actualInvoiceTax =
        (IObSalesInvoiceTaxesGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getSalesInvoiceTaxByCodeNotExist", salesInvoiceCode, taxCode);
    assertNull(actualInvoiceTax);
  }

  public void assertThatSalesInvoiceSummaryValuesUpdated(String salesInvoiceCode, DataTable salesInvoiceDataTable) {
    List<Map<String, String>> invoiceSummaryMaps =
        salesInvoiceDataTable.asMaps(String.class, String.class);

    for (Map<String, String> invoiceSummaryMap : invoiceSummaryMaps) {
      IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
          (IObInvoiceSummaryGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getSalesInvoiceItemsSummary",
                  constructSummaryQueryParams(salesInvoiceCode, invoiceSummaryMap));
      assertNotNull(invoiceSummaryGeneralModel);
    }
  }

  private Object[] constructSummaryQueryParams(String salesInvoiceCode, Map<String, String> invoiceSummaryMap) {
    return new Object[] {
        salesInvoiceCode,
        Double.valueOf(invoiceSummaryMap.get(ISalesFeatureFileCommonKeys.TOTAL_BEFORE_TAX)),
        Double.valueOf(invoiceSummaryMap.get(ISalesFeatureFileCommonKeys.TOTAL_AFTER_TAX)),
        Double.valueOf(invoiceSummaryMap.get(ISalesFeatureFileCommonKeys.REMAINING))
    };
  }
}
