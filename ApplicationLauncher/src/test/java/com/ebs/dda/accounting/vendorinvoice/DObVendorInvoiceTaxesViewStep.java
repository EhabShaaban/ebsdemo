package com.ebs.dda.accounting.vendorinvoice;

import java.sql.SQLException;
import java.util.Map;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceTaxesViewTestUtils;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObVendorInvoiceTaxesViewStep extends SpringBootRunner implements En {

	private IObVendorInvoiceTaxesViewTestUtils utils;

	public DObVendorInvoiceTaxesViewStep() {

		When("^\"([^\"]*)\" requests to view Taxes section of VendorInvoice with code \"([^\"]*)\"$",
						(String userName, String vendorInvoiceCode) -> {
							Cookie cookie = userActionsTestUtils.getUserCookie(userName);
							StringBuilder readUrl = new StringBuilder();
							readUrl.append(IDObVendorInvoiceRestUrls.GET_URL);
							readUrl.append(vendorInvoiceCode);
							readUrl.append(IDObVendorInvoiceRestUrls.TAXES);
							Response response = utils.sendGETRequest(cookie, readUrl.toString());
							userActionsTestUtils.setUserResponse(userName, response);
						});

		Then("^the following tax values for VendorInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
						(String vendorInvoiceCode, String userName,
										DataTable taxValuesDataTable) -> {
							Response response = userActionsTestUtils.getUserResponse(userName);
							utils.assertCorrectValuesAreDisplayedInTaxesSection(response,
											taxValuesDataTable);
						});
        And("^the following VendorInvoices Taxes exist:$", (DataTable vendorInvoiceDataTable) -> {
        	utils.insertVendorInvoiceTaxes(vendorInvoiceDataTable);
        });
    }

	@Before
	public void setup(Scenario scenario) throws Exception {
		Map<String, Object> properties = getProperties();
		utils = new IObVendorInvoiceTaxesViewTestUtils(properties);
		utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

		if (contains(scenario.getName(), "View VendorInvoiceTaxes")) {
			databaseConnector.createConnection();
			databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
		}
	}

	@After
	public void afterEachScenario(Scenario scenario) throws SQLException {
		if (contains(scenario.getName(), "View VendorInvoiceTaxes")) {
			databaseConnector.closeConnection();
		}
	}
}
