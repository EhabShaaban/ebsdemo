package com.ebs.dda.accounting.monetarynotes;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.monetarynotes.utils.IObNotesReceivableReferenceDocumentDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObNotesReceivableDeleteReferenceDocumentStep extends SpringBootRunner implements En {

  private IObNotesReceivableReferenceDocumentDeleteTestUtils utils;
    private static final String SCENARIO_NAME = "Delete ReferenceDocument from NotesReceivable,";

  public IObNotesReceivableDeleteReferenceDocumentStep() {
    When(
        "^\"([^\"]*)\" requests to delete ReferenceDocument with id \"([^\"]*)\" of type \"([^\"]*)\" in NotesReceivable with code \"([^\"]*)\"$",
        (String userName,
            Integer referenceDocumentId,
            String referenceDocumentType,
            String notesReceivableCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl =
              utils.getDeleteNotesReceivableReferenceDocumentRestUrl(
                  notesReceivableCode, referenceDocumentId, referenceDocumentType);
          Response response = utils.sendDeleteRequest(userCookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^ReferenceDocument with id \"([^\"]*)\" from NotesReceivable with code \"([^\"]*)\" is deleted$",
        (Integer referenceDocumentId, String notesReceivableCode) -> {
          utils.assertThatNotesReceivableReferenceDocumentIsDeletedSuccessfully(
              notesReceivableCode, referenceDocumentId);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObNotesReceivableReferenceDocumentDeleteTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void beforeSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterSetup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      userActionsTestUtils.unlockAllSections(
          IDObNotesReceivableRestURLs.UNLOCK_LOCKED_SECTIONS,
          utils.getSectionsNames());
    }
  }
}
