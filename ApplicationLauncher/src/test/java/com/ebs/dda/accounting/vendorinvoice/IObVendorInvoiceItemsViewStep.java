package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceItemsViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.sql.SQLException;
import java.util.Map;

public class IObVendorInvoiceItemsViewStep extends SpringBootRunner implements En, InitializingBean {

  private IObVendorInvoiceItemsViewTestUtils vendorInvoiceViewItemsTestUtils;

  IObVendorInvoiceItemsViewStep() {

    When(
        "^\"([^\"]*)\" requests to view InvoiceItems section of VendorInvoice with code \"([^\"]*)\"$",
        (String userName, String vendorInvoiceCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              vendorInvoiceViewItemsTestUtils.requestToReadVendorInvoiceItems(
                  cookie, vendorInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of InvoiceItems section for VendorInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String vendorInvoiceCode, String userName, DataTable VendorInvoicesItems) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          vendorInvoiceViewItemsTestUtils.assertThatInvoiceItemsMatch(
              VendorInvoicesItems, response);
        });
    Then(
        "^an empty InvoiceItems section is displayed to \"([^\"]*)\"$",
        (String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          vendorInvoiceViewItemsTestUtils.assertThatInvoiceItemsAreEmpty(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    vendorInvoiceViewItemsTestUtils = new IObVendorInvoiceItemsViewTestUtils(properties);
    vendorInvoiceViewItemsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View InvoiceItems")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          vendorInvoiceViewItemsTestUtils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View InvoiceItems")) {

      databaseConnector.closeConnection();
    }
  }
}
