package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.IDObSalesInvoiceRestUrls;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceDeleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceDeleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceDeleteTestUtils salesInvoiceDeleteTestUtils;

  public DObSalesInvoiceDeleteStep() {

      When(
              "^\"([^\"]*)\" requests to delete SalesInvoice with code \"([^\"]*)\"$",
              (String userName, String salesInvoiceCode) -> {
                  Response salesInvoiceDeleteResponse =
                          salesInvoiceDeleteTestUtils.deleteSalesInvoice(
                                  userActionsTestUtils.getUserCookie(userName), salesInvoiceCode);
                  userActionsTestUtils.setUserResponse(userName, salesInvoiceDeleteResponse);
              });

      Then(
              "^SalesInvoice with code \"([^\"]*)\" is deleted from the system$",
              (String salesInvoiceCode) -> {
                  salesInvoiceDeleteTestUtils.assertThatSalesInvoiceNotExist(salesInvoiceCode);
              });

      Given(
              "^\"([^\"]*)\" first deleted the SalesInvoice with code \"([^\"]*)\" successfully$",
              (String userName, String salesInvoiceCode) -> {
                  Response salesInvoiceDeleteResponse =
                          salesInvoiceDeleteTestUtils.deleteSalesInvoice(
                                  userActionsTestUtils.getUserCookie(userName), salesInvoiceCode);
                  userActionsTestUtils.setUserResponse(userName, salesInvoiceDeleteResponse);
              });
      Given(
              "^first \"([^\"]*)\" opens \"([^\"]*)\" section of SalesInvoice with code \"([^\"]*)\" in edit mode$",
              (String userName, String sectionName, String salesInvoiceCode) -> {
                  Map<String, String> sectionsLockUrlsMap =
                          salesInvoiceDeleteTestUtils.getSectionsLockUrlsMap();
                  String lockSectionUrl =
                          salesInvoiceDeleteTestUtils
                                  .getLockUrl(salesInvoiceCode, sectionsLockUrlsMap.get(sectionName))
                                  .toString();

                  Response lockRepponse =
                          userActionsTestUtils.lockSection(userName, lockSectionUrl, salesInvoiceCode);

                  salesInvoiceDeleteTestUtils.assertResponseSuccessStatus(lockRepponse);

                  userActionsTestUtils.setUserResponse(userName, lockRepponse);
              });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    salesInvoiceDeleteTestUtils = new DObSalesInvoiceDeleteTestUtils(properties);
    salesInvoiceDeleteTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "Delete SalesInvoice")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(
            salesInvoiceDeleteTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(salesInvoiceDeleteTestUtils.getDbScriptsExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
      salesInvoiceDeleteTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
              IDObSalesInvoiceRestUrls.UNLOCK_LOCKED_SECTIONS, salesInvoiceDeleteTestUtils.getSectionsNames());
      if (contains(scenario.getName(), "Delete SalesInvoice")) {
          databaseConnector.closeConnection();
      }
  }
}
