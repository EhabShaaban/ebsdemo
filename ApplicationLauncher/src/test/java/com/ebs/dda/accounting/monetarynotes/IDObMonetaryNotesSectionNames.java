package com.ebs.dda.accounting.monetarynotes;

public interface IDObMonetaryNotesSectionNames {

  String NOTES_DETAILS_SECTION = "NotesDetails";
  String REFERENCE_DOCUMENTS_SECTION = "ReferenceDocuments";
}
