package com.ebs.dda.accounting.vendorinvoice.utiles;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class DObVendorInvoiceCreateJournalEntryTestUtils extends DObVendorInvoiceActivateTestUtils {

  public DObVendorInvoiceCreateJournalEntryTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  @Override
  public String getDbScriptsOneTimeExecution() {
    return super.getDbScriptsOneTimeExecution()
        + ",db-scripts/accounting/vendorinvoice/DObVendorInvoiceServicePO_Create_JE.sql"
        + ",db-scripts/accounting/fiscal-year/CObFiscalYear.sql";
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/journal-entry-create.sql");
    str.append("," + "db-scripts/master-data/item/IObItemAccountingInfo.sql");
    return str.toString();
  }

  public void assertThatServicePOsExist(DataTable spoDT) {
    List<Map<String, String>> sPOsMap = spoDT.asMaps(String.class, String.class);

    for (Map<String, String> sPOMap : sPOsMap) {
      Object[] params = constructGettingSPOQueryParams(sPOMap);

      Object spo =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getServicePurchaseOrder", params);

      Assert.assertNotNull(
          "Service Purchase Order is not exist, given " + Arrays.toString(params), spo);
    }
  }

  private Object[] constructGettingSPOQueryParams(Map<String, String> sPOMap) {
    return new Object[] {
      sPOMap.get(IFeatureFileCommonKeys.CODE),
      sPOMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      "%" + sPOMap.get(IFeatureFileCommonKeys.STATE) + "%",
      "SERVICE_PO",
      sPOMap.get(IFeatureFileCommonKeys.VENDOR),
      sPOMap.get(IFeatureFileCommonKeys.REFERENCE_PO)
    };
  }

  public void assertThatSPOItemsExist(DataTable itemsDT) {
    List<Map<String, String>> itemsMap = itemsDT.asMaps(String.class, String.class);

    for (Map<String, String> itemMap : itemsMap) {
      Object[] params = constructGettingPOItemsQueryParams(itemMap);

      Object poItems =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("getPOItems", params);

      Assert.assertNotNull("PO Items are not exist, given " + Arrays.toString(params), poItems);
    }
  }

  private Object[] constructGettingPOItemsQueryParams(Map<String, String> itemMap) {
    return new Object[] {
      itemMap.get(IFeatureFileCommonKeys.CODE),
      itemMap.get(IFeatureFileCommonKeys.ITEM),
      itemMap.get(IOrderFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT),
    };
  }
}
