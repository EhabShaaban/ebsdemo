package com.ebs.dda.accounting.fiscalyear.utils;

import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.accounting.ICObFiscalYear;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CObFiscalYearTestUtils extends CommonTestUtils {

  public CObFiscalYearTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void createFiscalYear(DataTable fiscalYearTable) {
    List<Map<String, String>> fiscalYearMapList =
        fiscalYearTable.asMaps(String.class, String.class);
    for (Map<String, String> fiscalYearMap : fiscalYearMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createFiscalYear", getCreateFiscalYearParams(fiscalYearMap));
    }
  }

  private Object[] getCreateFiscalYearParams(Map<String, String> fiscalYearMap) {
    return new Object[] {
      fiscalYearMap.get(IFeatureFileCommonKeys.CODE),
      fiscalYearMap.get(IFeatureFileCommonKeys.STATE),
      fiscalYearMap.get(IFeatureFileCommonKeys.NAME),
      fiscalYearMap.get(IFeatureFileCommonKeys.NAME),
      fiscalYearMap.get(IFeatureFileCommonKeys.FROM),
      fiscalYearMap.get(IFeatureFileCommonKeys.TO)
    };
  }

  public void assertThatFiscalYearsAreRetrieved(
          Response userResponse, DataTable FiscalYearsDT) {
    List<Map<String, String>> fiscalYears =
            FiscalYearsDT.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualFiscalYears = getListOfMapsFromResponse(userResponse);
    int index = 0;
    for (Map<String, String> expectedFiscalYear : fiscalYears) {
      HashMap<String, Object> actualFiscalYear = actualFiscalYears.get(index++);
      MapAssertion mapAssertion = new MapAssertion(expectedFiscalYear, actualFiscalYear);
      mapAssertion.assertLocalizedValue(IFeatureFileCommonKeys.NAME, ICObFiscalYear.NAME);
      mapAssertion.assertValue(IFeatureFileCommonKeys.CODE, ICObFiscalYear.CODE);
    }
  }

  public void assertThatTotalNumberOfFiscalYearsIsCorrect(
          Integer expectedFiscalYearsCount) {
    Object actualFiscalYearsCount =
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getFiscalYearsTotalCount");

    Assert.assertEquals(Long.valueOf(expectedFiscalYearsCount), actualFiscalYearsCount);
  }

  public static String clearFiscalYearInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/fiscal-year/fiscal_year_Clear.sql");
    return str.toString();

  }
}
