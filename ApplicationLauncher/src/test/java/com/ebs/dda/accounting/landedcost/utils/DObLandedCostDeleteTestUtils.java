package com.ebs.dda.accounting.landedcost.utils;

import com.ebs.dda.accounting.landedcost.IDObLandedCostRestUrls;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostSectionNames;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.Map;
import org.junit.Assert;

public class DObLandedCostDeleteTestUtils extends DObLandedCostTestUtils {

  public DObLandedCostDeleteTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response deleteLandedCost(Cookie userCookie, String landedCostCode) {
    String deleteURL = IDObLandedCostRestUrls.COMMAND_URL + "/" + landedCostCode;
    return sendDeleteRequest(userCookie, deleteURL);
  }

  public void assertThatLandedCostNotExist(String landedCostCode) {
    DObLandedCostGeneralModel landedCost =
        (DObLandedCostGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getLandedCostByCode", landedCostCode);
    Assert.assertNull(landedCost);
  }

  public Map<String, String> getSectionsLockUrlsMap() {
    Map<String, String> sectionsLockUrls = new HashMap<>();
    sectionsLockUrls.put(
        IDObLandedCostSectionNames.LANDED_COST_SECTION,
        IDObLandedCostRestUrls.LOCK_LANDED_COST_DETAILS_SECTION);
    return sectionsLockUrls;
  }

  public StringBuilder getLockUrl(String landedCostCode, String sectionUrl) {
    StringBuilder lockUrl = new StringBuilder();
    lockUrl.append(IDObLandedCostRestUrls.COMMAND_URL);
    lockUrl.append("/");
    lockUrl.append(landedCostCode);
    lockUrl.append(sectionUrl);
    return lockUrl;
  }
}
