package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceSaveItemHappyPathTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceTaxesViewTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class IObSalesInvoiceSaveItemHappyPathStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private IObSalesInvoiceSaveItemHappyPathTestUtils utils;
  private IObSalesInvoiceTaxesViewTestUtils taxesViewTestUtils;

  public IObSalesInvoiceSaveItemHappyPathStep() {
    Given(
        "^the following SalesInvoices exist for save new Item:$",
        (DataTable salesInvoicesDataTable) -> {
          utils.assertSalesInvoiceExists(salesInvoicesDataTable);
        });

    Given(
        "^the following Items exist with the following details:$",
        (DataTable itemsDataTable) -> {
          utils.assertItemsExistByCodeTypeUomAndPurchaseUnits(itemsDataTable);
        });

    Given(
        "^InvoiceItems section of SalesInvoice with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String salesInvoiceCode, String userName, String lockTime) -> {
          utils.freeze(lockTime);
          String lockUrl = utils.getLockUrl(salesInvoiceCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, salesInvoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Given(
        "^SalesInvoice with code \"([^\"]*)\" has the following Items:$",
        (String salesInvoiceCode, DataTable itemsTable) -> {
          utils.assertThatSalesInvoiceHasItems(salesInvoiceCode, itemsTable);
        });
    When(
        "^\"([^\"]*)\" saves the following new item to InvoiceItems section of SalesInvoice with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String salesInvoiceCode,
            String saveTime,
            DataTable invoiceItemsDataTable) -> {
          utils.freeze(saveTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveItemToSalesInvoice(cookie, salesInvoiceCode, invoiceItemsDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^InvoiceItems section of SalesInvoice with code \"([^\"]*)\" is created as follows:$",
        (String salesInvoiceCode, DataTable salesInvoiceItemDataTable) -> {
          utils.assertThatSalesInvoiceHasItems(salesInvoiceCode, salesInvoiceItemDataTable);
        });

    Then(
        "^the lock by \"([^\"]*)\" on InvoiceItems section of SalesInvoice with Code \"([^\"]*)\" is released$",
        (String userName, String salesInvoiceCode) -> {
          utils.assertThatLockIsReleased(
              userName, salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);
        });
      And("^InvoiceTaxes section of SalesInvoice with Code \"([^\"]*)\" is updated as follows:$",	(String salesInvoiceCode, DataTable taxDetailsDataTable) -> {
          taxesViewTestUtils.assertThatTheFollowingTaxDetailsExistForSalesInvoice(
                  salesInvoiceCode, taxDetailsDataTable);
      });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new IObSalesInvoiceSaveItemHappyPathTestUtils(properties, entityManagerDatabaseConnector);
    taxesViewTestUtils = new IObSalesInvoiceTaxesViewTestUtils(getProperties());
    taxesViewTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    if (contains(scenario.getName(), "Save new SalesInvoice Item")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save new SalesInvoice Item")) {
      utils.unfreeze();
      databaseConnector.closeConnection();
    }
  }
}
