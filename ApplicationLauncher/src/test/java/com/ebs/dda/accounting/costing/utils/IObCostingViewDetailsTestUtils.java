package com.ebs.dda.accounting.costing.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.costing.IDObCostingRestUrls;
import com.ebs.dda.jpa.accounting.costing.IIObCostingDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.Map;

public class IObCostingViewDetailsTestUtils extends DObCostingCommonTestUtils {
  public IObCostingViewDetailsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String clearCostingDocumentInsertions() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/costing/costing_clear.sql");
    return str.toString();
  }

  public String getCostingDetailsViewDataUrl(String costingCode) {
    return IDObCostingRestUrls.QUERY_COSTING + "/" + costingCode + IDObCostingRestUrls.VIEW_DETAILS;
  }

  public void assertCorrectValuesAreDisplayed(Response response, DataTable detailsDataTable) {

    Map<String, String> expectedDetails =
        detailsDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualDetails = getMapFromResponse(response);
    MapAssertion mapAssertion = new MapAssertion(expectedDetails, actualDetails);
    assertGoodsReceiptCodeMatches(mapAssertion);
    assertStateMatches(mapAssertion);
    String currencyPriceEstimateTime =
        expectedDetails.get(IAccountingFeatureFileCommonKeys.CURRENCY_PRICE_ESTIMATE_TIME);
    if (currencyPriceEstimateTime.isEmpty()) {
      mapAssertion.assertBigDecimalNumberValueWithCompositeValue(
          IAccountingFeatureFileCommonKeys.CURRENCY_PRICE_ACTUAL_TIME,
          IIObCostingDetailsGeneralModel.CURRENCY_PRICE_ACTUAL_TIME,
          IIObCostingDetailsGeneralModel.LOCAL_CURRENCY_ISO);
      Assert.assertNull(
          actualDetails.get(IIObCostingDetailsGeneralModel.CURRENCY_PRICE_ESTIMATE_TIME));
    } else {
      mapAssertion.assertBigDecimalNumberValueWithCompositeValue(
          IAccountingFeatureFileCommonKeys.CURRENCY_PRICE_ESTIMATE_TIME,
          IIObCostingDetailsGeneralModel.CURRENCY_PRICE_ESTIMATE_TIME,
          IIObCostingDetailsGeneralModel.LOCAL_CURRENCY_ISO);
      Assert.assertNull(
          actualDetails.get(IIObCostingDetailsGeneralModel.CURRENCY_PRICE_ACTUAL_TIME));
    }
  }

  private void assertStateMatches(MapAssertion mapAssertion) {
    mapAssertion.assertValueContains(IFeatureFileCommonKeys.STATE, IIObCostingDetailsGeneralModel.CURRENT_STATES);
  }

  private void assertGoodsReceiptCodeMatches(MapAssertion mapAssertion) {
    mapAssertion.assertValue(
        IFeatureFileCommonKeys.GOODS_RECEIPT_CODE,
        IIObCostingDetailsGeneralModel.GOODS_RECEIPT_CODE);
  }
}
