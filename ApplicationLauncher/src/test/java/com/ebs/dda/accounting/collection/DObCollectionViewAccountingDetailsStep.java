package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionViewAccountingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionViewAccountingDetailsStep extends SpringBootRunner implements En {

  public static final String SCENARIO_NAME = "View AccountingDetails section in Collection";
  private DObCollectionViewAccountingDetailsTestUtils utils;

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObCollectionViewAccountingDetailsTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public DObCollectionViewAccountingDetailsStep() {
    Given("^the following AccountingDetails for Collections exist:$",
        (DataTable accountingDetailsDataTable) -> {
          utils.assertThatCollectionAccountingDetailsExists(accountingDetailsDataTable);
        });

    Given("^there is no AccountingDetails for the following Collections:$",
        (DataTable accountingDetailsDataTable) -> {
          utils.assertThatCollectionDoesNotHaveAccountingDetailsExists(accountingDetailsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view AccountingDetails section of Collection with code \"([^\"]*)\"$",
        (String userName, String collectionCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAccountingDetailsResponse(collectionCode, userCookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of AccountingDetails section for Collection with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String collectionCode, String userName, DataTable accountingDetailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertCorrectValuesAreDisplayed(response, accountingDetailsDataTable);
        });

    Then(
        "^an empty AccountingDetails section for Collection with code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String collectionCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatAccountingDetailsAreEmpty(response);
        });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {

    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.resetCollectionDataDependancies());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), SCENARIO_NAME)) {
      databaseConnector.closeConnection();
    }
  }
}
