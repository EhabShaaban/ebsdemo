package com.ebs.dda.accounting.collection;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.collection.utils.DObCollectionTypesDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObCollectionTypeDropdownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObCollectionTypesDropDownTestUtils utils;

  public DObCollectionTypeDropdownStep() {

    Given(
        "^the following CollectionTypes exist:$",
        (DataTable collectionTypesDataTable) -> {
          utils.assertThatCollectionTypesExist(collectionTypesDataTable);
        });

    Given(
        "^the total number of existing Collection Types are (\\d+)$",
        (Integer typesNumber) -> {
          utils.assertCollectionTypesCountMatchesExpected(typesNumber);
        });
    When(
        "^\"([^\"]*)\" requests to read all CollectionTypes",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, IDObCollectionRestURLs.VIEW_TYPES);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following Collection Types values will be presented to \"([^\"]*)\":$",
        (String userName, DataTable typesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadAllCollectionTypesResponseIsCorrect(response, typesDataTable);
        });

    Then(
        "^total number of Collection Types returned to \"([^\"]*)\" is equal to (\\d+)$",
        (String userName, Integer totalNumberOfTypes) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsEqualResponseSize(response, totalNumberOfTypes);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObCollectionTypesDropDownTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read list of CollectionTypes dropdown")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read list of CollectionTypes dropdown")) {
      databaseConnector.closeConnection();
    }
  }
}
