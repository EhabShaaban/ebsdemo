package com.ebs.dda.accounting.collection.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.accounting.collection.IDObCollectionRestURLs;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionCreateValueObject;
import com.ebs.dda.jpa.accounting.collection.IObCollectionCompanyDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObCollectionCreateTestUtils extends DObCollectionTestUtils {

  public DObCollectionCreateTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  private static JsonObject prepareCollectionJsonObject(DataTable collectionDataTable) {
    Map<String, String> collection = collectionDataTable.asMaps(String.class, String.class).get(0);
    JsonObject collectionJsonObject = new JsonObject();

    addValueToJsonObject(
        collectionJsonObject,
        IDObCollectionCreateValueObject.COLLECTION_TYPE,
        collection.get(IAccountingFeatureFileCommonKeys.COLLECTION_TYPE));

    addValueToJsonObject(
        collectionJsonObject,
        IDObCollectionCreateValueObject.COLLECTION_METHOD,
        collection.get(IAccountingFeatureFileCommonKeys.COLLECTION_METHOD));

    addValueToJsonObject(
        collectionJsonObject,
        IDObCollectionCreateValueObject.REF_DOCUMENT_CODE,
        collection.get(IAccountingFeatureFileCommonKeys.CR_REF_DOCUMENT));

    try {
      collectionJsonObject.addProperty(
          IDObCollectionCreateValueObject.DOCUMENT_OWNER_ID,
          Long.parseLong(collection.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID)));

    } catch (NumberFormatException e) {
      addValueToJsonObject(
          collectionJsonObject,
          IDObCollectionCreateValueObject.DOCUMENT_OWNER_ID,
          collection.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID));
    }
    return collectionJsonObject;
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append(",db-scripts/master-data/employee/MObEmployee.sql");
    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObCurrencySqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/order/salesorder/CObSalesOrderType.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/DObSalesOrder.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append(",").append("db-scripts/LookupSqlScript.sql");
    str.append(",").append("db-scripts/IObCompanyBasicDataScript.sql");
    str.append(",").append("db-scripts/master-data/customer/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/customer/MObCustomer.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerAddress.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerBusinessUnit.sql");
    str.append(",").append("db-scripts/master-data/customer/IObCustomerContactPerson.sql");
    str.append(",").append("db-scripts/CObPaymentTermsSqlScript.sql");
    str.append(",").append("db-scripts/sales/CObSalesInvoice.sql");
    str.append("," + "db-scripts/accounting/taxes.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice_ViewAll.sql");
    str.append(",").append("db-scripts/sales/DObSalesInvoice.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceSummary.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/sales/IObSalesInvoiceItems.sql");
    str.append(",").append("db-scripts/accounting/collection/CObCollectionType.sql");
    str.append(",").append("db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/chart-of-accounts/HObChartOfAccounts.sql");
    str.append(",").append("db-scripts/master-data/chart-of-accounts/GLAccountsLeafs.sql");
    str.append(",").append("db-scripts/LObGlobalGLAccountConfigScript.sql");
    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables_ViewAll.sql");
    str.append(",").append("db-scripts/notes-receivables/DObNotesReceivables.sql");
    str.append(",").append("db-scripts/accounting/collection/DObCollection_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/DObCollection.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionCompanyDetails.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionDetails_ViewAll.sql");
    str.append(",").append("db-scripts/accounting/collection/IObCollectionDetails.sql");

    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderData.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData_ViewAll.sql");
    str.append(",").append("db-scripts/order/salesorder/IObSalesOrderCompanyStoreData.sql");
    return str.toString();
  }

  public Response createCollection(DataTable collectionData, Cookie cookie) {
    JsonObject collection = prepareCollectionJsonObject(collectionData);
    String restURL = IDObCollectionRestURLs.CREATE_COLLECTION;
    return sendPostRequest(cookie, restURL, collection);
  }

  public void assertCollectionIsCreated(DataTable collectionDataTable) {
    List<Map<String, String>> collectionData =
        collectionDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionRow : collectionData) {
      DObCollectionGeneralModel collectionGeneralModel = retrieveCollectionData(collectionRow);
      assertNotNull(
          collectionGeneralModel,
          "CollectionData with code "
              + collectionRow.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist!");
      assertDateEquals(
          collectionRow.get(IFeatureFileCommonKeys.CREATION_DATE),
          collectionGeneralModel.getCreationDate());
    }
  }

  private DObCollectionGeneralModel retrieveCollectionData(Map<String, String> collectionRow) {
    return (DObCollectionGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCollectionByAllFields", constructCreatedCollectionQueryParams(collectionRow));
  }

  private Object[] constructCreatedCollectionQueryParams(Map<String, String> collectionRow) {
    return new Object[] {
      collectionRow.get(IFeatureFileCommonKeys.CODE),
      collectionRow.get(IFeatureFileCommonKeys.CREATED_BY),
      collectionRow.get(IAccountingFeatureFileCommonKeys.COLLECTION_TYPE),
      collectionRow.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      '%' + collectionRow.get(IFeatureFileCommonKeys.STATE) + '%'
    };
  }

  public void assertCollectionDetailsIsUpdated(
      String collectionCode, DataTable collectionDetailsDataTable) {
    List<Map<String, String>> collectionData =
        collectionDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> collectionDetailsRow : collectionData) {
      IObCollectionDetailsGeneralModel collectionDetails =
          retrieveCollectionDetailsData(collectionCode, collectionDetailsRow);
      assertNotNull(
          collectionDetails,
          "Details Data for Collection with code " + collectionCode + " doesn't exist!");

      Assert.assertNull(collectionDetails.getAmount());
    }
  }

  public void assertThatCollectionCompanyDetailsIsUpdated(
          String collectionCode, DataTable collectionCompanyDetailsDataTable) {
      List<Map<String, String>> collectionData =
              collectionCompanyDetailsDataTable.asMaps(String.class, String.class);
      for (Map<String, String> collectionDetailsRow : collectionData) {
          IObCollectionCompanyDetailsGeneralModel collectionCompanyDetails =
                  retrieveCollectionCompanyDetailsData(collectionCode, collectionDetailsRow);
          assertNotNull(
                  collectionCompanyDetails,
                  "Company Details Data for Collection with code " + collectionCode + " doesn't exist!");
      }
  }

  private IObCollectionDetailsGeneralModel retrieveCollectionDetailsData(
      String collectionCode, Map<String, String> collectionDetailsRow) {
    return (IObCollectionDetailsGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCreatedCollectionDetails",
            constructCreatedCollectionDetailsQueryParams(collectionCode, collectionDetailsRow));
  }

  private IObCollectionCompanyDetailsGeneralModel retrieveCollectionCompanyDetailsData(
      String collectionCode, Map<String, String> collectionCompanyDetailsRow) {
    return (IObCollectionCompanyDetailsGeneralModel)
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCollectionCompanyDetails",
            constructCreatedCollectionCompanyDetailsQueryParams(
                collectionCode, collectionCompanyDetailsRow));
  }

  private Object[] constructCreatedCollectionDetailsQueryParams(
      String collectionCode, Map<String, String> collectionDetailsRow) {
    String method = collectionDetailsRow.get(IAccountingFeatureFileCommonKeys.COLLECTION_METHOD);
    String currency = collectionDetailsRow.get(IAccountingFeatureFileCommonKeys.CURRENCY);
    String businessPartner =
        collectionDetailsRow.get(IAccountingFeatureFileCommonKeys.BUSINESS_PARTNER);
    String refDocument = collectionDetailsRow.get(IAccountingFeatureFileCommonKeys.REF_DOCUMENT);
    return new Object[] {collectionCode, method, currency, businessPartner, refDocument};
  }

  private Object[] constructCreatedCollectionCompanyDetailsQueryParams(
      String collectionCode, Map<String, String> collectionCompanyDetailsRow) {
    String company = collectionCompanyDetailsRow.get(IAccountingFeatureFileCommonKeys.COMPANY);
    String businessUnit = collectionCompanyDetailsRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT);
    return new Object[] {collectionCode, company, businessUnit};
  }

  public void assertNotesReceivablesWithCodeAndStateAndBusinessUnitExist(
      DataTable notesReceivablesDataTable) {
    List<Map<String, String>> notesReceivablesList =
        notesReceivablesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> notesReceivablesRow : notesReceivablesList) {
      DObNotesReceivablesGeneralModel notesReceivables =
          (DObNotesReceivablesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getDObNotesReceivablesByCodeAndStateAndBusinessUnit",
                  constructNotesReceivablesQueryParams(notesReceivablesRow));
      assertNotNull(
          notesReceivables,
          "NotesReceivable Data with code "
              + notesReceivablesRow.get(IFeatureFileCommonKeys.CODE)
              + " doesn't exist!");
    }
  }

  private Object[] constructNotesReceivablesQueryParams(Map<String, String> notesReceivablesRow) {
    return new Object[] {
      notesReceivablesRow.get(IFeatureFileCommonKeys.CODE),
      '%' + notesReceivablesRow.get(IFeatureFileCommonKeys.STATE) + '%',
      notesReceivablesRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertSalesOrdersWithCompanyAndCurrencyAndCustomerExist(
      DataTable salesOrderDataTable) {
    List<Map<String, String>> salesOrdersList =
        salesOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> salesOrderRow : salesOrdersList) {
      DObSalesOrderGeneralModel salesOrderGeneralModel =
          (DObSalesOrderGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getDObSalesOrderByCompanyAndCurrencyAndCustomer",
                  constructSalesOrderQueryParams(salesOrderRow));
      assertNotNull(
          salesOrderGeneralModel,
          "SalesOrderData with code "
              + salesOrderRow.get(IAccountingFeatureFileCommonKeys.SO_CODE)
              + " doesn't exist!");
    }
  }

  private Object[] constructSalesOrderQueryParams(Map<String, String> salesOrderRow) {
    return new Object[] {
      salesOrderRow.get(IAccountingFeatureFileCommonKeys.SO_CODE),
      salesOrderRow.get(IFeatureFileCommonKeys.COMPANY),
      salesOrderRow.get(IFeatureFileCommonKeys.CUSTOMER),
      salesOrderRow.get(IFeatureFileCommonKeys.CURRENCY)
    };
  }

}
