package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.utiles.DObVendorInvoiceRequestAddCancelItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObVendorInvoiceRequestAddCancelItemStep extends SpringBootRunner implements En {

  private DObVendorInvoiceRequestAddCancelItemTestUtils invoiceRequestAddCancelItemTestUtils;

  public DObVendorInvoiceRequestAddCancelItemStep() {
    Given(
        "^the following Vendor Invoices with details exist:$",
        (DataTable invoiceItemDataTable) -> {
          invoiceRequestAddCancelItemTestUtils.assertThatInvoicesExist(invoiceItemDataTable);
        });
    When(
        "^\"([^\"]*)\" requests to add Item to InvoiceItems section of Invoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
          String lockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new add Item dialoge is opened and InvoiceItems section of Invoice with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String invoiceCode, String userName) -> {
          invoiceRequestAddCancelItemTestUtils.assertThatResourceIsLockedByUser(
              userName, invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first requested to add Item to InvoiceItems section of Invoice with code \"([^\"]*)\" successfully$",
        (String userName, String invoiceCode) -> {
          String lockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, invoiceCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on InvoiceItems section of Invoice with code \"([^\"]*)\" is released$",
        (String userName, String invoiceCode) -> {
          invoiceRequestAddCancelItemTestUtils.assertThatLockIsReleased(
              userName, invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
        });

    Given(
        "^InvoiceItems section of Invoice with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String invoiceCode, String userName, String dateTime) -> {
          invoiceRequestAddCancelItemTestUtils.freeze(dateTime);
          String lockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.LOCK_ITEM_SECTION;
          Response response = userActionsTestUtils.lockSection(userName, lockURL, invoiceCode);
          invoiceRequestAddCancelItemTestUtils.assertResponseSuccessStatus(response);
        });

    When(
        "^\"([^\"]*)\" cancels saving InvoiceItems section of Invoice with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String invoiceCode, String dateTime) -> {
          invoiceRequestAddCancelItemTestUtils.freeze(dateTime);
          String unlockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.UNLOCK_ITEM_SECTION;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving InvoiceItems section of Invoice with code \"([^\"]*)\"$",
        (String userName, String invoiceCode) -> {
          String unlockURL =
              IDObVendorInvoiceRestUrls.COMMAND_URL
                  + invoiceCode
                  + IDObVendorInvoiceRestUrls.UNLOCK_ITEM_SECTION;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    invoiceRequestAddCancelItemTestUtils =
        new DObVendorInvoiceRequestAddCancelItemTestUtils(properties);
    invoiceRequestAddCancelItemTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to Add/Cancel Item to Invoice")) {

      databaseConnector.createConnection();
        databaseConnector.executeSQLScript(
            invoiceRequestAddCancelItemTestUtils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to Add/Cancel Item to Invoice")) {
      invoiceRequestAddCancelItemTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSectionsNewStandard(
          IDObVendorInvoiceRestUrls.COMMAND_URL,
          IDObVendorInvoiceRestUrls.UNLOCK_ALL_SECTION,
          invoiceRequestAddCancelItemTestUtils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }
}
