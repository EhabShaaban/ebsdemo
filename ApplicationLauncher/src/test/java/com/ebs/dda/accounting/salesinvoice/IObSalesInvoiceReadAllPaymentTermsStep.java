package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.IObSalesInvoiceBusinessPartnerTestUtils;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class IObSalesInvoiceReadAllPaymentTermsStep extends SpringBootRunner implements En {

    private static boolean hasBeenExecuted = false;
    private IObSalesInvoiceBusinessPartnerTestUtils iObSalesInvoiceBusinessPartnerTestUtils;

    public IObSalesInvoiceReadAllPaymentTermsStep() {

        When(
                "^\"([^\"]*)\" requests to read all PaymentTerms$",
                (String userName) -> {
                    Cookie cookie = userActionsTestUtils.getUserCookie(userName);
                    Response response =
                            iObSalesInvoiceBusinessPartnerTestUtils.sendGETRequest(
                                    cookie, IPurchaseOrderRestUrls.VIEW_PAYMENT_TERM);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following PaymentTerms values will be presented to \"([^\"]*)\":$",
                (String userName, DataTable paymenttermDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    iObSalesInvoiceBusinessPartnerTestUtils.assertThatResponsePaymentTermsDataIsCorrect(
                            response, paymenttermDataTable);
                });

        Then(
                "^total number of PaymentTerms returned to \"([^\"]*)\" is equal to (\\d+)$",
                (String userName, Integer totalRecordsNumber) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    iObSalesInvoiceBusinessPartnerTestUtils.assertTotalNumberOfRecordsEqualResponseSize(
                            response, totalRecordsNumber);
                });
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        Map<String, Object> properties = getProperties();
        iObSalesInvoiceBusinessPartnerTestUtils =
                new IObSalesInvoiceBusinessPartnerTestUtils(properties, entityManagerDatabaseConnector);
      if (contains(scenario.getName(), "Read list of PaymentTerms dropdown")) {
        databaseConnector.createConnection();

        databaseConnector.executeSQLScript(
                iObSalesInvoiceBusinessPartnerTestUtils.getDbScriptsOneTimeExecution());
        }
    }

    @After
    public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read list of PaymentTerms dropdown")) {
      databaseConnector.closeConnection();
        }
    }
}
