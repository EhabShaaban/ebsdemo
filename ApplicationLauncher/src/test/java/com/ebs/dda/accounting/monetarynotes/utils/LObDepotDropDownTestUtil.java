package com.ebs.dda.accounting.monetarynotes.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.jpa.masterdata.lookups.ILObDepot;
import com.ebs.dda.jpa.masterdata.lookups.LObDepot;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class LObDepotDropDownTestUtil extends DObMonetaryNotesCommonTestUtils {

  protected String ASSERTION_MSG = "[Depot]";

  public LObDepotDropDownTestUtil(Map<String, Object> properties, EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public String getClearLObDepotsDbScripts() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/accounting/monetarynote/Monetary_Notes_Clear.sql");
    str.append(",").append("db-scripts/accounting/monetarynote/Depot.sql");
    return str.toString();
  }

  public void assertThatLObDepotsAreExist(DataTable depotDataTable) {
    List<Map<String, String>> depotList = depotDataTable.asMaps(String.class, String.class);
    for (Map<String, String> depot : depotList) {
      LObDepot actualDepot =
          (LObDepot)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getDepotsByCodeAndName", depot.get(IAccountingFeatureFileCommonKeys.DEPOT));
      assertNotNull(ASSERTION_MSG + "Depot does not exist ", actualDepot);
    }
  }

  public void assertDepotsCountMatchesExpected(Integer depotsCount) {
    Long actualDepotsCount =
        (Long) entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult("getDepotsCount");
    assertEquals(
        ASSERTION_MSG + "Depot count is not correct", Long.valueOf(depotsCount), actualDepotsCount);
  }

  public void assertThatReadAllDepotsIsCorrect(Response response, DataTable depotDataTable)
      throws Exception {

    List<Map<String, String>> expectedDepots = depotDataTable.asMaps(String.class, String.class);

    List<HashMap<String, Object>> actualDepots = getListOfMapsFromResponse(response);

    for (int i = 0; i < expectedDepots.size(); i++) {

      MapAssertion mapAssertion = new MapAssertion(expectedDepots.get(i), actualDepots.get(i));

      mapAssertion.assertCodeAndLocalizedNameValue(
          IAccountingFeatureFileCommonKeys.DEPOT, ILObDepot.USER_CODE, ILObDepot.NAME);
    }
  }
  public void assertThatTotalNumberOfRecordsIsCorrect(
          Response response, Integer totalNumberOfRecords) {

    List<HashMap<String, Object>> depotList = getListOfMapsFromResponse(response);
    Assert.assertEquals(
            ASSERTION_MSG + " total Number Of Records is not correct",
            totalNumberOfRecords.intValue(),
            depotList.size());
  }
}
