package com.ebs.dda.accounting.salesinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceTypeDropDownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObSalesInvoiceTypeDropdownStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObSalesInvoiceTypeDropDownTestUtils utils;

  public DObSalesInvoiceTypeDropdownStep() {
    Given("^the total number of existing SalesInvoice Types are (\\d+)$", (Integer totalNumberOfTypes) -> {
      utils.assertThatSalesInvoiceTypesCountIsCorrect(totalNumberOfTypes);
    });

    When("^\"([^\"]*)\" requests to read all SalesInvoiceTypes$", (String userName) -> {
      Cookie cookie = userActionsTestUtils.getUserCookie(userName);
      Response response = utils.sendGETRequest(cookie, IDObSalesInvoiceRestUrls.VIEW_SI_TYPES);
      userActionsTestUtils.setUserResponse(userName, response);
    });

    Then("^the following SalesInvoice Types values will be presented to \"([^\"]*)\":$", (String userName, DataTable typesDataTable) -> {
      Response response = userActionsTestUtils.getUserResponse(userName);
      utils.assertResponseMatchesExpected(response, typesDataTable);
    });

    Then("^total number of SalesInvoices Types returned to \"([^\"]*)\" is equal to (\\d+)$", (String userName, Integer totalNumberOfTypes) -> {
      Response response = userActionsTestUtils.getUserResponse(userName);
      utils.assertTotalNumberOfRecordsEqualResponseSize(response, totalNumberOfTypes);
    });
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    Map<String, Object> properties = getProperties();
    utils = new DObSalesInvoiceTypeDropDownTestUtils(properties, entityManagerDatabaseConnector);

    if (contains(scenario.getName(), "Read list of SalesInvoiceTypes dropdown")) {
      databaseConnector.createConnection();
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read list of SalesInvoiceTypes dropdown")) {
      databaseConnector.closeConnection();
    }
  }
}
