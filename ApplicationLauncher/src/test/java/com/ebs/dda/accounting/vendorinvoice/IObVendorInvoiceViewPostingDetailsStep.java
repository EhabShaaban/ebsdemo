package com.ebs.dda.accounting.vendorinvoice;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.vendorinvoice.utiles.IObVendorInvoiceViewPostingDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

public class IObVendorInvoiceViewPostingDetailsStep extends SpringBootRunner implements En, InitializingBean {

    private static final String SCENARIO_NAME = "View PostingDetails section in VendorInvoice";
    private static boolean hasBeenExecutedOnce = false;
    private IObVendorInvoiceViewPostingDetailsTestUtils utils;

    public IObVendorInvoiceViewPostingDetailsStep() {
        Given("^the following PostingDetails for AccountingDocument with DocumentType \"([^\"]*)\" exist:$", (String documentType, DataTable postingDetailsDataTable) -> {
            utils.assertThatVendorInvoiceHasThoseActivationDetails(documentType, postingDetailsDataTable);
        });

        When("^\"([^\"]*)\" requests to view PostingDetails section of VendorInvoice with code \"([^\"]*)\"$",
                (String userName, String vendorInvoiceCode) -> {
            Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
            String readUrl = IDObVendorInvoiceRestUrls.GET_URL + vendorInvoiceCode
                    + IDObVendorInvoiceRestUrls.POSTING_DETAILS_SECTION;
            Response response = utils.sendGETRequest(userCookie, readUrl);
            userActionsTestUtils.setUserResponse(userName, response);
                });

        Then("^the following values of PostingDetails section for VendorInvoice with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
                (String vendorInvoiceCode, String userName, DataTable postingDetailsDataTable) -> {
            Response response = userActionsTestUtils.getUserResponse(userName);
            utils.assertCorrectValuesAreDisplayedInActivationDetails(response, postingDetailsDataTable);
                });
    }

    @Before
    public void before(Scenario scenario) throws Exception {
        Map<String, Object> properties = getProperties();
        utils = new IObVendorInvoiceViewPostingDetailsTestUtils(properties);
        utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.createConnection();
            if (!hasBeenExecutedOnce) {
                databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
                hasBeenExecutedOnce = true;
            }
            databaseConnector.executeSQLScript(utils.getDbScripts());
        }
    }

    @After
    public void after(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), SCENARIO_NAME)) {
            databaseConnector.closeConnection();
        }
    }
}
