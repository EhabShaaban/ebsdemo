package com.ebs.dda.accounting.settlement.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {

    "classpath:features/accounting/settlement/Settlement_AllowedActions_HS.feature",
    "classpath:features/accounting/settlement/Settlement_ViewAll.feature",
    "classpath:features/accounting/settlement/Settlement_Create_HP.feature",
    "classpath:features/accounting/settlement/Settlement_Create_Val.feature",
    "classpath:features/accounting/settlement/Settlement_Create_Auth.feature",
    "classpath:features/accounting/settlement/Settlement_DocumentOwner_Dropdown.feature",
    "classpath:features/accounting/settlement/Settlement_View_Details.feature",
    "classpath:features/accounting/settlement/Settlements_AllowedActions_OS.feature",
    "classpath:features/accounting/settlement/Settlement_View_GeneralData.feature",
    "classpath:features/accounting/settlement/Settlement_View_AccountingDetails.feature",
    "classpath:features/accounting/settlement/Settlement_RequestAdd_AccountingDetails.feature",
    "classpath:features/accounting/settlement/Settlement_Activate_HP.feature",
    "classpath:features/accounting/settlement/Settlement_Activate_Val.feature",
    "classpath:features/accounting/settlement/Settlement_Activate_Auth.feature",
    "classpath:features/accounting/settlement/Settlement_View_Company.feature",
    "classpath:features/accounting/settlement/Settlement_Save_AccountingDetails_HP.feature",
    "classpath:features/accounting/settlement/Settlement_Save_AccountingDetails_Auth.feature",
    "classpath:features/accounting/settlement/Settlement_Delete_AccountingDetails.feature",
    "classpath:features/accounting/settlement/Settlement_View_ActivationDetails.feature",
    "classpath:features/accounting/settlement/Settlement_View_Summary.feature"

}, glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.accounting.settlement"}, strict = true,
    tags = "not @Future")
public class DObSettlementCucumberRunner {
}
