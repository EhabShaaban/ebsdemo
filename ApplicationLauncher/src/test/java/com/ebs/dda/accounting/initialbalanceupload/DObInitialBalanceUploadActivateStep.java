package com.ebs.dda.accounting.initialbalanceupload;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.accounting.initialbalanceupload.utils.DObInitailBalanceUploadActivateTestUtils;
import com.ebs.dda.accounting.journalentry.utils.DObJournalEntryViewAllTestUtils;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceActivateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObInitialBalanceUploadActivateStep extends SpringBootRunner implements En {

  private DObInitailBalanceUploadActivateTestUtils utils;
  private DObJournalEntryViewAllTestUtils journalEntryViewAllTestUtils;
  private DObSalesInvoiceActivateTestUtils salesInvoicePostTestUtils;

  public DObInitialBalanceUploadActivateStep() {
    When(
        "^\"([^\"]*)\" activates InitialBalanceUpload with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String ibuCode, String date) -> {
          utils.freeze(date);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.activateInitailBalanceUplod(cookie, ibuCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^InitialBalanceUpload with code \"([^\"]*)\" is updated as follows:$",
        (String ibuCode, DataTable initialBalanceUploadDataTable) -> {
          utils.assertInitialBalanceUploadDetailsIsUpdatedAfterPost(
              ibuCode, initialBalanceUploadDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObInitailBalanceUploadActivateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    journalEntryViewAllTestUtils =
        new DObJournalEntryViewAllTestUtils(getProperties(), entityManagerDatabaseConnector);
    salesInvoicePostTestUtils = new DObSalesInvoiceActivateTestUtils(getProperties());
    salesInvoicePostTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Activate InitialBalanceUpload -")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.clearIBUInsertions());
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Activate InitialBalanceUpload -")) {
      databaseConnector.closeConnection();
    }
  }
}
