package com.ebs.dda.accounting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.salesinvoice.utils.DObSalesInvoiceTestUtils;
import com.ebs.dda.jpa.accounting.IIObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import cucumber.api.DataTable;
import io.restassured.response.Response;

public class IObInvoiceSummaryTestUtils extends DObSalesInvoiceTestUtils {

  public IObInvoiceSummaryTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatTheFollowingInvoiceSummaryDetailsExist(String invoiceCode,
      String invoiceType, DataTable invoiceSummaryDataTable) {
    List<Map<String, String>> summaryDetailsMaps =
        invoiceSummaryDataTable.asMaps(String.class, String.class);
    for (Map<String, String> summaryDetailsMap : summaryDetailsMaps) {

      Object[] queryParams =
          constructInvoiceSummaryQueryParams(invoiceCode, invoiceType, summaryDetailsMap);

      IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
          (IObInvoiceSummaryGeneralModel) entityManagerDatabaseConnector
              .executeNativeNamedQuerySingleResult("getSummaryDetailsInInvoice", queryParams);
      Assert.assertNotNull("Summary detail doesn't exist " + summaryDetailsMap.toString(),
          invoiceSummaryGeneralModel);
    }
  }

  public Object[] constructInvoiceSummaryQueryParams(String invoiceCode, String invoiceType,
      Map<String, String> summaryValues) {
    return new Object[] {invoiceCode, invoiceType,
        Double
            .valueOf(summaryValues.get(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES)),
        Double.valueOf(summaryValues.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT)),
        Double.valueOf(summaryValues.get(IAccountingFeatureFileCommonKeys.NET_AMOUNT)),
        Double.valueOf(summaryValues.get(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT)),
        Double.valueOf(summaryValues.get(IAccountingFeatureFileCommonKeys.TOTAL_REMAINING))};
  }

  public void assertThatSummaryDisplayedToUser(Response response, String salesInvoiceCode,
      DataTable salesInvoiceSummary) {
    Map<String, String> expectedSalesInvoiceSummary =
        salesInvoiceSummary.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualSalesInvoiceSummary = getMapFromResponse(response);

    MapAssertion mapAssertion =
        new MapAssertion(expectedSalesInvoiceSummary, actualSalesInvoiceSummary);

    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES,
        IIObInvoiceSummaryGeneralModel.AMOUNT_BEFORE_TAXES);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TAX_AMOUNT,
        IIObInvoiceSummaryGeneralModel.TAX_AMOUNT);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.NET_AMOUNT,
        IIObInvoiceSummaryGeneralModel.NET_AMOUNT);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.DOWN_PAYMENT,
        IIObInvoiceSummaryGeneralModel.DOWN_PAYMENT);
    mapAssertion.assertBigDecimalNumberValue(IAccountingFeatureFileCommonKeys.TOTAL_REMAINING,
        IIObInvoiceSummaryGeneralModel.REMAINING_AMOUNT);
  }
}
