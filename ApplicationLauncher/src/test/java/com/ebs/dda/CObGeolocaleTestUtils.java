package com.ebs.dda;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

public class CObGeolocaleTestUtils {

  private EntityManagerDatabaseConnector entityManagerDatabaseConnector;

  public CObGeolocaleTestUtils(EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    this.entityManagerDatabaseConnector = entityManagerDatabaseConnector;
  }

  public void assrtThatCountriesExistByCodeAndName(DataTable countriesDataTable) {
    List<Map<String, String>> countriesList = countriesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> country : countriesList) {
      Object actualCountry =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCountryByCodeAndName",
              country.get(IFeatureFileCommonKeys.CODE),
              country.get(IFeatureFileCommonKeys.NAME));
      assertNotNull(actualCountry);
    }
  }

  public void assrtThatCitiesExistByCodeAndName(DataTable citiesDataTable) {
    List<Map<String, String>> citiesList = citiesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> city : citiesList) {
      Object actualCity =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCityByCodeAndName",
              city.get(IFeatureFileCommonKeys.CODE),
              city.get(IFeatureFileCommonKeys.NAME));
      assertNotNull(actualCity);
    }
  }
}
