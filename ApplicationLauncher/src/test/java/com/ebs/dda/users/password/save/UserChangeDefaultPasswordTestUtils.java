package com.ebs.dda.users.password.save;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.ebs.dac.security.jpa.entities.User;
import com.ebs.dac.security.jpa.valueobjects.IUserPasswordValueObject;
import com.ebs.dac.security.usermanage.utils.BDKUserUtils;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.users.IUserFeatureFileCommonKeys;
import com.ebs.dda.users.IUserRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.springframework.core.env.Environment;
import rest.AuthenticationController;

public class UserChangeDefaultPasswordTestUtils extends CommonTestUtils {

  public static final String SHIRO_SUCCESS_URL = "shiro.successUrl";

  public UserChangeDefaultPasswordTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/user/users.sql");
    return str.toString();
  }

  public void assertThatUsersWithPasswordsExit(DataTable usersAndPasswordsDataTable) {
    List<Map<String, String>> usersAndPasswords =
        usersAndPasswordsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> userAndPassword : usersAndPasswords) {

      User user =
          (User)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getUserByNameAndPassword",
                  constructUsersAndPasswordQueryParams(userAndPassword));
      assertNotNull(user);
      assertTrue(
          BDKUserUtils.checkPassword(
              userAndPassword.get(IFeatureFileCommonKeys.PASSWORD), user.getPassword()));
    }
  }

  private Object[] constructUsersAndPasswordQueryParams(Map<String, String> userAndPassword) {
    return new Object[]{
        userAndPassword.get(IUserFeatureFileCommonKeys.USER_NAME),
        Boolean.parseBoolean(userAndPassword.get(IUserFeatureFileCommonKeys.IS_DEFAULT))
    };
  }

  public Response saveNewPassword(Cookie userCookie, DataTable userDataTable) {
    JsonObject userDataJsonObject = prepareUserDataJsonObject(userDataTable);
    String saveRestUrl = IUserRestUrls.UPDATE_PASSWORD;
    return sendPostRequest(userCookie, saveRestUrl, userDataJsonObject);
  }

  private JsonObject prepareUserDataJsonObject(DataTable userDataTable) {
    Map<String, String> userData = userDataTable.asMaps(String.class, String.class).get(0);

    JsonObject parsedData = new JsonObject();

    parsedData.addProperty(
        IUserPasswordValueObject.CURRENT_PASSWORD,
        userData.get(IUserFeatureFileCommonKeys.CURRENT_PASSWORD));

    parsedData.addProperty(
        IUserPasswordValueObject.NEW_PASSWORD,
        userData.get(IUserFeatureFileCommonKeys.NEW_PASSWORD));
    parsedData.addProperty(
        IUserPasswordValueObject.CONFIRMED_NEW_PASSWORD,
        userData.get(IUserFeatureFileCommonKeys.CONFIRMED_NEW_PASSWORD));

    return parsedData;
  }

  public void assertThatUserPasswordIsUpdated(String userName, DataTable passwordsDataTable) {
    List<Map<String, String>> passwords = passwordsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> password : passwords) {

      User user =
          (User)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getUserByNameAndPassword",
                  constructUpdatedUserAndPasswordQueryParams(userName, password));
      assertNotNull(user);
      assertTrue(
          BDKUserUtils.checkPassword(
              password.get(IUserFeatureFileCommonKeys.NEW_PASSWORD), user.getPassword()));
    }
  }

  private Object[] constructUpdatedUserAndPasswordQueryParams(
      String userName, Map<String, String> userAndPassword) {
    return new Object[]{
        userName, Boolean.parseBoolean(userAndPassword.get(IUserFeatureFileCommonKeys.IS_DEFAULT))
    };
  }

  public void assertThatChangeDefaultPasswordLocationHeaderExists(
      Environment environment, Response response) {
    String location = response.getHeader("Location");
    Assertions.assertNotNull(location);
    assertEquals(
        environment.getProperty(SHIRO_SUCCESS_URL)
            + AuthenticationController.CHANGE_DEFAULT_PASSWORD_PAGE,
        location);
  }

  public void assertThatLogOutLocationHeaderExists(Response response) {
    String location = response.getHeader("Location");
    Assertions.assertNotNull(location);
    assertEquals("logout", location);
  }
}
