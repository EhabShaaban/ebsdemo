package com.ebs.dda.users.password.save.happypath;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
        "classpath:features/users/Users_Default_Password_Change_HP.feature",
        "classpath:features/users/Users_Default_Password_Change_Val.feature"
    },
    glue = {
        "com.ebs.dda.generaldefinitions",
        "com.ebs.dda.users.password.save.happypath",
    },
    strict = true,
    tags = "not @Future")
public class UserChangeDefaultPasswordCucumberRunner {

}
