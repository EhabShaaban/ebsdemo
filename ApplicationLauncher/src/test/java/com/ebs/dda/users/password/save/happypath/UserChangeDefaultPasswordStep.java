package com.ebs.dda.users.password.save.happypath;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.users.password.save.UserChangeDefaultPasswordTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

public class UserChangeDefaultPasswordStep extends SpringBootRunner implements En {

  private UserChangeDefaultPasswordTestUtils userChangeDefaultPasswordTestUtils;

  @Autowired
  private Environment environment;

  public UserChangeDefaultPasswordStep() {

    Given("^the following users and passwords exist:$", (DataTable usersAndPasswordsDataTable) -> {
      userChangeDefaultPasswordTestUtils
          .assertThatUsersWithPasswordsExit(usersAndPasswordsDataTable);

    });

    Given("^user is logged in as \"([^\"]*)\" with limited session$", (String userName) -> {
      Response response = userActionsTestUtils.login(userName);
      userActionsTestUtils.setUserResponse(userName, response);

    });

    Given("^\"([^\"]*)\" is forwarded to the change password page$", (String userName) -> {
      userChangeDefaultPasswordTestUtils
          .assertThatChangeDefaultPasswordLocationHeaderExists(environment,
              userActionsTestUtils.getUserResponse(userName));
    });
    When("^\"([^\"]*)\" request to change the default password with the following values:$",
        (String userName,
            DataTable userDataTable) -> {
          Cookie userCookie = userActionsTestUtils
              .getUserCookie(userName);
          Response response = userChangeDefaultPasswordTestUtils
              .saveNewPassword(userCookie,
                  userDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then("^\"([^\"]*)\" is forwarded to the login page$", (String userName) -> {
      userChangeDefaultPasswordTestUtils.assertThatLogOutLocationHeaderExists(
          userActionsTestUtils.getUserResponse(userName));
    });
    Then("^\"([^\"]*)\" password is changed with the following values:$",
        (String userName, DataTable passwordsDataTable) -> {
          userChangeDefaultPasswordTestUtils
              .assertThatUserPasswordIsUpdated(userName, passwordsDataTable);
        });

  }

  @Before
  public void setup() throws Exception {

    Map<String, Object> properties = getProperties();
    userChangeDefaultPasswordTestUtils =
        new UserChangeDefaultPasswordTestUtils(properties, entityManagerDatabaseConnector);
    databaseConnector.createConnection();

    databaseConnector.executeSQLScript(userChangeDefaultPasswordTestUtils.getDbScriptPath());
  }

  @After
  public void afterEachScenario() throws SQLException {
    databaseConnector.closeConnection();
  }
}
