package com.ebs.dda.users;

public interface IUserFeatureFileCommonKeys {

  String USER_NAME = "UserName";
  String IS_DEFAULT = "IsDefault";
  String CURRENT_PASSWORD = "CurrentPassword";
  String NEW_PASSWORD = "NewPassword";
  String CONFIRMED_NEW_PASSWORD = "ConfirmedNewPassword";


}
