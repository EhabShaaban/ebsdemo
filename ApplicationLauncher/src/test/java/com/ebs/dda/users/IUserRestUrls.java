package com.ebs.dda.users;

public interface IUserRestUrls {

  String COMMAND_USER = "/command/user/";
  String UPDATE_PASSWORD = COMMAND_USER + "update/password";

}
