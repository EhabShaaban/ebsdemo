package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewItemsTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewItemsWithoutPricesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderViewItemsWithoutPricesStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderViewItemsWithoutPricesTestUtils utils;

  DObPurchaseOrderViewItemsWithoutPricesStep() {

    Given(
        "^the following PurchaseOrders has an empty OrderItems section:$",
        (DataTable purchaseOrdersDataTable) -> {
          utils.assertEmptyOrderItemsInPurchaseOrders(purchaseOrdersDataTable);
        });

    Given(
        "^PurchaseOrder with code \"([^\"]*)\" has the following OrderItems:$",
        (String purchaseOrderCode, DataTable orderItemsDataTable) -> {
          utils.assertThatItemsExistInPurchaseOrder(purchaseOrderCode, orderItemsDataTable);
        });

    Given(
        "^Item with code \"([^\"]*)\" PurchaseOrder with code \"([^\"]*)\" has empty Quantities$",
        (String itemCode, String purchaseOrderCode) -> {
          utils.assertEmptyQuantitiesInItemInPurchaseOrder(itemCode, purchaseOrderCode);
        });

    When(
        "^\"([^\"]*)\" requests to view OrderItemsWithoutPrices section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String restURL = utils.VIEW_ITEMS_WITHOUT_PRICES_URL + purchaseOrderCode;
          Response response = utils.sendGETRequest(cookie, restURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following OrderItemsWithoutPrices are displayed to \"([^\"]*)\":$",
        (String username, DataTable dataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatActualPurchaseOrderItemsAreCorrect(response, dataTable);
        });

    Then(
        "^the following Quantities list for Item \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" is displayed to \"([^\"]*)\":$",
        (String itemCode, String orderCode, String username, DataTable itemQuantitiesTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatOrderItemQuantitiesAreDisplayedCorrect(
              itemCode, itemQuantitiesTable, response);
        });

    Then(
        "^an empty Quantities list for Item \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String itemCode, String orderCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertEmptyResponseOfQuantities(response, itemCode);
        });

    Then(
        "^an empty OrderItemsWithoutPrices section in PurchaseOrder with code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String ordercODE, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertEmptyResponseOfItems(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderViewItemsWithoutPricesTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View OrderItems WithoutPrices,")) {
      DObPurchaseOrderViewItemsTestUtils.setItemQuantities("quantitiesWithoutPrices");
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View OrderItems WithoutPrices,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
