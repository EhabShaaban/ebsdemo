package com.ebs.dda.purchases.utils;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertArrayEquals;

import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.IResponseKeys;
import com.ebs.dda.purchases.ITableKeys;
import cucumber.api.DataTable;
import io.restassured.builder.MultiPartSpecBuilder;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class PurchaseOrderSaveAttachmentTestUtils extends PurchaseOrderAttachmentTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Save]";

  public PurchaseOrderSaveAttachmentTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public Response saveGoodsReceiptAttachmentData(
      DataTable attachmentData, String purchaseOrderCode, Cookie loginCookie) throws Exception {
    Map<String, String> attachmentMap = attachmentData.asMaps(String.class, String.class).get(0);
    String documentTypeCode = attachmentMap.get(IExpectedKeys.DOCUMENT_TYPE_CODE);
    String attachment = attachmentMap.get(IExpectedKeys.ATTACHMENT);
    return given()
        .cookie(loginCookie)
        .multiPart(
            new MultiPartSpecBuilder(convertFileToByteArray(attachment))
                .fileName(attachment)
                .controlName("file")
                .mimeType(getFileMimeType(attachment))
                .build())
        .param(IResponseKeys.DOCUMENT_TYPE_CODE, documentTypeCode)
        .post( urlPrefix +IPurchaseOrderRestUrls.SAVE_ATTACHMENTS + purchaseOrderCode);
  }

  public void assertThatPurchaseOrderAttachmentIsUpdatedWithTheseValues(
      String purchaseOrderCode, DataTable attachmentDataTable) throws Exception {
    List<Map<String, String>> attachmentsMap =
        attachmentDataTable.asMaps(String.class, String.class);
    for (Map<String, String> attachmentMap : attachmentsMap) {
      assertThatAttachmentExistInDataBase(attachmentMap, purchaseOrderCode);
    }
  }

  private byte[] convertFileToByteArray(String fileName) throws Exception {
    InputStream resourceAsStream = this.getClass().getResourceAsStream("/files/" + fileName);
    byte[] encbytes = new byte[resourceAsStream.available()];
    resourceAsStream.read(encbytes);
    return encbytes;
  }

  private String getFileMimeType(String fileName) throws Exception {
    Path path = new File("/files/" + fileName).toPath();
    return Files.probeContentType(path);
  }

  private void assertThatAttachmentExistInDataBase(
      Map<String, String> attachmentMap, String purchaseOrderCode) throws Exception {
    PreparedStatement preparedStatement =
        createReadAttachmentPreparedStatement(attachmentMap, purchaseOrderCode);

    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    assertThatFileUploadedSuccessfully(resultSet, attachmentMap.get(IExpectedKeys.ATTACHMENT));
  }

  private void assertThatFileUploadedSuccessfully(ResultSet resultSet, String attachment)
      throws Exception {
    resultSet.next();
    byte[] attachmentBytes = (byte[]) resultSet.getObject(ITableKeys.ATTACHMENT);
    InputStream resourceAsStream = this.getClass().getResourceAsStream("/files/" + attachment);
    byte[] encbytes = new byte[resourceAsStream.available()];
    resourceAsStream.read(encbytes);
    assertArrayEquals(ASSERTION_MSG + " File is not Uploaded Successfully" ,attachmentBytes, encbytes);
  }

  private PreparedStatement createReadAttachmentPreparedStatement(
      Map<String, String> attachmentMap, String purchaseOrderCode) throws Exception {

    String query =
        "SELECT  robattachment.content::bytea attachment,  robattachment.name FROM doborderdocument JOIN ioborderattachment ON\n"
            + "    doborderdocument.id = ioborderattachment.refinstanceid AND doborderdocument.objectTypeCode LIKE '%_PO' \n"
            + "    JOIN robattachment  on ioborderattachment.robattachmentid = robattachment.id\n"
            + "WHERE doborderdocument.code = ? AND ioborderattachment.lobattachmentid = (SELECT id From lobAttachmenttype WHERE code = ? ) AND robattachment.name = ? order by robattachment.id desc";
    return constructPreparedStatement(
        query,
        purchaseOrderCode,
        attachmentMap.get(IExpectedKeys.DOCUMENT_TYPE_CODE),
        attachmentMap.get(IExpectedKeys.ATTACHMENT));
  }
}
