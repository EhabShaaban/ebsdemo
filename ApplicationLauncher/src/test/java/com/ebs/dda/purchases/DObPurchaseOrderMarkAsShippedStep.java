package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderShippedValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsShippedTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderMarkAsShippedStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderMarkAsShippedTestUtils shippedTestUtils;

  public DObPurchaseOrderMarkAsShippedStep() {

    Given(
        "^the following Import PurchaseOrders exist with producation date:$",
        (DataTable purchaseOrderTable) -> {
          shippedTestUtils.assertThatImportPurchaseOrderWithProductionDateIsExist(
              purchaseOrderTable);
        });

    Given(
        "^the following Local PurchaseOrders exist with confirmation date:$",
        (DataTable purchaseOrderTable) -> {
          shippedTestUtils.assertThatLocalPurchaseOrderWithConfirmationDateIsExist(
              purchaseOrderTable);
        });

    When(
        "^\"([^\"]*)\" requests to MarkAsShipped the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String updateDate,
            DataTable shippedDatesTable) -> {
          shippedTestUtils.freezeWithTimeZone(updateDate);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              shippedTestUtils.markAsShipped(userCookie, purchaseOrderCode, shippedDatesTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated with ShippingDate as follows:$",
        (String purchaseOrderCode, DataTable shippedDataTable) -> {
          shippedTestUtils.assertThatPurchaseOrderShippedDetailsIsUpdated(
              purchaseOrderCode, shippedDataTable);
        });

    Then(
        "^the following error message is attached to shipping date field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          shippedTestUtils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObPurchaseOrderShippedValueObject.SHIPPING_DATE_TIME);
        });
  }

  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    shippedTestUtils = new DObPurchaseOrderMarkAsShippedTestUtils(getProperties());
    shippedTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsShipped PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(shippedTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(shippedTestUtils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "MarkAsShipped PO")) {
      shippedTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, shippedTestUtils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsShipped PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(shippedTestUtils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
