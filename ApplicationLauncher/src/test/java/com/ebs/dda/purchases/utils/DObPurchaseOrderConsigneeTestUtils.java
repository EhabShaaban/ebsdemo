package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;

import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderConsigneeTestUtils extends DObPurchaseOrderCommonTestUtils {

  protected final String CODE = "Code";
  protected final String CONSIGNEE = "Consignee";
  protected final String PLANT = "Plant";
  protected final String STOREHOUSE = "Storehouse";

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Consignee]";

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    return str.toString();
  }

  public void assertThatPlantsExist(DataTable plantsDataTable) {
    List<Map<String, String>> plantMapList = plantsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> plantMap : plantMapList) {
      Object plant =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPlantByCodeAndName", getPlantQueryParams(plantMap));
      assertNotNull(
          ASSERTION_MSG + " this Plant with code " + plantMap.get(ENTITY_CODE) + " doesn't exist",
          plant);
    }
  }

  private Object[] getPlantQueryParams(Map<String, String> plantMap) {
    return new Object[] {
      plantMap.get(ENTITY_CODE), plantMap.get(ENTITY_NAME),
    };
  }

  public void assertThatStorehousesExist(DataTable storehousesDataTable) {
    List<Map<String, String>> storehouseMapList =
        storehousesDataTable.asMaps(String.class, String.class);

    for (Map<String, String> storehouseMap : storehouseMapList) {
      Object storehouse =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getStorehouseByCodeAndName", getStorehouseQueryParams(storehouseMap));
      assertNotNull(
          ASSERTION_MSG
              + " this storehouse with code "
              + storehouseMap.get(ENTITY_CODE)
              + " doesn't exist",
          storehouse);
    }
  }

  private Object[] getStorehouseQueryParams(Map<String, String> storehouseMap) {
    return new Object[] {
      storehouseMap.get(ENTITY_CODE), storehouseMap.get(ENTITY_NAME),
    };
  }
}
