package com.ebs.dda.purchases.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/ordermanagement/purchase-order/PO_View_ItemsWithPrices.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_ItemsWithoutPrices.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestAdd_NewItem.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_NewItem_HP.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_NewItem_Val.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_NewItem_Auth.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestEdit_ItemQtyInDNPL.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_ItemQtyInDNPL.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Delete_Item.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestAdd_NewItemQty.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_NewItemQty_HP.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_NewItemQty_Val.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_NewItemQty_Auth.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestEdit_ItemQty.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_ExistingItemQty_HP.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_ExistingItemQty_Val.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_ExistingItemQty_Auth.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Delete_ItemQuantity.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.purchases"},
    strict = true,
    tags = "not @Future")
public class DObPurchaseOrderItemsCucumberRunner {}
