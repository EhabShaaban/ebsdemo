package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IResponseKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/** Created by "Zyad Ghorab" on Nov, 2018 */
public class IObPurchaseOrderViewRequiredDocumentsTestUtils
    extends IObPurchaseOrderRequiredDocumentsTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View]";

  public IObPurchaseOrderViewRequiredDocumentsTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public Response readPurchaseOrderRequiredDocuments(Cookie cookie, String purchaseOrderCode) {
    String restUrl = "/services/purchase/orders/requiredDocument/" + purchaseOrderCode;
    return sendGETRequest(cookie, restUrl);
  }

  public void assertThatPurchaseOrderRequiredDocumentsInResponse(
      DataTable purchaseOrderRequiredDocumentsTable, Response requiredDocumentsResponse) {

    List<Map<String, String>> purchaseOrderRequiredDocumentsData =
        purchaseOrderRequiredDocumentsTable.asMaps(String.class, String.class);
    List<Map<String, Object>> requiredDocumentsResponseDataMap =
        requiredDocumentsResponse.body().jsonPath().get("data");

    for (int i = 0; i < purchaseOrderRequiredDocumentsData.size(); i++) {
      String actualDocumentTypeName =
          requiredDocumentsResponse
              .body()
              .jsonPath()
              .get("data" + "[" + i + "]." + IResponseKeys.RESPONSE_ATTACHMENT_NAME + ".values.en");
      String actualNumberOfCopies =
          requiredDocumentsResponseDataMap
              .get(i)
              .get(IResponseKeys.RESPONSE_NUMBER_OF_COPIES)
              .toString();
      assertEquals(ASSERTION_MSG + " Document Type " + purchaseOrderRequiredDocumentsData.get(i).get(IExpectedKeys.DOCUMENT_TYPE) + " is not correct",
          purchaseOrderRequiredDocumentsData.get(i).get(IExpectedKeys.DOCUMENT_TYPE),
          actualDocumentTypeName);
      assertEquals(ASSERTION_MSG + " Number Of Copies " + purchaseOrderRequiredDocumentsData.get(i).get(IExpectedKeys.COPIES) + " is not correct",
          purchaseOrderRequiredDocumentsData.get(i).get(IExpectedKeys.COPIES),
          actualNumberOfCopies);
    }
  }
}
