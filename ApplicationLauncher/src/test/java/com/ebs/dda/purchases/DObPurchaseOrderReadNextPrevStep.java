package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderReadNextPrevTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderReadNextPrevStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderReadNextPrevTestUtils utils;

  public DObPurchaseOrderReadNextPrevStep() {

    Given(
        "^the following PurchaseOrders with the following order exist:$",
        (DataTable purchaseOrdersDataTable) -> {
          utils.assertThatPurchaseOrdersExistByCodeAndPurchaseUnit(purchaseOrdersDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view next PurchaseOrder of current PurchaseOrder with code \"([^\"]*)\"$",
        (String username, String currentPurchaseOrderCode) -> {
          Response response =
              utils.requestToViewNextPurchaseOrder(
                  userActionsTestUtils.getUserCookie(username), currentPurchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the next PurchaseOrder code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String nextVendorCode, String username) -> {
          utils.assertThatNextInstanceCodeIsCorrect(
              nextVendorCode, userActionsTestUtils.getUserResponse(username));
        });

    Then(
        "^the previous PurchaseOrder code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String previousVendorCode, String username) -> {
          utils.assertThatPreviousInstanceCodeIsCorrect(
              previousVendorCode, userActionsTestUtils.getUserResponse(username));
        });

    When(
        "^\"([^\"]*)\" requests to view previous PurchaseOrder of current PurchaseOrder with code \"([^\"]*)\"$",
        (String username, String currentPurchaseOrderCode) -> {
          Response response =
              utils.requestToViewPreviousPurchaseOrder(
                  userActionsTestUtils.getUserCookie(username), currentPurchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderReadNextPrevTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read previous PO")
        || contains(scenario.getName(), "Read Next PO")) {
      databaseConnector.createConnection();
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Read previous PO")
        || contains(scenario.getName(), "Read Next PO")) {
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
      databaseConnector.closeConnection();
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read previous PO")
        || contains(scenario.getName(), "Read Next PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
