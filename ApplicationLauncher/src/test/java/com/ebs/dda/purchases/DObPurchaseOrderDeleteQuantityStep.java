package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderDeleteQuantityTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObPurchaseOrderDeleteQuantityStep extends SpringBootRunner implements En {
  private static boolean hasBeenExecutedOnce = false;

  private DObPurchaseOrderDeleteQuantityTestUtils utils;

  public DObPurchaseOrderDeleteQuantityStep() {

    When(
        "^\"([^\"]*)\" requests to delete the quantity with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName,
            String quantityId,
            String itemCode,
            String purchaseOrderCode,
            String deleteTime) -> {
          utils.freeze(deleteTime);
          Response quantityDeleteResponse =
              utils.deleteQuantityById(
                  userActionsTestUtils.getUserCookie(userName),
                  Long.valueOf(quantityId),
                  itemCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, quantityDeleteResponse);
        });

    Then(
        "^quantity with id \"([^\"]*)\" is deleted successfully$",
        (String quantityId) -> {
          utils.assertQuantityIsDeleted(Long.valueOf(quantityId));
        });

    Then(
        "^PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable orderModificationInfo) -> {
          utils.assertPurchaseOrderIsUpdated(purchaseOrderCode, orderModificationInfo);
        });

    When(
        "^\"([^\"]*)\" requests to delete the quantity with id \"([^\"]*)\" of Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String quantityId, String itemCode, String purchaseOrderCode) -> {
          Response quantityDeleteResponse =
              utils.deleteQuantityById(
                  userActionsTestUtils.getUserCookie(userName),
                  Long.valueOf(quantityId),
                  itemCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, quantityDeleteResponse);
        });

    Given(
        "^OrderItems section of PurchaseOrder with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL = IPurchaseOrderRestUrls.ADD_ITEM_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first deleted the Quantity with id \"([^\"]*)\" of Item with code \"([^\"]*)\" of PurchaseOrder with code \"([^\"]*)\" successfully$",
        (String userName, String qtyId, String itemCode, String purchaseOrderCode) -> {
          Response quantityDeleteResponse =
              utils.deleteQuantityById(
                  userActionsTestUtils.getUserCookie(userName),
                  Long.valueOf(qtyId),
                  itemCode,
                  purchaseOrderCode);
          utils.assertResponseSuccessStatus(quantityDeleteResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderDeleteQuantityTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete Item Quantity from Import/Local PO,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Delete Item Quantity from Import/Local PO,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockSectionForAllUsers(IPurchaseOrderRestUrls.UNLOCK_ITEM_SECTION);
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete Item Quantity from Import/Local PO,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
