package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDobPurchaseOrderProductionFinishedValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsProductionFinishedTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderMarkAsProductionFinishedStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderMarkAsProductionFinishedTestUtils utils;

  public DObPurchaseOrderMarkAsProductionFinishedStep() {

    Given(
        "^the following PurchaseOrders exist with Confirmation date:$",
        (DataTable purchaseOrderTable) -> {
          utils.assertThatPurchaseOrderWithConfirmationDateDateIsExist(purchaseOrderTable);
        });

    When(
        "^\"([^\"]*)\" requests to MarkAsProductionFinished the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String updateDate,
            DataTable productionFinishedDatesTable) -> {
          utils.freezeWithTimeZone(updateDate);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.markAsProdcutionFinished(
                  userCookie, purchaseOrderCode, productionFinishedDatesTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated with ProductionFinishedDate as follows:$",
        (String purchaseOrderCode, DataTable updatedOrder) -> {
          utils.assertPurchaseOrderIsUpdated(purchaseOrderCode, updatedOrder);
        });

    Then(
        "^the following error message is attached to Production date field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response,
              errorCode,
              IDobPurchaseOrderProductionFinishedValueObject.PRODUCTION_FINISHED_DATE_TIME);
        });
  }

  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderMarkAsProductionFinishedTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsProductionFinished PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "MarkAsProductionFinished PO")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsProductionFinished PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
