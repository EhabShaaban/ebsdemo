package com.ebs.dda.purchases;

public interface IDObPurchaseOrderRestUrls {

  String VIEW_PAYMENT_AND_DELIVERY = "/services/purchase/orders/paymentanddelivery/";
  String AUTHORIZED_ACTIONS = "/services/purchase/orders/authorizedactions/";
  String VIEW_PURCHASE_ORDERS_FOR_GR = "/services/purchase/orders/purchaseordersforgoodsreceipt/";
}
