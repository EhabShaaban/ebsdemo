package com.ebs.dda.purchases;

import com.ebs.dda.CObGeolocaleTestUtils;
import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderExportPDFHeaderTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderPaymentAndDeliveryTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderExportPDFHeaderStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderExportPDFHeaderTestUtils exportPDFHeaderTestUtils;
  private MObVendorCommonTestUtils vendorCommonTestUtils;
  private DObPurchaseOrderPaymentAndDeliveryTestUtils paymentAndDeliveryTestUtils;
  private CObGeolocaleTestUtils geolocaleTestUtils;

  public DObPurchaseOrderExportPDFHeaderStep() {

    Given(
        "^the following Countries exist:$",
        (DataTable countriesDataTable) -> {
          geolocaleTestUtils.assrtThatCountriesExistByCodeAndName(countriesDataTable);
        });

    Given(
        "^the following Cities exist:$",
        (DataTable citiesDataTable) -> {
          geolocaleTestUtils.assrtThatCitiesExistByCodeAndName(citiesDataTable);
        });

    Given(
        "^the following Companies exist with the following data :$",
        (DataTable companyData) -> {
          exportPDFHeaderTestUtils.assertThatCompaniesExistWithAddressAndContacts(companyData);
        });

    Given(
        "^the following vendors exist:$",
        (DataTable vendorDataTable) -> {
          vendorCommonTestUtils.assertThatVendorsExistByCodeAndNameAndPurchaseUnits(vendorDataTable);
        });

    Given(
        "^the following PurchaseOrders exist with the following data:$",
        (DataTable purchaseOrdersDataTable) -> {
          exportPDFHeaderTestUtils.assertThatPurchaseOrdersExistWithPDFHeaderData(
              purchaseOrdersDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to export PurchaseOrder header of PurchaseOrder with code \"([^\"]*)\" as PDF$",
        (String username, String purchaseOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response response =
              exportPDFHeaderTestUtils.exportPurchaseOrderAsPDF(userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of PurchaseOrder Company data are displayed to \"([^\"]*)\":$",
        (String username, DataTable companyData) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          exportPDFHeaderTestUtils.assertThatPDFCompanyDataIsCorrect(response, companyData);
        });

    Then(
        "^the following values of PurchaseOrder Consignee data are displayed to \"([^\"]*)\":$",
        (String username, DataTable consigneeData) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          exportPDFHeaderTestUtils.assertThatPDFConsigneeDataIsCorrect(response, consigneeData);
        });

    Then(
        "^the following values of PurchaseOrder Vendor data are displayed to \"([^\"]*)\":$",
        (String username, DataTable vendorData) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          exportPDFHeaderTestUtils.assertThatPDFVendorDataIsCorrect(response, vendorData);
        });

    Then(
        "^the following values of PurchaseOrder PaymentAndDelivery data are displayed to \"([^\"]*)\":$",
        (String username, DataTable paymentAndDeliveryData) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          exportPDFHeaderTestUtils.assertThatPDFPaymentAndDeliveryDataIsCorrect(
              response, paymentAndDeliveryData);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    exportPDFHeaderTestUtils = new DObPurchaseOrderExportPDFHeaderTestUtils(properties);
    exportPDFHeaderTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    vendorCommonTestUtils = new MObVendorCommonTestUtils(properties);
    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    paymentAndDeliveryTestUtils = new DObPurchaseOrderPaymentAndDeliveryTestUtils(properties);
    geolocaleTestUtils = new CObGeolocaleTestUtils(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to export PO Header")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(exportPDFHeaderTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
