package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewConsigneeTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderViewConsigneeStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderViewConsigneeTestUtils utils;

  public DObPurchaseOrderViewConsigneeStep() {

    Given(
        "^the following Plants exist:$",
        (DataTable plantsDataTable) -> {
          utils.assertThatPlantsExist(plantsDataTable);
        });

    Given(
        "^the following Storehouses exist:$",
        (DataTable storehousesDataTable) -> {
          utils.assertThatStorehousesExist(storehousesDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view Consignee section of PurchaseOrder with code \"([^\"]*)\"$",
        (String username, String purchaseOrderCode) -> {
          Response purchaseOrderDataResponse =
              utils.readConsigneeData(
                  userActionsTestUtils.getUserCookie(username), purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, purchaseOrderDataResponse);
        });

    Then(
        "^the following values of Consignee section of PurchaseOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String poCode, String username, DataTable purchaseOrderDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatPurchaseOrderConsigneeSectionIsDiplayedToUser(
              purchaseOrderDataTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderViewConsigneeTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Consignee section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After()
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View Consignee section")) {
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Consignee section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
