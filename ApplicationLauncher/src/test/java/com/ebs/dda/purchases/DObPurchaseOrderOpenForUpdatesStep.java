package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderOpenForUpdatesTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewApprovalCyclesTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;

public class DObPurchaseOrderOpenForUpdatesStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderOpenForUpdatesTestUtils utils;
  private DObPurchaseOrderViewApprovalCyclesTestUtils approvalCyclesTestUtils;

  public DObPurchaseOrderOpenForUpdatesStep() {

    When(
        "^\"([^\"]*)\" requests to OpenForUpdates the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String purchaseOrderCode, String freezeTime) -> {
          utils.freeze(freezeTime);
          Response response =
              utils.openForUpdates(purchaseOrderCode, userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" of PurchaseOrder with Code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String username, String sectionName, String purchaseOrderCode, String editDateTime) -> {
          utils.freeze(editDateTime);
          String lockSectionUrl = utils.getLockSectionUrl(sectionName) + purchaseOrderCode;
          Response lockRepponse =
              userActionsTestUtils.lockSection(username, lockSectionUrl, purchaseOrderCode);
          utils.assertResponseSuccessStatus(lockRepponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderOpenForUpdatesTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    approvalCyclesTestUtils = new DObPurchaseOrderViewApprovalCyclesTestUtils(properties);
    approvalCyclesTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Open PO for updates")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Open PO for updates")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Open PO for updates")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
