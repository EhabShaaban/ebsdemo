package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class DObPurchaseOrderReadNextPrevTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Next/Prev]";

  public DObPurchaseOrderReadNextPrevTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/next-previous.sql");
    return str.toString();
  }

  public Response requestToViewPreviousPurchaseOrder(Cookie userCookie, String currentCode) {
    String restURL = "/services/purchase/orders/previous/" + currentCode;
    return sendGETRequest(userCookie, restURL);
  }

  public Response requestToViewNextPurchaseOrder(Cookie userCookie, String currentCode) {
    String restURL = "/services/purchase/orders/next/" + currentCode;
    return sendGETRequest(userCookie, restURL);
  }

  public void assertThatPurchaseOrdersExistByCodeAndPurchaseUnit(
      DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrderMapList =
        purchaseOrdersDataTable.asMaps(String.class, String.class);

    for (Map<String, String> purchaseOrderMap : purchaseOrderMapList) {
      Object purchaseOrder =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPOByCodeAndBusinessUnit", getPurchaseOrderQueryParams(purchaseOrderMap));

      assertNotNull(ASSERTION_MSG + " Purchase order does not exist", purchaseOrder);
    }
  }

  private Object[] getPurchaseOrderQueryParams(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      "%" + purchaseOrderMap.get(IFeatureFileCommonKeys.STATE) + "%",
      purchaseOrderMap.get(IFeatureFileCommonKeys.TYPE),
    };
  }
}
