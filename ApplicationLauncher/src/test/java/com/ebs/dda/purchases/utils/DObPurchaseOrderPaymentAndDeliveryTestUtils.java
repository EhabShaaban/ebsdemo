package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.ebs.dda.purchases.IAssertionMessages;
import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.ITableKeys;
import com.ebs.dda.purchases.PaymentAndDeliverySection;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderPaymentAndDeliveryTestUtils extends DObPurchaseOrderCommonTestUtils {

  private final String SHIPPING_TABLE_NAME = "cobshipping";
  private final String PAYMENT_TERMS_TABLE_NAME = "cobpaymentterms";
  private final String TRANSPORT_MODE_OBJECT_TYPE_CODE = "2";
  private final String TRANSPORT_MODE_TABLE_NAME = "localizedlobshipping";
  private final String CONTAINER_TYPES_OBJECT_TYPE_CODE = "17";
  private final String CONTAINER_TYPES_TABLE_NAME = "localizedlobmaterial";
  private final String INCOE_TERMS_OBJECT_TYPE_CODE = "31";
  private final String INCO_TERMS_TABLE_NAME = "lobsimplematerialdata";
  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Payment][And][Delivery]";

  public DObPurchaseOrderPaymentAndDeliveryTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    return str.toString();
  }

  public void assertThatPurchaseOrderExistWithPaymentAndDeliveryData(
      DataTable paymentAndDeliveryTable) throws Exception {

    List<Map<String, String>> paymentAndDeliveryMapsList =
        paymentAndDeliveryTable.asMaps(String.class, String.class);

    for (Map<String, String> paymentAndDeliveryMap : paymentAndDeliveryMapsList) {

      paymentAndDeliveryMap = convertEmptyStringsToNulls(paymentAndDeliveryMap);

      String query = getPaymentAndDeliveryDataQuery(paymentAndDeliveryMap);

      List<Map<String, Object>> records = getPaymentAndDeliveryData(paymentAndDeliveryMap, query);
      assertNotNull(
          ASSERTION_MSG + " " + IAssertionMessages.ORDER_PAYMENT_AND_DELIVERY_IS_NOT_NULL, records);
      assertEquals(
          ASSERTION_MSG + " " + IAssertionMessages.ORDER_PAYMENT_AND_DELIVERY_HAS_ONE_RECORD,
          1,
          records.size());
    }
  }

  public void assertThatIncotermsExist(DataTable incotermDataTable) throws Exception {
    assertThatAllEntityRecordsExist(
        incotermDataTable, INCO_TERMS_TABLE_NAME, INCOE_TERMS_OBJECT_TYPE_CODE, false);
  }

  public void assertThatPaymentTermsExist(DataTable paymentTermsDataTable) throws Exception {
    assertThatAllEntityRecordsExist(paymentTermsDataTable, PAYMENT_TERMS_TABLE_NAME, true);
  }

  public void assertThatShippingInstructionsExist(DataTable shippingInstructionsDataTable)
      throws Exception {
    assertThatAllEntityRecordsExist(shippingInstructionsDataTable, SHIPPING_TABLE_NAME, true);
  }

  public void assertThatContainerTypesExist(DataTable containerTypesDataTable) throws Exception {
    assertThatAllEntityRecordsExist(
        containerTypesDataTable,
        CONTAINER_TYPES_TABLE_NAME,
        CONTAINER_TYPES_OBJECT_TYPE_CODE,
        true);
  }

  public void assertThatTransportModesExist(DataTable transportModesDataTable) throws Exception {
    assertThatAllEntityRecordsExist(
        transportModesDataTable, TRANSPORT_MODE_TABLE_NAME, TRANSPORT_MODE_OBJECT_TYPE_CODE, true);
  }

  public void assertThatPortsExist(DataTable portsDataTable) throws Exception {
    List<Map<String, String>> expectedRecordsMapsList =
        portsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedRecordsMap : expectedRecordsMapsList) {
      String query =
          "SELECT name::json ->> 'en' as name , countryName::json ->> 'en' as countryName FROM portscountry , LocalizedLObMaterial "
              + "WHERE portscountry.id = LocalizedLObMaterial.id AND code = ?";
      PreparedStatement preparedStatement =
          constructPreparedStatement(query, expectedRecordsMap.get(ENTITY_CODE));
      ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
      resultSet.next();
      assertEquals(
          ASSERTION_MSG + " The Name " + expectedRecordsMap.get(ENTITY_NAME) + " is not correct",
          expectedRecordsMap.get(ENTITY_NAME),
          resultSet.getString("name"));
      assertEquals(
          ASSERTION_MSG
              + " The Country Name "
              + expectedRecordsMap.get("Country")
              + " is not correct",
          expectedRecordsMap.get("Country"),
          resultSet.getString("countryName"));
    }
  }

  public void assertThatPaymentAndDeliveryDataIsUpdated(
      String purchaseOrderCode, DataTable paymentAndDeliveryTable) throws Exception {

    Map<String, String> expectedMap =
        paymentAndDeliveryTable.asMaps(String.class, String.class).get(0);
    Map<String, String> modifiedMap = convertEmptyStringsToNulls(expectedMap);
    modifiedMap.put(IExpectedKeys.CODE, purchaseOrderCode);

    String paymentAndDeliveryUpdateQuery = getPaymentAndDeliveryUpdatedRecordQuery(modifiedMap);
    List<Map<String, Object>> paymentAndDeliveryData =
        getPaymentAndDeliveryUpdatedRecordData(modifiedMap, paymentAndDeliveryUpdateQuery);
    assertEquals(
        ASSERTION_MSG + " " + IAssertionMessages.ORDER_PAYMENT_AND_DELIVERY_HAS_ONE_RECORD,
        1,
        paymentAndDeliveryData.size());
  }

  public Response savePaymentAndDeliverySection(
      Cookie userCookie, String purchaseOrderCode, DataTable paymentTermsDataTable) {
    JsonObject jsonValueObject =
        initializePaymentTermsJsonValueObject(purchaseOrderCode, paymentTermsDataTable);
    String restURL = IPurchaseOrderRestUrls.SAVE_PAYMENT_DELIVERY_SECTION + purchaseOrderCode;
    return sendPUTRequest(userCookie, restURL, jsonValueObject);
  }

  // TODO: Move to common test utils
  protected String prepareQuery(Map<String, String> expectedMap, String key) {
    if (expectedMap.get(key) == null || expectedMap.get(key).isEmpty()) {
      return " IS NULL ";
    }
    return " = ? ";
  }

  // TODO: check null parameter not empty parameter
  @Override
  protected boolean isEmptyParameter(Object param) {
    return param == null;
  }

  private String getPaymentAndDeliveryDataQuery(Map<String, String> expectedMap) {

    if (expectedMap.get(IExpectedKeys.INCOTERM).equals("_")) {
      return "SELECT * "
          + "From PaymentAndDeliveryGeneralModel "
          + "WHERE code = ?"
          + "  And currentstates LIKE ?"
          + "  And purchaseunitcode = ?";
    } else {
      return "SELECT * "
          + "From PaymentAndDeliveryGeneralModel "
          + "WHERE code = ?"
          + "  And currentstates LIKE ?"
          + "  And purchaseunitcode = ?"
          + "  And "
          + ITableKeys.INCOTERM
          + prepareQuery(expectedMap, IExpectedKeys.INCOTERM)
          + "  And "
          + ITableKeys.CURRENCY
          + prepareQuery(expectedMap, IExpectedKeys.CURRENCY)
          + "  And "
          + ITableKeys.PAYMENT_TERM
          + prepareQuery(expectedMap, IExpectedKeys.PAYMENT_TERMS)
          + "  And "
          + ITableKeys.SHIPPING_INSTRUCTIONS
          + prepareQuery(expectedMap, IExpectedKeys.SHIPPING_INSTRUCTIONS)
          + "  And "
          + ITableKeys.CONTAINER_NO
          + prepareQuery(expectedMap, IExpectedKeys.CONTAINERS_NO)
          + "  And "
          + ITableKeys.CONTAINER
          + prepareQuery(expectedMap, IExpectedKeys.CONTAINERS_TYPE)
          + "  And "
          + ITableKeys.FORMATTED_COLLECTION_DATE
          + prepareQuery(expectedMap, IExpectedKeys.COLLECTION_DATE)
          + "  And "
          + ITableKeys.NUMBER_OF_DAYS
          + prepareQuery(expectedMap, IExpectedKeys.NUMBER_OF_DAYS)
          + "  And "
          + ITableKeys.MODE_OF_TRANSPORT
          + prepareQuery(expectedMap, IExpectedKeys.MODE_OF_TRANSPORT)
          + "  And "
          + ITableKeys.LOADING_PORT
          + prepareQuery(expectedMap, IExpectedKeys.LOADING_PORT)
          + "  And "
          + ITableKeys.DISCHARGE_PORT
          + prepareQuery(expectedMap, IExpectedKeys.DISCHARGE_PORT);
    }
  }

  private List<Map<String, Object>> getPaymentAndDeliveryData(
      Map<String, String> expectedMap, String query) throws Exception {

    if (expectedMap.get(IExpectedKeys.INCOTERM).equals("_")) {
      return getDatabaseRecords(
          query,
          expectedMap.get(IExpectedKeys.CODE),
          "%" + expectedMap.get(IExpectedKeys.STATE) + "%",
          expectedMap.get(IExpectedKeys.PURCHASE_UNIT));
    } else {
      return getDatabaseRecords(
          query,
          expectedMap.get(IExpectedKeys.CODE),
          "%" + expectedMap.get(IExpectedKeys.STATE) + "%",
          expectedMap.get(IExpectedKeys.PURCHASE_UNIT),
          expectedMap.get(IExpectedKeys.INCOTERM),
          expectedMap.get(IExpectedKeys.CURRENCY),
          expectedMap.get(IExpectedKeys.PAYMENT_TERMS),
          expectedMap.get(IExpectedKeys.SHIPPING_INSTRUCTIONS),
          getNumberValue(expectedMap.get(IExpectedKeys.CONTAINERS_NO)),
          expectedMap.get(IExpectedKeys.CONTAINERS_TYPE),
          expectedMap.get(IExpectedKeys.COLLECTION_DATE),
          getNumberValue(expectedMap.get(IExpectedKeys.NUMBER_OF_DAYS)),
          expectedMap.get(IExpectedKeys.MODE_OF_TRANSPORT),
          expectedMap.get(IExpectedKeys.LOADING_PORT),
          expectedMap.get(IExpectedKeys.DISCHARGE_PORT));
    }
  }

  private String getPaymentAndDeliveryUpdatedRecordQuery(Map<String, String> expectedMap) {
    return "SELECT * "
        + "From PaymentAndDeliveryGeneralModel "
        + "WHERE code = ? AND modificationinfo = ? "
        + "  AND "
        + ITableKeys.FORMATTED_LAST_UPDATED_DATE
        + " = ? "
        + "  And "
        + ITableKeys.INCOTERM
        + prepareQuery(expectedMap, IExpectedKeys.INCOTERM)
        + "  And "
        + ITableKeys.CURRENCY
        + prepareQuery(expectedMap, IExpectedKeys.CURRENCY)
        + "  And "
        + ITableKeys.PAYMENT_TERM
        + prepareQuery(expectedMap, IExpectedKeys.PAYMENT_TERMS)
        + "  And "
        + ITableKeys.SHIPPING_INSTRUCTIONS
        + prepareQuery(expectedMap, IExpectedKeys.SHIPPING_INSTRUCTIONS)
        + "  And "
        + ITableKeys.CONTAINER_NO
        + prepareQuery(expectedMap, IExpectedKeys.CONTAINERS_NO)
        + "  And "
        + ITableKeys.CONTAINER
        + prepareQuery(expectedMap, IExpectedKeys.CONTAINERS_TYPE)
        + "  And "
        + ITableKeys.FORMATTED_COLLECTION_DATE
        + prepareQuery(expectedMap, IExpectedKeys.COLLECTION_DATE)
        + "  And "
        + ITableKeys.NUMBER_OF_DAYS
        + prepareQuery(expectedMap, IExpectedKeys.NUMBER_OF_DAYS)
        + "  And "
        + ITableKeys.MODE_OF_TRANSPORT
        + prepareQuery(expectedMap, IExpectedKeys.MODE_OF_TRANSPORT)
        + "  And "
        + ITableKeys.LOADING_PORT
        + prepareQuery(expectedMap, IExpectedKeys.LOADING_PORT)
        + "  And "
        + ITableKeys.DISCHARGE_PORT
        + prepareQuery(expectedMap, IExpectedKeys.DISCHARGE_PORT);
  }

  private List<Map<String, Object>> getPaymentAndDeliveryUpdatedRecordData(
      Map<String, String> expectedMap, String query) throws Exception {

    return getDatabaseRecords(query, getPaymentAndDeliveryUpdateQueryParams(expectedMap).toArray());
  }

  private List<Object> getPaymentAndDeliveryUpdateQueryParams(Map<String, String> expectedMap) {
    List<Object> queryParams = new ArrayList<>();

    queryParams.addAll(
        Arrays.asList(
            expectedMap.get(IExpectedKeys.CODE),
            expectedMap.get(IExpectedKeys.LAST_UPDATED_BY),
            expectedMap.get(IExpectedKeys.LAST_UPDATED_DATE),
            expectedMap.get(IExpectedKeys.INCOTERM),
            expectedMap.get(IExpectedKeys.CURRENCY),
            expectedMap.get(IExpectedKeys.PAYMENT_TERMS),
            expectedMap.get(IExpectedKeys.SHIPPING_INSTRUCTIONS),
            getNumberValue(expectedMap.get(IExpectedKeys.CONTAINERS_NO)),
            expectedMap.get(IExpectedKeys.CONTAINERS_TYPE),
            expectedMap.get(IExpectedKeys.COLLECTION_DATE),
            getNumberValue(expectedMap.get(IExpectedKeys.NUMBER_OF_DAYS)),
            expectedMap.get(IExpectedKeys.MODE_OF_TRANSPORT),
            expectedMap.get(IExpectedKeys.LOADING_PORT),
            expectedMap.get(IExpectedKeys.DISCHARGE_PORT)));
    return queryParams;
  }

  private JsonObject initializePaymentTermsJsonValueObject(
      String purchaseOrderCode, DataTable paymentTermsDataTable) {

    Map<String, String> paymentTermsMap =
        paymentTermsDataTable.asMaps(String.class, String.class).get(0);
    paymentTermsMap = convertEmptyStringsToNulls(paymentTermsMap);
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("purchaseOrderCode", purchaseOrderCode);
    jsonObject.addProperty(
        PaymentAndDeliverySection.PAYMENT_TERMS_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_PAYMENT_TERMS_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.INCOTERM_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_INCOTERM_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.SHIPPING_INSTRUCTIONS_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_SHIPPING_INSTRUCTIONS_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.TRANSPORT_MODE_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_TRANSPORT_MODE_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.LOADING_PORT_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_LOADING_PORT_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.DISCHARGE_PORT_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_DISCHARGE_PORT_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.COLLECTION_DATE_JSON_KEY,
        (paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_COLLECTION_DATE_KEY)));
    jsonObject.addProperty(
        PaymentAndDeliverySection.NUMBER_OF_DAYS_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_NUMBER_OF_DAYS_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.CURRENCY_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_CURRENCY_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.CONTAINERS_TYPE_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_CONTAINERS_TYPE_KEY));
    jsonObject.addProperty(
        PaymentAndDeliverySection.CONTAINERS_NO_JSON_KEY,
        paymentTermsMap.get(PaymentAndDeliverySection.EXPECTED_CONTAINER_NO_KEY));

    return jsonObject;
  }

  private Float getNumberValue(Object value) {
    if (value != null && !value.toString().isEmpty()) {
      return Float.parseFloat(value.toString());
    }
    return null;
  }

  private void assertThatAllEntityRecordsExist(
      DataTable incotermDataTable,
      String tableName,
      String objectTypeCode,
      boolean isLocalizedNameSelector)
      throws Exception {
    List<Map<String, String>> expectedRecordsMapsList =
        incotermDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedRecordsMap : expectedRecordsMapsList) {
      assertEntityExistsByCodeAndName(
          expectedRecordsMap, tableName, objectTypeCode, isLocalizedNameSelector);
    }
  }

  private void assertThatAllEntityRecordsExist(
      DataTable incotermDataTable, String tableName, boolean isLocalizedNameSelector)
      throws Exception {
    List<Map<String, String>> expectedRecordsMapsList =
        incotermDataTable.asMaps(String.class, String.class);
    for (Map<String, String> expectedRecordsMap : expectedRecordsMapsList) {
      assertEntityExistsByCodeAndName(expectedRecordsMap, tableName, isLocalizedNameSelector);
    }
  }

  private void assertEntityExistsByCodeAndName(
      Map<String, String> recordMap,
      String tableName,
      String objectTypeCode,
      boolean isLocalizedNameSelector)
      throws Exception {

    String query =
        "SELECT name "
            + getNameSelector(isLocalizedNameSelector)
            + " as name FROM "
            + tableName
            + " WHERE objecttypecode = ?"
            + " AND code = ?";

    PreparedStatement preparedStatement =
        constructPreparedStatement(query, objectTypeCode, recordMap.get(ENTITY_CODE));
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    resultSet.next();
    assertEquals(
        ASSERTION_MSG + " This name " + recordMap.get(ENTITY_NAME) + " is not correct",
        recordMap.get(ENTITY_NAME),
        resultSet.getString("name"));
  }

  private void assertEntityExistsByCodeAndName(
      Map<String, String> recordMap, String tableName, boolean isLocalizedNameSelector)
      throws Exception {
    String query =
        "SELECT name "
            + getNameSelector(isLocalizedNameSelector)
            + " as name FROM "
            + tableName
            + " WHERE code = ?";
    PreparedStatement preparedStatement =
        constructPreparedStatement(query, recordMap.get(ENTITY_CODE));
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    resultSet.next();
    assertEquals(
        ASSERTION_MSG + " This name " + recordMap.get(ENTITY_NAME) + " is not correct",
        recordMap.get(ENTITY_NAME),
        resultSet.getString("name"));
  }

  private String getNameSelector(boolean isLocalizedNameSelector) {
    return isLocalizedNameSelector ? ":: json ->> 'en'" : "";
  }
}
