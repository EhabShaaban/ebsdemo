package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderDeleteQuantityTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class DObPurchaseOrderSaveAddItemQuantityStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderItemTestUtils utils;
  private DObPurchaseOrderDeleteQuantityTestUtils deleteQuantityTestUtils;

  public DObPurchaseOrderSaveAddItemQuantityStep() {

    Given(
        "^the following PurchaseOrders exist with the following Items and Quantities:$",
        (DataTable dataTable) -> {
          deleteQuantityTestUtils.assertThatPurchaseOrdersExistWithItemsAndQuantities(dataTable);
        });

    When(
        "^\"([^\"]*)\" adds Item Quantity in Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String itemCode,
            String purchaseOrderCode,
            String dateTime,
            DataTable quantitiesTable) -> {
          utils.freeze(dateTime);
          addItemQuantityToOrderItemsInPurchaseOrder(
              userName, itemCode, purchaseOrderCode, quantitiesTable);
        });

    Then(
        "^new Item Quantity is added to Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" with the following values:$",
        (String itemCode, String orderCode, DataTable dataTable) -> {
          utils.assertItemQuantityIsAdded(itemCode, orderCode, dataTable);
        });

    Then(
        "^Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String itemCode, String orderCode, DataTable dataTable) -> {
          deleteQuantityTestUtils.assertPurchaseOrderLineDetailsIsUpdated(
              itemCode, orderCode, dataTable);
        });

    Then(
        "^the PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable purchaseOrderData) -> {
          utils.assertThatPurchaseOrderUpdatedByUserAtDate(purchaseOrderCode, purchaseOrderData);
        });

    Then(
        "^add Qty dialog is closed and the lock by \"([^\"]*)\" on OrderItems section of PurchaseOrder with code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^add Quantity dialog is closed and the lock by \"([^\"]*)\" on OrderItems section of PurchaseOrder with code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    Then(
        "^the following error message is attached to orderUnitCode field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IIObOrderLineDetailsQuantitiesValueObject.ORDER_UNIT_CODE);
        });

    Given(
        "^Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\" has Item Quantity with OrderUnit \"([^\"]*)\"$",
        (String itemCode, String purchaseOrderCode, String orderUnitCode) -> {
          utils.assertOrderUnitExistsInItemInPurchaseOrder(
              itemCode, purchaseOrderCode, orderUnitCode);
        });
  }

  @When(
      "^\"([^\"]*)\" adds Item Quantity in Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\" with the following values:$")
  public void addItemQuantityToOrderItemsInPurchaseOrder(
      String userName, String itemCode, String purchaseOrderCode, DataTable quantitiesTable) {
    String url = IPurchaseOrderRestUrls.ADD_ITEM_QUANTITY;
    Response response =
        utils.addQuantity(
            url,
            purchaseOrderCode,
            itemCode,
            userActionsTestUtils.getUserCookie(userName),
            quantitiesTable);

    userActionsTestUtils.setUserResponse(userName, response);
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();

    utils = new DObPurchaseOrderItemTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);

    deleteQuantityTestUtils = new DObPurchaseOrderDeleteQuantityTestUtils(properties);
    deleteQuantityTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Add Item Quantity to Local/Import PurchaseOrder")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Save Add Item Quantity to Local/Import PurchaseOrder")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          new ArrayList<>(Collections.singletonList(IPurchaseOrderSectionNames.ITEMS_SECTION)));
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Add Item Quantity to Local/Import PurchaseOrder")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
