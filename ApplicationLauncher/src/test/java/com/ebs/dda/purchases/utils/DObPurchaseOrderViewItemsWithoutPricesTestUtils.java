package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IOrderItemsResponseKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class DObPurchaseOrderViewItemsWithoutPricesTestUtils
    extends DObPurchaseOrderViewItemsTestUtils {

  public final String VIEW_ITEMS_WITHOUT_PRICES_URL =
      "/services/purchase/orders/itemsviewwithoutprices/";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Items][Without][Prices]";


  public DObPurchaseOrderViewItemsWithoutPricesTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatOrderItemQuantitiesAreDisplayedCorrect(
      String itemCode, DataTable itemQuantitiesTable, Response purchaseOrderItemsDataResponse)
      throws Exception {
    List<Map<String, String>> expectedQuantities =
        itemQuantitiesTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItems =
        purchaseOrderItemsDataResponse.body().jsonPath().getList(DATA_ITEMS);
    for (HashMap<String, Object> actualItem : actualItems) {
      if (actualItem.get(IOrderItemsResponseKeys.ITEM_CODE).equals(itemCode)) {
        List<HashMap<String, Object>> actualQuantities =
            (List<HashMap<String, Object>>) actualItem.get(ITEM_QUANTITIES);
        assertCorrectOrderItemQuantities(expectedQuantities, actualQuantities);
      }
    }
  }

  private void assertCorrectOrderItemQuantities(
      List<Map<String, String>> expectedQuantities, List<HashMap<String, Object>> actualQuantities)
      throws Exception {
    int matchedQuantities = 0;
    for (Map<String, String> expectedQuantity : expectedQuantities) {
      for (Map<String, Object> actualQuantity : actualQuantities) {
        if (isSameItemQuantityOrderUnit(expectedQuantity, actualQuantity)) {
          expectedQuantity = convertEmptyStringsToNulls(expectedQuantity);
          assertResponseItemQuantity(expectedQuantity, actualQuantity);
          matchedQuantities++;
          break;
        }
      }
    }
    assertEquals(ASSERTION_MSG + " Quantities Size " + expectedQuantities.size() + " is not correct",
    expectedQuantities.size(), matchedQuantities);
  }
}
