package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObServicePurchaseOrderViewItemsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObServicePurchaseOrderViewItemsStep extends SpringBootRunner implements En {

  private DObServicePurchaseOrderViewItemsTestUtils utils;

  public DObServicePurchaseOrderViewItemsStep() {

    When(
        "^\"([^\"]*)\" requests to view Items section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readPurchaseOrderItemsData(cookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of Items section for PurchaseOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String purchaseOrderCode, String userName, DataTable expectedItems) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemsDataAreDisplayedToTheUserInResponse(expectedItems, response);
        });

    Then(
        "^the following values for TaxesSummary for PurchaseOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String purchaseOrderCode, String userName, DataTable taxDetailsDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatPurchaseOrderHasTaxesInResponse(taxDetailsDataTable, response);
        });

    Then(
        "^Items section for PurchaseOrder with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatItemResponseIsEmpty(response);
        });

    Then(
        "^TaxesSummary section for PurchaseOrder with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTaxesResponseIsEmpty(response);
        });

      Then(
              "^the following values for ItemsSummary for PurchaseOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
              (String purchaseOrderCode, String userName, DataTable itemSummaryDataTable) -> {
                  Response response = userActionsTestUtils.getUserResponse(userName);
                  utils.assertThatPurchaseOrderHasItemSummaryInResponse(itemSummaryDataTable, response);
              });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObServicePurchaseOrderViewItemsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View Service Purchase Order Items section,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
