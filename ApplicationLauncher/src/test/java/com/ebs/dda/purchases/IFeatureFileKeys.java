package com.ebs.dda.purchases;

public interface IFeatureFileKeys {

  String MAIN_USER = "User";
  String ALTERNATIVE_USER = "Alternative";
  String APPROVAL_POLICY = "ApprovalPolicy";
  String CONTROL_POINT = "ControlPoint";
  String USER = "User";
  String DATE_TIME = "DateTime";
  String DECISION = "Decision";
  String NOTES = "Notes";
}
