package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObServicePurchaseOrderViewItemsTestUtils;
import com.ebs.dda.purchases.utils.IObServicePurchaseOrderSaveNewItemHPTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObServicePurchaseOrderSaveNewItemStep extends SpringBootRunner implements En {

  private IObServicePurchaseOrderSaveNewItemHPTestUtils utils;
  private DObServicePurchaseOrderViewItemsTestUtils viewItemsTestUtils;

  public IObServicePurchaseOrderSaveNewItemStep() {

    Given(
        "^Items section of Service PurchaseOrder with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String servicePOCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockUrl = utils.getLockItemsURL(servicePOCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, servicePOCode);
          userActionsTestUtils.setUserResponse(userName, response);
          utils.assertThatResourceIsLockedByUser(
              userName,
              servicePOCode,
              IPurchaseOrderSectionNames.ITEMS_SECTION,
              utils.PO_JMX_LOCK_BEAN_NAME);
        });

    When(
        "^\"([^\"]*)\" saves the following new item to Items section of Service PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String servicePOCode, String dateTime, DataTable newItemDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.saveNewItem(cookie, servicePOCode, newItemDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Service PurchaseOrder with Code \"([^\"]*)\" is updated as follows:$",
        (String servicePOCode, DataTable purchaseOrderDataTable) -> {
          utils.assertThatPurchaseOrderIsUpdated(servicePOCode, purchaseOrderDataTable);
        });

    Then(
        "^the lock by \"([^\"]*)\" on Items section of Service PurchaseOrder with Code \"([^\"]*)\" is released$",
        (String userName, String servicePOCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              servicePOCode,
              utils.PO_JMX_LOCK_BEAN_NAME,
              IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    Then(
        "^Items section of Service PurchaseOrder with Code \"([^\"]*)\" is updated as follows and displayed to \"([^\"]*)\":$",
        (String servicePOCode, String userName, DataTable itemsDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(cookie, viewItemsTestUtils.getViewItemsUrl(servicePOCode));
          userActionsTestUtils.setUserResponse(userName, response);
          viewItemsTestUtils.assertThatItemsDataAreDisplayedToTheUserInResponse(
              itemsDataTable, response);
        });

    When(
        "^\"([^\"]*)\" saves the following new item to Items section of Service PurchaseOrder with Code \"([^\"]*)\" with the following values:$",
        (String userName, String servicePOCode, DataTable newItemDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.saveNewItem(cookie, servicePOCode, newItemDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new IObServicePurchaseOrderSaveNewItemHPTestUtils(
            getProperties(), entityManagerDatabaseConnector);
    viewItemsTestUtils =
        new DObServicePurchaseOrderViewItemsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save New Service PurchaseOrder Item")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save New Service PurchaseOrder Item")) {
      utils.unfreeze();
    }
  }
}
