package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class DObPurchaseOrderSaveEditItemQuantityStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderItemTestUtils utils;

  public DObPurchaseOrderSaveEditItemQuantityStep() {

    Given(
        "^\"([^\"]*)\" opened Item Quantity with id \"([^\"]*)\" in Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName,
            String quantityId,
            String itemCode,
            String purchaseOrderCode,
            String dateTime) -> {
          utils.freeze(dateTime);

          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_ADD_ITEM_QUANTITY + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);

          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" saves Item Quantity with id \"([^\"]*)\" for Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String quantityId,
            String itemCode,
            String purchaseOrderCode,
            String dateTime,
            DataTable quantitiesTable) -> {
          utils.freeze(dateTime);
          String url = IPurchaseOrderRestUrls.SAVE_ITEM_QUANTITY;
          Response response =
              utils.saveQuantity(
                  url,
                  purchaseOrderCode,
                  itemCode,
                  quantityId,
                  userActionsTestUtils.getUserCookie(userName),
                  quantitiesTable);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Item Quantity with id \"([^\"]*)\" in Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String quantityId, String itemCode, String orderCode, DataTable dataTable) -> {
          utils.assertItemQuantityIsUpdated(quantityId, itemCode, orderCode, dataTable);
        });

    Then(
        "^edit Qty dialog is closed and the lock by \"([^\"]*)\" on OrderItems section of PurchaseOrder with code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" saves Item Quantity with id \"([^\"]*)\" for Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\" with the following values:$",
        (String userName,
            String quantityId,
            String itemCode,
            String purchaseOrderCode,
            DataTable quantitiesTable) -> {
          String url = IPurchaseOrderRestUrls.SAVE_ITEM_QUANTITY;
          Response response =
              utils.saveQuantity(
                  url,
                  purchaseOrderCode,
                  itemCode,
                  quantityId,
                  userActionsTestUtils.getUserCookie(userName),
                  quantitiesTable);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^edit Quantity dialog is closed and the lock by \"([^\"]*)\" on OrderItems section of PurchaseOrder with code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderItemTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Edit Item Quantity to Local/Import PurchaseOrder")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Save Edit Item Quantity to Local/Import PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          new ArrayList<>(Collections.singletonList(IPurchaseOrderSectionNames.ITEMS_SECTION)));
    }

    if (scenario.getName().contains("Save Edit Item Quantity to Local/Import PurchaseOrder Val,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          new ArrayList<>(Collections.singletonList(IPurchaseOrderSectionNames.ITEMS_SECTION)));
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Edit Item Quantity to Local/Import PurchaseOrder")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
