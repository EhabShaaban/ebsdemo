package com.ebs.dda.purchases.utils;

import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.Assert.assertEquals;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderViewConsigneeTestUtils extends DObPurchaseOrderConsigneeTestUtils {

  private final String RESPONSE_USER_CODE = "userCode";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Consignee]";

  public DObPurchaseOrderViewConsigneeTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = getProperty(URL_PREFIX).toString();
  }

  public Response readConsigneeData(Cookie cookie, String puchaseOrderCode) {
    String restURL = "/services/purchase/orders/consignee/" + puchaseOrderCode;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatPurchaseOrderConsigneeSectionIsDiplayedToUser(
      DataTable purchaseOrderTable, Response purchaseOrderDataResponse) {

    Map<String, String> expectedConsigneeData =
        purchaseOrderTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualConsigneeData =
        purchaseOrderDataResponse.body().jsonPath().get("data");

    String consigneeName =
        purchaseOrderDataResponse.body().jsonPath().get("data.consigneeName.values.en");
    String plantName = purchaseOrderDataResponse.body().jsonPath().get("data.plantName.values.en");
    String storehouseName =
        purchaseOrderDataResponse.body().jsonPath().get("data.storehouseName.values.en");

    assertEquals(expectedConsigneeData.get(CODE), actualConsigneeData.get(RESPONSE_USER_CODE));
    assertEquals(ASSERTION_MSG + " Consignee Name " + expectedConsigneeData.get(CONSIGNEE) + " is not correct",
    expectedConsigneeData.get(CONSIGNEE), consigneeName);
    assertEquals(ASSERTION_MSG + " Plant Name " + expectedConsigneeData.get(PLANT) + " is not correct",
    expectedConsigneeData.get(PLANT), plantName);
    assertEquals(ASSERTION_MSG + " Storehouse Name " + expectedConsigneeData.get(STOREHOUSE) + " is not correct",
    expectedConsigneeData.get(STOREHOUSE), storehouseName);
  }
}
