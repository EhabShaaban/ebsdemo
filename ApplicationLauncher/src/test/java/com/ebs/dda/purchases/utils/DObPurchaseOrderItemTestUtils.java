package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.jpa.masterdata.item.IMObItemUOMGeneralModel;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.IQueries;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObOrderLineDetailsQuantitiesValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObPurchaseOrderItemTestUtils extends DObPurchaseOrderCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Item]";

  public DObPurchaseOrderItemTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");

    str.append(this.getDbScriptPathForItemVendorRecord());
    str.append(this.getDbScriptPathForItemQuantity());
    return str.toString();
  }

  public String getDbScriptPathForItemVendorRecord() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    return str.toString();
  }

  public String getDbScriptPathForServiceItemsDropdown() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public String getDbScriptPathForItemQuantity() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    return str.toString();
  }

  public Response saveQuantity(
      String url,
      String purchaseOrderCode,
      String itemCode,
      String quantityId,
      Cookie loginCookie,
      DataTable quantitiesTable) {
    JsonObject valueObject = initQuantityValueObject(quantitiesTable);
    return sendPUTRequest(
        loginCookie, url + purchaseOrderCode + "/" + itemCode + "/" + quantityId, valueObject);
  }

  public Response addQuantity(
      String url,
      String purchaseOrderCode,
      String itemCode,
      Cookie loginCookie,
      DataTable quantitiesTable) {
    JsonObject valueObject = initQuantityValueObject(quantitiesTable);
    return sendPUTRequest(loginCookie, url + purchaseOrderCode + "/" + itemCode, valueObject);
  }

  private JsonObject initQuantityValueObject(DataTable quantitiesTable) {
    Map<String, String> quantitiesMap = quantitiesTable.asMaps(String.class, String.class).get(0);
    quantitiesMap = convertEmptyStringsToNulls(quantitiesMap);
    JsonObject valueObject = new JsonObject();

    try {
      valueObject.addProperty(
          IIObOrderLineDetailsQuantitiesValueObject.QUANTITY,
          quantitiesMap.get(IFeatureFileCommonKeys.QUANTITY) != null
              ? new BigDecimal(quantitiesMap.get(IFeatureFileCommonKeys.QUANTITY))
              : null);
    } catch (NumberFormatException ex) {
      valueObject.addProperty(
          IIObOrderLineDetailsQuantitiesValueObject.QUANTITY,
          quantitiesMap.get(IFeatureFileCommonKeys.QUANTITY));
    }
    valueObject.addProperty(
        IIObOrderLineDetailsQuantitiesValueObject.ORDER_UNIT_CODE,
        quantitiesMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT));
    try {
      valueObject.addProperty(
          IIObOrderLineDetailsQuantitiesValueObject.PRICE,
          quantitiesMap.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE) != null
              ? new BigDecimal(quantitiesMap.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE))
              : null);
    } catch (NumberFormatException ex) {
      valueObject.addProperty(
          IIObOrderLineDetailsQuantitiesValueObject.PRICE,
          quantitiesMap.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE));
    }
    try {
      valueObject.addProperty(
          IIObOrderLineDetailsQuantitiesValueObject.DISCOUNT_PERCENTAGE,
          quantitiesMap.get(IFeatureFileCommonKeys.DISCOUNT) != null
              ? new BigDecimal(quantitiesMap.get(IFeatureFileCommonKeys.DISCOUNT).replace("%", ""))
              : null);
    } catch (NumberFormatException ex) {
      valueObject.addProperty(
          IIObOrderLineDetailsQuantitiesValueObject.DISCOUNT_PERCENTAGE,
          quantitiesMap.get(IFeatureFileCommonKeys.DISCOUNT));
    }
    return valueObject;
  }

  public void assertItemQuantityIsUpdated(
      String quantityId, String itemCode, String orderCode, DataTable expectedUpdates) {

    Map<String, String> expectedUpdatesMap =
        expectedUpdates.asMaps(String.class, String.class).get(0);
    Object orderLineDetailsQuantitiesGeneralModel =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getQuantityDetailsInItemsInPurchaseOrder",
            orderCode,
            expectedUpdatesMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            expectedUpdatesMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            itemCode,
            Long.parseLong(quantityId),
            new BigDecimal(expectedUpdatesMap.get(IFeatureFileCommonKeys.QUANTITY)),
            expectedUpdatesMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
            new BigDecimal(expectedUpdatesMap.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE)),
            new BigDecimal(
                expectedUpdatesMap.get(IFeatureFileCommonKeys.DISCOUNT).replace("%", "")));

    Assert.assertNotNull(
        ASSERTION_MSG + " this PO with code " + orderCode + " doesn't have Line Details Quantities",
        orderLineDetailsQuantitiesGeneralModel);
  }

  public void assertItemQuantityIsAdded(
      String itemCode, String orderCode, DataTable expectedUpdates) {

    Map<String, String> expectedUpdatesMap =
        expectedUpdates.asMaps(String.class, String.class).get(0);
    Object orderLineDetailsQuantitiesGeneralModel =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getNewQuantityDetailsInItemsInPurchaseOrder",
            orderCode,
            expectedUpdatesMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
            expectedUpdatesMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
            itemCode,
            new BigDecimal(expectedUpdatesMap.get(IFeatureFileCommonKeys.QUANTITY)),
            expectedUpdatesMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
            new BigDecimal(expectedUpdatesMap.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE)),
            new BigDecimal(
                expectedUpdatesMap.get(IFeatureFileCommonKeys.DISCOUNT).replace("%", "")));

    Assert.assertNotNull(
        ASSERTION_MSG + "The PO with code " + orderCode + " Doesn't exist!",
        orderLineDetailsQuantitiesGeneralModel);
  }

  public void assertOrderUnitExistsInItemInPurchaseOrder(
      String itemCode, String purchaseOrderCode, String orderUnitCode) {
    List orderUnitInItemInPO =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "checkOrderUnitInItemInPurchaseOrder", purchaseOrderCode, itemCode, orderUnitCode);
    Assert.assertTrue(
        ASSERTION_MSG
            + " There is no Order Unit with code "
            + orderUnitCode
            + " for This Item with code "
            + itemCode
            + " in PO with code purchaseOrderCode",
        orderUnitInItemInPO.size() == 1);
  }

  public Response deleteItemInPurchaseOrder(
      Cookie loginCookie, String purchaseOrderCode, String itemCode) {
    String deleteURL = IPurchaseOrderRestUrls.DELETE_ITEM + purchaseOrderCode + '/' + itemCode;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  public void assertEmptyItemSectionInPurchaseOrder(String purchaseOrderCode) throws Exception {
    List<Map<String, Object>> records =
        getDatabaseRecords(IQueries.ItemsInPOWithPOCode, purchaseOrderCode);
    assertTrue(
        ASSERTION_MSG + " There is no Items for Po with code " + purchaseOrderCode,
        records.isEmpty());
  }

  public void assertPurchaseOrdersExistWithItems(DataTable dataTable) {

    List<Map<String, String>> expectedOrderLineDetails =
        dataTable.asMaps(String.class, String.class);

    for (Map<String, String> record : expectedOrderLineDetails) {
      IObOrderLineDetailsQuantitiesGeneralModel orderHeaderGeneralModel =
          (IObOrderLineDetailsQuantitiesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrderItems",
                  record.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN),
                  record.get(IFeatureFileCommonKeys.PO_CODE),
                  record.get(IFeatureFileCommonKeys.ITEM_CODE));

      assertNotNull(
          ASSERTION_MSG
              + " There is no Line Details Quantities for Po with code "
              + record.get(IFeatureFileCommonKeys.PO_CODE),
          orderHeaderGeneralModel);
    }
  }

  public void assertThatUOMResponseCountIsCorrect(Response response, int uomCount) {
    List<HashMap<String, Object>> actualUOM = getListOfMapsFromResponse(response);
    Assert.assertEquals(uomCount, actualUOM.size());
  }

  public String getReadItemUOMDataUrl(String purchaseOrderCode, String itemCode) {
    return IPurchaseOrderRestUrls.QUERY_PURCHASE_ORDER
        + purchaseOrderCode
        + IPurchaseOrderRestUrls.READ_SERVICE_ITEM_UOM
        + itemCode;
  }

  public void assertThatUOMsValuesEqualExpectedInDropdown(
      DataTable uomDropdownValues, Response response) throws Exception {
    List<Map<String, String>> expectedUOMDropdownValues =
        uomDropdownValues.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualUOMDropdownValues = getListOfMapsFromResponse(response);
    for (int i = 0; i < actualUOMDropdownValues.size(); i++) {
      assertThatExpectedUOMsEqualActualUOMs(
          expectedUOMDropdownValues.get(i), actualUOMDropdownValues.get(i));
    }
  }

  private void assertThatExpectedUOMsEqualActualUOMs(
      Map<String, String> expectedMap, HashMap<String, Object> actualMap) throws Exception {
    MapAssertion mapAssertion =
        new MapAssertion(expectedMap, readMapFromString(actualMap, "UnitOfMeasure"));

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.UOM,
        IMObItemUOMGeneralModel.USER_CODE,
        IMObItemUOMGeneralModel.SYMBOL);
  }
}
