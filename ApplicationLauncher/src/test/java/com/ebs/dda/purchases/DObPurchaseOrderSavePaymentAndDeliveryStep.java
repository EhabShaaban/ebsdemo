package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderPaymentAndDeliveryTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderSavePaymentAndDeliveryStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObPurchaseOrderPaymentAndDeliveryTestUtils utils;

  public DObPurchaseOrderSavePaymentAndDeliveryStep() {

    When(
        "^\"([^\"]*)\" saves PaymentandDelivery section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String dateTime,
            DataTable paymentAndDeliveryDataTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePaymentAndDeliverySection(
                  userCookie, purchaseOrderCode, paymentAndDeliveryDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PaymentAndDelivery section of PurchaseOrder with Code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable paymentTermsDataTable) -> {
          utils.assertThatPaymentAndDeliveryDataIsUpdated(purchaseOrderCode, paymentTermsDataTable);
        });

    When(
        "^\"([^\"]*)\" saves PaymentandDelivery section of PurchaseOrder with Code \"([^\"]*)\" with the following values:$",
        (String username, String purchaseOrderCode, DataTable paymentAndDeliveryDataTable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response response =
              utils.savePaymentAndDeliverySection(
                  userCookie, purchaseOrderCode, paymentAndDeliveryDataTable);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderPaymentAndDeliveryTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save PaymentandDelivery section")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecuted = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save PaymentandDelivery section")) {
      utils.unfreeze();
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save PaymentandDelivery section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
