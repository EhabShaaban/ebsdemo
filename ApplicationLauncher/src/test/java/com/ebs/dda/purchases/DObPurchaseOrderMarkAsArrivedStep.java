package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderArrivalValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsArrivedTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderMarkAsArrivedStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderMarkAsArrivedTestUtils utils;

  public DObPurchaseOrderMarkAsArrivedStep() {

    When(
        "^\"([^\"]*)\" requests to MarkAsArrived the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String currentDateTime,
            DataTable arrivalDataTable) -> {
          utils.freezeWithTimeZone(currentDateTime);
          Map<String, String> arrivalDataMap =
              arrivalDataTable.asMaps(String.class, String.class).get(0);

          String arrivalUrl = utils.arrivalRestURL;
          JsonObject arrivalValueObject =
              utils.initArrivalValueObject(
                  arrivalDataMap.get(IExpectedKeys.ARRIVAL_DATE), purchaseOrderCode);

          Response response =
              utils.sendPostRequest(
                  userActionsTestUtils.getUserCookie(userName), arrivalUrl, arrivalValueObject);

          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated with ArrivalDate as follows:$",
        (String purchaseOrderCode, DataTable updateDetailsTable) -> {
          utils.assertThatPurchaseOrderUpdatedForMarkAsArrived(
              purchaseOrderCode, updateDetailsTable);
        });
    Given(
        "^the following PurchaseOrders exist with shipping date:$",
        (DataTable purchaseOrderTable) -> {
          utils.assertThatPurchaseOrderWithShippingDateDateIsExist(purchaseOrderTable);
        });

    Then(
        "^the following error message is attached to Arrival date field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObPurchaseOrderArrivalValueObject.ARRIVAL_DATE_TIME);
        });
  }

  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderMarkAsArrivedTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsArrived PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsForMarkAsArrived());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsArrived PO")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsArrived PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsForMarkAsArrived());
      databaseConnector.closeConnection();
    }
  }
}
