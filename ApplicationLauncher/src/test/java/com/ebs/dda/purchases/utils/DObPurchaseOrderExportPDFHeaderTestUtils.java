package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IExportPDFFeatureKeys;
import com.ebs.dda.purchases.IExportPDFResponseKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObPurchaseOrderExportPDFHeaderTestUtils extends DObPurchaseOrderExportPDFTestUtils {

  private static final String DATA_HEADER = "data.header";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Export][PDF][Items]";

  public DObPurchaseOrderExportPDFHeaderTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatCompaniesExistWithAddressAndContacts(DataTable companyData) {
    List<Map<String, String>> companiesList = companyData.asMaps(String.class, String.class);
    for (Map<String, String> companyMap : companiesList) {
      Object actualCompany =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getCompanyByAddressAndContactDetails",
              companyMap.get(IFeatureFileCommonKeys.CODE),
              companyMap.get(IFeatureFileCommonKeys.NAME),
              companyMap.get(IExportPDFFeatureKeys.ADDRESS),
              companyMap.get(IExportPDFFeatureKeys.POSTAL_CODE),
              companyMap.get(IExportPDFFeatureKeys.COUNTRY),
              companyMap.get(IExportPDFFeatureKeys.CITY),
              companyMap.get(IExportPDFFeatureKeys.TELEPHONE),
              companyMap.get(IExportPDFFeatureKeys.FAX));
      assertNotNull(ASSERTION_MSG + " There is no Company has this info", actualCompany);
    }
  }

  public void assertThatPDFCompanyDataIsCorrect(Response response, DataTable companyData)
      throws Exception {
    Map<String, String> expectedCompanyData = companyData.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualCompanyData = response.body().jsonPath().get(DATA_HEADER);
    assertEquals(ASSERTION_MSG + " Company name is not correct",
        expectedCompanyData.get(IExportPDFFeatureKeys.COMPANY),
        getEnglishValue(actualCompanyData.get(IExportPDFResponseKeys.COMPANY_NAME)));
    assertEquals(ASSERTION_MSG + " Company address is not correct",
        expectedCompanyData.get(IExportPDFFeatureKeys.COMPANY_ADDRESS),
        getEnglishValue(actualCompanyData.get(IExportPDFResponseKeys.COMPANY_ADDRESS)));
    assertEquals(ASSERTION_MSG + " Company country is not correct",
        expectedCompanyData.get(IExportPDFFeatureKeys.COMPANY_COUNTRY),
        getEnglishValue(actualCompanyData.get(IExportPDFResponseKeys.COMPANY_COUNTRY_NAME)));
    assertEquals(ASSERTION_MSG + " Company city is not correct",
        expectedCompanyData.get(IExportPDFFeatureKeys.COMPANY_CITY),
        getEnglishValue(actualCompanyData.get(IExportPDFResponseKeys.COMPANY_CITY_NAME)));
    assertEquals(ASSERTION_MSG + " Company Postal Code is not correct",
        expectedCompanyData.get(IExportPDFFeatureKeys.COMPANY_POSTAL_CODE),
        actualCompanyData.get(IExportPDFResponseKeys.COMPANY_POSTAL_CODE));
    assertEquals(ASSERTION_MSG + " Company Telephone is not correct",
        expectedCompanyData.get(IExportPDFFeatureKeys.COMPANY_TELEPHONE),
        actualCompanyData.get(IExportPDFResponseKeys.COMPANY_TELEPHONE));
    assertEquals(ASSERTION_MSG + " Company Fax is not correct",
        expectedCompanyData.get(IExportPDFFeatureKeys.COMPANY_FAX),
        actualCompanyData.get(IExportPDFResponseKeys.COMPANY_FAX));
  }

  public void assertThatPDFConsigneeDataIsCorrect(Response response, DataTable consigneeData)
      throws Exception {
    Map<String, String> expectedConsigneeData =
        consigneeData.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualConsigneeData = response.body().jsonPath().get(DATA_HEADER);
    assertEquals(ASSERTION_MSG + " Consignee name is not correct",
        expectedConsigneeData.get(IExportPDFFeatureKeys.CONSIGNEE),
        getEnglishValue(actualConsigneeData.get(IExportPDFResponseKeys.CONSIGNEE_NAME)));
    assertEquals(ASSERTION_MSG + " Consignee address is not correct",
        expectedConsigneeData.get(IExportPDFFeatureKeys.CONSIGNEE_ADDRESS),
        getEnglishValue(actualConsigneeData.get(IExportPDFResponseKeys.CONSIGNEE_ADDRESS)));
    assertEquals(ASSERTION_MSG + " Consignee country is not correct",
        expectedConsigneeData.get(IExportPDFFeatureKeys.CONSIGNEE_COUNTRY),
        getEnglishValue(actualConsigneeData.get(IExportPDFResponseKeys.CONSIGNEE_COUNTRY_NAME)));
    assertEquals(ASSERTION_MSG + " Consignee city is not correct",
        expectedConsigneeData.get(IExportPDFFeatureKeys.CONSIGNEE_CITY),
        getEnglishValue(actualConsigneeData.get(IExportPDFResponseKeys.CONSIGNEE_CITY_NAME)));
    assertEquals(ASSERTION_MSG + " Consignee Postal Code is not correct",
        expectedConsigneeData.get(IExportPDFFeatureKeys.CONSIGNEE_POSTAL_CODE),
        actualConsigneeData.get(IExportPDFResponseKeys.CONSIGNEE_POSTAL_CODE));
    assertEquals(ASSERTION_MSG + " Consignee Telephone is not correct",
        expectedConsigneeData.get(IExportPDFFeatureKeys.CONSIGNEE_TELEPHONE),
        actualConsigneeData.get(IExportPDFResponseKeys.CONSIGNEE_TELEPHONE));
    assertEquals(ASSERTION_MSG + " Consignee Fax is not correct",
        expectedConsigneeData.get(IExportPDFFeatureKeys.CONSIGNEE_FAX),
        actualConsigneeData.get(IExportPDFResponseKeys.CONSIGNEE_FAX));
  }

  public void assertThatPDFVendorDataIsCorrect(Response response, DataTable vendorData)
      throws Exception {
    Map<String, String> expectedVendorData = vendorData.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualVendorData = response.body().jsonPath().get(DATA_HEADER);
    assertEquals(ASSERTION_MSG + " Vendor name is not correct",
        expectedVendorData.get(IExportPDFFeatureKeys.VENDOR),
        getEnglishValue(actualVendorData.get(IExportPDFResponseKeys.VENDOR_NAME)));
  }

  public void assertThatPDFPaymentAndDeliveryDataIsCorrect(
      Response response, DataTable paymentAndDeliveryData) throws Exception {
    Map<String, String> expectedPaymentData =
        paymentAndDeliveryData.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualPaymentData = response.body().jsonPath().get(DATA_HEADER);
    assertEquals(ASSERTION_MSG + " Inco Term name is not correct",
        expectedPaymentData.get(IExportPDFFeatureKeys.INCOTERM),
        actualPaymentData.get(IExportPDFResponseKeys.INCOTERM_NAME));
    assertEquals(ASSERTION_MSG + " DischargePort is not correct",
        expectedPaymentData.get(IExportPDFFeatureKeys.DISCHARGE_PORT),
        getEnglishValue(actualPaymentData.get(IExportPDFResponseKeys.DISCHARGE_PORT_NAME)));
    assertEquals(ASSERTION_MSG + " Payment Term name is not correct",
        expectedPaymentData.get(IExportPDFFeatureKeys.PAYMENT_TERMS),
        getEnglishValue(actualPaymentData.get(IExportPDFResponseKeys.PAYMENT_TERM_NAME)));
    assertEquals(ASSERTION_MSG + " Collection Date is not correct",
        expectedPaymentData.get(IExportPDFFeatureKeys.COLLECTION_DATE),
        formatDate(actualPaymentData.get(IExportPDFResponseKeys.COLLECTION_DATE)));
    assertEquals(ASSERTION_MSG + " Mode Of Transport Name is not correct",
        expectedPaymentData.get(IExportPDFFeatureKeys.MODE_OF_TRANSPORT),
        getEnglishValue(actualPaymentData.get(IExportPDFResponseKeys.MODE_OF_TRANSPORT_NAME)));
  }

  private String formatDate(Object dateToFormat) {
    String expectedDate = null;
    if (dateToFormat != null) {
      DateTime dateInDatabaseFormat =
          DateTimeFormat.forPattern("yyyy-M-dd HH:mm:ss.S").parseDateTime(dateToFormat.toString());
      expectedDate = dateInDatabaseFormat.toString("dd-MMM-yyyy");
    }
    return expectedDate;
  }

  public void assertThatPurchaseOrdersExistWithPDFHeaderData(DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrdersList =
        purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersList) {
      Object purchaseOrder =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPurchaseOrderPDFHeaderData",
              purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
              purchaseOrderMap.get(IFeatureFileCommonKeys.TYPE),
              "%" + purchaseOrderMap.get(IFeatureFileCommonKeys.STATE) + "%",
              purchaseOrderMap.get(IExportPDFFeatureKeys.COMPANY),
              purchaseOrderMap.get(IExportPDFFeatureKeys.CONSIGNEE),
              purchaseOrderMap.get(IExportPDFFeatureKeys.VENDOR),
              purchaseOrderMap.get(IExportPDFFeatureKeys.MODE_OF_TRANSPORT),
              purchaseOrderMap.get(IExportPDFFeatureKeys.INCOTERM),
              purchaseOrderMap.get(IExportPDFFeatureKeys.DISCHARGE_PORT),
              purchaseOrderMap.get(IExportPDFFeatureKeys.PAYMENT_TERMS));
      assertNotNull(ASSERTION_MSG + " There is no Pdf Header Data for this PO with code " + purchaseOrderMap.get(IFeatureFileCommonKeys.CODE), purchaseOrder);
    }
  }
}
