package com.ebs.dda.purchases.utils;

import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderEditConsigneeTestUtils extends DObPurchaseOrderConsigneeTestUtils {

  public DObPurchaseOrderEditConsigneeTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  @Override
  protected void assertPurchaseOrdersExistWithStateAndPurchaseUnit(
      DataTable purchaseOrdersTable, String orderType) throws Exception {
    List<Map<String, String>> purchaseOrders =
        purchaseOrdersTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderData : purchaseOrders) {
      assertThatPurchaseOrderExistWithGivenStateAndPurchaseUnit(purchaseOrderData, orderType);
    }
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");

    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");

    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");

    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/CObApprovalPoliciesSqlScript.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");

    return str.toString();
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("," +"db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
            "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
            "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDocumentRequiredDocuments.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    return str.toString();
  }
  public String getDbScriptPathForDelete() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDocumentRequiredDocuments.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");

    return str.toString();
  }
}
