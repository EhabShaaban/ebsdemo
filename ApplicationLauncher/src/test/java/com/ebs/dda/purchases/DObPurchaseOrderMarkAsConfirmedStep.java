package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderConfirmValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsConfirmedTestUtils;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPurchaseOrderMarkAsConfirmedStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderMarkAsConfirmedTestUtils utils;

  public DObPurchaseOrderMarkAsConfirmedStep() {

    When(
        "^\"([^\"]*)\" requests to MarkAsConfirmed the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String currentDateTime,
            DataTable confrimationDataTable) -> {
          utils.freezeWithTimeZone(currentDateTime);

          String confirmationUrl = utils.confirmRestURL + purchaseOrderCode;

          JsonObject confirmationValueObject =
              utils.prepareCustomerJsonObject(confrimationDataTable);

          Response response =
              utils.sendPostRequest(
                  userActionsTestUtils.getUserCookie(userName),
                  confirmationUrl,
                  confirmationValueObject);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated with ConfirmationDate as follows:$",
        (String purchaseOrderCode, DataTable updatedPurchaseOrderTable) -> {
          utils.assertThatPurchaseOrderIsConfirmed(purchaseOrderCode, updatedPurchaseOrderTable);
        });

    Then(
        "^the following error message is attached to confirmation date field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObPurchaseOrderConfirmValueObject.CONFIRMATION_DATE_TIME);
        });
  }

  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderMarkAsConfirmedTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsConfirmed PO")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsConfirmed PO")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsConfirmed PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsAfterEachScenario());
      databaseConnector.closeConnection();
    }
  }
}
