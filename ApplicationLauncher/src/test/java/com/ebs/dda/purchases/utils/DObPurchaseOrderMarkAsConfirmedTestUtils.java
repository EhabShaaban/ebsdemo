package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderConfirmValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.util.Map;

public class DObPurchaseOrderMarkAsConfirmedTestUtils extends DObPurchaseOrderCommonTestUtils {

  public String confirmRestURL;
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Mark][As][Confirmed]";

  public DObPurchaseOrderMarkAsConfirmedTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    updateRestURLs();
  }

  protected void updateRestURLs() {
    confirmRestURL = "/command/purchaseorder/confirm/";
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public String getDbScriptsAfterEachScenario() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    return str.toString();
  }


  public JsonObject prepareCustomerJsonObject(DataTable confrimationDataTable) {
    Map<String, String> confirmationDataMap =
        confrimationDataTable.asMaps(String.class, String.class).get(0);

    JsonObject confirmationValueObject = new JsonObject();

    addValueToJsonObject(
        confirmationValueObject,
        IDObPurchaseOrderConfirmValueObject.CONFIRMATION_DATE_TIME,
        confirmationDataMap.get(IExpectedKeys.CONFIRMATION_DATE));

    return confirmationValueObject;
  }

  public void assertThatPurchaseOrderIsConfirmed(
      String purchaseOrderCode, DataTable expectedDataTable) {

    Map<String, String> expectedMap = expectedDataTable.asMaps(String.class, String.class).get(0);

    Object[] actualData =
        (Object[])
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getConfirmedPurchaseOrder",
                purchaseOrderCode,
                expectedMap.get(IExpectedKeys.LAST_UPDATED_BY),
                '%' + expectedMap.get(IExpectedKeys.STATE) + '%');

    Assert.assertNotNull(
        ASSERTION_MSG + " There is no Confirmed date for PO with code " + purchaseOrderCode,
        actualData);
    assertDateEquals(getDateTimeInMachineTimeZone(expectedMap.get(IExpectedKeys.LAST_UPDATED_DATE)),
        new DateTime(actualData[0]).withZone(DateTimeZone.UTC));
    assertDateEquals(getDateTimeInMachineTimeZone(expectedMap.get(IExpectedKeys.CONFIRMATION_DATE)),
        new DateTime(actualData[1]).withZone(DateTimeZone.UTC));
  }
}
