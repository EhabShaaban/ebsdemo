package com.ebs.dda.purchases;

public interface IQueries {

  String ItemsInPOWithPOCode =
      "select m.code from ioborderlinedetails ol "
          + "left join doborderdocument o on ol.refinstanceid=o.id AND objectTypeCode LIKE '%_PO' "
          + "left join masterdata m on m.id=ol.itemid "
          + "where o.code like ?";
}
