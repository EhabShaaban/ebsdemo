package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSaveQtyInDnPlTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObPurchaseOrderSaveQtyInDnPlStep extends SpringBootRunner implements En {
  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderSaveQtyInDnPlTestUtils utils;

  public DObPurchaseOrderSaveQtyInDnPlStep() {

    When(
        "^\"([^\"]*)\" saves Qty in DN/PL of Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String itemCode,
            String purchaseOrderCode,
            String dateTime,
            DataTable quantitiesTable) -> {
          utils.freeze(dateTime);
          String url = IPurchaseOrderRestUrls.SAVE_QTY_IN_DN_PL;
          Response response =
              utils.saveQtyInDnPl(
                  url,
                  purchaseOrderCode,
                  itemCode,
                  userActionsTestUtils.getUserCookie(userName),
                  quantitiesTable);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String itemCode, String orderCode, DataTable expectedUpdates) -> {
          utils.assertItemUpdated(itemCode, orderCode, expectedUpdates);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderSaveQtyInDnPlTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Item QtyInDnPl to Local/Import PurchaseOrder,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Save Item QtyInDnPl to Local/Import PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Item QtyInDnPl to Local/Import PurchaseOrder,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
