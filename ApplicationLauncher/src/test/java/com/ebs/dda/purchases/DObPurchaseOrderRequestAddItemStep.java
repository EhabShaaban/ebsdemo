package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderRequestAddItemStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderItemTestUtils utils;

  public DObPurchaseOrderRequestAddItemStep() {

    When(
        "^\"([^\"]*)\" requests to add Item to OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          String lockURL = IPurchaseOrderRestUrls.ADD_ITEM_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new add Item dialoge is opened and OrderItems section of PurchaseOrder with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertResponseSuccessStatus(userActionsTestUtils.getUserResponse(userName));
        });

    Given(
        "^\"([^\"]*)\" first requested to add Item to OrderItems section of PurchaseOrder with code \"([^\"]*)\" successfully$",
        (String userName, String purchaseOrderCode) -> {
          String lockURL = IPurchaseOrderRestUrls.ADD_ITEM_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
        });

    When(
        "^\"([^\"]*)\" requests to cancel add item to OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Response response =
              userActionsTestUtils.unlockSection(
                  username, IPurchaseOrderRestUrls.UNLOCK_ITEM_SECTION + purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to cancel add item to OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String username, String purchaseOrderCode) -> {
          Response response =
              userActionsTestUtils.unlockSection(
                  username, IPurchaseOrderRestUrls.UNLOCK_ITEM_SECTION + purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderItemTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request Add item in Local/Import PurchaseOrder,")
        || scenario.getName().contains("Request Cancel Add item in Local/Import PurchaseOrder,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Request Add item in Local/Import PurchaseOrder,")
        || scenario.getName().contains("Request Cancel Add item in Local/Import PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request Add item in Local/Import PurchaseOrder,")
        || scenario.getName().contains("Request Cancel Add item in Local/Import PurchaseOrder,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
