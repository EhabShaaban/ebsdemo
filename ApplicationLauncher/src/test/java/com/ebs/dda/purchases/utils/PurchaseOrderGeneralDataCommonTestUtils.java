package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class PurchaseOrderGeneralDataCommonTestUtils extends DObPurchaseOrderCommonTestUtils {

  protected static final String VENDOR = "Vendor";
  protected static final String CREATION_DATE_TIME = "CreationDateTime";
  protected static final String PURCHASING_RESPONSIBLE_CODE = "PurchasingResponsibleCode";
  protected static final String PURCHASE_RESPONSIBLE_CODE = "purchaseResponsibleCode";
  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[GeneralData]";

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    return str.toString();
  }

  public void assertThatImportPurchaseOrdersExist(DataTable purchaseOrderTable) throws Exception {
    List<Map<String, String>> purchaseOrderData =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrderData) {
      assertThatPurchaseOrderExistWithGivenStateAndPurchaseUnit(
          purchaseOrder, OrderTypeEnum.IMPORT_PO.name());
    }
  }

  public void assertThatLocalPurchaseOrdersExist(DataTable purchaseOrderTable) throws Exception {
    List<Map<String, String>> purchaseOrderData =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrderData) {
      assertThatPurchaseOrderExistWithGivenStateAndPurchaseUnit(
          purchaseOrder, OrderTypeEnum.LOCAL_PO.name());
    }
  }
}
