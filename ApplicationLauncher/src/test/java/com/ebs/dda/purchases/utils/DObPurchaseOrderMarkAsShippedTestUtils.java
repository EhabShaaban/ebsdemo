package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class DObPurchaseOrderMarkAsShippedTestUtils extends DObPurchaseOrderCommonTestUtils {

  public static final String SHIPPING_DATE = "ShippingDate";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Mark][As][Shipped]";

  public DObPurchaseOrderMarkAsShippedTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");

    return str.toString();
  }

  public Response markAsShipped(
      Cookie loginCookie, String purchaseOrderCode, DataTable shippedDatesDataTable) {
    JsonObject markShippedJsonData =
        getMarkAsShippedJsonObject(purchaseOrderCode, shippedDatesDataTable);
    String restURL = "/command/purchaseorder/markas/shipped";
    return sendPUTRequest(loginCookie, restURL, markShippedJsonData);
  }

  private JsonObject getMarkAsShippedJsonObject(
      String purchaseOrderCode, DataTable shippedDatesDataTable) {
    JsonObject markShippedRequestJsonData = new JsonObject();
    Map<String, String> shippedData =
        shippedDatesDataTable.asMaps(String.class, String.class).get(0);
    markShippedRequestJsonData.addProperty("purchaseOrderCode", purchaseOrderCode);
    markShippedRequestJsonData.addProperty("shippingDate", shippedData.get(SHIPPING_DATE));
    return markShippedRequestJsonData;
  }

  public void assertThatPurchaseOrderShippedDetailsIsUpdated(
      String purchaseOrderCode, DataTable shippedDataTable) {
    Map<String, String> expectedDataMap =
        shippedDataTable.asMaps(String.class, String.class).get(0);
    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderShippingDetails",
            purchaseOrderCode,
            expectedDataMap.get("LastUpdatedBy"));
    assertModificationDataUpdatedValues(expectedDataMap, actualData);
  }

  public void assertThatImportPurchaseOrderWithProductionDateIsExist(DataTable purchaseOrder) {
    Map<String, String> expectedDataMap = purchaseOrder.asMaps(String.class, String.class).get(0);
    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderDetails", expectedDataMap.get("Code"), "%FinishedProduction%");

    assertEquals(
        ASSERTION_MSG + " Production Date is not correct",
        expectedDataMap.get("productionDate"),
        actualData.get(0)[0]);
  }

  public void assertThatLocalPurchaseOrderWithConfirmationDateIsExist(DataTable purchaseOrder) {
    Map<String, String> expectedDataMap = purchaseOrder.asMaps(String.class, String.class).get(0);
    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderDetails", expectedDataMap.get("Code"), "%Confirmed%");

    assertEquals(
        ASSERTION_MSG + " Confirmation Date is not correct",
        expectedDataMap.get("confirmationDate"),
        actualData.get(0)[1]);
  }

  private void assertModificationDataUpdatedValues(
      Map<String, String> expectedDataMap, List<Object[]> actualData) {
    Object[] actualApprover = actualData.get(0);
    String currentState = actualApprover[0].toString();
    assertTrue(
        ASSERTION_MSG + " The State " + expectedDataMap.get("State") + " is not correct",
        currentState.contains(expectedDataMap.get("State")));
    assertDateEquals(
        getDateTimeInMachineTimeZone(expectedDataMap.get("LastUpdateDate")),
        new DateTime(actualApprover[1]).withZone(DateTimeZone.UTC));
    assertDateEquals(
        getDateTimeInMachineTimeZone(expectedDataMap.get("ShippingDate")),
        new DateTime(actualApprover[2]).withZone(DateTimeZone.UTC));
  }
}
