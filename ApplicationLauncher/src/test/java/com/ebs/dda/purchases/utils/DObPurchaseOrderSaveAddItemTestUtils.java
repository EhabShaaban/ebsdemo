package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class DObPurchaseOrderSaveAddItemTestUtils extends DObPurchaseOrderItemTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Save][Add][Item]";

  public DObPurchaseOrderSaveAddItemTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertItemsExistsInPurchaseOrder(
      String objectTypeCode, String purchaseOrderCode, DataTable itemDataTable) throws Exception {
    List<Map<String, String>> itemsData = itemDataTable.asMaps(String.class, String.class);
    for (Map<String, String> itemData : itemsData) {
      assertItemExistsInPurchaseOrder(objectTypeCode, purchaseOrderCode, itemData);
    }
  }

  public void assertItemExistsInPurchaseOrder(
      String objectTypeCode, String purchaseOrderCode, Map<String, String> itemData) {
    IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel =
        (IObOrderLineDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseOrderItemByItemCodeAndPurchaseOrderCodeAndPurchaseOrderType",
                new Object[] {
                  objectTypeCode, purchaseOrderCode, itemData.get(IExpectedKeys.ITEM_CODE)
                });
    assertNotNull(
        ASSERTION_MSG
            + " The Po with code "
            + purchaseOrderCode
            + " with type "
            + objectTypeCode
            + " doesnt have Item with code "
            + itemData.get(IExpectedKeys.ITEM_CODE),
        orderLineDetailsGeneralModel);
    assertItemHasCorrectQty(itemData, orderLineDetailsGeneralModel);
  }

  private void assertItemHasCorrectQty(
      Map<String, String> itemData, IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel) {
    if (!itemData.get(IExpectedKeys.QTYINDN).equals("")) {
      BigDecimal expectedQtyInFloatFormat = new BigDecimal(itemData.get(IExpectedKeys.QTYINDN));
      assertEquals(
          ASSERTION_MSG + " The Expected Qty " + expectedQtyInFloatFormat + " is not correct",
          expectedQtyInFloatFormat,
          orderLineDetailsGeneralModel.getQtyInDn());
    } else {
      assertNull(ASSERTION_MSG + " QtyInDn is Null", orderLineDetailsGeneralModel.getQtyInDn());
    }
  }

  private JsonObject createPurchaseOrderItemJsonObject(DataTable itemDataTable) {
    List<Map<String, String>> itemsData = itemDataTable.asMaps(String.class, String.class);

    JsonObject PurchaseOrderItemObject = new JsonObject();
    for (Map<String, String> itemData : itemsData) {
      if (!itemData.get(IExpectedKeys.ITEM_CODE).equals("")) {
        PurchaseOrderItemObject.addProperty("itemCode", itemData.get(IExpectedKeys.ITEM_CODE));
      } else {
        PurchaseOrderItemObject.add("itemCode", JsonNull.INSTANCE);
      }
      if (!itemData.get(IExpectedKeys.QTYINDN).equals("")) {
        String qtyInDNString = itemData.get(IExpectedKeys.QTYINDN);
        if (isBigDecimal(qtyInDNString)) {
          PurchaseOrderItemObject.addProperty("qtyInDn", new BigDecimal(qtyInDNString));
        } else {
          PurchaseOrderItemObject.addProperty("qtyInDn", qtyInDNString);
        }
      } else {
        PurchaseOrderItemObject.add("qtyInDn", JsonNull.INSTANCE);
      }
    }

    return PurchaseOrderItemObject;
  }

  private boolean isBigDecimal(String value) {
    try {
      new BigDecimal(value);
    } catch (NumberFormatException ex) {
      return false;
    }
    return true;
  }

  public Response savePurchaseOrderItemToItemSection(
      Cookie userCookie, String purchaseOrderCode, DataTable itemData) {
    JsonObject purchaseOrderItem = createPurchaseOrderItemJsonObject(itemData);
    String restURL = IPurchaseOrderRestUrls.UPDATE_ITEM_SECTION + purchaseOrderCode;
    return sendPUTRequest(userCookie, restURL, purchaseOrderItem);
  }

  public void assertThatItemHasNoItemVendorRecord(String itemCode) {
    List itemVendorRecord =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getIVRByItemCode", new Object[] {itemCode});
    assertTrue(
        ASSERTION_MSG + " There is no IVR for this Item with code " + itemCode,
        itemVendorRecord.isEmpty());
  }
}
