package com.ebs.dda.purchases.utils;

import java.util.Map;

public class DObPurchaseOrderViewAllTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View][All]";

  public DObPurchaseOrderViewAllTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  @Override
  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    return str.toString();
  }

  public String getDbScriptsExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument_ViewAll.sql");
    str.append(",").append("db-scripts/purchase-order/DObPurchaseOrder_ViewAll.sql");
    str.append(",").append("db-scripts/purchase-order/IObEnterpriseData_ViewAll.sql");
    return str.toString();
  }

  public String getPreparedViewAllUrl(Integer numOfRecoreds) {
    return "/services/purchase/orders/page=0/rows=" + numOfRecoreds + "/filters=";
  }
}
