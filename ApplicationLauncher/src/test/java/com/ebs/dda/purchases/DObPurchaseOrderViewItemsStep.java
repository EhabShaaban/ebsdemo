package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordCommonTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewItemsTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewItemsWithPricesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderViewItemsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderViewItemsWithPricesTestUtils utils;
  private ItemVendorRecordCommonTestUtils itemVendorRecordTestUtils;

  DObPurchaseOrderViewItemsStep() {

    Given(
        "^the following IVRs exist:$",
        (DataTable itemVendorRecordsList) -> {
          itemVendorRecordTestUtils.assertThatItemVendorRecordsWithoutCreationDateExists(
              itemVendorRecordsList);
        });

    When(
        "^\"([^\"]*)\" requests to view OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String restURL = utils.VIEW_ITEMS_URL + purchaseOrderCode;
          Response response = utils.sendGETRequest(cookie, restURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of OrderItems section are displayed to \"([^\"]*)\":$",
        (String username, DataTable dataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatActualPurchaseOrderItemsAreCorrect(response, dataTable);
        });

    Then(
        "^the following values of Quantities list for Item \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String itemCode, String username, DataTable itemQuantitiesTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatOrderItemQuantitiesAreDisplayedCorrect(
              itemCode, itemQuantitiesTable, response);
        });

    Then(
        "^an empty Quantities list for Item \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String itemCode, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertEmptyResponseOfQuantities(response, itemCode);
        });

    Then(
        "^an empty OrderItems section is displayed to \"([^\"]*)\"$",
        (String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertEmptyResponseOfItems(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderViewItemsWithPricesTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    itemVendorRecordTestUtils = new ItemVendorRecordCommonTestUtils(properties);
    itemVendorRecordTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View OrderItems WithPrices,")) {
      DObPurchaseOrderViewItemsTestUtils.setItemQuantities("quantities");
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getNewItemVendorRecordDataDbScriptPath());
        databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View OrderItems WithPrices,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
