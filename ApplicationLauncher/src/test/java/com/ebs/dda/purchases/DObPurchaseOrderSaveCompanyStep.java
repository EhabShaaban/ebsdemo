package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSaveCompanyTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderSaveCompanyStep extends SpringBootRunner implements En {
  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderSaveCompanyTestUtils utils;

  DObPurchaseOrderSaveCompanyStep() {

    When(
        "^\"([^\"]*)\" saves Company section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String dateTime,
            DataTable companyDataTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePurchaseOrderCompanySection(
                  userCookie, purchaseOrderCode, companyDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
            "^Company section of PurchaseOrder with Code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable companyDataTable) -> {
          utils.assertThatCompanyDataIsUpdated(purchaseOrderCode, companyDataTable);
        });

    Then(
        "^the lock by \"([^\"]*)\" on Company section of PurchaseOrder with Code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.COMPANY_SECTION);
        });

    And(
        "^the following error message is attached to Company field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(response, errorCode, "companyCode");
        });

    And(
        "^the following error message is attached to Bank field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(response, errorCode, "bankCode");
        });
    And(
        "^the following error message is attached to AccountNumber field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(response, errorCode, "accountNumber");
        });

    When(
        "^\"([^\"]*)\" saves Company section of PurchaseOrder with Code \"([^\"]*)\" with the following values:$",
        (String userName, String purchaseOrderCode, DataTable companyDataTable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePurchaseOrderCompanySection(
                  userCookie, purchaseOrderCode, companyDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderSaveCompanyTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Company section")) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Company section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
