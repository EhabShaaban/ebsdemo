package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderCreateValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

public class DObPurchaseOrderCreateTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Create]";

  public DObPurchaseOrderCreateTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  @Override
  public String getDbScriptsOneTimeExecution() {

    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");

    str.append(",").append("db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/item/CObItem.sql");
    str.append(",").append("db-scripts/master-data/item/MObItem.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");

    return str.toString();
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    return str.toString();
  }

  public Response requestCreatePurchaseOrder(Cookie cookie) {
    String restURL = IPurchaseOrderRestUrls.CREATE_PURCHASE_ORDER;
    return sendGETRequest(cookie, restURL);
  }

  public Response createPurchaseOrder(DataTable purchaseOrderDataTable, Cookie cookie) {
    JsonObject purchaseOrder = preparePurchaseOrderJsonObject(purchaseOrderDataTable);
    String restURL = IPurchaseOrderRestUrls.CREATE_PURCHASE_ORDER;
    return sendPostRequest(cookie, restURL, purchaseOrder);
  }

  private JsonObject preparePurchaseOrderJsonObject(DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderMap =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);
    JsonObject purchaseOrderJsonObject = new JsonObject();

    addValueToJsonObject(
        purchaseOrderJsonObject,
        IDObPurchaseOrderCreateValueObject.TYPE,
        purchaseOrderMap.get(IOrderFeatureFileCommonKeys.TYPE));

    addValueToJsonObject(
        purchaseOrderJsonObject,
        IDObPurchaseOrderCreateValueObject.REFERENCE_PURCHASE_ORDER_CODE,
        purchaseOrderMap.get(IOrderFeatureFileCommonKeys.REFERENCE_PURCHASE_ORDER));

    addValueToJsonObject(
        purchaseOrderJsonObject,
        IDObPurchaseOrderCreateValueObject.BUSINESS_UNIT_CODE,
        purchaseOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT));

    addValueToJsonObject(
        purchaseOrderJsonObject,
        IDObPurchaseOrderCreateValueObject.VENDOR_CODE,
        purchaseOrderMap.get(IFeatureFileCommonKeys.VENDOR));

    try {
      purchaseOrderJsonObject.addProperty(
          IDObPurchaseOrderCreateValueObject.DOCUMENT_OWNER_ID,
          NumberFormat.getNumberInstance()
              .parse(purchaseOrderMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID)));
    } catch (ParseException exception) {
      addValueToJsonObject(
          purchaseOrderJsonObject,
          IDObPurchaseOrderCreateValueObject.DOCUMENT_OWNER_ID,
          purchaseOrderMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER_ID));
    }

    return purchaseOrderJsonObject;
  }

  public void assertThatPurchaseOrderIsCreatedSuccessfully(DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderData =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);
    Object[] purchaseOrder = (Object[]) retrievePurchaseOrderData(purchaseOrderData);

    Assert.assertNotNull(
        ASSERTION_MSG + " Purchase order doesn't created successfully", purchaseOrder);
    assertDateEquals(
        purchaseOrderData.get(IFeatureFileCommonKeys.CREATION_DATE),
        new DateTime(purchaseOrder[0]).withZone(DateTimeZone.UTC));

    assertDateEquals(
        purchaseOrderData.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(purchaseOrder[1]).withZone(DateTimeZone.UTC));
  }

  private Object retrievePurchaseOrderData(Map<String, String> purchaseOrderRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedPurchaseOrder", constructCreatedPurchaseOrderQueryParams(purchaseOrderRow));
  }

  private Object[] constructCreatedPurchaseOrderQueryParams(Map<String, String> purchaseOrderRow) {
    return new Object[] {
      purchaseOrderRow.get(IFeatureFileCommonKeys.CODE),
      '%' + purchaseOrderRow.get(IFeatureFileCommonKeys.STATE) + '%',
      purchaseOrderRow.get(IFeatureFileCommonKeys.CREATED_BY),
      purchaseOrderRow.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      purchaseOrderRow.get(IOrderFeatureFileCommonKeys.TYPE),
      purchaseOrderRow.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      purchaseOrderRow.get(IFeatureFileCommonKeys.TOTAL_REMAINING),
    };
  }

  public void assertThatPurchaseOrderVendorIsCreatedSuccessfully(
      String purchaseOrderCode, DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderData =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);
    Object purchaseOrder = retrievePurchaseOrderVendorData(purchaseOrderCode, purchaseOrderData);

    Assert.assertNotNull(ASSERTION_MSG + " Vendor data doesn't exists", purchaseOrder);
  }

  public void assertThatPurchaseOrderCompanyIsCreatedSuccessfully(
      String purchaseOrderCode, DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderData =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);
    Object purchaseOrder = retrievePurchaseOrderCompanyData(purchaseOrderCode, purchaseOrderData);

    Assert.assertNotNull(ASSERTION_MSG + " Company data doesn't exists", purchaseOrder);
  }

  private Object retrievePurchaseOrderCompanyData(
      String purchaseOrderCode, Map<String, String> purchaseOrderRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedPurchaseOrderCompanyData",
        constructCreatedPurchaseOrderCompanyDataQueryParams(purchaseOrderCode, purchaseOrderRow));
  }

  private Object retrievePurchaseOrderVendorData(
      String purchaseOrderCode, Map<String, String> purchaseOrderRow) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getCreatedPurchaseOrderVendorData",
        constructCreatedPurchaseOrderVendorDataQueryParams(purchaseOrderCode, purchaseOrderRow));
  }

  private Object[] constructCreatedPurchaseOrderVendorDataQueryParams(
      String purchaseOrderCode, Map<String, String> purchaseOrderRow) {
    return new Object[] {
      purchaseOrderRow.get(IFeatureFileCommonKeys.REFERENCE_PO),
      purchaseOrderCode,
      purchaseOrderRow.get(IFeatureFileCommonKeys.VENDOR)
    };
  }

  private Object[] constructCreatedPurchaseOrderCompanyDataQueryParams(
      String purchaseOrderCode, Map<String, String> purchaseOrderRow) {
    return new Object[] {
      purchaseOrderRow.get(IFeatureFileCommonKeys.COMPANY),
      purchaseOrderCode,
      purchaseOrderRow.get(IFeatureFileCommonKeys.BUSINESS_UNIT)
    };
  }

  public void assertThatServicePurchaseOrderTaxesIsCreatedSuccessfully(
      String purchaseOrderCode, DataTable taxesDataTable) {
    List<Map<String, String>> taxesData = taxesDataTable.asMaps(String.class, String.class);
    taxesData.forEach(
        expectedTax -> {
          Object purchaseOrder = retrievePurchaseOrderTaxesData(purchaseOrderCode, expectedTax);
          Assert.assertNotNull(
              ASSERTION_MSG + "Tax " + expectedTax + " doesn't exist.", purchaseOrder);
        });
  }

  private Object retrievePurchaseOrderTaxesData(
      String purchaseOrderCode, Map<String, String> taxesData) {
    return entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
        "getIObOrderTax",
        constructCreatedPurchaseOrderTaxesDataQueryParams(purchaseOrderCode, taxesData));
  }

  private Object[] constructCreatedPurchaseOrderTaxesDataQueryParams(
      String purchaseOrderCode, Map<String, String> taxesData) {
    return new Object[] {
      purchaseOrderCode,
      taxesData.get(IAccountingFeatureFileCommonKeys.TAX),
      taxesData.get(IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE),
      taxesData.get(IAccountingFeatureFileCommonKeys.TAX_AMOUNT)
    };
  }

  public void assertThatTaxesSummaryIsDisplayedEmpty(String purchaseOrderCode) {
    List orderTaxes =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getIObOrderTaxByCode", purchaseOrderCode);
    Assert.assertEquals(
        ASSERTION_MSG + " PO " + purchaseOrderCode + " has taxes.", 0, orderTaxes.size());
  }
}
