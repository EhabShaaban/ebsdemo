package com.ebs.dda.purchases.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import java.util.Map;

public class DObServicePurchaseOrderRequestEditPaymentDetailsTestUtils
    extends DObPurchaseOrderCommonTestUtils {

  public String ASSERTION_MSG =
      super.ASSERTION_MSG + "[RequestEdit/Cancel][PaymentDetails][Service]";

  public DObServicePurchaseOrderRequestEditPaymentDetailsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public String getLockPaymentDetailsUrl(String servicePOCode) {
    return IPurchaseOrderRestUrls.COMMAND_PURCHASE_ORDER
        + servicePOCode
        + IPurchaseOrderRestUrls.LOCK_SERVICE_PAYMENT_DETAILS_SECTION;
  }

  public String getUnLockPaymentDetailsUrl(String servicePOCode) {
    return IPurchaseOrderRestUrls.COMMAND_PURCHASE_ORDER
        + servicePOCode
        + IPurchaseOrderRestUrls.UNLOCK_SERVICE_PAYMENT_DETAILS_SECTION;
  }
}
