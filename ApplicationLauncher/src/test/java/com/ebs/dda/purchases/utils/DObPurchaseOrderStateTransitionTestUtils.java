package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.ebs.dda.jpa.order.IObOrderDocumentRequiredDocumentsGeneralModel;
import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPaymentAndDeliveryGeneralModel;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

public class DObPurchaseOrderStateTransitionTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[State][Transition]";

  public DObPurchaseOrderStateTransitionTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public void assertThatPurchaseOrderHeaderHasTheFollowingValues(
      String purchaseOrderCode, DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderData =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);
    Object purchaseOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPurchaseOrderByHeaderData",
            purchaseOrderCode,
            purchaseOrderData.get("PurchasingResponsible"),
            purchaseOrderData.get("Vendor"),
            purchaseOrderData.get("PurchasingUnit"),
            purchaseOrderData.get("Type"));
    assertNotNull(
        ASSERTION_MSG
            + " There is no Header matches this info for the PO with code "
            + purchaseOrderCode,
        purchaseOrder);
  }

  public void assertThatPurchaseOrdersExistWithCompanyData(
      DataTable purchaseOrdersTable, String purchaseOrderCode) {

    Map<String, String> companyMap = purchaseOrdersTable.asMaps(String.class, String.class).get(0);

    Object company =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCompanyByPOCode", getCompanyQueryParams(companyMap, purchaseOrderCode));

    assertNotNull(company);
  }

  private Object[] getCompanyQueryParams(Map<String, String> companyMap, String purchaseOrderCode) {
    return new Object[] {
      companyMap.get("Company"),
      companyMap.get("Bank"),
      companyMap.get("AccountNumber"),
      purchaseOrderCode,
    };
  }

  public void assertThatPurchaseOrdersExistWithConsigneeData(
      String purchaseOrderCode, DataTable purchaseOrdersTable) {
    Map<String, String> consigneeMap =
        purchaseOrdersTable.asMaps(String.class, String.class).get(0);

    Object consignee =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getConsigneeByPOCode", getConsigneeQueryParams(consigneeMap, purchaseOrderCode));

    assertNotNull(consignee);
  }

  private Object[] getConsigneeQueryParams(
      Map<String, String> consigneeMap, String purchaseOrderCode) {
    return new Object[] {
      consigneeMap.get("Consignee"),
      consigneeMap.get("Plant"),
      consigneeMap.get("Storehouse"),
      purchaseOrderCode
    };
  }

  public void assertThatPurchaseOrderHasRequiredDocuments(
      String purchaseOrderCode, DataTable requiredDocumentsTable) {
    List<Map<String, String>> requiredDocumentData =
        requiredDocumentsTable.asMaps(String.class, String.class);
    assertThatPurchaseOrderHasRequiredDocumentWithCodeAndCopies(
        purchaseOrderCode, requiredDocumentData);
  }

  private void assertThatPurchaseOrderHasRequiredDocumentWithCodeAndCopies(
      String purchaseOrderCode, List<Map<String, String>> expectedRequiredDocs) {
    List<IObOrderDocumentRequiredDocumentsGeneralModel> actualPurchaseOrderRequiredDocs =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderRequiredDocuments", purchaseOrderCode);
    assertTrue(
        ASSERTION_MSG
            + " The number of Required Documents for PO with code "
            + purchaseOrderCode
            + " is not correct",
        actualPurchaseOrderRequiredDocs.size() == expectedRequiredDocs.size());
    int index = 0;
    for (Map<String, String> expectedDoc : expectedRequiredDocs) {
      IObOrderDocumentRequiredDocumentsGeneralModel orderRequiredDocument =
          actualPurchaseOrderRequiredDocs.get(index);
      assertEquals(
          ASSERTION_MSG
              + " Document Type "
              + expectedDoc.get(DOCUMENT_TYPE)
              + " for PO with code "
              + purchaseOrderCode
              + " is not correct",
          expectedDoc.get(DOCUMENT_TYPE),
          orderRequiredDocument.getAttachmentCode());
      assertEquals(
          ASSERTION_MSG
              + " The number of Copies "
              + expectedDoc.get(COPIES)
              + " for PO with code "
              + purchaseOrderCode
              + " is not correct",
          expectedDoc.get(COPIES),
          orderRequiredDocument.getNumberOfCopies().toString());
      index++;
    }
  }

  public void assertThatPurchaseOrderIsUpdated(
      String purchaseOrderCode, DataTable purchaseOrderData) {
    Map<String, String> purchaseOrderMap =
        purchaseOrderData.asMaps(String.class, String.class).get(0);

    Object purchaseOrderModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedPurchaseOrderAfterStateChange",
            getUpdatedPurchaseOrderQueryParams(purchaseOrderMap, purchaseOrderCode));

    assertNotNull("", purchaseOrderModifiedDate);
    assertDateEquals(
        purchaseOrderMap.get(UPDATED_DATE),
        new DateTime(purchaseOrderModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] getUpdatedPurchaseOrderQueryParams(
      Map<String, String> purchaseOrderMap, String purchaseOrderCode) {
    return new Object[] {
      purchaseOrderCode, purchaseOrderMap.get(UPDATED_BY), "%" + purchaseOrderMap.get(STATE) + "%"
    };
  }

  public void assertThatPurchaseOrderExistWithPaymentAndDeliveryData(
      String purchaseOrderCode, DataTable paymentAndDeliveryTable) {

    Map<String, String> paymentAndDeliveryMap =
        paymentAndDeliveryTable.asMaps(String.class, String.class).get(0);

    paymentAndDeliveryMap = convertEmptyStringsToNulls(paymentAndDeliveryMap);
    DObPaymentAndDeliveryGeneralModel paymentAndDelivary =
        (DObPaymentAndDeliveryGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseOrderPaymentAndDelivary", purchaseOrderCode);
    assertNotNull(
        ASSERTION_MSG
            + " Payment And Delivary data is not exist for Po with code "
            + purchaseOrderCode,
        paymentAndDelivary);
    assertEquals(
        ASSERTION_MSG
            + " Incoterm "
            + paymentAndDeliveryMap.get(IExpectedKeys.INCOTERM)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.INCOTERM),
        paymentAndDelivary.getIncotermCode());
    assertEquals(
        ASSERTION_MSG
            + " Currency "
            + paymentAndDeliveryMap.get(IExpectedKeys.CURRENCY)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.CURRENCY),
        paymentAndDelivary.getCurrencyCode());
    assertEquals(
        ASSERTION_MSG
            + " Payment Terms "
            + paymentAndDeliveryMap.get(IExpectedKeys.PAYMENT_TERMS)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.PAYMENT_TERMS),
        paymentAndDelivary.getPaymentTermCode());
    assertEquals(
        ASSERTION_MSG
            + " Shipping Instructions "
            + paymentAndDeliveryMap.get(IExpectedKeys.SHIPPING_INSTRUCTIONS)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.SHIPPING_INSTRUCTIONS),
        paymentAndDelivary.getShippingInstructionsCode());
    assertEquals(
        ASSERTION_MSG
            + " Containers No "
            + paymentAndDeliveryMap.get(IExpectedKeys.CONTAINERS_NO)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.CONTAINERS_NO),
        getValue(paymentAndDelivary.getContainersNo()));
    assertEquals(
        ASSERTION_MSG
            + " Containers Type "
            + paymentAndDeliveryMap.get(IExpectedKeys.CONTAINERS_TYPE)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.CONTAINERS_TYPE),
        paymentAndDelivary.getContainerCode());
    assertEquals(
        ASSERTION_MSG
            + " Collection Date "
            + paymentAndDeliveryMap.get(IExpectedKeys.COLLECTION_DATE)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.COLLECTION_DATE),
        formatDate(paymentAndDelivary.getCollectionDate()));
    assertEquals(
        ASSERTION_MSG
            + " Number of Days "
            + paymentAndDeliveryMap.get(IExpectedKeys.NUMBER_OF_DAYS)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.NUMBER_OF_DAYS),
        getValue(paymentAndDelivary.getNumberOfDays()));
    assertEquals(
        ASSERTION_MSG
            + " Mode Of Transport "
            + paymentAndDeliveryMap.get(IExpectedKeys.MODE_OF_TRANSPORT)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.MODE_OF_TRANSPORT),
        paymentAndDelivary.getTransportModeCode());
    assertEquals(
        ASSERTION_MSG
            + " Loading Port "
            + paymentAndDeliveryMap.get(IExpectedKeys.LOADING_PORT)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.LOADING_PORT),
        paymentAndDelivary.getLoadingPortCode());
    assertEquals(
        ASSERTION_MSG
            + " Discharge Port "
            + paymentAndDeliveryMap.get(IExpectedKeys.DISCHARGE_PORT)
            + " is not correct for Po with code "
            + purchaseOrderCode,
        paymentAndDeliveryMap.get(IExpectedKeys.DISCHARGE_PORT),
        paymentAndDelivary.getDischargePortCode());
  }

  public String getValue(Object value) {
    return value != null ? value.toString() : null;
  }

  private String formatDate(String dateToFormat) {
    String expectedDate = null;
    if (dateToFormat != null) {
      DateTime dateInDatabaseFormat =
          DateTimeFormat.forPattern("yyyy-M-dd HH:mm:ss.S").parseDateTime(dateToFormat);
      expectedDate = dateInDatabaseFormat.toString("dd-MMM-yyyy");
    }
    return expectedDate;
  }
}
