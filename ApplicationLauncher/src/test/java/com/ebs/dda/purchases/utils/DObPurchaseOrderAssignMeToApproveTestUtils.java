package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderAssignMeToApproveValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static com.ebs.dda.purchases.IFeatureFileKeys.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DObPurchaseOrderAssignMeToApproveTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Assign][Me][To][Approve]";

  public DObPurchaseOrderAssignMeToApproveTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  @Override
  public String getDbScriptsOneTimeExecution() {
    String dbScriptsOneTimeExecution = super.getDbScriptsOneTimeExecution();
    StringBuilder str = new StringBuilder();
    str.append(dbScriptsOneTimeExecution);
    str.append("," + "db-scripts/CObApprovalPoliciesSqlScript.sql");

    return str.toString();
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    return str.toString();
  }

  public Response assignMeToApprovePurchaseOrder(
      Cookie cookie, String purchaseOrderCode, String mainUser, String controlPointCode) {
    JsonObject jsonData = new JsonObject();
    jsonData.addProperty(
        IDObPurchaseOrderAssignMeToApproveValueObject.PURCHASE_ORDER_CODE, purchaseOrderCode);
    jsonData.addProperty(IDObPurchaseOrderAssignMeToApproveValueObject.MAIN_USER, mainUser);
    jsonData.addProperty(
        IDObPurchaseOrderAssignMeToApproveValueObject.CONTROL_POINT_CODE, controlPointCode);
    return sendPostRequest(cookie, IPurchaseOrderRestUrls.ASSIGN_ME_TO_APPROVE_PO, jsonData);
  }

  public Response assignMeToApprovePurchaseOrder(Cookie cookie, String purchaseOrderCode) {
    JsonObject jsonData = new JsonObject();
    jsonData.addProperty(
        IDObPurchaseOrderAssignMeToApproveValueObject.PURCHASE_ORDER_CODE, purchaseOrderCode);
    return sendPostRequest(cookie, IPurchaseOrderRestUrls.ASSIGN_ME_TO_APPROVE_PO, jsonData);
  }

  public void assertThatControlPointUsersHasAnAlternativeUsers(DataTable usersAndAlternatives) {
    List<Map<String, String>> usersAndAlternativesMap =
        usersAndAlternatives.asMaps(String.class, String.class);

    for (Map<String, String> userAndAlternative : usersAndAlternativesMap) {
      assertThatMainUserExistsForControlPoint(userAndAlternative);
      assertThatAlternativeUserExistsForControlPoint(userAndAlternative);
    }
  }

  public String getDecisionValue(String decisionValue) {
    return decisionValue != null ? decisionValue.toUpperCase() : null;
  }

  public void assertThatApprovalCycleDetailsAreCorrect(
      String approvalCycleNum, String purchaseOrderCode, DataTable approvalCycleDetails) {
    List<Map<String, String>> approversList =
        approvalCycleDetails.asMaps(String.class, String.class);
    List<Object[]> approvalCycleDetailsList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderApprovalCyclesDetails",
            purchaseOrderCode,
            Long.valueOf(approvalCycleNum));
    assertTrue(
        ASSERTION_MSG + " ApprovalCycleDetailsList is not correct",
        approvalCycleDetailsList.size() == approversList.size());
    int index = 0;
    for (Map<String, String> expectedApprover : approversList) {
      expectedApprover = convertEmptyStringsToNulls(expectedApprover);
      Object[] actualApprover = approvalCycleDetailsList.get(index);
      assertEquals(
          ASSERTION_MSG + " Control point for approver is not correct",
          expectedApprover.get(CONTROL_POINT),
          actualApprover[0]);
      assertEquals(
          ASSERTION_MSG + " the approver is not correct",
          expectedApprover.get(USER),
          actualApprover[1]);
      DateTime actualDecisionTime =
          actualApprover[2] != null
              ? new DateTime(actualApprover[2]).withZone(DateTimeZone.UTC)
              : null;
      assertDateEquals(expectedApprover.get(DATE_TIME), actualDecisionTime);
      assertEquals(
          ASSERTION_MSG + " the Decision is not correct",
          getDecisionValue(expectedApprover.get(DECISION)),
          actualApprover[3]);
      assertEquals(expectedApprover.get(NOTES), actualApprover[4]);
      index++;
    }
  }

  private void assertThatMainUserExistsForControlPoint(Map<String, String> userAndAlternative) {
    Object mainUserId =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUserIdByUserName", userAndAlternative.get(MAIN_USER));

    Object mainUser =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getMainAndAlternativeUserOfControlPoint",
            userAndAlternative.get(APPROVAL_POLICY),
            userAndAlternative.get(CONTROL_POINT),
            mainUserId,
            true);

    Assert.assertNotNull(ASSERTION_MSG + " this user is not the main user", mainUser);
  }

  private void assertThatAlternativeUserExistsForControlPoint(
      Map<String, String> userAndAlternative) {
    Object alternativeUserId =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUserIdByUserName", userAndAlternative.get(ALTERNATIVE_USER));

    Object alternativeUser =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getMainAndAlternativeUserOfControlPoint",
            userAndAlternative.get(APPROVAL_POLICY),
            userAndAlternative.get(CONTROL_POINT),
            alternativeUserId,
            false);

    Assert.assertNotNull(ASSERTION_MSG + " this user is not the alternative user", alternativeUser);
  }
}
