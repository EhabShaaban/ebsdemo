package com.ebs.dda.purchases.utils;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderCompanyTestUtils extends DObPurchaseOrderCommonTestUtils {

  protected final String COMPANY_CODE = "Company";
  protected final String BANK_CODE = "Bank";
  protected final String ACCOUNT_NUMBER = "AccountNumber";

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Company]";

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    return str.toString();
  }
}
