package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObPurchaseOrderDocumentValueObject;
import com.ebs.dda.purchases.utils.IObPurchaseOrderSaveRequiredDocumentsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObPurchaseOrderSaveRequiredDocumentsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObPurchaseOrderSaveRequiredDocumentsTestUtils utils;

  IObPurchaseOrderSaveRequiredDocumentsStep() {

    Given(
        "^the PurchaseOrder with code \"([^\"]*)\" has the following RequiredDocuments data:$",
        (String purchaseOrderCode, DataTable requiredDocumentsTable) -> {
          utils.assertThatPurchaseOrderHasRequiredDocuments(
              purchaseOrderCode, requiredDocumentsTable);
        });

    Given(
        "^the following RequiredDocuments exist:$",
        (DataTable requiredDocumentsTable) -> {
          utils.assertThatDocumentTypesExist(requiredDocumentsTable);
        });

    Given(
        "^RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          Response response =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_REQUIRED_DOCUMENTS_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\"  is updated as follows:$",
        (String purchaseOrderCode, DataTable requiredDocumentsTable) -> {
          utils.assertThatPurchaseOrderHasRequiredDocuments(
              purchaseOrderCode, requiredDocumentsTable);
        });

    Then(
        "^RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\"  is empty$",
        (String purchaseOrderCode) -> {
          utils.assertThatPurchaseOrderRequiredDocumentsIsEmpty(purchaseOrderCode);
        });
    When(
        "^\"([^\"]*)\" saves RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String dateTime,
            DataTable requiredDocumentsTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePurchaseOrderRequiredDocumentsSection(
                  userCookie, purchaseOrderCode, requiredDocumentsTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" saves RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with empty values$",
        (String userName, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.saveEmptyPurchaseOrderRequiredDocumentsSection(userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following error message is attached to DocumentType field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String msgCode, String userName) -> {
          Response userResponse = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              userResponse, msgCode, IIObPurchaseOrderDocumentValueObject.DOCUMENT_TYPE_CODE);
        });
    When(
        "^\"([^\"]*)\" saves RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\" with the following values:$",
        (String userName, String purchaseOrderCode, DataTable requiredDocumentsTable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePurchaseOrderRequiredDocumentsSection(
                  userCookie, purchaseOrderCode, requiredDocumentsTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.REQUIRED_DOCUMENTS_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObPurchaseOrderSaveRequiredDocumentsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save RequiredDocument section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Save RequiredDocument section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save RequiredDocument section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
