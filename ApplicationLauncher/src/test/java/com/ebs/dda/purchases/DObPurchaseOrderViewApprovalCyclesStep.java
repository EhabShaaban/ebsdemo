package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewApprovalCyclesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderViewApprovalCyclesStep extends SpringBootRunner implements En {
  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderViewApprovalCyclesTestUtils approvalCyclesTestUtils;

  public DObPurchaseOrderViewApprovalCyclesStep() {

    Given(
        "^the following ApprovalCycles section exist in PurchaseOrder with code \"([^\"]*)\":$",
        (String purchaseOrderCode, DataTable approvalCyclesDataTable) -> {
          approvalCyclesTestUtils.assertThatPurchaseOrderApprovalCyclesExist(
              purchaseOrderCode, approvalCyclesDataTable);
        });

    Given(
        "^the following PurchaseOrders has an empty ApprovalCycles section:$",
        (DataTable purchaseOrdersDataTable) -> {
          approvalCyclesTestUtils.assertThatPurchaseOrderHasNoApprovalCycles(
              purchaseOrdersDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to view ApprovalCycles section of PurchaseOrder with code \"([^\"]*)\"$",
        (String username, String purchaseOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response readResponse =
              approvalCyclesTestUtils.readPurchaseOrderApprovalCycles(
                  userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, readResponse);
        });

    Then(
        "^an empty ApprovalCycles section is displayed to \"([^\"]*)\"$",
        (String username) -> {
          Response readResponse = userActionsTestUtils.getUserResponse(username);
          approvalCyclesTestUtils.assertResponseSuccessStatus(readResponse);
          approvalCyclesTestUtils.assertThatReponseApprovalCyclesDataIsEmpty(readResponse);
        });

    Then(
        "^the following values of ApprovalCycles section are displayed to \"([^\"]*)\":$",
        (String username, DataTable expectedApprovalCyclesDetails) -> {
          Response readResponse = userActionsTestUtils.getUserResponse(username);
          approvalCyclesTestUtils.assertResponseSuccessStatus(readResponse);
          approvalCyclesTestUtils.assertThatResponseApprovalCyclesDataIsCorrect(
              expectedApprovalCyclesDetails, readResponse);
        });

    Then(
        "^the following details of ApprovalCycleNo \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String approvalCycleNum, String username, DataTable expectedApproversDetails) -> {
          Response readResponse = userActionsTestUtils.getUserResponse(username);
          approvalCyclesTestUtils.assertThatResponseApprovalCycleDetailsDataIsCorrect(
              approvalCycleNum, expectedApproversDetails, readResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    approvalCyclesTestUtils = new DObPurchaseOrderViewApprovalCyclesTestUtils(getProperties());
    approvalCyclesTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View ApprovalCycles section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(approvalCyclesTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(approvalCyclesTestUtils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View ApprovalCycles section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(approvalCyclesTestUtils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
