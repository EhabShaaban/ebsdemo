package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderDeleteItemStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderItemTestUtils utils;

  public DObPurchaseOrderDeleteItemStep() {

    When(
        "^\"([^\"]*)\" requests to delete Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          Cookie loginCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.deleteItemInPurchaseOrder(loginCookie, purchaseOrderCode, itemCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^an empty Item section of PurchaseOrder with code \"([^\"]*)\" is displayed to \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertEmptyItemSectionInPurchaseOrder(purchaseOrderCode);
        });

    Given(
        "^first \"([^\"]*)\" opens OrderItems section of PurchaseOrder with code \"([^\"]*)\" in edit mode$",
        (String userName, String purchaseOrderCode) -> {
          String lockURL = IPurchaseOrderRestUrls.ADD_ITEM_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderItemTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete Item from Import/Local PO,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Delete Item from Import/Local PO,")) {
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete Item from Import/Local PO,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
