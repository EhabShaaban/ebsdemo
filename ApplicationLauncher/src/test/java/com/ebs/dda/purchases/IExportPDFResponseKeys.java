package com.ebs.dda.purchases;

public interface IExportPDFResponseKeys {

  String MODE_OF_TRANSPORT_NAME = "modeOfTransportName";
  String COLLECTION_DATE = "collectionDate";
  String PAYMENT_TERM_NAME = "paymentTermName";
  String DISCHARGE_PORT_NAME = "dischargePortName";
  String INCOTERM_NAME = "incotermName";
  String VENDOR_NAME = "vendorName";
  String CONSIGNEE_FAX = "consigneeFax";
  String CONSIGNEE_TELEPHONE = "consigneeTelephone";
  String CONSIGNEE_POSTAL_CODE = "consigneePostalCode";
  String CONSIGNEE_CITY_NAME = "consigneeCityName";
  String CONSIGNEE_COUNTRY_NAME = "consigneeCountryName";
  String CONSIGNEE_ADDRESS = "consigneeAddress";
  String CONSIGNEE_NAME = "consigneeName";
  String COMPANY_FAX = "companyFax";
  String COMPANY_TELEPHONE = "companyTelephone";
  String COMPANY_POSTAL_CODE = "companyPostalCode";
  String COMPANY_CITY_NAME = "companyCityName";
  String COMPANY_COUNTRY_NAME = "companyCountryName";
  String COMPANY_ADDRESS = "companyAddress";
  String COMPANY_NAME = "companyName";
  String COPIES = "numberOfCopies";
  String ATTACHMENT_NAME = "attachmentName";
  String PURCHASE_RESPONSIBLE_NAME = "purchaseResponsibleName";
  String SHIPPING_INSTRUCTIONS_TEXT = "shippingInstructionsText";
}
