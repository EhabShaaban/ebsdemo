package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObServicePurchaseOrderDeleteItemsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObServicePurchaseOrderDeleteItemsStep extends SpringBootRunner implements En {

  private DObServicePurchaseOrderDeleteItemsTestUtils utils;

  public DObServicePurchaseOrderDeleteItemsStep() {

    When(
        "^\"([^\"]*)\" requests to delete SPOItem with id (\\d+) in PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, Integer itemId, String servicePOCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteServicePurchaseOrderItemRestUrl(servicePOCode, itemId);
          Response response = utils.sendDeleteRequest(userCookie, restUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^SPOItem with id (\\d+) from PurchaseOrder with code \"([^\"]*)\" is deleted$",
        (Integer itemId, String servicePOCode) -> {
          utils.assertThatServicePurchaseOrderItemIsDeletedSuccessfully(servicePOCode, itemId);
        });

    Then(
        "^Only the following Items remaining in PurchaseOrder with code \"([^\"]*)\":$",
        (String servicePOCode, DataTable itemDataTable) -> {
          utils.assertThatServicePurchaseOrderItemDataExists(itemDataTable);
          utils.assertThatTheseAreTheOnlyItemsExist(servicePOCode, itemDataTable);
        });

    Given(
        "^\"([^\"]*)\" first deleted SPOItem with id (\\d+) of PurchaseOrder with code \"([^\"]*)\" successfully$",
        (String userName, Integer itemId, String servicePOCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          String restUrl = utils.getDeleteServicePurchaseOrderItemRestUrl(servicePOCode, itemId);
          Response response = utils.sendDeleteRequest(userCookie, restUrl);
          utils.assertThatServicePurchaseOrderItemIsDeletedSuccessfully(servicePOCode, itemId);
          utils.assertResponseSuccessStatus(response);
        });

    Given(
        "^\"([^\"]*)\" first opened Items section of PurchaseOrder with code \"([^\"]*)\" in edit mode$",
        (String userName, String servicePOCode) -> {
          String lockURL = utils.getLockItemsURL(servicePOCode);
          Response response = userActionsTestUtils.lockSection(userName, lockURL, servicePOCode);
          utils.assertResponseSuccessStatus(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObServicePurchaseOrderDeleteItemsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete SPOItem from PurchaseOrder,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Delete SPOItem from PurchaseOrder,")) {
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
