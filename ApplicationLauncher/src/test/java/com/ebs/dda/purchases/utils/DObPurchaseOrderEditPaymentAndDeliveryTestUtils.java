package com.ebs.dda.purchases.utils;

import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderEditPaymentAndDeliveryTestUtils
    extends DObPurchaseOrderPaymentAndDeliveryTestUtils {

  public DObPurchaseOrderEditPaymentAndDeliveryTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    return str.toString();
  }
}
