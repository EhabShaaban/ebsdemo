package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.IObServicePurchaseOrderSavePaymentDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObServicePurchaseOrderSavePaymentDetailsStep extends SpringBootRunner
    implements En {

  private IObServicePurchaseOrderSavePaymentDetailsTestUtils utils;

  public IObServicePurchaseOrderSavePaymentDetailsStep() {

    When(
        "^\"([^\"]*)\" saves PaymentDetails section of Service PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String servicePOCode, String dateTime, DataTable paymentDetailsDataTable) -> {
          utils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePaymentDetails(servicePOCode, paymentDetailsDataTable, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String servicePOCode, DataTable paymentDetailsDataTable) -> {
          utils.assertThatPaymentDetailsIsUpdatedAsFollows(servicePOCode, paymentDetailsDataTable);
        });

    When(
        "^\"([^\"]*)\" saves PaymentDetails section of Service PurchaseOrder with Code \"([^\"]*)\" with the following values:$",
        (String userName, String servicePOCode, DataTable paymentDetailsDataTable) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePaymentDetails(servicePOCode, paymentDetailsDataTable, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObServicePurchaseOrderSavePaymentDetailsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save SPO PaymentDetails section,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save SPO PaymentDetails section,")) {
      utils.unfreeze();
    }
  }
}
