package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IExportPDFResponseKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DObPurchaseOrderExportPDFFooterTestUtils extends DObPurchaseOrderExportPDFTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Export][PDF][Footer]";
  public DObPurchaseOrderExportPDFFooterTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public void assertThatRequiredDocumentsResponseDataIsCorrect(
      DataTable requiredDocuments, Response response) throws Exception {
    List<Map<String, String>> expectedRequiredDocuments =
        requiredDocuments.asMaps(String.class, String.class);
    List<Map<String, Object>> actualRequiredDocuments =
        response.body().jsonPath().getList("data.requiredDocuments");
    int matchedDocs = 0;
    for (Map<String, Object> actualDoc : actualRequiredDocuments) {
      for (Map<String, String> expectedDoc : expectedRequiredDocuments) {
        if (getEnglishValue(actualDoc.get(IExportPDFResponseKeys.ATTACHMENT_NAME))
            .equals(expectedDoc.get(IFeatureFileCommonKeys.NAME))) {
          assertEquals(ASSERTION_MSG + " Copies are not correct",
              expectedDoc.get(IFeatureFileCommonKeys.COPIES),
              actualDoc.get(IExportPDFResponseKeys.COPIES).toString());
          matchedDocs++;
          break;
        }
      }
    }
    assertEquals(ASSERTION_MSG + " Document size is not correct", expectedRequiredDocuments.size(), matchedDocs);
  }

  public void assertThatFooterDataIsCorrect(DataTable footerDataTable, Response response)
      throws Exception {
    Map<String, String> expectedFooterData =
        footerDataTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualResponseData = response.body().jsonPath().get("data.header");
    assertEquals(
        expectedFooterData.get(IFeatureFileCommonKeys.SHIPPING_INSTRUCTIONS),
        getEnglishValue(actualResponseData.get(IExportPDFResponseKeys.SHIPPING_INSTRUCTIONS_TEXT)));
    assertEquals(
        expectedFooterData.get(IFeatureFileCommonKeys.PURCHASING_RESPONSIBLE),
        actualResponseData.get(IExportPDFResponseKeys.PURCHASE_RESPONSIBLE_NAME));
  }
}
