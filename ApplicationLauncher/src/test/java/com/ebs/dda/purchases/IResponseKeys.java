package com.ebs.dda.purchases;

public interface IResponseKeys {

  String DOCUMENT_TYPE_CODE = "documentTypeCode";
  String ATTACHMENT_TYPE_NAME = "attachmentTypeName";

  String ATTACHMENT_NAME = "attachmentName";
  String CREATION_INFO = "creationInfo";
  String CREATION_DATE = "creationDate";

  String RESPONSE_NUMBER_OF_COPIES = "numberOfCopies";
  String RESPONSE_ATTACHMENT_NAME = "attachmentName";

  String INCOTERM = "incotermName";
  String CURRENCY = "currency";
  String PAYMENT_TERM = "paymentTermName";
  String SHIPPING_INSTRUCTIONS = "shippingInstructions";
  String CONTAINER_TYPE = "containerName";
  String TRANSPORT_MODE = "modeOfTransport";
  String LOADING_PORT = "loadingPortName";
  String DISCHARGE_PORT = "dischargePortName";
  String COLLECTION_DATE = "collectionDate";
  String CONTAINER_NO = "containersNo";
}
