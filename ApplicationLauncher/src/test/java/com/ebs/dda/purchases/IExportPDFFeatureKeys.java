package com.ebs.dda.purchases;

public interface IExportPDFFeatureKeys {

  String FAX = "Fax";
  String TELEPHONE = "Telephone";
  String CITY = "City";
  String COUNTRY = "Country";
  String POSTAL_CODE = "PostalCode";
  String ADDRESS = "Address";
  String MODE_OF_TRANSPORT = "ModeOfTransport";
  String COLLECTION_DATE = "CollectionDate";
  String PAYMENT_TERMS = "PaymentTerms";
  String DISCHARGE_PORT = "DischargePort";
  String INCOTERM = "Incoterm";
  String VENDOR = "Vendor";
  String CONSIGNEE_FAX = "ConsigneeFax";
  String CONSIGNEE_TELEPHONE = "ConsigneeTelephone";
  String CONSIGNEE_POSTAL_CODE = "ConsigneePostalCode";
  String CONSIGNEE_CITY = "ConsigneeCity";
  String CONSIGNEE_COUNTRY = "ConsigneeCountry";
  String CONSIGNEE_ADDRESS = "ConsigneeAddress";
  String CONSIGNEE = "Consignee";
  String COMPANY_FAX = "CompanyFax";
  String COMPANY_TELEPHONE = "CompanyTelephone";
  String COMPANY_POSTAL_CODE = "CompanyPostalCode";
  String COMPANY_CITY = "CompanyCity";
  String COMPANY_COUNTRY = "CompanyCountry";
  String COMPANY_ADDRESS = "CompanyAddress";
  String COMPANY = "Company";
}
