package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IOrderItemsResponseKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DObPurchaseOrderViewItemsWithPricesTestUtils
    extends DObPurchaseOrderViewItemsTestUtils {

  public final String VIEW_ITEMS_URL = "/services/purchase/orders/itemsview/";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Items][With][Prices]";


  public DObPurchaseOrderViewItemsWithPricesTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getNewItemVendorRecordDataDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    return str.toString();
  }

  public void assertThatOrderItemQuantitiesAreDisplayedCorrect(
      String itemCode, DataTable itemQuantitiesTable, Response purchaseOrderItemsDataResponse)
      throws Exception {
    List<Map<String, String>> expectedQuantities =
        itemQuantitiesTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItems =
        purchaseOrderItemsDataResponse.body().jsonPath().getList(DATA_ITEMS);
    for (HashMap<String, Object> actualItem : actualItems) {
      if (actualItem.get(IOrderItemsResponseKeys.ITEM_CODE).equals(itemCode)) {
        List<HashMap<String, Object>> actualQuantities =
            (List<HashMap<String, Object>>) actualItem.get(ITEM_QUANTITIES);
        assertCorrectOrderItemQuantities(expectedQuantities, actualQuantities);
      }
    }
  }

  private void assertCorrectOrderItemQuantities(
      List<Map<String, String>> expectedQuantities, List<HashMap<String, Object>> actualQuantities)
      throws Exception {
    int matchedQuantities = 0;
    for (Map<String, String> expectedQuantity : expectedQuantities) {
      for (Map<String, Object> actualQuantity : actualQuantities) {
        if (isSameItemQuantityOrderUnit(expectedQuantity, actualQuantity)) {
          expectedQuantity = convertEmptyStringsToNulls(expectedQuantity);
          assertResponseItemQuantity(expectedQuantity, actualQuantity);
          assertResponseItemPrice(expectedQuantity, actualQuantity);
          assertResponseItemDiscountPercentage(expectedQuantity, actualQuantity);
          assertResponseItemCodeAtVendor(expectedQuantity, actualQuantity);
          matchedQuantities++;
          break;
        }
      }
    }
    assertEquals(ASSERTION_MSG + " Quantities Size " + expectedQuantities.size() + " is not correct",
    expectedQuantities.size(), matchedQuantities);
  }

  private void assertResponseItemCodeAtVendor(
      Map<String, String> expectedQuantity, Map<String, Object> actualQuantity) {
    String itemCodeAtVendor = expectedQuantity.get(IFeatureFileCommonKeys.ITEM_CODE_AT_VENDOR);
    assertEquals(ASSERTION_MSG + " Item Code At Vendor " + itemCodeAtVendor + " is not correct",
        itemCodeAtVendor,
        String.valueOf(actualQuantity.get(IOrderItemsResponseKeys.ITEM_CODE_AT_VENDOR)));
  }

  private void assertResponseItemDiscountPercentage(
      Map<String, String> expectedQuantity, Map<String, Object> actualQuantity) {
    String expectedDiscountString = expectedQuantity.get(IFeatureFileCommonKeys.DISCOUNT);
    BigDecimal expectedDiscount =
        new BigDecimal(expectedDiscountString.substring(0, expectedDiscountString.length() - 1));
    BigDecimal actualDiscount =
        new BigDecimal(
            (String) actualQuantity.get(IOrderItemsResponseKeys.ITEM_DISCOUNT_PERCENTAGE));
    assertEquals(
        ASSERTION_MSG
            + " Item Discount Percentage "
            + expectedDiscountString
            + " is not correct",
        0,
        expectedDiscount.compareTo(actualDiscount));
  }

  private void assertResponseItemPrice(
      Map<String, String> expectedQuantity, Map<String, Object> actualQuantity) {
    BigDecimal expected =
        new BigDecimal(expectedQuantity.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE));
    BigDecimal actual =
        new BigDecimal((String) actualQuantity.get(IOrderItemsResponseKeys.ITEM_PRICE));
    assertEquals(
        ASSERTION_MSG
            + " Item Price "
            + expectedQuantity.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE)
            + " is not correct",
        0,
        expected.compareTo(actual));
  }
}
