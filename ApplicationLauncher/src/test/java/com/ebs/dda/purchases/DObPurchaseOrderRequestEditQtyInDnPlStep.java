package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderRequestEditQtyInDnPlStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderItemTestUtils utils;

  public DObPurchaseOrderRequestEditQtyInDnPlStep() {

    Given(
        "^the following PurchaseOrders have the following items:$",
        (DataTable dataTable) -> {
          utils.assertPurchaseOrdersExistWithItems(dataTable);
        });

    When(
        "^\"([^\"]*)\" requests to edit Item Qty In DN/PL Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_EDIT_QTY_IN_DN_PL + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^edit Qty in DN/PL dialog is opened and OrderItems section of PurchaseOrder with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to cancel Item Qty In DN/PL Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          String url =
              IPurchaseOrderRestUrls.REQUEST_CANCEL_QTY_IN_DN_PL
                  + purchaseOrderCode
                  + "/"
                  + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, url);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^edit Qty in DN/PL dialog is closed and the lock by \"([^\"]*)\" on OrderItems section of PurchaseOrder with code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to edit Item Qty In DN/PL Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_EDIT_QTY_IN_DN_PL + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    And(
        "^\"([^\"]*)\" opened Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_EDIT_QTY_IN_DN_PL + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);

          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" opened Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" in edit mode$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_EDIT_QTY_IN_DN_PL + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to cancel Item Qty In DN/PL Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String url = IPurchaseOrderRestUrls.REQUEST_CANCEL_QTY_IN_DN_PL + purchaseOrderCode + '/';
          Response response = userActionsTestUtils.unlockSection(userName, url + itemCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    utils = new DObPurchaseOrderItemTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder,")
        || scenario
            .getName()
            .contains("Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder,")
        || scenario
            .getName()
            .contains("Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request Edit Item Qty In DN/PL in Import/Local PurchaseOrder,")
        || scenario
            .getName()
            .contains("Request Cancel Edit Item Qty In DN/PL in Import/Local PurchaseOrder,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
