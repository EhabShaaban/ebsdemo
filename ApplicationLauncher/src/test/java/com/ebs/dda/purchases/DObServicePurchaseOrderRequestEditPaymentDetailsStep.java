package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObServicePurchaseOrderRequestEditPaymentDetailsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObServicePurchaseOrderRequestEditPaymentDetailsStep extends SpringBootRunner
    implements En {

  private DObServicePurchaseOrderRequestEditPaymentDetailsTestUtils utils;

  public DObServicePurchaseOrderRequestEditPaymentDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to edit PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String servicePOCode) -> {
          String lockUrl = utils.getLockPaymentDetailsUrl(servicePOCode);
          Response response = userActionsTestUtils.lockSection(userName, lockUrl, servicePOCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\" becomes in edit mode and locked by \"([^\"]*)\"$",
        (String servicePOCode, String userName) -> {
          utils.assertThatResourceIsLockedByUser(
              userName,
              servicePOCode,
              IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION,
              utils.PO_JMX_LOCK_BEAN_NAME);
        });

    Given(
        "^PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String servicePOCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = utils.getLockPaymentDetailsUrl(servicePOCode);
          Response response = userActionsTestUtils.lockSection(userName, lock_url, servicePOCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String servicePOCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unLockUrl = utils.getUnLockPaymentDetailsUrl(servicePOCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\" is released$",
        (String userName, String servicePOCode) -> {
          utils.assertThatLockIsReleased(
              userName,
              servicePOCode,
              utils.PO_JMX_LOCK_BEAN_NAME,
              IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened the PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String servicePOCode) -> {
          String lock_url = utils.getLockPaymentDetailsUrl(servicePOCode);
          Response response = userActionsTestUtils.lockSection(userName, lock_url, servicePOCode);
          utils.assertThatResourceIsLockedByUser(
              userName,
              servicePOCode,
              IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION,
              utils.PO_JMX_LOCK_BEAN_NAME);
          utils.assertResponseSuccessStatus(response);
        });

    Then(
        "^PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String servicePOCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName,
              servicePOCode,
              utils.PO_JMX_LOCK_BEAN_NAME,
              IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving PaymentDetails section of Service PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String servicePOCode) -> {
          String unLockUrl = utils.getUnLockPaymentDetailsUrl(servicePOCode);
          Response response = userActionsTestUtils.unlockSection(userName, unLockUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObServicePurchaseOrderRequestEditPaymentDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario
        .getName()
        .contains("Request to edit PaymentDetails section in Service PurchaseOrder")
        || scenario
        .getName()
        .contains("Request to Cancel saving PaymentDetails section in Service PurchaseOrder")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (scenario
        .getName()
        .contains("Request to edit PaymentDetails section in Service PurchaseOrder")
        || scenario
        .getName()
        .contains("Request to Cancel saving PaymentDetails section in Service PurchaseOrder")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
