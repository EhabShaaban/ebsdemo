package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IExpectedKeys;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DObPurchaseOrderMarkAsProductionFinishedTestUtils
    extends DObPurchaseOrderCommonTestUtils {
  public static final String PRODUCTION_FINISHED_DATE = "ProductionFinishedDate";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Mark][As][Production][Finished]";

  public DObPurchaseOrderMarkAsProductionFinishedTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    return str.toString();
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    return str.toString();
  }

  public Response markAsProdcutionFinished(
      Cookie loginCookie, String purchaseOrderCode, DataTable productionFinishedDatesDataTable) {
    JsonObject markProductionFinishedJsonData =
        getMarkAsProductionFinishedJsonObject(productionFinishedDatesDataTable);
    String restURL = "/command/purchaseorder/markasproductionfinished/" + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, markProductionFinishedJsonData);
  }

  private JsonObject getMarkAsProductionFinishedJsonObject(
      DataTable productionFinishedDatesDataTable) {
    JsonObject markPIRequestJsonData = new JsonObject();
    Map<String, String> productionFinishedData =
        productionFinishedDatesDataTable.asMaps(String.class, String.class).get(0);
    markPIRequestJsonData.addProperty(
        "productionFinishedDate", productionFinishedData.get(PRODUCTION_FINISHED_DATE));
    return markPIRequestJsonData;
  }

  public void assertPurchaseOrderIsUpdated(String purchaseOrderCode, DataTable updatedOrders) {
    Map<String, String> expectedOrder = updatedOrders.asMaps(String.class, String.class).get(0);
    Object result =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getProductionFinishedUpdatedPurchaseOrder",
            purchaseOrderCode,
            expectedOrder.get(IExpectedKeys.LAST_UPDATED_BY),
            getDateTimeInMachineTimeZone(expectedOrder.get(IExpectedKeys.LAST_UPDATE_DATE)),
            getDateTimeInMachineTimeZone(
                expectedOrder.get(IExpectedKeys.PRODUCTION_FINISHED_DATE)));
    Assert.assertNotNull(
        ASSERTION_MSG + " The PO with code " + purchaseOrderCode + " is not updated with this info",
        result);
  }

  public void assertThatPurchaseOrderWithConfirmationDateDateIsExist(DataTable purchaseOrderTable) {
    Map<String, String> expectedDataMap =
        purchaseOrderTable.asMaps(String.class, String.class).get(0);
    List<Object> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getConfirmedPurchaseOrderDetails", expectedDataMap.get("Code"), "%Confirmed%");

    assertEquals(
        ASSERTION_MSG + " The Confirmation Date is not correct",
        expectedDataMap.get("confirmationDate"),
        actualData.get(0));
  }
}
