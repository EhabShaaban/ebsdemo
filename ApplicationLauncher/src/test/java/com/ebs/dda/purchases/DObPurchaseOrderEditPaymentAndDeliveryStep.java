package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderEditPaymentAndDeliveryTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPurchaseOrderEditPaymentAndDeliveryStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderEditPaymentAndDeliveryTestUtils utils;

  public DObPurchaseOrderEditPaymentAndDeliveryStep() {

    When(
        "^\"([^\"]*)\" requests to edit PaymentandDelivery section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Response lockResponse =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_PAYMENT_DELIVERY_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, lockResponse);
        });

    Given(
        "^\"([^\"]*)\" first opened the PaymentandDelivery section of PurchaseOrder with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String purchaseOrderCode) -> {
          Response lockResponse =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_PAYMENT_DELIVERY_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, lockResponse);
          utils.assertResponseSuccessStatus(userActionsTestUtils.getUserResponse(userName));
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION);
        });

    Then(
        "^PaymentandDelivery section of PurchaseOrder with code \"([^\"]*)\" becomes in the edit mode and locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertResponseSuccessStatus(userActionsTestUtils.getUserResponse(userName));
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION);
        });

    Given(
        "^PaymentandDelivery section of PurchaseOrder with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          Response lockResponse =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_PAYMENT_DELIVERY_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          utils.assertResponseSuccessStatus(lockResponse);
          userActionsTestUtils.setUserResponse(userName, lockResponse);
        });

    When(
        "^\"([^\"]*)\" cancels saving PaymentandDelivery section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          Response unlockResponse =
              userActionsTestUtils.unlockSection(
                  userName,
                  IPurchaseOrderRestUrls.UNLOCK_PAYMENT_DELIVERY_SECTION + purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, unlockResponse);
        });

    When(
        "^\"([^\"]*)\" cancels saving PaymentandDelivery section of PurchaseOrder with Code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Response unlockResponse =
              userActionsTestUtils.unlockSection(
                  userName,
                  IPurchaseOrderRestUrls.UNLOCK_PAYMENT_DELIVERY_SECTION + purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, unlockResponse);
        });

    Then(
        "^PaymentandDelivery section of PurchaseOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String username) -> {
          utils.assertThatLockIsReleased(
              username, purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderEditPaymentAndDeliveryTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit PaymentandDelivery section")
        || contains(scenario.getName(), "Cancel save PaymentandDelivery section")) {

      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit PaymentandDelivery section")
        || contains(scenario.getName(), "Cancel save PaymentandDelivery section")) {

      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit PaymentandDelivery section")
        || contains(scenario.getName(), "Cancel save PaymentandDelivery section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
