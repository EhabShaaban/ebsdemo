package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderStateTransitionTestUtils;
import com.ebs.dda.purchases.utils.IObPurchaseOrderSaveRequiredDocumentsTestUtils;
import com.ebs.dda.purchases.utils.MarkAsPIRequestedTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

public class DObPurchaseOrderMarkAsPIRequestedStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private MarkAsPIRequestedTestUtils utils;
  private DObPurchaseOrderStateTransitionTestUtils stateTransitionTestUtils;
  private IObPurchaseOrderSaveRequiredDocumentsTestUtils saveRequiredDocumentsTestUtils;

  public DObPurchaseOrderMarkAsPIRequestedStep() {

    When(
        "^\"([^\"]*)\" requests to MarkAsPIRequested the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String purchaseOrderCode, String updateDate, DataTable dataTable) -> {
          utils.freezeWithTimeZone(updateDate);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.markAsPIRequested(userCookie, purchaseOrderCode, dataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    And(
        "^PurchaseOrder with code \"([^\"]*)\" has the following Header section values:$",
        (String purchaseOrderCode, DataTable headerData) -> {
          stateTransitionTestUtils.assertThatPurchaseOrderHeaderHasTheFollowingValues(
              purchaseOrderCode, headerData);
        });
    And(
        "^PurchaseOrder with code \"([^\"]*)\" has the following Company section values:$",
        (String purchseOrderCode, DataTable compnayData) -> {
          stateTransitionTestUtils.assertThatPurchaseOrdersExistWithCompanyData(
              compnayData, purchseOrderCode);
        });
    And(
        "^PurchaseOrder with code \"([^\"]*)\" has the following Consignee section values:$",
        (String purchaseOrderCode, DataTable consigneeData) -> {
          stateTransitionTestUtils.assertThatPurchaseOrdersExistWithConsigneeData(
              purchaseOrderCode, consigneeData);
        });
    And(
        "^PurchaseOrder with code \"([^\"]*)\" has the following RequiredDocuments section values:$",
        (String purchaseOrderCode, DataTable requiredDocuments) -> {
          stateTransitionTestUtils.assertThatPurchaseOrderHasRequiredDocuments(
              purchaseOrderCode, requiredDocuments);
        });
    And(
        "^PurchaseOrder with code \"([^\"]*)\" has the following PaymentTerms section values:$",
        (String purchaseOrderCode, DataTable paymentTermDataTable) -> {
          stateTransitionTestUtils.assertThatPurchaseOrderExistWithPaymentAndDeliveryData(
              purchaseOrderCode, paymentTermDataTable);
        });
    And(
        "^Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" has the following Quantities:$",
        (String itemCode, String purchaseOrderCode, DataTable quantities) -> {
          utils.assertThatPOQuantitiesAreCorrect(itemCode, purchaseOrderCode, quantities);
        });
    And(
        "^PurchaseOrder with code \"([^\"]*)\" has the following OrderItems section values:$",
        (String purchaseOrderCode, DataTable itemsData) -> {
          List<Map<String, String>> itemMapList = itemsData.asMaps(String.class, String.class);
          for (Map<String, String> anItemMapList : itemMapList) {
            utils.assertItemExistsInPurchaseOrder(purchaseOrderCode, anItemMapList);
          }
        });
    And(
        "^PurchaseOrder with code \"([^\"]*)\" has an empty RequiredDocuments section$",
        (String purchaseOrderCode) -> {
          saveRequiredDocumentsTestUtils.assertThatPurchaseOrderRequiredDocumentsIsEmpty(
              purchaseOrderCode);
        });
    And(
        "^PurchaseOrder with code \"([^\"]*)\" has an empty OrderItems section$",
        (String purchaseOrderCode) -> {
          utils.assertNotItemExistInPurchaseOrder(purchaseOrderCode);
        });
    And(
        "^Item with code \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" has an empty Quantities in OrderItems section$",
        (String itemCode, String purchaseOrderCode) -> {
          utils.assertPurchaseOrderItemHasNoQuantities(purchaseOrderCode, itemCode);
        });
    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated as PIRequested as follows:$",
        (String purchaseOrderCode, DataTable consigneeData) -> {
          utils.assertThatPOMarkedAsPIRequested(purchaseOrderCode, consigneeData);
        });

    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable purchaseOrderData) -> {
          stateTransitionTestUtils.assertThatPurchaseOrderIsUpdated(
              purchaseOrderCode, purchaseOrderData);
        });

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" of PurchaseOrder with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String username, String sectionName, String purchaseOrderCode, String editDateTime) -> {
          utils.freeze(editDateTime);
          String lockSectionUrl = utils.getLockSectionUrl(sectionName) + purchaseOrderCode;
          Response lockResponse =
              userActionsTestUtils.lockSection(username, lockSectionUrl, purchaseOrderCode);
          utils.assertResponseSuccessStatus(lockResponse);
        });

    Then(
        "^the following fields \"([^\"]*)\" which sent to \"([^\"]*)\" are marked as missing$",
        (String missingFields, String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertThatResponseHasMissingFields(response, missingFields);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new MarkAsPIRequestedTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    stateTransitionTestUtils = new DObPurchaseOrderStateTransitionTestUtils(getProperties());
    stateTransitionTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    saveRequiredDocumentsTestUtils =
        new IObPurchaseOrderSaveRequiredDocumentsTestUtils(getProperties());
    saveRequiredDocumentsTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Mark PO as PI Requested")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptsAfterEachScenario());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Mark PO as PI Requested")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Mark PO as PI Requested")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptsAfterEachScenario());
      databaseConnector.closeConnection();
    }
  }
}
