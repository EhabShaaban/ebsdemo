package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.Map;

public class DObPurchaseOrderSaveQtyInDnPlTestUtils extends DObPurchaseOrderItemTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Save][QtyInDn]";

  public DObPurchaseOrderSaveQtyInDnPlTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response saveQtyInDnPl(
      String url,
      String purchaseOrderCode,
      String itemCode,
      Cookie loginCookie,
      DataTable quantitiesTable) {

    JsonObject valueObject = initQtyInDnPlValueObject(quantitiesTable);
    return sendPUTRequest(loginCookie, url + purchaseOrderCode + "/" + itemCode, valueObject);
  }

  private JsonObject initQtyInDnPlValueObject(DataTable quantitiesTable) {
    Map<String, String> quantitiesMap = quantitiesTable.asMaps(String.class, String.class).get(0);
    quantitiesMap = convertEmptyStringsToNulls(quantitiesMap);

    JsonObject valueObject = new JsonObject();
    try {
      valueObject.addProperty(
          IPurchaseOrderItemValueObject.QUANTITY_IN_DN,
          quantitiesMap.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN) != null
              ? new BigDecimal(quantitiesMap.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN))
              : null);
    } catch (NumberFormatException ex) {
      valueObject.addProperty(
          IPurchaseOrderItemValueObject.QUANTITY_IN_DN,
          (quantitiesMap.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN)));
    }

    return valueObject;
  }

  public void assertItemUpdated(String itemCode, String orderCode, DataTable expectedUpdates) {

    Map<String, String> expectedUpdatesMap =
        expectedUpdates.asMaps(String.class, String.class).get(0);
    IObOrderLineDetailsGeneralModel orderLineDetailsGeneralModel =
        (IObOrderLineDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getUpdatedItemAfterSaveQtyInDnPl",
                getItemAfterSaveQtyInDnPlQueryParams(itemCode, orderCode, expectedUpdatesMap));

    Assert.assertNotNull(
        ASSERTION_MSG
            + " Item with code "
            + itemCode
            + " in PO with code "
            + orderCode
            + " doesn't match this info",
        orderLineDetailsGeneralModel);
  }

  private Object[] getItemAfterSaveQtyInDnPlQueryParams(
      String itemCode, String orderCode, Map<String, String> expectedUpdatesMap) {
    return new Object[] {
      orderCode,
      itemCode,
      expectedUpdatesMap.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN),
      expectedUpdatesMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
      expectedUpdatesMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE)
    };
  }
}
