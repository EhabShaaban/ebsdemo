package com.ebs.dda.purchases.utils;

import com.ebs.dda.CommonKeys;
import com.ebs.dda.purchases.IAssertionMessages;
import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IResponseKeys;
import cucumber.api.DataTable;
import io.restassured.response.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Map;

import static org.junit.Assert.assertEquals;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderViewPaymentAndDeliveryTestUtils
    extends DObPurchaseOrderPaymentAndDeliveryTestUtils {

  private ObjectMapper objectMapper;
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View]";

  public DObPurchaseOrderViewPaymentAndDeliveryTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
    objectMapper = new ObjectMapper();
  }

  public void assertThatPaymentAndDeliveryReadResponseIsCorrect(
      DataTable paymentAndDeliveryTable, Response paymentAndDeliveryResponse) throws Exception {

    Map<String, String> expectedMap =
        paymentAndDeliveryTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> responseMap =
        paymentAndDeliveryResponse.body().jsonPath().get(CommonKeys.DATA);

    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.INCOTERM,
        expectedMap.get(IExpectedKeys.INCOTERM),
        responseMap.get(IResponseKeys.INCOTERM));
    String localizedCurrency =
        objectMapper.writeValueAsString(responseMap.get(IResponseKeys.CURRENCY));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.CURRENCY,
        expectedMap.get(IExpectedKeys.CURRENCY),
        getEnglishLocaleFromLocalizedResponse(localizedCurrency));
    String localizedPaymentTerms =
        objectMapper.writeValueAsString(responseMap.get(IResponseKeys.PAYMENT_TERM));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.PAYMENT_TERMS,
        expectedMap.get(IExpectedKeys.PAYMENT_TERMS),
        getEnglishLocaleFromLocalizedResponse(localizedPaymentTerms));
    String localizedShippingInstructions =
        objectMapper.writeValueAsString(responseMap.get(IResponseKeys.SHIPPING_INSTRUCTIONS));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.SHIPPING_INSTRUCTIONS,
        expectedMap.get(IExpectedKeys.SHIPPING_INSTRUCTIONS),
        getEnglishLocaleFromLocalizedResponse(localizedShippingInstructions));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.CONTAINER_NO,
            expectedMap.get(IExpectedKeys.CONTAINERS_NO),
        responseMap.get(IResponseKeys.CONTAINER_NO).toString());
    String localizedContainerType =
        objectMapper.writeValueAsString(responseMap.get(IResponseKeys.CONTAINER_TYPE));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.CONTAINER_TYPE,
        expectedMap.get(IExpectedKeys.CONTAINERS_TYPE),
        getEnglishLocaleFromLocalizedResponse(localizedContainerType));
    String localizedModeOfTransport =
        objectMapper.writeValueAsString(responseMap.get(IResponseKeys.TRANSPORT_MODE));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.TRANSPORT_MODE,
        expectedMap.get(IExpectedKeys.MODE_OF_TRANSPORT),
        getEnglishLocaleFromLocalizedResponse(localizedModeOfTransport));
    String localizedLoadingPort =
        objectMapper.writeValueAsString(responseMap.get(IResponseKeys.LOADING_PORT));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.LOADING_PORT,
        expectedMap.get(IExpectedKeys.LOADING_PORT),
        getEnglishLocaleFromLocalizedResponse(localizedLoadingPort));
    String localizedDischargePort =
        objectMapper.writeValueAsString(responseMap.get(IResponseKeys.DISCHARGE_PORT));
    assertEquals(ASSERTION_MSG + " " + IAssertionMessages.DISCHARGE_PORT,
        expectedMap.get(IExpectedKeys.DISCHARGE_PORT),
        getEnglishLocaleFromLocalizedResponse(localizedDischargePort));

    DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.s");
    DateTime responseDate =
        DateTime.parse(responseMap.get(IResponseKeys.COLLECTION_DATE).toString(), format);
    assertDateEquals(expectedMap.get(IExpectedKeys.COLLECTION_DATE), responseDate);
  }

  protected void assertDateEquals(String expectedDateStr, DateTime actualDate) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy");
    DateTime expectedDate = formatter.parseDateTime(expectedDateStr);
    assertEquals(ASSERTION_MSG + " Expected Date " + expectedDate + " is not correct" ,actualDate, expectedDate);
  }
}
