package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.PurchaseOrderViewAttachmentTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPurchaseOrderViewAttachmentStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private PurchaseOrderViewAttachmentTestUtils utils;

  DObPurchaseOrderViewAttachmentStep() {

    And(
        "^the following Attachments section exist in PurchaseOrder with code \"([^\"]*)\":$",
        (String purchaseOrderCode, DataTable orderAttachments) -> {
          utils.assertThatPurchaseOrderHasAttachments(purchaseOrderCode, orderAttachments);
        });

    When(
        "^\"([^\"]*)\" requests to view Attachments section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Response response =
              utils.readPurchaseOrderAttachments(
                  userActionsTestUtils.getUserCookie(userName), purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^the following values of Attachments section are displayed to \"([^\"]*)\":$",
        (String userName, DataTable purchaseOrderTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatAttachmentsDataTableEqualToResponse(purchaseOrderTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new PurchaseOrderViewAttachmentTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Attachments")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Attachments")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
