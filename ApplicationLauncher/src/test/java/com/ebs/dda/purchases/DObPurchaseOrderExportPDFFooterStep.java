package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderExportPDFFooterTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderExportPDFFooterStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderExportPDFFooterTestUtils exportPDFFooterTestUtils;

  public DObPurchaseOrderExportPDFFooterStep() {

    When(
        "^\"([^\"]*)\" requests to export PurchaseOrder footer of PurchaseOrder with code \"([^\"]*)\" as PDF$",
        (String username, String purchaseOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response response =
              exportPDFFooterTestUtils.exportPurchaseOrderAsPDF(userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of PurchaseOrder footer are displayed to \"([^\"]*)\":$",
        (String username, DataTable footerDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          exportPDFFooterTestUtils.assertThatFooterDataIsCorrect(footerDataTable, response);
        });

    Then(
        "^the following values of RequiredDocuments are displayed to \"([^\"]*)\":$",
        (String username, DataTable expectedRequiredDocuments) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          exportPDFFooterTestUtils.assertThatRequiredDocumentsResponseDataIsCorrect(
              expectedRequiredDocuments, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    exportPDFFooterTestUtils = new DObPurchaseOrderExportPDFFooterTestUtils(properties);
    exportPDFFooterTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to export PurchaseOrder footer")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(exportPDFFooterTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
