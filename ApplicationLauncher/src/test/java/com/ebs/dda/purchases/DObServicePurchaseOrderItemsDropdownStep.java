package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObServicePurchaseOrderItemsDropdownStep extends SpringBootRunner implements En {

  private DObPurchaseOrderItemTestUtils utils;

  public DObServicePurchaseOrderItemsDropdownStep() {

    When(
        "^\"([^\"]*)\" requests to read UOMs for Item with code \"([^\"]*)\" of Service Purchase Order with code \"([^\"]*)\" in a dropdown$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.sendGETRequest(
                  cookie, utils.getReadItemUOMDataUrl(purchaseOrderCode, itemCode));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following service item UOM values will be presented to \"([^\"]*)\" in the dropdown:$",
        (String userName, DataTable uomDropdownValues) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatUOMsValuesEqualExpectedInDropdown(uomDropdownValues, response);
        });

    Then(
        "^total number of records returned to \"([^\"]*)\" is \"(\\d+)\"$",
        (String userName, Integer uomCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatUOMResponseCountIsCorrect(response, uomCount);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderItemTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Read Service Purchase Order Item's UOMs dropdown,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptPathForServiceItemsDropdown());
    }
  }
}
