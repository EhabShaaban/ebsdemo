package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderDeleteTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.sql.SQLException;

public class DObPurchaseOrderDeleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderDeleteTestUtils utils;

  public DObPurchaseOrderDeleteStep() {

    Given(
        "^first \"([^\"]*)\" opens \"([^\"]*)\" of PurchaseOrder with code \"([^\"]*)\" in edit mode$",
        (String username, String sectionName, String purchaseOrderCode) -> {
          String lockSectionUrl = utils.getLockSectionUrl(sectionName) + purchaseOrderCode;
          Response lockRepponse =
              userActionsTestUtils.lockSection(username, lockSectionUrl, purchaseOrderCode);
          utils.assertResponseSuccessStatus(lockRepponse);
        });

    When(
        "^\"([^\"]*)\" requests to delete PurchaseOrder with code \"([^\"]*)\"$",
        (String username, String purchaseOrderCode) -> {
          Response deletePurchaseOrderResponse =
              utils.deletePurchaseOrderByCode(
                  userActionsTestUtils.getUserCookie(username), purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, deletePurchaseOrderResponse);
        });

    Then(
        "^PurchaseOrder with code \"([^\"]*)\" is deleted from the system$",
        (String purchaseOrderCode) -> {
          utils.assertThatPurchaseOrderIsDeleted(purchaseOrderCode);
        });
  }

  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderDeleteTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Delete PurchaseOrder,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Delete PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
