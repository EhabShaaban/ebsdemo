package com.ebs.dda.purchases;

public interface IPurchaseOrderColumns {
  String PURCHASE_UNIT_ID = "purchaseunitid";
  String PURCHASE_RESPONSIBLE_ID = "purchaseresponsibleid";
  String ORDER_CODE = "code";
  String VENDOR_ID = "vendorid";
  String MODIFICATION_INFO = "modificationinfo";
  String CREATION_INFO = "creationinfo";
  String CREATION_DATE = "creationdate";
  String MODIFICATION_DATE = "modifieddate";
  String CURRENT_STATES = "currentstates";
}
