package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderApproveTestUtils extends DObPurchaseOrderCommonTestUtils {

  public DObPurchaseOrderApproveTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  @Override
  public String getDbScriptsOneTimeExecution() {
    String dbScriptsOneTimeExecution = super.getDbScriptsOneTimeExecution();
    StringBuilder str = new StringBuilder();
    str.append(dbScriptsOneTimeExecution);
    str.append("," + "db-scripts/CObApprovalPoliciesSqlScript.sql");

    return str.toString();
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    return str.toString();
  }

  public Response approvePurchaseOrder(
      Cookie cookie, String purchaseOrderCode, DataTable approvalData) {
    Map<String, String> approvalMap = approvalData.asMaps(String.class, String.class).get(0);
    String approvalNote = approvalMap.get("Notes").isEmpty() ? null : approvalMap.get("Notes");
    JsonObject jsonData = new JsonObject();
    jsonData.addProperty("notes", approvalNote);
    jsonData.addProperty("purchaseOrderCode", purchaseOrderCode);
    return sendPostRequest(cookie, IPurchaseOrderRestUrls.APPROVE_PO, jsonData);
  }
}
