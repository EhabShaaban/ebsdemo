package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.IResponseKeys;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Map;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class PurchaseOrderViewAttachmentTestUtils extends PurchaseOrderAttachmentTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View]";

  public PurchaseOrderViewAttachmentTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public Response readPurchaseOrderAttachments(Cookie cookie, String puchaseOrderCode) {
    return sendGETRequest(
        cookie, IPurchaseOrderRestUrls.VIEW_ATTACHMETNS_SECTION + puchaseOrderCode);
  }

  public void assertThatAttachmentsDataTableEqualToResponse(
      DataTable attachmentsDataTable, Response attachmentsResponse) {
    List<List<String>> attachmentListDataTable = attachmentsDataTable.raw();
    List<Map<String, Object>> attachmentMapResponse =
        attachmentsResponse.body().jsonPath().get("data");
    String documentTypeName = "";
    String attachmentName = "";
    String uploadedBy = "";
    String uploadedDate = "";
    for (int i = 1; i < attachmentListDataTable.size(); i++) {
      documentTypeName = attachmentListDataTable.get(i).get(0);
      attachmentName = attachmentListDataTable.get(i).get(1);
      uploadedBy = attachmentListDataTable.get(i).get(2);
      uploadedDate = attachmentListDataTable.get(i).get(3);

      assertEquals(ASSERTION_MSG + " Document Type Name " + documentTypeName + " is not correct",
              documentTypeName, getActualDocumentTypeName(attachmentsResponse, i - 1));

      assertEquals(ASSERTION_MSG + " Attachment Name " + attachmentName + " is not correct",
          attachmentName,
          attachmentMapResponse.get(i - 1).get(IResponseKeys.ATTACHMENT_NAME).toString());

      assertEquals(ASSERTION_MSG + " Uploaded by User " + uploadedBy + " is not correct",
          uploadedBy, attachmentMapResponse.get(i - 1).get(IResponseKeys.CREATION_INFO).toString());

      assertEqualsDate(
          uploadedDate,
          attachmentMapResponse.get(i - 1).get(IResponseKeys.CREATION_DATE).toString());
    }
  }

  private String getActualDocumentTypeName(Response attachmentsResponse, int i) {
    return attachmentsResponse
        .body()
        .jsonPath()
        .get("data" + "[" + i + "]." + IResponseKeys.ATTACHMENT_TYPE_NAME + ".values.en");
  }

  private void assertEqualsDate(String expectedDate, String actualDate) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy HH:mm a");
    DateTimeFormatter databaseFormatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
    DateTime expectedCreationDateTime = formatter.parseDateTime(expectedDate);
    DateTime actualCreationDateTime = databaseFormatter.parseDateTime(actualDate);
    assertTrue(ASSERTION_MSG + " Creation Date " + expectedCreationDateTime + " is not correct",
    actualCreationDateTime.equals(expectedCreationDateTime));
  }
}
