package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSaveAddItemTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.When;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;
import java.util.Map;

public class DObPurchaseOrderSaveAddItemStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderSaveAddItemTestUtils utils;

  public DObPurchaseOrderSaveAddItemStep() {

    When(
        "^\"([^\"]*)\" saves the following item to the OrderItems of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName, String purchaseOrderCode, String saveTime, DataTable itemData) -> {
          utils.freeze(saveTime);
          saveItemToOrderItemsInPurchaseOrder(userName, purchaseOrderCode, itemData);
        });

    Then(
        "^OrderItems section of Import PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable itemsData) -> {
          utils.assertItemsExistsInPurchaseOrder(
              OrderTypeEnum.IMPORT_PO.name(), purchaseOrderCode, itemsData);
        });

    Then(
        "^OrderItems section of Local PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable itemsData) -> {
          utils.assertItemsExistsInPurchaseOrder(
              OrderTypeEnum.LOCAL_PO.name(), purchaseOrderCode, itemsData);
        });

    When(
        "^Item with code \"([^\"]*)\" has no IVR record$",
        (String itemCode) -> {
          utils.assertThatItemHasNoItemVendorRecord(itemCode);
        });

    Then(
        "^the following error message is attached to Item field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IPurchaseOrderItemValueObject.ITEM_CODE);
        });
  }

  @When(
      "^\"([^\"]*)\" saves the following item to the OrderItems of PurchaseOrder with code \"([^\"]*)\" with the following values:$")
  public void saveItemToOrderItemsInPurchaseOrder(
      String userName, String purchaseOrderCode, DataTable itemData) {
    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
    Response response =
        utils.savePurchaseOrderItemToItemSection(userCookie, purchaseOrderCode, itemData);
    userActionsTestUtils.setUserResponse(userName, response);
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderSaveAddItemTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Add Item In PurchaseOrder,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario.getName().contains("Save Add Item In PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Save Add Item In PurchaseOrder,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
