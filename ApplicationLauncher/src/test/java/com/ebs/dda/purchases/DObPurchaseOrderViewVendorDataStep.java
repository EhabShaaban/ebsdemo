package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewVendorDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderViewVendorDataStep extends SpringBootRunner implements En {

    private DObPurchaseOrderViewVendorDataTestUtils utils;

    public DObPurchaseOrderViewVendorDataStep() {

        When(
                "^\"([^\"]*)\" requests to view Vendor Data section of PurchaseOrder with code \"([^\"]*)\"$",
                (String userName, String purchaseOrderCode) -> {
                    Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
                    Response response = utils.readPurchaseOrderVendor(userCookie, purchaseOrderCode);
                    userActionsTestUtils.setUserResponse(userName, response);
                });

        Then(
                "^the following values of Vendor Data section are displayed to \"([^\"]*)\":$",
                (String userName, DataTable vendorDataTable) -> {
                    Response response = userActionsTestUtils.getUserResponse(userName);
                    utils.assertThatReadVendorDataResponseIsCorrect(vendorDataTable, response);
                });
    }

    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        utils =
                new DObPurchaseOrderViewVendorDataTestUtils(
                        getProperties(), entityManagerDatabaseConnector);
    }

    @Before
    public void setup(Scenario scenario) throws Exception {
        if (contains(scenario.getName(), "View Vendor Data section")) {
            databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        }
    }
}
