package com.ebs.dda.purchases.utils;

import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.models.BDKUser;
import com.ebs.dda.CommonTestUtils;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.ISalesReturnOrderFeatureFileCommonKeys;
import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Assert;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

import static org.junit.Assert.*;

public class DObPurchaseOrderCommonTestUtils extends CommonTestUtils {

  protected static final String COPIES = "Copies";
  protected static final String DOCUMENT_TYPE = "DocumentType";
  protected final String PURCHASE_MODIFICATION_INFO = "modificationinfo";
  protected final String PURCHASE_MODIFIED_DATE = "modifieddate";
  protected final String STATE = "State";
  protected final String PURCHASING_UNIT = "PurchasingUnit";
  protected final String ENTITY_NAME = "Name";
  protected final String ENTITY_CODE = "Code";
  protected final String UPDATED_BY = "LastUpdatedBy";
  protected final String UPDATED_DATE = "LastUpdateDate";
  protected final String RESPONSE_USER_CODE = "userCode";
  protected final String RESPONSE_NUMBER_OF_COPIES = "numberOfCopies";
  private final String BDK_COMPANY_CODE = "BDKCompanyCode";
  private final String REQUIRED_DOCUMENTS_SECTION = "RequiredDocuments";
  private final String PAYMENT_SECTION = "Paymentterms";
  private final String CONSIGNEE_SECTION = "Consignee";
  private final String COMPANY_SECTION = "Company";
  private final String HEADER_SECTION = "Header";
  private final String ITEM_SECTION = "OrderItems";
  private final String ITEM_DATA_SECTION = "ItemsData";
  private final String LOCAL_PURCHASE_ORDER = "Local Purchase Order";
  private final String IMPORT_PURCHASE_ORDER = "Import Purchase Order";
  public String PO_JMX_LOCK_BEAN_NAME = "purchaseOrderLockCommandJMXBean";
  protected String ASSERTION_MSG = "[PO]";
  Map<String, String> sectionsLockUrls;
  Map<String, String> sectionsUnlockUrls;
  private String requestlocale = "en";

  public DObPurchaseOrderCommonTestUtils() {
    prepareSectionsLockUrlsMap();
    prepareSectionsUnlockUrlsMap();
  }

  public void setProperties(HashMap<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");
    return str.toString();
  }

  public DObPurchaseOrderCommonTestUtils withLocale(String locale) {
    this.requestlocale = locale;
    return this;
  }

  public Response deletePurchaseOrderByCode(Cookie loginCookie, String purchaseOrderCode) {
    String deleteURL = IPurchaseOrderRestUrls.DELETE_PURCHASE_ORDER + purchaseOrderCode;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  public void assertThatCompaniesExist(DataTable companyTable) throws Exception {
    List<Map<String, String>> companies = companyTable.asMaps(String.class, String.class);
    for (Map<String, String> company : companies) {
      assertThatCompanyExistWithCode(company.get(ENTITY_CODE), company.get(ENTITY_NAME));
    }
  }

  public void assertThatResourceIsLockedByUser(
      String userName, String purchaseOrderCode, String sectionName) throws Exception {
    BDKUser user = new BDKUser();
    user.setUsername(userName);
    user.setCompany(BDK_COMPANY_CODE);
    ObjectName name = new ObjectName("com.ebs.dda.commands.general:name=" + PO_JMX_LOCK_BEAN_NAME);
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    String checkLockMethodName = "getLockForCurrentUserOfResource";
    Object[] parameters = new Object[] {user, sectionName, purchaseOrderCode};
    String[] parameterTypes =
        new String[] {IBDKUser.class.getName(), String.class.getName(), String.class.getName()};
    LockDetails lockReleaseDetails =
        (LockDetails) mbs.invoke(name, checkLockMethodName, parameters, parameterTypes);
    assertNotNull(lockReleaseDetails);
  }

  public void assertThatLockIsReleased(String userName, String code, String sectionName)
      throws Exception {
    super.assertThatLockIsReleased(userName, code, PO_JMX_LOCK_BEAN_NAME, sectionName);
  }

  public void assertThatPurchaseOrderUpdatedByUserAtDate(
      String purchaseOrderCode, DataTable updateDetailsTable) {
    Map<String, String> purchaseOrderMap =
        updateDetailsTable.asMaps(String.class, String.class).get(0);

    Object poModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedPO", getUpdatedPOQueryParams(purchaseOrderCode, purchaseOrderMap));

    assertNotNull(ASSERTION_MSG + " PO is not updated", poModifiedDate);
    assertDateEquals(
        purchaseOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(poModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] getUpdatedPOQueryParams(
      String purchaseOrderCode, Map<String, String> purchaseOrderHeaderMap) {
    return new Object[] {
      purchaseOrderCode, purchaseOrderHeaderMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY),
    };
  }

  public void assertThatImportPurchaseOrdersExist(DataTable purchaseOrderTable) throws Exception {
    assertPurchaseOrdersExistWithStateAndPurchaseUnit(
        purchaseOrderTable, OrderTypeEnum.IMPORT_PO.name());
  }

  public void assertThatLocalPurchaseOrdersExist(DataTable purchaseOrderTable) throws Exception {
    assertPurchaseOrdersExistWithStateAndPurchaseUnit(
        purchaseOrderTable, OrderTypeEnum.LOCAL_PO.name());
  }

  protected void assertThatPurchaseOrderExistWithGivenStateAndPurchaseUnit(
      Map<String, String> purchaseOrderData, String purchaseOrderType) throws Exception {

    Object purchaseOrder =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPOByCodeAndBusinessUnitAndType",
            constructPurchaseOrderQueryParameters(purchaseOrderData, purchaseOrderType));
    Assert.assertNotNull("Record not found", purchaseOrder);
  }

  private Object[] constructPurchaseOrderQueryParameters(
      Map<String, String> purchaseOrderData, String purchaseOrderType) {
    return new Object[] {
      purchaseOrderData.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderData.get(IFeatureFileCommonKeys.PURCHASING_UNIT),
      "%" + purchaseOrderData.get(IFeatureFileCommonKeys.STATE) + "%",
      purchaseOrderType
    };
  }

  private void assertThatCompanyExistWithCode(String companyCode, String companyName)
      throws Exception {
    PreparedStatement preparedStatement = createCompanyPreparedStatement(companyCode, companyName);
    ResultSet resultSet = databaseConnector.executeQuery(preparedStatement);
    assertTrue(
        ASSERTION_MSG
            + " this company "
            + companyName
            + " doesnt exist with this code "
            + companyCode,
        isNotEmptyResultSet(resultSet));
  }

  private PreparedStatement createCompanyPreparedStatement(String companyCode, String companyName)
      throws Exception {
    String query =
        "SELECT code FROM  cobcompany JOIN  cobenterprise  ON cobcompany.id = cobenterprise.id WHERE code = ? AND cobenterprise.name::json->>'en'=?;";
    PreparedStatement preparedStatement = databaseConnector.createPreparedStatement(query);
    preparedStatement.setString(1, companyCode);
    preparedStatement.setString(2, companyName);
    return preparedStatement;
  }

  protected void assertPurchaseOrdersExistWithStateAndPurchaseUnit(
      DataTable purchaseOrdersTable, String orderType) throws Exception {
    List<Map<String, String>> purchaseOrders =
        purchaseOrdersTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderData : purchaseOrders) {
      assertThatPurchaseOrderExistWithGivenStateAndPurchaseUnit(purchaseOrderData, orderType);
    }
  }

  protected String prepareQuery(Map<String, String> companyData, String key) {
    if (companyData.get(key) == null || companyData.get(key).isEmpty()) {
      return " IS NULL ";
    }
    return " = ? ";
  }

  private void prepareSectionsLockUrlsMap() {
    sectionsLockUrls = new HashMap<>();
    sectionsLockUrls.put(HEADER_SECTION, IPurchaseOrderRestUrls.LOCK_HEADER_SECTION);
    sectionsLockUrls.put(COMPANY_SECTION, IPurchaseOrderRestUrls.LOCK_COMPANY_SECTION);
    sectionsLockUrls.put(CONSIGNEE_SECTION, IPurchaseOrderRestUrls.LOCK_CONSIGNEE_SECTION);
    sectionsLockUrls.put(PAYMENT_SECTION, IPurchaseOrderRestUrls.LOCK_PAYMENT_DELIVERY_SECTION);
    sectionsLockUrls.put(
        REQUIRED_DOCUMENTS_SECTION, IPurchaseOrderRestUrls.LOCK_REQUIRED_DOCUMENTS_SECTION);

    sectionsLockUrls.put(ITEM_SECTION, IPurchaseOrderRestUrls.ADD_ITEM_SECTION);
    sectionsLockUrls.put(ITEM_DATA_SECTION, IPurchaseOrderRestUrls.REQUEST_EDIT_QTY_IN_DN_PL);
  }

  private void prepareSectionsUnlockUrlsMap() {
    sectionsUnlockUrls = new HashMap<>();
    sectionsUnlockUrls.put(HEADER_SECTION, IPurchaseOrderRestUrls.UNLOCK_HEADER_SECTION);
    sectionsUnlockUrls.put(COMPANY_SECTION, IPurchaseOrderRestUrls.UNLOCK_COMPANY_SECTION);
    sectionsUnlockUrls.put(CONSIGNEE_SECTION, IPurchaseOrderRestUrls.UNLOCK_CONSIGNEE_SECTION);
    sectionsUnlockUrls.put(PAYMENT_SECTION, IPurchaseOrderRestUrls.UNLOCK_PAYMENT_DELIVERY_SECTION);
    sectionsUnlockUrls.put(
        REQUIRED_DOCUMENTS_SECTION, IPurchaseOrderRestUrls.UNLOCK_REQUIRED_DOCUMENTS_SECTION);

    sectionsUnlockUrls.put(ITEM_SECTION, IPurchaseOrderRestUrls.UNLOCK_ITEM_SECTION);
    sectionsUnlockUrls.put(ITEM_DATA_SECTION, IPurchaseOrderRestUrls.UNLOCK_ITEM_SECTION);
  }

  public String getLockSectionUrl(String sectionName) {
    return sectionsLockUrls.get(sectionName);
  }

  public void assertThatPurchaseOrderHasRequiredDocuments(
      String purchaseOrderCode, DataTable requiredDocumentsTable) throws Exception {
    List<Map<String, String>> requiredDocumentData =
        requiredDocumentsTable.asMaps(String.class, String.class);
    for (Map<String, String> document : requiredDocumentData) {
      assertThatPurchaseOrderHasRequiredDocumentWithCodeAndCopies(purchaseOrderCode, document);
    }
  }

  private void assertThatPurchaseOrderHasRequiredDocumentWithCodeAndCopies(
      String purchaseOrderCode, Map<String, String> document) throws Exception {
    Object result =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getOrderDocumentRequiredDocuments",
            purchaseOrderCode,
            document.get(DOCUMENT_TYPE),
            Integer.parseInt(document.get(COPIES)));
    assertNotNull(ASSERTION_MSG + " there is no required document for this Po", result);
  }

  public void assertThatUsersInformationIsCorrect(DataTable usersInformationTable) {
    List<Map<String, String>> usersInformationList =
        usersInformationTable.asMaps(String.class, String.class);
    for (Map<String, String> userInformation : usersInformationList) {
      Object result =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getUserInfoByUsernameAndProfileName",
              userInformation.get("UserName"),
              userInformation.get("ProfileName"));
      assertNotNull(ASSERTION_MSG + " there is no user has this info", result);
    }
  }

  public List<String> getSectionsNames() {
    return new ArrayList<>(
        Arrays.asList(
            IPurchaseOrderSectionNames.HEADER_SECTION,
            IPurchaseOrderSectionNames.COMPANY_SECTION,
            IPurchaseOrderSectionNames.CONSIGNEE_SECTION,
            IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION,
            IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION,
            IPurchaseOrderSectionNames.REQUIRED_DOCUMENTS_SECTION,
            IPurchaseOrderSectionNames.ITEMS_SECTION));
  }

  public void assertPurchaseOrderDataIsCorrect(DataTable purchaseOrders, Response response)
      throws Exception {
    List<Map<String, String>> purchaseOrdersList =
        purchaseOrders.asMaps(String.class, String.class);
    List<HashMap<String, Object>> purchaseOrdersResponse =
        response.body().jsonPath().getList("data");
    for (int i = 1; i < purchaseOrdersList.size(); i++) {
      assertCorrectShownData(purchaseOrdersResponse.get(i - 1), purchaseOrdersList.get(i - 1));
    }
  }

  public void assertCorrectShownData(
      HashMap<String, Object> responsedPurchaseOrder, Map<String, String> purchaseOrderRowMap)
      throws Exception {

    String code = purchaseOrderRowMap.get(IFeatureFileCommonKeys.CODE);
    String type = purchaseOrderRowMap.get(IFeatureFileCommonKeys.TYPE);
    String vendorName = purchaseOrderRowMap.get(IFeatureFileCommonKeys.VENDOR);
    String purchaseOrderUnit = purchaseOrderRowMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT);
    String purchaseOrderState = purchaseOrderRowMap.get(IFeatureFileCommonKeys.STATE);
    String expectedReferenceDocument =
        purchaseOrderRowMap.get(IFeatureFileCommonKeys.REFERENCE_DOCUMENT);

    assertEquals(
        ASSERTION_MSG + " there is no Po with this code",
        code,
        responsedPurchaseOrder.get("userCode").toString());
    String objectTypeCode = String.valueOf(responsedPurchaseOrder.get("objectTypeCode"));
    assertEquals(ASSERTION_MSG + " there is no Po with this Type", type, objectTypeCode);
    assertEquals(
        ASSERTION_MSG + " there is no po with this vendor",
        vendorName,
        getLocaleFromLocalizedResponse(
            responsedPurchaseOrder.get("vendorName").toString(), requestlocale));
    assertTrue(
        ASSERTION_MSG + " there is no Po with this purchasingUnit",
        responsedPurchaseOrder.get("purchaseUnitName").toString().contains(purchaseOrderUnit));
    assertTrue(
        ASSERTION_MSG + " there is no Po with this state",
        responsedPurchaseOrder.get("currentStates").toString().contains(purchaseOrderState));
    assertThatRefDocumentTypeIsCorrect(responsedPurchaseOrder, expectedReferenceDocument);
  }

  private void assertThatRefDocumentTypeIsCorrect(
      HashMap<String, Object> responsedPurchaseOrder, String expectedReferenceDocument) {
    if (responsedPurchaseOrder.get(IDObPurchaseOrderGeneralModel.REF_DOCUMENT_TYPE) != null) {
      StringBuilder referenceDocumentTypeAndCode = new StringBuilder();
      referenceDocumentTypeAndCode.append(getRefDocumentTypeFromResponse(responsedPurchaseOrder));
      referenceDocumentTypeAndCode.append(" - ");
      referenceDocumentTypeAndCode.append(
          responsedPurchaseOrder.get(IDObPurchaseOrderGeneralModel.REF_DOCUMENT_CODE).toString());

      assertTrue(
          ASSERTION_MSG + " referenceDocument is not correct",
          referenceDocumentTypeAndCode.toString().equals(expectedReferenceDocument));
    }
  }

  private String getRefDocumentTypeFromResponse(HashMap<String, Object> responsedPurchaseOrder) {
    String responsedRefDocumentType =
        responsedPurchaseOrder.get(IDObPurchaseOrderGeneralModel.REF_DOCUMENT_TYPE).toString();
    if (responsedRefDocumentType.equals("LOCAL_PO")) return LOCAL_PURCHASE_ORDER;
    return IMPORT_PURCHASE_ORDER;
  }

  public void assertThatPOQuantitiesAreCorrect(
      String itemCode, String purchaseOrderCode, DataTable quantaties) throws Exception {
    List<Map<String, String>> itemQuantitiesLists = quantaties.asMaps(String.class, String.class);
    for (int i = 1; i < itemQuantitiesLists.size(); i++) {
      Object[] queryParams =
          constructItemQuantitiesExistenceQueryParams(
              purchaseOrderCode, itemCode, itemQuantitiesLists.get(i));
      assertPurchaseOrderItemQuantitiesExist(queryParams);
    }
  }

  public Object[] constructItemQuantitiesExistenceQueryParams(
      String purchaseOrderCode, String itemCode, Map<String, String> itemQuantitiesValues) {
    return new Object[] {
      itemCode,
      Float.parseFloat(itemQuantitiesValues.get(IFeatureFileCommonKeys.QUANTITY)),
      itemQuantitiesValues.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      Float.parseFloat(itemQuantitiesValues.get(IFeatureFileCommonKeys.GROSS_UNIT_PRICE)),
      Float.parseFloat(itemQuantitiesValues.get(IFeatureFileCommonKeys.DISCOUNT).replace("%", "")),
      purchaseOrderCode
    };
  }

  public void assertPurchaseOrderItemQuantitiesExist(Object[] queryParams) throws Exception {
    Object result =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getOrderLineDetailsQuantitiesGeneralModel", queryParams);
    assertNotNull(ASSERTION_MSG + " there is no quantities for this Po", result);
  }

  public void assertItemExistsInPurchaseOrder(String purchaseOrderCode, Map<String, String> item) {
    item = convertEmptyStringsToNulls(item);
    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getItemDetailsForItemsInPurchaseOrder",
            purchaseOrderCode,
            item.get(IExpectedKeys.ITEM_CODE),
            item.get(IExpectedKeys.ITEM_NAME),
            item.get(IExpectedKeys.BASE));

    assertNotNull(ASSERTION_MSG + " there is no Item details in this Po", actualData);
    assertQuantityInDnInItem(actualData, item);
  }

  private void assertQuantityInDnInItem(List<Object[]> actualData, Map<String, String> item) {
    Float actualQuantityInDN =
        actualData.get(0)[1] == null ? null : Float.valueOf(actualData.get(0)[1].toString());
    if (item.get(IExpectedKeys.QTYINDN) != null && actualQuantityInDN != null) {
      assertEquals(
          ASSERTION_MSG + " this quantity is not correct",
          Float.valueOf(item.get(IExpectedKeys.QTYINDN)),
          actualQuantityInDN);
    }
  }

  //  TODO: Refactor this method and add assertion not null for returned object
  public void assertPurchaseOrdersExistWithBasicData(DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purhcaseOrderMap : purchaseOrdersMaps) {
      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
          "getPurchaseOrderByBasicData",
          purhcaseOrderMap.get(IFeatureFileCommonKeys.CODE),
          purhcaseOrderMap.get(IFeatureFileCommonKeys.TYPE),
          "%" + purhcaseOrderMap.get(IFeatureFileCommonKeys.STATE) + "%",
          purhcaseOrderMap.get(IFeatureFileCommonKeys.PURCHASING_UNIT));
    }
  }

  // TODO: Remove this method and use assertThatResponseHasMissingFields in CommonTestUtils
  public void assertThatResponseHasMissingFields(Response response, String missingFields) {
    Map<String, List<String>> missingFieldsList = response.body().jsonPath().get("missingFields");

    Set<String> actualList = new HashSet<>();
    String[] expectedList = missingFields.replaceAll("\\s", "").split(",");
    for (Map.Entry<String, List<String>> entry : missingFieldsList.entrySet()) {
      actualList.addAll(entry.getValue());
    }
    assertEquals(
        ASSERTION_MSG + "this Po doesn't have this missing fields",
        expectedList.length,
        actualList.size());
    assertTrue(
        ASSERTION_MSG + " this Po doesn't have this missing fields",
        actualList.containsAll(Arrays.asList(expectedList)));
  }

  public Response deleteItemInPurchaseOrder(
      Cookie loginCookie, String purchaseOrderCode, String itemCode) {
    String deleteURL = IPurchaseOrderRestUrls.DELETE_ITEM + purchaseOrderCode + '/' + itemCode;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  public void createPurchaseOrderGeneralData(DataTable purchaseOrderTable) {
    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createPurchaseOrderGeneralData", getGeneralDataCreateParameters(purchaseOrderMap));
    }
  }

  private Object[] getGeneralDataCreateParameters(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CREATION_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CREATED_BY),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CREATED_BY),
      purchaseOrderMap.get(IFeatureFileCommonKeys.STATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.TYPE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.DOCUMENT_OWNER),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.TYPE)
    };
  }

  public void createPurchaseOrderCompanyData(DataTable purchaseOrderTable) {
    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createPurchaseOrderCompanyData", getCompanyDataCreateParameters(purchaseOrderMap));
    }
  }

  private Object[] getCompanyDataCreateParameters(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.COMPANY),
      purchaseOrderMap.get(IFeatureFileCommonKeys.BUSINESS_UNIT),
      purchaseOrderMap.get(IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER).equals("")
          ? null
          : purchaseOrderMap.get(IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER).split(" - ")[2],
      purchaseOrderMap.get(IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER).equals("")
          ? null
          : (purchaseOrderMap.get(IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER).split(" - ")[0]
              + " - "
              + purchaseOrderMap.get(IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER).split(" - ")[1])
    };
  }

  public void createPurchaseOrderVendorData(DataTable purchaseOrderTable) {
    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createPurchaseOrderVendorData", getVendorDataCreateParameters(purchaseOrderMap));
    }
  }

  private Object[] getVendorDataCreateParameters(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.VENDOR),
      purchaseOrderMap.get(IFeatureFileCommonKeys.REFERENCE_PO).isEmpty()
          ? null
          : purchaseOrderMap.get(IFeatureFileCommonKeys.REFERENCE_PO)
    };
  }

  public void assertPurchaseOrderIsUpdated(
      String purchaseOrderCode, DataTable orderModificationInfo) {
    Map<String, String> expectedOrder =
        orderModificationInfo.asMaps(String.class, String.class).get(0);

    DObPurchaseOrderGeneralModel orderHeaderGeneralModel =
        (DObPurchaseOrderGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                "getPurchaseOrderByCodeAndModificationInfo",
                purchaseOrderCode,
                expectedOrder.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                expectedOrder.get(IFeatureFileCommonKeys.LAST_UPDATED_BY));
    assertNotNull(
        ASSERTION_MSG + " There is no Header for this PO with code " + purchaseOrderCode,
        orderHeaderGeneralModel);
  }

  public void createPurchaseOrderItemsData(DataTable itemsDataTable) {
    List<Map<String, String>> itemsDataMap = itemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> itemMap : itemsDataMap) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createPurchaseOrderItemData", getItemCreateParameters(itemMap));
    }
  }

  private Object[] getItemCreateParameters(Map<String, String> itemMap) {
    return new Object[] {
      itemMap.get(IOrderFeatureFileCommonKeys.PRICE),
      itemMap.get(IOrderFeatureFileCommonKeys.PO_ITEM_ID),
      itemMap.get(IFeatureFileCommonKeys.CODE),
      itemMap.get(IFeatureFileCommonKeys.ITEM),
      itemMap.get(IOrderFeatureFileCommonKeys.ORDER_UNIT),
      itemMap.get(IOrderFeatureFileCommonKeys.QUANTITY)
    };
  }

  public void assertThatOrderHasNoItems(DataTable orderDataTable) {
    List<Map<String, String>> ordersList = orderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> orderRow : ordersList) {
      Object orderItem =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObOrderItemByCode", orderRow.get(IFeatureFileCommonKeys.CODE));
      Assert.assertNull(ASSERTION_MSG + " This PurchaseOrder has items. ", orderItem);
    }
  }

  public void assertThatPurchaseOrdersHaveTheFollowingTaxDetails(
      DataTable purchaseOrderTaxDetailsDataTable) {
    List<Map<String, String>> purchaseOrdersTaxDetailsList =
        purchaseOrderTaxDetailsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> taxDetailRow : purchaseOrdersTaxDetailsList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createIObOrderTax", constructTaxesDetailQueryParams(taxDetailRow));
    }
  }

  private Object[] constructTaxesDetailQueryParams(Map<String, String> taxDetailsMap) {
    return new Object[] {
      taxDetailsMap.get(IFeatureFileCommonKeys.CODE),
      getCodeFromCodeName(taxDetailsMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX)),
      getNameFromCodeName(taxDetailsMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX)),
      getNameFromCodeName(taxDetailsMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX)),
      taxDetailsMap.get(ISalesReturnOrderFeatureFileCommonKeys.TAX_PERCENTAGE)
    };
  }

  public void assertThatPurchaseOrderHasNoTaxes(DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrdersList =
        purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderRow : purchaseOrdersList) {
      Object taxId =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getIObOrderTaxByCode", purchaseOrderRow.get(IFeatureFileCommonKeys.CODE));
      Assert.assertNull(ASSERTION_MSG + " This PO has taxes. ", taxId);
    }
  }

  public void createPurchaseOrderPaymentTermData(DataTable purchaseOrderTable) {
    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createPurchaseOrderPaymentTermData",
          getPaymentTermDataCreateParameters(purchaseOrderMap));
    }
  }

  private Object[] getPaymentTermDataCreateParameters(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.PAYMENT_TERMS),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CURRENCY)
    };
  }

  public void createPurchaseOrderCycleDates(DataTable purchaseOrderTable) {
    List<Map<String, String>> purchaseOrdersMaps =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrderMap : purchaseOrdersMaps) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createPurchaseOrderCycleDates", getCycleDatesCreateParameters(purchaseOrderMap));
    }
  }

  private Object[] getCycleDatesCreateParameters(Map<String, String> purchaseOrderMap) {
    return new Object[] {
      purchaseOrderMap.get(IFeatureFileCommonKeys.PI_REQUEST_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CONFIRMATION_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.PRODUCTION_FINISHED_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.SHIPPING_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.ARRIVAL_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CLEARANCE_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.DELIVERY_COMPLETE_DATE),
      purchaseOrderMap.get(IFeatureFileCommonKeys.CODE)
    };
  }

  public void createPurchaseOrderApprovalCycle(DataTable approvalCyclesDataTable) {
    List<Map<String, String>> approvalCyclesMapList =
        approvalCyclesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> approvalCycleMap : approvalCyclesMapList) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "createPurchaseOrderApprovalCycle", getApprovalCycleCreateParams(approvalCycleMap));
    }
  }

  private Object[] getApprovalCycleCreateParams(Map<String, String> approvalCycleMap) {
    return new Object[] {
      approvalCycleMap.get(IFeatureFileCommonKeys.CODE),
      Integer.parseInt(approvalCycleMap.get(IFeatureFileCommonKeys.CYCLE_NUMBER)),
      approvalCycleMap.get(IFeatureFileCommonKeys.START_DATE),
      approvalCycleMap.get(IFeatureFileCommonKeys.END_DATE),
      approvalCycleMap.get(IFeatureFileCommonKeys.FINAL_DECISION)
    };
  }

  public String getLockItemsURL(String purchaseOrderCode) {
    return IPurchaseOrderRestUrls.COMMAND_PURCHASE_ORDER
        + purchaseOrderCode
        + IPurchaseOrderRestUrls.LOCK_SERVICE_ITEM_SECTION;
  }

  protected String getFirstValue(Map<String, String> purchaseOrderDataMap, String key) {
    return purchaseOrderDataMap.get(key).split(" - ")[0];
  }

  public void setPurchaseOrderRemaining(DataTable remainingDataTable) {
    List<Map<String, String>> remainingDataMap =
        remainingDataTable.asMaps(String.class, String.class);
    for (Map<String, String> remainingMap : remainingDataMap) {
      entityManagerDatabaseConnector.executeInsertQueryForObject(
          "setPurchaseOrderRemaining", getRemainingParameters(remainingMap));
    }
  }

  private Object[] getRemainingParameters(Map<String, String> remainingMap) {
    return new Object[] {
      new BigDecimal(remainingMap.get(IAccountingFeatureFileCommonKeys.REMAINING)),
      remainingMap.get(IFeatureFileCommonKeys.CODE)
    };
  }
  public static String clearOrderDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/Clear_DObOrderDocument.sql");
    return str.toString();
  }
}
