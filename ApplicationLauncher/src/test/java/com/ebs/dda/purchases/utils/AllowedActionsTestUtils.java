package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IDObPurchaseOrderRestUrls;

import java.util.Map;

public class AllowedActionsTestUtils extends DObPurchaseOrderCommonTestUtils {

  public AllowedActionsTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScripts() {
    return "db-scripts/PO_View_GeneralData_Clear.sql";
  }

  public String getAuthorizedActionsUrl(String purchaseOrderCode) {
    return IDObPurchaseOrderRestUrls.AUTHORIZED_ACTIONS + purchaseOrderCode;
  }

  public String getAuthorizedActionsUrl() {
    return IDObPurchaseOrderRestUrls.AUTHORIZED_ACTIONS;
  }
}