package com.ebs.dda.purchases.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObPurchaseOrderViewVendorDataTestUtils
    extends PurchaseOrderGeneralDataCommonTestUtils {

  public DObPurchaseOrderViewVendorDataTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_VendorData_Clear.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    return str.toString();
  }

  public Response readPurchaseOrderVendor(Cookie userCookie, String purchaseOrderCode) {
    return sendGETRequest(userCookie, getViewVendorUrl(purchaseOrderCode));
  }

  private String getViewVendorUrl(String purchaseOrderCode) {
    return IPurchaseOrderRestUrls.QUERY_PURCHASE_ORDER
        + purchaseOrderCode
        + IPurchaseOrderRestUrls.VIEW_VENDOR;
  }

  public void assertThatReadVendorDataResponseIsCorrect(
      DataTable vendorDataTable, Response response) throws Exception {
    List<Map<String, String>> VendorsData = vendorDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedVendorMap : VendorsData) {

      HashMap<String, Object> actualVendorMap = getMapFromResponse(response);

      MapAssertion mapAssertion = new MapAssertion(expectedVendorMap, actualVendorMap);
      mapAssertion.assertCodeAndLocalizedNameValue(
          IFeatureFileCommonKeys.VENDOR,
          IDObPurchaseOrderGeneralModel.VENDOR_CODE,
          IDObPurchaseOrderGeneralModel.VENDOR_NAME);
      mapAssertion.assertValue(
          IFeatureFileCommonKeys.REFERENCE_PO, IDObPurchaseOrderGeneralModel.REFERENCE_PO_CODE);
    }
  }
}
