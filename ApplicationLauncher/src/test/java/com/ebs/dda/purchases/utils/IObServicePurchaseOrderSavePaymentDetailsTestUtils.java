package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObServicePurchaseOrderPaymentDetailsValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

public class IObServicePurchaseOrderSavePaymentDetailsTestUtils
    extends DObPurchaseOrderCommonTestUtils {

  protected final String ASSERTION_MSG = "[SPO] [Save] [PaymentDetails]";

  public IObServicePurchaseOrderSavePaymentDetailsTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public Response savePaymentDetails(
      String servicePOCode, DataTable paymentDetailsDataTable, Cookie cookie) {
    JsonObject paymentDetailsJsonObject = getPaymentDetailsJsonObject(paymentDetailsDataTable);
    String restURL = getPaymentDetailsURL(servicePOCode);
    return sendPUTRequest(cookie, restURL, paymentDetailsJsonObject);
  }

  private JsonObject getPaymentDetailsJsonObject(DataTable paymentDetailsDataTable) {
    JsonObject paymentDetailsJsonObject = new JsonObject();
    Map<String, String> paymentDetailsMap =
        paymentDetailsDataTable.asMaps(String.class, String.class).get(0);
    paymentDetailsJsonObject.addProperty(
        IIObServicePurchaseOrderPaymentDetailsValueObject.PAYMENT_TERM_CODE,
        convertEmptyStringToNull(
            getFirstValue(paymentDetailsMap, IFeatureFileCommonKeys.PAYMENT_TERMS)));
    paymentDetailsJsonObject.addProperty(
        IIObServicePurchaseOrderPaymentDetailsValueObject.CURRENY_CODE,
        convertEmptyStringToNull(
            getFirstValue(paymentDetailsMap, IFeatureFileCommonKeys.CURRENCY)));
    return paymentDetailsJsonObject;
  }

  private String getPaymentDetailsURL(String servicePOCode) {
    return IPurchaseOrderRestUrls.COMMAND_PURCHASE_ORDER
        + servicePOCode
        + IPurchaseOrderRestUrls.UPDATE_PAYMENT_DETAILS_SECTION;
  }

  public void assertThatPaymentDetailsIsUpdatedAsFollows(
      String servicePOCode, DataTable paymentDetailsDataTable) {
    Map<String, String> paymentDetailsMap =
        paymentDetailsDataTable.asMaps(String.class, String.class).get(0);

    Object paymentDetailsObject =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedIObOrderPaymentDetails",
            constructPaymentDetailsQueryParams(servicePOCode, paymentDetailsMap));

    assertNotNull(ASSERTION_MSG + " Updated PaymentDetails Is not correct", paymentDetailsObject);
  }

  private Object[] constructPaymentDetailsQueryParams(
      String servicePOCode, Map<String, String> paymentDetailsMap) {
    return new Object[]{
        paymentDetailsMap.get(IFeatureFileCommonKeys.PAYMENT_TERMS),
        paymentDetailsMap.get(IFeatureFileCommonKeys.CURRENCY),
        servicePOCode
    };
  }
}
