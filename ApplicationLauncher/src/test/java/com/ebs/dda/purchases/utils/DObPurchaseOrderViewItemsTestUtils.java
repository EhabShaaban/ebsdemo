package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IOrderItemsResponseKeys;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import cucumber.api.DataTable;
import io.restassured.response.Response;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class DObPurchaseOrderViewItemsTestUtils extends DObPurchaseOrderItemTestUtils {

  protected static final String DATA_ITEMS = "data.items";
  protected static final String CODE = "Code";
  protected static String ITEM_QUANTITIES;
  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Items]";


  public DObPurchaseOrderViewItemsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public static void setItemQuantities(String itemQuantities) {
    ITEM_QUANTITIES = itemQuantities;
  }

  public void assertThatActualPurchaseOrderItemsAreCorrect(
      Response response, DataTable purchaseOrderItems) throws Exception {

    List<Map<String, String>> expectedItems = purchaseOrderItems.asMaps(String.class, String.class);
    List<Map<String, Object>> actualItems = response.body().jsonPath().getList(DATA_ITEMS);
    int matchedItems = 0;
    for (Map<String, Object> actualItem : actualItems) {
      for (Map<String, String> expectedItem : expectedItems) {
        if (actualItem
            .get(IOrderItemsResponseKeys.ITEM_CODE)
            .equals(expectedItem.get(IFeatureFileCommonKeys.ITEM_CODE))) {
          expectedItem = convertEmptyStringsToNulls(expectedItem);
          matchedItems++;
          assertItemName(expectedItem, actualItem);
          assertItemQuantityInDN(expectedItem, actualItem);
          assertItemBaseUnitSymbol(expectedItem, actualItem);
          break;
        }
      }
    }
    assertEquals(ASSERTION_MSG + " Expected Items Size " + expectedItems.size() + " is not correct",
    expectedItems.size(), matchedItems);
  }

  private void assertItemName(Map<String, String> expectedItem, Map<String, Object> actualItem)
      throws Exception {
    assertEquals(ASSERTION_MSG + " Item Name " + expectedItem.get(IFeatureFileCommonKeys.ITEM_NAME) + " is not correct",
        expectedItem.get(IFeatureFileCommonKeys.ITEM_NAME),
        getEnglishValue(actualItem.get(IOrderItemsResponseKeys.ITEM_NAME)));
  }

  private void assertItemQuantityInDN(
      Map<String, String> expectedItem, Map<String, Object> actualItem) {
    Object actualITemQnt = actualItem.get(IPurchaseOrderItemValueObject.QUANTITY_IN_DN);
    String actualQuantity = actualITemQnt == null ? null : String.valueOf(actualITemQnt);
    assertEquals(ASSERTION_MSG + " QtyInDN " + expectedItem.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN) + " is not correct",
    expectedItem.get(IFeatureFileCommonKeys.ITEM_QUANTITY_IN_DN), actualQuantity);
  }

  private void assertItemBaseUnitSymbol(
      Map<String, String> expectedItem, Map<String, Object> actualItem) throws Exception {
    assertEquals(ASSERTION_MSG + " Item BaseUnit " + expectedItem.get(IFeatureFileCommonKeys.ITEM_BASE_UNIT) + " is not correct",
        expectedItem.get(IFeatureFileCommonKeys.ITEM_BASE_UNIT),
        getEnglishValue(actualItem.get(IOrderItemsResponseKeys.ITEM_UNIT_SYMBOL)));
  }

  protected void assertResponseItemQuantity(
      Map<String, String> expectedQuantity, Map<String, Object> actualQuantity) {
    BigDecimal expected =
            new BigDecimal(expectedQuantity.get(IFeatureFileCommonKeys.QUANTITY));
    BigDecimal actual = new BigDecimal((String) actualQuantity.get(IOrderItemsResponseKeys.ITEM_QUANTITY));
    assertEquals(ASSERTION_MSG + " Quantity " + expectedQuantity.get(IFeatureFileCommonKeys.QUANTITY) + " is not correct",
        0,
        expected.compareTo(actual));
  }

  protected boolean isSameItemQuantityOrderUnit(
      Map<String, String> expectedQuantity, Map<String, Object> actualQuantity) throws Exception {
    return getEnglishValue(actualQuantity.get(IOrderItemsResponseKeys.ITEM_QUANTITY_UNIT_SYMBOL))
        .equals(expectedQuantity.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT));
  }

  public void assertEmptyResponseOfQuantities(
      Response purchaseOrderItemsDataResponse, String itemCode) {
    List<HashMap<String, Object>> items =
        purchaseOrderItemsDataResponse.body().jsonPath().getList(DATA_ITEMS);
    for (HashMap<String, Object> item : items) {
      if (item.get(IOrderItemsResponseKeys.ITEM_CODE).equals(itemCode)) {
        assertTrue(ASSERTION_MSG + " Size of Item Quantities List " + ((List<HashMap<String, Object>>) item.get(ITEM_QUANTITIES)).isEmpty()
        + " is not correct for Item with code " + itemCode,
        ((List<HashMap<String, Object>>) item.get(ITEM_QUANTITIES)).isEmpty());
      }
    }
  }

  public void assertEmptyResponseOfItems(Response purchaseOrderItemsResponse) {
    List<HashMap<String, Object>> items =
        purchaseOrderItemsResponse.body().jsonPath().getList(DATA_ITEMS);
    assertTrue(ASSERTION_MSG + " Item List is not empty",items.isEmpty());
  }

  public void assertEmptyOrderItemsInPurchaseOrders(DataTable purchaseOrderDataTable) {
    List<Map<String, String>> purchaseOrders =
        purchaseOrderDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrders) {
      String purchaseOrderCode = purchaseOrder.get(CODE);
      List<Object[]> actualData =
          entityManagerDatabaseConnector.executeNativeNamedQuery(
              "getItemsForPurchaseOrder", purchaseOrderCode);
      assertTrue(ASSERTION_MSG + " Item List is not empty", actualData.isEmpty());
    }
  }

  public void assertThatItemsExistInPurchaseOrder(
      String purchaseOrderCode, DataTable orderItemsDataTable) {
    List<Map<String, String>> itemsLists = orderItemsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> item : itemsLists) {
      assertItemExistsInPurchaseOrder(purchaseOrderCode, item);
    }
  }

  public void assertEmptyQuantitiesInItemInPurchaseOrder(
      String itemCode, String purchaseOrderCode) {
    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getQuantitiesInItemsInPurchaseOrder", purchaseOrderCode, itemCode);
    assertTrue(ASSERTION_MSG + " Item Quantities is not empty in item with code " + itemCode + " for PO with code " + purchaseOrderCode,
    actualData.isEmpty());
  }
}
