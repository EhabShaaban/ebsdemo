package com.ebs.dda.purchases.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/ordermanagement/purchase-order/PO_OpenForUpdates.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Cancel.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Approve.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Approve_Assign_Me_To_Approvers.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Reject.feature",
      "classpath:features/ordermanagement/purchase-order/PO_SubmitForApproval.feature",
      "classpath:features/ordermanagement/purchase-order/PO_MarkAsPIRequested.feature",
      "classpath:features/ordermanagement/purchase-order/PO_MarkAsProductionFinished.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Confirm.feature",
      "classpath:features/ordermanagement/purchase-order/PO_MarkAsCleared.feature",
      "classpath:features/ordermanagement/purchase-order/PO_MarkAsArrived.feature",
      "classpath:features/ordermanagement/purchase-order/PO_MarkAsShipped.feature",
      "classpath:features/ordermanagement/purchase-order/PO_MarkAsDeliveryComplete.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.purchases"},
    strict = true,
    tags = "not @Future")
public class DObPurchaseOrderStateChangeCucumberRunner {}
