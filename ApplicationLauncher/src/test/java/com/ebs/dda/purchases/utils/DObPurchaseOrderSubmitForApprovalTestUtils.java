package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertTrue;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

public class DObPurchaseOrderSubmitForApprovalTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Submit][For][Approval]";

  public DObPurchaseOrderSubmitForApprovalTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");

    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/CObApprovalPoliciesSqlScript.sql");
    return str.toString();
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDocumentRequiredDocuments.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    return str.toString();
  }

  public Response submitForApproval(String purchaseOrderCode, Cookie userCookie) {
    return sendPUTRequest(
        userCookie,
        IPurchaseOrderRestUrls.SUBMIT_FOR_APPROVAL + purchaseOrderCode,
        new JsonObject());
  }

  public void assertThatControlPointsExist(DataTable controlPointsDatatable) {
    List<Map<String, String>> controlPoints =
        controlPointsDatatable.asMaps(String.class, String.class);
    for (Map<String, String> controlPoint : controlPoints) {
      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
          "getControlPointUser",
          controlPoint.get(IFeatureFileCommonKeys.CONTROL_POINT),
          controlPoint.get(IFeatureFileCommonKeys.USER),
          Boolean.valueOf(controlPoint.get(IFeatureFileCommonKeys.IS_MAIN)));
    }
  }

  public void assertThatApprovalPoliciesExist(DataTable approvalPoliciesDatatable) {
    List<Map<String, String>> approvalPolicies =
        approvalPoliciesDatatable.asMaps(String.class, String.class);
    for (Map<String, String> approvalPolicy : approvalPolicies) {
      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
          "getApprovalPolicy",
          approvalPolicy.get(IFeatureFileCommonKeys.CODE),
          approvalPolicy.get(IFeatureFileCommonKeys.DOCUMENT_TYPE),
          approvalPolicy.get(IFeatureFileCommonKeys.CONDITION));
    }
  }

  public void assertThatApprovalPolicyControlPointsExist(
      String approvalPolicyCode, DataTable controlPointsDatatable) {
    List<Map<String, String>> expectedControlPoints =
        controlPointsDatatable.asMaps(String.class, String.class);
    List<Object[]> actualControlPoints =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getApprovalPolicyControlPoints", approvalPolicyCode);
    assertTrue(
        ASSERTION_MSG + " Control Points Size " + expectedControlPoints.size() + " is not correct",
        actualControlPoints.size() == expectedControlPoints.size());
    for (Map<String, String> expectedControlPoint : expectedControlPoints) {
      actualControlPoints.contains(expectedControlPoint.get(IFeatureFileCommonKeys.CONTROL_POINT));
    }
  }
}
