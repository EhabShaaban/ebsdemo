package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderCancelTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderEditCompanyTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSubmitForApprovalTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderEditCompanyDataStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderEditCompanyTestUtils utils;
  private DObPurchaseOrderSubmitForApprovalTestUtils submitForApprovalTestUtils;
  private DObPurchaseOrderCancelTestUtils purchaseOrderCancelTestUtils;

  public DObPurchaseOrderEditCompanyDataStep() {

    When(
        "^\"([^\"]*)\" requests to edit Company section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          String lock_url = IPurchaseOrderRestUrls.LOCK_COMPANY_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Company section of PurchaseOrder with code \"([^\"]*)\" becomes in the edit mode and locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.COMPANY_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened Company section of PurchaseOrder with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String purchaseOrderCode) -> {
          String lock_url = IPurchaseOrderRestUrls.LOCK_COMPANY_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^Company section of PurchaseOrder with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          Response response =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_COMPANY_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.COMPANY_SECTION);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving Company section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unLockURL = IPurchaseOrderRestUrls.UNLOCK_COMPANY_SECTION + purchaseOrderCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving Company section of PurchaseOrder with Code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          String unLockURL = IPurchaseOrderRestUrls.UNLOCK_COMPANY_SECTION + purchaseOrderCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Company section of PurchaseOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String username) -> {
          utils.assertThatLockIsReleased(
              username, purchaseOrderCode, IPurchaseOrderSectionNames.COMPANY_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first SubmittedForApproval the PurchaseOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String submitTime) -> {
          utils.freeze(submitTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              submitForApprovalTestUtils.submitForApproval(purchaseOrderCode, cookie);
          utils.assertResponseSuccessStatus(response);
        });

    When(
        "^\"([^\"]*)\" requests to edit Company section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = IPurchaseOrderRestUrls.LOCK_COMPANY_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" first Cancelled the PurchaseOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String cancelDataTime) -> {
          utils.freeze(cancelDataTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              purchaseOrderCancelTestUtils.cancelPurchaseOrder(purchaseOrderCode, cookie);
          utils.assertResponseSuccessStatus(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderEditCompanyTestUtils(getProperties());
    submitForApprovalTestUtils = new DObPurchaseOrderSubmitForApprovalTestUtils(getProperties());
    purchaseOrderCancelTestUtils = new DObPurchaseOrderCancelTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Company section")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Company section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          purchaseOrderCancelTestUtils.getSectionsNames());
    }
  }
}
