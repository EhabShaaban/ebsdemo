package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.enterprise.CObEnterpriseTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewGeneralDataTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderViewGeneralDataStep extends SpringBootRunner implements En {

  private DObPurchaseOrderViewGeneralDataTestUtils utils;
  private CObEnterpriseTestUtils enterpriseTestUtils;

  public DObPurchaseOrderViewGeneralDataStep() {

    Given(
        "^the following Purchasing Responsibles exist:$",
        (DataTable purchaseUnits) -> {
          enterpriseTestUtils.assertThatPurchaseResponsiblesExist(purchaseUnits);
        });

    When(
        "^\"([^\"]*)\" requests to view General Data section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readPurchaseOrderHeader(userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of General Data section are displayed to \"([^\"]*)\":$",
        (String userName, DataTable purchaseOrderTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatGeneralDataIsDisplayedToTheUserInResponse(purchaseOrderTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderViewGeneralDataTestUtils(getProperties());
    enterpriseTestUtils = new CObEnterpriseTestUtils(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View General Data section")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
