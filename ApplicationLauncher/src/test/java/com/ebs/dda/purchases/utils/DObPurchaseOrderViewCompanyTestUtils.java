package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderViewCompanyTestUtils extends DObPurchaseOrderCompanyTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Company]";

  public DObPurchaseOrderViewCompanyTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = getProperty(URL_PREFIX).toString();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public Response readPurchaseOrderCompanyData(Cookie cookie, String puchaseOrderCode) {
      String restURL = getViewCompanyUrl(puchaseOrderCode);
    return sendGETRequest(cookie, restURL);
  }

    private String getViewCompanyUrl(String purchaseOrderCode) {
        return IPurchaseOrderRestUrls.QUERY_PURCHASE_ORDER
                + purchaseOrderCode
                + IPurchaseOrderRestUrls.VIEW_COMPANY;
    }

  public void assertThatCompanyDataAreDisplayedToTheUserInResponse(
      DataTable purchaseOrderTable, Response purchaseOrderDataResponse) throws Exception {
    Map<String, String> expectedCompanyData =
        purchaseOrderTable.asMaps(String.class, String.class).get(0);
    Map<String, Object> actualCompanyData = purchaseOrderDataResponse.body().jsonPath().get("data");

    MapAssertion mapAssertion = new MapAssertion(expectedCompanyData, actualCompanyData);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BUSINESS_UNIT,
        IDObPurchaseOrderGeneralModel.PURCHASE_UNIT_CODE,
        IDObPurchaseOrderGeneralModel.PURCHASING_UNIT_NAME_ATTR_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.COMPANY,
        IDObPurchaseOrderGeneralModel.COMPANY_CODE,
        IDObPurchaseOrderGeneralModel.COMPANY_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER,
            IDObPurchaseOrderGeneralModel.BANK_ACCOUNT_NUMBER,
        IDObPurchaseOrderGeneralModel.BANK_NAME);
  }
}
