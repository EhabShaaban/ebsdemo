package com.ebs.dda.purchases.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderPaymentDetailsGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertTrue;

public class DObServicePurchaseOrderViewPaymentDetailsTestUtils
    extends DObPurchaseOrderCommonTestUtils {

  public DObServicePurchaseOrderViewPaymentDetailsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public Response readPurchaseOrderPaymentTermData(Cookie cookie, String purchaseOrderCode) {
    String restURL = getViewPaymentTermUrl(purchaseOrderCode);
    return sendGETRequest(cookie, restURL);
  }

  private String getViewPaymentTermUrl(String purchaseOrderCode) {
    return IPurchaseOrderRestUrls.QUERY_PURCHASE_ORDER
        + purchaseOrderCode
        + IPurchaseOrderRestUrls.VIEW_SERVICE_PAYMENT_TERM;
  }

  public void assertThatReadPaymentTermDataResponseIsCorrect(
      DataTable paymentTermDataTable, Response paymentTermDataResponse) throws Exception {
    Map<String, String> expectedPaymentTermData =
        paymentTermDataTable.asMaps(String.class, String.class).get(0);

    HashMap<String, Object> actualPaymentTermData = getMapFromResponse(paymentTermDataResponse);

    MapAssertion mapAssertion = new MapAssertion(expectedPaymentTermData, actualPaymentTermData);
    mapAssertion.assertValue(
        IOrderFeatureFileCommonKeys.CURRENCY_ISO, IIObOrderPaymentDetailsGeneralModel.CURRENCY_ISO);
    mapAssertion.assertCodeAndLocalizedNameValue(
        IOrderFeatureFileCommonKeys.PAYMENT_TERMS,
        IIObOrderPaymentDetailsGeneralModel.PAYMENT_TERM_CODE,
        IIObOrderPaymentDetailsGeneralModel.PAYMENT_TERM_NAME);
  }

  public void assertEmptyPaymentDetailsInPurchaseOrder(String purchaseOrderCode) {
    Object paymentDetailsId =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPaymentDetailsForPurchaseOrder", purchaseOrderCode);
    assertTrue(ASSERTION_MSG + " PaymentDetails is not empty", paymentDetailsId == null);
  }

  public void assertEmptyResponseOfPaymentDetails(Response purchaseOrderPaymentDetailsResponse) {
    HashMap<String, Object> actualPaymentTermData =
        getMapFromResponse(purchaseOrderPaymentDetailsResponse);

    assertTrue(ASSERTION_MSG + " PaymentDetails is not empty", actualPaymentTermData == null);
  }
}
