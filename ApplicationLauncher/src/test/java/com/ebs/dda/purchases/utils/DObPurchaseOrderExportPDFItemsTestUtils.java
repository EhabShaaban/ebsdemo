package com.ebs.dda.purchases.utils;

import java.util.Map;

public class DObPurchaseOrderExportPDFItemsTestUtils extends DObPurchaseOrderExportPDFTestUtils {

  public DObPurchaseOrderExportPDFItemsTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public String getNewItemVendorRecordDataDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    return str.toString();
  }

}
