package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderAssignMeToApproveTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSubmitForApprovalTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewApprovalCyclesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderSubmitForApprovalStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderSubmitForApprovalTestUtils submitForApprovalTestUtils;
  private DObPurchaseOrderViewApprovalCyclesTestUtils approvalCyclesTestUtils;
  private DObPurchaseOrderAssignMeToApproveTestUtils assignMeTestUtils;

  public DObPurchaseOrderSubmitForApprovalStep() {

    Given(
        "^the following control points exist:$",
        (DataTable controlPointsDatatable) -> {
          submitForApprovalTestUtils.assertThatControlPointsExist(controlPointsDatatable);
        });

    Given(
        "^the following approval policies exist:$",
        (DataTable approvalPoliciesDatatable) -> {
          submitForApprovalTestUtils.assertThatApprovalPoliciesExist(approvalPoliciesDatatable);
        });

    Given(
        "^Approval policy \"([^\"]*)\" has the following control points:$",
        (String approvalPolicyCode, DataTable controlPointsDatatable) -> {
          submitForApprovalTestUtils.assertThatApprovalPolicyControlPointsExist(
              approvalPolicyCode, controlPointsDatatable);
        });

    When(
        "^\"([^\"]*)\" requests to SubmitForApproval the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String purchaseOrderCode, String cancelTime) -> {
          submitForApprovalTestUtils.freeze(cancelTime);
          Response response =
              submitForApprovalTestUtils.submitForApproval(
                  purchaseOrderCode, userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^PurchaseOrder \"([^\"]*)\" has the following approval cycles:$",
        (String purchaseOrderCode, DataTable approvalCyclesDataTable) -> {
          approvalCyclesTestUtils.assertThatPurchaseOrderApprovalCyclesExist(
              purchaseOrderCode, approvalCyclesDataTable);
        });
    Then(
        "^the new ApprovalCycleNo \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" has the following values:$",
        (String approvalCycleNum, String purchaseOrderCode, DataTable approvalCycleDetails) -> {
          assignMeTestUtils.assertThatApprovalCycleDetailsAreCorrect(
              approvalCycleNum, purchaseOrderCode, approvalCycleDetails);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    submitForApprovalTestUtils = new DObPurchaseOrderSubmitForApprovalTestUtils(properties);
    submitForApprovalTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    approvalCyclesTestUtils = new DObPurchaseOrderViewApprovalCyclesTestUtils(properties);
    approvalCyclesTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    assignMeTestUtils = new DObPurchaseOrderAssignMeToApproveTestUtils(properties);
    assignMeTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "SubmitForApproval PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            submitForApprovalTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(submitForApprovalTestUtils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "SubmitForApproval PO")) {
      submitForApprovalTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          submitForApprovalTestUtils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "SubmitForApproval PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(submitForApprovalTestUtils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
