package com.ebs.dda.purchases.utils;

import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderSaveConsigneeTestUtils extends DObPurchaseOrderConsigneeTestUtils {

  private final String REQUEST_CONSIGNEE_CODE = "consigneeCode";
  private final String REQUEST_PLANT_CODE = "plantCode";
  private final String REQUEST_STOREHOUSE_CODE = "storehouseCode";

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Save]";

  public DObPurchaseOrderSaveConsigneeTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = getProperty(URL_PREFIX).toString();
  }

  public void assertThatPlantsRelateToCompany(DataTable plantsDataTable, String companyCode) {
    List<Map<String, String>> plantMapList = plantsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> plantMap : plantMapList) {
      Object plant =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPlantByCompanyCode", getPlantQueryParams(plantMap, companyCode));

      assertNotNull(
          ASSERTION_MSG
              + " Company "
              + companyCode
              + " does not have plant "
              + plantMap.get(ENTITY_CODE),
          plant);
    }
  }

  private Object[] getPlantQueryParams(Map<String, String> plantMap, String companyCode) {
    return new Object[] {plantMap.get(ENTITY_CODE), companyCode};
  }

  public void assertThatStorehousesRelateToPlant(DataTable storehousesDataTable, String plantCode) {
    List<Map<String, String>> storehouseMapList =
        storehousesDataTable.asMaps(String.class, String.class);
    for (Map<String, String> storehouseMap : storehouseMapList) {
      Object storehouse =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getStorehouseByPlantCode", getStorehouseQueryParams(storehouseMap, plantCode));

      assertNotNull(
          ASSERTION_MSG
              + " Plant "
              + plantCode
              + " does not have storehouse "
              + storehouseMap.get(ENTITY_CODE),
          storehouse);
    }
  }

  private Object[] getStorehouseQueryParams(Map<String, String> storehouseMap, String plantCode) {
    return new Object[] {storehouseMap.get(ENTITY_CODE), plantCode};
  }

  public void assertThatConsigneeUpdatedWithGivenValues(
      String purchaseOrderCode, DataTable consigneeData) throws Exception {

    Map<String, String> expectedConsigneeMap =
        consigneeData.asMaps(String.class, String.class).get(0);

    Object consigneeModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedConsigneeData",
            getUpdatedConsigneeQueryParams(expectedConsigneeMap, purchaseOrderCode));

    assertNotNull(ASSERTION_MSG + " Consignee is not updated", consigneeModifiedDate);
    assertDateEquals(
        expectedConsigneeMap.get(UPDATED_DATE),
        new DateTime(consigneeModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] getUpdatedConsigneeQueryParams(
      Map<String, String> expectedConsigneeMap, String purchaseOrderCode) {
    return new Object[] {
      expectedConsigneeMap.get(CONSIGNEE),
      expectedConsigneeMap.get(PLANT),
      expectedConsigneeMap.get(STOREHOUSE),
      purchaseOrderCode,
      expectedConsigneeMap.get(UPDATED_BY)
    };
  }

  public Response savePurchaseOrderConsigneeSection(
      Cookie loginCookie, String purchaseOrderCode, DataTable consigneeValuesTable) {
    JsonObject consigneeJsonData = getConsigneeJsonObject(consigneeValuesTable);
    String restURL = "/command/purchaseorder/update/consignee/" + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, consigneeJsonData);
  }

  private JsonObject getConsigneeJsonObject(DataTable consigneeValuesTable) {
    Map<String, String> consignee = consigneeValuesTable.asMaps(String.class, String.class).get(0);
    JsonObject consigneeJsonData = new JsonObject();
    consigneeJsonData.addProperty(REQUEST_CONSIGNEE_CODE, consignee.get(CONSIGNEE));
    consigneeJsonData.addProperty(REQUEST_PLANT_CODE, consignee.get(PLANT));
    consigneeJsonData.addProperty(REQUEST_STOREHOUSE_CODE, consignee.get(STOREHOUSE));
    return consigneeJsonData;
  }
}
