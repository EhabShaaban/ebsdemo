package com.ebs.dda.purchases.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import cucumber.api.DataTable;
import org.junit.Assert;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class DObServicePurchaseOrderDeleteItemsTestUtils extends DObPurchaseOrderCommonTestUtils {

  public String ASSERTION_MSG = super.ASSERTION_MSG + "[Delete][Items][Service]";

  public DObServicePurchaseOrderDeleteItemsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public String getDeleteServicePurchaseOrderItemRestUrl(String servicePOCode, Integer itemId) {
    StringBuilder deleteUrl = new StringBuilder();
    deleteUrl.append(IPurchaseOrderRestUrls.COMMAND_PURCHASE_ORDER);
    deleteUrl.append(servicePOCode);
    deleteUrl.append(IPurchaseOrderRestUrls.VIEW_ITEM_SECTION);
    deleteUrl.append("/");
    deleteUrl.append(itemId);
    return deleteUrl.toString();
  }

  public void assertThatServicePurchaseOrderItemIsDeletedSuccessfully(
      String servicePOCode, Integer itemId) {
    Object actualServicePurchaseOrderItem =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getServicePurchaseOrderItemByServicePurchaseOrderCodeAndItemId",
            servicePOCode,
            itemId);
    assertNull(
        ASSERTION_MSG + "Service Purchase Order Item does not deleted successfully",
        actualServicePurchaseOrderItem);
  }

  public void assertThatServicePurchaseOrderItemDataExists(DataTable itemDataTable) {
    List<Map<String, String>> itemDataListMap = itemDataTable.asMaps(String.class, String.class);

    for (Map<String, String> itemDataMap : itemDataListMap) {
      String servicePOCode = itemDataMap.get(IFeatureFileCommonKeys.CODE);
      assertThatServicePurchaseOrderItemExists(servicePOCode, itemDataMap);
    }
  }

  private void assertThatServicePurchaseOrderItemExists(
      String servicePOCode, Map<String, String> itemDataMap) {
    Object item =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getIObOrderItem", constructServicePurchaseOrderItemDataQueryParams(itemDataMap));

    Assert.assertNotNull(
        ASSERTION_MSG + " Item data of service purchase order " + servicePOCode + " does not exist",
        item);
  }

  private Object[] constructServicePurchaseOrderItemDataQueryParams(
      Map<String, String> itemDataMap) {
    return new Object[] {
      itemDataMap.get(IFeatureFileCommonKeys.CODE),
      Long.valueOf(itemDataMap.get(IOrderFeatureFileCommonKeys.PO_ITEM_ID)),
      itemDataMap.get(IFeatureFileCommonKeys.ITEM),
      itemDataMap.get(IFeatureFileCommonKeys.ITEM_ORDER_UNIT),
      itemDataMap.get(IOrderFeatureFileCommonKeys.QUANTITY),
      itemDataMap.get(IOrderFeatureFileCommonKeys.PRICE),
      itemDataMap.get(IOrderFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT)
    };
  }

  public void assertThatTheseAreTheOnlyItemsExist(String servicePOCode, DataTable itemDataTable) {
    List<Map<String, String>> expectedServicePurchaseOrderItemsList =
        itemDataTable.asMaps(String.class, String.class);
    Object actualServicePurchaseOrderItemsCount =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getCountIObOrderItemByPurchaseOrderCode", servicePOCode);
    assertEquals(
        "These are not only items exist in service purchase order",
        (long) expectedServicePurchaseOrderItemsList.size(),
        actualServicePurchaseOrderItemsCount);
  }
}
