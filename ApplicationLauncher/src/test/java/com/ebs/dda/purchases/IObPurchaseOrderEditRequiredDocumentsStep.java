package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.IObPurchaseOrderRequiredDocumentsTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class IObPurchaseOrderEditRequiredDocumentsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObPurchaseOrderRequiredDocumentsTestUtils utils;

  public IObPurchaseOrderEditRequiredDocumentsStep() {

    When(
        "^\"([^\"]*)\" requests to edit RequiredDocuments section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Response response =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_REQUIRED_DOCUMENTS_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^RequiredDocuments section of PurchaseOrder with code \"([^\"]*)\" becomes in the edit mode and locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          Response response =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_REQUIRED_DOCUMENTS_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^RequiredDocuments section of PurchaseOrder with code \"([^\"]*)\" becomes in the edit mode and locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          Response userResponse = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(userResponse);
        });

    Given(
        "^\"([^\"]*)\" first opened RequiredDocuments section of PurchaseOrder with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String purchaseOrderCode) -> {
          Response response =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_REQUIRED_DOCUMENTS_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
          utils.assertResponseSuccessStatus(userActionsTestUtils.getUserResponse(userName));
        });

    When(
        "^\"([^\"]*)\" cancels saving RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String date) -> {
          utils.freeze(date);
          Response response =
              userActionsTestUtils.unlockSection(
                  userName,
                  IPurchaseOrderRestUrls.UNLOCK_REQUIRED_DOCUMENTS_SECTION + purchaseOrderCode);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving RequiredDocuments section of PurchaseOrder with Code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Response response =
              userActionsTestUtils.unlockSection(
                  userName,
                  IPurchaseOrderRestUrls.UNLOCK_REQUIRED_DOCUMENTS_SECTION + purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^RequiredDocuments section of PurchaseOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String username) -> {
          utils.assertThatLockIsReleased(
              username, purchaseOrderCode, IPurchaseOrderSectionNames.REQUIRED_DOCUMENTS_SECTION);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObPurchaseOrderRequiredDocumentsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit RequiredDocuments section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Request to edit RequiredDocuments section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit RequiredDocuments section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
