package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.purchases.IExpectedKeys;
import cucumber.api.DataTable;
import java.util.List;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class PurchaseOrderAttachmentTestUtils extends DObPurchaseOrderCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[Attachments]";

  public String getDbScriptsOneTimeExecution() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/CObApprovalPoliciesSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/attachments.sql");
    return str.toString();
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/attachments.sql");
    return str.toString();
  }

  public void assertThatDocumentTypesExist(DataTable documentTypesDataTable) {
    List<Map<String, String>> documentTypeMapList =
        documentTypesDataTable.asMaps(String.class, String.class);

    for (Map<String, String> documentTypeMap : documentTypeMapList) {
      Object documentType =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getDocumentTypeByCodeAndName", getDocumentTypeQueryParams(documentTypeMap));

      assertNotNull(ASSERTION_MSG + " Document type does not exist", documentType);
    }
  }

  private Object[] getDocumentTypeQueryParams(Map<String, String> documentTypeMap) {
    return new Object[] {
      documentTypeMap.get(IExpectedKeys.DOCUMENT_TYPE_CODE),
      documentTypeMap.get(IExpectedKeys.DOCUMENT_TYPE_NAME)
    };
  }

  public void assertThatPurchaseOrderHasAttachments(
      String purchaseOrderCode, DataTable orderAttachments) {

    List<Map<String, String>> attachmentMapList =
        orderAttachments.asMaps(String.class, String.class);

    for (Map<String, String> attachmentMap : attachmentMapList) {
      Object attachment =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getPOAttachmentByOrderCode",
              getAttachmentQueryParams(attachmentMap, purchaseOrderCode));

      assertNotNull(
          ASSERTION_MSG
              + " Attachment "
              + attachmentMap.get(IExpectedKeys.ATTACHMENT)
              + " does not exist",
          attachment);
    }
  }

  private Object[] getAttachmentQueryParams(
      Map<String, String> attachmentMap, String purchaseOrderCode) {
    return new Object[] {
      purchaseOrderCode,
      attachmentMap.get(IExpectedKeys.ATTACHMENT),
      attachmentMap.get(IExpectedKeys.UPLOADED_BY),
      attachmentMap.get(IExpectedKeys.DOCUMENT_TYPE_CODE),
      attachmentMap.get(IExpectedKeys.UPLOAD_DATE_TIME)
    };
  }
}
