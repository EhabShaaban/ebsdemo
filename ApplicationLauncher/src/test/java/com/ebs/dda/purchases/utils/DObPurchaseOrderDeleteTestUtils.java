package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNull;

import java.util.Map;

public class DObPurchaseOrderDeleteTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Delete]";

  public DObPurchaseOrderDeleteTestUtils(Map<String, Object> properties) {
    super();
    setProperties(properties);
  }

  public String getDbScriptPath() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public void assertThatPurchaseOrderIsDeleted(String userCode) {
    Object order =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPurchaseOrderByCode", userCode);
    assertNull(ASSERTION_MSG + " Purchase Order " + userCode + " is not deleted", order);
  }
}
