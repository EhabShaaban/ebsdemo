package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderRejectTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderStateTransitionTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewApprovalCyclesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderRejectStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderRejectTestUtils rejectTestUtils;
  private DObPurchaseOrderViewApprovalCyclesTestUtils approvalCyclesTestUtils;
  private DObPurchaseOrderStateTransitionTestUtils stateTransitionUtils;

  public DObPurchaseOrderRejectStep() {

    When(
        "^\"([^\"]*)\" requests to Reject the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String username,
            String purchaseOrderCode,
            String approvalDateTime,
            DataTable approvalData) -> {
          rejectTestUtils.freeze(approvalDateTime);
          Response approveResponse =
              rejectTestUtils.rejectPurchaseOrder(
                  userActionsTestUtils.getUserCookie(username), purchaseOrderCode, approvalData);
          userActionsTestUtils.setUserResponse(username, approveResponse);
        });

    Then(
        "^the following ApprovalCycles in PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable approvalCyclesDataTable) -> {
          approvalCyclesTestUtils.assertThatPurchaseOrderApprovalCyclesExist(
              purchaseOrderCode, approvalCyclesDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    rejectTestUtils = new DObPurchaseOrderRejectTestUtils(properties);
    approvalCyclesTestUtils = new DObPurchaseOrderViewApprovalCyclesTestUtils(properties);
    stateTransitionUtils = new DObPurchaseOrderStateTransitionTestUtils(properties);
    approvalCyclesTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    stateTransitionUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Reject the PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(rejectTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Reject the PO")) {
      rejectTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, rejectTestUtils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Reject the PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(rejectTestUtils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
