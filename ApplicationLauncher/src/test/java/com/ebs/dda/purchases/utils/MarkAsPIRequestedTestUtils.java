package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.List;
import java.util.Map;

public class MarkAsPIRequestedTestUtils extends DObPurchaseOrderCommonTestUtils {
  String readRestUrl;
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Mark][As][PI][Requested]";


  public MarkAsPIRequestedTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    updateRestURLs();
  }

  protected void updateRestURLs() {
    readRestUrl = IPurchaseOrderRestUrls.AUTHORIZED_ACTIONS;
  }

  public Response markAsPIRequested(
      Cookie loginCookie, String purchaseOrderCode, DataTable companyDataTable) {
    JsonObject markPIRequestJsonData = getMarkAsPIRequestedJsonObject(companyDataTable);
    String restURL = IPurchaseOrderRestUrls.MARK_AS_PI_REQUEST + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, markPIRequestJsonData);
  }

  private JsonObject getMarkAsPIRequestedJsonObject(DataTable purchaseOrderPIRequestedDataTable) {
    JsonObject markPIRequestJsonData = new JsonObject();
    Map<String, String> piRequestData =
        purchaseOrderPIRequestedDataTable.asMaps(String.class, String.class).get(0);
    markPIRequestJsonData.addProperty("pIRequestedDate", piRequestData.get("PIRequestDate"));
    return markPIRequestJsonData;
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
        str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    return str.toString();
  }

  public String getDbScriptsAfterEachScenario() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/markaspirequested/purchase-order.sql");
    str.append(
        ","
            + "db-scripts/purchase-order/markaspirequested/local-purchase-order-payment-delivery.sql");
    str.append("," + "db-scripts/purchase-order/markaspirequested/purchase-order_item.sql");

    str.append("," + "db-scripts/purchase-order/markaspirequested/IObOrderDocumentRequiredDocuments.sql");

    return str.toString();
  }

  public void assertNotItemExistInPurchaseOrder(String purchaseOrderCode) {
    Object poItems =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPOItemsByOrderCode", purchaseOrderCode);

    assertNull(ASSERTION_MSG + " Items list of " + purchaseOrderCode + " is not empty", poItems);
  }

  public void assertPurchaseOrderItemHasNoQuantities(String purchaseOrderCode, String itemCode) {
    Object poItemQuantities =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getItemQuantitiesByPOCodeAndItemCode",
            getItemQuantitiesQueryParams(purchaseOrderCode, itemCode));

    assertNull(
        ASSERTION_MSG
            + " Quantities list of item "
            + itemCode
            + " of order "
            + purchaseOrderCode
            + " is not empty",
        poItemQuantities);
  }

  private Object[] getItemQuantitiesQueryParams(String purchaseOrderCode, String itemCode) {
    return new Object[] {itemCode, purchaseOrderCode};
  }

  public void assertThatPOMarkedAsPIRequested(
      String purchaseOrderCode, DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderData =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);
    Object result =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getMarkAsPIRequestedPurchaseOrder",
            purchaseOrderCode,
            purchaseOrderData.get("LastUpdatedBy"),
            getDateTimeInMachineTimeZone(purchaseOrderData.get("LastUpdateDate")),
            getDateTimeInMachineTimeZone(purchaseOrderData.get("PIRequestDate")));
    assertNotNull(ASSERTION_MSG + " There is no Mark As PI Requested PO matched code " + purchaseOrderCode,
    result);
    List<String> states = getStatesArray(result.toString());
    assertThatStatesArrayContainsValue(states, purchaseOrderData.get("State"));
  }
}
