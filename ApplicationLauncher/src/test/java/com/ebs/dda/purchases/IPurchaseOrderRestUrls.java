package com.ebs.dda.purchases;

public interface IPurchaseOrderRestUrls {

  String COMMAND_PURCHASE_ORDER = "/command/purchaseorder/";
  String QUERY_PURCHASE_ORDER = "/services/purchase/orders/";

  String LOCK_HEADER_SECTION = COMMAND_PURCHASE_ORDER + "lockheader/";
  String UNLOCK_HEADER_SECTION = COMMAND_PURCHASE_ORDER + "unlockheader/";

  String LOCK_COMPANY_SECTION = COMMAND_PURCHASE_ORDER + "lock/companysection/";
  String UNLOCK_COMPANY_SECTION = COMMAND_PURCHASE_ORDER + "unlock/companysection/";

  String LOCK_CONSIGNEE_SECTION = COMMAND_PURCHASE_ORDER + "consignee/lockheader/";
  String UNLOCK_CONSIGNEE_SECTION = COMMAND_PURCHASE_ORDER + "consignee/unlockheader/";

  String LOCK_PAYMENT_DELIVERY_SECTION = COMMAND_PURCHASE_ORDER + "lock/paymentanddeliverysection/";
  String UNLOCK_PAYMENT_DELIVERY_SECTION =
      COMMAND_PURCHASE_ORDER + "unlock/paymentanddeliverysection/";
  String SAVE_PAYMENT_DELIVERY_SECTION = COMMAND_PURCHASE_ORDER + "savepaymentanddelivery/";

  String LOCK_REQUIRED_DOCUMENTS_SECTION = COMMAND_PURCHASE_ORDER + "lock/requireddocuments/";
  String UNLOCK_REQUIRED_DOCUMENTS_SECTION = COMMAND_PURCHASE_ORDER + "unlock/requireddocuments/";

  String VIEW_APPROVAL_CYCLES_SECTION = QUERY_PURCHASE_ORDER + "approvalcycles/";
  String VIEW_ATTACHMETNS_SECTION = QUERY_PURCHASE_ORDER + "attachments/";

  String VIEW_ESTIMATED_COST_SECTION = QUERY_PURCHASE_ORDER + "estimatedcosts/";

  String UNLOCK_LOCKED_SECTIONS = COMMAND_PURCHASE_ORDER + "unlock/lockedsections/";
  String APPROVE_PO = COMMAND_PURCHASE_ORDER + "approve";

  String ASSIGN_ME_TO_APPROVE_PO = COMMAND_PURCHASE_ORDER + "replaceapprover";

  String VIEW_ITEM_SECTION = "/items";
  String LOCK_SERVICE_ITEM_SECTION = "/lock/items";
  String LOCK_SERVICE_PAYMENT_DETAILS_SECTION = "/lock/paymentdetails";
  String UPDATE_PAYMENT_DETAILS_SECTION = "/update/paymentdetails";
  String UNLOCK_SERVICE_PAYMENT_DETAILS_SECTION = "/unlock/paymentdetails";
  String UNLOCK_SERVICE_ITEM_SECTION = "/unlock/items";
  String ADD_ITEM_SECTION = COMMAND_PURCHASE_ORDER + "add/item/";
  String DELETE_ITEM = COMMAND_PURCHASE_ORDER + "delete/item/";
  String EDIT_ITEM_SECTION = COMMAND_PURCHASE_ORDER + "edit/item/";
  String UNLOCK_ITEM_SECTION = COMMAND_PURCHASE_ORDER + "unlock/itemsection/";

  String REQUEST_ADD_ITEM_QUANTITY = COMMAND_PURCHASE_ORDER + "lock/orderitems/itemquantity/";
  String REQUEST_CANCEL_ADD_ITEM_QUANTITY =
      COMMAND_PURCHASE_ORDER + "unlock/orderitems/itemquantity/";
  String REQUEST_EDIT_ITEM_QUANTITY = COMMAND_PURCHASE_ORDER + "lock/edit/itemquantity/";
  String REQUEST_CANCEL_EDIT_ITEM_QUANTITY = COMMAND_PURCHASE_ORDER + "unlock/edit/itemquantity/";
  String SAVE_ITEM_QUANTITY = COMMAND_PURCHASE_ORDER + "save/quantity/";
  String ADD_ITEM_QUANTITY = COMMAND_PURCHASE_ORDER + "add/quantity/";

  String REJECT_PO = COMMAND_PURCHASE_ORDER + "reject";

  String EXPORT_PDF = "/reporting/purchaseorder/exportpdf/";

  String PO_READ_ALL_URL = "/services/purchase/orders";

  String PURCHASE_ORDER_ACTIONS = COMMAND_PURCHASE_ORDER + "authorizedactions/";
  String CANCEL = COMMAND_PURCHASE_ORDER + "cancel/";
  String MARK_AS_CLEARED = COMMAND_PURCHASE_ORDER + "markascleared/";
  String MARK_AS_DELIVERY_COMPLETE = COMMAND_PURCHASE_ORDER + "markasdeliverycomplete/";
  String MARK_AS_PI_REQUEST = COMMAND_PURCHASE_ORDER + "markAsPIRequested/";
  String SUBMIT_FOR_APPROVAL = COMMAND_PURCHASE_ORDER + "submitforapproval/";
  String OPEN_FOR_UPDATES = COMMAND_PURCHASE_ORDER + "openforupdates/";

  String DELETE_QUANTITY_URL = COMMAND_PURCHASE_ORDER + "delete/quantity/";
  String REQUEST_EDIT_QTY_IN_DN_PL = COMMAND_PURCHASE_ORDER + "lock/itemsection/qtyindnpl/";
  String REQUEST_CANCEL_QTY_IN_DN_PL = COMMAND_PURCHASE_ORDER + "unlock/itemsection/qtyindnpl/";
  String SAVE_QTY_IN_DN_PL = COMMAND_PURCHASE_ORDER + "save/qtyindnpl/";
  String UPDATE_ITEM_SECTION = COMMAND_PURCHASE_ORDER + "update/OrderItems/";
  String DELETE_PURCHASE_ORDER = COMMAND_PURCHASE_ORDER + "delete/";
  String DELETE_ORDER_DOCUMENT = COMMAND_PURCHASE_ORDER + "delete/order/";
  String SAVE_ATTACHMENTS = COMMAND_PURCHASE_ORDER + "attachment/save/";
  String AUTHORIZED_ACTIONS = QUERY_PURCHASE_ORDER + "authorizedactions/";

  String LOCK_ESTIMATED_COST = "/lock/estimatedcost/";
  String UNLOCK_ESTIMATED_COST = "/unlock/estimatedcost/";
  String UPDATE_ESTIMATED_COST = "/update/estimatedcost/";
  String READ_ESTIMATED_COST_URL = "/services/estimatedcost/";
  String VIEW_PAYMENT_TERM = QUERY_PURCHASE_ORDER + "paymentterms";
  String VIEW_VENDOR = "/vendor";
  String VIEW_COMPANY = "/company";
  String VIEW_SERVICE_PAYMENT_TERM = "/paymentdetails";

  String READ_ALL_PURCHASE_ORDER_DOCUMENT_OWNERS = QUERY_PURCHASE_ORDER + "documentowners";
  String READ_ALL_TYPES = QUERY_PURCHASE_ORDER + "types";

  String CREATE_PURCHASE_ORDER = COMMAND_PURCHASE_ORDER;
  String UPDATE_ITEMS = "/update/items";

  String READ_SERVICE_ITEM_UOM = "/items/unitofmeasures/";
}
