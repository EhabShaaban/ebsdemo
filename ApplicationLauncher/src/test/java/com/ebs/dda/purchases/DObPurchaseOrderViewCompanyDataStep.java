package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewCompanyTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;

public class DObPurchaseOrderViewCompanyDataStep extends SpringBootRunner implements En {

  private DObPurchaseOrderViewCompanyTestUtils utils;

  public DObPurchaseOrderViewCompanyDataStep() {

    When(
        "^\"([^\"]*)\" requests to view Company section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response purchaseOrderCompanyData =
              utils.readPurchaseOrderCompanyData(userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, purchaseOrderCompanyData);
        });

    Then(
        "^the following values of Company section of PurchaseOrder with code \"([^\"]*)\" are displayed to \"([^\"]*)\":$",
        (String poCode, String userName, DataTable purchaseOrderTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatCompanyDataAreDisplayedToTheUserInResponse(purchaseOrderTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderViewCompanyTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View Company section")) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {

    if (contains(scenario.getName(), "View Company section")) {
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

}
