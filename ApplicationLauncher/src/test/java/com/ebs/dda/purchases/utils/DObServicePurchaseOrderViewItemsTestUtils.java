package com.ebs.dda.purchases.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.accounting.IAccountingFeatureFileCommonKeys;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderItemSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IIObOrderTaxGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;

public class DObServicePurchaseOrderViewItemsTestUtils extends DObPurchaseOrderCommonTestUtils {

  public String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Items][Service]";

  public DObServicePurchaseOrderViewItemsTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    str.append(",").append("db-scripts/accounting/taxes.sql");
    return str.toString();
  }

  public Response readPurchaseOrderItemsData(Cookie cookie, String purchaseOrderCode) {
    String restURL = getViewItemsUrl(purchaseOrderCode);
    return sendGETRequest(cookie, restURL);
  }

  public String getViewItemsUrl(String purchaseOrderCode) {
    return IPurchaseOrderRestUrls.QUERY_PURCHASE_ORDER
        + purchaseOrderCode
        + IPurchaseOrderRestUrls.VIEW_ITEM_SECTION;
  }

  public void assertThatItemsDataAreDisplayedToTheUserInResponse(
      DataTable expectedItemsTable, Response actualItemsResponse) throws Exception {
    List<Map<String, String>> expectedItemsData =
        expectedItemsTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualItemList = getItemsFromResponse(actualItemsResponse);
    for (int i = 0; i < expectedItemsData.size(); i++) {
      assertThatExpectedItemMatchesActualItem(expectedItemsData.get(i), actualItemList.get(i));
    }
  }

  private void assertThatExpectedItemMatchesActualItem(
      Map<String, String> expectedItemsData, HashMap<String, Object> actualItemsData)
      throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedItemsData, actualItemsData);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IFeatureFileCommonKeys.ITEM,
        IIObOrderItemGeneralModel.ITEM_CODE,
        IIObOrderItemGeneralModel.ITEM_NAME);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IOrderFeatureFileCommonKeys.ORDER_UNIT,
        IIObOrderItemGeneralModel.ORDER_UNIT_CODE,
        IIObOrderItemGeneralModel.ORDER_UNIT_SYMBOL);

    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.QUANTITY, IIObOrderItemGeneralModel.QUANTITY);

    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.PRICE, IIObOrderItemGeneralModel.PRICE);

    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.ITEM_TOTAL_AMOUNT, IIObOrderItemGeneralModel.ITEM_TOTAL_AMOUNT);
  }

  private List<HashMap<String, Object>> getItemsFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemsResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualItemsResponse, "Items");
  }

  public void assertThatItemResponseIsEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualItemsList = getItemsFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + " This PurchaseOrder items are not empty. ", 0, actualItemsList.size());
  }

  private List<HashMap<String, Object>> getTaxesFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemsResponse = getMapFromResponse(response);
    return readListOfMapFromMap(actualItemsResponse, "Taxes");
  }

  public void assertThatTaxesResponseIsEmpty(Response response) throws Exception {
    List<HashMap<String, Object>> actualItemsList = getTaxesFromResponse(response);
    Assert.assertEquals(
        ASSERTION_MSG + " This PurchaseOrder taxes are not empty. ", 0, actualItemsList.size());
  }

  public void assertThatPurchaseOrderHasTaxesInResponse(
      DataTable taxDetailsDataTable, Response response) throws Exception {
    List<Map<String, String>> expectedTaxesData =
        taxDetailsDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualTaxesList = getTaxesFromResponse(response);
    for (int i = 0; i < expectedTaxesData.size(); i++) {
      assertThatExpectedTaxMatchesActualTax(expectedTaxesData.get(i), actualTaxesList.get(i));
    }
  }

  private void assertThatExpectedTaxMatchesActualTax(
      Map<String, String> expectedTaxesData, HashMap<String, Object> actualTaxesData)
      throws Exception {
    MapAssertion mapAssertion = new MapAssertion(expectedTaxesData, actualTaxesData);

    mapAssertion.assertCodeAndLocalizedNameValue(
        IAccountingFeatureFileCommonKeys.TAX,
        IIObOrderTaxGeneralModel.CODE,
        IIObOrderTaxGeneralModel.NAME);

    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.TAX_PERCENTAGE, IIObOrderTaxGeneralModel.PERCENTAGE);

    mapAssertion.assertBigDecimalNumberValue(
        IAccountingFeatureFileCommonKeys.TAX_AMOUNT, IIObOrderTaxGeneralModel.AMOUNT);
  }

  public void assertThatPurchaseOrderHasItemSummaryInResponse(
      DataTable itemSummaryDataTable, Response response) throws Exception {
    Map<String, String> expectedItemSummary =
        itemSummaryDataTable.asMaps(String.class, String.class).get(0);
    HashMap<String, Object> actualItemSummary = getItemSummaryFromResponse(response);
    assertThatExpectedItemSummaryMatchesActualItemSummary(expectedItemSummary, actualItemSummary);
  }

  private HashMap<String, Object> getItemSummaryFromResponse(Response response) throws Exception {
    HashMap<String, Object> actualItemsResponse = getMapFromResponse(response);
    return readMapFromString(actualItemsResponse, "ItemSummary");
  }

  private void assertThatExpectedItemSummaryMatchesActualItemSummary(
      Map<String, String> expectedItemSummary, HashMap<String, Object> actualItemSummary) {
    MapAssertion mapAssertion = new MapAssertion(expectedItemSummary, actualItemSummary);
    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.TOTAL_AMOUNT_BEFORE_TAXES,
        IIObOrderItemSummaryGeneralModel.TOTAL_AMOUNT_BEFORE_TAXES);

    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.TOTAL_TAXES, IIObOrderItemSummaryGeneralModel.TOTAL_TAXES);

    mapAssertion.assertBigDecimalNumberValue(
        IOrderFeatureFileCommonKeys.TOTAL_AMOUNT_AFTER_TAXES,
        IIObOrderItemSummaryGeneralModel.TOTAL_AMOUNT_AFTER_TAXES);
  }
}
