package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderDeliveryCompleteValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsDeliveryCompleteTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderStateTransitionTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPurchaseOrderMarkAsDeliveryCompleteStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderMarkAsDeliveryCompleteTestUtils markAsDeliveryCompleteTestUtils;
  private DObPurchaseOrderStateTransitionTestUtils stateTransitionTestUtils;

  public DObPurchaseOrderMarkAsDeliveryCompleteStep() {

    Given(
        "^the following GRs exist with the following GR-POData:$",
        (DataTable goodsReceiptPurchaseOrderData) -> {
          markAsDeliveryCompleteTestUtils.assertThatGoodsReceiptsExist(
              goodsReceiptPurchaseOrderData);
        });

    Given(
        "^the following Import PurchaseOrders exist with clearance date:$",
        (DataTable purchaseOrderTable) -> {
          markAsDeliveryCompleteTestUtils.assertThatImportPurchaseOrderWithClearanceDateIsExist(
              purchaseOrderTable);
        });

    Given(
        "^the following Local PurchaseOrders exist with Shipping date:$",
        (DataTable purchaseOrderTable) -> {
          markAsDeliveryCompleteTestUtils.assertThatLocalPurchaseOrderWithShippingDateIsExist(
              purchaseOrderTable);
        });

    When(
        "^\"([^\"]*)\" requests to MarkAsDeliveryComplete the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String currentDateTime,
            DataTable deliveryCompleteDataTable) -> {
          markAsDeliveryCompleteTestUtils.freezeWithTimeZone(currentDateTime);
          Response response =
              markAsDeliveryCompleteTestUtils.markAsDeliveryComplete(
                  userActionsTestUtils.getUserCookie(userName),
                  purchaseOrderCode,
                  deliveryCompleteDataTable);

          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated with DeliveryCompleteDate as follows:$",
        (String purchaseOrderCode, DataTable updatedPurchaseOrderTable) -> {
          markAsDeliveryCompleteTestUtils.assertThatPurchaseOrderIsMarkedAsDeliveryComplete(
              purchaseOrderCode, updatedPurchaseOrderTable);
        });

    Then(
        "^the following error message is attached to DeliveryComplete date field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          markAsDeliveryCompleteTestUtils.assertThatFieldHasErrorMessageCode(
              response,
              errorCode,
              IDObPurchaseOrderDeliveryCompleteValueObject.DELIVERY_COMPLETE_DATE_TIME);
        });
  }

  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    markAsDeliveryCompleteTestUtils =
        new DObPurchaseOrderMarkAsDeliveryCompleteTestUtils(getProperties());
    markAsDeliveryCompleteTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
    stateTransitionTestUtils = new DObPurchaseOrderStateTransitionTestUtils(getProperties());
    stateTransitionTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsDeliveryComplete PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(
            markAsDeliveryCompleteTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(
            markAsDeliveryCompleteTestUtils.getDbScriptsAfterEachScenario());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsDeliveryComplete PO")) {
      markAsDeliveryCompleteTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          markAsDeliveryCompleteTestUtils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "MarkAsDeliveryComplete PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(
          markAsDeliveryCompleteTestUtils.getDbScriptsAfterEachScenario());
      databaseConnector.closeConnection();
    }
  }
}
