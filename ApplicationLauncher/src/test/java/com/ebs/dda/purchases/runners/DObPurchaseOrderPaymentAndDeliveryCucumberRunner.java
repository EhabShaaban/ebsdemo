package com.ebs.dda.purchases.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/ordermanagement/purchase-order/PO_View_ApprovalCycles.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Upload_Attachments_HP.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_Attachments.feature",
      "classpath:features/ordermanagement/purchase-order/PO_ExportPDF_Footer.feature",
      "classpath:features/ordermanagement/purchase-order/PO_ExportPDF_Header.feature",
      "classpath:features/ordermanagement/purchase-order/PO_ExportPDF_Items.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_RequiredDoc_Auth.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_RequiredDoc_HP.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_RequiredDoc_Val.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestEdit_RequiredDocs.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_RequiredDoc.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_PaymentAndDelivery.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestEdit_PaymentAndDelivery.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_PaymentAndDelivery_HP.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_PaymentAndDelivery_Val.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_PaymentAndDelivery_Auth.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.purchases"},
    strict = true,
    tags = "not @Future")
public class DObPurchaseOrderPaymentAndDeliveryCucumberRunner {}
