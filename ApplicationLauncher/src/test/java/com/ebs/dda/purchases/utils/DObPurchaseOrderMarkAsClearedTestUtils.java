package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderClearanceValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DObPurchaseOrderMarkAsClearedTestUtils extends DObPurchaseOrderCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Mark][As][Cleared]";

  public DObPurchaseOrderMarkAsClearedTestUtils(Map<String, Object> properties) {
    super();
    setProperties(properties);
  }

  public Response markAsCleared(
      Cookie loginCookie, String purchaseOrderCode, DataTable clearanceDataTable) {
    Map<String, String> clearanceDataMap =
        clearanceDataTable.asMaps(String.class, String.class).get(0);
    JsonObject clearanceValueObject =
        initClearanceUrlValueObject(clearanceDataMap.get(IExpectedKeys.CLEARANCE_DATE));
    String restURL = IPurchaseOrderRestUrls.MARK_AS_CLEARED + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, clearanceValueObject);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append("," + "db-scripts/clearUnsedCompanyBankDetails.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBasicDataScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/markascleared/purchase-order.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/purchase-order/markascleared/IObOrderApprovalCycle.sql");

    return str.toString();
  }

  public String getDbScriptsAfterEachScenario() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/markascleared/purchase-order.sql");
    str.append("," + "db-scripts/purchase-order/markascleared/IObOrderApprovalCycle.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    return str.toString();
  }

  public void assertThatPurchaseOrderIsCleared(
      String purchaseOrderCode, DataTable expectedDataTable) {

    Map<String, String> expectedMap = expectedDataTable.asMaps(String.class, String.class).get(0);

    List<DObPurchaseOrder> actualPurchaseOrderList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getClearedPurchaseOrder",
            purchaseOrderCode,
            expectedMap.get(IExpectedKeys.LAST_UPDATED_BY),
            getDateTimeInMachineTimeZone(expectedMap.get(IExpectedKeys.LAST_UPDATE_DATE)),
            '%' + expectedMap.get(IExpectedKeys.STATE) + '%',
            getDateTimeInMachineTimeZone(expectedMap.get(IExpectedKeys.CLEARANCE_DATE)));

    assertTrue(
        ASSERTION_MSG + " There is no PO with code " + purchaseOrderCode + " matches this info",
        actualPurchaseOrderList.size() == 1);
  }

  public JsonObject initClearanceUrlValueObject(String clearanceDateTime) {

    JsonObject clearanceValueObject = new JsonObject();
    clearanceValueObject.addProperty(
        IDObPurchaseOrderClearanceValueObject.CLEARANCE_DATE_TIME, clearanceDateTime);

    return clearanceValueObject;
  }

  public void assertThatPurchaseOrderWithArrivalDateIsExist(DataTable purchaseOrderTable) {
    List<Map<String, String>> expectedDataMap =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> order : expectedDataMap) {
      List<Object> actualData =
          entityManagerDatabaseConnector.executeNativeNamedQuery(
              "getArrivedPurchaseOrderDetails", order.get("Code"), "%Arrived%");
      assertEquals(
          ASSERTION_MSG + " The arrival date " + order.get("ArrivalDate") + " is not correct",
          order.get("ArrivalDate"),
          actualData.get(0));
    }
  }

  public void assertThatSectionsWithMissingFieldsExistInResponse(
      Response userResponse, DataTable sectionsWithMissingFieldsDataTable) {
    List<Map<String, String>> sections =
        sectionsWithMissingFieldsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> section : sections) {
      assertThatMissingFieldsExistInResponse(
          userResponse,
          section.get(IFeatureFileCommonKeys.SECTION_NAME),
          section.get(IFeatureFileCommonKeys.MISSING_FIELDS));
    }
  }

  public void assertThatMissingFieldsExistInResponse(
      Response response, String sectionName, String missingFields) {

    Map<String, Object> missingFieldsList = getResponseMissingFields(response);
    List<String> actualSectionMissingFields = (List<String>) missingFieldsList.get(sectionName);
    String[] expectedMissingFields = getStringAsArray(missingFields);
    Assert.assertTrue(
        ASSERTION_MSG + " This Missing Fields is not correct",
        actualSectionMissingFields.containsAll(Arrays.asList(expectedMissingFields)));
  }

  private Map<String, Object> getResponseMissingFields(Response response) {
    return response.body().jsonPath().getMap("missingFields");
  }

  private String[] getStringAsArray(String sectionMissingData) {
    return sectionMissingData.replaceAll("\\s", "").split(",");
  }
}
