package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.utils.DObPurchaseOrderCreateTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.SQLException;

public class DObPurchaseOrderCreateStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObPurchaseOrderCreateTestUtils utils;

  public DObPurchaseOrderCreateStep() {

    Given(
        "^CurrentDateTime = \"([^\"]*)\"$",
        (String currentDateTime) -> {
          utils.freeze(currentDateTime);
        });

    Given(
        "^Last created PurchaseOrder was with code \"([^\"]*)\"$",
        (String purchaseOrderCode) -> {
          utils.createLastEntityCode(DObPurchaseOrder.class.getSimpleName(), purchaseOrderCode);
        });

    When(
        "^\"([^\"]*)\" creates PurchaseOrder with following values:$",
        (String userName, DataTable purchaseOrderDataTable) -> {
          Response response =
              utils.createPurchaseOrder(
                  purchaseOrderDataTable, userActionsTestUtils.getUserCookie(userName));
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new PurchaseOrder is created as follows:$",
        (DataTable purchaseOrderDataTable) -> {
          utils.assertThatPurchaseOrderIsCreatedSuccessfully(purchaseOrderDataTable);
        });

    And(
        "^the Vendor Section of PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable purchaseOrderDataTable) -> {
          utils.assertThatPurchaseOrderVendorIsCreatedSuccessfully(
              purchaseOrderCode, purchaseOrderDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to creates PurchaseOrder$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.requestCreatePurchaseOrder(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    And(
        "^the Company Section of PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable purchaseOrderDataTable) -> {
          utils.assertThatPurchaseOrderCompanyIsCreatedSuccessfully(
              purchaseOrderCode, purchaseOrderDataTable);
        });

    Then(
        "^the Taxes Section for Service PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String purchaseOrderCode, DataTable taxesDataTable) -> {
          utils.assertThatServicePurchaseOrderTaxesIsCreatedSuccessfully(
              purchaseOrderCode, taxesDataTable);
        });

    Then(
        "^the TaxesSummary section for PurchaseOrder with code \"([^\"]*)\" is displayed empty to \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertThatTaxesSummaryIsDisplayedEmpty(purchaseOrderCode);
        });
  }

  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderCreateTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Create PurchaseOrder")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecuted = true;
      }
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Create PurchaseOrder")) {
      utils.unfreeze();
    }
  }
}
