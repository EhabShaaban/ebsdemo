package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.vendor.utils.MObVendorCommonTestUtils;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewAllTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObPurchaseOrderViewAllStep extends SpringBootRunner implements En {

  private DObPurchaseOrderViewAllTestUtils utils;
  private MObVendorCommonTestUtils vendorCommonTestUtils;

  public DObPurchaseOrderViewAllStep() {

    Given(
        "^the following Vendors exist:$",
        (DataTable vendorsData) -> {
          vendorCommonTestUtils.assertThatVendorsExistByCodeAndNameAndPurchaseUnits(vendorsData);
        });

    Given(
        "^the folowing PurchaseUnits exist:$",
        (DataTable purchaseUnits) -> {
          utils.assertThatPurchaseUnitsExist(purchaseUnits);
        });

    When(
        "^\"([^\"]*)\" requests to view first page of PurchaseOrders with (\\d+) records per page$",
        (String username, Integer numOfRecords) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          Response response =
              utils.sendGETRequest(cookie, utils.getPreparedViewAllUrl(numOfRecords));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values will be presented to \"([^\"]*)\":$",
        (String username, DataTable purchaseOrders) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertResponseSuccessStatus(response);

          utils.assertPurchaseOrderDataIsCorrect(purchaseOrders, response);
        });

    And(
        "^the total number of records presented to \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PurchaseOrders with filter applied on POCode which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String userCodeContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter = utils.getContainsFilterField("userCode", userCodeContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IPurchaseOrderRestUrls.PO_READ_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the total number of records in search results by \"([^\"]*)\" are (\\d+)$",
        (String userName, Integer totalRecordsNumber) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertTotalNumberOfRecordsAreCorrect(response, totalRecordsNumber);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PurchaseOrders with filter applied on State which contains \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String stateContains, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter = utils.getContainsFilterField("currentStates", stateContains);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IPurchaseOrderRestUrls.PO_READ_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PurchaseOrders with filter applied on OrderType which equals \"([^\"]*)\" with (\\d+) records per page$",
        (String username, Integer pageNumber, String orderTypeEquals, Integer pageSize) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter = utils.getEqualsFilterField("objectTypeCode", orderTypeEquals);
          Response response =
              utils.requestToReadFilteredData(
                  cookie, IPurchaseOrderRestUrls.PO_READ_ALL_URL, pageNumber, pageSize, filter);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PurchaseOrders with filter applied on PurchaseUnit which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String purchaseUnitName,
            Integer recordsNumberPerPage,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String purchaseUnitNameFilter =
              utils.getContainsFilterField("purchaseUnitName", purchaseUnitName);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IPurchaseOrderRestUrls.PO_READ_ALL_URL,
                  pageNumber,
                  recordsNumberPerPage,
                  purchaseUnitNameFilter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
          String lang = locale.split("-")[0];
          utils.withLocale(lang);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PurchaseOrders with filter applied on Vendor which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String username,
            Integer pageNumber,
            String userCodeContains,
            Integer pageSize,
            String locale) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(username);
          String filter = utils.getContainsFilterField("vendor", userCodeContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IPurchaseOrderRestUrls.PO_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(username, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PurchaseOrders with (\\d+) records per page and the following filters applied while current locale is \"([^\"]*)\":$",
        (String userName,
            Integer pageNumber,
            Integer pageSize,
            String locale,
            DataTable filters) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter = utils.getFilters(filters);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IPurchaseOrderRestUrls.PO_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  locale);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read page (\\d+) of PurchaseOrders with filter applied on ReferenceDocument which contains \"([^\"]*)\" with (\\d+) records per page while current locale is \"([^\"]*)\"$",
        (String userName,
            Integer pageNumber,
            String refDocumentContains,
            Integer pageSize,
            String local) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          String filter =
              utils.getContainsFilterField(
                  IDObPurchaseOrderGeneralModel.REF_DOCUMENT, refDocumentContains);
          Response response =
              utils.requestToReadFilteredDataWithLocale(
                  cookie,
                  IPurchaseOrderRestUrls.PO_READ_ALL_URL,
                  pageNumber,
                  pageSize,
                  filter,
                  local);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderViewAllTestUtils(properties);
    vendorCommonTestUtils = new MObVendorCommonTestUtils(properties);
    vendorCommonTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View all PurchaseOrders")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsExecution());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "View all PurchaseOrders")) {
      utils.unfreeze();
    }
  }
}
