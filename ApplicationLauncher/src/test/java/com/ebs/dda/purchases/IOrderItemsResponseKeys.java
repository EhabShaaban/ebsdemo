package com.ebs.dda.purchases;

public interface IOrderItemsResponseKeys {

  String ITEM_NAME = "name";
  String ITEM_CODE = "itemCode";
  Object ITEM_UNIT_SYMBOL = "unitSymbol";
  String ITEM_QUANTITY_UNIT_SYMBOL = "orderUnitSymbol";
  String ITEM_QUANTITY = "quantity";
  String ITEM_PRICE = "price";
  String ITEM_DISCOUNT_PERCENTAGE = "discountPercentage";
  String ITEM_CODE_AT_VENDOR = "itemCodeAtVendor";
}
