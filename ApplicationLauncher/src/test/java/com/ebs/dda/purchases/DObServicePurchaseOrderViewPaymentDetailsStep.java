package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObServicePurchaseOrderViewPaymentDetailsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObServicePurchaseOrderViewPaymentDetailsStep extends SpringBootRunner implements En {

  private DObServicePurchaseOrderViewPaymentDetailsTestUtils utils;

  public DObServicePurchaseOrderViewPaymentDetailsStep() {

    When(
        "^\"([^\"]*)\" requests to view PaymentDetails section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readPurchaseOrderPaymentTermData(cookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of Payment section are displayed to \"([^\"]*)\":$",
        (String userName, DataTable paymentTermDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatReadPaymentTermDataResponseIsCorrect(paymentTermDataTable, response);
        });
      Given("^the following PurchaseOrder with code \"([^\"]*)\" has an empty PaymentDetails section$", (String purchaseOrderCode) -> {
          utils.assertEmptyPaymentDetailsInPurchaseOrder(purchaseOrderCode);
      });

      Then("^PaymentDetails Section is displayed to \"([^\"]*)\" with empty values$", (String username) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          utils.assertEmptyResponseOfPaymentDetails(response);
      });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObServicePurchaseOrderViewPaymentDetailsTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("View Payment Details section")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }
}
