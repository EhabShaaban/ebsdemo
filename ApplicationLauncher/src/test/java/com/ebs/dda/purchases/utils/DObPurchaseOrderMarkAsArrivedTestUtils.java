package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderArrivalValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class DObPurchaseOrderMarkAsArrivedTestUtils extends DObPurchaseOrderCommonTestUtils {

  public String arrivalRestURL = "/command/purchaseorder/arrival/";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Mark][As][Arrived]";

  public DObPurchaseOrderMarkAsArrivedTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptsForMarkAsArrived() {
    StringBuilder str = new StringBuilder();

    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");

    return str.toString();
  }

  public void assertThatPurchaseOrderUpdatedForMarkAsArrived(
      String purchaseOrderCode, DataTable purchaseOrderDataTable) {

    Map<String, String> purchaseOrderData =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);
    Object result =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPurchaseOrderModificationDetailsForMarkAsArrived",
            purchaseOrderCode,
            getDateTimeInMachineTimeZone(purchaseOrderData.get(IExpectedKeys.ARRIVAL_DATE)));
    assertNotNull(
        ASSERTION_MSG
            + " The Modification Details is not correct for PO with code"
            + purchaseOrderCode,
        result);
  }

  public JsonObject initArrivalValueObject(String arrivalDateTime, String purchaseOrderCode) {

    JsonObject arrivalValueObject = new JsonObject();
    arrivalValueObject.addProperty(
        IDObPurchaseOrderArrivalValueObject.ARRIVAL_DATE_TIME, arrivalDateTime);
    arrivalValueObject.addProperty(
        IDObPurchaseOrderArrivalValueObject.PURCHASE_ORDER_CODE, purchaseOrderCode);
    return arrivalValueObject;
  }

  public void assertThatPurchaseOrderWithShippingDateDateIsExist(DataTable purchaseOrderTable) {
    Map<String, String> expectedDataMap =
        purchaseOrderTable.asMaps(String.class, String.class).get(0);
    List<Object> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getShippedPurchaseOrderDetails", expectedDataMap.get("Code"), "%Shipped%");

    assertEquals(
        ASSERTION_MSG + " There is no PO matches this Shipping Date",
        expectedDataMap.get("shippingDate"),
        actualData.get(0));
  }
}
