package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.jpa.order.IIObOrderItemValueObject;
import com.ebs.dda.order.IOrderFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.math.BigDecimal;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class IObServicePurchaseOrderSaveNewItemHPTestUtils extends DObPurchaseOrderCommonTestUtils {
  protected final String ASSERTION_MSG = "[SPO] [Save] [Add] [Item]";
  public String PO_JMX_LOCK_BEAN_NAME = "purchaseOrderLockCommandJMXBean";

  public IObServicePurchaseOrderSaveNewItemHPTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public Response saveNewItem(Cookie cookie, String servicePOCode, DataTable newItemDataTable) {
    JsonObject newItemJsonObject = constructNewItemValueObject(newItemDataTable);
    return sendPUTRequest(cookie, getSaveNewItemUrl(servicePOCode), newItemJsonObject);
  }

  private void addBigDecimalValueToJsonObject(
      JsonObject newItemValueObject,
      Map<String, String> newItemMap,
      String property,
      String valueKey) {
    String value = newItemMap.get(valueKey);
    if (value.equals("N/A")) {
      return;
    }
    try {
      newItemValueObject.addProperty(property, new BigDecimal(value));
    } catch (NumberFormatException ex) {
      newItemValueObject.addProperty(property, value);
    }
  }

  private JsonObject constructNewItemValueObject(DataTable newItemDataTable) {
    JsonObject newItemValueObject = new JsonObject();

    Map<String, String> newItemMap = newItemDataTable.asMaps(String.class, String.class).get(0);

    addValueToJsonObject(
        newItemValueObject,
        IIObOrderItemValueObject.ITEM_CODE,
        getFirstValue(newItemMap, IFeatureFileCommonKeys.ITEM));

    addValueToJsonObject(
        newItemValueObject,
        IIObOrderItemValueObject.UNIT_OF_MEASURE_CODE,
        getFirstValue(newItemMap, IFeatureFileCommonKeys.ITEM_ORDER_UNIT));

    addValueToJsonObject(
        newItemValueObject,
        IIObOrderItemValueObject.UNIT_OF_MEASURE_CODE,
        getFirstValue(newItemMap, IFeatureFileCommonKeys.ITEM_ORDER_UNIT));

    addBigDecimalValueToJsonObject(
        newItemValueObject,
        newItemMap,
        IIObOrderItemValueObject.QUANTITY,
        IOrderFeatureFileCommonKeys.QUANTITY);

    addBigDecimalValueToJsonObject(
        newItemValueObject,
        newItemMap,
        IIObOrderItemValueObject.PRICE,
        IOrderFeatureFileCommonKeys.PRICE);

    return newItemValueObject;
  }

  private String getSaveNewItemUrl(String servicePOCode) {
    StringBuilder url = new StringBuilder();
    url.append(IPurchaseOrderRestUrls.COMMAND_PURCHASE_ORDER)
        .append(servicePOCode)
        .append(IPurchaseOrderRestUrls.UPDATE_ITEMS);
    return url.toString();
  }

  public void assertThatPurchaseOrderIsUpdated(
      String servicePOCode, DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderMap =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);

    Object purchaseOrderModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getModifiedPurchaseOrder",
            constructModifiedPurchaseOrderQueryParams(servicePOCode, purchaseOrderMap));

    assertNotNull(
        ASSERTION_MSG + "purchase order is not updated successfully", purchaseOrderModifiedDate);

    assertDateEquals(
        purchaseOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
        new DateTime(purchaseOrderModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] constructModifiedPurchaseOrderQueryParams(
      String servicePOCode, Map<String, String> purchaseOrderMap) {
    return new Object[] {
      servicePOCode, purchaseOrderMap.get(IFeatureFileCommonKeys.LAST_UPDATED_BY)
    };
  }
}
