package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObServicePurchaseOrderRequestAddItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObServicePurchaseOrderRequestAddItemStep extends SpringBootRunner implements En {

  private DObServicePurchaseOrderRequestAddItemTestUtils utils;

  public DObServicePurchaseOrderRequestAddItemStep() {

    When(
        "^\"([^\"]*)\" requests to add Item to OrderItems section of Service PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          String lockURL = utils.getLockItemsURL(purchaseOrderCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^OrderItems section of Service PurchaseOrder with code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          String lockUrl = utils.getLockItemsURL(purchaseOrderCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockUrl, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to cancel add Item to OrderItems section of Service PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockUrl = utils.getUnLockItemsURL(purchaseOrderCode);
          Response response = userActionsTestUtils.unlockSection(username, unlockUrl);
          userActionsTestUtils.setUserResponse(username, response);
        });
    Given(
        "^\"([^\"]*)\" first requests to add Item to OrderItems section of Service PurchaseOrder with code \"([^\"]*)\" successfully$",
        (String userName, String purchaseOrderCode) -> {
          String lockURL = utils.getLockItemsURL(purchaseOrderCode);
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });
    Then(
        "^PurchaseOrderItems section of Service PurchaseOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName,
              purchaseOrderCode,
              utils.PO_JMX_LOCK_BEAN_NAME,
              IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to cancel add Item to OrderItems section of Service PurchaseOrder with code \"([^\"]*)\"$",
        (String username, String purchaseOrderCode) -> {
          String unlockUrl = utils.getUnLockItemsURL(purchaseOrderCode);
          Response response = userActionsTestUtils.unlockSection(username, unlockUrl);
          userActionsTestUtils.setUserResponse(username, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils =
        new DObServicePurchaseOrderRequestAddItemTestUtils(
            getProperties(), entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request to Add Item in Service PurchaseOrder,")
        || scenario.getName().contains("Request to Cancel Add Item in Service PurchaseOrder,")) {
      databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (scenario.getName().contains("Request to Add Item in Service PurchaseOrder,")
        || scenario.getName().contains("Request to Cancel Add Item in Service PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }
}
