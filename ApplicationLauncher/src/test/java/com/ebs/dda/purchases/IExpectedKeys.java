package com.ebs.dda.purchases;

public interface IExpectedKeys {

  String STATE = "State";
  String CLEARANCE_DATE = "ClearanceDate";
  String LAST_UPDATED_BY = "LastUpdatedBy";
  String LAST_UPDATE_DATE = "LastUpdateDate";

  String DOCUMENT_TYPE_CODE = "DocumentTypeCode";
  String ATTACHMENT = "Attachment";
  String DOCUMENT_TYPE_NAME = "DocumentTypeName";
  String UPLOADED_BY = "UploadedBy";
  String UPLOAD_DATE_TIME = "UploadDateTime";

  String CONFIRMATION_DATE = "ConfirmationDate";
  String PRODUCTION_FINISHED_DATE = "ProductionFinishedDate";

  String ITEM_CODE = "ItemCode";
  String ITEM_NAME = "ItemName";
  String QTYINDN = "QtyInDN";
  String BASE = "Base";

  String QTY = "Qty";
  String ORDER_UNIT = "OrderUnit";
  String UNIT_PRICE = "UnitPrice(Gross)";
  String DISCOUNT = "Discount";

  String DOCUMENT_TYPE = "DocumentType";
  String COPIES = "Copies";

  String TYPE = "Type";
  String CODE = "Code";
  String PURCHASE_UNIT = "PurchasingUnit";
  String INCOTERM = "Incoterm";
  String CURRENCY = "Currency";
  String PAYMENT_TERMS = "PaymentTerms";
  String SHIPPING_INSTRUCTIONS = "ShippingInstructions";
  String CONTAINERS_NO = "ContainersNo";
  String CONTAINERS_TYPE = "ContainersType";
  String COLLECTION_DATE = "CollectionDate_SpecificDate";
  String NUMBER_OF_DAYS = "CollectionDate_TimePeriod";
  String MODE_OF_TRANSPORT = "ModeOfTransport";
  String LOADING_PORT = "LoadingPort";
  String DISCHARGE_PORT = "DischargePort";

  String LAST_UPDATED_DATE = "LastUpdateDate";

  String ARRIVAL_DATE = "ArrivalDate";

  String PURCHASE_ORDER_CODE = "POCode";
  String DELIVERY_COMPLETE_DATE = "DeliveryCompleteDate";
}
