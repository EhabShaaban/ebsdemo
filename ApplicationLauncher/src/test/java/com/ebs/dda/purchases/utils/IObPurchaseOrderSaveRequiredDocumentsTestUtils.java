package com.ebs.dda.purchases.utils;

import static junit.framework.TestCase.assertNull;

import java.util.Map;

/** Created by "Zyad Ghorab" on Nov, 2018 */
public class IObPurchaseOrderSaveRequiredDocumentsTestUtils
    extends IObPurchaseOrderRequiredDocumentsTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Save]";

  public IObPurchaseOrderSaveRequiredDocumentsTestUtils(Map<String, Object> properties) {
    super(properties);
    setProperties(properties);
  }

  public void assertThatPurchaseOrderRequiredDocumentsIsEmpty(String purchaseOrderCode) {
    Object poRequiredDocuments =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getPurchaseOrderRequiredDocuments", purchaseOrderCode);

    assertNull(
        ASSERTION_MSG + " Required documents list of PO " + purchaseOrderCode + " is not empty",
        poRequiredDocuments);
  }
}
