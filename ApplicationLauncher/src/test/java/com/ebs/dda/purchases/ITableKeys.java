package com.ebs.dda.purchases;

public interface ITableKeys {

  String ATTACHMENT = "attachment";

  String UPDATED_DATE = "modifieddate";
  String FORMATTED_LAST_UPDATED_DATE =
          "TO_CHAR(" + UPDATED_DATE + "::TIMESTAMP,'dd-Mon-yyyy hh:mi AM')";

  String INCOTERM = "incotermcode";
  String CURRENCY = "currencycode";
  String PAYMENT_TERM = "paymenttermcode";
  String SHIPPING_INSTRUCTIONS = "shippinginstructionscode";
  String CONTAINER_NO = "containerno";
  String CONTAINER = "containercode";
  String COLLECTION_DATE = "collectiondate";
  String FORMATTED_COLLECTION_DATE = "TO_CHAR(" + COLLECTION_DATE + "::TIMESTAMP,'dd-Mon-yyyy')";
  String NUMBER_OF_DAYS = "numberofdays";
  String MODE_OF_TRANSPORT = "modeoftransportcode";
  String LOADING_PORT = "loadingportcode";
  String DISCHARGE_PORT = "dischargeportcode";

}
