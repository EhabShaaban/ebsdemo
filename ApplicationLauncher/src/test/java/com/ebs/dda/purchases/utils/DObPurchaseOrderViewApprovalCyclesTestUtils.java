package com.ebs.dda.purchases.utils;

import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderApprovalCycleGeneralModel;
import com.google.gson.Gson;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ebs.dac.common.utils.calendar.DateFormats.DATE_TIME_WITH_TIMEZONE;
import static org.junit.Assert.*;

public class DObPurchaseOrderViewApprovalCyclesTestUtils extends DObPurchaseOrderCommonTestUtils {

  private final String DECISION_DATETIME_MODEL = "decisionDatetime";
  private final String APPROVAL_CYCLE_NUM_MODEL = "approvalCycleNum";
  private final String END_DATE_MODEL = "endDate";
  private final String START_DATE_MODEL = "startDate";
  private final String FINAL_DECISION_MODEL = "finalDecision";
  private final String APPROVAL_CYCLE_NO = "ApprovalCycleNo";
  private final String CONTROL_POINT = "ControlPoint";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[View][Approval][Cycle]";

  public DObPurchaseOrderViewApprovalCyclesTestUtils(Map<String, Object> properties) {
    setProperties(properties);
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");
    return str.toString();
  }

  public void assertThatControlPointsExist(DataTable controlPointsDataTable) {

    List<Map<String, String>> controlPoints =
        controlPointsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> controlPoint : controlPoints) {
      Object result =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getControlPointByCodeAndName",
              controlPoint.get(ENTITY_CODE),
              controlPoint.get(ENTITY_NAME));
      assertNotNull(result);
    }
  }

  public void assertThatPurchaseOrderHasNoApprovalCycles(DataTable purchaseOrdersDataTable) {
    List<Map<String, String>> purchaseOrders =
        purchaseOrdersDataTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrders) {
      List approvalCycles =
          entityManagerDatabaseConnector.executeNativeNamedQuery(
              "getPurchaseOrderApprovalCycles", purchaseOrder.get(ENTITY_CODE));
      assertTrue(
          ASSERTION_MSG
              + " Approval Cycle is not empty for PO with code "
              + purchaseOrder.get(ENTITY_CODE),
          approvalCycles.isEmpty());
    }
  }

  public String getDecisionValue(String decisionValue) {
    return decisionValue != null ? decisionValue.toUpperCase() : null;
  }

  public void assertThatPurchaseOrderApprovalCyclesExist(
      String purchaseOrderCode, DataTable approvalCyclesDataTable) {
    List<Map<String, String>> approvalCycles =
        approvalCyclesDataTable.asMaps(String.class, String.class);
    List<IObOrderApprovalCycleGeneralModel> actualApprovalCycles =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderApprovalCycles", purchaseOrderCode);
    assertTrue(
        ASSERTION_MSG
            + " Approval Cycles Size is not correct for PO with code "
            + purchaseOrderCode,
        approvalCycles.size() == actualApprovalCycles.size());
    int index = 0;
    for (Map<String, String> expectedApprovalCycle : approvalCycles) {
      expectedApprovalCycle = convertEmptyStringsToNulls(expectedApprovalCycle);
      IObOrderApprovalCycleGeneralModel actualApprovalCycle = actualApprovalCycles.get(index);
      assertEquals(
          ASSERTION_MSG
              + " Number of approval cycles is not correct for PO with code "
              + purchaseOrderCode,
          expectedApprovalCycle.get(APPROVAL_CYCLE_NO),
          String.valueOf(actualApprovalCycle.getApprovalCycleNum()));
      assertEquals(
          ASSERTION_MSG
              + " The Final Decision "
              + getDecisionValue(expectedApprovalCycle.get("FinalDecision"))
              + " is not correct for PO with code "
              + purchaseOrderCode,
          getDecisionValue(expectedApprovalCycle.get("FinalDecision")),
          actualApprovalCycle.getFinalDecision());
      assertDateEquals(expectedApprovalCycle.get("StartDate"), actualApprovalCycle.getStartDate());
      assertDateEquals(expectedApprovalCycle.get("EndDate"), actualApprovalCycle.getEndDate());
      index++;
    }
  }

  private void assertExpectedDateEquals(String expectedDate, Object actualDate) {
    if (expectedDate == null && actualDate == null) {
      return;
    }
    assertDateEquals(expectedDate, getDatabaseFormattedDateTime(actualDate.toString()));
  }

  public Response readPurchaseOrderApprovalCycles(Cookie userCookie, String purchaseOrderCode) {
    String restURL = IPurchaseOrderRestUrls.VIEW_APPROVAL_CYCLES_SECTION + purchaseOrderCode;
    return sendGETRequest(userCookie, restURL);
  }

  public void assertThatResponseApprovalCyclesDataIsCorrect(
      DataTable expectedApprovalCyclesDataTable, Response readResponse) {
    List<Map<String, String>> expectedApprovalCycles =
        expectedApprovalCyclesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualApprovalCycles =
        readResponse.body().jsonPath().getList("data");
    int index = 0;
    for (Map<String, String> expectedApprovalCycle : expectedApprovalCycles) {
      HashMap<String, Object> actualApprovalCycle = actualApprovalCycles.get(index);
      expectedApprovalCycle = convertEmptyStringsToNulls(expectedApprovalCycle);
      assertEquals(
          ASSERTION_MSG
              + " Number of approval cycles "
              + expectedApprovalCycle.get(APPROVAL_CYCLE_NO)
              + " is not correct",
          expectedApprovalCycle.get(APPROVAL_CYCLE_NO),
          String.valueOf(actualApprovalCycle.get(APPROVAL_CYCLE_NUM_MODEL)));
      assertEquals(
          ASSERTION_MSG
              + " Final Decision "
              + getDecisionValue(expectedApprovalCycle.get("FinalDecision"))
              + " is not correct",
          getDecisionValue(expectedApprovalCycle.get("FinalDecision")),
          actualApprovalCycle.get(FINAL_DECISION_MODEL));
      assertDateEquals(
          expectedApprovalCycle.get("StartDate"),
          getDatabaseFormattedDateTime(actualApprovalCycle.get(START_DATE_MODEL).toString()));
      assertExpectedDateEquals(
          expectedApprovalCycle.get("EndDate"), actualApprovalCycle.get(END_DATE_MODEL));
      index++;
    }
  }

  private DateTime getDatabaseFormattedDateTime(String expectedDateStr) {
    DateTimeFormatter databaseFormatter = DateTimeFormat.forPattern(DATE_TIME_WITH_TIMEZONE);
    return databaseFormatter.parseDateTime(expectedDateStr).withZone(DateTimeZone.UTC);
  }

  public void assertThatResponseApprovalCycleDetailsDataIsCorrect(
      String approvalCycleNum, DataTable expectedApproversDetails, Response readResponse)
      throws JSONException {
    List<Map<String, String>> expectedApprovers =
        expectedApproversDetails.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualApprovalCycles =
        readResponse.body().jsonPath().getList("data");
    for (HashMap<String, Object> actualApprovalCycle : actualApprovalCycles) {
      if (actualApprovalCycle.get(APPROVAL_CYCLE_NUM_MODEL).toString().equals(approvalCycleNum)) {
        List<HashMap<String, Object>> actualApproversList =
            (List<HashMap<String, Object>>) actualApprovalCycle.get("approvers");
        assertTrue(
            ASSERTION_MSG + " Approvers List Size " + expectedApprovers.size() + " is not correct",
            actualApproversList.size() == expectedApprovers.size());
        assertActualResponseApproversDataIsCorrect(expectedApprovers, actualApproversList);
      }
    }
  }

  private void assertActualResponseApproversDataIsCorrect(
      List<Map<String, String>> expectedApprovers,
      List<HashMap<String, Object>> actualApproversList)
      throws JSONException {
    int index = 0;
    for (HashMap<String, Object> actualApprover : actualApproversList) {
      Map<String, String> expectedApprover =
          convertEmptyStringsToNulls(expectedApprovers.get(index));
      assertEquals(
          ASSERTION_MSG
              + " Control Point "
              + expectedApprover.get(CONTROL_POINT)
              + " is not correct",
          expectedApprover.get(CONTROL_POINT),
          getEnglishName(actualApprover.get("controlPointName")));
      assertEquals(
          ASSERTION_MSG + " Approver Name " + expectedApprover.get("User") + " is not correct",
          expectedApprover.get("User"),
          actualApprover.get("approverName"));
      assertExpectedDateEquals(
          expectedApprover.get("DateTime"), actualApprover.get(DECISION_DATETIME_MODEL));
      assertEquals(
          ASSERTION_MSG
              + " Decision "
              + getDecisionValue(expectedApprover.get("Decision"))
              + " is not correct",
          getDecisionValue(expectedApprover.get("Decision")),
          actualApprover.get("decision"));
      assertEquals(
          ASSERTION_MSG + " Notes " + expectedApprover.get("Notes") + " is not correct",
          expectedApprover.get("Notes"),
          actualApprover.get("notes"));
      index++;
    }
  }

  private String getEnglishName(Object localizedString) throws JSONException {
    String jsonString = new Gson().toJson(localizedString);
    return new JSONObject(jsonString).getJSONObject("values").getString("en");
  }

  public void assertThatReponseApprovalCyclesDataIsEmpty(Response readResponse) {
    List<HashMap<String, Object>> actualApprovalCycles =
        readResponse.body().jsonPath().getList("data");
    assertTrue(ASSERTION_MSG + " Approval Cycles is not Empty", actualApprovalCycles.isEmpty());
  }
}
