package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderDeleteQuantityTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;
import java.util.Map;

public class DObPurchaseOrderRequestEditItemQuantityStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderDeleteQuantityTestUtils deleteQuantityUtils;
  private DObPurchaseOrderItemTestUtils utils;

  public DObPurchaseOrderRequestEditItemQuantityStep() {

    When(
        "^\"([^\"]*)\" requests to Edit Item Quantity with id (\\d+) for Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, Long quantityId, String itemCode, String purchaseOrderCode) -> {
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_EDIT_ITEM_QUANTITY
                  + purchaseOrderCode
                  + "/"
                  + itemCode
                  + "/"
                  + quantityId;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^edit Item Quantity dialoge is opened and OrderItems section of PurchaseOrder with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertResponseSuccessStatus(userActionsTestUtils.getUserResponse(userName));
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" opened Item Quantity with id (\\d+) for Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName,
            Long quantityId,
            String itemCode,
            String purchaseOrderCode,
            String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_EDIT_ITEM_QUANTITY
                  + purchaseOrderCode
                  + "/"
                  + itemCode
                  + "/"
                  + quantityId;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to Edit Item Quantity with id (\\d+) for Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName,
            Long quantityId,
            String itemCode,
            String purchaseOrderCode,
            String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_EDIT_ITEM_QUANTITY
                  + purchaseOrderCode
                  + "/"
                  + itemCode
                  + "/"
                  + quantityId;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" first deleted Item Quantity with id (\\d+) of Item with code \"([^\"]*)\" of PurchaseOrder with code \"([^\"]*)\" successfully",
        (String userName, Long quantityId, String itemCode, String purchaseOrderCode) -> {
          Response quantityDeleteResponse =
              deleteQuantityUtils.deleteQuantityById(
                  userActionsTestUtils.getUserCookie(userName),
                  quantityId,
                  itemCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, quantityDeleteResponse);
        });

    When(
        "^\"([^\"]*)\" requests to cancel saving Item Quantity with id (\\d+) for Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName,
            Long quantityId,
            String itemCode,
            String purchaseOrderCode,
            String dateTime) -> {
          utils.freeze(dateTime);
          String unLockURL =
              IPurchaseOrderRestUrls.REQUEST_CANCEL_EDIT_ITEM_QUANTITY
                  + purchaseOrderCode
                  + "/"
                  + itemCode
                  + "/"
                  + quantityId;
          Response quantityUnlockResponse = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, quantityUnlockResponse);
        });

    When(
        "^\"([^\"]*)\" requests to cancel saving Item Quantity with id (\\d+) for Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with Code \"([^\"]*)\"$",
        (String userName, Long quantityId, String itemCode, String purchaseOrderCode) -> {
          String unLockURL =
              IPurchaseOrderRestUrls.REQUEST_CANCEL_EDIT_ITEM_QUANTITY
                  + purchaseOrderCode
                  + "/"
                  + itemCode
                  + "/"
                  + quantityId;
          Response quantityUnlockResponse = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, quantityUnlockResponse);
        });

    Given(
        "^\"([^\"]*)\" first deleted Item Quantity with id(\\d+) of Item with code \"([^\"]*)\" of PurchaseOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName,
            Long quantityId,
            String itemCode,
            String purchaseOrderCode,
            String dateTime) -> {
          utils.freeze(dateTime);
          Response quantityDeleteResponse =
              deleteQuantityUtils.deleteQuantityById(
                  userActionsTestUtils.getUserCookie(userName),
                  quantityId,
                  itemCode,
                  purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, quantityDeleteResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderItemTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    deleteQuantityUtils = new DObPurchaseOrderDeleteQuantityTestUtils(properties);
    deleteQuantityUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario
            .getName()
            .contains("Request Edit Item Quantity for Item in Local/Import PurchaseOrder,")
        || scenario
            .getName()
            .contains(
                "Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario
            .getName()
            .contains("Request Edit Item Quantity for Item in Local/Import PurchaseOrder,")
        || scenario
            .getName()
            .contains(
                "Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario
            .getName()
            .contains("Request Edit Item Quantity for Item in Local/Import PurchaseOrder,")
        || scenario
            .getName()
            .contains(
                "Request Cancel Edit Item Quantity for Item in Local/Import PurchaseOrder,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
