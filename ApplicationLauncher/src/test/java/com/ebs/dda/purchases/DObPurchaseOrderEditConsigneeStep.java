package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderCancelTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderEditConsigneeTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsDeliveryCompleteTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSubmitForApprovalTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderEditConsigneeStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderEditConsigneeTestUtils utils;
  private DObPurchaseOrderSubmitForApprovalTestUtils submitForApprovalTestUtils;
  private DObPurchaseOrderCancelTestUtils purchaseOrderCancelTestUtils;
  private DObPurchaseOrderMarkAsDeliveryCompleteTestUtils markAsDeliveryCompleteTestUtils;

  public DObPurchaseOrderEditConsigneeStep() {

    When(
        "^\"([^\"]*)\" requests to edit Consignee section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          String lock_url = IPurchaseOrderRestUrls.LOCK_CONSIGNEE_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Consignee section of PurchaseOrder with code \"([^\"]*)\" becomes in the edit mode and locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.CONSIGNEE_SECTION);
        });

    Given(
        "^\"([^\"]*)\" first opened the Consignee section of PurchaseOrder with code \"([^\"]*)\" in the edit mode successfully$",
        (String userName, String purchaseOrderCode) -> {
          String lock_url = IPurchaseOrderRestUrls.LOCK_CONSIGNEE_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" cancels saving Consignee section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String date) -> {
          utils.freeze(date);
          String unLockURL = IPurchaseOrderRestUrls.UNLOCK_CONSIGNEE_SECTION + purchaseOrderCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on Consignee section of PurchaseOrder with Code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.CONSIGNEE_SECTION);
        });

    When(
        "^\"([^\"]*)\" cancels saving Consignee section of PurchaseOrder with Code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          String unLockURL = IPurchaseOrderRestUrls.UNLOCK_CONSIGNEE_SECTION + purchaseOrderCode;
          Response response = userActionsTestUtils.unlockSection(userName, unLockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^Consignee section of PurchaseOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String username) -> {
          utils.assertThatLockIsReleased(
              username, purchaseOrderCode, IPurchaseOrderSectionNames.CONSIGNEE_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to edit Consignee section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lock_url = IPurchaseOrderRestUrls.LOCK_CONSIGNEE_SECTION + purchaseOrderCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lock_url, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" first MarkedAsDeliveryComplete the PurchaseOrder with code \"([^\"]*)\" successfully at \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String deliveryCompleteDataTime) -> {
          utils.freeze(deliveryCompleteDataTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              markAsDeliveryCompleteTestUtils.markAsDeliveryCompleteAtDateTime(
                  userCookie, purchaseOrderCode, deliveryCompleteDataTime);
          utils.assertResponseSuccessStatus(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderEditConsigneeTestUtils(properties);
    submitForApprovalTestUtils = new DObPurchaseOrderSubmitForApprovalTestUtils(properties);
    purchaseOrderCancelTestUtils = new DObPurchaseOrderCancelTestUtils(properties);
    markAsDeliveryCompleteTestUtils =
        new DObPurchaseOrderMarkAsDeliveryCompleteTestUtils(properties);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Consignee section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Consignee section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Consignee section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }

  @After("@ResetDataForDelete")
  public void resetDataForDelete(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to edit Consignee section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForDelete());
      databaseConnector.closeConnection();
    }
  }
}
