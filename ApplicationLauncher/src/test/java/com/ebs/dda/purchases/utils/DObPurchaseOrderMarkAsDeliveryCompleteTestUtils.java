package com.ebs.dda.purchases.utils;

import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderDeliveryCompleteValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DObPurchaseOrderMarkAsDeliveryCompleteTestUtils
    extends DObPurchaseOrderCommonTestUtils {

  private final String MISSING_FIELDS_KEY = "missingFields";
  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Mark][As][Delivery][Complete]";

  public DObPurchaseOrderMarkAsDeliveryCompleteTestUtils(Map<String, Object> properties) {
    super();
    setProperties(properties);
  }

  public Response markAsDeliveryComplete(
      Cookie loginCookie, String purchaseOrderCode, DataTable deliveryCompleteDataTable) {

    Map<String, String> deliveryCompleteDataMap =
        deliveryCompleteDataTable.asMaps(String.class, String.class).get(0);

    return getMarkAsDeliveryCompleteResponse(
        loginCookie, purchaseOrderCode, deliveryCompleteDataMap);
  }

  public Response markAsDeliveryCompleteAtDateTime(
      Cookie loginCookie, String purchaseOrderCode, String deliveryCompleteDataTime) {

    Map<String, String> deliveryCompleteDataTimeMap = new HashMap<>();
    deliveryCompleteDataTimeMap.put(IExpectedKeys.DELIVERY_COMPLETE_DATE, deliveryCompleteDataTime);

    return getMarkAsDeliveryCompleteResponse(
        loginCookie, purchaseOrderCode, deliveryCompleteDataTimeMap);
  }

  private Response getMarkAsDeliveryCompleteResponse(
      Cookie loginCookie, String purchaseOrderCode, Map<String, String> deliveryCompleteDataMap) {

    JsonObject deliveryCompleteValueObject =
        initDeliveryCompleteValueObject(deliveryCompleteDataMap);

    String restURL = IPurchaseOrderRestUrls.MARK_AS_DELIVERY_COMPLETE + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, deliveryCompleteValueObject);
  }

  public void assertThatGoodsReceiptsExist(DataTable goodsReceiptPurchaseOrderData) {
    List<Map<String, String>> goodsReceiptsPurchaseOrders =
        goodsReceiptPurchaseOrderData.asMaps(String.class, String.class);
    for (Map<String, String> goodsReceiptPurchaseOrder : goodsReceiptsPurchaseOrders) {
      assertThatGoodsReceiptExist(goodsReceiptPurchaseOrder);
      assertThatGoodsReceiptPurchaseOrderDataIsCorrect(goodsReceiptPurchaseOrder);
    }
  }

  private void assertThatGoodsReceiptExist(Map<String, String> goodsReceipt) {
    List<DObGoodsReceiptGeneralModel> goodsReceiptsList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getGoodsReceipts", constructGoodsReceiptQueryParams(goodsReceipt));
    Assert.assertEquals(
        ASSERTION_MSG + " There is no Goods Receipt matches this info",
        1,
        goodsReceiptsList.size());
  }

  private void assertThatGoodsReceiptPurchaseOrderDataIsCorrect(
      Map<String, String> goodsReceiptPurchaseOrderData) {
    List<IObGoodsReceiptPurchaseOrderDataGeneralModel> goodsReceiptPurchaseOrderDataList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getGoodsReceiptPODataByCode",
            constructGoodsReceiptPurchaseOrderQueryParams(goodsReceiptPurchaseOrderData));
    Assert.assertEquals(
        ASSERTION_MSG
            + " There is no Po matches Goods Receipt with code "
            + goodsReceiptPurchaseOrderData.get(IExpectedKeys.CODE),
        1,
        goodsReceiptPurchaseOrderDataList.size());
  }

  private Object[] constructGoodsReceiptPurchaseOrderQueryParams(
      Map<String, String> goodsReceiptPurchaseOrderData) {
    return new Object[] {
      goodsReceiptPurchaseOrderData.get(IExpectedKeys.CODE),
      goodsReceiptPurchaseOrderData.get(IExpectedKeys.PURCHASE_ORDER_CODE),
    };
  }

  private Object[] constructGoodsReceiptQueryParams(Map<String, String> goodsReceipt) {
    String currentStateWithOperation = "%" + goodsReceipt.get(IExpectedKeys.STATE) + "%";
    return new Object[] {
      goodsReceipt.get(IExpectedKeys.CODE),
      currentStateWithOperation,
      goodsReceipt.get(IExpectedKeys.TYPE)
    };
  }

  private JsonObject initDeliveryCompleteValueObject(Map<String, String> deliveryCompleteDateTime) {

    JsonObject deliveryCompleteValueObject = new JsonObject();
    deliveryCompleteValueObject.addProperty(
        IDObPurchaseOrderDeliveryCompleteValueObject.DELIVERY_COMPLETE_DATE_TIME,
        deliveryCompleteDateTime.get(IExpectedKeys.DELIVERY_COMPLETE_DATE));

    return deliveryCompleteValueObject;
  }

  public void assertThatPurchaseOrderIsMarkedAsDeliveryComplete(
      String purchaseOrderCode, DataTable expectedDataTable) {

    Map<String, String> expectedMap = expectedDataTable.asMaps(String.class, String.class).get(0);

    List<DObPurchaseOrder> actualPurchaseOrderList =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getDeliveryCompletePurchaseOrder",
            purchaseOrderCode,
            expectedMap.get(IExpectedKeys.LAST_UPDATED_BY),
            getDateTimeInMachineTimeZone(expectedMap.get(IExpectedKeys.LAST_UPDATE_DATE)),
            '%' + expectedMap.get(IExpectedKeys.STATE) + '%',
            getDateTimeInMachineTimeZone(expectedMap.get(IExpectedKeys.DELIVERY_COMPLETE_DATE)));

    assertTrue(
        ASSERTION_MSG
            + " This Po with code "
            + purchaseOrderCode
            + " is not updated with this info",
        actualPurchaseOrderList.size() == 1);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/clearDataSqlScript.sql");
    str.append("," + "db-scripts/LobSimpleMaterialSqlScript.sql");
    str.append("," + "db-scripts/CObBankSqlScript.sql");
    str.append("," + "db-scripts/CObGeolocaleSqlScript.sql");
    str.append("," + "db-scripts/CObEnterpriseSqlScript.sql");
    str.append("," + "db-scripts/CObControlPointSqlScript.sql");
    str.append("," + "db-scripts/CObCurrencySqlScript.sql");
    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/CObPaymentTermsSqlScript.sql");
    str.append("," + "db-scripts/LobPortSqlScript.sql");
    str.append("," + "db-scripts/CObMeasureScript.sql");
    str.append("," + "db-scripts/master-data/itemGroup/CObMaterialScript.sql");
    str.append("," + "db-scripts/CObShippingInstructionsSqlScript.sql");
    str.append(",").append("db-scripts/master-data/item/MasterData.sql");
    str.append(",").append("db-scripts/master-data/vendor/MasterData.sql");
    str.append(",").append("db-scripts/master-data/customer/MasterData.sql");
    str.append("," + "db-scripts/master-data/item/CObItem.sql");
    str.append("," + "db-scripts/master-data/item/MObItem.sql");
    str.append("," + "db-scripts/master-data/item/IObItemPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item/IObAlternativeUom.sql");
    str.append("," + "db-scripts/master-data/item/MObItem_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObItemPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/item/IObAlternativeUom_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit_ViewAll.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObBusinessPartner.sql");
    str.append(",").append("db-scripts/master-data/vendor/MObVendor.sql");
    str.append(",").append("db-scripts/master-data/vendor/IObVendorPurchaseUnit.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord_ViewAll.sql");
    str.append("," + "db-scripts/master-data/item-vendor-record/ItemVendorRecord.sql");
    str.append(",").append("db-scripts/inventory/goods-receipt/CObGoodsReceipt.sql");

    return str.toString();
  }

  public String getDbScriptsAfterEachScenario() {
    StringBuilder str = new StringBuilder();
    str.append("," + "db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");
    str.append("," + "db-scripts/inventory/goods-receipt/DObGoodsReceiptScript.sql");
    str.append(
            "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptPurchaseOrderDataScript.sql");
    str.append(
            "," + "db-scripts/inventory/goods-receipt/IObGoodsReceiptReceivedItemsDataScript.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");
    str.append("," + "db-scripts/purchase-order/purchase-order-cycle-dates.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderLineDetailsQuantities.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDeliveryDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderPaymentTermsDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderContainersDetails.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderDocumentRequiredDocuments.sql");
    str.append("," + "db-scripts/purchase-order/IObOrderApprovalCycle.sql");

    return str.toString();
  }

  public void assertThatImportPurchaseOrderWithClearanceDateIsExist(DataTable purchaseOrder) {
    Map<String, String> expectedDataMap = purchaseOrder.asMaps(String.class, String.class).get(0);
    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderDetails", expectedDataMap.get("Code"), "%Cleared%");

    assertEquals(expectedDataMap.get("clearanceDate"), actualData.get(0)[2]);
  }

  public void assertThatLocalPurchaseOrderWithShippingDateIsExist(DataTable purchaseOrder) {
    Map<String, String> expectedDataMap = purchaseOrder.asMaps(String.class, String.class).get(0);
    List<Object[]> actualData =
        entityManagerDatabaseConnector.executeNativeNamedQuery(
            "getPurchaseOrderDetails", expectedDataMap.get("Code"), "%Shipped%");

    assertEquals(
        ASSERTION_MSG
            + " This PO with Code "
            + expectedDataMap.get("Code")
            + " doesn't match Shipped Status",
        expectedDataMap.get("ShippingDate"),
        actualData.get(0)[3]);
  }
}
