package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewPaymentAndDeliveryTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderViewPaymentAndDeliveryStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecuted = false;
  private DObPurchaseOrderViewPaymentAndDeliveryTestUtils utils;

  public DObPurchaseOrderViewPaymentAndDeliveryStep() {

    And(
        "^the following PurchaseOrders exist with the following PaymentAndDelivery data:$",
        (DataTable paymentAndDeliveryTable) -> {
          utils.assertThatPurchaseOrderExistWithPaymentAndDeliveryData(paymentAndDeliveryTable);
        });

    When(
        "^\"([^\"]*)\" requests to view PaymentandDelivery section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String code) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, IDObPurchaseOrderRestUrls.VIEW_PAYMENT_AND_DELIVERY + code);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following values of PaymentandDelivery section are displayed to \"([^\"]*)\":$",
        (String userName, DataTable paymentAndDeliveryTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatPaymentAndDeliveryReadResponseIsCorrect(
              paymentAndDeliveryTable, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderViewPaymentAndDeliveryTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View PaymentandDelivery section")) {
      if (!hasBeenExecuted) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecuted = true;
      }
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View PaymentandDelivery section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
