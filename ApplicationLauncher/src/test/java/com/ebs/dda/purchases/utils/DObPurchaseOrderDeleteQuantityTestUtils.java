package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import javax.persistence.NoResultException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class DObPurchaseOrderDeleteQuantityTestUtils extends DObPurchaseOrderItemTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[DeleteQuantity]";
  public DObPurchaseOrderDeleteQuantityTestUtils(Map<String, Object> properties) {
    super(properties);
  }

  public Response deleteQuantityById(
      Cookie loginCookie, Long quantityId, String itemCode, String purchaseOrderCode) {
    String deleteURL =
        IPurchaseOrderRestUrls.DELETE_QUANTITY_URL
            + purchaseOrderCode
            + '/'
            + itemCode
            + '/'
            + quantityId;
    return sendDeleteRequest(loginCookie, deleteURL);
  }

  public void assertQuantityIsDeleted(Long quantityId) {

    try {
      entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
          "getPurchaseOrderItemQuantityById", quantityId);
      assertFalse("Purchase Order Item Quantity has not been deleted", false);
    } catch (NoResultException noResultException) {
      assertTrue(true);
    }
  }


  public void assertPurchaseOrderLineDetailsIsUpdated(
      String itemCode, String purchaseOrderCode, DataTable itemModificationInfo) {
    Map<String, String> expectedOrderLineDetails =
        itemModificationInfo.asMaps(String.class, String.class).get(0);

    IObOrderLineDetailsGeneralModel orderHeaderGeneralModel =
        (IObOrderLineDetailsGeneralModel)
            entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                    "getOrderLineByItemCodeAndModificationInfoAndOrderCode",
                    itemCode,
                    purchaseOrderCode,
                    expectedOrderLineDetails.get(IFeatureFileCommonKeys.LAST_UPDATE_DATE),
                    expectedOrderLineDetails.get(IFeatureFileCommonKeys.LAST_UPDATED_BY));
    assertNotNull(ASSERTION_MSG + " There is no Line Details for this PO with code " + purchaseOrderCode , orderHeaderGeneralModel);
  }

  public void assertThatPurchaseOrdersExistWithItemsAndQuantities(DataTable dataTable) {

    List<Map<String, String>> expectedOrderLineDetails =
        dataTable.asMaps(String.class, String.class);

    for (Map<String, String> record : expectedOrderLineDetails) {

      IObOrderLineDetailsQuantitiesGeneralModel orderHeaderGeneralModel =
          (IObOrderLineDetailsQuantitiesGeneralModel)
              entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
                  "getPurchaseOrderItemQuantity",
                  record.get(IFeatureFileCommonKeys.PO_CODE),
                  record.get(IFeatureFileCommonKeys.ITEM_CODE),
                  Long.valueOf(record.get(IFeatureFileCommonKeys.QUANTITY_ID)));

      assertNotNull(ASSERTION_MSG + " There is no Item Quantity for this PO with code " + record.get(IFeatureFileCommonKeys.PO_CODE) ,orderHeaderGeneralModel);
    }
  }
}
