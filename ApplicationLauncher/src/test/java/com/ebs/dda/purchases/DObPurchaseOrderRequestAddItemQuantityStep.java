package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.utils.DObPurchaseOrderItemTestUtils;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderRequestAddItemQuantityStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderItemTestUtils utils;

  public DObPurchaseOrderRequestAddItemQuantityStep() {

    When(
        "^\"([^\"]*)\" requests to add Item Quantity for Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_ADD_ITEM_QUANTITY + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^a new add Item Quantity dialoge is opened and OrderItems section of PurchaseOrder with code \"([^\"]*)\" becomes locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertResponseSuccessStatus(userActionsTestUtils.getUserResponse(userName));
        });

    When(
        "^OrderItems section of PurchaseOrder with code \"([^\"]*)\" is not locked by \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    When(
        "^\"([^\"]*)\" requests to add Item Quantity for Item with code \"([^\"]*)\" to OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_ADD_ITEM_QUANTITY + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Given(
        "^\"([^\"]*)\" opens Item Quantity for Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\" in edit mode at \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_ADD_ITEM_QUANTITY + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to cancel saving Item Quantity for Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode, String dateTime) -> {
          utils.freeze(dateTime);
          String unlockURL =
              IPurchaseOrderRestUrls.REQUEST_CANCEL_ADD_ITEM_QUANTITY
                  + purchaseOrderCode
                  + '/'
                  + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the lock by \"([^\"]*)\" on OrderItems section of PurchaseOrder with code \"([^\"]*)\" is released$",
        (String userName, String purchaseOrderCode) -> {
          utils.assertThatLockIsReleased(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        });

    Given(
        "^\"([^\"]*)\" opens Item Quantity for Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\" in edit mode$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          String lockURL =
              IPurchaseOrderRestUrls.REQUEST_ADD_ITEM_QUANTITY + purchaseOrderCode + "/" + itemCode;
          Response response =
              userActionsTestUtils.lockSection(userName, lockURL, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to cancel saving Item Quantity for Item with code \"([^\"]*)\" in OrderItems section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String itemCode, String purchaseOrderCode) -> {
          String unlockURL =
              IPurchaseOrderRestUrls.REQUEST_CANCEL_ADD_ITEM_QUANTITY
                  + purchaseOrderCode
                  + '/'
                  + itemCode;
          Response response = userActionsTestUtils.unlockSection(userName, unlockURL);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();

    utils = new DObPurchaseOrderItemTestUtils(getProperties());
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (scenario
            .getName()
            .contains("Request Add Item Quantity to Item for Local/Import PurchaseOrder,")
        || scenario
            .getName()
            .contains("Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder,")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (scenario
            .getName()
            .contains("Request Add Item Quantity to Item for Local/Import PurchaseOrder,")
        || scenario
            .getName()
            .contains("Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder,")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (scenario
            .getName()
            .contains("Request Add Item Quantity to Item for Local/Import PurchaseOrder,")
        || scenario
            .getName()
            .contains("Request Cancel Add Item Quantity to Item for Local/Import PurchaseOrder,")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPathForItemQuantity());
      databaseConnector.closeConnection();
    }
  }
}
