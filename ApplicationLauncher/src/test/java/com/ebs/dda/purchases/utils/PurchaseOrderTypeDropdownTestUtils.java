package com.ebs.dda.purchases.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.order.salesreturnorder.utils.DObSalesReturnOrderCommonTestUtils;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PurchaseOrderTypeDropdownTestUtils extends DObSalesReturnOrderCommonTestUtils {

  public PurchaseOrderTypeDropdownTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    super(properties, entityManagerDatabaseConnector);
  }

  public Response readAllTypes(Cookie cookie) {
    String restURL = IPurchaseOrderRestUrls.READ_ALL_TYPES;
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatTypesResponseIsCorrect(DataTable typesDataTable, Response response) {
    List<Map<String, String>> expectedTypeMapList =
        typesDataTable.asMaps(String.class, String.class);
    List<HashMap<String, Object>> actualTypesList = getListOfMapsFromResponse(response);
    Assert.assertEquals(expectedTypeMapList.size(), actualTypesList.size());
    for (int i = 0; i < expectedTypeMapList.size(); i++) {
      assertThatExpectedTypeMatchesActual(expectedTypeMapList.get(i), actualTypesList.get(i));
    }
  }

  private void assertThatExpectedTypeMatchesActual(
      Map<String, String> expectedType, Object actualType) {
    Assert.assertEquals(expectedType.get(IFeatureFileCommonKeys.TYPE), actualType);
  }

  public void assertThatTotalNumberOfPOTypesEquals(
      Integer expectedTotalNumber, Response response) {
    List<HashMap<String, Object>> actualTypes = getListOfMapsFromResponse(response);
    Assert.assertEquals(
        "Total number of PO types is not correct",
        expectedTotalNumber.intValue(),
        actualTypes.size());
  }
}
