package com.ebs.dda.purchases.utils;

import com.ebs.dda.EntityManagerDatabaseConnector;
import com.ebs.dda.purchases.IPurchaseOrderRestUrls;

import java.util.Map;

public class DObServicePurchaseOrderRequestAddItemTestUtils
    extends DObPurchaseOrderCommonTestUtils {

  public String ASSERTION_MSG = super.ASSERTION_MSG + "[RequestAdd][Items][Service]";

  public DObServicePurchaseOrderRequestAddItemTestUtils(
      Map<String, Object> properties,
      EntityManagerDatabaseConnector entityManagerDatabaseConnector) {
    setProperties(properties);
    setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }


  public String getUnLockItemsURL(String purchaseOrderCode) {
    return IPurchaseOrderRestUrls.COMMAND_PURCHASE_ORDER
        + purchaseOrderCode
        + IPurchaseOrderRestUrls.UNLOCK_SERVICE_ITEM_SECTION;
  }
}
