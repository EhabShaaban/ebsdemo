package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderConsigneeValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderSaveConsigneeTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderSaveConsigneeStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderSaveConsigneeTestUtils utils;

  public DObPurchaseOrderSaveConsigneeStep() {

    Given(
        "^the following Plants are related to Company with code \"([^\"]*)\":$",
        (String companyCode, DataTable plantsDataTable) -> {
          utils.assertThatPlantsRelateToCompany(plantsDataTable, companyCode);
        });

    Given(
        "^the following Storehouses for Plant with code \"([^\"]*)\" exist:$",
        (String plantCode, DataTable storehousesDataTable) -> {
          utils.assertThatStorehousesRelateToPlant(storehousesDataTable, plantCode);
        });

    Given(
        "^Consignee section of PurchaseOrder with Code \"([^\"]*)\" is locked by \"([^\"]*)\" at \"([^\"]*)\"$",
        (String purchaseOrderCode, String userName, String dateTime) -> {
          utils.freeze(dateTime);
          Response response =
              userActionsTestUtils.lockSection(
                  userName,
                  IPurchaseOrderRestUrls.LOCK_CONSIGNEE_SECTION + purchaseOrderCode,
                  purchaseOrderCode);
          utils.assertResponseSuccessStatus(response);
          utils.assertThatResourceIsLockedByUser(
              userName, purchaseOrderCode, IPurchaseOrderSectionNames.CONSIGNEE_SECTION);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" saves Consignee section of PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String dateTime,
            DataTable consigneeDataTable) -> {
          utils.freeze(dateTime);
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePurchaseOrderConsigneeSection(
                  userCookie, purchaseOrderCode, consigneeDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated as follows after save consignee:$",
        (String purchaseOrderCode, DataTable consigneeData) -> {
          utils.assertThatConsigneeUpdatedWithGivenValues(purchaseOrderCode, consigneeData);
        });

    And(
        "^the following error message is attached to Consignee field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObPurchaseOrderConsigneeValueObject.CONSIGNEE_CODE);
        });

    And(
        "^the following error message is attached to Plant field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObPurchaseOrderConsigneeValueObject.PLANT_CODE);
        });

    And(
        "^the following error message is attached to Storehouse field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObPurchaseOrderConsigneeValueObject.STOREHOUSE_CODE);
        });

    When(
        "^\"([^\"]*)\" saves Consignee section of PurchaseOrder with Code \"([^\"]*)\" with the following values:$",
        (String userName, String purchaseOrderCode, DataTable consigneeDataTable) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              utils.savePurchaseOrderConsigneeSection(
                  userCookie, purchaseOrderCode, consigneeDataTable);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new DObPurchaseOrderSaveConsigneeTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Consignee section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Consignee section")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Save Consignee section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
