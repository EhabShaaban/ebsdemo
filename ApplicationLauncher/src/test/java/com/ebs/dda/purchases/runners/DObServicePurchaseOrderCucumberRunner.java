package com.ebs.dda.purchases.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/ordermanagement/purchase-order/SPO_Item_Delete.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_View_PaymentDetails.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_View_Items.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_Item_UOM_Dropdown.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_Save_Add_Item_Auth.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_Save_Add_Item_Val.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_RequestAdd_Item.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_Save_PaymentDetails.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_Save_PaymentDetails_Auth.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_RequestEdit_PaymentDetails.feature",
      "classpath:features/ordermanagement/purchase-order/SPO_Save_Add_Item_HP.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.purchases"},
    strict = true,
    tags = "not @Future")
public class DObServicePurchaseOrderCucumberRunner {}
