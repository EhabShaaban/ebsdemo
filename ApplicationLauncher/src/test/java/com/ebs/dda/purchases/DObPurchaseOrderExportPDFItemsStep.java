package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.masterdata.ivr.utils.ItemVendorRecordCommonTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderExportPDFItemsTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewItemsTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewItemsWithPricesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderExportPDFItemsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderExportPDFItemsTestUtils exportPDFItemsTestUtils;
  private DObPurchaseOrderViewItemsWithPricesTestUtils viewPOItemsUtils;
  private ItemVendorRecordCommonTestUtils itemVendorRecordTestUtils;

  public DObPurchaseOrderExportPDFItemsStep() {

    When(
        "^\"([^\"]*)\" requests to export PurchaseOrder Items of PurchaseOrder with code \"([^\"]*)\" as PDF$",
        (String username, String purchaseOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(username);
          Response response =
              exportPDFItemsTestUtils.exportPurchaseOrderAsPDF(userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(username, response);
        });

    Then(
        "^the following values of PurchaseOrder Items are displayed to \"([^\"]*)\":$",
        (String username, DataTable purchaseOrderItems) -> {
          Response response = userActionsTestUtils.getUserResponse(username);
          viewPOItemsUtils.assertThatActualPurchaseOrderItemsAreCorrect(
              response, purchaseOrderItems);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    exportPDFItemsTestUtils = new DObPurchaseOrderExportPDFItemsTestUtils(properties);
    exportPDFItemsTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    viewPOItemsUtils = new DObPurchaseOrderViewItemsWithPricesTestUtils(properties);
    viewPOItemsUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    DObPurchaseOrderViewItemsTestUtils.setItemQuantities("quantities");
    itemVendorRecordTestUtils = new ItemVendorRecordCommonTestUtils(properties);
    itemVendorRecordTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Request to export PurchaseOrder Items")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(exportPDFItemsTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(
            exportPDFItemsTestUtils.getNewItemVendorRecordDataDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }
}
