package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderAssignMeToApproveTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderStateTransitionTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderAssignMeToApproveStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderAssignMeToApproveTestUtils assignMeTestUtils;

  public DObPurchaseOrderAssignMeToApproveStep() {

    Given(
        "^the following control point users and alternative users exist:$",
        (DataTable usersAndAlternatives) -> {
          assignMeTestUtils.assertThatControlPointUsersHasAnAlternativeUsers(usersAndAlternatives);
        });

    Given(
        "^the ApprovalCycleNo \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" has the following values:$",
        (String approvalCycleNum, String purchaseOrderCode, DataTable approvalCycleDetails) -> {
          assignMeTestUtils.assertThatApprovalCycleDetailsAreCorrect(
              approvalCycleNum, purchaseOrderCode, approvalCycleDetails);
        });

    When(
        "^\"([^\"]*)\" requests to assign himself to Approve the PurchaseOrder with Code \"([^\"]*)\" instead of \"([^\"]*)\" for control point with code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String userName,
            String purchaseOrderCode,
            String mainUser,
            String controlPointCode,
            String dateTime) -> {
          assignMeTestUtils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              assignMeTestUtils.assignMeToApprovePurchaseOrder(
                  cookie, purchaseOrderCode, mainUser, controlPointCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to assign himself to Approve the PurchaseOrder with Code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              assignMeTestUtils.assignMeToApprovePurchaseOrder(cookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to assign himself to Approve the PurchaseOrder with Code \"([^\"]*)\" instead of \"([^\"]*)\" for control point with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode, String mainUser, String controlPointCode) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              assignMeTestUtils.assignMeToApprovePurchaseOrder(
                  cookie, purchaseOrderCode, mainUser, controlPointCode);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following details of ApprovalCycleNo \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" is updated as follows:$",
        (String approvalCycleNum, String purchaseOrderCode, DataTable approvalCycleDetails) -> {
          assignMeTestUtils.assertThatApprovalCycleDetailsAreCorrect(
              approvalCycleNum, purchaseOrderCode, approvalCycleDetails);
        });

    Then(
        "^the following details of ApprovalCycleNo \"([^\"]*)\" in PurchaseOrder with code \"([^\"]*)\" is not updated as follows:$",
        (String approvalCycleNum, String purchaseOrderCode, DataTable approvalCycleDetails) -> {
          assignMeTestUtils.assertThatApprovalCycleDetailsAreCorrect(
              approvalCycleNum, purchaseOrderCode, approvalCycleDetails);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    assignMeTestUtils = new DObPurchaseOrderAssignMeToApproveTestUtils(properties);
    assignMeTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    stateTransitionUtils = new DObPurchaseOrderStateTransitionTestUtils(properties);
    stateTransitionUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "I want to assign myself")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(assignMeTestUtils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(assignMeTestUtils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "I want to assign myself")) {
      assignMeTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, assignMeTestUtils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "I want to assign myself")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(assignMeTestUtils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
