package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.PurchaseOrderSaveAttachmentTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.sql.SQLException;

public class DObPurchaseOrderSaveAttachmentStep extends SpringBootRunner implements En {

  private PurchaseOrderSaveAttachmentTestUtils purchaseOrderSaveAttachmentTestUtils;

  DObPurchaseOrderSaveAttachmentStep() {

    And(
        "^the following DocumentType exist:$",
        (DataTable documentTypesDataTable) -> {
          purchaseOrderSaveAttachmentTestUtils.assertThatDocumentTypesExist(documentTypesDataTable);
        });

    Then(
        "^attachment section in PurchaseOrder with Code \"([^\"]*)\" has the following attachements:$",
        (String purchaseOrderCode, DataTable attachments) -> {
          purchaseOrderSaveAttachmentTestUtils
              .assertThatPurchaseOrderAttachmentIsUpdatedWithTheseValues(
                  purchaseOrderCode, attachments);
        });
    And(
        "^\"([^\"]*)\" requests to upload attachment to PurchaseOrder with code \"([^\"]*)\" at \"([^\"]*)\" with following values:$",
        (String userName, String purchaseOrderCode, String dateTime, DataTable attachmentData) -> {
          purchaseOrderSaveAttachmentTestUtils.freeze(dateTime);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response =
              this.purchaseOrderSaveAttachmentTestUtils.saveGoodsReceiptAttachmentData(
                  attachmentData, purchaseOrderCode, cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    purchaseOrderSaveAttachmentTestUtils =
        new PurchaseOrderSaveAttachmentTestUtils(getProperties());
    purchaseOrderSaveAttachmentTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Upload Attachments")) {
      databaseConnector.executeSQLScript(purchaseOrderSaveAttachmentTestUtils.getDbScriptPath());
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws SQLException {
    if (contains(scenario.getName(), "Upload Attachments")) {
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          purchaseOrderSaveAttachmentTestUtils.getSectionsNames());
    }
  }
}
