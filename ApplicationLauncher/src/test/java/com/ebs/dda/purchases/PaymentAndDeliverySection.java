package com.ebs.dda.purchases;

public interface PaymentAndDeliverySection {

  String PAYMENT_TERMS_JSON_KEY = "paymentTerms";
  String INCOTERM_JSON_KEY = "incoterm";
  String SHIPPING_INSTRUCTIONS_JSON_KEY = "shippingInstructions";
  String TRANSPORT_MODE_JSON_KEY = "modeOfTransport";
  String LOADING_PORT_JSON_KEY = "loadingPort";
  String DISCHARGE_PORT_JSON_KEY = "dischargePort";
  String COLLECTION_DATE_JSON_KEY = "collectionDate";
  String NUMBER_OF_DAYS_JSON_KEY = "numberOfDays";
  String CURRENCY_JSON_KEY = "currency";
  String CONTAINERS_TYPE_JSON_KEY = "containersType";
  String CONTAINERS_NO_JSON_KEY = "containersNo";

  String EXPECTED_INCOTERM_KEY = "Incoterm";
  String EXPECTED_CONTAINER_NO_KEY = "ContainersNo";
  String EXPECTED_TRANSPORT_MODE_KEY = "ModeOfTransport";
  String EXPECTED_SHIPPING_INSTRUCTIONS_KEY = "ShippingInstructions";
  String EXPECTED_LOADING_PORT_KEY = "LoadingPort";
  String EXPECTED_DISCHARGE_PORT_KEY = "DischargePort";
  String EXPECTED_CURRENCY_KEY = "Currency";
  String EXPECTED_PAYMENT_TERMS_KEY = "PaymentTerms";
  String EXPECTED_CONTAINERS_TYPE_KEY = "ContainersType";
  String EXPECTED_COLLECTION_DATE_KEY = "CollectionDate_SpecificDate";
  String EXPECTED_NUMBER_OF_DAYS_KEY = "CollectionDate_TimePeriod";

}
