package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.AllowedActionsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class DObPurchaseOrderAllowedActionsStep extends SpringBootRunner implements En {

  private AllowedActionsTestUtils utils;

  public DObPurchaseOrderAllowedActionsStep() {
    Given(
        "^the following PurchaseOrders exist:$",
        (DataTable purchaseOrderDataTable) -> {
          utils.assertPurchaseOrdersExistWithBasicData(purchaseOrderDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          String readUrl = utils.getAuthorizedActionsUrl(purchaseOrderCode);
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    When(
        "^\"([^\"]*)\" requests to read actions of PurchaseOrder home screen$",
        (String userName) -> {
          String readUrl = utils.getAuthorizedActionsUrl();
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.sendGETRequest(cookie, readUrl);
          userActionsTestUtils.setUserResponse(userName, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new AllowedActionsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Read PO Object Screen Allowed Actions")) {
      databaseConnector.executeSQLScript(utils.getDbScripts());
    }
  }
}
