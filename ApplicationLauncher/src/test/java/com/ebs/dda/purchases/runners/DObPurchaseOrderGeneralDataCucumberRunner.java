package com.ebs.dda.purchases.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    features = {
      "classpath:features/ordermanagement/purchase-order/PO_AllowedActions_ObjectScreen.feature",
      "classpath:features/ordermanagement/purchase-order/PO_AllowedActions_HomeScreen.feature",
      "classpath:features/ordermanagement/purchase-order/PO_ViewAll.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Create_HP.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Create_Val.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Create_Auth.feature",
      "classpath:features/ordermanagement/purchase-order/PO_NextPrev.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Delete.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_GeneralData.feature",
      "classpath:features/ordermanagement/purchase-order/PO_DocumentOwner_Dropdown.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Type_Dropdown.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_CompanyData.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestEdit_CompanyData.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_CompanyData.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_Consignee.feature",
      "classpath:features/ordermanagement/purchase-order/PO_RequestEdit_ConsigneeData.feature",
      "classpath:features/ordermanagement/purchase-order/PO_Save_ConsigneeData.feature",
      "classpath:features/ordermanagement/purchase-order/PO_View_Vendor.feature"
    },
    glue = {"com.ebs.dda.generaldefinitions", "com.ebs.dda.purchases"},
    strict = true,
    tags = "not @Future")
public class DObPurchaseOrderGeneralDataCucumberRunner {}
