package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.PurchaseOrderTypeDropdownTestUtils;
import cucumber.api.DataTable;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class PurchaseOrderTypeDropdownStep extends SpringBootRunner implements En {

  private PurchaseOrderTypeDropdownTestUtils utils;

  public PurchaseOrderTypeDropdownStep() {
    When(
        "^\"([^\"]*)\" requests to read all PurchaseOrder types in a dropdown$",
        (String userName) -> {
          Cookie cookie = userActionsTestUtils.getUserCookie(userName);
          Response response = utils.readAllTypes(cookie);
          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^the following PurchaseOrder type values will be presented to \"([^\"]*)\" in a dropdown:$",
        (String userName, DataTable typesDataTable) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTypesResponseIsCorrect(typesDataTable, response);
        });

    Then(
        "^total number of PurchaseOrder types returned to \"([^\"]*)\" in a dropdown is equal to (\\d+)$",
        (String userName, Integer typesCount) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatTotalNumberOfPOTypesEquals(typesCount, response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new PurchaseOrderTypeDropdownTestUtils(getProperties(), entityManagerDatabaseConnector);
  }
}
