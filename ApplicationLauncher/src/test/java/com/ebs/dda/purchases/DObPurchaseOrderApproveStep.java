package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderApproveTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderStateTransitionTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderViewApprovalCyclesTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

import java.util.Map;

public class DObPurchaseOrderApproveStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderApproveTestUtils utils;
  private DObPurchaseOrderViewApprovalCyclesTestUtils approvalCyclesTestUtils;

  public DObPurchaseOrderApproveStep() {

    Given(
        "^the following ControlPoints exist:$",
        (DataTable controlPointsDataTable) -> {
          approvalCyclesTestUtils.assertThatControlPointsExist(controlPointsDataTable);
        });

    When(
        "^\"([^\"]*)\" requests to Approve the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String username,
            String purchaseOrderCode,
            String approvalDateTime,
            DataTable approvalData) -> {
          utils.freeze(approvalDateTime);
          Response approveResponse =
              utils.approvePurchaseOrder(
                  userActionsTestUtils.getUserCookie(username), purchaseOrderCode, approvalData);
          userActionsTestUtils.setUserResponse(username, approveResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderApproveTestUtils(properties);
    approvalCyclesTestUtils = new DObPurchaseOrderViewApprovalCyclesTestUtils(properties);
    approvalCyclesTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    stateTransitionUtils = new DObPurchaseOrderStateTransitionTestUtils(properties);
    stateTransitionUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Approve the PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Approve the PO")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          approvalCyclesTestUtils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Approve the PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
