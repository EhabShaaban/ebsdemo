package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.ebs.dda.purchases.IExpectedKeys;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObPurchaseOrderDocumentValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObPurchaseOrderRequiredDocumentsValueObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/** Created by "Zyad Ghorab" on Nov, 2018 */
public class IObPurchaseOrderRequiredDocumentsTestUtils extends DObPurchaseOrderCommonTestUtils {

  protected String ASSERTION_MSG = super.ASSERTION_MSG + "[RequiredDocs]";

  public IObPurchaseOrderRequiredDocumentsTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    urlPrefix = getProperty(URL_PREFIX).toString();
  }

  public String getDbScriptPath() {

    StringBuilder str = new StringBuilder();
    str.append("db-scripts/purchase-order/DObOrderDocument.sql");
    str.append("," + "db-scripts/purchase-order/DObPurchaseOrder.sql");

    str.append("," + "db-scripts/IObCompanyBankDetailsSqlScript.sql");
    str.append("," + "db-scripts/purchase-order/IObEnterpriseData.sql");

    str.append("," + "db-scripts/purchase-order/IObOrderDocumentRequiredDocuments.sql");
    return str.toString();
  }

  public void assertThatImportPurchaseOrdersExist(DataTable purchaseOrderTable) throws Exception {
    List<Map<String, String>> purchaseOrderData =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrderData) {
      assertThatPurchaseOrderExistWithGivenStateAndPurchaseUnit(
          purchaseOrder, OrderTypeEnum.IMPORT_PO.name());
    }
  }

  public void assertThatLocalPurchaseOrdersExist(DataTable purchaseOrderTable) throws Exception {
    List<Map<String, String>> purchaseOrderData =
        purchaseOrderTable.asMaps(String.class, String.class);
    for (Map<String, String> purchaseOrder : purchaseOrderData) {
      assertThatPurchaseOrderExistWithGivenStateAndPurchaseUnit(
          purchaseOrder, OrderTypeEnum.LOCAL_PO.name());
    }
  }

  public Response saveEmptyPurchaseOrderRequiredDocumentsSection(
      Cookie loginCookie, String purchaseOrderCode) {
    JsonObject requiredDocumentsDataJsonObject =
        createEmptyPurchaseOrderRequiredDocumentsJsonObject(purchaseOrderCode);
    String restURL = "/command/purchaseorder/update/requireddocuments/" + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, requiredDocumentsDataJsonObject);
  }

  public Response savePurchaseOrderRequiredDocumentsSection(
      Cookie loginCookie, String purchaseOrderCode, DataTable requiredDocumentsDataTable) {
    JsonObject requiredDocumentsDataJsonObject =
        createPurchaseOrderRequiredDocumentsJsonObject(
            purchaseOrderCode, requiredDocumentsDataTable);
    String restURL = "/command/purchaseorder/update/requireddocuments/" + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, requiredDocumentsDataJsonObject);
  }

  public void assertThatDocumentTypesExist(DataTable requiredDocumentTable) throws Exception {
    List<Map<String, String>> requiredDocumentMapList =
        requiredDocumentTable.asMaps(String.class, String.class);
    for (Map<String, String> requiredDocumentMap : requiredDocumentMapList) {
      Object requiredDocument =
          entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
              "getRequiredDocumentByCode", requiredDocumentMap.get(ENTITY_CODE));
      assertNotNull(
          ASSERTION_MSG
              + " Required document type "
              + requiredDocumentMap.get(ENTITY_CODE)
              + " does not exist",
          requiredDocument);
    }
  }

  private JsonObject createEmptyPurchaseOrderRequiredDocumentsJsonObject(String purchaseOrderCode) {
    JsonObject emptyRequiredDocumentsJsonObject = new JsonObject();
    emptyRequiredDocumentsJsonObject.addProperty(
        IIObPurchaseOrderRequiredDocumentsValueObject.PURCHASE_ORDER_CODE, purchaseOrderCode);
    emptyRequiredDocumentsJsonObject.add(
        IIObPurchaseOrderRequiredDocumentsValueObject.PURCHASE_ORDER_DOCUMENTS, new JsonArray());
    return emptyRequiredDocumentsJsonObject;
  }

  private JsonObject createPurchaseOrderRequiredDocumentsJsonObject(
      String purchaseOrderCode, DataTable requiredDocumentsDataTable) {
    JsonObject requiredDocumentsJsonObject = new JsonObject();
    JsonArray documents = new JsonArray();
    List<Map<String, String>> requiredDocumentsData =
        requiredDocumentsDataTable.asMaps(String.class, String.class);
    for (Map<String, String> document : requiredDocumentsData) {
      addRequiredDocumentJsonObject(document, documents);
    }
    requiredDocumentsJsonObject.addProperty(
        IIObPurchaseOrderRequiredDocumentsValueObject.PURCHASE_ORDER_CODE, purchaseOrderCode);
    requiredDocumentsJsonObject.add(
        IIObPurchaseOrderRequiredDocumentsValueObject.PURCHASE_ORDER_DOCUMENTS, documents);
    return requiredDocumentsJsonObject;
  }

  private void addRequiredDocumentJsonObject(Map<String, String> document, JsonArray documents) {
    JsonObject documentJsonObject = new JsonObject();
    String copies = document.get(IExpectedKeys.COPIES);
    Integer numberOfCopies =
        copies.isEmpty() || !isIntegerValue(copies) ? null : Integer.parseInt(copies);
    String documentTypeCode =
        document.get(IExpectedKeys.DOCUMENT_TYPE).isEmpty()
            ? null
            : document.get(IExpectedKeys.DOCUMENT_TYPE);

    documentJsonObject.addProperty(
        IIObPurchaseOrderDocumentValueObject.DOCUMENT_TYPE_CODE, documentTypeCode);
    documentJsonObject.addProperty(
        IIObPurchaseOrderDocumentValueObject.DOCUMENT_NUMBER_OF_COPIES, numberOfCopies);
    documents.add(documentJsonObject);
  }

  private boolean isIntegerValue(String copies) {
    return copies.matches("\\d+");
  }

  protected void assertThatRecordUpdatedDate(String expectedUpdatedDate, ResultSet resultSet)
      throws SQLException {
    DateTime actualLastUpdateDate =
        new DateTime(resultSet.getTimestamp(PURCHASE_MODIFIED_DATE).getTime())
            .withZone(DateTimeZone.UTC);
    assertDateEquals(expectedUpdatedDate, actualLastUpdateDate);
  }

  protected void assertThatRecordUpdatedBy(String expectedUpdatedUser, ResultSet resultSet)
      throws SQLException {
    String actualLastUpdateBy = resultSet.getString(PURCHASE_MODIFICATION_INFO);
    assertTrue(
        ASSERTION_MSG + " Updated User " + expectedUpdatedUser + " is not correct",
        actualLastUpdateBy.contains(expectedUpdatedUser));
  }
}
