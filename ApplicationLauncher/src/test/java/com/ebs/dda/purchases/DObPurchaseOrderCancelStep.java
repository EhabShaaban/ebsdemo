package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.DObPurchaseOrderCancelTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsClearedTestUtils;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsDeliveryCompleteTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;
import java.util.Map;

public class DObPurchaseOrderCancelStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderCancelTestUtils utils;
  private DObPurchaseOrderMarkAsClearedTestUtils markAsClearedTestUtils;
  private DObPurchaseOrderMarkAsDeliveryCompleteTestUtils markAsDeliveryCompleteTestUtils;

  public DObPurchaseOrderCancelStep() {

    When(
        "^\"([^\"]*)\" requests to cancel the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\"$",
        (String username, String purchaseOrderCode, String cancelTime) -> {
          utils.freeze(cancelTime);
          Response response =
              utils.cancelPurchaseOrder(
                  purchaseOrderCode, userActionsTestUtils.getUserCookie(username));
          userActionsTestUtils.setUserResponse(username, response);
        });

    Given(
        "^first \"([^\"]*)\" changed state of PurchaseOrder with Code \"([^\"]*)\" to Cleared successfully with the following values:$",
        (String username, String purchaseOrderCode, DataTable clearanceDataTable) -> {
          Response response =
              markAsClearedTestUtils.markAsCleared(
                  userActionsTestUtils.getUserCookie(username),
                  purchaseOrderCode,
                  clearanceDataTable);
          utils.assertResponseSuccessStatus(response);
        });

    Given(
        "^then \"([^\"]*)\" changed state of PurchaseOrder with Code \"([^\"]*)\" to DeliveryComplete successfully with the following values:$",
        (String username, String purchaseOrderCode, DataTable deliveryCompleteDataTable) -> {
          Response response =
              markAsDeliveryCompleteTestUtils.markAsDeliveryComplete(
                  userActionsTestUtils.getUserCookie(username),
                  purchaseOrderCode,
                  deliveryCompleteDataTable);
          utils.assertResponseSuccessStatus(response);
        });

    Given(
        "^first \"([^\"]*)\" changed state of PurchaseOrder with Code \"([^\"]*)\" to DeliveryComplete successfully with the following values:$",
        (String username, String purchaseOrderCode, DataTable deliveryCompleteDataTable) -> {
          Response response =
              markAsDeliveryCompleteTestUtils.markAsDeliveryComplete(
                  userActionsTestUtils.getUserCookie(username),
                  purchaseOrderCode,
                  deliveryCompleteDataTable);
          utils.assertResponseSuccessStatus(response);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    Map<String, Object> properties = getProperties();
    utils = new DObPurchaseOrderCancelTestUtils(properties);
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    markAsClearedTestUtils = new DObPurchaseOrderMarkAsClearedTestUtils(properties);
    markAsClearedTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
    markAsDeliveryCompleteTestUtils =
        new DObPurchaseOrderMarkAsDeliveryCompleteTestUtils(properties);
    markAsDeliveryCompleteTestUtils.setEntityManagerDatabaseConnector(
        entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Cancel PO")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void afterEachScenario(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Cancel PO")) {
      utils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS, utils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Cancel PO")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
