package com.ebs.dda.purchases;

public interface IAssertionMessages {

  String PAYMENT_AND_DELIVERY = "[ORDER PAYMENT AND DELIVERY]";

  String QUERY_IS_NOT_NULL = " Query result is not null";
  String QUERY_IS_SINGLE = " Query result has only 1 record";

  String ORDER_PAYMENT_AND_DELIVERY_IS_NOT_NULL = PAYMENT_AND_DELIVERY + QUERY_IS_NOT_NULL;
  String ORDER_PAYMENT_AND_DELIVERY_HAS_ONE_RECORD = PAYMENT_AND_DELIVERY + QUERY_IS_SINGLE;

  String INCOTERM = PAYMENT_AND_DELIVERY + "assert incoterm equals";
  String CURRENCY = PAYMENT_AND_DELIVERY + "assert currency equals";
  String PAYMENT_TERMS = PAYMENT_AND_DELIVERY + "assert payment terms equals";
  String CONTAINER_NO = PAYMENT_AND_DELIVERY + "assert containers no. equals";
  String CONTAINER_TYPE = PAYMENT_AND_DELIVERY + "assert containers type equals";
  String SHIPPING_INSTRUCTIONS = PAYMENT_AND_DELIVERY + "assert shipping instructions equals";
  String TRANSPORT_MODE = PAYMENT_AND_DELIVERY + "assert transport mode equals";
  String LOADING_PORT = PAYMENT_AND_DELIVERY + "assert loading port equals";
  String DISCHARGE_PORT = PAYMENT_AND_DELIVERY + "assert discharge port equals";
}
