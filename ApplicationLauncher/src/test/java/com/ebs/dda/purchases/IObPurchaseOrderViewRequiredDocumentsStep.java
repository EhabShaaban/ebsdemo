package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.utils.IObPurchaseOrderViewRequiredDocumentsTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class IObPurchaseOrderViewRequiredDocumentsStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private IObPurchaseOrderViewRequiredDocumentsTestUtils utils;

  public IObPurchaseOrderViewRequiredDocumentsStep() {

    And(
        "^the following RequiredDocuments section exist in PurchaseOrder with code \"([^\"]*)\":$",
        (String purchaseOrderCode, DataTable requiredDocumentTable) -> {
          utils.assertThatPurchaseOrderHasRequiredDocuments(
              purchaseOrderCode, requiredDocumentTable);
        });

    Given(
        "^the following DocumentTypes exist:$",
        (DataTable requiredDocumentsTable) -> {
          utils.assertThatDocumentTypesExist(requiredDocumentsTable);
        });

    When(
        "^\"([^\"]*)\" requests to view RequiredDocuments section of PurchaseOrder with code \"([^\"]*)\"$",
        (String userName, String purchaseOrderCode) -> {
          Cookie userCookie = userActionsTestUtils.getUserCookie(userName);
          Response requiredDocumentsResponse =
              utils.readPurchaseOrderRequiredDocuments(userCookie, purchaseOrderCode);
          userActionsTestUtils.setUserResponse(userName, requiredDocumentsResponse);
        });

    Then(
        "^the following values of RequiredDocuments section are displayed to \"([^\"]*)\":$",
        (String userName, DataTable purchaseOrderTable) -> {
          Response userResponse = userActionsTestUtils.getUserResponse(userName);
          utils.assertThatPurchaseOrderRequiredDocumentsInResponse(
              purchaseOrderTable, userResponse);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    utils = new IObPurchaseOrderViewRequiredDocumentsTestUtils(getProperties());
    utils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void setup(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View RequiredDocuments section")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(utils.getDbScriptsOneTimeExecution());
        databaseConnector.executeSQLScript(utils.getDbScriptPath());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "View RequiredDocuments section")) {
      databaseConnector.createConnection();
      databaseConnector.executeSQLScript(utils.getDbScriptPath());
      databaseConnector.closeConnection();
    }
  }
}
