package com.ebs.dda.purchases;

import com.ebs.dda.SpringBootRunner;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderClearanceValueObject;
import com.ebs.dda.purchases.utils.DObPurchaseOrderMarkAsClearedTestUtils;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java8.En;
import io.restassured.response.Response;

public class DObPurchaseOrderMarkAsClearedStep extends SpringBootRunner implements En {

  private static boolean hasBeenExecutedOnce = false;
  private DObPurchaseOrderMarkAsClearedTestUtils markAsClrearedTestUtils;

  public DObPurchaseOrderMarkAsClearedStep() {

    Given(
        "^the following PurchaseOrders exist with arrival date:$",
        (DataTable purchaseOrderTable) -> {
          markAsClrearedTestUtils.assertThatPurchaseOrderWithArrivalDateIsExist(purchaseOrderTable);
        });

    When(
        "^\"([^\"]*)\" requests to MarkAsCleared the PurchaseOrder with Code \"([^\"]*)\" at \"([^\"]*)\" with the following values:$",
        (String userName,
            String purchaseOrderCode,
            String currentDateTime,
            DataTable clearanceDataTable) -> {
          markAsClrearedTestUtils.freezeWithTimeZone(currentDateTime);
          Response response =
              markAsClrearedTestUtils.markAsCleared(
                  userActionsTestUtils.getUserCookie(userName),
                  purchaseOrderCode,
                  clearanceDataTable);

          userActionsTestUtils.setUserResponse(userName, response);
        });

    Then(
        "^PurchaseOrder with Code \"([^\"]*)\" is updated as Cleared as follows:$",
        (String purchaseOrderCode, DataTable updatedPurchaseOrderTable) -> {
          markAsClrearedTestUtils.assertThatPurchaseOrderIsCleared(
              purchaseOrderCode, updatedPurchaseOrderTable);
        });

    Then(
        "^the following error message is attached to Clearance date field \"([^\"]*)\" and sent to \"([^\"]*)\"$",
        (String errorCode, String userName) -> {
          Response response = userActionsTestUtils.getUserResponse(userName);
          markAsClrearedTestUtils.assertThatFieldHasErrorMessageCode(
              response, errorCode, IDObPurchaseOrderClearanceValueObject.CLEARANCE_DATE_TIME);
        });

    Then(
        "^the following sections with missing fields are sent to \"([^\"]*)\":$",
        (String userName, DataTable sectionsWithMissingFieldsDataTable) -> {
          markAsClrearedTestUtils.assertThatSectionsWithMissingFieldsExistInResponse(
              userActionsTestUtils.getUserResponse(userName), sectionsWithMissingFieldsDataTable);
        });
  }

  @Override
  public void afterPropertiesSet() {
    super.afterPropertiesSet();
    markAsClrearedTestUtils = new DObPurchaseOrderMarkAsClearedTestUtils(getProperties());
    markAsClrearedTestUtils.setEntityManagerDatabaseConnector(entityManagerDatabaseConnector);
  }

  @Before
  public void before(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Mark PO as Cleared")) {
      if (!hasBeenExecutedOnce) {
        databaseConnector.executeSQLScript(markAsClrearedTestUtils.getDbScriptsOneTimeExecution());
        hasBeenExecutedOnce = true;
      }
    }
  }

  @After
  public void after(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Mark PO as Cleared")) {
      markAsClrearedTestUtils.unfreeze();
      userActionsTestUtils.unlockAllSections(
          IPurchaseOrderRestUrls.UNLOCK_LOCKED_SECTIONS,
          markAsClrearedTestUtils.getSectionsNames());
    }
  }

  @After("@ResetData")
  public void resetData(Scenario scenario) throws Exception {
    if (contains(scenario.getName(), "Mark PO as Cleared")) {
        databaseConnector.createConnection();
        databaseConnector.executeSQLScript(markAsClrearedTestUtils.getDbScriptsAfterEachScenario());
        databaseConnector.closeConnection();
    }
  }
}
