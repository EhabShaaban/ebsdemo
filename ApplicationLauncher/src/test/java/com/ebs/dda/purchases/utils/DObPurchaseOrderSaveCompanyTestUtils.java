package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDobPurchaseOrderCompanyValueObject;
import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;

import java.util.Map;

import static org.junit.Assert.assertNotNull;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderSaveCompanyTestUtils extends DObPurchaseOrderCompanyTestUtils {

  private final String REQUEST_COMPANY_CODE = "companyCode";

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Save]";

  public DObPurchaseOrderSaveCompanyTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = getProperty(URL_PREFIX).toString();
  }

  public void assertThatCompanyDataIsUpdated(String purchaseOrderCode, DataTable companyDataTable) {
    Map<String, String> expectedCompanyMap =
        companyDataTable.asMaps(String.class, String.class).get(0);

    Object companyModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedCompanyData",
            getUpdatedCompanyQueryParams(expectedCompanyMap, purchaseOrderCode));

    assertNotNull(ASSERTION_MSG + " Company is not updated", companyModifiedDate);
  }

  private Object[] getUpdatedCompanyQueryParams(
      Map<String, String> expectedCompanyMap, String purchaseOrderCode) {
    return new Object[] {
            expectedCompanyMap.get(IFeatureFileCommonKeys.COMPANY),
            expectedCompanyMap.get(IFeatureFileCommonKeys.BANK_ACCOUNT_NUMBER),
      purchaseOrderCode,
    };
  }

  public Response savePurchaseOrderCompanySection(
      Cookie loginCookie, String purchaseOrderCode, DataTable companyDataTable) {
    JsonObject companyJsonData = getCompanyJsonObject(companyDataTable);
    String restURL = "/command/purchaseorder/" + purchaseOrderCode + "/update/company";
    return sendPUTRequest(loginCookie, restURL, companyJsonData);
  }

  private JsonObject getCompanyJsonObject(DataTable purchaseOrderCompanyDataTable) {
    JsonObject companyJsonData = new JsonObject();
    Map<String, String> companyData =
        purchaseOrderCompanyDataTable.asMaps(String.class, String.class).get(0);
      addValueToJsonObject(
              companyJsonData,
            IDobPurchaseOrderCompanyValueObject.COMPANY_CODE,
            companyData.get(IFeatureFileCommonKeys.COMPANY_CODE));
      try {
          companyJsonData.addProperty(
                  IDobPurchaseOrderCompanyValueObject.BANK_ACCOUNT_ID,
                  Long.parseLong(companyData.get(IFeatureFileCommonKeys.BANK_ACCOUNT_ID)));
      } catch (NumberFormatException ex) {
          addValueToJsonObject(
                  companyJsonData,
                  IDobPurchaseOrderCompanyValueObject.BANK_ACCOUNT_ID,
                  companyData.get(IFeatureFileCommonKeys.BANK_ACCOUNT_ID));
      }

    return companyJsonData;
  }
}
