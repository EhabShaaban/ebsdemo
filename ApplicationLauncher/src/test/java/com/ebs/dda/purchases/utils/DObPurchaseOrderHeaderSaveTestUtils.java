package com.ebs.dda.purchases.utils;

import static org.junit.Assert.assertNotNull;

import com.google.gson.JsonObject;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderHeaderSaveTestUtils extends PurchaseOrderGeneralDataCommonTestUtils {

  private String ASSERTION_MSG = super.ASSERTION_MSG + "[Save]";

  public DObPurchaseOrderHeaderSaveTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = getProperty(URL_PREFIX).toString();
  }

  public void assertThatPurchaseOrderHeaderUpdatedWithGivenValues(
      String purchaseOrderCode, DataTable purchaseOrderDataTable) {
    Map<String, String> purchaseOrderHeaderMap =
        purchaseOrderDataTable.asMaps(String.class, String.class).get(0);

    Object poHeaderModifiedDate =
        entityManagerDatabaseConnector.executeNativeNamedQuerySingleResult(
            "getUpdatedPOGeneralData", getHeaderQueryParams(purchaseOrderCode, purchaseOrderHeaderMap));

    assertNotNull(ASSERTION_MSG + " PO GeneralData is not updated", poHeaderModifiedDate);
    assertDateEquals(
        purchaseOrderHeaderMap.get(UPDATED_DATE),
        new DateTime(poHeaderModifiedDate).withZone(DateTimeZone.UTC));
  }

  private Object[] getHeaderQueryParams(
      String purchaseOrderCode, Map<String, String> purchaseOrderHeaderMap) {
    return new Object[] {
      purchaseOrderCode,
      purchaseOrderHeaderMap.get(UPDATED_BY),
      purchaseOrderHeaderMap.get(PURCHASING_RESPONSIBLE_CODE)
    };
  }

  public Response savePurchaseOrderHeaderSection(
      Cookie loginCookie, String purchaseOrderCode, DataTable headerDataTable) {
    JsonObject headerJsonData = getHeaderJsonObject(headerDataTable);
    String restURL = "/command/purchaseorder/updateheadersection/" + purchaseOrderCode;
    return sendPUTRequest(loginCookie, restURL, headerJsonData);
  }

  private JsonObject getHeaderJsonObject(DataTable purchaseOrderHeaderDataTable) {
    JsonObject headerJsonData = new JsonObject();
    Map<String, String> headerData =
        purchaseOrderHeaderDataTable.asMaps(String.class, String.class).get(0);
    headerJsonData.addProperty(
        PURCHASE_RESPONSIBLE_CODE, headerData.get(PURCHASING_RESPONSIBLE_CODE));
    return headerJsonData;
  }
}
