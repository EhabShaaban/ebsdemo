package com.ebs.dda.purchases.utils;

import com.ebs.dda.IFeatureFileCommonKeys;
import com.ebs.dda.MapAssertion;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObOrderDocumentGeneralModel;
import cucumber.api.DataTable;
import io.restassured.http.Cookie;
import io.restassured.response.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Created by "Mohamed Adel" on Nov, 2018 */
public class DObPurchaseOrderViewGeneralDataTestUtils
    extends PurchaseOrderGeneralDataCommonTestUtils {

  public DObPurchaseOrderViewGeneralDataTestUtils(Map<String, Object> properties) {
    setProperties(properties);
    this.urlPrefix = getProperty(URL_PREFIX).toString();
  }

  public String getDbScriptsOneTimeExecution() {
    StringBuilder str = new StringBuilder();
    str.append("db-scripts/PO_View_GeneralData_Clear.sql");
    return str.toString();
  }

  public Response readPurchaseOrderHeader(Cookie cookie, String puchaseOrderCode) {
    String restURL = "/services/purchase/orders/" + puchaseOrderCode + "/generaldata";
    return sendGETRequest(cookie, restURL);
  }

  public void assertThatGeneralDataIsDisplayedToTheUserInResponse(
      DataTable purchaseOrderDataTable, Response purchaseOrderDataResponse) {
    List<Map<String, String>> purchaseOrdersData =
        purchaseOrderDataTable.asMaps(String.class, String.class);

    for (Map<String, String> expectedPurchaseOrderMap : purchaseOrdersData) {

      HashMap<String, Object> actualPurchaseOrderData =
          getMapFromResponse(purchaseOrderDataResponse);

      MapAssertion mapAssertion =
          new MapAssertion(expectedPurchaseOrderMap, actualPurchaseOrderData);

      mapAssertion.assertValue(
          IFeatureFileCommonKeys.CODE, IDObOrderDocumentGeneralModel.USER_CODE);
      mapAssertion.assertDateTime(
          IFeatureFileCommonKeys.CREATION_DATE, IDObOrderDocumentGeneralModel.CREATION_DATE);
      mapAssertion.assertValue(
          IFeatureFileCommonKeys.CREATED_BY, IDObOrderDocumentGeneralModel.CREATION_INFO);
      mapAssertion.assertValueContains(
          IFeatureFileCommonKeys.STATE, IDObOrderDocumentGeneralModel.CURRENT_STATES);
      mapAssertion.assertValue(
          IFeatureFileCommonKeys.TYPE, IDObOrderDocumentGeneralModel.OBJECT_TYPE_CODE);
      mapAssertion.assertValue(
          IFeatureFileCommonKeys.DOCUMENT_OWNER, IDObOrderDocumentGeneralModel.DOCUMENT_OWNER_NAME);
    }
  }
}
