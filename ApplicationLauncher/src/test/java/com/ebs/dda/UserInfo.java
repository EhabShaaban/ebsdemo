package com.ebs.dda;

import io.restassured.http.Cookie;
import io.restassured.response.Response;

public class UserInfo {

  private Cookie loginCookie;
  private Response response;

  public UserInfo(Response response) {
    this.response = response;
  }

  public UserInfo(Cookie loginCookie) {
    this.loginCookie = loginCookie;
  }

  public Cookie getLoginCookie() {
    return loginCookie;
  }

  public void setLoginCookie(Cookie loginCookie) {
    this.loginCookie = loginCookie;
  }

  public Response getResponse() {
    return response;
  }

  public void setResponse(Response response) {
    this.response = response;
  }
}
