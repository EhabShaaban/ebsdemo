package com.ebs.dac.security.services;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.jpa.repositories.AuthorizationConditionRepository;
import com.ebs.dac.security.jpa.repositories.UserAssignmentRepository;
import com.ebs.dac.security.jpa.repositories.UserPermissionGeneralModelRepository;
import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {AuthorizationService.class})
public class AuthorizationServiceTest {

  @MockBean private UserAccountSecurityService userAccountSecurityService;
  @MockBean private UserAssignmentRepository userAssignmentRepository;

  @MockBean private UserPermissionGeneralModelRepository userPermissionGeneralModelRepository;

  @MockBean private AuthorizationConditionRepository authorizationConditionRepository;

  @MockBean private AuthorizationManager authorizationManager;
  private AuthorizationService serviceFixture;

  @Before
  public void setup() {
    IBDKUser user = createUser("Amr.Khalil");
    Mockito.doReturn(user).when(userAccountSecurityService).getLoggedInUser();
    Mockito.doReturn(new ArrayList<>())
        .when(userAssignmentRepository)
        .getUserRoles("Amr.Khalil", "CO", "ACTIVE");
    serviceFixture =
        new AuthorizationService(
            userAccountSecurityService,
            userAssignmentRepository,
            userPermissionGeneralModelRepository,
            authorizationConditionRepository,
            authorizationManager);
  }

  private IBDKUser createUser(final String username) {
    return new IBDKUser() {
      @Override
      public String getUsername() {
        return username;
      }

      @Override
      public String getCompany() {
        return "CO";
      }

      @Override
      public boolean equals(IBDKUser anotherUser) {
        return false;
      }

      @Override
      public Long getUserId() {
        return 0L;
      }

      @Override
      public Boolean getSysPasswordFlag() {
        return false;
      }
    };
  }

  @Test
  public void test_getCurrentUserAuthorizedActionsWithConditions() {
    Assert.assertTrue(true);
  }
}
