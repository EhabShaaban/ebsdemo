package com.ebs.dac.security.shiro.filters.mocks;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.InvalidRequestFilter;

public class InvalidRequestFilterMock extends InvalidRequestFilter {

	@Override
	public boolean isAccessAllowed(ServletRequest req, ServletResponse response, Object mappedValue)
					throws Exception {
		return super.isAccessAllowed(req, response, mappedValue);
	}
}
