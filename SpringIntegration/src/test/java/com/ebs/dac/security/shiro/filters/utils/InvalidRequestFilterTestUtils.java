package com.ebs.dac.security.shiro.filters.utils;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.mockito.Mockito;

import com.ebs.dac.security.shiro.filters.mocks.InvalidRequestFilterMock;

public class InvalidRequestFilterTestUtils {

	private InvalidRequestFilterMock invalidRequestFilter;
	private String url;
	private HttpServletRequest httpRequest;
	private Boolean isAccessAllowedResult;

	public InvalidRequestFilterTestUtils() {
		invalidRequestFilter = new InvalidRequestFilterMock();
		initInvalidRequestFilter();
	}

	public InvalidRequestFilterTestUtils withUrl(String url) {
		this.url = url;
		return this;
	}

	public InvalidRequestFilterTestUtils whenCheckThatAccessAllowed() throws Exception {
		mockHttpServletRequest();
		isAccessAllowedResult = invalidRequestFilter.isAccessAllowed(httpRequest, null, null);
		return this;
	}

	private void initInvalidRequestFilter() {
		httpRequest = Mockito.mock(HttpServletRequest.class);
	}

	private void mockHttpServletRequest() {
		Mockito.when(httpRequest.getRequestURI()).thenReturn(url);

		Mockito.when(httpRequest.getServletPath()).thenReturn(url);
	}

	public void thenAssertThatReturnsFalse() {
		Assert.assertFalse(isAccessAllowedResult);
	}
}
