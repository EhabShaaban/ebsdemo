package com.ebs.dac.security.shiro.filters;

import org.junit.Before;
import org.junit.Test;

import com.ebs.dac.security.shiro.filters.utils.InvalidRequestFilterTestUtils;

public class InvalidRequestFilterTests {

	private InvalidRequestFilterTestUtils utils;

	@Before
	public void init() {
		utils = new InvalidRequestFilterTestUtils();
	}

	@Test
	public void Should_ReturnsInvalid_When_UrlContainsSemicolon() throws Exception {
		utils.withUrl("/resources/..;/services/masterdataobjects/customers/").whenCheckThatAccessAllowed()
						.thenAssertThatReturnsFalse();

	}
	@Test
	public void Should_ReturnsInvalid_When_UrlContainsBackslash() throws Exception {
		utils.withUrl("\\/services/masterdataobjects/customers/").whenCheckThatAccessAllowed()
						.thenAssertThatReturnsFalse();

	}
	@Test
	public void Should_ReturnsInvalid_When_UrlContainsNonAsciiCharacters() throws Exception {
		utils.withUrl("/services/order/salesorder/page=0/rows=50/filters=customer-contains-هندسية").whenCheckThatAccessAllowed()
						.thenAssertThatReturnsFalse();

	}
}
