package com.ebs.dac.restexceptionhandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ebs.dac.foundation.exceptions.data.MissingFieldsException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class MissingFieldsExceptionResponseHandler implements IExceptionResponseHandler {

  private static final String MISSING_FIELDS_KEY = "missingFields";

  MissingFieldsExceptionResponseHandler() {}

  @Override
  public void handleException(Exception ex, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    MissingFieldsException exceptionResponse = (MissingFieldsException) ex;
    response.setStatus(exceptionResponse.getHttpStatusCode());
    response.getWriter().write(getErrorResponse( exceptionResponse));
  }

  protected String getErrorResponse(MissingFieldsException ex) {
    JsonParser parser = new JsonParser();
    JsonObject response = new JsonObject();
    response.add(MISSING_FIELDS_KEY, parser.parse(new Gson().toJson(ex.getMissingFields())));
    response.addProperty(STATUS, FAIL_RESPONSE_KEY);
    response.addProperty(MSG_CODE, IExceptionsCodes.General_UC_Failure_MISSING_MANDATORIES);
    return response.toString();
  }
}
