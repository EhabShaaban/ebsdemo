package com.ebs.dac.restexceptionhandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;
import com.ebs.dac.security.shiro.models.EBSSubject;

public class SecurityExceptionResponseHandler implements IExceptionResponseHandler {

  private Logger logger =
      LoggerFactory.getLogger(SecurityExceptionResponseHandler.class.getSimpleName());

  private IConcurrentDataAccessManagersObjectPool accessManagersPool;

  SecurityExceptionResponseHandler(IConcurrentDataAccessManagersObjectPool accessManagersPool) {
    this.accessManagersPool = accessManagersPool;
  }

  @Override
  public void handleException(Exception ex, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    EBSSubject subject = (EBSSubject) SecurityUtils.getSubject();
    logger.warn("Security exception has been thrown and user is " + subject.getPrincipal());
    accessManagersPool.releaseAllLocksByCurrentUser();
    subject.logout();
    response.setStatus(IExceptionResponse.SECURITY_VIOLATION_HTTP_STATUS_CODE);
    response.setHeader("Location", "securityerror");
  }

}
