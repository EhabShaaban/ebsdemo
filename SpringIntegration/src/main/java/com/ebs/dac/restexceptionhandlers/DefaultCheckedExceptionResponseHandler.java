package com.ebs.dac.restexceptionhandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;
import com.google.gson.JsonObject;

public class DefaultCheckedExceptionResponseHandler implements IExceptionResponseHandler {

  DefaultCheckedExceptionResponseHandler() {}

  @Override
  public void handleException(Exception ex, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    IExceptionResponse exceptionResponse = (IExceptionResponse) ex;
    response.setStatus(exceptionResponse.getHttpStatusCode());
    response.getWriter().write(getErrorResponse(exceptionResponse));
  }

  protected String getErrorResponse(IExceptionResponse ex) {
    JsonObject response = new JsonObject();
    response.addProperty(STATUS, ERROR);
    response.addProperty(MSG_CODE, ex.getResponseCode());
    return response.toString();
  }
}
