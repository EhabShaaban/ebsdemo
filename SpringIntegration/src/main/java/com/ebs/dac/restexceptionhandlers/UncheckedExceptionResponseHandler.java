package com.ebs.dac.restexceptionhandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.google.gson.JsonObject;

public class UncheckedExceptionResponseHandler implements IExceptionResponseHandler {

  private final String STATUS = "status";
  private final String ERROR = "ERROR";
  private final String MSG_CODE = "code";

  UncheckedExceptionResponseHandler() {}

  @Override
  public void handleException(Exception ex, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    response.setStatus(500);
    response.getWriter().write(getErrorResponse());
  }

  protected String getErrorResponse() {
    JsonObject response = new JsonObject();
    response.addProperty(STATUS, ERROR);
    response.addProperty(MSG_CODE, IExceptionsCodes.INTERNAL_SERVER_ERROR);
    return response.toString();
  }

}
