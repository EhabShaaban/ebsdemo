package com.ebs.dac.restexceptionhandlers;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.data.MissingFieldsException;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ExceptionResponseHandlerFactory {

  private IConcurrentDataAccessManagersObjectPool accessManagersPool;

  public ExceptionResponseHandlerFactory(
      IConcurrentDataAccessManagersObjectPool accessManagersPool) {
    this.accessManagersPool = accessManagersPool;
  }

  public IExceptionResponseHandler getHandler(Exception exception) {

    if (exception instanceof IExceptionResponse) {
      IExceptionResponse exceptionResponse = (IExceptionResponse) exception;

      if (exceptionResponse
          .getHttpStatusCode() == IExceptionResponse.SECURITY_VIOLATION_HTTP_STATUS_CODE) {
        return new SecurityExceptionResponseHandler(accessManagersPool);
      }

      if (exception instanceof BusinessRuleViolationException) {
        return new BusinessRulesViolationExceptionResponseHandler();
      }

      if (exception instanceof MissingFieldsException) {
        return new MissingFieldsExceptionResponseHandler();
      }

      return new DefaultCheckedExceptionResponseHandler();
    }

    return new UncheckedExceptionResponseHandler();
  }
}
