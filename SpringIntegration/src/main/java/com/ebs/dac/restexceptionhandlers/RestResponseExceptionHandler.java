package com.ebs.dac.restexceptionhandlers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

  private static final Logger logger = LoggerFactory.getLogger(RestResponseExceptionHandler.class);
  @Autowired private ExceptionResponseHandlerFactory exceptionResponseHandlerFactory;

  @ExceptionHandler(Exception.class)
  public void handleException(
      Exception exception, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    logger.error("Exception: ", exception);
    IExceptionResponseHandler exceptionResponseHandler =
        exceptionResponseHandlerFactory.getHandler(exception);

    exceptionResponseHandler.handleException(exception, request, response);
  }
}
