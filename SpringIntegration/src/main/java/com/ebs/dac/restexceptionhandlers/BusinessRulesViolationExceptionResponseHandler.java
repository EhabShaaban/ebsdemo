package com.ebs.dac.restexceptionhandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class BusinessRulesViolationExceptionResponseHandler implements IExceptionResponseHandler {

  BusinessRulesViolationExceptionResponseHandler() {}

  @Override
  public void handleException(
      Exception ex, HttpServletRequest request, HttpServletResponse response) throws Exception {
    BusinessRuleViolationException businessRuleEx = (BusinessRuleViolationException) ex;
    response.setStatus(businessRuleEx.getHttpStatusCode());
    response.setCharacterEncoding("UTF-8");
    response.getWriter().write(getErrorResponse(businessRuleEx));
  }

  protected String getErrorResponse(BusinessRuleViolationException exception) {

    JsonParser parser = new JsonParser();
    JsonObject response = new JsonObject();

    ValidationResult validationResult = exception.getValidationResult();
    if (validationResult != null) {
      response = parseValidationResult(validationResult, parser);
    }

    response.addProperty(STATUS, FAIL_RESPONSE_KEY);
    response.addProperty(MSG_CODE, exception.getResponseCode());
    return response.toString();
  }

  private JsonObject parseValidationResult(ValidationResult validationResult, JsonParser parser) {
    return (JsonObject) parser.parse(new Gson().toJson(validationResult));
  }
}
