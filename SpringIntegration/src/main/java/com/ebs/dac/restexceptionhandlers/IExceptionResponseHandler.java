package com.ebs.dac.restexceptionhandlers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IExceptionResponseHandler {

  String STATUS = "status";
  String ERROR = "ERROR";
  String FAIL_RESPONSE_KEY = "FAIL";
  String MSG_CODE = "code";

  void handleException(Exception ex, HttpServletRequest request,
      HttpServletResponse response) throws Exception;
}
