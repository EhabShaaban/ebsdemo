package com.ebs.dac.spring.configs;

import javax.persistence.EntityManager;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactoryBean;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;

// Created By xerix - 4/18/18 - 5:19 PM
public class RepositoryFactoryBean<R extends Repository<T, Long>, T>
    extends JpaRepositoryFactoryBean<R, T, Long> implements ApplicationContextAware {
  private ApplicationContext applicationContext;

  public RepositoryFactoryBean(Class repositoryInterface) {
    super(repositoryInterface);
  }

  protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
    JpaRepositoryFactory jpaRepositoryFactory = new JpaRepositoryFactory(entityManager);
    jpaRepositoryFactory.setBeanFactory(this.applicationContext.getAutowireCapableBeanFactory());
    return jpaRepositoryFactory;
  }

  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }
}
