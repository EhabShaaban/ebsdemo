package com.ebs.dac.spring.configs;
// Created By xerix - 5/2/18 - 10:25 AM

import java.util.HashMap;
import java.util.Map;
import org.apache.catalina.filters.CorsFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class CorsFilterBean {

  @Bean("CorsFilter")
  public FilterRegistrationBean CorsFilter(Environment env) {
    CorsFilter CorsFilter = new CorsFilter();
    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(CorsFilter);
    registration.addUrlPatterns("/*");
    registration.setName("CorsFilter");
    Map<String, String> initParams = new HashMap<>();
    initParams.put("cors.allowed.origins", env.getRequiredProperty("cors.allowed.origins"));
    initParams.put("cors.allowed.methods", env.getRequiredProperty("cors.allowed.methods"));
    initParams.put("cors.allowed.headers", env.getRequiredProperty("cors.allowed.headers"));
    initParams.put("cors.exposed.headers", env.getRequiredProperty("cors.exposed.headers"));
    initParams.put("cors.support.credentials", env.getRequiredProperty("cors.support.credentials"));
    initParams.put("cors.preflight.maxage", env.getRequiredProperty("cors.preflight.maxage"));
    registration.setInitParameters(initParams);
    registration.setOrder(1);
    return registration;
  }
}
