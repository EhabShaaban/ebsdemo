package com.ebs.dac.spring.configs;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.security.jpa.repositories.AuthorizationConditionRepository;
import com.ebs.dac.security.jpa.repositories.UserAssignmentRepository;
import com.ebs.dac.security.jpa.repositories.UserPermissionGeneralModelRepository;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dac.security.web.filters.CacheControlFilter;
import com.ebs.dac.security.web.filters.ThreadLocalHandlerFilter;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/** Created by marisoft on 10/30/16. */
@Configuration
public class WebConfig implements WebMvcConfigurer {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
  }

  @Bean("userAccountSecurityService")
  public IUserAccountSecurityService createUserAccountSecurityService() {
    return new UserAccountSecurityService();
  }

  @Bean("authorizationService")
  @DependsOn("userAccountSecurityService")
  public AuthorizationService createAuthorizationService(
      IUserAccountSecurityService userAccountSecurityService,
      UserAssignmentRepository userAssignmentRepository,
      UserPermissionGeneralModelRepository userPermissionGeneralModelRepository,
      AuthorizationConditionRepository authorizationConditionRepository,
      AuthorizationManager authorizationManager) {
    return new AuthorizationService(
        (UserAccountSecurityService) userAccountSecurityService,
        userAssignmentRepository,
        userPermissionGeneralModelRepository,
        authorizationConditionRepository,
        authorizationManager);
  }

  @Bean("cacheControlFilter")
  public FilterRegistrationBean cacheControlFilter() {
    CacheControlFilter cacheControlFilter = new CacheControlFilter();
    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(cacheControlFilter);
    registration.addUrlPatterns("/*");
    registration.setName("cacheControlFilter");
    registration.setOrder(3);
    return registration;
  }

  @Bean("threadLocalHandlerFilter")
  public FilterRegistrationBean threadLocalHandlerFilter() {
    ThreadLocalHandlerFilter threadLocalHandlerFilter = new ThreadLocalHandlerFilter();
    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(threadLocalHandlerFilter);
    registration.addUrlPatterns("/*");
    registration.setName("threadLocalHandlerFilter");
    registration.setOrder(4);
    return registration;
  }

  @Bean("shiroFilter")
  @DependsOn({"shiroFilterBean"})
  public FilterRegistrationBean shiroFilter(ApplicationContext applicationContext) {
    AbstractShiroFilter shiroFilter =
        applicationContext.getBean("shiroFilterBean", AbstractShiroFilter.class);
    FilterRegistrationBean registration = new FilterRegistrationBean();
    registration.setFilter(shiroFilter);
    registration.addUrlPatterns("/*");
    registration.setName("shiroFilter");
    registration.setOrder(7);
    return registration;
  }
}
