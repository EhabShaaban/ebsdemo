package com.ebs.dac.spring.configs;

import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;

/** Created by marisoft on 11/15/16. */
@Configuration
public class ShiroHazelCastCacheConfig {

  @Bean("sessionDAO")
  @DependsOn({"entityManagerFactory"})
  public SessionDAO sessionDAO() {
    EnterpriseCacheSessionDAO sessionDAO = new EnterpriseCacheSessionDAO();
    sessionDAO.setActiveSessionsCacheName("shiro-activeSessionCache");
    return sessionDAO;
  }

  @Bean("cacheManager")
  @ConditionalOnClass(name = {"com.hazelcast.core.HazelcastInstance"})
  public CacheManager cacheManager(Environment env) {
    MemoryConstrainedCacheManager memoryCacheManager = new MemoryConstrainedCacheManager();
    return memoryCacheManager;
  }
}
