package com.ebs.dac.spring.configs;

import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import org.eclipse.persistence.config.PersistenceUnitProperties;
import org.eclipse.persistence.jpa.PersistenceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
import org.springframework.boot.autoconfigure.domain.EntityScanPackages;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.SharedCacheMode;
import javax.persistence.ValidationMode;
import javax.sql.DataSource;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** Created by marisoft on 10/23/16. */
@Configuration
public class JPAConfiguration implements BeanFactoryAware {

  private static final Logger logger = LoggerFactory.getLogger(JPAConfiguration.class);
  private ConfigurableListableBeanFactory beanFactory;
    @Autowired
    private Environment env;

  @Bean("dataSource")
  public DriverManagerDataSource dataSource() throws Exception {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    String database = env.getRequiredProperty("database");
    dataSource.setDriverClassName(env.getRequiredProperty(database + ".driverClassName"));
    dataSource.setUrl(env.getRequiredProperty(database + ".url"));
    dataSource.setUsername(env.getRequiredProperty(database + ".username"));
    dataSource.setPassword(env.getRequiredProperty(database + ".password"));
    dataSource.getConnection().setAutoCommit(false);
    return dataSource;
  }

  @Bean("entityManagerFactory")
  @DependsOn({"dataSource"})
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
    LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean =
        new LocalContainerEntityManagerFactoryBean();
    localContainerEntityManagerFactoryBean.setDataSource(dataSource);
    localContainerEntityManagerFactoryBean.setPersistenceUnitName("ApplicationPU");
    localContainerEntityManagerFactoryBean.setPersistenceProvider(new PersistenceProvider());
    localContainerEntityManagerFactoryBean.setPackagesToScan(getPackagesToScan());
    localContainerEntityManagerFactoryBean.setJpaPropertyMap(initJpaProperties());
    localContainerEntityManagerFactoryBean.setSharedCacheMode(SharedCacheMode.NONE);
    localContainerEntityManagerFactoryBean.setValidationMode(ValidationMode.NONE);
      String environmentType = env.getProperty("environmentType");
      if (environmentType.equals("test")) {
          localContainerEntityManagerFactoryBean.setMappingResources(getMappingResources());
      }
    return localContainerEntityManagerFactoryBean;
  }

  private String[] getMappingResources() {
    File file = new File("src/test/resources/META-INF");
    List<String> ormFiles = new ArrayList<>();
    filterORMFiles(ormFiles, file.list());
    return ormFiles.toArray(new String[ormFiles.size()]);
  }

  private void filterORMFiles(List<String> ormFiles, String[] list) {
    for (String aList : list) {
      checkIfFileIsORMAndAddToList(ormFiles, aList);
    }
  }

  private void checkIfFileIsORMAndAddToList(List<String> ormFiles, String aList) {
    if (aList.matches("(.*)-orm.xml")) {
      ormFiles.add("META-INF/" + aList);
    }
  }

  private Map<String, ?> initJpaProperties() {
    final Map<String, Object> ret = new HashMap<>();
    ret.put(PersistenceUnitProperties.WEAVING, "static");
    ret.put(PersistenceUnitProperties.LOGGING_PARAMETERS, "true");
    return ret;
  }

  @Bean("transactionManager")
  @DependsOn({"entityManagerFactory"})
  public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) {
    return new JpaTransactionManager(entityManagerFactory);
  }

  @Bean("transactionManagerDelegate")
  @DependsOn({"transactionManager"})
  public TransactionManagerDelegate transactionManagerDelegate(
      PlatformTransactionManager platformTransactionManager) {
    TransactionManagerDelegate transactionManagerDelegate = new TransactionManagerDelegate();
    transactionManagerDelegate.setTransactionManager(platformTransactionManager);
    return transactionManagerDelegate;
  }

  @Override
  public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
    this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
  }

  protected String[] getPackagesToScan() {
    List packages = EntityScanPackages.get(this.beanFactory).getPackageNames();
    if (packages.isEmpty() && AutoConfigurationPackages.has(this.beanFactory)) {
      packages = AutoConfigurationPackages.get(this.beanFactory);
    }
    return (String[]) packages.toArray(new String[packages.size()]);
  }
}
