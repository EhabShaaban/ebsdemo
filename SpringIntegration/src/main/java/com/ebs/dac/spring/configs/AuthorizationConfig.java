package com.ebs.dac.spring.configs;

import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dac.security.services.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class AuthorizationConfig {

  @Bean("AuthorizationManager")
  public AuthorizationManager createAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    AuthorizationManager authorizationManager =
        new AuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("invoiceAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObInvoiceAuthorizationManager createDObInvoiceAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository,
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep,
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep,
      MObVendorGeneralModelRep VendorGeneralModelRep) {
    DObInvoiceAuthorizationManager invoiceAuthorizationManager =
        new DObInvoiceAuthorizationManager(authorizationGeneralModelRepository);
    invoiceAuthorizationManager.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    invoiceAuthorizationManager.setcObPurchasingUnitGeneralModelRep(
        cObPurchasingUnitGeneralModelRep);
    invoiceAuthorizationManager.setVendorGeneralModelRep(VendorGeneralModelRep);
    return invoiceAuthorizationManager;
  }

  @Bean("DObPaymentRequestAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObPaymentRequestAuthorizationManager createDObPaymentRequestAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    DObPaymentRequestAuthorizationManager authorizationManager =
        new DObPaymentRequestAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("DObLandedCostAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObLandedCostAuthorizationManager createDObLandedCOstAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    DObLandedCostAuthorizationManager authorizationManager =
        new DObLandedCostAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("CObExchangeRateAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public CObExchangeRateAuthorizationManager createCObExchangeRateAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    CObExchangeRateAuthorizationManager authorizationManager =
        new CObExchangeRateAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("DObGoodsIssueAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObGoodsIssueAuthorizationManager createDObGoodsIssueAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository,
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep,
      DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    DObGoodsIssueAuthorizationManager authorizationManager =
        new DObGoodsIssueAuthorizationManager(authorizationGeneralModelRepository);
    authorizationManager.setcObPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
    authorizationManager.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    return authorizationManager;
  }

  @Bean("DObCollectionAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObCollectionAuthorizationManager createDObCollectionAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    DObCollectionAuthorizationManager authorizationManager =
        new DObCollectionAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("DObSalesInvoiceAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObSalesInvoiceAuthorizationManager createDObSalesInvoiceAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    DObSalesInvoiceAuthorizationManager authorizationManager =
        new DObSalesInvoiceAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("DObSalesReturnOrderAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObSalesReturnOrderAuthorizationManager createDObSalesReturnOrderAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    DObSalesReturnOrderAuthorizationManager authorizationManager =
        new DObSalesReturnOrderAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("DObInitialStockUploadAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObInitialStockUploadAuthorizationManager createDObInitialStockUploadAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository,
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep) {
    DObInitialStockUploadAuthorizationManager authorizationManager =
        new DObInitialStockUploadAuthorizationManager(authorizationGeneralModelRepository);
    authorizationManager.setcObPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
    return authorizationManager;
  }

  @Bean("DObInitialBalanceUploadAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObInitialBalanceUploadAuthorizationManager createDObInitialBalanceUploadAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository,
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep) {
    DObInitialBalanceUploadAuthorizationManager authorizationManager =
        new DObInitialBalanceUploadAuthorizationManager(authorizationGeneralModelRepository);
    authorizationManager.setcObPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
    return authorizationManager;
  }

  @Bean("DObSettlementAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObSettlementAuthorizationManager createDObSettlementAuthorizationManager(
          AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    DObSettlementAuthorizationManager authorizationManager =
            new DObSettlementAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

  @Bean("DObCostingAuthorizationManager")
  @DependsOn("AuthorizationManager")
  public DObCostingnAuthorizationManager createDObCostingAuthorizationManager(
          AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    DObCostingnAuthorizationManager authorizationManager =
            new DObCostingnAuthorizationManager(authorizationGeneralModelRepository);
    return authorizationManager;
  }

}
