package com.ebs.dac.spring.configs;

import com.ebs.dac.security.jpa.repositories.UserAccountRepository;
import com.ebs.dac.security.jpa.repositories.UserAssignmentRepository;
import com.ebs.dac.security.shiro.GlobalUsersHandler;
import com.ebs.dac.security.shiro.filters.CustomFormAuthenticationFilter;
import com.ebs.dac.security.shiro.filters.twofactorauth.SecurityConfigurationHandler;
import com.ebs.dac.security.shiro.listeners.SessionsListener;
import com.ebs.dac.security.shiro.listeners.UserEntityAuthenticationListener;
import com.ebs.dac.security.shiro.matchers.BDKPasswordCredentialsMatcher;
import com.ebs.dac.security.shiro.models.EBSModularRealmAuthorizer;
import com.ebs.dac.security.shiro.models.EBSSecurityManager;
import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.ebs.dac.security.shiro.realms.AllSupportAuthenticationStrategy;
import com.ebs.dac.security.shiro.realms.BDKSecurityRealm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.Filter;
import org.apache.shiro.authc.AuthenticationListener;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.mgt.ExecutorServiceSessionValidationScheduler;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.session.mgt.eis.SessionDAO;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.servlet.Cookie;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;

/** Created by marisoft on 10/27/16. */
@Configuration
public class ShiroConfig {

  @Bean("globalUsersHandler")
  public GlobalUsersHandler globalUsersHandler() {
    return new GlobalUsersHandler();
  }

  @Bean("sessionIdCookie")
  public Cookie sessionIdCookie(Environment env) {
    Cookie cookie = new SimpleCookie();
    cookie.setHttpOnly(Boolean.parseBoolean(env.getRequiredProperty("shiro.uid.cookie.httpOnly")));
    cookie.setSecure(Boolean.parseBoolean(env.getRequiredProperty("shiro.uid.cookie.secure")));
    cookie.setName(env.getRequiredProperty("shiro.uid.cookie.name"));
    cookie.setPath(env.getRequiredProperty("shiro.uid.cookie.path"));
    String domain = env.getRequiredProperty("shiro.uid.cookie.domain").replaceAll("http(s?)://","");
    int portIndex = domain.indexOf(':');
    int end = portIndex > 0 ? portIndex : domain.length();
    cookie.setDomain(domain.substring(0,end));
    return cookie;
  }

  @Bean("validationScheduler")
  public ExecutorServiceSessionValidationScheduler validationScheduler() {
    ExecutorServiceSessionValidationScheduler validationScheduler =
        new ExecutorServiceSessionValidationScheduler();
    return validationScheduler;
  }

  @Bean("sessionManager")
  @DependsOn({"sessionIdCookie", "sessionDAO", "validationScheduler", "globalUsersHandler"})
  public SessionManager sessionManager(ApplicationContext applicationContext) {
    DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
    List<SessionListener> listeners = new ArrayList<>();
    SessionsListener listener = new SessionsListener();
    listener.setGlobalUserHandler(
        applicationContext.getBean("globalUsersHandler", GlobalUsersHandler.class));
    listeners.add(listener);
    sessionManager.setSessionListeners(listeners);
    sessionManager.setSessionDAO(applicationContext.getBean("sessionDAO", SessionDAO.class));
    // [test] To test session expiration and concurrent access
    sessionManager.setGlobalSessionTimeout(-1);
    // sessionManager.setDeleteInvalidSessions(true);
    sessionManager.setSessionIdCookie(applicationContext.getBean("sessionIdCookie", Cookie.class));
    ExecutorServiceSessionValidationScheduler validationScheduler =
        applicationContext.getBean(
            "validationScheduler", ExecutorServiceSessionValidationScheduler.class);
    validationScheduler.setSessionManager(sessionManager);
    sessionManager.setSessionValidationScheduler(validationScheduler);
    return sessionManager;
  }

  @Bean("securityConfigurationHandler")
  public SecurityConfigurationHandler securityConfigurationHandler() {
    SecurityConfiguration securityConfiguration =
        new SecurityConfiguration(SecurityConfiguration.SecurityConfigurationType.LOCAL);
    securityConfiguration.setSecurityClearnceLevel(
        SecurityConfiguration.SecurityClearnceLevel.STANDARD);
    securityConfiguration.setSecondFactorCredentialValidity(0);
    SecurityConfigurationHandler securityConfigurationHandler =
        new SecurityConfigurationHandler(securityConfiguration);
    return securityConfigurationHandler;
  }

  @Bean("allRealmsSuccessStrategy")
  public AllSupportAuthenticationStrategy allRealmsSuccessStrategy() {
    return new AllSupportAuthenticationStrategy();
  }

  @Bean("lifecycleBeanPostProcessor")
  public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
    return new LifecycleBeanPostProcessor();
  }

  @Bean("securityManager")
  @DependsOn({
    "entityManagerFactory",
    "sessionManager",
    "cacheManager",
    "userAccountRepository",
    "userAssignmentRepository"
  })
  public SecurityManager securityManager(ApplicationContext applicationContext, Environment env) {
    EBSSecurityManager securityManager = new EBSSecurityManager();

    ModularRealmAuthenticator authenticator =
        (ModularRealmAuthenticator) securityManager.getAuthenticator();
    authenticator.setAuthenticationListeners(getAuthenticationListeners(applicationContext));

    List<Realm> realms = new ArrayList<>();
    realms.add(buildSecurityRealms(applicationContext, env));
    authenticator.setRealms(realms);

    EBSModularRealmAuthorizer authorizer = new EBSModularRealmAuthorizer();
    authorizer.setRealms(realms);
    securityManager.setAuthorizer(authorizer);

    securityManager.setSessionManager(
        applicationContext.getBean("sessionManager", SessionManager.class));
    securityManager.setCacheManager(applicationContext.getBean("cacheManager", CacheManager.class));
    return securityManager;
  }

  private BDKSecurityRealm buildSecurityRealms(
      ApplicationContext applicationContext, Environment env) {

    UserAccountRepository userAccountRepository =
        applicationContext.getBean(UserAccountRepository.class);
    UserAssignmentRepository userAssignmentRepository =
        applicationContext.getBean(UserAssignmentRepository.class);

    BDKSecurityRealm bdkSecurityRealm =
        new BDKSecurityRealm(userAccountRepository, userAssignmentRepository);

    bdkSecurityRealm.setCompanyCode(env.getRequiredProperty("shiro.token.companyCode"));
    bdkSecurityRealm.setCredentialsMatcher(new BDKPasswordCredentialsMatcher());
    bdkSecurityRealm.setOrigin(env.getRequiredProperty("shiro.security.realm.origin"));
    bdkSecurityRealm.setSecurityConfigurationHandler(
        applicationContext.getBean(
            "securityConfigurationHandler", SecurityConfigurationHandler.class));

    return bdkSecurityRealm;
  }

  private List<AuthenticationListener> getAuthenticationListeners(
      ApplicationContext applicationContext) {
    List<AuthenticationListener> listeners = new ArrayList<>();
    UserEntityAuthenticationListener userEntityAuthenticationListener =
        new UserEntityAuthenticationListener();
    userEntityAuthenticationListener.setGlobalUserHandler(
        applicationContext.getBean("globalUsersHandler", GlobalUsersHandler.class));
    listeners.add(userEntityAuthenticationListener);
    return listeners;
  }

  @Bean("filterChainDefinitionInterceptor")
  // @ConfigurationProperties(prefix = "shiro.filter-chain-definition")
  public FilterChainDefinitionInterceptor filterChainDefinitionInterceptor() {
    FilterChainDefinitionInterceptor filterChainDefinitionInterceptor =
        new FilterChainDefinitionInterceptor();
    filterChainDefinitionInterceptor.getMap().put("/contactAdmin.jsp", "anon");
    filterChainDefinitionInterceptor.getMap().put("/alreadyLoggedin.jsp", "anon");
    filterChainDefinitionInterceptor.getMap().put("/loginAttemptsExceeded.jsp", "anon");
    filterChainDefinitionInterceptor.getMap().put("/login", "rest");
    filterChainDefinitionInterceptor.getMap().put("/logout", "authc,rest");
    filterChainDefinitionInterceptor.getMap().put("/services/**", "authc,rest");
    filterChainDefinitionInterceptor.getMap().put("/command/**", "authc,rest");
    return filterChainDefinitionInterceptor;
  }

  @Bean("shiroFilterBean")
  @DependsOn({"entityManagerFactory","securityManager", "filterChainDefinitionInterceptor"})
  public ShiroFilterFactoryBean shiroFilterBean(
      ApplicationContext applicationContext, Environment env) {
    CustomFormAuthenticationFilter authenticationFilter = new CustomFormAuthenticationFilter();
    ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
    Map<String, String> filterChainDefinitionMap =
        applicationContext.getBean(FilterChainDefinitionInterceptor.class).getMap();
    shiroFilterFactoryBean.setSecurityManager(
        applicationContext.getBean("securityManager", SecurityManager.class));
    shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
    shiroFilterFactoryBean.setLoginUrl(env.getRequiredProperty("shiro.loginUrl"));
    shiroFilterFactoryBean.setSuccessUrl(env.getRequiredProperty("shiro.successUrl"));
    HashMap<String, Filter> filters = new HashMap<>();
    filters.put("authc", authenticationFilter);
    shiroFilterFactoryBean.setFilters(filters);
    return shiroFilterFactoryBean;
  }

  public static class FilterChainDefinitionInterceptor {
    private Map<String, String> filterChainDefinitionMap = new HashMap<String, String>();

    public Map<String, String> getMap() {
      return this.filterChainDefinitionMap;
    }
  }
}
