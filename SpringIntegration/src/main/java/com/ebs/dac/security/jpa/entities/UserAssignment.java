/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** @author hesham */
@Entity
@Table(name = "UserAssignment")
@NamedQueries({
  @NamedQuery(name = "UserAssignment.findAll", query = "SELECT u FROM UserAssignment u"),
  @NamedQuery(
      name = "UserAssignment.findById",
      query = "SELECT u FROM UserAssignment u WHERE u.id = :id"),
  @NamedQuery(
      name = "UserAssignment.findByAssignmentTime",
      query = "SELECT u FROM UserAssignment u WHERE u.assignmentTime = :assignmentTime"),
  @NamedQuery(
      name = "UserAssignment.findByAssignmentState",
      query = "SELECT u FROM UserAssignment u WHERE u.assignmentState = :assignmentState")
})
public class UserAssignment implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = false)
  @Column(name = "AssignmentTime")
  @Temporal(TemporalType.TIMESTAMP)
  private Date assignmentTime;

  @Basic(optional = false)
  @Column(name = "AssignmentState")
  private String assignmentState;

  @JoinColumn(name = "UserId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private UserAccount userId;

  @JoinColumn(name = "RoleId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private Role roleId;

  public UserAssignment() {}

  public UserAssignment(Long id) {
    this.id = id;
  }

  public UserAssignment(Long id, Date assignmentTime, String assignmentState) {
    this.id = id;
    this.assignmentTime = assignmentTime;
    this.assignmentState = assignmentState;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getAssignmentTime() {
    return assignmentTime;
  }

  public void setAssignmentTime(Date assignmentTime) {
    this.assignmentTime = assignmentTime;
  }

  public String getAssignmentState() {
    return assignmentState;
  }

  public void setAssignmentState(String assignmentState) {
    this.assignmentState = assignmentState;
  }

  public UserAccount getUserId() {
    return userId;
  }

  public void setUserId(UserAccount userId) {
    this.userId = userId;
  }

  public Role getRoleId() {
    return roleId;
  }

  public void setRoleId(Role roleId) {
    this.roleId = roleId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof UserAssignment)) {
      return false;
    }
    UserAssignment other = (UserAssignment) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "UserAssignment [id="
        + id
        + ", assignmentTime="
        + assignmentTime
        + ", assignmentState="
        + assignmentState
        + ", userId="
        + userId
        + ", roleId="
        + roleId
        + "]";
  }
}
