package com.ebs.dac.security.shiro.matchers;

import com.ebs.dac.security.usermanage.utils.BDKUserUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BDKPasswordCredentialsMatcher implements CredentialsMatcher {

  private static final Logger logger = LoggerFactory.getLogger(BDKPasswordCredentialsMatcher.class);

  public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
    logger.debug("Trying to verify the supplied credentials.");
    boolean isMatched =
        BDKUserUtils.checkPassword(
            new String((char[]) token.getCredentials()), (String) info.getCredentials());
    logger.debug(
        "CredentialsMatcher tried to match the password and they do"
            + (isMatched ? " match." : "n't match."));
    return isMatched;
  }
}
