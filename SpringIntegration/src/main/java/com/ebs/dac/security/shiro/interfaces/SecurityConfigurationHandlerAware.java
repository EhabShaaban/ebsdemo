package com.ebs.dac.security.shiro.interfaces;

import com.ebs.dac.security.shiro.filters.twofactorauth.SecurityConfigurationHandler;

public interface SecurityConfigurationHandlerAware {
  void setSecurityConfigurationHandler(SecurityConfigurationHandler securityConfigurationHandler);
}
