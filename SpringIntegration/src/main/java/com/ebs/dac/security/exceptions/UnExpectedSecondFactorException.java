package com.ebs.dac.security.exceptions;

/** The passed 2nd-Factor name has NO filter to handle. */
public class UnExpectedSecondFactorException extends SecondFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private String factorName;

  {
    ERROR_CODE = "ACESS-WRNG_2NDF";
  }

  public UnExpectedSecondFactorException() {}

  public UnExpectedSecondFactorException(String factorName) {
    this.factorName = factorName;
  }

  @Override
  public String toString() {
    return "The 2nd-Factor [" + factorName + "] has no filter to handle it.";
  }
}
