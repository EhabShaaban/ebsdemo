package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** When a logged in user tries to login again. */
public class MultiLoginForSameAccountException extends FirstFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_MESSAGE = "Login access type is forbidden";

  {
    ERROR_CODE = "LOGIN-MULT_ACCS";
    STATUS_CODE = HttpStatusCode.LOCKED.code();
  }

  public MultiLoginForSameAccountException() {
    super(DEFAULT_MESSAGE);
  }
}
