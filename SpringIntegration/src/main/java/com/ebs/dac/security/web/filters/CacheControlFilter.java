package com.ebs.dac.security.web.filters;

import java.io.IOException;
import java.util.Date;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This disables any caching to be made browser-side.<br>
 * <br>
 * Check http://stackoverflow.com/a/4997498
 *
 * @author Hesham Saleh
 */
public class CacheControlFilter implements Filter {

  private static final Logger logger = LoggerFactory.getLogger(CacheControlFilter.class);

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    try {
      HttpServletResponse resp = (HttpServletResponse) response;
      resp.setHeader("Expires", "Tue, 03 Jul 2001 06:00:00 GMT");
      resp.setDateHeader("Last-Modified", new Date().getTime());
      resp.setHeader(
          "Cache-Control",
          "no-store, no-cache, must-revalidate, max-age=0, post-check=0, pre-check=0");
      resp.setHeader("Pragma", "no-cache");
    } catch (Exception e) {
      logger.error(
          "An error occuered while trying to prevent browser caching by customizing the "
              + "returned response object, this should not have a serious effect on the response it "
              + "self but you may experience some cache-related issues.",
          e);
    } finally {
      chain.doFilter(request, response);
    }
  }

  public void init(FilterConfig filterConfig) {}

  public void destroy() {}
}
