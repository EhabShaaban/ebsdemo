package com.ebs.dac.security.services;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.ICObFiscalYear;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUploadCreateValueObject;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUpload;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;

public class DObInitialBalanceUploadAuthorizationManager {

  private AuthorizationManager authorizationManager;
  private AuthorizationCommonUtils authorizationCommonUtils;
  private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;

  public DObInitialBalanceUploadAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
    authorizationCommonUtils = new AuthorizationCommonUtils(authorizationManager);
  }

  public AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(
        ICObFiscalYear.SYS_NAME, IActionsNames.READ_ALL, "ReadFiscalYears");
    permissionsConfig.addPermissionConfig(
        IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL, "ReadDocumentOwners");
    permissionsConfig.addPermissionConfig(
        ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadCompanies");
    permissionsConfig.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME,
        IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER,
        "ReadBusinessUnits");

    return permissionsConfig;
  }

  public void applyAuthorizationOnCreateAction(
      DObInitialBalanceUploadCreateValueObject initialBalanceUploadCreateValueObject) throws Exception {
    authorizationManager.authorizeAction(IDObInitialBalanceUpload.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IDObInitialBalanceUpload.SYS_NAME, IActionsNames.CREATE);

    authorizationManager.authorizeAction(ICObFiscalYear.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);

    authorizationCommonUtils.applyAuthorizationForPurchaseUnit(
        initialBalanceUploadCreateValueObject.getPurchaseUnitCode());
  }

  public void setcObPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep) {
    this.cObPurchasingUnitGeneralModelRep = cObPurchasingUnitGeneralModelRep;
    authorizationCommonUtils.setcObPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
  }
}
