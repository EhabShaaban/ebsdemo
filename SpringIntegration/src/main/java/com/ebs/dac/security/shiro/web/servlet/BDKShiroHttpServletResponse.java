package com.ebs.dac.security.shiro.web.servlet;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.servlet.ShiroHttpServletResponse;

/**
 * The intention of this class is to allow to disable Apache Shiro's URL Encoding support. <br>
 * <br>
 * See BDK-138
 *
 * @author Hesham Saleh
 */
public class BDKShiroHttpServletResponse extends ShiroHttpServletResponse {

  /** BDK-138 Check https://issues.apache.org/jira/browse/SHIRO-360 */
  boolean isUrlEncodingEnabled = false;

  public BDKShiroHttpServletResponse(
      HttpServletResponse wrapped, ServletContext context, ShiroHttpServletRequest request) {
    this(wrapped, context, request, false);
  }

  public BDKShiroHttpServletResponse(
      HttpServletResponse wrapped,
      ServletContext context,
      ShiroHttpServletRequest request,
      boolean allowUrlEncoding) {
    super(wrapped, context, request);
    this.isUrlEncodingEnabled = allowUrlEncoding;
  }

  @Override
  protected boolean isEncodeable(String location) {
    if (!isUrlEncodingEnabled) {
      return false;
    }
    return super.isEncodeable(location);
  }
}
