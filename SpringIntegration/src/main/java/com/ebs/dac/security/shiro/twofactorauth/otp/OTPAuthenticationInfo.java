package com.ebs.dac.security.shiro.twofactorauth.otp;

import com.ebs.dac.security.shiro.models.EBSPrincipal;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

/**
 * The correct authentication info to be checked against the passed {@link OTPAuthenticationToken}
 * in {@link OTPAuthenticatingRealm}.
 */
public class OTPAuthenticationInfo implements AuthenticationInfo {

  private static final long serialVersionUID = 1L;

  private EBSPrincipal principal;
  private String otpCode;
  private String realmName;

  public OTPAuthenticationInfo(EBSPrincipal principal, String otpCode, String realmName) {
    this.principal = principal;
    this.otpCode = otpCode;
    this.realmName = realmName;
  }

  public PrincipalCollection getPrincipals() {
    return new SimplePrincipalCollection(principal, realmName);
  }

  public Object getCredentials() {
    return getOtpCode();
  }

  public String getOtpCode() {
    return otpCode;
  }

  public void setOtpCode(String otpCode) {
    this.otpCode = otpCode;
  }

  public String getRealmName() {
    return realmName;
  }

  public void setRealmName(String realmName) {
    this.realmName = realmName;
  }

  public EBSPrincipal getPrincipal() {
    return principal;
  }

  public void setPrincipal(EBSPrincipal principal) {
    this.principal = principal;
  }
}
