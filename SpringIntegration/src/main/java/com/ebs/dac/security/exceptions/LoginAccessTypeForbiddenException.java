package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/**
 * When this kind of access (local/remote) is forbidden i.e. the corresponding user configuration is
 * locked
 */
public class LoginAccessTypeForbiddenException extends FirstFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_MESSAGE = "Login access type is forbidden";

  /** local or remote */
  private String origin;

  {
    ERROR_CODE = "LOGIN-ACCS_FORB";
    STATUS_CODE = HttpStatusCode.FORBIDDEN.code();
  }

  public LoginAccessTypeForbiddenException() {
    super(DEFAULT_MESSAGE);
  }

  public LoginAccessTypeForbiddenException(String origin) {
    this();
    this.origin = origin;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  @Override
  public String toString() {
    return origin + " access is prohibited";
  }
}
