package com.ebs.dac.security.shiro.twofactorauth.otp;

import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.twofactorauth.SecondFactorToken;

/**
 * The token that will be generated from {@link OTPAuthenticatingRealm} to hold the user principal
 * ({@link EBSPrincipal}) and credentials ({@link #otpCode})
 */
public class OTPAuthenticationToken implements SecondFactorToken {

  private static final long serialVersionUID = 1L;

  EBSPrincipal principal;
  String otpCode;

  public OTPAuthenticationToken(EBSPrincipal principal, String otpCode) {
    this.principal = principal;
    this.otpCode = otpCode;
  }

  public Object getPrincipal() {
    return principal;
  }

  public void setPrincipal(EBSPrincipal principal) {
    this.principal = principal;
  }

  public Object getCredentials() {
    return getOtpCode();
  }

  public String getOtpCode() {
    return otpCode;
  }

  public void setOtpCode(String otpCode) {
    this.otpCode = otpCode;
  }
}
