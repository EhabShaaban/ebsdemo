package com.ebs.dac.security.services;

import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;

public class CObExchangeRateAuthorizationManager {
    private AuthorizationManager authorizationManager;

    public CObExchangeRateAuthorizationManager(
            AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
        this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
    }

    public void authorizeReadAction(String sysName, String actionName) {
        this.authorizationManager.authorizeAction(sysName, actionName);
    }
}
