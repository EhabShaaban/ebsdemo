package com.ebs.dac.security.exceptions;

/**
 * When a resource that should NOT be accessed externally (by user type its url) e.g. 2nd-Factor
 * page, as some logic may redirect him to it and he is not allowed to navigate to it directly using
 * its url (The behavior of redirecting or directly accessing it is determined by checking if there
 * is a saved request).
 */
public class DirectResourceAccessException extends ForbiddenResourceException {

  /** */
  private static final long serialVersionUID = 1L;

  {
    ERROR_CODE = "ACESS-DRCT_FRBD";
  }
}
