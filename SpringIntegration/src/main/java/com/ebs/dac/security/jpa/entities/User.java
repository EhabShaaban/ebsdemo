/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** @author hesham */
@Entity
@Table(name = "EBSUser")
public class User implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = false)
  @Column(name = "Username")
  private String username;

  @Basic(optional = false)
  @Column(name = "Password")
  private String password;


  @Basic(optional = false)
  private Boolean sysFlag;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
  private List<UserAccount> userAccountList;

  public User() {}

  public User(Long id) {
    this.id = id;
  }

  public User(Long id, String username, String password) {
    this.id = id;
    this.username = username;
    this.password = password;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public List<UserAccount> getUserAccountList() {
    return userAccountList;
  }

  public void setUserAccountList(List<UserAccount> userAccountList) {
    this.userAccountList = userAccountList;
  }

  public void setSysFlag(Boolean sysFlag) {
    this.sysFlag = sysFlag;
  }

  public Boolean getSysFlag() {
    return sysFlag;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof User)) {
      return false;
    }
    User other = (User) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "User [id="
        + id
        + ", username="
        + username
        + ", password="
        + password
        + ", userAccountList="
        + userAccountList
        + "]";
  }
}
