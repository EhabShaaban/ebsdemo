package com.ebs.dac.security.shiro.models;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.subject.support.WebDelegatingSubject;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 20, 2018 9:31:08 AM
 */
public class EBSSubject extends WebDelegatingSubject {

  public EBSSubject(
      PrincipalCollection principals,
      boolean authenticated,
      String host,
      Session session,
      boolean sessionEnabled,
      ServletRequest request,
      ServletResponse response,
      SecurityManager securityManager) {
    super(
        principals,
        authenticated,
        host,
        session,
        sessionEnabled,
        request,
        response,
        securityManager);
  }

  public EBSSubject(
      PrincipalCollection principals,
      boolean authenticated,
      String host,
      Session session,
      ServletRequest request,
      ServletResponse response,
      SecurityManager securityManager) {
    super(principals, authenticated, host, session, request, response, securityManager);
  }

  public void checkPermission(String permission, BusinessObject entity) throws Exception {
    assertAuthzCheckPossible();
    ((EBSSecurityManager) securityManager).checkPermission(getPrincipals(), permission, entity);
  }
}
