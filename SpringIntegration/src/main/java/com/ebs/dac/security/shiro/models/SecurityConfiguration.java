package com.ebs.dac.security.shiro.models;

import java.io.Serializable;

/**
 * This is a POJO class used to hold the configuration to be used with users (either his specific
 * configuration or the global one).<br>
 * <br>
 * By configuration we mean:<br>
 * - Used in local or remote accessing (defined by {@link SecurityConfigurationType})<br>
 * - What is access level (defined by {@link SecurityClearnceLevel})<br>
 * - Validity od his 2nd-Factor authentication token (defined by {@link
 * 'SecondFactorCredentialValidity'})<br>
 */
public class SecurityConfiguration implements Serializable {

  private static final long serialVersionUID = 1L;

  private SecurityConfigurationType securityConfigurationType;

  private SecurityClearnceLevel securityClearnceLevel;

  private Integer secondFactorCredentialValidity;

  private String configurationState;

  public SecurityConfiguration(SecurityConfigurationType securityConfigurationType) {
    this.securityConfigurationType = securityConfigurationType;
  }

  public SecurityConfigurationType getSecurityConfigurationType() {
    return securityConfigurationType;
  }

  public void setSecurityConfigurationType(SecurityConfigurationType securityConfigurationType) {
    this.securityConfigurationType = securityConfigurationType;
  }

  public Integer getSecondFactorCredentialValidity() {
    return secondFactorCredentialValidity;
  }

  public void setSecondFactorCredentialValidity(Integer secondFactorCredentialValidity) {
    this.secondFactorCredentialValidity = secondFactorCredentialValidity;
  }

  /**
   * Copies the properties from the parameter (user-specific) configuration to a merged
   * configuration overwriting the global configuration.
   *
   * @param otherConf The user specific configuration for this kind of access (Local or Remote)
   * @return The merged configuration
   */
  public SecurityConfiguration mergeWithUserConfiguration(SecurityConfiguration otherConf) {
    SecurityConfiguration merged = new SecurityConfiguration(otherConf.securityConfigurationType);

    if (otherConf.secondFactorCredentialValidity != null) {
      merged.setSecondFactorCredentialValidity(otherConf.secondFactorCredentialValidity);
    } else {
      merged.setSecondFactorCredentialValidity(this.secondFactorCredentialValidity);
    }
    if (otherConf.securityClearnceLevel != null) {
      merged.setSecurityClearnceLevel(otherConf.securityClearnceLevel);
    } else {
      merged.setSecurityClearnceLevel(this.securityClearnceLevel);
    }

    return merged;
  }

  public SecurityClearnceLevel getSecurityClearnceLevel() {
    return securityClearnceLevel;
  }

  public void setSecurityClearnceLevel(SecurityClearnceLevel securityClearnceLevel) {
    this.securityClearnceLevel = securityClearnceLevel;
  }

  public String getConfigurationState() {
    return configurationState;
  }

  public void setConfigurationState(String configurationState) {
    this.configurationState = configurationState;
  }

  @Override
  public String toString() {

    return "Type: "
        + securityConfigurationType
        + ", Level: "
        + securityClearnceLevel
        + ", Validity: "
        + secondFactorCredentialValidity
        + ", State: "
        + configurationState;
  }

  /**
   * Determines whether this configuration are intended to be used in local accesses (intranet) or
   * remote accesses (internet).
   */
  public enum SecurityConfigurationType implements Serializable {
    LOCAL,
    REMOTE
  }

  /**
   * Ordered from the lowest to the highest. The order is very important for {@link #
   * 'compareTo(SecurityClearnceLevel)'} usages by clients.<br>
   * <br>
   * Order is:<br>
   * {@link #LEAST_RESTRICTIVE}<br>
   * {@link #STANDARD}<br>
   * {@link #RESTRICTIVE}
   */
  public enum SecurityClearnceLevel implements Serializable, Comparable<SecurityClearnceLevel> {
    /**
     * The used type NO url need 2nd-Factor authentication, so NO 2nd-Factor protected services are
     * accessible
     */
    LEAST_RESTRICTIVE,

    /**
     * The used type when ONLY some urls that may be accessed require 2nd-Factor authentication.
     * <br>
     * All 2nd-Factor protected urls are in {@link
     * /BDKCommonUtils/src/main/resources/second-factor-protected-urls.properties}
     */
    STANDARD,

    /** The type used when all service urls need 2nd-Factor authentication */
    RESTRICTIVE
  }
}
