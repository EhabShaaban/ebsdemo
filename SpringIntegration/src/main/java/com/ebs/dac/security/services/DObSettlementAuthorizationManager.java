package com.ebs.dac.security.services;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementCreationValueObject;
import com.ebs.dda.jpa.accounting.settlements.ICObSettlementType;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlement;

public class DObSettlementAuthorizationManager {
    private AuthorizationManager authorizationManager;

    public DObSettlementAuthorizationManager(
            AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
        this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
    }

    public void authorizeActionOnSection(BusinessObject businessObject, String actionName)
            throws Exception {
        try {
            this.authorizationManager.authorizeAction(businessObject, actionName);
        } catch (ConditionalAuthorizationException ex) {
            throw new AuthorizationException(ex);
        }
    }

    public void authorizeReadAction(String sysName, String actionName) {
        this.authorizationManager.authorizeAction(sysName, actionName);
    }

    public AuthorizationManager getAuthorizationManager() {
        return authorizationManager;
    }

    public void applyAuthorizationForCreation(DObSettlementCreationValueObject valueObject)
            throws Exception {

        authorizationManager.authorizeAction(IDObSettlement.SYS_NAME, IActionsNames.CREATE);
        authorizationManager.authorizeAction(ICObSettlementType.SYS_NAME, IActionsNames.READ_ALL);
    }

    public void authorizeReadAction(String objectName) {
        try {
            authorizationManager.authorizeAction(objectName, IActionsNames.READ_ALL);
        } catch (ConditionalAuthorizationException ex) {
            AuthorizationException authorizationException = new AuthorizationException(ex);
            throw authorizationException;
        }
    }
}