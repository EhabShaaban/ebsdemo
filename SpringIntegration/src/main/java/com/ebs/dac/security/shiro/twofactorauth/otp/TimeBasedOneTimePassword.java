package com.ebs.dac.security.shiro.twofactorauth.otp;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeBasedOneTimePassword {

  public static final String KEY = "3132333435363738393031323334353637383930";
  public static final String DIGITS = "6";
  public static final int INTERVAL_PERIOD_SECONDS = 30;
  private static final Logger log = LoggerFactory.getLogger(TimeBasedOneTimePassword.class);
  private static final int[] DIGITS_POWER
      //	0 1 2 3 4 5 6 7 8
      = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000};

  private static byte[] hmac_sha(String crypto, byte[] keyBytes, byte[] text) {
    try {
      Mac hmac;
      hmac = Mac.getInstance(crypto);
      SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
      hmac.init(macKey);
      return hmac.doFinal(text);
    } catch (GeneralSecurityException gse) {
      throw new UndeclaredThrowableException(gse);
    }
  }

  private static byte[] hexStr2Bytes(String hex) {
    //		Adding one byte to get the right conversion
    //		Values starting with "0" can be converted
    byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();
    //		Copy all the REAL bytes, not the "first"
    byte[] ret = new byte[bArray.length - 1];
    for (int i = 0; i < ret.length; i++) ret[i] = bArray[i + 1];
    return ret;
  }

  public static String generateTOTP(String key, String time, String returnDigits) {
    int codeDigits = Integer.decode(returnDigits).intValue();
    String result = null;
    String crypto = "HmacSHA1";
    //		Using the counter
    //		First 8 bytes are for the movingFactor
    //		Compliant with base RFC 4226 (HOTP)
    while (time.length() < 16) time = "0" + time;

    //		Get the HEX in a Byte[]
    byte[] msg = hexStr2Bytes(time);
    byte[] k = hexStr2Bytes(key);
    byte[] hash = hmac_sha(crypto, k, msg);

    //		put selected bytes into result int
    int offset = hash[hash.length - 1] & 0xf;

    int binary =
        ((hash[offset] & 0x7f) << 24)
            | ((hash[offset + 1] & 0xff) << 16)
            | ((hash[offset + 2] & 0xff) << 8)
            | (hash[offset + 3] & 0xff);

    int otp = binary % DIGITS_POWER[codeDigits];

    result = Integer.toString(otp);
    while (result.length() < codeDigits) {
      result = "0" + result;
    }
    return result;
  }

  public static String generateTOTP(String time) {
    return generateTOTP(KEY, time, DIGITS);
  }

  public static String generateTOTP(long time) {

    String timeStr = Long.toHexString(time).toUpperCase();

    return generateTOTP(KEY, timeStr, DIGITS);
  }

  public static String toHex(String str) {
    return String.format("%x", new BigInteger(str.getBytes()));
  }

  public static long getCurrentTime(int intervalPeriod) {

    long millis = System.currentTimeMillis() / 1000;
    millis = millis / intervalPeriod;

    return millis;
  }

  public static long getCurrentTime() {

    long millis = System.currentTimeMillis();

    millis = millis / (1000 * INTERVAL_PERIOD_SECONDS);

    return millis;
  }

  public static boolean isValid(String code, int pastIntervals, int futureIntervals) {

    long currentTime = getCurrentTime();

    String target = generateTOTP(currentTime);

    if (code.equals(target)) {
      return true;
    }

    for (int i = 1; i <= pastIntervals; ++i) {
      target = generateTOTP(currentTime - i);

      if (code.equals(target)) {
        return true;
      }
    }

    for (int i = 1; i <= futureIntervals; ++i) {
      target = generateTOTP(currentTime + i);

      if (code.equals(target)) {
        return true;
      }
    }

    return false;
  }

  public static boolean test(String code, int seconds) {
    try {
      Thread.sleep(seconds * 1000);

      return isValid(code, 1, 1);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }

    return false;
  }

  public static void main(String[] args) {

    long start = System.currentTimeMillis();

    String code = generateTOTP(getCurrentTime());

    boolean isValid = true;

    for (int i = 0; isValid; ++i) {
      log.info("Code [" + code + "] is valid? " + (isValid = test(code, 1)));
    }

    long end = System.currentTimeMillis();

    log.info(String.valueOf((end - start) / 1000));
  }
}
