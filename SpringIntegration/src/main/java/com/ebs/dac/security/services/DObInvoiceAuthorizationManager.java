package com.ebs.dac.security.services;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObInvoiceActionNames;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;

public class DObInvoiceAuthorizationManager {

  private AuthorizationManager authorizationManager;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;
  private MObVendorGeneralModelRep VendorGeneralModelRep;
  private AuthorizationCommonUtils authorizationCommonUtils;
  public DObInvoiceAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
    authorizationCommonUtils = new AuthorizationCommonUtils(authorizationManager);
  }

  public void applyAuthorizationForCreate(
      DObVendorInvoiceCreationValueObject invoiceCreationValueObject) throws Exception {
    authorizationManager.authorizeAction(IDObVendorInvoice.SYS_NAME, IActionsNames.CREATE);
    authorizationManager.authorizeAction(IMObVendor.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);
    checkPurchaseOrderPurchaseUnitAuthorization(invoiceCreationValueObject);
    checkVendorPurchaseUnitAuthorization(invoiceCreationValueObject);
  }

  private void checkPurchaseOrderPurchaseUnitAuthorization(
      DObVendorInvoiceCreationValueObject invoiceCreationValueObject) throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel = purchaseOrderGeneralModelRep
        .findOneByUserCode(invoiceCreationValueObject.getPurchaseOrderCode());
    if (purchaseOrderGeneralModel != null) {
      authorizationCommonUtils.applyAuthorizationForPurchaseUnit(purchaseOrderGeneralModel.getPurchaseUnitCode());
    }
  }

  private void checkVendorPurchaseUnitAuthorization(
      DObVendorInvoiceCreationValueObject invoiceCreationValueObject) throws Exception {
    MObVendorGeneralModel vendorGeneralModel =
        VendorGeneralModelRep.findOneByUserCode(invoiceCreationValueObject.getVendorCode());
    if (vendorGeneralModel != null) {
      authorizationCommonUtils.applyAuthorizationForPurchaseUnit(vendorGeneralModel.getPurchasingUnitCodes());
    }
  }

  public void authorizeReadGeneralData(DObVendorInvoiceGeneralModel invoiceGeneralData)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoiceGeneralData,
          IDObInvoiceActionNames.READ_GENERAL_DATA_SECTION);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException();
    }
  }

  public void authorizeReadInvoiceSummary(DObVendorInvoiceGeneralModel invoiceGeneralData)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoiceGeneralData,
          IDObInvoiceActionNames.READ_Summary_SECTION);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException();
    }
  }

  public void authorizeReadItems(DObVendorInvoiceGeneralModel invoice) throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoice, IDObInvoiceActionNames.READ_ITEMS_SECTION);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException();
    }
  }

  public void authorizeReadTaxes(DObVendorInvoiceGeneralModel invoice) throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoice, IDObInvoiceActionNames.READ_TAXES_SECTION);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException();
    }
  }

  public void authorizeDeleteItems(DObVendorInvoiceGeneralModel invoice) throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoice, IDObInvoiceActionNames.UPDATE_ITEMS);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  public void authorizeDeleteTaxes(DObVendorInvoiceGeneralModel invoice) throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoice, IDObInvoiceActionNames.UPDATE_TAXES);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  public void authorizeReadSection(IObVendorInvoiceDetailsGeneralModel invoiceDetailsGeneralModel,
      String actionName) throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoiceDetailsGeneralModel, actionName);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException();
    }
  }

  public void authorizeReadCompanyData(IObVendorInvoiceCompanyDataGeneralModel companyGeneralModel)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(companyGeneralModel,
          IDObInvoiceActionNames.READ_COMPANY_DATA_SECTION);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException();
    }
  }

  public AuthorizationManager getAuthorizationManager() {
    return this.authorizationManager;
  }

  public void authorizeUpdateItem(DObVendorInvoiceGeneralModel invoiceGeneralModel)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoiceGeneralModel,
          IDObInvoiceActionNames.UPDATE_ITEMS);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  public void authorizeCreateJournalEntryForInvoice(
      DObVendorInvoiceGeneralModel invoiceGeneralModel) throws Exception {
    try {
      this.authorizationManager.authorizeAction(invoiceGeneralModel,
          IDObInvoiceActionNames.CREATE_JOURNAL_ENTRY_INVOICE);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  public void applyAuthorizatioinOnReads(DObVendorInvoiceGeneralModel invoiceGeneralModel) {
    try {
      authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);

      authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  public void authorizeActionOnSection(BusinessObject businessObject, String actionName)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

  public void setcObPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep) {
    this.cObPurchasingUnitGeneralModelRep = cObPurchasingUnitGeneralModelRep;
    authorizationCommonUtils.setcObPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
  }

  public void setVendorGeneralModelRep(MObVendorGeneralModelRep vendorGeneralModelRep) {
    VendorGeneralModelRep = vendorGeneralModelRep;
  }

}
