package com.ebs.dac.security.shiro.filters;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BdkLogoutFilter extends LogoutFilter {

  private static final Logger log = LoggerFactory.getLogger(BdkLogoutFilter.class);

  /**
   * The only difference between this implementation and its super, is that NO redirection is made.
   */
  @Override
  protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {

    Subject subject = getSubject(request, response);
    try {
      subject.logout();
      return handleLogoutSuccess(request, response);
    } catch (SessionException ise) {
      log.debug(
          "Encountered session exception during logout.  This can generally safely be ignored.",
          ise);
      return handleLogoutFailure(request, response, ise);
    }
  }

  protected abstract boolean handleLogoutSuccess(ServletRequest request, ServletResponse response)
      throws Exception;

  protected abstract boolean handleLogoutFailure(
      ServletRequest request, ServletResponse response, Exception ex) throws Exception;
}
