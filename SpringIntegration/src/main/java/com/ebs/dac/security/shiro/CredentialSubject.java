package com.ebs.dac.security.shiro;

import org.apache.shiro.subject.Subject;

public class CredentialSubject {

  private Subject subject;
  private String username;
  private String ccode;

  public Subject getSubject() {
    return subject;
  }

  public void setSubject(Subject subject) {
    this.subject = subject;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getCcode() {
    return ccode;
  }

  public void setCcode(String ccode) {
    this.ccode = ccode;
  }

  @Override
  public boolean equals(Object obj) {

    if (obj instanceof CredentialSubject) {
      String objUsername = ((CredentialSubject) obj).getUsername();
      String objCcode = ((CredentialSubject) obj).getCcode();

      boolean result = username == null && objUsername == null;
      result = result && this.ccode == null && objCcode == null;

      if (!result) {
        result = username.equals(objUsername) && ccode.equals(objCcode);
      }
      return result;
    }
    return false;
  }

  @Override
  public String toString() {
    return "CredentialSubject [subject="
        + subject
        + ", username="
        + username
        + ", ccode="
        + ccode
        + "]";
  }
}
