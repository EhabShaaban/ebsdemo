package com.ebs.dac.security.shiro.models;

import java.io.Serializable;
import java.util.List;

/**
 * Principal is what defines user identity. It is defined by:<br>
 * - Username<br>
 * - CompanyCode<br>
 * - EmployeeID<br>
 * - SecondFactorMeanName (i.e. will be authenticated via OTP, finger print, ...etc)<br>
 * - SavedConfiguration (i.e. local and remote configurations "Just n case we needed them")<br>
 * - PrimaryConfiguration (The configuration to be used which is either his specified one or the
 * global configuration in case that he doesn't have specific configuration for this application
 * instance "local or remote")<br>
 */
public class EBSPrincipal implements Serializable {

  private static final long serialVersionUID = 1L;

  private String username;
  private String companyCode;
  private Long userId;
  private Boolean isSysPasswordFlag;
  private String secondFactorMeanName;
  private List<SecurityConfiguration>
      savedConfiguration; // List because it may has local and remote configuration
  private SecurityConfiguration primaryConfiguration;

  public EBSPrincipal(Long userId, String username, Boolean isSysPasswordFlag, String companyCode) {
    this(userId, username, isSysPasswordFlag, companyCode, null);
  }

  public EBSPrincipal(
      Long userId, String username, Boolean isSysPasswordFlag, String companyCode,
      String secondFactorMeanName) {
    this.username = username;
    this.companyCode = companyCode;
    this.userId = userId;
    this.isSysPasswordFlag = isSysPasswordFlag;
    this.secondFactorMeanName = secondFactorMeanName;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getCompany() {
    return companyCode;
  }

  public Boolean getSysPasswordFlag() {
    return isSysPasswordFlag;
  }

  public void setCompany(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getSecondFactorMeanName() {
    return secondFactorMeanName;
  }

  public void setSecondFactorMeanName(String secondFactorMeanName) {
    this.secondFactorMeanName = secondFactorMeanName;
  }

  public List<SecurityConfiguration> getSavedConfiguration() {
    return savedConfiguration;
  }

  public void setSavedConfiguration(List<SecurityConfiguration> savedConfiguration) {
    this.savedConfiguration = savedConfiguration;
  }

  public SecurityConfiguration getPrimaryConfiguration() {
    return primaryConfiguration;
  }

  public void setPrimaryConfiguration(SecurityConfiguration primaryConfiguration) {
    this.primaryConfiguration = primaryConfiguration;
  }

  public Long getUserId() {
    return userId;
  }

  @Override
  public String toString() {
    return username;
  }
}
