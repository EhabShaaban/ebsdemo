package com.ebs.dac.security.services;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;

public class DObPaymentRequestAuthorizationManager {
  private AuthorizationManager authorizationManager;

  public DObPaymentRequestAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
  }

  public void authorizeActionOnSection(BusinessObject businessObject, String actionName)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void authorizeReadAction(String sysName, String actionName) {
    this.authorizationManager.authorizeAction(sysName, actionName);
  }

  public AuthorizationManager getAuthorizationManager() {
    return authorizationManager;
  }

  public void authorizeAction(BusinessObject businessObject, String permission) throws Exception {
    authorizationManager.authorizeAction(businessObject, permission);
  }
}
