package com.ebs.dac.security.exceptions;

import java.util.HashMap;
import java.util.Map;

/**
 * Trying to access a resource that is forbidden to be accessed (e.g. some urls should be allowed
 * via redirection only not by entering its url).
 *
 * @see DirectResourceAccessException
 * @author mismail
 */
public class ForbiddenResourceException extends Exception implements IGeneralException {

  /** */
  private static final long serialVersionUID = 1L;
  public static String ERROR_CODE;
  public Map<String, Object> params = new HashMap<String, Object>();

  {
    ERROR_CODE = "ACESS-RSRC_FRBD";
  }

  /**
   * Don't override, just change the value of {@link #ERROR_CODE}
   *
   * @return {@link #ERROR_CODE}
   */
  public final String getErrorCode() {
    return ERROR_CODE;
  }

  public Map<String, Object> getParams() {
    return params;
  }
}
