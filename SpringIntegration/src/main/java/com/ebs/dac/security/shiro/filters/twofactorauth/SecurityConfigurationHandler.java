package com.ebs.dac.security.shiro.filters.twofactorauth;

import com.ebs.dac.security.shiro.models.SecurityConfiguration;

/**
 * Responsible for merging the global configuration with user-specific configuration in a single
 * {@link SecurityConfiguration}. The priority is for user-specific configuration. This is done in
 * {@link #calculateConfiguration(SecurityConfiguration)}
 */
public class SecurityConfigurationHandler {

  SecurityConfiguration globalSecurityConfiguration;

  public SecurityConfigurationHandler(SecurityConfiguration globalSecurityConfiguration) {
    this.globalSecurityConfiguration = globalSecurityConfiguration;
  }

  /**
   * This is used to merge user specific configuration for the current application instance
   * (local/remote), if exists, with global configuration, otherwise if no user-specific
   * configuration it returns the global. <br>
   * This returns a new instance of {@link SecurityConfiguration} and in case of existence of
   * user-specific configuration its values have higher priority in the merge operation.<br>
   * see {@link SecurityConfiguration#mergeWithUserConfiguration(SecurityConfiguration)}
   *
   * @param userConf
   * @return
   */
  public SecurityConfiguration calculateConfiguration(SecurityConfiguration userConf) {
    if (userConf != null) return globalSecurityConfiguration.mergeWithUserConfiguration(userConf);
    else return globalSecurityConfiguration;
  }

  public SecurityConfiguration getGlobalSecurityConfiguration() {
    return globalSecurityConfiguration;
  }

  public void setGlobalSecurityConfiguration(SecurityConfiguration globalSecurityConfiguration) {
    this.globalSecurityConfiguration = globalSecurityConfiguration;
  }
}
