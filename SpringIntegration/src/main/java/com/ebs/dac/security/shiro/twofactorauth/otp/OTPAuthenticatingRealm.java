package com.ebs.dac.security.shiro.twofactorauth.otp;

import com.ebs.dac.security.shiro.models.EBSPrincipal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for providing a correct {@link OTPAuthenticationInfo} to be matched
 * with a passed {@link OTPAuthenticationToken}.
 */
public class OTPAuthenticatingRealm extends AuthenticatingRealm {

  private static final Logger logger = LoggerFactory.getLogger(OTPAuthenticatingRealm.class);

  @Override
  public boolean supports(AuthenticationToken token) {
    return token != null && OTPAuthenticationToken.class.isAssignableFrom(token.getClass());
  }

  @SuppressWarnings("rawtypes")
  @Override
  public Class getAuthenticationTokenClass() {
    return OTPAuthenticationToken.class;
  }

  @Override
  protected void onInit() {
    super.onInit();
    setName(this.getClass().getSimpleName());

    setCredentialsMatcher(
        new CredentialsMatcher() {

          public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
            // Casting is usually dangerous, but here we do guarantee that no token but OTPAu*Token
            // will exist thanks to #supports method
            boolean isValid =
                TimeBasedOneTimePassword.isValid((String) token.getCredentials(), 1, 1);
            logger.debug("OTP CredentialsMatcher sees the code valid ? " + isValid);
            return isValid;
          }
        });
  }

  /**
   * Generates a valid {@link OTPAuthenticationInfo} to be matched with the passed {@link
   * OTPAuthenticationToken}.<br>
   * <b>Note: </b>the "DUMMY" code is not used as we check that code in passed token is valid in the
   * current time window.
   */
  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken)
      throws AuthenticationException {
    /*String code = TimeBasedOneTimePassword.generateTOTP(TimeBasedOneTimePassword.getCurrentTime());
    return new OTPAuthenticationInfo((String) authToken.getPrincipal(), code , this.getName());*/
    // Creating a DUMMY AuthInfo object, because per the design of TimeBasedOneTimePassword
    // utilties, the isValid check does this job
    return new OTPAuthenticationInfo(
        (EBSPrincipal) SecurityUtils.getSubject().getPrincipal(), "DUMMY", this.getName());
  }
}
