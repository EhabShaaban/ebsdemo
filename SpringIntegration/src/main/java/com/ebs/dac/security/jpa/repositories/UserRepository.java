package com.ebs.dac.security.jpa.repositories;

import com.ebs.dac.security.jpa.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

  User findOneById(Long id);
}
