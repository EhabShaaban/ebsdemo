package com.ebs.dac.security.utils;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.models.ClassPermission;
import java.lang.annotation.Annotation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 1, 2018 1:07:54 PM
 */
public class AuthorizationUtils {

  public static final String WILD_CARD_PERMISSION = "*";
  public static final String ADMIN_PERMISSION = "*:*";
  public static final String PERMISSION_SEPERATOR = ":";
  public static final String PERMISSION_ACTIONS_SEPERATOR = ",";

  public static ClassPermission createReadPermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.READ);
  }

  public static ClassPermission createReadAllPermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.READ_ALL);
  }

  public static ClassPermission createLockPermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.LOCK);
  }

  public static ClassPermission createUnlockPermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.UNLOCK);
  }

  public static ClassPermission createCreatePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.CREATE);
  }

  public static ClassPermission createUpdatePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.SAVE);
  }

  public static ClassPermission createDeletePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.DELETE);
  }

  public static ClassPermission createDeleteAllPermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.DELETE_ALL);
  }

  public static ClassPermission createActivatePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.ACTIVATE);
  }

  public static ClassPermission createActivateAllPermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.ACTIVATE_ALL);
  }

  public static ClassPermission createDeactivatePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.DEACTIVATE);
  }

  public static ClassPermission createDeactivateAllPermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.DEACTIVATE_ALL);
  }

  public static ClassPermission createUploadResourcePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.UPLOAD_RESOURCE);
  }

  public static ClassPermission createDownloadResourcePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.DOWNLOAD_RESOURCE);
  }

  public static ClassPermission createDeleteResourcePermission(String objectName) {
    return createClassPermission(objectName, IActionsNames.DELETE_RESOURCE);
  }

  public static ClassPermission createClassPermission(String objectName, String actionName) {
    String permission = createPermissionExpression(objectName, actionName);
    RequiresPermissions requiresPermissions = createRequiresPermissionsAnnotation(permission);
    return createClassPermissionAnnotation(actionName, requiresPermissions);
  }

  private static String createPermissionExpression(String objectName, String actionName) {
    return objectName + PERMISSION_SEPERATOR + actionName;
  }

  private static RequiresPermissions createRequiresPermissionsAnnotation(String value) {
    return new RequiresPermissions() {

      @Override
      public Class<? extends Annotation> annotationType() {
        return RequiresPermissions.class;
      }

      @Override
      public String[] value() {
        return new String[] {value};
      }

      @Override
      public Logical logical() {
        return null;
      }
    };
  }

  private static ClassPermission createClassPermissionAnnotation(
      String key, RequiresPermissions permission) {
    return new ClassPermission(key, permission);
  }
}
