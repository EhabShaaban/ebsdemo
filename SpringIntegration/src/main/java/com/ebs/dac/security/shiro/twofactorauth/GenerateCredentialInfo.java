package com.ebs.dac.security.shiro.twofactorauth;

import org.joda.time.DateTime;

public class GenerateCredentialInfo {

  private DateTime expiryDate;
  private int timeout;
  private boolean invalidated;

  public GenerateCredentialInfo(DateTime expiryDate) {
    super();
    this.expiryDate = expiryDate;
    invalidated = false;
  }
  //	TimeBasedOneTimePassword.INTERVAL_PERIOD_SECONDS
  public GenerateCredentialInfo(int timeout) {
    super();
    this.expiryDate = DateTime.now().plusSeconds(timeout);
    invalidated = false;
  }

  public DateTime getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(DateTime expiryDate) {
    this.expiryDate = expiryDate;
  }

  public boolean isValid() {
    return !invalidated && expiryDate.isAfterNow();
  }

  public void invalidate() {
    invalidated = true;
  }

  @Override
  public String toString() {
    return "GenerateCredentialInfo [expiryDate="
        + expiryDate
        + ", invalidated="
        + invalidated
        + "]";
  }

  public int getTimeout() {
    return timeout;
  }

  public void setTimeout(int timeout) {
    this.timeout = timeout;
  }
}
