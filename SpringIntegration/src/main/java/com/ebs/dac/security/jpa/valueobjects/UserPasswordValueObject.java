package com.ebs.dac.security.jpa.valueobjects;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class UserPasswordValueObject implements IValueObject {

  String currentPassword;
  String newPassword;
  String confirmedNewPassword;

  public String getCurrentPassword() {
    return currentPassword;
  }

  public String getNewPassword() {
    return newPassword;
  }

  public String getConfirmedNewPassword() {
    return confirmedNewPassword;
  }
}
