package com.ebs.dac.security.jpa.repositories;

import com.ebs.dac.security.jpa.entities.UserPermissionGeneralModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface UserPermissionGeneralModelRepository
        extends CrudRepository<UserPermissionGeneralModel, Long> {

  @Query(
          "SELECT p FROM UserPermissionGeneralModel p "
                  + "WHERE (p.authorizationConditionId IN :authorization_conditions Or p.authorizationConditionId IS NULL)"
                  + "AND p.userName = :username "
                  + "AND (p.objectName = :objectname OR p.objectName = '*')")
  List<UserPermissionGeneralModel> findAllUserPermissionWithConditions(
          @Param("authorization_conditions") Collection<Long> authorizationConditionIds,
          @Param("username") String userName,
          @Param("objectname") String ObjectName);

  @Query(
          "SELECT p FROM UserPermissionGeneralModel p "
                  + "WHERE p.userName = :username "
                  + "AND (p.objectName = :objectname OR p.objectName = '*')")
  List<UserPermissionGeneralModel> findAllUserPermissionWithOutConditions(
          @Param("username") String userName, @Param("objectname") String ObjectName);

}
