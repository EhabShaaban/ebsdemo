package com.ebs.dac.security.shiro.models;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.shiro.realms.BDKSecurityRealm;
import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 20, 2018 10:25:05 AM
 */
public class EBSModularRealmAuthorizer extends ModularRealmAuthorizer {

  public boolean isPermitted(
      PrincipalCollection principals, String permission, BusinessObject entity) throws Exception {
    assertRealmsConfigured();
    for (Realm realm : getRealms()) {
      if (!(realm instanceof BDKSecurityRealm)) {
        continue;
      }
      if (((BDKSecurityRealm) realm).isPermitted(principals, permission, entity)) {
        return true;
      }
    }
    return false;
  }

  public void checkPermission(
      PrincipalCollection principals, String permission, BusinessObject entity) throws Exception {
    assertRealmsConfigured();
    if (!isPermitted(principals, permission, entity)) {
      throw new AuthorizationException("Subject does not have permission [" + permission + "]");
    }
  }
}
