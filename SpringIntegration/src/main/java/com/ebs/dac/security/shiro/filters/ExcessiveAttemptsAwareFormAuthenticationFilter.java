package com.ebs.dac.security.shiro.filters;

import com.ebs.dac.common.utils.CommonConstants;
import com.ebs.dac.security.exceptions.UnauthenticatedForFirstFactorException;
import com.ebs.dac.security.shiro.CredentialSubject;
import com.ebs.dac.security.shiro.IGlobalUsersHandler;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.UsernamePasswordCompanyToken;
import com.ebs.dac.security.shiro.session.SessionAttributeKeys;
import com.ebs.dac.security.usermanage.utils.SessionVariableUtils;
import java.util.Set;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the basic authentication filter (replacement of the shiro's authc filter) and adds the
 * ability to control maximum number of wrong login attempts. It is also responsible for preventing
 * users from repeating the login process on the same account (<i>username and company</i>).
 */
public abstract class ExcessiveAttemptsAwareFormAuthenticationFilter
    extends FormAuthenticationFilter {

  private static final Logger logger =
      LoggerFactory.getLogger(ExcessiveAttemptsAwareFormAuthenticationFilter.class);

  private IGlobalUsersHandler globalUserHandler;

  /**
   * This property is set from shiro-security.xml (This is the max. login attempts eg. if it is 3
   * then after the fourth attempt, the user is kicked
   */
  private Integer maxLoginTrialsCount;

  /**
   * This property is set from shiro-security.xml and Will be used when max. wrong login attempts is
   * reached
   */
  private int lockLoginTimeout;

  /**
   * Allows login if the user is authenticated, or sent correct authentication info.<br>
   * <b>Logic:</b><br>
   * - First it checks if the user is authenticated.<br>
   * - If the previous check fails, then it checks if user tries to authenticate (POST to login
   * page), then:<br>
   * <li>First check if reached maximum number of wrong login attempts to either lock login and kick
   *     him or the lock timeout is expired and user can try to login again.<br>
   * <li>If the previous check fails, it checks if he is already logged in before and trying to
   *     login again to prevent account multi-access.<br>
   *     <br>
   *     - If the user tries to GET the login page then allow him.<br>
   *     - If all these checks fail, then it {@link #onAccessDenied(ServletRequest,
   *     ServletResponse)} is called to check if the request is for resource to throw an {@link
   *     UnauthenticatedForFirstFactorException} or the request is to login so try to make login
   *     operation<br>
   */
  /*
  @Override
  public boolean onPreHandle(ServletRequest request,
  		ServletResponse response, Object mappedValue) throws Exception {

  	Subject subject = SecurityUtils.getSubject();
  	Session session = subject.getSession();

  	if (subject.isAuthenticated()){
  		if (isLoginRequest(request, response)){
  			return handleLoginRequestWhileAuthenticated(request, response);
  		}
  		return handleAuthenticatedRequest();
  	}

  	if (subject.isRemembered()){
  		// We don't support Remember Me yet, so log him out
  		subject.logout();
  		return handleAccessDenied(request, response);
  	}

  	String username = getUsername(request);
  	String ccode = getCcode(request);

  	if(isLoginRequest(request, response)) {

  		Boolean result = checkAndReactIfLoginAttemptsExceededLimit(request, response, session);
  		if (result != null){
  			return result;
  		}

  		if(isPOSTRequest(request, response)) {
  			//<Concurrent> Check if the user trying to login has logged in before (Concurrent login issue)
  			if(username != null) {//has a username (note: this filter is only used with basic login i.e. username/password)
  				if(isUserAlreadyLoggedIn(username, ccode)) {//Here I want to make sure that the user has already logged in
  					//trying to login while he already logged in before (in the same session)
  					return handleMultiAccessOnAccountAttempt(request, response);
  					// return false;
  				}
  			}
  		} else if(isGETRequest(request, response)) {//get login
  			//allow them to see login page ;)
  			return true;
  		}
  	}
  	return super.onPreHandle(request, response, mappedValue);
  	//</Concurrent>
  }
  //*/
  protected Boolean checkAndReactIfLoginAttemptsExceededLimit(
      ServletRequest request, ServletResponse response, Session session) throws Exception {
    Integer loginAttempts = (Integer) session.getAttribute(SessionAttributeKeys.LOGIN_ATTEMPTS);
    logger.debug("Current login attempts counter: " + loginAttempts);

    if (loginAttempts != null
        && loginAttempts >= maxLoginTrialsCount) { // exceeded max. Wrong attempts
      DateTime lockLoginTime =
          (DateTime) session.getAttribute(SessionAttributeKeys.MAX_LOGIN_ATTEMPTS_REACHED_TIME);

      if (lockLoginTime != null) { // login was locked earlier
        if (DateTime.now()
            .isAfter(
                lockLoginTime.plusSeconds(lockLoginTimeout))) { // unlock login timeout is expired
          SessionVariableUtils.resetCounter(session, SessionAttributeKeys.LOGIN_ATTEMPTS);
          SessionVariableUtils.removeKey(
              session, SessionAttributeKeys.MAX_LOGIN_ATTEMPTS_REACHED_TIME);
        } else {
          lockLoginOperation(session);
          return handleExcessiveAttempts(request, response, loginAttempts);
          // return false;
        }
      } else { // first wrong attempt after the maximum number is reached, so lock login and kick
               // user
        lockLoginOperation(session);
        return handleExcessiveAttempts(request, response, loginAttempts);
      }
    }

    // No problems so far
    return null;
  }

  /**
   * Same as {@link #handleAuthenticatedRequest()}, except when the request is to the login page.
   * <br>
   * You may choose to return true to let the chain continue and let the login page be responsible
   * to handle it, or simply override it and forward to the success URL first then and return false,
   * or even return a custom REST message, as you wish.
   *
   * @param response
   * @param request
   * @return
   * @throws Exception
   */
  protected abstract boolean handleLoginRequestWhileAuthenticated(
      ServletRequest request, ServletResponse response) throws Exception;

  /**
   * Usually nothing should change this, all authenticated requests should get allowed to continue
   * the chain
   *
   * @return
   */
  protected boolean handleAuthenticatedRequest() {
    return true;
  }

  private void lockLoginOperation(Session session) {
    session.setAttribute(SessionAttributeKeys.MAX_LOGIN_ATTEMPTS_REACHED_TIME, DateTime.now());
  }

  @Override
  protected boolean isAccessAllowed(
      ServletRequest request, ServletResponse response, Object mappedValue) {

    // Just in case we needed it
    return super.isAccessAllowed(request, response, mappedValue);
  }

  /**
   * That means that a url that is 1st-Factor protected was called and {@link
   * #onPreHandle(ServletRequest, ServletResponse, Object)} failed. Then either login is being tried
   * (call {@link #executeLogin(ServletRequest, ServletResponse)}) or accessing another resource is
   * being tried (throw {@link UnauthenticatedForFirstFactorException}).
   */
  @Override
  protected boolean onAccessDenied(
      ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {

    if (isLoginRequest(request, response)) {
      if (isLoginSubmission(request, response)) {

        return executeLogin(request, response);
      } else {
        // Shouldn't come here as it was handled before (onPreHandle)
        return true;
      }
    }

    return handleAccessDenied(request, response);
  }

  /**
   * @param username
   * @param ccode
   * @return true in case that he already made successful authentication operation before (in logged
   *     in users list), false otherwise.
   */
  private boolean isUserAlreadyLoggedIn(String username, String ccode) {
    Set<CredentialSubject> loggedInUsers = globalUserHandler.getLoggedInUsers();
    logger.info(
        "Trying to check if user "
            + username
            + " and ccode "
            + ccode
            + " is already logged in. Current list: "
            + loggedInUsers);

    for (CredentialSubject credentialSubject : loggedInUsers) {
      if (username.equals(credentialSubject.getUsername())
          && ccode.equals(credentialSubject.getCcode())) {
        logger.info(
            "User "
                + username
                + " and ccode "
                + ccode
                + " was found to be already logged in. Current list: "
                + loggedInUsers);
        return true;
      }
    }
    logger.info(
        "User "
            + username
            + " and ccode "
            + ccode
            + " was not found to be already logged in. Current list: "
            + loggedInUsers);
    return false;
  }

  /**
   * In case that {@link #onLoginSuccess(AuthenticationToken, Subject, ServletRequest,
   * ServletResponse)} returns false (i.e. user isn't authenticated or remembered), then it checks:
   * <br>
   * - if the request is to login then the normal login procedure is executed - if the request was
   * for any other resource, then save request and go to login.jsp
   */
  @Override
  protected boolean onAccessDenied(ServletRequest request, ServletResponse response)
      throws Exception {
    logger.trace("ExcessiveAttemptsAwareFormAuthenticationFilter.onAccessDenied()");
    // Just in case we needed it
    return super.onAccessDenied(request, response);
  }

  @Override
  protected AuthenticationToken createToken(
      String username, String password, ServletRequest request, ServletResponse response) {
    HttpServletRequest httpRequest = (HttpServletRequest) request;

    String companyCode =
        httpRequest.getHeader(CommonConstants.COMPANY_CODE_HTTP_REQUEST_HEADER_ATTRIBUTE);
    boolean rememberMe = isRememberMe(request);
    String host = getHost(request);
    EBSPrincipal subject = (EBSPrincipal) getSubject(request, response).getPrincipal();
    Boolean isSysPasswordFlag = subject.getSysPasswordFlag();
    return new UsernamePasswordCompanyToken(username, password, isSysPasswordFlag, companyCode,
        rememberMe, host);
  }

  @Override
  protected AuthenticationToken createToken(
      String username, String password, boolean rememberMe, String host) {
    return super.createToken(username, password, rememberMe, host);
  }

  /**
   * Adds the current user to the list of logged-in users (to prevent further accesses) and
   * redirects to the saved request (the originally called, log-in protected, resource OR home page
   * for application).
   */
  @Override
  protected boolean onLoginSuccess(
      AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response)
      throws Exception {

    logger.info(
        "Adding Subject "
            + subject
            + " to system-wide logged in users. Current logged in users: "
            + globalUserHandler.getLoggedInUsers());
    globalUserHandler.addUserToLoggedInUsers(subject);
    logger.info(
        "Added Subject "
            + subject
            + " to system-wide logged in users. Current logged in users: "
            + globalUserHandler.getLoggedInUsers());

    //		WebUtils.redirectToSavedRequest(request, response, getSuccessUrl());

    return handleLoginSuccess(token, subject, request, response);
  }

  /** Simply redirects to login.jsp */
  @Override
  protected boolean onLoginFailure(
      AuthenticationToken token,
      AuthenticationException e,
      ServletRequest request,
      ServletResponse response) {
    super.onLoginFailure(token, e, request, response);

    return handleFailedLogin(token, e, request, response);
  }

  /**
   * Called only when a failed login occurs
   *
   * @param token
   * @param e
   * @param request
   * @param response
   * @return
   */
  protected abstract boolean handleFailedLogin(
      AuthenticationToken token,
      AuthenticationException e,
      ServletRequest request,
      ServletResponse response);

  /**
   * Called only when a successful login occurs
   *
   * @param token
   * @param subject
   * @param request
   * @param response
   * @return
   * @throws Exception
   */
  protected abstract boolean handleLoginSuccess(
      AuthenticationToken token, Subject subject, ServletRequest request, ServletResponse response)
      throws Exception;

  protected abstract boolean handleExcessiveAttempts(
      ServletRequest request, ServletResponse response, Integer loginAttempts) throws Exception;

  protected abstract boolean handleMultiAccessOnAccountAttempt(
      ServletRequest request, ServletResponse response) throws Exception;

  /**
   * May have similar logic to
   * org.apache.shiro.web.filter.authc.FormAuthenticationFilter.onAccessDenied(ServletRequest,
   * ServletResponse), only that it gives the option to react, either redirect or throw an
   * exception, for example.
   *
   * @param request
   * @param response
   * @return
   */
  protected abstract boolean handleAccessDenied(ServletRequest request, ServletResponse response);

  public Integer getMaxLoginTrialsCount() {
    return maxLoginTrialsCount;
  }

  public void setMaxLoginTrialsCount(Integer maxLoginTrialsCount) {
    this.maxLoginTrialsCount = maxLoginTrialsCount;
  }

  protected String getCcode(ServletRequest request) {
    return ((HttpServletRequest) request)
        .getHeader(CommonConstants.COMPANY_CODE_HTTP_REQUEST_HEADER_ATTRIBUTE);
  }

  protected boolean isPOSTRequest(ServletRequest request, ServletResponse response) {
    return (request instanceof HttpServletRequest)
        && WebUtils.toHttp(request).getMethod().equalsIgnoreCase(POST_METHOD);
  }

  protected boolean isGETRequest(ServletRequest request, ServletResponse response) {
    return (request instanceof HttpServletRequest)
        && WebUtils.toHttp(request).getMethod().equalsIgnoreCase(GET_METHOD);
  }

  public IGlobalUsersHandler getGlobalUserHandler() {
    return globalUserHandler;
  }

  public void setGlobalUserHandler(IGlobalUsersHandler globalUserHandler) {
    this.globalUserHandler = globalUserHandler;
  }

  public int getLockLoginTimeout() {
    return lockLoginTimeout;
  }

  public void setLockLoginTimeout(int lockLoginTimeout) {
    this.lockLoginTimeout = lockLoginTimeout;
  }
}
