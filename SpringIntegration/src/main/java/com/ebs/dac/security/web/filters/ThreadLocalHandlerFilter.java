package com.ebs.dac.security.web.filters;

import com.ebs.dac.common.utils.threads.ThreadLocalUtil;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This filter is responsible to configure a ThreadLocal for every received request.<br>
 * <br>
 * This filter is critical for the successful operation of some further operations in the chain.<br>
 * <br>
 * This filter also, by introducing it in the top of the chain, gives a guaranteed safety margin
 * when using ThreadLocal's inside further chain operations, because it always cleans up the
 * ThreadLocal associated with current thread so that a request's variables don't interfere with
 * other requests, considering we are in a Thread Pool environment.
 *
 * @author Hesham Saleh
 */
public class ThreadLocalHandlerFilter implements Filter {

  public void init(FilterConfig filterConfig) {}

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;

    try {
      chain.doFilter(httpRequest, httpResponse);
    } finally {
      ThreadLocalUtil.destroy();
    }
  }

  public void destroy() {}
}
