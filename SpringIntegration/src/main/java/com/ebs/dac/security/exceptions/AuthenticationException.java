package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;
import java.util.HashMap;
import java.util.Map;

/** Parent class for any authentication exceptions. */
public class AuthenticationException extends org.apache.shiro.authc.AuthenticationException
    implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_MESSAGE = "Couldn't authenticate user";

  /**
   * Map to be used to store some values (e.g. error message), map keys are stored as final fields
   */
  public Map<String, Object> params = new HashMap<String, Object>();

  public String ERROR_CODE = "AUTHC-GNRL_EROR";
  public int STATUS_CODE = HttpStatusCode.UNAUTHORIZED.code();

  public AuthenticationException() {}

  public AuthenticationException(Throwable e) {
    super(DEFAULT_MESSAGE, e);
  }

  public AuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }

  public AuthenticationException(String message) {
    super(message);
  }

  public String getResponseCode() {
    return ERROR_CODE;
  }

  public int getHttpStatusCode() {
    return STATUS_CODE;
  }

  public Map<String, Object> getParams() {
    return params;
  }
}
