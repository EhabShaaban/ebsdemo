/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** @author hesham */
@Entity
@Table(name = "PermissionAssignment")
@NamedQueries({
  @NamedQuery(
      name = "PermissionAssignment.findAll",
      query = "SELECT p FROM PermissionAssignment p"),
  @NamedQuery(
      name = "PermissionAssignment.findById",
      query = "SELECT p FROM PermissionAssignment p WHERE p.id = :id"),
  @NamedQuery(
      name = "PermissionAssignment.findByAssignmentTime",
      query = "SELECT p FROM PermissionAssignment p WHERE p.assignmentTime = :assignmentTime")
})
public class PermissionAssignment implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = false)
  @Column(name = "AssignmentTime")
  @Temporal(TemporalType.TIMESTAMP)
  private Date assignmentTime;

  @JoinColumn(name = "RoleId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private Role roleId;

  @JoinColumn(name = "PermissionId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private Permission permissionId;

  @OneToOne
  @JoinColumn(name = "authorizationconditionid", referencedColumnName = "id")
  private AuthorizationCondition auhtorizationCondition;

  public PermissionAssignment() {}

  public PermissionAssignment(Long id) {
    this.id = id;
  }

  public PermissionAssignment(Long id, Date assignmentTime) {
    this.id = id;
    this.assignmentTime = assignmentTime;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getAssignmentTime() {
    return assignmentTime;
  }

  public void setAssignmentTime(Date assignmentTime) {
    this.assignmentTime = assignmentTime;
  }

  public Role getRoleId() {
    return roleId;
  }

  public void setRoleId(Role roleId) {
    this.roleId = roleId;
  }

  public Permission getPermissionId() {
    return permissionId;
  }

  public void setPermissionId(Permission permissionId) {
    this.permissionId = permissionId;
  }

  public AuthorizationCondition getAuhtorizationCondition() {
    return auhtorizationCondition;
  }

  public void setAuhtorizationCondition(AuthorizationCondition auhtorizationCondition) {
    this.auhtorizationCondition = auhtorizationCondition;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof PermissionAssignment)) {
      return false;
    }
    PermissionAssignment other = (PermissionAssignment) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "PermissionAssignment [id="
        + id
        + ", assignmentTime="
        + assignmentTime
        + ", roleId="
        + roleId
        + ", permissionId="
        + permissionId
        + "]";
  }
}
