package com.ebs.dac.security.shiro.filters.factory;

import com.ebs.dac.security.shiro.web.servlet.BDKShiroHttpServletResponse;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.FilterChainResolver;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;

/**
 * This is mainly a copy of {@link ShiroFilterFactoryBean.SpringShiroFilter}, creating it because it
 * the original was meant to be a quick syntatic sugar and thus it was created as a private static
 * final class, however we do need to customize some behavior.<br>
 * <br>
 * See {@link BDKShiroHttpServletResponse}<br>
 * <br>
 * See also BDK-138<br>
 * <br>
 *
 * @author Hesham Saleh
 */
public class BDKSpringShiroFilter extends AbstractShiroFilter {

  boolean isResponseUrlEncodingEnabled = false;

  /**
   * Should be an exact copy of the constructor of the inner class {@link
   * ShiroFilterFactoryBean.SpringShiroFilter}
   *
   * @param webSecurityManager
   * @param resolver
   */
  public BDKSpringShiroFilter(
      WebSecurityManager webSecurityManager,
      FilterChainResolver resolver,
      boolean isResponseUrlEncodingEnabled) {
    super();
    if (webSecurityManager == null) {
      throw new IllegalArgumentException("WebSecurityManager property cannot be null.");
    }
    setSecurityManager(webSecurityManager);
    if (resolver != null) {
      setFilterChainResolver(resolver);
    }

    this.isResponseUrlEncodingEnabled = isResponseUrlEncodingEnabled;
  }

  @Override
  protected ServletResponse wrapServletResponse(
      HttpServletResponse orig, ShiroHttpServletRequest request) {
    return new BDKShiroHttpServletResponse(
        orig, getServletContext(), request, isResponseUrlEncodingEnabled);
  }
}
