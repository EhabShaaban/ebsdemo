package com.ebs.dac.security.exceptions;

/** The 2nd-Factor credentials (e.g. otp generated code) is expired so need to regenerate it. */
public class ExpiredSecondFactorCredentialException extends SecondFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;
  /** Can be used to display it to user like "You waited more than 'x' seconds so try again" */
  private int timeout;

  {
    ERROR_CODE = "ACESS-2NDF_EXPR";
    // No suitable HttpStatusCode so take the UNAUTHORIZED code with ERROR_CODE as identifier.
  }

  public ExpiredSecondFactorCredentialException() {}

  public ExpiredSecondFactorCredentialException(int timeout) {
    this.timeout = timeout;
  }

  @Override
  public String toString() {
    return "The generated credentials are expired as '"
        + timeout
        + "' passed since its generation.";
  }
}
