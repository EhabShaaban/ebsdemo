package com.ebs.dac.security.shiro.filters.twofactorauth.models;

import org.apache.shiro.authc.AuthenticationInfo;

/**
 * The authentication info that will be valid for only one time usage. When it is used the {@link
 * #isUsed} will be set to true.
 */
public class OneTimerAuthenticationFactorInfo extends AuthenticationFactorInfo {

  protected boolean isUsed = false;

  public OneTimerAuthenticationFactorInfo(AuthenticationInfo info, String factorName) {
    super(info, factorName);
  }

  public void setUsed(boolean isUsed) {
    this.isUsed = isUsed;
  }

  @Override
  public boolean isValid() {
    return !isUsed;
  }
}
