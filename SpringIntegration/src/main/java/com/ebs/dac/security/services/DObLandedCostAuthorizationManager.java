package com.ebs.dac.security.services;


import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import org.springframework.beans.factory.annotation.Autowired;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostActionNames;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostCreationValueObject;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.ICObLandedCostType;
import com.ebs.dda.jpa.accounting.landedcost.IDObLandedCost;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;

public class DObLandedCostAuthorizationManager {
  private AuthorizationManager authorizationManager;

  @Autowired
  private DObPurchaseOrderGeneralModelRep purchaseOrderGMRep;

  public DObLandedCostAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
  }

  public void authorizeActionOnSection(BusinessObject businessObject, String actionName)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void authorizeReadAction(String sysName, String actionName) {
    this.authorizationManager.authorizeAction(sysName, actionName);
  }

  public AuthorizationManager getAuthorizationManager() {
    return authorizationManager;
  }

  public void applyAuthorizationForCreation(DObLandedCostCreationValueObject valueObject)
      throws Exception {

    authorizationManager.authorizeAction(IDObLandedCost.SYS_NAME, IActionsNames.CREATE);
    authorizationManager.authorizeAction(ICObLandedCostType.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL);
    checkPurchaseOrderAuthorization(valueObject);
  }

  private void checkPurchaseOrderAuthorization(DObLandedCostCreationValueObject valueObject)
      throws Exception {
    if (valueObject.getLandedCostType().equals(ICObLandedCostType.IMPORT)) {
      authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);
    }
    if (valueObject.getLandedCostType().equals(ICObLandedCostType.LOCAL)) {
      authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);
    }

    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderGMRep.findOneByUserCode(valueObject.getPurchaseOrderCode());
    if (purchaseOrder != null) {
      try {
        authorizationManager.authorizeAction(purchaseOrder, IActionsNames.READ_ALL);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  public void authorizeConvertToEstimateAction(DObLandedCostGeneralModel landedCost)
      throws Exception {
    try {
      authorizationManager.authorizeAction(landedCost,
          IDObLandedCostActionNames.CONVERT_TO_ESTIMATE);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  public void authorizeConvertToActualAction(DObLandedCostGeneralModel landedCost)
      throws Exception {
    try {
      authorizationManager.authorizeAction(landedCost,
          IDObLandedCostActionNames.CONVERT_TO_ACTUAL);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  public void authorizeUpdateItem(DObLandedCostGeneralModel landedCost)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(landedCost,
          IDObLandedCostActionNames.UPDATE_COST_FACTOR_ITEMS);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }

  public void applyAuthorizatioinOnReads(DObLandedCostGeneralModel landedCost) {
    try {
      authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }
}
