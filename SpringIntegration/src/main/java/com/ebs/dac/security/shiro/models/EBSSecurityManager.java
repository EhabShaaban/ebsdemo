package com.ebs.dac.security.shiro.models;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 20, 2018 9:36:30 AM
 */
public class EBSSecurityManager extends DefaultWebSecurityManager {

  public EBSSecurityManager() {
    super();
    setSubjectFactory(new EBSSubjectFactory());
  }

  public void checkPermission(
      PrincipalCollection principals, String permission, BusinessObject entity) throws Exception {
    ((EBSModularRealmAuthorizer) getAuthorizer()).checkPermission(principals, permission, entity);
  }
}
