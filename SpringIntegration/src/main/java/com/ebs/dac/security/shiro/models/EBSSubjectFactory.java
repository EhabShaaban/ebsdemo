package com.ebs.dac.security.shiro.models;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.subject.SubjectContext;
import org.apache.shiro.web.mgt.DefaultWebSubjectFactory;
import org.apache.shiro.web.subject.WebSubjectContext;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 20, 2018 12:21:23 PM
 */
public class EBSSubjectFactory extends DefaultWebSubjectFactory {

  public Subject createSubject(SubjectContext context) {
    if (!(context instanceof WebSubjectContext)) {
      return super.createSubject(context);
    }
    WebSubjectContext wsc = (WebSubjectContext) context;
    SecurityManager securityManager = wsc.resolveSecurityManager();
    Session session = wsc.resolveSession();
    boolean sessionEnabled = wsc.isSessionCreationEnabled();
    PrincipalCollection principals = wsc.resolvePrincipals();
    boolean authenticated = wsc.resolveAuthenticated();
    String host = wsc.resolveHost();
    ServletRequest request = wsc.resolveServletRequest();
    ServletResponse response = wsc.resolveServletResponse();

    return new EBSSubject(
        principals,
        authenticated,
        host,
        session,
        sessionEnabled,
        request,
        response,
        securityManager);
  }
}
