package com.ebs.dac.security.exceptions;

import com.ebs.dac.infrastructure.exception.IExceptionResponse;

/** Parent class for any authentication exceptions. */
public class AuthorizationException extends org.apache.shiro.authz.AuthorizationException
    implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  public AuthorizationException() {
    super();
  }

  public AuthorizationException(Exception e) {
    this();
    initCause(e);
  }
  public AuthorizationException(String msg) {
    super(msg);
  }

  public String getResponseCode() {
    return "";
  }

  public int getHttpStatusCode() {
    return IExceptionResponse.SECURITY_VIOLATION_HTTP_STATUS_CODE;
  }
}
