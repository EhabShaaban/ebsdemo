package com.ebs.dac.security.jpa.valueobjects;

public interface IUserPasswordValueObject {

  String CURRENT_PASSWORD = "currentPassword";
  String NEW_PASSWORD = "newPassword";
  String CONFIRMED_NEW_PASSWORD = "confirmedNewPassword";
}
