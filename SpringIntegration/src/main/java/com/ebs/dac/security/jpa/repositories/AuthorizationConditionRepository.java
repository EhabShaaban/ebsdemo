package com.ebs.dac.security.jpa.repositories;

import com.ebs.dac.security.jpa.entities.AuthorizationCondition;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface AuthorizationConditionRepository
    extends CrudRepository<AuthorizationCondition, Long> {

  List<AuthorizationCondition> findAllByObjectName(String objectName);
}
