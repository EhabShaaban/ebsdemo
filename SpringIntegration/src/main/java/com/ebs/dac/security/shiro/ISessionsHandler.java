package com.ebs.dac.security.shiro;

import java.io.Serializable;
import java.util.Collection;
import org.apache.shiro.session.Session;

public interface ISessionsHandler {

  void saveSession(Serializable id, Session sesion);

  Session doReadSession(Serializable sessionId);

  void delete(Session session);

  Collection<Session> getActiveSessions();
}
