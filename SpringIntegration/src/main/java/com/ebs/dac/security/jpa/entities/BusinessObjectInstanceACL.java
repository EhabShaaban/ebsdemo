/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/** @author hesham */
@Entity
@Table(name = "BusinessObjectInstanceACL")
@NamedQueries({
  @NamedQuery(
      name = "BusinessObjectInstanceACL.findAll",
      query = "SELECT b FROM BusinessObjectInstanceACL b"),
  @NamedQuery(
      name = "BusinessObjectInstanceACL.findById",
      query = "SELECT b FROM BusinessObjectInstanceACL b WHERE b.id = :id"),
  @NamedQuery(
      name = "BusinessObjectInstanceACL.findByBusinessObjectKey",
      query =
          "SELECT b FROM BusinessObjectInstanceACL b WHERE b.businessObjectKey = :businessObjectKey"),
  @NamedQuery(
      name = "BusinessObjectInstanceACL.findByInstanceKey",
      query = "SELECT b FROM BusinessObjectInstanceACL b WHERE b.instanceKey = :instanceKey")
})
public class BusinessObjectInstanceACL implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = false)
  @Column(name = "BusinessObjectKey")
  private String businessObjectKey;

  @Column(name = "InstanceKey")
  private String instanceKey;

  @JoinColumn(name = "UserAccountId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private UserAccount userAccountId;

  public BusinessObjectInstanceACL() {}

  public BusinessObjectInstanceACL(Long id) {
    this.id = id;
  }

  public BusinessObjectInstanceACL(Long id, String businessObjectKey) {
    this.id = id;
    this.businessObjectKey = businessObjectKey;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBusinessObjectKey() {
    return businessObjectKey;
  }

  public void setBusinessObjectKey(String businessObjectKey) {
    this.businessObjectKey = businessObjectKey;
  }

  public String getInstanceKey() {
    return instanceKey;
  }

  public void setInstanceKey(String instanceKey) {
    this.instanceKey = instanceKey;
  }

  public UserAccount getUserAccountId() {
    return userAccountId;
  }

  public void setUserAccountId(UserAccount userAccountId) {
    this.userAccountId = userAccountId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof BusinessObjectInstanceACL)) {
      return false;
    }
    BusinessObjectInstanceACL other = (BusinessObjectInstanceACL) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "BusinessObjectInstanceACL [id="
        + id
        + ", businessObjectKey="
        + businessObjectKey
        + ", instanceKey="
        + instanceKey
        + ", userAccountId="
        + userAccountId
        + "]";
  }
}
