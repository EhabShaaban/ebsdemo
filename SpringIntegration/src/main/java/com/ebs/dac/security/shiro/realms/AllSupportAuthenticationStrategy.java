package com.ebs.dac.security.shiro.realms;

import com.ebs.dac.security.exceptions.AuthenticationException;
import com.ebs.dac.security.exceptions.NoUserAccountFoundException;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.pam.AbstractAuthenticationStrategy;
import org.apache.shiro.authc.pam.AllSuccessfulStrategy;
import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.realm.Realm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Authentication strategy defines how the authentication process will react in case that multiple
 * realms exist. In this strategy ALL realms THAT SUPPORT the passed token should return {@link
 * AuthenticationInfo}. So it is a mix between {@link AllSuccessfulStrategy} (but restricts those
 * who support token) and {@link AtLeastOneSuccessfulStrategy} (but any exception from a realm that
 * supports the passed token) will be propagated rather than swallowing it.
 */
public class AllSupportAuthenticationStrategy extends AbstractAuthenticationStrategy {

  private static final Logger log = LoggerFactory.getLogger(AllSupportAuthenticationStrategy.class);

  /**
   * Merges the specified <code>info</code> into the <code>aggregate</code> argument and returns it
   * (just as the parent implementation does), but additionally ensures the following:
   *
   * <ol>
   *   <li>if the <code>Throwable</code> argument is not <code>null</code>, re-throws it to
   *       immediately cancel the authentication process, since this strategy requires all realms to
   *       authenticate successfully.
   *   <li>neither the <code>info</code> or <code>aggregate</code> argument is <code>null</code> to
   *       ensure that each realm did in fact authenticate successfully
   * </ol>
   */
  public AuthenticationInfo afterAttempt(
      Realm realm,
      AuthenticationToken token,
      AuthenticationInfo info,
      AuthenticationInfo aggregate,
      Throwable t)
      throws AuthenticationException {
    if (t != null) {
      if (t instanceof AuthenticationException) {
        // propagate:
        throw ((AuthenticationException) t);
      } else {

        throw new AuthenticationException(t);
      }
    }
    if (info == null) {
      EBSPrincipal principal = (EBSPrincipal) token.getPrincipal();

      if (principal != null) {
        String username = principal.getUsername();
        String companyCode = principal.getCompany();
        throw new NoUserAccountFoundException(username, companyCode);
      }
      // May never be reached
      throw new NoUserAccountFoundException();
    }

    log.debug("Account successfully authenticated using realm [{}]", realm);

    // If non-null account is returned, then the realm was able to authenticate the
    // user - so merge the account with any accumulated before:
    merge(info, aggregate);

    return aggregate;
  }
}
