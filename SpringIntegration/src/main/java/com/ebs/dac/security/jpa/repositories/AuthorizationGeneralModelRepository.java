package com.ebs.dac.security.jpa.repositories;

import com.ebs.dac.security.jpa.entities.AuthorizationGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface AuthorizationGeneralModelRepository
    extends CrudRepository<AuthorizationGeneralModel, Long> {

  @Query(
      "SELECT e FROM AuthorizationGeneralModel e WHERE e.userName = :username AND e.permissionExpression IN :expressions")
  List<AuthorizationGeneralModel> findByUserNameAndPermissionExpressionIn(
      @Param("username") String username, @Param("expressions") List<String> permissionExpression);
}
