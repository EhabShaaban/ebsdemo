package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** Trying to access a 1st-Factor protected resource with no authentication info. */
public class UnauthenticatedForFirstFactorException extends FirstFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_MESSAGE = "Unauthenticated access to restricted resource.";

  {
    ERROR_CODE = "ACESS-1STF_EROR";
    STATUS_CODE = HttpStatusCode.UNAUTHORIZED.code();
  }

  public UnauthenticatedForFirstFactorException() {
    this(DEFAULT_MESSAGE);
  }

  public UnauthenticatedForFirstFactorException(Exception e) {
    super(DEFAULT_MESSAGE, e);
  }

  public UnauthenticatedForFirstFactorException(String message, Exception cause) {
    super(message, cause);
  }

  public UnauthenticatedForFirstFactorException(String message) {
    super(message);
  }
}
