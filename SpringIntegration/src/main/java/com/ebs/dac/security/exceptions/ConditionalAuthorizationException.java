package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

/**
 * @author Ahmed Ali Elfeky
 * @since Jul 22, 2018 4:46:46 PM
 */
public class ConditionalAuthorizationException extends org.apache.shiro.authz.AuthorizationException
    implements IExceptionResponse {

  private static final long serialVersionUID = 1L;
  private static String RESPONSE_MSG_CODE = IExceptionsCodes.General_CONDITIONAL_AUTHORIZATION_CODE;

  public ConditionalAuthorizationException(String msg) {
    super(msg);
  }

  public String getResponseCode() {
    return RESPONSE_MSG_CODE;
  }

  public int getHttpStatusCode() {
    return HttpStatusCode.FORBIDDEN.code();
  }
}
