package com.ebs.dac.security.exceptions;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;

public class ResourceAccessException extends Exception implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  @Override
  public String getResponseCode() {
    return IExceptionsCodes.AUTHORIZATION_EXCEPTION;
  }
}
