package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** When there is no account for the passed username and company. */
public class NoUserAccountFoundException extends FirstFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_MESSAGE =
      "No user account could be found for the supplied user name";

  private String username;
  private String companyCode;

  {
    ERROR_CODE = "LOGIN-UACC_NFND";
    STATUS_CODE = HttpStatusCode.UNAUTHORIZED.code();
  }

  public NoUserAccountFoundException() {
    super(DEFAULT_MESSAGE);
  }

  public NoUserAccountFoundException(String username, String companyCode) {
    this();
    this.username = username;
    this.companyCode = companyCode;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String toString() {
    return "No account for username [" + username + "] and company [" + companyCode + "]";
  }
}
