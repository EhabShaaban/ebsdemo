package com.ebs.dac.security.shiro.annotations;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.shiro.models.EBSSubject;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.aop.PermissionAnnotationHandler;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 18, 2018 1:21:54 PM
 */
public class EBSAuthorizationHandler extends PermissionAnnotationHandler {

  public void assertAuthorized(RequiresPermissions permissions, BusinessObject entity)
      throws Exception {
    if (entity == null) {
      super.assertAuthorized(permissions);
      return;
    }
    String[] perms = getAnnotationValue(permissions);
    EBSSubject subject = (EBSSubject) getSubject();
    if (perms.length == 1) {
      subject.checkPermission(perms[0], entity);
      return;
    }
  }

  public void authorizeAction(RequiresPermissions permissions) throws Exception {
    checkAuthorization(null, permissions);
  }

  public void authorizeAction(BusinessObject entity, RequiresPermissions permission)
      throws Exception {
    assertAuthorized(permission, entity);
  }

  private void checkAuthorization(BusinessObject entity, RequiresPermissions permissions)
      throws Exception {
    try {
      if (permissions == null) {
        return;
      }
      assertAuthorized(permissions, entity);
    } catch (ConditionalAuthorizationException e) {
      throw e;
    } catch (AuthorizationException e) {
      throw new com.ebs.dac.security.exceptions.AuthorizationException(e.getMessage());
    }
  }
}
