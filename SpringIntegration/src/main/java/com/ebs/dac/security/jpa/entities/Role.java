/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** @author hesham */
@Entity
@Table(name = "Role")
@NamedQueries({
  @NamedQuery(name = "Role.findAll", query = "SELECT r FROM Role r"),
  @NamedQuery(name = "Role.findById", query = "SELECT r FROM Role r WHERE r.id = :id"),
  @NamedQuery(
      name = "Role.findByRoleExpression",
      query = "SELECT r FROM Role r WHERE r.roleExpression = :roleExpression"),
  @NamedQuery(
      name = "Role.findByDescription",
      query = "SELECT r FROM Role r WHERE r.description = :description")
})
public class Role implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = false)
  @Column(name = "RoleExpression")
  private String roleExpression;

  @Column(name = "Description")
  private String description;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId")
  private List<UserAssignment> userAssignmentList;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId")
  private List<RoleAssignment> childrenRoles;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "parentRoleId")
  private List<RoleAssignment> parentRoles;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId")
  private List<PermissionAssignment> permissionAssignmentList;

  public Role() {}

  public Role(Long id) {
    this.id = id;
  }

  public Role(Long id, String roleExpression) {
    this.id = id;
    this.roleExpression = roleExpression;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRoleExpression() {
    return roleExpression;
  }

  public void setRoleExpression(String roleExpression) {
    this.roleExpression = roleExpression;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<UserAssignment> getUserAssignmentList() {
    return userAssignmentList;
  }

  public void setUserAssignmentList(List<UserAssignment> userAssignmentList) {
    this.userAssignmentList = userAssignmentList;
  }

  public List<RoleAssignment> getChildrenRoles() {
    return childrenRoles;
  }

  public void setChildrenRoles(List<RoleAssignment> childrenRoles) {
    this.childrenRoles = childrenRoles;
  }

  public List<RoleAssignment> getParentRoles() {
    return parentRoles;
  }

  public void setParentRoles(List<RoleAssignment> parentRoles) {
    this.parentRoles = parentRoles;
  }

  public List<PermissionAssignment> getPermissionAssignmentList() {
    return permissionAssignmentList;
  }

  public void setPermissionAssignmentList(List<PermissionAssignment> permissionAssignmentList) {
    this.permissionAssignmentList = permissionAssignmentList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Role)) {
      return false;
    }
    Role other = (Role) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "Role [id="
        + id
        + ", roleExpression="
        + roleExpression
        + ", description="
        + description
        + ", userAssignmentList="
        + userAssignmentList
        + ", parentRoleList="
        + childrenRoles
        + ", parentRoleList1="
        + parentRoles
        + ", permissionAssignmentList="
        + permissionAssignmentList
        + "]";
  }
}
