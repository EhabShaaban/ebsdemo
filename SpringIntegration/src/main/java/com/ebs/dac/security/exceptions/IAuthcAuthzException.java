package com.ebs.dac.security.exceptions;

public interface IAuthcAuthzException extends IEBSException {

  int getHttpStatusCode();
}
