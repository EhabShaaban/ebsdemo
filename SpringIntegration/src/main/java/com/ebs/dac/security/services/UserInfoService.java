package com.ebs.dac.security.services;

import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.EBSUserInfo;

public class UserInfoService {

  public Object getUserInfo(EBSPrincipal principal) {
    EBSUserInfo userInfo = new EBSUserInfo();
    userInfo.setUsername(principal.getUsername());
    userInfo.setSysPasswordFlag(principal.getSysPasswordFlag());

    return userInfo;
  }
}
