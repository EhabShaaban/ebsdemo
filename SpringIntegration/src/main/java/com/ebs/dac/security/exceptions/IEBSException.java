package com.ebs.dac.security.exceptions;

import java.util.Map;

public interface IEBSException {

  /*
   * Keys to be used in Params
   */
  String ERROR_MESSAGE_KEY = "ERROR_MESSAGE";

  String getErrorCode();

  Map<String, Object> getParams();
}
