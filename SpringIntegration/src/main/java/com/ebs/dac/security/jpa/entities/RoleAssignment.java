/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/** @author hesham */
@Entity
@Table(name = "RoleAssignment")
public class RoleAssignment implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "Id")
  private Long id;

  @JoinColumn(name = "RoleId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private Role roleId;

  @JoinColumn(name = "ParentRoleId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private Role parentRoleId;

  public RoleAssignment() {}

  public RoleAssignment(Long id) {
    this.id = id;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Role getRoleId() {
    return roleId;
  }

  public void setRoleId(Role roleId) {
    this.roleId = roleId;
  }

  public Role getParentRoleId() {
    return parentRoleId;
  }

  public void setParentRoleId(Role parentRoleId) {
    this.parentRoleId = parentRoleId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof RoleAssignment)) {
      return false;
    }
    RoleAssignment other = (RoleAssignment) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "ParentRole [id=" + id + ", roleId=" + roleId + ", parentRoleId=" + parentRoleId + "]";
  }
}
