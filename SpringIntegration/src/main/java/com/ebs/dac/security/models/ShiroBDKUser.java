package com.ebs.dac.security.models;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import org.apache.shiro.authc.AuthenticationListener;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom {@link IBDKUser} implementation that is mainly used to actually represent the currently
 * executing user, but not necessary, as it depends ultimately on a proper Shiro's {@link Subject}
 * instance.<br>
 * <br>
 * {@link AuthenticationListener}
 *
 * @author Hesham Saleh
 */
public class ShiroBDKUser extends BDKUser {

  Subject shiroSubject;

  Logger logger = LoggerFactory.getLogger(ShiroBDKUser.class);

  public ShiroBDKUser(Subject shiroSubject) {
    this.shiroSubject = shiroSubject;

    try {
      setUsername(((EBSPrincipal) shiroSubject.getPrincipal()).getUsername());
      setUserId(((EBSPrincipal) shiroSubject.getPrincipal()).getUserId());
      setSysPasswordFlag((((EBSPrincipal) shiroSubject.getPrincipal()).getSysPasswordFlag()));
    } catch (Exception e) {
      logger.error("Couldn't retrieve username from supplied " + shiroSubject + " instance.", e);
    }

    try {
      setCompany(((EBSPrincipal) shiroSubject.getPrincipal()).getCompany());
    } catch (Exception e) {
      logger.error(
          "Couldn't retrieve Company Name from supplied " + shiroSubject + " instance.", e);
    }
  }

  public Subject getSubject() {
    return shiroSubject;
  }

  @Override
  public String toString() {
    if (shiroSubject == null) {
      return "shiroSubject is null, couldn't toString()";
    }
    return shiroSubject.getPrincipal().toString();
  }
}
