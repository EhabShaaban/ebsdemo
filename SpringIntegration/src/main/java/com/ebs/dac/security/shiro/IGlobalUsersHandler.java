package com.ebs.dac.security.shiro;

import java.util.Set;
import org.apache.shiro.subject.Subject;

/** Handler to be able to add/remove users to/from logged-in users list, logout any user. */
public interface IGlobalUsersHandler {

  void addUserToLoggedInUsers(CredentialSubject subject);

  void addUserToLoggedInUsers(Subject subject);

  void removeUserFromLoggedInUsers(CredentialSubject subject);

  void removeUserFromLoggedInUsers(Subject subject);

  void removeUserFromLoggedInUsers(String username);

  void logoutUser(CredentialSubject subject);

  void logoutUser(Subject subject);

  void logoutUser(String username);

  Set<CredentialSubject> getLoggedInUsers();

  void setLoggedInUsers(Set<CredentialSubject> loggedInUsers);
}
