package com.ebs.dac.security.shiro.models;

public class EBSUserInfo {

  private String username;
  private Boolean isSysPasswordFlag;

  public void setUsername(String username) {
    this.username = username;
  }

  public void setSysPasswordFlag(Boolean sysPasswordFlag) {
    isSysPasswordFlag = sysPasswordFlag;
  }
}
