package com.ebs.dac.security.enums;

public class SecurityEntitiesState {
  /** The String that represents an active state (user account, account configuration ...) */
  public static final String ACTIVE_STATE = "ACTIVE"; // TODO Handle this better

  /** The String that represents a blocked state (user account, account configuration ...) */
  public static final String BLOCKED_STATE = "BLOCKED"; // TODO Handle this better

  public static final boolean isActiveState(String state) {
    return ACTIVE_STATE.equalsIgnoreCase(state);
  }
}
