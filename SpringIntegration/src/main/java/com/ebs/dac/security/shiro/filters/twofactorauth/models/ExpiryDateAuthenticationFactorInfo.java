package com.ebs.dac.security.shiro.filters.twofactorauth.models;

import org.apache.shiro.authc.AuthenticationInfo;
import org.joda.time.DateTime;

/** The 2nd-Factor authentication info that will be valid only till {@link #expiryDate}. */
public class ExpiryDateAuthenticationFactorInfo extends AuthenticationFactorInfo {

  private DateTime expiryDate;

  /**
   * Defaults the Expiry Date to after a month
   *
   * @param info
   * @param factorName
   */
  public ExpiryDateAuthenticationFactorInfo(AuthenticationInfo info, String factorName) {
    super(info, factorName);
    this.expiryDate = DateTime.now().plusMonths(1);
  }

  public ExpiryDateAuthenticationFactorInfo(
      AuthenticationInfo info, String factorName, DateTime expiryDate) {
    super(info, factorName);
    this.expiryDate = expiryDate;
  }

  @Override
  public boolean isValid() {
    return expiryDate.isAfterNow();
  }
}
