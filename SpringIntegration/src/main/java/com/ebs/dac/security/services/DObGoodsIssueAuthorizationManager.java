package com.ebs.dac.security.services;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueCreateValueObject;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

public class DObGoodsIssueAuthorizationManager {

  private AuthorizationManager authorizationManager;
  private AuthorizationCommonUtils authorizationCommonUtils;
  private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private final String GOODS_ISSUE_SO_TYPE = "BASED_ON_SALES_ORDER";

  public DObGoodsIssueAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
    authorizationCommonUtils = new AuthorizationCommonUtils(authorizationManager);
  }

  public void authorizeActionOnSection(BusinessObject businessObject, String actionName)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void checkCreateValueObjectValuesAuthorization(DObGoodsIssueCreateValueObject goodsIssueCreateValueObject) throws Exception {
    authorizationCommonUtils.applyAuthorizationForPurchaseUnit(goodsIssueCreateValueObject.getPurchaseUnitCode());
    applyAuthorizationForRefDocument(goodsIssueCreateValueObject.getTypeCode(), goodsIssueCreateValueObject.getRefDocumentCode());
  }

  private void applyAuthorizationForRefDocument(String typeCode, String refDocumentCode) throws Exception {
    if (typeCode.equals(GOODS_ISSUE_SO_TYPE)){
      DObSalesOrderGeneralModel salesOrderGeneralModel = salesOrderGeneralModelRep.findOneByUserCode(refDocumentCode);
        try {
          authorizationCommonUtils.checkIfEntityIsNull(salesOrderGeneralModel);
          authorizationManager.authorizeAction(salesOrderGeneralModel,
                  IActionsNames.READ_ALL);
        } catch (ConditionalAuthorizationException | InstanceNotExistException exception) {
          throw new AuthorizationException();
        }
    }
  }

  public void setcObPurchasingUnitGeneralModelRep(
          CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep) {
    this.cObPurchasingUnitGeneralModelRep = cObPurchasingUnitGeneralModelRep;
    authorizationCommonUtils.setcObPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

}
