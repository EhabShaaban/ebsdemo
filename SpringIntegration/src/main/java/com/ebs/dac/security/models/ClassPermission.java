package com.ebs.dac.security.models;

import org.apache.shiro.authz.annotation.RequiresPermissions;

public class ClassPermission {
  private String key;
  private RequiresPermissions permission;

  public ClassPermission() {}

  public ClassPermission(String key, RequiresPermissions permission) {
    this.key = key;
    this.permission = permission;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public RequiresPermissions getPermission() {
    return permission;
  }

  public void setPermission(RequiresPermissions permission) {
    this.permission = permission;
  }

  @Override
  public String toString() {
    return "Key: " + getKey();
  }
}
