package com.ebs.dac.security.shiro.session;

import com.ebs.dac.security.shiro.filters.ExcessiveAttemptsAwareFormAuthenticationFilter;
import com.ebs.dac.security.shiro.filters.twofactorauth.SecondFactorAuthenticationDispatcherFilter;
import com.ebs.dac.security.shiro.listeners.UserEntityAuthenticationListener;
import org.joda.time.DateTime;

/** Contains all keys that can be stored in user sessions. */
public class SessionAttributeKeys {

  /** Value Type : String */
  public static final String USERNAME = "USERNAME";

  /**
   * Value Type : Integer <br>
   * Can be used in {@link ExcessiveAttemptsAwareFormAuthenticationFilter}
   */
  public static final String LOGIN_ATTEMPTS = "LOGIN_ATTEMPTS";

  /**
   * Value Type :
   * com.ebs.dac.security.shiro.filters.twofactorauth.AuthenticationFactorVerifiedToken.AuthenticationFactorToken(AuthenticationToken)
   * <br>
   * Can be used in {@link
   * 'SecondFactorAuthenticationDispatcherFilter#assertSessionContainValidSecondFactorCredentials(Session)'}
   * and in {@link
   * UserEntityAuthenticationListener#onSuccess(org.apache.shiro.authc.AuthenticationToken,
   * org.apache.shiro.authc.AuthenticationInfo)}
   */
  public static final String SECOND_FACTOR_CREDENTIAL_VERIFIED_TOKEN =
      "SECOND_FACTOR_CREDENTIAL_VERIFIED_TOKEN";

  /**
   * Value Type : SecondFactorAuthAttempts <br>
   * Can be used in {@link SecondFactorAuthenticationDispatcherFilter} and in {@link
   * UserEntityAuthenticationListener}
   */
  public static final String TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO =
      "TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO";

  /**
   * Value Type: {@link DateTime} Used to prevent access for some time after the maximum nuber of
   * wrong login attempts is reached.
   */
  public static final String MAX_LOGIN_ATTEMPTS_REACHED_TIME = "LOGIN_ATTEMPTS_REACHED_TIME";
}
