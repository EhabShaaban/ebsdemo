package com.ebs.dac.security.services;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.security.models.ShiroBDKUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

public class UserAccountSecurityService implements IUserAccountSecurityService {

  public UserAccountSecurityService() {}

  public IBDKUser getLoggedInUser() {
    Subject subject = SecurityUtils.getSubject();
    if (subject.isRemembered() == false && subject.isAuthenticated() == false) {
      return null;
    }
    return new ShiroBDKUser(subject);
  }
}
