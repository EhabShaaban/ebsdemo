/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** @author hesham */
@Entity
@Table(name = "Authenticator")
@NamedQueries({
  @NamedQuery(name = "Authenticator.findAll", query = "SELECT a FROM Authenticator a"),
  @NamedQuery(
      name = "Authenticator.findById",
      query = "SELECT a FROM Authenticator a WHERE a.id = :id"),
  @NamedQuery(
      name = "Authenticator.findByAuthenticationStrategy",
      query =
          "SELECT a FROM Authenticator a WHERE a.authenticationStrategy = :authenticationStrategy"),
  @NamedQuery(
      name = "Authenticator.findByAuthenticationProvider",
      query =
          "SELECT a FROM Authenticator a WHERE a.authenticationProvider = :authenticationProvider")
})
public class Authenticator implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = false)
  @Column(name = "AuthenticationStrategy")
  private String authenticationStrategy;

  @Basic(optional = false)
  @Column(name = "AuthenticationProvider")
  private String authenticationProvider;

  @OneToMany(mappedBy = "secondAuthenticator")
  private List<UserAccount> userAccountList;

  public Authenticator() {}

  public Authenticator(Long id) {
    this.id = id;
  }

  public Authenticator(Long id, String authenticationStrategy, String authenticationProvider) {
    this.id = id;
    this.authenticationStrategy = authenticationStrategy;
    this.authenticationProvider = authenticationProvider;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAuthenticationStrategy() {
    return authenticationStrategy;
  }

  public void setAuthenticationStrategy(String authenticationStrategy) {
    this.authenticationStrategy = authenticationStrategy;
  }

  public String getAuthenticationProvider() {
    return authenticationProvider;
  }

  public void setAuthenticationProvider(String authenticationProvider) {
    this.authenticationProvider = authenticationProvider;
  }

  public List<UserAccount> getUserAccountList() {
    return userAccountList;
  }

  public void setUserAccountList(List<UserAccount> userAccountList) {
    this.userAccountList = userAccountList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Authenticator)) {
      return false;
    }
    Authenticator other = (Authenticator) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "Authenticator [id="
        + id
        + ", authenticationStrategy="
        + authenticationStrategy
        + ", authenticationProvider="
        + authenticationProvider
        + ", userAccountList="
        + userAccountList
        + "]";
  }
}
