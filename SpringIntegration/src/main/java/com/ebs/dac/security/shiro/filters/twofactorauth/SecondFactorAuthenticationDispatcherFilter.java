package com.ebs.dac.security.shiro.filters.twofactorauth;

import com.ebs.dac.security.URLManager;
import com.ebs.dac.security.exceptions.ExpiredSecondFactorCredentialException;
import com.ebs.dac.security.exceptions.NoSecondFactorConfiguredException;
import com.ebs.dac.security.exceptions.SecondFactorAuthenticationException;
import com.ebs.dac.security.exceptions.UnExpectedSecondFactorException;
import com.ebs.dac.security.exceptions.UnauthenticatedForFirstFactorException;
import com.ebs.dac.security.exceptions.UnauthenticatedForSecondFactorException;
import com.ebs.dac.security.shiro.filters.twofactorauth.models.AuthenticationFactorInfo;
import com.ebs.dac.security.shiro.filters.twofactorauth.models.OneTimerAuthenticationFactorInfo;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.ebs.dac.security.shiro.session.SessionAttributeKeys;
import com.ebs.dac.security.shiro.twofactorauth.GenerateCredentialInfo;
import com.ebs.dac.security.shiro.twofactorauth.SecondFactorAuthAttempts;
import com.ebs.dac.security.shiro.twofactorauth.SecondFactorAuthenticator;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Checks if any resource url (as they are all protected with 2nd-Factor) is protected using
 * 2nd-Factor or not (This happens when user {@link SecurityConfiguration.SecurityClearnceLevel}
 * equals {@link SecurityConfiguration.SecurityClearnceLevel#STANDARD} and the url is in
 * second-factor-protected-urls.properties file)
 */
public class SecondFactorAuthenticationDispatcherFilter extends PathMatchingFilter {

  public static final String DEFAULT_USERNAME_PARAM = "username";
  public static final String DEFAULT_CREDENTIAL_PARAM = "code";
  public static final String DEFAULT_INVALIDATE_GENERATED_CREDENTIAL_PARAM =
      "invalidateGeneratedCredential";
  public static final String SKIP_REQUEST_SAVING_QUERY_PARAM = "skipRequestSaving";
  private static final Logger logger =
      LoggerFactory.getLogger(SecondFactorAuthenticationDispatcherFilter.class);
  /** This property is set from shiro-security.xml */
  protected String noSecondAuthFactorConfiguredErrorRedirectUrl;
  protected String contactAdministratorRedirectUrl;
  /** This property is set from shiro-security.xml */
  protected String generatedCredentialsBecameInvalidRedirectUrl;
  /** This property is set from shiro-security.xml */
  protected String unauthorizedRedirectUrl;
  /** This property is set from shiro-security.xml */
  protected Map<String, SecondFactorAuthenticator> supportedFilters;
  /** This property is set from shiro-security.xml */
  private URLManager urlManager;

  protected static boolean assertSessionContainValidSecondFactorCredentials(Session session) {
    logger.debug(
        "Trying to see if a previously verified second factor credential can be remembered");

    AuthenticationFactorInfo verifiedToken =
        (AuthenticationFactorInfo)
            session.getAttribute(SessionAttributeKeys.SECOND_FACTOR_CREDENTIAL_VERIFIED_TOKEN);
    if (verifiedToken != null && verifiedToken.isValid()) {
      logger.debug("Found a second factor valid credentials stored in the session.");
      if (verifiedToken instanceof OneTimerAuthenticationFactorInfo) {
        logger.debug(
            "And it happens to be a single-use credentials, marking it as used to prevent further requests using it.");
        ((OneTimerAuthenticationFactorInfo) verifiedToken).setUsed(true);
      }
      return true;
    }

    logger.debug("No previously verified second factor credential could be remembered");
    return false;
  }

  protected static boolean isPOSTRequest(ServletRequest request) {
    return (request instanceof HttpServletRequest)
        && WebUtils.toHttp(request).getMethod().equalsIgnoreCase("POST");
  }

  protected static boolean isGETRequest(ServletRequest request) {
    return (request instanceof HttpServletRequest)
        && WebUtils.toHttp(request).getMethod().equalsIgnoreCase("GET");
  }

  public SecurityConfiguration getCurrentSubjectSecurityConfiguration() {
    return ((EBSPrincipal) SecurityUtils.getSubject().getPrincipal()).getPrimaryConfiguration();
  }

  /**
   * Returns true if:<br>
   * - User clearance level is {@link SecurityConfiguration.SecurityClearnceLevel#LEAST_RESTRICTIVE}
   * so no 2nd-Factor authentication is required for this user - User clearance level is {@link
   * SecurityConfiguration.SecurityClearnceLevel#STANDARD} and the requested url is NOT 2nd-Factor
   * protected (NOT in second-factor-protected-urls.properties file) - (User clearance level is
   * {@link SecurityConfiguration.SecurityClearnceLevel#STANDARD} and the url is protected or User
   * clearance level is {@link SecurityConfiguration.SecurityClearnceLevel#RESTRICTIVE}) but there
   * is a valid 2nd-factor verified token.
   *
   * <p>Returns false if:<br>
   * - (User clearance level is {@link SecurityConfiguration.SecurityClearnceLevel#STANDARD} and the
   * url is protected or User clearance level is {@link
   * SecurityConfiguration.SecurityClearnceLevel#RESTRICTIVE}) and there is NO valid 2nd-factor
   * verified token. - The request is to 2nd-Factor login page itself so the chain stops and a
   * direct forwarding to the page is done
   */
  @Override
  public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue)
      throws Exception {

    Subject subject = SecurityUtils.getSubject();
    Session session = subject.getSession();

    // TODO Decide the involvment of remembered user logic here
    if (!subject.isAuthenticated()) {
      //			return onSubjectNotAuthenticated(request, response, mappedValue);
      return onSubjectNotAuthenticated();
    }

    SecurityConfiguration conf = getCurrentSubjectSecurityConfiguration();

    if (conf.getSecurityClearnceLevel() == null) {
      logger.error("No system wide security clearance level, this is a programming error.");

      //			handleNoSecurityClearance(request, response);
      handleNoSecurityClearance();

      return false;
    }

    // ONLY Allow if the request is to one of the supported factor means URLs in a GET request
    // ONLY Allow if the request intends to only show the login page of supported user's factor
    // login page
    SecondFactorAuthenticator factorAuthenticator;
    /*
     * Translate Starting from here in the javadoc of the method. (Also if found any below function that needs javadoc, please do :)
     */
    if ((factorAuthenticator = isRequestForSecondFactorAuthenticatorFormURL(request)) == null) {
      // Trying to access a 2nd-Factor protected url NOT the page itself

      // LeastRestrictive (no 2nd-Factor)
      if (conf.getSecurityClearnceLevel()
              .compareTo(SecurityConfiguration.SecurityClearnceLevel.STANDARD)
          < 0) {
        return true;
      }

      // STANDARD clearance level
      if (conf.getSecurityClearnceLevel()
              .compareTo(SecurityConfiguration.SecurityClearnceLevel.STANDARD)
          == 0) {

        boolean isUrlProtected = isSecondFactorProtectedURL(request);

        if (!isUrlProtected) {
          return true;
        }
      }

      // So this is either a protected URL in Mixed mode or the clerance level is CONFIDENTIAL all
      // together
      logger.debug("Trying to get the user's configured second factor");

      // Check if he has a valid 2nd factor code in his session
      if (assertSessionContainValidSecondFactorCredentials(session)) {
        logger.debug(
            "Previously verified second factor credential is remembered, allowing to continue the chain");
        return onSessionContainValidSecondFactorCredentials(request, response, mappedValue);
      }

      // No 2nd-Facor valid info
      logger.debug(
          "Detected a request to a protected resource, redirecting to a suitable second factor authentication page");
      return handleRedirectionForSecondFactorAuthenticationFormURL(request, response, subject);
    } else {
      // Trying to access the page itself
      if (isGETRequest(request)) {

        logger.debug(
            "Detected a GET request to the form of factor " + factorAuthenticator.getFactorName());
        return handleGETRequestForSecondFactorAuthenticatorFormURL(
            factorAuthenticator, request, response, session);
      } else if (isPOSTRequest(request)) {

        logger.debug(
            "Detected a POST request to the form of factor " + factorAuthenticator.getFactorName());
        return handlePostRequestForSecondFactorAuthenticatorFormURL(
            factorAuthenticator, request, response, session);
      } else {
        // Malformed Request: Status Code:
        // TODO HttpURLConnection.HTTP_BAD_REQUEST

        return false;
      }
    }
  }

  /**
   * Used in {@link SecurityConfiguration.SecurityClearnceLevel#STANDARD} mode
   *
   * @param request
   * @return
   */
  private boolean isSecondFactorProtectedURL(ServletRequest request) {

    List<String> protectedUrls = urlManager.getProtectedURLs();

    for (String url : protectedUrls) {
      if (pathsMatch(url, request)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Not used
   *
   * @param request
   * @param response
   * @param mappedValue
   * @return
   * @throws Exception
   */
  protected boolean onSubjectNotAuthenticated(
      ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
    // TODO Redirect to SOMETHING or STATUS CODE ANYTHING
    WebUtils.issueRedirect(request, response, "/");
    return false;
  }

  protected boolean onSubjectNotAuthenticated() {
    throw new UnauthenticatedForFirstFactorException();
  }

  protected boolean onSessionContainValidSecondFactorCredentials(
      ServletRequest request, ServletResponse response, Object mappedValue) {
    return true;
  }

  protected boolean handleRedirectionForSecondFactorAuthenticationFormURL(
      ServletRequest request, ServletResponse response, Subject subject) {

    String username = ((EBSPrincipal) subject.getPrincipal()).getUsername();
    String userSecondFactorMean = ((EBSPrincipal) subject.getPrincipal()).getSecondFactorMeanName();

    if (userSecondFactorMean == null || userSecondFactorMean.isEmpty()) {
      logger.debug(
          "No second factor authentication mean was configured for the username " + username);
      /*
      redirectToNoSecondAuthFactorConfiguredPage(request, response);
      // Through an exception ? The user doesn't have a configured 2nd factor method: NoSecondFactorConfiguredForUserException
      return false; // No , don't allow the request to go through the chain
      /*/
      throw new NoSecondFactorConfiguredException();
      // */
    }

    // It is neither a service request that contains valid credentials, nor a GET request that asks
    // for one, so redirect to a one
    logger.debug(
        "Detected an access to a resource that requires a second factor authentication, redirecting the user to perform his Second Factor authentication.");
    boolean saveRequest = true;
    String skipRequestSaving = request.getParameter(SKIP_REQUEST_SAVING_QUERY_PARAM);
    if (skipRequestSaving != null
        && (skipRequestSaving.equalsIgnoreCase("true")
            || skipRequestSaving.equalsIgnoreCase("1"))) {
      saveRequest = false;
    }
    if (saveRequest) {
      logger.debug("Saving the request for later redirection");
      saveRequest(request);
    }

    for (SecondFactorAuthenticator factorAuthenticator : supportedFilters.values()) {
      if (factorAuthenticator.getFactorName().equals(userSecondFactorMean)) {

        /*
        logger.debug("Redirecting the user to the Second Factor: " + factorAuthenticator.getFactorName() + " through URL " + factorAuthenticator.getFormUrl());
        WebUtils.issueRedirect(request, response, factorAuthenticator.getFormUrl());
        return false;
        /*/
        throw new UnauthenticatedForSecondFactorException(factorAuthenticator.getFormUrl());
        // */
      }
    }

    logger.debug(
        "Something is wrong, most probably invalid request headers were sent. The system won't allow this resource to be accessed.");

    /*
    redirectToContactAdministratorPage(request, response);
    return false;
    /*/
    throw new UnExpectedSecondFactorException(userSecondFactorMean);
    // */
  }

  protected boolean handlePostRequestForSecondFactorAuthenticatorFormURL(
      SecondFactorAuthenticator factorAuthenticator,
      ServletRequest request,
      ServletResponse response,
      Session session)
      throws IOException {
    return handlePostRequestForSecondFactorAuthenticatorFormURL(
        factorAuthenticator, request, response, session, "/");
  }

  protected boolean handlePostRequestForSecondFactorAuthenticatorFormURL(
      SecondFactorAuthenticator factorAuthenticator,
      ServletRequest request,
      ServletResponse response,
      Session session,
      String successRedirectionFallbackUrl)
      throws IOException {
    // Start authenticating
    logger.debug("Generating a token to be verified from supplied credentials");
    AuthenticationToken token = factorAuthenticator.createToken(request);
    try {
      logger.debug("Trying to authenticate the token...");
      SecondFactorAuthAttempts loginAttemptsInfo =
          (SecondFactorAuthAttempts)
              SecurityUtils.getSubject()
                  .getSession()
                  .getAttribute(
                      SessionAttributeKeys.TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO);

      AuthenticationInfo info =
          factorAuthenticator.authenticate(
              token, loginAttemptsInfo.getActiveGeneratedCredentialInfo());
      logger.debug("The token is verified.");
      return onSecondFactorAuthSuccess();
    } catch (AuthenticationException e) {
      logger.debug("The token couldn't be verified.");
      return onSecondFactorAuthFailure(token, factorAuthenticator, e, request, response);
    }
  }

  protected boolean handleGETRequestForSecondFactorAuthenticatorFormURL(
      SecondFactorAuthenticator factorAuthenticator,
      ServletRequest request,
      ServletResponse response,
      Session session) {
    // Handle if he gets back from a failed submission
    logger.debug("Detected an access to a Second Factor Authentication form URL");

    SecondFactorAuthAttempts secondFactorAuthAttemptsInfo =
        (SecondFactorAuthAttempts)
            session.getAttribute(
                SessionAttributeKeys.TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO);

    if (secondFactorAuthAttemptsInfo == null) {
      logger.debug("No previous attempts detected");
      secondFactorAuthAttemptsInfo = new SecondFactorAuthAttempts();
    } else {
      logger.debug("Detected a previous attempts audit");
      if (isPreviousCredentialInvalidationRequested(request)) {
        logger.debug(
            "A request to invalidate a previous system-generated credentials was detected");
        secondFactorAuthAttemptsInfo.invalidateActiveGenerateCredentialInfo();
      }
    }

    // If invalid or no credentials generated, generate new one
    if (secondFactorAuthAttemptsInfo.isEmpty() || !secondFactorAuthAttemptsInfo.isValid()) {
      logger.debug(
          "No previous valid system-generated credentials was detected, generating new ones");
      GenerateCredentialInfo generatedCredentialInfo =
          factorAuthenticator.generateCredentials(request);
      secondFactorAuthAttemptsInfo.addGenerateCredentialInfo(generatedCredentialInfo);
      session.setAttribute(
          SessionAttributeKeys.TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO,
          secondFactorAuthAttemptsInfo);
    }
    return true;
  }

  @Override
  public void afterCompletion(ServletRequest request, ServletResponse response, Exception exception)
      throws Exception {
    super.afterCompletion(request, response, exception);

    //		HttpServletResponse httpResponse = (HttpServletResponse) response;
    //		httpResponse.setStatus(sc);

  }

  protected SecondFactorAuthenticator isRequestForSecondFactorAuthenticatorFormURL(
      ServletRequest request) {
    for (SecondFactorAuthenticator factorAuthenticator : supportedFilters.values()) {
      if (pathsMatch(factorAuthenticator.getFormUrl(), request)) {
        return factorAuthenticator;
      }
    }
    return null;
  }

  /**
   * This happens when the parameter "invalidateGeneratedCredential" is sent from
   * Regenerate_credentials link to invalidate the generated credentials and generate another one.
   */
  protected boolean isPreviousCredentialInvalidationRequested(ServletRequest request) {
    String invalidateCredentials =
        request.getParameter(
            DEFAULT_INVALIDATE_GENERATED_CREDENTIAL_PARAM); // can be either 1, or 0
    return invalidateCredentials != null
        && (invalidateCredentials.trim().equals("1")
            || invalidateCredentials.trim().equalsIgnoreCase("true"));
  }

  protected boolean onSecondFactorAuthSuccess() {
    return true;
  }

  protected boolean onSecondFactorAuthSuccess(
      AuthenticationToken token,
      AuthenticationInfo info,
      SecondFactorAuthenticator factorAuthenticator,
      ServletRequest request,
      ServletResponse response,
      String redirectionFallbackUrl) {

    try {
      WebUtils.redirectToSavedRequest(request, response, redirectionFallbackUrl);
    } catch (IOException e) {
      logger.error(e.getMessage(), e);
    }
    return false;
  }

  protected boolean onSecondFactorAuthFailure(
      AuthenticationToken token,
      SecondFactorAuthenticator factorAuthenticator,
      AuthenticationException e,
      ServletRequest request,
      ServletResponse response) {
    // TODO REDIRECT TO UNAUTHORIZED/AuthenticationException page/MAYBE SAME PAGE AGAIN
    // Save in the session a counter

    if (e instanceof ExpiredSecondFactorCredentialException) {
      // Handle if there there is a credential generated from previous requests that became invalid
      logger.debug("The previously system-generated credentials were found to be invalid.");
      SecurityUtils.getSubject()
          .getSession()
          .setAttribute(
              SessionAttributeKeys.TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO, null);

      //			handleExpiredCredentials(request, response);
      throw e;
    }

    logger.debug("2nd-Factor Authentication Exception and the 2nd-Factor credentials are valid.");
    throw new SecondFactorAuthenticationException();
    //		return false;
  }

  /**
   * Not used
   *
   * @param request
   * @param response
   * @throws IOException
   */
  protected void handleExpiredCredentials(ServletRequest request, ServletResponse response)
      throws Exception {
    WebUtils.issueRedirect(request, response, getGeneratedCredentialsBecameInvalidRedirectUrl());
  }

  /**
   * Convenience method merely delegates to {@link
   * WebUtils#saveRequest(javax.servlet.ServletRequest) WebUtils.saveRequest(request)} to save the
   * request state for reuse later. This is mostly used to retain user request state when a redirect
   * is issued to return the user to their originally requested url/resource.
   *
   * <p>If you need to save and then immediately redirect the user to login, consider using
   * saveRequestAndRedirectToLogin(request,response)} directly.
   *
   * @param request the incoming ServletRequest to save for re-use later (for example, after a
   *     redirect).
   */
  protected void saveRequest(ServletRequest request) {
    WebUtils.saveRequest(request);
  }

  public List<SecondFactorAuthenticator> getSupportedFilters() {
    return (List<SecondFactorAuthenticator>) supportedFilters.values();
  }

  public void setSupportedFilters(List<SecondFactorAuthenticator> supportedFilters) {
    this.supportedFilters = new HashMap<String, SecondFactorAuthenticator>();
    for (SecondFactorAuthenticator filter : supportedFilters) {
      this.supportedFilters.put(filter.getFormUrl(), filter);
    }
  }

  /**
   * Not used
   *
   * @param request
   * @param response
   * @throws IOException
   */
  protected void redirectToNoSecondAuthFactorConfiguredPage(
      ServletRequest request, ServletResponse response) throws IOException {
    WebUtils.issueRedirect(request, response, getNoSecondAuthFactorConfiguredErrorRedirectUrl());
  }

  /**
   * Not used
   *
   * @param request
   * @param response
   * @throws IOException
   */
  protected void handleNoSecurityClearance(ServletRequest request, ServletResponse response)
      throws Exception {
    WebUtils.issueRedirect(request, response, getContactAdministratorRedirectUrl());
  }

  protected void handleNoSecurityClearance() {
    throw new SecondFactorAuthenticationException();
  }

  private String getGeneratedCredentialsBecameInvalidRedirectUrl() {
    return generatedCredentialsBecameInvalidRedirectUrl;
  }

  public void setGeneratedCredentialsBecameInvalidRedirectUrl(
      String generatedCredentialsBecameInvalidRedirectUrl) {
    this.generatedCredentialsBecameInvalidRedirectUrl =
        generatedCredentialsBecameInvalidRedirectUrl;
  }

  private String getNoSecondAuthFactorConfiguredErrorRedirectUrl() {
    return noSecondAuthFactorConfiguredErrorRedirectUrl;
  }

  public void setNoSecondAuthFactorConfiguredErrorRedirectUrl(
      String noSecondAuthFactorConfiguredErrorRedirectUrl) {
    this.noSecondAuthFactorConfiguredErrorRedirectUrl =
        noSecondAuthFactorConfiguredErrorRedirectUrl;
  }

  public String getContactAdministratorRedirectUrl() {
    return contactAdministratorRedirectUrl;
  }

  public void setContactAdministratorRedirectUrl(String contactAdministratorRedirectUrl) {
    this.contactAdministratorRedirectUrl = contactAdministratorRedirectUrl;
  }

  public String getUnauthorizedRedirectUrl() {
    return unauthorizedRedirectUrl;
  }

  public void setUnauthorizedRedirectUrl(String unauthorizedRedirectUrl) {
    this.unauthorizedRedirectUrl = unauthorizedRedirectUrl;
  }

  public URLManager getUrlManager() {
    return urlManager;
  }

  public void setUrlManager(URLManager urlManager) {
    this.urlManager = urlManager;
  }
}
