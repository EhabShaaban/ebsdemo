package com.ebs.dac.security.shiro.twofactorauth;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * Only a marker interface to be able to detect authentication type (First or Second) based on token
 * type.
 */
public interface SecondFactorToken extends AuthenticationToken {}
