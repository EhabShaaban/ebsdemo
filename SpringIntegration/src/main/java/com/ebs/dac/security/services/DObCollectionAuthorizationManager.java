package com.ebs.dac.security.services;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.accounting.collection.apis.IDObCollectionActionNames;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;

public class DObCollectionAuthorizationManager {
  private AuthorizationManager authorizationManager;

  public DObCollectionAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
  }

  public void authorizeActionOnSection(BusinessObject businessObject, String actionName)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void authorizeReadAction(String sysName, String actionName) {
    this.authorizationManager.authorizeAction(sysName, actionName);
  }

  public void authorizeActivateCollection(DObCollectionGeneralModel collectionGeneralModel) throws Exception {
    try {
      this.authorizationManager.authorizeAction(
              collectionGeneralModel, IDObCollectionActionNames.ACTIVATE);
    } catch (ConditionalAuthorizationException e) {
      throw new AuthorizationException(e);
    }
  }
}
