package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** When the account for the passed username and company is blocked */
public class BlockedAccountException extends FirstFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_MESSAGE = "This accout has been blocked";

  private String username;
  private String companyCode;

  {
    ERROR_CODE = "LOGIN-ACCT_BLKD";
    STATUS_CODE = HttpStatusCode.FORBIDDEN.code();
  }

  public BlockedAccountException() {
    super(DEFAULT_MESSAGE);
  }

  public BlockedAccountException(String username, String companyCode) {
    this();
    this.username = username;
    this.companyCode = companyCode;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String toString() {
    return "Account for username [" + username + "] and company [" + companyCode + "] is BLOCKED.";
  }
}
