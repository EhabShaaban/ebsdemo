package com.ebs.dac.security.shiro.filters.factory;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.filter.mgt.FilterChainManager;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;

/**
 * This class is made mainly to build the <i>URL</i> section in the "shiro-security.xml" file. i.e.
 * stating the urls with their filters.<br>
 */
public class CustomShiroFilterFactoryBean extends ShiroFilterFactoryBean {

  private static final Logger logger = LoggerFactory.getLogger(CustomShiroFilterFactoryBean.class);

  /** Check BDK-138 Check https://issues.apache.org/jira/browse/SHIRO-360 */
  private boolean isResponseUrlEncodingEnabled = false;

  @Override
  protected AbstractShiroFilter createInstance() throws Exception {
    logger.debug("Creating Shiro Filter instance.");

    SecurityManager securityManager = getSecurityManager();
    if (securityManager == null) {
      String msg = "SecurityManager property must be set.";
      throw new BeanInitializationException(msg);
    }

    if (!(securityManager instanceof WebSecurityManager)) {
      String msg = "The security manager does not implement the WebSecurityManager interface.";
      throw new BeanInitializationException(msg);
    }

    FilterChainManager manager = createFilterChainManager();

    // Expose the constructed FilterChainManager by first wrapping it in a
    // FilterChainResolver implementation. The AbstractShiroFilter implementations
    // do not know about FilterChainManagers - only resolvers:
    PathMatchingFilterChainResolver chainResolver = new PathMatchingFilterChainResolver();
    chainResolver.setFilterChainManager(manager);

    // Now create a concrete ShiroFilter instance and apply the acquired SecurityManager and built
    // FilterChainResolver.  It doesn't matter that the instance is an anonymous inner class
    // here - we're just using it because it is a concrete AbstractShiroFilter instance that accepts
    // injection of the SecurityManager and FilterChainResolver:
    return new BDKSpringShiroFilter(
        (WebSecurityManager) securityManager, chainResolver, isResponseUrlEncodingEnabled);
  }

  public boolean isResponseUrlEncodingEnabled() {
    return isResponseUrlEncodingEnabled;
  }

  public void setResponseUrlEncodingEnabled(boolean isResponseUrlEncodingEnabled) {
    this.isResponseUrlEncodingEnabled = isResponseUrlEncodingEnabled;
  }
}
