package com.ebs.dac.security.shiro.twofactorauth;

import com.ebs.dac.security.exceptions.ExpiredSecondFactorCredentialException;
import javax.servlet.ServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;

public abstract class SecondFactorAuthenticator {

  public static final String DEFAULT_USERNAME_PARAM = "username";
  public static final String DEFAULT_CREDENTIAL_PARAM = "credentials";

  protected String factorName;
  protected String formUrl;

  public SecondFactorAuthenticator(String factorName, String formUrl) {
    this.factorName = factorName;
    this.formUrl = formUrl;
  }

  /**
   * Not recommended to be used, made specifically for ini-based configuration where no constructor
   * initialization is supported
   */
  public SecondFactorAuthenticator() {}

  public AuthenticationInfo authenticate(
      AuthenticationToken token, GenerateCredentialInfo credentialInfo)
      throws AuthenticationException {
    if (credentialInfo != null && !credentialInfo.isValid()) {
      // Don't effort yourself, these credentials you are checking against are no longer valid
      throw new ExpiredSecondFactorCredentialException(credentialInfo.getTimeout());
    }
    // Build corresponding token
    AuthenticationInfo info;
    info = SecurityUtils.getSecurityManager().authenticate(token);
    return info;
  }

  /** @return The validity of the generated credentials in seconds */
  public abstract GenerateCredentialInfo generateCredentials(ServletRequest request);

  /**
   * Used in preparation to authenticate supplied code against the correct ones
   *
   * @param request
   * @return
   */
  public abstract AuthenticationToken createToken(ServletRequest request);

  public String getFormUrl() {
    return formUrl;
  }

  public void setFormUrl(String formUrl) {
    this.formUrl = formUrl;
  }

  public String getFactorName() {
    return factorName;
  }

  public void setFactorName(String factorName) {
    this.factorName = factorName;
  }
}
