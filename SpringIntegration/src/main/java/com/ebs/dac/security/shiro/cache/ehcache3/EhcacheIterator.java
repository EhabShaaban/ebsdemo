package com.ebs.dac.security.shiro.cache.ehcache3;

import java.util.Iterator;

/** Created by marisoft on 11/14/16. */
abstract class EhcacheIterator<K, V, T> implements Iterator<T> {

  private final Iterator<org.ehcache.Cache.Entry<K, V>> cacheIterator;

  EhcacheIterator(Iterator<org.ehcache.Cache.Entry<K, V>> cacheIterator) {
    this.cacheIterator = cacheIterator;
  }

  public boolean hasNext() {
    return cacheIterator.hasNext();
  }

  public T next() {
    return getNext(cacheIterator);
  }

  public void remove() {
    throw new UnsupportedOperationException("remove");
  }

  protected abstract T getNext(Iterator<org.ehcache.Cache.Entry<K, V>> cacheIterator);
}
