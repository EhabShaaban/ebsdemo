package com.ebs.dac.security.services;

import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.exceptions.ResourceAccessException;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AuthorizedPurchaseUnitService {

  private static final String WILD_CARD_CONDITION = "*";

  private AuthorizationManager authorizationManager;
  private CObPurchasingUnitGeneralModelRep purchasingUnitRep;

  public List<CObPurchasingUnitGeneralModel> getAllowedPurchaseUnitsBasedOnUserPermission(
      String readPermission) throws Exception {
    checkIfReadPermissionIsAllowed(ICObPurchasingUnit.SYS_NAME, readPermission);
    Set<String> conditions = authorizationManager.getPermissionConditions();
    return filterPurchaseUnitAccordingToCondition(conditions);
  }

  private void checkIfReadPermissionIsAllowed(String sysName, String readPermission)
      throws Exception {
    try {
      authorizationManager.authorizeAction(sysName, readPermission);
    } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      throw new ResourceAccessException();
    }
  }

  private List<CObPurchasingUnitGeneralModel> filterPurchaseUnitAccordingToCondition(
      Set<String> conditions) {
    List<CObPurchasingUnitGeneralModel> purchasingUnitGeneralModelList = null;
    if (!conditions.contains(WILD_CARD_CONDITION)) {
      purchasingUnitGeneralModelList = getPurchaseUnitsForUsersWithoutWildCardCondition(conditions);
    } else {
      purchasingUnitGeneralModelList = purchasingUnitRep.findAllByOrderByIdDesc();
    }
    return purchasingUnitGeneralModelList;
  }

  private List<CObPurchasingUnitGeneralModel> getPurchaseUnitsForUsersWithoutWildCardCondition(
      Set<String> conditions) {
    List<String> purchaseUnitNames =
        getConditionAttributeValue(ICObPurchasingUnit.PURCHASING_UNIT_NAME, conditions);
    return purchasingUnitRep.findByPurchaseUnitNameInOrderByIdDesc(purchaseUnitNames);
  }

  private List<String> getConditionAttributeValue(String attribute, Set<String> conditions) {
    List<String> attributeValuesList = new ArrayList<>();
    for (String conditionValue : conditions) {
      addConditionalAttributeValueToList(attributeValuesList, conditionValue, attribute);
    }
    return attributeValuesList;
  }

  private void addConditionalAttributeValueToList(
      List<String> attributeValuesList, String conditionValue, String attribute) {
    if (!conditionValue.equals(WILD_CARD_CONDITION)) {
      addConditionalAttributeValueToListWithoutWildCardCondition(
          attributeValuesList, conditionValue, attribute);
    } else {
      attributeValuesList.add(WILD_CARD_CONDITION);
    }
  }

  private void addConditionalAttributeValueToListWithoutWildCardCondition(
      List<String> attributeValuesList, String conditionValue, String attribute) {
    int startIndex = conditionValue.indexOf(attribute);
    int operationIndex = conditionValue.indexOf("]", startIndex);
    String attributeValue =
        conditionValue.substring(startIndex + attribute.length() + 2, operationIndex - 1);
    attributeValuesList.add(attributeValue);
  }

  public void setAuthorizationManager(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void setPurchasingUnitRep(CObPurchasingUnitGeneralModelRep purchasingUnitRep) {
    this.purchasingUnitRep = purchasingUnitRep;
  }
}
