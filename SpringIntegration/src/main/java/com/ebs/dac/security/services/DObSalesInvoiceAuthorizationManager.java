package com.ebs.dac.security.services;

import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.jpa.accounting.salesinvoice.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

public class DObSalesInvoiceAuthorizationManager {

  @Autowired
  private AuthorizationManager authorizationManager;

  @Autowired
  private DObSalesOrderGeneralModelRep salesOrderGMRep;

  @Autowired
  private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitRep;

  @Autowired
  private DocumentOwnerGeneralModelRep documentOwnerGMRep;

  public DObSalesInvoiceAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
  }

  public void applyAuthorizationForCreation(DObSalesInvoiceCreationValueObject creationValueObject)
      throws Exception {

    authorizationManager.authorizeAction(IDObSalesInvoice.SYS_NAME, IActionsNames.CREATE);
    authorizationManager.authorizeAction(ICObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);

    checkDocumentOwnerAuthCondition(creationValueObject.getDocumentOwnerId(),
        creationValueObject.getBusinessUnitCode());

    checkSalesOrderAuthCondition(creationValueObject.getSalesOrderCode());

    if (isSalesInvoiceWithoutReference(creationValueObject.getInvoiceTypeCode())) {

      authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
      checkCustomerAuthCondition(creationValueObject.getCustomerCode(),
          creationValueObject.getBusinessUnitCode());
    }
  }

  private void checkDocumentOwnerAuthCondition(Long docOwnerId, String businessUnitCode)
      throws Exception {
    // authorizationManager.authorizeAction(DocumentOwnerGeneralModel.SYS_NAME,
    // IActionsNames.READ_ALL);

    DocumentOwnerGeneralModel docOwner =
        documentOwnerGMRep.findOneByUserIdAndObjectName(docOwnerId, IDObSalesInvoice.SYS_NAME);
    if (docOwner != null) {
      authorizationManager.isConditionMatch(DocumentOwnerGeneralModel.SYS_NAME,
          IActionsNames.READ_ALL, docOwner.getCondition());
    }

  }

  private boolean isSalesInvoiceWithoutReference(String invoiceTypeCode) {
    return invoiceTypeCode.equals("SALES_INVOICE_WITHOUT_REFERENCE");
  }

  private void checkSalesOrderAuthCondition(String code) throws Exception {
    DObSalesOrderGeneralModel salesOrder = salesOrderGMRep.findOneByUserCode(code);
    if (salesOrder != null) {
      try {
        authorizationManager.authorizeAction(salesOrder, IActionsNames.READ_ALL);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  private void checkCustomerAuthCondition(String customerCode, String businessUnitCode)
      throws Exception {
    IObCustomerBusinessUnitGeneralModel customer = customerBusinessUnitRep
        .findOneByUserCodeAndBusinessUnitcode(customerCode, businessUnitCode);
    if (customer != null) {
      try {
        authorizationManager.authorizeAction(customer, IActionsNames.READ_ALL);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }

  }

  public void authorizeActionOnSection(BusinessObject businessObject, String actionName)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public AuthorizationManager getAuthorizationManager() {
    return authorizationManager;
  }

    public void authorizePostSalesInvoice(DObSalesInvoiceDeliverToCustomerGeneralModel invoiceGeneralModel) throws Exception {
      try {
        this.authorizationManager.authorizeAction(
                invoiceGeneralModel, IDObSalesInvoiceActionNames.DELIVER_TO_CUSTOMER);
      } catch (ConditionalAuthorizationException e) {
        throw new AuthorizationException(e);
      }
    }
}
