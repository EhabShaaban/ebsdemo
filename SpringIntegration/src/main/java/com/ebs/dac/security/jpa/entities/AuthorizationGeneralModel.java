package com.ebs.dac.security.jpa.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AuthorizationGeneralModel")
public class AuthorizationGeneralModel {

  @Id private Long id;
  private Long userId;
  private String userName;
  private String permissionExpression;
  private String condition;

  public Long getId() {
    return id;
  }

  public Long getUserId() {
    return userId;
  }

  public String getUserName() {
    return userName;
  }

  public String getPermissionExpression() {
    return permissionExpression;
  }

  public String getCondition() {
    return condition;
  }
}
