package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/**
 * When user is configured to use 2nd-Factor authentication but no 2nd-Factor is configured for him.
 */
public class NoSecondFactorConfiguredException extends SecondFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  {
    ERROR_CODE = "ACESS-2NDF_NFND";
    STATUS_CODE =
        HttpStatusCode.CONFLICT
            .code(); // conflict between user configurations (will use 2nd-Factor but no 2nd-Factor
                     // is assigned to him)
  }
}
