package com.ebs.dac.security.shiro;

import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.session.SessionAttributeKeys;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlobalUsersHandler implements IGlobalUsersHandler {

  private static final Logger logger = LoggerFactory.getLogger(GlobalUsersHandler.class);

  private Set<CredentialSubject> loggedInUsers;

  public GlobalUsersHandler() {
    loggedInUsers = new CopyOnWriteArraySet<CredentialSubject>();
  }

  public void addUserToLoggedInUsers(Subject subject) {
    logger.info(
        "GlobalUsersHandler.addUserToLoggedInUsers(Subject): "
            + subject
            + " : Current list of users: "
            + this.toString());

    CredentialSubject credentialSubject = new CredentialSubject();
    credentialSubject.setSubject(subject);

    String username = getUsernameFromSubject(subject);
    credentialSubject.setUsername(username);

    //		String ccode = (String)
    // ThreadLocalUtil.getThreadVariable(CommonConstants.COMPANY_CODE_HTTP_REQUEST_HEADER_ATTRIBUTE);
    EBSPrincipal principal = (EBSPrincipal) subject.getPrincipal();
    String ccode = principal.getCompany();
    credentialSubject.setCcode(ccode);
    addUserToLoggedInUsers(credentialSubject);

    logger.info(
        "GlobalUsersHandler.addUserToLoggedInUsers(Subject): End: "
            + subject
            + " : Current list of users: "
            + this.toString());
  }

  public void addUserToLoggedInUsers(CredentialSubject subject) {
    logger.info(
        "GlobalUsersHandler.addUserToLoggedInUsers(CredentialSubject): "
            + subject
            + " : Current list of users: "
            + this.toString());

    loggedInUsers.add(subject);

    logger.info(
        "GlobalUsersHandler.addUserToLoggedInUsers(CredentialSubject): End: "
            + " : Current list of users: "
            + this.toString());
  }

  public void removeUserFromLoggedInUsers(CredentialSubject subject) {
    logger.info(
        "GlobalUsersHandler.removeUserFromLoggedInUsers(CredentialSubject): "
            + subject
            + " : Current list of users: "
            + this.toString());

    removeUserFromLoggedInUsers(subject.getUsername());

    logger.info(
        "GlobalUsersHandler.removeUserFromLoggedInUsers(CredentialSubject): End: "
            + subject
            + " : Current list of users: "
            + this.toString());
  }

  public void removeUserFromLoggedInUsers(Subject subject) {
    logger.info(
        "GlobalUsersHandler.removeUserFromLoggedInUsers(Subject): "
            + subject
            + " : Current list of users: "
            + this.toString());

    String username = getUsernameFromSubject(subject);

    removeUserFromLoggedInUsers(username);

    logger.info(
        "GlobalUsersHandler.removeUserFromLoggedInUsers(Subject): End: "
            + " : Current list of users: "
            + this.toString());
  }

  public void removeUserFromLoggedInUsers(String username) {
    logger.info(
        "GlobalUsersHandler.removeUserFromLoggedInUsers(String): "
            + username
            + " : Current list of users: "
            + this.toString());

    if (username != null) {
      for (CredentialSubject credentialSubject : loggedInUsers) {
        if (username.equals(credentialSubject.getUsername())) {

          loggedInUsers.remove(credentialSubject);
          break;
        }
      }
    }

    logger.info(
        "GlobalUsersHandler.removeUserFromLoggedInUsers(String): End: "
            + " : Current list of users: "
            + this.toString());
  }

  public void logoutUser(CredentialSubject subject) {
    logger.info(
        "GlobalUsersHandler.logoutUser(CredentialSubject): "
            + subject
            + " : Current list of users: "
            + this.toString());

    logoutUser(subject.getSubject());

    logger.info(
        "GlobalUsersHandler.logoutUser(CredentialSubject): End: "
            + " : Current list of users: "
            + this.toString());
  }

  public void logoutUser(String username) {
    logger.info(
        "GlobalUsersHandler.logoutUser(String): "
            + username
            + " : Current list of users: "
            + this.toString());

    if (username != null) {
      for (CredentialSubject credentialSubject : loggedInUsers) {
        if (username.equals(credentialSubject.getUsername())) {

          logoutUser(credentialSubject.getSubject());
          break;
        }
      }
    }

    logger.info(
        "GlobalUsersHandler.logoutUser(String): End: "
            + " : Current list of users: "
            + this.toString());
  }

  public void logoutUser(Subject subject) {
    logger.info(
        "GlobalUsersHandler.logoutUser(Subject): "
            + subject
            + " : Current list of users: "
            + this.toString());

    subject
        .logout(); // This will cause the user to be logged out and onLogout
                   // (UserEnityAuthenticationListener) will be called

    logger.info(
        "GlobalUsersHandler.logoutUser(Subject): End: "
            + " : Current list of users: "
            + this.toString());
  }

  public Set<CredentialSubject> getLoggedInUsers() {
    return loggedInUsers;
  }

  public void setLoggedInUsers(Set<CredentialSubject> loggedInUsers) {
    this.loggedInUsers = loggedInUsers;
  }

  private String getUsernameFromSubject(Subject subject) {
    String username = null;
    Session session = subject.getSession(false);
    if (session
        != null) { // I use the session first as sometimes the subject has no principals e.g. in
                   // onSuccess in AutheticationListener
      username = (String) session.getAttribute(SessionAttributeKeys.USERNAME);
    }
    if (username == null) { // Search in Subject Principals
      username = ((EBSPrincipal) subject.getPrincipal()).getUsername();
    }

    return username;
  }

  public String toString() {
    String result = "[";
    for (CredentialSubject s : loggedInUsers) {
      result += s.getUsername() + ",";
    }
    result += "]";
    return result;
  }
}
