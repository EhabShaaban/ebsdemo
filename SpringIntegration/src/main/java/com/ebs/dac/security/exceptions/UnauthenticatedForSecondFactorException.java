package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** Trying to access a 2nd-Factor protected resource with no authentication info. */
public class UnauthenticatedForSecondFactorException extends SecondFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private String supposedFactorUrl;

  {
    ERROR_CODE = "ACESS-2NDF_EROR";
    STATUS_CODE = HttpStatusCode.UNAUTHORIZED.code();
  }

  public UnauthenticatedForSecondFactorException() {
    super();
  }

  public UnauthenticatedForSecondFactorException(String message, Throwable cause) {
    super(message, cause);
  }

  public UnauthenticatedForSecondFactorException(String supposedSecondFactorUrl) {
    this.supposedFactorUrl = supposedSecondFactorUrl;
  }

  public UnauthenticatedForSecondFactorException(Throwable e) {
    super(e);
  }

  public String getSupposedFactorUrl() {
    return supposedFactorUrl;
  }
}
