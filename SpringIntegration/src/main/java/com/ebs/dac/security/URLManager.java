package com.ebs.dac.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class URLManager {

  private List<String> protectedURLs;
  private List<String> publicURLs;

  private String urlsFilePrefix;

  public URLManager(String urlsFilePrefix) {
    this.urlsFilePrefix = urlsFilePrefix;
  }

  public String buildPublicUrlSection(String... filters) {
    List<String> urls = getPublicURLs();
    String definitions = "";
    for (String url : urls) {
      definitions += url + " = " + buildChainString(filters) + "\n";
    }
    return definitions;
  }

  public void buildPublicUrlSection(Map<String, String> def, String... filters) {
    if (def == null) return;
    List<String> urls = getPublicURLs();
    for (String url : urls) {
      def.put(url, buildChainString(filters));
    }
  }

  protected void initPublicUrls() {

    String publicAllowedUrlsFilePath =
        "/modules-security/" + urlsFilePrefix + "public-allowed-urls";
    Scanner scanner = new Scanner(this.getClass().getResourceAsStream(publicAllowedUrlsFilePath));

    publicURLs = new ArrayList<String>();

    while (scanner.hasNextLine()) {
      String line = scanner.nextLine();

      if (line.trim().equals("") || line.trim().startsWith("#")) {
        continue;
      }
      publicURLs.add(line);
    }
    scanner.close();
  }

  /**
   * Normally this will be called once to init the protectedUrls.
   *
   * @param urlsFilePrefix e.g. qa, hr, ...
   */
  protected void initProtectedUrls(String urlsFilePrefix) {
    String protectedAllowedUrlsFilePath =
        "/modules-security/" + urlsFilePrefix + "second-factor-protected-urls";
    protectedURLs = new ArrayList<String>();

    Scanner scanner =
        new Scanner(this.getClass().getResourceAsStream(protectedAllowedUrlsFilePath));
    while (scanner.hasNextLine()) {
      protectedURLs.add(scanner.nextLine());
    }
    scanner.close();
  }

  private String buildChainString(String... filters) {
    String chain = "";
    for (int i = 0; i < filters.length; i++) {
      chain += filters[i];
      if (i != filters.length - 1) {
        chain += ", ";
      }
    }
    return chain;
  }

  public List<String> getProtectedURLs() {
    if (protectedURLs == null) {
      initProtectedUrls(urlsFilePrefix);
    }
    return new ArrayList<>(protectedURLs);
  }

  public List<String> getPublicURLs() {
    if (publicURLs == null) {
      initPublicUrls();
    }
    return new ArrayList<>(publicURLs);
  }

  public String getUrlsFilePrefix() {
    return urlsFilePrefix;
  }

  public void setUrlsFilePrefix(String urlsFilePrefix) {
    this.urlsFilePrefix = urlsFilePrefix;
  }
}
