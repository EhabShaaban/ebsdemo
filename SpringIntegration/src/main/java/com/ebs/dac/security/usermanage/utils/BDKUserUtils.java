package com.ebs.dac.security.usermanage.utils;

import org.mindrot.jbcrypt.BCrypt;

public class BDKUserUtils {

  public static String encodePassword(String password) {
    return BCrypt.hashpw(password, BCrypt.gensalt(10));
  }

  public static String decodePassword(String password) {
    throw new UnsupportedOperationException("Encryption algorithm is one-way only.");
  }

  public static boolean checkPassword(String plainPassword, String encryptedPassword) {
    return BCrypt.checkpw(plainPassword, encryptedPassword);
  }
}
