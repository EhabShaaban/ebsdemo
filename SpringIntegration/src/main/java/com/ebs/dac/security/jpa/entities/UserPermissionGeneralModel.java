package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UserPermissionGeneralModel")
public class UserPermissionGeneralModel implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;

  @Column(name = "permissionexpression")
  private String permissionExpression;

  @Column(name = "objectname")
  private String objectName;

  @Column(name = "username")
  private String userName;

  @Column(name = "authorizationConditionId")
  private Long authorizationConditionId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPermissionExpression() {
    return permissionExpression;
  }

  public void setPermissionExpression(String permissionExpression) {
    this.permissionExpression = permissionExpression;
  }

  public String getObjectName() {
    return objectName;
  }

  public void setObjectName(String objectName) {
    this.objectName = objectName;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public Long getAuthorizationConditionId() {
    return authorizationConditionId;
  }

  public void setAuthorizationConditionId(Long authorizationConditionId) {
    this.authorizationConditionId = authorizationConditionId;
  }
}
