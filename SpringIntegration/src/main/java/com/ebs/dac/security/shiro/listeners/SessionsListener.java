package com.ebs.dac.security.shiro.listeners;

import com.ebs.dac.security.shiro.IGlobalUsersHandler;
import com.ebs.dac.security.shiro.session.SessionAttributeKeys;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listens on the Session start, stop, and expiration events. Currently it is responsible for
 * removing user from logged-in list when his session is stopped or expired.
 */
public class SessionsListener implements SessionListener {

  private static final Logger logger = LoggerFactory.getLogger(SessionsListener.class);

  private IGlobalUsersHandler globalUserHandler;

  /**
   * When session starts (user login).<br>
   * <b>Note: </b>The adding of users in {@code globalUserInfo} is done in {@link
   * UserEntityAuthenticationListener} as it has both subject and username
   */
  public void onStart(Session session) {}

  /**
   * This is triggered on users logout. <br>
   * Currently handles the user removing from the logged-in list.
   */
  public void onStop(Session session) {
    String username = (String) session.getAttribute(SessionAttributeKeys.USERNAME);

    /*if (username == null){
    	throwUsernameNotFoundInSessionException(session);
    }*/
    logger.info(
        "SessionListener.onStop: removing user "
            + username
            + " from system-wide list of currently logged in users");
    globalUserHandler.removeUserFromLoggedInUsers(username);
  }

  /**
   * This is triggered when session is expired (Also this is the case for orphan sessions i.e.
   * deleted and forgotten sessions). <br>
   * Currently handles the user removing from the logged-in list.
   */
  public void onExpiration(Session session) {
    String username = (String) session.getAttribute(SessionAttributeKeys.USERNAME);

    /*if (username == null){
    	throwUsernameNotFoundInSessionException(session);
    }*/

    logger.info(
        "SessionListener.onExpiration: removing user "
            + username
            + " from system-wide list of currently logged in users");
    globalUserHandler.removeUserFromLoggedInUsers(username);
  }

  public IGlobalUsersHandler getGlobalUserHandler() {
    return globalUserHandler;
  }

  public void setGlobalUserHandler(IGlobalUsersHandler globalUserHandler) {
    this.globalUserHandler = globalUserHandler;
  }

  private void throwUsernameNotFoundInSessionException(Session session) {
    throw new NullPointerException(
        "Couldn't find a username associated with the session "
            + session.getId()
            + " to remove him from the system-wide list of currently logged in users, "
            + "the user of this session may face some relogin problems. Current Shiro subject:"
            + SecurityUtils.getSubject()
            + " and principal: "
            + (SecurityUtils.getSubject() == null
                ? "[couldn't retrieve due to Subject being null]"
                : SecurityUtils.getSubject().getPrincipal()));
  }
}
