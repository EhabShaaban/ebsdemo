package com.ebs.dac.security.usermanage.utils;

import org.apache.shiro.session.Session;

public class SessionVariableUtils {

  public static int incrementCounter(Session session, String key) {
    Integer counter;
    if ((counter = (Integer) session.getAttribute(key)) != null) {
      counter++;
    } else {
      counter = 1;
    }
    session.setAttribute(key, counter);
    return counter;
  }

  public static void resetCounter(Session session, String key) {
    session.setAttribute(key, null);
  }

  public static void removeKey(Session session, String key) {
    session.removeAttribute(key);
  }
}
