package com.ebs.dac.security.shiro.realms;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.security.enums.SecurityEntitiesState;
import com.ebs.dac.security.exceptions.BlockedAccountException;
import com.ebs.dac.security.exceptions.FirstFactorAuthenticationException;
import com.ebs.dac.security.exceptions.LoginAccessTypeForbiddenException;
import com.ebs.dac.security.exceptions.NoUserAccountFoundException;
import com.ebs.dac.security.exceptions.UnauthenticatedForFirstFactorException;
import com.ebs.dac.security.jpa.entities.Authenticator;
import com.ebs.dac.security.jpa.entities.PermissionAssignment;
import com.ebs.dac.security.jpa.entities.Role;
import com.ebs.dac.security.jpa.entities.RoleAssignment;
import com.ebs.dac.security.jpa.entities.User;
import com.ebs.dac.security.jpa.entities.UserAccount;
import com.ebs.dac.security.jpa.entities.UserAssignment;
import com.ebs.dac.security.jpa.repositories.UserAccountRepository;
import com.ebs.dac.security.jpa.repositories.UserAssignmentRepository;
import com.ebs.dac.security.shiro.AuthorizationConditionHandler;
import com.ebs.dac.security.shiro.filters.twofactorauth.SecurityConfigurationHandler;
import com.ebs.dac.security.shiro.interfaces.SecurityConfigurationHandlerAware;
import com.ebs.dac.security.shiro.matchers.BDKPasswordCredentialsMatcher;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.ebs.dac.security.shiro.models.adapters.SecurityConfigurationAdapter;
import java.util.List;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The responsible realm for getting the correct {@link AuthenticationInfo} and {@link
 * AuthorizationInfo} from DB. The read info will be matched against the passed token.
 */
public class BDKSecurityRealm extends AuthorizingRealm
    implements SecurityConfigurationHandlerAware {

  private static final Logger logger = LoggerFactory.getLogger(BDKSecurityRealm.class);

  private String origin; // either local or remote

  private SecurityConfigurationHandler securityConfigurationHandler;

  private UserAccountRepository userAccountRepository;

  private UserAssignmentRepository userAssignmentRepository;

  private String companyCode;

  public BDKSecurityRealm() {}

  public BDKSecurityRealm(
      UserAccountRepository userAccountRepository,
      UserAssignmentRepository userAssignmentRepository) {
    this.userAccountRepository = userAccountRepository;
    this.userAssignmentRepository = userAssignmentRepository;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  @Override
  public boolean supports(AuthenticationToken token) {
    return token != null && UsernamePasswordToken.class.isAssignableFrom(token.getClass());
  }

  @Override
  public Class getAuthenticationTokenClass() {
    return UsernamePasswordToken.class;
  }

  public boolean isPermitted(
      PrincipalCollection principals, String permission, BusinessObject entity) throws Exception {
    Permission p = getPermissionResolver().resolvePermission(permission);
    return isPermitted(principals, p, entity);
  }

  public boolean isPermitted(
      PrincipalCollection principals, Permission permission, BusinessObject entity)
      throws Exception {
    EBSAuthorizationInfo info = (EBSAuthorizationInfo) getAuthorizationInfo(principals);
    return isPermitted(permission, info) && isValidAuthorizationCondition(info, entity);
  }

  private boolean isValidAuthorizationCondition(EBSAuthorizationInfo info, BusinessObject entity)
      throws Exception {
    if (info.getAuthorizationConditions() == null || info.getAuthorizationConditions().isEmpty()) {
      return true;
    }
    AuthorizationConditionHandler conditionHandler = new AuthorizationConditionHandler();
    return conditionHandler.isValidAuthorizationConditionOnEntity(
        info.getAuthorizationConditions(), entity);
  }

  @Override
  protected void onInit() {
    super.onInit();
    setCredentialsMatcher(new BDKPasswordCredentialsMatcher());
  }

  /**
   * To get the correct {@link AuthenticationInfo} from DB by reading the user account (active) with
   * his password to be matched later against the passed {@link AuthenticationToken} using {@link
   * BDKPasswordCredentialsMatcher}. <br>
   * Note that if the user account has a security configuration (local/remote depending on the
   * application instance) is LOCKED then the access is forbidden.
   */
  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken)
      throws AuthenticationException {

    UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authToken;
    String username = usernamePasswordToken.getUsername();

    logger.debug("Trying to find the account of the username" + username);

    UserAccount account;
    try {
      logger.info("User and CompanyCode " + username + "," + companyCode);
      account = userAccountRepository.getUserAccount(username, companyCode);
    } catch (Exception e) {
      throw new UnauthenticatedForFirstFactorException(e);
    }

    if (account == null) {
      throw new NoUserAccountFoundException(username, companyCode);
    } else if (!SecurityEntitiesState.isActiveState(account.getAccountState())) {
      throw new BlockedAccountException(username, companyCode);
    }

    User accountUser = account.getUserId();

    String configurationsString = account.getAccountConfiguration();

    Authenticator authenticator = account.getSecondAuthenticator();
    String authenticationName = null;

    if (authenticator != null) {
      authenticationName = authenticator.getAuthenticationProvider();
    }

    EBSPrincipal principal =
        new EBSPrincipal(
            accountUser.getId(),
            accountUser.getUsername(),
            accountUser.getSysFlag(),
            account.getCompanyCode(),
            authenticationName);

    SecurityConfiguration configToMerge = null;
    if (configurationsString != null) {
      List<SecurityConfiguration> configurations =
          parseConfigurationsStringToList(configurationsString);
      principal.setSavedConfiguration(configurations);

      configToMerge = getCurrentSecurityConfiguration(configurations);
      if (!SecurityEntitiesState.isActiveState(configToMerge.getConfigurationState())) { // current
        // access
        // attempt
        // is
        // locked(e.g.
        // lock
        // remote
        // access)
        throw new LoginAccessTypeForbiddenException(origin);
      }
    }
    SecurityConfiguration userMergedConfiguration =
        securityConfigurationHandler.calculateConfiguration(configToMerge);
    principal.setPrimaryConfiguration(userMergedConfiguration);

    return new SimpleAuthenticationInfo(principal, accountUser.getPassword(), getName());
  }

  /**
   * This method is overridden only to throw our Exception type which is {@link
   * FirstFactorAuthenticationException}.
   */
  @Override
  protected void assertCredentialsMatch(AuthenticationToken token, AuthenticationInfo info)
      throws AuthenticationException {
    try {
      super.assertCredentialsMatch(token, info);
    } catch (IncorrectCredentialsException e) {
      throw new FirstFactorAuthenticationException(e);
    }
  }

  /**
   * The active Security Configuration currently is based on the {@link
   * SecurityConfiguration#getSecurityConfigurationType()} with values from {@link
   * SecurityConfiguration.SecurityConfigurationType}
   *
   * @param configurations
   * @return
   */
  private SecurityConfiguration getCurrentSecurityConfiguration(
      List<SecurityConfiguration> configurations) {

    if (origin != null && !origin.equals("")) {
      for (SecurityConfiguration configuration : configurations) {

        if (configuration.getSecurityConfigurationType().name().equalsIgnoreCase(origin)) {
          return configuration;
        }
      }
    }
    return null;
  }

  private List<SecurityConfiguration> parseConfigurationsStringToList(String configurationsString) {
    return SecurityConfigurationAdapter.deserializeConfigurationsStringToList(configurationsString);
  }

  // =========================================
  // ============ Authorization ==============
  // =========================================
  /**
   * To read the {@link AuthorizationInfo} from the DB by reading user's roles then permissions
   * assigned to these roles, then reading the parent's roles and their permissions (role hierarchy)
   */
  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    logger.trace("BDKSecurityRealm.doGetAuthorizationInfo()");

    String username = getUsernameFromPrincipals(principals);
    String company = getCompanyCodeOfPrincipal(principals);

    logger.info("Getting AuthorizationInfo for Username: " + username + ", Company: " + company);
    EBSAuthorizationInfo authInfo = new EBSAuthorizationInfo();
    List<UserAssignment> userRolesAssignments =
        userAssignmentRepository.getUserRoles(
            username, company, SecurityEntitiesState.ACTIVE_STATE);
    for (UserAssignment userRoleAssignment : userRolesAssignments) {
      Role role = userRoleAssignment.getRoleId();
      addUniqueChildrenRoles(role, authInfo);
    }
    return authInfo;
  }

  private void addUniqueChildrenRoles(Role role, EBSAuthorizationInfo authInfo) {
    if (role == null) return;

    if (authInfo.getRoles() != null && authInfo.getRoles().contains(role.getRoleExpression()))
      return;

    authInfo.addRole(role);
    List<PermissionAssignment> permsAssign = role.getPermissionAssignmentList();
    for (PermissionAssignment permAssign : permsAssign) {
      Permission permission = permAssign.getPermissionId();
      if (permAssign.getAuhtorizationCondition() != null) {
        authInfo.addAuthorizationCondition(permAssign.getAuhtorizationCondition());
      }
      authInfo.addObjectPermission(permission);
    }

    List<RoleAssignment> parentRoles = role.getParentRoles();
    if (parentRoles != null && !parentRoles.isEmpty()) {
      for (RoleAssignment parentRole : parentRoles) {
        addUniqueChildrenRoles(parentRole.getRoleId(), authInfo);
      }
    }
  }

  @Override
  protected Object getAuthorizationCacheKey(PrincipalCollection principals) {
    String username = getUsernameFromPrincipals(principals);
    String company = getCompanyCodeOfPrincipal(principals);
    return username + ":" + company;
  }

  private String getCompanyCodeOfPrincipal(PrincipalCollection principals) {
    return ((EBSPrincipal) principals.getPrimaryPrincipal()).getCompany();
  }

  private String getUsernameFromPrincipals(PrincipalCollection principals) {
    return ((EBSPrincipal) principals.getPrimaryPrincipal()).getUsername();
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public void setSecurityConfigurationHandler(
      SecurityConfigurationHandler securityConfigurationHandler) {
    this.securityConfigurationHandler = securityConfigurationHandler;
  }
}
