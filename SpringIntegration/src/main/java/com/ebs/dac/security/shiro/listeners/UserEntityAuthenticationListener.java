package com.ebs.dac.security.shiro.listeners;

import com.ebs.dac.security.shiro.IGlobalUsersHandler;
import com.ebs.dac.security.shiro.filters.ExcessiveAttemptsAwareFormAuthenticationFilter;
import com.ebs.dac.security.shiro.filters.twofactorauth.models.AuthenticationFactorInfo;
import com.ebs.dac.security.shiro.filters.twofactorauth.models.ExpiryDateAuthenticationFactorInfo;
import com.ebs.dac.security.shiro.filters.twofactorauth.models.OneTimerAuthenticationFactorInfo;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.ebs.dac.security.shiro.session.SessionAttributeKeys;
import com.ebs.dac.security.shiro.twofactorauth.SecondFactorAuthAttempts;
import com.ebs.dac.security.shiro.twofactorauth.SecondFactorToken;
import com.ebs.dac.security.usermanage.utils.SessionVariableUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationListener;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listens on user successful and failed authentication attempts. Currently it is responsible for
 * counter for login attempts (first and second) and 2nd-Factor info validity.<br>
 */
public class UserEntityAuthenticationListener implements AuthenticationListener {

  private static final Logger logger =
      LoggerFactory.getLogger(UserEntityAuthenticationListener.class);

  private IGlobalUsersHandler globalUserHandler;

  public UserEntityAuthenticationListener() {}

  /**
   * Triggers when user has successfully authenticated (by first or second factor).<br>
   * It is responsible for:<br>
   * - Resetting the number of wrong login attempts when user successfully authenticates (first or
   * second factor).<br>
   * - In case of 2nd-Factor login, An {@link AuthenticationFactorInfo} object is instantiated based
   * on the type of 2nd-Factor info validity defined in {@link SecurityConfiguration} in {@link
   * 'SecondFactorCredentialValidity'}. Other types of authentication info can be {@link
   * ExpiryDateAuthenticationFactorInfo}, or {@link OneTimerAuthenticationFactorInfo}<br>
   */
  public void onSuccess(AuthenticationToken token, AuthenticationInfo info) {
    logger.debug("Successful Subject authentication notified.");

    try {
      EBSPrincipal principal = (EBSPrincipal) info.getPrincipals().getPrimaryPrincipal();
      Session session = SecurityUtils.getSubject().getSession();
      String factorAuthenticator = principal.getSecondFactorMeanName();
      SecurityConfiguration conf = principal.getPrimaryConfiguration();

      if (session != null) {
        session.removeAttribute("LOGIN_ERROR_MESSAGE");
      }

      AuthenticationType authenticationType = determineAuthenticationType(token);
      if (authenticationType == AuthenticationType.FirstAuthentication) {
        String username = principal.getUsername();
        session.setAttribute(
            SessionAttributeKeys.USERNAME,
            username); // so that when the session is deleted or expired
        // I remove the corresponding subject from logged in users list
        SessionVariableUtils.resetCounter(session, SessionAttributeKeys.LOGIN_ATTEMPTS);

      } else if (authenticationType == AuthenticationType.SecondAuthentication) {

        AuthenticationFactorInfo verifiedToken;

        if (conf.getSecondFactorCredentialValidity().equals(0)) {
          logger.debug("Generating session-long-valid verified second factor credentials");
          verifiedToken = new AuthenticationFactorInfo(info, factorAuthenticator);
        } else if (conf.getSecondFactorCredentialValidity().equals(-1)) {
          logger.debug("Generating one-use verified second factor credentials");
          verifiedToken = new OneTimerAuthenticationFactorInfo(info, factorAuthenticator);
        } else {
          int validityOfCredentialsInSeconds = conf.getSecondFactorCredentialValidity();
          DateTime expiryDate = new DateTime().now().plusSeconds(validityOfCredentialsInSeconds);
          logger.debug(
              "Generating verified second factor credentials that expires in " + expiryDate);
          verifiedToken =
              new ExpiryDateAuthenticationFactorInfo(info, factorAuthenticator, expiryDate);
        }

        session.setAttribute(
            SessionAttributeKeys.SECOND_FACTOR_CREDENTIAL_VERIFIED_TOKEN, verifiedToken);
        session.setAttribute(
            SessionAttributeKeys.TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO, null);
      }
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
    }
  }

  /**
   * Triggers when the user fails to authenticate.<br>
   * Currently it is responsible for:<br>
   * - Incrementing the number of wrong login attempts (for first or second factor authentication)
   * to prevent exceeding the maximum number of wrong login attempts. This number (for the first
   * login) is defined in {@link
   * ExcessiveAttemptsAwareFormAuthenticationFilter#maxLoginTrialsCount}. while for the 2nd-Factor
   * login it is not handled.<br>
   */
  public void onFailure(AuthenticationToken token, AuthenticationException e) {
    String logMessage = "";

    Session session = SecurityUtils.getSubject().getSession();

    if (session != null) {
      session.setAttribute(
          "LOGIN_ERROR_MESSAGE", "Incorrect username or password. Please try again");
    }

    AuthenticationType authenticationType = determineAuthenticationType(token);
    if (authenticationType == AuthenticationType.FirstAuthentication) {
      logMessage += "First Factor Authentication failure for " + token.getPrincipal();

      int currentFailedAttempts =
          SessionVariableUtils.incrementCounter(session, SessionAttributeKeys.LOGIN_ATTEMPTS);

      logMessage +=
          ", current failed login attempts for this factor (for current session only) are "
              + currentFailedAttempts;

    } else if (authenticationType == AuthenticationType.FirstAuthentication) {
      logMessage += "Second Factor Authentication failure for " + token.getPrincipal();
      SecondFactorAuthAttempts secondFactorAuthAttemptsInfo =
          (SecondFactorAuthAttempts)
              session.getAttribute(
                  SessionAttributeKeys.TWO_FACTOR_GENERATED_CREDENTIALS_LOGIN_ATTEMPTS_INFO);

      int currentFailedAttempts = secondFactorAuthAttemptsInfo.incrementLoginAttempt();

      logMessage +=
          ", current failed login attempts for this factor (for current session only) are "
              + currentFailedAttempts;
    }

    logger.info(logMessage);
  }

  /**
   * Handles removing user from loggedin users list (moved from {@link SessionsListener} as session
   * attributes were removed there in case of logout).
   */
  public void onLogout(PrincipalCollection principals) {

    EBSPrincipal principal = (EBSPrincipal) principals.getPrimaryPrincipal();
    logger.info(
        "UserEntityAuthenticationListener.onLogout: removing principal->username "
            + principal.getUsername()
            + " from system-wide list of currently logged in users");
    if (principal != null) {
      globalUserHandler.removeUserFromLoggedInUsers(principal.getUsername());
    }

    logger.debug("Subject logout notified.");
  }

  private AuthenticationType determineAuthenticationType(AuthenticationToken token) {
    if (UsernamePasswordToken.class.isAssignableFrom(
        token.getClass())) { // username password means that it is first-factor login
      return AuthenticationType.FirstAuthentication;
    }
    if (SecondFactorToken.class.isAssignableFrom(
        token.getClass())) { // e.g. otp, that means that it is second-factor login
      return AuthenticationType.SecondAuthentication;
    }
    return null;
  }

  public IGlobalUsersHandler getGlobalUserHandler() {
    return globalUserHandler;
  }

  public void setGlobalUserHandler(IGlobalUsersHandler globalUserHandler) {
    this.globalUserHandler = globalUserHandler;
  }

  /** States whether it is a first-factor successful login or second-factor successful login */
  private enum AuthenticationType {
    FirstAuthentication,
    SecondAuthentication
  }
}
