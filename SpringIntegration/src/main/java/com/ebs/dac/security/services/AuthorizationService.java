package com.ebs.dac.security.services;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.apis.security.IAuthorizationService;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.security.enums.SecurityEntitiesState;
import com.ebs.dac.security.jpa.entities.*;
import com.ebs.dac.security.jpa.repositories.AuthorizationConditionRepository;
import com.ebs.dac.security.jpa.repositories.UserAssignmentRepository;
import com.ebs.dac.security.jpa.repositories.UserPermissionGeneralModelRepository;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.EBSSubject;
import com.ebs.dac.security.utils.AuthorizationUtils;
import org.apache.shiro.SecurityUtils;

import java.util.*;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 29, 2018 9:52:37 AM
 */
public class AuthorizationService implements IAuthorizationService {

  private UserAccountSecurityService userAccountSecurityService;
  private UserAssignmentRepository userAssignmentRepository;
  private Map<String, Set<String>> objectsDefaultActions;
  private Set<String> stateMachineAllowedActions;
  private UserPermissionGeneralModelRepository userPermissionGeneralModelRepository;
  private AuthorizationConditionRepository authorizationConditionRepository;
  private AuthorizationManager authorizationManager;

  public AuthorizationService(
          UserAccountSecurityService userAccountSecurityService,
          UserAssignmentRepository userAssignmentRepository,
          UserPermissionGeneralModelRepository userPermissionGeneralModelRepository,
          AuthorizationConditionRepository authorizationConditionRepository,
          AuthorizationManager authorizationManager) {
    this.userAccountSecurityService = userAccountSecurityService;
    this.userAssignmentRepository = userAssignmentRepository;
    objectsDefaultActions = new HashMap<>();
    this.userPermissionGeneralModelRepository = userPermissionGeneralModelRepository;
    this.authorizationConditionRepository = authorizationConditionRepository;
    this.authorizationManager = authorizationManager;
  }

  @Override
  public <T extends BusinessObject> Set<String> getCurrentUserAuthorizedActions(T entity)
          throws Exception {

    List<Long> validConditions = getEntityAuthorizationConditions(entity);

    List<UserPermissionGeneralModel> userPermissions;
    if (validConditions.isEmpty()) {
      userPermissions =
              this.userPermissionGeneralModelRepository.findAllUserPermissionWithOutConditions(
                      getAuthorizedUser().getUsername(), entity.getSysName());
    } else {
      userPermissions =
              this.userPermissionGeneralModelRepository.findAllUserPermissionWithConditions(
                      validConditions, getAuthorizedUser().getUsername(), entity.getSysName());
    }

    Set<String> allowedAction = getAllowedAction(userPermissions);

    isAllPermissionsAllowed(allowedAction);

    return allowedAction;
  }

  @Override
  public Set<String> getClassAuthorizedActions(String className) {

    List<UserPermissionGeneralModel> userPermissions =
            this.userPermissionGeneralModelRepository.findAllUserPermissionWithOutConditions(
                    getAuthorizedUser().getUsername(), className);

    return getAllowedAction(userPermissions);
  }

  private EBSPrincipal getAuthorizedUser() {

    EBSSubject subject = (EBSSubject) SecurityUtils.getSubject();

    return (EBSPrincipal) subject.getPrincipals().getPrimaryPrincipal();
  }

  private <T extends BusinessObject> List<Long> getEntityAuthorizationConditions(T entity)
          throws Exception {

    List<AuthorizationCondition> authorizationConditions =
            this.authorizationConditionRepository.findAllByObjectName(entity.getSysName());

    List<Long> validConditions = new ArrayList<>();

    if (!entity.isNull()) {
      for (AuthorizationCondition authorizationCondition : authorizationConditions) {
        boolean conditionPassed =
                this.authorizationManager.isConditionPassed(
                        entity, authorizationCondition.getCondition());
        if (conditionPassed) validConditions.add(authorizationCondition.getId());
      }
    }

    return validConditions;
  }

  private Set<String> getAllowedAction(List<UserPermissionGeneralModel> userPermissions) {
    Set<String> allowedActions = new HashSet<>();
    for (UserPermissionGeneralModel userPermission : userPermissions) {
      String actionsString =
              userPermission.getPermissionExpression()
                      .split(AuthorizationUtils.PERMISSION_SEPERATOR)[1];
      String[] actions = actionsString.split(AuthorizationUtils.PERMISSION_ACTIONS_SEPERATOR);
      allowedActions.addAll(Arrays.asList(actions));
    }
    return allowedActions;
  }

  @Override
  public void setStateMachineActions(Set<String> stateMachineAllowedActions) {
    this.stateMachineAllowedActions = stateMachineAllowedActions;
  }

  /** this for registration custom object default actions */
  public void registerObjectDefaultActions(String objectSysName, Set<String> actionsNames) {
    objectsDefaultActions.put(objectSysName, actionsNames);
  }

  private Set<String> getCurrentUserAuthorizedActions(String objectSysName) {
    Set<String> authorizedActions = new HashSet<>();

    IBDKUser user = userAccountSecurityService.getLoggedInUser();
    Set<Permission> permissionsList = getPermissionsList(user);

    for (Permission permission : permissionsList) {
      if (isTargetObject(objectSysName, permission)) {
        authorizedActions.addAll(Arrays.asList(permission.getActions()));
      }
    }

    return authorizedActions;
  }

  private boolean isTargetObject(String objectSysName, Permission permission) {
    return objectSysName.equals(permission.getObjectName())
            || permission.getObjectName().equals(AuthorizationUtils.WILD_CARD_PERMISSION);
  }

  public boolean isAdmin() {
    IBDKUser user = userAccountSecurityService.getLoggedInUser();
    Set<Permission> permissionsList = getPermissionsList(user);

    for (Permission permission : permissionsList) {
      if (permission.getPermissionExpression().equals(AuthorizationUtils.ADMIN_PERMISSION)) {
        return true;
      }
    }
    return false;
  }

  private <T extends BusinessObject> void isAllPermissionsAllowed(Set<String> authorizedActions) {
    if (authorizedActions.contains(AuthorizationUtils.WILD_CARD_PERMISSION)) {
      authorizedActions.clear();
      authorizedActions.add(AuthorizationUtils.WILD_CARD_PERMISSION);
    }
  }

  private Set<Permission> getPermissionsList(IBDKUser user) {

    String username = user.getUsername();
    String company = user.getCompany();

    Set<Permission> permissions = new HashSet<>();
    List<UserAssignment> userRolesAssignments =
            userAssignmentRepository.getUserRoles(
                    username, company, SecurityEntitiesState.ACTIVE_STATE);

    for (UserAssignment userRoleAssignment : userRolesAssignments) {
      Role role = userRoleAssignment.getRoleId();
      addUniqueChildrenRoles(role, permissions);
    }
    return permissions;
  }

  private void addUniqueChildrenRoles(Role role, Set<Permission> permissions) {
    if (role == null) {
      return;
    }

    List<PermissionAssignment> permsAssign = role.getPermissionAssignmentList();
    for (PermissionAssignment permAssign : permsAssign) {
      Permission permission = permAssign.getPermissionId();
      permissions.add(permission);
    }

    List<RoleAssignment> parentRoles = role.getParentRoles();
    if (parentRoles != null && !parentRoles.isEmpty()) {
      for (RoleAssignment parentRole : parentRoles) {
        addUniqueChildrenRoles(parentRole.getRoleId(), permissions);
      }
    }
  }
}
