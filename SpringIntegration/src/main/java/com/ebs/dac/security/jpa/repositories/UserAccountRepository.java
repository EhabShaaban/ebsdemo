package com.ebs.dac.security.jpa.repositories;

import com.ebs.dac.security.jpa.entities.UserAccount;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserAccountRepository extends CrudRepository<UserAccount, Long> {

  @Query(
      "SELECT u FROM UserAccount u WHERE u.companyCode = :companyCode AND u.userId.username = :username")
  UserAccount getUserAccount(
      @Param("username") String username, @Param("companyCode") String companyCode);

  @Query(
      "SELECT u FROM UserAccount u WHERE u.companyCode = :companyCode AND u.userId.username = :username"
          + " AND u.accountState = :accountState")
  UserAccount getActiveUserAccount(
      @Param("username") String username,
      @Param("companyCode") String companyCode,
      @Param("accountState") String accountState);
}
