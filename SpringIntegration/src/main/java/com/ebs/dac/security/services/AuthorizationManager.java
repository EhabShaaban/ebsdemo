package com.ebs.dac.security.services;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.condition.adapter.ConditionManager;
import com.ebs.dac.foundation.realization.condition.resolvers.EntityAttributeResolverContext;
import com.ebs.dac.foundation.realization.processing.DataObjectProcessor;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.entities.AuthorizationGeneralModel;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.models.EBSSubject;
import org.apache.shiro.SecurityUtils;

import java.util.*;

public class AuthorizationManager {

  private AuthorizationGeneralModelRepository authorizationGeneralModelRepository;
  private ConditionManager conditionManager;
  private Set<String> permissionConditions;
  private String WILD_CARD_PERMISSION = "*";

  public AuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationGeneralModelRepository = authorizationGeneralModelRepository;
    conditionManager = new ConditionManager();
  }

  public Set<String> getPermissionConditions() {
    return new HashSet<>(this.permissionConditions);
  }

  public void authorizeAction(String objectSysName, String action) {

    EBSPrincipal principal = getEbsPrincipal();

    String permissionExpression = objectSysName + ":" + action;
    this.permissionConditions =
        getUserPermissionConditions(objectSysName, permissionExpression, principal);
    checkIfPermissionExists(permissionConditions);
  }

  public void authorizeAction(BusinessObject businessObject, String action) throws Exception {
    if (!isConditionApplied(businessObject, action)) {
      throwConditionAuthorizationException(businessObject);
    }
  }

  public void authorizeActionOnObject(BusinessObject businessObject, String action)
          throws Exception {
    checkIfEntityIsNull(businessObject);

    if (!isConditionApplied(businessObject, action)) {
      throw new AuthorizationException();
    }
  }

  public boolean isConditionMatch(String objectName, String permission, String targetCondition) {
    EBSPrincipal principal = getEbsPrincipal();
    String permissonExpresssion = objectName + ":" + permission;

    this.permissionConditions =
        getUserPermissionConditions(objectName, permissonExpresssion, principal);
    checkIfPermissionExists(permissionConditions);

    for (String userCondition : permissionConditions) {
      if (userCondition.equals(WILD_CARD_PERMISSION) || userCondition.equals(targetCondition)) {
        return true;
      }
    }
    throw new AuthorizationException();
  }

  private void throwConditionAuthorizationException(BusinessObject businessObject) {
    throw new ConditionalAuthorizationException(
            "Subject Does Not Have Permission On "
                    + businessObject.getSysName()
                    + "  when "
                    + String.join(",", permissionConditions));
  }

  private boolean isConditionApplied(BusinessObject businessObject, String permission)
      throws Exception {
    EBSPrincipal principal = getEbsPrincipal();
    String permissionExpression = businessObject.getSysName() + ":" + permission;
    this.permissionConditions =
            getUserPermissionConditions(businessObject.getSysName(), permissionExpression, principal);
    checkIfPermissionExists(permissionConditions);
    return isConditionPassed(businessObject, permissionConditions);
  }

  private EBSPrincipal getEbsPrincipal() {
    EBSSubject subject = (EBSSubject) SecurityUtils.getSubject();
    return (EBSPrincipal) subject.getPrincipals().getPrimaryPrincipal();
  }

  private boolean isConditionPassed(BusinessObject businessObject, Set<String> conditions)
      throws Exception {
    boolean isOneConditionPassed = false;
    for (String condition : conditions) {
      if (condition.equals(WILD_CARD_PERMISSION) || executeCondition(condition, businessObject)) {
        isOneConditionPassed = true;
        break;
      }
    }
    return isOneConditionPassed;
  }

  public boolean isConditionPassed(BusinessObject businessObject, String condition)
      throws Exception {
    boolean isOneConditionPassed = false;
    if (condition.equals(WILD_CARD_PERMISSION) || executeCondition(condition, businessObject)) {
      isOneConditionPassed = true;
    }
    return isOneConditionPassed;
  }

  private void checkIfPermissionExists(Set<String> permissionConditions) {
    if (permissionConditions.isEmpty()) {
      throw new AuthorizationException();
    }
  }

  private Set<String> getUserPermissionConditions(
          String objectSysName, String permission, EBSPrincipal principal) {
    Set<String> permissionConditions = new HashSet<>();
    String objectWildCardPermission = objectSysName + ":*";
    String wildcardPermission = "*:*";
    List<String> permissions =
            new ArrayList<>(Arrays.asList(permission, wildcardPermission, objectWildCardPermission));

    List<AuthorizationGeneralModel> userPermissions =
            this.authorizationGeneralModelRepository.findByUserNameAndPermissionExpressionIn(
                    principal.getUsername(), permissions);
    for (AuthorizationGeneralModel userPermission : userPermissions) {
      String permissionExp = userPermission.getPermissionExpression();
      String conditionExpression = userPermission.getCondition();
      if (conditionExpression == null) {
        conditionExpression = WILD_CARD_PERMISSION;
      }
      if (permissionExp.equals(permission)
              || permissionExp.equals(objectWildCardPermission)
              || permissionExp.equals(wildcardPermission)) {
        permissionConditions.add(conditionExpression);
      }
    }
    return permissionConditions;
  }

  private boolean executeCondition(String condition, BusinessObject entity) throws Exception {

    Set<String> conditionVariables = conditionManager.getConditionVariables(condition);
    if (conditionVariables == null) {
      throw new IllegalArgumentException("Invalid Authorization Condition: " + condition);
    }

    Map<String, Object> objectAttributeMap = buildObjectAttributeMap(conditionVariables, entity);
    isAllConditionAttributesExist(conditionVariables, objectAttributeMap);
    return executeCondition(condition, objectAttributeMap);
  }

  private boolean executeCondition(String condition, Map<String, Object> objectAttributeMap)
          throws Exception {
    Map<String, Object> resolvedReservedWords = new HashMap<>();
    resolvedReservedWords.put("LoggedInUser", getEbsPrincipal().getUsername());
    conditionManager.setResolvedreservedWords(resolvedReservedWords);
    return conditionManager.executeCondition(objectAttributeMap, condition);
  }

  private void isAllConditionAttributesExist(
          Set<String> conditionVariables, Map<String, Object> objectAttributeMap) {
    for (String attrName : conditionVariables) {
      if (!objectAttributeMap.containsKey(attrName)) {
        throw new ConditionalAuthorizationException(attrName + " can't be resolved");
      }
    }
  }

  private Map<String, Object> buildObjectAttributeMap(
          Set<String> conditionVariables, BusinessObject entity) throws Exception {
    DataObjectProcessor processor = new DataObjectProcessor(entity);
    Map<String, Object> attributesValuesMap =
            processor.getAttributesValuesMap(entity, conditionVariables);

    EntityAttributeResolverContext attributesResolver = new EntityAttributeResolverContext();
    Map<String, Object> resolvedMap = attributesResolver.resolveAttributes(attributesValuesMap);

    return resolvedMap;
  }

  private void checkIfEntityIsNull(BusinessObject businessObject) throws Exception {
    if (businessObject == null) {
      throw new InstanceNotExistException();
    }
  }
}
