package com.ebs.dac.security.shiro.twofactorauth;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * This class is created to contain information related to the count of failed login attempts done
 * on every system-generated credentials, as well as the total count of all of them.<br>
 * <br>
 * Every new {@link GenerateCredentialInfo} generated by {@link
 * SecondFactorAuthenticator#generateCredentials(javax.servlet.ServletRequest)} should be added here
 * so the count can be tracked, with the last one added considered being the Active one which a call
 * to {@link #incrementLoginAttempt()} affects its counter.
 *
 * @author Hesham Saleh
 */
public class SecondFactorAuthAttempts {

  /** The last one is the currently active one. The previous ones are which got invalidated */
  LinkedHashMap<GenerateCredentialInfo, Integer> generatedCredentialsAndAuthCount =
      new LinkedHashMap<GenerateCredentialInfo, Integer>();

  public SecondFactorAuthAttempts() {}

  public SecondFactorAuthAttempts(GenerateCredentialInfo initialGenerateCredentialInfo) {
    addGenerateCredentialInfo(initialGenerateCredentialInfo);
  }

  public int getTotalAuthAttemptsCount() {
    int total = 0;
    for (Integer loginAttemp : generatedCredentialsAndAuthCount.values()) {
      total += loginAttemp;
    }
    return total;
  }

  public int getAuthAttemptsForActiveCredential() {
    return getActiveGeneratedCredentialInfoAndAuthCountEntry().getValue();
  }

  /**
   * Increment the login attempts for the currently active Generated Credential Info (being matched
   * against)
   *
   * @throws @{@link SecondFactorAuthAttempts.NoActiveCredentialException} Happens after a call to
   *     {@link #invalidateActiveGenerateCredentialInfo()} without {@link
   *     #addGenerateCredentialInfo(GenerateCredentialInfo)}. In other words, {@link #isValid()}
   *     returns false
   */
  public int incrementLoginAttempt() {
    // Get the last one
    Entry<GenerateCredentialInfo, Integer> lastOne =
        getActiveGeneratedCredentialInfoAndAuthCountEntry();
    Integer updatedCounter = new Integer(lastOne.getValue() + 1);
    lastOne.setValue(updatedCounter);
    return updatedCounter;
  }

  /**
   * Registers a new {@link GenerateCredentialInfo} marking it as the currently active.
   *
   * @param info
   */
  public void addGenerateCredentialInfo(GenerateCredentialInfo info) {
    generatedCredentialsAndAuthCount.put(info, new Integer(0));
  }

  public void invalidateActiveGenerateCredentialInfo() {
    getActiveGeneratedCredentialInfoAndAuthCountEntry().getKey().invalidate();
  }

  /**
   * @return False if either the active GenerateCredentialInfo got invalidated via {@link
   *     #invalidateActiveGenerateCredentialInfo()}, or if there is no active one.
   */
  public boolean isValid() {
    Entry<GenerateCredentialInfo, Integer> lastOne =
        getActiveGeneratedCredentialInfoAndAuthCountEntry();
    return lastOne != null && lastOne.getKey().isValid();
  }

  public boolean isEmpty() {
    return generatedCredentialsAndAuthCount.isEmpty();
  }

  public GenerateCredentialInfo getActiveGeneratedCredentialInfo() {
    return getActiveGeneratedCredentialInfoAndAuthCountEntry().getKey();
  }

  private Entry<GenerateCredentialInfo, Integer>
      getActiveGeneratedCredentialInfoAndAuthCountEntry() {
    Entry<GenerateCredentialInfo, Integer> lastOne = null;
    Iterator<Entry<GenerateCredentialInfo, Integer>> itr =
        generatedCredentialsAndAuthCount.entrySet().iterator();
    while (itr.hasNext()) lastOne = itr.next();
    return lastOne;
  }

  public class NoActiveCredentialException extends RuntimeException {
    private static final long serialVersionUID = 1L;
  }
}
