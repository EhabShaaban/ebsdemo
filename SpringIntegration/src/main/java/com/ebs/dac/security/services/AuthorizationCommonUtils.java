package com.ebs.dac.security.services;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;

public class AuthorizationCommonUtils {

  private AuthorizationManager authorizationManager;
  private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;

  public AuthorizationCommonUtils(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void applyAuthorizationForPurchaseUnit(String purchaseUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        cObPurchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    if (purchasingUnitGeneralModel != null) {
      try {
        authorizationManager.authorizeAction(purchasingUnitGeneralModel, IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  protected void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }

  public void setcObPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep) {
    this.cObPurchasingUnitGeneralModelRep = cObPurchasingUnitGeneralModelRep;
  }
}
