package com.ebs.dac.security.shiro.twofactorauth.otp;

import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.shiro.twofactorauth.GenerateCredentialInfo;
import com.ebs.dac.security.shiro.twofactorauth.SecondFactorAuthenticator;
import javax.servlet.ServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the filter to create the {@link OTPAuthenticationToken} from the request. Also it
 * generates the code if needed.
 */
public class OTPSecondFactorAuthenticator extends SecondFactorAuthenticator {

  private static final Logger logger = LoggerFactory.getLogger(OTPSecondFactorAuthenticator.class);

  public OTPSecondFactorAuthenticator(String factorName, String formUrl) {
    super(factorName, formUrl);
  }

  public OTPSecondFactorAuthenticator(String formUrl) {
    super("otp", formUrl);
  }

  /**
   * Not recommended to be used, made specifically for ini-based configuration where no constructor
   * initialization is supported
   */
  public OTPSecondFactorAuthenticator() {}

  /** Creates the {@link OTPAuthenticationToken} from the passed request. */
  @Override
  public AuthenticationToken createToken(ServletRequest request) {
    Subject subject = SecurityUtils.getSubject();
    String otpCode;

    otpCode = WebUtils.getCleanParam(request, DEFAULT_CREDENTIAL_PARAM);

    return new OTPAuthenticationToken((EBSPrincipal) subject.getPrincipal(), otpCode);
  }

  /**
   * Generates another otp code that will be valid for {@link
   * TimeBasedOneTimePassword#INTERVAL_PERIOD_SECONDS} seconds
   */
  @Override
  public GenerateCredentialInfo generateCredentials(ServletRequest request) {
    Subject subject = SecurityUtils.getSubject();

    String otpCode =
        TimeBasedOneTimePassword.generateTOTP(TimeBasedOneTimePassword.getCurrentTime());
    logger.info("Generated OTP Code is " + otpCode);
    // TODO This code should be sent to the user

    return new GenerateCredentialInfo(TimeBasedOneTimePassword.INTERVAL_PERIOD_SECONDS);
  }
}
