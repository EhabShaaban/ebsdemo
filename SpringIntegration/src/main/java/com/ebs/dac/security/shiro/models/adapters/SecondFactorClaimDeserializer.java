package com.ebs.dac.security.shiro.models.adapters;

import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

public class SecondFactorClaimDeserializer
    implements JsonDeserializer<SecurityConfiguration.SecurityClearnceLevel> {

  public SecurityConfiguration.SecurityClearnceLevel deserialize(
      JsonElement element, Type type, JsonDeserializationContext context)
      throws JsonParseException {

    SecurityConfiguration.SecurityClearnceLevel[] claimModes =
        SecurityConfiguration.SecurityClearnceLevel.values();
    for (SecurityConfiguration.SecurityClearnceLevel claimMode : claimModes) {
      if (claimMode.name().equalsIgnoreCase(element.getAsString())) return claimMode;
    }
    return null;
  }
}
