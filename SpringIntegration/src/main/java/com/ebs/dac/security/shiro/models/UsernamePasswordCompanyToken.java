package com.ebs.dac.security.shiro.models;

import org.apache.shiro.authc.UsernamePasswordToken;

public class UsernamePasswordCompanyToken extends UsernamePasswordToken {

  private static final long serialVersionUID = 1L;

  private String companyCode;
  private Long userId;
  private Boolean isSysPasswordFlag;

  public UsernamePasswordCompanyToken(
      String username, String password, Boolean isSysPasswordFlag, String companyCode,
      boolean rememberMe, String host) {
    super(username, password, rememberMe, host);
    this.companyCode = companyCode;
    this.isSysPasswordFlag = isSysPasswordFlag;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Boolean getSysPasswordFlag() {
    return isSysPasswordFlag;
  }

  @Override
  public Object getPrincipal() {
    return new EBSPrincipal(getUserId(), getUsername(), getSysPasswordFlag(), getCompanyCode());
  }
}
