package com.ebs.dac.security.shiro.filters.twofactorauth;

import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.ebs.dac.security.shiro.twofactorauth.SecondFactorAuthenticator;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This filter is not intended to protect a resource, but to append Second Factor Authentication
 * process to the First Factor. Use @link {@link SecondFactorAuthenticationDispatcherFilter} instead
 */
public class LoginSecondFactorAuthenticationDispatcherFilter
    extends SecondFactorAuthenticationDispatcherFilter {

  private static final Logger logger =
      LoggerFactory.getLogger(LoginSecondFactorAuthenticationDispatcherFilter.class);

  protected AuthenticatingFilter firstFactorLoginFilter;

  /**
   * @param firstFactorLoginFilter An AuthenticatingFilter that allows a request to continue the
   *     chain after successful authentication
   */
  public LoginSecondFactorAuthenticationDispatcherFilter(
      AuthenticatingFilter firstFactorLoginFilter) {
    super();
    this.firstFactorLoginFilter = firstFactorLoginFilter;
  }

  @Override
  public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue)
      throws Exception {

    Subject subject = SecurityUtils.getSubject();
    Session session = subject.getSession();

    if (!subject.isAuthenticated()) {
      return false;
    }

    SecurityConfiguration conf = getCurrentSubjectSecurityConfiguration();

    // Check if he has a valid 2nd factor code in his session
    if (assertSessionContainValidSecondFactorCredentials(session)) {
      logger.debug("Previously verified second factor credential is remembered");
      WebUtils.redirectToSavedRequest(request, response, firstFactorLoginFilter.getSuccessUrl());
      return false;
    }

    // ONLY Allow if the request is to one of the supported factor means URLs in a GET reques
    // ONLY Allow if the request intends to only show the login page of supported user's factor
    // login page

    if (isPOSTRequest(request) && isFirstFactorLoginURL(request)) {
      return handleRedirectionForSecondFactorAuthenticationFormURL(request, response, subject);
    }

    SecondFactorAuthenticator factorAuthenticator;
    if ((factorAuthenticator = isRequestForSecondFactorAuthenticatorFormURL(request)) != null) {
      if (isGETRequest(request)) {

        logger.debug(
            "Detected a GET request to the form of " + factorAuthenticator.getFactorName());

        return handleGETRequestForSecondFactorAuthenticatorFormURL(
            factorAuthenticator, request, response, session);
      } else if (isPOSTRequest(request)) {

        logger.debug(
            "Detected a POST request to the form of " + factorAuthenticator.getFactorName());

        return handlePostRequestForSecondFactorAuthenticatorFormURL(
            factorAuthenticator,
            request,
            response,
            session,
            firstFactorLoginFilter.getSuccessUrl());
      }
    }

    // TODO Calling a URL that this filter is not aware of, thus it won't allow the chain
    //		handleNoSecurityClearance(request, response);
    handleNoSecurityClearance();
    return false;
  }

  protected boolean isFirstFactorLoginURL(ServletRequest request) {
    return pathsMatch(firstFactorLoginFilter.getLoginUrl(), request);
  }

  protected boolean isFirstFactorLoginURL(String path) {
    return pathsMatch(firstFactorLoginFilter.getLoginUrl(), path);
  }
}
