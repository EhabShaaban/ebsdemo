package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** When maximum wrong attempts is exceeded and another attempt is made. */
public class MaximumWrongLoginAttemptsReachedException extends FirstFactorAuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;
  private static final String DEFAULT_MESSAGE = "Maximum wrong login attempts exceeded";
  private int loginAttempts;

  {
    ERROR_CODE = "LOGIN-MXMM_WRNG";
    STATUS_CODE = HttpStatusCode.LOCKED.code();
  }

  public MaximumWrongLoginAttemptsReachedException() {
    super(DEFAULT_MESSAGE);
  }

  public MaximumWrongLoginAttemptsReachedException(int loginAttempts) {
    this();
    this.loginAttempts = loginAttempts;
  }

  public int getLoginAttempts() {
    return loginAttempts;
  }

  public void setLoginAttempts(int loginAttempts) {
    this.loginAttempts = loginAttempts;
  }

  @Override
  public String toString() {
    return "You exceeded '" + loginAttempts + "' wrong login attempts.";
  }
}
