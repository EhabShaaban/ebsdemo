package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** The user sent wrong 2nd-Factor authentication info. */
public class SecondFactorAuthenticationException extends AuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  {
    ERROR_CODE = "ACESS-2NDF_EROR";
    STATUS_CODE = HttpStatusCode.UNAUTHORIZED.code();
  }

  public SecondFactorAuthenticationException() {
    super();
  }

  public SecondFactorAuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }

  public SecondFactorAuthenticationException(String message) {
    super(message);
  }

  public SecondFactorAuthenticationException(Throwable e) {
    super(e);
  }
}
