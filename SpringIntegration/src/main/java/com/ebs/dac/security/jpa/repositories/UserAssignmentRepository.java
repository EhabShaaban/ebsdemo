package com.ebs.dac.security.jpa.repositories;

import com.ebs.dac.security.jpa.entities.UserAssignment;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserAssignmentRepository extends CrudRepository<UserAssignment, Long> {

  @Query(
      "SELECT u FROM UserAssignment u WHERE u.assignmentState = :assignmentState AND "
          + "u.userId.companyCode=:companyCode AND u.userId.userId.username=:username")
  List<UserAssignment> getUserRoles(
      @Param("username") String username,
      @Param("companyCode") String companyCode,
      @Param("assignmentState") String assignmentState);
}
