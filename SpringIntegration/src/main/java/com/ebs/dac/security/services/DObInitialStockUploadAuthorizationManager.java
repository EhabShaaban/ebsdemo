package com.ebs.dac.security.services;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.jpa.repositories.AuthorizationGeneralModelRepository;
import com.ebs.dda.jpa.inventory.goodsreceipt.ICObStorekeeper;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUploadCreateValueObject;
import com.ebs.dda.jpa.inventory.initialstockupload.IDObInitialStockUpload;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;

public class DObInitialStockUploadAuthorizationManager {

  private AuthorizationManager authorizationManager;
  private AuthorizationCommonUtils authorizationCommonUtils;
  private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;

  public DObInitialStockUploadAuthorizationManager(
      AuthorizationGeneralModelRepository authorizationGeneralModelRepository) {
    this.authorizationManager = new AuthorizationManager(authorizationGeneralModelRepository);
    authorizationCommonUtils = new AuthorizationCommonUtils(authorizationManager);
  }

  public AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(
        ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL, "ReadStoreHouses");
    permissionsConfig.addPermissionConfig(
        ICObStorekeeper.SYS_NAME, IActionsNames.READ_ALL, "ReadStoreKeepers");
    permissionsConfig.addPermissionConfig(
        ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadCompanies");
    permissionsConfig.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME,
        IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER,
        "ReadBusinessUnits");

    return permissionsConfig;
  }

  public void applyAuthorizationOnCreateAction(
      DObInitialStockUploadCreateValueObject initialStockUploadCreateValueObject) throws Exception {
    authorizationManager.authorizeAction(IDObInitialStockUpload.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IDObInitialStockUpload.SYS_NAME, IActionsNames.CREATE);

    authorizationManager.authorizeAction(ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObStorekeeper.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);

    authorizationCommonUtils.applyAuthorizationForPurchaseUnit(
        initialStockUploadCreateValueObject.getPurchaseUnitCode());
  }

  public void setcObPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep) {
    this.cObPurchasingUnitGeneralModelRep = cObPurchasingUnitGeneralModelRep;
    authorizationCommonUtils.setcObPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
  }
}
