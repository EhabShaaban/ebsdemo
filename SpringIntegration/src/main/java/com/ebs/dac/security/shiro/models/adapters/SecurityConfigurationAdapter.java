package com.ebs.dac.security.shiro.models.adapters;

import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;

public class SecurityConfigurationAdapter {

  private static Gson gson;

  static {
    GsonBuilder builder = new GsonBuilder();

    builder.registerTypeAdapter(
        SecurityConfiguration.SecurityConfigurationType.class,
        new SecurityCofigurationTypeDeserializer());
    builder.registerTypeAdapter(
        SecurityConfiguration.SecurityClearnceLevel.class, new SecondFactorClaimDeserializer());
    //		builder.registerTypeAdapter(SecondFactorCredentialValidityMode.class, new
    // SecondFactorCredentialValidityDeserializer());
    //		builder.registerTypeAdapter(SecondFactorCredentialValidityMode.class, new
    // SecondFactorCredentialValiditySerializer());

    gson = builder.create();
  }

  public static List<SecurityConfiguration> deserializeConfigurationsStringToList(
      String configurationsString) {

    Type configurationsListType = new TypeToken<List<SecurityConfiguration>>() {}.getType();

    List<SecurityConfiguration> securityConfigurationsList =
        gson.fromJson(configurationsString, configurationsListType);

    return securityConfigurationsList;
  }

  public static String serializeConfigurationsListToString(
      List<SecurityConfiguration> securityConfigurationsList) {

    String configurationsString = gson.toJson(securityConfigurationsList);

    return configurationsString;
  }

  public static void main(String[] args) {
    System.out.println(deserializeConfigurationsStringToList(getString()));
  }

  private static String getString() {
    return "["
        + "{\"securityConfigurationType\":\"LOCAL\","
        + "\"securityClearnceLevel\":\"LEAST_RESTRICTIVE\","
        + "\"secondFactorCredentialValidity\":10},"
        + "{\"securityConfigurationType\":\"REMOTE\","
        + "\"securityClearnceLevel\":\"STANDARD\","
        + "\"secondFactorCredentialValidity\":10}"
        + "]";
  }
}
