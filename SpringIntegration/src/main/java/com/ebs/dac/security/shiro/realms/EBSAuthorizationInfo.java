package com.ebs.dac.security.shiro.realms;

import com.ebs.dac.security.jpa.entities.AuthorizationCondition;
import com.ebs.dac.security.jpa.entities.Role;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.shiro.authz.SimpleAuthorizationInfo;

public class EBSAuthorizationInfo extends SimpleAuthorizationInfo {

  private static final long serialVersionUID = 1L;

  private List<AuthorizationCondition> authorizationConditions;
  private Set<Role> roles;

  public EBSAuthorizationInfo() {
    authorizationConditions = new ArrayList<>();
    roles = new HashSet<>();
  }

  public void addRole(Role role) {
    super.addRole(role.getRoleExpression());
    roles.add(role);
  }

  public void addAuthorizationCondition(AuthorizationCondition condition) {
    authorizationConditions.add(condition);
  }

  public Set<Role> getRoleObjects() {
    return roles;
  }

  public List<AuthorizationCondition> getAuthorizationConditions() {
    return authorizationConditions;
  }

  public void setAuthorizationConditions(List<AuthorizationCondition> authorizationConditions) {
    this.authorizationConditions = authorizationConditions;
  }
}
