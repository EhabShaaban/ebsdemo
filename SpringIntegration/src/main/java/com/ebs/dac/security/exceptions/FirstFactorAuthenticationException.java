package com.ebs.dac.security.exceptions;

import com.ebs.dac.common.utils.HttpStatusCode;

/** sent wrong info in 1st-Factor authentication */
public class FirstFactorAuthenticationException extends AuthenticationException {

  /** */
  private static final long serialVersionUID = 1L;

  private static final String DEFAULT_MESSAGE = "Couldn't authenticate user";

  {
    ERROR_CODE = "LOGIN-1STF_EROR";
    STATUS_CODE =
        HttpStatusCode.UNAUTHORIZED
            .code(); // same as parent but set explicitly to handle if parent changes
  }

  public FirstFactorAuthenticationException() {
    this(DEFAULT_MESSAGE);
  }

  public FirstFactorAuthenticationException(String message) {
    super(message);
  }

  public FirstFactorAuthenticationException(Exception e) {
    super(DEFAULT_MESSAGE, e);
  }

  public FirstFactorAuthenticationException(String message, Exception cause) {
    super(message, cause);
  }
}
