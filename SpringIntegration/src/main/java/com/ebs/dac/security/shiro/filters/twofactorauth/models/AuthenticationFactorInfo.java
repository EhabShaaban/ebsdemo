package com.ebs.dac.security.shiro.filters.twofactorauth.models;

import org.apache.shiro.authc.AuthenticationInfo;

/**
 * The parent class for any 2nd-Factor authentication info. This type means that the authentication
 * info will be valid for the whole session.<br>
 * Other types are {@link ExpiryDateAuthenticationFactorInfo} or {@link
 * OneTimerAuthenticationFactorInfo}.
 */
public class AuthenticationFactorInfo {

  private AuthenticationInfo info;
  private String factorName;

  public AuthenticationFactorInfo(AuthenticationInfo info, String factorName) {
    this.info = info;
    this.factorName = factorName;
  }

  /** For subclasses to override */
  public boolean isValid() {
    return true;
  }

  public Object getPrincipal() {
    return info.getPrincipals().getPrimaryPrincipal();
  }

  public Object getCredentials() {
    return info.getCredentials();
  }

  public String getFactorName() {
    return factorName;
  }

  public void setFactorName(String factorName) {
    this.factorName = factorName;
  }
}
