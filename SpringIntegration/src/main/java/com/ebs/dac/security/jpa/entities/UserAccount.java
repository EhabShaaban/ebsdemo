/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/** @author hesham */
@Entity
@Table(name = "UserAccount")
@NamedQueries({
  @NamedQuery(name = "UserAccount.findAll", query = "SELECT u FROM UserAccount u"),
  @NamedQuery(
      name = "UserAccount.findById",
      query = "SELECT u FROM UserAccount u WHERE u.id = :id"),
  @NamedQuery(
      name = "UserAccount.findByAccountState",
      query = "SELECT u FROM UserAccount u WHERE u.accountState = :accountState"),
  @NamedQuery(
      name = "UserAccount.findByAccountCreateTime",
      query = "SELECT u FROM UserAccount u WHERE u.accountCreateTime = :accountCreateTime"),
  @NamedQuery(
      name = "UserAccount.findByCompanyCode",
      query = "SELECT u FROM UserAccount u WHERE u.companyCode = :companyCode"),
  @NamedQuery(
      name = "UserAccount.findByCompanyId",
      query = "SELECT u FROM UserAccount u WHERE u.companyId = :companyId")
})
public class UserAccount implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = false)
  @Column(name = "AccountState")
  private String accountState;

  @Basic(optional = false)
  @Column(name = "AccountCreateTime")
  @Temporal(TemporalType.TIMESTAMP)
  private Date accountCreateTime;

  @Column(name = "CompanyCode")
  private String companyCode;

  @Basic(optional = false)
  @Column(name = "CompanyId")
  private long companyId;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
  private List<UserAssignment> userAssignmentList;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "userAccountId")
  private List<BusinessObjectInstanceACL> businessObjectInstanceACLList;

  @JoinColumn(name = "UserId", referencedColumnName = "Id")
  @ManyToOne(optional = false)
  private User userId;

  @JoinColumn(name = "SecondAuthenticator", referencedColumnName = "Id")
  @ManyToOne
  private Authenticator secondAuthenticator;

  @Column(name = "accountconfiguration")
  private String accountConfiguration;

  public UserAccount() {}

  public UserAccount(Long id) {
    this.id = id;
  }

  public UserAccount(Long id, String accountState, Date accountCreateTime, long companyId) {
    this.id = id;
    this.accountState = accountState;
    this.accountCreateTime = accountCreateTime;
    this.companyId = companyId;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAccountState() {
    return accountState;
  }

  public void setAccountState(String accountState) {
    this.accountState = accountState;
  }

  public Date getAccountCreateTime() {
    return accountCreateTime;
  }

  public void setAccountCreateTime(Date accountCreateTime) {
    this.accountCreateTime = accountCreateTime;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(long companyId) {
    this.companyId = companyId;
  }

  public List<UserAssignment> getUserAssignmentList() {
    return userAssignmentList;
  }

  public void setUserAssignmentList(List<UserAssignment> userAssignmentList) {
    this.userAssignmentList = userAssignmentList;
  }

  public List<BusinessObjectInstanceACL> getBusinessObjectInstanceACLList() {
    return businessObjectInstanceACLList;
  }

  public void setBusinessObjectInstanceACLList(
      List<BusinessObjectInstanceACL> businessObjectInstanceACLList) {
    this.businessObjectInstanceACLList = businessObjectInstanceACLList;
  }

  public User getUserId() {
    return userId;
  }

  public void setUserId(User userId) {
    this.userId = userId;
  }

  public Authenticator getSecondAuthenticator() {
    return secondAuthenticator;
  }

  public void setSecondAuthenticator(Authenticator secondAuthenticator) {
    this.secondAuthenticator = secondAuthenticator;
  }

  public String getAccountConfiguration() {
    return accountConfiguration;
  }

  public void setAccountConfiguration(String accountConfiguration) {
    this.accountConfiguration = accountConfiguration;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof UserAccount)) {
      return false;
    }
    UserAccount other = (UserAccount) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "UserAccount [id="
        + id
        + ", accountState="
        + accountState
        + ", accountCreateTime="
        + accountCreateTime
        + ", companyCode="
        + companyCode
        + ", companyId="
        + companyId
        + ", userAssignmentList="
        + userAssignmentList
        + ", businessObjectInstanceACLList="
        + businessObjectInstanceACLList
        + ", userId="
        + userId
        + ", secondAuthenticator="
        + secondAuthenticator
        + "]";
  }
}
