/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import com.ebs.dac.security.utils.AuthorizationUtils;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import org.apache.shiro.authz.permission.WildcardPermission;

/** @author hesham */
@Entity
@Table(name = "Permission")
@NamedQueries({
  @NamedQuery(name = "Permission.findAll", query = "SELECT p FROM Permission p"),
  @NamedQuery(name = "Permission.findById", query = "SELECT p FROM Permission p WHERE p.id = :id"),
  @NamedQuery(
      name = "Permission.findByPermissionExpression",
      query = "SELECT p FROM Permission p WHERE p.permissionExpression = :permissionExpression"),
  @NamedQuery(
      name = "Permission.findByDescription",
      query = "SELECT p FROM Permission p WHERE p.description = :description"),
  @NamedQuery(
      name = "Permission.findByObjectCatalogId",
      query = "SELECT p FROM Permission p WHERE p.objectCatalogId = :objectCatalogId"),
  @NamedQuery(
      name = "Permission.findByOperationId",
      query = "SELECT p FROM Permission p WHERE p.operationId = :operationId")
})
public class Permission extends WildcardPermission implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "Id")
  private Long id;

  @Basic(optional = true)
  @Column(name = "objectName")
  private String objectName;

  @Basic(optional = false)
  @Column(name = "PermissionExpression")
  private String permissionExpression;

  @Column(name = "Description")
  private String description;

  @Column(name = "ObjectCatalogId")
  private BigInteger objectCatalogId;

  @Column(name = "OperationId")
  private BigInteger operationId;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "permissionId")
  private List<PermissionAssignment> permissionAssignmentList;

  public Permission() {}

  public Permission(Long id) {
    this.id = id;
  }

  public Permission(Long id, String permissionExpression) {
    this.id = id;
    this.permissionExpression = permissionExpression;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getPermissionExpression() {
    return permissionExpression;
  }

  public void setPermissionExpression(String permissionExpression) {
    this.permissionExpression = permissionExpression;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigInteger getObjectCatalogId() {
    return objectCatalogId;
  }

  public void setObjectCatalogId(BigInteger objectCatalogId) {
    this.objectCatalogId = objectCatalogId;
  }

  public BigInteger getOperationId() {
    return operationId;
  }

  public void setOperationId(BigInteger operationId) {
    this.operationId = operationId;
  }

  public List<PermissionAssignment> getPermissionAssignmentList() {
    return permissionAssignmentList;
  }

  public void setPermissionAssignmentList(List<PermissionAssignment> permissionAssignmentList) {
    this.permissionAssignmentList = permissionAssignmentList;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (id != null ? id.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof Permission)) {
      return false;
    }
    Permission other = (Permission) object;
    return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
  }

  @Override
  public String toString() {
    return "Permission [id="
        + id
        + ", permissionExpression="
        + permissionExpression
        + ", description="
        + description
        + ", objectCatalogId="
        + objectCatalogId
        + ", operationId="
        + operationId
        + ", permissionAssignmentList="
        + permissionAssignmentList
        + "]";
  }

  @PostLoad
  public void updatePermissionExpression() {
    setParts(getPermissionExpression());
  }

  public String[] getActions() {
    String actionsString =
        getPermissionExpression().split(AuthorizationUtils.PERMISSION_SEPERATOR)[1];
    String[] actions = actionsString.split(AuthorizationUtils.PERMISSION_ACTIONS_SEPERATOR);
    return actions;
  }

  public String getObjectName() {
    return getPermissionExpression().split(":")[0];
  }

  public void setObjectName(String objectName) {
    this.objectName = objectName;
  }
}
