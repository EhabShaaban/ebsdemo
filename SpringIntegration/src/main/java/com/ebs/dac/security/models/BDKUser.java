package com.ebs.dac.security.models;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import java.io.Serializable;

/** This class is intended to only contain {@link Serializable}/Deserializable content */
public class BDKUser implements IBDKUser {

  private String username;
  private String company;
  private Long userId;
  private Boolean isSysPasswordFlag;

  public BDKUser() {}

  public BDKUser(IBDKUser user) {
    setUsername(user.getUsername());
    setCompany(user.getCompany());
    setUserId(user.getUserId());
    setSysPasswordFlag(user.getSysPasswordFlag());
  }

  public BDKUser(String userName, String company, Long userId, Boolean isSysPasswordFlag) {
    setUsername(userName);
    setCompany(company);
    setUserId(userId);
    setSysPasswordFlag(isSysPasswordFlag);
  }

  public String getCompany() {
    return company;
  }

  public void setCompany(String company) {
    this.company = company;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public boolean equals(IBDKUser obj) {
    return this.getUsername().equals(obj.getUsername())
        && this.getCompany().equals(obj.getCompany());
  }

  public Boolean getSysPasswordFlag() {
    return isSysPasswordFlag;
  }

  public void setSysPasswordFlag(Boolean sysPasswordFlag) {
    isSysPasswordFlag = sysPasswordFlag;
  }
}
