package com.ebs.dac.security.shiro;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.realization.condition.adapter.ConditionManager;
import com.ebs.dac.foundation.realization.condition.resolvers.EntityAttributeResolverContext;
import com.ebs.dac.foundation.realization.processing.DataObjectProcessor;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.jpa.entities.AuthorizationCondition;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 20, 2018 9:42:30 AM
 */
public class AuthorizationConditionHandler {

  // TODO: refactor to take AuthorizationCondition Not List
  public boolean isValidAuthorizationConditionOnEntity(
      List<AuthorizationCondition> conditions, BusinessObject entity) throws Exception {
    AuthorizationCondition condition = getTargetObjectCondition(conditions, entity.getSysName());
    if (condition == null) {
      return true;
    }
    return isValidCondition(condition.getCondition(), entity);
  }

  private boolean isValidCondition(String condition, BusinessObject entity) throws Exception {
    ConditionManager conditionManager = new ConditionManager();
    Set<String> conditionVariables = conditionManager.getConditionVariables(condition);
    if (conditionVariables == null) {
      throw new IllegalArgumentException("Invaild Authorization Condition: " + condition);
    }

    Map<String, Object> objectAttributeMap = buildObjectAttributeMap(conditionVariables, entity);
    isAllConditionAttributesExist(conditionVariables, objectAttributeMap);

    boolean isValid = conditionManager.executeCondition(objectAttributeMap, condition);
    if (!isValid) {
      throw new ConditionalAuthorizationException(
          "Subject Does Not Have Permission On " + entity.getSysName() + "  when " + condition);
    }
    return isValid;
  }

  private void isAllConditionAttributesExist(
      Set<String> conditionVariables, Map<String, Object> objectAttributeMap) throws Exception {
    for (String attrName : conditionVariables) {
      if (!objectAttributeMap.containsKey(attrName)) {
        throw new ConditionalAuthorizationException(attrName + " can't be resolved");
      }
    }
  }

  private AuthorizationCondition getTargetObjectCondition(
      List<AuthorizationCondition> authorizationConditions, String objectName) {
    AuthorizationCondition targetObjectCondition = null;

    for (AuthorizationCondition condition : authorizationConditions) {
      if (condition.getObjectName().equals(objectName)) {
        targetObjectCondition = condition;
        break;
      }
    }
    return targetObjectCondition;
  }

  private Map<String, Object> buildObjectAttributeMap(
      Set<String> conditionVariables, BusinessObject entity) throws Exception {
    DataObjectProcessor processor = new DataObjectProcessor(entity);
    Map<String, Object> attributesValuesMap =
        processor.getAttributesValuesMap(entity, conditionVariables);

    EntityAttributeResolverContext attributesResolver = new EntityAttributeResolverContext();
    Map<String, Object> resolvedMap = attributesResolver.resolveAttributes(attributesValuesMap);

    return resolvedMap;
  }
}
