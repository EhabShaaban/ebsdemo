/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ebs.dac.security.jpa.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/** @author hesham */
@Entity
@Table(name = "UserAuthenticator")
@NamedQueries({
  @NamedQuery(name = "UserAuthenticator.findAll", query = "SELECT u FROM UserAuthenticator u"),
  @NamedQuery(
      name = "UserAuthenticator.findByUserAuthenticatorId",
      query =
          "SELECT u FROM UserAuthenticator u WHERE u.userAuthenticatorId = :userAuthenticatorId"),
  @NamedQuery(
      name = "UserAuthenticator.findByAccountId",
      query = "SELECT u FROM UserAuthenticator u WHERE u.accountId = :accountId"),
  @NamedQuery(
      name = "UserAuthenticator.findByAuthenticatorId",
      query = "SELECT u FROM UserAuthenticator u WHERE u.authenticatorId = :authenticatorId")
})
public class UserAuthenticator implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long userAuthenticatorId;

  @Basic(optional = false)
  @Column(name = "AccountId")
  private long accountId;

  @Basic(optional = false)
  @Column(name = "AuthenticatorId")
  private long authenticatorId;

  public UserAuthenticator() {}

  public UserAuthenticator(Long userAuthenticatorId) {
    this.userAuthenticatorId = userAuthenticatorId;
  }

  public UserAuthenticator(Long userAuthenticatorId, long accountId, long authenticatorId) {
    this.userAuthenticatorId = userAuthenticatorId;
    this.accountId = accountId;
    this.authenticatorId = authenticatorId;
  }

  public Long getUserAuthenticatorId() {
    return userAuthenticatorId;
  }

  public void setUserAuthenticatorId(Long userAuthenticatorId) {
    this.userAuthenticatorId = userAuthenticatorId;
  }

  public long getAccountId() {
    return accountId;
  }

  public void setAccountId(long accountId) {
    this.accountId = accountId;
  }

  public long getAuthenticatorId() {
    return authenticatorId;
  }

  public void setAuthenticatorId(long authenticatorId) {
    this.authenticatorId = authenticatorId;
  }

  @Override
  public int hashCode() {
    int hash = 0;
    hash += (userAuthenticatorId != null ? userAuthenticatorId.hashCode() : 0);
    return hash;
  }

  @Override
  public boolean equals(Object object) {
    // TODO: Warning - this method won't work in the case the id fields are not set
    if (!(object instanceof UserAuthenticator)) {
      return false;
    }
    UserAuthenticator other = (UserAuthenticator) object;
    return (this.userAuthenticatorId != null || other.userAuthenticatorId == null)
        && (this.userAuthenticatorId == null
            || this.userAuthenticatorId.equals(other.userAuthenticatorId));
  }

  @Override
  public String toString() {
    return "UserAuthenticator [userAuthenticatorId="
        + userAuthenticatorId
        + ", accountId="
        + accountId
        + ", authenticatorId="
        + authenticatorId
        + "]";
  }
}
