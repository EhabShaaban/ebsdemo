package com.ebs.dac.security.shiro.models.adapters;

import com.ebs.dac.security.shiro.models.SecurityConfiguration;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

public class SecurityCofigurationTypeDeserializer
    implements JsonDeserializer<SecurityConfiguration.SecurityConfigurationType> {

  public SecurityConfiguration.SecurityConfigurationType deserialize(
      JsonElement element, Type type, JsonDeserializationContext context)
      throws JsonParseException {

    SecurityConfiguration.SecurityConfigurationType[] types =
        SecurityConfiguration.SecurityConfigurationType.values();
    for (SecurityConfiguration.SecurityConfigurationType configurationType : types) {
      if (configurationType.name().equalsIgnoreCase(element.getAsString()))
        return configurationType;
    }
    return null;
  }
}
