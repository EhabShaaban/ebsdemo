package com.ebs.dac.security.commands;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.security.jpa.entities.User;
import com.ebs.dac.security.jpa.repositories.UserRepository;
import com.ebs.dac.security.jpa.valueobjects.UserPasswordValueObject;
import com.ebs.dac.security.usermanage.utils.BDKUserUtils;
import io.reactivex.Observable;

public class UserChangePasswordCommand {

  private IUserAccountSecurityService userAccountSecurityService;
  private UserRepository userRepository;

  public Observable<String> executeCommand(UserPasswordValueObject userPasswordValueObject) {
    IBDKUser loggedInUser = userAccountSecurityService.getLoggedInUser();

    User user = userRepository.findOneById(loggedInUser.getUserId());
    user.setPassword(BDKUserUtils.encodePassword(userPasswordValueObject.getNewPassword()));
    user.setSysFlag(false);
    userRepository.save(user);
    return Observable.just("SUCCESS");
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setUserRepository(UserRepository userRepository) {
    this.userRepository = userRepository;
  }
}
