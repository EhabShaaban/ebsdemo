package com.ebs.dac.common.utils.codes;
/** @author Yara Ameen */
public class SystemCodes {
  public static String SUCCESS_CODE = "Gen-msg-11";
  public static String FAIL_CODE = "Gen-msg-05";
  public static String UPLOAD_SUCCESSFUL_CODE = "Gen-msg-23";
}
