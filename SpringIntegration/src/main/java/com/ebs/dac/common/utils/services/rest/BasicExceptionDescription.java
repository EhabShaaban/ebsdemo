package com.ebs.dac.common.utils.services.rest;

class BasicExceptionDescription {

  private String code;
  private String userMessage;

  public BasicExceptionDescription(String code, String userMessage) {
    this.code = code;
    this.userMessage = userMessage;
  }

  public String getCode() {
    return code;
  }

  public String getUserMessage() {
    return userMessage;
  }
}
