package com.ebs.dac.common.utils.services.rest;

public class CustomExceptionMessageModel {

  private String title;
  private String reason;
  private String suggestion;
  private String code;
  private CustomExceptionMessageModel cause;

  public CustomExceptionMessageModel() {}

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    throw new UnsupportedOperationException();
    // this.title = JSONUtils.encodeString(title);
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    throw new UnsupportedOperationException();
    // this.reason = JSONUtils.encodeString(reason);
  }

  public String getSuggestion() {
    return suggestion;
  }

  public void setSuggestion(String suggestion) {
    throw new UnsupportedOperationException();
    // this.suggestion = JSONUtils.encodeString(suggestion);
  }

  public CustomExceptionMessageModel getCause() {
    return cause;
  }

  public void setCause(CustomExceptionMessageModel cause) {
    this.cause = cause;
  }

  @Override
  public String toString() {
    return "CustomExceptionMessageModel [title="
        + title
        + ", reason="
        + reason
        + ", suggestion="
        + suggestion
        + ", code="
        + code
        + ", cause="
        + cause
        + "]";
  }
}
