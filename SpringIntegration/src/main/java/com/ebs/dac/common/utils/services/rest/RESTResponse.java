package com.ebs.dac.common.utils.services.rest;

import java.util.Locale;

public abstract class RESTResponse implements IRestResponse {

  protected Locale locale;
  protected String status;
  protected String code;
  public RESTResponse(Locale locale) {
    this.locale = locale;
  }

  public Locale getLocale() {
    return locale;
  }

  public boolean isSuccess() {
    return status.equals(ResponseState.SUCCESS.toString());
  }

  public String getStatus() {
    return status;
  }

  public enum ResponseState {
    SUCCESS,
    FAIL,
    ERROR
  }
}
