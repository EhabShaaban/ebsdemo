package com.ebs.dac.common.utils.services.rest;

import com.ebs.dac.common.utils.codes.SystemCodes;
import java.util.Locale;

/** @author Hesham Saleh */
public class BasicRESTResponse extends RESTResponse {

  private Object data;

  public BasicRESTResponse(Locale locale) {
    super(locale);
  }

  public void success(Object data) {
    this.status = ResponseState.SUCCESS.name();
    this.data = data;
    this.code = SystemCodes.SUCCESS_CODE;
  }

  public void fail(Object data) {
    this.status = ResponseState.FAIL.name();
    this.data = data;
    this.code = SystemCodes.FAIL_CODE;
  }

  public void successUpload(Object data) {
    this.status = ResponseState.FAIL.name();
    this.data = data;
    this.code = SystemCodes.UPLOAD_SUCCESSFUL_CODE;
  }

  public Object getData() {
    return this.data;
  }
}
