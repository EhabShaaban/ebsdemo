package config;

import com.ebs.dac.dbo.jpa.repositories.BusinessObjectRepository;
import com.ebs.dac.spring.configs.RepositoryFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(
    repositoryFactoryBeanClass = RepositoryFactoryBean.class,
    repositoryBaseClass = BusinessObjectRepository.class,
    basePackages = {
            "com.ebs.dac.security.jpa.repositories", "com.ebs.dda.masterdata.dbo.jpa.repositories.*",
            "com.ebs.dda.purchases.dbo.jpa.repositories.*",
            "com.ebs.dda.repositories.inventory",
        "com.ebs.dda.accounting.notesreceivables.dbo.jpa.repositories"
    })
@ComponentScan({"rest.purchases"})
public class ReportingApplicationLauncher {
  private static final Logger log = LoggerFactory.getLogger(ReportingApplicationLauncher.class);

  public static void main(String[] args) {
    log.info(
        "ReportingApplicationLauncher "
            + SpringApplication.run(ReportingApplicationLauncher.class, args));
  }
}
