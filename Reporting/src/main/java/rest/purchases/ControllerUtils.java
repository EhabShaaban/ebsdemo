package rest.purchases;

import com.ebs.dac.common.utils.json.adapters.BigDecimalAdapter;
import org.joda.time.DateTime;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dac.common.utils.json.adapters.JodaTimeJsonAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.math.BigDecimal;

public class ControllerUtils {
  private final String DATA_KEY = "data";
  private final String RESPONSE_STATUS = "status";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";

  public Gson getGson() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setDateFormat(DateFormats.DATE_TIME_WITH_TIMEZONE);
    gsonBuilder.registerTypeAdapter(DateTime.class, new JodaTimeJsonAdapter());
    gsonBuilder.registerTypeAdapter(BigDecimal.class, new BigDecimalAdapter());
    gsonBuilder.enableComplexMapKeySerialization().setPrettyPrinting();
    return gsonBuilder.create();
  }

  public String serializeOrderPDFData(JsonObject orderPDFData) {

    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.add(DATA_KEY, orderPDFData);
    return successResponse.toString();
  }
}
