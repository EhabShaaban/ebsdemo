package rest.purchases;

import com.ebs.dda.jpa.order.IObOrderDocumentRequiredDocumentsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderHeaderPDFData;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderHeaderPDFDataRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderDocumentRequiredDocumentsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsQuantitiesGeneralModelRep;
import com.google.gson.JsonObject;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/reporting/purchaseorder")
public class DObPurchaseOrderReportsController {

  @Autowired private DObPurchaseOrderHeaderPDFDataRep purchaseOrderHeaderPDFDataRep;
  @Autowired private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;

  @Autowired
  private IObOrderLineDetailsQuantitiesGeneralModelRep orderLineDetailsQuantitiesGeneralModelRep;
  @Autowired private IObOrderDocumentRequiredDocumentsGeneralModelRep requiredDocumentsGeneralModelRep;

  private ControllerUtils controllerUtils;

  public DObPurchaseOrderReportsController() {
    controllerUtils = new ControllerUtils();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/exportpdf/{purchaseOrderCode}")
  public @ResponseBody String exportAsPDF(@PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderHeaderPDFData purchaseOrderPDFHeaderData =
        purchaseOrderHeaderPDFDataRep.findOneByPurchaseOrderCode(purchaseOrderCode);
    List<IObOrderLineDetailsGeneralModel> orderItems = getPurchaseOrderItems(purchaseOrderCode);
    List<IObOrderDocumentRequiredDocumentsGeneralModel> requiredDocuments = requiredDocumentsGeneralModelRep.findByUserCodeOrderById(purchaseOrderCode);
    JsonObject pdfData = new JsonObject();
    pdfData.add("header", controllerUtils.getGson().toJsonTree(purchaseOrderPDFHeaderData));
    pdfData.add("items", controllerUtils.getGson().toJsonTree(orderItems));
    pdfData.add("requiredDocuments", controllerUtils.getGson().toJsonTree(requiredDocuments));
    return controllerUtils.serializeOrderPDFData(pdfData);
  }

  private List<IObOrderLineDetailsGeneralModel> getPurchaseOrderItems(String purchaseOrderCode) {
    List<IObOrderLineDetailsGeneralModel> orderLineDetailsGeneralModels =
        orderLineDetailsGeneralModelRep.findByOrderCode(purchaseOrderCode);
    for (IObOrderLineDetailsGeneralModel orderLineItem : orderLineDetailsGeneralModels) {
      List<IObOrderLineDetailsQuantitiesGeneralModel> orderLineDetailsQuantitiesGeneralModel =
          orderLineDetailsQuantitiesGeneralModelRep.findByRefInstanceIdOrderByIdDesc(orderLineItem.getId());
      orderLineItem.setQuantities(orderLineDetailsQuantitiesGeneralModel);
    }
    return orderLineDetailsGeneralModels;
  }
}
