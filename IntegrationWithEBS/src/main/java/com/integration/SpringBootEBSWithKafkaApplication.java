package com.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SpringBootApplication
@EnableAutoConfiguration(
    exclude = {
        HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class
    })

public class SpringBootEBSWithKafkaApplication {

  private static final Logger log = LoggerFactory
      .getLogger(SpringBootEBSWithKafkaApplication.class);

  public static void main(String[] args) {
    log.info("" + SpringApplication.run(SpringBootEBSWithKafkaApplication.class, args));
  }

  @Bean("dataSource")
  public DriverManagerDataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setUsername("bdk");
    dataSource.setPassword("bdk");
    dataSource.setUrl("jdbc:postgresql://34.248.182.119:5432/ebdk_core_v2");
    return dataSource;
  }
}
