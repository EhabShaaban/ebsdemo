package com.integration.web.consumers;

import com.integration.web.utils.DatabaseConnector;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class PurchaseOrderConsumer implements InitializingBean {

  private DatabaseConnector postgresDB;

  @Autowired
  private DriverManagerDataSource dataSource;

  @KafkaListener(topics = "ebs_purchase-requests", groupId = "group_id")
  public void mapPurchaseOrderToPurchaseRequest(String insertionQuery) {
    postgresDB.update(insertionQuery);
  }

  @Override
  public void afterPropertiesSet() {
    this.postgresDB = new DatabaseConnector(dataSource);
  }
}
