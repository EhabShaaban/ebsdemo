package com.integration.web.utils;


import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DatabaseConnector {


  private DriverManagerDataSource dataSource;

  public DatabaseConnector(DriverManagerDataSource dataSource) {
    this.dataSource = dataSource;
  }

  public Map<String, Object> getMap(String sql, Object... args) {
    return getJDBCTemplate().queryForMap(sql, args);
  }

  public List<Map<String, Object>> getMaps(String sql, Object... args) {
    return getJDBCTemplate().queryForList(sql, args);
  }

  public void update(String sql, Object... args) {
    getJDBCTemplate().update(sql, args);
  }


  private JdbcTemplate getJDBCTemplate() {
    return new JdbcTemplate(dataSource);
  }


}
