package com.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@SpringBootApplication
@EnableAutoConfiguration(
    exclude = {
        HibernateJpaAutoConfiguration.class, JpaRepositoriesAutoConfiguration.class
    })
public class SpringBootWithKafkaApplication {

  private static final Logger log = LoggerFactory.getLogger(SpringBootWithKafkaApplication.class);

  public static void main(String[] args) {
    log.info("" + SpringApplication.run(SpringBootWithKafkaApplication.class, args));
  }

  @Bean
  public DriverManagerDataSource myDataSource() throws Exception {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("oracle.jdbc.OracleDriver");
    dataSource.setUsername("signage");
    dataSource.setPassword("signage21");
    dataSource.setUrl("jdbc:oracle:thin:@//52.209.33.50:1521/madinapdb");
    return dataSource;
  }

}
