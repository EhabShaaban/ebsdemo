package com.integration.web.kafka.consumers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.integration.web.commands.IQueries;
import com.integration.web.commands.PurchaseOrderCommand;
import com.integration.web.mapping.PurchaseOrderMapping;
import com.integration.web.utils.DatabaseConnector;
import com.integration.web.utils.IFileKeyValues;
import com.integration.web.utils.IntegrationUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class PurchaseOrderConsumer implements InitializingBean {

  private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderConsumer.class);

  private DatabaseConnector oracleDB;

  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  @Autowired
  private DriverManagerDataSource dataSource;

  @KafkaListener(topics = "ebs-purchaseorderevent", groupId = "group_id")
  public void createImportPurchaseRequestReadyForOperation(String message) throws Exception {

    JsonNode poEventData = IntegrationUtils.retrieveEventData(message);
    PurchaseOrderMapping purchaseOrderMapping = new PurchaseOrderMapping(poEventData);

    boolean readyForOperation = isPurchaseOrderReadyForOperation(poEventData.get("currentstates"));

    if (readyForOperation && isSignmediaPurchaseOrder(poEventData)) {
      String lastPRId = getLastPurchaseRequestId(purchaseOrderMapping);
      logger.info("lastPRId: " + lastPRId);
      Long newPRId = purchaseOrderMapping.getMappedPurchaseRequestId(lastPRId);
      JsonNode mappedData = purchaseOrderMapping.mapPurchaseOrder(newPRId);
      PurchaseOrderCommand purchaseOrderCommand = new PurchaseOrderCommand(oracleDB);
      executeTransaction(purchaseOrderCommand, poEventData, mappedData);
    }
  }

  private boolean isSignmediaPurchaseOrder(JsonNode poEventData) {
    String purchaseUnit = poEventData.get("purchaseunitcode").asText();
    if (purchaseUnit.equals(IFileKeyValues.SIGNMEDIA_CODE)) {
      return true;
    }
    return false;
  }

  private void executeTransaction(
      PurchaseOrderCommand purchaseOrderCommand, JsonNode poEventData, JsonNode mappedData)
      throws Exception {

    Boolean completed = true;
    Connection connection = dataSource.getConnection();

    try {
      connection.setAutoCommit(false);
      insertMappedPurchaseOrderWithPurchaseRequest(poEventData, mappedData, connection);
      purchaseOrderCommand.executeCommand(mappedData, connection);
      connection.commit();
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      completed = false;
      connection.rollback();
    }

    Long prId = IntegrationUtils.getLongFieldFromJsonNode(mappedData, IEventNames.PR_ID);
    String poCode = IntegrationUtils.getStringFieldFromJsonNode(poEventData, "code");
    sendToEbsTransactionIsComplete(completed, poCode, prId.toString());
  }

  private void insertMappedPurchaseOrderWithPurchaseRequest(
      JsonNode poEventData,
      JsonNode mappedData,
      Connection connection) throws SQLException {
    String purchaseOrderCode = IntegrationUtils.getStringFieldFromJsonNode(poEventData, "code");
    Long prId = IntegrationUtils.getLongFieldFromJsonNode(mappedData, IEventNames.PR_ID);
    String purchaseRequestCode = prId.toString();
    PreparedStatement preparedStatement = connection.prepareStatement(IQueries.INSERT_MAPPED_PO_PR);
    oracleDB.createPreparedStatement(preparedStatement, purchaseOrderCode, 1);
    oracleDB.createPreparedStatement(preparedStatement, purchaseRequestCode, 2);
    preparedStatement.executeQuery();
  }

  private void sendToEbsTransactionIsComplete(
      boolean isTransactionCompleted, String poCode, String prCode) {
    if (isTransactionCompleted) {
      StringBuilder insertionQuery = new StringBuilder();
      insertionQuery.append(
          "INSERT INTO MappedPurchaseOrderWithPurchaseRequest (pocode,prcode) values(");
      insertionQuery.append(poCode).append(",").append(prCode).append(")");
      kafkaTemplate.send("ebs_purchase-requests", insertionQuery.toString());
    }
  }

  private String getLastPurchaseRequestId(PurchaseOrderMapping purchaseOrderMapping)
      throws Exception {
    String concatenatedPRId = purchaseOrderMapping.createPurchaseRequestIdUsingMappedDetails();
    List<Map<String, Object>> purchaseRequests =
        oracleDB.getMaps(IQueries.GET_LAST_PR, concatenatedPRId + '%');
    if (purchaseRequests.isEmpty()) {
      return concatenatedPRId + "0000";
    }
    return purchaseRequests.get(0).get("PURCHASEREQUESTID").toString();
  }

  private Boolean isPurchaseOrderReadyForOperation(JsonNode states) throws Exception {
    JsonNode statesList = new ObjectMapper().readTree(states.asText());
    for (JsonNode stateNode : statesList) {
      String state = stateNode.asText();
      if (state.equals(IFileKeyValues.READY_FOR_PAYMENT)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void afterPropertiesSet() {
    this.oracleDB = new DatabaseConnector(dataSource);
  }
}
