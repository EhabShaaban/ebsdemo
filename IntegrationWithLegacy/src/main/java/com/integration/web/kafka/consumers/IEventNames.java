package com.integration.web.kafka.consumers;

public interface IEventNames {

  //Purchase Request
  String PR_ID = "PurchaseRequestId";
  String PR_DATE = "PurchaseRequestDate";
  String CURRENCY_TYPE_ID = "CurrencyTypeId";
  String SUPPLIER_ID = "SupplierId";
  String SUITABLE_ARRIVAL_DATE = "SuitableArriveDate";
  String EXTERNAL_LOCAL = "ExternalOrLocal";
  String SHIPMENT_TYPE_ID = "ShipmentTypeId";
  String RECEIVING_TYPE_ID = "ReceivingType";
  String EMP_ID = "EmpId";
  String PI_REQUEST_DATE = "PI_Request";
  String PI_GET_DATE = "PI_Get";
  String IS_FINAL_REQ = "Final_Req";
  String NEW_PR_ID = "New_PurchaseRequestId";
  String DELAY_CHK = "DelayChk";
  String IS_PRINT_NOTES = "PrintNotes";
  String PORT_CARGO_ID = "PortCargoId";
  String PORT_ARRIVED_ID = "PortArrived";
  String IS_SUPPLIER_PAY = "SupplierPayments";
  String STORE_ID = "StoreId";

  //Items
  String QUANTITY = "Quantity";
  String ITEM_ID = "ItemId";
  String PRICE = "Price";

  //Signature Fields
  String SIGNDATE = "SignDate";
  String AGREE = "Agree";
  String PASSWORD = "Password";
  String SIGN_PERSON = "SignPerson";
  String SIGN_TITLE = "SignTitle";
  String FORM_ID = "FormId";
  String ORDER = "Order";
  String SECOND_SIGN = "SecondSign";
  String MONTH = "Month";

}

