package com.integration.web.utils;


import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class DatabaseConnector {


  private DriverManagerDataSource dataSource;

  public DatabaseConnector(DriverManagerDataSource dataSource) {
    this.dataSource = dataSource;
  }


  public void createPreparedStatement(PreparedStatement preparedStatement,
      Object obj, int parameterNumber) throws SQLException {
    if (obj == null) {
      preparedStatement.setNull(parameterNumber, Types.NULL);
    } else if (obj instanceof Long) {
      preparedStatement.setLong(parameterNumber, (Long) obj);
    } else if (obj instanceof String) {
      preparedStatement.setString(parameterNumber, (String) obj);
    } else if (obj instanceof Double) {
      preparedStatement.setDouble(parameterNumber, (Double) obj);
    }
  }

  public Map<String, Object> getMap(String sql, Object... args) {
    return getJDBCTemplate().queryForMap(sql, args);
  }

  public List<Map<String, Object>> getMaps(String sql, Object... args) {
    return getJDBCTemplate().queryForList(sql, args);
  }

  public void update(String sql, Object... args) {
    getJDBCTemplate().update(sql, args);
  }

  private JdbcTemplate getJDBCTemplate() {
    return new JdbcTemplate(dataSource);
  }


}
