package com.integration.web.utils;

public interface IFileKeyValues {

  String SIGNMEDIA_CODE = "0002";
  String IMPORT_PO_PR_ID = "004";
  String LOCAL_PO_PR_ID = "048";

  String IMPORT_PO_NAME = "Import Purchase Order";
  String READY_FOR_PAYMENT = "ReadyForPayment";
}
