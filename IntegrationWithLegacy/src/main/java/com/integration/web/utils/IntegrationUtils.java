package com.integration.web.utils;

import com.fasterxml.jackson.databind.JsonNode;

public class IntegrationUtils {

  public static JsonNode retrieveEventData(String data) throws Exception {
    JsonEventFactory jsonMappingFactory = new JsonEventFactory();
    return jsonMappingFactory.getObjectDataNodeFromString(data);
  }

  public static String getStringFieldFromJsonNode(JsonNode jsonNode, String fieldName) {
    if (jsonNode.get(fieldName).isNull()) {
      return null;
    }
    return jsonNode.get(fieldName).asText();
  }

  public static Long getLongFieldFromJsonNode(JsonNode jsonNode, String fieldName) {
    if (jsonNode.get(fieldName).isNull()) {
      return null;
    }
    return jsonNode.get(fieldName).asLong();
  }

  public static Double getDoubleFieldFromJsonNode(JsonNode jsonNode, String fieldName) {
    if (jsonNode.get(fieldName).isNull()) {
      return null;
    }
    return jsonNode.get(fieldName).asDouble();
  }


}
