package com.integration.web.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.github.fge.jackson.JsonLoader;
import java.util.List;
import java.util.Map;

public class JsonEventFactory {


  public JsonNode getObjectDataNodeFromResources(String objectType) throws Exception {
    JsonNode objectData = JsonLoader.fromResource("/META-INF/MappingData/" + objectType + ".json");
    return objectData;
  }

  public JsonNode getObjectDataNodeFromString(String source) throws Exception {
    JsonNode objectData = JsonLoader.fromString(source);
    return objectData;
  }

  public List<Map<String, String>> convertToListOfMaps(JsonNode arrayNode) throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    ObjectReader reader = mapper.readerFor(new TypeReference<List<Map<String, String>>>() {
    });
    return reader.readValue(arrayNode);
  }

  public Map<String, String> convertObjectToMap(JsonNode jsonNode) throws Exception {
    ObjectMapper mapper = new ObjectMapper();
    ObjectReader reader = mapper.readerFor(new TypeReference<Map<String, String>>() {
    });
    return reader.readValue(jsonNode);
  }

}