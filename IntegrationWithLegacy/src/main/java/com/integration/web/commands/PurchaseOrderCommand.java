package com.integration.web.commands;

import com.fasterxml.jackson.databind.JsonNode;
import com.integration.web.kafka.consumers.IEventNames;
import com.integration.web.utils.DatabaseConnector;
import com.integration.web.utils.IntegrationUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PurchaseOrderCommand {

  private DatabaseConnector oracleDB;

  public PurchaseOrderCommand(DatabaseConnector oracleDB) {
    this.oracleDB = oracleDB;
  }

  public void executeCommand(JsonNode poEventData, Connection connection) throws SQLException {
    insertPurchaseRequest(poEventData, connection);
    insertItems(poEventData, connection);
    insertSignatures(poEventData, connection);
  }

  private void insertSignatures(JsonNode poEventData, Connection connection) throws SQLException {
    Long prId = IntegrationUtils.getLongFieldFromJsonNode(poEventData, IEventNames.PR_ID);
    JsonNode poSignatures = poEventData.get("signatures");

    for (JsonNode poSignature : poSignatures) {
      String signDate = IntegrationUtils
          .getStringFieldFromJsonNode(poSignature, IEventNames.SIGNDATE);
      Long agree = IntegrationUtils.getLongFieldFromJsonNode(poSignature, IEventNames.AGREE);
      String password = IntegrationUtils
          .getStringFieldFromJsonNode(poSignature, IEventNames.PASSWORD);
      Long signPerson = IntegrationUtils
          .getLongFieldFromJsonNode(poSignature, IEventNames.SIGN_PERSON);
      Long signTitle = IntegrationUtils
          .getLongFieldFromJsonNode(poSignature, IEventNames.SIGN_TITLE);
      Long formId = IntegrationUtils.getLongFieldFromJsonNode(poSignature, IEventNames.FORM_ID);
      Long order = IntegrationUtils.getLongFieldFromJsonNode(poSignature, IEventNames.ORDER);
      String secondSign = IntegrationUtils
          .getStringFieldFromJsonNode(poSignature, IEventNames.SECOND_SIGN);
      Long month = IntegrationUtils.getLongFieldFromJsonNode(poSignature, IEventNames.MONTH);

      PreparedStatement preparedStatement = connection.prepareStatement(IQueries.INSERT_SIGNATURE);
      oracleDB.createPreparedStatement(preparedStatement, prId, 1);
      oracleDB.createPreparedStatement(preparedStatement, signDate, 2);
      oracleDB.createPreparedStatement(preparedStatement, agree, 3);
      oracleDB.createPreparedStatement(preparedStatement, password, 4);
      oracleDB.createPreparedStatement(preparedStatement, signPerson, 5);
      oracleDB.createPreparedStatement(preparedStatement, signTitle, 6);
      oracleDB.createPreparedStatement(preparedStatement, formId, 7);
      oracleDB.createPreparedStatement(preparedStatement, order, 8);
      oracleDB.createPreparedStatement(preparedStatement, secondSign, 9);
      oracleDB.createPreparedStatement(preparedStatement, month, 10);
      preparedStatement.executeQuery();

    }
  }

  private void insertItems(JsonNode poEventData, Connection connection) throws SQLException {
    Long prId = IntegrationUtils.getLongFieldFromJsonNode(poEventData, IEventNames.PR_ID);
    JsonNode poItems = poEventData.get("items");

    for (JsonNode poItem : poItems) {
      Long quantity = IntegrationUtils.getLongFieldFromJsonNode(poItem, IEventNames.QUANTITY);
      Long itemId = IntegrationUtils.getLongFieldFromJsonNode(poItem, IEventNames.ITEM_ID);
      Double price = IntegrationUtils.getDoubleFieldFromJsonNode(poItem, IEventNames.PRICE);

      PreparedStatement preparedStatement = connection.prepareStatement(IQueries.INSERT_ITEM);
      oracleDB.createPreparedStatement(preparedStatement, prId, 1);
      oracleDB.createPreparedStatement(preparedStatement, quantity, 2);
      oracleDB.createPreparedStatement(preparedStatement, itemId, 3);
      oracleDB.createPreparedStatement(preparedStatement, price, 4);
      preparedStatement.executeQuery();
    }
  }

  private void insertPurchaseRequest(JsonNode poEventData, Connection connection)
      throws SQLException {
    Long prId = IntegrationUtils.getLongFieldFromJsonNode(poEventData, IEventNames.PR_ID);
    String prDate = IntegrationUtils.getStringFieldFromJsonNode(poEventData, IEventNames.PR_DATE);
    Long currencyTypeId = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.CURRENCY_TYPE_ID);
    Long supplierId = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.SUPPLIER_ID);

    Long externalOrLocal = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.EXTERNAL_LOCAL);
    Long shipmentTypeId = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.SHIPMENT_TYPE_ID);
    Long receivingTypeId = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.RECEIVING_TYPE_ID);
    Long employeeId = IntegrationUtils.getLongFieldFromJsonNode(poEventData, IEventNames.EMP_ID);

    String piRequestDate = IntegrationUtils
        .getStringFieldFromJsonNode(poEventData, IEventNames.PI_REQUEST_DATE);
    Long newPurchaseRequestId = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.NEW_PR_ID);
    String delayCheck = IntegrationUtils
        .getStringFieldFromJsonNode(poEventData, IEventNames.DELAY_CHK);

    Long printNotes = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.IS_PRINT_NOTES);
    Long portCargoId = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.PORT_CARGO_ID);
    Long portArriveId = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.PORT_ARRIVED_ID);
    Long supplierPayments = IntegrationUtils
        .getLongFieldFromJsonNode(poEventData, IEventNames.IS_SUPPLIER_PAY);
    Long storeId = IntegrationUtils.getLongFieldFromJsonNode(poEventData, IEventNames.STORE_ID);

    PreparedStatement preparedStatement = connection.prepareStatement(IQueries.INSERT_PR);

    oracleDB.createPreparedStatement(preparedStatement, prId, 1);
    oracleDB.createPreparedStatement(preparedStatement, prDate, 2);
    oracleDB.createPreparedStatement(preparedStatement, currencyTypeId, 3);
    oracleDB.createPreparedStatement(preparedStatement, supplierId, 4);
    oracleDB.createPreparedStatement(preparedStatement, externalOrLocal, 5);
    oracleDB.createPreparedStatement(preparedStatement, shipmentTypeId, 6);
    oracleDB.createPreparedStatement(preparedStatement, receivingTypeId, 7);
    oracleDB.createPreparedStatement(preparedStatement, employeeId, 8);
    oracleDB.createPreparedStatement(preparedStatement, piRequestDate, 9);
    oracleDB.createPreparedStatement(preparedStatement, newPurchaseRequestId, 10);
    oracleDB.createPreparedStatement(preparedStatement, delayCheck, 11);
    oracleDB.createPreparedStatement(preparedStatement, printNotes, 12);
    oracleDB.createPreparedStatement(preparedStatement, portCargoId, 13);
    oracleDB.createPreparedStatement(preparedStatement, portArriveId, 14);
    oracleDB.createPreparedStatement(preparedStatement, supplierPayments, 15);
    oracleDB.createPreparedStatement(preparedStatement, storeId, 16);

    preparedStatement.executeQuery();
  }


}
