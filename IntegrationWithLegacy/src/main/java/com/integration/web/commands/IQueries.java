package com.integration.web.commands;

public interface IQueries {

  String INSERT_PR = "Insert into PURCHASEREQUEST ("
      + "PURCHASEREQUESTID, PURCHASEREQUESTDATE, CURRENCYTYPEID, SUPPLIERID,"
      + "EXTERNAL_OR_LOCAL, SHIPMENTTYPEID, RECIVINGTYPE, EMPID, PI_REQUEST,"
      + "FINAL_REQ, NEW_PURCHASEREQUESTID, DELAY_CHK, PRINTNOTES,"
      + "PORTCARGOID, PORTARRIVEID, SUPPLIER_PAYMENTS, STOREID) values ("
      + "?, to_date(?,'YYYY-MM-DD'), ?, ?,"
      + "?, ?, ?, ?, to_date(?,'YYYY-MM-DD'),"
      + "'0', ?, ?, ?,"
      + "?, ?, ?, ?)";

  String INSERT_ITEM = "Insert into PURCHASEREQUESTITEMS ("
      + "PURCHASEREQUESTID,QNTY,ITEMID,COSTPRICE) "
      + "values (?, ?, ?, ?)";

  String INSERT_SIGNATURE = "Insert into SIGNATURES ("
      + "MOVECODE, SIGNDATE, AGREE, PASSWORD, SIGNPERSON,"
      + "SIGNTITLE, FORMID, ORD, SECOD_SIGN, MONTH) values ("
      + "?, to_date(?,'YYYY-MM-DD'), ?, ?, ?, "
      + "?, ?, ?, ?, ?)";

  String GET_LAST_PR = "select * from PURCHASEREQUEST where PURCHASEREQUESTID like ? Order by PURCHASEREQUESTID desc";

  String INSERT_MAPPED_PO_PR = "INSERT INTO MappedPurchaseOrderWithPurchaseRequest (pocode,prcode) values(?, ?)";

}
