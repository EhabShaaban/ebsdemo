package com.integration.web.mapping;

public interface IEventFieldNames {

  String COMPANYCODE = "companycode";
  String INCOTERM = "incoterm";
  String MODEOFTRANS = "modeoftrans";
  String CONTAINER = "container";
  String LOADING_PORT = "loadingport";
  String DISCHARGE_PORT = "dischargeport";
  String CURRENCY = "currency";
  String SIGNATURES = "signatures";
  String ORDERTYPE = "ordertypename";
  String STOREHOUSE = "storehousecode";
  String VENDOR = "vendorcode";
  String ITEMS = "items";
  String PURCHASERESPONSIBLE = "purchaseresponsiblecode";
}
