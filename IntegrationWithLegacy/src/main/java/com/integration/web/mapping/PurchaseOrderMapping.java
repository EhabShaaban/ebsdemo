package com.integration.web.mapping;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.integration.web.utils.IFileKeyValues;
import com.integration.web.utils.IntegrationUtils;
import com.integration.web.utils.JsonEventFactory;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Objects;

public class PurchaseOrderMapping {

  private JsonNode eventData;

  public PurchaseOrderMapping(JsonNode eventData) {
    this.eventData = eventData;
  }

  public JsonNode mapPurchaseOrder(Long newPRId) throws Exception {
    JsonObject purchaseRequest = new JsonObject();
    purchaseRequest.addProperty("PurchaseRequestId", newPRId);
    addGeneralDataInPurchaseOrderToJsonObject(purchaseRequest);
    addPurchaseOrderDatesToJsonObject(purchaseRequest);
    addHardCodedDataToJsonObject(purchaseRequest);
    addItemsToJsonObject(purchaseRequest);
    addApproversToJsonObject(purchaseRequest);
    return JsonLoader.fromString(purchaseRequest.toString());
  }

  public String createPurchaseRequestIdUsingMappedDetails() throws Exception {
    Long motakamelCompanyCode =
        generalMappingForLongValues(IEventFieldNames.COMPANYCODE, IJsonFileNames.COMPANIES);
    String orderType =
        IntegrationUtils.getStringFieldFromJsonNode(eventData, IEventFieldNames.ORDERTYPE);
    String purchaseRequestDate =
        IntegrationUtils.getStringFieldFromJsonNode(eventData, "creationdate");
    return concatenatePurchaseRequestId(motakamelCompanyCode, orderType,
        purchaseRequestDate);
  }

  private String concatenatePurchaseRequestId(Long motakamelCompanyCode, String orderType,
      String purchaseRequestDate) {
    StringBuilder purchaseRequestId = new StringBuilder();
    String importCode;
    if (orderType.equals(IFileKeyValues.IMPORT_PO_NAME)) {
      importCode = IFileKeyValues.IMPORT_PO_PR_ID;
    } else {
      importCode = IFileKeyValues.LOCAL_PO_PR_ID;
    }
    purchaseRequestId.append(motakamelCompanyCode);
    purchaseRequestId.append(getYearFromTimestamp(purchaseRequestDate));
    purchaseRequestId.append(importCode);
    return purchaseRequestId.toString();
  }

  public Long getMappedPurchaseRequestId(String lastPRId) {
    String purchaseRequestId = createPurchaseRequestId(lastPRId);
    return Long.parseLong(purchaseRequestId);
  }

  private void addApproversToJsonObject(JsonObject purchaseRequest) throws Exception {
    JsonNode motkamelApprovers = mapImportAndLocalPurchaseOrderApprovers();
    JsonArray signatures = new JsonArray();
    int i = 1;
    for (JsonNode approver : motkamelApprovers) {
      JsonObject signature = new JsonObject();
      addSignatureDataToJsonObject(signature, approver);
      addHardCodedSignatureDataToJsonObject(signature);
      signature.addProperty("Order", i++);
      signatures.add(signature);
    }
    purchaseRequest.add("signatures", signatures);
  }

  private void addSignatureDataToJsonObject(JsonObject signature, JsonNode approver) {
    Timestamp ts = Timestamp.valueOf(approver.get("approvalDate").asText());
    Date date = new Date(ts.getTime());
    signature.addProperty("SignDate", date.toString());
    signature.addProperty("SignPerson", approver.get("approverId").asLong());
    signature.addProperty("SignTitle", approver.get("approverTitle").asLong());
  }

  private void addHardCodedSignatureDataToJsonObject(JsonObject signature) {
    signature.addProperty("Agree", 1);
    signature.addProperty("Password", "1");
    signature.addProperty("FormId", 4);
    signature.addProperty("SecondSign", "0");
    signature.addProperty("Month", 0);
  }

  private void addHardCodedDataToJsonObject(JsonObject purchaseRequest) {
    purchaseRequest.addProperty("Final_Req", "1");
    purchaseRequest.addProperty("StoreId", "");
    purchaseRequest.addProperty("New_PurchaseRequestId", "null");
    purchaseRequest.addProperty("DelayChk", "0");
    purchaseRequest.addProperty("PrintNotes", 1);
    purchaseRequest.addProperty("SupplierPayments", 0);
  }

  private void addItemsToJsonObject(JsonObject purchaseRequest) throws Exception {
    String itemsData = IntegrationUtils
        .getStringFieldFromJsonNode(eventData, IEventFieldNames.ITEMS);
    JsonNode items = JsonLoader.fromString(Objects.requireNonNull(itemsData));
    JsonArray purchaseRequestItems = new JsonArray();
    for (JsonNode item : items) {
      JsonObject purchaseRequestItem = new JsonObject();
      purchaseRequestItem.addProperty("Quantity", item.get("quantity").asLong());
      purchaseRequestItem.addProperty("ItemId", item.get("oldId").asLong());
      purchaseRequestItem.addProperty("Price", item.get("price").asDouble());
      purchaseRequestItems.add(purchaseRequestItem);
    }
    purchaseRequest.add("items", purchaseRequestItems);
  }

  private String createPurchaseRequestId(String lastPRId) {
    Long serial = Long.parseLong(lastPRId.substring(lastPRId.length() - 4));
    String newPl = Long.toString(serial + 1);
    StringBuilder newIdString = new StringBuilder();
    newIdString.append(Long.parseLong(lastPRId.substring(0, lastPRId.length() - 4)));
    for (int i = 0; i < (4 - newPl.length()); i++) {
      newIdString.append("0");
    }
    newIdString.append(newPl);
    return newIdString.toString();
  }

  private String getYearFromTimestamp(String dateTime) {
    long dateString = Long.parseLong(dateTime);
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(dateString);
    return String.valueOf(cal.get(Calendar.YEAR));
  }

  private void addPurchaseOrderDatesToJsonObject(JsonObject purchaseRequest) {
    addDatePropertyToJsonObject(purchaseRequest, "creationdate", "PurchaseRequestDate");
    addDatePropertyToJsonObject(purchaseRequest, "pirequestdate", "PI_Request");
    addDatePropertyToJsonObject(purchaseRequest, "piconfirmationdate", "PI_Get");
    addDatePropertyToJsonObject(purchaseRequest, "collectiondate", "SuitableArriveDate");
  }

  private void addGeneralDataInPurchaseOrderToJsonObject(JsonObject purchaseRequest)
      throws Exception {
    addLongValueToJsonObject(purchaseRequest, "ExternalOrLocal", IEventFieldNames.ORDERTYPE,
        IJsonFileNames.PO_TYPE);
    addLongValueToJsonObject(purchaseRequest, "SupplierId", IEventFieldNames.VENDOR,
        IJsonFileNames.SUPPLIERS);
    addLongValueToJsonObject(purchaseRequest, "PortCargoId", IEventFieldNames.LOADING_PORT,
        IJsonFileNames.PORTS);
    addLongValueToJsonObject(purchaseRequest, "PortArrived", IEventFieldNames.DISCHARGE_PORT,
        IJsonFileNames.PORTS);
    addLongValueToJsonObject(purchaseRequest, "CurrencyTypeId", IEventFieldNames.CURRENCY,
        IJsonFileNames.CURRENCIES);
    addLongValueToJsonObject(purchaseRequest, "ReceivingType", IEventFieldNames.INCOTERM,
        IJsonFileNames.INCOTERMS);
    purchaseRequest.addProperty("ShipmentTypeId", mapShippingMode());
    addLongValueToJsonObject(purchaseRequest, "EmpId", IEventFieldNames.PURCHASERESPONSIBLE,
        IJsonFileNames.PURCHASE_RESPONSIBLES);
  }

  private void addDatePropertyToJsonObject(JsonObject jsonObject, String propertyName,
      String oracleDBName) {
    String dateValue = IntegrationUtils.getStringFieldFromJsonNode(eventData, propertyName);
    Timestamp ts;
    if (dateValue != null) {
      ts = new Timestamp(Long.parseLong(dateValue));
      Date date = new Date(ts.getTime());
      jsonObject.addProperty(oracleDBName, date.toString());
    } else {
      java.util.Date dateUtil = new java.util.Date();
      ts = new Timestamp(dateUtil.getTime());
      Date date = new Date(ts.getTime());
      jsonObject.addProperty(oracleDBName, date.toString());
    }
  }

  private void addLongValueToJsonObject(JsonObject jsonObject, String propertyName,
      String fieldName, String jsonFile) throws Exception {
    Long longValue =
        generalMappingForLongValues(fieldName, jsonFile);
    jsonObject.addProperty(propertyName, longValue);

  }

  private JsonNode mapImportAndLocalPurchaseOrderApprovers() throws Exception {
    JsonNode approversMappings = retrieveMappingData(IJsonFileNames.PO_APPROVERS);
    String approversData =
        IntegrationUtils.getStringFieldFromJsonNode(eventData, IEventFieldNames.SIGNATURES);
    JsonNode approversDataNode = JsonLoader.fromString(approversData);
    JsonArray approversJsonArray = new JsonArray();
    for (JsonNode approverDataNode : approversDataNode) {
      JsonObject approverDetails = mapApproverNode(approverDataNode, approversMappings);
      approversJsonArray.add(approverDetails);
    }
    return JsonLoader.fromString(approversJsonArray.toString());

  }

  private JsonObject mapApproverNode(JsonNode approverDataNode, JsonNode approversMappings) {
    JsonObject approverJsonObject = new JsonObject();
    approverJsonObject.addProperty("approverId",
        mapApprverId(approverDataNode, approversMappings.get("approvers")));
    approverJsonObject.addProperty("approverTitle",
        mapApproverTitle(approverDataNode, approversMappings.get("titles")));
    approverJsonObject.addProperty("approvalDate",
        IntegrationUtils.getStringFieldFromJsonNode(approverDataNode, "decisiondate"));
    return approverJsonObject;
  }

  private Long mapApprverId(JsonNode approverDataNode, JsonNode approverMappingTitles) {
    String approverId =
        IntegrationUtils.getStringFieldFromJsonNode(approverDataNode, "approverId");
    return IntegrationUtils.getLongFieldFromJsonNode(approverMappingTitles, approverId);
  }

  private Long mapApproverTitle(JsonNode approverDataNode, JsonNode approverMappingTitles) {
    String controlPoint =
        IntegrationUtils.getStringFieldFromJsonNode(approverDataNode, "controlpoint");
    return IntegrationUtils.getLongFieldFromJsonNode(approverMappingTitles, controlPoint);
  }

  private Long mapShippingMode() throws Exception {
    String shippingCode =
        IntegrationUtils.getStringFieldFromJsonNode(eventData, IEventFieldNames.MODEOFTRANS);
    String container =
        IntegrationUtils.getStringFieldFromJsonNode(eventData, IEventFieldNames.CONTAINER);
    JsonNode shippingModes = retrieveMappingData(IJsonFileNames.SHIPPINGMODE);
    JsonNode shippingMode = shippingModes.get(shippingCode);
    if (shippingMode.isContainerNode()) {
      shippingMode = shippingMode.get(container);
    }
    if (shippingMode != null) {
      return shippingMode.asLong();
    }
    return null;
  }

  private Long generalMappingForLongValues(String fieldName, String jsonFileName) throws Exception {
    String objectCode =
        IntegrationUtils.getStringFieldFromJsonNode(eventData, fieldName);
    JsonNode objects = retrieveMappingData(jsonFileName);
    JsonNode object = objects.get(objectCode);
    if (object != null) {
      return object.asLong();
    }
    return null;
  }

  private JsonNode retrieveMappingData(String dataType) throws Exception {
    JsonEventFactory jsonMappingFactory = new JsonEventFactory();
    return jsonMappingFactory.getObjectDataNodeFromResources(dataType);
  }

}
