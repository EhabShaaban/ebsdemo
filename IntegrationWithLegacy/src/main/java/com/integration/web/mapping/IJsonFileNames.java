package com.integration.web.mapping;

public interface IJsonFileNames {

  String COMPANIES = "Companies";
  String SHIPPINGMODE = "ShippingMode";
  String INCOTERMS = "Incoterms";
  String PORTS = "Ports";
  String CURRENCIES = "Currencies";
  String PO_APPROVERS = "POApprovers";
  String PO_TYPE = "PurchaseOrderType";
  String SUPPLIERS = "Suppliers";
  String PURCHASE_RESPONSIBLES = "PurchaseResponsibles";

}
