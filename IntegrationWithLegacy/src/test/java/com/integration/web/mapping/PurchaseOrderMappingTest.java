package com.integration.web.mapping;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.JsonObject;
import java.util.Calendar;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class PurchaseOrderMappingTest {

  private JsonNode eventData;

  @Before
  public void setup() {
    try {
      JsonObject jsonObject = new JsonObject();
      jsonObject.addProperty(IEventFieldNames.ORDERTYPE, "Import Purchase Order");
      jsonObject.addProperty(IEventFieldNames.COMPANYCODE, "0002");

      Calendar c = Calendar.getInstance();
      Date todayDate = new Date();
      c.setTime(todayDate);
      c.set(Calendar.YEAR, 2019);
      jsonObject.addProperty("creationdate", String.valueOf(c.getTimeInMillis()));
      eventData = JsonLoader.fromString(jsonObject.toString());
    } catch (Exception ex) {
      System.out.println("Error loading json from String");
    }
  }


  @Test
  public void testGenerateNextIdSuccessfully() {
    //Setup
    String lastPRId = "10120190040012";
    PurchaseOrderMapping purchaseOrderMapping = new PurchaseOrderMapping(eventData);

    //act
    Long prID = purchaseOrderMapping.getMappedPurchaseRequestId(lastPRId);

    //assert
    Long expectedId = 10120190040013L;
    Assertions.assertEquals(expectedId, prID, "Initial Code Generated must be 10120190040013");

  }

  @Test
  public void testCreatePurchaseRequestIdUsingMappedDetails() {
    //Setup
    PurchaseOrderMapping purchaseOrderMapping = new PurchaseOrderMapping(eventData);
    String prIDString = "";
    //act
    try {
      prIDString = purchaseOrderMapping.createPurchaseRequestIdUsingMappedDetails();
    } catch (Exception ex) {
      Assertions.fail("Error Loading json from File or String");
    }
    Long prID = Long.parseLong(prIDString);
    //assert
    Long expectedId = 1122019004L;
    Assertions.assertEquals(expectedId, prID, "Initial Code Generated must be 1122019004");

  }


}
