package com.ebs.dac.security.idm;

import com.ebs.dac.dbo.jpa.repositories.BusinessObjectRepository;
import com.ebs.dac.spring.configs.CorsFilterBean;
import com.ebs.dac.spring.configs.JPAConfiguration;
import com.ebs.dac.spring.configs.RepositoryFactoryBean;
import com.ebs.dac.spring.configs.ShiroConfig;
import com.ebs.dac.spring.configs.ShiroHazelCastCacheConfig;
import com.ebs.dac.spring.configs.WebConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Import({
  JPAConfiguration.class,
  CorsFilterBean.class,
  ShiroHazelCastCacheConfig.class,
  ShiroConfig.class,
  WebConfig.class
})
@EntityScan({"com.ebs.dac.security.jpa.entities", "com.ebs.dac.dbo.jpa.entities"})
@EnableJpaRepositories(
    repositoryFactoryBeanClass = RepositoryFactoryBean.class,
    repositoryBaseClass = BusinessObjectRepository.class,
    basePackages = {"com.ebs.dac.security.jpa.repositories"})
public class IdentityManager {
  private static final Logger log = LoggerFactory.getLogger(IdentityManager.class);

  public static void main(String[] args) {
    log.info(
        ""
            + SpringApplication.run(IdentityManager.class, args)
                .getBean(ShiroConfig.FilterChainDefinitionInterceptor.class)
                .getMap());
  }
}
