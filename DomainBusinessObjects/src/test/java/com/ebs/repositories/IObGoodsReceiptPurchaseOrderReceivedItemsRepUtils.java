package com.ebs.repositories;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsData;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsRep;
import com.ebs.entities.GoodsReceiptPurchaseOrderReceivedItemsDataMockUtils;
import org.mockito.Mockito;

public class IObGoodsReceiptPurchaseOrderReceivedItemsRepUtils {

  private static final IObGoodsReceiptPurchaseOrderReceivedItemsRep repository =
      Mockito.mock(IObGoodsReceiptPurchaseOrderReceivedItemsRep.class);

  private IObGoodsReceiptPurchaseOrderReceivedItemsRepUtils() {}

  public static IObGoodsReceiptPurchaseOrderReceivedItemsRep getRepository() {
    return IObGoodsReceiptPurchaseOrderReceivedItemsRepUtils.repository;
  }

  public static IObGoodsReceiptPurchaseOrderReceivedItemsData findOneById(Long grItemId,Long itemId) {

     IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptPurchaseOrderReceivedItemData =
             GoodsReceiptPurchaseOrderReceivedItemsDataMockUtils.mockEntity().itemId(itemId).id(grItemId).build();

    Mockito.doReturn(goodsReceiptPurchaseOrderReceivedItemData)
        .when(IObGoodsReceiptPurchaseOrderReceivedItemsRepUtils.repository)
        .findOneById(grItemId);
    return goodsReceiptPurchaseOrderReceivedItemData;
  }
}
