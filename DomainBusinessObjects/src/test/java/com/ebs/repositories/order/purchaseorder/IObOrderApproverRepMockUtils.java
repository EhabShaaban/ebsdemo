package com.ebs.repositories.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApproverRep;
import java.util.List;
import org.mockito.Mockito;

public class IObOrderApproverRepMockUtils {

  private static final IObOrderApproverRep repository = Mockito.mock(IObOrderApproverRep.class);

  public static void findByRefInstanceIdOrderByIdAsc(List<IObOrderApprover> entity) {
    Mockito.doReturn(entity)
        .when(IObOrderApproverRepMockUtils.repository)
        .findByRefInstanceIdOrderByIdAsc(Mockito.anyLong());
  }

  public static IObOrderApproverRep getRepository() {
    return IObOrderApproverRepMockUtils.repository;
  }
}
