package com.ebs.repositories.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemSummaryGeneralModelRep;
import org.mockito.Mockito;

public class IObOrderItemSummaryGeneralModelRepMockUtils {

  private static final IObOrderItemSummaryGeneralModelRep repository =
      Mockito.mock(IObOrderItemSummaryGeneralModelRep.class);

  public static void findOneByUserCode(IObOrderItemSummaryGeneralModel entity) {
    String code = entity.getOrderCode();
    Mockito.doReturn(entity)
        .when(IObOrderItemSummaryGeneralModelRepMockUtils.repository)
        .findOneByOrderCode(code);
  }

  public static IObOrderItemSummaryGeneralModelRep getRepository() {
    return IObOrderItemSummaryGeneralModelRepMockUtils.repository;
  }
}
