package com.ebs.repositories.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsSummaryGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsSummaryGeneralModelRep;
import org.mockito.Mockito;

public class IObOrderLineDetailsSummaryGeneralModelRepMockUtils {

  private static final IObOrderLineDetailsSummaryGeneralModelRep repository =
      Mockito.mock(IObOrderLineDetailsSummaryGeneralModelRep.class);

  public static void findOneByUserCode(IObOrderLineDetailsSummaryGeneralModel entity) {
    String code = entity.getUserCode();
    Mockito.doReturn(entity)
        .when(IObOrderLineDetailsSummaryGeneralModelRepMockUtils.repository)
        .findOneByUserCode(code);
  }

  public static IObOrderLineDetailsSummaryGeneralModelRep getRepository() {
    return IObOrderLineDetailsSummaryGeneralModelRepMockUtils.repository;
  }
}
