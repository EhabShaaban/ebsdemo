package com.ebs.repositories.order.salesreturn;

import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModel;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModelRep;
import org.mockito.Mockito;

public class SalesReturnOrderDetailsGeneralModelRepMockUtils {

  private static final IObSalesReturnOrderDetailsGeneralModelRep repository =
      Mockito.mock(IObSalesReturnOrderDetailsGeneralModelRep.class);

  private SalesReturnOrderDetailsGeneralModelRepMockUtils() {}

  public static void findOneBySalesReturnOrderCode(IObSalesReturnOrderDetailsGeneralModel entity) {
    String salesReturnOrderCode = entity.getSalesReturnOrderCode();
    Mockito.doReturn(entity)
        .when(SalesReturnOrderDetailsGeneralModelRepMockUtils.repository)
        .findOneBySalesReturnOrderCode(salesReturnOrderCode);
  }

  public static IObSalesReturnOrderDetailsGeneralModelRep getRepository() {
    return SalesReturnOrderDetailsGeneralModelRepMockUtils.repository;
  }
  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
