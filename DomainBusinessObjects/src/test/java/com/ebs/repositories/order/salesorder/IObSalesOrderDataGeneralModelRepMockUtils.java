package com.ebs.repositories.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataGeneralModel;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;
import org.mockito.Mockito;

public class IObSalesOrderDataGeneralModelRepMockUtils {

    private static final IObSalesOrderDataGeneralModelRep repository = Mockito.mock(IObSalesOrderDataGeneralModelRep.class);

    public IObSalesOrderDataGeneralModelRepMockUtils() {}

    public static void findOneByRefInstanceId(IObSalesOrderDataGeneralModel entity) {
        Long id = entity.getId();
        Mockito.doReturn(entity).when(IObSalesOrderDataGeneralModelRepMockUtils.repository).findOneByRefInstanceId(id);
    }

    public static IObSalesOrderDataGeneralModelRep getRepository() {
        return IObSalesOrderDataGeneralModelRepMockUtils.repository;
    }

    public static void resetRepository() {
        Mockito.reset(repository);
    }
}
