package com.ebs.repositories.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import org.mockito.Mockito;

public class DObPurchaseOrderRepMockUtils {

  private static final DObPurchaseOrderRep repository = Mockito.mock(DObPurchaseOrderRep.class);

  public static void findOneByUserCode(DObPurchaseOrder entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity)
        .when(DObPurchaseOrderRepMockUtils.repository)
        .findOneByUserCode(userCode);
  }

  public static DObPurchaseOrderRep getRepository() {
    return DObPurchaseOrderRepMockUtils.repository;
  }
}
