package com.ebs.repositories.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import org.mockito.Mockito;

public class DObSalesOrderRepMockUtils {

  private static final DObSalesOrderRep repository = Mockito.mock(DObSalesOrderRep.class);

  private DObSalesOrderRepMockUtils() {}

  public static void findOneByUserCode(DObSalesOrder entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity).when(DObSalesOrderRepMockUtils.repository).findOneByUserCode(userCode);
  }

  public static DObSalesOrderRep getRepository() {
    return DObSalesOrderRepMockUtils.repository;
  }
}
