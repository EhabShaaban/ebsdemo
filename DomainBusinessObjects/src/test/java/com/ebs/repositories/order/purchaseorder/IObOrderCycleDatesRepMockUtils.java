package com.ebs.repositories.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderCycleDatesRep;
import org.mockito.Mockito;

public class IObOrderCycleDatesRepMockUtils {

  private static final IObOrderCycleDatesRep repository = Mockito.mock(IObOrderCycleDatesRep.class);

  public static void findOneByRefInstanceId(IObOrderCycleDates entity) {
    Long purchaseOrderId = entity.getId();
    Mockito.doReturn(entity)
        .when(IObOrderCycleDatesRepMockUtils.repository)
        .findOneByRefInstanceId(purchaseOrderId);
  }

  public static IObOrderCycleDatesRep getRepository() {
    return IObOrderCycleDatesRepMockUtils.repository;
  }
}
