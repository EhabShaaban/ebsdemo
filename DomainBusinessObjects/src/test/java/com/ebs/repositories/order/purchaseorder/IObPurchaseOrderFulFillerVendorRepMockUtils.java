package com.ebs.repositories.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObPurchaseOrderFulFillerVendor;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObPurchaseOrderFulFillerVendorRep;
import org.mockito.Mockito;

public class IObPurchaseOrderFulFillerVendorRepMockUtils {

    private static final IObPurchaseOrderFulFillerVendorRep repository =
            Mockito.mock(IObPurchaseOrderFulFillerVendorRep.class);

    public static void findOneByRefInstanceId(IObPurchaseOrderFulFillerVendor entity) {
        Long purchaseOrderId = entity.getRefInstanceId();
        Mockito.doReturn(entity)
                .when(IObPurchaseOrderFulFillerVendorRepMockUtils.repository)
                .findOneByRefInstanceId(purchaseOrderId);
    }

    public static IObPurchaseOrderFulFillerVendorRep getRepository() {
        return IObPurchaseOrderFulFillerVendorRepMockUtils.repository;
    }
}
