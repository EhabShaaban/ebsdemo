package com.ebs.repositories.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApprovalCycleRep;
import org.mockito.Mockito;

public class IObOrderApprovalCycleRepMockUtils {

  private static final IObOrderApprovalCycleRep repository =
      Mockito.mock(IObOrderApprovalCycleRep.class);

  public static void findByRefInstanceIdAndFinalDecisionIsNull(IObOrderApprovalCycle entity) {
    Long purchaseOrderId = entity.getId();
    Mockito.doReturn(entity)
        .when(IObOrderApprovalCycleRepMockUtils.repository)
        .findByRefInstanceIdAndFinalDecisionIsNull(purchaseOrderId);
  }

  public static IObOrderApprovalCycleRep getRepository() {
    return IObOrderApprovalCycleRepMockUtils.repository;
  }
}
