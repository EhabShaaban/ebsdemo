package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class VendorInvoiceItemsGeneralModelRepMockUtils {
  private static final IObVendorInvoiceItemsGeneralModelRep repository =
      Mockito.mock(IObVendorInvoiceItemsGeneralModelRep.class);

  private VendorInvoiceItemsGeneralModelRepMockUtils() {}

  public static void findByGoodsReceiptCode(List<IObVendorInvoiceItemsGeneralModel> entities) {
    String invoiceCode = entities.get(0).getInvoiceCode();
    Mockito.doReturn(entities)
        .when(VendorInvoiceItemsGeneralModelRepMockUtils.repository)
        .findByInvoiceCode(invoiceCode);
  }

  public static IObVendorInvoiceItemsGeneralModelRep getRepository() {
    return VendorInvoiceItemsGeneralModelRepMockUtils.repository;
  }
}
