package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import org.mockito.Mockito;

public class IObVendorInvoiceItemsGeneralModelRepUtils {
  private static final IObVendorInvoiceItemsGeneralModelRep repository =
      Mockito.mock(IObVendorInvoiceItemsGeneralModelRep.class);

  private IObVendorInvoiceItemsGeneralModelRepUtils() {}

  public static void findOneByInvoiceCodeAndItemIdAndOrderUnitId(
      IObVendorInvoiceItemsGeneralModel vendorInvoiceItemsGeneralModel) {

    String vendorInvoiceCode = vendorInvoiceItemsGeneralModel.getInvoiceCode();
    Long orderUnitId = vendorInvoiceItemsGeneralModel.getOrderUnitId();
    Long itemId = vendorInvoiceItemsGeneralModel.getItemId();

    Mockito.doReturn(vendorInvoiceItemsGeneralModel)
        .when(repository)
        .findOneByInvoiceCodeAndItemIdAndOrderUnitId(
            vendorInvoiceCode, itemId, orderUnitId);
  }

  public static IObVendorInvoiceItemsGeneralModelRep getRepository() {
    return IObVendorInvoiceItemsGeneralModelRepUtils.repository;
  }
}
