package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsSummaryGeneralModelRep;
import org.mockito.Mockito;

public class LandedCostFactorItemsSummaryGeneralModelRepUtils {

  private static final IObLandedCostFactorItemsSummaryGeneralModelRep repository =
      Mockito.mock(IObLandedCostFactorItemsSummaryGeneralModelRep.class);

  private LandedCostFactorItemsSummaryGeneralModelRepUtils() {}

  public static void findOneByCode(IObLandedCostFactorItemsSummary entity) {
    String code = entity.getCode();
    Mockito.doReturn(entity)
        .when(LandedCostFactorItemsSummaryGeneralModelRepUtils.repository)
        .findOneByCode(code);
  }

  public static IObLandedCostFactorItemsSummaryGeneralModelRep getRepository() {
    return LandedCostFactorItemsSummaryGeneralModelRepUtils.repository;
  }
}
