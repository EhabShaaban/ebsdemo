package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemGeneralModelRep;
import java.util.List;
import org.mockito.Mockito;

public class CostingItemGeneralModelRepMockUtils {
  private static IObCostingItemGeneralModelRep repository =
      Mockito.mock(IObCostingItemGeneralModelRep.class);

  public static void findByUserCodeOrderByItemCodeAsc(List<IObCostingItemGeneralModel> entities) {
    String costingCode = entities.get(0).getUserCode();
    Mockito.doReturn(entities).when(repository).findByUserCodeOrderByItemCodeAsc(costingCode);
  }

  public static IObCostingItemGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
