package com.ebs.repositories.accounting.landedcost;

import java.util.List;

import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItems;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsRep;

public class CostFactorItemRepTestUtils {

  private static final Logger LOGGER = LoggerFactory.getLogger(CostFactorItemRepTestUtils.class);

  private static IObLandedCostFactorItemsRep repository =
    Mockito.mock(IObLandedCostFactorItemsRep.class);

  private CostFactorItemRepTestUtils() {
  }

  public static void findCostFactorItems(List<IObLandedCostFactorItems> entities,
    Long landedcostId) {

    Mockito.doReturn(entities).when(repository).findByRefInstanceId(landedcostId);

    entities.forEach(entity -> {
      try {
        Mockito.doReturn(entity).when(repository).update(entity);
      } catch (Exception e) {
        LOGGER.error("Error while mocking update method", e);
      }
    });
  }

  public static IObLandedCostFactorItemsRep getRepository() {
    return repository;
  }

  public static void reset() {
    Mockito.reset(repository);
  }
}
