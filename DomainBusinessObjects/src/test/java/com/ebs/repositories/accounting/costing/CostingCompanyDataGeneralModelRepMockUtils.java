package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingCompanyDataGeneralModel;
import com.ebs.dda.repositories.accounting.costing.IObCostingCompanyDataGeneralModelRep;
import org.mockito.Mockito;

public class CostingCompanyDataGeneralModelRepMockUtils {

  public static IObCostingCompanyDataGeneralModelRep repository =
      Mockito.mock(IObCostingCompanyDataGeneralModelRep.class);

  public static void findOneByDocumentCode(IObCostingCompanyDataGeneralModel entity) {
    String documentCode = entity.getDocumentCode();
    Mockito.doReturn(entity).when(repository).findOneByDocumentCode(documentCode);
  }

  public static IObCostingCompanyDataGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
