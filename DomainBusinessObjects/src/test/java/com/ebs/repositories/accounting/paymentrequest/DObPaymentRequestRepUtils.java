package com.ebs.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import org.mockito.Mockito;

public class DObPaymentRequestRepUtils {

    private static DObPaymentRequestRep repository =
            Mockito.mock(DObPaymentRequestRep.class);

    private DObPaymentRequestRepUtils() {
    }

    public static void findOneByUserCode(DObPaymentRequest entity) {
        String paymentRequestCode = entity.getUserCode();
        Mockito.doReturn(entity).when(DObPaymentRequestRepUtils.repository).findOneByUserCode(paymentRequestCode);
    }

    public static DObPaymentRequestRep getRepository() {
        return DObPaymentRequestRepUtils.repository;
    }

    public static void resetRepository() {
        DObPaymentRequestRepUtils.repository = Mockito.mock(DObPaymentRequestRep.class);
    }
}
