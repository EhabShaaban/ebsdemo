package com.ebs.repositories.accounting.salesInvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelRep;
import org.mockito.Mockito;

public class DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils {

    private static final DObSalesInvoiceJournalEntryPreparationGeneralModelRep repository =
            Mockito.mock(DObSalesInvoiceJournalEntryPreparationGeneralModelRep.class);

    private DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils() {
    }

    public static void findOneByCode(DObSalesInvoiceJournalEntryPreparationGeneralModel entity) {
        String salesInvoiceCode = entity.getCode();
        Mockito.doReturn(entity)
                .when(DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils.repository)
                .findOneByCode(salesInvoiceCode);
    }

    public static DObSalesInvoiceJournalEntryPreparationGeneralModelRep getRepository() {
        return DObSalesInvoiceJournalEntryPreparationGeneralModelRepUtils.repository;
    }
}
