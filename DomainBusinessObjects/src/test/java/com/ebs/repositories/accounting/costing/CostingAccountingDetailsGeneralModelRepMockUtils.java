package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingAccountingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.costing.IObCostingAccountingDetailsGeneralModelRep;
import java.util.List;
import org.mockito.Mockito;

public class CostingAccountingDetailsGeneralModelRepMockUtils {
  private static final IObCostingAccountingDetailsGeneralModelRep repository =
      Mockito.mock(IObCostingAccountingDetailsGeneralModelRep.class);

  public static void findByRefInstanceId(
      List<IObCostingAccountingDetailsGeneralModel> entities, Long refInstanceId) {
    Mockito.doReturn(entities).when(repository).findByRefInstanceId(refInstanceId);
  }

  public static IObCostingAccountingDetailsGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
