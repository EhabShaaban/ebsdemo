package com.ebs.repositories.accounting.electronicinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class DObSalesInvoiceItemsEInvoiceDataGeneralModelRepMockUtils {

    private static final DObSalesInvoiceItemsEInvoiceDataGeneralModelRep repository =
        Mockito.mock(DObSalesInvoiceItemsEInvoiceDataGeneralModelRep.class);

    public static void mockFindByRefInstanceId(Long salesInvoiceId,
        List<DObSalesInvoiceItemsEInvoiceDataGeneralModel> items) {

        Mockito.doReturn(items).when(repository).findByRefInstanceId(salesInvoiceId);
    }

    public static DObSalesInvoiceItemsEInvoiceDataGeneralModelRep getRepository() {
        return repository;
    }
}
