package com.ebs.repositories.accounting.salesInvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import org.mockito.Mockito;

public class SalesInvoiceBusinessPartnerGeneralModelRepMockUtils {

  private static final IObSalesInvoiceBusinessPartnerGeneralModelRep repository =
      Mockito.mock(IObSalesInvoiceBusinessPartnerGeneralModelRep.class);

  private SalesInvoiceBusinessPartnerGeneralModelRepMockUtils() {}

  public static void findOneByCode(IObSalesInvoiceBusinessPartnerGeneralModel entity) {
    String salesInvoiceCode = entity.getCode();
    Mockito.doReturn(entity)
        .when(SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.repository)
        .findOneByCode(salesInvoiceCode);
  }

  public static void findOneByRefInstanceId(IObSalesInvoiceBusinessPartnerGeneralModel entity) {
    Long salesInvoiceId = entity.getRefInstanceId();
    Mockito.doReturn(entity)
            .when(SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.repository)
            .findOneByRefInstanceId(salesInvoiceId);
  }

  public static IObSalesInvoiceBusinessPartnerGeneralModelRep getRepository() {
    return SalesInvoiceBusinessPartnerGeneralModelRepMockUtils.repository;
  }
  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
