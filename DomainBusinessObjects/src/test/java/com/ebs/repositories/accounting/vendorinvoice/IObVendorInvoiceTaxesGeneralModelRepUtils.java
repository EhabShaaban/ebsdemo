package com.ebs.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class IObVendorInvoiceTaxesGeneralModelRepUtils {
    private static final IObVendorInvoiceTaxesGeneralModelRep repository =
            Mockito.mock(IObVendorInvoiceTaxesGeneralModelRep.class);

    private IObVendorInvoiceTaxesGeneralModelRepUtils() {
    }

    public static void findAllByVendorInvoiceCode(List<IObVendorInvoiceTaxesGeneralModel> entity, String vendorInvoiceCode) {
        Mockito.doReturn(entity)
                .when(IObVendorInvoiceTaxesGeneralModelRepUtils.repository)
                .findAllByInvoiceCode(vendorInvoiceCode);
    }

    public static IObVendorInvoiceTaxesGeneralModelRep getRepository() {
        return IObVendorInvoiceTaxesGeneralModelRepUtils.repository;
    }
}
