package com.ebs.repositories.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class IObNotesReceivableAccountingDetailsGeneralModelRepMockUtils {

  private static IObNotesReceivableAccountingDetailsGeneralModelRep repository =
    Mockito.mock(IObNotesReceivableAccountingDetailsGeneralModelRep.class);

  public static void mockFindByDocumentCodeOrderByAccountingEntryDesc(
    List<IObNotesReceivableAccountingDetailsGeneralModel> accDetailsItems, String nrCode) {

    Mockito.doReturn(accDetailsItems).when(repository)
      .findByDocumentCodeOrderByAccountingEntryDesc(nrCode);

  }

  public static IObNotesReceivableAccountingDetailsGeneralModelRep getRepository() {
    return repository;
  }
}
