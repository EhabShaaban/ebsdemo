package com.ebs.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import org.mockito.Mockito;

public class IObPaymentRequestPaymentDetailsGeneralModelRepUtils {

    private static IObPaymentRequestPaymentDetailsGeneralModelRep repository =
            Mockito.mock(IObPaymentRequestPaymentDetailsGeneralModelRep.class);

    private IObPaymentRequestPaymentDetailsGeneralModelRepUtils() {
    }

    public static void findOneByRefInstanceId(IObPaymentRequestPaymentDetailsGeneralModel generalModel) {
        Long paymentRequestId = generalModel.getRefInstanceId();
        Mockito.doReturn(generalModel)
                .when(IObPaymentRequestPaymentDetailsGeneralModelRepUtils.repository)
                .findOneByRefInstanceId(paymentRequestId);
    }

    public static IObPaymentRequestPaymentDetailsGeneralModelRep getRepository() {
        return IObPaymentRequestPaymentDetailsGeneralModelRepUtils.repository;
    }

    public static void resetRepository() {
        IObPaymentRequestPaymentDetailsGeneralModelRepUtils.repository =
                Mockito.mock(IObPaymentRequestPaymentDetailsGeneralModelRep.class);
    }
}
