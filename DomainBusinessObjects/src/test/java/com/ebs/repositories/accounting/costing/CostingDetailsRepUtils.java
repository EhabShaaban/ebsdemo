package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingDetails;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsRep;
import org.mockito.Mockito;

public class CostingDetailsRepUtils {
  private static final IObCostingDetailsRep repository =
      Mockito.mock(IObCostingDetailsRep.class);

  public static void findOneById(IObCostingDetails entity) {
    Long id = entity.getId();
    Mockito.doReturn(entity).when(repository).findOneById(id);
  }

  public static IObCostingDetailsRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
