package com.ebs.repositories.accounting.monetarynotes;

import com.ebs.dda.repositories.accounting.journalentry.DObNotesReceivableJournalEntryRep;
import org.mockito.Mockito;

public class DObMonetaryNotesJournalEntryRepMockUtils {

  private static DObNotesReceivableJournalEntryRep repository =
    Mockito.mock(DObNotesReceivableJournalEntryRep.class);

  public static DObNotesReceivableJournalEntryRep getRepository() {
    return repository;
  }

  public static void reset() {
    Mockito.reset(repository);
  }
}
