package com.ebs.repositories.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import org.mockito.Mockito;

public class IObJournalEntryItemGeneralModelRepMockUtils {

  private static final IObJournalEntryItemsGeneralModelRep repository =
      Mockito.mock(IObJournalEntryItemsGeneralModelRep.class);

  public static void findFirstByCode(IObJournalEntryItemGeneralModel journalEntryItem) {
    String journalEntryCode = journalEntryItem.getCode();
    Mockito.doReturn(journalEntryItem).when(repository).findFirstByCode(journalEntryCode);
  }

  public static IObJournalEntryItemsGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
