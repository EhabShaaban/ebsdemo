package com.ebs.repositories.accounting.electronicinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceEInvoiceDataGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceEInvoiceDataGeneralModelRep;
import org.mockito.Mockito;

public class DObSalesInvoiceEInvoiceDataGeneralModelRepMockUtils {

    private static final DObSalesInvoiceEInvoiceDataGeneralModelRep repository =
        Mockito.mock(DObSalesInvoiceEInvoiceDataGeneralModelRep.class);

    public static void mockFindOneByCode(String userCode, DObSalesInvoiceEInvoiceDataGeneralModel eInvoiceData) {
        Mockito.doReturn(eInvoiceData).when(repository).findOneByUserCode(userCode);
    }

    public static DObSalesInvoiceEInvoiceDataGeneralModelRep getRepository() {
        return repository;
    }
}
