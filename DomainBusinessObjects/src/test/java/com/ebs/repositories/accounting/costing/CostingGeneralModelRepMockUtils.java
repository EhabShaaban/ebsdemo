package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import com.ebs.dda.repositories.accounting.costing.DObCostingGeneralModelRep;
import org.mockito.Mockito;

public class CostingGeneralModelRepMockUtils {
    private static final DObCostingGeneralModelRep repository =
            Mockito.mock(DObCostingGeneralModelRep.class);

    public static void findOneByUserCode(DObCostingGeneralModel entity) {
        String userCode = entity.getUserCode();
        Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
    }

    public static DObCostingGeneralModelRep getRepository() {
        return repository;
    }

    public static void resetRepository() {
        Mockito.reset(repository);
    }
}
