package com.ebs.repositories.accounting.electronicinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class IObSalesInvoiceTaxesGeneralModelRepMockUtils {

    private static final IObSalesInvoiceTaxesGeneralModelRep repository =
        Mockito.mock(IObSalesInvoiceTaxesGeneralModelRep.class);

    public static void mockFindByRefInstanceId(Long salesInvoiceId,
        List<IObSalesInvoiceTaxesGeneralModel> taxes) {

        Mockito.doReturn(taxes).when(repository).findByRefInstanceId(salesInvoiceId);
    }

    public static IObSalesInvoiceTaxesGeneralModelRep getRepository() {
        return repository;
    }
}
