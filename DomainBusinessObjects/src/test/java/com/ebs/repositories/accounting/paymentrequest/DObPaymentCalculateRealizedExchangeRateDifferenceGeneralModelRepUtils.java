package com.ebs.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep;
import org.mockito.Mockito;

public class DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRepUtils {
    private static final DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep repository =
            Mockito.mock(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep.class);

    public static void findOneByPaymentCode(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel generalModel) {
        String paymentRequestCode = generalModel.getPaymentCode();
        Mockito.doReturn(generalModel).when(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRepUtils.repository).findOneByPaymentCode(paymentRequestCode);
    }

    public static DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep getRepository() {
        return repository;
    }
}
