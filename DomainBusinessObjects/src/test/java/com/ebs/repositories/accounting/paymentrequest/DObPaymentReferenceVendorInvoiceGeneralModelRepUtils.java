package com.ebs.repositories.accounting.paymentrequest;

import org.mockito.Mockito;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceVendorInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceVendorInvoiceGeneralModelRep;

public class DObPaymentReferenceVendorInvoiceGeneralModelRepUtils {
	private static DObPaymentReferenceVendorInvoiceGeneralModelRep repository = Mockito
					.mock(DObPaymentReferenceVendorInvoiceGeneralModelRep.class);

	private DObPaymentReferenceVendorInvoiceGeneralModelRepUtils() {
	}

	public static void findOneByUserCode(DObPaymentReferenceVendorInvoiceGeneralModel entity) {
		String purchaseOrderCode = entity.getCode();
		Mockito.doReturn(entity).when(repository).findOneByCode(purchaseOrderCode);
	}

	public static DObPaymentReferenceVendorInvoiceGeneralModelRep getRepository() {
		return repository;
	}

	public static void resetRepository() {
		repository = Mockito.mock(DObPaymentReferenceVendorInvoiceGeneralModelRep.class);
	}
}
