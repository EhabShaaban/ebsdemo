package com.ebs.repositories.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import org.mockito.Mockito;

public class DObNotesReceivablesGeneralModelRepMockUtils {

    private static DObNotesReceivablesGeneralModelRep repository =
            Mockito.mock(DObNotesReceivablesGeneralModelRep.class);

    public static void findById(DObNotesReceivablesGeneralModel entity) {
        Mockito.when(repository.findById(entity.getId())).thenReturn(java.util.Optional.of(entity));
    }

    public static DObNotesReceivablesGeneralModelRep getRepository() {
        return repository;
    }

    public static void reset() {
        repository = Mockito.mock(DObNotesReceivablesGeneralModelRep.class);
    }
}
