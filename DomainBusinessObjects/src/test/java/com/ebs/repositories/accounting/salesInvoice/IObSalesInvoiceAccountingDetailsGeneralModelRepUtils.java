package com.ebs.repositories.accounting.salesInvoice;

import java.util.List;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModelRep;
import org.mockito.Mockito;

public class IObSalesInvoiceAccountingDetailsGeneralModelRepUtils {

    private static final IObSalesInvoiceAccountingDetailsGeneralModelRep repository =
            Mockito.mock(IObSalesInvoiceAccountingDetailsGeneralModelRep.class);

    private IObSalesInvoiceAccountingDetailsGeneralModelRepUtils() {
    }

    public static void findByDocumentCodeOrderByAccountingEntryDesc(List<IObSalesInvoiceAccountingDetailsGeneralModel> accountingDetailsGeneralModels, String salesInvoiceCode) {
        Mockito.doReturn(accountingDetailsGeneralModels)
                .when(IObSalesInvoiceAccountingDetailsGeneralModelRepUtils.repository)
                .findByDocumentCodeOrderByAccountingEntryDesc(salesInvoiceCode);
    }

    public static IObSalesInvoiceAccountingDetailsGeneralModelRep getRepository() {
        return IObSalesInvoiceAccountingDetailsGeneralModelRepUtils.repository;
    }
}
