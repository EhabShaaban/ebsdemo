package com.ebs.repositories.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObVendorInvoiceJournalEntry;
import com.ebs.dda.repositories.accounting.journalentry.DObVendorInvoiceJournalEntryRep;
import org.mockito.Mockito;

public class DobVendorInvoiceJournalEntryRepMockUtils {
  private static DObVendorInvoiceJournalEntryRep repository =
      Mockito.mock(DObVendorInvoiceJournalEntryRep.class);

  public static void findOneByUserCode(DObVendorInvoiceJournalEntry journalEntry) {
    String journalEntryCode = journalEntry.getUserCode();
    Mockito.doReturn(journalEntry).when(repository).findOneByUserCode(journalEntryCode);
  }

  public static DObVendorInvoiceJournalEntryRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    repository = Mockito.mock(DObVendorInvoiceJournalEntryRep.class);
  }
}
