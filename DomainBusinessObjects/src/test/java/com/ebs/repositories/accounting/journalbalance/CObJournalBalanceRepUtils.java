package com.ebs.repositories.accounting.journalbalance;

import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalance;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceRep;
import org.mockito.Mockito;

public class CObJournalBalanceRepUtils {
    private static final CObJournalBalanceRep repository =
            Mockito.mock(CObJournalBalanceRep.class);

    private CObJournalBalanceRepUtils() {
    }

    public static void findOneByUserCode(CObJournalBalance entity) {
        String journalBalance = entity.getUserCode();
        Mockito.doReturn(entity)
                .when(CObJournalBalanceRepUtils.repository)
                .findOneByUserCode(journalBalance);
    }

    public static CObJournalBalanceRep getRepository() {
        return CObJournalBalanceRepUtils.repository;
    }
}
