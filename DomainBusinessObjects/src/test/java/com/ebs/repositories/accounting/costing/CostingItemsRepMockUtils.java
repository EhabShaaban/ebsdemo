package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingItem;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemRep;
import org.mockito.Mockito;

import java.util.List;

public class CostingItemsRepMockUtils {
  private static final IObCostingItemRep repository =
      Mockito.mock(IObCostingItemRep.class);

  public static void findByRefInstanceId(List<IObCostingItem> costingItems) {
    Long id = costingItems.get(0).getRefInstanceId();
    Mockito.doReturn(costingItems).when(repository).findByRefInstanceId(id);
  }

  public static IObCostingItemRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
