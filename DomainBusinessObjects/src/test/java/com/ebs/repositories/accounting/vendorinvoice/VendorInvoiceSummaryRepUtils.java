package com.ebs.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import org.mockito.Mockito;

public class VendorInvoiceSummaryRepUtils {

  private static final IObVendorInvoiceSummaryRep repository =
      Mockito.mock(IObVendorInvoiceSummaryRep.class);

  private VendorInvoiceSummaryRepUtils() {}

  public static void findOneByRefInstanceId(IObVendorInvoiceSummary entity) {
    Long refInstanceId = entity.getRefInstanceId();
    Mockito.doReturn(entity)
        .when(VendorInvoiceSummaryRepUtils.repository)
        .findOneByRefInstanceId(refInstanceId);
  }

  public static IObVendorInvoiceSummaryRep getRepository() {
    return VendorInvoiceSummaryRepUtils.repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
