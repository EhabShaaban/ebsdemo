package com.ebs.repositories.accounting.salesInvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceSummaryRep;
import org.mockito.Mockito;

public class DObSalesInvoiceRepMockUtils {

  private static final IObSalesInvoiceSummaryRep salesInvoiceSummaryRep =
      Mockito.mock(IObSalesInvoiceSummaryRep.class);
  private static final DObSalesInvoiceRep salesInvoiceRep = Mockito.mock(DObSalesInvoiceRep.class);

  private DObSalesInvoiceRepMockUtils() {}

  public static void findOneByUserCode(DObSalesInvoice entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity)
        .when(DObSalesInvoiceRepMockUtils.getDObSalesInvoiceRep())
        .findOneByUserCode(userCode);
  }
  public static void findIObSalesInvoiceSummaryBySiId(IObSalesInvoiceSummary salesInvoiceSummary, DObSalesInvoice salesInvoice) {
    Long salesInvoiceId = salesInvoice.getId();
    Mockito.doReturn(salesInvoiceSummary)
        .when(DObSalesInvoiceRepMockUtils.getIObSalesInvoiceSummaryRep())
        .findOneByRefInstanceId(salesInvoiceId);
  }

  public static IObSalesInvoiceSummaryRep getIObSalesInvoiceSummaryRep() {
    return DObSalesInvoiceRepMockUtils.salesInvoiceSummaryRep;
  }
  public static DObSalesInvoiceRep getDObSalesInvoiceRep() {
    return DObSalesInvoiceRepMockUtils.salesInvoiceRep;
  }
}
