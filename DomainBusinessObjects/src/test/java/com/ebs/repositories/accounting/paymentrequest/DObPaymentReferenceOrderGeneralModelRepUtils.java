package com.ebs.repositories.accounting.paymentrequest;

import org.mockito.Mockito;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModelRep;

public class DObPaymentReferenceOrderGeneralModelRepUtils {
	private static DObPaymentReferenceOrderGeneralModelRep repository = Mockito
					.mock(DObPaymentReferenceOrderGeneralModelRep.class);

	private DObPaymentReferenceOrderGeneralModelRepUtils() {
	}

	public static void findOneByUserCode(DObPaymentReferenceOrderGeneralModel entity) {
		String purchaseOrderCode = entity.getCode();
		Mockito.doReturn(entity).when(repository).findOneByCode(purchaseOrderCode);
	}

	public static DObPaymentReferenceOrderGeneralModelRep getRepository() {
		return repository;
	}

	public static void resetRepository() {
		repository = Mockito.mock(DObPaymentReferenceOrderGeneralModelRep.class);
	}
}
