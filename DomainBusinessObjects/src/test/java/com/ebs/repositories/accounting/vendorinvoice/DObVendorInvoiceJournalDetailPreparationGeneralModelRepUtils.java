package com.ebs.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep;
import org.mockito.Mockito;

public class DObVendorInvoiceJournalDetailPreparationGeneralModelRepUtils {
    private static final DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep repository =
            Mockito.mock(DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep.class);

    private DObVendorInvoiceJournalDetailPreparationGeneralModelRepUtils() {
    }

    public static void findOneByVendorInvoiceCode(DObVendorInvoiceJournalDetailPreparationGeneralModel entity) {
        String vendorInvoiceCode = entity.getCode();
        Mockito.doReturn(entity)
                .when(DObVendorInvoiceJournalDetailPreparationGeneralModelRepUtils.repository)
                .findOneByCode(vendorInvoiceCode);
    }

    public static DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep getRepository() {
        return DObVendorInvoiceJournalDetailPreparationGeneralModelRepUtils.repository;
    }
}
