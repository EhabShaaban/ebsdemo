package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import org.mockito.Mockito;

public class CostingDetailsGeneralModelRepMockUtils {
    private static IObCostingDetailsGeneralModelRep repository =
            Mockito.mock(IObCostingDetailsGeneralModelRep.class);

    public static void findOneByGoodsReceiptCode(IObCostingDetailsGeneralModel entity) {
        String goodsReceiptCode = entity.getGoodsReceiptCode();
        Mockito.doReturn(entity).when(repository).findOneByGoodsReceiptCode(goodsReceiptCode);
    }

    public static void findOneByUserCode(IObCostingDetailsGeneralModel entity) {
        String userCode = null;
        if (entity != null) {
            userCode = entity.getUserCode();
        }
        Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
    }

    public static IObCostingDetailsGeneralModelRep getRepository() {
        return repository;
    }

    public static void resetRepository() {
        Mockito.reset(repository);
    }
}
