package com.ebs.repositories.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.DObJournalEntryGeneralModelRep;
import org.mockito.Mockito;

public class DObJournalEntryGeneralModelRepMockUtils {

  private static final DObJournalEntryGeneralModelRep repository =
      Mockito.mock(DObJournalEntryGeneralModelRep.class);

  public static void findOneByUserCode(DObJournalEntryGeneralModel journalEntry) {
    String journalEntryCode = journalEntry.getUserCode();
    Mockito.doReturn(journalEntry).when(repository).findOneByUserCode(journalEntryCode);
  }

  public static DObJournalEntryGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
