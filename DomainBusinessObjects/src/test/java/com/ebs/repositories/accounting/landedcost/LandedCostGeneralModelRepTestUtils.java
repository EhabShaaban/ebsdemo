package com.ebs.repositories.accounting.landedcost;

import org.mockito.Mockito;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;

public class LandedCostGeneralModelRepTestUtils {

  private static final DObLandedCostGeneralModelRep repository =
      Mockito.mock(DObLandedCostGeneralModelRep.class);

  private LandedCostGeneralModelRepTestUtils() {}

  public static void findLandedCostByPOAndState(DObLandedCostGeneralModel entity, String poCode,
      String state) {
    Mockito.doReturn(entity).when(repository).findByPurchaseOrderCodeAndCurrentStatesLike(poCode,
        "%" + state + "%");
  }

  public static DObLandedCostGeneralModelRep getRepository() {
    return repository;
  }
}
