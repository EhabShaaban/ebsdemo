package com.ebs.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import org.mockito.Mockito;

public class DObVendorInvoiceRepMockUtils {
  private static DObVendorInvoiceRep repository = Mockito.mock(DObVendorInvoiceRep.class);

  private DObVendorInvoiceRepMockUtils() {}

  public static void findOneByVendorInvoiceCode(DObVendorInvoice entity) {
    String vendorInvoiceCode = entity.getUserCode();
    Mockito.doReturn(entity).when(repository).findOneByUserCode(vendorInvoiceCode);
  }

  public static DObVendorInvoiceRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
