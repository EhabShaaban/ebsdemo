package com.ebs.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import org.mockito.Mockito;

public class CostingRepUtils {
  private static final DObCostingRep repository = Mockito.mock(DObCostingRep.class);

  public static void findOneByUserCode(DObCosting entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
  }

  public static DObCostingRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
