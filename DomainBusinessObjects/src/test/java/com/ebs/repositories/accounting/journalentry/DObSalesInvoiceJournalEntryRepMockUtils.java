package com.ebs.repositories.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObSalesInvoiceJournalEntry;
import com.ebs.dda.repositories.accounting.journalentry.DObSalesInvoiceJournalEntryRep;
import org.mockito.Mockito;

public class DObSalesInvoiceJournalEntryRepMockUtils {

  private static DObSalesInvoiceJournalEntryRep repository =
      Mockito.mock(DObSalesInvoiceJournalEntryRep.class);

  public static void findOneByUserCode(DObSalesInvoiceJournalEntry journalEntry) {
    String journalEntryCode = journalEntry.getUserCode();
    Mockito.doReturn(journalEntry).when(repository).findOneByUserCode(journalEntryCode);
  }

  public static DObSalesInvoiceJournalEntryRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    repository = Mockito.mock(DObSalesInvoiceJournalEntryRep.class);
  }
}
