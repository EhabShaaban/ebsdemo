package com.ebs.repositories.accounting.landedcost;

import org.mockito.Mockito;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;

public class LandedCostRepTestUtils {

	private static final DObLandedCostRep repository = Mockito.mock(DObLandedCostRep.class);

	private LandedCostRepTestUtils() {
	}

	public static void findLandedCostByUserCode(DObLandedCost entity, String landedCostCode) {
		Mockito.doReturn(entity).when(repository).findOneByUserCode(landedCostCode);
	}

	public static DObLandedCostRep getRepository() {
		return repository;
	}
}
