package com.ebs.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class DObVendorInvoiceItemsAccountsPreparationGeneralModelRepUtils {
    private static final DObVendorInvoiceItemAccountsPreparationGeneralModelRep repository =
            Mockito.mock(DObVendorInvoiceItemAccountsPreparationGeneralModelRep.class);

    private DObVendorInvoiceItemsAccountsPreparationGeneralModelRepUtils() {
    }

    public static void findAllByVendorInvoiceCode(List<DObVendorInvoiceItemAccountsPreparationGeneralModel> entity, String vendorInvoiceCode) {
        Mockito.doReturn(entity)
                .when(DObVendorInvoiceItemsAccountsPreparationGeneralModelRepUtils.repository)
                .findAllByCode(vendorInvoiceCode);
    }

    public static DObVendorInvoiceItemAccountsPreparationGeneralModelRep getRepository() {
        return DObVendorInvoiceItemsAccountsPreparationGeneralModelRepUtils.repository;
    }
}
