package com.ebs.repositories.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModelRep;
import org.mockito.Mockito;

public class DObNotesReceivableJournalEntryPreparationGeneralModelRepMockUtils {

  private static DObNotesReceivableJournalEntryPreparationGeneralModelRep repository =
    Mockito.mock(DObNotesReceivableJournalEntryPreparationGeneralModelRep.class);

  public static void mockFindOneByCode(DObNotesReceivableJournalEntryPreparationGeneralModel entity,
    String nrCode) {

    Mockito.doReturn(entity).when(repository).findOneByCode(nrCode);

  }

  public static DObNotesReceivableJournalEntryPreparationGeneralModelRep getRepository() {
    return repository;
  }
}
