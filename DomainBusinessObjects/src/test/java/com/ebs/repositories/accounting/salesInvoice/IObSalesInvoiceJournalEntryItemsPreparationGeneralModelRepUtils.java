package com.ebs.repositories.accounting.salesInvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRepUtils {

    private static final IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep repository =
            Mockito.mock(IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep.class);

    private IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRepUtils() {
    }

    public static void findByCode(List<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel> items, String salesInvoiceCode) {
        Mockito.doReturn(items)
                .when(IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRepUtils.repository)
                .findByCode(salesInvoiceCode);
    }

    public static IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep getRepository() {
        return IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRepUtils.repository;
    }
}
