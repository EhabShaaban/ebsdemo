package com.ebs.repositories.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObPaymentRequestJournalEntry;
import com.ebs.dda.repositories.accounting.journalentry.DObPaymentRequestJournalEntryRep;
import org.mockito.Mockito;

public class DObPaymentRequestJournalEntryRepUtils {

    private static DObPaymentRequestJournalEntryRep repository =
            Mockito.mock(DObPaymentRequestJournalEntryRep.class);

    public static void findOneByUserCode(DObPaymentRequestJournalEntry journalEntry) {
        String journalEntryCode = journalEntry.getUserCode();
        Mockito.doReturn(journalEntry).when(repository).findOneByUserCode(journalEntryCode);
    }

    public static DObPaymentRequestJournalEntryRep getRepository() {
        return repository;
    }

    public static void resetRepository() {
        repository =
                Mockito.mock(DObPaymentRequestJournalEntryRep.class);
    }
}
