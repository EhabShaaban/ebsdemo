package com.ebs.repositories;

import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import org.mockito.Mockito;

public class CompanyRepMockUtils {

  public static CObCompanyGeneralModelRep repository =
      Mockito.mock(CObCompanyGeneralModelRep.class);

  public static void findOneByUserCode(CObCompanyGeneralModel entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
  }

  public static CObCompanyGeneralModelRep getRepository() {
    return repository;
  }
}
