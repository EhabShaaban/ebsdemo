package com.ebs.repositories;

import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import org.mockito.Mockito;

public class DocumentOwnerRepTestUtils {

  private static DocumentOwnerGeneralModelRep repository =
    Mockito.mock(DocumentOwnerGeneralModelRep.class);

  private DocumentOwnerRepTestUtils() {
  }

  public static void mockFindByUserIdAndObjectName(DocumentOwnerGeneralModel documentOwner,
    Long userId, String objectName) {
    Mockito.doReturn(documentOwner).when(repository)
      .findOneByUserIdAndObjectName(userId, objectName);
  }

  public static DocumentOwnerGeneralModelRep getRepository() {
    return repository;
  }
}
