package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import org.mockito.Mockito;

public class AccountingDocumentActivationDetailsGeneralModelRepMockUtils {
  private static final IObAccountingDocumentActivationDetailsGeneralModelRep repository =
      Mockito.mock(IObAccountingDocumentActivationDetailsGeneralModelRep.class);

  private AccountingDocumentActivationDetailsGeneralModelRepMockUtils() {}

  public static void findOneByCodeAndObjectTypeCode(
      IObAccountingDocumentActivationDetailsGeneralModel entity) {
    String invoiceCode = entity.getCode();
    String objectTypeCode = entity.getObjectTypeCode();
    Mockito.doReturn(entity)
        .when(AccountingDocumentActivationDetailsGeneralModelRepMockUtils.repository)
        .findOneByCodeAndObjectTypeCode(invoiceCode, objectTypeCode);
  }

  public static IObAccountingDocumentActivationDetailsGeneralModelRep getRepository() {
    return AccountingDocumentActivationDetailsGeneralModelRepMockUtils.repository;
  }
}
