package com.ebs.repositories.inventory.goodsissue;

import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModelRep;
import org.mockito.Mockito;

public class GoodsIssueRefDocumentDataGeneralModelRepMockUtils {

  private static final IObGoodsIssueRefDocumentDataGeneralModelRep repository =
      Mockito.mock(IObGoodsIssueRefDocumentDataGeneralModelRep.class);

  private GoodsIssueRefDocumentDataGeneralModelRepMockUtils() {}

  public static void findOneByRefDocumentAndGoodsIssueStateContains(IObGoodsIssueRefDocumentDataGeneralModel entity) {
    String refDocumentCode = entity.getRefDocument();
    Mockito.doReturn(entity)
        .when(GoodsIssueRefDocumentDataGeneralModelRepMockUtils.repository)
        .findOneByRefDocumentAndGoodsIssueStateContains(refDocumentCode, "Active");
  }

  public static IObGoodsIssueRefDocumentDataGeneralModelRep getRepository() {
    return GoodsIssueRefDocumentDataGeneralModelRepMockUtils.repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
