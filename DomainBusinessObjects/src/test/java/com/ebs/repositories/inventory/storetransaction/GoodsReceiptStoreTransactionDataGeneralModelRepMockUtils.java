package com.ebs.repositories.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModel;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModelRep;
import java.util.List;
import org.mockito.Mockito;

public class GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils {
  private static final TObGoodsReceiptStoreTransactionDataGeneralModelRep repository =
      Mockito.mock(TObGoodsReceiptStoreTransactionDataGeneralModelRep.class);

  private GoodsReceiptStoreTransactionDataGeneralModelRepMockUtils() {}

  public static void findByUserCode(
      List<TObGoodsReceiptStoreTransactionDataGeneralModel> entities) {
    String goodsReceiptCode = entities.get(0).getUserCode();
    Mockito.doReturn(entities).when(repository).findByUserCode(goodsReceiptCode);
  }

  public static TObGoodsReceiptStoreTransactionDataGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
