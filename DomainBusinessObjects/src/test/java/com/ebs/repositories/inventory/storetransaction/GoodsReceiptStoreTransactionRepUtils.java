package com.ebs.repositories.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsReceiptStoreTransactionRep;
import org.mockito.Mockito;

import java.util.List;

public class GoodsReceiptStoreTransactionRepUtils {
  private static final TObGoodsReceiptStoreTransactionRep repository =
      Mockito.mock(TObGoodsReceiptStoreTransactionRep.class);

  public GoodsReceiptStoreTransactionRepUtils() {}

  public static void findByDocumentReferenceId(List<TObGoodsReceiptStoreTransaction> entityList) {
    Long documentReferenceId = entityList.get(0).getDocumentReferenceId();
    Mockito.doReturn(entityList).when(repository).findByDocumentReferenceId(documentReferenceId);
  }

  public static void findAllById(List<TObGoodsReceiptStoreTransaction> entityList, List<Long> ids) {
    Mockito.doReturn(entityList).when(repository).findAllById(ids);
  }

  public static TObGoodsReceiptStoreTransactionRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
