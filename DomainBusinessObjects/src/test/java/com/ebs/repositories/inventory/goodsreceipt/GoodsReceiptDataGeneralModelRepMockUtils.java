package com.ebs.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModelRep;
import org.mockito.Mockito;

public class GoodsReceiptDataGeneralModelRepMockUtils {

  private static final DObGoodsReceiptDataGeneralModelRep repository =
      Mockito.mock(DObGoodsReceiptDataGeneralModelRep.class);

  private GoodsReceiptDataGeneralModelRepMockUtils() {}

  public static void findOneByUserCode(DObGoodsReceiptDataGeneralModel entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity)
        .when(GoodsReceiptDataGeneralModelRepMockUtils.repository)
        .findOneByUserCode(userCode);
  }

  public static DObGoodsReceiptDataGeneralModelRep getRepository() {
    return GoodsReceiptDataGeneralModelRepMockUtils.repository;
  }
  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
