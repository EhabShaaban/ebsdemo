package com.ebs.repositories.inventory.storetransaction;

import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionRep;
import org.mockito.Mockito;

import java.util.List;

public class StoreTransactionRepUtils {
  private static final TObStoreTransactionRep repository =
      Mockito.mock(TObStoreTransactionRep.class);

  public StoreTransactionRepUtils() {}

  public static void findAllChildrenByRefInstanceIdAndAddTransactionOperation(
      Long id, List<Long> documentReferenceIds) {
    Mockito.doReturn(documentReferenceIds)
        .when(repository)
        .findAllChildrenByRefInstanceIdAndAddTransactionOperation(id);
  }

  public static TObStoreTransactionRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
