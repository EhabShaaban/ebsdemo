package com.ebs.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils {

  private static final IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep repository =
      Mockito.mock(IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep.class);

  private GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils() {}

  public static void findOneByGoodsReceiptCode(List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel> entities) {
    String goodsReceiptCode = entities.get(0).getGoodsReceiptCode();
    Mockito.doReturn(entities)
        .when(GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils.repository)
        .findAllByGoodsReceiptCodeOrderByIdAsc(goodsReceiptCode);
  }

  public static IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep getRepository() {
    return GoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRepMockUtils.repository;
  }
  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
