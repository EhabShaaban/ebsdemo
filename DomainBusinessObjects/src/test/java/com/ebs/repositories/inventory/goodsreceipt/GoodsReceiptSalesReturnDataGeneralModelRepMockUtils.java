package com.ebs.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModelRep;
import org.mockito.Mockito;

public class GoodsReceiptSalesReturnDataGeneralModelRepMockUtils {

  private static final IObGoodsReceiptSalesReturnDataGeneralModelRep repository =
      Mockito.mock(IObGoodsReceiptSalesReturnDataGeneralModelRep.class);

  private GoodsReceiptSalesReturnDataGeneralModelRepMockUtils() {}

  public static void findOneByGoodsReceiptCode(IObGoodsReceiptSalesReturnDataGeneralModel entity) {
    String goodsReceiptCode = entity.getGoodsReceiptCode();
    Mockito.doReturn(entity)
        .when(GoodsReceiptSalesReturnDataGeneralModelRepMockUtils.repository)
        .findOneByGoodsReceiptCode(goodsReceiptCode);
  }

  public static IObGoodsReceiptSalesReturnDataGeneralModelRep getRepository() {
    return GoodsReceiptSalesReturnDataGeneralModelRepMockUtils.repository;
  }
  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
