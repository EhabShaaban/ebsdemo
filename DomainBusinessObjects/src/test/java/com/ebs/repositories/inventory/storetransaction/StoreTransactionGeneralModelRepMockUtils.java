package com.ebs.repositories.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import org.mockito.Mockito;

public class StoreTransactionGeneralModelRepMockUtils {

  private static final TObStoreTransactionGeneralModelRep repository =
      Mockito.mock(TObStoreTransactionGeneralModelRep.class);

  private StoreTransactionGeneralModelRepMockUtils() {}

  public static void findOneByUserCode(TObStoreTransactionGeneralModel entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity)
        .when(StoreTransactionGeneralModelRepMockUtils.repository)
        .findOneByUserCode(userCode);
  }

  public static TObStoreTransactionGeneralModelRep getRepository() {
    return StoreTransactionGeneralModelRepMockUtils.repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
