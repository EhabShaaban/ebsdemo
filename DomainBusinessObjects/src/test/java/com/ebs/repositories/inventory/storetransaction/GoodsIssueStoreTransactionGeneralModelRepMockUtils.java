package com.ebs.repositories.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsIssueStoreTransactionGeneralModel;
import com.ebs.dda.repositories.inventory.storetransaction.TObGoodsIssueStoreTransactionGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class GoodsIssueStoreTransactionGeneralModelRepMockUtils {

  private static final TObGoodsIssueStoreTransactionGeneralModelRep repository =
      Mockito.mock(TObGoodsIssueStoreTransactionGeneralModelRep.class);

  private GoodsIssueStoreTransactionGeneralModelRepMockUtils() {}

  public static void
      findAllByGoodsIssueCodeAndItemCodeAndUnitOfMeasureCodeAndCompanyCodeAndStorehouseCodeAndPlantCodeOrderByUserCodeAsc(
          List<TObGoodsIssueStoreTransactionGeneralModel> entities) {
    String goodsIssueCode = entities.get(0).getGoodsIssueCode();
    String itemCode = entities.get(0).getItemCode();
    String unitOfMeasureCode = entities.get(0).getUnitOfMeasureCode();
    String companyCode = entities.get(0).getCompanyCode();
    String storehouseCode = entities.get(0).getStorehouseCode();
    String plantCode = entities.get(0).getPlantCode();
    Mockito.doReturn(entities)
        .when(GoodsIssueStoreTransactionGeneralModelRepMockUtils.repository)
        .findAllByGoodsIssueCodeAndItemCodeAndUnitOfMeasureCodeAndCompanyCodeAndStorehouseCodeAndPlantCodeOrderByUserCodeAsc(
            goodsIssueCode, itemCode, unitOfMeasureCode, companyCode, storehouseCode, plantCode);
  }

  public static TObGoodsIssueStoreTransactionGeneralModelRep getRepository() {
    return GoodsIssueStoreTransactionGeneralModelRepMockUtils.repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
