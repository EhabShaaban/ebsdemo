package com.ebs.repositories.purchases;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import org.mockito.Mockito;

public class PurchaseOrderRepTestUtils {
    private static final DObPurchaseOrderRep repository = Mockito.mock(DObPurchaseOrderRep.class);

    private PurchaseOrderRepTestUtils() {}

    public static void findOneByUserCode(DObPurchaseOrder entity, String userCode) {
        Mockito.when(PurchaseOrderRepTestUtils.repository.findOneByUserCode(userCode))
                .thenReturn(entity);
    }

    public static DObPurchaseOrderRep getRepository() {
        return PurchaseOrderRepTestUtils.repository;
    }
}
