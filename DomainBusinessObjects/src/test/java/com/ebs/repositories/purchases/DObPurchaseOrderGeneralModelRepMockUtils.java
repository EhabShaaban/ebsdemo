package com.ebs.repositories.purchases;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import org.mockito.Mockito;

public class DObPurchaseOrderGeneralModelRepMockUtils {
    private static final DObPurchaseOrderGeneralModelRep repository = Mockito.mock(DObPurchaseOrderGeneralModelRep.class);

    private DObPurchaseOrderGeneralModelRepMockUtils() {}

    public static void findOneByUserCode(DObPurchaseOrderGeneralModel entity, String userCode) {
        Mockito.when(DObPurchaseOrderGeneralModelRepMockUtils.repository.findOneByUserCode(userCode))
                .thenReturn(entity);
    }

    public static DObPurchaseOrderGeneralModelRep getRepository() {
        return DObPurchaseOrderGeneralModelRepMockUtils.repository;
    }
}
