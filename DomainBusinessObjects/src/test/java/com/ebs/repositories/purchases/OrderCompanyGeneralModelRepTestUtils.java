package com.ebs.repositories.purchases;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import org.mockito.Mockito;

public class OrderCompanyGeneralModelRepTestUtils {
    private static final IObOrderCompanyGeneralModelRep repository = Mockito.mock(IObOrderCompanyGeneralModelRep.class);

    private OrderCompanyGeneralModelRepTestUtils() {}

    public static void findOneByUserCode(IObOrderCompanyGeneralModel entity, String userCode) {
        Mockito.when(OrderCompanyGeneralModelRepTestUtils.repository.findByUserCode(userCode))
                .thenReturn(entity);
    }

    public static IObOrderCompanyGeneralModelRep getRepository() {
        return OrderCompanyGeneralModelRepTestUtils.repository;
    }
}
