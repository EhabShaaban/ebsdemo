package com.ebs.repositories.purchases;

import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class CompanyTaxesGeneralModelRepTestUtils {
    private static final LObCompanyTaxesGeneralModelRep repository = Mockito.mock(LObCompanyTaxesGeneralModelRep.class);

    private CompanyTaxesGeneralModelRepTestUtils() {}

    public static void findOneByUserCode(List<LObCompanyTaxesGeneralModel> entity, String companyCode) {
        Mockito.when(CompanyTaxesGeneralModelRepTestUtils.repository.findAllByCompanyCode(companyCode))
                .thenReturn(entity);
    }

    public static LObCompanyTaxesGeneralModelRep getRepository() {
        return CompanyTaxesGeneralModelRepTestUtils.repository;
    }
}
