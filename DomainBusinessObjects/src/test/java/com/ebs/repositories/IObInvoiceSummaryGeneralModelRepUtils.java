package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import org.mockito.Mockito;

public class IObInvoiceSummaryGeneralModelRepUtils {
  private static final IObInvoiceSummaryGeneralModelRep repository =
      Mockito.mock(IObInvoiceSummaryGeneralModelRep.class);

  private IObInvoiceSummaryGeneralModelRepUtils() {}

  public static IObInvoiceSummaryGeneralModel findOneByInvoiceCodeAndInvoiceType(
      IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel) {

    String vendorInvoiceCode = invoiceSummaryGeneralModel.getInvoiceCode();
    String invoiceType = invoiceSummaryGeneralModel.getInvoiceType();

    Mockito.doReturn(invoiceSummaryGeneralModel)
        .when(repository)
        .findOneByInvoiceCodeAndInvoiceType(vendorInvoiceCode, invoiceType);
    return invoiceSummaryGeneralModel;
  }

  public static IObInvoiceSummaryGeneralModelRep getRepository() {
    return repository;
  }
}
