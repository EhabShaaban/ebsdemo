package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import org.mockito.Mockito;

public class LandedCostGeneralModelRepMockUtils {

  private static final DObLandedCostGeneralModelRep repository =
      Mockito.mock(DObLandedCostGeneralModelRep.class);

  private LandedCostGeneralModelRepMockUtils() {}

  public static void findByPurchaseOrderCodeAndCurrentStatesNotLike(
      DObLandedCostGeneralModel entity) {
    String code = entity.getUserCode();
    String state = "%Draft%";
    Mockito.doReturn(entity)
        .when(repository)
        .findByPurchaseOrderCodeAndCurrentStatesNotLike(code, state);
  }
  public static void findByPurchaseOrderCodeAndCurrentStatesLike(
      DObLandedCostGeneralModel entity,String state) {
    String purchaseOrderCode = entity.getPurchaseOrderCode();
    Mockito.doReturn(entity)
        .when(repository)
        .findByPurchaseOrderCodeAndCurrentStatesLike(purchaseOrderCode,"%"+ state+"%");
  }

  public static void findOneByUserCode(DObLandedCostGeneralModel entity) {
    String code = entity.getUserCode();
    Mockito.doReturn(entity).when(repository).findOneByUserCode(code);
  }

  public static DObLandedCostGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
