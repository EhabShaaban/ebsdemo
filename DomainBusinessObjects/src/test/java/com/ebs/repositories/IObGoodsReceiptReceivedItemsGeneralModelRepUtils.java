package com.ebs.repositories;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModelRep;
import com.ebs.entities.IObGoodsReceiptReceivedItemsGeneralModelMockUtils;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class IObGoodsReceiptReceivedItemsGeneralModelRepUtils {

  private static final IObGoodsReceiptReceivedItemsGeneralModelRep repository =
      Mockito.mock(IObGoodsReceiptReceivedItemsGeneralModelRep.class);

  private IObGoodsReceiptReceivedItemsGeneralModelRepUtils() {}

  public static List<IObGoodsReceiptReceivedItemsGeneralModel> findByGoodsReceiptCode(
      String goodsReceiptCode,List<IObGoodsReceiptReceivedItemsGeneralModel> goodsReceiptReceivedItemsGeneralModels) {


    Mockito.doReturn(goodsReceiptReceivedItemsGeneralModels)
        .when(IObGoodsReceiptReceivedItemsGeneralModelRepUtils.repository)
        .findByGoodsReceiptCode(goodsReceiptCode);
    return goodsReceiptReceivedItemsGeneralModels;
  }

  public static IObGoodsReceiptReceivedItemsGeneralModelRep getRepository() {
    return IObGoodsReceiptReceivedItemsGeneralModelRepUtils.repository;
  }
}
