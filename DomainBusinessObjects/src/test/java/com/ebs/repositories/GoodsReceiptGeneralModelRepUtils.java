package com.ebs.repositories;

import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import org.mockito.Mockito;

public class GoodsReceiptGeneralModelRepUtils {

  private static final DObGoodsReceiptGeneralModelRep repository =
      Mockito.mock(DObGoodsReceiptGeneralModelRep.class);

  private GoodsReceiptGeneralModelRepUtils() {}

  public static void findOneByUserCode(DObGoodsReceiptGeneralModel entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
  }

  public static void findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
      DObGoodsReceiptGeneralModel entity, String currentStateLike) {
    String purchaseOrderCode = entity.getRefDocumentCode();
    String documentType = entity.getInventoryDocumentType();
    Mockito.doReturn(entity)
        .when(repository)
        .findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
            purchaseOrderCode, documentType, currentStateLike);
  }

  public static DObGoodsReceiptGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
