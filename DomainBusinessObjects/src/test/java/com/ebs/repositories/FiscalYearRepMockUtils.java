package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FiscalYearRepMockUtils {

  private final static CObFiscalYearRep repository = Mockito.mock(CObFiscalYearRep.class);

  public static void mockFindActiveFiscalYears(CObFiscalYear ...entities) {
    Mockito.doReturn(Arrays.asList(entities)).when(repository).findByCurrentStatesLike(("%Active%"));
  }

  public static CObFiscalYearRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }

}
