package com.ebs.repositories;

import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.dda.repositories.masterdata.item.IObItemAccountingInfoGeneralModelRep;
import org.mockito.Mockito;

public class ItemAccountingInfoGeneralModelRepMockUtils {
  private static final IObItemAccountingInfoGeneralModelRep repository =
      Mockito.mock(IObItemAccountingInfoGeneralModelRep.class);

  private ItemAccountingInfoGeneralModelRepMockUtils() {}

  public static void findOneByUserCode(IObItemAccountingInfoGeneralModel entity) {
    String itemCode = entity.getItemCode();
    Mockito.doReturn(entity)
        .when(ItemAccountingInfoGeneralModelRepMockUtils.repository)
        .findByItemCode(itemCode);
  }

  public static IObItemAccountingInfoGeneralModelRep getRepository() {
    return ItemAccountingInfoGeneralModelRepMockUtils.repository;
  }
}
