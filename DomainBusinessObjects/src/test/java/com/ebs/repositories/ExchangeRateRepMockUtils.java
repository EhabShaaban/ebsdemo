package com.ebs.repositories;

import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import org.mockito.Mockito;

public class ExchangeRateRepMockUtils {

  private static final CObExchangeRateGeneralModelRep repository =
      Mockito.mock(CObExchangeRateGeneralModelRep.class);

  private ExchangeRateRepMockUtils() {}

  public static void findLatestDefaultExchangeRate(
      CObExchangeRateGeneralModel entity, String firstCurrencyIso, String secondCurrencyIso) {
    Mockito.doReturn(entity)
        .when(repository)
        .findLatestDefaultExchangeRate(firstCurrencyIso, secondCurrencyIso);
  }

  public static CObExchangeRateGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
