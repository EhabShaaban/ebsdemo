package com.ebs.repositories;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import org.mockito.Mockito;

public class VendorInvoiceGeneralModelRepMockUtils {

  private static final DObVendorInvoiceGeneralModelRep repository =
      Mockito.mock(DObVendorInvoiceGeneralModelRep.class);

  private VendorInvoiceGeneralModelRepMockUtils() {}

  public static void findOneByPurchaseOrderCode(DObVendorInvoiceGeneralModel entity) {
    String purchaseOrderCode = entity.getPurchaseOrderCode();
    Mockito.doReturn(entity).when(repository).findOneByPurchaseOrderCode(purchaseOrderCode);
  }

  public static void findOneByUserCode(DObVendorInvoiceGeneralModel entity, String userCode) {
    Mockito.when(repository.findOneByUserCode(userCode))
            .thenReturn(entity);
  }

  public static DObVendorInvoiceGeneralModelRep getRepository() {
    return repository;
  }

  public static void resetRepository() {
    Mockito.reset(repository);
  }
}
