package com.ebs.repositories.payment;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivatePreparationGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObPaymentRequestActivatePreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import org.mockito.Mockito;

public class PaymentRequestRepUtils {

  private static final IObPaymentRequestPaymentDetailsGeneralModelRep
      paymentRequestPaymentDetailsRep =
          Mockito.mock(IObPaymentRequestPaymentDetailsGeneralModelRep.class);
  private static final DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep =
      Mockito.mock(DObPaymentRequestGeneralModelRep.class);
  private static final DObPaymentRequestActivatePreparationGeneralModelRep
      paymentRequestActivatePreparationGeneralModelRep =
          Mockito.mock(DObPaymentRequestActivatePreparationGeneralModelRep.class);

  private static DObPaymentRequestRep paymentRequestRep = Mockito.mock(DObPaymentRequestRep.class);

  private PaymentRequestRepUtils() {}

  public static void findIObPaymentRequestPaymentDetailsByPaymentCode(
      IObPaymentRequestPaymentDetailsGeneralModel entity) {
    String paymentCode = entity.getUserCode();
    Mockito.doReturn(entity)
        .when(PaymentRequestRepUtils.paymentRequestPaymentDetailsRep)
        .findOneByUserCode(paymentCode);
  }

  public static IObPaymentRequestPaymentDetailsGeneralModelRep
      getPaymentRequestPaymentDetailsRep() {
    return PaymentRequestRepUtils.paymentRequestPaymentDetailsRep;
  }

  public static void findPaymentRequestGeneralModelByPaymentCode(
      DObPaymentRequestGeneralModel entity) {
    String paymentCode = entity.getUserCode();
    Mockito.doReturn(entity)
        .when(PaymentRequestRepUtils.paymentRequestGeneralModelRep)
        .findOneByUserCode(paymentCode);
  }

  public static DObPaymentRequestGeneralModelRep getPaymentRequestGeneralModelRep() {
    return PaymentRequestRepUtils.paymentRequestGeneralModelRep;
  }

  public static void findPaymentRequestActivatePreparationGeneralModelByPaymentCode(
      DObPaymentRequestActivatePreparationGeneralModel entity) {
    String paymentCode = entity.getPaymentRequestCode();
    Mockito.doReturn(entity)
        .when(PaymentRequestRepUtils.paymentRequestActivatePreparationGeneralModelRep)
        .findOneByPaymentRequestCode(paymentCode);
  }

  public static DObPaymentRequestActivatePreparationGeneralModelRep
      getPaymentRequestActivatePreparationGeneralModelRep() {
    return PaymentRequestRepUtils.paymentRequestActivatePreparationGeneralModelRep;
  }

  public static DObPaymentRequestRep getPaymentRequestRep() {
    return paymentRequestRep;
  }
  public static void resetRepository() {
    paymentRequestRep = Mockito.mock(DObPaymentRequestRep.class);
  }
}
