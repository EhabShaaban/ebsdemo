package com.ebs.repositories;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep;
import org.mockito.Mockito;

import java.util.List;

public class IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRepUtils {
  private static final IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep repository =
      Mockito.mock(IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep.class);

  private IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRepUtils() {}

  public static List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel> findByGoodsReceiptCodeAndItemCode(
          String grCode,String itemCode, List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel> goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModels) {

    Mockito.doReturn(goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModels)
            .when(IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRepUtils.repository)
            .findByGoodsReceiptCodeAndItemCode(grCode,itemCode);
    return goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModels;
  }

  public static IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep getRepository() {
    return IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRepUtils.repository;
  }
}
