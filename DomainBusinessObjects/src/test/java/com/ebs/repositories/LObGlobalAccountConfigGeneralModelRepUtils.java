package com.ebs.repositories;

import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.entities.LObGlobalAccountConfigGeneralModelMockUtils;
import org.mockito.Mockito;

public class LObGlobalAccountConfigGeneralModelRepUtils {

  private static final LObGlobalAccountConfigGeneralModelRep repository =
      Mockito.mock(LObGlobalAccountConfigGeneralModelRep.class);

  private LObGlobalAccountConfigGeneralModelRepUtils() {}

  public static LObGlobalGLAccountConfigGeneralModel findOneByUserCode(String userCode) {
    if (userCode.equals(ILObGlobalGLAccountConfig.PURCHASING_GL_ACCOUNT)) {
      LObGlobalGLAccountConfigGeneralModel purchasingGLAccountConfigGeneralModel =
          LObGlobalAccountConfigGeneralModelMockUtils.mockGeneralModel().accountId(65L).build();
      Mockito.doReturn(purchasingGLAccountConfigGeneralModel)
          .when(LObGlobalAccountConfigGeneralModelRepUtils.repository)
          .findOneByUserCode(userCode);
      return purchasingGLAccountConfigGeneralModel;
    } else if (userCode.equals(ILObGlobalGLAccountConfig.PURCHASE_ORDER_GL_ACCOUNT)){
      LObGlobalGLAccountConfigGeneralModel purchaseOrderGLAccountConfigGeneralModel =
              LObGlobalAccountConfigGeneralModelMockUtils.mockGeneralModel().accountId(25L).build();
      Mockito.doReturn(purchaseOrderGLAccountConfigGeneralModel)
              .when(LObGlobalAccountConfigGeneralModelRepUtils.repository)
              .findOneByUserCode(userCode);
      return purchaseOrderGLAccountConfigGeneralModel;
    }
    else if (userCode.equals(ILObGlobalGLAccountConfig.REALIZED_GLA_CCOUNT)){
      LObGlobalGLAccountConfigGeneralModel realizedGLAccountConfigGeneralModel =
              LObGlobalAccountConfigGeneralModelMockUtils.mockGeneralModel().accountId(14L).build();
      Mockito.doReturn(realizedGLAccountConfigGeneralModel)
              .when(LObGlobalAccountConfigGeneralModelRepUtils.repository)
              .findOneByUserCode(userCode);
      return realizedGLAccountConfigGeneralModel;
    } else if (userCode.equals(ILObGlobalGLAccountConfig.BANK_GL_ACCOUNT)) {
      LObGlobalGLAccountConfigGeneralModel bankGlAccount = LObGlobalAccountConfigGeneralModelMockUtils
              .mockGeneralModel().accountId(15L).build();
      Mockito.doReturn(bankGlAccount).when(LObGlobalAccountConfigGeneralModelRepUtils.repository)
              .findOneByUserCode(userCode);
      return bankGlAccount;
    } else if (userCode.equals(ILObGlobalGLAccountConfig.CASH_GL_ACCOUNT)) {
      LObGlobalGLAccountConfigGeneralModel cachGlAccount = LObGlobalAccountConfigGeneralModelMockUtils
              .mockGeneralModel().accountId(13L).build();
      Mockito.doReturn(cachGlAccount).when(LObGlobalAccountConfigGeneralModelRepUtils.repository)
              .findOneByUserCode(userCode);
      return cachGlAccount;
    }
    return null;
  }

  public static LObGlobalAccountConfigGeneralModelRep getRepository() {
    return LObGlobalAccountConfigGeneralModelRepUtils.repository;
  }
}
