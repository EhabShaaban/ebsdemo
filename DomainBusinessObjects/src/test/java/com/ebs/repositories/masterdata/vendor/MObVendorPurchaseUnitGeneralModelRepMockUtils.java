package com.ebs.repositories.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRep;
import org.mockito.Mockito;

public class MObVendorPurchaseUnitGeneralModelRepMockUtils {
  private static final MObVendorPurchaseUnitGeneralModelRep repository =
      Mockito.mock(MObVendorPurchaseUnitGeneralModelRep.class);

  private MObVendorPurchaseUnitGeneralModelRepMockUtils() {}

  public static void findOneByUserCodeAndPurchaseUnitCode(
      MObVendorPurchaseUnitGeneralModel entity, String userCode, String PurchaseUnitCode) {
    Mockito.when(
            MObVendorPurchaseUnitGeneralModelRepMockUtils.repository
                .findOneByUserCodeAndPurchaseUnitCode(userCode, PurchaseUnitCode))
        .thenReturn(entity);
  }

  public static MObVendorPurchaseUnitGeneralModelRep getRepository() {
    return MObVendorPurchaseUnitGeneralModelRepMockUtils.repository;
  }
}
