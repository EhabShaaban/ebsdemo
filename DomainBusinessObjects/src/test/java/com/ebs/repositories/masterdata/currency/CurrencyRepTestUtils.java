package com.ebs.repositories.masterdata.currency;

import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import org.mockito.Mockito;

public class CurrencyRepTestUtils {

  private static final CObCurrencyRep repository = Mockito.mock(CObCurrencyRep.class);

  private CurrencyRepTestUtils() {
  }

  public static void mockFindByIso(CObCurrency entity, String iso) {
    Mockito.doReturn(entity).when(repository).findOneByIso(iso);
  }

  public static void reset() {
    Mockito.reset(repository);
  }

  public static CObCurrencyRep getRepository() {
    return repository;
  }

}
