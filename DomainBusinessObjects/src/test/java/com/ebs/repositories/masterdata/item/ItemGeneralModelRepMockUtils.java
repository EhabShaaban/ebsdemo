package com.ebs.repositories.masterdata.item;

import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import org.mockito.Mockito;

public class ItemGeneralModelRepMockUtils {
  public static MObItemGeneralModelRep repository = Mockito.mock(MObItemGeneralModelRep.class);

  public static void findByUserCode(MObItemGeneralModel entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity).when(repository).findByUserCode(userCode);
  }

  public static MObItemGeneralModelRep getRepository() {
    return repository;
  }
}
