package com.ebs.repositories.masterdata.businessunit;

import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import org.mockito.Mockito;

public class BusinessUnitRepTestUtils {

  private static final CObPurchasingUnitGeneralModelRep repository =
    Mockito.mock(CObPurchasingUnitGeneralModelRep.class);

  private BusinessUnitRepTestUtils() {
  }

  public static void mockFindBusinessUnitByCode(CObPurchasingUnitGeneralModel entity, String userCode) {
    Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
  }

  public static void reset() {
    Mockito.reset(repository);
  }

  public static CObPurchasingUnitGeneralModelRep getRepository() {
    return repository;
  }
}
