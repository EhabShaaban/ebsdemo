package com.ebs.repositories.masterdata.businessunit;

import org.mockito.Mockito;

import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;

public class CObPurchasingUnitRepTestUtils {

	private static final CObPurchasingUnitRep repository = Mockito.mock(CObPurchasingUnitRep.class);

	private CObPurchasingUnitRepTestUtils() {
	}

	public static void mockFindBusinessUnitByCode(CObPurchasingUnit entity, String userCode) {
		Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
	}

	public static void reset() {
		Mockito.reset(repository);
	}

	public static CObPurchasingUnitRep getRepository() {
		return repository;
	}
}
