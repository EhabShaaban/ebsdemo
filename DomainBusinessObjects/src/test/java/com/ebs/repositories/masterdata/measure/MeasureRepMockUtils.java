package com.ebs.repositories.masterdata.measure;

import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import org.mockito.Mockito;

public class MeasureRepMockUtils {
  public static CObMeasureRep repository = Mockito.mock(CObMeasureRep.class);

  public static void findOneByUserCode(CObMeasure entity) {
    String userCode = entity.getUserCode();
    Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
  }

  public static CObMeasureRep getRepository() {
    return repository;
  }
}
