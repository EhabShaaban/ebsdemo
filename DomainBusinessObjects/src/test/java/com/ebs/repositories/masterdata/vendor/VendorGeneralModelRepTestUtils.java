package com.ebs.repositories.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import org.mockito.Mockito;

public class VendorGeneralModelRepTestUtils {
    private static final MObVendorGeneralModelRep repository = Mockito.mock(MObVendorGeneralModelRep.class);

    private VendorGeneralModelRepTestUtils() {}

    public static void findOneByUserCode(MObVendorGeneralModel entity, String userCode) {
        Mockito.when(VendorGeneralModelRepTestUtils.repository.findOneByUserCode(userCode))
                .thenReturn(entity);
    }

    public static MObVendorGeneralModelRep getRepository() {
        return VendorGeneralModelRepTestUtils.repository;
    }
}
