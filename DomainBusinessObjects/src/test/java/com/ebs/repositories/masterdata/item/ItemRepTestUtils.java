package com.ebs.repositories.masterdata.item;

import org.mockito.Mockito;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;

public class ItemRepTestUtils {

  private static final MObItemRep repository = Mockito.mock(MObItemRep.class);

  private ItemRepTestUtils() {}

  public static void findItemByCode(MObItem entity, String userCode) {
    Mockito.doReturn(entity).when(repository).findOneByUserCode(userCode);
  }

  public static MObItemRep getRepository() {
    return repository;
  }
}
