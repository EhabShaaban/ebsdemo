package com.ebs.repositories.masterdata.customer;

import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import org.mockito.Mockito;

public class CustomerBusinessUnitRepTestUtils {

  private static final IObCustomerBusinessUnitGeneralModelRep repository =
    Mockito.mock(IObCustomerBusinessUnitGeneralModelRep.class);

  private CustomerBusinessUnitRepTestUtils() {
  }

  public static void mockFindCustomerByCodeAndBusinessUnitCode(IObCustomerBusinessUnitGeneralModel entity,
    String customerCode, String businessUnitCode) {
    Mockito.doReturn(entity).when(repository)
      .findOneByUserCodeAndBusinessUnitcode(customerCode, businessUnitCode);
  }

  public static void reset() {
    Mockito.reset(repository);
  }

  public static IObCustomerBusinessUnitGeneralModelRep getRepository() {
    return repository;
  }
}
