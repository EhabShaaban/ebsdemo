package com.ebs.repositories.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import org.mockito.Mockito;

public class MObVendorRepUtils {

    private static MObVendorRep repository = Mockito.mock(MObVendorRep.class);

    private MObVendorRepUtils() {}

    public static void findOneByUserCode(MObVendor entity) {
        String userCode = entity.getUserCode();
        Mockito.doReturn(entity).when(MObVendorRepUtils.repository).findOneByUserCode(userCode);
    }

    public static MObVendorRep getRepository() {
        return MObVendorRepUtils.repository;
    }

    public static void resetRepository() { MObVendorRepUtils.repository = Mockito.mock(MObVendorRep.class); }
}
