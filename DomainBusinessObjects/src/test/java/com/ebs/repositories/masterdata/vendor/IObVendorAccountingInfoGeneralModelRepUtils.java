package com.ebs.repositories.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import org.mockito.Mockito;

public class IObVendorAccountingInfoGeneralModelRepUtils {

    private static IObVendorAccountingInfoGeneralModelRep repository = Mockito.mock(IObVendorAccountingInfoGeneralModelRep.class);

    private IObVendorAccountingInfoGeneralModelRepUtils() {}

    public static void findByVendorCode(IObVendorAccountingInfoGeneralModel entity) {
        String vendorCode = entity.getVendorCode();
        Mockito.doReturn(entity).when(IObVendorAccountingInfoGeneralModelRepUtils.repository).findByVendorCode(vendorCode);
    }

    public static IObVendorAccountingInfoGeneralModelRep getRepository() {
        return IObVendorAccountingInfoGeneralModelRepUtils.repository;
    }

    public static void resetRepository() { IObVendorAccountingInfoGeneralModelRepUtils.repository = Mockito.mock(IObVendorAccountingInfoGeneralModelRep.class); }
}
