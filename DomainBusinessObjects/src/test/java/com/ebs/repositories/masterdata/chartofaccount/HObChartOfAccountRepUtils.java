package com.ebs.repositories.masterdata.chartofaccount;

import com.ebs.dda.jpa.masterdata.chartofaccount.HObGLAccount;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountRep;
import org.mockito.Mockito;

public class HObChartOfAccountRepUtils {

    private static HObChartOfAccountRep repository = Mockito.mock(HObChartOfAccountRep.class);

    private HObChartOfAccountRepUtils() {}

    public static void findOneByUserCode(HObGLAccount entity) {
        String userCode = entity.getUserCode();
        Mockito.doReturn(entity).when(HObChartOfAccountRepUtils.repository).findOneByUserCode(userCode);
    }

    public static HObChartOfAccountRep getRepository() {
        return HObChartOfAccountRepUtils.repository;
    }

    public static void resetRepository() { HObChartOfAccountRepUtils.repository = Mockito.mock(HObChartOfAccountRep.class); }
}
