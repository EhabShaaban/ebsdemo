package com.ebs.entities;

import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class DocumentOwnerTestUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static DocumentOwnerGeneralModel buildGeneralModel(Long userId) {
    DocumentOwnerGeneralModel documentOwner = Mockito.mock(DocumentOwnerGeneralModel.class);
    Mockito.doReturn(userId).when(documentOwner).getUserId();
    return documentOwner;
  }
}
