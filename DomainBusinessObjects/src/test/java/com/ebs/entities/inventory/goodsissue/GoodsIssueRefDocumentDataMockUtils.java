package com.ebs.entities.inventory.goodsissue;

import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class GoodsIssueRefDocumentDataMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObGoodsIssueRefDocumentDataGeneralModel buildGeneralModel(String goodsIssueCode, String salesOrderCode) {
    IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel =
        Mockito.mock(IObGoodsIssueRefDocumentDataGeneralModel.class);
    Mockito.doReturn(goodsIssueCode)
        .when(goodsIssueRefDocumentDataGeneralModel)
        .getGoodsIssueCode();
    Mockito.doReturn(salesOrderCode)
        .when(goodsIssueRefDocumentDataGeneralModel)
        .getRefDocument();
    return goodsIssueRefDocumentDataGeneralModel;
  }
}
