package com.ebs.entities.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import lombok.Builder;
import lombok.Singular;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.List;

public class GoodsReceiptStoreTransactionMockUtils {
  @Builder(builderMethodName = "mockEntity")
  public static TObGoodsReceiptStoreTransaction buildEntity(
      String userCode,
      String creationInfo,
      String modificationInfo,
      DateTime creationDate,
      DateTime modifiedDate,
      Long documentReferenceId,
      Long itemId,
      Long companyId,
      Long plantId,
      Long storehouseId,
      Long purchaseUnitId,
      DateTime originalAddingDate,
      Long unitOfMeasureId,
      StockTypeEnum stockType,
      TransactionTypeEnum transactionOperation,
      BigDecimal quantity,
      BigDecimal remainingQuantity,
      BigDecimal estimateCost,
      BigDecimal actualCost,
      Long id) {
    TObGoodsReceiptStoreTransaction goodsReceiptStoreTransaction =
        Mockito.mock(TObGoodsReceiptStoreTransaction.class);

    Mockito.doReturn(userCode).when(goodsReceiptStoreTransaction).getUserCode();
    Mockito.doReturn(creationInfo).when(goodsReceiptStoreTransaction).getCreationInfo();
    Mockito.doReturn(modificationInfo).when(goodsReceiptStoreTransaction).getModificationInfo();
    Mockito.doReturn(creationDate).when(goodsReceiptStoreTransaction).getCreationDate();
    Mockito.doReturn(modifiedDate).when(goodsReceiptStoreTransaction).getModifiedDate();
    Mockito.doReturn(documentReferenceId)
        .when(goodsReceiptStoreTransaction)
        .getDocumentReferenceId();
    Mockito.doReturn(itemId).when(goodsReceiptStoreTransaction).getItemId();
    Mockito.doReturn(id).when(goodsReceiptStoreTransaction).getId();
    Mockito.doReturn(companyId).when(goodsReceiptStoreTransaction).getCompanyId();
    Mockito.doReturn(plantId).when(goodsReceiptStoreTransaction).getPlantId();
    Mockito.doReturn(storehouseId).when(goodsReceiptStoreTransaction).getStorehouseId();
    Mockito.doReturn(purchaseUnitId).when(goodsReceiptStoreTransaction).getPurchaseUnitId();
    Mockito.doReturn(originalAddingDate).when(goodsReceiptStoreTransaction).getOriginalAddingDate();
    Mockito.doReturn(unitOfMeasureId).when(goodsReceiptStoreTransaction).getUnitOfMeasureId();
    Mockito.doReturn(stockType).when(goodsReceiptStoreTransaction).getStockType();
    Mockito.doReturn(transactionOperation)
        .when(goodsReceiptStoreTransaction)
        .getTransactionOperation();
    Mockito.doReturn(quantity).when(goodsReceiptStoreTransaction).getQuantity();
    Mockito.doReturn(remainingQuantity).when(goodsReceiptStoreTransaction).getRemainingQuantity();

    Mockito.doReturn(estimateCost).when(goodsReceiptStoreTransaction).getEstimateCost();
    Mockito.doReturn(actualCost).when(goodsReceiptStoreTransaction).getActualCost();
    return goodsReceiptStoreTransaction;
  }
  @Builder(builderMethodName = "mockEntityList")
  public static List<TObGoodsReceiptStoreTransaction> buildEntityList(
      @Singular List<TObGoodsReceiptStoreTransaction> goodsReceiptStoreTransactions) {
    return goodsReceiptStoreTransactions;
  }
}
