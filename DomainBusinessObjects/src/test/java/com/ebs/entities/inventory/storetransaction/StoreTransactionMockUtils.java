package com.ebs.entities.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class StoreTransactionMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static TObStoreTransactionGeneralModel buildGeneralModel(
          String userCode, DateTime originalAddingDate, BigDecimal estimateCost, BigDecimal actualCost) {
    TObStoreTransactionGeneralModel storeTransactionGeneralModel =
        Mockito.mock(TObStoreTransactionGeneralModel.class);
    Mockito.doReturn(userCode).when(storeTransactionGeneralModel).getUserCode();
    Mockito.doReturn(originalAddingDate)
        .when(storeTransactionGeneralModel)
        .getOriginalAddingDate();
    Mockito.doReturn(estimateCost).when(storeTransactionGeneralModel).getEstimateCost();
    Mockito.doReturn(actualCost).when(storeTransactionGeneralModel).getActualCost();
    return storeTransactionGeneralModel;
  }
}
