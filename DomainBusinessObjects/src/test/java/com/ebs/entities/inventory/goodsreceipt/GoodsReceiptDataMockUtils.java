package com.ebs.entities.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class GoodsReceiptDataMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static DObGoodsReceiptDataGeneralModel buildGeneralModel(
      Long id,
      String userCode,
      String companyCode,
      String plantCode,
      String storehouseCode,
      Long companyId,
      Long plantId,
      Long storehouseId,
      Long purchaseUnitId) {
    DObGoodsReceiptDataGeneralModel goodsReceiptGeneralModel =
        Mockito.mock(DObGoodsReceiptDataGeneralModel.class);
    Mockito.doReturn(id).when(goodsReceiptGeneralModel).getId();
    Mockito.doReturn(userCode).when(goodsReceiptGeneralModel).getUserCode();
    Mockito.doReturn(companyCode).when(goodsReceiptGeneralModel).getCompanyCode();
    Mockito.doReturn(plantCode).when(goodsReceiptGeneralModel).getPlantCode();
    Mockito.doReturn(storehouseCode).when(goodsReceiptGeneralModel).getStorehouseCode();
    Mockito.doReturn(companyId).when(goodsReceiptGeneralModel).getId();
    Mockito.doReturn(plantId).when(goodsReceiptGeneralModel).getId();
    Mockito.doReturn(storehouseId).when(goodsReceiptGeneralModel).getId();
    Mockito.doReturn(purchaseUnitId).when(goodsReceiptGeneralModel).getId();
    return goodsReceiptGeneralModel;
  }
}
