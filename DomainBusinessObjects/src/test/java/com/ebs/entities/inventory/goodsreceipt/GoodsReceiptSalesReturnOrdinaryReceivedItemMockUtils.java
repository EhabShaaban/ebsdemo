package com.ebs.entities.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class GoodsReceiptSalesReturnOrdinaryReceivedItemMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel buildGeneralModel(
      String goodsReceiptCode,
      String itemCode,
      String unitOfEntryCode,
      String stockType,
      BigDecimal receivedQtyUoE) {
    IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
        goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel =
            Mockito.mock(IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel.class);
    Mockito.doReturn(goodsReceiptCode)
        .when(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
        .getGoodsReceiptCode();
    Mockito.doReturn(itemCode)
        .when(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
        .getItemCode();
    Mockito.doReturn(unitOfEntryCode)
        .when(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
        .getUnitOfEntryCode();
    Mockito.doReturn(stockType)
        .when(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
        .getStockType();
    Mockito.doReturn(receivedQtyUoE)
        .when(goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel)
        .getReceivedQtyUoE();
    return goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
  }
}
