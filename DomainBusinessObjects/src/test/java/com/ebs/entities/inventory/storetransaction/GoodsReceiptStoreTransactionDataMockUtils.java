package com.ebs.entities.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransactionDataGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class GoodsReceiptStoreTransactionDataMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static TObGoodsReceiptStoreTransactionDataGeneralModel buildGeneralModel(
      String userCode,
      Long goodsReceiptId,
      Long itemId,
      Long companyId,
      Long plantId,
      Long storehouseId,
      Long purchaseUnitId,
      Long receivedUnitOfEntryId,
      BigDecimal receivedQty,
      String stockType,
      String itemCode,
      String receivedUnitOfEntryCode) {

    TObGoodsReceiptStoreTransactionDataGeneralModel goodsReceiptStoreTransactionData =
        Mockito.mock(TObGoodsReceiptStoreTransactionDataGeneralModel.class);

    Mockito.doReturn(userCode).when(goodsReceiptStoreTransactionData).getUserCode();
    Mockito.doReturn(goodsReceiptId).when(goodsReceiptStoreTransactionData).getGoodsReceiptId();
    Mockito.doReturn(itemId).when(goodsReceiptStoreTransactionData).getItemId();
    Mockito.doReturn(companyId).when(goodsReceiptStoreTransactionData).getCompanyId();
    Mockito.doReturn(plantId).when(goodsReceiptStoreTransactionData).getPlantId();
    Mockito.doReturn(storehouseId).when(goodsReceiptStoreTransactionData).getStorehouseId();
    Mockito.doReturn(purchaseUnitId).when(goodsReceiptStoreTransactionData).getPurchaseUnitId();
    Mockito.doReturn(receivedUnitOfEntryId)
        .when(goodsReceiptStoreTransactionData)
        .getReceivedUoeId();
    Mockito.doReturn(receivedQty).when(goodsReceiptStoreTransactionData).getReceivedQty();
    Mockito.doReturn(stockType).when(goodsReceiptStoreTransactionData).getReceivedStockType();

    Mockito.doReturn(itemCode).when(goodsReceiptStoreTransactionData).getItemCode();
    Mockito.doReturn(receivedUnitOfEntryCode)
        .when(goodsReceiptStoreTransactionData)
        .getReceivedUoeCode();
    return goodsReceiptStoreTransactionData;
  }
}
