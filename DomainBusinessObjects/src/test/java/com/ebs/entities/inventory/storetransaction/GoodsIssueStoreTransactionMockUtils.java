package com.ebs.entities.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsIssueStoreTransactionGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class GoodsIssueStoreTransactionMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static TObGoodsIssueStoreTransactionGeneralModel buildGeneralModel(
      String userCode, String refTransactionCode, BigDecimal quantity, Long id, String goodsIssueCode,
      String itemCode, String unitOfMeasureCode, String companyCode, String storeHouseCode, String plantCode) {
    TObGoodsIssueStoreTransactionGeneralModel goodsIssueStoreTransactionGeneralModel =
        Mockito.mock(TObGoodsIssueStoreTransactionGeneralModel.class);
    Mockito.doReturn(userCode).when(goodsIssueStoreTransactionGeneralModel).getUserCode();
    Mockito.doReturn(refTransactionCode)
        .when(goodsIssueStoreTransactionGeneralModel)
        .getRefTransactionCode();
    Mockito.doReturn(id).when(goodsIssueStoreTransactionGeneralModel).getId();
    Mockito.doReturn(quantity).when(goodsIssueStoreTransactionGeneralModel).getQuantity();
    Mockito.doReturn(goodsIssueCode).when(goodsIssueStoreTransactionGeneralModel).getGoodsIssueCode();
    Mockito.doReturn(itemCode).when(goodsIssueStoreTransactionGeneralModel).getItemCode();
    Mockito.doReturn(unitOfMeasureCode).when(goodsIssueStoreTransactionGeneralModel).getUnitOfMeasureCode();
    Mockito.doReturn(companyCode).when(goodsIssueStoreTransactionGeneralModel).getCompanyCode();
    Mockito.doReturn(storeHouseCode).when(goodsIssueStoreTransactionGeneralModel).getStorehouseCode();
    Mockito.doReturn(plantCode).when(goodsIssueStoreTransactionGeneralModel).getPlantCode();
    return goodsIssueStoreTransactionGeneralModel;
  }
}
