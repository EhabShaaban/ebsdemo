package com.ebs.entities.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class GoodsReceiptSalesReturnDataMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObGoodsReceiptSalesReturnDataGeneralModel buildGeneralModel(
      String goodsReceiptCode, String salesReturnCode) {
    IObGoodsReceiptSalesReturnDataGeneralModel goodsReceiptSalesReturnDataGeneralModel =
        Mockito.mock(IObGoodsReceiptSalesReturnDataGeneralModel.class);
    Mockito.doReturn(goodsReceiptCode).when(goodsReceiptSalesReturnDataGeneralModel).getGoodsReceiptCode();
    Mockito.doReturn(salesReturnCode).when(goodsReceiptSalesReturnDataGeneralModel).getSalesReturnOrderCode();
    return goodsReceiptSalesReturnDataGeneralModel;
  }
}
