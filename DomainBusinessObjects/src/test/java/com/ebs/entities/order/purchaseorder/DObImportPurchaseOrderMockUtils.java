package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObImportPurchaseOrder;
import lombok.Builder;
import org.mockito.Mockito;

public class DObImportPurchaseOrderMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static DObImportPurchaseOrder buildEntity(
      Long id, String userCode) {
    DObImportPurchaseOrder purchaseOrder = Mockito.mock(DObImportPurchaseOrder.class);
    Mockito.doReturn(id).when(purchaseOrder).getId();
    Mockito.doReturn(userCode).when(purchaseOrder).getUserCode();
    return purchaseOrder;
  }
}
