package com.ebs.entities.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.HashSet;

public class DObSalesOrderMockUtils {

  @Builder(builderMethodName = "newEntity")
  public static DObSalesOrder buildDObSalesOrder(Long id, String userCode, BigDecimal remaining) {
    DObSalesOrder salesOrder = new DObSalesOrder();
    salesOrder.setId(id);
    salesOrder.setUserCode(userCode);
    salesOrder.setRemaining(remaining);
    return salesOrder;
  }

  @Builder(builderMethodName = "mockGeneralModel")
  public static DObSalesOrderGeneralModel buildGeneralModel(
      String salesOrderCode,
      BigDecimal remaining,
      HashSet<String> currentStates,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        Mockito.mock(DObSalesOrderGeneralModel.class);

    Mockito.doReturn(salesOrderCode).when(salesOrderGeneralModel).getUserCode();
    Mockito.doReturn(remaining).when(salesOrderGeneralModel).getRemaining();
    Mockito.doReturn(currentStates).when(salesOrderGeneralModel).getCurrentStates();
    Mockito.doReturn(businessPartnerCode).when(salesOrderGeneralModel).getCustomerCode();
    Mockito.doReturn(companyCode).when(salesOrderGeneralModel).getCompanyCode();
    Mockito.doReturn(businessUnitCode).when(salesOrderGeneralModel).getPurchaseUnitCode();

    return salesOrderGeneralModel;
  }
}
