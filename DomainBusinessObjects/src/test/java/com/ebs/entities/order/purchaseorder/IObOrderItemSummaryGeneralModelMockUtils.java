package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemSummaryGeneralModel;
import java.math.BigDecimal;
import lombok.Builder;
import org.mockito.Mockito;

public class IObOrderItemSummaryGeneralModelMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObOrderItemSummaryGeneralModel buildGeneralModel(
      String userCode, BigDecimal totalAmountAfterDiscount) {
    IObOrderItemSummaryGeneralModel orderItemSummaryGeneralModel =
        Mockito.mock(IObOrderItemSummaryGeneralModel.class);
    Mockito.doReturn(userCode).when(orderItemSummaryGeneralModel).getOrderCode();
    Mockito.doReturn(totalAmountAfterDiscount)
        .when(orderItemSummaryGeneralModel)
        .getTotalAmountAfterTaxes();
    return orderItemSummaryGeneralModel;
  }
}
