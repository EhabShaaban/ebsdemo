package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObImportPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import lombok.Builder;

import java.math.BigDecimal;

public class DObPurchaseOrderMockUtils {

  private static String poCode;
  private static BigDecimal remaining;

  @Builder(builderMethodName = "newPurchaseOrder")
  public static DObPurchaseOrder buildPurchaseOrder() {
    DObPurchaseOrder purchaseOrder = new DObImportPurchaseOrder();
    purchaseOrder.setRemaining(remaining);
    purchaseOrder.setUserCode(poCode);
    return purchaseOrder;
  }

  public static void withpoCode(String poCode) {
    DObPurchaseOrderMockUtils.poCode = poCode;
  }

  public static void withRemaining(BigDecimal remaining) {
    DObPurchaseOrderMockUtils.remaining = remaining;
  }
}
