package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObLocalPurchaseOrder;
import lombok.Builder;
import org.mockito.Mockito;

public class DObLocalPurchaseOrderMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static DObLocalPurchaseOrder buildEntity(
      Long id, String userCode) {
    DObLocalPurchaseOrder purchaseOrder = Mockito.mock(DObLocalPurchaseOrder.class);
    Mockito.doReturn(id).when(purchaseOrder).getId();
    Mockito.doReturn(userCode).when(purchaseOrder).getUserCode();
    return purchaseOrder;
  }
}
