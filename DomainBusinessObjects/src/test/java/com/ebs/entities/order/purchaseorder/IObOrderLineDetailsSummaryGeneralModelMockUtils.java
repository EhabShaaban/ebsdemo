package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsSummaryGeneralModel;
import java.math.BigDecimal;
import lombok.Builder;
import org.mockito.Mockito;

public class IObOrderLineDetailsSummaryGeneralModelMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObOrderLineDetailsSummaryGeneralModel buildGeneralModel(String userCode, BigDecimal totalAmountAfterDiscount) {
    IObOrderLineDetailsSummaryGeneralModel orderLineDetailsSummaryGeneralModel =
        Mockito.mock(IObOrderLineDetailsSummaryGeneralModel.class);
    Mockito.doReturn(userCode).when(orderLineDetailsSummaryGeneralModel).getUserCode();
    Mockito.doReturn(totalAmountAfterDiscount).when(orderLineDetailsSummaryGeneralModel).getTotalAmountAfterDiscount();
    return orderLineDetailsSummaryGeneralModel;
  }
}
