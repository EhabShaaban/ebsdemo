package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import lombok.Builder;
import org.mockito.Mockito;

public class IObOrderApprovalCycleMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static IObOrderApprovalCycle buildEntity(Long id) {
    IObOrderApprovalCycle orderApprovalCycle = Mockito.mock(IObOrderApprovalCycle.class);
    Mockito.doReturn(id).when(orderApprovalCycle).getId();
    return orderApprovalCycle;
  }
}
