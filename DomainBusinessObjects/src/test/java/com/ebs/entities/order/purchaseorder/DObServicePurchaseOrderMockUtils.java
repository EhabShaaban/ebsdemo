package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObServicePurchaseOrder;
import lombok.Builder;
import org.mockito.Mockito;

public class DObServicePurchaseOrderMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static DObServicePurchaseOrder buildEntity(
      Long id, String userCode, String objectTypeCode) {
    DObServicePurchaseOrder purchaseOrder = Mockito.mock(DObServicePurchaseOrder.class);
    Mockito.doReturn(id).when(purchaseOrder).getId();
    Mockito.doReturn(userCode).when(purchaseOrder).getUserCode();
    Mockito.doReturn(objectTypeCode).when(purchaseOrder).getObjectTypeCode();
    return purchaseOrder;
  }
}
