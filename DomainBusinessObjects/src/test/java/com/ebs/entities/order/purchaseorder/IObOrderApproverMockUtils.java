package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import lombok.Builder;
import org.mockito.Mockito;

public class IObOrderApproverMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static IObOrderApprover buildEntity() {
    IObOrderApprover approver = Mockito.mock(IObOrderApprover.class);
    return approver;
  }
}
