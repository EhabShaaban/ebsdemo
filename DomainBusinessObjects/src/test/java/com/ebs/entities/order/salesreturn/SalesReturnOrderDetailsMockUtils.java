package com.ebs.entities.order.salesreturn;

import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class SalesReturnOrderDetailsMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObSalesReturnOrderDetailsGeneralModel buildGeneralModel(
      String salesReturnCode, String salesInvoiceCode) {
    IObSalesReturnOrderDetailsGeneralModel salesReturnOrderDetailsGeneralModel =
        Mockito.mock(IObSalesReturnOrderDetailsGeneralModel.class);
    Mockito.doReturn(salesReturnCode)
        .when(salesReturnOrderDetailsGeneralModel)
        .getSalesReturnOrderCode();
    Mockito.doReturn(salesInvoiceCode)
        .when(salesReturnOrderDetailsGeneralModel)
        .getSalesInvoiceCode();
    return salesReturnOrderDetailsGeneralModel;
  }
}
