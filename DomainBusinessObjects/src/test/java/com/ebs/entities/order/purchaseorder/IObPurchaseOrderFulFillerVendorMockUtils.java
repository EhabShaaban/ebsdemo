package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObPurchaseOrderFulFillerVendor;
import lombok.Builder;
import org.mockito.Mockito;

public class IObPurchaseOrderFulFillerVendorMockUtils {

  private static Long orderId;
  private static Long referencePurchaseOrderId;
  @Builder(builderMethodName = "mockEntity")
  public static IObPurchaseOrderFulFillerVendor buildEntity() {
    IObPurchaseOrderFulFillerVendor purchaseOrderFulFillerVendor = Mockito.mock(IObPurchaseOrderFulFillerVendor.class);
    Mockito.doReturn(orderId).when(purchaseOrderFulFillerVendor).getRefInstanceId();
    Mockito.doReturn(referencePurchaseOrderId).when(purchaseOrderFulFillerVendor).getReferencePoId();
    return purchaseOrderFulFillerVendor;
  }

  public static void withOrderId(Long orderId) {
    IObPurchaseOrderFulFillerVendorMockUtils.orderId = orderId;
  }

  public static void withReferencePurchaseOrderId(Long referencePurchaseOrderId) {
    IObPurchaseOrderFulFillerVendorMockUtils.referencePurchaseOrderId = referencePurchaseOrderId;
  }
}
