package com.ebs.entities.order.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;
import lombok.Builder;
import org.mockito.Mockito;

public class IObOrderCycleDatesMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static IObOrderCycleDates buildEntity(Long id) {
    IObOrderCycleDates orderCycleDates = Mockito.mock(IObOrderCycleDates.class);
    Mockito.doReturn(id).when(orderCycleDates).getId();
    return orderCycleDates;
  }
}
