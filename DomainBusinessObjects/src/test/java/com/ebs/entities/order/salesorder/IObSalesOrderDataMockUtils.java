package com.ebs.entities.order.salesorder;

import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class IObSalesOrderDataMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static IObSalesOrderDataGeneralModel buildGeneralModel(
          Long salesOrderId,
          String currencyIso) {
    IObSalesOrderDataGeneralModel salesOrderDataGeneralModel =
            Mockito.mock(IObSalesOrderDataGeneralModel.class);

    Mockito.doReturn(salesOrderId).when(salesOrderDataGeneralModel).getId();
    Mockito.doReturn(currencyIso).when(salesOrderDataGeneralModel).getCurrencyIso();

    return salesOrderDataGeneralModel;
  }
}
