package com.ebs.entities;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import lombok.Builder;
import org.joda.time.DateTime;

import java.math.BigDecimal;

public class AccountingDocumentAccountingDetailsUtils {
  @Builder(builderMethodName = "newEntity")
  public static IObAccountingDocumentAccountingDetails buildEntity(
      String creationInfo,
      String modificationInfo,
      DateTime creationDate,
      DateTime modifiedDate,
      Long glAccountId,
      String accountingEntry,
      BigDecimal amount, String objectTypeCode, Long subAccountId) {
    IObAccountingDocumentAccountingDetails accountingDetails =
        new IObAccountingDocumentAccountingDetails();
    accountingDetails.setCreationInfo(creationInfo);
    accountingDetails.setModificationInfo(modificationInfo);
    accountingDetails.setCreationDate(creationDate);
    accountingDetails.setModifiedDate(modifiedDate);
    accountingDetails.setGlAccountId(glAccountId);
    accountingDetails.setAccountingEntry(accountingEntry);
    accountingDetails.setAmount(amount);
    accountingDetails.setObjectTypeCode(objectTypeCode);
    accountingDetails.setGlSubAccountId(subAccountId);
    return accountingDetails;
  }
}
