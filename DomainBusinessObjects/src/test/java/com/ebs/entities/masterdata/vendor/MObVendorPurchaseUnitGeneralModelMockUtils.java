package com.ebs.entities.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class MObVendorPurchaseUnitGeneralModelMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static MObVendorPurchaseUnitGeneralModel buildGeneralModel(Long id) {
    MObVendorPurchaseUnitGeneralModel vendorPurchaseUnitGeneralModel =
            Mockito.mock(MObVendorPurchaseUnitGeneralModel.class);
    Mockito.doReturn(id).when(vendorPurchaseUnitGeneralModel).getId();

    return vendorPurchaseUnitGeneralModel;
  }
}
