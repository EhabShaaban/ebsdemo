package com.ebs.entities.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class VendorMockUtils {
    @Builder(builderMethodName = "mockGeneralModel")
    public static MObVendorGeneralModel buildGeneralModel(Long id) {
        MObVendorGeneralModel vendor = Mockito.mock(MObVendorGeneralModel.class);

        Mockito.doReturn(id).when(vendor).getId();

        return vendor;
    }

    @Builder(builderMethodName = "buildMObVendor")
    public static MObVendor buildMObVendor(String userCode, Long id) {
        MObVendor vendor = Mockito.mock(MObVendor.class);
        Mockito.doReturn(id).when(vendor).getId();
        Mockito.doReturn(userCode).when(vendor).getUserCode();
        return vendor;
    }
}
