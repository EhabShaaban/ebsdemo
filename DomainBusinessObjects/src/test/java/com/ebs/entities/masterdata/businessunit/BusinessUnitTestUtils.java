package com.ebs.entities.masterdata.businessunit;

import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class BusinessUnitTestUtils {

  @Builder(builderMethodName = "newGeneralModel")
  public static CObPurchasingUnitGeneralModel buildGeneralModel(String userCode) {
    CObPurchasingUnitGeneralModel businessUnit = Mockito.mock(CObPurchasingUnitGeneralModel.class);

    Mockito.doReturn(userCode).when(businessUnit).getUserCode();

    return businessUnit;
  }
  @Builder(builderMethodName = "newEntity")
  public static CObPurchasingUnit buildEntity(String userCode) {
    CObPurchasingUnit businessUnit = Mockito.mock(CObPurchasingUnit.class);
    Mockito.doReturn(userCode).when(businessUnit).getUserCode();
    return businessUnit;
  }
}
