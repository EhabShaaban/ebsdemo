package com.ebs.entities.masterdata.customer;

import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class IObCustomerBusinessUnitTestUtils {

  @Builder(builderMethodName = "newGeneralModel")
  public static IObCustomerBusinessUnitGeneralModel buildGeneralModel(String businessUnitCode) {

    IObCustomerBusinessUnitGeneralModel customer =
      Mockito.mock(IObCustomerBusinessUnitGeneralModel.class);

    Mockito.doReturn(businessUnitCode).when(customer).getBusinessUnitcode();

    return customer;
  }

}
