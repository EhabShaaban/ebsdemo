package com.ebs.entities.masterdata.item;

import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class ItemMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static MObItemGeneralModel buildGeneralModel(String itemCode, Long itemid,Long basicUnitOfMeasureId) {
    MObItemGeneralModel itemGeneralModel = Mockito.mock(MObItemGeneralModel.class);
    Mockito.doReturn(itemCode).when(itemGeneralModel).getUserCode();
    Mockito.doReturn(itemid).when(itemGeneralModel).getId();
    Mockito.doReturn(basicUnitOfMeasureId).when(itemGeneralModel).getBasicUnitOfMeasure();
    return itemGeneralModel;
  }
}
