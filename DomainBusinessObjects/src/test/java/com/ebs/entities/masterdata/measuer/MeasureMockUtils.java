package com.ebs.entities.masterdata.measuer;

import com.ebs.dda.jpa.masterdata.measure.CObMeasure;
import lombok.Builder;
import org.mockito.Mockito;

public class MeasureMockUtils {
  @Builder(builderMethodName = "mockEntity")
  public static CObMeasure buildEntity(String measureCode, Long measureid) {
    CObMeasure measure = Mockito.mock(CObMeasure.class);
    Mockito.doReturn(measureCode).when(measure).getUserCode();
    Mockito.doReturn(measureid).when(measure).getId();
    return measure;
  }
}
