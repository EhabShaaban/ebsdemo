package com.ebs.entities.masterdata.chartofaccount;

import com.ebs.dda.jpa.masterdata.chartofaccount.HObGLAccount;
import lombok.Builder;
import org.mockito.Mockito;

public class HObGLAccountUtils {

    @Builder(builderMethodName = "buildHObGLAccount")
    public static HObGLAccount buildHObGLAccount(String userCode) {
        HObGLAccount glAccount = Mockito.mock(HObGLAccount.class);
        Mockito.doReturn(userCode).when(glAccount).getUserCode();
        return glAccount;
    }
}
