package com.ebs.entities.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class IObVendorAccountingInfoGeneralModelMockUtils {

    @Builder(builderMethodName = "buildGeneralModel")
    public static IObVendorAccountingInfoGeneralModel buildGeneralModel(String vendorCode, String accountCode) {
        IObVendorAccountingInfoGeneralModel vendorAccountingInfo = Mockito.mock(IObVendorAccountingInfoGeneralModel.class);
        Mockito.doReturn(vendorCode).when(vendorAccountingInfo).getVendorCode();
        Mockito.doReturn(accountCode).when(vendorAccountingInfo).getAccountCode();
        return vendorAccountingInfo;
    }
}
