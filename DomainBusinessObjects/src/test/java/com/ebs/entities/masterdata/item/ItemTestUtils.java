package com.ebs.entities.masterdata.item;

import org.mockito.Mockito;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import lombok.Builder;

public class ItemTestUtils {

  @Builder(builderMethodName = "newEntity")
  public static MObItem buildEntity(Long id, String userCode) {
    MObItem item = Mockito.mock(MObItem.class);

    Mockito.doReturn(id).when(item).getId();
    Mockito.doReturn(userCode).when(item).getUserCode();

    return item;
  }

}
