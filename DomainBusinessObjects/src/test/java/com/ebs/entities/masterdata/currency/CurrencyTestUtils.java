package com.ebs.entities.masterdata.currency;

import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import lombok.Builder;
import org.mockito.Mockito;

public class CurrencyTestUtils {

  @Builder(builderMethodName = "mockEntity")
  public static CObCurrency buildEntity(String iso) {
    CObCurrency currency = Mockito.mock(CObCurrency.class);
    Mockito.doReturn(iso).when(currency).getIso();
    return currency;
  }
}
