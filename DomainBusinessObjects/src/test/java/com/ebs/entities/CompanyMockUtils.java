package com.ebs.entities;

import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class CompanyMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static CObCompanyGeneralModel buildGeneralModel(
      String currencyIso, String userCode, Long id) {
    CObCompanyGeneralModel companyGeneralModel = Mockito.mock(CObCompanyGeneralModel.class);
    Mockito.doReturn(id).when(companyGeneralModel).getId();
    Mockito.doReturn(userCode).when(companyGeneralModel).getUserCode();
    Mockito.doReturn(currencyIso).when(companyGeneralModel).getCurrencyIso();
    return companyGeneralModel;
  }
}
