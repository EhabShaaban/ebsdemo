package com.ebs.entities;

import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class LandedCostSummaryGeneralModelMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObLandedCostFactorItemsSummary buildGeneralModel(
          String userCode, BigDecimal actualValue, BigDecimal estimateValue) {
    IObLandedCostFactorItemsSummary landedCostFactorItemsSummary =
        Mockito.mock(IObLandedCostFactorItemsSummary.class);
    Mockito.doReturn(userCode).when(landedCostFactorItemsSummary).getCode();
    Mockito.doReturn(actualValue).when(landedCostFactorItemsSummary).getActualTotalAmount();
    Mockito.doReturn(estimateValue).when(landedCostFactorItemsSummary).getEstimateTotalAmount();
    return landedCostFactorItemsSummary;
  }
}
