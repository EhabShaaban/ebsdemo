package com.ebs.entities;

import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;
import java.math.BigDecimal;
import lombok.Builder;
import org.mockito.Mockito;

public class ExchangeRateMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static CObExchangeRateGeneralModel buildGeneralModel(
      BigDecimal secondValue,
      Long id,
      Long firstCurrencyId,
      Long secondCurrencyId,
      String firstCurrencyIso,
      String secondCurrencyIso) {
    CObExchangeRateGeneralModel exchangeRateGeneralModel =
        Mockito.mock(CObExchangeRateGeneralModel.class);
    Mockito.doReturn(id).when(exchangeRateGeneralModel).getId();
    Mockito.doReturn(secondValue).when(exchangeRateGeneralModel).getSecondValue();
    Mockito.doReturn(firstCurrencyId).when(exchangeRateGeneralModel).getFirstCurrencyId();
    Mockito.doReturn(secondCurrencyId).when(exchangeRateGeneralModel).getSecondCurrencyId();
    Mockito.doReturn(firstCurrencyIso).when(exchangeRateGeneralModel).getFirstCurrencyIso();
    Mockito.doReturn(secondCurrencyIso).when(exchangeRateGeneralModel).getSecondCurrencyIso();
    return exchangeRateGeneralModel;
  }
}
