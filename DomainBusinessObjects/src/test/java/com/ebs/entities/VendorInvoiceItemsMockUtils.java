package com.ebs.entities;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class VendorInvoiceItemsMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObVendorInvoiceItemsGeneralModel buildGeneralModel(
      String itemCode,
      String vendorInvoiceCode,
      BigDecimal totalAmount,
      Long itemId,
      BigDecimal quantityInOrderUnit,
      BigDecimal orderUnitPrice,
      String orderUnitCode,
      Long orderUnitId) {
    IObVendorInvoiceItemsGeneralModel vendorInvoiceItemsGeneralModel =
        Mockito.mock(IObVendorInvoiceItemsGeneralModel.class);
    Mockito.doReturn(itemCode).when(vendorInvoiceItemsGeneralModel).getItemCode();
    Mockito.doReturn(vendorInvoiceCode).when(vendorInvoiceItemsGeneralModel).getInvoiceCode();
    Mockito.doReturn(totalAmount).when(vendorInvoiceItemsGeneralModel).getTotalAmount();
    Mockito.doReturn(itemId).when(vendorInvoiceItemsGeneralModel).getItemId();
    Mockito.doReturn(quantityInOrderUnit).when(vendorInvoiceItemsGeneralModel).getQuantityInOrderUnit();
    Mockito.doReturn(orderUnitPrice).when(vendorInvoiceItemsGeneralModel).getOrderUnitPrice();
    Mockito.doReturn(orderUnitCode).when(vendorInvoiceItemsGeneralModel).getOrderUnitCode();
    Mockito.doReturn(orderUnitId).when(vendorInvoiceItemsGeneralModel).getOrderUnitId();
    return vendorInvoiceItemsGeneralModel;
  }
}
