package com.ebs.entities;

import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import java.math.BigDecimal;
import lombok.Builder;
import org.mockito.Mockito;

public class VendorInvoiceSummaryUtils {

  @Builder(builderMethodName = "newEntity")
  public static IObVendorInvoiceSummary buildEntity(
      BigDecimal total, BigDecimal remaining, Long refInstanceId) {
    IObVendorInvoiceSummary vendorInvoiceSummary = Mockito.mock(IObVendorInvoiceSummary.class);
    Mockito.doReturn(total).when(vendorInvoiceSummary).getTotalAmount();
    Mockito.doReturn(remaining).when(vendorInvoiceSummary).getRemaining();
    Mockito.doReturn(refInstanceId).when(vendorInvoiceSummary).getRefInstanceId();
    return vendorInvoiceSummary;
  }

  @Builder(builderMethodName = "mockGeneralModel")
  public static IObInvoiceSummaryGeneralModel buildGeneralModel(
      String totalAmountBeforeTaxes, String invoiceCode, String invoiceType) {
    IObInvoiceSummaryGeneralModel vendorInvoiceSummary =
        Mockito.mock(IObInvoiceSummaryGeneralModel.class);
    Mockito.doReturn(totalAmountBeforeTaxes).when(vendorInvoiceSummary).getTotalAmountBeforeTaxes();
    Mockito.doReturn(invoiceCode).when(vendorInvoiceSummary).getInvoiceCode();
    Mockito.doReturn(invoiceType).when(vendorInvoiceSummary).getInvoiceType();
    return vendorInvoiceSummary;
  }
}
