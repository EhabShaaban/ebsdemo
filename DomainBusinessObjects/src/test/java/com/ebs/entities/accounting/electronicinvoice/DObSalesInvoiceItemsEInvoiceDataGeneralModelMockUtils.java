package com.ebs.entities.accounting.electronicinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObSalesInvoiceItemsEInvoiceDataGeneralModelMockUtils {

    @Builder(builderMethodName = "buildGeneralModel")
    public static DObSalesInvoiceItemsEInvoiceDataGeneralModel mockGeneralModel(String itemUserCode,
        String itemName, String itemType, String itemBrickCode, String itemBaseUnit,
        BigDecimal quantity, String currencySoldIso, BigDecimal amountSold,
        BigDecimal currencyExchangeRate, BigDecimal salesTotal, BigDecimal valueDifference,
        BigDecimal totalTaxableFees, BigDecimal itemsDiscount
    ) {
        DObSalesInvoiceItemsEInvoiceDataGeneralModel item =
            Mockito.mock(DObSalesInvoiceItemsEInvoiceDataGeneralModel.class);

        Mockito.doReturn(itemUserCode).when(item).getItemUserCode();
        Mockito.doReturn(itemName).when(item).getItemName();
        Mockito.doReturn(itemType).when(item).getItemType();
        Mockito.doReturn(itemBrickCode).when(item).getItemBrickCode();
        Mockito.doReturn(itemBaseUnit).when(item).getItemBaseUnit();
        Mockito.doReturn(quantity).when(item).getQuantity();
        Mockito.doReturn(currencySoldIso).when(item).getCurrencySoldIso();
        Mockito.doReturn(amountSold).when(item).getAmountSold();
        Mockito.doReturn(currencyExchangeRate).when(item).getCurrencyExchangeRate();
        Mockito.doReturn(salesTotal).when(item).getSalesTotal();
        Mockito.doReturn(valueDifference).when(item).getValueDifference();
        Mockito.doReturn(totalTaxableFees).when(item).getTotalTaxableFees();
        Mockito.doReturn(itemsDiscount).when(item).getItemsDiscount();

        return item;
    }
}
