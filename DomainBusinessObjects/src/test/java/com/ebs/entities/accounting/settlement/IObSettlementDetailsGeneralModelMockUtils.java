package com.ebs.entities.accounting.settlement;

import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;
import lombok.Builder;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class IObSettlementDetailsGeneralModelMockUtils {

  @Builder(builderMethodName = "buildGeneralModel")
  public static IObSettlementDetailsGeneralModel mockGeneralModel(
    String currencyCode,
    String currencyIso,
    String settlementCode
  ) {
    IObSettlementDetailsGeneralModel generalModel =
      mock(IObSettlementDetailsGeneralModel.class);

    doReturn(currencyCode).when(generalModel).getCurrencyCode();
    doReturn(currencyIso).when(generalModel).getCurrencyISO();
    doReturn(settlementCode).when(generalModel).getSettlementCode();

    return generalModel;
  }

}
