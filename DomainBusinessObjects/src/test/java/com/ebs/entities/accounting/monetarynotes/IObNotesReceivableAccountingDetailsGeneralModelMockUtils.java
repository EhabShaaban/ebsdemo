package com.ebs.entities.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObNotesReceivableAccountingDetailsGeneralModelMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static IObNotesReceivableAccountingDetailsGeneralModel buildGeneralModel(String subLedger,
    Long accountId, Long subAccountId, BigDecimal amount, String accountingEntry) {

    IObNotesReceivableAccountingDetailsGeneralModel accDetails =
      Mockito.mock(IObNotesReceivableAccountingDetailsGeneralModel.class);

    Mockito.doReturn(subLedger).when(accDetails).getSubLedger();
    Mockito.doReturn(accountId).when(accDetails).getGlAccountId();
    Mockito.doReturn(subAccountId).when(accDetails).getGlSubAccountId();
    Mockito.doReturn(amount).when(accDetails).getAmount();
    Mockito.doReturn(accountingEntry).when(accDetails).getAccountingEntry();

    return accDetails;
  }
}
