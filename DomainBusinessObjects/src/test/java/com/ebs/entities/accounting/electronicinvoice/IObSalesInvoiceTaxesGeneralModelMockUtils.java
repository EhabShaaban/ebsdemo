package com.ebs.entities.accounting.electronicinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObSalesInvoiceTaxesGeneralModelMockUtils {

    @Builder(builderMethodName = "buildGeneralModel")
    public static IObSalesInvoiceTaxesGeneralModel mockGeneralModel(String taxCode,
        BigDecimal taxAmount) {
        IObSalesInvoiceTaxesGeneralModel tax = Mockito.mock(IObSalesInvoiceTaxesGeneralModel.class);

        Mockito.doReturn(taxCode).when(tax).getTaxCode();
        Mockito.doReturn(taxAmount).when(tax).getTaxAmount();
        return tax;
    }
}
