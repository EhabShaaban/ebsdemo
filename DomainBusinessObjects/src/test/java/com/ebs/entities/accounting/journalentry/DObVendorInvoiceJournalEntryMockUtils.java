package com.ebs.entities.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObVendorInvoiceJournalEntry;
import lombok.Builder;
import org.mockito.Mockito;

public class DObVendorInvoiceJournalEntryMockUtils {

  private static String userCode;
  private static DObVendorInvoiceJournalEntry vendorInvoiceJournalEntry =
      Mockito.mock(DObVendorInvoiceJournalEntry.class);

  @Builder(builderMethodName = "mockEntity")
  public static DObVendorInvoiceJournalEntry buildEntity() {
    Mockito.doReturn(userCode).when(vendorInvoiceJournalEntry).getUserCode();
    return vendorInvoiceJournalEntry;
  }

  public static void withUserCode(String userCode) {
    DObVendorInvoiceJournalEntryMockUtils.userCode = userCode;
  }

  public static DObVendorInvoiceJournalEntry getVendorInvoiceJournalEntry() {
    return vendorInvoiceJournalEntry;
  }

  public static void resetVendorInvoiceJournalEntry() {
    vendorInvoiceJournalEntry = Mockito.mock(DObVendorInvoiceJournalEntry.class);
  }
}
