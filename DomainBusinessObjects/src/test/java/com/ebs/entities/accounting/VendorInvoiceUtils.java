package com.ebs.entities.accounting;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceSummary;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class VendorInvoiceUtils {
    private static String vendorInvoiceCode;
    private static Long vendorInvoiceId;
    private static BigDecimal remaining;
    private static IObVendorInvoiceSummary vendorInvoiceSummary =
            Mockito.mock(IObVendorInvoiceSummary.class);
    private static DObVendorInvoice vendorInvoice =
            Mockito.mock(DObVendorInvoice.class);
    private static String vendorInvoiceType;

    @Builder(builderMethodName = "buildVendorInvoice")
    public static DObVendorInvoice buildVendorInvoice() {
        Mockito.doReturn(vendorInvoiceCode).when(vendorInvoice).getUserCode();
        Mockito.doReturn(vendorInvoiceId).when(vendorInvoice).getId();
        return vendorInvoice;
    }

    @Builder(builderMethodName = "buildVendorInvoiceSummary")
    public static IObVendorInvoiceSummary buildVendorInvoiceSummary() {
        Mockito.doReturn(vendorInvoiceId).when(vendorInvoiceSummary).getRefInstanceId();
        Mockito.doReturn(remaining).when(vendorInvoiceSummary).getRemaining();
        return vendorInvoiceSummary;
    }

    public static void withVendorInvoiceCode(String vendorInvoiceCode) {
        VendorInvoiceUtils.vendorInvoiceCode = vendorInvoiceCode;
    }

    public static void withVendorInvoiceType(String vendorInvoiceType) {
        VendorInvoiceUtils.vendorInvoiceType = vendorInvoiceType;
    }

    public static void withVendorInvoiceId(Long vendorInvoiceId) {
        VendorInvoiceUtils.vendorInvoiceId = vendorInvoiceId;
    }

    public static void withVendorInvoiceRemaining(BigDecimal remaining) {
        VendorInvoiceUtils.remaining = remaining;
    }

    public static DObVendorInvoice getVendorInvoice() {
        return vendorInvoice;
    }

    public static String getVendorInvoiceType() {
        return vendorInvoiceType;
    }
    public static void resetValue(){
        vendorInvoice =
                Mockito.mock(DObVendorInvoice.class);
    }
}
