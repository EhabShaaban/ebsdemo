package com.ebs.entities.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils {
    private static String vendorInvoiceCode;
    private static Long glAccountId;
    private static Long orderId;
    private static BigDecimal amount;

    @Builder(builderMethodName = "mockEntity")
    public static DObVendorInvoiceItemAccountsPreparationGeneralModel buildEntity() {
        DObVendorInvoiceItemAccountsPreparationGeneralModel itemAccountsPreparationGeneralModel = Mockito.mock(DObVendorInvoiceItemAccountsPreparationGeneralModel.class);
        Mockito.doReturn(vendorInvoiceCode).when(itemAccountsPreparationGeneralModel).getCode();
        Mockito.doReturn(glAccountId).when(itemAccountsPreparationGeneralModel).getGlAccountId();
        Mockito.doReturn(orderId).when(itemAccountsPreparationGeneralModel).getOrderId();
        Mockito.doReturn(amount).when(itemAccountsPreparationGeneralModel).getAmount();
        return itemAccountsPreparationGeneralModel;
    }

    public static void withVendorInvoiceCode(String vendorInvoiceCode) {
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.vendorInvoiceCode = vendorInvoiceCode;
    }

    public static void withGlAccountId(Long glAccountId) {
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.glAccountId = glAccountId;
    }

    public static void withOrderId(Long orderId) {
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.orderId = orderId;
    }

    public static void withAmount(BigDecimal amount) {
        DObVendorInvoiceItemAccountsPreparationGeneralModelMockUtils.amount = amount;
    }
}
