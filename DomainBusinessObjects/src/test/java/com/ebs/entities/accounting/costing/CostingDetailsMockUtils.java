package com.ebs.entities.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingDetails;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CostingDetailsMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObCostingDetailsGeneralModel buildGeneralModel(
      String userCode,
      String goodsReceiptUserCode,
      Long refInstanceId,
      Long id,
      BigDecimal currencyPriceActualTime,
      String purchaseOrderCode) {
    IObCostingDetailsGeneralModel costingDetails =
        Mockito.mock(IObCostingDetailsGeneralModel.class);
    Mockito.doReturn(refInstanceId).when(costingDetails).getRefInstanceId();
    Mockito.doReturn(userCode).when(costingDetails).getUserCode();
    Mockito.doReturn(goodsReceiptUserCode).when(costingDetails).getGoodsReceiptCode();
    Mockito.doReturn(id).when(costingDetails).getId();
    Mockito.doReturn(currencyPriceActualTime).when(costingDetails).getCurrencyPriceActualTime();
    Mockito.doReturn(purchaseOrderCode).when(costingDetails).getPurchaseOrderCode();
    return costingDetails;
  }

  @Builder(builderMethodName = "mockEntity")
  public static IObCostingDetails buildEntity(
      String creationInfo,
      String modificationInfo,
      DateTime creationDate,
      DateTime modifiedDate,
      Long goodsReceiptId,
      BigDecimal currencyPriceEstimateTime,
      BigDecimal currencyPriceActualTime,
      Long id) {
    IObCostingDetails costingDetails = Mockito.mock(IObCostingDetails.class);
    Mockito.doReturn(creationInfo).when(costingDetails).getCreationInfo();
    Mockito.doReturn(modificationInfo).when(costingDetails).getModificationInfo();
    Mockito.doReturn(creationDate).when(costingDetails).getCreationDate();
    Mockito.doReturn(modifiedDate).when(costingDetails).getModifiedDate();
    Mockito.doReturn(goodsReceiptId).when(costingDetails).getGoodsReceiptId();
    Mockito.doReturn(currencyPriceEstimateTime).when(costingDetails).getCurrencyPriceEstimateTime();
    Mockito.doReturn(currencyPriceActualTime).when(costingDetails).getCurrencyPriceActualTime();
    Mockito.doReturn(id).when(costingDetails).getId();
    return costingDetails;
  }
}
