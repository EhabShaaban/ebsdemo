package com.ebs.entities.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.HashSet;

public class DObSalesInvoiceMockUtils {

  private static Long salesInvoiceId;
  private static String salesInvoiceCode;
  private static DObSalesInvoice salesInvoice =
          Mockito.mock(DObSalesInvoice.class);

  @Builder(builderMethodName = "buildEntity")
  public static DObSalesInvoice buildEntity() {
    Mockito.doReturn(salesInvoiceCode).when(salesInvoice).getUserCode();
    Mockito.doReturn(salesInvoiceId).when(salesInvoice).getId();
    return salesInvoice;
  }

  @Builder(builderMethodName = "newEntity")
  public static DObSalesInvoice buildEntity(Long id, String userCode) {
    DObSalesInvoice salesInvoice = new DObSalesInvoice();
    salesInvoice.setId(id);
    salesInvoice.setUserCode(userCode);
    return salesInvoice;
  }

  @Builder(builderMethodName = "mockGeneralModel")
  public static DObSalesInvoiceGeneralModel buildGeneralModel(
          String salesInvoiceCode,
          HashSet<String> currentStates,
          String businessPartnerCode,
          String businessUnitCode,
          BigDecimal remaining,
          String companyCode) {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
            Mockito.mock(DObSalesInvoiceGeneralModel.class);

    Mockito.doReturn(salesInvoiceCode).when(salesInvoiceGeneralModel).getUserCode();
    Mockito.doReturn(currentStates).when(salesInvoiceGeneralModel).getCurrentStates();
    Mockito.doReturn(businessPartnerCode).when(salesInvoiceGeneralModel).getCustomerCode();
    Mockito.doReturn(companyCode).when(salesInvoiceGeneralModel).getCompanyCode();
    Mockito.doReturn(businessUnitCode).when(salesInvoiceGeneralModel).getPurchaseUnitCode();
    Mockito.doReturn(remaining).when(salesInvoiceGeneralModel).getRemaining();

    return salesInvoiceGeneralModel;
  }

  @Builder(builderMethodName = "newIObSalesInvoiceSummary")
  public static IObSalesInvoiceSummary buildIObSalesInvoiceSummary(Long refInstanceId, BigDecimal remaining) {
    IObSalesInvoiceSummary salesInvoiceSummary = new IObSalesInvoiceSummary();
    salesInvoiceSummary.setRefInstanceId(refInstanceId);
    salesInvoiceSummary.setRemaining(remaining);
    return salesInvoiceSummary;
  }

  public static void withSalesInvoiceCode(String salesInvoiceCode) {
    DObSalesInvoiceMockUtils.salesInvoiceCode = salesInvoiceCode;
  }

  public static void withSalesInvoiceId(Long salesInvoiceId) {
    DObSalesInvoiceMockUtils.salesInvoiceId = salesInvoiceId;
  }

  public static DObSalesInvoice getSalesInvoice() {
    return salesInvoice;
  }

  public static void resetValue(){
    salesInvoice =
            Mockito.mock(DObSalesInvoice.class);
  }
}
