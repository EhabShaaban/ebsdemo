package com.ebs.entities.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObPaymentRequestJournalEntry;
import lombok.Builder;
import org.mockito.Mockito;

public class DObPaymentRequestJournalEntryUtils {
    private static String userCode;
    private static DObPaymentRequestJournalEntry paymentRequestJournalEntry = Mockito.mock(DObPaymentRequestJournalEntry.class);

    @Builder(builderMethodName = "buildPaymentRequestJournalEntry")
    public static DObPaymentRequestJournalEntry
    buildPaymentRequestJournalEntry() {
        Mockito.doReturn(userCode).when(paymentRequestJournalEntry).getUserCode();
        return paymentRequestJournalEntry;
    }

    public static void withUserCode(String userCode) {
        DObPaymentRequestJournalEntryUtils.userCode = userCode;
    }

    public static DObPaymentRequestJournalEntry getPaymentRequestJournalEntry() {
        return paymentRequestJournalEntry;
    }

    public static void resetPaymentRequestJournalEntry() {
        paymentRequestJournalEntry = Mockito.mock(DObPaymentRequestJournalEntry.class);
    }
}
