package com.ebs.entities.accounting;

import com.ebs.dda.jpa.accounting.CObFiscalYear;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

public class FiscalYearMockUtils {
  @Builder(builderMethodName = "mockEntity")
  public static CObFiscalYear buildEntity(
      String currentState, Long id, DateTime fromDate, DateTime toDate) {
    CObFiscalYear fiscalYear = Mockito.mock(CObFiscalYear.class);
    Mockito.doReturn(id).when(fiscalYear).getId();
    Mockito.doReturn(currentState).when(fiscalYear).getCurrentState();
    Mockito.doReturn(fromDate).when(fiscalYear).getFromDate();
    Mockito.doReturn(toDate).when(fiscalYear).getToDate();
    return fiscalYear;
  }
}
