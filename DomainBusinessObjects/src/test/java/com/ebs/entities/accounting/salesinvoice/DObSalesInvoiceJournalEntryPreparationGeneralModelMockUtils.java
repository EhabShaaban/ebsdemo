package com.ebs.entities.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils {
    private static String salesInvoiceCode;
    private static Long customerAccountId;
    private static Long customerId;
    private static Long salesAccountId;
    private static BigDecimal amountBeforeTaxes;
    private static BigDecimal amountAfterTaxes;

    @Builder(builderMethodName = "mockEntity")
    public static DObSalesInvoiceJournalEntryPreparationGeneralModel buildEntity() {
        DObSalesInvoiceJournalEntryPreparationGeneralModel creditPreparationGeneralModel = Mockito.mock(DObSalesInvoiceJournalEntryPreparationGeneralModel.class);
        Mockito.doReturn(salesInvoiceCode).when(creditPreparationGeneralModel).getCode();
        Mockito.doReturn(customerAccountId).when(creditPreparationGeneralModel).getCustomerAccountId();
        Mockito.doReturn(customerId).when(creditPreparationGeneralModel).getCustomerId();
        Mockito.doReturn(salesAccountId).when(creditPreparationGeneralModel).getSalesAccountId();
        Mockito.doReturn(amountBeforeTaxes).when(creditPreparationGeneralModel).getAmountBeforeTaxes();
        Mockito.doReturn(amountAfterTaxes).when(creditPreparationGeneralModel).getAmountAfterTaxes();
        return creditPreparationGeneralModel;
    }

    public static void withSalesInvoiceCode(String salesInvoiceCode) {
        DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.salesInvoiceCode = salesInvoiceCode;
    }

    public static void withCustomerAccountId(Long customerAccountId) {
        DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.customerAccountId = customerAccountId;
    }

    public static void withCustomerId(Long customerId) {
        DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.customerId = customerId;
    }

    public static void withSalesAccountId(Long salesAccountId) {
        DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.salesAccountId = salesAccountId;
    }

    public static void withAmountBeforeTaxes(BigDecimal amountBeforeTaxes) {
        DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.amountBeforeTaxes = amountBeforeTaxes;
    }

    public static void withAmountAfterTaxes(BigDecimal amountAfterTaxes) {
        DObSalesInvoiceJournalEntryPreparationGeneralModelMockUtils.amountAfterTaxes = amountAfterTaxes;
    }
}
