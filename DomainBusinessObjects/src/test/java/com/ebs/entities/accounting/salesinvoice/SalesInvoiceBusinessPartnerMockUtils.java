package com.ebs.entities.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class SalesInvoiceBusinessPartnerMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObSalesInvoiceBusinessPartnerGeneralModel buildGeneralModel(
      Long salesInvoiceId, String salesInvoiceCode, String salesOrderCode, String currencyIso) {
    IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceBusinessPartnerGeneralModel =
        Mockito.mock(IObSalesInvoiceBusinessPartnerGeneralModel.class);
    Mockito.doReturn(salesInvoiceCode).when(salesInvoiceBusinessPartnerGeneralModel).getCode();
    Mockito.doReturn(salesOrderCode).when(salesInvoiceBusinessPartnerGeneralModel).getSalesOrder();
    Mockito.doReturn(salesInvoiceId).when(salesInvoiceBusinessPartnerGeneralModel).getRefInstanceId();
    Mockito.doReturn(currencyIso).when(salesInvoiceBusinessPartnerGeneralModel).getCurrencyIso();
    return salesInvoiceBusinessPartnerGeneralModel;
  }
}
