package com.ebs.entities.accounting.landedcost;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import org.mockito.Mockito;
import lombok.Builder;

public class LandedCostTestUtils {

  @Builder(builderMethodName = "newEntity")
  public static DObLandedCost buildEntity(Long id, String userCode) {
    DObLandedCost landedCost = Mockito.mock(DObLandedCost.class);

    Mockito.doReturn(id).when(landedCost).getId();
    Mockito.doReturn(userCode).when(landedCost).getUserCode();
    return landedCost;
  }

}
