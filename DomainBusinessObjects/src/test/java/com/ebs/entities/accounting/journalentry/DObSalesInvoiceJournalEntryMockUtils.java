package com.ebs.entities.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObSalesInvoiceJournalEntry;
import lombok.Builder;
import org.mockito.Mockito;

public class DObSalesInvoiceJournalEntryMockUtils {

  private static String userCode;
  private static DObSalesInvoiceJournalEntry salesInvoiceJournalEntry =
      Mockito.mock(DObSalesInvoiceJournalEntry.class);

  @Builder(builderMethodName = "mockEntity")
  public static DObSalesInvoiceJournalEntry buildEntity() {
    Mockito.doReturn(userCode).when(salesInvoiceJournalEntry).getUserCode();
    return salesInvoiceJournalEntry;
  }

  public static void withUserCode(String userCode) {
    DObSalesInvoiceJournalEntryMockUtils.userCode = userCode;
  }

  public static void resetSalesInvoiceJournalEntry() {
    salesInvoiceJournalEntry = Mockito.mock(DObSalesInvoiceJournalEntry.class);
  }
}
