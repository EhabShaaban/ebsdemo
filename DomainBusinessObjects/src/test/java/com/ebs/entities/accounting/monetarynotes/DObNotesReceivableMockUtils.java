package com.ebs.entities.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.util.HashSet;

public class DObNotesReceivableMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static DObMonetaryNotes buildMockEntity(Long id, String code) {
    DObMonetaryNotes monetaryNotes = Mockito.mock(DObMonetaryNotes.class);

    Mockito.doReturn(id).when(monetaryNotes).getId();
    Mockito.doReturn(code).when(monetaryNotes).getUserCode();
    return monetaryNotes;
  }

  @Builder(builderMethodName = "mockGeneralModel")
  public static DObNotesReceivablesGeneralModel buildGeneralModel(
      String notesReceivableCode,
      Long id,
      HashSet<String> currentStates,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode,
      String currencyIso) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        Mockito.mock(DObNotesReceivablesGeneralModel.class);

    Mockito.doReturn(notesReceivableCode).when(notesReceivablesGeneralModel).getUserCode();

    Mockito.doReturn(id).when(notesReceivablesGeneralModel).getId();
    Mockito.doReturn(currentStates).when(notesReceivablesGeneralModel).getCurrentStates();
    Mockito.doReturn(businessPartnerCode)
        .when(notesReceivablesGeneralModel)
        .getBusinessPartnerCode();
    Mockito.doReturn(businessUnitCode).when(notesReceivablesGeneralModel).getPurchaseUnitCode();
    Mockito.doReturn(companyCode).when(notesReceivablesGeneralModel).getCompanyCode();
    Mockito.doReturn(currencyIso).when(notesReceivablesGeneralModel).getCurrencyIso();
    return notesReceivablesGeneralModel;
  }
}
