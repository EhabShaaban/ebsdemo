package com.ebs.entities.accounting.collection;

import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObCollectionMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static DObCollectionGeneralModel buildGeneralModel(
      String collectionCode, String refDocumentCode, BigDecimal amount) {
    DObCollectionGeneralModel collectionGeneralModel =
        Mockito.mock(DObCollectionGeneralModel.class);

    Mockito.doReturn(collectionCode).when(collectionGeneralModel).getUserCode();
    Mockito.doReturn(refDocumentCode).when(collectionGeneralModel).getRefDocumentCode();
    Mockito.doReturn(amount).when(collectionGeneralModel).getAmount();
    return collectionGeneralModel;
  }
}
