package com.ebs.entities.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils {

    private static String salesInvoiceCode;
    private static Long taxesAccountId;
    private static Long taxId;
    private static BigDecimal taxAmount;

    @Builder(builderMethodName = "mockEntity")
    public static IObSalesInvoiceJournalEntryItemsPreparationGeneralModel buildEntity() {
        IObSalesInvoiceJournalEntryItemsPreparationGeneralModel taxes = Mockito.mock(IObSalesInvoiceJournalEntryItemsPreparationGeneralModel.class);
        Mockito.doReturn(salesInvoiceCode).when(taxes).getCode();
        Mockito.doReturn(taxesAccountId).when(taxes).getTaxesAccountId();
        Mockito.doReturn(taxId).when(taxes).getTaxId();
        Mockito.doReturn(taxAmount).when(taxes).getTaxAmount();
        return taxes;
    }

    public static void withSalesInvoiceCode(String salesInvoiceCode) {
        IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.salesInvoiceCode = salesInvoiceCode;
    }

    public static void withTaxesAccountId(Long taxesAccountId) {
        IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.taxesAccountId = taxesAccountId;
    }

    public static void withTaxId(Long taxId) {
        IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.taxId = taxId;
    }

    public static void withTaxAmount(BigDecimal taxAmount) {
        IObSalesInvoiceJournalEntryItemsPreparationGeneralModelMockUtils.taxAmount = taxAmount;
    }
}
