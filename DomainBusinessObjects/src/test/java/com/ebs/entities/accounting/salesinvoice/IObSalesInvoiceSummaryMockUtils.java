package com.ebs.entities.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObSalesInvoiceSummaryMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static IObInvoiceSummaryGeneralModel buildGeneralModel(
          String salesInvoiceCode, String remaining) {
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
            Mockito.mock(IObInvoiceSummaryGeneralModel.class);

    Mockito.doReturn(salesInvoiceCode).when(invoiceSummaryGeneralModel).getInvoiceCode();
    Mockito.doReturn(remaining).when(invoiceSummaryGeneralModel).getTotalRemaining();
    Mockito.doReturn("SalesInvoice").when(invoiceSummaryGeneralModel).getInvoiceType();

    return invoiceSummaryGeneralModel;
  }

  @Builder(builderMethodName = "newEntity")
  public static IObSalesInvoiceSummary buildEntity(Long refInstanceId, BigDecimal remaining) {
    IObSalesInvoiceSummary salesInvoiceSummary = new IObSalesInvoiceSummary();
    salesInvoiceSummary.setRefInstanceId(refInstanceId);
    salesInvoiceSummary.setRemaining(remaining);
    return salesInvoiceSummary;
  }
}
