package com.ebs.entities.accounting.landedcost;

import org.mockito.Mockito;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import lombok.Builder;

public class LandedCostGeneralModelTestUtils {

  @Builder(builderMethodName = "newGeneralModel")
  public static DObLandedCostGeneralModel buildGeneralModel(Long id, String userCode,
      String purhcaseOrderCode) {
    DObLandedCostGeneralModel landedCost = Mockito.mock(DObLandedCostGeneralModel.class);

    Mockito.doReturn(id).when(landedCost).getId();
    Mockito.doReturn(userCode).when(landedCost).getUserCode();
    Mockito.doReturn(purhcaseOrderCode).when(landedCost).getPurchaseOrderCode();

    return landedCost;
  }

}
