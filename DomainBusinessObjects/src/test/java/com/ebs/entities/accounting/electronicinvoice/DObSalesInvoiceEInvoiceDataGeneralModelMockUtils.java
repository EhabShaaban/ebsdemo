package com.ebs.entities.accounting.electronicinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceEInvoiceDataGeneralModel;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObSalesInvoiceEInvoiceDataGeneralModelMockUtils {

    @Builder(builderMethodName = "newGeneralModel")
    public static DObSalesInvoiceEInvoiceDataGeneralModel mockGeneralModel(
        Long id, String companyTaxRegistrationNumber, String companyName, String companyCountry,
        String companyGovernate, String companyRegion, String companyStreet,
        String companyBuildingNumber, String companyPostalCode,
        String customerTaxRegistrationNumber, String customerName, String customerType,
        String customerCountry, String customerGovernate, String customerRegion,
        String customerStreet, String customerBuildingNumber, String customerPostalCode,
        DateTime journalDate, String taxpayerActivityCode, String invoiceSerialNumber,
        BigDecimal extraDiscountAmount) {

        DObSalesInvoiceEInvoiceDataGeneralModel eInvoiceData =
            Mockito.mock(DObSalesInvoiceEInvoiceDataGeneralModel.class);

        Mockito.doReturn(id).when(eInvoiceData).getId();
        Mockito.doReturn(companyTaxRegistrationNumber).when(eInvoiceData).getCompanyTaxRegistrationNumber();
        Mockito.doReturn(companyName).when(eInvoiceData).getCompanyName();
        Mockito.doReturn(companyCountry).when(eInvoiceData).getCompanyCountry();
        Mockito.doReturn(companyGovernate).when(eInvoiceData).getCompanyGovernate();
        Mockito.doReturn(companyRegion).when(eInvoiceData).getCompanyRegion();
        Mockito.doReturn(companyStreet).when(eInvoiceData).getCompanyStreet();
        Mockito.doReturn(companyBuildingNumber).when(eInvoiceData).getCompanyBuildingNumber();
        Mockito.doReturn(companyPostalCode).when(eInvoiceData).getCompanyPostalCode();
        Mockito.doReturn(customerTaxRegistrationNumber).when(eInvoiceData).getCustomerTaxRegistrationNumber();
        Mockito.doReturn(customerName).when(eInvoiceData).getCustomerName();
        Mockito.doReturn(customerType).when(eInvoiceData).getCustomerType();
        Mockito.doReturn(customerCountry).when(eInvoiceData).getCustomerCountry();
        Mockito.doReturn(customerGovernate).when(eInvoiceData).getCustomerGovernate();
        Mockito.doReturn(customerRegion).when(eInvoiceData).getCustomerRegion();
        Mockito.doReturn(customerStreet).when(eInvoiceData).getCustomerStreet();
        Mockito.doReturn(customerBuildingNumber).when(eInvoiceData).getCustomerBuildingNumber();
        Mockito.doReturn(customerPostalCode).when(eInvoiceData).getCustomerPostalCode();
        Mockito.doReturn(journalDate).when(eInvoiceData).getJournalDate();
        Mockito.doReturn(taxpayerActivityCode).when(eInvoiceData).getTaxpayerActivityCode();
        Mockito.doReturn(invoiceSerialNumber).when(eInvoiceData).getInvoiceSerialNumber();
        Mockito.doReturn(extraDiscountAmount).when(eInvoiceData).getExtraDiscountAmount();

        return eInvoiceData;
    }
}
