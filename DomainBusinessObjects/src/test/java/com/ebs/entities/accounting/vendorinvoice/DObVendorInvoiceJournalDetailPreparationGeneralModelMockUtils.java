package com.ebs.entities.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils {
    private static String vendorInvoiceCode;
    private static Long glAccountId;
    private static Long glSubAccountId;
    private static BigDecimal amount;

    @Builder(builderMethodName = "mockEntity")
    public static DObVendorInvoiceJournalDetailPreparationGeneralModel buildEntity() {
        DObVendorInvoiceJournalDetailPreparationGeneralModel creditPreparationGeneralModel = Mockito.mock(DObVendorInvoiceJournalDetailPreparationGeneralModel.class);
        Mockito.doReturn(vendorInvoiceCode).when(creditPreparationGeneralModel).getCode();
        Mockito.doReturn(glAccountId).when(creditPreparationGeneralModel).getGlAccountId();
        Mockito.doReturn(glSubAccountId).when(creditPreparationGeneralModel).getGlSubAccountId();
        Mockito.doReturn(amount).when(creditPreparationGeneralModel).getAmount();
        return creditPreparationGeneralModel;
    }

    public static void withVendorInvoiceCode(String vendorInvoiceCode) {
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.vendorInvoiceCode = vendorInvoiceCode;
    }

    public static void withGlAccountId(Long glAccountId) {
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.glAccountId = glAccountId;
    }

    public static void withGlSubAccountId(Long glSubAccountId) {
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.glSubAccountId = glSubAccountId;
    }

    public static void withAmount(BigDecimal amount) {
        DObVendorInvoiceJournalDetailPreparationGeneralModelMockUtils.amount = amount;
    }
}
