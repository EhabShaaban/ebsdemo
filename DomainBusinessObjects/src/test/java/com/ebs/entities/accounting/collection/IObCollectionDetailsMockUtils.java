package com.ebs.entities.accounting.collection;

import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObCollectionDetailsMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObCollectionDetailsGeneralModel buildGeneralModel(
      String collectionCode,
      BigDecimal amount,
      String collectionMethod,
      String treasuryCode,
      String backAccountCode) {
    IObCollectionDetailsGeneralModel collectionDetails =
        Mockito.mock(IObCollectionDetailsGeneralModel.class);

    Mockito.doReturn(collectionCode).when(collectionDetails).getCollectionCode();
    Mockito.doReturn(amount).when(collectionDetails).getAmount();
    Mockito.doReturn(collectionMethod).when(collectionDetails).getCollectionMethod();
    Mockito.doReturn(treasuryCode).when(collectionDetails).getTreasuryCode();
    Mockito.doReturn(backAccountCode).when(collectionDetails).getBankAccountCode();

    return collectionDetails;
  }
}
