package com.ebs.entities.accounting.payment;

import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class PaymentUtils {
    private static String paymentCode;
    private static BigDecimal amount;
    private static String dueDocumentCode;
    private static Long dueDocumentCompanyId;
    private static Long dueDocumentCurrencyId;
    private static Long dueDocumentId;
    private static String dueDocumentType;
    private static String businessPartnerCode;
    private static String businessUnitCode;
    private static String companyCode;
    private static Long documentOwnerId;
    private static Long businessPartnerId;
    private static DObPaymentRequest paymentRequest = Mockito.mock(DObPaymentRequest.class);
    private static IObPaymentRequestPaymentDetailsGeneralModel paymentDetails = Mockito.mock(IObPaymentRequestPaymentDetailsGeneralModel.class);
    private static DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel calculateRealizedExchangeRateDifferenceGeneralModel =
            Mockito.mock(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel.class);
    private static DObPaymentRequestCreateValueObject paymentRequestCreateValueObject =
            Mockito.mock(DObPaymentRequestCreateValueObject.class);
    private static DObPaymentReferenceOrderGeneralModel paymentReferenceOrderGeneralModel =
            Mockito.mock(DObPaymentReferenceOrderGeneralModel.class);
    private static DObPaymentReferenceVendorInvoiceGeneralModel paymentReferenceVendorInvoiceGeneralModel =
            Mockito.mock(DObPaymentReferenceVendorInvoiceGeneralModel.class);
    private static String paymentType;
    private static Long paymentRequestId;
    private static Long refInstanceId;
    private static Long bankAccountId;
    private static Long treasuryAccountId;
    private static String paymentForm;
    private static String exchangeRate;
    private static BigDecimal currencyPrice;
    private static BigDecimal balance;
    private static BigDecimal dueDocumentCurrencyPrice;
    private static Long localCurrencyId;
    private static String debitSubLedger;
    private static Long glDebitAccountId;

    @Builder(builderMethodName = "buildPaymentDetailsGeneralModel")
    public static IObPaymentRequestPaymentDetailsGeneralModel buildPaymentDetailsGeneralModel() {
        Mockito.doReturn(paymentCode).when(paymentDetails).getUserCode();
        Mockito.doReturn(amount).when(paymentDetails).getNetAmount();
        Mockito.doReturn(dueDocumentCode).when(paymentDetails).getDueDocumentCode();
        Mockito.doReturn(dueDocumentType).when(paymentDetails).getDueDocumentType();
        Mockito.doReturn(businessPartnerCode).when(paymentDetails).getBusinessPartnerCode();
        Mockito.doReturn(bankAccountId).when(paymentDetails).getBankAccountId();
        Mockito.doReturn(treasuryAccountId).when(paymentDetails).getTreasuryAccountId();
        Mockito.doReturn(refInstanceId).when(paymentDetails).getRefInstanceId();
        Mockito.doReturn(paymentForm).when(paymentDetails).getPaymentForm();
        return paymentDetails;
    }

    @Builder(builderMethodName = "buildPaymentRequest")
    public static DObPaymentRequest buildPaymentRequest() {
        Mockito.doReturn(paymentCode).when(paymentRequest).getUserCode();
        Mockito.doReturn(paymentType).when(paymentRequest).getPaymentType();
        Mockito.doReturn(paymentRequestId).when(paymentRequest).getId();
        return paymentRequest;
    }
    @Builder(builderMethodName = "buildPaymentRequestGeneralModel")
    public static DObPaymentRequestGeneralModel buildPaymentRequestGeneralModel() {
        DObPaymentRequestGeneralModel paymentRequestGeneralModel =
                Mockito.mock(DObPaymentRequestGeneralModel.class);
        Mockito.doReturn(paymentCode).when(paymentRequestGeneralModel).getUserCode();
        Mockito.doReturn(amount).when(paymentRequestGeneralModel).getAmountValue();
        Mockito.doReturn(dueDocumentCode).when(paymentRequestGeneralModel).getReferenceDocumentCode();
        return paymentRequestGeneralModel;
    }

  @Builder(builderMethodName = "buildPaymentRequestActivatePreparationGeneralModel")
  public static DObPaymentRequestActivatePreparationGeneralModel
      buildPaymentRequestActivatePreparationGeneralModel() {
    DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel =
            Mockito.mock(DObPaymentRequestActivatePreparationGeneralModel.class);
    Mockito.doReturn(paymentCode).when(paymentRequestActivatePreparationGeneralModel).getPaymentRequestCode();
    Mockito.doReturn(dueDocumentCode).when(paymentRequestActivatePreparationGeneralModel).getDueDocumentCode();
    Mockito.doReturn(currencyPrice).when(paymentRequestActivatePreparationGeneralModel).getCurrencyPrice();
    Mockito.doReturn(exchangeRate).when(paymentRequestActivatePreparationGeneralModel).getExchangeRate();
    Mockito.doReturn(balance).when(paymentRequestActivatePreparationGeneralModel).getBalance();
    Mockito.doReturn(amount).when(paymentRequestActivatePreparationGeneralModel).getPaymentNetAmount();
    return paymentRequestActivatePreparationGeneralModel;
  }

    @Builder(builderMethodName = "buildPaymentCalculateRealizedExchangeRateDifferenceGeneralModel")
    public static DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel
    buildPaymentCalculateRealizedExchangeRateDifferenceGeneralModel() {
        Mockito.doReturn(paymentCode).when(calculateRealizedExchangeRateDifferenceGeneralModel).getPaymentCode();
        Mockito.doReturn(currencyPrice).when(calculateRealizedExchangeRateDifferenceGeneralModel).getPaymentCurrencyPrice();
        Mockito.doReturn(localCurrencyId).when(calculateRealizedExchangeRateDifferenceGeneralModel).getLocalCurrencyId();
        Mockito.doReturn(amount).when(calculateRealizedExchangeRateDifferenceGeneralModel).getPaymentAmount();
        Mockito.doReturn(dueDocumentCurrencyPrice).when(calculateRealizedExchangeRateDifferenceGeneralModel).getVendorInvoiceCurrencyPrice();
        Mockito.doReturn(glDebitAccountId).when(calculateRealizedExchangeRateDifferenceGeneralModel).getGlAccountId();
        Mockito.doReturn(businessPartnerId).when(calculateRealizedExchangeRateDifferenceGeneralModel).getGlSubAccountId();
        Mockito.doReturn(debitSubLedger).when(calculateRealizedExchangeRateDifferenceGeneralModel).getSubLedger();

        return calculateRealizedExchangeRateDifferenceGeneralModel;
    }

    @Builder(builderMethodName = "buildPaymentCreateValueObject")
    public static DObPaymentRequestCreateValueObject
    buildPaymentCreateValueObject() {
        PaymentTypeEnum.PaymentType paymentTypeEnum
                = PaymentTypeEnum.PaymentType.valueOf(paymentType);
        Mockito.doReturn(paymentTypeEnum).when(paymentRequestCreateValueObject).getPaymentType();
        PaymentFormEnum.PaymentForm paymentFormEnum
                = PaymentFormEnum.PaymentForm.valueOf(paymentForm);
        Mockito.doReturn(paymentFormEnum).when(paymentRequestCreateValueObject).getPaymentForm();
        DueDocumentTypeEnum.DueDocumentType dueDocumentTypeEnum
                = DueDocumentTypeEnum.DueDocumentType.valueOf(dueDocumentType);
        Mockito.doReturn(dueDocumentTypeEnum).when(paymentRequestCreateValueObject).getDueDocumentType();
        Mockito.doReturn(dueDocumentCode).when(paymentRequestCreateValueObject).getDueDocumentCode();
        Mockito.doReturn(businessPartnerCode).when(paymentRequestCreateValueObject).getBusinessPartnerCode();
        Mockito.doReturn(businessUnitCode).when(paymentRequestCreateValueObject).getBusinessUnitCode();
        Mockito.doReturn(companyCode).when(paymentRequestCreateValueObject).getCompanyCode();
        Mockito.doReturn(documentOwnerId).when(paymentRequestCreateValueObject).getDocumentOwnerId();

        return paymentRequestCreateValueObject;
    }

    @Builder(builderMethodName = "buildPaymentReferenceOrderGeneralModel")
    public static DObPaymentReferenceOrderGeneralModel
    buildPaymentReferenceOrderGeneralModel() {
        Mockito.doReturn(dueDocumentId).when(paymentReferenceOrderGeneralModel).getId();
        Mockito.doReturn(dueDocumentCode).when(paymentReferenceOrderGeneralModel).getCode();
        Mockito.doReturn(dueDocumentCompanyId).when(paymentReferenceOrderGeneralModel).getCompanyId();
        Mockito.doReturn(dueDocumentCurrencyId).when(paymentReferenceOrderGeneralModel).getCurrencyId();
        return paymentReferenceOrderGeneralModel;
    }

    @Builder(builderMethodName = "buildPaymentReferenceVendorInvoiceGeneralModel")
    public static DObPaymentReferenceVendorInvoiceGeneralModel
    buildPaymentReferenceVendorInvoiceGeneralModel() {
        Mockito.doReturn(dueDocumentId).when(paymentReferenceVendorInvoiceGeneralModel).getId();
        Mockito.doReturn(dueDocumentCode).when(paymentReferenceVendorInvoiceGeneralModel).getCode();
        Mockito.doReturn(dueDocumentCompanyId).when(paymentReferenceVendorInvoiceGeneralModel).getCompanyId();
        Mockito.doReturn(dueDocumentCurrencyId).when(paymentReferenceVendorInvoiceGeneralModel).getCurrencyId();
        return paymentReferenceVendorInvoiceGeneralModel;
    }

  public static void withPaymentCode(String paymentCode) {
    PaymentUtils.paymentCode = paymentCode;
  }

  public static void withAmount(BigDecimal amount) {
    PaymentUtils.amount = amount;
  }

  public static void withDueDocumentCode(String dueDocumentCode) {
    PaymentUtils.dueDocumentCode = dueDocumentCode;
  }

  public static void withDueDocumentCompanyId(Long dueDocumentCompanyId) {
    PaymentUtils.dueDocumentCompanyId = dueDocumentCompanyId;
  }
  public static void withDueDocumentCurrencyId(Long dueDocumentCurrencyId) {
    PaymentUtils.dueDocumentCurrencyId = dueDocumentCurrencyId;
  }
  public static void withDueDocumentId(Long dueDocumentId) {
    PaymentUtils.dueDocumentId = dueDocumentId;
  }

  public static void withCurrencyPrice(BigDecimal currencyPrice) {
    PaymentUtils.currencyPrice = currencyPrice;
  }

  public static void withBalance(BigDecimal balance) {
    PaymentUtils.balance = balance;
  }

  public static void withExchangeRate(String exchangeRate) {
    PaymentUtils.exchangeRate = exchangeRate;
  }
    public static void withDueDocumentType(String dueDocumentType) { PaymentUtils.dueDocumentType = dueDocumentType; }

    public static void withBusinessPartnerCode(String businessPartnerCode) { PaymentUtils.businessPartnerCode = businessPartnerCode; }

    public static void withBusinessUnitCode(String businessUnitCode) { PaymentUtils.businessUnitCode = businessUnitCode; }

    public static void withCompanyCode(String companyCode) { PaymentUtils.companyCode = companyCode; }

    public static void withDocumentOwnerId(Long documentOwnerId) { PaymentUtils.documentOwnerId = documentOwnerId; }

    public static void withPaymentType(String paymentType) { PaymentUtils.paymentType = paymentType; }

    public static void withPaymentRequestId(Long paymentRequestId) { PaymentUtils.paymentRequestId = paymentRequestId; }

    public static void withRefInstanceId(Long refInstanceId) { PaymentUtils.refInstanceId = refInstanceId; }

    public static void withBankAccountId(Long bankAccountId) { PaymentUtils.bankAccountId = bankAccountId; }

    public static void withTreasuryAccountId(Long treasuryAccountId) { PaymentUtils.treasuryAccountId = treasuryAccountId; }

    public static void withPaymentForm(String paymentForm) {PaymentUtils.paymentForm = paymentForm; }

    public static DObPaymentRequest getPaymentRequest() { return paymentRequest; }

    public static IObPaymentRequestPaymentDetailsGeneralModel getPaymentDetails() { return paymentDetails; }

    public static DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel getCalculateRealizedExchangeRateDifferenceGeneralModel() {
        return calculateRealizedExchangeRateDifferenceGeneralModel;
    }

    public static DObPaymentRequestCreateValueObject getPaymentRequestCreateValueObject() {
        return paymentRequestCreateValueObject;
    }

    public static DObPaymentReferenceOrderGeneralModel getPaymentReferenceOrderGeneralModel() {
        return paymentReferenceOrderGeneralModel;
    }

    public static DObPaymentReferenceVendorInvoiceGeneralModel getPaymentReferenceVendorInvoiceGeneralModel() {
        return paymentReferenceVendorInvoiceGeneralModel;
    }

    public static void withDueDocumentCurrencyPrice(BigDecimal dueDocumentCurrencyPrice) {
        PaymentUtils.dueDocumentCurrencyPrice = dueDocumentCurrencyPrice;

    }

    public static void withLocalCurrencyId(Long localCurrencyId) {
        PaymentUtils.localCurrencyId = localCurrencyId;

    }

    public static void withDebitSubLedger(String debitSubLedger) {
        PaymentUtils.debitSubLedger = debitSubLedger;

    }

    public static void withGlDebitAccountId(Long glDebitAccountId) {
        PaymentUtils.glDebitAccountId = glDebitAccountId;

    }

    public static void withBusinessPartnerId(Long businessPartnerId) {
        PaymentUtils.businessPartnerId = businessPartnerId;

    }

    public static void resetValue(){
        paymentRequest = Mockito.mock(DObPaymentRequest.class);
        paymentDetails = Mockito.mock(IObPaymentRequestPaymentDetailsGeneralModel.class);
        calculateRealizedExchangeRateDifferenceGeneralModel = Mockito.mock(DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel.class);
        paymentRequestCreateValueObject = Mockito.mock(DObPaymentRequestCreateValueObject.class);
        paymentReferenceOrderGeneralModel = Mockito.mock(DObPaymentReferenceOrderGeneralModel.class);
        paymentReferenceVendorInvoiceGeneralModel = Mockito.mock(DObPaymentReferenceVendorInvoiceGeneralModel.class);
    }
}
