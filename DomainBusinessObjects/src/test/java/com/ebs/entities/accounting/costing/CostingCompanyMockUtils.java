package com.ebs.entities.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingCompanyDataGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class CostingCompanyMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObCostingCompanyDataGeneralModel buildGeneralModel(
      Long purchaseUnitId, Long companyId, String documentCode) {
    IObCostingCompanyDataGeneralModel companyDataGeneralModel =
        Mockito.mock(IObCostingCompanyDataGeneralModel.class);

    Mockito.doReturn(purchaseUnitId).when(companyDataGeneralModel).getPurchaseUnitId();
    Mockito.doReturn(companyId).when(companyDataGeneralModel).getCompanyId();
    Mockito.doReturn(documentCode).when(companyDataGeneralModel).getDocumentCode();

    return companyDataGeneralModel;
  }
}
