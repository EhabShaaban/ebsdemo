package com.ebs.entities.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObNotesReceivableDetailsMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObMonetaryNotesDetailsGeneralModel buildGeneralModel(
          String notesReceivableCode, BigDecimal amount, BigDecimal remaining, Long refInstanceId, String noteNumber, String noteBank, DateTime dueDate, String depotCode, String noteForm, String currencyIso, String businessPartnerCode) {
    IObMonetaryNotesDetailsGeneralModel notesDetailsGeneralModel =
        Mockito.mock(IObMonetaryNotesDetailsGeneralModel.class);

    Mockito.doReturn(notesReceivableCode).when(notesDetailsGeneralModel).getUserCode();
    Mockito.doReturn(amount).when(notesDetailsGeneralModel).getAmount();
    Mockito.doReturn(remaining).when(notesDetailsGeneralModel).getRemaining();
    Mockito.doReturn(refInstanceId).when(notesDetailsGeneralModel).getRefInstanceId();
    Mockito.doReturn(noteNumber).when(notesDetailsGeneralModel).getNoteNumber();
    Mockito.doReturn(noteBank).when(notesDetailsGeneralModel).getNoteBank();
    Mockito.doReturn(dueDate).when(notesDetailsGeneralModel).getDueDate();
    Mockito.doReturn(depotCode).when(notesDetailsGeneralModel).getDepotCode();
    Mockito.doReturn(noteForm).when(notesDetailsGeneralModel).getNoteForm();
    Mockito.doReturn(currencyIso).when(notesDetailsGeneralModel).getCurrencyIso();
    Mockito.doReturn(businessPartnerCode).when(notesDetailsGeneralModel).getBusinessPartnerCode();

    return notesDetailsGeneralModel;
  }
}
