package com.ebs.entities.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObNotesReceivableReferenceDocumentMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObNotesReceivablesReferenceDocumentGeneralModel buildGeneralModel(
      String notesReceivableCode,
      String refDocumentCode,
      BigDecimal amountToCollect,
      String documentType,
      String currencyIso) {
    IObNotesReceivablesReferenceDocumentGeneralModel notesReceivablesReferenceDocumentGeneralModel =
        Mockito.mock(IObNotesReceivablesReferenceDocumentGeneralModel.class);

    Mockito.doReturn(notesReceivableCode)
        .when(notesReceivablesReferenceDocumentGeneralModel)
        .getNotesReceivableCode();
    Mockito.doReturn(refDocumentCode)
        .when(notesReceivablesReferenceDocumentGeneralModel)
        .getRefDocumentCode();
    Mockito.doReturn(amountToCollect)
        .when(notesReceivablesReferenceDocumentGeneralModel)
        .getAmountToCollect();
    Mockito.doReturn(documentType)
        .when(notesReceivablesReferenceDocumentGeneralModel)
        .getDocumentType();
    Mockito.doReturn(currencyIso)
            .when(notesReceivablesReferenceDocumentGeneralModel)
            .getCurrencyIso();

    return notesReceivablesReferenceDocumentGeneralModel;
  }
}
