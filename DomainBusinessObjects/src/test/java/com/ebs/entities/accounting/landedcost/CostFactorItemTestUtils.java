package com.ebs.entities.accounting.landedcost;

import java.math.BigDecimal;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItems;
import lombok.Builder;

public class CostFactorItemTestUtils {

  @Builder(builderMethodName = "newEntity")
  public static IObLandedCostFactorItems buildEntity(Long landedcostId, Long mobItemId,
      BigDecimal estimatedValue, BigDecimal actualValue) {

    IObLandedCostFactorItems costFactorItem = new IObLandedCostFactorItems();
    costFactorItem.setRefInstanceId(landedcostId);
    costFactorItem.setCostItemId(mobItemId);

    if (estimatedValue != null) {
      costFactorItem.setEstimateValue(estimatedValue);
    }
    if (actualValue != null) {
      costFactorItem.setActualValue(actualValue);
    }
    return costFactorItem;
  }

}
