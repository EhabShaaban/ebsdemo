package com.ebs.entities.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.DObCosting;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

public class CostingMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static DObCosting buildEntity(
      Long id,
      String userCode,
      String creationInfo,
      String modificationInfo,
      DateTime creationDate,
      DateTime modifiedDate) {
    DObCosting costing = Mockito.mock(DObCosting.class);
    Mockito.doReturn(id).when(costing).getId();
    Mockito.doReturn(userCode).when(costing).getUserCode();
    Mockito.doReturn(creationInfo).when(costing).getCreationInfo();
    Mockito.doReturn(modificationInfo).when(costing).getModificationInfo();
    Mockito.doReturn(modificationInfo).when(costing).getModificationInfo();
    Mockito.doReturn(creationDate).when(costing).getCreationDate();
    Mockito.doReturn(modifiedDate).when(costing).getModifiedDate();
    return costing;
  }

  @Builder(builderMethodName = "mockGeneralModel")
  public static DObCostingGeneralModel buildGeneralModel(
          String userCode) {
    DObCostingGeneralModel costingGeneralModel =
            Mockito.mock(DObCostingGeneralModel.class);
    Mockito.doReturn(userCode).when(costingGeneralModel).getUserCode();
    return costingGeneralModel;
  }
}

