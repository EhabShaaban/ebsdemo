package com.ebs.entities.accounting.settlement;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import lombok.Builder;

import java.math.BigDecimal;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class IObSettlementAccountDetailsGeneralModelMockUtils {

  @Builder(builderMethodName = "buildGeneralModel")
  public static IObSettlementAccountDetailsGeneralModel mockGeneralModel(
    BigDecimal amount,
    String accountingEntry,
    String docCode,
    String subLedger,
    String glSubAccountCode,
    LocalizedString glSubAccountName,
    BigDecimal balance
  ) {
    IObSettlementAccountDetailsGeneralModel generalModel =
      mock(IObSettlementAccountDetailsGeneralModel.class);

    doReturn(amount).when(generalModel).getAmount();
    doReturn(accountingEntry).when(generalModel).getAccountingEntry();
    doReturn(docCode).when(generalModel).getDocumentCode();
    doReturn(subLedger).when(generalModel).getSubLedger();
    doReturn(balance).when(generalModel).getBalance();
    doReturn(glSubAccountCode).when(generalModel).getGlSubAccountCode();
    doReturn(glSubAccountName).when(generalModel).getGlSubAccountName();

    return generalModel;
  }

}
