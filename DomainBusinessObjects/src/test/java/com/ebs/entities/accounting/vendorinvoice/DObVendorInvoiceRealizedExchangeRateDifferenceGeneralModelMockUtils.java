package com.ebs.entities.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils {

  private static BigDecimal paymentCurrencyPrice;
  private static BigDecimal paymentRequestNetAmount;
  private static Long companyCurrencyId;
  private static BigDecimal vendorInvoiceCurrencyPrice;
  private static Long vendorInvoiceGlAccountId;
  private static Long vendorInvoiceGlSubAccountId;
  private static String vendorInvoiceAccountLedger;
  private static DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel
      calculateRealizedExchangeRateDifferenceGeneralModel =
          Mockito.mock(DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel.class);

  @Builder(builderMethodName = "mockEntity")
  public static DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel buildEntity() {
    Mockito.doReturn(paymentCurrencyPrice)
        .when(calculateRealizedExchangeRateDifferenceGeneralModel)
        .getPaymentCurrencyPrice();
    Mockito.doReturn(paymentRequestNetAmount)
        .when(calculateRealizedExchangeRateDifferenceGeneralModel)
        .getPaymentRequestNetAmount();
    Mockito.doReturn(companyCurrencyId)
        .when(calculateRealizedExchangeRateDifferenceGeneralModel)
        .getCompanyCurrencyId();
    Mockito.doReturn(vendorInvoiceCurrencyPrice)
        .when(calculateRealizedExchangeRateDifferenceGeneralModel)
        .getVendorInvoiceCurrencyPrice();
    Mockito.doReturn(vendorInvoiceGlAccountId)
        .when(calculateRealizedExchangeRateDifferenceGeneralModel)
        .getVendorInvoiceGlAccountId();
    Mockito.doReturn(vendorInvoiceGlSubAccountId)
        .when(calculateRealizedExchangeRateDifferenceGeneralModel)
        .getVendorInvoiceGlSubAccountId();
    Mockito.doReturn(vendorInvoiceAccountLedger)
        .when(calculateRealizedExchangeRateDifferenceGeneralModel)
        .getVendorInvoiceAccountLedger();

    return calculateRealizedExchangeRateDifferenceGeneralModel;
  }

  public static void withPaymentCurrencyPrice(BigDecimal paymentCurrencyPrice) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.paymentCurrencyPrice =
        paymentCurrencyPrice;
  }

  public static void withPaymentRequestNetAmount(BigDecimal paymentRequestNetAmount) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.paymentRequestNetAmount =
        paymentRequestNetAmount;
  }

  public static void withCompanyCurrencyId(Long companyCurrencyId) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.companyCurrencyId =
        companyCurrencyId;
  }

  public static void withVendorInvoiceCurrencyPrice(BigDecimal vendorInvoiceCurrencyPrice) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.vendorInvoiceCurrencyPrice =
        vendorInvoiceCurrencyPrice;
  }

  public static void withVendorInvoiceGlAccountId(Long vendorInvoiceGlAccountId) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.vendorInvoiceGlAccountId =
        vendorInvoiceGlAccountId;
  }

  public static void withVendorInvoiceGlSubAccountId(Long vendorInvoiceGlSubAccountId) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils
            .vendorInvoiceGlSubAccountId =
        vendorInvoiceGlSubAccountId;
  }

  public static void withVendorInvoiceAccountLedger(String vendorInvoiceAccountLedger) {
    DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelMockUtils.vendorInvoiceAccountLedger =
        vendorInvoiceAccountLedger;
  }

  public static void resetValue() {
    calculateRealizedExchangeRateDifferenceGeneralModel =
        Mockito.mock(DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel.class);
  }
}
