package com.ebs.entities.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

public class DObJournalEntryMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static DObJournalEntryGeneralModel buildGeneralModel(
      String userCode,
      String documentReferenceCode,
      DateTime dueDate,
      Long fiscalPeriodId,
      Object documentType) {
    DObJournalEntryGeneralModel journalEntryGeneralModel =
        Mockito.mock(DObJournalEntryGeneralModel.class);
    Mockito.doReturn(userCode).when(journalEntryGeneralModel).getUserCode();
    Mockito.doReturn(documentReferenceCode)
        .when(journalEntryGeneralModel)
        .getDocumentReferenceCode();
    Mockito.doReturn(dueDate).when(journalEntryGeneralModel).getDueDate();
    Mockito.doReturn(fiscalPeriodId).when(journalEntryGeneralModel).getFiscalPeriodId();
    Mockito.doReturn(documentType).when(journalEntryGeneralModel).getDocumentReferenceType();
    return journalEntryGeneralModel;
  }
}
