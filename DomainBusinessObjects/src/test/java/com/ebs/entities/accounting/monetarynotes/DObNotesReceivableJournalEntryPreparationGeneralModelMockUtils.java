package com.ebs.entities.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class DObNotesReceivableJournalEntryPreparationGeneralModelMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static DObNotesReceivableJournalEntryPreparationGeneralModel buildGeneralModel(Long id,
    String code, Long companyId, Long businessUnitId, Long fiscalPeriodId, Long companyCurrencyId,
    Long documentCurrencyId, Long exchangeRateId, BigDecimal currencyPrice) {

    DObNotesReceivableJournalEntryPreparationGeneralModel preparationGeneralModel =
      Mockito.mock(DObNotesReceivableJournalEntryPreparationGeneralModel.class);

    Mockito.doReturn(id).when(preparationGeneralModel).getId();
    Mockito.doReturn(code).when(preparationGeneralModel).getCode();
    Mockito.doReturn(companyId).when(preparationGeneralModel).getCompanyId();
    Mockito.doReturn(businessUnitId).when(preparationGeneralModel).getBusinessUnitId();
    Mockito.doReturn(fiscalPeriodId).when(preparationGeneralModel).getFiscalPeriodId();
    Mockito.doReturn(companyCurrencyId).when(preparationGeneralModel).getCompanyCurrencyId();
    Mockito.doReturn(documentCurrencyId).when(preparationGeneralModel).getDocumentCurrencyId();
    Mockito.doReturn(exchangeRateId).when(preparationGeneralModel).getExchangeRateId();
    Mockito.doReturn(currencyPrice).when(preparationGeneralModel).getCurrencyPrice();

    return preparationGeneralModel;
  }

}
