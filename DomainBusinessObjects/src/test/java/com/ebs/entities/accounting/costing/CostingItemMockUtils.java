package com.ebs.entities.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingItem;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CostingItemMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static IObCostingItemGeneralModel buildGeneralModel(
      String userCode,
      String itemCode,
      String uomCode,
      String stockType,
      BigDecimal estimateUnitPrice,
      BigDecimal estimateUnitLandedCost,
      BigDecimal actualUnitPrice,
      BigDecimal actualUnitLandedCost) {
    IObCostingItemGeneralModel costingItem = Mockito.mock(IObCostingItemGeneralModel.class);

    Mockito.doReturn(userCode).when(costingItem).getUserCode();
    Mockito.doReturn(itemCode).when(costingItem).getItemCode();
    Mockito.doReturn(uomCode).when(costingItem).getUomCode();
    Mockito.doReturn(stockType).when(costingItem).getStockType();
    Mockito.doReturn(estimateUnitPrice).when(costingItem).getEstimateUnitPrice();
    Mockito.doReturn(estimateUnitLandedCost).when(costingItem).getEstimateUnitLandedCost();
    Mockito.doReturn(actualUnitPrice).when(costingItem).getActualUnitPrice();
    Mockito.doReturn(actualUnitLandedCost).when(costingItem).getActualUnitLandedCost();
    return costingItem;
  }

  @Builder(builderMethodName = "mockEntity")
  public static IObCostingItem buildEntity(
      Long refInstanceId,
      Long id,
      Long itemId,
      Long uomId,
      BigDecimal quantity,
      String stockType,
      BigDecimal itemWeight) {
    IObCostingItem costingItem = Mockito.mock(IObCostingItem.class);
    Mockito.doReturn(id).when(costingItem).getId();
    Mockito.doReturn(refInstanceId).when(costingItem).getRefInstanceId();
    Mockito.doReturn(itemId).when(costingItem).getItemId();
    Mockito.doReturn(uomId).when(costingItem).getUomId();
    Mockito.doReturn(quantity).when(costingItem).getQuantity();
    Mockito.doReturn(stockType).when(costingItem).getStockType();
    Mockito.doReturn(itemWeight).when(costingItem).getToBeConsideredInCostingPer();
    return costingItem;
  }
}
