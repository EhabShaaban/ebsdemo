package com.ebs.entities.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class IObVendorInvoiceTaxesGeneralModelMockUtils {
    @Builder(builderMethodName = "mockGeneralModel")
    public static IObVendorInvoiceTaxesGeneralModel buildGeneralModel(BigDecimal percentage) {
        IObVendorInvoiceTaxesGeneralModel taxesGeneralModel = Mockito.mock(IObVendorInvoiceTaxesGeneralModel.class);
        Mockito.doReturn(percentage).when(taxesGeneralModel).getTaxPercentage();
        return taxesGeneralModel;
    }

}
