package com.ebs.entities.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class IObJournalEntryItemMockUtils {

  @Builder(builderMethodName = "mockGeneralModel")
  public static IObJournalEntryItemGeneralModel buildGeneralModel(
      String journalEntryCode, Long exchangeRateId, String currencyPrice) {
    IObJournalEntryItemGeneralModel costingJournalEntry =
        Mockito.mock(IObJournalEntryItemGeneralModel.class);
    Mockito.doReturn(journalEntryCode).when(costingJournalEntry).getCode();
    Mockito.doReturn(exchangeRateId).when(costingJournalEntry).getCompanyExchangeRateId();
    Mockito.doReturn(currencyPrice).when(costingJournalEntry).getCompanyCurrencyPrice();
    return costingJournalEntry;
  }
}
