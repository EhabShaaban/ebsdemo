package com.ebs.entities.accounting.salesinvoice;

import java.math.BigDecimal;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModel;
import org.mockito.Mockito;
import lombok.Builder;

public class IObSalesInvoiceAccountingDetailsGeneralModelMockUtils {

    private static String salesInvoiceCode;
    private static Long glAccountId;
    private static Long glSubAccountId;
    private static String accountingEntry;
    private static String subLedger;
    private static BigDecimal amount;

    @Builder(builderMethodName = "mockGeneralModel")
    public static IObSalesInvoiceAccountingDetailsGeneralModel buildEntity() {
        IObSalesInvoiceAccountingDetailsGeneralModel accountingDetailsGeneralModel = Mockito.mock(IObSalesInvoiceAccountingDetailsGeneralModel.class);
        Mockito.doReturn(salesInvoiceCode).when(accountingDetailsGeneralModel).getDocumentCode();
        Mockito.doReturn(glAccountId).when(accountingDetailsGeneralModel).getGlAccountId();
        Mockito.doReturn(glSubAccountId).when(accountingDetailsGeneralModel).getGlSubAccountId();
        Mockito.doReturn(amount).when(accountingDetailsGeneralModel).getAmount();
        Mockito.doReturn(accountingEntry).when(accountingDetailsGeneralModel).getAccountingEntry();
        Mockito.doReturn(subLedger).when(accountingDetailsGeneralModel).getSubLedger();
        return accountingDetailsGeneralModel;
    }

    public static void withSalesInvoiceCode(String salesInvoiceCode) {
        IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.salesInvoiceCode = salesInvoiceCode;
    }

    public static void withGlAccountId(Long glAccountId) {
        IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.glAccountId = glAccountId;
    }

    public static void withGlSubAccountId(Long glSubAccountId) {
        IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.glSubAccountId = glSubAccountId;
    }

    public static void withAccountingEntry(String accountingEntry) {
        IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.accountingEntry = accountingEntry;
    }

    public static void withSubLedger(String subLedger) {
        IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.subLedger = subLedger;
    }

    public static void withAmount(BigDecimal amount) {
        IObSalesInvoiceAccountingDetailsGeneralModelMockUtils.amount = amount;
    }
}
