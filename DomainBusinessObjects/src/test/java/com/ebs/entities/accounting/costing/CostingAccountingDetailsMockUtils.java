package com.ebs.entities.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingAccountingDetailsGeneralModel;
import java.math.BigDecimal;
import lombok.Builder;
import org.mockito.Mockito;

public class CostingAccountingDetailsMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObCostingAccountingDetailsGeneralModel buildGeneralModel(
      String currencyISO,
      Long glSubAccountId,
      Long glAccountId,
      String accountingEntry,
      String amount) {
    IObCostingAccountingDetailsGeneralModel costingAccountingDetailsGeneralModel =
        Mockito.mock(IObCostingAccountingDetailsGeneralModel.class);
    Mockito.doReturn(currencyISO).when(costingAccountingDetailsGeneralModel).getCurrencyISO();
    Mockito.doReturn(glAccountId).when(costingAccountingDetailsGeneralModel).getGlAccountId();
    Mockito.doReturn(glSubAccountId).when(costingAccountingDetailsGeneralModel).getGlSubAccountId();
    Mockito.doReturn(accountingEntry)
        .when(costingAccountingDetailsGeneralModel)
        .getAccountingEntry();
    Mockito.doReturn(new BigDecimal(amount)).when(costingAccountingDetailsGeneralModel).getAmount();
    return costingAccountingDetailsGeneralModel;
  }
}
