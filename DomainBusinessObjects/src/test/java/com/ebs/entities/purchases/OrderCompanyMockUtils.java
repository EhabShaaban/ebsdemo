package com.ebs.entities.purchases;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class OrderCompanyMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObOrderCompanyGeneralModel buildGeneralModel(
      Long purchaseUnitId, Long companyId, String companyCode) {
    IObOrderCompanyGeneralModel orderCompany = Mockito.mock(IObOrderCompanyGeneralModel.class);

    Mockito.doReturn(purchaseUnitId).when(orderCompany).getPurchaseUnitId();
    Mockito.doReturn(companyId).when(orderCompany).getCompanyId();
    Mockito.doReturn(companyCode).when(orderCompany).getCompanyCode();

    return orderCompany;
  }
}
