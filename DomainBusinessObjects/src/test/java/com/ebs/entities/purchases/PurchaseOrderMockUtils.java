package com.ebs.entities.purchases;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.Set;

public class PurchaseOrderMockUtils {
  @Builder(builderMethodName = "mockEntity")
  public static DObPurchaseOrder buildEntity(Long id) {
    DObPurchaseOrder purchaseOrder = Mockito.mock(DObPurchaseOrder.class);

    Mockito.doReturn(id).when(purchaseOrder).getId();

    return purchaseOrder;
  }

  @Builder(builderMethodName = "newGeneralModel")
  public static DObPurchaseOrderGeneralModel buildGeneralModel(
      String userCode, String purchaseUnitCode, String vendorCode, String state) {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        Mockito.mock(DObPurchaseOrderGeneralModel.class);

    Set<String> currentStates=new HashSet<String>();
    currentStates.add(state);

    Mockito.doReturn(userCode).when(purchaseOrderGeneralModel).getUserCode();
    Mockito.doReturn(purchaseUnitCode).when(purchaseOrderGeneralModel).getPurchaseUnitCode();
    Mockito.doReturn(vendorCode).when(purchaseOrderGeneralModel).getVendorCode();
    Mockito.doReturn(currentStates).when(purchaseOrderGeneralModel).getCurrentStates();
    return purchaseOrderGeneralModel;
  }
}
