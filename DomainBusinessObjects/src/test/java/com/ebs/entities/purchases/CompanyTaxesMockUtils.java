package com.ebs.entities.purchases;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CompanyTaxesMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static LObCompanyTaxesGeneralModel buildGeneralModel(
          String userCode, LocalizedString name, String percentage) {
    LObCompanyTaxesGeneralModel companyTaxes = Mockito.mock(LObCompanyTaxesGeneralModel.class);

    Mockito.doReturn(userCode).when(companyTaxes).getUserCode();
    Mockito.doReturn(name).when(companyTaxes).getName();
    Mockito.doReturn(new BigDecimal(percentage)).when(companyTaxes).getPercentage();
    return companyTaxes;
  }
}
