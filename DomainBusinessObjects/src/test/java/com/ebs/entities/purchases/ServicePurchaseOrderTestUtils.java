package com.ebs.entities.purchases;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObServicePurchaseOrder;
import lombok.Builder;
import org.mockito.Mockito;

public class ServicePurchaseOrderTestUtils {
    @Builder(builderMethodName = "newEntity")
    public static DObServicePurchaseOrder buildEntity() {
        return Mockito.mock(DObServicePurchaseOrder.class);
    }
}
