package com.ebs.entities;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsData;
import lombok.Builder;
import org.mockito.Mockito;

public class GoodsReceiptPurchaseOrderReceivedItemsDataMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static IObGoodsReceiptPurchaseOrderReceivedItemsData buildEntity(Long itemId,Long id) {
    IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptPurchaseOrderReceivedItemsData =
        Mockito.mock(IObGoodsReceiptPurchaseOrderReceivedItemsData.class);
    Mockito.doReturn(id).when(goodsReceiptPurchaseOrderReceivedItemsData).getId();
    Mockito.doReturn(itemId).when(goodsReceiptPurchaseOrderReceivedItemsData).getItemId();
    return goodsReceiptPurchaseOrderReceivedItemsData;
  }
}
