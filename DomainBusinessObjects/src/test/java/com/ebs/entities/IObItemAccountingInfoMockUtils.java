package com.ebs.entities;

import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class IObItemAccountingInfoMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObItemAccountingInfoGeneralModel buildGeneralModel(String itemCode, String accountCode, Long accountId) {
    IObItemAccountingInfoGeneralModel itemAccountingInfoGeneralModel =
        Mockito.mock(IObItemAccountingInfoGeneralModel.class);
    Mockito.doReturn(itemCode).when(itemAccountingInfoGeneralModel).getItemCode();
    Mockito.doReturn(accountId).when(itemAccountingInfoGeneralModel).getAccountId();
    Mockito.doReturn(accountCode).when(itemAccountingInfoGeneralModel).getAccountCode();
    return itemAccountingInfoGeneralModel;
  }
}
