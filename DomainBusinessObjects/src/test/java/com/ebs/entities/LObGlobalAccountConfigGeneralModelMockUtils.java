package com.ebs.entities;

import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class LObGlobalAccountConfigGeneralModelMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static LObGlobalGLAccountConfigGeneralModel buildGeneralModel(Long accountId) {
    LObGlobalGLAccountConfigGeneralModel globalAccountConfigGeneralModel =
        Mockito.mock(LObGlobalGLAccountConfigGeneralModel.class);
    Mockito.doReturn(accountId).when(globalAccountConfigGeneralModel).getGlAccountId();
    return globalAccountConfigGeneralModel;
  }
}
