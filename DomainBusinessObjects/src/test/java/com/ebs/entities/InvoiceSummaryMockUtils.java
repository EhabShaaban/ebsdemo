package com.ebs.entities;

import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class InvoiceSummaryMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObInvoiceSummaryGeneralModel buildGeneralModel(String invoiceCode,String invoiceType,String totalAmountBeforeTaxes) {
    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        Mockito.mock(IObInvoiceSummaryGeneralModel.class);

    Mockito.doReturn(invoiceCode).when(invoiceSummaryGeneralModel).getInvoiceCode();
    Mockito.doReturn(invoiceType).when(invoiceSummaryGeneralModel).getInvoiceType();
    Mockito.doReturn(totalAmountBeforeTaxes).when(invoiceSummaryGeneralModel).getTotalAmountBeforeTaxes();
    return invoiceSummaryGeneralModel;
  }
}
