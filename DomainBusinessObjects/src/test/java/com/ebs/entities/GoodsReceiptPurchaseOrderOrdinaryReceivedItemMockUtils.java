package com.ebs.entities;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class GoodsReceiptPurchaseOrderOrdinaryReceivedItemMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel buildGeneralModel(
          Long id, String stockType, BigDecimal quantity, String itemCode, String unitOfEntryCode) {
    IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel =
        Mockito.mock(IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel.class);
    Mockito.doReturn(id).when(goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel).getId();

    Mockito.doReturn(stockType).when(goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel).getStockType();
    Mockito.doReturn(quantity).when(goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel).getReceivedQtyUoE();
    Mockito.doReturn(itemCode).when(goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel).getItemCode();
    Mockito.doReturn(unitOfEntryCode).when(goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel).getUnitOfEntryCode();
    return goodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
  }
}
