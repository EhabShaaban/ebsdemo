package com.ebs.entities;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import lombok.Builder;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class AccountingDocumentActivationDetailsMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObAccountingDocumentActivationDetailsGeneralModel buildGeneralModel(
      String userCode,
      Long refInstanceId,
      String accountant,
      DateTime activationDate,
      DateTime dueDate,
      Long exchangeRateId,
      BigDecimal currencyPrice,
      String objectTypeCode,
      Long fiscalPeriodId) {
    IObAccountingDocumentActivationDetailsGeneralModel
        accountingDocumentActivationDetailsGeneralModel =
            Mockito.mock(IObAccountingDocumentActivationDetailsGeneralModel.class);
    Mockito.doReturn(userCode).when(accountingDocumentActivationDetailsGeneralModel).getCode();
    Mockito.doReturn(refInstanceId)
        .when(accountingDocumentActivationDetailsGeneralModel)
        .getRefInstanceId();
    Mockito.doReturn(accountant)
        .when(accountingDocumentActivationDetailsGeneralModel)
        .getAccountant();
    Mockito.doReturn(activationDate)
        .when(accountingDocumentActivationDetailsGeneralModel)
        .getActivationDate();
    Mockito.doReturn(dueDate).when(accountingDocumentActivationDetailsGeneralModel).getDueDate();
    Mockito.doReturn(exchangeRateId)
        .when(accountingDocumentActivationDetailsGeneralModel)
        .getExchangeRateId();
    Mockito.doReturn(currencyPrice)
        .when(accountingDocumentActivationDetailsGeneralModel)
        .getCurrencyPrice();
    Mockito.doReturn(objectTypeCode)
        .when(accountingDocumentActivationDetailsGeneralModel)
        .getObjectTypeCode();
    Mockito.doReturn(fiscalPeriodId)
        .when(accountingDocumentActivationDetailsGeneralModel)
        .getFiscalPeriodId();
    return accountingDocumentActivationDetailsGeneralModel;
  }
}
