package com.ebs.entities;

import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class GoodsReceiptMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static DObGoodsReceiptGeneralModel buildGeneralModel(
      Long id, String userCode, String refDocumentCode, String type, String inventoryDocumentType) {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        Mockito.mock(DObGoodsReceiptGeneralModel.class);
    Mockito.doReturn(id).when(goodsReceiptGeneralModel).getId();
    Mockito.doReturn(userCode).when(goodsReceiptGeneralModel).getUserCode();
    Mockito.doReturn(refDocumentCode).when(goodsReceiptGeneralModel).getRefDocumentCode();
    Mockito.doReturn(type).when(goodsReceiptGeneralModel).getType();
    Mockito.doReturn(inventoryDocumentType).when(goodsReceiptGeneralModel).getInventoryDocumentType();
    return goodsReceiptGeneralModel;
  }
}
