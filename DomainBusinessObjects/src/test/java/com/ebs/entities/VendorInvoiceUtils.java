package com.ebs.entities;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.Set;

public class VendorInvoiceUtils {
  @Builder(builderMethodName = "newGeneralModel")
  public static DObVendorInvoiceGeneralModel buildGeneralModel(
      Long id,
      String userCode,
      String purchaseOrderCode,
      String currencyIso,
      String purchaseUnitCode,
      String vendorCode,
      String state) {
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        Mockito.mock(DObVendorInvoiceGeneralModel.class);
    Set<String> currentStates = new HashSet<String>();
    currentStates.add(state);

    Mockito.doReturn(id).when(vendorInvoiceGeneralModel).getId();
    Mockito.doReturn(userCode).when(vendorInvoiceGeneralModel).getUserCode();
    Mockito.doReturn(purchaseOrderCode).when(vendorInvoiceGeneralModel).getPurchaseOrderCode();
    Mockito.doReturn(currencyIso).when(vendorInvoiceGeneralModel).getCurrencyISO();
    Mockito.doReturn(purchaseUnitCode).when(vendorInvoiceGeneralModel).getPurchaseUnitCode();
    Mockito.doReturn(vendorCode).when(vendorInvoiceGeneralModel).getVendorCode();
    Mockito.doReturn(currentStates).when(vendorInvoiceGeneralModel).getCurrentStates();
    return vendorInvoiceGeneralModel;
  }

  @Builder(builderMethodName = "newEntity")
  public static DObVendorInvoice buildEntity(String userCode, String purchaseOrderCode) {
    DObVendorInvoice vendorInvoice = Mockito.mock(DObVendorInvoice.class);
    Mockito.doReturn(userCode).when(vendorInvoice).getUserCode();
    Mockito.doReturn(purchaseOrderCode).when(vendorInvoice).getSysName();
    return vendorInvoice;
  }
}
