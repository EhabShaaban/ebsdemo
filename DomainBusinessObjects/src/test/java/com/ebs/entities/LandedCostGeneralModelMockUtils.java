package com.ebs.entities;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import java.util.HashSet;
import java.util.Set;
import lombok.Builder;
import org.mockito.Mockito;

public class LandedCostGeneralModelMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static DObLandedCostGeneralModel buildGeneralModel(
      String userCode, String state, String purchaseOrderCode, Long purchaseOrderId) {
    DObLandedCostGeneralModel landedCostGeneralModel =
        Mockito.mock(DObLandedCostGeneralModel.class);
    Mockito.doReturn(userCode).when(landedCostGeneralModel).getUserCode();
    Mockito.doReturn(purchaseOrderCode).when(landedCostGeneralModel).getPurchaseOrderCode();

    Set currentStates=new HashSet<String>();
    currentStates.add(state);

    Mockito.doReturn(currentStates).when(landedCostGeneralModel).getCurrentStates();
    Mockito.doReturn(purchaseOrderId).when(landedCostGeneralModel).getPurchaseOrderId();
    return landedCostGeneralModel;
  }
}
