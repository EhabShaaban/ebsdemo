package com.ebs.entities;

import lombok.Builder;
import org.mockito.Mockito;
import org.springframework.transaction.PlatformTransactionManager;

public class PlatformTransactionManagerMockUtils {

  @Builder(builderMethodName = "mockEntity")
  public static PlatformTransactionManager buildEntity() {
    PlatformTransactionManager platformTransactionManager =
        Mockito.mock(PlatformTransactionManager.class);
    return platformTransactionManager;
  }
}
