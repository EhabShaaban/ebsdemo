package com.ebs.entities;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import lombok.Builder;
import org.mockito.Mockito;

public class IObGoodsReceiptReceivedItemsGeneralModelMockUtils {
  @Builder(builderMethodName = "mockGeneralModel")
  public static IObGoodsReceiptReceivedItemsGeneralModel buildGeneralModel(String goodsReceiptCode,String itemCode) {
    IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptReceivedItemsGeneralModel =
        Mockito.mock(IObGoodsReceiptReceivedItemsGeneralModel.class);

    Mockito.doReturn(goodsReceiptCode).when(goodsReceiptReceivedItemsGeneralModel).getGoodsReceiptCode();
    Mockito.doReturn(itemCode).when(goodsReceiptReceivedItemsGeneralModel).getItemCode();
    return goodsReceiptReceivedItemsGeneralModel;
  }
}
