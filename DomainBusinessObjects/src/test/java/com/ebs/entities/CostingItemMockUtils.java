package com.ebs.entities;

import com.ebs.dda.jpa.accounting.costing.IObCostingItem;
import lombok.Builder;
import org.mockito.Mockito;

import java.math.BigDecimal;

public class CostingItemMockUtils {
  @Builder(builderMethodName = "mockEntity")
  public static IObCostingItem buildEntity(
      Long itemId,
      Long uomId,
      BigDecimal quantity,
      String stockType,
      BigDecimal toBeConsideredInCostingPer,
      BigDecimal estimateUnitPrice,
      BigDecimal actualUnitPrice,
      BigDecimal estimateUnitLandedCost,
      BigDecimal actualUnitLandedCost) {
    IObCostingItem costingItem = Mockito.mock(IObCostingItem.class);

    Mockito.doReturn(itemId).when(costingItem).getItemId();
    Mockito.doReturn(uomId).when(costingItem).getUomId();
    Mockito.doReturn(quantity).when(costingItem).getQuantity();
    Mockito.doReturn(stockType).when(costingItem).getStockType();
    Mockito.doReturn(toBeConsideredInCostingPer).when(costingItem).getToBeConsideredInCostingPer();
    Mockito.doReturn(estimateUnitPrice).when(costingItem).getEstimateUnitPrice();
    Mockito.doReturn(actualUnitPrice).when(costingItem).getActualUnitPrice();
    Mockito.doReturn(estimateUnitLandedCost).when(costingItem).getEstimateUnitLandedCost();
    Mockito.doReturn(actualUnitLandedCost).when(costingItem).getActualUnitLandedCost();

    return costingItem;
  }
}
