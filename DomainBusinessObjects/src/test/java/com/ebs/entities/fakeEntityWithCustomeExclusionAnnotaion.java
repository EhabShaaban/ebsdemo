package com.ebs.entities;

import com.ebs.dac.dbo.api.annotations.Exclude;

public class fakeEntityWithCustomeExclusionAnnotaion {
  String notAnnotatedString;
  @Exclude String annotatedString;

  public void setNotAnnotatedString(String notAnnotatedString) {
    this.notAnnotatedString = notAnnotatedString;
  }

  public void setAnnotatedString(String annotatedString) {
    this.annotatedString = annotatedString;
  }
}
