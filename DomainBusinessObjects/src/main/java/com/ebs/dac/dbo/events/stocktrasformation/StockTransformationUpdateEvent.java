package com.ebs.dac.dbo.events.stocktrasformation;

import com.ebs.dac.dbo.events.ActivateEvent;

public class StockTransformationUpdateEvent extends ActivateEvent {
    public StockTransformationUpdateEvent(String userCode) {
        super(userCode);
    }
}
