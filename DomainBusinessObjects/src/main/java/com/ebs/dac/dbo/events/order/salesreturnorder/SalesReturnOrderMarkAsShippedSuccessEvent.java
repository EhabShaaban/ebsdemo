package com.ebs.dac.dbo.events.order.salesreturnorder;

import com.ebs.dac.dbo.events.ActivateEvent;

public class SalesReturnOrderMarkAsShippedSuccessEvent extends ActivateEvent {

    public SalesReturnOrderMarkAsShippedSuccessEvent(String userCode) {
        super(userCode);
    }

}
