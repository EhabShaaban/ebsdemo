package com.ebs.dac.dbo.events.landedCost;

import com.ebs.dac.dbo.events.ActivateEvent;

public class LandedCostConvertToActualEvent extends ActivateEvent {

    public LandedCostConvertToActualEvent(String userCode) {
        super(userCode);
    }

}
