package com.ebs.dac.dbo.types;

import java.io.Serializable;
import javax.persistence.Transient;

public abstract class BusinessEnum implements Serializable {

  private static final long serialVersionUID = 1L;

  @Transient private LocalizedString name;

  public abstract Enum<?> getId();

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public boolean isNull() {
    return getId() == null && (name == null || name.isNull());
  }
}
