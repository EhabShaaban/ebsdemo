package com.ebs.dac.dbo.events.vendorinvoice;

import com.ebs.dac.dbo.events.CreateEvent;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;

public class VendorInvoiceCreateSuccessEvent extends CreateEvent {
	private DObVendorInvoice vendorInvoice;

	public VendorInvoiceCreateSuccessEvent(DObVendorInvoice vendorInvoice) {
		super(vendorInvoice.getUserCode());
		this.vendorInvoice = vendorInvoice;

	}

	public DObVendorInvoice getVendorInvoice() {
		return vendorInvoice;
	}
}
