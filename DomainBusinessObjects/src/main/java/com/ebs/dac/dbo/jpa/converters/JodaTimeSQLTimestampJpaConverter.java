package com.ebs.dac.dbo.jpa.converters;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;

@Converter
public class JodaTimeSQLTimestampJpaConverter implements AttributeConverter<DateTime, Timestamp> {

  @Override
  public Timestamp convertToDatabaseColumn(DateTime attribute) {
    Timestamp timestamp = null;

    if (attribute != null) {
      DateTime converted = attribute.withZone(DateTimeZone.UTC);
      timestamp = new Timestamp(converted.getMillis());
    }
    return timestamp;
  }

  @Override
  public DateTime convertToEntityAttribute(Timestamp dbData) {

    DateTime dateTime = null;
    if (dbData != null) {

      dateTime = new DateTime(dbData.getTime()).withZone(DateTimeZone.UTC);
    }
    return dateTime;
  }
}
