package com.ebs.dac.dbo.processing;

import com.ebs.dac.dbo.types.LocalizedString;

public class LocalizedTextAttribute<T> extends EmbeddedAttribute<T> {

  private static final long serialVersionUID = 1L;

  public LocalizedTextAttribute(String name, Class<T> type) {
    super(name, type);
  }

  public LocalizedTextAttribute(String name) {
    super(name, (Class<T>) LocalizedString.class);
  }
}
