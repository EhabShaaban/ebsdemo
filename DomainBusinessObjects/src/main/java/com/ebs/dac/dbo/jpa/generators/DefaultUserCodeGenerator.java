package com.ebs.dac.dbo.jpa.generators;

import com.ebs.dac.dbo.api.usercode.IUserCodeGenerator;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;

public class DefaultUserCodeGenerator<
        T extends CodedBusinessObject<DefaultUserCode>, UC extends UserCode>
    implements IUserCodeGenerator<T, UC> {
  @Override
  public String generate(T object) {
    return object.getSysName() + ":" + object.getId();
  }
}
