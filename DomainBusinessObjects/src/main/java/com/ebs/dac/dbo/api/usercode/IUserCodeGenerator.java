package com.ebs.dac.dbo.api.usercode;

public interface IUserCodeGenerator<CodedBusinessObject, UserCode> {

  String generate(CodedBusinessObject object);
}
