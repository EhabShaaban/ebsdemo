package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface StatefullBusinessObjectRep<
        T extends StatefullBusinessObject<UC>, UC extends UserCode>
    extends CodedBusinessObjectRep<T> {

  T findByIdAndCurrentStatesLike(Long id, String state);

  T findOneByUserCodeAndCurrentStatesLike(String code, String state);

}
