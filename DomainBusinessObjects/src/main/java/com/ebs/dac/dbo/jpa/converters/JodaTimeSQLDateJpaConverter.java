package com.ebs.dac.dbo.jpa.converters;

import java.sql.Date;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.joda.time.DateTime;

@Converter
public class JodaTimeSQLDateJpaConverter implements AttributeConverter<DateTime, Date> {

  @Override
  public Date convertToDatabaseColumn(DateTime attribute) {
    Date date = null;
    if (attribute != null) {
      date = new Date(attribute.getMillis());
    }
    return date;
  }

  @Override
  public DateTime convertToEntityAttribute(Date dbData) {
    DateTime dateTime = null;
    if (dbData != null) {
      dateTime = new DateTime(dbData.toString());
    }
    return dateTime;
  }
}
