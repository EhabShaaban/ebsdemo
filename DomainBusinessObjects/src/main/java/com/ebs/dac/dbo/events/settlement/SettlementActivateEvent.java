package com.ebs.dac.dbo.events.settlement;

import com.ebs.dac.dbo.events.ActivateEvent;

public class SettlementActivateEvent extends ActivateEvent {

	public SettlementActivateEvent(String userCode) {
		super(userCode);
	}
}
