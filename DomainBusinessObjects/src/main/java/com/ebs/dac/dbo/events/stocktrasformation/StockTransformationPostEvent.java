package com.ebs.dac.dbo.events.stocktrasformation;

import com.ebs.dac.dbo.events.ActivateEvent;

public class StockTransformationPostEvent extends ActivateEvent {

    public StockTransformationPostEvent(String userCode) {
        super(userCode);
    }

}
