package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class LookupObject<UC extends UserCode> extends CodedBusinessObject<UC> {

  private static final long serialVersionUID = 1L;

  private String description;

  public LookupObject() {}

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

}
