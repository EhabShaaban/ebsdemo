package com.ebs.dac.dbo.processing;

import com.ebs.dac.dbo.api.processing.IBasicAttribute;
import com.ebs.dac.dbo.api.processing.IEntityAttribute;

public class BasicAttribute extends EntityAttribute implements IBasicAttribute,
    IEntityAttribute {

  private static final long serialVersionUID = 1L;

  public BasicAttribute( String name) {
    super( name);
  }
}
