package com.ebs.dac.dbo.events.sales;

import com.ebs.dac.dbo.events.UpdateEvent;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;

public class SalesInvoiceUpdateItemsEvent extends UpdateEvent {

  public SalesInvoiceUpdateItemsEvent(String userCode) {
    super(userCode);
  }
  public SalesInvoiceUpdateItemsEvent(DObSalesInvoice salesInvoice) {
    super(salesInvoice);
  }

}
