package com.ebs.dac.dbo.api.objects;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Since payload of an entity can be considered a map of {@link ICompositeAttribute} and their
 * values are which their realization types are heterogeneous. Type-safe heterogeneous container
 * pattern
 *
 * @author shaf3y
 */
public interface IEntityPayloadContainer<BusinessObject> {

  /**
   * To put a value of a single instance attribute into the container
   *
   * @param attribute
   * @param value
   */
  <DetailBusinessObject> void putSingleInstanceAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute, DetailBusinessObject value);

  /**
   * To retrieve the value of a single instance attribute
   *
   * @param attribute
   * @return
   */
  <DetailBusinessObject> DetailBusinessObject getSingleInstanceAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute);

  /**
   * To put a value of a multiple instances attribute into the container
   *
   * @param attribute
   * @param value
   */
  <DetailBusinessObject> void putMultipleInstancesAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute, List<DetailBusinessObject> value);

  /**
   * Add an instance to the value of a multiple instances attribute
   *
   * @param attribute
   * @param instance
   */
  <DetailBusinessObject> void addInstanceToMultipleInstancesAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute, DetailBusinessObject instance);

  /**
   * To retrieve the value of a multiple instances attribute
   *
   * @param attribute
   * @return
   */
  <DetailBusinessObject> List<DetailBusinessObject> getMultipleInstancesAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute);

  /**
   * Search for attribute according to its multiplicity
   *
   * @param attribute
   * @return
   */
  <DetailBusinessObject> boolean containsAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute);

  /** Clear all entries of single instance and multiple instances attributes. */
  void clearAllPayload();

  /** @return set of all single instance attributes entries */
  Set<ICompositeAttribute<? extends BusinessObject>> getSingleInstanceAttributes();

  /** @return all entries of single instance attributes */
  Map<ICompositeAttribute<? extends BusinessObject>, BusinessObject>
      getSingleInstanceAttributesPayload();

  /** @return set of all multiple instances attributes entries */
  Set<ICompositeAttribute<? extends BusinessObject>> getMultipleInstancesAttributes();

  /** @return all entries of multiple instance attributes */
  Map<ICompositeAttribute<? extends BusinessObject>, List<? extends BusinessObject>>
      getMultipleInstancesAttributesPayload();
}
