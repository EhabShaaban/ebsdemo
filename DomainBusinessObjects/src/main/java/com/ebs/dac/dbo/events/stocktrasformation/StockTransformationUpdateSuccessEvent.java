package com.ebs.dac.dbo.events.stocktrasformation;

import com.ebs.dac.dbo.events.ActivateEvent;

public class StockTransformationUpdateSuccessEvent extends ActivateEvent {

    public StockTransformationUpdateSuccessEvent(String userCode) {
        super(userCode);
    }
}
