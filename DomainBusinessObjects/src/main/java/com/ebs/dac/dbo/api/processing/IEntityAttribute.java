package com.ebs.dac.dbo.api.processing;

/** Created by marisoft on 10/5/16. */
public interface IEntityAttribute {
  String getName();
}
