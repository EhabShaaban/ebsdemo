package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.TransactionObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;


@NoRepositoryBean
public interface TransactionObjectRep<T extends TransactionObject<UC>, UC extends UserCode>
        extends CodedBusinessObjectRep<T> {
}
