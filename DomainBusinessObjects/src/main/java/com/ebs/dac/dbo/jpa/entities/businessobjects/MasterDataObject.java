package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import java.util.Collection;
import java.util.List;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class MasterDataObject<UC extends UserCode> extends StatefullBusinessObject<UC>
    implements IAggregateAwareEntity {

  private static final long serialVersionUID = 1L;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private transient AggregateBusinessObject masterDetails;

  public MasterDataObject() {
    masterDetails = new AggregateBusinessObject();
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  @Override
  public AggregateBusinessObject aggregateBusinessObject() {
    return masterDetails;
  }

  public <SingleDetail extends InformationObject> void setSingleDetail(
      ICompositeAttribute<SingleDetail> singleDetailAttribute, SingleDetail singleDetail)
      throws Exception {
    this.aggregateBusinessObject().putSingleInstanceAttribute(singleDetailAttribute, singleDetail);
  }

  public <SingleDetail extends InformationObject> SingleDetail getSingleDetail(
      ICompositeAttribute<SingleDetail> singleDetailAttribute) throws Exception {
    return this.aggregateBusinessObject().getSingleInstanceAttribute(singleDetailAttribute);
  }

  public <MultipleDetail extends InformationObject> void addMultipleDetail(
      ICompositeAttribute<MultipleDetail> multipleDetailAttribute, MultipleDetail multipleDetail)
      throws Exception {
    this.aggregateBusinessObject()
        .addInstanceToMultipleInstancesAttribute(multipleDetailAttribute, multipleDetail);
  }

  public <MultipleDetail extends InformationObject>
      Collection<MultipleDetail> getAllMultipleDetails(
          ICompositeAttribute<MultipleDetail> multipleAttribute) throws Exception {
    return this.aggregateBusinessObject().getMultipleInstancesAttribute(multipleAttribute);
  }

  public <MultipleDetail extends InformationObject> void setAllMultipleDetails(
      ICompositeAttribute<MultipleDetail> multipleAttribute, List<MultipleDetail> listOfValues)
      throws Exception {
    this.aggregateBusinessObject().putMultipleInstancesAttribute(multipleAttribute, listOfValues);
  }

  public void removeMultipleDetail(
      ICompositeAttribute<InformationObject> multipleAttribute, InformationObject multipleDetail)
      throws Exception {
    this.aggregateBusinessObject()
        .removeInstanceFromMultipleInstancesAttribute(multipleAttribute, multipleDetail);
  }

  public void removeSingleDetail(ICompositeAttribute<InformationObject> singleAttribute)
      throws Exception {
    this.aggregateBusinessObject().removeSingleInstanceAttribute(singleAttribute);
  }
}
