package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;
import java.io.Serializable;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@EntityInterface(IInformationObject.class)
public abstract class InformationObject extends BusinessObject
    implements  Serializable {

  private static final long serialVersionUID = -4062178489385273764L;

  private Long refInstanceId;

  public Long getRefInstanceId() {
    return refInstanceId;
  }

  public void setRefInstanceId(Long mdInstanceId) {
    this.refInstanceId = mdInstanceId;
  }
}
