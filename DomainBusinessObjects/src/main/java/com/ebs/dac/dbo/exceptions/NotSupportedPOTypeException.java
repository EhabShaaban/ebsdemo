package com.ebs.dac.dbo.exceptions;

public class NotSupportedPOTypeException extends RuntimeException {

  public NotSupportedPOTypeException() {
    super();
  }

  public NotSupportedPOTypeException(String message) {
    super(message);
  }
}
