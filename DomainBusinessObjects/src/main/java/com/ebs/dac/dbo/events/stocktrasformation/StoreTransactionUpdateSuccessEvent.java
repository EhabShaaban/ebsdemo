package com.ebs.dac.dbo.events.stocktrasformation;

import com.ebs.dac.dbo.events.ActivateEvent;

public class StoreTransactionUpdateSuccessEvent extends ActivateEvent {

    public StoreTransactionUpdateSuccessEvent(String userCode) {
        super(userCode);
    }
}
