package com.ebs.dac.dbo.events.sales;


import com.ebs.dac.dbo.events.LockEvent;

public class SalesInvoiceUnlockSuccessEvent extends LockEvent {

  public SalesInvoiceUnlockSuccessEvent(String userCode) {
    super(userCode);
  }

}