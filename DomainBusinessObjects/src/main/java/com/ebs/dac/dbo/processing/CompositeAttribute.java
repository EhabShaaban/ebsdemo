package com.ebs.dac.dbo.processing;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;

public class CompositeAttribute<T> extends EntityAttribute implements ICompositeAttribute<T> {

  private static final long serialVersionUID = 1L;

  private boolean multiple;
  private Class<T> attributeType;

  public CompositeAttribute( String name, Class<T> type, boolean multiple) {
    super(name);
    this.attributeType = type;
    this.multiple = multiple;
  }

  public Class<T> getAttributeType() {
    return attributeType;
  }

  public boolean isMultiple() {
    return multiple;
  }

  public void setMultiple(boolean multiple) {
    this.multiple = multiple;
  }

  @Override
  public boolean equals(Object obj) {
    boolean equals = false;

    if (obj instanceof CompositeAttribute) {
      Class<T> objAttributeType = ((CompositeAttribute) obj).getAttributeType();
      if (super.equals(obj) && this.getAttributeType().equals(objAttributeType)) {
        equals = true;
      }
    }

    return equals;
  }
}
