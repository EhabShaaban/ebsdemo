package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import java.io.Serializable;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

/**
 * @author Yara Ameen
 * @since Mar 24, 2016 , 12:33:41 PM
 */
@MappedSuperclass
public abstract class ConfigurationObject<UC extends UserCode> extends StatefullBusinessObject<UC>
    implements Serializable {

  private static final long serialVersionUID = 1L;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }
}
