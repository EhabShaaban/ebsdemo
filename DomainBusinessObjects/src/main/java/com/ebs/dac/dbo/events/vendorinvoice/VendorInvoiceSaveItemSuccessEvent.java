package com.ebs.dac.dbo.events.vendorinvoice;

import com.ebs.dac.dbo.events.UpdateEvent;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;

public class VendorInvoiceSaveItemSuccessEvent extends UpdateEvent {
	private DObVendorInvoice vendorInvoice;

	public VendorInvoiceSaveItemSuccessEvent(DObVendorInvoice vendorInvoice) {
		super(vendorInvoice.getUserCode());
		this.vendorInvoice = vendorInvoice;

	}

	public DObVendorInvoice getVendorInvoice() {
		return vendorInvoice;
	}
}
