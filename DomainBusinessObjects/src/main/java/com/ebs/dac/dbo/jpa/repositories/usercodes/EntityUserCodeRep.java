package com.ebs.dac.dbo.jpa.repositories.usercodes;

import com.ebs.dac.dbo.jpa.entities.usercodes.EntityUserCode;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 19, 2018 12:14:47 PM
 */
public interface EntityUserCodeRep extends JpaRepository<EntityUserCode, Integer> {
  EntityUserCode findOneByType(String type);
}
