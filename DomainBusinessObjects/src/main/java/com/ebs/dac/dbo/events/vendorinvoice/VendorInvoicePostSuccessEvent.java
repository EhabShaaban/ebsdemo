package com.ebs.dac.dbo.events.vendorinvoice;

public class VendorInvoicePostSuccessEvent extends VendorInvoicePostEvent {

  public VendorInvoicePostSuccessEvent(String userCode) {
    super(userCode);
  }
}
