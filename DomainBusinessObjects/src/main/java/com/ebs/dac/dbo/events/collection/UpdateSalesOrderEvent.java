package com.ebs.dac.dbo.events.collection;

import com.ebs.dac.dbo.events.ActivateEvent;

public class UpdateSalesOrderEvent extends ActivateEvent {


    public UpdateSalesOrderEvent(String userCode) {
        super(userCode);
    }
}
