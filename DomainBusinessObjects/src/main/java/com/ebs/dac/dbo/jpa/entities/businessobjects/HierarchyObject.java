package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import java.io.Serializable;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class HierarchyObject<UC extends UserCode> extends ConfigurationObject<UC>
    implements Serializable {

  private static final long serialVersionUID = 1762340398820978287L;

  public HierarchyObject() {
    super();
  }
}
