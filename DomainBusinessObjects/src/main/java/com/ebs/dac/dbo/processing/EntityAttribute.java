package com.ebs.dac.dbo.processing;

import com.ebs.dac.dbo.api.processing.IEntityAttribute;
import java.io.Serializable;

public abstract class EntityAttribute implements IEntityAttribute, Serializable {

  private static final long serialVersionUID = 1L;

  private String name;

  public EntityAttribute( String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public boolean equals(Object obj) {
    boolean equals = false;

    if (obj instanceof EntityAttribute) {
      EntityAttribute attr = (EntityAttribute) obj;

      if ( this.getName().equals(attr.getName())) {
        equals = true;
      }
    }

    return equals;
  }
}
