package com.ebs.dac.dbo.types;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LocalizedString implements Serializable {

  private Map<String, String> values;

  public LocalizedString() {
    values = new HashMap<>();
  }

  public LocalizedString(Map<String, String> values) {
    this.values = values;
  }

  public void setValue(Locale locale, String value) {
    values.put(locale.getLanguage(), value);
  }

  public String getValue(String langKey) {
    if (values == null) {
      return null;
    }
    return values.get(langKey);
  }

  public String getValue(Locale locale) {
    if (values == null) {
      return null;
    }
    return values.get(locale.getLanguage());
  }

  public Map<String, String> getValues() {
    return this.values;
  }

  public boolean isNull() {
    // TODO Auto-generated method stub
    return values == null || values.size() == 0 || allAttributesValuesAreNull();
  }

  private boolean allAttributesValuesAreNull() {
    for (String key : values.keySet()) {
      if (values.get(key) != null) return false;
    }
    return true;
  }
}
