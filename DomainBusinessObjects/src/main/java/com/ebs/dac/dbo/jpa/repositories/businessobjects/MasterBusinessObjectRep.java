package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface MasterBusinessObjectRep<T extends BusinessObject> extends BusinessObjectRep<T> {}
