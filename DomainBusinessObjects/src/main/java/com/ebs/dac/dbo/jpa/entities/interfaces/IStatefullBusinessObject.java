package com.ebs.dac.dbo.jpa.entities.interfaces;

import java.util.Set;

public interface IStatefullBusinessObject{

  String CURRENT_STATES_ATTR_NAME = "currentStates";

  Set<String> getCurrentStates();

  void setCurrentStates(Set<String> currentStates);

  String getCurrentState();

}
