package com.ebs.dac.dbo.jpa.converters;

import com.ebs.dac.dbo.types.LocalizedString;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 27, 2015 , 4:13:44 PM
 */
@Converter
public class LocalizedStringConverter implements AttributeConverter<LocalizedString, PGobject> {

  private static final Logger logger = LoggerFactory.getLogger(LocalizedStringConverter.class);

  private static final Type dataValueType = new TypeToken<HashMap<String, String>>() {}.getType();

  private static final Gson gson = new Gson();

  @Override
  public PGobject convertToDatabaseColumn(LocalizedString attribute) {
    PGobject dbData = new PGobject();
    dbData.setType("json");
    try {
      dbData.setValue(convertToString(attribute));
    } catch (SQLException e) {
      logger.error("", e);
    }
    return dbData;
  }

  @Override
  public LocalizedString convertToEntityAttribute(PGobject dbData) {
    if (dbData != null) return convertToLocalizedString(dbData.getValue());
    return null;
  }

  public String convertToString(LocalizedString attribute) {
    if (attribute != null && !attribute.isNull()) {
      return gson.toJson(attribute.getValues());
    }
    return null;
  }

  public LocalizedString convertToLocalizedString(String dbData) {
    Map<String, String> values;
    if (dbData != null && dbData.length() > 0) {
      values = gson.fromJson(dbData, dataValueType);
      return new LocalizedString(values);
    }
    return null;
  }
}
