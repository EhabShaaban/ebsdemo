package com.ebs.dac.dbo.jpa.entities.businessobjects;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class MasterDataView extends AggregateInformationObject {

  public static EnterpriseLevelCode GLOBAL_VIEW_CODE =
      new EnterpriseLevelCode(EnterpriseLevelCode.GLOBAL_LEVEL);

  private EnterpriseLevelCode enterpriseLevelCode;

  public MasterDataView() {
    this.enterpriseLevelCode = new EnterpriseLevelCode();
  }

  public MasterDataView(EnterpriseLevelCode code) {
    this.enterpriseLevelCode = code;
  }

  public EnterpriseLevelCode getEnterpriseLevelCode() {
    return enterpriseLevelCode;
  }

  public void setEnterpriseLevelCode(EnterpriseLevelCode enterpriseLevelCode) {
    this.enterpriseLevelCode = enterpriseLevelCode;
  }

  public boolean hasEnterpriseCode(EnterpriseLevelCode code) {
    return enterpriseLevelCode.equals(code);
  }

  public boolean isEmpty() {
    return enterpriseLevelCode.isEmpty();
  }

  public boolean isGlobal() {
    return enterpriseLevelCode.isGlobalLevel();
  }
}
