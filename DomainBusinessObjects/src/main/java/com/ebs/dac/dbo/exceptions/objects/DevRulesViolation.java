package com.ebs.dac.dbo.exceptions.objects;

/**
 * Indicate design problem or violation for a development guideline or rule Shouldn't be propagated
 * to client
 *
 * @author shaf3y
 */
public class DevRulesViolation extends RuntimeException {

  public DevRulesViolation() {}

  public DevRulesViolation(String message) {
    super(message);
  }

  public DevRulesViolation(Exception cause) {
    super(cause);
  }

  public DevRulesViolation(String message, Exception cause) {
    super(message, cause);
  }
}
