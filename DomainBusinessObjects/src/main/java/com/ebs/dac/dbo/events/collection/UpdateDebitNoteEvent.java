package com.ebs.dac.dbo.events.collection;

import com.ebs.dac.dbo.events.ActivateEvent;

public class UpdateDebitNoteEvent extends ActivateEvent {

  public UpdateDebitNoteEvent(String userCode) {
    super(userCode);
  }
}
