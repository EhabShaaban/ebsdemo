package com.ebs.dac.dbo.events;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;

public class ActivateEvent extends EBSEvent {

    private String userCode;
    private DocumentObject documentObject;

    public ActivateEvent(String userCode) {
        super();
        this.userCode = userCode;
    }

    public ActivateEvent(DocumentObject businessObject) {
        super();
        this.documentObject = businessObject;
        this.userCode = businessObject.getUserCode();
    }

  public String getUserCode() {
    return userCode;
  }

  public DocumentObject getDocumentObject() {
    return documentObject;
  }
}
