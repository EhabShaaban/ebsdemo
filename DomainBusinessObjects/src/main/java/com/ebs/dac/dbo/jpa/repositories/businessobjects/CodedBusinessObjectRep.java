package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import java.util.List;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface CodedBusinessObjectRep<T extends BusinessObject>
    extends BusinessObjectRep<T>, MasterBusinessObjectRep<T> {
  T findOneByUserCode(String userCode);

  List<T> findByUserCode(String userCode);

  boolean existsByUserCode(String userCode);
}
