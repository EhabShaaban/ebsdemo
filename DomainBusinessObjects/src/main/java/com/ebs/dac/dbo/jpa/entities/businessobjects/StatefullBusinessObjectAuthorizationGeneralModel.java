package com.ebs.dac.dbo.jpa.entities.businessobjects;

import javax.persistence.*;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

@MappedSuperclass
public abstract class StatefullBusinessObjectAuthorizationGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private String purchaseUnitCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;
  private String purchaseUnitNameEn;

  public StatefullBusinessObjectAuthorizationGeneralModel() {}

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }
}
