package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface DocumentObjectRep<T extends DocumentObject<UC>, UC extends UserCode>
    extends StatefullBusinessObjectRep<T, UC> {}
