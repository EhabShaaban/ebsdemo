package com.ebs.dac.dbo.events.sales;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;

public class SalesInvoicePostSuccessEvent extends SalesInvoicePostEvent {

  public SalesInvoicePostSuccessEvent(DObSalesInvoice salesInvoice) {
    super(salesInvoice);
  }
}
