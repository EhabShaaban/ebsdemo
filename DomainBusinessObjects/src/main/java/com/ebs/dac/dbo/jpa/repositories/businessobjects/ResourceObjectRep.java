package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.ResourceObject;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface ResourceObjectRep<T extends ResourceObject> extends BusinessObjectRep<T> {}
