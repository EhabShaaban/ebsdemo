package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 27, 2016 , 2:07:17 PM
 */
@NoRepositoryBean
public interface ConfigurationObjectRep<T extends ConfigurationObject<UC>, UC extends UserCode>
    extends StatefullBusinessObjectRep<T, UC> {}
