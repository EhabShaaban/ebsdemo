package com.ebs.dac.dbo.events.sales;

import com.ebs.dac.dbo.events.LockEvent;

public class SalesInvoiceUnlockRequestEvent extends LockEvent {

  public SalesInvoiceUnlockRequestEvent(String userCode) {
    super(userCode);
  }

}
