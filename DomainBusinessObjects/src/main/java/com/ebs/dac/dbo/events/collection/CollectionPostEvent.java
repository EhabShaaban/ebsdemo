package com.ebs.dac.dbo.events.collection;

import com.ebs.dac.dbo.events.ActivateEvent;

public class CollectionPostEvent extends ActivateEvent {

    public CollectionPostEvent(String userCode) {
        super(userCode);
    }

}
