package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.objects.IEntityPayloadContainer;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.types.EntityPayloadContainer;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** Created by marisoft on 3/16/17. */
public class AggregateBusinessObject
    implements IAggregateBusinessObject<InformationObject>, Serializable {

  private static final long serialVersionUID = 1L;

  private transient EntityPayloadContainer<InformationObject> payload;

  public AggregateBusinessObject() {
    payload = new EntityPayloadContainer<>();
  }

  @Override
  public IEntityPayloadContainer getPayload() {
    return payload;
  }

  @Override
  public <T extends InformationObject> T getSingleInstanceAttribute(
      ICompositeAttribute<T> attribute) {
    return payload.getSingleInstanceAttribute(attribute);
  }

  @Override
  public <T extends InformationObject> void putSingleInstanceAttribute(
      ICompositeAttribute<T> attribute, T attrValue) {
    payload.putSingleInstanceAttribute(attribute, attrValue);
  }

  public <T extends InformationObject> T removeSingleInstanceAttribute(
      ICompositeAttribute<T> attribute) {
    return payload.removeSingleInstanceAttribute(attribute);
  }

  @Override
  public <T extends InformationObject> List<T> getMultipleInstancesAttribute(
      ICompositeAttribute<T> attribute) {
    return payload.getMultipleInstancesAttribute(attribute);
  }

  @Override
  public <T extends InformationObject> boolean containsAttribute(ICompositeAttribute<T> attribute) {
    return payload.containsAttribute(attribute);
  }

  @Override
  public <T extends InformationObject> void putMultipleInstancesAttribute(
      ICompositeAttribute<T> attribute, List<T> attrValues) {
    payload.putMultipleInstancesAttribute(
        (ICompositeAttribute<InformationObject>) attribute, (List<InformationObject>) attrValues);
  }

  @Override
  public <T extends InformationObject> void addInstanceToMultipleInstancesAttribute(
      ICompositeAttribute<T> attribute, T instance) {
    payload.addInstanceToMultipleInstancesAttribute(attribute, instance);
  }

  @Override
  public <Detail extends InformationObject> void removeInstanceFromMultipleInstancesAttribute(
      ICompositeAttribute<Detail> attribute, Detail instance) {
    payload.getMultipleInstancesAttribute(attribute).remove(instance);
  }

  @Override
  public void clearAllPayload() {
    payload = new EntityPayloadContainer();
  }

  @Override
  public Map<ICompositeAttribute<? extends InformationObject>, ? extends InformationObject>
      getSingleAttributesPayload() {
    return payload.getSingleInstanceAttributesPayload();
  }

  @Override
  public Map<ICompositeAttribute<? extends InformationObject>, List<? extends InformationObject>>
      getMultipleAttributesPayload() {
    return payload.getMultipleInstancesAttributesPayload();
  }

  @Override
  public Set<ICompositeAttribute<? extends InformationObject>> getSingleInstanceAttributes() {
    return payload.getSingleInstanceAttributes();
  }

  @Override
  public Set<ICompositeAttribute<? extends InformationObject>> getMultipleInstancesAttributes() {
    return payload.getMultipleInstancesAttributes();
  }

  @Override
  public <T extends InformationObject> void copyPayload(
      IAggregateBusinessObject<InformationObject> source) {
    Set<ICompositeAttribute<? extends InformationObject>> singleInstanceAttrs =
        source.getSingleInstanceAttributes();
    for (ICompositeAttribute<? extends InformationObject> attr : singleInstanceAttrs) {
      putSingleInstanceAttribute(
          (ICompositeAttribute<T>) attr,
          source.getSingleInstanceAttribute((ICompositeAttribute<T>) attr));
    }

    Set<ICompositeAttribute<? extends InformationObject>> multipleInstancesAttrs =
        source.getMultipleInstancesAttributes();
    for (ICompositeAttribute<? extends InformationObject> attr : multipleInstancesAttrs) {
      putMultipleInstancesAttribute(
          (ICompositeAttribute<T>) attr,
          source.getMultipleInstancesAttribute((ICompositeAttribute<T>) attr));
    }
  }

  @Override
  public Set<ICompositeAttribute<? extends InformationObject>> getAllInstanceAttributes() {
    Set<ICompositeAttribute<? extends InformationObject>> attributes = new HashSet<>();
    attributes.addAll(this.payload.getSingleInstanceAttributes());
    attributes.addAll(this.payload.getMultipleInstancesAttributes());
    return attributes;
  }

  @Override
  public <Detail extends InformationObject> Object getInstanceAttribute(
      ICompositeAttribute<Detail> attribute) {
    if (attribute.isMultiple()) return getMultipleInstancesAttribute(attribute);
    return getSingleInstanceAttribute(attribute);
  }

  public boolean isNull() {
    return payload.isNull();
  }
}
