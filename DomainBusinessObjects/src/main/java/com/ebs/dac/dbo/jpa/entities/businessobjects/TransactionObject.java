package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class TransactionObject<UC extends UserCode> extends CodedBusinessObject<UC> {

    private static final long serialVersionUID = 1L;

}
