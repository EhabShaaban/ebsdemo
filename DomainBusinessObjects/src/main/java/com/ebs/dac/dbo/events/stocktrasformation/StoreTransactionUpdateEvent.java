package com.ebs.dac.dbo.events.stocktrasformation;

import com.ebs.dac.dbo.events.ActivateEvent;

public class StoreTransactionUpdateEvent extends ActivateEvent {
    public StoreTransactionUpdateEvent(String userCode) {
        super(userCode);
    }
}
