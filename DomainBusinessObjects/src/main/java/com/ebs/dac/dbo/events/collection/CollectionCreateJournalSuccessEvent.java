package com.ebs.dac.dbo.events.collection;

import com.ebs.dac.dbo.events.CreateEvent;

public class CollectionCreateJournalSuccessEvent extends CreateEvent {

  public CollectionCreateJournalSuccessEvent(String userCode) {
    super(userCode);
  }
}
