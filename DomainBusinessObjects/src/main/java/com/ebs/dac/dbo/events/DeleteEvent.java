package com.ebs.dac.dbo.events;

public class DeleteEvent extends EBSEvent {

  private String userCode;

  public DeleteEvent(String userCode) {
    super();
    this.userCode = userCode;
  }

  public String getPurchaseOrderCode() {
    return userCode;
  }
}
