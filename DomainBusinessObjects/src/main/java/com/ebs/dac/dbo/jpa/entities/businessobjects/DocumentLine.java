package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import java.util.Collection;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DocumentLine extends AggregateInformationObject {

  private static final long serialVersionUID = 1L;

  public <Header extends DocumentHeader> Header getHeaderDetail(
      ICompositeAttribute<Header> headerAttribute) throws Exception {
    return this.aggregateBusinessObject().getSingleInstanceAttribute(headerAttribute);
  }

  public <Line extends DocumentLine> Collection<Line> getAllLines(
      ICompositeAttribute<Line> lineAttribute) throws Exception {
    return this.aggregateBusinessObject().getMultipleInstancesAttribute(lineAttribute);
  }

  public <Header extends DocumentHeader> void setHeaderDetail(
      ICompositeAttribute<Header> headerAttribute, Header header) throws Exception {
    this.aggregateBusinessObject().putSingleInstanceAttribute(headerAttribute, header);
  }

  public <Line extends DocumentLine> void addLineDetail(
      ICompositeAttribute<Line> lineAttribute, Line line) throws Exception {
    this.aggregateBusinessObject().addInstanceToMultipleInstancesAttribute(lineAttribute, line);
  }
}
