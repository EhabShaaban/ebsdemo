package com.ebs.dac.dbo.jpa.converters;

import com.google.gson.Gson;
import java.util.Set;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class StatefullBusinessObjectCurrentStatesJpaConverter
    implements AttributeConverter<Set<String>, String> {

  private static final Gson gson = new Gson();

  @Override
  public String convertToDatabaseColumn(Set<String> strings) {
    String currentStatesDataVal = null;

    if (strings != null && !strings.isEmpty()) {
      currentStatesDataVal = gson.toJson(strings, Set.class);
    }

    return currentStatesDataVal;
  }

  @Override
  public Set<String> convertToEntityAttribute(String s) {
    Set<String> currentStatesObjVal = null;
    if (s != null) {
      currentStatesObjVal = gson.fromJson(s, Set.class);
    }
    return currentStatesObjVal;
  }
}
