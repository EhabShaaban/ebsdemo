package com.ebs.dac.dbo.api.objects;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface IAggregateBusinessObject<InformationObject> {

  IEntityPayloadContainer getPayload();

  <Detail extends InformationObject> Detail getSingleInstanceAttribute(
      ICompositeAttribute<Detail> attribute);

  <Detail extends InformationObject> void putSingleInstanceAttribute(
      ICompositeAttribute<Detail> attribute, Detail attrValue);

  <Detail extends InformationObject> List<Detail> getMultipleInstancesAttribute(
      ICompositeAttribute<Detail> attribute);

  <Detail extends InformationObject> void putMultipleInstancesAttribute(
      ICompositeAttribute<Detail> attribute, List<Detail> attrValues);

  <Detail extends InformationObject> void addInstanceToMultipleInstancesAttribute(
      ICompositeAttribute<Detail> attribute, Detail instance);

  <Detail extends InformationObject> void removeInstanceFromMultipleInstancesAttribute(
      ICompositeAttribute<Detail> attribute, Detail instance);

  <Detail extends InformationObject> boolean containsAttribute(
      ICompositeAttribute<Detail> attribute);

  void clearAllPayload();

  Set<ICompositeAttribute<? extends InformationObject>> getSingleInstanceAttributes();

  Map<ICompositeAttribute<? extends InformationObject>, ? extends InformationObject>
      getSingleAttributesPayload();

  Set<ICompositeAttribute<? extends InformationObject>> getMultipleInstancesAttributes();

  Map<ICompositeAttribute<? extends InformationObject>, List<? extends InformationObject>>
      getMultipleAttributesPayload();

  <Detail extends InformationObject> void copyPayload(
      IAggregateBusinessObject<InformationObject> source) throws Exception;

  Set<ICompositeAttribute<? extends InformationObject>> getAllInstanceAttributes();

  <Detail extends InformationObject> Object getInstanceAttribute(
      ICompositeAttribute<Detail> attribute) throws Exception;
}
