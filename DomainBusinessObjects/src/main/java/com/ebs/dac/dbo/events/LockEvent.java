package com.ebs.dac.dbo.events;

public class LockEvent extends EBSEvent {

  private String userCode;

  public LockEvent(String userCode) {
    super();
    this.userCode = userCode;
  }

  public String getUserCode() {
    return userCode;
  }
}
