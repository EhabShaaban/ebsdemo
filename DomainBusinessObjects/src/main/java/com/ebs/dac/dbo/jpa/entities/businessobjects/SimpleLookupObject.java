package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class SimpleLookupObject<UC extends UserCode> extends LookupObject<UC> {

  private static final long serialVersionUID = 1L;

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
