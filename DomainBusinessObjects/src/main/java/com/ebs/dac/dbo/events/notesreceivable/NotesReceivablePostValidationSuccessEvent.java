package com.ebs.dac.dbo.events.notesreceivable;

import com.ebs.dac.dbo.events.ActivateEvent;

public class NotesReceivablePostValidationSuccessEvent extends ActivateEvent {
    public NotesReceivablePostValidationSuccessEvent(String userCode) {
        super(userCode);
    }
}
