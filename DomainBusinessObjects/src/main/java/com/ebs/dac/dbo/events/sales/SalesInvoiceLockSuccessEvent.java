package com.ebs.dac.dbo.events.sales;

import com.ebs.dac.dbo.events.LockEvent;

public class SalesInvoiceLockSuccessEvent extends LockEvent {

  public SalesInvoiceLockSuccessEvent(String userCode) {
    super(userCode);
  }

}
