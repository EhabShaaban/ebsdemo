package com.ebs.dac.dbo.jpa.entities.businessobjects;

import java.io.Serializable;
import javax.persistence.Embeddable;

@Embeddable
public class EnterpriseLevelCode implements Serializable {

  public static Long GLOBAL_LEVEL = 0L;
  private Long enterpriseLevelCode;

  public EnterpriseLevelCode() {}

  public EnterpriseLevelCode(Long code) {
    this.enterpriseLevelCode = code;
  }

  public Long getValue() {
    return enterpriseLevelCode;
  }

  public void setValue(Long enterpriseLevelCode) {
    this.enterpriseLevelCode = enterpriseLevelCode;
  }

  public boolean equals(EnterpriseLevelCode code) {
    if (code == null) return false;
    return this.enterpriseLevelCode.equals(code.getValue());
  }

  public boolean isEmpty() {
    return enterpriseLevelCode == null;
  }

  public boolean isGlobalLevel() {
    if (this.enterpriseLevelCode == null) return false;
    return this.enterpriseLevelCode.equals(GLOBAL_LEVEL);
  }
}
