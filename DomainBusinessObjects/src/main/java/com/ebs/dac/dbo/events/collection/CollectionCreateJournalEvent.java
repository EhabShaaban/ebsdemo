package com.ebs.dac.dbo.events.collection;

import com.ebs.dac.dbo.events.CreateEvent;

public class CollectionCreateJournalEvent extends CreateEvent {

  public CollectionCreateJournalEvent(String userCode) {
    super(userCode);
  }
}
