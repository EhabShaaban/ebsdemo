package com.ebs.dac.dbo.events.sales;

import com.ebs.dac.dbo.events.LockEvent;

public class SalesInvoiceLockRequestEvent extends LockEvent {

  public SalesInvoiceLockRequestEvent(String userCode) {
    super(userCode);
  }

}
