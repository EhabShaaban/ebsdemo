package com.ebs.dac.dbo.events.sales;

public class SalesInvoiceJournalEntryCreatedSuccessEvent extends SalesInvoicePostEvent {

  public SalesInvoiceJournalEntryCreatedSuccessEvent(String userCode) {
    super(userCode);
  }
}
