package com.ebs.dac.dbo.api.objects;

/** Created by marisoft on 3/16/17. */
public interface IAggregateAwareEntity {
  <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject();
}
