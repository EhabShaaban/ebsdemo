package com.ebs.dac.dbo.api.processing;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 26, 2018 10:41:19 AM
 */
@Retention(RUNTIME)
@Target(TYPE)
public @interface EntityInterface {
  Class<?> value();
}
