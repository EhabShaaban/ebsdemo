package com.ebs.dac.dbo.events.initialbalanceupload;

import com.ebs.dac.dbo.events.ActivateEvent;

public class InitialBalanceUploadActivateEvent extends ActivateEvent {
    public InitialBalanceUploadActivateEvent(String userCode) {
        super(userCode);
    }
}
