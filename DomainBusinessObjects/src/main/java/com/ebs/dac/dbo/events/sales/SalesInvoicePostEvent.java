package com.ebs.dac.dbo.events.sales;

import com.ebs.dac.dbo.events.ActivateEvent;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;

public class SalesInvoicePostEvent extends ActivateEvent {

    public SalesInvoicePostEvent(String userCode) {
        super(userCode);
    }

    public SalesInvoicePostEvent(DObSalesInvoice salesInvoice) {
        super(salesInvoice);
    }

}
