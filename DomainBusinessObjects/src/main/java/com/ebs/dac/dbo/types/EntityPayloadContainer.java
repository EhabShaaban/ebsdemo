package com.ebs.dac.dbo.types;

import com.ebs.dac.dbo.api.objects.IEntityPayloadContainer;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** @author shaf3y */
public class EntityPayloadContainer<BusinessObject>
    implements IEntityPayloadContainer<BusinessObject> {

  /** Map to hold values of {@link ICompositeAttribute}s that their values are single instance. */
  private final Map<ICompositeAttribute<? extends BusinessObject>, BusinessObject>
      singleInstanceAttrMap;

  /**
   * Map to hold values of {@link ICompositeAttribute}s that their values are multiple instances.
   */
  private final Map<ICompositeAttribute<? extends BusinessObject>, List<? extends BusinessObject>>
      multipleInstancesAttrMap;

  public EntityPayloadContainer() {
    singleInstanceAttrMap = new HashMap<>();
    multipleInstancesAttrMap = new HashMap<>();
  }

  @Override
  public <DetailBusinessObject> void putSingleInstanceAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute, DetailBusinessObject value) {
    if (attribute == null) throw new IllegalArgumentException("Invalid Attribute: [NULL]");

    if (attribute.isMultiple())
      throw new IllegalArgumentException(
          "Invalid Attribute: Expected single instance attribute but the incoming is multiple");

    getSingleInstanceAttributesPayload()
        .put((ICompositeAttribute<? extends BusinessObject>) attribute, (BusinessObject) value);
  }

  public <DetailBusinessObject> DetailBusinessObject removeSingleInstanceAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute) {
    if (attribute == null) throw new IllegalArgumentException("Invalid Attribute: [NULL]");

    if (attribute.isMultiple())
      throw new IllegalArgumentException(
          "Invalid Attribute: Expected single instance attribute but the incoming is multiple");

    return (DetailBusinessObject) (getSingleInstanceAttributesPayload().remove(attribute));
  }

  @Override
  public <DetailBusinessObject> DetailBusinessObject getSingleInstanceAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute) {
    if (attribute == null) throw new IllegalArgumentException("Invalid Attribute: [NULL]");
    DetailBusinessObject value = null;
    Object valueObj = getSingleInstanceAttributesPayload().get(attribute);
    if (valueObj != null) {
      Class<DetailBusinessObject> attrType = attribute.getAttributeType();
      try {
        value = attrType.cast(valueObj);
      } catch (ClassCastException e) {
        String message =
            "Invalid Attribute Data Type: "
                + "Expected ["
                + attrType
                + "] but "
                + "the actual types is ["
                + valueObj.getClass()
                + "]";
        ClassCastException classCastException = new ClassCastException(message);
        classCastException.initCause(e);
        throw classCastException;
      }
    }
    return value;
  }

  @Override
  public <DetailBusinessObject> void putMultipleInstancesAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute, List<DetailBusinessObject> value) {
    if (attribute == null) {
      throw new IllegalArgumentException("Invalid Attribute: [NULL]");
    }

    if (!attribute.isMultiple()) {
      throw new IllegalArgumentException(
          "Invalid Attribute: Expected multiple instances attribute but the incoming is single");
    }

    getMultipleInstancesAttributesPayload()
        .put(
            (ICompositeAttribute<? extends BusinessObject>) attribute,
            (List<? extends BusinessObject>) value);
  }

  @Override
  public <DetailBusinessObject> void addInstanceToMultipleInstancesAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute, DetailBusinessObject instance) {
    if (attribute == null || instance == null) {
      throw new IllegalArgumentException("Invalid attribute or instance");
    }

    if (attribute.getAttributeType() == null) {
      throw new IllegalArgumentException("Invalid Composite Attribute Type");
    }

    if (!attribute.isMultiple()) {
      throw new IllegalArgumentException("Invalid Composite Attribute : expected is multiple");
    }

    List<DetailBusinessObject> attributeInstances =
        (List<DetailBusinessObject>) getMultipleInstancesAttributesPayload().get(attribute);

    if (attributeInstances == null) {
      attributeInstances = new ArrayList<>();
    }

    attributeInstances.add(instance);
    getMultipleInstancesAttributesPayload()
        .put(
            (ICompositeAttribute<? extends BusinessObject>) attribute,
            (List<? extends BusinessObject>) attributeInstances);
  }

  @Override
  public <DetailBusinessObject> List<DetailBusinessObject> getMultipleInstancesAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute) {
    if (attribute == null) throw new IllegalArgumentException("Invalid Attribute: [NULL]");
    List<DetailBusinessObject> value = new ArrayList<>();
    Object valueObj = getMultipleInstancesAttributesPayload().get(attribute);
    if (valueObj != null) {
      value.addAll((List<DetailBusinessObject>) valueObj);
    }
    return value;
  }

  @Override
  public <DetailBusinessObject> boolean containsAttribute(
      ICompositeAttribute<DetailBusinessObject> attribute) {
    return (getSingleInstanceAttributesPayload().containsKey(attribute)
        || getMultipleInstancesAttributesPayload().containsKey(attribute));
  }

  @Override
  public void clearAllPayload() {
    getSingleInstanceAttributesPayload().clear();
    getMultipleInstancesAttributesPayload().clear();
  }

  @Override
  public Map<ICompositeAttribute<? extends BusinessObject>, BusinessObject>
      getSingleInstanceAttributesPayload() {
    return singleInstanceAttrMap;
  }

  @Override
  public Map<ICompositeAttribute<? extends BusinessObject>, List<? extends BusinessObject>>
      getMultipleInstancesAttributesPayload() {
    return multipleInstancesAttrMap;
  }

  @Override
  public Set<ICompositeAttribute<? extends BusinessObject>> getSingleInstanceAttributes() {
    return getSingleInstanceAttributesPayload().keySet();
  }

  @Override
  public Set<ICompositeAttribute<? extends BusinessObject>> getMultipleInstancesAttributes() {
    return getMultipleInstancesAttributesPayload().keySet();
  }

  public boolean isNull() {
    return singleInstanceAttrMap.isEmpty() && multipleInstancesAttrMap.isEmpty();
  }
}
