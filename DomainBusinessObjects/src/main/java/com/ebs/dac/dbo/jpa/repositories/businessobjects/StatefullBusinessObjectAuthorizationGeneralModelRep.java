package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObjectAuthorizationGeneralModel;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface StatefullBusinessObjectAuthorizationGeneralModelRep<T extends StatefullBusinessObjectAuthorizationGeneralModel>
				extends StatefullBusinessObjectRep<T, DefaultUserCode> {
	T findOneByUserCode(String userCode);

}
