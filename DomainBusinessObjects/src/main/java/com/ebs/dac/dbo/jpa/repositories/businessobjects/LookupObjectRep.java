package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface LookupObjectRep<T extends LookupObject<UC>, UC extends UserCode>
    extends CodedBusinessObjectRep<T> {
}
