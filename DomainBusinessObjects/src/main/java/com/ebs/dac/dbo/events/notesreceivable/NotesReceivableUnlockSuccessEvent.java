package com.ebs.dac.dbo.events.notesreceivable;

import com.ebs.dac.dbo.events.LockEvent;

public class NotesReceivableUnlockSuccessEvent extends LockEvent {

  public NotesReceivableUnlockSuccessEvent(String userCode) {
    super(userCode);
  }

}
