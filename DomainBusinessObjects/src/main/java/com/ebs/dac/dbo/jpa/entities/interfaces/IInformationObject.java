package com.ebs.dac.dbo.jpa.entities.interfaces;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 27, 2018 2:24:02 PM
 */
public interface IInformationObject{

  String REF_INSTANCE_ID_NAME = "refInstanceId";
}
