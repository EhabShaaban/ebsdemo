package com.ebs.dac.dbo.events.initialstockupload;

import com.ebs.dac.dbo.events.ActivateEvent;

public class InitialStockUploadActivateEvent extends ActivateEvent {

    private Long goodsIssueId;

    public InitialStockUploadActivateEvent(String userCode, Long goodsIssueId) {
        super(userCode);
        this.goodsIssueId = goodsIssueId;
    }

    public Long getGoodsIssueId() {
        return goodsIssueId;
    }
}
