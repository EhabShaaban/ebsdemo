package com.ebs.dac.dbo.events.sales;

import com.ebs.dac.dbo.events.EBSEvent;

public class SalesInvoiceUpdateStateSuccesEvent extends EBSEvent {

    private String userCode;

    public SalesInvoiceUpdateStateSuccesEvent(String userCode) {
        super();
        this.userCode = userCode;
    }

    public String getUserCode() {
        return userCode;
    }
}
