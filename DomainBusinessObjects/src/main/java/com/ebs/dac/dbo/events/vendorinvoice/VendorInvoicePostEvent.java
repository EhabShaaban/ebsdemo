package com.ebs.dac.dbo.events.vendorinvoice;

import com.ebs.dac.dbo.events.ActivateEvent;

public class VendorInvoicePostEvent extends ActivateEvent {

    public VendorInvoicePostEvent(String userCode) {
        super(userCode);
    }

}
