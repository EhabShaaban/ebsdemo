package com.ebs.dac.dbo.events;

public class CreateEvent extends EBSEvent {

  private String userCode;

  public CreateEvent(String userCode) {
    super();
    this.userCode = userCode;
  }

  public String getUserCode() {
    return userCode;
  }
}
