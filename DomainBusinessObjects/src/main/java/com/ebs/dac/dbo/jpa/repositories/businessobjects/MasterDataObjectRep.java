package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.MasterDataObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface MasterDataObjectRep<T extends MasterDataObject<UC>, UC extends UserCode>
    extends StatefullBusinessObjectRep<T, UC> {}
