package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface GeneralModelRepository<T, ID extends Serializable>
    extends PagingAndSortingRepository<T, ID> {

  List<T> findAllByOrderByIdAsc();
}
