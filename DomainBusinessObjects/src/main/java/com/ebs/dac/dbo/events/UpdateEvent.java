package com.ebs.dac.dbo.events;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;

public class UpdateEvent extends EBSEvent {

  private String userCode;
  private DocumentObject documentObject;

  public UpdateEvent(String userCode) {
    super();
    this.userCode = userCode;
  }

  public UpdateEvent(DocumentObject documentObject) {
    super();
    this.userCode = userCode;
    this.documentObject = documentObject;
  }

  public String getUserCode() {
    return userCode;
  }

  public DocumentObject getDocumentObject() {
    return documentObject;
  }
}
