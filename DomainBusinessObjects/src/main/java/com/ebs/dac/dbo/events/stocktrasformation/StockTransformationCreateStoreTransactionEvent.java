package com.ebs.dac.dbo.events.stocktrasformation;

import com.ebs.dac.dbo.events.ActivateEvent;

public class StockTransformationCreateStoreTransactionEvent extends ActivateEvent {
    public StockTransformationCreateStoreTransactionEvent(String userCode) {
        super(userCode);
    }
}
