package com.ebs.dac.dbo.jpa.entities.usercodes;

import java.io.Serializable;

public abstract class UserCode implements Serializable {

  public static final String CODE_ATT_NAME = "code";
  private static final long serialVersionUID = 1L;
  private String code;

  public UserCode() {}

  public UserCode(String unFormattedCode) {
    setCode(unFormattedCode);
  }

  protected abstract String formatCode(String unFormattedCode);

  public abstract String getFormattedUserCode();

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = formatCode(code);
  }
}
