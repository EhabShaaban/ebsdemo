package com.ebs.dac.dbo.jpa.entities.usercodes;

public class DefaultUserCode extends UserCode {

  private static final long serialVersionUID = 1L;

  public DefaultUserCode() {}

  public DefaultUserCode(String unFormattedCode) {
    super(unFormattedCode);
  }

  @Override
  protected String formatCode(String unFormattedCode) {
    return unFormattedCode;
  }

  @Override
  public String getFormattedUserCode() {
    return getCode();
  }
}
