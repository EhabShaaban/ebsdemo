package com.ebs.dac.dbo.events.collection;

import com.ebs.dac.dbo.events.ActivateEvent;

public class CollectionUpdateRefDocumentSuccessEvent extends ActivateEvent {

    public CollectionUpdateRefDocumentSuccessEvent(String userCode) {
        super(userCode);
    }
}
