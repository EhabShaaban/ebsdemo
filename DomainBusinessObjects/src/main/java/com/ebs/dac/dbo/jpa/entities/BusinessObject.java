package com.ebs.dac.dbo.jpa.entities;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;
import com.ebs.dac.dbo.types.BusinessEnum;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@MappedSuperclass
public abstract class BusinessObject implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime creationDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column()
  private DateTime modifiedDate;

  @Column(updatable = false)
  private String creationInfo;

  @Column private String modificationInfo;

  private static boolean isNotEmptyField(BusinessObject instance, Field field) {
    return (extractAttributeValue(instance, field) != null);
  }

  private static boolean isNotRefInstanceId(Field field) {
    return !Objects.equals(field.getName(), IInformationObject.REF_INSTANCE_ID_NAME);
  }

  private static boolean isAggregateAndNotEmpty(BusinessObject instance, Field field) {
    if (isAggregate(field)) {
      AggregateBusinessObject attributeValue =
          (AggregateBusinessObject) extractAttributeValue(instance, field);
      return attributeValue != null && !attributeValue.isNull();
    }
    return false;
  }

  private static boolean isAggregate(Field field) {
    return AggregateBusinessObject.class.isAssignableFrom(field.getType());
  }

  private static boolean isLocalizedStringAndNotEmpty(BusinessObject instance, Field field) {
    if (isLocalizedString(field)) {
      LocalizedString attributeValue = (LocalizedString) extractAttributeValue(instance, field);
      return attributeValue != null && !attributeValue.isNull();
    }
    return false;
  }

  protected static boolean isLocalizedString(Field field) {
    return LocalizedString.class.isAssignableFrom(field.getType());
  }

  private static boolean isBusinessEnumAndNotEmpty(BusinessObject instance, Field field) {
    if (isBusinessEnum(field)) {
      BusinessEnum attributeValue = (BusinessEnum) extractAttributeValue(instance, field);
      return attributeValue != null && !attributeValue.isNull();
    }
    return false;
  }

  private static boolean isBusinessEnum(Field field) {
    return BusinessEnum.class.isAssignableFrom(field.getType());
  }

  private static Object extractAttributeValue(BusinessObject instance, Field field) {
    field.setAccessible(true); // remove field secure accessibility to read its value
    try {
      return field.get(instance);
    } catch (IllegalAccessException e) {

    } finally {
      field.setAccessible(false); // recover field secure accessibility
    }
    return null;
  }

  private static List<Field> getNotEmptyModelFields(BusinessObject instance, Class aClass) {
    List<Field> fields = new ArrayList<>();
    if (aClass == null) return fields;
    fields.addAll(getInstanceFieldsOnly(instance, aClass));
    fields.addAll(getNotEmptyModelFields(instance, aClass.getSuperclass()));
    return fields;
  }

  private static List<Field> getInstanceFieldsOnly(BusinessObject instance, Class aClass) {
    return Arrays.stream(aClass.getDeclaredFields())
        .filter(fiter -> (!Modifier.isStatic(fiter.getModifiers())))
        .filter(fiter -> isNotEmptyField(instance, fiter))
        .filter(BusinessObject::isNotRefInstanceId)
        .filter(fiter -> (!isAggregate(fiter) || isAggregateAndNotEmpty(instance, fiter)))
        .filter(
            fiter -> (!isLocalizedString(fiter) || isLocalizedStringAndNotEmpty(instance, fiter)))
        .filter(fiter -> (!isBusinessEnum(fiter) || isBusinessEnumAndNotEmpty(instance, fiter)))
        .collect(Collectors.toList());
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public DateTime getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(DateTime creationDate) {
    this.creationDate = creationDate;
  }

  public DateTime getModifiedDate() {
    return modifiedDate;
  }

  public void setModifiedDate(DateTime modifiedDate) {
    this.modifiedDate = modifiedDate;
  }

  public String getCreationInfo() {
    return creationInfo;
  }

  public void setCreationInfo(String creationInfo) {
    this.creationInfo = creationInfo;
  }

  public String getModificationInfo() {
    return modificationInfo;
  }

  public void setModificationInfo(String modificationInfo) {
    this.modificationInfo = modificationInfo;
  }

  public abstract String getSysName();

  public boolean isNew() throws Exception {
    return !isNull() && readyToCreate();
  }

  public boolean readyToUpdate() throws Exception {
    return !isNull() && !readyToCreate();
  }

  public boolean isNull() throws Exception {
    List<Field> notEmptyModelFields = getNotEmptyModelFields(this, getClass());
    return notEmptyModelFields.isEmpty();
  }

  private boolean readyToCreate() {
    return !(id != null && id > 0);
  }

  public String toString() {
    StringBuilder builder = new StringBuilder();
    List<Field> notEmptyModelFields = getNotEmptyModelFields(this, getClass());
    builder.append("{ ");
    for(Field field: notEmptyModelFields){
      builder.append(field.getName())
              .append(" : ")
              .append(extractAttributeValue(this,field));
    }
    builder.append(" }");
    return builder.toString();
  }
}
