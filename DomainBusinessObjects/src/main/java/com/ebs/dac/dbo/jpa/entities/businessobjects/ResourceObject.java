package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IResourceObject;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@EntityInterface(IResourceObject.class)
public abstract class ResourceObject extends BusinessObject {

  private static final long serialVersionUID = 1L;

  @Column(length = 512)
  private String name;

  @Column(length = 512)
  private String format;

  @Lob
  @Basic(fetch = FetchType.LAZY)
  private byte[] content;

  public byte[] getContent() {
    return content;
  }

  public void setContent(byte[] content) {
    this.content = content;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }
}
