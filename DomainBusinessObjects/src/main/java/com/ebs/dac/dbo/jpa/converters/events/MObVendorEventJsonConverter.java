package com.ebs.dac.dbo.jpa.converters.events;

import com.ebs.dda.jpa.masterdata.vendor.IObVendorPurchaseUnitValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorCreationValueObject;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

@Converter
public class MObVendorEventJsonConverter implements AttributeConverter<MObVendorCreationValueObject, PGobject> {

  public static final String VENDOR_NAME = "vendorName";
  public static final String PURCHASE_UNITS = "purchaseUnits";
  public static final String PURCHASE_RESPONSIBLE_CODE = "purchaseResponsibleCode";
  public static final String ACCOUNT_WITH_VENDOR = "accountWithVendor";
  public static final String OLD_VENDOR_REFERENCE = "oldVendorReference";
  public static final String PURCHASEUNIT_CODE = "purchaseUnitCode";

  private static final Logger logger = LoggerFactory
      .getLogger(com.ebs.dac.dbo.jpa.converters.events.MObVendorEventJsonConverter.class);

  private static final Type dataValueType = new TypeToken<HashMap<String, String>>() {}.getType();

  private static final Gson gson = new Gson();

  @Override
  public PGobject convertToDatabaseColumn(MObVendorCreationValueObject attribute) {
    PGobject dbData = initPGObject();
    try {
      dbData.setValue(convertToString(attribute));
      return dbData;
    } catch (SQLException e) {
      logger.error("", e);
    }
    return dbData;
  }


  @Override
  public MObVendorCreationValueObject convertToEntityAttribute(PGobject dbData) {
    return new MObVendorCreationValueObject();
  }

  public String convertToString(MObVendorCreationValueObject attribute) {
    return gson.toJson(prepareVendorJsonObject(attribute));
  }



  private JsonObject prepareVendorJsonObject(MObVendorCreationValueObject attribute) {

    JsonObject vendorObject = new JsonObject();

    vendorObject.addProperty(VENDOR_NAME, attribute.getVendorName());
    vendorObject.add(PURCHASE_UNITS, getPurchaseUnitsJsonAttribute(attribute.getPurchaseUnits()));
    vendorObject.addProperty(PURCHASE_RESPONSIBLE_CODE, attribute.getPurchaseResponsibleCode());
    vendorObject.addProperty(OLD_VENDOR_REFERENCE, attribute.getOldVendorReference());
    vendorObject.addProperty(ACCOUNT_WITH_VENDOR, attribute.getAccountWithVendor());

    return vendorObject;
  }

  private JsonArray getPurchaseUnitsJsonAttribute(
      List<IObVendorPurchaseUnitValueObject> purchaseUnitData) {
    return preparePurchaseUnitsJsonArray(purchaseUnitData);
  }

  private JsonArray preparePurchaseUnitsJsonArray(List<IObVendorPurchaseUnitValueObject> purchaseUnitCodes) {
    JsonArray vendorPurchaseUnits = new JsonArray();
    for (IObVendorPurchaseUnitValueObject purchaseUnitCode : purchaseUnitCodes) {
      JsonObject vendorPurchaseUnit = new JsonObject();
      vendorPurchaseUnit.addProperty(PURCHASEUNIT_CODE, purchaseUnitCode.getPurchaseUnitCode());
      vendorPurchaseUnits.add(vendorPurchaseUnit);
    }
    return vendorPurchaseUnits;
  }


  private PGobject initPGObject() {
    PGobject dbData = new PGobject();
    try {
      dbData.setType("json");
      dbData.setValue("{}");
    } catch (SQLException e) {
      logger.error("", e);
    }
    return dbData;
  }
}

