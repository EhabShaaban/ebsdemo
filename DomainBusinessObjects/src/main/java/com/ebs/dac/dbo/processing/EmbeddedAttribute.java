package com.ebs.dac.dbo.processing;

import com.ebs.dac.dbo.api.processing.IEmbeddedAttribute;

public class EmbeddedAttribute<T> extends EntityAttribute implements IEmbeddedAttribute<T> {

  private Class<T> type;

  public EmbeddedAttribute(String name, Class<T> type) {
    super(name);
    this.type = type;
  }

  public Class<T> getType() {
    return type;
  }
}
