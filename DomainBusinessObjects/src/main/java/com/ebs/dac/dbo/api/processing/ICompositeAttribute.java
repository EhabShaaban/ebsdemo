package com.ebs.dac.dbo.api.processing;

/** Created by marisoft on 10/5/16. */
public interface ICompositeAttribute<T> extends IEntityAttribute {

  Class<T> getAttributeType();

  boolean isMultiple();

  void setMultiple(boolean multiple);
}
