package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import javax.persistence.MappedSuperclass;

/**
 * @author Yara Ameen
 * @since May 30, 2017 11:33:45 AM
 */
@MappedSuperclass
public class AggregateInformationObject extends InformationObject implements IAggregateAwareEntity {

  private static final long serialVersionUID = 1L;

  private transient AggregateBusinessObject aggregateBusinessObject;

  public AggregateInformationObject() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  @Override
  public AggregateBusinessObject aggregateBusinessObject() {
    return aggregateBusinessObject;
  }

  @Override
  public String getSysName() {
    return "";
  }
}
