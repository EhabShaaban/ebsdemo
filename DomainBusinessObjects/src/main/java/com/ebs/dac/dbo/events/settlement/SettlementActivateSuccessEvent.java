package com.ebs.dac.dbo.events.settlement;

import com.ebs.dac.dbo.events.ActivateEvent;

public class SettlementActivateSuccessEvent extends ActivateEvent {
	public SettlementActivateSuccessEvent(String userCode) {
		super(userCode);
	}
}
