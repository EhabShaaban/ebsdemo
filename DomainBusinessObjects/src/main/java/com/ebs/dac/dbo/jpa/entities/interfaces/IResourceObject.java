package com.ebs.dac.dbo.jpa.entities.interfaces;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IResourceObject{

  String NAME_CODE_ATTR_NAME = "name";
  BasicAttribute name = new BasicAttribute( NAME_CODE_ATTR_NAME);

  String FORMAT_ATTR_NAME = "format";
  BasicAttribute format = new BasicAttribute( FORMAT_ATTR_NAME);

  String CONTENT_ATTR_NAME = "content";
  BasicAttribute content = new BasicAttribute( CONTENT_ATTR_NAME);
}
