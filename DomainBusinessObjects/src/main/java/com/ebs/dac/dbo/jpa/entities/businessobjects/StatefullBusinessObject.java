package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import java.util.Set;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class StatefullBusinessObject<UC extends UserCode> extends CodedBusinessObject<UC> implements
    IStatefullBusinessObject {

  private static final long serialVersionUID = 7033540286313247494L;

  @Convert(converter = StatefullBusinessObjectCurrentStatesJpaConverter.class)
  private Set<String> currentStates;

  public StatefullBusinessObject() {}

  public Set<String> getCurrentStates() {
    return currentStates;
  }

  public void setCurrentStates(Set<String> currentStates) {
    this.currentStates = currentStates;
  }

  @Override
  public String getCurrentState() {
    if (!currentStates.isEmpty()) {
      return currentStates.iterator().next();
    }
    return "";
  }
}
