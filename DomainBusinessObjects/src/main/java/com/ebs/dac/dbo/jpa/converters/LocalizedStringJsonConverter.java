package com.ebs.dac.dbo.jpa.converters;

import com.ebs.dac.dbo.types.LocalizedString;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 27, 2015 , 4:13:44 PM
 */
@Converter
public class LocalizedStringJsonConverter implements AttributeConverter<LocalizedString, PGobject> {

  private static final Logger logger = LoggerFactory.getLogger(LocalizedStringJsonConverter.class);

  private static final Type dataValueType = new TypeToken<HashMap<String, String>>() {}.getType();

  private static final Gson gson = new Gson();

  @Override
  public PGobject convertToDatabaseColumn(LocalizedString attribute) {
    PGobject dbData = initPGObject();
    try {
      if (!attribute.isNull()) {
        dbData.setValue(convertToString(attribute));
      }
      return dbData;
    } catch (SQLException e) {
      logger.error("", e);
    }
    return dbData;
  }

  public LocalizedString convertToLocalizedString(String dbData) {
    Map<String, String> values;
    if (dbData != null && dbData.length() > 0) {
      values = gson.fromJson(dbData, dataValueType);
      return new LocalizedString(values);
    }
    return new LocalizedString();
  }

  @Override
  public LocalizedString convertToEntityAttribute(PGobject dbData) {
    if (dbData != null) return convertToLocalizedString(dbData.getValue());
    return new LocalizedString();
  }

  public String convertToString(LocalizedString attribute) {
    return gson.toJson(attribute.getValues());
  }

  private PGobject initPGObject() {
    PGobject dbData = new PGobject();
    try {
      dbData.setType("json");
      dbData.setValue("{}");
    } catch (SQLException e) {
      logger.error("", e);
    }
    return dbData;
  }
}
