package com.ebs.dac.dbo.events.notesreceivable;

import com.ebs.dac.dbo.events.ActivateEvent;

public class NotesReceivablePostSuccessEvent extends ActivateEvent {

    public NotesReceivablePostSuccessEvent(String userCode) {
        super(userCode);
    }
}
