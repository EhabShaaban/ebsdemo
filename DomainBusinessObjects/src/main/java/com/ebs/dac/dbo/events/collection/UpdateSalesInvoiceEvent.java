package com.ebs.dac.dbo.events.collection;

import com.ebs.dac.dbo.events.ActivateEvent;

public class UpdateSalesInvoiceEvent extends ActivateEvent {

    public UpdateSalesInvoiceEvent(String userCode) {
        super(userCode);
    }
}
