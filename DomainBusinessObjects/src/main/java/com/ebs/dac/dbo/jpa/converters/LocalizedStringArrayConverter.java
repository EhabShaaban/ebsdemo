package com.ebs.dac.dbo.jpa.converters;

import com.ebs.dac.dbo.types.LocalizedString;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Converter
public class LocalizedStringArrayConverter
    implements AttributeConverter<List<LocalizedString>, PGobject> {

  private static final Logger logger = LoggerFactory.getLogger(LocalizedStringArrayConverter.class);

  private static final Type dataValueType =
      new TypeToken<List<HashMap<String, String>>>() {}.getType();

  private static final Gson gson = new Gson();

  @Override
  public List<LocalizedString> convertToEntityAttribute(PGobject dbData) {
    if (dbData != null) return convertToLocalizedStringArray(dbData.getValue());
    return new ArrayList<>();
  }

  public List<LocalizedString> convertToLocalizedStringArray(String dbData) {
    List<LocalizedString> valuesList = new ArrayList<>();
    if (dbData != null && dbData.length() > 0) {
      List<Map<String, String>> values = gson.fromJson(dbData, dataValueType);
      for (Map<String, String> value : values) {
        valuesList.add(new LocalizedString(value));
      }
      return valuesList;
    }
    return new ArrayList<>();
  }

  @Override
  public PGobject convertToDatabaseColumn(List<LocalizedString> attribute) {
    PGobject dbData = new PGobject();
    dbData.setType("json");
    try {
      dbData.setValue(convertToString(attribute));
    } catch (SQLException e) {
      logger.error("", e);
    }
    return dbData;
  }

  public String convertToString(List<LocalizedString> attribute) {
    if (attribute != null && !attribute.isEmpty()) {
      return gson.toJson(attribute);
    }
    return null;
  }
}
