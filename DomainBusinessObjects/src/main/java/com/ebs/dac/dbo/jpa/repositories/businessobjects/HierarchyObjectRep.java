package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.HierarchyObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface HierarchyObjectRep<T extends HierarchyObject<UC>, UC extends UserCode>
    extends ConfigurationObjectRep<T, UC> {}
