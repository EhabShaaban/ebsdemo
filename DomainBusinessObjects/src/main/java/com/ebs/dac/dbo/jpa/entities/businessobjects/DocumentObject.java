package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import java.util.Collection;
import java.util.List;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DocumentObject<U extends UserCode> extends StatefullBusinessObject<U>
    implements IAggregateAwareEntity {

  private static final long serialVersionUID = 879540205955638120L;
  private transient AggregateBusinessObject documentDetails;
  private transient String documentType;
  private transient String document;

  public DocumentObject() {
    this.documentDetails = new AggregateBusinessObject();
  }

  public String getDocumentType() {
    return documentType;
  }

  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }

  public String getDocument() {
    return document;
  }

  public void setDocument(String document) {
    this.document = document;
  }

  @Override
  public AggregateBusinessObject aggregateBusinessObject() {
    return this.documentDetails;
  }

  public <Header extends DocumentHeader> void setHeaderDetail(
      ICompositeAttribute<Header> headerAttribute, Header header) {
    this.aggregateBusinessObject().putSingleInstanceAttribute(headerAttribute, header);
  }

  public <Header extends DocumentHeader> Header getHeaderDetail(
      ICompositeAttribute<Header> headerAttribute) {
    return this.aggregateBusinessObject().getSingleInstanceAttribute(headerAttribute);
  }

  public <Line extends DocumentLine> void addLineDetail(
      ICompositeAttribute<Line> lineAttribute, Line line) {
    this.aggregateBusinessObject().addInstanceToMultipleInstancesAttribute(lineAttribute, line);
  }

  public <Line extends DocumentLine> void setLinesDetails(
      ICompositeAttribute<Line> lineAttribute, List<Line> lines) {
    this.aggregateBusinessObject().putMultipleInstancesAttribute(lineAttribute, lines);
  }

  public <Line extends DocumentLine> Collection<Line> getAllLines(
      ICompositeAttribute<Line> lineAttribute) {
    return this.aggregateBusinessObject().getMultipleInstancesAttribute(lineAttribute);
  }

  public void removeLineDetail(ICompositeAttribute<DocumentLine> lineAttribute, DocumentLine line) {
    this.aggregateBusinessObject()
        .removeInstanceFromMultipleInstancesAttribute(lineAttribute, line);
  }

  public void removeHeaderDetail(ICompositeAttribute<DocumentHeader> headerAttribute) {
    this.aggregateBusinessObject().removeSingleInstanceAttribute(headerAttribute);
  }
}
