package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.infrastructure.jpa.repositories.IBusinessObjectRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface BusinessObjectRep<T extends BusinessObject> extends IBusinessObjectRepository<T> {

  List<T> findByCreationInfo(String creationInfo);

  List<T> findByIdGreaterThan(Long id, Pageable page);

  List<T> findByIdLessThan(Long id, Pageable page);

  List<T> findAllByOrderByIdAsc();

  List<T> findAllByOrderByIdDesc();

  Page<T> findAllByOrderByIdAsc(Pageable page);

  Page<T> findAllByOrderByIdDesc(Pageable page);


  long count(); // total number of records

  long countByIdGreaterThan(Long id);
}
