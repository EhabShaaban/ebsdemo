package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import java.util.List;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface InformationObjectRep<T extends InformationObject> extends BusinessObjectRep<T> {
  List<T> findByRefInstanceId(Long refInstanceId);
  T findOneByRefInstanceId(Long refInstanceId);
  T findOneById(Long id);
}
