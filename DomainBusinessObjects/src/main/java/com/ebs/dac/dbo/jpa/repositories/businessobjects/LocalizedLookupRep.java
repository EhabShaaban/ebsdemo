package com.ebs.dac.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * @author Menna Sayed
 * @since Dec 29, 2015, 1:46:05 PM
 */
@NoRepositoryBean
public interface LocalizedLookupRep<T extends LookupObject<UC>, UC extends UserCode>
    extends LookupObjectRep<T, UC> {}
