package com.ebs.dac.dbo.events.notesreceivable;

import com.ebs.dac.dbo.events.ActivateEvent;

public class NotesReceivableRequestPostEvent extends ActivateEvent {
    public NotesReceivableRequestPostEvent(String userCode) {
        super(userCode);
    }
}
