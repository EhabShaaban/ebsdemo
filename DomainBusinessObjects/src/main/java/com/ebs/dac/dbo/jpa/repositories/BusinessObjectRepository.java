package com.ebs.dac.dbo.jpa.repositories;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.infrastructure.jpa.repositories.IBusinessObjectRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public class BusinessObjectRepository<T extends BusinessObject>
    extends SimpleJpaRepository<T, Long> // NOPMD
    implements IBusinessObjectRepository<T>, PagingAndSortingRepository<T, Long> { // NOPMD
  private final EntityManager em;

  public BusinessObjectRepository(Class<T> domainClass, EntityManager em) {
    super(domainClass, em);
    this.em = em;
  }

  public BusinessObjectRepository(
      JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) {
    super(entityInformation, entityManager);
    this.em = entityManager;
  }

  public EntityManager getEntityManager() {
    return this.em;
  }

  public void detach(T obj) {
    if (obj != null && this.em.contains(obj)) {
      this.em.detach(obj);
    }
  }

  public T create(T obj) throws Exception {
    detachBusinessObjectFromEntityManager(obj);
    // we need to generate code here if any
    obj.setCreationDate(new DateTime());
    obj.setModifiedDate(new DateTime());
    T t = this.saveAndFlush(obj);
    this.detach(t);
    createBusinessObjectDetails(t);
    return t;
  }

  public T update(T obj) throws Exception {
    T t = this.saveAndFlush(obj);
    updateBusinessObjectDetails(obj);
    return t;
  }

  public T updateAndClear(T obj) throws Exception {
    T t = this.saveAndFlush(obj);
    updateAndClearBusinessObjectDetails(obj);
    return t;
  }

  private void detachBusinessObjectFromEntityManager(T obj) {
    if (obj.getId() != null && obj.getId() > 0L && this.em.contains(obj)) {
      this.em.detach(obj);
    }
    obj.setId(null);
  }

  private void createBusinessObjectDetails(T obj) throws Exception {
    if (obj instanceof IAggregateAwareEntity) {
      IAggregateBusinessObject<InformationObject> compositeEntity =
          ((IAggregateAwareEntity) obj).aggregateBusinessObject();
      createBusinessObjectDetails(obj.getId(), compositeEntity);
    }
  }

  private void createBusinessObjectDetails(
      Long masterId, IAggregateBusinessObject<InformationObject> compositeEntity) throws Exception {
    if (compositeEntity != null) {
      for (ICompositeAttribute<? extends InformationObject> attribute :
          compositeEntity.getAllInstanceAttributes()) {
        if (attribute.isMultiple()) {
          createAttibuteAsMulitple(masterId, compositeEntity, attribute);
        } else {
          createAttributeAsSingle(masterId, compositeEntity, attribute);
        }
      }
    }
  }

  private void createAttributeAsSingle(
      Long masterId,
      IAggregateBusinessObject<InformationObject> compositeEntity,
      ICompositeAttribute<? extends InformationObject> attribute)
      throws Exception {
    InformationObject singleInstanceAttribute =
        compositeEntity.getSingleInstanceAttribute(attribute);
    singleInstanceAttribute.setRefInstanceId(masterId);
    create((T) singleInstanceAttribute);
  }

  private void createAttibuteAsMulitple(
      Long masterId,
      IAggregateBusinessObject<InformationObject> compositeEntity,
      ICompositeAttribute<? extends InformationObject> attribute)
      throws Exception {
    List<? extends InformationObject> multipleInstancesAttribute =
        compositeEntity.getMultipleInstancesAttribute(attribute);
    if (multipleInstancesAttribute != null && !multipleInstancesAttribute.isEmpty()) {
      for (InformationObject currentAttribute : multipleInstancesAttribute) {
        currentAttribute.setRefInstanceId(masterId);
        create((T) currentAttribute);
      }
    }
  }

  private void updateBusinessObjectDetails(T obj) throws Exception {
    if (obj instanceof IAggregateAwareEntity) {
      IAggregateBusinessObject<InformationObject> compositeEntity =
          ((IAggregateAwareEntity) obj).aggregateBusinessObject();
      updateBusinessObjectDetails(obj.getId(), compositeEntity);
    }
  }

  private void updateBusinessObjectDetails(
      Long masterId, IAggregateBusinessObject<InformationObject> compositeEntity) throws Exception {
    if (compositeEntity != null) {
      for (ICompositeAttribute<? extends InformationObject> iCompositeAttribute :
          compositeEntity.getAllInstanceAttributes()) {
        ICompositeAttribute<InformationObject> attribute =
            (ICompositeAttribute<InformationObject>) iCompositeAttribute;
        if (attribute.isMultiple()) {
          updateExistedInstancesAndAddNewOnes(masterId, compositeEntity, attribute);
        } else {
          updateAttibuteAsSingle(masterId, compositeEntity, attribute);
        }
      }
    }
  }

  private void updateAndClearBusinessObjectDetails(T obj) throws Exception {
    if (obj instanceof IAggregateAwareEntity) {
      IAggregateBusinessObject<InformationObject> compositeEntity =
          ((IAggregateAwareEntity) obj).aggregateBusinessObject();
      updateAndClearBusinessObjectDetails(obj.getId(), compositeEntity);
    }
  }

  private void updateAndClearBusinessObjectDetails(
      Long masterId, IAggregateBusinessObject<InformationObject> compositeEntity) throws Exception {
    if (compositeEntity != null) {
      for (ICompositeAttribute<? extends InformationObject> iCompositeAttribute :
          compositeEntity.getAllInstanceAttributes()) {
        ICompositeAttribute<InformationObject> attribute =
            (ICompositeAttribute<InformationObject>) iCompositeAttribute;
        if (attribute.isMultiple()) {
          updateMultipleInstancesAndClear(
              masterId, compositeEntity, attribute, attribute.getAttributeType());
        } else {
          updateAttibuteAsSingle(masterId, compositeEntity, attribute);
        }
      }
    }
  }

  private void updateAttibuteAsSingle(
      Long masterId,
      IAggregateBusinessObject<InformationObject> compositeEntity,
      ICompositeAttribute<? extends InformationObject> attribute)
      throws Exception {
    InformationObject singleInstanceAttribute =
        compositeEntity.getSingleInstanceAttribute(attribute);
    singleInstanceAttribute.setRefInstanceId(masterId);
    update((T) singleInstanceAttribute);
  }

  private void updateMultipleInstancesAndClear(
      Long masterId,
      IAggregateBusinessObject<InformationObject> compositeEntity,
      ICompositeAttribute<? extends InformationObject> attribute,
      Class<InformationObject> infoClass)
      throws Exception {

    List<InformationObject> original = retrieveInformationObjectOriginalList(infoClass, masterId);
    updateExistedInstancesAndAddNewOnes(masterId, compositeEntity, attribute);
    deleteUnwantedInstances(compositeEntity, attribute, original);
  }

  private void updateExistedInstancesAndAddNewOnes(
      Long masterId,
      IAggregateBusinessObject<InformationObject> compositeEntity,
      ICompositeAttribute<? extends InformationObject> attribute)
      throws Exception {
    List<? extends InformationObject> multipleInstancesAttribute =
        compositeEntity.getMultipleInstancesAttribute(attribute);
    if (multipleInstancesAttribute != null && !multipleInstancesAttribute.isEmpty()) {
      for (InformationObject currentInfo : multipleInstancesAttribute) {
        currentInfo.setRefInstanceId(masterId);
        update((T) currentInfo);
      }
    }
  }

  private void deleteUnwantedInstances(
      IAggregateBusinessObject<InformationObject> compositeEntity,
      ICompositeAttribute<? extends InformationObject> attribute,
      List<InformationObject> original) {
    List<InformationObject> target =
        compositeEntity.getMultipleInstancesAttribute(
            (ICompositeAttribute<InformationObject>) attribute);
    for (InformationObject info : original) {
      if (!containsInfo(info, target)) {
        delete((T) info);
      }
    }
  }

  private boolean containsInfo(InformationObject info, List<InformationObject> infos) {
    if (infos == null || infos.isEmpty()) return false;
    for (InformationObject item : infos) {
      if (info.getId().equals(item.getId())) return true;
    }
    return false;
  }

  private List<InformationObject> retrieveInformationObjectOriginalList(
      Class<InformationObject> infoClass, Long ownerId) {
    CriteriaBuilder cb = em.getCriteriaBuilder();
    CriteriaQuery<InformationObject> c = cb.createQuery(infoClass);
    Root<InformationObject> list = c.from(infoClass);
    c.select(list).where(cb.equal(list.get("refInstanceId"), ownerId));
    Query q = em.createQuery(c);
    return (q.getResultList());
  }
}
