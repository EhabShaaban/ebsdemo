package com.ebs.dac.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class CodedBusinessObject<UC extends UserCode> extends BusinessObject {

  private static final long serialVersionUID = 1L;

  @Column(name = "code")
  private String userCode;

  public String getUserCode() {
    return userCode;
  }

  public void setUserCode(String userCode) {
    this.userCode = userCode;
  }
}
