package com.ebs.dac.dbo.jpa.entities.usercodes;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 19, 2018 12:09:23 PM
 */
@Entity
@Table(name = "EntityUserCode")
public class EntityUserCode {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String type;
  private String code;

  public EntityUserCode() {}

  public EntityUserCode(String type, String code) {
    this.type = type;
    this.code = code;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
