package com.ebs.dac.dbo.events.stocktrasformation;

import com.ebs.dac.dbo.events.ActivateEvent;

public class StockTransformationActivateSuccessEvent extends ActivateEvent {

    public StockTransformationActivateSuccessEvent(String userCode) {
        super(userCode);
    }
}
