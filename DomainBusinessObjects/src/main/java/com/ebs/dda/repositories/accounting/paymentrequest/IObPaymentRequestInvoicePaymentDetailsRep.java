package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestInvoicePaymentDetails;

import java.util.List;

public interface IObPaymentRequestInvoicePaymentDetailsRep
    extends IObPaymentRequestPaymentDetailsRep<IObPaymentRequestInvoicePaymentDetails> {
  IObPaymentRequestInvoicePaymentDetails findOneByRefInstanceIdAndDueDocumentType(
      Long id, String type);

  List<IObPaymentRequestInvoicePaymentDetails> findOneByDueDocumentId(Long id);
}
