package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModel;

public interface DObPaymentReferenceOrderGeneralModelRep extends
				DObPaymentReferenceDocumentGeneralModelRep<DObPaymentReferenceOrderGeneralModel> {
}
