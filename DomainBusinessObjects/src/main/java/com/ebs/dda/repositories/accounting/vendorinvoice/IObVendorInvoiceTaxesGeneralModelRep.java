package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceTaxesGeneralModelRep;

import java.util.List;

public interface IObVendorInvoiceTaxesGeneralModelRep
				extends IObInvoiceTaxesGeneralModelRep<IObVendorInvoiceTaxesGeneralModel> {

	IObVendorInvoiceTaxesGeneralModel findOneByInvoiceCodeAndTaxCode(String invoiceCode, String taxCode);
	List<IObVendorInvoiceTaxesGeneralModel> findAllByInvoiceCode(String invoiceCode);
}
