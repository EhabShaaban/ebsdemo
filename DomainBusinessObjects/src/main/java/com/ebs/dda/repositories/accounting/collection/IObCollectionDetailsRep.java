package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetails;

public interface IObCollectionDetailsRep<T extends IObCollectionDetails> extends InformationObjectRep<T> {

    T findOneByRefInstanceId(Long refInstanceId);
}
