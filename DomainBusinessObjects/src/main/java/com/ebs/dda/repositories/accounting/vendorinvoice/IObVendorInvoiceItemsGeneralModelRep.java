package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceItemsGeneralModelRep;

import java.util.List;

public interface IObVendorInvoiceItemsGeneralModelRep
    extends IObInvoiceItemsGeneralModelRep<IObVendorInvoiceItemsGeneralModel> {

  List<IObVendorInvoiceItemsGeneralModel> findByInvoiceCodeOrderByIdAsc(String invoiceCode);
  List<IObVendorInvoiceItemsGeneralModel> findByInvoiceCodeOrderByIdDesc(String invoiceCode);

  IObVendorInvoiceItemsGeneralModel findOneByInvoiceCodeAndId(String invoiceCode, Long itemId);
  IObVendorInvoiceItemsGeneralModel findOneByInvoiceCodeAndItemIdAndOrderUnitId(String invoiceCode, Long itemId, Long orderUnitId);
  IObVendorInvoiceItemsGeneralModel findOneByInvoiceCodeAndItemCodeAndOrderUnitCode(String invoiceCode, String itemCode, String orderUnitCode);

  List<IObVendorInvoiceItemsGeneralModel> findByInvoiceCode(String invoiceCode);
}
