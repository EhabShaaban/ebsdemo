package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataGeneralModel;

public interface IObSalesOrderDataGeneralModelRep
    extends InformationObjectRep<IObSalesOrderDataGeneralModel> {

  IObSalesOrderDataGeneralModel findOneBySalesOrderCode(String salesOrderCode);
}
