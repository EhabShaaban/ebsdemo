package com.ebs.dda.repositories.masterdata.measure;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.GeneralModelRepository;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoMGeneralModel;

import java.util.List;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 27, 2016
 */
public interface IObAlternativeUoMGeneralModelRep
        extends GeneralModelRepository<IObAlternativeUoMGeneralModel, Long> {
  List<IObAlternativeUoMGeneralModel> findAllByItemCode(String itemCode);

  List<IObAlternativeUoMGeneralModel> findAllByItemCodeOrderByIdDesc(String itemCode);

  IObAlternativeUoMGeneralModel findByItemCodeAndAlternativeUnitOfMeasureCode(
          String itemCode, String measureCode);

  IObAlternativeUoMGeneralModel findByItemCodeAndAlternativeUnitOfMeasureId(
          String itemCode, Long alternativeMeasureId);
}
