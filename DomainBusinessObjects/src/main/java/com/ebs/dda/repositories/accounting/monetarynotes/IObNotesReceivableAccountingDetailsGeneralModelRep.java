package com.ebs.dda.repositories.accounting.monetarynotes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableAccountingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsGeneralModelRep;


public interface IObNotesReceivableAccountingDetailsGeneralModelRep
    extends IObAccountingDocumentAccountingDetailsGeneralModelRep<
            IObNotesReceivableAccountingDetailsGeneralModel> {
}
