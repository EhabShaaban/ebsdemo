package com.ebs.dda.repositories.inventory.stockavailability;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface IObItemQuantityGeneralModelRep<T extends IObItemQuantityGeneralModel>
        extends BusinessObjectRep<T> {
    List<T> findAllByUserCode(String code);

}
