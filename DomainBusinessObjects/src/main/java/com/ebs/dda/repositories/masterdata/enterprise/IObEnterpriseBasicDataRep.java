package com.ebs.dda.repositories.masterdata.enterprise;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseBasicData;

public interface IObEnterpriseBasicDataRep extends InformationObjectRep<IObEnterpriseBasicData> {
}
