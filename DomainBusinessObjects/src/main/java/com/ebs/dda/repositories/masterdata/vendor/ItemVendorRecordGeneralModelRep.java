package com.ebs.dda.repositories.masterdata.vendor;

import static org.eclipse.persistence.config.QueryHints.JDBC_MAX_ROWS;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import java.util.List;
import javax.persistence.QueryHint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

public interface ItemVendorRecordGeneralModelRep
    extends BusinessObjectRep<ItemVendorRecordGeneralModel>,
    JpaSpecificationExecutor<ItemVendorRecordGeneralModel> {

  Page<ItemVendorRecordGeneralModel> findAll(
      Specification<ItemVendorRecordGeneralModel> specification, Pageable page);

  @Query(
      value =
          "SELECT * from itemvendorrecordgeneralmodel "
              + "where purchaseunitname->> 'en' = ANY(string_to_array(?1, ','))",
      nativeQuery = true)
  Page<ItemVendorRecordGeneralModel> findByPurchasingUnitNames(
      String purchaseUnitNames, Pageable pageable);

  @Query(
      value =
          "SELECT COUNT( distinct id) from itemvendorrecordgeneralmodel "
              + "where purchaseunitname->> 'en' = ANY(string_to_array(?1, ','))",
      nativeQuery = true)
  long countByPurchasingUnitNames(String purchaseUnitNames);

  List<ItemVendorRecordGeneralModel> findAllByItemCode(String itemCode);

  List<ItemVendorRecordGeneralModel> findAllByVendorCode(String vendorCode);

  ItemVendorRecordGeneralModel findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
      String itemCode, String vendorCode, String uomCode, String purchaseUnitCode);

  ItemVendorRecordGeneralModel findByUserCode(String itemVendorRecordCode);

  @Query(
      value =
          "SELECT  e FROM ItemVendorRecordGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) > :code "
              + "AND e.purchaseUnitNameEn IN :purchaseUnitNames "
              + "ORDER BY CAST(e.userCode AS BIGINT) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  ItemVendorRecordGeneralModel findNextByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT  e FROM ItemVendorRecordGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) > :code "
              + "ORDER BY CAST(e.userCode AS BIGINT) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  ItemVendorRecordGeneralModel findNextByUserCodeUnconditionalRead(@Param("code") Long code);

  @Query(
      value =
          "SELECT  e FROM ItemVendorRecordGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) < :code "
              + "AND e.purchaseUnitNameEn IN :purchaseUnitNames "
              + "ORDER BY CAST(e.userCode AS BIGINT) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  ItemVendorRecordGeneralModel findPreviousByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT  e FROM ItemVendorRecordGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) < :code "
              + "ORDER BY CAST(e.userCode AS BIGINT) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  ItemVendorRecordGeneralModel findPreviousByUserCodeUnconditionalRead(@Param("code") Long code);

  List<ItemVendorRecordGeneralModel> findByVendorCodeAndPurchasingUnitCode(
      String vendorCode, String purchaseUnitCode);

  List<ItemVendorRecordGeneralModel> findByItemCodeAndVendorCodeAndPurchasingUnitCode(String itemCode, String vendorCode, String purchaseUnitCode);

  List<ItemVendorRecordGeneralModel> findByOldItemNumberAndPurchasingUnitCode(String oldItemNumber,
      String purchaseUnitCode);
}
