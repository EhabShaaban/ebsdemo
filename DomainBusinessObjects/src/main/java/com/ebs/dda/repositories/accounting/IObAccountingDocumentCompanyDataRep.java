package com.ebs.dda.repositories.accounting;

import org.springframework.data.repository.NoRepositoryBean;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

@NoRepositoryBean
public interface IObAccountingDocumentCompanyDataRep<T extends IObAccountingDocumentCompanyData>
    extends InformationObjectRep<T> {
}
