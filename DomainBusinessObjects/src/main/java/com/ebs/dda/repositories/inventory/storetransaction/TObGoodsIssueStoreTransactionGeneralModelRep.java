package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.TransactionObjectRep;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsIssueStoreTransactionGeneralModel;

import java.util.List;

public interface TObGoodsIssueStoreTransactionGeneralModelRep
        extends TransactionObjectRep<TObGoodsIssueStoreTransactionGeneralModel, DefaultUserCode> {

    List<TObGoodsIssueStoreTransactionGeneralModel>
    findAllByGoodsIssueCodeAndItemCodeAndUnitOfMeasureCodeAndCompanyCodeAndStorehouseCodeAndPlantCodeOrderByUserCodeAsc(
            String issueCode,
            String itemCode,
            String unitOfEntryCode,
            String companyCode,
            String storehouseCode,
            String goodsIssueCode);
}
