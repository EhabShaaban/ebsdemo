package com.ebs.dda.repositories.accounting.costing;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;

public interface IObCostingDetailsGeneralModelRep extends
		InformationObjectRep<IObCostingDetailsGeneralModel> {

	IObCostingDetailsGeneralModel findOneByGoodsReceiptCode(
			String goodsReceiptCode);
	IObCostingDetailsGeneralModel findOneByUserCode(String costingCode);
}
