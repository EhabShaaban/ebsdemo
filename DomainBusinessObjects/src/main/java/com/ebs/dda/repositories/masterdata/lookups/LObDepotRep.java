package com.ebs.dda.repositories.masterdata.lookups;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.masterdata.lookups.LObDepot;

import java.util.List;

public interface LObDepotRep extends LocalizedLookupRep<LObDepot, DefaultUserCode> {
  List<LObDepot> findAllByOrderByUserCodeDesc();
}
