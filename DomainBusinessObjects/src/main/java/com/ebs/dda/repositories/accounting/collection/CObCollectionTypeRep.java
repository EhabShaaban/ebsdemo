package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.accounting.collection.CObCollectionType;

public interface CObCollectionTypeRep
    extends ConfigurationObjectRep<CObCollectionType, DefaultUserCode> {}
