package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartner;

public interface IObSalesInvoiceBusinessPartnerRep<T extends IObSalesInvoiceBusinessPartner>
    extends InformationObjectRep<T> {

  T findOneByRefInstanceId(Long id);
}
