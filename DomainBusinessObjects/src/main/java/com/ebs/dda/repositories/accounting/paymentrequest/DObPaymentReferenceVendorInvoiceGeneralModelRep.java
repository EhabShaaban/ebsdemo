package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceVendorInvoiceGeneralModel;

public interface DObPaymentReferenceVendorInvoiceGeneralModelRep extends
				DObPaymentReferenceDocumentGeneralModelRep<DObPaymentReferenceVendorInvoiceGeneralModel> {
}
