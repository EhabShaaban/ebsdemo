package com.ebs.dda.repositories.accounting.accountingnote;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNoteGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface DObAccountingNoteGeneralModelRep
    extends StatefullBusinessObjectRep<DObAccountingNoteGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObAccountingNoteGeneralModel> {

  @Override
  Page<DObAccountingNoteGeneralModel> findAll(
      Specification<DObAccountingNoteGeneralModel> specification, Pageable page);

  List<DObAccountingNoteGeneralModel>
  findAllByPurchaseUnitCodeAndObjectTypeCodeLikeAndCurrentStatesLikeAndRemainingGreaterThanOrderByUserCodeDesc(
          String purchaseUnitCode, String objectTypeCode, String state, Double invalidRemainingValue
  );
}
