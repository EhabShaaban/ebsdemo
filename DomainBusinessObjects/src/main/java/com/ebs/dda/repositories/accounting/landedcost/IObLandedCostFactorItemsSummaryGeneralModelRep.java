package com.ebs.dda.repositories.accounting.landedcost;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsSummary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObLandedCostFactorItemsSummaryGeneralModelRep extends
        JpaRepository<IObLandedCostFactorItemsSummary, Long> {
        IObLandedCostFactorItemsSummary findOneByCode(String userCode);

        }
