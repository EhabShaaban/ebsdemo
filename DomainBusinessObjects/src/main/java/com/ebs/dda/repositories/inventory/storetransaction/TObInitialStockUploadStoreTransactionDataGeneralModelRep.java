package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.TransactionObjectRep;
import com.ebs.dda.jpa.inventory.storetransaction.TObInitialStockUploadStoreTransactionDataGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TObInitialStockUploadStoreTransactionDataGeneralModelRep
        extends TransactionObjectRep<TObInitialStockUploadStoreTransactionDataGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<TObInitialStockUploadStoreTransactionDataGeneralModel> {
}
