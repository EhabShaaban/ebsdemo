package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxes;
import com.ebs.dda.repositories.accounting.IObInvoiceTaxesRep;

public interface IObVendorInvoiceTaxesRep extends IObInvoiceTaxesRep<IObVendorInvoiceTaxes> {
}
