package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.sales.dbo.jpa.entities.GeneralModels.ItemsLastSalesPriceGeneralModel;
import org.joda.time.DateTime;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemsLastSalesPriceGeneralModelRep
    extends JpaRepository<ItemsLastSalesPriceGeneralModel, Long> {

  List<ItemsLastSalesPriceGeneralModel> findByItemCodeAndUomCodeAndPurchaseUnitCodeAndCustomerCode(
      String itemCode, String uomCode, String purchaseUnitCode, String customerCode);

  ItemsLastSalesPriceGeneralModel
      findTopByItemCodeAndUomCodeAndPurchaseUnitCodeAndCustomerCodeAndSalesInvoicePostingDateIsLessThanEqualOrderBySalesInvoicePostingDateDesc(
          String itemCode, String uomCode, String purchaseUnitCode, String customerCode, DateTime date);

  ItemsLastSalesPriceGeneralModel
      findTopByItemCodeAndUomCodeAndPurchaseUnitCodeAndCustomerCodeOrderBySalesInvoicePostingDateDesc(
          String itemCode, String uomCode, String purchaseUnitCode, String customerCode);
}
