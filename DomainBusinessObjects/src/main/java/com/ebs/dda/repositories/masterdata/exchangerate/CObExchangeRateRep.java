package com.ebs.dda.repositories.masterdata.exchangerate;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRate;

/** @author xerix on Thu Nov 2017 at 09 : 20 */
public interface CObExchangeRateRep
    extends ConfigurationObjectRep<CObExchangeRate, DefaultUserCode> {
}
