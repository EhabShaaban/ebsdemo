package com.ebs.dda.repositories.accounting.costing;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.costing.IObCostingItem;

public interface IObCostingItemRep
		extends InformationObjectRep<IObCostingItem> {

}
