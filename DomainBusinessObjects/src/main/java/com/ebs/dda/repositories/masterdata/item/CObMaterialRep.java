package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.item.CObMaterial;

public interface CObMaterialRep<T extends CObMaterial>
    extends ConfigurationObjectRep<T, DefaultUserCode> {}
