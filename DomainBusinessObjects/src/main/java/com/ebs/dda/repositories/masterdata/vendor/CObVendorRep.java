package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.vendor.CObVendor;

public interface CObVendorRep extends ConfigurationObjectRep<CObVendor, DefaultUserCode> {
}
