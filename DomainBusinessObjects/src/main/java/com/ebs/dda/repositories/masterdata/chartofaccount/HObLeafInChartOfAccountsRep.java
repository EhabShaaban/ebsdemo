package com.ebs.dda.repositories.masterdata.chartofaccount;

import com.ebs.dda.jpa.masterdata.chartofaccount.HObLeafGLAccount;

public interface HObLeafInChartOfAccountsRep
        extends HObChartOfAccountsRep<HObLeafGLAccount> {

  HObLeafGLAccount findOneById(Long glaccountId);
  HObLeafGLAccount findOneByUserCode(String userCode);

}
