package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;

public interface DObGoodsReceiptRep extends DocumentObjectRep<DObGoodsReceipt, DefaultUserCode> {}
