package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData;

public interface IObGoodsReceiptBatchedRecievedItemsRep
        extends InformationObjectRep<IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData> {

    IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData findOneByItemIdAndRefInstanceId(
            Long itemId, Long refInstanceId);
}

