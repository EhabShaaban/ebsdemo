package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoicePurchaseOrder;

public interface IObVendorInvoicePurchaseOrdeRep
		extends
			InformationObjectRep<IObVendorInvoicePurchaseOrder> {

}
