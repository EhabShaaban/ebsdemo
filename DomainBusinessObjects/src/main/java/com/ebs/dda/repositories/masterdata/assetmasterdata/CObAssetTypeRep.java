package com.ebs.dda.repositories.masterdata.assetmasterdata;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.assetmasterdata.CObAssetType;

public interface CObAssetTypeRep extends ConfigurationObjectRep<CObAssetType, DefaultUserCode> {}
