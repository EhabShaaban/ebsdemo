package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep
    extends JpaRepository<DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel, Long> {
  List<DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel> findByVendorInvoiceCodeAndPaymentType(
      String vendorInvoiceCode, String paymentType);
}
