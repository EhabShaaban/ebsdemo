package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.StoreKeeper;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreKeeperRep extends JpaRepository<StoreKeeper, Long> {

  StoreKeeper findOneByCode(String userCode);
}
