package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DObVendorInvoiceRep<T extends DObVendorInvoice> extends DocumentObjectRep<T, DefaultUserCode> {

    @Query("SELECT I.id FROM DObVendorInvoice I WHERE I.userCode = :userCode")
    Long findInvoiceIdByUserCode(@Param("userCode") String userCode);

    T findOneByUserCode(String userCode);
    T findOneById(Long id);
}
