package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItem;
import com.ebs.dda.repositories.accounting.IObInvoiceItemsRep;

public interface IObSalesInvoiceItemsRep extends IObInvoiceItemsRep<IObSalesInvoiceItem> {}
