package com.ebs.dda.repositories.inventory;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompany;

public interface IObInventoryCompanyRep extends InformationObjectRep<IObInventoryCompany> {

}
