package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dda.jpa.masterdata.customer.IObCustomerContactPersonGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObCustomerContactPersonGeneralModelRep
        extends JpaRepository<IObCustomerContactPersonGeneralModel, Long> {
    List<IObCustomerContactPersonGeneralModel> findAllByUserCodeOrderByIdAsc(String customerCode);
}
