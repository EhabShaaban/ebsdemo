package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceTaxesGeneralModelRep;

public interface IObSalesInvoiceTaxesGeneralModelRep
				extends IObInvoiceTaxesGeneralModelRep<IObSalesInvoiceTaxesGeneralModel> {

	IObSalesInvoiceTaxesGeneralModel findOneByInvoiceCodeAndTaxCode(String salesInvoiceCode, String taxCode);
}
