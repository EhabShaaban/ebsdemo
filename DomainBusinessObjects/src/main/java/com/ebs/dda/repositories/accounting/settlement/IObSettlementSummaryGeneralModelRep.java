package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementSummaryGeneralModel;

public interface IObSettlementSummaryGeneralModelRep extends InformationObjectRep<IObSettlementSummaryGeneralModel> {

    IObSettlementSummaryGeneralModel findOneBySettlementCode(String settlementCode);
}