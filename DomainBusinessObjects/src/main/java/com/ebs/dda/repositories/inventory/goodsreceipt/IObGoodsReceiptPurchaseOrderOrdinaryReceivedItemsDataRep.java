package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData;

public interface IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep
        extends InformationObjectRep<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData> {
}
