package com.ebs.dda.repositories.inventory.initialstockupload;

import com.ebs.dda.jpa.inventory.initialstockupload.IObInitialStockUploadItemGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObInitialStockUploadItemGeneralModelRep extends JpaRepository<IObInitialStockUploadItemGeneralModel, Long> {
    List<IObInitialStockUploadItemGeneralModel> findAllByInitialStockUploadCode(String initialStockUploadCode);
}
