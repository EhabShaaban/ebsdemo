package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableReferenceDocumentSalesInvoice;

public interface IObNotesReceivableReferenceDocumentSalesInvoiceRep<
        T extends IObNotesReceivableReferenceDocumentSalesInvoice>
    extends InformationObjectRep<T> {

  T findOneByRefInstanceId(Long refInstanceId);
}
