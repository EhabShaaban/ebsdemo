package com.ebs.dda.repositories.masterdata.chartofaccount;

import com.ebs.dda.jpa.masterdata.chartofaccount.HObIntermadiatGLAccount;

public interface HObIntermediateInChartOfAccountRep
        extends HObChartOfAccountsRep<HObIntermadiatGLAccount> {
}
