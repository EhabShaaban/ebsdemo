package com.ebs.dda.repositories.masterdata.salescoordinator;

import com.ebs.dda.jpa.masterdata.salescoordinator.CObSalesCoordinator;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

public interface CObSalesCoordinatorRep extends CObEnterpriseRep<CObSalesCoordinator> {}
