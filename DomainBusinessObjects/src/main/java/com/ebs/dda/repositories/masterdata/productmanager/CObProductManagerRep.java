package com.ebs.dda.repositories.masterdata.productmanager;

import com.ebs.dda.jpa.masterdata.productmanager.CObProductManager;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

public interface CObProductManagerRep extends CObEnterpriseRep<CObProductManager> {}
