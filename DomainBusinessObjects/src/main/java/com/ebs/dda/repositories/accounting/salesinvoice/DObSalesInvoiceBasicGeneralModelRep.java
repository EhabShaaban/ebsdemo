package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceBasicGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.GoodsReceiptItemDataGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObSalesInvoiceBasicGeneralModelRep
        extends JpaSpecificationExecutor<DObSalesInvoiceBasicGeneralModel>, JpaRepository<DObSalesInvoiceBasicGeneralModel, Long> {

    @Override
    Page<DObSalesInvoiceBasicGeneralModel> findAll(
            Specification<DObSalesInvoiceBasicGeneralModel> specification,
            Pageable page);

}
