package com.ebs.dda.repositories.masterdata.enterprise;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;

public interface CObEnterpriseRep<T extends CObEnterprise>
    extends ConfigurationObjectRep<T, DefaultUserCode> {}
