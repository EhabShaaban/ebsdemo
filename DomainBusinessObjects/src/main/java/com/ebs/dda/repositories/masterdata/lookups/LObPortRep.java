package com.ebs.dda.repositories.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.lookups.LObPort;
import com.ebs.dda.repositories.masterdata.item.LObMaterialRep;

/** @author xerix on Wed Nov 2017 at 15 : 58 */
public interface LObPortRep extends LObMaterialRep<LObPort> {}
