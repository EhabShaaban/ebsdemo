package com.ebs.dda.repositories.masterdata.measure;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.measure.CObMeasure;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 27, 2016
 */
public interface CObMeasureRep extends ConfigurationObjectRep<CObMeasure, DefaultUserCode> {}
