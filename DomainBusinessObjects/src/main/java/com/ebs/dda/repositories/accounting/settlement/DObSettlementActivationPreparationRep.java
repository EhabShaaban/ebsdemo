package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dda.jpa.accounting.settlements.DObSettlementActivationPreparation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObSettlementActivationPreparationRep
    extends JpaRepository<DObSettlementActivationPreparation, Long> {}
