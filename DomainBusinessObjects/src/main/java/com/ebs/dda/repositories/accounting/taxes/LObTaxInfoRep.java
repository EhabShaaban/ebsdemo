package com.ebs.dda.repositories.accounting.taxes;

import org.springframework.data.repository.NoRepositoryBean;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.accounting.taxes.LObTaxInfo;

@NoRepositoryBean
public interface LObTaxInfoRep<T extends LObTaxInfo>
				extends LocalizedLookupRep<T, DefaultUserCode> {
}
