package com.ebs.dda.repositories.accounting.journalbalance;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;

public interface CObJournalBalanceGeneralModelRep
    extends CodedBusinessObjectRep<CObJournalBalanceGeneralModel> {}
