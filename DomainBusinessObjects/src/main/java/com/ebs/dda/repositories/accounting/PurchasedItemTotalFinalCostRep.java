package com.ebs.dda.repositories.accounting;

import com.ebs.dda.jpa.accounting.PurchasedItemsTotalFinalCost;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PurchasedItemTotalFinalCostRep
    extends JpaRepository<PurchasedItemsTotalFinalCost, Long> {
  PurchasedItemsTotalFinalCost findOneByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCode(
      String goodsReceiptCode, String itemCode, String unitOfEntryCode);

  List<PurchasedItemsTotalFinalCost> findAllByPurchaseorderCode(String purchaseOrderCode);

  List<PurchasedItemsTotalFinalCost> findByLandedCostCode(String landedCostCode);
}
