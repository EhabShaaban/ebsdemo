package com.ebs.dda.repositories.accounting.settlement;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ebs.dda.jpa.accounting.settlements.DObSettlementJournalEntryPreparationGeneralModel;

public interface DObSettlementJournalEntryPreparationGeneralModelRep
				extends JpaRepository<DObSettlementJournalEntryPreparationGeneralModel, Long> {

	DObSettlementJournalEntryPreparationGeneralModel findOneByCode(String settlementCode);
}
