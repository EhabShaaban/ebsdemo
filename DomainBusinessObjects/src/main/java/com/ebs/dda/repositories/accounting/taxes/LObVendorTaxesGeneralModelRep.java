package com.ebs.dda.repositories.accounting.taxes;

import java.util.List;

import com.ebs.dda.jpa.accounting.taxes.LObVendorTaxesGeneralModel;

public interface LObVendorTaxesGeneralModelRep
				extends LObTaxInfoGeneralModelRep<LObVendorTaxesGeneralModel> {
	List<LObVendorTaxesGeneralModel> findAllByVendorCode(String vendorCode);

}
