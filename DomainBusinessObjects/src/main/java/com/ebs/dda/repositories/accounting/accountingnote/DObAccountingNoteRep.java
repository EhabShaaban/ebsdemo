package com.ebs.dda.repositories.accounting.accountingnote;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;

public interface DObAccountingNoteRep
    extends DocumentObjectRep<DObAccountingNote, DefaultUserCode> {}
