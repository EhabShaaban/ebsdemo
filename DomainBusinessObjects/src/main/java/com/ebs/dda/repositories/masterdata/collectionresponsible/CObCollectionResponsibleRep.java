package com.ebs.dda.repositories.masterdata.collectionresponsible;

import com.ebs.dda.jpa.masterdata.collectionresponsible.CObCollectionResponsible;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

public interface CObCollectionResponsibleRep extends CObEnterpriseRep<CObCollectionResponsible> {}
