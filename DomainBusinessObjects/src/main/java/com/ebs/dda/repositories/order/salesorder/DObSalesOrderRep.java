package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;

public interface DObSalesOrderRep extends DocumentObjectRep<DObSalesOrder, DefaultUserCode> {}
