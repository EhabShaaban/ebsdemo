package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dda.jpa.accounting.collection.IObCollectionAccountingDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsGeneralModelRep;

public interface IObCollectionAccountingDetailsGeneralModelRep
    extends IObAccountingDocumentAccountingDetailsGeneralModelRep<
        IObCollectionAccountingDetailsGeneralModel> {

  IObCollectionAccountingDetailsGeneralModel findOneByDocumentCodeAndAccountingEntry(
      String userCode, String accountingEntry);
}
