package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dda.jpa.masterdata.item.MObItemUOMGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MObItemUOMGeneralModelRep extends JpaRepository<MObItemUOMGeneralModel, Long> {
    List<MObItemUOMGeneralModel> findAllByItemCodeOrderByUserCodeDesc(String itemCode);

    MObItemUOMGeneralModel findOneByItemCodeAndUserCode(String itemCode, String uomCode);
}
