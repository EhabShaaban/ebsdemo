package com.ebs.dda.repositories.accounting.journalbalance;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalance;

/**
 * @author Ahmed Ali Elfeky
 * @since May 25, 2021
 */
public interface CObJournalBalanceRep extends CodedBusinessObjectRep<CObJournalBalance> {
}
