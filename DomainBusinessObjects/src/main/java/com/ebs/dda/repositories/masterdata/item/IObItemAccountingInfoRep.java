package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfo;

public interface IObItemAccountingInfoRep extends InformationObjectRep<IObItemAccountingInfo> {
}
