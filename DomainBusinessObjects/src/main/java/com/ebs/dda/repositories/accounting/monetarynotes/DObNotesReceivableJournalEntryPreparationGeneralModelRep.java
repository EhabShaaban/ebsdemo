package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableJournalEntryPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObNotesReceivableJournalEntryPreparationGeneralModelRep
  extends JpaRepository<DObNotesReceivableJournalEntryPreparationGeneralModel, Long> {

  DObNotesReceivableJournalEntryPreparationGeneralModel findOneByCode(String notesReceivableCode);

}
