package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObGoodsReceiptPurchaseOrderDataRep
    extends JpaRepository<IObGoodsReceiptPurchaseOrderData, Long> {

    IObGoodsReceiptPurchaseOrderData findByRefInstanceId(Long goodsReceiptId);
}
