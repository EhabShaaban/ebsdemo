package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep
        extends CrudRepository<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel, Long> {

    List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
    findByOrOrdinaryItemIdOrderByIdDesc(Long ordinaryItemId);

    List<IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel>
    findByGoodsReceiptCodeAndItemCode(String goodsReceiptCode, String itemCode);

    IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel
    findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockType(
            String goodsReceiptCode, String itemCode, String unitOfEntryCode, String stockType);

    IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel
    findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockTypeAndIdNot(
            String goodsReceiptCode,
            String itemCode,
            String unitOfEntryCode,
            String stockType,
            Long id);
}
