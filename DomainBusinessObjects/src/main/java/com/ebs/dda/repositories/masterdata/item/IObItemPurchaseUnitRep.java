package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.item.IObItemPurchaseUnit;
import java.util.List;

public interface IObItemPurchaseUnitRep extends InformationObjectRep<IObItemPurchaseUnit> {

  List<IObItemPurchaseUnit> findAllByRefInstanceId(Long refInstanceId);

  IObItemPurchaseUnit findByRefInstanceIdAndPurchaseUnitId(Long refInstanceId, Long purchaseUnitId);
}
