package com.ebs.dda.repositories.masterdata.measure;

import com.ebs.dda.jpa.masterdata.measure.CObUoMGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 27, 2016
 */
public interface CObUoMGeneralModelRep extends JpaRepository<CObUoMGeneralModel, Long> {
    CObUoMGeneralModel findOneByUserCode(String userCode);
    List<CObUoMGeneralModel> findAllByOrderByUserCodeDesc();
}
