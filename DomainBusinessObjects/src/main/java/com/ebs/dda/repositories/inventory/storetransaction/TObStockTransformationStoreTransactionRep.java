package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.TransactionObjectRep;
import com.ebs.dda.jpa.inventory.storetransaction.TObStockTransformationStoreTransaction;
import com.ebs.dda.jpa.inventory.storetransaction.TransactionTypeEnum;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface TObStockTransformationStoreTransactionRep
    extends TransactionObjectRep<TObStockTransformationStoreTransaction, DefaultUserCode>,
        JpaSpecificationExecutor<TObStockTransformationStoreTransaction> {
  TObStockTransformationStoreTransaction findOneByDocumentReferenceIdAndTransactionOperation(
      Long documentReferenceId, TransactionTypeEnum.TransactionType transactionOperation);
}
