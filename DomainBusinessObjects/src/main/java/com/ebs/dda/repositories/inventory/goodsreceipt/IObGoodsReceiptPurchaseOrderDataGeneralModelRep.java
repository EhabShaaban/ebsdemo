package com.ebs.dda.repositories.inventory.goodsreceipt;
// Created By Hossam Hassan @ 7/25/18

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObGoodsReceiptPurchaseOrderDataGeneralModelRep
    extends JpaRepository<IObGoodsReceiptPurchaseOrderDataGeneralModel, Long> {

  IObGoodsReceiptPurchaseOrderDataGeneralModel findByGoodsReceiptCode(String goodsReceiptCode);

  List<IObGoodsReceiptPurchaseOrderDataGeneralModel> findByPurchaseOrderCode(
      String purchaseOrderCode);
}
