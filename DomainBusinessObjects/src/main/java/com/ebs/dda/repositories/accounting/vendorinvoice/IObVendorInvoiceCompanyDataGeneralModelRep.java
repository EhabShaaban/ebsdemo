package com.ebs.dda.repositories.accounting.vendorinvoice;

import org.springframework.stereotype.Repository;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceCompanyDataGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataGeneralModelRep;

@Repository
public interface IObVendorInvoiceCompanyDataGeneralModelRep extends
    IObAccountingDocumentCompanyDataGeneralModelRep<IObVendorInvoiceCompanyDataGeneralModel> {

}
