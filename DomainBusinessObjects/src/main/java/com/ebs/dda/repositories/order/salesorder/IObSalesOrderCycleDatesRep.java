package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCycleDates;

public interface IObSalesOrderCycleDatesRep extends InformationObjectRep<IObSalesOrderCycleDates> {}
