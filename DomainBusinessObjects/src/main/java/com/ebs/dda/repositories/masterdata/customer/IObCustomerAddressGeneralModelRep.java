package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dda.jpa.masterdata.customer.IObCustomerAddressGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObCustomerAddressGeneralModelRep
    extends JpaRepository<IObCustomerAddressGeneralModel, Long> {
  List<IObCustomerAddressGeneralModel> findAllByUserCodeOrderByIdDesc(String customerCode);
}
