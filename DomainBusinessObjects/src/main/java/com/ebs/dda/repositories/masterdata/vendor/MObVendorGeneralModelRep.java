package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

import static org.eclipse.persistence.config.QueryHints.JDBC_MAX_ROWS;

public interface MObVendorGeneralModelRep
    extends StatefullBusinessObjectRep<MObVendorGeneralModel, DefaultUserCode> {

  String vendorsSql =
      "SELECT vendor FROM MObVendorGeneralModel vendor "
          + "WHERE vendor.userCode IN (SELECT distinct(vendor.userCode) FROM MObVendorGeneralModel vendor "
          + "LEFT JOIN IObVendorPurchaseUnit unit ON vendor.id = unit.refInstanceId WHERE unit.purchaseUnitName IN :purchaseUnitNames)";

  Page<MObVendorGeneralModel> findAll(Specification<MObVendorGeneralModel> specification,
					Pageable page);

  List<MObVendorGeneralModel> findByCurrentStatesLike(String currentStates);

  @Query(
      value =
          "SELECT vendor FROM MObVendorGeneralModel vendor LEFT JOIN IObVendorPurchaseUnit unit ON vendor.id = unit.refInstanceId "
              + "WHERE CAST(vendor.userCode AS INTEGER) > :code "
              + "AND unit.purchaseUnitName IN :purchaseUnitNames "
              + "ORDER BY CAST(vendor.userCode AS INTEGER) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  MObVendorGeneralModel findNextByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT e FROM MObVendorGeneralModel e "
              + "WHERE CAST (e.userCode AS INTEGER) > :code "
              + "ORDER BY CAST (e.userCode AS INTEGER) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  MObVendorGeneralModel findNextByUserCodeUnconditionalRead(@Param("code") Long code);

  @Query(
      value =
          "SELECT vendor FROM MObVendorGeneralModel vendor LEFT JOIN IObVendorPurchaseUnit unit ON vendor.id = unit.refInstanceId "
              + "WHERE CAST(vendor.userCode AS INTEGER) < :code "
              + "AND unit.purchaseUnitName IN :purchaseUnitNames "
              + "ORDER BY CAST(vendor.userCode AS INTEGER) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  MObVendorGeneralModel findPreviousByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT e FROM MObVendorGeneralModel e "
              + "WHERE CAST (e.userCode AS INTEGER) < :code "
              + "ORDER BY CAST (e.userCode AS INTEGER) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  MObVendorGeneralModel findPreviousByUserCodeUnconditionalRead(@Param("code") Long code);

    List<MObVendorGeneralModel> findAllByPurchasingUnitCodesContainsOrderByUserCodeDesc(String purchaseUnitCode);

    MObVendorGeneralModel findOneByOldVendorNumber(String oldVendorNumber);
}
