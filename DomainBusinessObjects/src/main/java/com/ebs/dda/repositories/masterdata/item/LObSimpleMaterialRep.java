package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.masterdata.item.LObSimpleMaterial;

/** Created by yara on 11/2/17. */
public interface LObSimpleMaterialRep<T extends LObSimpleMaterial>
    extends LocalizedLookupRep<T, DefaultUserCode> {}
