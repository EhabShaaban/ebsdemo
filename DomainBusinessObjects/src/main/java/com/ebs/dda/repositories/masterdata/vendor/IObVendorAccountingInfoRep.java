package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfo;

public interface IObVendorAccountingInfoRep extends BusinessObjectRep<IObVendorAccountingInfo> {
     IObVendorAccountingInfo findByRefInstanceId(Long vendorId);
}
