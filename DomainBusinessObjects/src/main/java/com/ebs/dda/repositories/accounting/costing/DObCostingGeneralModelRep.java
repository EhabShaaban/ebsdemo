package com.ebs.dda.repositories.accounting.costing;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObCostingGeneralModelRep
    extends DocumentObjectRep<DObCostingGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObCostingGeneralModel> {
  DObCostingGeneralModel findOneByUserCode(String userCode);
}
