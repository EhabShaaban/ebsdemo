package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.ItemUnitOfEntries;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemUnitOfEntriesRep extends JpaRepository<ItemUnitOfEntries, Long> {

  List<ItemUnitOfEntries> findAllByItemCode(String Code);
}
