package com.ebs.dda.repositories.accounting.collection;

import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;

public interface DObCollectionGeneralModelRep
    extends DocumentObjectRep<DObCollectionGeneralModel, DefaultUserCode>,
    JpaSpecificationExecutor<DObCollectionGeneralModel> {

  List<DObCollectionGeneralModel> findAllByRefDocumentCodeAndRefDocumentTypeCodeAndCurrentStatesLike(
      String refDocumentCode, String refDocumentTypeCode, String state);
}
