package com.ebs.dda.repositories.masterdata.productmanager;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.productmanager.CObProductManagerGeneralModel;

public interface CObProductManagerGeneralModelRep
    extends StatefullBusinessObjectRep<CObProductManagerGeneralModel, DefaultUserCode> {}
