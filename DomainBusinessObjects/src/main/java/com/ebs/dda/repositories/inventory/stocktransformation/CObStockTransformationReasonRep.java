package com.ebs.dda.repositories.inventory.stocktransformation;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.inventory.stocktransformation.CObStockTransformationReason;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface CObStockTransformationReasonRep
        extends ConfigurationObjectRep<CObStockTransformationReason, DefaultUserCode> {

    List<CObStockTransformationReason> findByTypeIdAndFromStockTypeAndToStockType(Long typeId,String fromStockType,String toStockType, Sort by);
}
