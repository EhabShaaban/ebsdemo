package com.ebs.dda.repositories.masterdata.company;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetails;

import java.util.List;

public interface IObCompanyBankDetailsRep extends InformationObjectRep<IObCompanyBankDetails> {

  List<IObCompanyBankDetails> findByRefInstanceId(Long companyId);

  IObCompanyBankDetails findOneByIdAndRefInstanceId(
      Long companyBankDetailsDetailsId, Long companyId);

  IObCompanyBankDetails findOneByBankIdAndRefInstanceIdAndAccountNo(
      Long bankId, Long companyId, String accountNumber);

  IObCompanyBankDetails findOneByBankIdAndRefInstanceId(Long bankId, Long companyId);

  List<IObCompanyBankDetails> findByBankIdAndRefInstanceId(Long bankId, Long companyId);

  IObCompanyBankDetails findOneByAccountNoAndRefInstanceId(String accountNo, Long companyId);

}
