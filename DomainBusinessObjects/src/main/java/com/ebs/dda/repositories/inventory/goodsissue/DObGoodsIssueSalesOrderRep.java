package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueSalesOrder;

public interface DObGoodsIssueSalesOrderRep
        extends DocumentObjectRep<DObGoodsIssueSalesOrder, DefaultUserCode> {

  DObGoodsIssueSalesOrder findOneByUserCode(String goodsIssueCode);
}
