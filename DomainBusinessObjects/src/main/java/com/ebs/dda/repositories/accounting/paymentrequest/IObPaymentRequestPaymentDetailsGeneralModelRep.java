package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;

import java.util.List;

public interface IObPaymentRequestPaymentDetailsGeneralModelRep extends
        InformationObjectRep<IObPaymentRequestPaymentDetailsGeneralModel> {

    List<IObPaymentRequestPaymentDetailsGeneralModel> findByCurrentStatesLikeAndDueDocumentCodeAndDueDocumentTypeLikeAndAndBusinessPartnerCodeAndCurrencyCode(String currentStates, String documentCode, String documentType, String vendorCode, String CurrencyCode);


    List<IObPaymentRequestPaymentDetailsGeneralModel> findByDueDocumentCode(String invoiceCode);

    List<IObPaymentRequestPaymentDetailsGeneralModel> findByDueDocumentCodeAndDueDocumentType(
            String documentCode, String documentType);

    List<IObPaymentRequestPaymentDetailsGeneralModel> findByDueDocumentCodeAndDueDocumentTypeAndCurrentStatesLike(
            String documentCode, String documentType, String state);


    IObPaymentRequestPaymentDetailsGeneralModel findOneByUserCode(String userCode);
}
