package com.ebs.dda.repositories.accounting.accountingnote;

import com.ebs.dda.jpa.accounting.accountingnotes.IObAccountingNoteCompanyGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataGeneralModelRep;

public interface IObAccountingNoteCompanyGeneralModelRep extends
				IObAccountingDocumentCompanyDataGeneralModelRep<IObAccountingNoteCompanyGeneralModel> {
}
