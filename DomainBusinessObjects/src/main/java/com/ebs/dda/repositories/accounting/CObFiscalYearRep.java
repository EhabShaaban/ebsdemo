package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.accounting.CObFiscalYear;

import java.util.List;

public interface CObFiscalYearRep extends ConfigurationObjectRep<CObFiscalYear, DefaultUserCode> {
  List<CObFiscalYear> findByCurrentStatesLike(String activeState);
}
