package com.ebs.dda.repositories.inventory.stockavailability;

import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObStockAvailabilityGeneralModelRep
        extends JpaRepository<CObStockAvailabilitiesGeneralModel, Long>,
        JpaSpecificationExecutor<CObStockAvailabilitiesGeneralModel> {
    CObStockAvailabilitiesGeneralModel
    findByCompanyCodeAndStorehouseCodeAndUnitOfMeasureCodeAndPurchaseUnitCodeAndItemCodeAndStockType(
            String companyCode,
            String storehouseCode,
            String unitOfMeasureCode,
            String purchaseUnitCode,
            String itemCode,
            String stockType);

    CObStockAvailabilitiesGeneralModel
    findByItemCodeAndCompanyCodeAndPlantCodeAndStorehouseCodeAndPurchaseUnitCodeAndStockTypeAndUnitOfMeasureCode(
            String itemCode,
            String companyCode,
            String plantCode,
            String storehouseCode,
            String purchaseUnitCode,
            String stockType,
            String unitOfMeasureCode);
}
