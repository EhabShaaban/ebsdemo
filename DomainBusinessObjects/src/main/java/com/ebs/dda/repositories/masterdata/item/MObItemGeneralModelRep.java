package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;
import java.util.Set;

import static org.eclipse.persistence.config.QueryHints.JDBC_MAX_ROWS;

public interface MObItemGeneralModelRep
        extends BusinessObjectRep<MObItemGeneralModel>, JpaSpecificationExecutor<MObItemGeneralModel> {

  Page<MObItemGeneralModel> findAll(
          Specification<MObItemGeneralModel> specification, Pageable page);

  MObItemGeneralModel findByUserCode(String code);

  MObItemGeneralModel findByUserCodeAndBasicUnitOfMeasureCode(
          String code, String basicUnitOfMeasureCode);

  Page<MObItemGeneralModel> findAllByOrderByIdDesc(Pageable pageable);

  List<MObItemGeneralModel> findAllByTypeCodeAndPurchasingUnitCodesContainsAndCostFactor(String typeCode, String LandedCostCode, Boolean isCostFactor);

  @Query(
          value =
                  "SELECT e.* from MObItemGeneralModel e WHERE e.code IN (SELECT distinct(item.code) FROM MObItemGeneralModel item "
                          + "inner join json_array_elements(purchaseunitsnames) purchaseUnit "
                          + "on purchaseUnit->> 'en' = ANY(string_to_array(?1, ',')))",
          nativeQuery = true)
  Page<MObItemGeneralModel> findByPurchasingUnitNamesInOrderByIdDesc(
          String purchaseUnitNames, Pageable pageable);

  @Query(
          value =
                  "SELECT e.* FROM MObItemGeneralModel e, json_array_elements(purchaseunitsnames) purchaseUnit "
                          + "where purchaseUnit->> 'en' = ANY(string_to_array(?1, ',')) AND CAST(e.code AS INTEGER) > ?2 ORDER BY CAST(e.code AS INTEGER) ASC LIMIT 1",
          nativeQuery = true)
  MObItemGeneralModel findNextByUserCode(String purchaseunitsnames, Long code);

  @Query(
      value =
          "SELECT e FROM MObItemGeneralModel e "
              + "WHERE CAST (e.userCode AS INTEGER) > :code "
              + "ORDER BY CAST (e.userCode AS INTEGER) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  MObItemGeneralModel findNextByUserCodeUnconditionalRead(Long code);

  @Query(
      value =
          "SELECT e.* FROM MObItemGeneralModel e, json_array_elements(purchaseunitsnames) purchaseUnit "
              + "where purchaseUnit->> 'en' = ANY(string_to_array(?1, ',')) AND CAST(e.code AS INTEGER) < ?2 ORDER BY CAST(e.code AS INTEGER)  DESC LIMIT 1",
      nativeQuery = true)
  MObItemGeneralModel findPreviousByUserCode(String purchaseunitsnames, Long code);

  @Query(
          value =
                  "SELECT e FROM MObItemGeneralModel e "
                          + "WHERE CAST (e.userCode AS INTEGER) < :code "
                          + "ORDER BY CAST (e.userCode AS INTEGER) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  MObItemGeneralModel findPreviousByUserCodeUnconditionalRead(Long code);

  @Query(
          value =
                  "SELECT e FROM MObItemGeneralModel e WHERE  e.userCode IN :itemCodes order by e.userCode DESC")
  List<MObItemGeneralModel> findByUserCodeIn(@Param("itemCodes") Set<String> itemCodes);

  List<MObItemGeneralModel> findAllByPurchasingUnitCodesContainsOrderByUserCodeDesc(
          String purchaseUnitCode);
}
