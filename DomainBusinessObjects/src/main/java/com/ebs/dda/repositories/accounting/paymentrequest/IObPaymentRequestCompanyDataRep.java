package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestCompanyData;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataRep;

public interface IObPaymentRequestCompanyDataRep
    extends IObAccountingDocumentCompanyDataRep<IObPaymentRequestCompanyData> {}
