package com.ebs.dda.repositories.inventory.initialstockupload;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUpload;

public interface DObInitialStockUploadRep
    extends DocumentObjectRep<DObInitialStockUpload, DefaultUserCode> {}
