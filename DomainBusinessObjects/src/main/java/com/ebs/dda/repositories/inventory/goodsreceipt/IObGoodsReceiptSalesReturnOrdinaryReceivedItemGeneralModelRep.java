package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
    extends CrudRepository<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel, Long> {

  List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel> findByOrdinaryItemIdOrderByIdAsc(
      Long ordinaryItemId);

  IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
      findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockType(
          String goodsReceiptCode, String itemCode, String unitOfEntryCode, String name);

  List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
      findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCode(
          String goodsReceiptCode, String itemCode, String unitOfEntryCode);

  List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
      findAllByGoodsReceiptCodeOrderByIdAsc(String goodsReceiptCode);

    IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel
    findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockTypeAndIdNot(
            String goodsReceiptCode, String itemCode, String unitOfEntryCode, String name, Long id);

    List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
    findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndIdNot(
            String goodsReceiptCode, String itemCode, String unitOfEntryCode, Long id);

    List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel>
    findByGoodsReceiptCodeAndItemCode(String goodsReceiptCode, String itemCode);
}
