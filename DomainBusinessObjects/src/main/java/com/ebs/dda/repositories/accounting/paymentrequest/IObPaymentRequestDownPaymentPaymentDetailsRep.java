package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPurchaseOrderPaymentDetails;

public interface IObPaymentRequestDownPaymentPaymentDetailsRep
    extends IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPurchaseOrderPaymentDetails> {
  IObPaymentRequestPurchaseOrderPaymentDetails findOneByRefInstanceIdAndDueDocumentType(
      Long id, String type);
}
