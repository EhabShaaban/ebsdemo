package com.ebs.dda.repositories.masterdata.company;

import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetailsGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObCompanyBankDetailsGeneralModelRep
    extends JpaRepository<IObCompanyBankDetailsGeneralModel, Long> {

  List<IObCompanyBankDetailsGeneralModel> findByCompanyCode(String companyCode);

  List<IObCompanyBankDetailsGeneralModel> findByCodeAndCompanyCode(
      String bankCode, String companyCode);

  IObCompanyBankDetailsGeneralModel findOneByCodeAndCompanyCodeAndAccountNumber(
      String bankCode, String companyCode, String accountNumber);

  IObCompanyBankDetailsGeneralModel findOneByCompanyCodeAndAccountNumber(String companyCode, String accountNumber);

  IObCompanyBankDetailsGeneralModel findOneByIdAndCompanyCode(Long bankAccountId, String companyCode );

}
