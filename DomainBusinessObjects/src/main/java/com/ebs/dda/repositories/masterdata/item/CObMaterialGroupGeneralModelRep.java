package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.item.CObMaterialGroupGeneralModel;
import com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CObMaterialGroupGeneralModelRep
        extends CodedBusinessObjectRep<CObMaterialGroupGeneralModel> {
  List<CObMaterialGroupGeneralModel> findByLevel(HierarchyLevelEnumType level);

  @Query(
          "Select max(G.id) from CObMaterialGroupGeneralModel G where G.level.value= com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType.HierarchyLevelEnum.ROOT ")
  Optional<Long> findItemGroupMaxIdAsRoot();

  @Query(
          "Select G.userCode from CObMaterialGroupGeneralModel G where G.id=:maxId and G.level.value= com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType.HierarchyLevelEnum.ROOT")
  Optional<String> findItemGroupCodeAsRootWithMaxId(@Param("maxId") Long maxId);

  @Query("Select max(G.id) from CObMaterialGroupGeneralModel G where G.parentCode = :parentCode ")
  Optional<Long> findItemGroupMaxIdAsHierarchy(@Param("parentCode") String parentCode);

  @Query("Select G.userCode from CObMaterialGroupGeneralModel G where G.id=:maxChildId")
  Optional<String> findItemGroupCodeAsHierarchyWithMaxId(@Param("maxChildId")  Long maxChildId);

  @Query("Select G.id from CObMaterialGroupGeneralModel G where G.userCode=:usercode")
  Long findParentItemGroupId(@Param("usercode") String usercode);

  @Query("Select G from CObMaterialGroupGeneralModel G where G.userCode=:usercode")
  CObMaterialGroupGeneralModel findItemGroupLevel(@Param("usercode") String usercode);

  List<CObMaterialGroupGeneralModel> findAllByLevelNot(HierarchyLevelEnumType level);

  List<CObMaterialGroupGeneralModel> findByParentCode(String itemGroupParentCode);

  Page<CObMaterialGroupGeneralModel> findAll(
          Specification<CObMaterialGroupGeneralModel> specification, Pageable page);


}
