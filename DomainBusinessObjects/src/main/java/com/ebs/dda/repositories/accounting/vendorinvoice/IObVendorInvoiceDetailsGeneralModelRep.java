package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;

public interface IObVendorInvoiceDetailsGeneralModelRep
    extends BusinessObjectRep<IObVendorInvoiceDetailsGeneralModel> {

  IObVendorInvoiceDetailsGeneralModel findByInvoiceCode(String invoiceCode);
}
