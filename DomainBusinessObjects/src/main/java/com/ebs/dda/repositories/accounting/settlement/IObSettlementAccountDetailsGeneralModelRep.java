package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsGeneralModelRep;

import java.util.List;

public interface IObSettlementAccountDetailsGeneralModelRep
    extends IObAccountingDocumentAccountingDetailsGeneralModelRep<
        IObSettlementAccountDetailsGeneralModel> {
  List<IObSettlementAccountDetailsGeneralModel> findAllByDocumentCode(String documentCode);
}
