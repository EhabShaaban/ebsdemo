package com.ebs.dda.repositories.accounting.salesinvoice;

import org.springframework.stereotype.Repository;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceCompanyData;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataRep;

@Repository
public interface IObSalesInvoiceCompanyDataRep
    extends IObAccountingDocumentCompanyDataRep<IObSalesInvoiceCompanyData> {
}
