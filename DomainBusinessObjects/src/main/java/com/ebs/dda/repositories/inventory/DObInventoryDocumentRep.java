package com.ebs.dda.repositories.inventory;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.inventory.inventorydocument.DObInventoryDocument;

public interface DObInventoryDocumentRep<T extends DObInventoryDocument>
        extends DocumentObjectRep<T, DefaultUserCode> {
  DObInventoryDocument findOneByUserCodeAndInventoryDocumentType(
          String userCode, String inventoryDocumentType);
}
