package com.ebs.dda.repositories.accounting.costing;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.costing.IObCostingItemGeneralModel;

import java.util.List;

public interface IObCostingItemGeneralModelRep
		extends InformationObjectRep<IObCostingItemGeneralModel> {

	List<IObCostingItemGeneralModel> findByUserCode(String userCode);
	List<IObCostingItemGeneralModel> findByUserCodeOrderByItemCodeAsc(String userCode);

}
