package com.ebs.dda.repositories.masterdata.lookups;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.masterdata.lookups.LObReason;

import java.util.List;

public interface LObReasonRep extends LocalizedLookupRep<LObReason, DefaultUserCode> {
  List<LObReason> findAllByTypeOrderByUserCodeDesc(String type);
  LObReason findOneByUserCodeAndType(String code , String type);

  LObReason findTopByType(String type);
}
