package com.ebs.dda.repositories.accounting.landedcost;

import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostCompanyData;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataRep;

public interface IObLandedCostCompanyDataRep extends IObAccountingDocumentCompanyDataRep<IObLandedCostCompanyData> {
}
