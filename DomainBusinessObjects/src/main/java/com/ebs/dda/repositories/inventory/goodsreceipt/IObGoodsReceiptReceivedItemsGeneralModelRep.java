package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptReceivedItemsGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObGoodsReceiptReceivedItemsGeneralModelRep
        extends JpaRepository<IObGoodsReceiptReceivedItemsGeneralModel, Long> {

    List<IObGoodsReceiptReceivedItemsGeneralModel> findByGoodsReceiptCodeOrderByGrItemIdDesc(
            String goodsReceiptCode);

    IObGoodsReceiptReceivedItemsGeneralModel findByGoodsReceiptCodeAndItemCode(
            String goodsReceiptCode, String itemCode);

    List<IObGoodsReceiptReceivedItemsGeneralModel> findByGoodsReceiptCode(
            String goodsReceiptCode);
}
