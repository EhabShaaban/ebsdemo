package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.OrderPaymentsGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderPaymentsGeneralModelRep extends JpaRepository<OrderPaymentsGeneralModel, Long> {
    OrderPaymentsGeneralModel findOneByPaymentCode(String paymentCode);
}
