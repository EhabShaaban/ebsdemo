package com.ebs.dda.repositories.accounting.landedcost;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;

import java.util.List;

public interface IObLandedCostDetailsGeneralModelRep extends
        InformationObjectRep<IObLandedCostDetailsGeneralModel> {

    IObLandedCostDetailsGeneralModel findOneByLandedCostCode(String userCode);
    List<IObLandedCostDetailsGeneralModel> findByPurchaseOrderCode(String purchaseOrderCode);
}
