package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueSalesInvoice;

public interface DObGoodsIssueSalesInvoiceRep
        extends DocumentObjectRep<DObGoodsIssueSalesInvoice, DefaultUserCode> {

  DObGoodsIssueSalesInvoice findOneByUserCode(String goodsIssueCode);
}
