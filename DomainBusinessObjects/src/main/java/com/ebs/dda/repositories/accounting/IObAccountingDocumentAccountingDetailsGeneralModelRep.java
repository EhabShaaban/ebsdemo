package com.ebs.dda.repositories.accounting;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

@NoRepositoryBean
public interface IObAccountingDocumentAccountingDetailsGeneralModelRep<T extends IObAccountingDocumentAccountingDetailsGeneralModel>
				extends InformationObjectRep<T> {
	T findOneByRefInstanceIdAndAccountingEntry(Long id, String accountingEntry);

	List<T> findByDocumentCodeOrderByAccountingEntryDesc(String documentCode);
	T findByDocumentCodeAndAccountingEntry(String documentCode, String accountingEntry);
}
