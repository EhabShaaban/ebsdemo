package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModel;

public interface DObGoodsReceiptDataGeneralModelRep
        extends StatefullBusinessObjectRep<DObGoodsReceiptDataGeneralModel, DefaultUserCode> {
}
