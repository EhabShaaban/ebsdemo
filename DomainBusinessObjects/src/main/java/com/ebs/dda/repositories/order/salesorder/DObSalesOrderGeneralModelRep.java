package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DObSalesOrderGeneralModelRep
        extends DocumentObjectRep<DObSalesOrderGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObSalesOrderGeneralModel> {

    @Query(
            "select s from DObSalesOrderGeneralModel s where s.purchaseUnitCode = :purchaseUnitCode and (s.currentStates like '%Approved%' or s.currentStates like '%SalesInvoiceActivated%') "
                    + "order by s.userCode DESC")
    List<DObSalesOrderGeneralModel>
    findAllApprovedAndSalesInvoiceActivatedByPurchaseUnitCodeOrderedByCodeDesc(
            @Param("purchaseUnitCode") String purchaseUnitCode);

    @Query(
            "select s from DObSalesOrderGeneralModel s where s.purchaseUnitCode = :purchaseUnitCode and s.salesResponsibleNameEn = :salesResponsibleName and (s.currentStates like '%Approved%' or s.currentStates like '%SalesInvoiceActivated%') "
                    + "order by s.userCode DESC")
    List<DObSalesOrderGeneralModel>
    findAllApprovedAndSalesInvoiceActivatedByPurchaseUnitCodeAndSalesResponsibleNameOrderedByCodeDesc(
            @Param("purchaseUnitCode") String purchaseUnitCode,
            @Param("salesResponsibleName") String salesResponsibleName);

    DObSalesOrderGeneralModel findOneByUserCodeAndCurrentStatesLike(String userCode, String State);

    DObSalesOrderGeneralModel findOneByUserCodeAndPurchaseUnitCodeAndCompanyCodeAndStoreCode(
            String userCode, String purchaseUnitCode, String companyCode, String storehouseCode);

    List<DObSalesOrderGeneralModel> findAllByCurrentStatesLikeOrCurrentStatesLikeOrderByUserCodeDesc(String firstState, String secondState);

}
