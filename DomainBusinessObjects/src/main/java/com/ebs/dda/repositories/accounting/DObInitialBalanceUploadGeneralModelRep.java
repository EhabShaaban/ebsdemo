package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUploadGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObInitialBalanceUploadGeneralModelRep extends StatefullBusinessObjectRep<DObInitialBalanceUploadGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObInitialBalanceUploadGeneralModel> {
    @Override
    Page<DObInitialBalanceUploadGeneralModel> findAll(
            Specification<DObInitialBalanceUploadGeneralModel> specification, Pageable page);

}
