package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCurrencyPriceGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DObPaymentRequestCurrencyPriceGeneralModelRep
    extends JpaRepository<DObPaymentRequestCurrencyPriceGeneralModel, Long> {
  List<DObPaymentRequestCurrencyPriceGeneralModel> findAllByReferenceDocumentCodeAndDueDocumentType(
      String referenceCode, String dueDocumentType);

  List<DObPaymentRequestCurrencyPriceGeneralModel>
      findAllByReferenceDocumentCodeAndDueDocumentTypeAndPaymentTypeNotLike(
          String referenceCode, String dueDocumentType, String paymentType);
}
