package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObGoodsIssueGeneralModelRep
    extends StatefullBusinessObjectRep<DObGoodsIssueGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObGoodsIssueGeneralModel> {

  @Override
  Page<DObGoodsIssueGeneralModel> findAll(
      Specification<DObGoodsIssueGeneralModel> specification, Pageable page);

  DObGoodsIssueGeneralModel findOneByUserCode(String goodsIssueCode);
}
