package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestSaveDetailsPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObPaymentRequestSaveDetailsPreparationGeneralModelRep
				extends JpaRepository<DObPaymentRequestSaveDetailsPreparationGeneralModel, Long> {
    DObPaymentRequestSaveDetailsPreparationGeneralModel findOneByCode(String paymentRequestCode);
}
