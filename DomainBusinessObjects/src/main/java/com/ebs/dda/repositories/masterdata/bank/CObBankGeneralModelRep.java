package com.ebs.dda.repositories.masterdata.bank;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.GeneralModelRepository;
import com.ebs.dda.jpa.masterdata.bank.CObBankGeneralModel;

public interface CObBankGeneralModelRep extends GeneralModelRepository<CObBankGeneralModel, Long> {
  CObBankGeneralModel findOneByCode(String code);
}
