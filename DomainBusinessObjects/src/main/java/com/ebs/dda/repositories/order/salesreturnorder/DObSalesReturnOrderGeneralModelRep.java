package com.ebs.dda.repositories.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObSalesReturnOrderGeneralModelRep
    extends DocumentObjectRep<DObSalesReturnOrderGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObSalesReturnOrderGeneralModel> {}
