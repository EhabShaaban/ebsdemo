package com.ebs.dda.repositories.accounting.taxes;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.accounting.taxes.LObTaxInfoGeneralModel;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface LObTaxInfoGeneralModelRep<T extends LObTaxInfoGeneralModel>
				extends LocalizedLookupRep<T, DefaultUserCode> {
}
