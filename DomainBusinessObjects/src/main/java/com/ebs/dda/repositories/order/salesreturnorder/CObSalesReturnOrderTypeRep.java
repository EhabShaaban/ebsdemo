package com.ebs.dda.repositories.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.order.salesreturnorder.CObSalesReturnOrderType;

public interface CObSalesReturnOrderTypeRep extends ConfigurationObjectRep<CObSalesReturnOrderType, DefaultUserCode> {}
