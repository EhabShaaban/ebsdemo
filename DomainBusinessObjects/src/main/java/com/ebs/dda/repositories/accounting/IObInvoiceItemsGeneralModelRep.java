package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObInvoiceItemsGeneralModel;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IObInvoiceItemsGeneralModelRep<T extends IObInvoiceItemsGeneralModel> extends InformationObjectRep<T> {
}
