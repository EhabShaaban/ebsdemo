package com.ebs.dda.repositories.inventory.initialstockupload;

import com.ebs.dda.jpa.inventory.initialstockupload.IObInitialStockUploadItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.stockavailability.IObItemQuantityGeneralModelRep;


public interface IObInitialStockUploadItemQuantityGeneralModelRep
        extends IObItemQuantityGeneralModelRep<IObInitialStockUploadItemQuantityGeneralModel> {

}
