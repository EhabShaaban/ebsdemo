package com.ebs.dda.repositories.accounting.landedcost;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsDetails;
import org.springframework.stereotype.Repository;

@Repository
public interface IObLandedCostFactorItemsDetailsRep extends InformationObjectRep<IObLandedCostFactorItemsDetails> {
}
