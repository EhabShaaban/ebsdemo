package com.ebs.dda.repositories.masterdata.bank;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.bank.CObBank;

public interface CObBankRep extends CodedBusinessObjectRep<CObBank> {}
