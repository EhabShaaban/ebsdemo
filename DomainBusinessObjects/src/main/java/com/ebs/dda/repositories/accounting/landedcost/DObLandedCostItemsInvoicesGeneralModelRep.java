package com.ebs.dda.repositories.accounting.landedcost;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostItemsInvoicesGeneralModel;

import java.util.List;

public interface DObLandedCostItemsInvoicesGeneralModelRep
				extends JpaRepository<DObLandedCostItemsInvoicesGeneralModel, Long> {
	List<DObLandedCostItemsInvoicesGeneralModel> findAllByPurchaseOrderCode(String purchaseOrderCode);
}
