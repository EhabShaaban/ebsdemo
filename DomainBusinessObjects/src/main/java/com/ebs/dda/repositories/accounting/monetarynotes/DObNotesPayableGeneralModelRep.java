package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesPayableGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObNotesPayableGeneralModelRep
    extends StatefullBusinessObjectRep<DObNotesPayableGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObNotesPayableGeneralModel> {}
