package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dda.jpa.accounting.settlements.IObSettlementCompanyDataGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataGeneralModelRep;

public interface IObSettlementCompanyDataGeneralModelRep extends
				IObAccountingDocumentCompanyDataGeneralModelRep<IObSettlementCompanyDataGeneralModel> {
}
