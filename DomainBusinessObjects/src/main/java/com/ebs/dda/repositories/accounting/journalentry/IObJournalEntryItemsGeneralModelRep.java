package com.ebs.dda.repositories.accounting.journalentry;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IObJournalEntryItemsGeneralModelRep
    extends InformationObjectRep<IObJournalEntryItemGeneralModel>,
        JpaSpecificationExecutor<IObJournalEntryItemGeneralModel> {

  List<IObJournalEntryItemGeneralModel> findAllByAccountCodeAndSubAccountCode(
      String accountCode, String subAccountCode);

  List<IObJournalEntryItemGeneralModel> findAllBySubAccountCode(String subAccountCode);

  List<IObJournalEntryItemGeneralModel> findAllByAccountCode(String accountCode);

  IObJournalEntryItemGeneralModel findFirstByCode(String journalEntryCode);
}
