package com.ebs.dda.repositories.masterdata.storehouse;

import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouse;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

public interface CObStorehouseRep extends CObEnterpriseRep<CObStorehouse> {

    CObStorehouse findOneByUserCodeAndPlantId(String userCode, Long plantId);
}
