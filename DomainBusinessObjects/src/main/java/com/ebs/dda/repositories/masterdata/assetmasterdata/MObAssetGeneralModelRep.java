package com.ebs.dda.repositories.masterdata.assetmasterdata;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.assetmasterdata.MObAssetGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MObAssetGeneralModelRep
    extends StatefullBusinessObjectRep<MObAssetGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<MObAssetGeneralModel> {}
