package com.ebs.dda.repositories.inventory.stocktransformation;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.inventory.stocktransformation.CObStockTransformationType;

public interface CObStockTransformationTypeRep
        extends ConfigurationObjectRep<CObStockTransformationType, DefaultUserCode> {
}
