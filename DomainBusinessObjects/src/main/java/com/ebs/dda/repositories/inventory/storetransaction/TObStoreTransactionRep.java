package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.TransactionObjectRep;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransaction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TObStoreTransactionRep
    extends TransactionObjectRep<TObStoreTransaction, DefaultUserCode>,
        JpaSpecificationExecutor<TObStoreTransaction> {
  @Query(
      value =
          "with recursive ancestry as (\n"
              + "    select id,\n"
              + "           transactionoperation,\n"
              + "           1 as level\n"
              + "    from tobstoretransaction\n"
              + "    where reftransactionid = ? \n"
              + "    union all\n"
              + "    select c.id, c.transactionoperation,  p.level + 1\n"
              + "    from tobstoretransaction c\n"
              + "             join ancestry p on p.id = c.reftransactionid\n"
              + ")\n"
              + "select id\n"
              + "from ancestry\n"
              + "where transactionoperation = 'ADD'\n"
              + "order by level desc",
      nativeQuery = true)
  List<Long> findAllChildrenByRefInstanceIdAndAddTransactionOperation(Long id);
}
