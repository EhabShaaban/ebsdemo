package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceEInvoiceDataGeneralModel;

public interface DObSalesInvoiceEInvoiceDataGeneralModelRep
    extends StatefullBusinessObjectRep<DObSalesInvoiceEInvoiceDataGeneralModel, DefaultUserCode> {

}
