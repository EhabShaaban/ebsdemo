package com.ebs.dda.repositories.accounting.journalentry;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObJournalEntryGeneralModelRep
    extends DocumentObjectRep<DObJournalEntryGeneralModel, DefaultUserCode>,
    JpaSpecificationExecutor<DObJournalEntryGeneralModel> {

}
