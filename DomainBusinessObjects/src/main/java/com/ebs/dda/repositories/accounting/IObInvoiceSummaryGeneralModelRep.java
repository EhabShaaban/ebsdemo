package com.ebs.dda.repositories.accounting;

import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObInvoiceSummaryGeneralModelRep
    extends JpaRepository<IObInvoiceSummaryGeneralModel, Long> {

  IObInvoiceSummaryGeneralModel findOneByInvoiceCodeAndInvoiceType(String invoiceCode, String invoiceType);

}
