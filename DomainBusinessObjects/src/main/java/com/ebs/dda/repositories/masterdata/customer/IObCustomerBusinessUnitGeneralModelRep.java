package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObCustomerBusinessUnitGeneralModelRep
    extends JpaRepository<IObCustomerBusinessUnitGeneralModel, Long> {
  IObCustomerBusinessUnitGeneralModel findOneByUserCode(String customerCode);

  List<IObCustomerBusinessUnitGeneralModel> findAllByBusinessUnitcodeOrderByUserCodeDesc(String customerCode);

  List<IObCustomerBusinessUnitGeneralModel> findAllByUserCode(String customerCode);

  IObCustomerBusinessUnitGeneralModel findOneByUserCodeAndBusinessUnitcode(String customerCode,
      String businessUnitCode);

}
