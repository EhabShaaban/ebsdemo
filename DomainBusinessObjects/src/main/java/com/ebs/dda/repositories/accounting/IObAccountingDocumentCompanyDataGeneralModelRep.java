package com.ebs.dda.repositories.accounting;

import java.util.Optional;
import org.springframework.data.repository.NoRepositoryBean;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;

@NoRepositoryBean
public interface IObAccountingDocumentCompanyDataGeneralModelRep<T extends IObAccountingDocumentCompanyDataGeneralModel>
    extends InformationObjectRep<T> {

  T findOneByDocumentCode(String documentCode);

}
