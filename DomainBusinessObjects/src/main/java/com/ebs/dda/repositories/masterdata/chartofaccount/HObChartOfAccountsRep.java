package com.ebs.dda.repositories.masterdata.chartofaccount;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.HierarchyObjectRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObGLAccount;

public interface HObChartOfAccountsRep<T extends HObGLAccount> extends
        HierarchyObjectRep<T, DefaultUserCode> {

}
