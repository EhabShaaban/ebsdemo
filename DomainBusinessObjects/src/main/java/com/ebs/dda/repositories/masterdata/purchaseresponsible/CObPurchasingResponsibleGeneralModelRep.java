package com.ebs.dda.repositories.masterdata.purchaseresponsible;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;

/** @author xerix on Tue Nov 2017 at 11 : 09 */
public interface CObPurchasingResponsibleGeneralModelRep
    extends StatefullBusinessObjectRep<CObPurchasingResponsibleGeneralModel, DefaultUserCode> {}
