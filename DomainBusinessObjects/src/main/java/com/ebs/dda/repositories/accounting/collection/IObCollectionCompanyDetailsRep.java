package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.collection.IObCollectionCompanyDetails;

public interface IObCollectionCompanyDetailsRep
        extends InformationObjectRep<IObCollectionCompanyDetails> {
}
