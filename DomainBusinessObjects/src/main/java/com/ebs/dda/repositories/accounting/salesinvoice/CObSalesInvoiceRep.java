package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.CObSalesInvoice;

public interface CObSalesInvoiceRep extends ConfigurationObjectRep<CObSalesInvoice, DefaultUserCode> {



}
