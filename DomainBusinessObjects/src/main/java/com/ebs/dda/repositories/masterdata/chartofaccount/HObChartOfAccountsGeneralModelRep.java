package com.ebs.dda.repositories.masterdata.chartofaccount;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HObChartOfAccountsGeneralModelRep
    extends StatefullBusinessObjectRep<HObChartOfAccountsGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<HObChartOfAccountsGeneralModel> {

  @Query("Select max(G.id) from HObChartOfAccountsGeneralModel G where G.parentCode = :parentCode ")
  Optional<Long> findChartOfAccountMaxIdAsHierarchy(@Param("parentCode") String parentCode);

  @Query("Select G.userCode from HObChartOfAccountsGeneralModel G where G.id=:maxChildId")
  Optional<String> findChartOfAccountCodeAsHierarchyWithMaxId(@Param("maxChildId") Long maxChildId);

  @Query(
      "Select G.userCode from HObChartOfAccountsGeneralModel G where G.id =(select max(X.id) from HObChartOfAccountsGeneralModel X where X.level = '1') ")
  Optional<String> findLastChartOfAccountCodeAsRootWithMaxId();

  List<HObChartOfAccountsGeneralModel> findAllByLevel(String level);

  @Query(
      "Select G from HObChartOfAccountsGeneralModel G where G.currentStates not like CONCAT('%',:state,'%') and G.level = :level order by G.userCode DESC")
  List<HObChartOfAccountsGeneralModel> findByCurrentStateNotContainsAndLevel(
      String state, String level);

  @Query(
      "Select G from HObChartOfAccountsGeneralModel G where G.currentStates not like CONCAT('%',:state,'%') and G.level = :level and G.userCode = :code order by G.userCode DESC")
  HObChartOfAccountsGeneralModel findByCurrentStateNotContainsAndLevelAndCode(
      @Param("state") String state, @Param("level") String level, @Param("code") String code);

  List<HObChartOfAccountsGeneralModel> findAllByParentCode(String parentCode);

  HObChartOfAccountsGeneralModel findOneByOldAccountCode(String oldAccountCode);
}
