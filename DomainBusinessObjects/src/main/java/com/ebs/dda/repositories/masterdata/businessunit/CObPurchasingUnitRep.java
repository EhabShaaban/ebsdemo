package com.ebs.dda.repositories.masterdata.businessunit;

import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

/** @author xerix on Tue Nov 2017 at 13 : 09 */
public interface CObPurchasingUnitRep extends CObEnterpriseRep<CObPurchasingUnit> {}
