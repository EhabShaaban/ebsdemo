package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.repositories.masterdata.item.MObMaterialRep;

public interface MObCustomerRep extends MObMaterialRep<MObCustomer> {
    MObCustomer findOneByOldCustomerNumber(String oldCustomerNumber);
}
