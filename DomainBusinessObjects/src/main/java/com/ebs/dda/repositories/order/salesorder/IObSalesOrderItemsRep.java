package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItem;

public interface IObSalesOrderItemsRep extends InformationObjectRep<IObSalesOrderItem> {}
