package com.ebs.dda.repositories.inventory.stocktransformation;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformation;

public interface DObStockTransformationRep
        extends DocumentObjectRep<DObStockTransformation, DefaultUserCode> {


    DObStockTransformation findOneByUserCode(String stockTransformationCode);

}


