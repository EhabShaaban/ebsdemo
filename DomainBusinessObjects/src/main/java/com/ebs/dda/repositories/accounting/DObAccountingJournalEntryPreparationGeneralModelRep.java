package com.ebs.dda.repositories.accounting;

import com.ebs.dda.jpa.accounting.DObAccountingJournalEntryPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObAccountingJournalEntryPreparationGeneralModelRep
        extends JpaRepository<DObAccountingJournalEntryPreparationGeneralModel, Long> {

    DObAccountingJournalEntryPreparationGeneralModel findOneByCodeAndObjectTypeCode(String documentCode, String objectTypeCode);
}
