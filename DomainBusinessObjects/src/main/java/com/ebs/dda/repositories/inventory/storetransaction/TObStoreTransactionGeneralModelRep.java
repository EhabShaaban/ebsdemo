package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.TransactionObjectRep;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.math.BigDecimal;
import java.util.List;

public interface TObStoreTransactionGeneralModelRep
    extends TransactionObjectRep<TObStoreTransactionGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<TObStoreTransactionGeneralModel> {

  List<TObStoreTransactionGeneralModel>
      findByItemCodeAndCompanyCodeAndPlantCodeAndStorehouseCodeAndPurchaseUnitCodeAndStockTypeAndUnitOfMeasureCodeAndBatchNoAndRemainingQuantityGreaterThanOrderByOriginalAddingDateAsc(
          String itemCode,
          String companyCode,
          String plantCode,
          String storeHouseCode,
          String purchaseUnitCode,
          String stockType,
          String unitOfMeasureCode,
          String batchNo,
          BigDecimal nonRemainingValue);

  List<TObStoreTransactionGeneralModel>
      findByItemCodeAndCompanyCodeAndPlantCodeAndStorehouseCodeAndPurchaseUnitCodeAndStockTypeAndUnitOfMeasureCode(
          String itemCode,
          String companyCode,
          String plantCode,
          String storehouseCode,
          String purchaseUnitCode,
          String stockTyp,
          String unitOfMeasureCode);
}
