package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPostingDetails;

public interface IObPaymentRequestPostingDetailsRep
    extends InformationObjectRep<IObPaymentRequestPostingDetails> {

}
