package com.ebs.dda.repositories.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IObSalesReturnOrderItemGeneralModelRep
        extends InformationObjectRep<IObSalesReturnOrderItemGeneralModel>,
        JpaSpecificationExecutor<IObSalesReturnOrderItemGeneralModel> {
    List<IObSalesReturnOrderItemGeneralModel> findBySalesReturnOrderCodeOrderByIdDesc(
            String salesReturnOrderCode);

    IObSalesReturnOrderItemGeneralModel findOneBySalesReturnOrderCodeAndId(
            String salesReturnOrderCode, long itemId);

    IObSalesReturnOrderItemGeneralModel findOneBySalesReturnOrderCodeAndItemCodeAndOrderUnitCode(
            String salesReturnOrderCode, String itemCode, String orderUnitCode);

    List<IObSalesReturnOrderItemGeneralModel>
    findAllBySalesReturnOrderCodeAndItemCodeAndOrderUnitCode(
            String salesReturnCode, String itemCode, String orderUnitCode);

    List<IObSalesReturnOrderItemGeneralModel>
    findAllBySalesReturnOrderCodeAndItemCodeOrderByOrderUnitCodeAsc(
            String salesReturnCode, String itemCode);
}
