package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObInvoiceTaxesGeneralModel;
import java.util.List;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IObInvoiceTaxesGeneralModelRep< T extends IObInvoiceTaxesGeneralModel>
    extends InformationObjectRep<T> {

  List<T> findByInvoiceCode(String invoiceCode);
  List<T> findByInvoiceCodeOrderByTaxCodeAsc(String invoiceCode);
}
