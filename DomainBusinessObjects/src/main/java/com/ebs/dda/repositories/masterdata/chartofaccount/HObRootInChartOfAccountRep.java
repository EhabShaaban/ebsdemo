package com.ebs.dda.repositories.masterdata.chartofaccount;

import com.ebs.dda.jpa.masterdata.chartofaccount.HObRootGLAccount;

public interface HObRootInChartOfAccountRep extends HObChartOfAccountsRep<HObRootGLAccount> {

}
