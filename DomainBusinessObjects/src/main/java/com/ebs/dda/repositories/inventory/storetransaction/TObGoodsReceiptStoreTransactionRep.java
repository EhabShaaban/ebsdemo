package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.TransactionObjectRep;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransaction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface TObGoodsReceiptStoreTransactionRep
    extends TransactionObjectRep<TObGoodsReceiptStoreTransaction, DefaultUserCode>,
        JpaSpecificationExecutor<TObGoodsReceiptStoreTransaction> {
  TObGoodsReceiptStoreTransaction findOneById(Long storeTransactionId);
  List<TObGoodsReceiptStoreTransaction> findByDocumentReferenceId(Long documentReferenceId);
}
