package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivableReferenceDocumentNotesReceivable;

public interface IObNotesReceivableReferenceDocumentNotesReceivableRep<
        T extends IObNotesReceivableReferenceDocumentNotesReceivable>
    extends InformationObjectRep<T> {

  T findOneByRefInstanceId(Long refInstanceId);
}
