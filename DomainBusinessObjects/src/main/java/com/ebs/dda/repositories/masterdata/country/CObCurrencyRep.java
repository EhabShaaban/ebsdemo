package com.ebs.dda.repositories.masterdata.country;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.GeneralModelRepository;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import java.util.List;

public interface CObCurrencyRep
    extends ConfigurationObjectRep<CObCurrency, DefaultUserCode>,
        GeneralModelRepository<CObCurrency, Long> {
    CObCurrency findOneByIso(String iso);
    CObCurrency findOneById(Long id);

    List<CObCurrency> findAllByIsoIsNot(String iso);
    List<CObCurrency> findAllByOrderByUserCodeAsc();
}
