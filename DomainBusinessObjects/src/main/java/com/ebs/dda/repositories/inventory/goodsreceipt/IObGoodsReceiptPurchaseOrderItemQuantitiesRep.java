package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemDetail;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderItemQuantities;

import java.util.List;

public interface IObGoodsReceiptPurchaseOrderItemQuantitiesRep
        extends BusinessObjectRep<IObGoodsReceiptPurchaseOrderItemQuantities> {
  IObGoodsReceiptPurchaseOrderItemQuantities findOneById(Long id);

  IObGoodsReceiptPurchaseOrderItemQuantities findOneByRefInstanceId(Long id);

  List<IObGoodsReceiptPurchaseOrderItemQuantities> findAllByRefInstanceId(Long id);

  IObGoodsReceiptItemDetail findOneByRefInstanceIdAndUnitOfEntryId(
          Long id, Long alternativeUnitOfMeasureId);

    List<IObGoodsReceiptItemDetail> findByRefInstanceIdAndUnitOfEntryId(Long id, Long unitOfMeasureId);
}
