package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentReferenceDocumentGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface DObPaymentReferenceDocumentGeneralModelRep<T extends DObPaymentReferenceDocumentGeneralModel>
				extends JpaRepository<T, Long> {
	T findOneByCode (String code);
}
