package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;

public interface IObMonetaryNotesDetailsGeneralModelRep
    extends InformationObjectRep<IObMonetaryNotesDetailsGeneralModel> {

  IObMonetaryNotesDetailsGeneralModel findOneByRefInstanceId(Long refInstanceId);

  IObMonetaryNotesDetailsGeneralModel findByUserCodeAndObjectTypeCode(String userCode, String objectTypeCode);
}
