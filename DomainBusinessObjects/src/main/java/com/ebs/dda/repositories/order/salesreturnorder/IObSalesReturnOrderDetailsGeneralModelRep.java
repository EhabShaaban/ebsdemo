package com.ebs.dda.repositories.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModel;

public interface IObSalesReturnOrderDetailsGeneralModelRep
    extends InformationObjectRep<IObSalesReturnOrderDetailsGeneralModel> {
  IObSalesReturnOrderDetailsGeneralModel findOneBySalesReturnOrderCode(String salesReturnOrderCode);
}
