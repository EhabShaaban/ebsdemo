package com.ebs.dda.repositories.accounting;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetailsGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;


public interface IObAccountingDocumentActivationDetailsGeneralModelRep extends JpaRepository<IObAccountingDocumentActivationDetailsGeneralModel, Long> {
    IObAccountingDocumentActivationDetailsGeneralModel findOneByCodeAndObjectTypeCode(String code, String objectTypeCode);
    IObAccountingDocumentActivationDetailsGeneralModel findOneByCodeAndDocumentType(String code, String documentType);

    IObAccountingDocumentActivationDetailsGeneralModel findOneByRefInstanceIdAndObjectTypeCode(Long refInstanceId, String documentType);
}
