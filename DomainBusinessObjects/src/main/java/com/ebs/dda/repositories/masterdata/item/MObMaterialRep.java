package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.MasterDataObjectRep;
import com.ebs.dda.jpa.masterdata.MasterDataMinor;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 31, 2017 , 10:31:40 AM
 */
public interface MObMaterialRep<T extends MasterDataMinor>
    extends MasterDataObjectRep<T, DefaultUserCode> {}
