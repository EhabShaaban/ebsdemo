package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep
        extends JpaRepository<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel, Long> {

  List<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel> findByBatchedItemIdOrderByIdDesc(
          Long batchedItemId);

  List<IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel> findByGoodsReceiptCodeAndItemCode(
          String goodsReceiptCode, String itemCode);

  IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel
  findByGoodsReceiptCodeAndItemCodeAndUnitOfEntryCodeAndStockTypeAndBatchCode(
          String goodsReceiptCode,
          String itemCode,
          String unitOfEntryCode,
          String name,
          String batchCode);
}
