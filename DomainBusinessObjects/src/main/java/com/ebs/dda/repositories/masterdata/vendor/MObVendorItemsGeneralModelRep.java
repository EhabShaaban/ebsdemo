package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.MObVendorItemsGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MObVendorItemsGeneralModelRep extends JpaRepository<MObVendorItemsGeneralModel, Long> {

    List<MObVendorItemsGeneralModel> findByVendorCodeAndBusinessUnitCodeOrderByItemCodeDesc(String vendorCode, String businessUnitCode);
    List<MObVendorItemsGeneralModel> findByVendorCodeAndBusinessUnitCodeAndItemCodeOrderByIvrMeasureCodeDesc(String vendorCode, String businessUnitCode, String itemCode);
}
