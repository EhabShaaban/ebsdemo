package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceSummary;

public interface IObSalesInvoiceSummaryRep extends InformationObjectRep<IObSalesInvoiceSummary> {

  IObSalesInvoiceSummary findOneByRefInstanceId(Long refInstanceId);
}
