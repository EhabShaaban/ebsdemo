package com.ebs.dda.repositories.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.TreasuryBalanceGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TreasuryBalanceGeneralModelRep
    extends JpaRepository<TreasuryBalanceGeneralModel, Long> {
  TreasuryBalanceGeneralModel
      findByBusinessUnitCodeAndCompanyCodeAndTreasuryCodeAndCurrencyCodeAndFiscalYearCode(
          String businessUnitCode,
          String companyCode,
          String treasuryCode,
          String currencyCode,
          String fiscalYearCode);
}
