package com.ebs.dda.repositories.inventory;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryActivationDetails;

public interface IObInventoryActivationDetailsRep extends InformationObjectRep<IObInventoryActivationDetails> {

}
