package com.ebs.dda.repositories.inventory.stocktransformation;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObStockTransformationGeneralModelRep extends StatefullBusinessObjectRep<DObStockTransformationGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObStockTransformationGeneralModel> {

    @Override
    Page<DObStockTransformationGeneralModel> findAll(
            Specification<DObStockTransformationGeneralModel> specification, Pageable page);

    DObStockTransformationGeneralModel findOneByUserCode(String stockTransformationCode);
}
