package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;

public interface DObPaymentRequestRep extends DocumentObjectRep<DObPaymentRequest, DefaultUserCode> {
}
