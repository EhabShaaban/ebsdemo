package com.ebs.dda.repositories.masterdata.treasury;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.company.CObTreasury;

public interface CObTreasuryRep extends CodedBusinessObjectRep<CObTreasury> {
}
