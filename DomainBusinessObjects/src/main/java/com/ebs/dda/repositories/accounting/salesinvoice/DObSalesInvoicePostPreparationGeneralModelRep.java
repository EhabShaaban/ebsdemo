package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoicePostPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObSalesInvoicePostPreparationGeneralModelRep
				extends JpaRepository<DObSalesInvoicePostPreparationGeneralModel, Long> {

	DObSalesInvoicePostPreparationGeneralModel findOneByCode (String code);
}
