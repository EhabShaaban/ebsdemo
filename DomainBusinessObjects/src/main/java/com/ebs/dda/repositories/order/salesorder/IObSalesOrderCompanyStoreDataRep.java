package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCompanyStoreData;

public interface IObSalesOrderCompanyStoreDataRep
    extends InformationObjectRep<IObSalesOrderCompanyStoreData> {}
