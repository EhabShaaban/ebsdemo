package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface MObCustomerGeneralModelRep
    extends StatefullBusinessObjectRep<MObCustomerGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<MObCustomerGeneralModel> {}
