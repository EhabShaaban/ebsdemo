package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 4, 2021
 */
public interface IObSettlementDetailsGeneralModelRep
    extends InformationObjectRep<IObSettlementDetailsGeneralModel> {

  IObSettlementDetailsGeneralModel findOneBySettlementCode(String settlementCode);

}
