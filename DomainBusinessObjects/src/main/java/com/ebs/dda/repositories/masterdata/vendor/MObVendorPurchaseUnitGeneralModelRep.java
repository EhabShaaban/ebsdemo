package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;

import java.util.List;

public interface MObVendorPurchaseUnitGeneralModelRep
    extends StatefullBusinessObjectRep<MObVendorPurchaseUnitGeneralModel, DefaultUserCode> {

  List<MObVendorPurchaseUnitGeneralModel> findAllByUserCode(String userCode);

  MObVendorPurchaseUnitGeneralModel findOneByUserCodeAndPurchaseUnitCode(String userCode, String PurchaseUnitCode);

}