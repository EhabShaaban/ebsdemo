package com.ebs.dda.repositories.masterdata.lookups;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.masterdata.lookups.LObIssueFrom;

import java.util.List;

public interface LObIssueFromRep extends LocalizedLookupRep<LObIssueFrom, DefaultUserCode> {
  List<LObIssueFrom> findAllByOrderByUserCodeDesc();
}
