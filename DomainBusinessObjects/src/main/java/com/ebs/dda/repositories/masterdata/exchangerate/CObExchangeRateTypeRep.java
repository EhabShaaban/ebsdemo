package com.ebs.dda.repositories.masterdata.exchangerate;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateType;

/**
 * @author Ahmed Ali Elfeky
 * @since Dec 28, 2020
 */
public interface CObExchangeRateTypeRep
    extends ConfigurationObjectRep<CObExchangeRateType, DefaultUserCode> {

  CObExchangeRateType findOneByIsDefaultIsTrue();

}
