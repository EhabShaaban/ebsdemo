package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsDownPayment;

public interface IObVendorInvoiceDetailsDownPaymentRep
		extends
			InformationObjectRep<IObVendorInvoiceDetailsDownPayment> {

	IObVendorInvoiceDetailsDownPayment findByRefInstanceIdAndDownPaymentId(
			Long refInstanceId, Long downPaymentId);

	void deleteAllByRefInstanceId(Long refInstanceId);


}
