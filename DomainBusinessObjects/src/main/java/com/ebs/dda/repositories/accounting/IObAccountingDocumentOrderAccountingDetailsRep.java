package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentOrderAccountingDetails;

public interface IObAccountingDocumentOrderAccountingDetailsRep extends InformationObjectRep<IObAccountingDocumentOrderAccountingDetails> {
}
