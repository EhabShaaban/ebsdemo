package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceItemsGeneralModelRep;
import java.util.List;

public interface IObSalesInvoiceItemsGeneralModelRep
				extends IObInvoiceItemsGeneralModelRep<IObSalesInvoiceItemsGeneralModel> {
	List<IObSalesInvoiceItemsGeneralModel> findByInvoiceCodeOrderByIdDesc(String invoiceCode);

	List<IObSalesInvoiceItemsGeneralModel> findByInvoiceCodeOrderByIdAsc(String invoiceCode);

	List<IObSalesInvoiceItemsGeneralModel> findByInvoiceCode(String invoiceCode);

}
