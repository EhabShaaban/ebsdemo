package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IObGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep
        extends CrudRepository<IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData, Long> {

  IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData findOneById(Long id);

  List<IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData> findAllByRefInstanceIdAndIdNot(
          Long refInstanceId, Long id);
}
