package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObGoodsIssueItemGeneralModelRep
        extends JpaRepository<IObGoodsIssueItemGeneralModel, Long> {

   List<IObGoodsIssueItemGeneralModel> findByGoodsIssueCodeOrderByIdDesc(String goodsIssueCode);

   List<IObGoodsIssueItemGeneralModel> findByGoodsIssueCode(String goodsIssueCode);
}
