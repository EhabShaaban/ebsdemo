package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceItemsEInvoiceDataGeneralModel;

public interface DObSalesInvoiceItemsEInvoiceDataGeneralModelRep
    extends InformationObjectRep<DObSalesInvoiceItemsEInvoiceDataGeneralModel> {
}
