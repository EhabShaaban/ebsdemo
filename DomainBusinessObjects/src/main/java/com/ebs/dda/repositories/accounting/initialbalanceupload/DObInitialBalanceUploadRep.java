package com.ebs.dda.repositories.accounting.initialbalanceupload;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUpload;

public interface DObInitialBalanceUploadRep
    extends DocumentObjectRep<DObInitialBalanceUpload, DefaultUserCode> {}
