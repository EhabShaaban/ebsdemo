package com.ebs.dda.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingCompanyDataGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataGeneralModelRep;

public interface IObCostingCompanyDataGeneralModelRep
    extends IObAccountingDocumentCompanyDataGeneralModelRep<IObCostingCompanyDataGeneralModel> {}
