package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.customer.CObCustomer;

public interface CObCustomerRep extends ConfigurationObjectRep<CObCustomer, DefaultUserCode> {

}
