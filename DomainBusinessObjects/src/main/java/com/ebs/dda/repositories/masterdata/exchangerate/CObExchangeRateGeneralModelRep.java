package com.ebs.dda.repositories.masterdata.exchangerate;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateGeneralModel;

public interface CObExchangeRateGeneralModelRep
    extends StatefullBusinessObjectRep<CObExchangeRateGeneralModel, DefaultUserCode>,
    JpaSpecificationExecutor<CObExchangeRateGeneralModel> {

  @Query(nativeQuery = true, 
      value = "SELECT * FROM CObExchangeRateGeneralModel "
            + "WHERE firstCurrencyIso = ? "
            + "AND  secondCurrencyIso = ? "
            + "AND exchangeType = (SELECT code FROM CObExchangeRateType WHERE isDefault IS true) "
            + "ORDER BY id DESC "
            + "LIMIT 1")
  CObExchangeRateGeneralModel findLatestDefaultExchangeRate(String firstCurrencyIso, String secondCurrencyIso);


  CObExchangeRateGeneralModel findTopByFirstCurrencyIdAndExchangeRateTypeOrderByCreationDateDesc(
      Long firstCurrencyId, String exRateType);
}
