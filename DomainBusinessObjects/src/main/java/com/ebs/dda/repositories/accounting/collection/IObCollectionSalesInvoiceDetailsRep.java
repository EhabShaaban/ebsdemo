package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dda.jpa.accounting.collection.IObCollectionSalesInvoiceDetails;

public interface IObCollectionSalesInvoiceDetailsRep
    extends IObCollectionDetailsRep<IObCollectionSalesInvoiceDetails> {

}
