package com.ebs.dda.repositories.inventory;

import com.ebs.dda.jpa.inventory.inventorydocument.IObInventoryCompanyGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObInventoryCompanyGeneralModelRep
    extends JpaRepository<IObInventoryCompanyGeneralModel, Long> {

  IObInventoryCompanyGeneralModel findOneByInventoryDocumentCodeAndInventoryDocumentType(String inventoryDocumentCode, String inventoryDocumentType);
}
