package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObSalesInvoiceJournalEntryPreparationGeneralModelRep
				extends JpaRepository<DObSalesInvoiceJournalEntryPreparationGeneralModel, Long> {
	DObSalesInvoiceJournalEntryPreparationGeneralModel findOneByCode(String userCode);
}
