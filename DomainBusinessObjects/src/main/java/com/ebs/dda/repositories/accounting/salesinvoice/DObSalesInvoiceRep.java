package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;

public interface DObSalesInvoiceRep extends DocumentObjectRep<DObSalesInvoice, DefaultUserCode> {

  DObSalesInvoice findOneByUserCode(String userCode);
}
