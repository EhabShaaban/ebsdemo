package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dda.jpa.accounting.settlements.IObSettlementCompanyData;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataRep;

public interface IObSettlementCompanyDataRep
    extends IObAccountingDocumentCompanyDataRep<IObSettlementCompanyData> {}
