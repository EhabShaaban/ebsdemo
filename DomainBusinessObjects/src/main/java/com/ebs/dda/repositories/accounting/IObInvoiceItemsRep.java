package com.ebs.dda.repositories.accounting;

import org.springframework.data.repository.NoRepositoryBean;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObInvoiceItem;

@NoRepositoryBean
public interface IObInvoiceItemsRep<T extends IObInvoiceItem> extends InformationObjectRep<T> {
}
