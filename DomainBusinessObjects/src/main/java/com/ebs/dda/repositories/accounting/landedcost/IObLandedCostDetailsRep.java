package com.ebs.dda.repositories.accounting.landedcost;

import org.springframework.stereotype.Repository;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetails;

@Repository
public interface IObLandedCostDetailsRep extends InformationObjectRep<IObLandedCostDetails> {

}
