package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceCompanyDataGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataGeneralModelRep;

public interface IObSalesInvoiceCompanyDataGeneralModelRep extends
    IObAccountingDocumentCompanyDataGeneralModelRep<IObSalesInvoiceCompanyDataGeneralModel> {
}
