package com.ebs.dda.repositories.accounting.taxes;

import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxesGeneralModel;

import java.util.List;

public interface LObCompanyTaxesGeneralModelRep extends LObTaxInfoGeneralModelRep<LObCompanyTaxesGeneralModel> {
    List<LObCompanyTaxesGeneralModel> findAllByCompanyCode(String companyCode);
    List<LObCompanyTaxesGeneralModel> findByCompanyCodeOrderByUserCodeAsc(String companyCode);
    LObCompanyTaxesGeneralModel findOneByCompanyCodeAndUserCode(String companyCode , String taxCode);
}
