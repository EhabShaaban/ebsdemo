package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dda.jpa.inventory.storetransaction.ITObStoreTransactionGeneralModel;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;


public class ReadStoreTransactionSpecification {

    private final String DAMAGED_STOCK_TYPE = "DAMAGED_STOCK";
    private final String UNRESTRICTED_STOCK_TYPE = "UNRESTRICTED_USE";

    public Specification<TObStoreTransactionGeneralModel> isAllowedStoreTransaction(String businessUnitCode) {
        return new Specification<TObStoreTransactionGeneralModel>() {
            @Override
            public Predicate toPredicate(
                    Root<TObStoreTransactionGeneralModel> root,
                    CriteriaQuery<?> query,
                    CriteriaBuilder criteriaBuilder) {

                Predicate zeroRemainingQuantity = whenRemainingGreaterThanZero(root, criteriaBuilder);
                Predicate nullRemainingQuantity = whenRemainingIsNotNull(root, criteriaBuilder);
                Predicate remainingQuantity =
                        criteriaBuilder.and(zeroRemainingQuantity, nullRemainingQuantity);

                Predicate damagedStockType = whenStockTypeIs(root, criteriaBuilder, DAMAGED_STOCK_TYPE);
                Predicate unrestrictedStockType =
                        whenStockTypeIs(root, criteriaBuilder, UNRESTRICTED_STOCK_TYPE);
                Predicate stockTypes = criteriaBuilder.or(damagedStockType, unrestrictedStockType);
                Predicate purchaseUnit = isChoosenPurchaseUnit(root, criteriaBuilder, businessUnitCode);

                return criteriaBuilder.and(remainingQuantity, stockTypes, purchaseUnit);
            }
        };
    }

    private Predicate whenRemainingGreaterThanZero(
            Root<TObStoreTransactionGeneralModel> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.greaterThan(
                root.get(ITObStoreTransactionGeneralModel.REMAINING_QUANTITY), 0);
    }

    private Predicate whenRemainingIsNotNull(
            Root<TObStoreTransactionGeneralModel> root, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.isNotNull(root.get(ITObStoreTransactionGeneralModel.REMAINING_QUANTITY));
    }

    private Predicate whenStockTypeIs(
            Root<TObStoreTransactionGeneralModel> root,
            CriteriaBuilder criteriaBuilder,
            String stockType) {
        return criteriaBuilder.equal(root.get(ITObStoreTransactionGeneralModel.STOCK_TYPE), stockType);
    }

    public Predicate isChoosenPurchaseUnit(Root<TObStoreTransactionGeneralModel> root,
                                           CriteriaBuilder criteriaBuilder,
                                           String businessUnitCode) {
        return criteriaBuilder.equal(root.get(ITObStoreTransactionGeneralModel.PURCHASEUNIT_CODE), businessUnitCode);

    }
}

