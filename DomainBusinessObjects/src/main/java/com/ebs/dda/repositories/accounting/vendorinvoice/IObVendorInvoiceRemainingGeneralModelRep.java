package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceRemainingGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IObVendorInvoiceRemainingGeneralModelRep
        extends DocumentObjectRep<IObVendorInvoiceRemainingGeneralModel, DefaultUserCode>,
        StatefullBusinessObjectRep<IObVendorInvoiceRemainingGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<IObVendorInvoiceRemainingGeneralModel> {

	IObVendorInvoiceRemainingGeneralModel findOneByPurchaseOrderCodeAndInvoiceTypeIsIn(String purchaseOrderCode , String[] invoiceTypes
																				   );
}
