package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.inventory.goodsissue.CObGoodsIssue;

public interface CObGoodsIssueRep extends ConfigurationObjectRep<CObGoodsIssue, DefaultUserCode> {

}
