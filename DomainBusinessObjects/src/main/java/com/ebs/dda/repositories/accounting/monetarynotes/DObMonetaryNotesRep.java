package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;

public interface DObMonetaryNotesRep extends DocumentObjectRep<DObMonetaryNotes, DefaultUserCode> {

  DObMonetaryNotes findOneByUserCode(String userCode);

}
