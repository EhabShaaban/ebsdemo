package com.ebs.dda.repositories.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.lookups.LObContainerRequirement;
import com.ebs.dda.repositories.masterdata.item.LObMaterialRep;

public interface LObContainerRequirementRep extends LObMaterialRep<LObContainerRequirement> {}
