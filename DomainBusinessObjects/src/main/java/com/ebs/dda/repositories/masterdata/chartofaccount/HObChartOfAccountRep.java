package com.ebs.dda.repositories.masterdata.chartofaccount;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObGLAccount;

public interface HObChartOfAccountRep extends StatefullBusinessObjectRep<HObGLAccount, DefaultUserCode> {

    void deleteByUserCode(String userCode);
}
