package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;

public interface IObSalesInvoiceBusinessPartnerGeneralModelRep
    extends InformationObjectRep<IObSalesInvoiceBusinessPartnerGeneralModel> {

  IObSalesInvoiceBusinessPartnerGeneralModel findOneByCodeAndAndBusinessPartnerCode(
      String salesInvoiceCode, String businessPartnerCode);

  IObSalesInvoiceBusinessPartnerGeneralModel findOneByCode(String salesInvoiceCode);
}
