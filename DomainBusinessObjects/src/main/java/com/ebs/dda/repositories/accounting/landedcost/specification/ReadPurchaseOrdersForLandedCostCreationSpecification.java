package com.ebs.dda.repositories.accounting.landedcost.specification;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.landedcost.ICObLandedCostType;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class ReadPurchaseOrdersForLandedCostCreationSpecification {

  public Specification<IObOrderCompanyGeneralModel> isAllowedType(String landedCostType) {
    return new Specification<IObOrderCompanyGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<IObOrderCompanyGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {
        String orderTypeCode = getPurchaseOrderType(landedCostType);
        return criteriaBuilder.equal(
            root.get(IDObPurchaseOrderGeneralModel.OBJECT_TYPE_CODE), orderTypeCode);
      }
    };
  }

  private String getPurchaseOrderType(String landedCostType) {
    if (landedCostType.equals(ICObLandedCostType.IMPORT)) {
      return OrderTypeEnum.IMPORT_PO.name();
    }
    if (landedCostType.equals(ICObLandedCostType.LOCAL)) {
      return OrderTypeEnum.LOCAL_PO.name();
    }
    throw new IllegalArgumentException("Not Supported Landed Cost Type");
  }

  public Specification<IObOrderCompanyGeneralModel> isAllowedState() {
    return new Specification<IObOrderCompanyGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<IObOrderCompanyGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {
        Predicate isOpenForUpdates = whenStateIs(root, criteriaBuilder, "OpenForUpdates");
        Predicate isWaitungForApproval = whenStateIs(root, criteriaBuilder, "WaitingApproval");
        Predicate isApproved = whenStateIs(root, criteriaBuilder, "Approved");
        Predicate isConfirmed = whenStateIs(root, criteriaBuilder, "Confirmed");
        Predicate isFinishedProduction = whenStateIs(root, criteriaBuilder, "FinishedProduction");
        Predicate isShipped = whenStateIs(root, criteriaBuilder, "Shipped");
        Predicate isArrived = whenStateIs(root, criteriaBuilder, "Arrived");
        Predicate isCleared = whenStateIs(root, criteriaBuilder, "Cleared");
        Predicate isDeliveryComplete = whenStateIs(root, criteriaBuilder, "DeliveryComplete");
        Predicate isActive = whenStateIs(root, criteriaBuilder, "Active");

        Predicate allowedStates =
            criteriaBuilder.or(
                isOpenForUpdates,
                isWaitungForApproval,
                isApproved,
                isConfirmed,
                isFinishedProduction,
                isShipped,
                isArrived,
                isCleared,
                isDeliveryComplete,
                isActive);

        return criteriaBuilder.and(allowedStates);
      }
    };
  }

  private Predicate whenStateIs(
      Root<IObOrderCompanyGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }

  public Specification<IObOrderCompanyGeneralModel> isCompanyExist() {
    return new Specification<IObOrderCompanyGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<IObOrderCompanyGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        return criteriaBuilder.isNotNull(root.get(IDObPurchaseOrderGeneralModel.COMPANY_CODE));
      }
    };
  }

  public Specification<IObOrderCompanyGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<IObOrderCompanyGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<IObOrderCompanyGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<IObOrderCompanyGeneralModel> poHasNotPostedLandedCost(List<String> poCodes) {
    return new Specification<IObOrderCompanyGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<IObOrderCompanyGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        return root.get(IDObPurchaseOrderGeneralModel.CODE).in(poCodes).not();
      }
    };
  }
}
