package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface DObVendorInvoiceGeneralModelRep
        extends DocumentObjectRep<DObVendorInvoiceGeneralModel, DefaultUserCode>, StatefullBusinessObjectRep<DObVendorInvoiceGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObVendorInvoiceGeneralModel> {

  List<DObVendorInvoiceGeneralModel> findAllByInvoiceTypeAndPurchaseOrderCode(String invoiceType,
                                                                                  String purchaseOrderCode);

  DObVendorInvoiceGeneralModel findOneByPurchaseOrderCode(String purchaseOrderCode);
  DObVendorInvoiceGeneralModel findOneByPurchaseOrderCodeAndInvoiceTypeAndAndCurrentStatesLike(String poCode, String DocumentType, String state);

}
