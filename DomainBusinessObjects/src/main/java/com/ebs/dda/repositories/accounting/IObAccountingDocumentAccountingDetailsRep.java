package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;

import java.util.List;

public interface IObAccountingDocumentAccountingDetailsRep
    extends InformationObjectRep<IObAccountingDocumentAccountingDetails> {

  <T extends IObAccountingDocumentAccountingDetails> T findOneByRefInstanceIdAndAccountingEntryAndObjectTypeCode(
      Long id, String accountingEntry, String objectTypeCode);

  <T extends IObAccountingDocumentAccountingDetails> List<T> findByRefInstanceIdAndAccountingEntryAndObjectTypeCode(
      Long id, String accountingEntry, String objectTypeCode);
}
