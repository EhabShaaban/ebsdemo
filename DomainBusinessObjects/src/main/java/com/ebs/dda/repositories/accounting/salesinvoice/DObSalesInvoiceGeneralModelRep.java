package com.ebs.dda.repositories.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface DObSalesInvoiceGeneralModelRep
    extends StatefullBusinessObjectRep<DObSalesInvoiceGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObSalesInvoiceGeneralModel> {

  List<DObSalesInvoiceGeneralModel> findAllByPurchaseUnitCodeAndCurrentStatesLikeOrderByIdDesc(
      String purchaseUnitCode, String state);

  List<DObSalesInvoiceGeneralModel>
      findAllByPurchaseUnitCodeAndCustomerCodeAndCurrentStatesLikeAndCurrentStatesLikeOrderByUserCodeDesc(
          String purchaseUnitCode, String customerCode, String firstState, String secondState);

  List<DObSalesInvoiceGeneralModel>
      findAllByCurrentStatesLikeAndCurrentStatesLikeOrderByUserCodeDesc(
          String firstState, String secondState);

  List<DObSalesInvoiceGeneralModel>
      findAllByPurchaseUnitCodeAndCustomerCodeAndCurrentStatesLikeOrderByIdDesc(
          String purchaseUnitCode, String customerCode, String state);

  DObSalesInvoiceGeneralModel findOneByUserCodeAndPurchaseUnitCode(
      String invoiceCode, String purchaseUnitCode);

  DObSalesInvoiceGeneralModel findOneByUserCodeAndCompanyCode(
      String invoiceCode, String companyCode);

  List<DObSalesInvoiceGeneralModel> findByPurchaseUnitCodeAndCustomerCodeAndCurrentStatesLike(
      String PurchaseUnitCode, String CustomerCode, String State);

  DObSalesInvoiceGeneralModel findOneByUserCodeAndCurrentStatesLike(
      String invoiceCode, String currentState);

  @Override
  Page<DObSalesInvoiceGeneralModel> findAll(
      Specification<DObSalesInvoiceGeneralModel> specification, Pageable page);

  DObSalesInvoiceGeneralModel findOneByUserCodeAndInvoiceTypeCodeAndCurrentStatesLike(
      String invoiceCode, String invoiceTypeCode, String currentState);
}
