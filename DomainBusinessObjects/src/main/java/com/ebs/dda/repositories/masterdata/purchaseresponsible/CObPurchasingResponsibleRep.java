package com.ebs.dda.repositories.masterdata.purchaseresponsible;

import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsible;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

/** @author xerix on Tue Nov 2017 at 13 : 10 */
public interface CObPurchasingResponsibleRep extends CObEnterpriseRep<CObPurchasingResponsible> {}
