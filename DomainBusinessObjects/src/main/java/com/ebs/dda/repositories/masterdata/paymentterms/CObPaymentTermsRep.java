package com.ebs.dda.repositories.masterdata.paymentterms;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;

public interface CObPaymentTermsRep
    extends ConfigurationObjectRep<CObPaymentTerms, DefaultUserCode> {
  CObPaymentTerms findOneByUserCode(String code);
}
