package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorPurchaseUnit;

public interface IObVendorPurchaseUnitRep extends InformationObjectRep<IObVendorPurchaseUnit> {

  IObVendorPurchaseUnit findByRefInstanceIdAndPurchaseUnitId(
      Long refInstanceId, Long purchaseUnitId);
}
