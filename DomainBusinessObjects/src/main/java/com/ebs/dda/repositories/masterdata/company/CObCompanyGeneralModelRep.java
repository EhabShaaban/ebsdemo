package com.ebs.dda.repositories.masterdata.company;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.company.CObCompanyGeneralModel;

public interface CObCompanyGeneralModelRep
    extends StatefullBusinessObjectRep<CObCompanyGeneralModel, DefaultUserCode> {}
