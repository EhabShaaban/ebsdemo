package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnItemQuantities;

public interface IObGoodsReceiptSalesReturnItemQuantitiesRep
        extends BusinessObjectRep<IObGoodsReceiptSalesReturnItemQuantities> {
  IObGoodsReceiptSalesReturnItemQuantities findOneById(Long id);

  IObGoodsReceiptSalesReturnItemQuantities findOneByRefInstanceId(Long id);
}
