package com.ebs.dda.repositories.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.TransactionObjectRep;
import com.ebs.dda.jpa.inventory.storetransaction.TObGoodsReceiptStoreTransactionGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface TObGoodsReceiptStoreTransactionGeneralModelRep
    extends TransactionObjectRep<TObGoodsReceiptStoreTransactionGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<TObGoodsReceiptStoreTransactionGeneralModel> {
  List<TObGoodsReceiptStoreTransactionGeneralModel> findByGoodsReceiptCode(String goodsReceiptCode);
}
