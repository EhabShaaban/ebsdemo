package com.ebs.dda.repositories.accounting.costing;

import com.ebs.dda.jpa.accounting.costing.IObCostingAccountingDetailsGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IObCostingAccountingDetailsGeneralModelRep
        extends JpaRepository<IObCostingAccountingDetailsGeneralModel, Long> {

    List<IObCostingAccountingDetailsGeneralModel> findByGoodsReceiptCodeOrderByAccountingEntryDesc(
            String goodsReceiptCode);

    List<IObCostingAccountingDetailsGeneralModel> findByRefInstanceId(Long refInstanceId);
}
