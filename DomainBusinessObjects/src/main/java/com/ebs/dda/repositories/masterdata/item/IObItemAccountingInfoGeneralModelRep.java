package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;

public interface IObItemAccountingInfoGeneralModelRep extends BusinessObjectRep<IObItemAccountingInfoGeneralModel> {
    IObItemAccountingInfoGeneralModel findByItemCode(String itemCode);
}
