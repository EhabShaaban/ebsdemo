package com.ebs.dda.repositories.accounting.landedcost;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsDetailsGeneralModel;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 18, 2021
 */
public interface IObLandedCostFactorItemsDetailsGeneralModelRep
    extends InformationObjectRep<IObLandedCostFactorItemsDetailsGeneralModel> {

}
