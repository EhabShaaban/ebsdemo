package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentBankAccountingDetails;


public interface IObAccountingDocumentBankAccountingDetailsRep
				extends InformationObjectRep<IObAccountingDocumentBankAccountingDetails> {
}
