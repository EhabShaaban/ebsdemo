package com.ebs.dda.repositories.accounting.journalentry;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.journalentry.DObLandedCostJournalEntry;

public interface DObLandedCostJournalEntryRep
    extends DocumentObjectRep<DObLandedCostJournalEntry, DefaultUserCode> {}
