package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObGoodsReceiptSalesReturnDataGeneralModelRep
    extends JpaRepository<IObGoodsReceiptSalesReturnDataGeneralModel, Long> {
    IObGoodsReceiptSalesReturnDataGeneralModel findOneByGoodsReceiptCode(String goodsReceiptCode);

}
