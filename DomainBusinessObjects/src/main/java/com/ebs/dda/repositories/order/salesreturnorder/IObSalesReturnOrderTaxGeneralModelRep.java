package com.ebs.dda.repositories.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderTaxGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface IObSalesReturnOrderTaxGeneralModelRep
    extends InformationObjectRep<IObSalesReturnOrderTaxGeneralModel>,
        JpaSpecificationExecutor<IObSalesReturnOrderTaxGeneralModel> {
  List<IObSalesReturnOrderTaxGeneralModel> findBySalesReturnOrderCodeOrderByCodeAsc(
      String salesReturnOrderCode);
}
