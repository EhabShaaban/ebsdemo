package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetails;

public interface IObMonetaryNotesDetailsRep<T extends IObMonetaryNotesDetails>
    extends InformationObjectRep<T> {
  T findOneByRefInstanceId(Long refInstanceId);
}
