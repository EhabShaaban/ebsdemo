package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesReferenceDocuments;

public interface IObMonetaryNotesReferenceDocumentsRep<T extends IObMonetaryNotesReferenceDocuments>
    extends InformationObjectRep<T> {

  T findOneByRefInstanceId(Long refInstanceId);
}
