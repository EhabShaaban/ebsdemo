package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.masterdata.item.LObMaterial;

public interface LObMaterialRep<T extends LObMaterial>
    extends LocalizedLookupRep<T, DefaultUserCode> {}
