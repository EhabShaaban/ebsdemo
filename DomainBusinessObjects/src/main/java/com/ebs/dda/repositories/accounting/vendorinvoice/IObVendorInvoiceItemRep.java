package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItem;
import com.ebs.dda.repositories.accounting.IObInvoiceItemsRep;

public interface IObVendorInvoiceItemRep extends IObInvoiceItemsRep<IObVendorInvoiceItem> {

}
