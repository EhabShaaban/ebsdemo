package com.ebs.dda.repositories.accounting.initialbalanceupload;

import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DObInitialBalanceUploadJournalEntryItemPreparationGeneralModelRep
				extends JpaRepository<DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel, Long> {

	List<DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel> findByCode(String initialBalanceUploadCode);
}
