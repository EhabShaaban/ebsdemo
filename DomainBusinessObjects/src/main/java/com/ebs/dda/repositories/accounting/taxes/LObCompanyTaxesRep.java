package com.ebs.dda.repositories.accounting.taxes;

import org.springframework.stereotype.Repository;
import com.ebs.dda.jpa.accounting.taxes.LObCompanyTaxes;

import java.util.List;

@Repository
public interface LObCompanyTaxesRep extends LObTaxInfoRep<LObCompanyTaxes> {
  List<LObCompanyTaxes> findAllByCompanyId(Long companyId);
}
