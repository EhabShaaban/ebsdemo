package com.ebs.dda.repositories.masterdata.plant;

import com.ebs.dda.jpa.masterdata.plant.CObPlant;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

public interface CObPlantRep extends CObEnterpriseRep<CObPlant> {

  CObPlant findOneByUserCodeAndCompanyId(String userCode, Long CompanyId);
  CObPlant findOneByCompanyId(Long CompanyId);
}
