package com.ebs.dda.repositories.masterdata.ivr;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;

/**
 * @author: Maged Zakzouk
 * @date: Nov 1, 2017
 */
public interface ItemVendorRecordRep extends CodedBusinessObjectRep<ItemVendorRecord> {
    ItemVendorRecord findOneByItemId(Long itemId);
    ItemVendorRecord findOneByOldItemNumber(String oldItemNumber);
}
