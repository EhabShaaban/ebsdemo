package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.item.MObItemAuthorizationGeneralModel;
import java.util.List;

public interface MObItemAuthorizationGeneralModelRep
    extends StatefullBusinessObjectRep<MObItemAuthorizationGeneralModel, DefaultUserCode> {

  List<MObItemAuthorizationGeneralModel> findAllByUserCode(String userCode);
}
