package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.itemgroup.CObItemGroup;

public interface CObMaterialGroupRep extends CodedBusinessObjectRep<CObItemGroup> {
}
