package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataGeneralModelRep;

public interface IObPaymentRequestCompanyDataGeneralModelRep
    extends
    IObAccountingDocumentCompanyDataGeneralModelRep<IObPaymentRequestCompanyDataGeneralModel> {}
