package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;

import java.util.List;

public interface IObNotesReceivablesReferenceDocumentGeneralModelRep
    extends InformationObjectRep<IObNotesReceivablesReferenceDocumentGeneralModel> {

  List<IObNotesReceivablesReferenceDocumentGeneralModel> findByNotesReceivableCodeOrderByIdAsc(
      String notesReceivableCode);

  List<IObNotesReceivablesReferenceDocumentGeneralModel>
      findByNotesReceivableCodeAndRefDocumentCodeAndDocumentTypeOrderByIdAsc(
          String notesReceivableCode, String refDocumentCode, String DocumentType);

    List<IObNotesReceivablesReferenceDocumentGeneralModel> findByRefDocumentCodeAndDocumentTypeAndCurrentStatesLike(
            String refDocumentCode, String refDocumentTypeCode, String state);

    List<IObNotesReceivablesReferenceDocumentGeneralModel> findAllByNotesReceivableCodeAndDocumentType(
            String notesReceivableCode, String refDocumentTypeCode);
}
