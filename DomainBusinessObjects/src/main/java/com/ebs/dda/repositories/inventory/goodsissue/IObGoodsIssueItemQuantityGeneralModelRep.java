package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.stockavailability.IObItemQuantityGeneralModelRep;


public interface IObGoodsIssueItemQuantityGeneralModelRep
        extends IObItemQuantityGeneralModelRep<IObGoodsIssueItemQuantityGeneralModel> {

}
