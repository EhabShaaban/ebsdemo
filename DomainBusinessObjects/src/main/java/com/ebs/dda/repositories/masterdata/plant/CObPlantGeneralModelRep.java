package com.ebs.dda.repositories.masterdata.plant;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.GeneralModelRepository;
import com.ebs.dda.jpa.masterdata.plant.CObPlantGeneralModel;

import java.util.List;

public interface CObPlantGeneralModelRep
        extends GeneralModelRepository<CObPlantGeneralModel, Long> {

    CObPlantGeneralModel findOneByUserCode(String code);

    List<CObPlantGeneralModel> findByCompanyCode(String companyCode);

    CObPlantGeneralModel findOneByUserCodeAndCompanyCode(String plantCode, String companyCode);

    CObPlantGeneralModel findOneByCompanyCode(String companyCode);
}
