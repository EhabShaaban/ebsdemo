package com.ebs.dda.repositories.accounting.landedcost;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostItemsPaymentRequestsGeneralModel;

public interface DObLandedCostItemsPaymentRequestsGeneralModelRep
				extends JpaRepository<DObLandedCostItemsPaymentRequestsGeneralModel, Long> {
	List<DObLandedCostItemsPaymentRequestsGeneralModel> findAllByPurchaseOrderCode(
					String purchaseOrderCode);

}
