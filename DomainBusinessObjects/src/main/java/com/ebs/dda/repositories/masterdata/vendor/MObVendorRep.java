package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.repositories.masterdata.item.MObMaterialRep;

import java.util.List;

public interface MObVendorRep extends MObMaterialRep<MObVendor> {
  List<MObVendor> findByCurrentStatesLike(String currentStates);
  MObVendor findOneByOldVendorNumber(String oldVendorNumber);
}
