package com.ebs.dda.repositories;

import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentOwnerGeneralModelRep
    extends JpaRepository<DocumentOwnerGeneralModel, Long> {

  List<DocumentOwnerGeneralModel> findAllByObjectNameOrderByUserNameAsc(String objectName);

  List<DocumentOwnerGeneralModel> findAllByObjectNameOrderByUserIdDesc(String objectName);

  DocumentOwnerGeneralModel findOneByUserIdAndObjectName(Long documentOwnerId, String sysName);
}
