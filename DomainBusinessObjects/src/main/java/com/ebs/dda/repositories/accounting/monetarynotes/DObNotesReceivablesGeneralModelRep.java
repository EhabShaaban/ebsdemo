package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObNotesReceivablesGeneralModelRep
    extends StatefullBusinessObjectRep<DObNotesReceivablesGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObNotesReceivablesGeneralModel> {

  List<DObNotesReceivablesGeneralModel> findAllByCurrentStatesLikeOrderByIdDesc(String state);
}
