package com.ebs.dda.repositories.inventory.stockavailability;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CObStockAvailabilityRep
        extends CodedBusinessObjectRep<CObStockAvailability>,
        JpaSpecificationExecutor<CObStockAvailability> {

}
