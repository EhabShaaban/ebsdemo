package com.ebs.dda.repositories.accounting.paymentrequest;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObPaymentRequestGeneralModelRep
    extends StatefullBusinessObjectRep<DObPaymentRequestGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObPaymentRequestGeneralModel> {
  @Override
  Page<DObPaymentRequestGeneralModel> findAll(
      Specification<DObPaymentRequestGeneralModel> specification, Pageable page);

  @Override
  List<DObPaymentRequestGeneralModel> findAll(
      Specification<DObPaymentRequestGeneralModel> specification, Sort sort);

  List<DObPaymentRequestGeneralModel> findByCurrentStatesLike(String currentStates);

  List<DObPaymentRequestGeneralModel> findByReferenceDocumentCodeAndDueDocumentType(
      String referenceCode, String dueDocumentType);

  DObPaymentRequestGeneralModel findOneById(Long id);

  List<DObPaymentRequestGeneralModel> findAllByReferenceDocumentCode(String refDocCode);
  List<DObPaymentRequestGeneralModel> findAllByPaymentTypeAndReferenceDocumentCodeAndDueDocumentTypeAndCurrentStatesLike(String paymentType, String refDocCode, String refDocType, String currentState);
}
