package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.GoodsReceiptItemDataGeneralModel;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface GoodsReceiptItemDataGeneralModelRep
    extends JpaRepository<GoodsReceiptItemDataGeneralModel, Long> ,
    JpaSpecificationExecutor<GoodsReceiptItemDataGeneralModel>{

  List<GoodsReceiptItemDataGeneralModel> findAll(Specification<GoodsReceiptItemDataGeneralModel> specification);
}
