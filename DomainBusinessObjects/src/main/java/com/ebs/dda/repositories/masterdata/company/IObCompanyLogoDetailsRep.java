package com.ebs.dda.repositories.masterdata.company;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.company.IObCompanyLogoDetails;

public interface IObCompanyLogoDetailsRep extends InformationObjectRep<IObCompanyLogoDetails> {

  IObCompanyLogoDetails findOneByRefInstanceId(Long companyId);
}
