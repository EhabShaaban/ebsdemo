package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModel;
import com.ebs.dda.repositories.inventory.stockavailability.IObItemQuantityGeneralModelRep;


public interface IObGoodsReceiptItemQuantityGeneralModelRep
        extends IObItemQuantityGeneralModelRep<IObGoodsReceiptItemQuantityGeneralModel> {

}
