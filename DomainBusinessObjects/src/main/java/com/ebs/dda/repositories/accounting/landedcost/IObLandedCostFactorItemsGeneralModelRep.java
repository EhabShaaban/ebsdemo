package com.ebs.dda.repositories.accounting.landedcost;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostFactorItemsGeneralModel;

import java.util.List;

public interface IObLandedCostFactorItemsGeneralModelRep extends
        InformationObjectRep<IObLandedCostFactorItemsGeneralModel> {
    List<IObLandedCostFactorItemsGeneralModel> findAllByCodeOrderByIdDesc(String userCode);
    List<IObLandedCostFactorItemsGeneralModel> findAllByCode(String userCode);

}
