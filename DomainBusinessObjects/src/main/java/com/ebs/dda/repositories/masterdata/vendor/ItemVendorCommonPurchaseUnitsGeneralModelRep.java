package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorCommonPurchaseUnitsGeneralModel;
import java.util.List;

public interface ItemVendorCommonPurchaseUnitsGeneralModelRep extends
    BusinessObjectRep<ItemVendorCommonPurchaseUnitsGeneralModel> {

  ItemVendorCommonPurchaseUnitsGeneralModel findOneByVendorCodeAndItemCodeAndPurchasingUnitCode(
      String vendorCode, String itemCode, String purchaseUnitCode);

  List<ItemVendorCommonPurchaseUnitsGeneralModel> findAllByVendorCodeAndItemCode(String vendorCode,
      String itemCode);

List<ItemVendorCommonPurchaseUnitsGeneralModel> findByPurchaseUnitNameInOrderByIdDesc(List<String> purchaseUnitNames);

}
