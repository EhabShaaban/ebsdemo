package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dda.jpa.accounting.collection.IObCollectionSalesOrderDetails;

public interface IObCollectionSalesOrderDetailsRep
    extends IObCollectionDetailsRep<IObCollectionSalesOrderDetails> {

}
