package com.ebs.dda.repositories.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.lookups.LObIncoterms;
import com.ebs.dda.repositories.masterdata.item.LObSimpleMaterialRep;

public interface LObIncotermsRep extends LObSimpleMaterialRep<LObIncoterms> {}
