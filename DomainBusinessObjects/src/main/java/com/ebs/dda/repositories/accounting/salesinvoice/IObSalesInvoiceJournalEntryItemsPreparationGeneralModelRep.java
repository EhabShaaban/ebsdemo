package com.ebs.dda.repositories.accounting.salesinvoice;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceJournalEntryItemsPreparationGeneralModel;

import java.util.List;

public interface IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep extends
				JpaRepository<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel, Long> {
	List<IObSalesInvoiceJournalEntryItemsPreparationGeneralModel> findByCode(String code);
}
