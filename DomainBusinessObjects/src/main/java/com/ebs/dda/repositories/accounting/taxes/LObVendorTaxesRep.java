package com.ebs.dda.repositories.accounting.taxes;

import com.ebs.dda.jpa.accounting.taxes.LObVendorTaxes;

import java.util.List;

public interface LObVendorTaxesRep extends LObTaxInfoRep<LObVendorTaxes> {
	List<LObVendorTaxes> findAllByVendorId(Long vendorId);
}
