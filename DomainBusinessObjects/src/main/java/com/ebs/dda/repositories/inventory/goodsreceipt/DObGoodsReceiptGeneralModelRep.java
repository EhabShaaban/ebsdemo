package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

import static org.eclipse.persistence.config.QueryHints.JDBC_MAX_ROWS;

public interface DObGoodsReceiptGeneralModelRep
    extends StatefullBusinessObjectRep<DObGoodsReceiptGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObGoodsReceiptGeneralModel> {

  @Override
  Page<DObGoodsReceiptGeneralModel> findAll(
          Specification<DObGoodsReceiptGeneralModel> specification, Pageable page);

  DObGoodsReceiptGeneralModel findOneByUserCode(String goodsReceiptCode);

  @Query(
      value =
          "SELECT  e FROM DObGoodsReceiptGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) > :code "
              + "AND e.purchaseUnitNameEn IN :purchaseUnitNames "
              + "ORDER BY CAST(e.userCode AS BIGINT) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObGoodsReceiptGeneralModel findNextByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT e FROM DObGoodsReceiptGeneralModel e "
              + "WHERE CAST (e.userCode AS BIGINT) > :code "
              + "ORDER BY CAST (e.userCode AS BIGINT) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObGoodsReceiptGeneralModel findNextByUserCodeUnconditionalRead(Long code);

  @Query(
      value =
          "SELECT  e FROM DObGoodsReceiptGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) < :code "
              + "AND e.purchaseUnitNameEn IN :purchaseUnitNames "
              + "ORDER BY CAST(e.userCode AS BIGINT) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObGoodsReceiptGeneralModel findPreviousByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT e FROM DObGoodsReceiptGeneralModel e "
              + "WHERE CAST (e.userCode AS BIGINT) < :code "
              + "ORDER BY CAST (e.userCode AS BIGINT) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObGoodsReceiptGeneralModel findPreviousByUserCodeUnconditionalRead(Long code);

  DObGoodsReceiptGeneralModel findOneByUserCodeAndTypeAndCurrentStatesLike(
          String invoiceCode, String TypeCode, String currentState);

  DObGoodsReceiptGeneralModel findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
          String refDocCode, String TypeCode, String currentState);
}
