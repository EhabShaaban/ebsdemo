package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;

public interface DObSettlementRep extends DocumentObjectRep<DObSettlement, DefaultUserCode> {
}
