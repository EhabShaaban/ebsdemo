package com.ebs.dda.repositories.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderCompanyDataGeneralModel;

public interface IObSalesReturnOrderCompanyDataGeneralModelRep
    extends InformationObjectRep<IObSalesReturnOrderCompanyDataGeneralModel> {
  IObSalesReturnOrderCompanyDataGeneralModel findOneBySalesReturnOrderCode(
      String salesReturnOrderCode);
}
