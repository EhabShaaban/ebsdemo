package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerAddress;

public interface IObCustomerAddressRep extends InformationObjectRep<IObCustomerAddress> {
}
