package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.item.IObAlternativeUoM;

public interface IObItemAlternativeUOMRep extends InformationObjectRep<IObAlternativeUoM> {
  IObAlternativeUoM findOneByRefInstanceIdAndAlternativeUnitOfMeasureIdAndConversionFactor(
          Long refInstanceId, Long alternativeUnitOfMeasureId, Float conversionFactor);
}
