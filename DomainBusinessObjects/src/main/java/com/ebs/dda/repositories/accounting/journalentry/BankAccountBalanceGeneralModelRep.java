package com.ebs.dda.repositories.accounting.journalentry;

import com.ebs.dda.jpa.accounting.journalentry.BankAccountBalanceGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankAccountBalanceGeneralModelRep
    extends JpaRepository<BankAccountBalanceGeneralModel, Long> {
  BankAccountBalanceGeneralModel
      findByBusinessUnitCodeAndCompanyCodeAndBankAccountNumberAndCurrencyCodeAndFiscalYearCode(
          String businessUnitCode,
          String companyCode,
          String bankAccountNumber,
          String currencyCode,
          String fiscalYearCode);
}
