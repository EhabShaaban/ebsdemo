package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoicePostPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObVendorInvoicePostPreparationGeneralModelRep
        extends JpaRepository<DObVendorInvoicePostPreparationGeneralModel, Long> {

    DObVendorInvoicePostPreparationGeneralModel findOneByCode(String code);
}
