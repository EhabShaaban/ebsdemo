package com.ebs.dda.repositories.masterdata.company;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.masterdata.company.IObCompanyTreasuriesGeneralModel;

import java.util.List;

public interface IObCompanyTreasuriesGeneralModelRep
				extends InformationObjectRep<IObCompanyTreasuriesGeneralModel> {
	IObCompanyTreasuriesGeneralModel findOneByCompanyCodeAndTreasuryCode(String companyCode, String treasuryCode);
	List<IObCompanyTreasuriesGeneralModel> findAllByCompanyCodeOrderByTreasuryCodeDesc(String companyCode);
}
