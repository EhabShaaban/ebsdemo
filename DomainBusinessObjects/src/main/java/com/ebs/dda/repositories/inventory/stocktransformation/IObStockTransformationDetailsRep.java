package com.ebs.dda.repositories.inventory.stocktransformation;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.inventory.stocktransformation.IObStockTransformationDetails;

public interface IObStockTransformationDetailsRep
    extends InformationObjectRep<IObStockTransformationDetails> {

  IObStockTransformationDetails findOneByRefInstanceId(Long refInstanceId);
}
