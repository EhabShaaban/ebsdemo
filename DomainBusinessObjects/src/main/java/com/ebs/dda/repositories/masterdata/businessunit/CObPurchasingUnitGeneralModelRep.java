package com.ebs.dda.repositories.masterdata.businessunit;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/** @author xerix on Tue Nov 2017 at 11 : 10 */
public interface CObPurchasingUnitGeneralModelRep
    extends StatefullBusinessObjectRep<CObPurchasingUnitGeneralModel, DefaultUserCode> {

  @Query(
      "SELECT e FROM CObPurchasingUnitGeneralModel e "
          + "WHERE e.purchaseUnitName IN :purchaseUnitNames "
          + "ORDER BY e.id DESC")
  List<CObPurchasingUnitGeneralModel> findByPurchaseUnitNameInOrderByIdDesc(
      @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  List<CObPurchasingUnitGeneralModel> findByCurrentStatesLikeOrderByIdDesc(String currentStates);

	List<CObPurchasingUnitGeneralModel> findAllByOrderByIdDesc();

	CObPurchasingUnitGeneralModel findOneById(Long id);

  CObPurchasingUnitGeneralModel findOneByUserCode(String userCode);

}
