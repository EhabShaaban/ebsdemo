package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderTaxGeneralModel;

import java.util.List;

public interface IObSalesOrderTaxGeneralModelRep
    extends InformationObjectRep<IObSalesOrderTaxGeneralModel> {

  List<IObSalesOrderTaxGeneralModel> findBySalesOrderCodeOrderByCodeAsc(String salesOrderCode);
}
