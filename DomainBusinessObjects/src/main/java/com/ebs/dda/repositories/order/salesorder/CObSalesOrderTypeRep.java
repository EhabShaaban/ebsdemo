package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.order.salesorder.CObSalesOrderType;

public interface CObSalesOrderTypeRep extends ConfigurationObjectRep<CObSalesOrderType, DefaultUserCode> {}
