package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssue;

public interface DObGoodsIssueRep extends DocumentObjectRep<DObGoodsIssue, DefaultUserCode> {
}
