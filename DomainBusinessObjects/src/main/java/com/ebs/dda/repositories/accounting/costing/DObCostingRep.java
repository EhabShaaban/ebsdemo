package com.ebs.dda.repositories.accounting.costing;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.costing.DObCosting;

public interface DObCostingRep
    extends DocumentObjectRep<DObCosting, DefaultUserCode> {}
