package com.ebs.dda.repositories.masterdata.customer;

import com.ebs.dda.jpa.masterdata.customer.MObEmployee;
import com.ebs.dda.repositories.masterdata.item.MObMaterialRep;

import java.util.List;

public interface MObEmployeeRep extends MObMaterialRep<MObEmployee> {

    List<MObEmployee> findAllByOrderByUserCodeDesc();

}
