package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.collection.DObCollectionNotesReceivable;

public interface DObCollectionNotesReceivableRep
        extends DocumentObjectRep<DObCollectionNotesReceivable, DefaultUserCode> {
}
