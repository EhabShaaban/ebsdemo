package com.ebs.dda.repositories.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivatePreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObPaymentRequestActivatePreparationGeneralModelRep extends JpaRepository<DObPaymentRequestActivatePreparationGeneralModel, Long> {
    DObPaymentRequestActivatePreparationGeneralModel findOneByPaymentRequestCode(String paymentRequestCode);
}
