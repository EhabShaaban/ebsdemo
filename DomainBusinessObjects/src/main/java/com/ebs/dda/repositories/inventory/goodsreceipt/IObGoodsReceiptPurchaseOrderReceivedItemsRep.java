package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderReceivedItemsData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IObGoodsReceiptPurchaseOrderReceivedItemsRep
        extends CrudRepository<IObGoodsReceiptPurchaseOrderReceivedItemsData, Long> {
    IObGoodsReceiptPurchaseOrderReceivedItemsData findOneById(Long id);

    IObGoodsReceiptPurchaseOrderReceivedItemsData findByRefInstanceIdAndItemId(
            Long goodsReceiptId, Long itemId);

    List<IObGoodsReceiptPurchaseOrderReceivedItemsData> findAllByRefInstanceIdAndIdNot(
            Long refInstanceId, Long id);
}
