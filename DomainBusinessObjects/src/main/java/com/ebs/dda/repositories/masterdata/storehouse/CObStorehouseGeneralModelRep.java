package com.ebs.dda.repositories.masterdata.storehouse;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import java.util.List;

public interface CObStorehouseGeneralModelRep
    extends StatefullBusinessObjectRep<CObStorehouseGeneralModel, DefaultUserCode> {

  List<CObStorehouseGeneralModel> findByPlantCode(String plantCode);
  
  CObStorehouseGeneralModel findByUserCodeAndPlantCode(String storehouseCode,String plantCode);

}
