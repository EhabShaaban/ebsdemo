package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObGoodsIssueRefDocumentDataGeneralModelRep
        extends JpaRepository<IObGoodsIssueRefDocumentDataGeneralModel, Long> {
    IObGoodsIssueRefDocumentDataGeneralModel findOneByGoodsIssueCode(String goodsIssueCode);

    IObGoodsIssueRefDocumentDataGeneralModel findOneByRefDocumentAndGoodsIssueStateContains(
            String refDocumentCode, String state);
}
