package com.ebs.dda.repositories.masterdata.lookups;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.jpa.masterdata.lookups.LObTaxAdministrative;

import java.util.List;

public interface LObTaxAdministrativeRep
    extends LocalizedLookupRep<LObTaxAdministrative, DefaultUserCode> {
  List<LObTaxAdministrative> findAllByOrderByUserCodeDesc();
}
