package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCompanyAndStoreDataGeneralModel;

public interface IObSalesOrderCompanyAndStoreDataGeneralModelRep
        extends InformationObjectRep<IObSalesOrderCompanyAndStoreDataGeneralModel> {

  IObSalesOrderCompanyAndStoreDataGeneralModel findOneBySalesOrderCode(String salesOrderCode);
}
