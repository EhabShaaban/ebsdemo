package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueSalesInvoiceData;

public interface IObGoodsIssueSalesInvoiceDataRep<T extends IObGoodsIssueSalesInvoiceData> extends
        InformationObjectRep<T> {

}
