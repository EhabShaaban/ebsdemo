package com.ebs.dda.repositories.accounting.landedcost;

import org.springframework.stereotype.Repository;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostCompanyDataGeneralModel;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataGeneralModelRep;

@Repository
public interface IObLandedCostCompanyDataGeneralModelRep
    extends IObAccountingDocumentCompanyDataGeneralModelRep<IObLandedCostCompanyDataGeneralModel> {

}
