package com.ebs.dda.repositories.inventory.initialstockupload;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUploadGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObInitialStockUploadGeneralModelRep extends StatefullBusinessObjectRep<DObInitialStockUploadGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObInitialStockUploadGeneralModel>{
    @Override
    Page<DObInitialStockUploadGeneralModel> findAll(
            Specification<DObInitialStockUploadGeneralModel> specification, Pageable page);

}
