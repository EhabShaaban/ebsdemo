package com.ebs.dda.repositories.masterdata.salesresponsible;

import com.ebs.dda.jpa.masterdata.salesresponsible.CObSalesResponsible;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

public interface CObSalesResponsibleRep extends CObEnterpriseRep<CObSalesResponsible> {}
