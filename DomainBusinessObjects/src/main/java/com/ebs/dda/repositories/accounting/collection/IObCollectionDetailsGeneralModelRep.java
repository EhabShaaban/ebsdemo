package com.ebs.dda.repositories.accounting.collection;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;

public interface IObCollectionDetailsGeneralModelRep
    extends InformationObjectRep<IObCollectionDetailsGeneralModel> {
  IObCollectionDetailsGeneralModel findByCollectionCode(String collectionCode);
}
