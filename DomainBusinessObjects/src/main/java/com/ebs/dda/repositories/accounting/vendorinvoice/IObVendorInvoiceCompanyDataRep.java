package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceCompanyData;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentCompanyDataRep;
import org.springframework.stereotype.Repository;

@Repository
public interface IObVendorInvoiceCompanyDataRep extends IObAccountingDocumentCompanyDataRep<IObVendorInvoiceCompanyData> {
}
