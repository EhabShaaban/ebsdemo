package com.ebs.dda.repositories.accounting.landedcost;

import java.util.List;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;

public interface DObLandedCostGeneralModelRep
    extends DocumentObjectRep<DObLandedCostGeneralModel, DefaultUserCode>,
    JpaSpecificationExecutor<DObLandedCostGeneralModel> {

  List<DObLandedCostGeneralModel> findByCurrentStatesNotLike(String state);

  DObLandedCostGeneralModel findByPurchaseOrderCodeAndCurrentStatesNotLike(String poCode,
      String state);
  DObLandedCostGeneralModel findByPurchaseOrderCodeAndCurrentStatesLike(String poCode,
      String state);

}
