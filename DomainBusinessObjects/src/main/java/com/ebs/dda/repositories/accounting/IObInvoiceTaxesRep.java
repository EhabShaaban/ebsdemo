package com.ebs.dda.repositories.accounting;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.IObTaxes;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IObInvoiceTaxesRep<T extends IObTaxes> extends InformationObjectRep<T> {
}
