package com.ebs.dda.repositories.accounting.settlement;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObSettlementGeneralModelRep
    extends StatefullBusinessObjectRep<DObSettlementGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObSettlementGeneralModel> {
  @Override
  Page<DObSettlementGeneralModel> findAll(
      Specification<DObSettlementGeneralModel> specification, Pageable page);

}
