package com.ebs.dda.repositories.masterdata.vendor;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;

public interface IObVendorAccountingInfoGeneralModelRep
    extends BusinessObjectRep<IObVendorAccountingInfoGeneralModel> {
  IObVendorAccountingInfoGeneralModel findByVendorCode(String vendorCode);

}
