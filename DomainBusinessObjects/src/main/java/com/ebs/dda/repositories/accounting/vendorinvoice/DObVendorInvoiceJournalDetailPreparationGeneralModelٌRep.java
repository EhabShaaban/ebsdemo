package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep extends JpaRepository<DObVendorInvoiceJournalDetailPreparationGeneralModel, Long> {

    DObVendorInvoiceJournalDetailPreparationGeneralModel findOneByCodeAndObjectTypeCode(String documentCode, String objectTypeCode);
    DObVendorInvoiceJournalDetailPreparationGeneralModel findOneByCode(String documentCode);
}
