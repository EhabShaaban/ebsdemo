package com.ebs.dda.repositories.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IObSalesReturnOrderItemRep extends InformationObjectRep<IObSalesReturnOrderItem> {

  @Query(
          "Select DISTINCT (item.itemId) from IObSalesReturnOrderItem item where item.refInstanceId = :refInstanceId")
  List<Long> findDistinctSalesReturnItems(@Param("refInstanceId") Long refInstanceId);
}
