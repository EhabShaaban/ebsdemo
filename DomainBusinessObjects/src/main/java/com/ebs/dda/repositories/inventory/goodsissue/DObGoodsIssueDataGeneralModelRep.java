package com.ebs.dda.repositories.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueDataGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface DObGoodsIssueDataGeneralModelRep
        extends StatefullBusinessObjectRep<DObGoodsIssueDataGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObGoodsIssueDataGeneralModel> {

    List<DObGoodsIssueDataGeneralModel> findAllByUserCode(String goodsIssueCode);
    List<DObGoodsIssueDataGeneralModel> findAllByUserCodeAndGoodsIssueTypeCodeAndCurrentStatesLike(
            String goodsIssueCode, String goodsIssueTypeCode, String currentState);
}
