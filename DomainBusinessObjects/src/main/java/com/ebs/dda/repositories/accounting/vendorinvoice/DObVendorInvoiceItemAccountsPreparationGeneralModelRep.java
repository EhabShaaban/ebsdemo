package com.ebs.dda.repositories.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceItemAccountsPreparationGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DObVendorInvoiceItemAccountsPreparationGeneralModelRep extends JpaRepository<DObVendorInvoiceItemAccountsPreparationGeneralModel, Long> {
    List<DObVendorInvoiceItemAccountsPreparationGeneralModel> findAllByCode(String code);
}
