package com.ebs.dda.repositories.accounting.salesinvoice;

import org.springframework.stereotype.Repository;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxes;
import com.ebs.dda.repositories.accounting.IObInvoiceTaxesRep;

@Repository
public interface IObSalesInvoiceTaxesRep extends IObInvoiceTaxesRep<IObSalesInvoiceTaxes> { }
