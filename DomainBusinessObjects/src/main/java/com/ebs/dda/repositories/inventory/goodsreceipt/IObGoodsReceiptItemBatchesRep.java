package com.ebs.dda.repositories.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.BusinessObjectRep;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemBatches;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemDetail;

import java.util.List;

public interface IObGoodsReceiptItemBatchesRep
    extends BusinessObjectRep<IObGoodsReceiptItemBatches> {

  IObGoodsReceiptItemBatches findOneById(Long id);
  IObGoodsReceiptItemBatches findOneByRefInstanceIdAndUnitOfEntryId(Long refInstanceId, Long unitOfEntryId);

    List<IObGoodsReceiptItemDetail> findByRefInstanceIdAndUnitOfEntryId(Long id, Long unitOfMeasureId);
}
