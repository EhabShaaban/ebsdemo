package com.ebs.dda.repositories.masterdata.item;

import com.ebs.dda.jpa.masterdata.item.MObItem;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 31, 2017 , 10:34:59 AM
 */
public interface MObItemRep extends MObMaterialRep<MObItem> {}
