package com.ebs.dda.repositories.accounting.landedcost;

import org.springframework.stereotype.Repository;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.jpa.accounting.landedcost.CObLandedCostType;

@Repository
public interface CObLandedCostTypeRep
    extends ConfigurationObjectRep<CObLandedCostType, DefaultUserCode> {

}
