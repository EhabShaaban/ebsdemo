package com.ebs.dda.repositories.masterdata.company;

import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;

public interface CObCompanyRep extends CObEnterpriseRep<CObCompany> {}
