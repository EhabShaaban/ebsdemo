package com.ebs.dda.repositories.order.salesorder;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemGeneralModel;
import java.util.List;

public interface IObSalesOrderItemsGeneralModelRep
    extends InformationObjectRep<IObSalesOrderItemGeneralModel> {

  List<IObSalesOrderItemGeneralModel> findBySalesOrderCodeOrderByIdDesc(String salesOrderCode);

  List<IObSalesOrderItemGeneralModel> findBySalesOrderCodeAndItemCode(String salesOrderCode, String itemCode);

  IObSalesOrderItemGeneralModel findOneBySalesOrderCodeAndItemCodeAndUnitOfMeasureCode(
      String salesOrderCode, String itemCode, String unitOfMeasureCode);
}
