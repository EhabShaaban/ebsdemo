package com.ebs.dda.converters;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class JodaTimeCSVConverter extends AbstractBeanField<String, Long> {

  @Override
  protected Object convert(String csvDate)
      throws CsvDataTypeMismatchException, CsvConstraintViolationException {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("d/M/y");
    return formatter.parseDateTime(csvDate).withZone(DateTimeZone.UTC);
  }
}
