package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesOrderItem")
public class IObSalesOrderItem extends DocumentLine {

  private Long itemId;

  @Column(name = "orderunitid")
  private Long unitOfMeasureId;

  private BigDecimal quantity;
  private BigDecimal salesPrice;

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Long getUnitOfMeasureId() {
    return unitOfMeasureId;
  }

  public void setUnitOfMeasureId(Long unitOfMeasureId) {
    this.unitOfMeasureId = unitOfMeasureId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getSalesPrice() {
    return salesPrice;
  }

  public void setSalesPrice(BigDecimal salesPrice) {
    this.salesPrice = salesPrice;
  }

  @Override
  public String getSysName() {
    return IDObSalesOrder.SYS_NAME;
  }
}
