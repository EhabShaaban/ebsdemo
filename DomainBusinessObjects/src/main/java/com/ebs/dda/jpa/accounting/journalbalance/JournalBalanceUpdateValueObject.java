package com.ebs.dda.jpa.accounting.journalbalance;

import java.math.BigDecimal;

/**
 * @author Ahmed Ali Elfeky
 * @since May 29, 2021
 */
public class JournalBalanceUpdateValueObject {

  private String journalBalanceCode;
  private BigDecimal amount;

  public String getJournalBalanceCode() {
    return journalBalanceCode;
  }

  public void setJournalBalanceCode(String journalBalanceCode) {
    this.journalBalanceCode = journalBalanceCode;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

}
