package com.ebs.dda.jpa.masterdata.chartofaccount;

public interface IHObChartOfAccounts {

  String SYS_NAME = "GLAccount";

  String ROOT = "1";
  String INTERMEDIATE = "2";
  String LEAF = "3";
}
