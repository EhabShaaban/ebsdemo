package com.ebs.dda.jpa.masterdata.enterprise;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class ContactDetails extends InformationObject {

  private static final long serialVersionUID = 1L;

  @Column(name = "contactTypeId")
  private Long contactTypeId;

  private String contactValue;

  public String getContactValue() {
    return contactValue;
  }

  public void setContactValue(String contactValue) {
    this.contactValue = contactValue;
  }

  @Override
  public String getSysName() {
    return "ContactDetails";
  }
}
