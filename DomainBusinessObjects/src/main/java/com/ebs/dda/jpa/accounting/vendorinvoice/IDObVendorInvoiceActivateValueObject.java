package com.ebs.dda.jpa.accounting.vendorinvoice;

public interface IDObVendorInvoiceActivateValueObject {

    String INVOICE_CODE = "vendorInvoiceCode";
    String JOURNAL_DATE = "journalDate";
}
