package com.ebs.dda.jpa.accounting.collection;

public interface ICObCollectionType {
  String SYS_NAME = "CollectionType";
  String USER_CODE = "userCode";
  String NAME = "name";
}
