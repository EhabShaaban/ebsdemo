package com.ebs.dda.jpa.accounting.paymentrequest;

public interface IDObPaymentRequestPostValueObject {
    String EXCHANGE_RATE_CODE = "exchangeRateCode";
    String DUE_DATE = "dueDate";
    String CURRENCY_PRICE = "currencyPrice";
}