package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IDObSalesReturnOrderGeneralModel {

  String PURCHASE_UNIT_NAME_ATTR_NAME = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR_NAME);

  String USER_CODE = "userCode";
  String SALES_RETURN_ORDER_TYPE_CODE = "salesReturnOrderTypeCode";
  String SALES_RETURN_ORDER_TYPE_NAME = "salesReturnOrderTypeName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String SALES_INVOICE_CODE = "salesInvoiceCode";

  String CUSTOMER = "customer";
  String CUSTOMER_CODE = "customerCode";
  String CUSTOMER_NAME = "customerName";
  String CODE = "Code";
  String NAME = "Name";
  String CURRENT_STATES = "currentStates";
  String CREATION_INFO_NAME = "creationInfo";
  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
  String DOCUMENT_OWNER_CODE = "documentOwnerCode";
  String DOCUMENT_OWNER_NAME = "documentOwnerName";
}
