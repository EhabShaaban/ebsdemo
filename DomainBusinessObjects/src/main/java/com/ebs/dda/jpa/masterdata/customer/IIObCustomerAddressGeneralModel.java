package com.ebs.dda.jpa.masterdata.customer;

public interface IIObCustomerAddressGeneralModel {
  String SYS_NAME = "Customer";

  String ID = "id";
  String ADDRESS_NAME = "addressName";
  String ADDRESS_DESCRIPTION = "addressDescription";
}
