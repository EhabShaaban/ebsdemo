package com.ebs.dda.jpa.masterdata.storehouse;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.IIObContactDetails;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseContactDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("4")
@EntityInterface(IIObContactDetails.class)
public class IObStorehouseContactDetails extends IObEnterpriseContactDetails {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return "IObStorehouseContactDetails";
  }
}
