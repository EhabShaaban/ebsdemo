package com.ebs.dda.jpa.accounting.vendorinvoice;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE)
public class DObImportGoodsInvoice extends DObVendorInvoice implements Serializable {

}
