/** */
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.geolocale.LocalizedLObGeoLocale;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 24, 2016 2:53:12 PM
 */
@Entity
@DiscriminatorValue("3")
public class LObSubdivisionCategory extends LocalizedLObGeoLocale implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_SUBDIVISION_CATEGORY_SYS_NAME;
  }
}
