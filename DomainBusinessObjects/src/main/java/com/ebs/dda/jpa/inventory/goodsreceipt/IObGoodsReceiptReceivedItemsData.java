package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class IObGoodsReceiptReceivedItemsData extends DocumentLine {
  private static final long serialVersionUID = 1L;

  private Long itemId;
  private String differenceReason;

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public String getDifferenceReason() {
    return differenceReason;
  }

  public void setDifferenceReason(String differenceReason) {
    this.differenceReason = differenceReason;
  }
}
