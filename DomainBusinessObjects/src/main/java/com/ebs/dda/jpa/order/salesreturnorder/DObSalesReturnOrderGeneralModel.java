package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@EntityInterface(IDObSalesReturnOrderGeneralModel.class)
@Table(name = "DObSalesReturnOrderGeneralModel")
public class DObSalesReturnOrderGeneralModel extends DocumentObject<DefaultUserCode> {

  private String salesReturnOrderTypeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString salesReturnOrderTypeName;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private String salesInvoiceCode;

  private String customerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  private Long documentOwnerId;

  private String documentOwnerName;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private BigDecimal amount;
  private String currencyIso;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  private String currencyCode;

  public BigDecimal getAmount() {
    return amount;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getSalesReturnOrderTypeCode() {
    return salesReturnOrderTypeCode;
  }

  public LocalizedString getSalesReturnOrderTypeName() {
    return salesReturnOrderTypeName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public String getDocumentOwnerName() {
    return documentOwnerName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
