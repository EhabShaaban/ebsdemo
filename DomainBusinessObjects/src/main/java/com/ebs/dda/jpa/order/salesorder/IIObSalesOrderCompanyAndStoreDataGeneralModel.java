package com.ebs.dda.jpa.order.salesorder;

public interface IIObSalesOrderCompanyAndStoreDataGeneralModel {

  String SYS_NAME = "SalesOrder";

  String SALES_ORDER_CODE = "salesOrderCode";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String STORE_CODE = "storeCode";
  String STORE_NAME = "storeName";
}
