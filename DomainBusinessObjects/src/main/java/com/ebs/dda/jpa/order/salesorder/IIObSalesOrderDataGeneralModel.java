package com.ebs.dda.jpa.order.salesorder;

public interface IIObSalesOrderDataGeneralModel {

  String SALES_ORDER_CODE = "salesOrderCode";
  String CUSTOMER_CODE = "customerCode";
  String CUSTOMER_NAME = "customerName";
  String CUSTOMER_ADDRESS_ID = "customerAddressId";
  String CONTACT_PERSON_ID = "contactPersonId";
  String PAYMENT_TERM_CODE = "paymentTermCode";
  String PAYMENT_TERM_NAME = "paymentTermName";
  String CURRENCY_CODE = "currencyCode";
  String CURRENCY_ISO = "currencyIso";
  String CUSTOMER_ADDRESS = "customerAddress";
  String CONTACT_PERSON_NAME = "contactPersonName";
  String CREDIT_LIMIT = "creditLimit";
  String EXPECTED_DELIVERY_DATE = "expectedDeliveryDate";
  String NOTES = "notes";
}
