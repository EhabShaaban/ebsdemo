package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObInitialStockUploadItemGeneralModel {
  String USER_CODE = "userCode";
  String CURRENT_STATES = "currentStates";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String UOM_CODE = "unitOfMeasureCode";
  String UOM_SYMBOL = "unitOfMeasureSymbol";
  String ORIGINAL_ADDING_DATE = "originalAddingDate";
  String STOCK_TYPE = "stockType";
  String QUANTITY = "quantity";
  String ACTUAL_COST = "actualCost";
  String REMAINING_QUANTITY = "remainingQuantity";

  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME_EN);
}
