package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesOrderTaxGeneralModel")
public class IObSalesOrderTaxGeneralModel extends InformationObject {

  private String salesOrderCode;
  private String code;
  private BigDecimal percentage;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private BigDecimal amount;

  public String getSalesOrderCode() {
    return salesOrderCode;
  }

  public String getCode() {
    return code;
  }

  public BigDecimal getPercentage() {
    return percentage;
  }

  public LocalizedString getName() {
    return name;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override
  public String getSysName() {
    return IDObSalesOrder.SYS_NAME;
  }
}
