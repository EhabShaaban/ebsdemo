package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class RefDocumentTypeEnum extends BusinessEnum {

  public static final String SYS_NAME = "RefDocumentType";
  public static final String SALES_INVOICE = "SALES_INVOICE";
  public static final String NOTES_RECEIVABLE = "NOTES_RECEIVABLE";
  public static final String SALES_ORDER = "SALES_ORDER";
  public static final String DEBIT_NOTE = "DEBIT_NOTE";
  public static final String PAYMENT_REQUEST = "PAYMENT_REQUEST";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private RefDocumentType value;

  public RefDocumentTypeEnum() {}

  @Override
  public RefDocumentType getId() {
    return this.value;
  }

  public void setId(RefDocumentType id) {
    this.value = id;
  }

  public enum RefDocumentType {
    SALES_INVOICE, NOTES_RECEIVABLE, SALES_ORDER, DEBIT_NOTE, PAYMENT_REQUEST
  }
}
