package com.ebs.dda.jpa.masterdata.ivr;

public interface IItemVendorRecordValueObject {

  String IVR = "ItemVendorRecord";
  String VENDOR_CODE = "VendorCode";
  String ITEM_CODE = "ItemCode";
  String UOM_CODE = "UOMCode";
  String CURRENCY_CODE = "CurrencyCode";
  String PURCHASE_UNIT = "PurchasingUnit";
  String OLD_ITEM_NUMBER = "OldItemNumber";

}
