package com.ebs.dda.jpa.accounting.paymentrequest;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

@Entity
@Table(name = "IObPaymentRequestAccountDetailsGeneralModel")
@EntityInterface(IIObAccountingDocumentAccountDetailsGeneralModel.class)
public class IObPaymentRequestAccountDetailsGeneralModel
				extends IObAccountingDocumentAccountingDetailsGeneralModel {
	private static final long serialVersionUID = 1L;

	@Override
	public String getSysName() {
		return IDObPaymentRequest.SYS_NAME;
	}
}
