package com.ebs.dda.jpa.masterdata.measure;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 24, 2016
 */
@Entity
@Table(name = "IObUoMConversion")
@EntityInterface(IIObUoMConversion.class)
public class IObUoMConversion extends InformationObject implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "numerator")
  private Double numerator;

  @Column(name = "denominator")
  private Double denominator;

  @Column(name = "roundDecimalPlaces")
  private Integer roundDecimalPlaces;

  @Column(name = "description")
  private String description;

  public Double getNumerator() {
    return numerator;
  }

  public void setNumerator(Double numerator) {
    this.numerator = numerator;
  }

  public Double getDenominator() {
    return denominator;
  }

  public void setDenominator(Double denominator) {
    this.denominator = denominator;
  }

  public Integer getRoundDecimalPlaces() {
    return roundDecimalPlaces;
  }

  public void setRoundDecimalPlaces(Integer roundDecimalPlaces) {
    this.roundDecimalPlaces = roundDecimalPlaces;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getSysName() {
    return IIObUoMConversion.SYS_NAME;
  }
}
