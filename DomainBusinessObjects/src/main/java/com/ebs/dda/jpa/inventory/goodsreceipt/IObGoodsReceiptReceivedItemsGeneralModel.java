package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObGoodsReceiptReceivedItemsGeneralModel")
@EntityInterface(IIObGoodsReceiptReceivedItemsGeneralModel.class)
public class IObGoodsReceiptReceivedItemsGeneralModel {

  @Id private Long id;
  private Long grItemId;
  private Long goodsReceiptInstanceId;

  private BigDecimal quantityInDn;
  private BigDecimal returnedQuantityBase;
  private String objectTypeCode;
  private String differenceReason;
  private String itemCode;
  private String goodsReceiptCode;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime modifiedDate;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString baseUnit;

  private String baseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;

  private Boolean isBatchManaged;

  public Long getId() {
    return id;
  }
  public Long getGRItemId() {
    return grItemId;
  }

  public Long getGoodsReceiptInstanceId() {
    return goodsReceiptInstanceId;
  }

  public BigDecimal getQuantityInDn() {
    return quantityInDn;
  }

  public String getDifferenceReason() {
    return differenceReason;
  }

  public String getItemCode() {
    return itemCode;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public LocalizedString getBaseUnit() {
    return baseUnit;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getBaseUnitCode() {
    return baseUnitCode;
  }

  public BigDecimal getReturnedQuantityBase() {
    return returnedQuantityBase;
  }

  public Boolean getIsBatchManaged() {
    return isBatchManaged;
  }

  public DateTime getModifiedDate() {
    return modifiedDate;
  }
}
