package com.ebs.dda.jpa.accounting.salesinvoice;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesInvoiceJournalEntryItemsPreparationGeneralModel")
public class IObSalesInvoiceJournalEntryItemsPreparationGeneralModel {

	@Id
	Long id;
	String code;
	Long taxesAccountId;
	Long taxId;
	BigDecimal taxAmount;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public Long getTaxesAccountId() {
		return taxesAccountId;
	}

	public Long getTaxId() {
		return taxId;
	}

	public BigDecimal getTaxAmount() {
		return taxAmount;
	}
}
