package com.ebs.dda.jpa.accounting.salesinvoice;


public interface IIObSalesInvoiceBusinessPartnerGeneralModel {

    String SYS_NAME = "salesInvoice";
    String CUSTOMER = "businesspartnercodename";
    String SALES_ORDER = "salesOrder";
    String PAYMENT_TERM = "paymentTermCodeName";
    String PAYMENT_TERM_CODE = "paymentTermCode";
    String CURRENCY = "currencyIso";
    String DOWN_PAYMENT = "downPayment";
}
