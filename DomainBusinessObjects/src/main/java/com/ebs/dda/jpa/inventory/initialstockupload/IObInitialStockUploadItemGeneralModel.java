package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObInitialStockUploadItemGeneralModel")
@EntityInterface(IDObInitialStockUploadItemGeneralModel.class)
public class IObInitialStockUploadItemGeneralModel extends InformationObject {

  private String initialStockUploadCode;
  private Long itemId;
  private String itemCode;
  private LocalizedString itemName;

  private Long unitOfMeasureId;
  private String unitOfMeasureCode;
  private LocalizedString unitOfMeasureSymbol;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime originalAddingDate;

  private String stockType;
  private BigDecimal quantity;
  private BigDecimal estimateCost;
  private BigDecimal actualCost;
  private BigDecimal remainingQuantity;

  public Long getItemId() {
    return itemId;
  }

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public Long getUnitOfMeasureId() {
    return unitOfMeasureId;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public LocalizedString getUnitOfMeasureSymbol() {
    return unitOfMeasureSymbol;
  }

  public DateTime getOriginalAddingDate() {
    return originalAddingDate;
  }

  public String getStockType() {
    return stockType;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public BigDecimal getEstimateCost() {
    return estimateCost;
  }

  public BigDecimal getActualCost() {
    return actualCost;
  }

  public BigDecimal getRemainingQuantity() {
    return remainingQuantity;
  }

  public String getInitialStockUploadCode() {
    return initialStockUploadCode;
  }

  @Override
  public String getSysName() {
    return IDObInitialStockUpload.SYS_NAME;
  }
}
