package com.ebs.dda.jpa.masterdata.item;

public class MObItemAccountingInfoValueObject {
  private String itemCode;
  private String accountCode;
  private Boolean costFactor;

  public Boolean getCostFactor() {
    return costFactor;
  }

  public void setCostFactor(Boolean costFactor) {
    this.costFactor = costFactor;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getAccountCode() {
    return accountCode;
  }

  public void setAccountCode(String accountCode) {
    this.accountCode = accountCode;
  }
}
