package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObGoodsReceiptDataGeneralModel")
@EntityInterface(IDObGoodsReceiptGeneralModel.class)
public class DObGoodsReceiptDataGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;
  private String inventoryDocumentType;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private Long purchaseUnitId;

  private String purchaseUnitNameEn;

  private String purchaseUnitCode;

  private String refDocumentCode;

  private String type;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString typeName;

  private Long storehouseId;

  private String storehouseCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storehouseName;

  private String storekeeperCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storekeeperName;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime activationDate;

  private Long plantId;

  private String plantCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString plantName;

  private Long companyId;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  private String businessPartnerCode;

  public LocalizedString getStorekeeperName() {
    return storekeeperName;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public LocalizedString getStorehouseName() {
    return storehouseName;
  }

  public String getStorekeeperCode() {
    return storekeeperCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public DateTime getActivationDate() {
    return activationDate;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public LocalizedString getBusinessPartnerName() {
    return businessPartnerName;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public LocalizedString getPlantName() {
    return plantName;
  }

  public String getType() {
    return type;
  }

  public LocalizedString getTypeName() {
    return typeName;
  }

  public boolean isGoodsReceiptPurchaseOrder() {
    return inventoryDocumentType.equals("GR_PO");
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public String getInventoryDocumentType() {
    return inventoryDocumentType;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public Long getStorehouseId() {
    return storehouseId;
  }

  public Long getPlantId() {
    return plantId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  @Override
  public String getSysName() {
    return IDObGoodsReceipt.SYS_NAME;
  }
}
