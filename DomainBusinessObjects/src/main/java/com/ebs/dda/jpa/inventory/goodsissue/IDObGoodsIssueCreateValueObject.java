package com.ebs.dda.jpa.inventory.goodsissue;

public interface IDObGoodsIssueCreateValueObject {

    String TYPE = "typeCode";
    String PURCHASE_UNIT = "purchaseUnitCode";
    String REF_DOCUMENT_CODE = "refDocumentCode";
    String COMPANY = "companyCode";
    String STORE_HOUSE = "storeHouseCode";
    String STORE_KEEPER = "storeKeeperCode";
}
