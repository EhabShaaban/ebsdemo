package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("NotesReceivable")
public class IObNotesReceivableCompanyData extends IObAccountingDocumentCompanyData {

  private static final long serialVersionUID = 1L;

}
