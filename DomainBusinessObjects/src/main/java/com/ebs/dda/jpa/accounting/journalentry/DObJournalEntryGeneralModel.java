package com.ebs.dda.jpa.accounting.journalentry;

import com.ebs.dac.dbo.api.annotations.Exclude;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObJournalEntryGeneralModel")
@EntityInterface(IDObJournalEntryGeneralModel.class)
public class DObJournalEntryGeneralModel extends DocumentObject<DefaultUserCode> {

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String purchaseUnitCode;

  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String documentReferenceType;
  private String documentReferenceCode;

  private String fiscalPeriod;

  @Exclude private Long fiscalPeriodId;

  public DateTime getDueDate() {
    return dueDate;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getDocumentReferenceType() {
    return documentReferenceType;
  }

  public String getDocumentReferenceCode() {
    return documentReferenceCode;
  }

  public String getFiscalPeriod() {
    return fiscalPeriod;
  }

  public Long getFiscalPeriodId() {
    return fiscalPeriodId;
  }

  @Override
  public String getSysName() {
    return IDObJournalEntry.SYS_NAME;
  }
}
