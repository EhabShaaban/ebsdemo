package com.ebs.dda.jpa.accounting.vendorinvoice;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = VendorInvoiceTypeEnum.Values.CUSTOM_TRUSTEE_INVOICE)
public class DObCustomTrusteeInvoice extends DObPurchaseServiceLocalInvoice implements Serializable {

}
