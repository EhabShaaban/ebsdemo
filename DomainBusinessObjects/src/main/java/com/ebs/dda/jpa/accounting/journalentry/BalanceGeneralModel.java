package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class BalanceGeneralModel {

  @Id private Long id;

  private String companyCode;
  private String businessUnitCode;
  private String fiscalYearCode;
  private String currencyCode;
  private BigDecimal balance;

  public String getCompanyCode() {
    return companyCode;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public String getFiscalYearCode() {
    return fiscalYearCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public Long getId() {
    return id;
  }
}
