package com.ebs.dda.jpa.accounting.collection;

import java.math.BigDecimal;

public class IObCollectionDetailsValueObject {
    private String collectionCode;
    private BigDecimal amount;
    private String bankAccountNumber;
    private String treasuryCode;

    public String getCollectionCode() {
        return collectionCode;
    }

    public void setCollectionCode(String collectionCode) {
        this.collectionCode = collectionCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public String getTreasuryCode() {
        return treasuryCode;
    }
}
