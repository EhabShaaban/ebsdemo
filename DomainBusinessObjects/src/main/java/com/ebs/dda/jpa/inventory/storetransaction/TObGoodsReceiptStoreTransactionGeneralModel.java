package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.TransactionObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "TObGoodsReceiptStoreTransactionGeneralModel")
@EntityInterface(ITObGoodsReceiptStoreTransactionGeneralModel.class)
public class TObGoodsReceiptStoreTransactionGeneralModel
    extends TransactionObject<DefaultUserCode> {

  private BigDecimal quantity;
  private BigDecimal estimateCost;
  private String stockType;
  private BigDecimal actualCost;
  private String batchNo;
  private BigDecimal remainingQuantity;
  private String transactionOperation;
  private String companyCode;
  private Long refTransactionId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String storehouseCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storehouseName;

  private String plantCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString plantName;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString unitofmeasurename;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString unitOfMeasureSymbol;

  private String unitOfMeasureCode;

  private String refTransactionCode;

  private String itemCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;

  private String goodsReceiptCode;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime originalAddingDate;

  public BigDecimal getQuantity() {
    return quantity;
  }

  public BigDecimal getEstimateCost() {
    return estimateCost;
  }

  public String getStockType() {
    return stockType;
  }

  public BigDecimal getActualCost() {
    return actualCost;
  }

  public String getBatchNo() {
    return batchNo;
  }

  public BigDecimal getRemainingQuantity() {
    return remainingQuantity;
  }

  public String getTransactionOperation() {
    return transactionOperation;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public LocalizedString getStorehouseName() {
    return storehouseName;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public LocalizedString getPlantName() {
    return plantName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public LocalizedString getUnitOfMeasureSymbol() {
    return unitOfMeasureSymbol;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public String getRefTransactionCode() {
    return refTransactionCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public LocalizedString getUnitofmeasurename() {
    return unitofmeasurename;
  }

  public DateTime getOriginalAddingDate() {
    return originalAddingDate;
  }

  public Long getRefTransactionId() {
    return refTransactionId;
  }

  @Override
  public String getSysName() {
    return ITObStoreTransaction.SYS_NAME;
  }
}
