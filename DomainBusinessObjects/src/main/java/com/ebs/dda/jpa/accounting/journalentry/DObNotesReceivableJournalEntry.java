package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "DObNotesReceivableJournalEntry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("NotesReceivable")
public class DObNotesReceivableJournalEntry extends DObJournalEntry {

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }

  public Long getDocumentReferenceId() {
    return documentReferenceId;
  }
}
