package com.ebs.dda.jpa.accounting;

public interface IIObInvoiceTaxesGeneralModel {

  String SI_CODE = "invoiceCode";
  String TAX_NAME = "taxName";
  String TAX_CODE = "taxCode";
  String TAX_AMOUNT = "taxAmount";
  String TAX_Percentage = "taxPercentage";
}
