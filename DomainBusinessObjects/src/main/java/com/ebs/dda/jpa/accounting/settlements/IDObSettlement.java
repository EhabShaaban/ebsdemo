package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;

public interface IDObSettlement extends IDObAccountingDocument {
  String SYS_NAME = "Settlement";

  String COMPANY_DATA_ATTR_NAME = "companydata";
  CompositeAttribute<IObSettlementCompanyData> companyDataAttr =
      new CompositeAttribute<>(COMPANY_DATA_ATTR_NAME, IObSettlementCompanyData.class, false);

  String DETAILS_ATTR_NAME = "details";
  CompositeAttribute<IObSettlementDetails> detailsAttr =
      new CompositeAttribute<>(COMPANY_DATA_ATTR_NAME, IObSettlementDetails.class, false);

  String POSTING_DETAILS_ATTR_NAME = "postingDetails";
  CompositeAttribute<IObSettlementPostingDetails> postingDetailsAttr =
      new CompositeAttribute<>(POSTING_DETAILS_ATTR_NAME, IObSettlementPostingDetails.class, false);

  static final String ACCOUNTING_DETAILS_ATTR_NAME = "accountingDetails";
  CompositeAttribute<IObAccountingDocumentAccountingDetails> accountingDetailsAttr =
      new CompositeAttribute<IObAccountingDocumentAccountingDetails>(ACCOUNTING_DETAILS_ATTR_NAME,
          IObAccountingDocumentAccountingDetails.class, true);
}
