package com.ebs.dda.jpa;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DocumentOwnerGeneralModel")
public class DocumentOwnerGeneralModel implements Serializable {

  private static final long serialVersionUID = 1L;
  public static final String SYS_NAME = "DocumentOwner";

  private String permissionExpression;

  private String objectName;

  private String userName;

  @Id
  private Long userId;

  private Long authorizationConditionId;

  private String condition;

  public String getPermissionExpression() {
    return permissionExpression;
  }

  public String getObjectName() {
    return objectName;
  }

  public String getUserName() {
    return userName;
  }

  public Long getUserId() {
    return userId;
  }

  public Long getAuthorizationConditionId() {
    return authorizationConditionId;
  }

  public String getCondition() {
    return condition;
  }

}
