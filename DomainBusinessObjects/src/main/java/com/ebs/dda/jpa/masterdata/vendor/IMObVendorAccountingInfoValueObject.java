package com.ebs.dda.jpa.masterdata.vendor;

public interface IMObVendorAccountingInfoValueObject {
  String VENDOR_CODE = "vendorCode";
  String ACCOUNT_CODE = "accountCode";

}
