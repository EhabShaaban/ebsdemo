package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel {
    String STOCK_TYPE = "stockType";
    String PRODUCTION_DATE = "productionDate";
    String EXPIRATION_DATE =  "expirationDate";
    String UNIT_OF_ENTRY = "unitOfEntry";
    String UNIT_OF_ENTRY_CODE = "unitOfEntryCode";
    String RECEIVED_QTY_UOE =  "receivedQtyUoE";
    String BATCH_NUMBER = "batchCode";
    String NOTES = "notes";

}
