package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 24, 2016
 */
@Entity
@Table(name = "iobalternativeuomgeneralmodel")
public class IObAlternativeUoMGeneralModel extends BusinessObject {

  private static final long serialVersionUID = 1L;

  @Column(name = "oldItemNumber")
  private String oldItemNumber;

  @Column(name = "alternativeunitofmeasurecode")
  private String alternativeUnitOfMeasureCode;

  @Column(name = "alternativeunitofmeasureid")
  private Long alternativeUnitOfMeasureId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString alternativeUnitOfMeasureName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString alternativeUnitOfMeasureSymbol;

  @Column(name = "basicunitofmeasurecode")
  private String basicUnitOfMeasureCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString basicUnitOfMeasureName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString basicUnitOfMeasureSymbol;

  @Column(name = "itemcode")
  private String itemCode;

  @Column(name = "itemname")
  private String itemName;

  @Column(name = "conversionFactor")
  private Float conversionFactor;

  public IObAlternativeUoMGeneralModel() {}

  public String getAlternativeUnitOfMeasureCode() {
    return alternativeUnitOfMeasureCode;
  }

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  public String getItemCode() {
    return itemCode;
  }

  public Float getConversionFactor() {
    return conversionFactor;
  }

  public LocalizedString getAlternativeUnitOfMeasureName() {
    return alternativeUnitOfMeasureName;
  }

  public LocalizedString getAlternativeUnitOfMeasureSymbol() {
    return alternativeUnitOfMeasureSymbol;
  }

  public Long getAlternativeUnitOfMeasureId() {
    return alternativeUnitOfMeasureId;
  }

  public String getItemName() {
    return itemName;
  }

  public String getBasicUnitOfMeasureCode() {
    return basicUnitOfMeasureCode;
  }

  public LocalizedString getBasicUnitOfMeasureName() {
    return basicUnitOfMeasureName;
  }

  public LocalizedString getBasicUnitOfMeasureSymbol() {
    return basicUnitOfMeasureSymbol;
  }

  public void setAlternativeUnitOfMeasureCode(String alternativeUnitOfMeasureCode) {
    this.alternativeUnitOfMeasureCode = alternativeUnitOfMeasureCode;
  }

  public void setAlternativeUnitOfMeasureName(LocalizedString alternativeUnitOfMeasureName) {
    this.alternativeUnitOfMeasureName = alternativeUnitOfMeasureName;
  }

  public void setConversionFactor(Float conversionFactor) {
    this.conversionFactor = conversionFactor;
  }

  public String getSysName() {
    return "Alternative UoM GeneralModel";
  }
}
