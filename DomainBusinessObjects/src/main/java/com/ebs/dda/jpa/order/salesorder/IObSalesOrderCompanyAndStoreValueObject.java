package com.ebs.dda.jpa.order.salesorder;

public class IObSalesOrderCompanyAndStoreValueObject {
    private String salesOrderCode;
    private String storeCode;

    public String getSalesOrderCode() {
        return salesOrderCode;
    }

    public void setSalesOrderCode(String salesOrderCode) {
        this.salesOrderCode = salesOrderCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreCode() {
        return storeCode;
    }

}
