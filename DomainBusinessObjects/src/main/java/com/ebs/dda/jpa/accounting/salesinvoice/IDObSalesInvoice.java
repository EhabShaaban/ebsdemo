package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObSalesInvoice {

  String SYS_NAME = "SalesInvoice";

  String SALES_INVOICE_ITEMS_ATTR_NAME = "salesInvoiceItems";
  CompositeAttribute<IObSalesInvoiceItem> salesInvoiceItemsAttr =
      new CompositeAttribute<>(SALES_INVOICE_ITEMS_ATTR_NAME, IObSalesInvoiceItem.class, true);

  String POSTING_DETAILS_NAME = "postingDetails";
  ICompositeAttribute<IObSalesInvoicePostingDetails> postingDetails =
      new CompositeAttribute<>(POSTING_DETAILS_NAME, IObSalesInvoicePostingDetails.class, false);

  String BUSINESS_PARTNER_NAME = "businessPartner";
  CompositeAttribute<IObSalesInvoiceBusinessPartner> businessPartner =
      new CompositeAttribute<>(BUSINESS_PARTNER_NAME, IObSalesInvoiceBusinessPartner.class, false);

  String TAXES_NAME = "taxesData";
  CompositeAttribute<IObSalesInvoiceTaxes> taxesData =
      new CompositeAttribute<>(TAXES_NAME, IObSalesInvoiceTaxes.class, true);

  String COMPANY_DATA_NAME = "companyData";
  CompositeAttribute<IObSalesInvoiceCompanyData> companyData =
      new CompositeAttribute<>(COMPANY_DATA_NAME, IObSalesInvoiceCompanyData.class, false);

  String SUMMARY = "summary";
  ICompositeAttribute<IObSalesInvoiceSummary> summary =
          new CompositeAttribute<>(SUMMARY, IObSalesInvoiceSummary.class, false);
}
