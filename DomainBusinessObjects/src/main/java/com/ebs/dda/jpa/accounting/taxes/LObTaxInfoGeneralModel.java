package com.ebs.dda.jpa.accounting.taxes;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class LObTaxInfoGeneralModel extends LocalizedLookupObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
  private BigDecimal percentage;

  public BigDecimal getPercentage() {
    return percentage;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
