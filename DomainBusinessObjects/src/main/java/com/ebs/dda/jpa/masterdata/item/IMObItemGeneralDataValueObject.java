package com.ebs.dda.jpa.masterdata.item;

public interface IMObItemGeneralDataValueObject {
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String PURCHASING_UNIT_CODES = "purchasingUnitCodes";
  String MARKET_NAME = "marketName";

  String PURCHASE_UNIT_FIELD = "purchaseUnits";
  String PRODUCT_MANAGER_CODE = "productManagerCode";

}
