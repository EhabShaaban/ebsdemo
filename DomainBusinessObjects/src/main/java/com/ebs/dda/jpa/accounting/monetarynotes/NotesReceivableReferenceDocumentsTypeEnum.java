package com.ebs.dda.jpa.accounting.monetarynotes;

public enum NotesReceivableReferenceDocumentsTypeEnum {
  SALES_INVOICE(Values.SALES_INVOICE),
  SALES_ORDER(Values.SALES_ORDER),
  NOTES_RECEIVABLE(Values.NOTES_RECEIVABLE);


  private NotesReceivableReferenceDocumentsTypeEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }
  public static class Values {
    public static final String SALES_INVOICE = "SALES_INVOICE";
    public static final String SALES_ORDER = "SALES_ORDER";
    public static final String NOTES_RECEIVABLE = "NOTES_RECEIVABLE";
  }
}
