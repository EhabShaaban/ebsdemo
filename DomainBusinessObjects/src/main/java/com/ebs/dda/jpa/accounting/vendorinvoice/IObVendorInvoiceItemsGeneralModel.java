package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.api.annotations.Exclude;
import com.ebs.dda.jpa.accounting.IObInvoiceItemsGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObVendorInvoiceItemsGeneralModel")
public class IObVendorInvoiceItemsGeneralModel extends IObInvoiceItemsGeneralModel {
  private Long itemId;
  @Exclude private Long orderUnitId;

  public Long getItemId() {
    return itemId;
  }

  public Long getOrderUnitId() {
    return orderUnitId;
  }

  @Override
  public String getSysName() {
    return IIObVendorInvoiceItems.SYS_NAME;
  }
}
