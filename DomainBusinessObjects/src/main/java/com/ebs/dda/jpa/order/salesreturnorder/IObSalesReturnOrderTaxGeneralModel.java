package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesReturnOrderTaxGeneralModel")
public class IObSalesReturnOrderTaxGeneralModel extends InformationObject {

  private String salesReturnOrderCode;

  private String code;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private BigDecimal percentage;

  @Transient private BigDecimal amount;

  public String getSalesReturnOrderCode() {
    return salesReturnOrderCode;
  }

  public String getCode() {
    return code;
  }

  public LocalizedString getName() {
    return name;
  }

  public BigDecimal getPercentage() {
    return percentage;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
