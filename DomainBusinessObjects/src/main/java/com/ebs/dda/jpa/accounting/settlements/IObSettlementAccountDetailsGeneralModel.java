package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dac.dbo.api.annotations.Exclude;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSettlementAccountingDetailsGeneralModel")
@EntityInterface(IIObAccountingDocumentAccountDetailsGeneralModel.class)
public class IObSettlementAccountDetailsGeneralModel
    extends IObAccountingDocumentAccountingDetailsGeneralModel {
  private static final long serialVersionUID = 1L;

  @Exclude
  BigDecimal balance;


  public BigDecimal getBalance() {
    return balance;
  }

  @Override
  public String getSysName() {
    return IDObSettlement.SYS_NAME;
  }
}
