package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

public interface IDObMonetaryNotes {

  String NOTES_RECEIVABLE_SYS_NAME = "NotesReceivable";
  String NOTES_PAYABLE_SYS_NAME = "NotesPayable";

  String COMPANY = "company";
  String DETAILS_ATTR_NAME = "Details";
  String ACTIVATION_DETAILS = "activationDetails";
  String ACCOUNTING_DETAILS_ATTR_NAME = "AccountingDetails";
  String REFERENCE_DOCUMENTS_ATTR_NAME = "ReferenceDocuments";

  String PURCHASE_UNIT_NAME_ATTR = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR);

  CompositeAttribute<IObAccountingDocumentCompanyData> companyAttr =
      new CompositeAttribute<>(COMPANY, IObAccountingDocumentCompanyData.class, false);

  CompositeAttribute<IObMonetaryNotesDetails> detailsAttr =
      new CompositeAttribute<>(DETAILS_ATTR_NAME, IObMonetaryNotesDetails.class, false);

  CompositeAttribute<IObMonetaryNotesReferenceDocuments> referenceDocumentsAttr =
      new CompositeAttribute<>(
          REFERENCE_DOCUMENTS_ATTR_NAME, IObMonetaryNotesReferenceDocuments.class, true);

  CompositeAttribute<IObNotesReceivableActivationDetails> activationDetails =
      new CompositeAttribute(ACTIVATION_DETAILS, IObNotesReceivableActivationDetails.class, false);

  CompositeAttribute<IObAccountingDocumentAccountingDetails> accountingDetailsAttr =
      new CompositeAttribute<>(
          ACCOUNTING_DETAILS_ATTR_NAME, IObAccountingDocumentAccountingDetails.class, true);
}
