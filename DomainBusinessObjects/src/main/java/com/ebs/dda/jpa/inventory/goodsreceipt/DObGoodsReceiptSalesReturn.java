package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.inventory.goodsissue.IDObGoodsIssueSalesOrder;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dobgoodsreceiptsalesreturn")
@Inheritance(strategy = InheritanceType.JOINED)
@EntityInterface(IDObGoodsReceiptSalesReturn.class)
@DiscriminatorValue(value = "GR_SR")
public class DObGoodsReceiptSalesReturn extends DObGoodsReceipt implements Serializable {

  private static final long serialVersionUID = 1L;
  private Long typeId;

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  @Override
  public String getSysName() {
    return IDObGoodsIssueSalesOrder.SYS_NAME;
  }
}
