package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

public interface IDObCosting {
  String SYS_NAME = "Costing";

  String ACCOUNTING_DETAILS_ATTR_NAME = "AccountingDetails";
  String DETAILS_ATTR_NAME = "Details";
  String ACCOUNTING_DOCUMENT_ITEMS_ATTR_NAME = "AccountingDocumentItems";
  String ACTIVATION_DETAILS_ATTR_NAME = "activationDetails";

  String COMPANY_DATA_ATTR_NAME = "companydata";
  CompositeAttribute<IObCostingCompanyData> companyDataAttr =
      new CompositeAttribute<>(COMPANY_DATA_ATTR_NAME, IObCostingCompanyData.class, false);

  CompositeAttribute<IObAccountingDocumentAccountingDetails> accountingDetailsAttr =
      new CompositeAttribute<>(
          ACCOUNTING_DETAILS_ATTR_NAME, IObAccountingDocumentAccountingDetails.class, true);

  CompositeAttribute<IObCostingDetails> costingDetailsAttr =
      new CompositeAttribute<>(DETAILS_ATTR_NAME, IObCostingDetails.class, false);

  CompositeAttribute<IObCostingItem> accountingDocumentItemsAttr =
      new CompositeAttribute<>(ACCOUNTING_DOCUMENT_ITEMS_ATTR_NAME, IObCostingItem.class, true);

  CompositeAttribute<IObAccountingDocumentActivationDetails> accountingActivationDetailsAttr =
      new CompositeAttribute<>(
          ACTIVATION_DETAILS_ATTR_NAME, IObAccountingDocumentActivationDetails.class, false);
}
