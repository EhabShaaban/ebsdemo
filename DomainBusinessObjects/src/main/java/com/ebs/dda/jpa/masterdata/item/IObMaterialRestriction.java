package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author: Menna Sayed
 * @date: Jan 11, 2017
 */
@Entity
@Table(name = "IObMaterialRestriction")
@EntityInterface(IIObMaterialRestriction.class)
public class IObMaterialRestriction extends InformationObject implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "businessOperation")
  private Long businessOperation;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "restriction"))
  private MaterialStatusRestrictionEnum restriction;

  public MaterialStatusRestrictionEnum getRestriction() {
    return restriction;
  }

  public void setRestriction(MaterialStatusRestrictionEnum restriction) {
    this.restriction = restriction;
  }

  @Override
  public String getSysName() {
    return IIObMaterialRestriction.SYS_NAME;
  }
}
