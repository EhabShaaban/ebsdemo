package com.ebs.dda.jpa.accounting.vendorinvoice;

import javax.persistence.*;


@Entity
@Table(name = "dobvendorinvoicepostpreparationgeneralmodel")
public class DObVendorInvoicePostPreparationGeneralModel {
    @Id
    Long id;
    String code;
    Long currencyid;
    Long exchangerateid;
    String currencyprice;

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Long getCurrencyid() {
        return currencyid;
    }

    public Long getExchangerateid() {
        return exchangerateid;
    }

    public String getCurrencyprice() {
        return currencyprice;
    }
}
