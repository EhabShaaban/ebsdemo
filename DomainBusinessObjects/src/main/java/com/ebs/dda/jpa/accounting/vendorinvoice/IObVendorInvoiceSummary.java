package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IObVendorInvoiceSummary")
public class IObVendorInvoiceSummary extends DocumentHeader implements Serializable {

  private BigDecimal remaining;
  private BigDecimal downPayment;
  private BigDecimal totalAmount;

  public BigDecimal getRemaining() {
    return remaining;
  }

  public void setRemaining(BigDecimal remaining) {
    this.remaining = remaining;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(BigDecimal total) {
    this.totalAmount = total;
  }

  public BigDecimal getDownPayment() {
    return downPayment;
  }

  public void setDownPayment(BigDecimal downPayment) {
    this.downPayment = downPayment;
  }
}
