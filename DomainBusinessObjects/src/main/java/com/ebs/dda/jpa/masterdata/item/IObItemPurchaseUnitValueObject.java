package com.ebs.dda.jpa.masterdata.item;

// Created By Hossam Hassan @ 10/31/18
public class IObItemPurchaseUnitValueObject {

  private String purchaseUnitCode;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public void setPurchaseUnitCode(String purchaseUnitCode) {
    this.purchaseUnitCode = purchaseUnitCode;
  }
}
