package com.ebs.dda.jpa.masterdata.lookups;

public interface ILObIssueFrom {
  String SYS_NAME = "IssueFrom";
  String NAME = "name";
  String USER_CODE = "userCode";
  String ADDRESS = "address";
  String TELEPHONE = "telephone";
  String DESCRIPTION = "description";
}
