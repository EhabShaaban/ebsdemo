package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceiptGeneralModel;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObInitialStockUploadGeneralModel")
@EntityInterface(IDObInitialStockUploadGeneralModel.class)
public class DObInitialStockUploadGeneralModel extends StatefullBusinessObject<DefaultUserCode> {
    private static final long serialVersionUID = 1L;
    private String purchaseUnitCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString purchaseUnitName;
    private String purchaseUnitNameEn;

    private String companyCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString companyName;

    private String storeHouseCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString storehouseName;

    private String storekeeperCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString storekeeperName;

    public String getPurchaseUnitCode() {
        return purchaseUnitCode;
    }

    public LocalizedString getPurchaseUnitName() {
        return purchaseUnitName;
    }

    public String getPurchaseUnitNameEn() {
        return purchaseUnitNameEn;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public LocalizedString getCompanyName() {
        return companyName;
    }

    public String getStoreHouseCode() {
        return storeHouseCode;
    }

    public LocalizedString getStorehouseName() {
        return storehouseName;
    }

    public String getStorekeeperCode() {
        return storekeeperCode;
    }

    public LocalizedString getStorekeeperName() {
        return storekeeperName;
    }

    @Override
    public String getSysName() {
        return IDObInitialStockUpload.SYS_NAME;
    }
}
