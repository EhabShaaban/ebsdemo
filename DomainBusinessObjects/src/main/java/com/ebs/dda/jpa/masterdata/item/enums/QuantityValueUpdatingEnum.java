package com.ebs.dda.jpa.masterdata.item.enums;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 10, 2017 , 11:44:34 AM
 */
@Embeddable
public class QuantityValueUpdatingEnum extends BusinessEnum {

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private QuantityValueUpdating value;

  public QuantityValueUpdatingEnum() {}

  public QuantityValueUpdatingEnum(QuantityValueUpdating value) {
    this.value = value;
  }

  @Override
  public QuantityValueUpdating getId() {
    return value;
  }

  public enum QuantityValueUpdating {
    ALL_VALUATION_AREAS,
    NO_VALUATION_AREAS,
    PER_VALUATION_AREAS
  }
}
