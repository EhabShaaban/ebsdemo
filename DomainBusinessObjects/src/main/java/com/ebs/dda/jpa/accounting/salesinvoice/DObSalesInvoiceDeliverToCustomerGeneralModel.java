package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObjectAuthorizationGeneralModel;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "DObSalesInvoiceDeliverToCustomerGeneralModel")
@EntityInterface(IDObSalesInvoiceDeliverToCustomerGeneralModel.class)
public class DObSalesInvoiceDeliverToCustomerGeneralModel
				extends StatefullBusinessObjectAuthorizationGeneralModel {

	private String invoiceTypeCode;
	@Convert(converter = StatefullBusinessObjectCurrentStatesJpaConverter.class)
	private Set<String> salesOrderCurrentStates;
	@Override
	public String getSysName() {
		return IDObSalesInvoice.SYS_NAME;
	}


	public String getInvoiceTypeCode() {
		return invoiceTypeCode;
	}

	public Set<String> getSalesOrderCurrentStates() {
		return salesOrderCurrentStates;
	}
}
