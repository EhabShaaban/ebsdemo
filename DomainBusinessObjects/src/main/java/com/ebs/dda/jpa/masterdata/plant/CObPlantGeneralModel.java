package com.ebs.dda.jpa.masterdata.plant;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.plant.ICObPlant;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObPlantGeneralModel")
public class CObPlantGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "companyName")
  private LocalizedString companyName;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  @Column(name = "companyid")
  private Long companyId;

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public void setCompanyName(LocalizedString companyName) {
    this.companyName = companyName;
  }

  @Override
  public String getSysName() {
    return ICObPlant.SYS_NAME;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }
}
