package com.ebs.dda.jpa.masterdata.customer;

import java.util.List;

public class MObCustomerCreateValueObject {

  private String legalName;
  private String marketName;
  private List<String> businessUnitCodes;
  private Double creditLimit;
  private String currencyIso;
  private String registrationNumber;
  private String issuedFromCode;
  private String taxAdministrationCode;
  private String taxCardNumber;
  private String taxFileNumber;
  private String oldCustomerNumber;

  public String getLegalName() {
    return legalName;
  }

  public void setLegalName(String legalName) {
    this.legalName = legalName;
  }

  public String getMarketName() {
    return marketName;
  }

  public void setMarketName(String marketName) {
    this.marketName = marketName;
  }

  public List<String> getBusinessUnitCodes() {
    return businessUnitCodes;
  }

  public void setBusinessUnitCodes(List<String> businessUnitCodes) {
    this.businessUnitCodes = businessUnitCodes;
  }

  public Double getCreditLimit() {
    return creditLimit;
  }

  public void setCreditLimit(Double creditLimit) {
    this.creditLimit = creditLimit;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public void setCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
  }

  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  public String getIssuedFromCode() {
    return issuedFromCode;
  }

  public void setIssuedFromCode(String issuedFromCode) {
    this.issuedFromCode = issuedFromCode;
  }

  public String getTaxAdministrationCode() {
    return taxAdministrationCode;
  }

  public void setTaxAdministrationCode(String taxAdministrationCode) {
    this.taxAdministrationCode = taxAdministrationCode;
  }

  public String getTaxCardNumber() {
    return taxCardNumber;
  }

  public void setTaxCardNumber(String taxCardNumber) {
    this.taxCardNumber = taxCardNumber;
  }

  public String getTaxFileNumber() {
    return taxFileNumber;
  }

  public void setTaxFileNumber(String taxFileNumber) {
    this.taxFileNumber = taxFileNumber;
  }

  public String getOldCustomerNumber() {
    return oldCustomerNumber;
  }

  public void setOldCustomerNumber(String oldCustomerNumber) {
    this.oldCustomerNumber = oldCustomerNumber;
  }
}
