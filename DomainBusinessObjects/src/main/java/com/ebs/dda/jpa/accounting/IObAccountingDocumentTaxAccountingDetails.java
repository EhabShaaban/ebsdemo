package com.ebs.dda.jpa.accounting;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "IObAccountingDocumentTaxAccountingDetails")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Taxes")
public class IObAccountingDocumentTaxAccountingDetails
				extends IObAccountingDocumentAccountingDetails implements Serializable {
	private Long glSubAccountId;

	@Override
	public Long getGlSubAccountId() {
		return glSubAccountId;
	}

	@Override
	public void setGlSubAccountId(Long glSubAccountId) {
		this.glSubAccountId = glSubAccountId;
	}
}
