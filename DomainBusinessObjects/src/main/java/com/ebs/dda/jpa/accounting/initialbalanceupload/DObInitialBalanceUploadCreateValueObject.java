package com.ebs.dda.jpa.accounting.initialbalanceupload;

import org.springframework.web.multipart.MultipartFile;

public class DObInitialBalanceUploadCreateValueObject {

  private String purchaseUnitCode;
  private String companyCode;
  private String fiscalPeriodCode;
  private Long documentOwnerId;
  private MultipartFile attachment;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getFiscalPeriodCode() {
    return fiscalPeriodCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public MultipartFile getAttachment() {
    return attachment;
  }

  public void setAttachment(MultipartFile attachment) {
    this.attachment = attachment;
  }
}
