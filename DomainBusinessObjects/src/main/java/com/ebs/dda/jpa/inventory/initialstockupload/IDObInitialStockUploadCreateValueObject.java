package com.ebs.dda.jpa.inventory.initialstockupload;

public interface IDObInitialStockUploadCreateValueObject {

    String PURCHASE_UNIT = "purchaseUnitCode";
    String COMPANY = "companyCode";
    String STORE_HOUSE = "storeHouseCode";
    String STORE_KEEPER = "storeKeeperCode";
}
