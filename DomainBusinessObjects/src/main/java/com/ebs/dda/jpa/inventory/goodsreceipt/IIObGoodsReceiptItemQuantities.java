package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptItemQuantities {

    String SYS_NAME = "GoodsReceiptOrdinaryRecievedItems";
    String RECEIVED_QTY_UOE = "receivedQtyUoE";
    String UNIT_OF_ENTRY_CODE = "unitOfEntryCode";
    String NOTES = "notes";
    String STOCK_TYPE = "stockType";
}
