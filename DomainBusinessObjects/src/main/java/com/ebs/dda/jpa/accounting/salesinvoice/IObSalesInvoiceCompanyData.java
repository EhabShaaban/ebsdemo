package com.ebs.dda.jpa.accounting.salesinvoice;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

@Entity
@DiscriminatorValue(value = "SalesInvoice")
public class IObSalesInvoiceCompanyData extends IObAccountingDocumentCompanyData {

  private static final long serialVersionUID = 1L;

}
