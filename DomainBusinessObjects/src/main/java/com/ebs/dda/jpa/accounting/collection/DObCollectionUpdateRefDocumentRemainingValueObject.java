package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dda.jpa.accounting.monetarynotes.RefDocumentRemainingValueObject;

public class DObCollectionUpdateRefDocumentRemainingValueObject
    extends RefDocumentRemainingValueObject {
  private String collectionCode;

  public String getCollectionCode() {
    return collectionCode;
  }

  public void setCollectionCode(String collectionCode) {
    this.collectionCode = collectionCode;
  }
}
