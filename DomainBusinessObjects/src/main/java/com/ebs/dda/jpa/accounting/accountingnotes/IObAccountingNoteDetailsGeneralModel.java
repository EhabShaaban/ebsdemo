package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObAccountingNoteDetailsGeneralModel")
public class IObAccountingNoteDetailsGeneralModel extends InformationObject {

  private String code;

  private String businessPartnercode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnername;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerType;

  private Double amount;

  private String currencyISO;

  private String currencyCode;

  @Override
  public String getSysName() {
    return IDObAccountingNote.SYS_NAME;
  }
}
