package com.ebs.dda.jpa.masterdata.salesresponsible;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("7")
@EntityInterface(ICObSalesResponsible.class)
public class CObSalesResponsible extends CObEnterprise {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ICObSalesResponsible.SYS_NAME;
  }
}
