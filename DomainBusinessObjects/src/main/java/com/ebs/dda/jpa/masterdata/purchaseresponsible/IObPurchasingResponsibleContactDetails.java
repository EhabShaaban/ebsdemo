package com.ebs.dda.jpa.masterdata.purchaseresponsible;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.IIObContactDetails;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseContactDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/** @author xerix on Tue Nov 2017 at 10 : 44 */
@Entity
@DiscriminatorValue("9")
@EntityInterface(IIObContactDetails.class)
public class IObPurchasingResponsibleContactDetails extends IObEnterpriseContactDetails {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return "IObPurchasingResponsibleContactDetails";
  }
}
