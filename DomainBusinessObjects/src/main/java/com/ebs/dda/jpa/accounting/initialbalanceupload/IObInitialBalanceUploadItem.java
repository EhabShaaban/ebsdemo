package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.opencsv.bean.CsvBindByName;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "IObInitialBalanceUploadItem")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "subLedger")
public class IObInitialBalanceUploadItem extends DocumentLine implements Serializable {
  @Transient
  @CsvBindByName(column = "OldAccount")
  private String oldGlAccountCode;

  @CsvBindByName(column = "Debit")
  private Double debit;

  @CsvBindByName(column = "Credit")
  private Double credit;

  @Transient
  @CsvBindByName(column = "OldSubAccount")
  private String oldSubAccount;

  @Transient
  @CsvBindByName(column = "Currency")
  private String currencyCode;

  @CsvBindByName(column = "CurrencyPrice")
  private Double currencyPrice;

  private Long glAccountId;
  private Long currencyId;

  public void setOldGlAccountCode(String oldGlAccountCode) {
    this.oldGlAccountCode = oldGlAccountCode;
  }

  public void setOldSubAccount(String oldSubAccount) {
    this.oldSubAccount = oldSubAccount;
  }

  public String getOldGlAccountCode() {
    return oldGlAccountCode;
  }

  public String getOldSubAccount() {
    return oldSubAccount;
  }

  public void setDebit(Double debit) {
    this.debit = debit;
  }

  public void setCredit(Double credit) {
    this.credit = credit;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setCurrencyPrice(Double currencyPrice) {
    this.currencyPrice = currencyPrice;
  }

  public void setGlAccountId(Long glAccountId) {
    this.glAccountId = glAccountId;
  }

  public Double getDebit() {
    return debit;
  }

  public Double getCredit() {
    return credit;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public Double getCurrencyPrice() {
    return currencyPrice;
  }

  public Long getGlAccountId() {
    return glAccountId;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  @Override
  public String getSysName() {
    return IDObInitialBalanceUpload.SYS_NAME;
  }
}
