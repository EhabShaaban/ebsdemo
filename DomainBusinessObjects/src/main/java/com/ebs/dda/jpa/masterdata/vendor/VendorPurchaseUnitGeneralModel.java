package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;

public class VendorPurchaseUnitGeneralModel {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchasingUnitName;

  public LocalizedString getPurchasingUnitName() {
    return purchasingUnitName;
  }

  public void setPurchasingUnitName(LocalizedString purchasingUnitName) {
    this.purchasingUnitName = purchasingUnitName;
  }
}
