package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("NOTES_RECEIVABLE_AS_COLLECTION")
public class IObNotesReceivableActivationDetails extends IObAccountingDocumentActivationDetails {
}
