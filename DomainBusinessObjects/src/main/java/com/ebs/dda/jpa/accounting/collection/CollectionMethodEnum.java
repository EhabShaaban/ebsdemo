package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class CollectionMethodEnum extends BusinessEnum {

  public static final String SYS_NAME = "CollectionForm";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private CollectionMethod value;

  public CollectionMethodEnum() {}

  @Override
  public CollectionMethod getId() {
    return this.value;
  }

  public void setId(CollectionMethod id) {
    this.value = id;
  }

  public enum CollectionMethod {
    CASH,
    BANK
  }
}
