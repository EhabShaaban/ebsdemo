package com.ebs.dda.jpa.inventory.stocktransformation;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObStockTransformationGeneralModel")
@EntityInterface(IDObStockTransformationGeneralModel.class)
public class DObStockTransformationGeneralModel extends DocumentObject<DefaultUserCode> {

  private String newStockType;

  private Double transformedQty;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private String refStoreTransactionCode;

  private String newStoreTransactionCode;

  private String stockTransformationTypeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString stockTransformationTypeName;

  private String transformationReasonCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString transformationReasonName;

  public String getNewStockType() {
    return newStockType;
  }

  public Double getTransformedQty() {
    return transformedQty;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getRefStoreTransactionCode() {
    return refStoreTransactionCode;
  }

  public String getNewStoreTransactionCode() {
    return newStoreTransactionCode;
  }

  public String getStockTransformationTypeCode() {
    return stockTransformationTypeCode;
  }

  public LocalizedString getStockTransformationTypeName() {
    return stockTransformationTypeName;
  }

  public String getTransformationReasonCode() {
    return transformationReasonCode;
  }

  public LocalizedString getTransformationReasonName() {
    return transformationReasonName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  @Override
  public String getSysName() {
    return IDObStockTransformation.SYS_NAME;
  }
}
