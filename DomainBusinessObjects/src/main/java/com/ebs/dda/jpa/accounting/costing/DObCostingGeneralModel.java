package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "DObCostingGeneralModel")
@EntityInterface(IDObCostingGeneralModel.class)
public class DObCostingGeneralModel extends DocumentObject<DefaultUserCode>
    implements Serializable {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitCode;
  private String purchaseUnitNameEn;

  private String companyCodeName;
  private String purchaseUnitCodeName;
  private String documentOwner;

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getCompanyCodeName() {
    return companyCodeName;
  }

  public String getPurchaseUnitCodeName() {
    return purchaseUnitCodeName;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  @Override
  public String getSysName() {
    return IDObCosting.SYS_NAME;
  }
}
