package com.ebs.dda.jpa.accounting.landedcost;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 18, 2021
 */
public interface IIObLandedCostFactorItemsDetailsGeneralModel {

  final static String ACTUAL_VALUE = "actualValue";
  final static String DOCUMENT_INFO_CODE = "documentInfoCode";
  final static String DOCUMENT_INFO_TYPE = "documentInfoType";

}
