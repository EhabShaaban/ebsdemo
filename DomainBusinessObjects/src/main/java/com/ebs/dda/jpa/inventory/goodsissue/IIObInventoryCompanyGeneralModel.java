package com.ebs.dda.jpa.inventory.goodsissue;

public interface IIObInventoryCompanyGeneralModel {
  String COMPANY_NAME = "companyName";
  String STOREHOUSE_NAME = "storehouseName";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String PURCHASING_UNIT = "purchaseUnit";
}
