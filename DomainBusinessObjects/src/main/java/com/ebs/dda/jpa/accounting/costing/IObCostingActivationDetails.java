package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Costing")
public class IObCostingActivationDetails extends IObAccountingDocumentActivationDetails {
}
