package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IDObGoodsReceiptCreationValueObject {

  String TYPE = "type";
  String REF_DOCUMENT_CODE = "refDocumentCode";
  String STOREHOUSE = "storehouseCode";
  String STOREKEEPER = "storeKeeperCode";
  String COMPANY = "companyCode";
  String PURCHASE_ORDER = "PURCHASE_ORDER";
  String SALES_RETURN = "SALES_RETURN";
}
