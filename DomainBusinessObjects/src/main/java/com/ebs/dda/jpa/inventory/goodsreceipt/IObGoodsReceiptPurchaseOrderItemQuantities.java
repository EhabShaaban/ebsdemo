package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IIObGoodsReceiptItemQuantities.class)
@Table(name = "IObGoodsReceiptPurchaseOrderItemQuantities")
public class IObGoodsReceiptPurchaseOrderItemQuantities extends IObGoodsReceiptItemDetail {
}
