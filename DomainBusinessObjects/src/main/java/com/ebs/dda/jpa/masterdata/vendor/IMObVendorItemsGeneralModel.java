package com.ebs.dda.jpa.masterdata.vendor;

public interface IMObVendorItemsGeneralModel {
	String ITEM_CODE = "itemCode";
	String ITEM_NAME = "itemName";
	String VENDOR_CODE = "vendorCode";
	String VENDOR_NAME = "vendorName";
	String BASE_UNIT_CODE = "baseUnitCode";
	String BASE_UNIT_SYMBOL = "baseUnitSymbol";
	String IVR_MEASURE_CODE = "ivrMeasureCode";
	String IVR_MEASURE_SYMBOL = "ivrMeasureSymbol";
	String BUSINESS_UNIT_CODE = "businessUnitCode";
	String BUSINESS_UNIT_NAME = "businessUnitName";
    String CONVERSION_FACTOR = "conversionFactor";
}
