package com.ebs.dda.jpa.accounting.paymentrequest;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DObPaymentReferenceDocumentGeneralModel {
	@Id
	private Long id;
	private String code;
	private Long companyId;
	private Long currencyId;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}
}
