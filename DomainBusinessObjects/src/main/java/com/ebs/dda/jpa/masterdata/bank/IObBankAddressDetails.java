package com.ebs.dda.jpa.masterdata.bank;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.AddressDetails;
import com.ebs.dda.jpa.masterdata.enterprise.IIObEnterpriseAddressDetails;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObBankAddressDetails")
@EntityInterface(IIObEnterpriseAddressDetails.class)
public class IObBankAddressDetails extends AddressDetails {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return "IObBankAddressDetails";
  }
}
