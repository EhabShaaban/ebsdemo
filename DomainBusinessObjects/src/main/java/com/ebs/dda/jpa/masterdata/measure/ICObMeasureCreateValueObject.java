package com.ebs.dda.jpa.masterdata.measure;

public interface ICObMeasureCreateValueObject {
  String UNIT_OF_MEASURE_NAME = "unitOfMeasureName";
  String IS_STANDARD = "isStandard";
}
