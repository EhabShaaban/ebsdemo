package com.ebs.dda.jpa.accounting.costing;

public class DObCostingDocumentRecostingValueObject {
    private String purchaseOrderCode;
    private String goodsReceiptCode;
    private String landedCostCode;

    public DObCostingDocumentRecostingValueObject(String vendorInvoiceCode, String goodsReceiptCode, String landedCostCode) {
        this.purchaseOrderCode = vendorInvoiceCode;
        this.goodsReceiptCode = goodsReceiptCode;
        this.landedCostCode = landedCostCode;
    }

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    public String getGoodsReceiptCode() {
        return goodsReceiptCode;
    }

    public void setGoodsReceiptCode(String goodsReceiptCode) {
        this.goodsReceiptCode = goodsReceiptCode;
    }

    public String getLandedCostCode() {
        return landedCostCode;
    }

    public void setLandedCostCode(String landedCostCode) {
        this.landedCostCode = landedCostCode;
    }
}
