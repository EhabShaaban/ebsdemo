package com.ebs.dda.jpa.order.salesorder;

public interface IIObSalesOrderItemValueObject {
  String ITEM_CODE = "itemCode";
  String UNIT_OF_MEASURE_CODE = "unitOfMeasureCode";
  String SALES_PRICE = "salesPrice";
  String QUANTITY = "quantity";
}
