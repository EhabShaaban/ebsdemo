package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IIObCustomerBusinessUnitGeneralModel.class)
@Table(name = "IObCustomerBusinessUnitGeneralModel")
public class IObCustomerBusinessUnitGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;
  private String businessUnitcode;
  private String businessUnitName;
  private String purchaseUnitName;
  private Long customerId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public String getBusinessUnitcode() {
    return businessUnitcode;
  }

  public String getBusinessUnitName() {
    return businessUnitName;
  }

  public Long getCustomerId() {
    return customerId;
  }

  public String getPurchaseUnitName() {
    return purchaseUnitName;
  }

  @Override
  public String getSysName() {
    return IIObCustomerBusinessUnitGeneralModel.SYS_NAME;
  }
}
