package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IObGoodsIssueRefDocumentDataGeneralModel")
@EntityInterface(IIObGoodsIssueRefDocumentDataGeneralModel.class)
public class IObGoodsIssueRefDocumentDataGeneralModel {

  @Id private String id;
  private String goodsIssueCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  private String customerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString address;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString contactPersonName;

  private String contactPersonNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString salesRepresentativeName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString kapName;

  private String kapCode;

  private String comments;

  private String goodsIssueState;

  private String refDocument;

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public LocalizedString getAddress() {
    return address;
  }

  public LocalizedString getContactPersonName() {
    return contactPersonName;
  }

  public String getComments() {
    return comments;
  }

  public LocalizedString getKapName() {
    return kapName;
  }

  public String getKapCode() {
    return kapCode;
  }

  public String getGoodsIssueCode() {
    return goodsIssueCode;
  }

  public String getId() {
    return id;
  }

  public String getContactPersonNameEn() {
    return contactPersonNameEn;
  }

  public LocalizedString getSalesRepresentativeName() {
    return salesRepresentativeName;
  }

  public String getRefDocument() {
    return refDocument;
  }

  public String getGoodsIssueState() {
    return goodsIssueState;
  }
}
