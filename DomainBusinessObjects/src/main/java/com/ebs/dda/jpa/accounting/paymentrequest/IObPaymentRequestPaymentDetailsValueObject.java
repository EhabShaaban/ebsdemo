package com.ebs.dda.jpa.accounting.paymentrequest;

import java.math.BigDecimal;

public class IObPaymentRequestPaymentDetailsValueObject {
	private String paymentRequestCode;
	private String currencyCode;
	private BigDecimal netAmount;
	private String description;
	private Long companyBankId;
	private String costFactorItemCode;
	private String treasuryCode;

	public String getPaymentRequestCode() {
		return paymentRequestCode;
	}

	public void setPaymentRequestCode(String paymentRequestCode) {
		this.paymentRequestCode = paymentRequestCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}

	public String getDescription() {
		return description;
	}

	public Long getCompanyBankId() {
		return companyBankId;
	}

	public String getCostFactorItemCode() {
		return costFactorItemCode;
	}

	public String getTreasuryCode() {
		return treasuryCode;
	}
}
