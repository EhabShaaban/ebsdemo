package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IObAccountingDocumentActivationDetails")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public class IObAccountingDocumentActivationDetails extends DocumentHeader implements Serializable {

  private String accountant;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime activationDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private Long fiscalPeriodId;

  private String objectTypeCode;

  private Long exchangeRateId;
  private BigDecimal currencyPrice;

  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }

  public void setCurrencyPrice(BigDecimal currencyPrice) {
    this.currencyPrice = currencyPrice;
  }

  public Long getExchangeRateId() {
    return exchangeRateId;
  }

  public void setExchangeRateId(Long exchangeRateId) {
    this.exchangeRateId = exchangeRateId;
  }

  public String getAccountant() {
    return accountant;
  }

  public void setAccountant(String accountant) {
    this.accountant = accountant;
  }

  public DateTime getActivationDate() {
    return activationDate;
  }

  public void setActivationDate(DateTime postingDate) {
    this.activationDate = postingDate;
  }

  public DateTime getDueDate() {
    return dueDate;
  }

  public void setDueDate(DateTime dueDate) {
    this.dueDate = dueDate;
  }

  public Long getFiscalPeriodId() {
    return fiscalPeriodId;
  }

  public void setFiscalPeriodId(Long fiscalPeriodId) {
    this.fiscalPeriodId = fiscalPeriodId;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public void setObjectTypeCode(String objectTypeCode) {
    this.objectTypeCode = objectTypeCode;
  }
}
