package com.ebs.dda.jpa.masterdata.buisnesspartner;

import com.ebs.dda.jpa.masterdata.MasterDataMinor;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "MObBusinessPartner")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class MObBusinessPartner extends MasterDataMinor {

  private static final long serialVersionUID = 1L;
}
