package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.*;

/**
 * @author: Menna Sayed
 * @date: Jan 11, 2017
 */
@Entity
@Table(name = "IObMaterialRestrictionGeneralModel")
public class IObMaterialRestrictionGeneralModel extends BusinessObject {
  private static final long serialVersionUID = 1L;

  @Column(name = "refinstanceid")
  private Long materialStatusId;

  @Column(name = "businessOperation")
  private Long businessOperation;

  @Embedded
  @Column(name = "restriction")
  private MaterialStatusRestrictionEnum restriction;

  @Transient private LocalizedString localizedRestriction;

  public IObMaterialRestrictionGeneralModel() {}

  public Long getMaterialStatusId() {
    return materialStatusId;
  }

  public void setMaterialStatusId(Long materialStatusId) {
    this.materialStatusId = materialStatusId;
  }

  public MaterialStatusRestrictionEnum getRestriction() {
    return restriction;
  }

  public void setRestriction(MaterialStatusRestrictionEnum restriction) {
    this.restriction = restriction;
  }

  @Override
  public String getSysName() {
    return "Material Restriction General Model";
  }

  public LocalizedString getLocalizedRestriction() {
    return this.localizedRestriction;
  }

  public void setLocalizedRestriction(LocalizedString localizedRestriction) {
    this.localizedRestriction = localizedRestriction;
  }
}
