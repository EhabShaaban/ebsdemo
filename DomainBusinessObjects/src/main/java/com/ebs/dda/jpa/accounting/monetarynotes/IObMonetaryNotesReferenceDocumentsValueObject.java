package com.ebs.dda.jpa.accounting.monetarynotes;

import java.math.BigDecimal;

public class IObMonetaryNotesReferenceDocumentsValueObject {

  private String notesReceivableCode;
  private String documentType;
  private BigDecimal amountToCollect;
  private String refDocumentCode;

  public String getNotesReceivableCode() {
    return notesReceivableCode;
  }

  public void setNotesReceivableCode(String notesReceivableCode) {
    this.notesReceivableCode = notesReceivableCode;
  }

  public String getDocumentType() {
    return documentType;
  }

  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }

  public BigDecimal getAmountToCollect() {
    return amountToCollect;
  }

  public void setAmountToCollect(BigDecimal amountToCollect) {
    this.amountToCollect = amountToCollect;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public void setRefDocumentCode(String refDocumentCode) {
    this.refDocumentCode = refDocumentCode;
  }
}
