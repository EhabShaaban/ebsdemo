package com.ebs.dda.jpa.masterdata.ivr;

public class ItemVendorRecordValueObject {

  private String itemCode;

  private String vendorCode;

  private String itemVendorCode;

  private String oldItemNumber;

  private Double price;

  private String currencyCode;

  private String purchaseUnitCode;

  private String unitOfMeasureCode;

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public String getItemVendorCode() {
    return itemVendorCode;
  }

  public void setItemVendorCode(String itemVendorCode) {
    this.itemVendorCode = itemVendorCode;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public void setPurchaseUnitCode(String purchaseUnitsCode) {
    this.purchaseUnitCode = purchaseUnitsCode;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public void setUnitOfMeasureCode(String unitOfMeasureCode) {
    this.unitOfMeasureCode = unitOfMeasureCode;
  }

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  public void setOldItemNumber(String oldItemNumber) {
    this.oldItemNumber = oldItemNumber;
  }
}
