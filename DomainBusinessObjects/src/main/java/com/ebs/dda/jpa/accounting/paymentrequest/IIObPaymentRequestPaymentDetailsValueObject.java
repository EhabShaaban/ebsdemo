package com.ebs.dda.jpa.accounting.paymentrequest;

public interface IIObPaymentRequestPaymentDetailsValueObject {
    String PR_CODE = "paymentRequestCode";
    String PR_NET_AMOUNT = "netAmount";
    String PR_DESCRIPTION = "description";
    String PR_CURRENCY_CODE = "currencyCode";
    String COST_FACTOR_ITEM_CODE = "costFactorItemCode";
    String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
    String BANK_ACCOUNT_CODE = "bankAccountCode";
    String COMPANY_BANK_ID = "companyBankId";
    String TREASURY_CODE = "treasuryCode";
}
