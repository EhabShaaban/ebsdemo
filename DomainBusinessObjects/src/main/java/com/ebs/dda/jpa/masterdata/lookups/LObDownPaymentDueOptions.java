package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("6")
public class LObDownPaymentDueOptions extends LObMaterial {

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_DOWN_PAYMENT_DUE_OPTIONS_SYS_NAME;
  }
}
