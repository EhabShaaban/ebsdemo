package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dda.jpa.IValueObject;

public class DObPaymentRequestAccountDeterminationValueObject implements IValueObject {

  private String code;

  public void setCode(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  @Override
  public void userCode(String userCode) {
  this.code = userCode;
  }
}
