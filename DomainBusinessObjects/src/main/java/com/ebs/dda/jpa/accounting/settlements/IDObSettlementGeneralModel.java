package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObSettlementGeneralModel {

  String SYS_NAME = "Settlement";
  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String COMPANY_NAME_EN = "companyNameEn";
  String COMPANY_NAME = "companyName";
  String SETTLEMENT_TYPE = "settlementType";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME_EN);

  String CREATION_INFO = "creationInfo";
  BasicAttribute creationInfoAttr = new BasicAttribute(CREATION_INFO);

  String MODIFICATION_INFO = "modificationInfo";
  BasicAttribute modificationInfoAttr = new BasicAttribute(MODIFICATION_INFO);

  String ACTIVATED_BY = "activatedBy";

  String USER_CODE = "userCode";

  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String COMPANY_CODE = "companyCode";
  String CURRENT_STATES = "currentStates";
  String CREATION_DATE = "creationDate";
  String CURRENCY_ISO = "currencyIso";
  String CODE = "Code";
  String NAME = "Name";
  String TYPE = "Type";
  String VALUE = "Value";
  String CURRENCY = "Currency";
  String REMAINING = "remaining";
  String DOCUMENT_OWNER = "documentOwner";
}
