package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.eclipse.persistence.annotations.ReadOnly;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObPaymentRequestGeneralModel")
@EntityInterface(IDObPaymentRequestGeneralModel.class)
public class DObPaymentRequestGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;
  private String paymentType;
  private String paymentForm;
  private BigDecimal amountValue;
  private String amountCurrency;
  private Long purchaseUnitId;
  private String purchaseUnitCode;
  private String dueDocumentType;
  private String documentOwner;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private String businessPartnerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString referenceDocumentType;

  private String referenceDocumentCode;
  private BigDecimal remaining;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime expectedDueDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime fromExpectedDueDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime toExpectedDueDate;

  private String bankTransRef;

  public String getPaymentType() {
    return paymentType;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPaymentForm() {
    return paymentForm;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public BigDecimal getAmountValue() {
    return amountValue;
  }

  public String getAmountCurrency() {
    return amountCurrency;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public LocalizedString getBusinessPartnerName() {
    return businessPartnerName;
  }

  public LocalizedString getReferenceDocumentType() {
    return referenceDocumentType;
  }

  public String getReferenceDocumentCode() {
    return referenceDocumentCode;
  }

  public String getDueDocumentType() {
    return dueDocumentType;
  }

  public void setDueDocumentType(String dueDocumentType) {
    this.dueDocumentType = dueDocumentType;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }

  public DateTime getExpectedDueDate() {
    return expectedDueDate;
  }

  public String getBankTransRef() {
    return bankTransRef;
  }

  public DateTime getFromExpectedDueDate() {
    return fromExpectedDueDate;
  }

  public DateTime getToExpectedDueDate() {
    return toExpectedDueDate;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  @Override
  public String getSysName() {
    return IDObPaymentRequest.SYS_NAME;
  }
}
