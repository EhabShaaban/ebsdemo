package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObGoodsIssueItemGeneralModel")
@EntityInterface(IIObGoodsIssueItemGeneralModel.class)
public class IObGoodsIssueItemGeneralModel {

    @Id private String id;

    private String goodsIssueCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString itemName;

    private String itemCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString unitOfMeasureSymbol;

    private String unitOfMeasureCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString baseUnitSymbol;

    private String baseUnitCode;

    private BigDecimal quantity;

    private BigDecimal receivedQtyBase;

    private String batchNo;

    private BigDecimal price;

    public String getGoodsIssueCode() {
        return goodsIssueCode;
    }

    public LocalizedString getItemName() {
        return itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public LocalizedString getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public String getUnitOfMeasureCode() {
        return unitOfMeasureCode;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public LocalizedString getBaseUnitSymbol() {
        return baseUnitSymbol;
    }

    public String getBaseUnitCode() {
        return baseUnitCode;
    }

    public BigDecimal getReceivedQtyBase() {
        return receivedQtyBase;
    }
}
