package com.ebs.dda.jpa.accounting.taxes;

import com.ebs.dda.jpa.accounting.taxes.LObTaxInfo;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "LObCompanyTaxes")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "CompanyTaxes")
public class LObCompanyTaxes extends LObTaxInfo implements Serializable {

  private Long companyId;

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }
}
