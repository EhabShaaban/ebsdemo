package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObMonetaryNotesDetailsGeneralModel")
public class IObMonetaryNotesDetailsGeneralModel extends InformationObject {

  private String userCode;

  private String objectTypeCode;

  private BigDecimal amount;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private String noteNumber;
  private String noteBank;

  private String depotCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString depotName;

  private String businessPartnerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  private String currencyCode;
  private String currencyIso;

  private BigDecimal remaining;
  private String noteForm;

  private String companyCurrencyCode;
  private String companyCurrencyIso;

  public String getUserCode() {
    return userCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public DateTime getDueDate() {
    return dueDate;
  }

  public String getNoteNumber() {
    return noteNumber;
  }

  public String getNoteBank() {
    return noteBank;
  }

  public String getDepotCode() {
    return depotCode;
  }

  public LocalizedString getDepotName() {
    return depotName;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public LocalizedString getBusinessPartnerName() {
    return businessPartnerName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }

  public String getNoteForm() {
    return noteForm;
  }

  public String getCompanyCurrencyCode() {
    return companyCurrencyCode;
  }

  public String getCompanyCurrencyIso() {
    return companyCurrencyIso;
  }

  @Override
  public String getSysName() {
    return IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME;
  }

}
