package com.ebs.dda.jpa.accounting.salesinvoice;

public interface IDObSalesInvoiceCreationValueObject {


  public static final String INVOICE_TYPE_CODE = "invoiceTypeCode";
  public static final String SALES_ORDER_CODE = "salesOrderCode";
  public static final String BUSINESS_UNIT_CODE = "businessUnitCode";
  public static final String COMPANY_CODE = "companyCode";
  public static final String CUSTOMER_CODE = "customerCode";
  public static final String DOCUMENT_OWNER_ID = "documentOwnerId";

}
