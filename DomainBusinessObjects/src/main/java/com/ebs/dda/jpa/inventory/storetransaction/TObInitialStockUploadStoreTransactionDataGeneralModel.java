package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.TransactionObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "TObInitialStockUploadStoreTransactionDataGeneralModel")
public class TObInitialStockUploadStoreTransactionDataGeneralModel
    extends TransactionObject<DefaultUserCode> {

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime originalAddingDate;

  private Long storehouseId;
  private Long plantId;
  private Long companyId;
  private Long purchaseunitId;
  private Long itemId;
  private Long initialStockUploadId;
  private BigDecimal actualCost;
  private BigDecimal estimateCost;
  private BigDecimal quantity;
  private BigDecimal remainingQuantity;
  private String stockType;
  private Long unitOfMeasureId;

  public Long getInitialStockUploadId() {
    return initialStockUploadId;
  }

  public Long getStorehouseId() {
    return storehouseId;
  }

  public Long getPlantId() {
    return plantId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public Long getPurchaseunitId() {
    return purchaseunitId;
  }

  public Long getItemId() {
    return itemId;
  }

  public BigDecimal getActualCost() {
    return actualCost;
  }

  public BigDecimal getEstimateCost() {
    return estimateCost;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public BigDecimal getRemainingQuantity() {
    return remainingQuantity;
  }

  public String getStockType() {
    return stockType;
  }

  public Long getUnitOfMeasureId() {
    return unitOfMeasureId;
  }

  public DateTime getOriginalAddingDate() {
    return originalAddingDate;
  }

  @Override
  public String getSysName() {
    return ITObStoreTransaction.SYS_NAME;
  }
}
