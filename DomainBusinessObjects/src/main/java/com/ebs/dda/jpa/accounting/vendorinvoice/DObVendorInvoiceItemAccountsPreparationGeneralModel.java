package com.ebs.dda.jpa.accounting.vendorinvoice;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObVendorInvoiceItemAccountsPreparationGeneralModel")
public class DObVendorInvoiceItemAccountsPreparationGeneralModel {
    @Id
    private Long id;
    private String code;
    private String objectTypeCode;
    private Long glAccountId;
    private Long orderId;
    private String accountLedger;
    private BigDecimal amount;

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getObjectTypeCode() {
        return objectTypeCode;
    }

    public Long getGlAccountId() {
        return glAccountId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public String getAccountLedger() {
        return accountLedger;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
