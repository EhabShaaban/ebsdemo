package com.ebs.dda.jpa.masterdata.salesresponsible;

public interface ICObSalesResponsible {

  String SYS_NAME = "SalesResponsible";

  String USER_CODE = "userCode";
  String NAME = "name";
}
