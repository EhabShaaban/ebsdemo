package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IMObVendor {

  String SYS_NAME = "Vendor";
  String CODE = "userCode";
  String VENDOR_NAME = "vendorName";

  String PURCHASE_UNIT_ATTR_NAME = "purchasingUnitNames";
  CompositeAttribute<IObVendorPurchaseUnit> purchseUnitAttr =
          new CompositeAttribute<>(PURCHASE_UNIT_ATTR_NAME, IObVendorPurchaseUnit.class, true);

  String ACCOUNTING_INFO_ATTR_NAME = "accountingDetails";
  CompositeAttribute<IObVendorAccountingInfo> accountingDetailsAttr =
          new CompositeAttribute<>(ACCOUNTING_INFO_ATTR_NAME, IObVendorAccountingInfo.class, false);
}
