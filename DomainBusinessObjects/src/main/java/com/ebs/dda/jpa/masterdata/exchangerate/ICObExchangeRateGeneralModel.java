package com.ebs.dda.jpa.masterdata.exchangerate;

public interface ICObExchangeRateGeneralModel {

  String EXCHANGE_RATE_CODE = "userCode";
  String EXCHANGE_RATE_TYPE = "exchangeRateType";
  String EXCHANGE_RATE_TYPE_NAME = "exchangeRateTypeName";
  String VALID_FROM = "validFrom";
  String FIRST_CURRENCY_ISO = "firstCurrencyIso";
  String SECOND_CURRENCY_ISO = "secondCurrencyIso";
  String FIRST_CURRENCY_CODE = "firstCurrencyCode";
  String SECOND_CURRENCY_CODE = "secondCurrencyCode";
  String FIRST_VALUE = "firstValue";
  String SECOND_VALUE = "secondValue";
  String CREATED_BY = "creationInfo";
}
