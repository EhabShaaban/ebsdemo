package com.ebs.dda.jpa.masterdata.vendor;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import java.math.BigDecimal;

@Entity
@Table(name = "MObVendorItemsGeneralModel")
public class MObVendorItemsGeneralModel {
	@Id
	private Long id;

	private String itemCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString itemName;

	private String vendorCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString vendorName;

	private String baseUnitCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString baseUnitSymbol;

	private String ivrMeasureCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString ivrMeasureSymbol;

	private String businessUnitCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString businessUnitName;

	private BigDecimal conversionFactor;

    public Long getId() {
        return id;
    }

    public String getItemCode() {
        return itemCode;
    }

    public LocalizedString getItemName() {
        return itemName;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public LocalizedString getVendorName() {
        return vendorName;
    }

    public String getBaseUnitCode() {
        return baseUnitCode;
    }

    public LocalizedString getBaseUnitSymbol() {
        return baseUnitSymbol;
    }

    public String getBusinessUnitCode() {
        return businessUnitCode;
    }

    public LocalizedString getBusinessUnitName() {
        return businessUnitName;
    }

    public String getIvrMeasureCode() {
        return ivrMeasureCode;
    }

    public LocalizedString getIvrMeasureSymbol() {
        return ivrMeasureSymbol;
    }

    public BigDecimal getConversionFactor() {
        return conversionFactor;
    }
}
