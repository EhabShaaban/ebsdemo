package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 30, 2017 , 4:16:59 PM
 */
@Entity
@DiscriminatorValue("30")
public class LObDivision extends LObMaterial {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_DIVISION_SYS_NAME;
  }
}
