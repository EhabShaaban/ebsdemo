package com.ebs.dda.jpa.masterdata.ivr;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ItemVendorCommonPurchaseUnitsGeneralModel")
public class ItemVendorCommonPurchaseUnitsGeneralModel extends BusinessObject {

  @Column(name = "vendorcode")
  private String vendorCode;

  @Column(name = "itemcode")
  private String itemCode;

  @Column(name = "purchaseunitcode")
  private String purchasingUnitCode;


  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "purchaseunitname")
  private LocalizedString purchaseUnitName;

  @Override
  public String getSysName() {
    return "CommonPurchaseUnits";
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public String getPurchasingUnitCode() {
    return purchasingUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }
}
