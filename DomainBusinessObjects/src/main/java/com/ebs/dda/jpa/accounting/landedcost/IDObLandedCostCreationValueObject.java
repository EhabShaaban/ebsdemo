package com.ebs.dda.jpa.accounting.landedcost;

public interface IDObLandedCostCreationValueObject {

  String LANDED_COST_TYPE = "landedCostType";
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String DOCUMENT_OWNER = "documentOwnerId";


}
