package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.processing.CompositeAttribute;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 30, 2017 , 3:11:55 PM
 */
public interface IMObCustomer {
  String SYS_NAME = "Customer";

  String PURCHASE_UNIT_ATTR_NAME = "purchaseUnit";
  CompositeAttribute<IObCustomerBusinessUnit> purchaseUnitAttr =
      new CompositeAttribute<>(PURCHASE_UNIT_ATTR_NAME, IObCustomerBusinessUnit.class, true);

  String LEGAL_REGISTRATION_DATA_ATTR_NAME = "legalRegistrationData";
  CompositeAttribute<IObCustomerLegalRegistrationData> legalRegistrationDataAttr =
      new CompositeAttribute<>(LEGAL_REGISTRATION_DATA_ATTR_NAME, IObCustomerLegalRegistrationData.class, false);

}
