package com.ebs.dda.jpa.accounting.vendorinvoice;
public interface IIObVendorInvoiceDetails {
  String INVOICE_NUMBER = "invoiceNumber";
  String CURRENCY_ISO = "currencyISO";
  String CURRENCY_NAME = "currencyName";
  String PAYMENT_TERM = "paymentTerm";
  String INVOICE_DATE="invoiceDate";
  String DOWN_PAYMENT = "downPayment";
  String TAX="tax";
}
