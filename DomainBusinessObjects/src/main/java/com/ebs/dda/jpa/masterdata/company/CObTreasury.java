package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObTreasury")
public class CObTreasury extends CodedBusinessObject<DefaultUserCode> {

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString name;

	@Override
	public String getSysName() {
		return ICObTreasury.SYS_NAME;
	}

}
