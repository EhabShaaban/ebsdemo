package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.IObTaxes;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObSalesInvoiceTaxes")
public class IObSalesInvoiceTaxes extends IObTaxes implements Serializable {
}
