package com.ebs.dda.jpa.accounting.landedcost;

import java.math.BigDecimal;

public class IObAddLandedCostFactorItemValueObject {

  private String landedCostCode;
  private String itemCode;
  private BigDecimal value;

  public String getLandedCostCode() {
    return landedCostCode;
  }

  public void setLandedCostCode(String landedCostCode) {
    this.landedCostCode = landedCostCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }


  public BigDecimal getValue() {
    return value;
  }

  public void setValue(BigDecimal value) {
    this.value = value;
  }
}
