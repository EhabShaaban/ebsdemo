package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;

import javax.persistence.*;

@Entity
@Table(name = "CObCompany")
@DiscriminatorValue("1")
@Inheritance(strategy = InheritanceType.JOINED)
@EntityInterface(ICObCompany.class)
public class CObCompany extends CObEnterprise {
  public static final String SYS_NAME = "Company";
  private static final long serialVersionUID = 1L;

  @Column(name = "companyGroupId")
  private Long companyGroup;

  private Long taxAdministrativeId;

  private String regNumber;

  private String taxCardNumber;

  private String taxFileNumber;

  public Long getTaxAdministrativeId() {
    return taxAdministrativeId;
  }

  public void setTaxAdministrativeId(Long taxAdministrativeId) {
    this.taxAdministrativeId = taxAdministrativeId;
  }

  public String getRegNumber() {
    return regNumber;
  }

  public void setRegNumber(String regNumber) {
    this.regNumber = regNumber;
  }

  public String getTaxCardNumber() {
    return taxCardNumber;
  }

  public void setTaxCardNumber(String taxCardNumber) {
    this.taxCardNumber = taxCardNumber;
  }

  public String getTaxFileNumber() {
    return taxFileNumber;
  }

  public void setTaxFileNumber(String taxFileNumber) {
    this.taxFileNumber = taxFileNumber;
  }

  @Override
  public String getSysName() {
    return CObCompany.SYS_NAME;
  }

  public <SingleDetail extends InformationObject> void setSingleDetail(
      ICompositeAttribute<SingleDetail> singleDetailAttribute, SingleDetail singleDetail) {
    this.aggregateBusinessObject().putSingleInstanceAttribute(singleDetailAttribute, singleDetail);
  }
}
