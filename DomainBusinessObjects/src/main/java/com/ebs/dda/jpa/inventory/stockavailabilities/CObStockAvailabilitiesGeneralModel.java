package com.ebs.dda.jpa.inventory.stockavailabilities;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "Cobstockavailabilitiesgeneralmodel")
public class CObStockAvailabilitiesGeneralModel extends CodedBusinessObject<DefaultUserCode> {

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString name;

    private String itemCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString itemName;

    private String companyCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString companyName;

    private String plantCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString plantName;

    private String storehouseCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString storehouseName;

    private String purchaseUnitCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString purchaseUnitName;

    private String purchaseUnitNameEn;

    private String unitOfMeasureCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString unitOfMeasureName;

    private String stockType;

    private BigDecimal availableQuantity;

    public String getItemCode() {
        return itemCode;
    }

    public LocalizedString getItemName() {
        return itemName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public LocalizedString getCompanyName() {
        return companyName;
    }

    public String getPlantCode() {
        return plantCode;
    }

    public LocalizedString getPlantName() {
        return plantName;
    }

    public String getStorehouseCode() {
        return storehouseCode;
    }

    public LocalizedString getStorehouseName() {
        return storehouseName;
    }

    public String getPurchaseUnitCode() {
        return purchaseUnitCode;
    }

    public LocalizedString getPurchaseUnitName() {
        return purchaseUnitName;
    }

    public String getPurchaseUnitNameEn() {
        return purchaseUnitNameEn;
    }

    public String getUnitOfMeasureCode() {
        return unitOfMeasureCode;
    }

    public LocalizedString getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public String getStockType() {
        return stockType;
    }

    public BigDecimal getAvailableQuantity() {
        return availableQuantity;
    }

    public String getSysName() {
        return ICObStockAvailabilitiyGeneralModel.SYS_NAME;
    }
}
