package com.ebs.dda.jpa.inventory.stockavailabilities;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "cobstockavailability")
public class CObStockAvailability extends CodedBusinessObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private Long itemId;

  private Long companyId;

  private Long plantId;

  private Long storehouseId;

  private Long businessUnitId;

  private Long unitOfMeasureId;

  private String stockType;

  private BigDecimal availableQuantity;

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getPlantId() {
    return plantId;
  }

  public void setPlantId(Long plantId) {
    this.plantId = plantId;
  }

  public Long getStorehouseId() {
    return storehouseId;
  }

  public void setStorehouseId(Long storehouseId) {
    this.storehouseId = storehouseId;
  }

  public Long getBusinessUnitId() {
    return businessUnitId;
  }

  public void setBusinessUnitId(Long purchaseUnitId) {
    this.businessUnitId = purchaseUnitId;
  }

  public Long getUnitOfMeasureId() {
    return unitOfMeasureId;
  }

  public void setUnitOfMeasureId(Long unitOfMeasureId) {
    this.unitOfMeasureId = unitOfMeasureId;
  }

  public String getStockType() {
    return stockType;
  }

  public void setStockType(String stockType) {
    this.stockType = stockType;
  }

  public BigDecimal getAvailableQuantity() {
    return availableQuantity;
  }

  public void setAvailableQuantity(BigDecimal availableQuantity) {
    this.availableQuantity = availableQuantity;
  }

  public String getSysName() {
    return ICObStockAvailabilitiyGeneralModel.SYS_NAME;
  }
}
