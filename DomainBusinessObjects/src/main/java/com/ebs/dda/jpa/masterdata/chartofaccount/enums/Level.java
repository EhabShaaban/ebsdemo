package com.ebs.dda.jpa.masterdata.chartofaccount.enums;

public enum Level {
    L1 {
        @Override
        public String toString() {
            return "1";
        }
    },
    L2 {
        @Override
        public String toString() {
            return "2";
        }
    },
    L3 {
        @Override
        public String toString() {
            return "3";
        }
    }
}
