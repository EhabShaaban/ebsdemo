package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class DObJournalEntryCreateCollectionValueObject implements IValueObject {

  private String collectionCode;

  public String getCollectionCode() {
    return collectionCode;
  }

  public void setCollectionCode(String collectionCode) {
    this.collectionCode = collectionCode;
  }
}
