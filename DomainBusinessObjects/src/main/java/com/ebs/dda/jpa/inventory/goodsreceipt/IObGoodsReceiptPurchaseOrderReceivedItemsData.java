package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;

@Entity
@EntityInterface(IIObGoodsReceiptReceivedItemsData.class)
@DiscriminatorColumn(name = "objectTypeCode")
public class IObGoodsReceiptPurchaseOrderReceivedItemsData
        extends IObGoodsReceiptReceivedItemsData {
}
