package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCustomerAccountingInfo")
public class IObCustomerAccountingInfo extends InformationObject {

  private Long chartOfAccountId;

  @Override
  public String getSysName() {
    return "IObCustomerAccountingInfo";
  }

  public void setChartOfAccountId(Long chartOfAccountId) {
    this.chartOfAccountId = chartOfAccountId;
  }
}
