package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.accounting.IObInvoiceItemsGeneralModel;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "iobsalesinvoiceitemgeneralmodel")
public class IObSalesInvoiceItemsGeneralModel extends IObInvoiceItemsGeneralModel {

    private String itemCodeName;
    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString orderUnitName;
    private String orderUnitCodeName;
    private String orderUnitCodeSymbol;
    private BigDecimal returnedQuantity;

    public String getItemCodeName() {
        return itemCodeName;
    }

    public LocalizedString getOrderUnitName() {
        return orderUnitName;
    }

    public String getOrderUnitCodeName() {
        return orderUnitCodeName;
    }

    public String getOrderUnitCodeSymbol() {
        return orderUnitCodeSymbol;
    }

    public BigDecimal getReturnedQuantity() { return returnedQuantity; }

    @Override
    public String getSysName() {
        return IIObSalesInvoiceItems.SYS_NAME;
    }
}
