package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(CreditNoteTypeEnum.Values.CREDIT_NOTE_BASED_ON_SALES_RETURN)
@EntityInterface(IDObAccountingNote.class)
public class DObCreditNoteSalesReturn extends DObAccountingNote {

  public DObCreditNoteSalesReturn() {}

  @Override
  public String getSysName() {
    return IDObAccountingNote.SYS_NAME;
  }
}
