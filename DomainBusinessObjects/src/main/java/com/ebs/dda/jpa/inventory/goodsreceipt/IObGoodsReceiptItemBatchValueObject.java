package com.ebs.dda.jpa.inventory.goodsreceipt;

public class IObGoodsReceiptItemBatchValueObject extends IObGoodsReceiptItemDetailValueObject {

  private String batchCode;
  private String productionDate;
  private String expirationDate;

  public String getBatchCode() {
    return batchCode;
  }

  public String getProductionDate() {
    return productionDate;
  }

  public String getExpirationDate() {
    return expirationDate;
  }
}
