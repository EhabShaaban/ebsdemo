package com.ebs.dda.jpa.accounting.vendorinvoice;

public class IObVendorInvoiceItemsDeletionValueObject {

  private String invoiceCode;
  private String itemCode;
  private Long itemId;

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getInvoiceCode() {
    return invoiceCode;
  }

  public void setInvoiceCode(String invoiceCode) {
    this.invoiceCode = invoiceCode;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId= itemId;
  }
}
