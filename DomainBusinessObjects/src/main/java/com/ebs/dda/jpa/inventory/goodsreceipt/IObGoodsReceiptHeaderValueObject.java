package com.ebs.dda.jpa.inventory.goodsreceipt;

public class IObGoodsReceiptHeaderValueObject {

  private String goodsReceiptCode;

  private String storehouseCode;

  private String storekeeperCode;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public void setStorehouseCode(String storehouseCode) {
    this.storehouseCode = storehouseCode;
  }

  public String getStorekeeperCode() {
    return storekeeperCode;
  }

  public void setStorekeeperCode(String storekeeperCode) {
    this.storekeeperCode = storekeeperCode;
  }
}
