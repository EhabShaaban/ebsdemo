package com.ebs.dda.jpa.accounting.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObCollectionNotesReceivableDetails")
@DiscriminatorValue("2")
public class IObCollectionNotesReceivableDetails extends IObCollectionDetails
    implements Serializable {
  private Long notesReceivableId;

  public Long getNotesReceivableId() {
    return notesReceivableId;
  }

  public void setNotesReceivableId(Long notesReceivableId) {
    this.notesReceivableId = notesReceivableId;
  }
}
