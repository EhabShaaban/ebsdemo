package com.ebs.dda.jpa.masterdata.exchangerate;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CBEExchangeRate implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(updatable = false)
  private LocalDate creationDate;

  @Column(updatable = false)
  private LocalDate ratesForDate;

  private String currencyName;

  private String currencyISO;

  private Double buy;

  private Double sell;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public LocalDate getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(LocalDate creationDate) {
    this.creationDate = creationDate;
  }

  public LocalDate getRatesForDate() { return ratesForDate; }

  public void setRatesForDate(LocalDate ratesForDate) { this.ratesForDate = ratesForDate; }

  public String getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public void setCurrencyISO(String currencyISO) {
    this.currencyISO = currencyISO;
  }

  public Double getBuy() {
    return buy;
  }

  public void setBuy(Double buy) {
    this.buy = buy;
  }

  public Double getSell() {
    return sell;
  }

  public void setSell(Double sell) {
    this.sell = sell;
  }
}
