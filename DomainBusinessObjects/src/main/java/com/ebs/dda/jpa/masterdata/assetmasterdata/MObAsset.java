package com.ebs.dda.jpa.masterdata.assetmasterdata;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.MasterDataObject;

import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "mobasset")
@EntityInterface(IMObAsset.class)
public class MObAsset extends MasterDataObject {

  private String oldNumber;
  private Long assetTypeId;

  public String getOldNumber() {
    return oldNumber;
  }

  public void setOldNumber(String oldNumber) {
    this.oldNumber = oldNumber;
  }

  public Long getAssetTypeId() {
    return assetTypeId;
  }

  public void setAssetTypeId(Long assetTypeId) {
    this.assetTypeId = assetTypeId;
  }

  @Override
  public String getSysName() {
    return IMObAsset.SYS_NAME;
  }
}
