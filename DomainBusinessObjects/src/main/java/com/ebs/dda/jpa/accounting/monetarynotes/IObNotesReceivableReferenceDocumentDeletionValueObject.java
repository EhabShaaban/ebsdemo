package com.ebs.dda.jpa.accounting.monetarynotes;

public class IObNotesReceivableReferenceDocumentDeletionValueObject {

  private String notesReceivableCode;
  private String referenceDocumentType;
  private Long referenceDocumentId;

  public String getNotesReceivableCode() {
    return notesReceivableCode;
  }

  public void setNotesReceivableCode(String notesReceivableCode) {
    this.notesReceivableCode = notesReceivableCode;
  }

  public Long getReferenceDocumentId() {
    return referenceDocumentId;
  }

  public void setReferenceDocumentId(Long referenceDocumentId) {
    this.referenceDocumentId = referenceDocumentId;
  }

  public String getReferenceDocumentType() {
    return referenceDocumentType;
  }

  public void setReferenceDocumentType(String referenceDocumentType) {
    this.referenceDocumentType = referenceDocumentType;
  }
}
