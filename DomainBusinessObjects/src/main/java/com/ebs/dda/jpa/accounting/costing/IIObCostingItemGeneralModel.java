package com.ebs.dda.jpa.accounting.costing;

public interface IIObCostingItemGeneralModel {
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String USER_CODE = "userCode";
  String UOM_SYMBOL = "uomSymbol";
  String UOM_CODE = "uomCode";
  String QUANTITY = "quantity";
  String STOCK_TYPE = "stockType";
  String TO_BE_CONSIDERED_IN_COSTING_PER = "toBeConsideredInCostingPer";
  String ESTIMATE_ITEM_TOTAL_COST = "estimateItemTotalCost";
  String ACTUAL_ITEM_TOTAL_COST = "actualItemTotalCost";
  String ESTIMATE_UNIT_PRICE = "estimateUnitPrice";
  String ACTUAL_UNIT_PRICE = "actualUnitPrice";
  String ESTIMATE_UNIT_LANDED_COST = "estimateUnitLandedCost";
  String ACTUAL_UNIT_LANDED_COST = "actualUnitLandedCost";
}
