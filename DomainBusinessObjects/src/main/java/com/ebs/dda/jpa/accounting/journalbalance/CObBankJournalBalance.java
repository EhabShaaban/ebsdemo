package com.ebs.dda.jpa.accounting.journalbalance;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObBankJournalBalance")
@DiscriminatorValue(value = "BANK")
public class CObBankJournalBalance extends CObJournalBalance {
  private Long accountId;

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }
}
