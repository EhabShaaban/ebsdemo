package com.ebs.dda.jpa.inventory.inventorydocument;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObInventoryActivationDetails")
public class IObInventoryActivationDetails extends DocumentHeader implements Serializable {

    private static final long serialVersionUID = 1L;

    @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
    private DateTime activationDate;

    private String activatedBy;

    public DateTime getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(DateTime activationDate) {
        this.activationDate = activationDate;
    }

    public String getActivatedBy() {
        return activatedBy;
    }

    public void setActivatedBy(String activatedBy) {
        this.activatedBy = activatedBy;
    }
}
