package com.ebs.dda.jpa.accounting.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObCollectionDebitNoteDetails")
@DiscriminatorValue("4")
public class IObCollectionDebitNoteDetails extends IObCollectionDetails implements Serializable {
  private Long accountingNoteId;

  public Long getDebitNoteId() {
    return accountingNoteId;
  }

  public void setDebitNoteId(Long debitNoteId) {
    this.accountingNoteId = debitNoteId;
  }
}
