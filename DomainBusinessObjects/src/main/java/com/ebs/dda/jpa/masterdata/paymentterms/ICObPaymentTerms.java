package com.ebs.dda.jpa.masterdata.paymentterms;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface ICObPaymentTerms {

  String SYS_NAME = "PaymentTerms";
  String NAME = "name";
  String CODE = "userCode";

  String PAYMENT_INSTALLMENTS_ATTR_NAME = "paymentInstallments";
  CompositeAttribute<IObPaymentTermsInstallments> paymentInstallmentsAttr =
      new CompositeAttribute<>(
          PAYMENT_INSTALLMENTS_ATTR_NAME,
          IObPaymentTermsInstallments.class,
          true);
}
