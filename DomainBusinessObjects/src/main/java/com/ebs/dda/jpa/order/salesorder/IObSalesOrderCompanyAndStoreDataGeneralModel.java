package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IIObSalesOrderCompanyAndStoreDataGeneralModel.class)
@Table(name = "IObSalesOrderCompanyAndStoreDataGeneralModel")
public class IObSalesOrderCompanyAndStoreDataGeneralModel extends InformationObject {

  private String salesOrderCode;
  private String companyCode;
  private String storeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storeName;

  public String getSalesOrderCode() {
    return salesOrderCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getStoreCode() {
    return storeCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public LocalizedString getStoreName() {
    return storeName;
  }

  @Override
  public String getSysName() {
    return IDObSalesOrder.SYS_NAME;
  }
}
