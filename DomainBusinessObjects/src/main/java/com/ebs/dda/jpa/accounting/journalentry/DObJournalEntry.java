package com.ebs.dda.jpa.accounting.journalentry;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import org.joda.time.DateTime;

@Entity
@Table(name = "DObJournalEntry")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class DObJournalEntry extends DocumentObject<DefaultUserCode>
    implements Serializable {

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private Long companyId;
  private Long purchaseUnitId;
  private Long fiscalPeriodId;

  public DateTime getDueDate() {
    return dueDate;
  }

  public void setDueDate(DateTime dueDate) {
    this.dueDate = dueDate;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public void setPurchaseUnitId(Long purchaseUnitId) {
    this.purchaseUnitId = purchaseUnitId;
  }

  public Long getFiscalPeriodId() {
    return fiscalPeriodId;
  }

  public void setFiscalPeriodId(Long fiscalPeriodId) {
    this.fiscalPeriodId = fiscalPeriodId;
  }

  @Override
  public String getSysName() {
    return IDObJournalEntry.SYS_NAME;
  }
}
