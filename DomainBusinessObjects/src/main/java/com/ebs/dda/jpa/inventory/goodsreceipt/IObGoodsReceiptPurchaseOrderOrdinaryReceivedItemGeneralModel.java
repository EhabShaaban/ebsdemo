package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.annotations.Exclude;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel")
@EntityInterface(IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.class)
public class IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel {

  @Id private Long id;
  private Long ordinaryItemId;
  @Exclude private Long UnitOfEntryId;
  private BigDecimal receivedQtyUoE;
  private BigDecimal receivedQtyBase;
  private BigDecimal receivedqtyWithDefectsbase;
  private String stockType;
  private String notes;
  private String goodsReceiptCode;
  private BigDecimal estimatedCost;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString baseUnitSymbol;

  private String itemCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString unitOfEntry;

  private String unitOfEntryCode;

  private BigDecimal baseUnitFactor;

  private String itemCodeAtVendor;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public LocalizedString getBaseUnitName() {
    return baseUnitSymbol;
  }

  public String getItemCode() {
    return itemCode;
  }

  public Long getId() {
    return id;
  }

  public BigDecimal getReceivedQtyUoE() {
    return receivedQtyUoE;
  }

  public BigDecimal getReceivedQtyBase() {
    return receivedQtyBase;
  }

  public BigDecimal getreceivedqtyWithDefectsbase() {
    return receivedqtyWithDefectsbase;
  }

  public String getStockType() {
    return stockType;
  }

  public String getNotes() {
    return notes;
  }

  public LocalizedString getUnitOfEntry() {
    return unitOfEntry;
  }

  public String getUnitOfEntryCode() {
    return unitOfEntryCode;
  }

  public Long getOrdinaryItemId() {
    return ordinaryItemId;
  }

  public BigDecimal getEstimatedCost() {
    return estimatedCost;
  }

  public Long getUnitOfEntryId() {
    return UnitOfEntryId;
  }
}
