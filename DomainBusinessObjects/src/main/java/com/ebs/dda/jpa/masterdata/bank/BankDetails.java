package com.ebs.dda.jpa.masterdata.bank;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BankDetails extends InformationObject {

  private static final long serialVersionUID = 1L;

  @Column(name = "bankId")
  private Long bankId;

  private String accountNo;

  private String swiftCode;

  private String iban;

  private String beneficiary;

  public String getAccountNo() {
    return accountNo;
  }

  public void setAccountNo(String accountNo) {
    this.accountNo = accountNo;
  }

  public String getSwiftCode() {
    return swiftCode;
  }

  public void setSwiftCode(String swiftCode) {
    this.swiftCode = swiftCode;
  }

  public String getIban() {
    return iban;
  }

  public void setIban(String iban) {
    this.iban = iban;
  }

  public String getBeneficiary() {
    return beneficiary;
  }

  public void setBeneficiary(String beneficiary) {
    this.beneficiary = beneficiary;
  }

  public Long getBankId() {
    return bankId;
  }

  public void setBankId(Long bankId) {
    this.bankId = bankId;
  }
}
