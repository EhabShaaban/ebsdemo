package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
@EntityInterface(ICObCompanyGroup.class)
public class CObCompanyGroup extends CObEnterprise {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ICObCompanyGroup.SYS_NAME;
  }
}
