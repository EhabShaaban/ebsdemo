package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@EntityInterface(IIObGoodsReceiptItemQuantities.class)
@Table(name = "IObGoodsReceiptSalesReturnItemQuantities")
public class IObGoodsReceiptSalesReturnItemQuantities extends DocumentLine {

    private BigDecimal receivedQtyUoE;

    private Long unitOfEntryId;

    private String notes;

    @Embedded
    @AttributeOverride(name = "value", column = @Column(name = "stocktype"))
    private StockTypeEnum stockType;

    public BigDecimal getReceivedQtyUoE() {
        return receivedQtyUoE;
    }

    public void setReceivedQtyUoE(BigDecimal receivedQtyUoE) {
    this.receivedQtyUoE = receivedQtyUoE;
  }

  public Long getUnitOfEntryId() {
    return unitOfEntryId;
  }

  public void setUnitOfEntryId(Long unitOfEntryId) {
      this.unitOfEntryId = unitOfEntryId;
  }

    public StockTypeEnum getStockType() {
        return stockType;
    }

    public void setStockType(StockTypeEnum stockType) {
        this.stockType = stockType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
