package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dda.converters.JodaTimeCSVConverter;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "iobinitialstockuploaditem")
public class IObInitialStockUploadItem extends DocumentLine {

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  @CsvCustomBindByName(converter = JodaTimeCSVConverter.class, column = "add_date")
  private DateTime originalAddingDate;

  private Long itemId;
  private Long unitOfMeasureId;

  @CsvBindByName(column = "StockType")
  private String stockType;

  @CsvBindByName(column = "qnty")
  private BigDecimal quantity;

  @CsvBindByName(column = "EstimateCost")
  private BigDecimal estimateCost = BigDecimal.ZERO;

  @CsvBindByName(column = "ActualCost")
  private BigDecimal actualCost = BigDecimal.ZERO;

  @CsvBindByName(column = "RestQnty")
  private BigDecimal remainingQuantity;

  @Transient
  @CsvBindByName(column = "itemid")
  private String oldItemNumber;

  public void setOriginalAddingDate(DateTime originalAddingDate) {
    this.originalAddingDate = originalAddingDate;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public void setUnitOfMeasureId(Long unitOfMeasureId) {
    this.unitOfMeasureId = unitOfMeasureId;
  }

  public void setStockType(String stockType) {
    this.stockType = stockType;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public void setEstimateCost(BigDecimal estimateCost) {
    this.estimateCost = estimateCost;
  }

  public void setActualCost(BigDecimal actualCost) {
    this.actualCost = actualCost;
  }

  public void setRemainingQuantity(BigDecimal remainingQuantity) {
    this.remainingQuantity = remainingQuantity;
  }

  public void setOldItemNumber(String oldItemNumber) {
    this.oldItemNumber = oldItemNumber;
  }

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  @Override
  public String getSysName() {
    return IDObInitialStockUpload.SYS_NAME;
  }
}
