package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dda.jpa.IValueObject;

public class DObNotesReceivableActivateValueObject implements IValueObject {

  private String notesReceivableCode;
  private String journalEntryDate;

  public String getNotesReceivableCode() {
    return notesReceivableCode;
  }

  public void setNotesReceivableCode(String notesReceivableCode) {
    this.notesReceivableCode = notesReceivableCode;
  }

  public String getJournalEntryDate() {
    return journalEntryDate;
  }

  public void setJournalEntryDate(String journalEntryDate) {
    this.journalEntryDate = journalEntryDate;
  }

  @Override
  public void userCode(String userCode) {
    this.notesReceivableCode = userCode;
  }
}
