package com.ebs.dda.jpa.masterdata.lookups;

public interface ILObTaxAdministrative {
  String USER_CODE = "userCode";
  String NAME = "name";
}
