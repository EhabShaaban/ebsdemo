package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObGoodsReceiptItemQuantityGeneralModel")
public class IObGoodsReceiptItemQuantityGeneralModel extends IObItemQuantityGeneralModel {

  private static final long serialVersionUID = 1L;
  private String goodsReceiptType;
  private String stockType;

  public String getStockType() {
    return stockType;
  }

  public String getGoodsReceiptType() {
    return goodsReceiptType;
  }

  @Override
  public String getSysName() {
    return "IObGoodsReceiptItemQuantityGeneralModel";
  }
}
