package com.ebs.dda.jpa.accounting.salesinvoice;

public class IObSalesInvoiceTaxesDeletionValueObject {

  private String salesInvoiceCode;
  private String taxCode;

  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public void setSalesInvoiceCode(String salesInvoiceCode) {
    this.salesInvoiceCode = salesInvoiceCode;
  }

  public String getTaxCode() {
    return taxCode;
  }

  public void setTaxCode(String taxCode) {
    this.taxCode = taxCode;
  }
}
