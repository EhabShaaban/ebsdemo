package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCompanyLogoDetails")
@EntityInterface(IIObCompanyLogoDetails.class)
public class IObCompanyLogoDetails extends InformationObject {

  private byte[] logo;

  public byte[] getLogo() {
    return logo;
  }

  public void setLogo(byte[] logo) {
    this.logo = logo;
  }

  @Override
  public String getSysName() {
    return "IObCompanyLogoDetails";
  }
}
