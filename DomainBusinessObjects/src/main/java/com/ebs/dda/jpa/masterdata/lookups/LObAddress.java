
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "lobaddress")
@DiscriminatorColumn(name = "objectTypeCode")
public class LObAddress extends LookupObject<DefaultUserCode> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String address;
    private String district;
    private String postalCode;
    private Long country;
    private Long city;
    private Long government;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCountry() {
        return country;
    }

    public void setCountry(Long country) {
        this.country = country;
    }

    public Long getCity() {
        return city;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public Long getGovernment() {
        return government;
    }

    public void setGovernment(Long government) {
        this.government = government;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String getSysName() {
        return null;
    }
}
