package com.ebs.dda.jpa.masterdata.ivr;

public class ItemVendorRecordGeneralDataValueObject {

	private Double price;
	private String currencyCode;
	private String itemCodeAtVendor;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getItemCodeAtVendor() {
		return itemCodeAtVendor;
	}

	public void setItemCodeAtVendor(String itemCodeAtVendor) {
		this.itemCodeAtVendor = itemCodeAtVendor;
	}
}
