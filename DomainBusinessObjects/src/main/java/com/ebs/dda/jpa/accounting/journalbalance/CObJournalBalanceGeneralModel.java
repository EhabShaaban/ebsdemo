package com.ebs.dda.jpa.accounting.journalbalance;

import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

@Entity
@Table(name = "CObJournalBalanceGeneralModel")
public class CObJournalBalanceGeneralModel extends CodedBusinessObject<DefaultUserCode> {

  private String objectTypeCode;
  private String businessUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessUnitName;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String currencyCode;
  private String currencyIso;

  private String bankCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString bankName;

  private String bankAccountNumber;

  private String treasuryCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString treasuryName;

  private BigDecimal balance;

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public LocalizedString getBusinessUnitName() {
    return businessUnitName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public String getBankCode() {
    return bankCode;
  }

  public LocalizedString getBankName() {
    return bankName;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public String getTreasuryCode() {
    return treasuryCode;
  }

  public LocalizedString getTreasuryName() {
    return treasuryName;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
