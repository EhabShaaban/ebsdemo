package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.processing.BasicAttribute;
import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IDObSalesOrderGeneralModel {

  String PURCHASE_UNIT_NAME_ATTR_NAME = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR_NAME);

  String USER_CODE = "userCode";
  String SALES_ORDER_TYPE_CODE = "salesOrderTypeCode";
  String SALES_ORDER_TYPE_NAME = "salesOrderTypeName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String SALES_RESPONSIBLE_CODE = "salesResponsibleCode";

  String SALES_RESPONSIBLE_NAME = "salesResponsibleName";
  String SALES_RESPONSIBLE_NAME_EN = "salesResponsibleNameEn";
  BasicAttribute salesResponsibleNameAttr = new BasicAttribute(SALES_RESPONSIBLE_NAME_EN);

  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String CUSTOMER_CODE = "customerCode";
  String CUSTOMER_NAME = "customerName";
  String EXPECTED_DELIVERY_DATE = "expectedDeliveryDate";
  String NOTES = "notes";
  String CUSTOMER = "customer";
  String CODE = "Code";
  String NAME = "Name";
  String CURRENT_STATES = "currentStates";
  String CREATION_INFO_NAME = "creationInfo";
  String CREATION_DATE = "creationDate";
  String REMAINING = "remaining";
}
