package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObSalesInvoiceJournalEntryPreparationGeneralModel")
public class DObSalesInvoiceJournalEntryPreparationGeneralModel {
	@Id
	Long id;
	String code;
	@Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
	DateTime dueDate;
	Long purchaseUnitId;
	Long companyId;
	Long customerCurrencyId;
	Long companyCurrencyId;
	BigDecimal currencyPrice;
	Long exchangeRateId;
	BigDecimal amountBeforeTaxes;
	BigDecimal amountAfterTaxes;
	Long customerId;
	Long customerAccountId;
	String customerLeadger;
	Long salesAccountId;
	Long fiscalYearId;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public DateTime getDueDate() {
		return dueDate;
	}

	public Long getPurchaseUnitId() {
		return purchaseUnitId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public Long getCustomerCurrencyId() {
		return customerCurrencyId;
	}

	public Long getCompanyCurrencyId() {
		return companyCurrencyId;
	}

	public BigDecimal getCurrencyPrice() {
		return currencyPrice;
	}

	public Long getExchangeRateId() {
		return exchangeRateId;
	}

	public BigDecimal getAmountBeforeTaxes() {
		return amountBeforeTaxes;
	}

	public BigDecimal getAmountAfterTaxes() {
		return amountAfterTaxes;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public Long getCustomerAccountId() {
		return customerAccountId;
	}

	public String getCustomerLeadger() {
		return customerLeadger;
	}

	public Long getSalesAccountId() {
		return salesAccountId;
	}

	public Long getFiscalYearId() {	return fiscalYearId; }
}
