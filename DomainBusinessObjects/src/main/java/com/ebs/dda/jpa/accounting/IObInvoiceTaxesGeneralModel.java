package com.ebs.dda.jpa.accounting;

import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import java.math.BigDecimal;

@MappedSuperclass
public class IObInvoiceTaxesGeneralModel extends InformationObject {

  private String invoiceCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString taxName;

  private BigDecimal taxPercentage;

  private BigDecimal taxAmount;

  private String taxCode;

  public String getInvoiceCode() {
    return invoiceCode;
  }

  public LocalizedString getTaxName() {
    return taxName;
  }

  public BigDecimal getTaxPercentage() {
    return taxPercentage;
  }

  public BigDecimal getTaxAmount() {
    return taxAmount;
  }

  public String getTaxCode() {
    return taxCode;
  }

  @Override
  public String getSysName() {
    return "InvoiceTaxes";
  }
}
