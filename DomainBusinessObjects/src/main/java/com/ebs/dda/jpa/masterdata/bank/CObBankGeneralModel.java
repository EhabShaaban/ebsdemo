package com.ebs.dda.jpa.masterdata.bank;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObBankGeneralModel")
public class CObBankGeneralModel extends BusinessObject {

  private static final long serialVersionUID = 1L;

  @Column(name = "code")
  private String code;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "bankName")
  private LocalizedString bankName;

  @Column(name = "countryName")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString countryName;

  @Column(name = "cityName")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString cityName;

  @Column(name = "addressLine")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString address;

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public LocalizedString getBankName() {
    return bankName;
  }

  public void setBankName(LocalizedString bankName) {
    this.bankName = bankName;
  }

  public LocalizedString getCountryName() {
    return countryName;
  }

  public void setCountryName(LocalizedString countryName) {
    this.countryName = countryName;
  }

  public LocalizedString getCityName() {
    return cityName;
  }

  public void setCityName(LocalizedString cityName) {
    this.cityName = cityName;
  }

  public LocalizedString getAddress() {
    return address;
  }

  public void setAddress(LocalizedString address) {
    this.address = address;
  }

  @Override
  public String getSysName() {
    return CObBank.SYS_NAME;
  }
}
