package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.annotations.Exclude;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObItemAccountingInfoGeneralModel")
public class IObItemAccountingInfoGeneralModel extends InformationObject {
  @Exclude
  private Long accountId;
  private String accountCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString accountName;

  private String itemCode;
  private Boolean costFactor;

  public Boolean getCostFactor() {
    return costFactor;
  }

  public void setCostFactor(Boolean costFactor) {
    this.costFactor = costFactor;
  }

  @Override
  public String getSysName() {
    return "ItemAccountingInfo";
  }

  public String getAccountCode() {
    return accountCode;
  }

  public void setAccountCode(String accountCode) {
    this.accountCode = accountCode;
  }

  public LocalizedString getAccountName() {
    return accountName;
  }

  public void setAccountName(LocalizedString accountName) {
    this.accountName = accountName;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public Long getAccountId() {
    return accountId;
  }
}
