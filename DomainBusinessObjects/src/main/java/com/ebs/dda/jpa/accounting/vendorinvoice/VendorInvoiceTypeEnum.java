package com.ebs.dda.jpa.accounting.vendorinvoice;

public enum VendorInvoiceTypeEnum {
    LOCAL_GOODS_INVOICE(Values.LOCAL_GOODS_INVOICE),
    IMPORT_GOODS_INVOICE(Values.IMPORT_GOODS_INVOICE),
    SHIPMENT_INVOICE(Values.SHIPMENT_INVOICE),
    CUSTOM_TRUSTEE_INVOICE(Values.CUSTOM_TRUSTEE_INVOICE),
    INSURANCE_INVOICE(Values.INSURANCE_INVOICE);

    VendorInvoiceTypeEnum(String vendorInvoiceType) {
    }

    public static class Values {
        public static final String LOCAL_GOODS_INVOICE = "LOCAL_GOODS_INVOICE";
        public static final String IMPORT_GOODS_INVOICE = "IMPORT_GOODS_INVOICE";
        public static final String SHIPMENT_INVOICE = "SHIPMENT_INVOICE";
        public static final String CUSTOM_TRUSTEE_INVOICE = "CUSTOM_TRUSTEE_INVOICE";
        public static final String INSURANCE_INVOICE = "INSURANCE_INVOICE";
    }

    public static class VendorInvoiceType {
        public static Boolean isGoodsInvoice(String invoiceType) {
            return invoiceType.equals(Values.LOCAL_GOODS_INVOICE) || invoiceType.equals(Values.IMPORT_GOODS_INVOICE);
        }

        public static Boolean isPurchaseServiceLocalInvoice(String invoiceType) {
            return invoiceType.equals(Values.CUSTOM_TRUSTEE_INVOICE) || invoiceType.equals(Values.SHIPMENT_INVOICE) || invoiceType.equals(Values.INSURANCE_INVOICE);
        }
    }
}
