package com.ebs.dda.jpa.accounting.salesinvoice;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

@Entity
@Table(name = "DObSalesInvoiceGeneralModel")
@EntityInterface(IDObSalesInvoiceGeneralModel.class)
public class DObSalesInvoiceGeneralModel extends StatefullBusinessObject<DefaultUserCode>
    implements Serializable {

  private String salesOrder;

  private String invoiceTypeCode;

  private String creationInfoName;

  private String purchaseUnitNameEn;

  private String companyCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String customerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString invoiceType;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customer;

  private String documentOwner;

  private String documentOwnerUserName;

  private Long documentOwnerId;

  private BigDecimal remaining;

  public String getSalesOrder() {
    return salesOrder;
  }

  public String getInvoiceTypeCode() {
    return invoiceTypeCode;
  }

  public LocalizedString getInvoiceType() {
    return invoiceType;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public LocalizedString getCustomer() {
    return customer;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  public String getDocumentOwnerUserName() {
    return documentOwnerUserName;
  }

  public String getCreationInfoName() {
    return creationInfoName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  @Override
  public String getSysName() {
    return IDObSalesInvoiceGeneralModel.SYS_NAME;
  }

}
