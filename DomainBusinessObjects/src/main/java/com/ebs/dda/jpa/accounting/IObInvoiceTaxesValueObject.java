package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class IObInvoiceTaxesValueObject implements IValueObject {

  private String invoiceCode;
  private DocumentObject invoice;

  public IObInvoiceTaxesValueObject(DocumentObject invoice) {
    setInvoice(invoice);
    setInvoiceCode(invoice.getUserCode());
  }
  public DocumentObject getInvoice() {
    return invoice;
  }

  public void setInvoice(DocumentObject invoice) {
    this.invoice = invoice;
  }

  public String getInvoiceCode() {
    return invoiceCode;
  }

  public void setInvoiceCode(String invoiceCode) {
    this.invoiceCode = invoiceCode;
  }
}
