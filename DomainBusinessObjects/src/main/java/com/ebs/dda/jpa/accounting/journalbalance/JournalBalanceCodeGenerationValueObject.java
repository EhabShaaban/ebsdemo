package com.ebs.dda.jpa.accounting.journalbalance;

public class JournalBalanceCodeGenerationValueObject {
  String businessUnitCode;
  String companyCode;
  String currencyCode;
  String treasuryCode;
  String bankCode;
  String bankAccountNumber;
  String objectTypeCode;

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }



  public String getTreasuryCode() {
    return treasuryCode;
  }

  public void setTreasuryCode(String treasuryCode) {
    this.treasuryCode = treasuryCode;
  }

  public String getBankCode() {
    return bankCode;
  }

  public void setBankCode(String bankCode) {
    this.bankCode = bankCode;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public void setBankAccountNumber(String bankAccountNumber) {
    this.bankAccountNumber = bankAccountNumber;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public void setObjectTypeCode(String objectTypeCode) {
    this.objectTypeCode = objectTypeCode;
  }
}
