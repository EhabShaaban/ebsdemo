package com.ebs.dda.jpa.masterdata.item;

public interface IMObItemCreateValueObject {
  String ITEM_GROUP_CODE = "itemGroupCode";
  String BASE_UNIT_CODE = "baseUnitCode";
  String BATCH_MANAGED = "batchManaged";
  String PURCHASE_UNITS = "purchaseUnits";
  String ITEM_TYPE = "itemType";
  String ITEM_NAME = "itemName";
  String ITEM_BASE_UNIT_Code = "baseUnitCode";
  String ITEM_OLD_NUMBER = "itemOldNumber";
  String MARKET_NAME = "marketName";
  String ITEM_BATCH_MANAGED = "batchManaged";

}
