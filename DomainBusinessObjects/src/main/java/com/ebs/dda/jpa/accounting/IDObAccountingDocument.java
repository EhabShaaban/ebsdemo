package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObAccountingDocument {

	String ACCOUNTING_DETAILS_ATTR_NAME = "accountingDetails";
	CompositeAttribute<IObAccountingDocumentAccountingDetails> accountingDetailsAttr = new CompositeAttribute<>(
					ACCOUNTING_DETAILS_ATTR_NAME, IObAccountingDocumentAccountingDetails.class,
					true);

	CompositeAttribute<IObAccountingDocumentOrderAccountingDetails> accountingOrderDetailsAttr = new CompositeAttribute<>(
					ACCOUNTING_DETAILS_ATTR_NAME, IObAccountingDocumentOrderAccountingDetails.class,
					true);

	CompositeAttribute<IObAccountingDocumentVendorAccountingDetails> accountingVendorDetailsAttr = new CompositeAttribute<>(
					ACCOUNTING_DETAILS_ATTR_NAME,
					IObAccountingDocumentVendorAccountingDetails.class, true);

	CompositeAttribute<IObAccountingDocumentBankAccountingDetails> accountingBankDetailsAttr = new CompositeAttribute<>(
					ACCOUNTING_DETAILS_ATTR_NAME, IObAccountingDocumentBankAccountingDetails.class,
					true);

	CompositeAttribute<IObAccountingDocumentCustomerAccountingDetails> accountingCustomerDetailsAttr = new CompositeAttribute<>(
					ACCOUNTING_DETAILS_ATTR_NAME,
					IObAccountingDocumentCustomerAccountingDetails.class, true);

	CompositeAttribute<IObAccountingDocumentNotesReceivableAccountingDetails> accountingNotesReceivableDetailsAttr = new CompositeAttribute<>(
					ACCOUNTING_DETAILS_ATTR_NAME,
					IObAccountingDocumentNotesReceivableAccountingDetails.class, true);

	CompositeAttribute<IObAccountingDocumentTreasuryAccountingDetails> accountingCashDetailsAttr = new CompositeAttribute<>(
					ACCOUNTING_DETAILS_ATTR_NAME,
					IObAccountingDocumentTreasuryAccountingDetails.class, true);

}
