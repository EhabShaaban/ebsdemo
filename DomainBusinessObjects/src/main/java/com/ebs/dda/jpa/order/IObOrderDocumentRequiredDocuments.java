/** */
package com.ebs.dda.jpa.order;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderRequiredDocuments;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 2:55:54 PM
 */
@Entity
@Table(name = "IObOrderDocumentRequiredDocuments")
public class IObOrderDocumentRequiredDocuments extends DocumentLine implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "numberOfCopies")
  private Integer numberOfCopies;

  @Column(name = "attachmentTypeId")
  private Long attachmentTypeId;

  public Integer getNumberOfCopies() {
    return numberOfCopies;
  }

  public void setNumberOfCopies(Integer numberOfCopies) {
    this.numberOfCopies = numberOfCopies;
  }

  public Long getAttachmentTypeId() {
    return attachmentTypeId;
  }

  public void setAttachmentTypeId(Long attachmentTypeId) {
    this.attachmentTypeId = attachmentTypeId;
  }

  @Override
  public String getSysName() {
    return IIObOrderRequiredDocuments.SYS_NAME;
  }
}
