package com.ebs.dda.jpa.masterdata.country;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yara Ameen
 * @since Mar 27, 2016 8:52:03 AM
 */
@Entity
@Table(name = "cobsubdivisiongeneralmodel")
public class CObSubdivisionGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name", updatable = false)
  private LocalizedString name;

  @Column(updatable = false)
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString language;

  @Column(updatable = false)
  private String standardTimezone;

  @Column(updatable = false)
  private String dstTimezone;

  @Column(updatable = false)
  private Long countryId;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(updatable = false)
  private LocalizedString country;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "parent", updatable = false)
  private LocalizedString subdivisionParent;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(updatable = false)
  private LocalizedString category;

  public CObSubdivisionGeneralModel() {}

  public CObSubdivisionGeneralModel(CObSubdivision subbdivision) {
    this.setId(subbdivision.getId());
    this.setUserCode(subbdivision.getUserCode());
    this.setCurrentStates(subbdivision.getCurrentStates());
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public LocalizedString getLanguage() {
    return language;
  }

  public void setLanguage(LocalizedString language) {
    this.language = language;
  }

  public String getStandardTimezone() {
    return standardTimezone;
  }

  public void setStandardTimezone(String standardTimezone) {
    this.standardTimezone = standardTimezone;
  }

  public String getDstTimezone() {
    return dstTimezone;
  }

  public void setDstTimezone(String dstTimezone) {
    this.dstTimezone = dstTimezone;
  }

  public Long getCountryId() {
    return countryId;
  }

  public void setCountryId(Long countryId) {
    this.countryId = countryId;
  }

  public LocalizedString getCountry() {
    return country;
  }

  public void setCountry(LocalizedString country) {
    this.country = country;
  }

  public LocalizedString getSubdivisionParent() {
    return subdivisionParent;
  }

  public void setSubdivisionParent(LocalizedString parent) {
    this.subdivisionParent = parent;
  }

  public LocalizedString getCategory() {
    return category;
  }

  public void setCategory(LocalizedString category) {
    this.category = category;
  }

  @Override
  public String getSysName() {
    return ICObSubdivision.SYS_NAME;
  }
}
