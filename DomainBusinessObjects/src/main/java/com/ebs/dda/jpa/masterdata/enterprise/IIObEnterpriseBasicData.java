package com.ebs.dda.jpa.masterdata.enterprise;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;
import com.ebs.dac.dbo.processing.EmbeddedAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObEnterpriseBasicData extends IInformationObject {

  String SYS_NAME = "EnterpriseBasicData";
  byte SHORT_NAME_CODE = 10;
  String SHORT_NAME_ATTR_NAME = "shortName";
  EmbeddedAttribute<LocalizedString> shortNameAttr =
      new EmbeddedAttribute<>(SHORT_NAME_ATTR_NAME, LocalizedString.class);

  byte TAGLINE_CODE = 11;
  String TAGLINE_ATTR_NAME = "tagline";
  EmbeddedAttribute<LocalizedString> taglineAttr =
      new EmbeddedAttribute<>(TAGLINE_ATTR_NAME, LocalizedString.class);

}
