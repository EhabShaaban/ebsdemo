package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface ITObStoreTransactionGeneralModel {

  String SYS_NAME = "StoreTransactions";

  String ST_CODE = "userCode";
  String TRANSACTION_OPERATION = "transactionOperation";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String PLANT_CODE = "plantCode";
  String PLANT_NAME = "plantName";
  String STOREHOUSE_CODE = "storehouseCode";
    String STOREHOUSE_NAME = "storehouseName";
    String PURCHASEUNIT_CODE = "purchaseUnitCode";
    String PURCHASEUNIT_NAME = "purchaseUnitName";
    String STOCK_TYPE = "stockType";
    String ITEM = "item";
    String ITEM_CODE = "itemCode";
    String ITEM_NAME = "itemName";
    String UOM_CODE = "unitOfMeasureCode";
    String UOM_NAME = "unitOfMeasureName";
    String QUANTITY = "quantity";
    String ESTIMATE_COST = "estimateCost";
    String ACTUAL_COST = "actualCost";
    String REF_DOCUMENT = "refDocument";
    String REF_DOCUMENT_CODE = "refDocumentCode";
    String REF_DOCUMENT_TYPE = "refDocumentType";
    String REMAINING_QUANTITY = "remainingQuantity";
    String REF_TRANSACTION_CODE = "refTransactionCode";
    String ORIGINAL_ADDING_DATE = "originalAddingDate";


    String PURCHASEUNIT_NAME_EN = "purchaseUnitNameEn";
    BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASEUNIT_NAME);

}