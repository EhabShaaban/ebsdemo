package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObCollectionType")
@EntityInterface(ICObCollectionType.class)
public class CObCollectionType extends ConfigurationObject<DefaultUserCode>
    implements Serializable {
  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ICObCollectionType.SYS_NAME;
  }
}
