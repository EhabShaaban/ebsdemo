package com.ebs.dda.jpa.inventory.inventorydocument;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObInventoryDocument {

    String SYS_NAME = "Inventory";

    String COMPANY_ATTR_NAME = "company";
    CompositeAttribute<IObInventoryCompany> companyAttr =
            new CompositeAttribute<>(COMPANY_ATTR_NAME, IObInventoryCompany.class, false);

    String ACTIVATION_DETAILS_ATTR_NAME = "activationDetails";
    CompositeAttribute<IObInventoryActivationDetails> activationDetailsAttr =
            new CompositeAttribute<>(ACTIVATION_DETAILS_ATTR_NAME, IObInventoryActivationDetails.class, false);

}
