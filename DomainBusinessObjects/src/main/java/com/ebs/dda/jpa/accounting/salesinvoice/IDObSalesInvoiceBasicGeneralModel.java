package com.ebs.dda.jpa.accounting.salesinvoice;


public interface IDObSalesInvoiceBasicGeneralModel {

    String SYS_NAME = "SalesInvoice";
    String USER_CODE = "userCode";
    String CREATION_DATE = "creationDate";
    String CURRENT_STATE = "currentState";
    String SALES_ORDER = "salesOrder";
    String INVOICE_TYPE_CODE = "invoiceTypeCode";
    String INVOICE_TYPE_NAME = "invoiceTypeName";
    String PURCHASE_UNIT_CODE = "purchaseUnitCode";
    String PURCHASE_UNIT_NAME = "purchaseUnitName";
    String COMPANY_CODE = "companyCode";
    String COMPANY_NAME = "companyName";
    String CUSTOMER_CODE = "customerCode";
    String CUSTOMER_NAME = "customerName";


}
