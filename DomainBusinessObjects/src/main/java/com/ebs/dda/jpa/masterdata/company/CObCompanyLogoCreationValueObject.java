package com.ebs.dda.jpa.masterdata.company;

public class CObCompanyLogoCreationValueObject {

  private String companyCode;
  private String logoFilePath;

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getLogoFilePath() {
    return logoFilePath;
  }

  public void setLogoFilePath(String logoFilePath) {
    this.logoFilePath = logoFilePath;
  }
}
