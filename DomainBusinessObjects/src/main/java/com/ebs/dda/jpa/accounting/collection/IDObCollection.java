package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;

public interface IDObCollection extends IDObAccountingDocument {
  String SYS_NAME = "Collection";

  String COLLECTION_DETAILS_ATTR_NAME = "collectiondetails";
  String COLLECTION_COMPANY_DETAILS_ATTR_NAME = "collectioncompanydetails";
  CompositeAttribute<IObCollectionDetails> collectionDetails =
          new CompositeAttribute<>(COLLECTION_DETAILS_ATTR_NAME, IObCollectionDetails.class, false);

  CompositeAttribute<IObCollectionCompanyDetails> collectionCompanyDetails =
          new CompositeAttribute<>(
                  COLLECTION_COMPANY_DETAILS_ATTR_NAME, IObCollectionCompanyDetails.class, false);

  String ACTIVATION_DETAILS = "activationDetails";
  CompositeAttribute<IObCollectionActivationDetails> activationDetails =
          new CompositeAttribute(ACTIVATION_DETAILS, IObCollectionActivationDetails.class, false);
}
