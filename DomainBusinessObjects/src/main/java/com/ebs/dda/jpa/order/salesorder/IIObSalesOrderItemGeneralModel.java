package com.ebs.dda.jpa.order.salesorder;

public interface IIObSalesOrderItemGeneralModel {

  String SALES_ORDER_CODE = "salesOrderCode";
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String UNIT_OF_MEASURE_CODE = "unitOfMeasureCode";
  String UNIT_OF_MEASURE_NAME = "unitOfMeasureName";
  String UNIT_OF_MEASURE_SYMBOL = "unitOfMeasureSymbol";
  String QUANTITY = "quantity";
  String SALES_PRICE = "salesPrice";
  String TOTAL_AMOUNT = "totalAmount";
  String CREATION_DATE = "creationDate";
  String LAST_SALES_PRICE = "lastSalesPrice";
  String AVAILABLE_QUANTITY = "availableQuantity";

  String TOTAL_AMOUNT_BEFORE_TAXES = "totalAmountBeforeTaxes";
  String TOTAL_TAXES = "totalTaxes";
  String TOTAL_AMOUNT_AFTER_TAXES = "totalAmountAfterTaxes";
}
