package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@EntityInterface(IDObCollection.class)
@DiscriminatorValue(value = "C_DN")
public class DObCollectionDebitNote extends DObCollection implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String getSysName() {
        return IDObCollection.SYS_NAME;
    }
}
