package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@EntityInterface(IIObGoodsReceiptBatchedReceivedItemsData.class)
@DiscriminatorValue(IIObGoodsReceiptReceivedItemsData.BATCHED_ITEMS_OBJECT_TYPE_CODE)
public class IObGoodsReceiptBatchedPurchaseOrderReceivedItemsData
        extends IObGoodsReceiptPurchaseOrderReceivedItemsData {

    private static final long serialVersionUID = 1L;
}
