package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = "Collection")
public class IObCollectionCompanyDetails extends IObAccountingDocumentCompanyData
        implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long bankAccountId;

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }
}
