package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetails;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "IObCostingAccountingDetails")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Costing")
public class IObCostingAccountingDetails
    extends IObAccountingDocumentAccountingDetails implements Serializable {
  private Long glSubAccountId;

  @Override
  public Long getGlSubAccountId() {
    return glSubAccountId;
  }

  @Override
  public void setGlSubAccountId(Long glSubAccountId) {
    this.glSubAccountId = glSubAccountId;
  }

}
