/** */
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.geolocale.LObGeoLocale;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 24, 2016 2:51:06 PM
 */
@Entity
@DiscriminatorValue("2")
public class LObDecimalFormat extends LObGeoLocale implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_DECIMAL_FORMAT_SYS_NAME;
  }
}
