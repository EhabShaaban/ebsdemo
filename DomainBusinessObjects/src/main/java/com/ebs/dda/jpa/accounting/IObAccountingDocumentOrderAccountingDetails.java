package com.ebs.dda.jpa.accounting;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "IObAccountingDocumentOrderAccountingDetails")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "PO")
public class IObAccountingDocumentOrderAccountingDetails
				extends IObAccountingDocumentAccountingDetails implements Serializable {
	private Long glSubAccountId;

	@Override
	public Long getGlSubAccountId() {
		return glSubAccountId;
	}

	@Override
	public void setGlSubAccountId(Long glSubAccountId) {
		this.glSubAccountId = glSubAccountId;
	}


}
