package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObVendorAccountingInfo")
public class IObVendorAccountingInfo extends InformationObject {



    private Long chartOfAccountId;

    @Override
    public String getSysName() {
        return IIObVendorAccountingInfo.SYS_NAME;
    }
    public void setChartOfAccountId(Long chartOfAccountId) {
        this.chartOfAccountId = chartOfAccountId;
    }
}
