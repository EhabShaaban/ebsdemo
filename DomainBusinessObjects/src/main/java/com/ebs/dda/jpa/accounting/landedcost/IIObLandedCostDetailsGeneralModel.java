package com.ebs.dda.jpa.accounting.landedcost;


public interface IIObLandedCostDetailsGeneralModel {

  String SYS_NAME = "LandedCost";

  String LANDED_COST_CODE = "landedCostCode";
  String VENDOR_INVOICE_CODE = "vendorInvoiceCode";
  String INVOICE_AMOUNT = "invoiceAmount";
  String INVOICE_CURRENCY_ISO = "invoiceCurrencyIso";
  String COMPANY_CURRENCY_ISO = "companyCurrencyIso";
  String INVOICE_AMOUNT_CURRENCY = "invoiceAmountCurrency";
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String BUSINESS_UNIT_CODE = "businessUnitCode";
  String BUSINESS_UNIT_NAME = "businessUnitName";
  String BUSINESS_UNIT_CODE_NAME = "businessUnitCodeName";
  String CURRENCY_PRICE = "currencyPrice";
  String DAMAGE_STOCK = "damageStock";

}
