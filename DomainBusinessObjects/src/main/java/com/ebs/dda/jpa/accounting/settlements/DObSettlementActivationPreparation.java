package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 4, 2021
 */
@Entity
@Table(name = "DObSettlementActivationPreparation")
public class DObSettlementActivationPreparation {

  private static final long serialVersionUID = 1L;
  @Id private Long id;
  private String code;
  private Long fiscalPeriodId;
  private Long exchangeRateId;
  private BigDecimal currencyPrice;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Long getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public Long getFiscalPeriodId() {
    return fiscalPeriodId;
  }

  public Long getExchangeRateId() {
    return exchangeRateId;
  }

  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }
}
