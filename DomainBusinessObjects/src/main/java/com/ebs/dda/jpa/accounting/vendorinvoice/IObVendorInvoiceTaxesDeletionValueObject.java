package com.ebs.dda.jpa.accounting.vendorinvoice;

public class IObVendorInvoiceTaxesDeletionValueObject {

  private String vendorInvoiceCode;
  private String taxCode;

  public String getVendorInvoiceCode() { return vendorInvoiceCode; }

  public void setVendorInvoiceCode(String vendorInvoiceCode) { this.vendorInvoiceCode = vendorInvoiceCode; }

  public String getTaxCode() {
    return taxCode;
  }

  public void setTaxCode(String taxCode) {
    this.taxCode = taxCode;
  }
}
