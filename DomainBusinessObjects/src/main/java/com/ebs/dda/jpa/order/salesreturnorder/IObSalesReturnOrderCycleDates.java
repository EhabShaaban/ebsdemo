package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObSalesReturnOrderCycleDates")
public class IObSalesReturnOrderCycleDates extends DocumentHeader implements Serializable {

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime ShippingDate;

  public DateTime getShippingDate() {
    return ShippingDate;
  }

  public void setShippingDate(DateTime ShippingDate) {
    this.ShippingDate = ShippingDate;
  }
}
