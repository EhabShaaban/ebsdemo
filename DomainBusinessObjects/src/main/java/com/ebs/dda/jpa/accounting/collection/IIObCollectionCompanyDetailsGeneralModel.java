package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObCollectionCompanyDetailsGeneralModel {

  String SYS_NAME = "Collection";

  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASE_UNIT_NAME_ATTR = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
          new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR);
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
}
