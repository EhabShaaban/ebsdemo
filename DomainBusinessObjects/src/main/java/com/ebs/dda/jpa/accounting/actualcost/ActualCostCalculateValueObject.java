package com.ebs.dda.jpa.accounting.actualcost;

public class ActualCostCalculateValueObject {

  private String goodsReceiptCode;


  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }

}
