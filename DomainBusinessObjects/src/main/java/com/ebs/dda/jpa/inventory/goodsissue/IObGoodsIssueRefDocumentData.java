package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class IObGoodsIssueRefDocumentData extends DocumentHeader implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long customerId;
    private Long addressId;
    private Long contactPersonId;
    private Long salesRepresentativeId;
    private String comments;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public Long getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Long contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    public Long getSalesRepresentativeId() {
        return salesRepresentativeId;
    }

    public void setSalesRepresentativeId(Long salesRepresentativeId) {
        this.salesRepresentativeId = salesRepresentativeId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
