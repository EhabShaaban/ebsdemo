package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IDObNotesReceivablesGeneralModel {

  String TYPE = "Type";
  String SOURCE = "source";

  String CUSTOMER = "customer";
  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";
  String CURRENT_STATES = "currentStates";

  String NOTES_INFORMATION = "notesInformation";
  String DUE_DATE = "dueDate";
  String ISSUING_BANK = "notesBank";

  String REF_DOCUMENT_TYPE = "refDocumentType";
  String REF_DOCUMENT_NAME = "refDocumentName";
  String REF_DOCUMENT_CODE = "refDocumentCode";
  String REF_DOCUMENT = "refDocument";

  String BANK = "Bank";
  String NOTES = "notes";

  String PURCHASE_UNIT_NAME_ATTR = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR);


  String USER_CODE = "userCode";
  String CODE = "Code";
  String NAME = "Name";

  String OBJECT_TYPE_CODE = "objectTypeCode";
  String BUSINESS_PARTNER = "businessPartner";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String REMAINING = "remaining";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
  String DOCUMENT_OWNER_NAME = "documentOwner";
  String COMPANY_CODE_NAME = "companyCodeName";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String NOTE_NUMBER = "noteNumber";
  String AMOUNT = "amount";
  String CURRENCY_ISO = "currencyIso";
  String NOTE_BANK = "noteBank";
  String NOTE_FORM = "noteForm";
  String DEPOT = "depot";
  String DEPOT_CODE = "depotCode";
  String DEPOT_NAME = "depotName";

  String NOTES_RECEIVABLE = "NOTES_RECEIVABLE";
}
