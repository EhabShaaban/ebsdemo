package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringArrayConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "MObVendorGeneralModel")
@EntityInterface(value = IMObVendorGeneralModel.class)
public class MObVendorGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "vendorname")
  private LocalizedString vendorName;

  @Convert(converter = LocalizedStringArrayConverter.class)
  @Column(name = "purchaseunitsnames")
  private List<LocalizedString> purchasingUnitNames;

  @Column(name = "purchaseunitscodes")
  private String purchasingUnitCodes;

  @Column(name = "purchaseunits")
  private String purchaseUnits;

  @Column private String purchaseResponsibleCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "purchaseresponsiblename")
  private LocalizedString purchaseResponsibleName;

  @Column(name = "accountwithvendor")
  private String accountWithVendor;

  @Column private String oldVendorNumber;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "typename")
  private LocalizedString typeName;

  @Column(name = "typecode")
  private String typeCode;

  public String getSysName() {
    return IMObVendor.SYS_NAME;
  }

  public List<LocalizedString> getPurchasingUnitNames() {
    return purchasingUnitNames;
  }

  public String getPurchasingUnitCodes() {
    return purchasingUnitCodes;
  }

  public String getPurchaseResponsibleId() {
    return purchaseResponsibleCode;
  }

  public String getAccountWithVendor() {
    return accountWithVendor;
  }

  public String getOldVendorNumber() {
    return oldVendorNumber;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public LocalizedString getPurchaseResponsibleName() {
    return purchaseResponsibleName;
  }

  public LocalizedString getTypeName() {
    return typeName;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public String getPurchaseUnits() {
    return purchaseUnits;
  }
}
