package com.ebs.dda.jpa.accounting.landedcost;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObLandedCostGeneralModel {

  String SYS_NAME = "LandedCost";

  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";

  String CODE = "userCode";
  String CURRENT_STATES = "currentStates";

  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME_EN);

  String TYPE_CODE = "typeCode";
  String TYPE_NAME = "typeName";
  String TYPE_CODE_NAME = "typeCodeName";

  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String GOODS_INVOICE_CODE = "goodsInvoiceCode";
  String DOCUMENT_OWNER = "documentOwner";

}
