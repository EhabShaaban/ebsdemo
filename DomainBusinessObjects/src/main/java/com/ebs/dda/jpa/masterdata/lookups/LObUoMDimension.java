package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.measure.LObMeasure;
import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 24, 2016
 */
@Entity
@DiscriminatorValue("1")
public class LObUoMDimension extends LObMeasure implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_UOM_DIMENSION_SYS_NAME;
  }
}
