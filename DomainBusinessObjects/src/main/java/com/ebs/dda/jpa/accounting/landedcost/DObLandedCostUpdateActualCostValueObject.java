package com.ebs.dda.jpa.accounting.landedcost;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 22, 2021
 */
public class DObLandedCostUpdateActualCostValueObject {

  private String purchaseOrderCode;
  private Map<String, BigDecimal> itemsCodeAmount;
  private Long refDocumentId;

  public DObLandedCostUpdateActualCostValueObject() {
    itemsCodeAmount = new HashMap<>();
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public void addItem(String itemCode, BigDecimal itemAmount) {
    itemsCodeAmount.put(itemCode, itemAmount);
  }
  
  public Map<String, BigDecimal> getItemsCodeAmount() {
    return itemsCodeAmount;
  }

  public Long getRefDocumentId() {
    return refDocumentId;
  }

  public void setRefDocumentId(Long refDocumentId) {
    this.refDocumentId = refDocumentId;
  }


}
