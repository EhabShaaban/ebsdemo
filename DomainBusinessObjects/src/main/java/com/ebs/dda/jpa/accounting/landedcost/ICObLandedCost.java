package com.ebs.dda.jpa.accounting.landedcost;

public interface ICObLandedCost {

  String SYS_NAME = "LandedCostType";
  String CODE = "userCode";
  String NAME = "name";
}
