package com.ebs.dda.jpa.accounting.taxes;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "LObCompanyTaxesGeneralModel")
public class LObCompanyTaxesGeneralModel extends LObTaxInfoGeneralModel {
  String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  LocalizedString companyName;

  @Transient private BigDecimal taxAmount;

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public void setTaxAmount(BigDecimal taxAmount) {
    this.taxAmount = taxAmount;
  }
}
