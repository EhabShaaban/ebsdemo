package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObAccountingDocumentCompanyDataGeneralModel {

  String PAYMENT_REQUEST_SYS_NAME = "PaymentRequest";

  String DOCUMENT_CODE = "documentCode";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String COMPANY_NAME = "companyName";
  String COMPANY_CODE = "companyCode";

  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASING_UNIT_NAME);

}
