package com.ebs.dda.jpa.inventory.goodsissue;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObGoodsIssueSalesOrderData")
public class IObGoodsIssueSalesOrderData extends IObGoodsIssueRefDocumentData {

    private Long salesOrderId;

    public void setSalesOrderId(Long salesOrderId) {
        this.salesOrderId = salesOrderId;
    }

    public Long getSalesOrderId() {
        return salesOrderId;
    }
}
