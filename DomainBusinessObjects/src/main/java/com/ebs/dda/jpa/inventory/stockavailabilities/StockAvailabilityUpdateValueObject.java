package com.ebs.dda.jpa.inventory.stockavailabilities;

public class StockAvailabilityUpdateValueObject {
    private String code;
    private String type;

    public StockAvailabilityUpdateValueObject(String code, String type) {
        this.code = code;
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }
}
