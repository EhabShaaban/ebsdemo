package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptSalesReturnDataGeneralModel {

  String SALES_RETURN_ORDER_CODE = "salesReturnOrderCode";
  String CUSTOMER_NAME = "customerName";
  String CUSTOMER_CODE = "customerCode";
  String SALES_REPRESENTATIVE_NAME = "salesRepresentativeName";
  String COMMENTS = "comments";
}
