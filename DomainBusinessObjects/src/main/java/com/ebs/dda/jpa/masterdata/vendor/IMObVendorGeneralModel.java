package com.ebs.dda.jpa.masterdata.vendor;

public interface IMObVendorGeneralModel {

    String USER_CODE = "userCode";
    String VENDOR_NAME = "vendorName";
    String PURCHASING_UNIT_NAMES = "purchasingUnitNames";
    String PURCHASING_UNIT_CODES = "purchasingUnitCodes";
    String PURCHASING_UNIT = "purchaseUnits";
    String PURCHASING_RESPONSIBLE_CODE = "purchaseResponsibleCode";
    String PURCHASING_RESPONSIBLE_NAME = "purchaseResponsibleName";
    String ACCOUNT_WITH_VENDOR = "accountWithVendor";
    String OLD_VENDOR_NUMBER = "oldVendorNumber";
    String VENDOR_TYPE_NAME = "typeName";
    String VENDOR_TYPE_CODE = "typeCode";
}
