package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObCollectionDetailsGeneralModel")
@EntityInterface(IIObCollectionDetailsGeneralModel.class)
public class IObCollectionDetailsGeneralModel extends InformationObject {

  private String collectionCode;
  private String currentStates;
  private String refDocumentType;
  private String refDocumentCode;
  private String currencyISO;
  private String bankAccountCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString bankAccountName;

  private String bankAccountNumber;

  private String treasuryCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString treasuryName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  private String currencyCode;

  private String collectionMethod;
  private BigDecimal amount;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerType;

  private String businessPartnerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitCode;
  private String purchaseUnitNameEn;

  private String companyCurrencyISO;
  private String companyCurrencyCode;

  public String getBankAccountCode() {
    return bankAccountCode;
  }

  public String getTreasuryCode() {
    return treasuryCode;
  }

  public String getCollectionCode() {
    return collectionCode;
  }

  public String getCurrentStates() {
    return currentStates;
  }

  public String getRefDocumentType() {
    return refDocumentType;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCollectionMethod() {
    return collectionMethod;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public LocalizedString getBusinessPartnerName() {
    return businessPartnerName;
  }

  public LocalizedString getBusinessPartnerType() {
    return businessPartnerType;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public LocalizedString getBankAccountName() {
    return bankAccountName;
  }

  public LocalizedString getTreasuryName() {
    return treasuryName;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public String getCompanyCurrencyISO() {
    return companyCurrencyISO;
  }

  public String getCompanyCurrencyCode() {
    return companyCurrencyCode;
  }

  @Override
  public String getSysName() {
    return IIObCollectionDetailsGeneralModel.SYS_NAME;
  }
}
