package com.ebs.dda.jpa.accounting.monetarynotes;

public class RefDocumentRemainingValueObject {

  private String refDocumentType;
  private String refDocumentCode;

  public String getRefDocumentType() {
    return refDocumentType;
  }

  public void setRefDocumentType(String refDocumentType) {
    this.refDocumentType = refDocumentType;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public void setRefDocumentCode(String refDocumentCode) {
    this.refDocumentCode = refDocumentCode;
  }
}
