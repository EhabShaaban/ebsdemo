package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObInitialBalanceUpload {
    String INITIAL_STOCK_UPLOAD_KEY = "IBU";
    String SYS_NAME = "InitialBalanceUpload";

    String COMPANY_DETAILS_ATTR_NAME = "companydetails";

    CompositeAttribute<IObInitialBalanceUploadCompanyData> initialBalanceUploadCompanyDetails =
            new CompositeAttribute<>(
                    COMPANY_DETAILS_ATTR_NAME,
                    IObInitialBalanceUploadCompanyData.class,
                    false);

    String ITEM_ATTR_NAME = "items";
    CompositeAttribute<IObInitialBalanceUploadItem> itemsAttr =
            new CompositeAttribute<>(ITEM_ATTR_NAME, IObInitialBalanceUploadItem.class,
                    true);

    String ACTIVATION_DETAILS = "activationDetails";
    CompositeAttribute<IObInitialBalanceUploadActivationDetails> activationDetails =
            new CompositeAttribute<>(ACTIVATION_DETAILS, IObInitialBalanceUploadActivationDetails.class, false);
}
