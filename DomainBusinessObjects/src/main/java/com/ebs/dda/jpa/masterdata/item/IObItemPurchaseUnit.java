package com.ebs.dda.jpa.masterdata.item;

// Created By Hossam Hassan @ 10/30/18

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobitempurchaseunit")
public class IObItemPurchaseUnit extends InformationObject implements Serializable {

  @Column(name = "purchaseunitid")
  private Long purchaseUnitId;

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public void setPurchaseUnitId(Long purchaseUnitId) {
    this.purchaseUnitId = purchaseUnitId;
  }

  @Override
  public String getSysName() {
    return "IObItemPurchaseUnit";
  }
}
