package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("PaymentRequest")
public class IObPaymentRequestPostingDetails extends IObAccountingDocumentActivationDetails {
}

