package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobcustomeraddress")
public class IObCustomerAddress extends InformationObject {

    private static final long serialVersionUID = 1L;

    private Long addressId;
    private String type;

    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String getSysName() {
        return "IObCustomerAddress";
    }
}
