package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;

import javax.persistence.Entity;

@Entity
@EntityInterface(IIObAccountingDocumentCompanyDataGeneralModel.class)
public class IObInitialBalanceUploadCompanyDataGeneralModel
    extends IObAccountingDocumentCompanyDataGeneralModel {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IDObInitialBalanceUpload.SYS_NAME;
  }
}
