package com.ebs.dda.jpa.accounting;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "IObAccountingDocumentNotesReceivableAccountingDetails")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Notes_Receivables")
public class IObAccountingDocumentNotesReceivableAccountingDetails
				extends IObAccountingDocumentAccountingDetails implements Serializable {
	private Long glSubAccountId;

	@Override
	public Long getGlSubAccountId() {
		return glSubAccountId;
	}

	@Override
	public void setGlSubAccountId(Long glSubAccountId) {
		this.glSubAccountId = glSubAccountId;
	}
}
