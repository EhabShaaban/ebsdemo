package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObSalesInvoiceDeliverToCustomerGeneralModel {
    String PURCHASE_UNIT_CODE = "purchaseUnitCode";
    String PURCHASE_UNIT = "purchaseUnitName";
    String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
    String INVOICE_TYPE_CODE = "invoiceTypeCode";

    BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASE_UNIT);
    BasicAttribute invoiceTypeCodeAttr = new BasicAttribute(INVOICE_TYPE_CODE);


}
