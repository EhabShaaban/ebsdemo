package com.ebs.dda.jpa.masterdata.geonames;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/** The persistent class for the geonames_geoname database table. */
@Entity
@Table(name = "geonames_geoname")
public class Geonames implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "geonameid", unique = true, nullable = false)
  private Integer id;

  @Column(length = 200)
  private String admin1;

  @Column(length = 800)
  private String admin2;

  @Column(length = 200)
  private String admin3;

  @Column(length = 200)
  private String admin4;

  /*	@Column(length=2147483647)
      private String alternatenames;
  */
  @Column(length = 2000)
  private String asciiname;

  @Column(length = 600)
  private String cc2;

  @Column(length = 20)
  private String country;

  private Integer elevation;

  @Column(length = 1)
  private String fclass;

  @Column(length = 100)
  private String fcode;

  private Integer gtopo30;

  private double latitude;

  private double longitude;

  @Temporal(TemporalType.DATE)
  private Date moddate;

  @Column(length = 2000)
  private String name;

  private Long population;

  @Column(length = 400)
  private String timezone;

  public Geonames() {}

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getAdmin1() {
    return this.admin1;
  }

  public void setAdmin1(String admin1) {
    this.admin1 = admin1;
  }

  public String getAdmin2() {
    return this.admin2;
  }

  public void setAdmin2(String admin2) {
    this.admin2 = admin2;
  }

  public String getAdmin3() {
    return this.admin3;
  }

  public void setAdmin3(String admin3) {
    this.admin3 = admin3;
  }

  public String getAdmin4() {
    return this.admin4;
  }

  public void setAdmin4(String admin4) {
    this.admin4 = admin4;
  }

  /*	public String getAlternatenames() {
          return this.alternatenames;
      }

      public void setAlternatenames(String alternatenames) {
          this.alternatenames = alternatenames;
      }
  */
  public String getAsciiname() {
    return this.asciiname;
  }

  public void setAsciiname(String asciiname) {
    this.asciiname = asciiname;
  }

  public String getCc2() {
    return this.cc2;
  }

  public void setCc2(String cc2) {
    this.cc2 = cc2;
  }

  public String getCountry() {
    return this.country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public Integer getElevation() {
    return this.elevation;
  }

  public void setElevation(Integer elevation) {
    this.elevation = elevation;
  }

  public String getFclass() {
    return this.fclass;
  }

  public void setFclass(String fclass) {
    this.fclass = fclass;
  }

  public String getFcode() {
    return this.fcode;
  }

  public void setFcode(String fcode) {
    this.fcode = fcode;
  }

  public Integer getGtopo30() {
    return this.gtopo30;
  }

  public void setGtopo30(Integer gtopo30) {
    this.gtopo30 = gtopo30;
  }

  public double getLatitude() {
    return this.latitude;
  }

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  public double getLongitude() {
    return this.longitude;
  }

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  public Date getModdate() {
    return this.moddate;
  }

  public void setModdate(Date moddate) {
    this.moddate = moddate;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getPopulation() {
    return this.population;
  }

  public void setPopulation(Long population) {
    this.population = population;
  }

  public String getTimezone() {
    return this.timezone;
  }

  public void setTimezone(String timezone) {
    this.timezone = timezone;
  }
}
