package com.ebs.dda.jpa.masterdata;

import com.ebs.dac.dbo.jpa.entities.businessobjects.MasterDataObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 30, 2017 , 1:39:20 PM
 */
@Entity
@Table(name = "MasterData")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class MasterDataMinor extends MasterDataObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
