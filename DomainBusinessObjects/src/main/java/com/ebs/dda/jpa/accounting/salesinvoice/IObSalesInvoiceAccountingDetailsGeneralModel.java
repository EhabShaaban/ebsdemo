package com.ebs.dda.jpa.accounting.salesinvoice;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

@Entity
@Table(name = "IObSalesInvoiceAccountingDetailsGeneralModel")
public class IObSalesInvoiceAccountingDetailsGeneralModel
				extends IObAccountingDocumentAccountingDetailsGeneralModel {
	private static final long serialVersionUID = 1L;

	@Override
	public String getSysName() {
		return IDObSalesInvoice.SYS_NAME;
	}
}
