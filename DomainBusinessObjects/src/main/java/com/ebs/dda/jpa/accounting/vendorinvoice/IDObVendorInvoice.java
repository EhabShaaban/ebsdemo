package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObVendorInvoice {

  String SYS_NAME = "VendorInvoice";

  String PURCHASE_ORDER_NAME = "purchaseOrder";
  CompositeAttribute<IObVendorInvoicePurchaseOrder> purchaseOrderAttr =
      new CompositeAttribute<>(PURCHASE_ORDER_NAME, IObVendorInvoicePurchaseOrder.class, false);

  String INVOICE_DETAILS_ATTR_NAME = "invoiceDetails";
  CompositeAttribute<IObVendorInvoiceDetails> invoiceDetailsAttr =
      new CompositeAttribute<>(INVOICE_DETAILS_ATTR_NAME, IObVendorInvoiceDetails.class, false);

  String DOWN_PAYMENT_ATTR_NAME = "downPayment";
  CompositeAttribute<IObVendorInvoiceDetailsDownPayment> downPaymentAttr = new CompositeAttribute<>(
      DOWN_PAYMENT_ATTR_NAME, IObVendorInvoiceDetailsDownPayment.class, true);

  String POSTING_DETAILS_ATTR_NAME = "postingDetails";
  CompositeAttribute<IObVendorInvoicePostingDetails> postingDetailsAttr = new CompositeAttribute<>(
      POSTING_DETAILS_ATTR_NAME, IObVendorInvoicePostingDetails.class, false);

  String INVOICE_ITEMS_ATTR_NAME = "invoiceItems";
  CompositeAttribute<IObVendorInvoiceItem> invoiceItemsAttr =
      new CompositeAttribute<>(INVOICE_ITEMS_ATTR_NAME, IObVendorInvoiceItem.class, true);

  String COMPANY_DATA_NAME = "companyData";
  CompositeAttribute<IObVendorInvoiceCompanyData> companyData =
      new CompositeAttribute<>(COMPANY_DATA_NAME, IObVendorInvoiceCompanyData.class, false);

  String SUMMARY_ATTR_NAME = "summary";
  CompositeAttribute<IObVendorInvoiceSummary> summaryAttr =
          new CompositeAttribute<>(SUMMARY_ATTR_NAME, IObVendorInvoiceSummary.class, false);

}
