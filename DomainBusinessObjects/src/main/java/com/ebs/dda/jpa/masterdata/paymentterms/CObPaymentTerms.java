package com.ebs.dda.jpa.masterdata.paymentterms;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CObPaymentTerms")
@EntityInterface(ICObPaymentTerms.class)
public class CObPaymentTerms extends ConfigurationObject<DefaultUserCode>
    implements IAggregateAwareEntity, Serializable {

  private static final long serialVersionUID = 1L;

  private Boolean appliedToVendor;

  private Boolean appliedToCustomer;

  private Boolean requiredDownPayment;

  private Short downPaymentPercentage;

  private Long downPaymentMethod;

  private Long downPaymentDue;

  private String description;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  public CObPaymentTerms() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }

  @Override
  public String getSysName() {
    return ICObPaymentTerms.SYS_NAME;
  }

  public Boolean getAppliedToVendor() {
    return appliedToVendor;
  }

  public void setAppliedToVendor(Boolean appliedToVendor) {
    this.appliedToVendor = appliedToVendor;
  }

  public Boolean getAppliedToCustomer() {
    return appliedToCustomer;
  }

  public void setAppliedToCustomer(Boolean appliedToCustomer) {
    this.appliedToCustomer = appliedToCustomer;
  }

  public Boolean getRequiredDownPayment() {
    return requiredDownPayment;
  }

  public void setRequiredDownPayment(Boolean requiredDownPayment) {
    this.requiredDownPayment = requiredDownPayment;
  }

  public Short getDownPaymentPercentage() {
    return downPaymentPercentage;
  }

  public void setDownPaymentPercentage(Short downPaymentPercentage) {
    this.downPaymentPercentage = downPaymentPercentage;
  }

  public Long getDownPaymentMethod() {
    return downPaymentMethod;
  }

  public void setDownPaymentMethod(Long downPaymentMethod) {
    this.downPaymentMethod = downPaymentMethod;
  }

  public Long getDownPaymentDue() {
    return downPaymentDue;
  }

  public void setDownPaymentDue(Long downPaymentDue) {
    this.downPaymentDue = downPaymentDue;
  }

  public AggregateBusinessObject getAggregateBusinessObject() {
    return aggregateBusinessObject;
  }

  public void setAggregateBusinessObject(AggregateBusinessObject aggregateBusinessObject) {
    this.aggregateBusinessObject = aggregateBusinessObject;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
