package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObSettlementJournalEntryPreparationGeneralModel")
public class DObSettlementJournalEntryPreparationGeneralModel {
  @Id private Long id;
  private String code;
  private Long businessUnitId;
  private Long companyId;
  private Long fiscalPeriodId;
  private Long exchangeRateId;
  private Long companyCurrencyId;
  private Long documentCurrencyId;
  private BigDecimal currencyPrice;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  DateTime dueDate;

  public Long getId() {
    return id;
  }

  public String getCode() {
    return code;
  }

  public Long getBusinessUnitId() {
    return businessUnitId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public Long getFiscalPeriodId() {
    return fiscalPeriodId;
  }

  public Long getExchangeRateId() {
    return exchangeRateId;
  }

  public Long getCompanyCurrencyId() {
    return companyCurrencyId;
  }

  public Long getDocumentCurrencyId() {
    return documentCurrencyId;
  }

  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }

  public DateTime getDueDate() {
    return dueDate;
  }
}
