package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObItemAccountingInfo")
public class IObItemAccountingInfo extends InformationObject {

  private Long chartOfAccountId;
  private Boolean costFactor;

  public Boolean getCostFactor() {
    return costFactor;
  }

  public void setCostFactor(Boolean costFactor) {
    this.costFactor = costFactor;
  }

  public Long getChartOfAccountId() {
    return chartOfAccountId;
  }

  public void setChartOfAccountId(Long chartOfAccountId) {
    this.chartOfAccountId = chartOfAccountId;
  }

  @Override
  public String getSysName() {
    return IIObItemAccountingInfo.SYS_NAME;
  }
}
