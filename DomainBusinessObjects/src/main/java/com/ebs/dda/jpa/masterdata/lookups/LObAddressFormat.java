/** */
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.geolocale.LObGeoLocale;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 24, 2016 2:58:09 PM
 */
@Entity
@Table(name = "LObGeoLocale")
@DiscriminatorValue("6")
public class LObAddressFormat extends LObGeoLocale implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_ADDRESS_FORMAT_SYS_NAME;
  }
}
