package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
@EntityInterface(IIObSalesOrderItemGeneralModel.class)
@Table(name = "IObSalesOrderItemGeneralModel")
public class IObSalesOrderItemGeneralModel extends InformationObject {

    private String salesOrderCode;
    private String itemCode;
    private String itemTypeCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString itemName;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString itemTypeName;

    private String unitOfMeasureCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString unitOfMeasureName;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString unitOfMeasureSymbol;

    private BigDecimal quantity;
    private BigDecimal salesPrice;
    private BigDecimal totalAmount;

    @Transient
    private BigDecimal lastSalesPrice;
    @Transient
    private BigDecimal availableQuantity;

    public String getSalesOrderCode() {
        return salesOrderCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public String getItemTypeCode() {
        return itemTypeCode;
    }

    public LocalizedString getItemTypeName() {
        return itemTypeName;
    }

    public LocalizedString getItemName() {
        return itemName;
    }

    public String getUnitOfMeasureCode() {
        return unitOfMeasureCode;
    }

    public LocalizedString getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public LocalizedString getUnitOfMeasureSymbol() {
        return unitOfMeasureSymbol;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public BigDecimal getSalesPrice() {
        return salesPrice;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setLastSalesPrice(BigDecimal lastSalesPrice) {
        this.lastSalesPrice = lastSalesPrice;
    }

    public void setAvailableQuantity(BigDecimal availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    @Override
    public String getSysName() {
        return IDObSalesOrder.SYS_NAME;
    }
}
