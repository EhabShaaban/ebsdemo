package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObSalesOrder {
  String SYS_NAME = "SalesOrder";

  String SALES_ORDER_DATA_ATTR_NAME = "salesorderdata";
  CompositeAttribute<IObSalesOrderData> salesOrderDataAttr =
      new CompositeAttribute<>(SALES_ORDER_DATA_ATTR_NAME, IObSalesOrderData.class, false);

  String SALES_ORDER_ITEMS_ATTR_NAME = "items";
  CompositeAttribute<IObSalesOrderItem> salesOrderItemsAttr =
      new CompositeAttribute<>(SALES_ORDER_ITEMS_ATTR_NAME, IObSalesOrderItem.class, true);

  String SALES_ORDER_COMPANY_STORE_ATTR_NAME = "companyandstoredata";
  CompositeAttribute<IObSalesOrderCompanyStoreData> companyStoreDataAttr =
      new CompositeAttribute<>(
          SALES_ORDER_COMPANY_STORE_ATTR_NAME, IObSalesOrderCompanyStoreData.class, false);

  String SALES_ORDER_CYCLE_DATES_ATTR_NAME = "cycledates";
  CompositeAttribute<IObSalesOrderCycleDates> cycleDatesAttr =
      new CompositeAttribute<>(
          SALES_ORDER_CYCLE_DATES_ATTR_NAME, IObSalesOrderCycleDates.class, false);

  String SALES_ORDER_TAXES_ATTR_NAME = "taxes";
  CompositeAttribute<IObSalesOrderTax> salesOrderTaxesAttr =
      new CompositeAttribute<>(SALES_ORDER_TAXES_ATTR_NAME, IObSalesOrderTax.class, true);
}
