package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class IObInvoiceItemsGeneralModel extends InformationObject {

    @Column(name = "code")
    private String invoiceCode;

    private String itemCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString itemName;

    private String baseUnitCode;

    @Convert(converter = LocalizedStringConverter.class)
    @Column(name = "baseunitsymbol")
    private LocalizedString baseUnitSymbol;

    private String orderUnitCode;

    @Convert(converter = LocalizedStringConverter.class)
    @Column(name = "orderunitsymbol")
    private LocalizedString orderUnitSymbol;

    private BigDecimal quantityInOrderUnit;

    private BigDecimal price;

    private BigDecimal orderUnitPrice;

    private BigDecimal conversionFactorToBase;

    private BigDecimal totalAmount;

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public LocalizedString getItemName() {
        return itemName;
    }

    public String getBaseUnitCode() {
        return baseUnitCode;
    }

    public LocalizedString getBaseUnitSymbol() {
        return baseUnitSymbol;
    }

    public String getOrderUnitCode() {
        return orderUnitCode;
    }

    public LocalizedString getOrderUnitSymbol() {
        return orderUnitSymbol;
    }

    public BigDecimal getQuantityInOrderUnit() {
        return quantityInOrderUnit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getConversionFactorToBase() {
        return conversionFactorToBase;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public BigDecimal getOrderUnitPrice() {
        return orderUnitPrice;
    }

    @Override
    public String getSysName() {
        return null;
    }
}
