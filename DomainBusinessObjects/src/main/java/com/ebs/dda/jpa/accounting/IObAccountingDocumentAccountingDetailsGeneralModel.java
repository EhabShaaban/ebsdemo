package com.ebs.dda.jpa.accounting;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import java.math.BigDecimal;

@MappedSuperclass
public abstract class IObAccountingDocumentAccountingDetailsGeneralModel extends InformationObject {

	private String documentCode;

	private String accountingEntry;
	private String subLedger;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString glAccountName;

	private String glAccountCode;

	private Long glAccountId;

	private String glSubAccountCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString glSubAccountName;

	private Long glSubAccountId;

	private BigDecimal amount;

	public String getAccountingEntry() {
		return accountingEntry;
	}

	public String getSubLedger() {
		return subLedger;
	}

	public LocalizedString getGlAccountName() {
		return glAccountName;
	}

	public String getGlAccountCode() {
		return glAccountCode;
	}

	public String getGlSubAccountCode() {
		return glSubAccountCode;
	}

	public LocalizedString getGlSubAccountName() {
		return glSubAccountName;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Long getGlAccountId() {
		return glAccountId;
	}

	public Long getGlSubAccountId() {
		return glSubAccountId;
	}

	public String getDocumentCode() {
		return documentCode;
	}
}
