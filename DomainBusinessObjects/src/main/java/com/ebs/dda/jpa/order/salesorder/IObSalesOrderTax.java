package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dac.dbo.types.LocalizedString;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObSalesOrderTax")
public class IObSalesOrderTax extends DocumentLine implements Serializable {

  private String code;
  private BigDecimal percentage;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  public void setPercentage(BigDecimal percentage) {
    this.percentage = percentage;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }
}
