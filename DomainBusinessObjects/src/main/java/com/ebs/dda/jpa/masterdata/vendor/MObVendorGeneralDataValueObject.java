package com.ebs.dda.jpa.masterdata.vendor;

import java.util.List;

public class MObVendorGeneralDataValueObject {

  private String vendorCode;

  private String vendorName;

  private String purchaseResponsibleCode;

  private String accountWithVendor;

  private List<String> purchaseUnitCodes;

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public String getVendorName() {
    return vendorName;
  }

  public void setVendorName(String vendorName) {
    this.vendorName = vendorName;
  }

  public String getPurchaseResponsibleCode() {
    return purchaseResponsibleCode;
  }

  public void setPurchaseResponsibleCode(String purchaseResponsibleCode) {
    this.purchaseResponsibleCode = purchaseResponsibleCode;
  }

  public String getAccountWithVendor() {
    return accountWithVendor;
  }

  public void setAccountWithVendor(String accountWithVendor) {
    this.accountWithVendor = accountWithVendor;
  }

  public List<String> getPurchaseUnits() {
    return purchaseUnitCodes;
  }

  public void setPurchaseUnits(List<String> purchaseUnitCodes) {
    this.purchaseUnitCodes = purchaseUnitCodes;
  }
}
