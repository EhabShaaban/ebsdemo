package com.ebs.dda.jpa.accounting.factories;

import com.ebs.dda.jpa.accounting.*;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 3, 2021
 */
public class IObAccountingDocumentAccountingDetailsFactory {

	public static final String PO = SubLedgers.PO.toString();
	public static final String BANKS = SubLedgers.Banks.toString();
	public static final String LOCAL_CUSTOMER = SubLedgers.Local_Customer.toString();
	public static final String EXPORT_CUSTOMER = SubLedgers.Export_Customers.toString();
	public static final String TAXES = SubLedgers.Taxes.toString();
	public static final String LOCAL_VENDORS = SubLedgers.Local_Vendors.toString();
	public static final String IMPORT_VENDORS = SubLedgers.Import_Vendors.toString();
	public static final String NOTES_RECEIVABLE = SubLedgers.Notes_Receivables.toString();;
	public static final String TREASURIES = SubLedgers.Treasuries.toString();
	public static final String UNREALIZED_CURRENCY_GAIN_LOSS = SubLedgers.UnrealizedCurrencyGainLoss
					.toString();

	private IObAccountingDocumentAccountingDetailsFactory() {
	}

	public static IObAccountingDocumentAccountingDetails initAccountingDetails(String ledger) {
		if (ledger == null) {
			return new IObAccountingDocumentAccountingDetails();
		}
		if (ledger.equals(BANKS)) {

			return new IObAccountingDocumentBankAccountingDetails();
		}
		if (ledger.equals(LOCAL_CUSTOMER) || ledger.equals(EXPORT_CUSTOMER)) {

			return new IObAccountingDocumentCustomerAccountingDetails();
		}
		if (ledger.equals(PO)) {
			return new IObAccountingDocumentOrderAccountingDetails();
		}
		if (ledger.equals(TREASURIES)) {

			return new IObAccountingDocumentTreasuryAccountingDetails();
		}
		if (ledger.equals(UNREALIZED_CURRENCY_GAIN_LOSS)) {
			return new IObAccountingDocumentUnRealizedExchangeRateAccountingDetails();
		}
		if (ledger.equals(NOTES_RECEIVABLE)) {

			return new IObAccountingDocumentNotesReceivableAccountingDetails();
		}
		if (ledger.equals(LOCAL_VENDORS) || ledger.equals(IMPORT_VENDORS)) {

			return new IObAccountingDocumentVendorAccountingDetails();
		}
		if (ledger.equals(TAXES)) {

			return new IObAccountingDocumentTaxAccountingDetails();
		}
		throw new IllegalArgumentException(
						String.format("Ledger type '%s' is not supported", ledger));
	}

}
