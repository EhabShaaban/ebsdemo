package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.*;

@Entity
@Table(name = "IObAccountingDocumentCompanyData")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class IObAccountingDocumentCompanyData extends DocumentHeader {

  private static final long serialVersionUID = 1L;

  private Long companyId;
  private Long purchaseUnitId;

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public void setPurchaseUnitId(Long purchaseUnitId) {
    this.purchaseUnitId = purchaseUnitId;
  }
}
