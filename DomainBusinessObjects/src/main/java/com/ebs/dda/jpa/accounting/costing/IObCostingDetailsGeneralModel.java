package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "IObCostingDetailsGeneralModel")
@EntityInterface(IIObCostingDetailsGeneralModel.class)
public class IObCostingDetailsGeneralModel extends InformationObject {
  private String goodsReceiptCode;
  private String purchaseOrderCode;
  private String userCode;
  private String localCurrencyIso;
  private BigDecimal currencyPriceEstimateTime;
  private BigDecimal currencyPriceActualTime;

  @Convert(converter = StatefullBusinessObjectCurrentStatesJpaConverter.class)
  private Set<String> currentStates;

  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getUserCode() {
    return userCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getLocalCurrencyIso() {
    return localCurrencyIso;
  }

  public BigDecimal getCurrencyPriceEstimateTime() {
    return currencyPriceEstimateTime;
  }

  public BigDecimal getCurrencyPriceActualTime() {
    return currencyPriceActualTime;
  }

  public Set<String> getCurrentStates() {
    return currentStates;
  }

  @Override
  public String getSysName() {
    return IDObCosting.SYS_NAME;
  }
}
