package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.jpa.masterdata.ivr.IItemVendorRecord;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Menna Sayed
 * @date Nov 1, 2017
 */
@Entity
@Table(name = "ItemVendorRecord")
public class ItemVendorRecord extends CodedBusinessObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long vendorId;

  private Long itemId;

  private Long currencyId;

  private Long uomId;

  private Double price;

  private String itemVendorCode;

  private String oldItemNumber;

  private Long purchaseUnitId;

  private Long alternativeUomId;

  public Long getVendorId() {
    return vendorId;
  }

  public void setVendorId(Long vendorId) {
    this.vendorId = vendorId;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getItemVendorCode() {
    return itemVendorCode;
  }

  public void setItemVendorCode(String itemVendorCode) {
    this.itemVendorCode = itemVendorCode;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public void setPurchaseUnitId(Long purchaseUnitId) {
    this.purchaseUnitId = purchaseUnitId;
  }

  public Long getUomId() {
    return uomId;
  }

  public void setUomId(Long uomId) {
    this.uomId = uomId;
  }

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  public void setOldItemNumber(String oldItemNumber) {
    this.oldItemNumber = oldItemNumber;
  }

  public Long getAlternativeUomId() {
    return alternativeUomId;
  }

  public void setAlternativeUomId(Long alternativeUomId) {
    this.alternativeUomId = alternativeUomId;
  }

  @Override
  public String getSysName() {
    return IItemVendorRecord.SYS_NAME;
  }
}
