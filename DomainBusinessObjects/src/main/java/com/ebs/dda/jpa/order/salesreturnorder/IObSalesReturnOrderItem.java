package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesReturnOrderItems")
public class IObSalesReturnOrderItem extends DocumentLine {

  private Long itemId;
  private Long orderUnitId;
  private BigDecimal returnQuantity;
  private BigDecimal salesPrice;
  private Long returnReasonId;

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public void setOrderUnitId(Long orderUnitId) {
    this.orderUnitId = orderUnitId;
  }

  public void setReturnQuantity(BigDecimal returnQuantity) {
    this.returnQuantity = returnQuantity;
  }

  public void setSalesPrice(BigDecimal salesPrice) {
    this.salesPrice = salesPrice;
  }

  public void setReturnReasonId(Long returnReasonId) {
    this.returnReasonId = returnReasonId;
  }

  public Long getItemId() {
    return itemId;
  }

  public Long getOrderUnitId() {
    return orderUnitId;
  }

  public BigDecimal getReturnQuantity() {
    return returnQuantity;
  }

  public BigDecimal getSalesPrice() {
    return salesPrice;
  }

  public Long getReturnReasonId() {
    return returnReasonId;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
