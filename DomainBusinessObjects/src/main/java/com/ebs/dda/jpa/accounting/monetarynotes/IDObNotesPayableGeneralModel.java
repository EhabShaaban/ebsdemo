package com.ebs.dda.jpa.accounting.monetarynotes;

public interface IDObNotesPayableGeneralModel {

  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";
  String CURRENT_STATES = "currentStates";

  String USER_CODE = "userCode";
  String CODE = "Code";
  String NAME = "Name";

  String OBJECT_TYPE_CODE = "objectTypeCode";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String REMAINING = "remaining";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
  String DOCUMENT_OWNER_NAME = "documentOwner";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String NOTE_DATE = "noteDate";
  String AMOUNT = "amount";
  String CURRENCY_ISO = "currencyIso";
  String NOTE_FORM = "noteForm";
}
