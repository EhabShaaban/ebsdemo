package com.ebs.dda.jpa.accounting;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IObInvoiceSummaryGeneralModel")
public class IObInvoiceSummaryGeneralModel {

  @Id
  private Long id;
  private String invoiceCode;
  private String invoiceType;
  private String totalAmountBeforeTaxes;
  private String taxAmount;
  private String netAmount;
  private String downpayment;
  private String totalRemaining;

  public Long getId() {
    return id;
  }

  public String getInvoiceCode() { return invoiceCode; }

  public String getInvoiceType() { return invoiceType; }

  public String getTotalAmountBeforeTaxes() {
    return totalAmountBeforeTaxes;
  }

  public String getTaxAmount() {
    return taxAmount;
  }

  public String getNetAmount() {
    return netAmount;
  }

  public String getDownpayment() {
    return downpayment;
  }

  public String getTotalRemaining() {
    return totalRemaining;
  }

}
