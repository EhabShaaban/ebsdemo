package com.ebs.dda.jpa.order.salesreturnorder;

public class IObSalesReturnOrderDeleteItemValueObject {

  private String salesReturnOrderCode;
  private Long itemId;

  public String getSalesReturnOrderCode() {
    return salesReturnOrderCode;
  }

  public void setSalesReturnOrderCode(String salesReturnOrderCode) {
    this.salesReturnOrderCode = salesReturnOrderCode;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }
}
