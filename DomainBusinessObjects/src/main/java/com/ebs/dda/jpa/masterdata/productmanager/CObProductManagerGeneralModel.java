package com.ebs.dda.jpa.masterdata.productmanager;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObProductManagerGeneralModel")
public class CObProductManagerGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  @Override
  public String getSysName() {
    return ICObProductManager.SYS_NAME;
  }
}
