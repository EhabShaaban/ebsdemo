package com.ebs.dda.jpa.masterdata.item;

public interface IMObItemUOMValueObject {

  String ALTERNATIVE_UNIT = "alternativeUnit";
  String CONVERSION_FACTOR = "conversionFactor";
  String OLD_ITEM_NUMBER = "oldItemNumber";
}
