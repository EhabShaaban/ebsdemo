package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.item.enums.QuantityValueUpdatingEnum;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.embededobjects.ProcurementSettings;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.embededobjects.ValuationSetting;

import javax.persistence.*;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 5:10:58 PM
 */
@Entity
@Table(name = "CObMaterialTypeGeneralModel")
public class CObMaterialTypeGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  @Embedded private ProcurementSettings procurementSettings;

  @Embedded private ValuationSetting valuationSetting;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "quantityUpdating"))
  private QuantityValueUpdatingEnum quantityUpdatingEnum;

  @Transient private LocalizedString localizedQuantityUpdating;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "valueUpdating"))
  private QuantityValueUpdatingEnum valueUpdatingEnum;

  @Transient private LocalizedString localizedValueUpdating;

  public CObMaterialTypeGeneralModel() {}

  @Override
  public String getSysName() {
    return ICObMaterialType.SYS_NAME;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public ProcurementSettings getProcurementSettings() {
    return procurementSettings;
  }

  public void setProcurementSettings(ProcurementSettings procurementSettings) {
    this.procurementSettings = procurementSettings;
  }

  public ValuationSetting getValuationSetting() {
    return valuationSetting;
  }

  public void setValuationSetting(ValuationSetting valuationSetting) {
    this.valuationSetting = valuationSetting;
  }

  public QuantityValueUpdatingEnum getQuantityUpdatingEnum() {
    return quantityUpdatingEnum;
  }

  public void setQuantityUpdatingEnum(QuantityValueUpdatingEnum quantityUpdatingEnum) {
    this.quantityUpdatingEnum = quantityUpdatingEnum;
  }

  public LocalizedString getLocalizedQuantityUpdating() {
    return localizedQuantityUpdating;
  }

  public void setLocalizedQuantityUpdating(LocalizedString localizedQuantityUpdating) {
    this.localizedQuantityUpdating = localizedQuantityUpdating;
  }

  public QuantityValueUpdatingEnum getValueUpdatingEnum() {
    return valueUpdatingEnum;
  }

  public void setValueUpdatingEnum(QuantityValueUpdatingEnum valueUpdatingEnum) {
    this.valueUpdatingEnum = valueUpdatingEnum;
  }

  public LocalizedString getLocalizedValueUpdating() {
    return localizedValueUpdating;
  }

  public void setLocalizedValueUpdating(LocalizedString localizedValueUpdating) {
    this.localizedValueUpdating = localizedValueUpdating;
  }
}
