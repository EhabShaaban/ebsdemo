package com.ebs.dda.jpa.accounting.accountingnotes;

public interface IDObAccountingNoteGeneralModel {

  String SYS_NAME = "AccountingNote";
  String USER_CODE = "userCode";
  String TYPE = "type";
  String AMOUNT = "amount";
  String REMAINING = "remaining";
  String BUSINESS_PARTNER_TYPE = "businessPartnerType";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String OBJECT_TYPE_CODE = "objectTypeCode";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String CURRENCY_ISO = "currencyIso";
  String CURRENCY_CODE = "currencyCode";
  String REFERENCE_DOCUMENT_TYPE = "referenceDocumentType";
  String REFERENCE_DOCUMENT_CODE = "referenceDocumentCode";
  String DOCUMENT_OWNER_NAME = "documentOwnerName";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String ACTIVATION_DATE = "activationDate";
  String CURRENT_STATES = "currentStates";

//  View All filter Columns
  String BUSINESS_PARTNER = "businessPartner";
  String BP_CODE = "Code";
  String BP_TYPE = "Type";
  String BP_NAME = "Name";

  String REFERENCE_DOCUMENT = "referenceDocument";
  String RD_CODE = "Code";
  String RD_Type = "Type";
}
