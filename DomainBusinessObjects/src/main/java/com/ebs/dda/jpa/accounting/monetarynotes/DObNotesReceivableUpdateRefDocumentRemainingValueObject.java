package com.ebs.dda.jpa.accounting.monetarynotes;

public class DObNotesReceivableUpdateRefDocumentRemainingValueObject{
  private String notesReceivableCode;

  public String getNotesReceivableCode() {
    return notesReceivableCode;
  }

  public void setNotesReceivableCode(String notesReceivableCode) {
    this.notesReceivableCode = notesReceivableCode;
  }
}
