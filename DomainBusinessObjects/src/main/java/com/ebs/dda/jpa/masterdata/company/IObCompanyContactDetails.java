package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.IIObContactDetails;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseContactDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
@EntityInterface(IIObContactDetails.class)
public class IObCompanyContactDetails extends IObEnterpriseContactDetails {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return "IObCompanyContactDetails";
  }
}
