package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ItemUnitOfEntries")
public class ItemUnitOfEntries {
  @Id
  @Column(name = "id")
  private Long id;

  @Column(name = "measurecode")
  private String measureCode;

  @Column(name = "itemcode")
  private String itemCode;

  @Column(name = "quantityinbaseunit")
  private float quantityinBaseUnit;

  @Column(name = "symbol")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString symbol;

  public Long getId() {
    return id;
  }

  public float getQuantityinBaseUnit() {
    return quantityinBaseUnit;
  }

  public LocalizedString getSymbol() {
    return symbol;
  }

  public String getMeasureCode() {
    return measureCode;
  }

  public String getItemCode() {
    return itemCode;
  }
}
