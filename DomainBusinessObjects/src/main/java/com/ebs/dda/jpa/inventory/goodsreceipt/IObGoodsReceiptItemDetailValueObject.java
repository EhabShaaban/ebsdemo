package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum.StockType;

import java.math.BigDecimal;

public class IObGoodsReceiptItemDetailValueObject {
    private Long id;
    private BigDecimal receivedQtyUoE;
    private StockType stockType;
    private String unitOfEntryCode;
    private String notes;
    private String goodsReceiptCode;
    private String itemCode;
    private String detailType;
    private String goodsReceiptType;

  public String getDetailType() {
    return detailType;
  }

  public void setDetailType(String detailType) {
    this.detailType = detailType;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public BigDecimal getReceivedQtyUoE() {
    return receivedQtyUoE;
  }

    public String getUnitOfEntryCode() {
        return unitOfEntryCode;
    }

    public StockType getStockType() {
        return stockType;
    }

    public String getNotes() {
        return notes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGoodsReceiptType() {
        return goodsReceiptType;
    }

    public void setGoodsReceiptType(String goodsReceiptType) {
        this.goodsReceiptType = goodsReceiptType;
    }
}
