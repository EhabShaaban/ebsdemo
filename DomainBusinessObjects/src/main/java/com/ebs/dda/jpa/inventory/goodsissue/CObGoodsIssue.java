package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObGoodsIssue")
@EntityInterface(ICObGoodsIssue.class)
public class CObGoodsIssue extends ConfigurationObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
      return ICObGoodsIssue.SYS_NAME;
  }
}
