package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObSalesReturnOrderCompanyDataGeneralModel")
public class IObSalesReturnOrderCompanyDataGeneralModel extends InformationObject {

  private String salesReturnOrderCode;
  private String businessUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessUnitName;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public LocalizedString getBusinessUnitName() {
    return businessUnitName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }


  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
