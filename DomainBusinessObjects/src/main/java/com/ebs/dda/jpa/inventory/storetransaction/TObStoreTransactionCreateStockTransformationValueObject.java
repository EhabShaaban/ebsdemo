package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class TObStoreTransactionCreateStockTransformationValueObject implements IValueObject {

  private String stockTransformationCode;

  public String getStockTransformationCode() {
    return stockTransformationCode;
  }

  public void setStockTransformationCode(String stockTransformationCode) {
    this.stockTransformationCode = stockTransformationCode;
  }
}
