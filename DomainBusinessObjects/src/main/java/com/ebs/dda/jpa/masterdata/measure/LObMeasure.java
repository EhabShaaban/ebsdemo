package com.ebs.dda.jpa.masterdata.measure;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 24, 2016
 */
@Entity
@Table(name = "LocalizedLObMeasure")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class LObMeasure extends LocalizedLookupObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
