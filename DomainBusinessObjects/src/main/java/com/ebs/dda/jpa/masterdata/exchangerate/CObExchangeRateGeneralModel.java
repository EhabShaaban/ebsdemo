package com.ebs.dda.jpa.masterdata.exchangerate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.joda.time.DateTime;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import java.math.BigDecimal;

@Entity
@Table(name = "cobexchangerategeneralmodel")
public class CObExchangeRateGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Column(name = "exchangetype")
  private String exchangeRateType;

  @Column(name = "exchangetypename")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString exchangeRateTypeName;

  @Column(name = "validfrom")
  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime validFrom;

  @Column(name = "firstCurrencyIso")
  private String firstCurrencyIso;

  @Column(name = "secondCurrencyIso")
  private String secondCurrencyIso;

  @Column(name = "firstCurrencyCode")
  private String firstCurrencyCode;

  @Column(name = "secondCurrencyCode")
  private String secondCurrencyCode;

  @Column(name = "firstcurrencyid")
  private Long firstCurrencyId;

  @Column(name = "secondcurrencyid")
  private Long secondCurrencyId;

  @Column(name = "firstvalue")
  private BigDecimal firstValue;

  @Column(name = "secondvalue")
  private BigDecimal secondValue;

  public String getExchangeRateType() {
    return exchangeRateType;
  }

  public LocalizedString getExchangeRateTypeName() {
    return exchangeRateTypeName;
  }

  public DateTime getValidFrom() {
    return validFrom;
  }

  public String getFirstCurrencyIso() {
    return firstCurrencyIso;
  }

  public String getSecondCurrencyIso() {
    return secondCurrencyIso;
  }

  public String getFirstCurrencyCode() {
    return firstCurrencyCode;
  }

  public String getSecondCurrencyCode() {
    return secondCurrencyCode;
  }

  public Long getFirstCurrencyId() {
    return firstCurrencyId;
  }

  public Long getSecondCurrencyId() {
    return secondCurrencyId;
  }

  public BigDecimal getFirstValue() {
    return firstValue;
  }

  public BigDecimal getSecondValue() {
    return secondValue;
  }

  @Override
  public String getSysName() {
    return ICObExchangeRate.SYS_NAME;
  }
}
