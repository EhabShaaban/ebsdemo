package com.ebs.dda.jpa.inventory.stocktransformation;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObStockTransformation {
    String SYS_NAME = "StockTransformation";
    String STOCK_TRANSFORMATION_KEY = "StkTr";

    String STOCK_TRANSFORMATION_DETAILS_ATTR_NAME = "stockTransformationDetails";
    CompositeAttribute<IObStockTransformationDetails> stockTransformationDetailsAttr =
            new CompositeAttribute<>(
                    STOCK_TRANSFORMATION_DETAILS_ATTR_NAME, IObStockTransformationDetails.class, false);
}
