package com.ebs.dda.jpa.accounting.paymentrequest;

public class DObPaymentRequestCreateValueObject {

  private PaymentTypeEnum.PaymentType paymentType;
  private PaymentFormEnum.PaymentForm paymentForm;
  private String businessUnitCode;
  private String businessPartnerCode;
  private DueDocumentTypeEnum.DueDocumentType dueDocumentType;
  private String dueDocumentCode;
  private String companyCode;
  private Long documentOwnerId;

  public PaymentTypeEnum.PaymentType getPaymentType() {
    return paymentType;
  }

  public PaymentFormEnum.PaymentForm getPaymentForm() {
    return paymentForm;
  }


  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }


  public DueDocumentTypeEnum.DueDocumentType getDueDocumentType() {
    return dueDocumentType;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public String getDueDocumentCode() {
    return dueDocumentCode;
  }

  public void setPaymentType(PaymentTypeEnum.PaymentType paymentType) {
    this.paymentType = paymentType;
  }

  public void setPaymentForm(PaymentFormEnum.PaymentForm paymentForm) {
    this.paymentForm = paymentForm;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public void setBusinessPartnerCode(String businessPartnerCode) {
    this.businessPartnerCode = businessPartnerCode;
  }

  public void setDueDocumentType(DueDocumentTypeEnum.DueDocumentType dueDocumentType) {
    this.dueDocumentType = dueDocumentType;
  }

  public void setDueDocumentCode(String dueDocumentCode) {
    this.dueDocumentCode = dueDocumentCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }
}
