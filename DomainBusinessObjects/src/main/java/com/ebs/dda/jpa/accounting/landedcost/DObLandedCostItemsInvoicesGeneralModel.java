package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObLandedCostItemsInvoicesGeneralModel")
public class DObLandedCostItemsInvoicesGeneralModel {

	@Id
	private Long id;
	private String vendorInvoiceCode;
	private String purchaseOrderCode;
	private Long itemId;
	private BigDecimal totalAmount;

	public Long getId() {
		return id;
	}

	public String getVendorInvoiceCode() {
		return vendorInvoiceCode;
	}

	public String getPurchaseOrderCode() {
		return purchaseOrderCode;
	}

	public Long getItemId() {
		return itemId;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}
}
