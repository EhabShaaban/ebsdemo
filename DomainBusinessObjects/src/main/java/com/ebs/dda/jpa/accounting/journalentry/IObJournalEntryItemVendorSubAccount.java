package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "IObJournalEntryItemVendorSubAccount")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Vendors")
public class IObJournalEntryItemVendorSubAccount extends IObJournalEntryItem {

  private Long subAccountId;

  private String type;

  public void setSubAccountId(Long subAccountId) {
    this.subAccountId = subAccountId;
  }

  public void setType(String type) {
    this.type = type;
  }
}
