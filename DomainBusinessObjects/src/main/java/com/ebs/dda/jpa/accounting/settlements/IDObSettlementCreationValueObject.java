package com.ebs.dda.jpa.accounting.settlements;

public interface IDObSettlementCreationValueObject {

	String TYPE = "type";
	String BUSINESS_UNIT_CODE = "businessUnitCode";
	String COMPANY_CODE = "companyCode";
	String CURRENCY_CODE = "currencyCode";
	String DOCUMENT_OWNER_ID = "documentOwnerId";
}
