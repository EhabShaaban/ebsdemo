package com.ebs.dda.jpa.accounting.vendorinvoice;


import com.ebs.dda.jpa.accounting.IObTaxes;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObVendorInvoiceTaxes")
public class IObVendorInvoiceTaxes extends IObTaxes {

}
