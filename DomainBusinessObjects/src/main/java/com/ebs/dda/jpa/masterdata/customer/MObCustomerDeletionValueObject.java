package com.ebs.dda.jpa.masterdata.customer;

public class MObCustomerDeletionValueObject {

  private String customerCode;

  public String getCustomerCode() {
    return customerCode;
  }

  public void setCustomerCode(String customerCode) {
    this.customerCode = customerCode;
  }
}
