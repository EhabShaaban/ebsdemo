package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObCostingItems")
public class IObCostingItem extends DocumentLine {

  private Long itemId;
  private Long uomId;
  private BigDecimal quantity;
  private String stockType;
  private BigDecimal toBeConsideredInCostingPer;
  private BigDecimal estimateUnitPrice;
  private BigDecimal actualUnitPrice;
  private BigDecimal estimateUnitLandedCost;
  private BigDecimal actualUnitLandedCost;

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Long getUomId() {
    return uomId;
  }

  public void setUomId(Long uomId) {
    this.uomId = uomId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getStockType() {
    return stockType;
  }

  public void setStockType(String stockType) {
    this.stockType = stockType;
  }

  public BigDecimal getToBeConsideredInCostingPer() {
    return toBeConsideredInCostingPer;
  }

  public void setToBeConsideredInCostingPer(BigDecimal toBeConsideredInCostingPer) {
    this.toBeConsideredInCostingPer = toBeConsideredInCostingPer;
  }

  public BigDecimal getEstimateUnitPrice() {
    return estimateUnitPrice;
  }

  public void setEstimateUnitPrice(BigDecimal estimateUnitPrice) {
    this.estimateUnitPrice = estimateUnitPrice;
  }

  public BigDecimal getActualUnitPrice() {
    return actualUnitPrice;
  }

  public void setActualUnitPrice(BigDecimal actualUnitPrice) {
    this.actualUnitPrice = actualUnitPrice;
  }

  public BigDecimal getEstimateUnitLandedCost() {
    return estimateUnitLandedCost;
  }

  public void setEstimateUnitLandedCost(BigDecimal estimateUnitLandedCost) {
    this.estimateUnitLandedCost = estimateUnitLandedCost;
  }

  public BigDecimal getActualUnitLandedCost() {
    return actualUnitLandedCost;
  }

  public void setActualUnitLandedCost(BigDecimal actualUnitLandedCost) {
    this.actualUnitLandedCost = actualUnitLandedCost;
  }
}
