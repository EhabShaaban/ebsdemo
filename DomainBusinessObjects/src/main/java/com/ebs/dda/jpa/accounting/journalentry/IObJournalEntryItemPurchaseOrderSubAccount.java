package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "IObJournalEntryItemPurchaseOrderSubAccount")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "PO")
public class IObJournalEntryItemPurchaseOrderSubAccount extends IObJournalEntryItem {

  Long subAccountId;

  public void setSubAccountId(Long subAccountId) {
    this.subAccountId = subAccountId;
  }
}
