package com.ebs.dda.jpa.masterdata.chartofaccount;

public class HObChartOfAccountDeleteValueObject {

	private String glAccountCode;

	public String getGlAccountCode() {
		return glAccountCode;
	}

	public void setGlAccountCode(String glAccountCode) {
		this.glAccountCode = glAccountCode;
	}
}
