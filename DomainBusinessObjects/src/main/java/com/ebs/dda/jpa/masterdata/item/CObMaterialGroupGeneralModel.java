package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType;
import com.ebs.dda.jpa.masterdata.itemgroup.IHObItemGroup;

import javax.persistence.*;

@Entity
@Table(name = "CObMaterialGroupGeneralModel")
public class CObMaterialGroupGeneralModel extends CodedBusinessObject {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  @Column(name = "level")
  @Embedded
  private HierarchyLevelEnumType level;

  @Column(name = "parentcode")
  private String parentCode;

  @Column(name = "parentName")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString parentName;

  @Transient private LocalizedString localizedParent;
  @Transient private LocalizedString localizedLevel;

  public LocalizedString getParentName() {
    return parentName;
  }

  public LocalizedString getName() {
    return name;
  }

  public HierarchyLevelEnumType getLevel() {
    return level;
  }

  public String getParentCode() {
    return parentCode;
  }

  public LocalizedString getLocalizedParent() {
    return localizedParent;
  }

  public LocalizedString getLocalizedLevel() {
    return localizedLevel;
  }

  @Override
  public String getSysName() {
    return IHObItemGroup.SYS_NAME;
  }
}
