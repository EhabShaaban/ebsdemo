package com.ebs.dda.jpa.inventory.goodsissue;

public interface ICObGoodsIssue {

  String SYS_NAME = "GoodsIssueType";
  String CODE = "userCode";
}
