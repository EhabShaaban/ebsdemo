package com.ebs.dda.jpa.accounting;

public interface IIObAccountingDocumentAccountDetailsGeneralModel {

  String ACCOUNTING_ENTRY = "accountingEntry";
  String SUB_LEDGER = "subLedger";
  String ACCOUNT_NAME = "glAccountName";
  String ACCOUNT_CODE = "glAccountCode";
  String SUB_ACCOUNT_CODE = "glSubAccountCode";
  String SUB_ACCOUNT_NAME = "glSubAccountName";
  String AMOUNT = "amount";
  String DOCUMENT_CODE = "documentCode";
}
