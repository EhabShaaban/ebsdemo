package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IObCollectionDetails")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "refDocumentTypeId", discriminatorType = DiscriminatorType.INTEGER)
public abstract class IObCollectionDetails extends DocumentHeader implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long businessPartnerId;
  private Long currencyId;
  private Long refDocumentTypeId;
  private String collectionMethod;
  private String description;
  private BigDecimal amount;
  private Long bankAccountId;
  private Long treasuryId;

  public Long getBusinessPartnerId() {
    return businessPartnerId;
  }

  public void setBusinessPartnerId(Long businessPartnerId) {
    this.businessPartnerId = businessPartnerId;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public Long getRefDocumentTypeId() {
    return refDocumentTypeId;
  }

  public void setRefDocumentTypeId(Long refDocumentTypeId) {
    this.refDocumentTypeId = refDocumentTypeId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getCollectionMethod() {
    return collectionMethod;
  }

  public void setCollectionMethod(String collectionMethod) {
    this.collectionMethod = collectionMethod;
  }

  public Long getBankAccountId() {
    return bankAccountId;
  }

  public void setBankAccountId(Long bankAccountId) {
    this.bankAccountId = bankAccountId;
  }

  public Long getTreasuryId() {
    return treasuryId;
  }

  public void setTreasuryId(Long treasuryId) {
    this.treasuryId = treasuryId;
  }
}
