package com.ebs.dda.jpa.order;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "DObOrderDocument")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class DObOrderDocument extends DocumentObject<DefaultUserCode> {

  private Long documentOwnerId;
  private String objectTypeCode;

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }

  public String getObjectTypeCode() {
    return this.objectTypeCode;
  }
}
