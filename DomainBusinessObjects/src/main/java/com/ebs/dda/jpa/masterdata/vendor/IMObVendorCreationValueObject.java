package com.ebs.dda.jpa.masterdata.vendor;

public interface IMObVendorCreationValueObject {
  String VENDOR_NAME = "vendorName";
  String VENDOR_TYPE = "vendorType";
  String PURCHASE_RESPONSIBLE_CODE = "purchaseResponsibleCode";
  String OLD_VENDOR_REFERENCE = "oldVendorReference";
  String ACCOUNT_WITH_VENDOR = "accountWithVendor";
  String PURCHASE_UNITS = "purchaseUnits";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
}
