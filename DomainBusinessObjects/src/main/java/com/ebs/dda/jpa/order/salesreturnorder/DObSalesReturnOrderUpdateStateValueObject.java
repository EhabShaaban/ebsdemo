package com.ebs.dda.jpa.order.salesreturnorder;

public class DObSalesReturnOrderUpdateStateValueObject {

  private String salesReturnOrderCode;

  public String getSalesReturnOrderCode() {
    return salesReturnOrderCode;
  }

  public void setSalesReturnOrderCode(String salesReturnOrderCode) {
    this.salesReturnOrderCode = salesReturnOrderCode;
  }
}
