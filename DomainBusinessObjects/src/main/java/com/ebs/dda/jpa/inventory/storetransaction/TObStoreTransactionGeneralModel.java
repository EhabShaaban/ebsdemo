package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.TransactionObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "TObStoreTransactionGeneralModel")
@EntityInterface(ITObStoreTransactionGeneralModel.class)
public class TObStoreTransactionGeneralModel extends TransactionObject<DefaultUserCode> {

  private String transactionOperation;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String plantCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString plantName;

  private String storehouseCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storehouseName;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String stockType;

  private String itemCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;

  private String unitOfMeasureCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString unitOfMeasureName;

  private BigDecimal quantity;
  private BigDecimal estimateCost;
  private BigDecimal actualCost;
  private String refDocumentCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString refDocumentType;

  private BigDecimal remainingQuantity;
  private String refTransactionCode;

  private String purchaseUnitNameEn;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime originalAddingDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime updatingActualCostDate;

  private String batchNo;

  public String getTransactionOperation() {
    return transactionOperation;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public LocalizedString getPlantName() {
    return plantName;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public LocalizedString getStorehouseName() {
    return storehouseName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getStockType() {
    return stockType;
  }

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public LocalizedString getUnitOfMeasureName() {
    return unitOfMeasureName;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public BigDecimal getEstimateCost() {
    return estimateCost;
  }

  public BigDecimal getActualCost() {
    return actualCost;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public LocalizedString getRefDocumentType() {
    return refDocumentType;
  }

  public BigDecimal getRemainingQuantity() {
    return remainingQuantity;
  }

  public String getRefTransactionCode() {
    return refTransactionCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public DateTime getOriginalAddingDate() {
    return originalAddingDate;
  }

  public DateTime getUpdatingActualCostDate() {
    return updatingActualCostDate;
  }

  public String getBatchNo() {
    return batchNo;
  }

  @Override
  public String getSysName() {
    return ITObStoreTransactionGeneralModel.SYS_NAME;
  }
}
