package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObGoodsIssueItemQuantityGeneralModel")
public class IObGoodsIssueItemQuantityGeneralModel extends IObItemQuantityGeneralModel {

  private static final long serialVersionUID = 1L;
  private String goodsIssueType;

  public String getGoodsIssueType() {
    return goodsIssueType;
  }

  public String getStockType() {
    return "UNRESTRICTED_USE";
  }
  @Override
  public String getSysName() {
    return "IObGoodsReceiptItemQuantityGeneralModel";
  }
}
