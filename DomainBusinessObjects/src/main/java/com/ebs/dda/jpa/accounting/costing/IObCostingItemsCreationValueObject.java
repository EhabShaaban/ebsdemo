package com.ebs.dda.jpa.accounting.costing;

import java.math.BigDecimal;

public class IObCostingItemsCreationValueObject {
  private String vendorInvoiceCode;
  private String goodsReceiptCode;
  private String landedCostCode;
  private BigDecimal currencyPrice;
  private boolean isLandedCostActual;

  public IObCostingItemsCreationValueObject(
      String vendorInvoiceCode,
      String goodsReceiptCode,
      String landedCostCode,
      BigDecimal currencyPrice,boolean isLandedCostActual) {
    this.vendorInvoiceCode = vendorInvoiceCode;
    this.goodsReceiptCode = goodsReceiptCode;
    this.landedCostCode = landedCostCode;
    this.currencyPrice = currencyPrice;
    this.isLandedCostActual=isLandedCostActual;
  }

  public String getVendorInvoiceCode() {
    return vendorInvoiceCode;
  }

  public void setVendorInvoiceCode(String vendorInvoiceCode) {
    this.vendorInvoiceCode = vendorInvoiceCode;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }

  public String getLandedCostCode() {
    return landedCostCode;
  }

  public void setLandedCostCode(String landedCostCode) {
    this.landedCostCode = landedCostCode;
  }

  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }

  public void setCurrencyPrice(BigDecimal currencyPrice) {
    this.currencyPrice = currencyPrice;
  }

  public boolean isLandedCostActual() {
    return isLandedCostActual;
  }

  public void setLandedCostActual(boolean landedCostActual) {
    isLandedCostActual = landedCostActual;
  }
}
