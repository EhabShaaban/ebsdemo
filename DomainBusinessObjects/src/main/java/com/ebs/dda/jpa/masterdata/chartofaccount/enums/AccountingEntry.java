package com.ebs.dda.jpa.masterdata.chartofaccount.enums;

public enum AccountingEntry {
  CREDIT(Values.CREDIT),
  DEBIT(Values.DEBIT);

  private AccountingEntry(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }

  public static class Values {
    public static final String CREDIT = "CREDIT";
    public static final String DEBIT = "DEBIT";
  }
}
