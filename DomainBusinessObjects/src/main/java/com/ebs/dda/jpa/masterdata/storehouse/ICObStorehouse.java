package com.ebs.dda.jpa.masterdata.storehouse;

public interface ICObStorehouse {
  String SYS_NAME = "Storehouse";
  String USER_CODE = "userCode";
  String NAME = "name";
}
