package com.ebs.dda.jpa.accounting.salesinvoice;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobsalesinvoicecustomerbusinesspartner")
@DiscriminatorValue("CUSTOMER")
public class IObSalesInvoiceCustomerBusinessPartner extends IObSalesInvoiceBusinessPartner
    implements Serializable {

  private Long customerId;

  public Long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }
}
