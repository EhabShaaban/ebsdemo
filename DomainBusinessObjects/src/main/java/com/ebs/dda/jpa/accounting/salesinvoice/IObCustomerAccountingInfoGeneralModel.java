package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCustomerAccountingInfoGeneralModel")
public class IObCustomerAccountingInfoGeneralModel extends InformationObject {

    private String accountCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString accountName;

    private String accountLedger;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString customerName;

    private String customerCode;


    @Override
    public String getSysName() {
        return "CustomerAccountingInfo";
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public LocalizedString getAccountName() {
        return accountName;
    }

    public void setAccountName(LocalizedString accountName) {
        this.accountName = accountName;
    }

    public String getAccountLedger() {
        return accountLedger;
    }

    public LocalizedString getCustomerName() {
        return customerName;
    }

    public String getCustomerCode() {
        return customerCode;
    }
}
