package com.ebs.dda.jpa.masterdata.paymentterms;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObPaymentTermsGeneralModel")
public class CObPaymentTermsGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  private Boolean appliedToVendor;

  private Boolean appliedToCustomer;

  private Boolean requiredDownPayment;

  private Short downPaymentPercentage;

  private int downPaymentMethod;

  private int downPaymentDue;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString downPaymentMethodName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString downPaymentDueName;

  @Override
  public String getSysName() {
    return ICObPaymentTerms.SYS_NAME;
  }

  public Boolean getAppliedToVendor() {
    return appliedToVendor;
  }

  public void setAppliedToVendor(Boolean appliedToVendor) {
    this.appliedToVendor = appliedToVendor;
  }

  public Boolean getAppliedToCustomer() {
    return appliedToCustomer;
  }

  public void setAppliedToCustomer(Boolean appliedToCustomer) {
    this.appliedToCustomer = appliedToCustomer;
  }

  public Boolean getRequiredDownPayment() {
    return requiredDownPayment;
  }

  public void setRequiredDownPayment(Boolean requiredDownPayment) {
    this.requiredDownPayment = requiredDownPayment;
  }

  public Short getDownPaymentPercentage() {
    return downPaymentPercentage;
  }

  public void setDownPaymentPercentage(Short downPaymentPercentage) {
    this.downPaymentPercentage = downPaymentPercentage;
  }

  public int getDownPaymentMethod() {
    return downPaymentMethod;
  }

  public void setDownPaymentMethod(int downPaymentMethod) {
    this.downPaymentMethod = downPaymentMethod;
  }

  public int getDownPaymentDue() {
    return downPaymentDue;
  }

  public void setDownPaymentDue(int downPaymentDue) {
    this.downPaymentDue = downPaymentDue;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public LocalizedString getDownPaymentMethodName() {
    return downPaymentMethodName;
  }

  public void setDownPaymentMethodName(LocalizedString downPaymentMethodName) {
    this.downPaymentMethodName = downPaymentMethodName;
  }

  public LocalizedString getDownPaymentDueName() {
    return downPaymentDueName;
  }

  public void setDownPaymentDueName(LocalizedString downPaymentDueName) {
    this.downPaymentDueName = downPaymentDueName;
  }
}
