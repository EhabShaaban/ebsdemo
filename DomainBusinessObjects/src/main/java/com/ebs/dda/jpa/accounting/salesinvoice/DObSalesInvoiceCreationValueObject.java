package com.ebs.dda.jpa.accounting.salesinvoice;

public class DObSalesInvoiceCreationValueObject {

  private String invoiceTypeCode;
  private String salesOrderCode;
  private String businessUnitCode;
  private String companyCode;
  private String customerCode;
  private Long documentOwnerId;

  public String getInvoiceTypeCode() {
    return invoiceTypeCode;
  }

  public void setInvoiceTypeCode(String invoiceTypeCode) {
    this.invoiceTypeCode = invoiceTypeCode;
  }

  public String getSalesOrderCode() {
    return salesOrderCode;
  }

  public void setSalesOrderCode(String salesOrderCode) {
    this.salesOrderCode = salesOrderCode;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public void setCustomerCode(String customerCode) {
    this.customerCode = customerCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }

}
