package com.ebs.dda.jpa.accounting.paymentrequest;

public interface IIObPaymentRequestPostingDetailsGeneralModel {
  String SYS_NAME = "PaymentRequest";

    String ACCOUNTANT = "accountant";
  String ACTIVATION_DATE = "activationDate";
  String CURRENCY_PRICE = "currencyPrice";
    String JOURNAL_ENTRY_CODE = "journalEntryCode";

}
