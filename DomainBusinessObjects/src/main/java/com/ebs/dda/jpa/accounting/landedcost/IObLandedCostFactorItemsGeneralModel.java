package com.ebs.dda.jpa.accounting.landedcost;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

@Entity
@Table(name = "IObLandedCostFactorItemsGeneralModel")
public class IObLandedCostFactorItemsGeneralModel extends InformationObject {

  private static final long serialVersionUID = 1L;
  private String code;
  private String itemCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;
  private String companyCurrency;
  private BigDecimal estimatedValue;
  private BigDecimal actualValue;
  private BigDecimal difference;
  @Transient
  private List<IObLandedCostFactorItemsDetailsGeneralModel> details;

  public String getCode() {
    return code;
  }

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getCompanyCurrency() {
    return companyCurrency;
  }

  public BigDecimal getEstimatedValue() {
    return estimatedValue;
  }

  public BigDecimal getActualValue() {
    return actualValue;
  }

  public BigDecimal getDifference() {
    return difference;
  }

  public List<IObLandedCostFactorItemsDetailsGeneralModel> getDetails() {
    return details;
  }

  public void setDetails(List<IObLandedCostFactorItemsDetailsGeneralModel> details) {
    this.details = details;
  }

  @Override
  public String getSysName() {
    return IDObLandedCost.SYS_NAME;
  }
}
