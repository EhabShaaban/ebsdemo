package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "DObSettlementJournalEntry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("Settlement")
public class DObSettlementJournalEntry extends DObJournalEntry {

	private Long documentReferenceId;

	public void setDocumentReferenceId(Long documentReferenceId) {
		this.documentReferenceId = documentReferenceId;
	}
}
