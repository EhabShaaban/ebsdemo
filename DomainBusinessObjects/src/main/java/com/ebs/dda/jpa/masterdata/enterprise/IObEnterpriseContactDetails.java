package com.ebs.dda.jpa.masterdata.enterprise;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.*;

@Entity
@Table(name = "IObEnterpriseContact")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
@EntityInterface(IIObContactDetails.class)
public class IObEnterpriseContactDetails extends ContactDetails {

  private static final long serialVersionUID = 1L;
}
