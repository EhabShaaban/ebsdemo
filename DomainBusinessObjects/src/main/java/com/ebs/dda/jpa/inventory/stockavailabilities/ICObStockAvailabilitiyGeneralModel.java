package com.ebs.dda.jpa.inventory.stockavailabilities;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface ICObStockAvailabilitiyGeneralModel {

  String SYS_NAME = "StockAvailability";
  String ITEM = "item";
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String UOM_NAME = "unitOfMeasureName";
  String UOM_CODE = "unitOfMeasureCode";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String PLANT_CODE = "plantCode";
  String PLANT_NAME = "plantName";
  String STOCK_TYPE = "stockType";
  String AVAILABLE_QUANTITY = "availableQuantity";
  String PURCHASEUNIT_CODE = "purchaseUnitCode";
  String PURCHASEUNIT_NAME = "purchaseUnitName";
  String STOREHOUSE_CODE = "storehouseCode";
  String STOREHOUSE_NAME= "storehouseName";
  String PURCHASEUNIT_NAME_EN = "purchaseUnitNameEn";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASEUNIT_NAME);

}