package com.ebs.dda.jpa.accounting.paymentrequest;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OrderPaymentsGeneralModel")
public class OrderPaymentsGeneralModel {
    @Id
    private Long paymentId;
    private String paymentCode;
    private String orderCode;
    private String orderType;
    private String referenceOrderCode;

    public Long getPaymentId() {
        return paymentId;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public String getOrderType() {
        return orderType;
    }

    public String getReferenceOrderCode() {
        return referenceOrderCode;
    }
}
