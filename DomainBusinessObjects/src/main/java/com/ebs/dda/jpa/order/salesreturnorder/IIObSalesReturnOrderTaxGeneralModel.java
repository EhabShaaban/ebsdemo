package com.ebs.dda.jpa.order.salesreturnorder;

public interface IIObSalesReturnOrderTaxGeneralModel {
  String TAX_NAME = "name";
  String TAX_CODE = "code";
  String TAX_PERCENTAGE = "percentage";
  String TAX_AMOUNT = "amount";
}
