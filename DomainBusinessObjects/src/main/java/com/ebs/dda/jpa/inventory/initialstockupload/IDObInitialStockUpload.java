package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObInitialStockUpload {
  String SYS_NAME = "InitialStockUpload";
  String INITIAL_STOCK_UPLOAD_KEY = "ISU";

  String ITEMS_ATTR_NAME = "items";
  CompositeAttribute<IObInitialStockUploadItem> itemsAttr =
          new CompositeAttribute<>(ITEMS_ATTR_NAME, IObInitialStockUploadItem.class, true);

}
