package com.ebs.dda.jpa.inventory.goodsissue;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObGoodsIssueSalesInvoiceData")
public class IObGoodsIssueSalesInvoiceData extends IObGoodsIssueRefDocumentData {

    private Long salesInvoiceId;

    public void setSalesInvoiceId(Long salesInvoiceId) {
        this.salesInvoiceId = salesInvoiceId;
    }

    public Long getSalesInvoiceId() {
        return salesInvoiceId;
    }
}
