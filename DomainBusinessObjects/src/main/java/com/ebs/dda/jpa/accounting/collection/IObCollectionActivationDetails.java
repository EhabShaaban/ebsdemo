package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Collection")
public class IObCollectionActivationDetails extends IObAccountingDocumentActivationDetails {
}
