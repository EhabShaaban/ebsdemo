package com.ebs.dda.jpa.inventory.goodsissue;

public interface IIObGoodsIssueItemGeneralModel {

    String ITEM_NAME = "itemName";
    String ITEM_CODE = "itemCode";
    String UNIT_OF_MEASURE_SYMBOL = "unitOfMeasureSymbol";
    String QUANTITY = "quantity";
    String BATCH_NO = "batchNo";
    String BASE_UNIT_CODE = "baseUnitCode";
    String RECEIVED_QTY_BASE = "receivedQtyBase";
    String UOM_CODE = "unitOfMeasureCode";
}
