package com.ebs.dda.jpa.accounting.landedcost;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObLandedCostFactorItems")
public class IObLandedCostFactorItems extends DocumentLine {

  private static final long serialVersionUID = 1L;

  private Long costItemId;
  private BigDecimal estimateValue;
  private BigDecimal actualValue;

  public Long getCostItemId() {
    return costItemId;
  }

  public void setCostItemId(Long costItemId) {
    this.costItemId = costItemId;
  }

  public BigDecimal getEstimateValue() {
    return estimateValue;
  }

  public void setEstimateValue(BigDecimal estimateValue) {
    this.estimateValue = estimateValue;
  }

  public BigDecimal getActualValue() {
    return actualValue;
  }

  public void setActualValue(BigDecimal actualValue) {
    this.actualValue = actualValue;
  }
}
