package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObLandedCostItemsPaymentRequestsGeneralModel")
public class DObLandedCostItemsPaymentRequestsGeneralModel {

	@Id
	private Long id;
	private String paymentRequestCode;
	private String purchaseOrderCode;
	private Long itemId;
	private BigDecimal netAmount;

	public Long getId() {
		return id;
	}

	public String getPaymentRequestCode() {
		return paymentRequestCode;
	}

	public String getPurchaseOrderCode() {
		return purchaseOrderCode;
	}

	public Long getItemId() {
		return itemId;
	}

	public BigDecimal getNetAmount() {
		return netAmount;
	}
}
