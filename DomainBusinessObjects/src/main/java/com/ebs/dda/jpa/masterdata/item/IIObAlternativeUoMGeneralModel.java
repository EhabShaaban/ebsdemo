package com.ebs.dda.jpa.masterdata.item;

public interface IIObAlternativeUoMGeneralModel {

  String ALTERNATIVE_UOM_CODE = "alternativeUnitOfMeasureCode";
  String ALTERNATIVE_UOM_NAME = "alternativeUnitOfMeasureName";
  String ALTERNATIVE_UOM_SYMBOL = "alternativeUnitOfMeasureSymbol";
  String BASIC_UOM_CODE = "basicUnitOfMeasureCode";
  String BASIC_UOM_NAME = "basicUnitOfMeasureName";
  String BASIC_UOM_SYMBOL = "basicUnitOfMeasureSymbol";
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String USER_CODE = "userCode";
  String SYMBOL = "symbol";
  String CONVERSION_FACTOR = "conversionFactor";
}
