package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 12, 2017 , 11:32:40 AM
 */
@Entity
@DiscriminatorValue("3")
public class LObValuationArea extends LObMaterial {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_VALUATION_AREA_SYS_NAME;
  }
}
