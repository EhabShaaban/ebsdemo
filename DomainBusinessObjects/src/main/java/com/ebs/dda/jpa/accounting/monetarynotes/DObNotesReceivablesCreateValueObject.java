package com.ebs.dda.jpa.accounting.monetarynotes;

import java.math.BigDecimal;

public class DObNotesReceivablesCreateValueObject {
  private MonetaryNoteFormEnum noteForm;
  private NotesReceivableTypeEnum type;
  private String businessUnitCode;
  private String companyCode;
  private String businessPartnerCode;
  private Long documentOwnerId;
  private BigDecimal amount;
  private String currencyIso;

  public MonetaryNoteFormEnum getNoteForm() {
    return noteForm;
  }

  public NotesReceivableTypeEnum getType() {
    return type;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public void setNoteForm(MonetaryNoteFormEnum noteForm) {
    this.noteForm = noteForm;
  }

  public void setType(NotesReceivableTypeEnum type) {
    this.type = type;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public void setBusinessPartnerCode(String businessPartnerCode) {
    this.businessPartnerCode = businessPartnerCode;
  }

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public void setCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
  }
}
