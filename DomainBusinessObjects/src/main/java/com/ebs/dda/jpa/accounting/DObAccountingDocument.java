package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;

@Entity
@Table(name = "DObAccountingDocument")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class DObAccountingDocument extends DocumentObject<DefaultUserCode> {

   private Long documentOwnerId;

    public void setDocumentOwnerId(Long documentOwnerId) {
        this.documentOwnerId = documentOwnerId;
    }


}
