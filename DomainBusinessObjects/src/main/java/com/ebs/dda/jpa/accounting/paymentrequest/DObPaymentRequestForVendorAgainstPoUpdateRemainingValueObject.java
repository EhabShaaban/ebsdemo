package com.ebs.dda.jpa.accounting.paymentrequest;

import java.math.BigDecimal;

public class DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject {
  private String paymentRequestCode;
  private BigDecimal amountValue;

  public String getPaymentRequestCode() {
    return paymentRequestCode;
  }

  public void setPaymentRequestCode(String paymentRequestCode) {
    this.paymentRequestCode = paymentRequestCode;
  }

  public BigDecimal getAmountValue() {
    return amountValue;
  }

  public void setAmountValue(BigDecimal amountValue) {
    this.amountValue = amountValue;
  }
}
