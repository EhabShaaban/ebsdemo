package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@EntityInterface(IDObSalesOrderGeneralModel.class)
@Table(name = "DObSalesOrderGeneralModel")
public class DObSalesOrderGeneralModel extends DocumentObject<DefaultUserCode> {

  private String salesOrderTypeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString salesOrderTypeName;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private String salesResponsibleCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString salesResponsibleName;

  private String salesResponsibleNameEn;

  private String customerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String storeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storeName;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime expectedDeliveryDate;

  private String notes;

  private BigDecimal totalAmount;
  private BigDecimal remaining;

  public String getSalesOrderTypeCode() {
    return salesOrderTypeCode;
  }

  public LocalizedString getSalesOrderTypeName() {
    return salesOrderTypeName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getSalesResponsibleCode() {
    return salesResponsibleCode;
  }

  public LocalizedString getSalesResponsibleName() {
    return salesResponsibleName;
  }

  public String getSalesResponsibleNameEn() {
    return salesResponsibleNameEn;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getStoreCode() {
    return storeCode;
  }

  public LocalizedString getStoreName() {
    return storeName;
  }

  public DateTime getExpectedDeliveryDate() {
    return expectedDeliveryDate;
  }

  public String getNotes() {
    return notes;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }

  @Override
  public String getSysName() {
    return IDObSalesOrder.SYS_NAME;
  }
}
