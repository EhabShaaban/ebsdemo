package com.ebs.dda.jpa.accounting.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObCollectionSalesOrderDetails")
@DiscriminatorValue("3")
public class IObCollectionSalesOrderDetails extends IObCollectionDetails implements Serializable {
  private Long salesOrderId;

  public Long getSalesOrderId() {
    return salesOrderId;
  }

  public void setSalesOrderId(Long salesOrderId) {
    this.salesOrderId = salesOrderId;
  }
}
