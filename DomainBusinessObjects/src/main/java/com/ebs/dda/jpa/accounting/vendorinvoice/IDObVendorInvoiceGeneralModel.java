package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObVendorInvoiceGeneralModel {

  String PURCHASING_UNIT_NAME = "purchaseUnitNameEn";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME);
  String INVOICE_CODE = "userCode";
  String CREATION_INFO = "creationInfo";
  String CREATION_DATE = "creationDate";
  String VENDOR_CODE = "vendorCode";
  String INVOICE_TYPE = "invoiceType";
  String INVOICE_NUMBER = "invoiceNumber";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String CURRENCY_CODE = "currencyCode";
    String CURRENCY_NAME = "currencyName";
    String CURRENCY_ISO = "currencyISO";
  String PAYMENT_TERM_CODE = "paymentTermCode";
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String PRUCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String VENDOR_CODE_NAME = "vendorCodeName";
  String DOCUMENT_OWNER = "documentOwner";
}
