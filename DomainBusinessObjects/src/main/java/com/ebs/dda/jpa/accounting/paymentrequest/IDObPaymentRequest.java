package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.jpa.accounting.IDObAccountingDocument;

public interface IDObPaymentRequest extends IDObAccountingDocument {
  String SYS_NAME = "PaymentRequest";

  String COMPANY_DATA_ATTR_NAME = "companydata";
  CompositeAttribute<IObPaymentRequestCompanyData> companyDataAttr =
      new CompositeAttribute<>(COMPANY_DATA_ATTR_NAME, IObPaymentRequestCompanyData.class, false);

  String PAYMENT_DETAILS_ATTR_NAME = "paymentdetails";
  CompositeAttribute<IObPaymentRequestPaymentDetails> paymentdetailsAttr = new CompositeAttribute<>(
      PAYMENT_DETAILS_ATTR_NAME, IObPaymentRequestPaymentDetails.class, false);

  String POSTING_DETAILS_ATTR_NAME = "postingDetails";
  CompositeAttribute<IObPaymentRequestPostingDetails> postingDetailsDeAttr =
      new CompositeAttribute<>(POSTING_DETAILS_ATTR_NAME, IObPaymentRequestPostingDetails.class,
          false);
}
