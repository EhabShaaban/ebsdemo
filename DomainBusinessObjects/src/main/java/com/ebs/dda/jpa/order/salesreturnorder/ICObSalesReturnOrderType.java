package com.ebs.dda.jpa.order.salesreturnorder;

public interface ICObSalesReturnOrderType {

  String SYS_NAME = "SalesReturnOrderType";

  String USER_CODE = "userCode";
  String NAME = "name";
}
