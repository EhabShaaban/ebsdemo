
package com.ebs.dda.jpa.masterdata.exchangerate;

public interface ICObExchangeRateValueObject {

    String FIRST_CURRENCY = "firstCurrencyCode";
    String SECOND_VALUE = "secondValue";

}
