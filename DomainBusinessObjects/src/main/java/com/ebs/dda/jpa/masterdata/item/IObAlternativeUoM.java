package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dda.jpa.masterdata.measure.IIObAlternativeUoM;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 24, 2016
 */
@Entity
@Table(name = "IObAlternativeUoM")
@EntityInterface(IIObAlternativeUoM.class)
public class IObAlternativeUoM extends InformationObject implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "oldItemNumber")
  private String oldItemNumber;

  @Column(name = "alternativeUnitOfMeasureId")
  private Long alternativeUnitOfMeasureId;

  @Column(name = "conversionFactor")
  private Float conversionFactor;

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  public void setOldItemNumber(String oldItemNumber) {
    this.oldItemNumber = oldItemNumber;
  }

  public Long getAlternativeUnitOfMeasureId() {
    return alternativeUnitOfMeasureId;
  }

  public void setAlternativeUnitOfMeasureId(Long alternativeUnitOfMeasureId) {
    this.alternativeUnitOfMeasureId = alternativeUnitOfMeasureId;
  }

  public Float getConversionFactor() {
    return conversionFactor;
  }

  public void setConversionFactor(Float conversionFactor) {
    this.conversionFactor = conversionFactor;
  }

  @Override
  public String getSysName() {
    return IIObAlternativeUoM.SYS_NAME;
  }
}
