package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObPaymentRequestGeneralModel {

  String SYS_NAME = "PaymentRequest";
  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME_EN);

  String CREATION_INFO = "creationInfo";
  BasicAttribute creationInfoAttr = new BasicAttribute(CREATION_INFO);

  String USER_CODE = "userCode";
  String PAYMENT_TYPE = "paymentType";
  String PAYMENT_FORM = "paymentForm";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String CURRENT_STATES = "currentStates";
  String CREATION_DATE = "creationDate";
  String CURRENCY_ISO = "currencyISO";
  String AMOUNT = "amount";
  String AMOUNT_VALUE = "amountValue";
  String AMOUNT_CURRENCY = "amountCurrency";
  String STATE = "currentStates";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String BUSINESS_PARTNER = "businessPartner";
  String REFERENCE_DOCUMENT_TYPE = "referenceDocumentType";
  String REFERENCE_DOCUMENT_CODE = "referenceDocumentCode";
  String REFERENCE_DOCUMENT = "referenceDocument";
  String CODE = "Code";
  String NAME = "Name";
  String TYPE = "Type";
  String VALUE = "Value";
  String CURRENCY = "Currency";
  String REMAINING = "remaining";

  String BANK_TRANS_REF = "bankTransRef";
  String EXPECTED_DUE_DATE = "expectedDueDate";

  String FROM_EXPECTED_DUE_DATE = "fromExpectedDueDate";
  String TO_EXPECTED_DUE_DATE = "toExpectedDueDate";
  String DOCUMENT_OWNER = "documentOwner";
}
