package com.ebs.dda.jpa.masterdata.bank;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import java.io.Serializable;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CObBank")
public class CObBank extends CodedBusinessObject<DefaultUserCode>
    implements IAggregateAwareEntity, Serializable {

  public static final String SYS_NAME = "Bank";;
  private static final long serialVersionUID = 1L;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  public CObBank() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }

  @Override
  public String getSysName() {
    return SYS_NAME;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }
}
