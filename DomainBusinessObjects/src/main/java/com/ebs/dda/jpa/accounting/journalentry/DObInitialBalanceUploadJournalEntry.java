package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "DObInitialBalanceUploadJournalEntry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("InitialBalanceUpload")
public class DObInitialBalanceUploadJournalEntry extends DObJournalEntry {

	private Long documentReferenceId;

	public void setDocumentReferenceId(Long documentReferenceId) {
		this.documentReferenceId = documentReferenceId;
	}
}
