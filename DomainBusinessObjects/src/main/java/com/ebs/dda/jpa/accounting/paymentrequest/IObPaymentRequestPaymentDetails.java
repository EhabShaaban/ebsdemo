package com.ebs.dda.jpa.accounting.paymentrequest;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import org.joda.time.DateTime;

@Entity
@Table(name = "IObPaymentRequestPaymentDetails")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "dueDocumentType")
@EntityInterface(IIObPaymentRequestPaymentDetails.class)
public abstract class IObPaymentRequestPaymentDetails extends DocumentHeader
    implements Serializable {
  private static final long serialVersionUID = 1L;

  private Long businessPartnerId;
  private String dueDocumentType;
  private BigDecimal netAmount;
  private Long currencyId;
  private String description;
  private Long bankAccountId;
  private String paymentForm;
  private Long costFactorItemId;
  private Long treasuryId;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime expectedDueDate;

  private String bankTransRef;

  public Long getBusinessPartnerId() {
    return businessPartnerId;
  }

  public void setBusinessPartnerId(Long businessPartnerId) {
    this.businessPartnerId = businessPartnerId;
  }

  public String getDueDocumentType() {
    return dueDocumentType;
  }

  public void setDueDocumentType(String dueDocumentType) {
    this.dueDocumentType = dueDocumentType;
  }

  public BigDecimal getNetAmount() {
    return netAmount;
  }

  public void setNetAmount(BigDecimal netAmount) {
    this.netAmount = netAmount;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Long getBankAccountId() {
    return bankAccountId;
  }

  public void setBankAccountId(Long bankAccountId) {
    this.bankAccountId = bankAccountId;
  }

  public String getPaymentForm() {
    return paymentForm;
  }

  public void setPaymentForm(String paymentForm) {
    this.paymentForm = paymentForm;
  }

  public void setCostFactorItemId(Long costFactorItemId) {
    this.costFactorItemId = costFactorItemId;
  }

  public void setTreasuryId(Long treasuryId) {
    this.treasuryId = treasuryId;
  }

  public Long getCostFactorItemId() {
    return costFactorItemId;
  }

  public Long getTreasuryId() {
    return treasuryId;
  }

  public DateTime getExpectedDueDate() {
    return expectedDueDate;
  }

  public void setExpectedDueDate(DateTime expectedDueDate) {
    this.expectedDueDate = expectedDueDate;
  }

  public String getBankTransRef() {
    return bankTransRef;
  }

  public void setBankTransRef(String bankTransRef) {
    this.bankTransRef = bankTransRef;
  }

  @Override
  public String getSysName() {
    return IIObPaymentRequestPaymentDetails.SYS_NAME;
  }
}
