package com.ebs.dda.jpa.accounting.landedcost;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObLandedCostDetailsGeneralModel")
public class IObLandedCostDetailsGeneralModel extends InformationObject {

    private static final long serialVersionUID = 1L;
    private String landedCostCode;
    private String vendorInvoiceCode;
    private String invoiceCurrencyIso;
    private String companyCurrencyIso;
    private String businessUnitCode;
    private String purchaseOrderCode;
    private String businessUnitCodeName;
    private BigDecimal invoiceAmount;
    private Boolean damageStock;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getLandedCostCode() {
        return landedCostCode;
    }

    public String getVendorInvoiceCode() {
        return vendorInvoiceCode;
    }

    public String getInvoiceCurrencyIso() {
        return invoiceCurrencyIso;
    }

    public String getBusinessUnitCode() {
        return businessUnitCode;
    }

    public String getBusinessUnitCodeName() {
        return businessUnitCodeName;
    }

    public BigDecimal getInvoiceAmount() {
        return invoiceAmount;
    }

    public Boolean getDamageStock() {
        return damageStock;
    }

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public String getCompanyCurrencyIso() { return companyCurrencyIso; }

    @Override
    public String getSysName() {
        return IDObLandedCost.SYS_NAME;
    }
}
