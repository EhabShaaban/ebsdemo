package com.ebs.dda.jpa.accounting.accountingnotes;

public enum DebitNoteTypeEnum {
  DEBIT_NOTE_BASED_ON_COMMISSION(Values.DEBIT_NOTE_BASED_ON_COMMISSION);

  private DebitNoteTypeEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }

  public static class Values {
    public static final String DEBIT_NOTE_BASED_ON_COMMISSION = "DEBIT_NOTE_BASED_ON_COMMISSION";
  }
}
