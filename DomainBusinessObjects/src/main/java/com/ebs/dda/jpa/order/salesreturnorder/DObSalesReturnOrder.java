package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.SalesTypeEnum;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "DObSalesReturnOrder")
@EntityInterface(IDObSalesReturnOrder.class)
@DiscriminatorValue(SalesTypeEnum.Values.SALES_RETURN)
public class DObSalesReturnOrder extends DObOrderDocument implements Serializable {

  private Long salesReturnOrderTypeId;

  public Long getSalesReturnOrderTypeId() {
    return salesReturnOrderTypeId;
  }

  public void setSalesReturnOrderTypeId(Long salesOrderTypeId) {
    this.salesReturnOrderTypeId = salesOrderTypeId;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
