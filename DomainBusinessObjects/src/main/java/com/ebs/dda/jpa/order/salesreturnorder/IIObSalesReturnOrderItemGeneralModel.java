package com.ebs.dda.jpa.order.salesreturnorder;

public interface IIObSalesReturnOrderItemGeneralModel {
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String ORDER_UNIT_CODE = "orderUnitCode";
  String ORDER_UNIT_SYMBOL = "orderUnitSymbol";
  String RETURN_REASON_CODE = "returnReasonCode";
  String RETURN_REASON_NAME = "returnReasonName";
  String RETURN_QUANTITY = "returnQuantity";
  String MAX_RETURN_QUANTITY = "maxReturnQuantity";
  String SALES_PRICE = "salesPrice";
  String TOTAL_AMOUNT = "totalAmount";
  String TOTAL_AMOUNT_BEFORE_TAXES = "totalAmountBeforeTaxes";
  String TOTAL_AMOUNT_AFTER_TAXES = "totalAmountAfterTaxes";
  String TOTAL_TAXES = "totalTaxes";
}
