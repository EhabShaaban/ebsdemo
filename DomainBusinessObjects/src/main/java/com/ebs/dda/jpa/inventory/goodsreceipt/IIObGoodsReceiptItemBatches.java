package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptItemBatches {

    String BATCH_CODE = "batchCode";
    String RECEIVED_QTY_UOE = "receivedQtyUoE";
    String UNIT_OF_ENTRY_CODE = "unitOfEntryCode";
    String PRODUCTION_DATE = "productionDate";
    String EXPIRATION_DATE = "expirationDate";
    String STOCK_TYPE = "stockType";
    String NOTES = "notes";
}
