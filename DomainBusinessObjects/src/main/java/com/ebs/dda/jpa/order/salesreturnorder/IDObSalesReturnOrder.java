package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObSalesReturnOrder {
  String SYS_NAME = "SalesReturnOrder";

  String SALES_RETURN_ORDER_COMPANY_DATA_ATTR_NAME = "companydata";
  CompositeAttribute<IObSalesReturnOrderCompanyData> companyDataAttr =
      new CompositeAttribute<>(
          SALES_RETURN_ORDER_COMPANY_DATA_ATTR_NAME, IObSalesReturnOrderCompanyData.class, false);

  String SALES_RETURN_ORDER_DETAILS_ATTR_NAME = "returndetails";
  CompositeAttribute<IObSalesReturnOrderDetails> returnDetailsAttr =
      new CompositeAttribute<>(
          SALES_RETURN_ORDER_DETAILS_ATTR_NAME, IObSalesReturnOrderDetails.class, false);

  String SALES_RETURN_ORDER_ITEMS_ATTR_NAME = "items";
  CompositeAttribute<IObSalesReturnOrderItem> itemsAttr =
      new CompositeAttribute<>(
          SALES_RETURN_ORDER_ITEMS_ATTR_NAME, IObSalesReturnOrderItem.class, true);

  String SALES_RETURN_ORDER_TAXES_ATTR_NAME = "taxes";
  CompositeAttribute<IObSalesReturnOrderTax> taxesAttr =
      new CompositeAttribute<>(
          SALES_RETURN_ORDER_TAXES_ATTR_NAME, IObSalesReturnOrderTax.class, true);

  String SALES_RETURN_ORDER_CYCLE_DATES_ATTR_NAME = "cycledates";
  CompositeAttribute<IObSalesReturnOrderCycleDates> cycleDatesAttr =
      new CompositeAttribute<>(
          SALES_RETURN_ORDER_CYCLE_DATES_ATTR_NAME, IObSalesReturnOrderCycleDates.class, false);
}
