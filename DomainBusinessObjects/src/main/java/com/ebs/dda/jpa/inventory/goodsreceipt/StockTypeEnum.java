package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class StockTypeEnum extends BusinessEnum {

  public static final String SYS_NAME = "StockType";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private StockType value;

  public StockTypeEnum() {}

  @Override
  public StockType getId() {
    return this.value;
  }

  public void setId(StockType id) {
    this.value = id;
  }

  public enum StockType {
    UNRESTRICTED_USE,
    DAMAGED_STOCK
  }
}
