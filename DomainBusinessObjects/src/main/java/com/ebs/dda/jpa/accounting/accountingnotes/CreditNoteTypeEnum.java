package com.ebs.dda.jpa.accounting.accountingnotes;

public enum CreditNoteTypeEnum {
  CREDIT_NOTE_BASED_ON_SALES_RETURN(Values.CREDIT_NOTE_BASED_ON_SALES_RETURN);

  private CreditNoteTypeEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }

  public static class Values {
    public static final String CREDIT_NOTE_BASED_ON_SALES_RETURN =
        "CREDIT_NOTE_BASED_ON_SALES_RETURN";
  }
}
