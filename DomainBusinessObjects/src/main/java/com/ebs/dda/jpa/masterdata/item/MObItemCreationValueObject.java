package com.ebs.dda.jpa.masterdata.item;

import java.util.List;

// Created By Hossam Hassan @ 10/10/18
public class MObItemCreationValueObject {

  private String itemName;
  private String itemGroupCode;
  private String baseUnitCode;
  private Boolean batchManaged;
  private String itemOldNumber;
  private List<IObItemPurchaseUnitValueObject> purchaseUnits;
  private String marketName;
  private String itemType;

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public String getItemGroupCode() {
    return itemGroupCode;
  }

  public void setItemGroupCode(String itemGroupCode) {
    this.itemGroupCode = itemGroupCode;
  }

  public String getBaseUnitCode() {
    return baseUnitCode;
  }

  public void setBaseUnitCode(String baseUnitCode) {
    this.baseUnitCode = baseUnitCode;
  }

  public Boolean getBatchManaged() {
    return batchManaged;
  }

  public void setBatchManaged(Boolean batchManaged) {
    this.batchManaged = batchManaged;
  }

  public String getItemOldNumber() {
    return itemOldNumber;
  }

  public void setItemOldNumber(String itemOldNumber) {
    this.itemOldNumber = itemOldNumber;
  }

  public List<IObItemPurchaseUnitValueObject> getPurchaseUnits() {
    return purchaseUnits;
  }

  public void setPurchaseUnits(List<IObItemPurchaseUnitValueObject> purchaseUnits) {
    this.purchaseUnits = purchaseUnits;
  }

  public String getMarketName() {
    return marketName;
  }

  public void setMarketName(String marketName) {
    this.marketName = marketName;
  }

  public String getItemType() {
    return itemType;
  }

  public void setItemType(String itemType) {
    this.itemType = itemType;
  }
}
