package com.ebs.dda.jpa.accounting.vendorinvoice;

public interface IDObVendorInvoiceItemValueObject {

  String ITEM_CODE = "itemCode";
  String QTY = "quantityInOrderUnit";
  String ORDER_UNIT_CODE = "orderUnitCode";
  String UNIT_PRICE = "price";
}
