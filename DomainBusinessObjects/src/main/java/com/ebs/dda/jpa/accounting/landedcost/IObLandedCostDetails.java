package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

@Entity
@Table(name = "IObLandedCostDetails")
public class IObLandedCostDetails extends DocumentHeader {

  private static final long serialVersionUID = 1L;

  private Long purchaseOrderId;
  private Long vendorInvoiceId;
  private Boolean damageStock;

  public Long getPurchaseOrderId() {
    return purchaseOrderId;
  }

  public void setPurchaseOrderId(Long purchaseOrderId) {
    this.purchaseOrderId = purchaseOrderId;
  }

  public Long getVendorInvoiceId() {
    return vendorInvoiceId;
  }

  public void setVendorInvoiceId(Long vendorInvoiceId) {
    this.vendorInvoiceId = vendorInvoiceId;
  }

  public Boolean getDamageStock() {
    return damageStock;
  }

  public void setDamageStock(Boolean damageStock) {
    this.damageStock = damageStock;
  }

}
