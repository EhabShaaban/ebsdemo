package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.processing.CompositeAttribute;

/**
 * @author: Menna Sayed
 * @date: Jan 11, 2017
 */
public interface ICObMaterialStatus extends ICObMaterial {
  String SYS_NAME = "MaterialStatus";

  /** ******************************** PayLoad Attributes ********************************** */
  String MATERIAL_RESTRICTION_NAME = "materialRestriction";

  CompositeAttribute<IObMaterialRestriction> materialRestrictionAttr =
      new CompositeAttribute<>( MATERIAL_RESTRICTION_NAME, IObMaterialRestriction.class, true);
}
