package com.ebs.dda.jpa.masterdata.chartofaccount.enums;

public enum SubLedgers {
  NONE,
  PO,
  Local_Vendors,
  Import_Vendors,
  Banks,
  Local_Customer,
  Export_Customers,
  Taxes,
  Notes_Receivables,
  Treasuries,
  UnrealizedCurrencyGainLoss
}