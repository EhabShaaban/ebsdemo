package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "DObVendorInvoiceJournalEntry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("VendorInvoice")
public class DObVendorInvoiceJournalEntry extends DObJournalEntry {

  private Long documentReferenceId;

  public Long getDocumentReferenceId() {
    return documentReferenceId;
  }

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }
}
