package com.ebs.dda.jpa.accounting.journalbalance;

import java.math.BigDecimal;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

@Entity
@Table(name = "CObJournalBalance")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "objectTypeCode")
public class CObJournalBalance extends CodedBusinessObject<DefaultUserCode> {

  private String objectTypeCode;
  private Long businessUnitId;
  private Long companyId;
  private Long currencyId;
  private BigDecimal balance;


  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public void setObjectTypeCode(String objectTypeCode) {
    this.objectTypeCode = objectTypeCode;
  }

  public Long getBusinessUnitId() {
    return businessUnitId;
  }

  public void setBusinessUnitId(Long businessUnitId) {
    this.businessUnitId = businessUnitId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  @Override
  public String getSysName() {
    return ICObJournalBalance.SYS_NAME;
  }
}
