package com.ebs.dda.jpa.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dac.dbo.types.LocalizedString;

@MappedSuperclass
public abstract class IObTaxes extends DocumentLine implements Serializable {

  private String code;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;
  private BigDecimal percentage;
  private BigDecimal amount;

  public BigDecimal getPercentage() {
    return percentage;
  }

  public void setPercentage(BigDecimal percentage) {
    this.percentage = percentage;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }
}
