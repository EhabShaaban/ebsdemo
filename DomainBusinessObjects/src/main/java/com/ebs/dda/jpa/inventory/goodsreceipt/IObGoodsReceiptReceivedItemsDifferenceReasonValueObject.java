package com.ebs.dda.jpa.inventory.goodsreceipt;

public class IObGoodsReceiptReceivedItemsDifferenceReasonValueObject {

  private String goodsReceiptCode;
  private String itemCode;
  private String differenceReason;

  public String getDifferenceReason() {
    return differenceReason;
  }

  public void setDifferenceReason(String differenceReason) {
    this.differenceReason = differenceReason;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }
}
