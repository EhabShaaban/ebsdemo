package com.ebs.dda.jpa.inventory.storetransaction;

public interface ITObGoodsReceiptStoreTransactionDataGeneralModel {
    String GOODS_RECEIPT_CODE = "goodsReceiptCode";
    String STORE_HOUSE_ID = "storehouseid";
    String PLANT_ID = "plantid";
    String COMPANY_ID = "companyid";
    String PURCHASE_UNIT_ID = "purchaseunitid";
    String ITEM_ID = "itemid";
    String STOCK_TYPE = "stockType";
    String RECEIVED_QTY = "receivedQty";
    String RECEIVED_UOE_ID = "receivedUoeId";
    String RECEIVED_DEFECT_QTY = "receivedDefectQty";
    String RECEIVED_DEFECT_STOCK_TYPE = "receivedDefectStockType";
    String BATCH_NO = "batchNo";
    String BATCH_QTY = "batchQty";
    String BATCH_UOE_ID = "batchUoeId";
    String BATCH_DEFECT_QTY = "batchDefectQty";
    String BATCH_DEFECT_STOCK_TYPE = "batchDefectStockType";
}
