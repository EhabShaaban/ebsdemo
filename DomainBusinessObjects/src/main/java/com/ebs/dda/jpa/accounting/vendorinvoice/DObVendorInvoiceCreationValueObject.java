package com.ebs.dda.jpa.accounting.vendorinvoice;

public class DObVendorInvoiceCreationValueObject {

  private String invoiceType;
  private String purchaseOrderCode;
  private String vendorCode;
  private String invoiceCode;
  private Long documentOwnerId;

  public String getInvoiceType() {
    return invoiceType;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public String getInvoiceCode() {
    return invoiceCode;
  }

  public void setInvoiceCode(String invoiceCode) {
    this.invoiceCode = invoiceCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }
}
