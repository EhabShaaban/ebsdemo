package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobvendorpurchaseunit")
@EntityInterface(IIObVendorPurchaseUnit.class)
public class IObVendorPurchaseUnit extends InformationObject implements Serializable {

  @Column(name = "purchaseunitid")
  private Long purchaseUnitId;

  private String purchaseUnitName;

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public void setPurchaseUnitId(Long purchaseUnitId) {
    this.purchaseUnitId = purchaseUnitId;
  }

  public String getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public void setPurchaseUnitName(String purchaseUnitName) {
    this.purchaseUnitName = purchaseUnitName;
  }

  @Override
  public String getSysName() {
    return IMObVendor.SYS_NAME;
  }
}
