package com.ebs.dda.jpa.masterdata.enterprise;

public interface IIObEnterpriseAddressDetails {

  String SYS_NAME = "AddressDetails";
  String COUNTRY_ATTR_NAME = "countryId";
  String CITY_ATTR_NAME = "cityId";
  String POSTAL_CODE_ATTR_NAME = "postalCode";
  String ADDRESS_LINE_ATTR = "addressLine";
}
