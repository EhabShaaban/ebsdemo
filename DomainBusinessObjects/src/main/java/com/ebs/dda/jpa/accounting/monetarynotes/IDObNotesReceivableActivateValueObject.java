package com.ebs.dda.jpa.accounting.monetarynotes;

public interface IDObNotesReceivableActivateValueObject {

  String NOTES_RECEIVABLE_CODE = "notesReceivableCode";
  String JOURNAL_ENTRY_DATE = "journalEntryDate";
}
