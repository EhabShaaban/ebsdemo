
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.LookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "lobissuefrom")
@EntityInterface(ILObIssueFrom.class)
public class LObIssueFrom extends LookupObject<DefaultUserCode> implements Serializable {

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString name;
    private String telephone;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString address;

    public LocalizedString getName() {
        return name;
    }

    public void setName(LocalizedString name) {
        this.name = name;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public LocalizedString getAddress() {
        return address;
    }

    public void setAddress(LocalizedString address) {
        this.address = address;
    }

    @Override
    public String getSysName() {
        return IMasterDataLookupsSysNames.LOB_REASON;
    }
}
