package com.ebs.dda.jpa.masterdata.vendor;

public interface IIObVendorAccountingInfo {

    String SYS_NAME = "IObVendorAccountingInfo";
    String ACCOUNT_CODE = "accountCode";
    String ACCOUNT_NAME = "accountName";

}
