package com.ebs.dda.jpa.masterdata.vendor;

public interface IMObVendorGeneralDataValueObject {
  String VENDOR_CODE = "vendorCode";
  String VENDOR_NAME = "vendorName";
  String PURCHASE_RESPONSIBLE_CODE = "purchaseResponsibleCode";
  String ACCOUNT_WITH_VENDOR = "accountWithVendor";
  String PURCHASE_UNIT_CODES = "purchaseUnitCodes";
}
