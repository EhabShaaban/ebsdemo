package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.*;
import java.math.BigDecimal;

@MappedSuperclass
@Inheritance(strategy = InheritanceType.JOINED)
public class IObGoodsReceiptItemDetail extends DocumentLine {
  private BigDecimal receivedQtyUoE;
  private Long unitOfEntryId;
  private BigDecimal estimatedCost;
  private BigDecimal actualCost;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "stocktype"))
  private StockTypeEnum stockType;

  private String notes;

  private Long itemVendorRecordId;

  public BigDecimal getReceivedQtyUoE() {
    return receivedQtyUoE;
  }

  public void setReceivedQtyUoE(BigDecimal receivedQtyUoE) {
    this.receivedQtyUoE = receivedQtyUoE;
  }

  public Long getUnitOfEntryId() {
    return unitOfEntryId;
  }

  public void setUnitOfEntryId(Long unitOfEntryId) {
    this.unitOfEntryId = unitOfEntryId;
  }

  public StockTypeEnum getStockType() {
    return stockType;
  }

  public void setStockType(StockTypeEnum stockType) {
    this.stockType = stockType;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String defectDescription) {
    this.notes = defectDescription;
  }

  public Long getItemVendorRecordId() {
    return itemVendorRecordId;
  }

  public void setItemVendorRecordId(Long itemVendorRecordId) {
    this.itemVendorRecordId = itemVendorRecordId;
  }

  public BigDecimal getEstimatedCost() {
    return estimatedCost;
  }

  public void setEstimatedCost(BigDecimal estimatedCost) {
    this.estimatedCost = estimatedCost;
  }

  public BigDecimal getActualCost() {
    return actualCost;
  }

  public void setActualCost(BigDecimal actualCost) {
    this.actualCost = actualCost;
  }
}
