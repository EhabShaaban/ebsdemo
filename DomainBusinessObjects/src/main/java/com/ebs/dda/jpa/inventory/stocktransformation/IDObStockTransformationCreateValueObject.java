package com.ebs.dda.jpa.inventory.stocktransformation;

public interface IDObStockTransformationCreateValueObject {

    String NEW_STOCK_TYPE = "newStockType";

    String TRANSFORMED_QTY = "transformedQty";

    String PURCHASE_UNIT_CODE = "purchaseUnitCode";

    String STORE_TRANSACTION_CODE = "storeTransactionCode";

    String TRANSFORMATION_TYPE_CODE = "transformationTypeCode";

    String TRANSFORMATION_REASON_CODE = "transformationReasonCode";
}
