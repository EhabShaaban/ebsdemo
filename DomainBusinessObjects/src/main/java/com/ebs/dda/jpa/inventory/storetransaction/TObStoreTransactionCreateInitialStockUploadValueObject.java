package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class TObStoreTransactionCreateInitialStockUploadValueObject implements IValueObject {

    private String initialStockUploadCode;
    private Long initialStockUploadId;

    public String getInitialStockUploadCode() {
        return initialStockUploadCode;
    }

    public void setInitialStockUploadCode(String initialStockUploadCode) {
        this.initialStockUploadCode = initialStockUploadCode;
    }

    public Long getInitialStockUploadId() {
        return initialStockUploadId;
    }

    public void setInitialStockUploadId(Long initialStockUploadId) {
        this.initialStockUploadId = initialStockUploadId;
    }
}
