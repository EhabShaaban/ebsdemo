package com.ebs.dda.jpa.accounting.salesinvoice;

public interface ICObSalesInvoice {

  String SYS_NAME = "SalesInvoiceType";
  String CODE = "userCode";
  String NAME = "name";
}
