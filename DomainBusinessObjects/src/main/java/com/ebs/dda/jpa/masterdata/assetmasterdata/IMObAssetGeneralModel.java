package com.ebs.dda.jpa.masterdata.assetmasterdata;

public interface IMObAssetGeneralModel {

  String USER_CODE = "userCode";
  String CURRENT_STATES = "currentStates";
  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";
  String ASSET_NAME = "assetName";
  String ASSET_TYPE_CODE = "assetTypeCode";
  String ASSET_TYPE_NAME = "assetTypeName";
  String BUSINESS_UNIT_CODES = "businessUnitCodes";
  String BUSINESS_UNIT_NAMES = "businessUnitNames";
  String OLD_NUMBER = "oldNumber";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
}
