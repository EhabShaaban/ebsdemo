package com.ebs.dda.jpa.masterdata.assetmasterdata;

public interface ICObAssetType {

  String SYS_NAME = IMObAsset.SYS_NAME;

  String USER_CODE = "userCode";
  String NAME = "name";
}
