package com.ebs.dda.jpa.accounting.collection;

public interface IDObCollectionCreateValueObject {
  String COLLECTION_TYPE = "collectionType";
  String COLLECTION_METHOD = "collectionMethod";
  String REF_DOCUMENT_CODE = "refDocumentCode";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
}
