package com.ebs.dda.jpa.accounting.monetarynotes;

public enum NotesReceivableTypeEnum {
  NOTES_RECEIVABLE_AS_COLLECTION(Values.NOTES_RECEIVABLE_AS_COLLECTION);


  private NotesReceivableTypeEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }
  public static class Values {
    public static final String NOTES_RECEIVABLE_AS_COLLECTION = "NOTES_RECEIVABLE_AS_COLLECTION";
  }
}
