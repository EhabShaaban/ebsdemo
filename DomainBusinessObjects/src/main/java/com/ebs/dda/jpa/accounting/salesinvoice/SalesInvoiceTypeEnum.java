package com.ebs.dda.jpa.accounting.salesinvoice;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.ebs.dac.dbo.types.BusinessEnum;

public class SalesInvoiceTypeEnum extends BusinessEnum {

	public static final String SYS_NAME = "SalesInvoiceType";

	private static final long serialVersionUID = 1L;

	@Enumerated(EnumType.STRING)
	private SalesInvoiceType value;

	public SalesInvoiceTypeEnum(SalesInvoiceType type) {
		this.value = type;
	}

	@Override
	public SalesInvoiceType getId() {
		return this.value;
	}

	public enum SalesInvoiceType {
		SALES_INVOICE_WITHOUT_REFERENCE, SALES_INVOICE_FOR_SALES_ORDER
	}
}
