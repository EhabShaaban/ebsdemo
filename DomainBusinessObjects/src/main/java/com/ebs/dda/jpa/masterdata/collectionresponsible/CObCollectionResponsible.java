package com.ebs.dda.jpa.masterdata.collectionresponsible;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("9")
@EntityInterface(ICObCollectionResponsible.class)
public class CObCollectionResponsible extends CObEnterprise {

    private static final long serialVersionUID = 1L;

    @Override
    public String getSysName() {
        return ICObCollectionResponsible.SYS_NAME;
    }
}
