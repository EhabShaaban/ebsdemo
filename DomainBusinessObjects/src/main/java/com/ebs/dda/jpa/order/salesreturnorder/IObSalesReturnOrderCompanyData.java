package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObSalesReturnOrderCompanyData")
public class IObSalesReturnOrderCompanyData extends DocumentHeader {

  private Long businessUnitId;
  private Long companyId;

  public void setBusinessUnitId(Long businessUnitId) {
    this.businessUnitId = businessUnitId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
