package com.ebs.dda.jpa.accounting.journalentry;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IIObJournalEntryItemGeneralModel {

  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASE_UNIT_NAME_EN);

  String ID = "id";
  String CODE = "code";
  String SUBLEDGER = "subLedger";
  String CREATION_DATE = "creationDate";
  String CREATED_BY = "creationInfo";
  String DUE_DATE = "dueDate";
  String ACCOUNT = "account";
  String ACCOUNT_CODE = "accountCode";
  String ACCOUNT_NAME = "accountName";
  String SUB_ACCOUNT = "subAccount";
  String SUB_ACCOUNT_CODE = "subAccountCode";
  String SUB_ACCOUNT_NAME = "subAccountName";
  String DEBIT = "debit";
  String CREDIT = "credit";
  String DOCUMENT_CURRENCY_CODE = "documentCurrencyCode";
  String DOCUMENT_CURRENCY_NAME = "documentCurrencyName";
  String COMPANY_CURRENCY_CODE = "companyCurrencyCode";
  String COMPANY_CURRENCY_NAME = "companyCurrencyName";
  String COMPANY_GROUP_CURRENCY_CODE = "companyGroupCurrencyCode";
  String COMPANY_GROUP_CURRENCY_NAME = "companyGroupCurrencyName";
  String COMPANY_CURRENCY_PRICE = "companyCurrencyPrice";
  String COMPANY_GROUP_CURRENCY_PRICE = "companyGroupCurrencyPrice";


    String COMPANY_CODE = "companyCode";
    String COMPANY_NAME = "companyName";
}
