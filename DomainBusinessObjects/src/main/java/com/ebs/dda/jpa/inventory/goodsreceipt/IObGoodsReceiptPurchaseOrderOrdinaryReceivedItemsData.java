package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@EntityInterface(IIObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData.class)
@DiscriminatorValue(IIObGoodsReceiptReceivedItemsData.ORDINARY_ITEMS_OBJECT_TYPE_CODE)
public class IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsData
        extends IObGoodsReceiptPurchaseOrderReceivedItemsData {
}
