package com.ebs.dda.jpa.order.salesorder;

public interface IIObSalesOrderTaxGeneralModel {

  String TAX_CODE = "code";
  String TAX_NAME = "name";
  String TAX_AMOUNT = "amount";
}
