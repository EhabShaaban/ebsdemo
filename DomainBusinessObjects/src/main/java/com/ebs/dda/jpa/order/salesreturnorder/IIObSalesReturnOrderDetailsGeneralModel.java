package com.ebs.dda.jpa.order.salesreturnorder;

public interface IIObSalesReturnOrderDetailsGeneralModel {
  String CUSTOMER_CODE = "customerCode";
  String CUSTOMER_NAME = "customerName";
  String PRODUCT_MANAGER_Code = "productManagerCode";
  String PRODUCT_MANAGER_Name = "productManagerName";
  String SALES_INVOICE_CODE = "salesInvoiceCode";
  String CURRENCY_ISO = "currencyIso";

}
