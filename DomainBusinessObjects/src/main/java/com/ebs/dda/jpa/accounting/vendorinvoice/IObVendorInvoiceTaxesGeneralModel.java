package com.ebs.dda.jpa.accounting.vendorinvoice;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dda.jpa.accounting.IObInvoiceTaxesGeneralModel;

@Entity
@Table(name = "IObVendorInvoiceTaxesGeneralModel")
public class IObVendorInvoiceTaxesGeneralModel extends IObInvoiceTaxesGeneralModel {

	@Override
	public String getSysName() {
		return IDObVendorInvoice.SYS_NAME;
	}
}
