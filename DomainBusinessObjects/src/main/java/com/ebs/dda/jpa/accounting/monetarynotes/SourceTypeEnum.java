package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class SourceTypeEnum extends BusinessEnum {

  public static final String SYS_NAME = "Source";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private SourceType value;

  public SourceTypeEnum() {
  }

  @Override
  public SourceType getId() {
    return this.value;
  }

  public void setId(SourceType id) {
    this.value = id;
  }

  public enum SourceType {
    CUSTOMER
  }
}
