package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 4:51:46 PM
 */
@Entity
@DiscriminatorValue("1")
public class LObMaterialView extends LObMaterial {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_MATERIAL_VIEW_SYS_NAME;
  }
}
