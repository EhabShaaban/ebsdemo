package com.ebs.dda.jpa.order.salesorder;

public interface IDObSalesOrderCreateValueObject {
  String TYPE_CODE = "typeCode";
  String BUSINESS_UNIT_CODE = "businessUnitCode";
  String COMPANY_CODE = "companyCode";
  String CUSTOMER_CODE = "customerCode";
  String SALES_RESPONSIBLE_CODE = "salesResponsibleCode";
}
