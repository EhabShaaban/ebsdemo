package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MObItemPurchaseUnitGeneralModel")
public class MObItemPurchaseUnitGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Column(name = "itemcode")
  private String itemCode;

  @Column(name = "purchaseunitcode")
  private String purchaseUnitCode;

  @Column(name = "purchaseunitname")
  private String purchaseUnitName;

  @Override
  public String getSysName() {
    return IMObVendor.SYS_NAME;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }
}