package com.ebs.dda.jpa.accounting.paymentrequest;


import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "IObPaymentRequestPurchaseOrderPaymentDetails")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(DueDocumentTypeEnum.PURCHASEORDER)
public class IObPaymentRequestPurchaseOrderPaymentDetails extends IObPaymentRequestPaymentDetails implements Serializable {

    private Long dueDocumentId;

    public Long getDueDocumentId() {
        return dueDocumentId;
    }

    public void setDueDocumentId(Long dueDocumentId) {
        this.dueDocumentId = dueDocumentId;
    }
}
