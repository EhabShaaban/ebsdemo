package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("NONE")
public class IObJournalEntryItemGeneral extends IObJournalEntryItem {}
