package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 4:59:15 PM
 */
public interface IIObMaterialAllowedView extends IInformationObject {

  String SYSTEM_NAME = "IObMaterialAllowedView";

}
