package com.ebs.dda.jpa.accounting.settlements;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 4, 2021
 */
@Entity
@Table(name = "IObSettlementDetailsGeneralModel")
@EntityInterface(IIObSettlementDetailsGeneralModel.class)
public class IObSettlementDetailsGeneralModel extends DocumentHeader {

  private static final long serialVersionUID = 1L;

  private String settlementCode;
  private String currencyISO;
  private String currencyCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  public String getSettlementCode() {
    return settlementCode;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

}
