package com.ebs.dda.jpa.accounting.paymentrequest;

import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

@Entity
@Table(name = "IObPaymentRequestPaymentDetailsGeneralModel")
@EntityInterface(IIObPaymentRequestPaymentDetailsGeneralModel.class)
public class IObPaymentRequestPaymentDetailsGeneralModel extends InformationObject {

  @Column(name = "code")
  private String userCode;

  private String businessPartnerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  private String dueDocumentType;
  private String dueDocumentCode;
  private BigDecimal netAmount;
  private String currencyISO;
  private String currencyCode;
  private Long companyCurrencyId;
  private String companyCurrencyISO;
  private String companyCurrencyCode;
  private String costFactorItemCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString costFactorItemName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  @Convert(converter = StatefullBusinessObjectCurrentStatesJpaConverter.class)
  private Set<String> currentStates;

  private String description;

  private String paymentForm;

  private Long bankAccountId;

  private String bankAccountCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString bankAccountName;

  private String bankAccountNumber;

  private Long treasuryAccountId;

  private String treasuryCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString treasuryName;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime expectedDueDate;

  private String bankTransRef;

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public LocalizedString getBusinessPartnerName() {
    return businessPartnerName;
  }

  public String getDueDocumentType() {
    return dueDocumentType;
  }

  public String getDueDocumentCode() {
    return dueDocumentCode;
  }

  public BigDecimal getNetAmount() {
    return netAmount;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public String getDescription() {
    return description;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public Set<String> getCurrentStates() {
    return currentStates;
  }

  public void setCurrentStates(Set<String> currentStates) {
    this.currentStates = currentStates;
  }

  public String getCompanyCurrencyISO() {
    return companyCurrencyISO;
  }

  public String getCompanyCurrencyCode() {
    return companyCurrencyCode;
  }

  public Long getCompanyCurrencyId() {
    return companyCurrencyId;
  }

  public String getUserCode() {
    return userCode;
  }

  public String getPaymentForm() {
    return paymentForm;
  }

  public Long getBankAccountId() {
    return bankAccountId;
  }

  public String getBankAccountCode() {
    return bankAccountCode;
  }

  public LocalizedString getBankAccountName() {
    return bankAccountName;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public String getCostFactorItemCode() {
    return costFactorItemCode;
  }

  public LocalizedString getCostFactorItemName() {
    return costFactorItemName;
  }

  public String getTreasuryCode() {
    return treasuryCode;
  }

  public LocalizedString getTreasuryName() {
    return treasuryName;
  }

  public DateTime getExpectedDueDate() {
    return expectedDueDate;
  }

  public String getBankTransRef() {
    return bankTransRef;
  }

  public Long getTreasuryAccountId() { return treasuryAccountId; }

  @Override
  public String getSysName() {
    return IIObPaymentRequestPaymentDetailsGeneralModel.SYS_NAME;
  }
}
