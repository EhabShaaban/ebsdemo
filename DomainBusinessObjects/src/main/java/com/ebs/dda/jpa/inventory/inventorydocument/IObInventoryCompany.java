package com.ebs.dda.jpa.inventory.inventorydocument;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObInventoryCompany")
public class IObInventoryCompany extends DocumentHeader implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long companyId;
  private Long plantId;
  private Long storehouseId;
  private Long storeKeeperId;

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getPlantId() {
    return plantId;
  }

  public void setPlantId(Long plantId) {
    this.plantId = plantId;
  }

  public Long getStorehouseId() {
    return storehouseId;
  }

  public void setStorehouseId(Long storehouseId) {
    this.storehouseId = storehouseId;
  }

  public Long getStoreKeeperId() {
    return storeKeeperId;
  }

  public void setStoreKeeperId(Long storeKeeperId) {
    this.storeKeeperId = storeKeeperId;
  }
}
