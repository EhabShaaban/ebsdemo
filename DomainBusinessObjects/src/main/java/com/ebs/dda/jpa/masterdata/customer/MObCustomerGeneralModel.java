package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringArrayConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "MObCustomerGeneralModel")
@EntityInterface(IMObCustomerGeneralModel.class)
public class MObCustomerGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString marketName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString legalName;

  private String type;

  private String kapCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString kapName;

  private String businessUnitCodes;

  private String oldCustomerNumber;

  @Convert(converter = LocalizedStringArrayConverter.class)
  private List<LocalizedString> businessUnitNames;

  public LocalizedString getMarketName() {
    return marketName;
  }

  public LocalizedString getLegalName() {
    return legalName;
  }

  public String getType() {
    return type;
  }

  public String getKapCode() {
    return kapCode;
  }

  public LocalizedString getKapName() {
    return kapName;
  }

  public String getBusinessUnitCodes() {
    return businessUnitCodes;
  }

  public List<LocalizedString> getBusinessUnitNames() {
    return businessUnitNames;
  }

  public String getOldCustomerNumber() {
    return oldCustomerNumber;
  }

  @Override
  public String getSysName() {
    return IMObCustomer.SYS_NAME;
  }
}
