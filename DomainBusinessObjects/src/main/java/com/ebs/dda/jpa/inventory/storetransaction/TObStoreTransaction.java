package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.TransactionObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "TObStoreTransaction")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class TObStoreTransaction extends TransactionObject<DefaultUserCode>
    implements Serializable {

  private Long itemId;
  private Long unitOfMeasureId;
  private BigDecimal quantity;
  private BigDecimal estimateCost;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "stockType"))
  private StockTypeEnum stockType;

  private BigDecimal actualCost;
  private String batchNo;
  private BigDecimal remainingQuantity;
  private Long refTransactionId;
  private Long companyId;
  private Long plantId;
  private Long storehouseId;
  private Long purchaseUnitId;

  @AttributeOverride(name = "value", column = @Column(name = "transactionOperation"))
  private TransactionTypeEnum transactionOperation;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime originalAddingDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime updatingActualCostDate;

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Long getUnitOfMeasureId() {
    return unitOfMeasureId;
  }

  public void setUnitOfMeasureId(Long unitOfMeasureId) {
    this.unitOfMeasureId = unitOfMeasureId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getEstimateCost() {
    return estimateCost;
  }

  public void setEstimateCost(BigDecimal estimateCost) {
    this.estimateCost = estimateCost;
  }

  public StockTypeEnum getStockType() {
    return stockType;
  }

  public void setStockType(StockTypeEnum stockType) {
    this.stockType = stockType;
  }

  public BigDecimal getActualCost() {
    return actualCost;
  }

  public void setActualCost(BigDecimal actualCost) {
    this.actualCost = actualCost;
  }

  public String getBatchNo() {
    return batchNo;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }

  public BigDecimal getRemainingQuantity() {
    return remainingQuantity;
  }

  public void setRemainingQuantity(BigDecimal remainingQuantity) {
    this.remainingQuantity = remainingQuantity;
  }

  public Long getRefTransactionId() {
    return refTransactionId;
  }

  public void setRefTransactionId(Long refTransactionId) {
    this.refTransactionId = refTransactionId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getPlantId() {
    return plantId;
  }

  public void setPlantId(Long plantId) {
    this.plantId = plantId;
  }

  public Long getStorehouseId() {
    return storehouseId;
  }

  public void setStorehouseId(Long storehouseId) {
    this.storehouseId = storehouseId;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public void setPurchaseUnitId(Long purchaseUnitId) {
    this.purchaseUnitId = purchaseUnitId;
  }

  public TransactionTypeEnum getTransactionOperation() {
    return this.transactionOperation;
  }

  public void setTransactionOperation(TransactionTypeEnum transactionOperation) {
    this.transactionOperation = transactionOperation;
  }

  public DateTime getOriginalAddingDate() {
    return originalAddingDate;
  }

  public void setOriginalAddingDate(DateTime originalAddingDate) {
    this.originalAddingDate = originalAddingDate;
  }

  public DateTime getUpdatingActualCostDate() {
    return updatingActualCostDate;
  }

  public void setUpdatingActualCostDate(DateTime updatingActualCostDate) {
    this.updatingActualCostDate = updatingActualCostDate;
  }

  @Override
  public String getSysName() {
    return ITObStoreTransaction.SYS_NAME;
  }
}
