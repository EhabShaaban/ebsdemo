package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "IObCostingDetails")
public class IObCostingDetails extends DocumentHeader implements Serializable {

  private Long goodsReceiptId;
  private BigDecimal currencyPriceEstimateTime;
  private BigDecimal currencyPriceActualTime;

  public Long getGoodsReceiptId() {
    return goodsReceiptId;
  }

  public void setGoodsReceiptId(Long goodsReceiptId) {
    this.goodsReceiptId = goodsReceiptId;
  }

  public BigDecimal getCurrencyPriceEstimateTime() {
    return currencyPriceEstimateTime;
  }

  public void setCurrencyPriceEstimateTime(BigDecimal currencyPriceEstimateTime) {
    this.currencyPriceEstimateTime = currencyPriceEstimateTime;
  }

  public BigDecimal getCurrencyPriceActualTime() {
    return currencyPriceActualTime;
  }

  public void setCurrencyPriceActualTime(BigDecimal currencyPriceActualTime) {
    this.currencyPriceActualTime = currencyPriceActualTime;
  }

  @Override
  public String getSysName() {
    return "iObCostingDetails";
  }
}
