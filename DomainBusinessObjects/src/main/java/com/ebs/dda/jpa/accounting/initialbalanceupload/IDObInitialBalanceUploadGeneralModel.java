package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObInitialBalanceUploadGeneralModel {
    String USER_CODE = "userCode";
    String CREATION_DATE = "creationDate";
    String DOCUMENT_OWNER = "documentOwner";
    String PURCHASING_UNIT_NAME = "purchaseUnitName";
    String PURCHASE_UNIT_CODE = "purchaseUnitCode";
    String COMAPNY_CODE = "companyCode";
    String COMPANY_NAME = "companyName";
    String FISCAL_YEAR = "fiscalYear";
    String CURRENT_STATES = "currentStates";
    String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
    BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME_EN);
}