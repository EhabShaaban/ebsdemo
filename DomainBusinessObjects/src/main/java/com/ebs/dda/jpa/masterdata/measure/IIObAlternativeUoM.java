package com.ebs.dda.jpa.masterdata.measure;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 27, 2016
 */
public interface IIObAlternativeUoM extends IInformationObject {

  String SYS_NAME = "UC";
}
