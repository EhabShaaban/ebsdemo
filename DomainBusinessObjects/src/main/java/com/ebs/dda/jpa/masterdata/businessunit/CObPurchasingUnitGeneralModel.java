package com.ebs.dda.jpa.masterdata.businessunit;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/** @author xerix on Tue Nov 2017 at 10 : 59 */
@Entity
@Table(name = "cobpurchasingunitgeneralmodel")
@EntityInterface(ICObPurchasingUnit.class)
public class CObPurchasingUnitGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Column(name = "description")
  private String description;

  @Column(name = "name")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private String purchaseUnitName;

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getSysName() {
    return ICObPurchasingUnit.SYS_NAME;
  }

  public String getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public void setPurchaseUnitName(String purchaseUnitName) {
    this.purchaseUnitName = purchaseUnitName;
  }
}
