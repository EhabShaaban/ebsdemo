package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class RefDocumentTypeEnum extends BusinessEnum {

    public static final String SYS_NAME = "RefDocumentType";
    public static final String SALES_INVOICE = "SALES_INVOICE";


    private static final long serialVersionUID = 1L;

    @Enumerated(EnumType.STRING)
    private RefDocumentType value;

    public RefDocumentTypeEnum() {
    }

    @Override
    public RefDocumentType getId() {
        return this.value;
    }

    public void setId(RefDocumentType id) {
        this.value = id;
    }

    public enum RefDocumentType {
        SALES_INVOICE
    }
}
