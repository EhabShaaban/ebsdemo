package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class PaymentTypeEnum extends BusinessEnum {

    public static final String SYS_NAME = "PaymentType";

    private static final long serialVersionUID = 1L;

    @Enumerated(EnumType.STRING)
    private PaymentType value;

    public PaymentTypeEnum() {
    }

    @Override
    public PaymentType getId() {
        return this.value;
    }

    public void setId(PaymentType id) {
        this.value = id;
    }

    public enum PaymentType {
        OTHER_PARTY_FOR_PURCHASE,
        VENDOR,
        UNDER_SETTLEMENT,
        BASED_ON_CREDIT_NOTE
    }
}
