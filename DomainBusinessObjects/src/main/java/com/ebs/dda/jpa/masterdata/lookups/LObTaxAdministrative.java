package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "lobtaxadministrative")
@EntityInterface(ILObTaxAdministrative.class)
public class LObTaxAdministrative extends LocalizedLookupObject<DefaultUserCode> implements Serializable {

  @Override
  public String getSysName() {
    return null;
  }
}
