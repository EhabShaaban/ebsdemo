package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MObItemUOMGeneralModel")
@EntityInterface(IMObItemUOMGeneralModel.class)
public class MObItemUOMGeneralModel extends CodedBusinessObject implements Serializable {

  private String itemCode;
  private String itemType;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString symbol;

  public String getItemCode() {
    return itemCode;
  }

  public String getItemType() {
    return itemType;
  }

  public LocalizedString getSymbol() {
    return symbol;
  }

  @Override
  public String getSysName() {
    return IMObItemUOMGeneralModel.SYS_NAME;
  }
}
