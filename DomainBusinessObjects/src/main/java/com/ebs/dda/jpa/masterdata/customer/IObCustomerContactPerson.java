package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobcustomercontactperson")
public class IObCustomerContactPerson extends InformationObject {

    private static final long serialVersionUID = 1L;

    private Long contactPersonId;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getContactPersonId() {
        return contactPersonId;
    }

    public void setContactPersonId(Long contactPersonId) {
        this.contactPersonId = contactPersonId;
    }

    @Override
    public String getSysName() {
        return "IObCustomerContactPerson";
    }
}
