package com.ebs.dda.jpa.masterdata.geonames;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/** The persistent class for the geonames_countryinfo database table. */
@Entity
@Table(name = "geonames_countryinfo")
public class GeonamesCountryinfo implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "iso_alpha2", unique = true, nullable = false, length = 20)
  private String id;

  private double areainsqkm;

  @Column(length = 2147483647)
  private String capital;

  @Column(length = 20)
  private String continent;

  @Column(length = 2147483647)
  private String country;

  @Column(name = "currency_code", length = 30)
  private String currencyCode;

  @Column(name = "currency_name", length = 150)
  private String currencyName;

  @Column(name = "equivalent_fips_code", length = 2147483647)
  private String equivalentFipsCode;

  @Column(name = "fips_code", length = 2147483647)
  private String fipsCode;

  @Column(name = "iso_alpha3", length = 30)
  private String isoAlpha3;

  @Column(name = "iso_numeric")
  private Integer isoNumeric;

  @Column(length = 2147483647)
  private String languages;

  @Column(length = 2147483647)
  private String neighbours;

  @Column(length = 2147483647)
  private String phone;

  private Integer population;

  @Column(length = 2147483647)
  private String postal;

  @Column(length = 2147483647)
  private String postalregex;

  @Column(length = 100)
  private String tld;

  @Column private Integer geonameid;

  public GeonamesCountryinfo() {}

  public String getId() {
    return this.id;
  }

  public void setId(String isoAlpha2) {
    this.id = isoAlpha2;
  }

  public double getAreainsqkm() {
    return this.areainsqkm;
  }

  public void setAreainsqkm(double areainsqkm) {
    this.areainsqkm = areainsqkm;
  }

  public String getCapital() {
    return this.capital;
  }

  public void setCapital(String capital) {
    this.capital = capital;
  }

  public String getContinent() {
    return this.continent;
  }

  public void setContinent(String continent) {
    this.continent = continent;
  }

  public String getCountry() {
    return this.country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCurrencyCode() {
    return this.currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getCurrencyName() {
    return this.currencyName;
  }

  public void setCurrencyName(String currencyName) {
    this.currencyName = currencyName;
  }

  public String getEquivalentFipsCode() {
    return this.equivalentFipsCode;
  }

  public void setEquivalentFipsCode(String equivalentFipsCode) {
    this.equivalentFipsCode = equivalentFipsCode;
  }

  public String getFipsCode() {
    return this.fipsCode;
  }

  public void setFipsCode(String fipsCode) {
    this.fipsCode = fipsCode;
  }

  public String getIsoAlpha3() {
    return this.isoAlpha3;
  }

  public void setIsoAlpha3(String isoAlpha3) {
    this.isoAlpha3 = isoAlpha3;
  }

  public Integer getIsoNumeric() {
    return this.isoNumeric;
  }

  public void setIsoNumeric(Integer isoNumeric) {
    this.isoNumeric = isoNumeric;
  }

  public String getLanguages() {
    return this.languages;
  }

  public void setLanguages(String languages) {
    this.languages = languages;
  }

  public String getNeighbours() {
    return this.neighbours;
  }

  public void setNeighbours(String neighbours) {
    this.neighbours = neighbours;
  }

  public String getPhone() {
    return this.phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Integer getPopulation() {
    return this.population;
  }

  public void setPopulation(Integer population) {
    this.population = population;
  }

  public String getPostal() {
    return this.postal;
  }

  public void setPostal(String postal) {
    this.postal = postal;
  }

  public String getPostalregex() {
    return this.postalregex;
  }

  public void setPostalregex(String postalregex) {
    this.postalregex = postalregex;
  }

  public String getTld() {
    return this.tld;
  }

  public void setTld(String tld) {
    this.tld = tld;
  }

  public Integer getGeonameid() {
    return geonameid;
  }

  public void setGeonameid(Integer geonameid) {
    this.geonameid = geonameid;
  }
}
