package com.ebs.dda.jpa.accounting;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(value = "UnrealizedCurrencyGainLoss")
public class IObAccountingDocumentUnRealizedExchangeRateAccountingDetails
				extends IObAccountingDocumentAccountingDetails implements Serializable {




}
