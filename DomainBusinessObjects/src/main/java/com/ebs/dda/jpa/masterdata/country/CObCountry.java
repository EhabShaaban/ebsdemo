package com.ebs.dda.jpa.masterdata.country;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dda.jpa.masterdata.geolocale.CObGeoLocale;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 27, 2016 10:19:57 AM
 */
@Entity
@DiscriminatorValue("2")
@Table(name = "CountryDefinition")
public class CObCountry extends CObGeoLocale implements IAggregateAwareEntity, Serializable {

  public static final String SYS_NAME = "Country";

  private static final long serialVersionUID = 1L;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  @Column(name = "dateFormatid")
  private Long dateFormat;

  @Column(name = "decimalFormatid")
  private Long decimalFormat;

  @Column(name = "languageid")
  private Long language;

  @Column(name = "standardTimezoneid")
  private Long standardTimezone;

  @Column(name = "dstTimezoneid")
  private Long dstTimezone;

  @Column(name = "addressFormatid")
  private Long addressFormat;

  private String isoCode;

  public CObCountry() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  public String getIsoCode() {
    return isoCode;
  }

  public void setIsoCode(String isoCode) {
    this.isoCode = isoCode;
  }

  @Override
  public String getSysName() {
    return SYS_NAME;
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }
}
