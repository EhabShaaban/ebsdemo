package com.ebs.dda.jpa.accounting.accountingnotes;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;

@Entity
@DiscriminatorValue("AccountingNote")
public class IObAccountingNoteCompanyGeneralModel extends IObAccountingDocumentCompanyDataGeneralModel {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IDObAccountingNote.SYS_NAME;
  }
}
