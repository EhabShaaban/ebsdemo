package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLDateJpaConverter;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.joda.time.DateTime;

@Entity
@EntityInterface(IIObGoodsReceiptItemBatches.class)
@Table(name = "iobgoodsreceiptitembatches")
public class IObGoodsReceiptItemBatches extends IObGoodsReceiptItemDetail
    implements IIObGoodsReceiptItemBatches {

  private String batchCode;

  @Convert(converter = JodaTimeSQLDateJpaConverter.class)
  private DateTime productionDate;

  @Convert(converter = JodaTimeSQLDateJpaConverter.class)
  private DateTime expirationDate;

  public String getBatchCode() {
    return batchCode;
  }

  public void setBatchCode(String batchCode) {
    this.batchCode = batchCode;
  }

  public DateTime getProductionDate() {
    return productionDate;
  }

  public void setProductionDate(DateTime productionDate) {
    this.productionDate = productionDate;
  }

  public DateTime getExpirationDate() {
    return expirationDate;
  }

  public void setExpirationDate(DateTime expirationDate) {
    this.expirationDate = expirationDate;
  }
}
