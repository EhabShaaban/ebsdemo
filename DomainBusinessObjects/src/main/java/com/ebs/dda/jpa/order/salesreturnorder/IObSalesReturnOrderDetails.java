package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObSalesReturnOrderDetails")
public class IObSalesReturnOrderDetails extends DocumentHeader {

  private Long salesInvoiceId;
  private Long customerId;
  private Long currencyId;

  public void setSalesInvoiceId(Long businessUnitId) {
    this.salesInvoiceId = businessUnitId;
  }

  public void setCustomerId(Long companyId) {
    this.customerId = companyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
