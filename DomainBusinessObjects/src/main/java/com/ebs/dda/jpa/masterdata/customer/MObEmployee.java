package com.ebs.dda.jpa.masterdata.customer;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.buisnesspartner.MObBusinessPartner;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 5, 2021
 */

@Entity
@Table(name = "MObEmployee")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "4")
@EntityInterface(IMObEmployee.class)
public class MObEmployee extends MObBusinessPartner implements IMObEmployee {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return SYS_NAME;
  }
}
