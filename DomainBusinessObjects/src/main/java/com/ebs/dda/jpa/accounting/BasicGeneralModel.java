package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.UserCode;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Set;

@MappedSuperclass
public abstract class BasicGeneralModel<UC extends UserCode>  {

  private static final long serialVersionUID = 7033540286313247494L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "code")
  private String userCode;


  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime creationDate;

  @Convert(converter = StatefullBusinessObjectCurrentStatesJpaConverter.class)
  private Set<String> currentStates;

  public BasicGeneralModel() {}

  public Set<String> getCurrentStates() {
    return currentStates;
  }

  public String getCurrentState() {
    if (!currentStates.isEmpty()) {
      return currentStates.iterator().next();
    }
    return "";
  }

  public Long getId() {
    return id;
  }

  public String getUserCode() {
    return userCode;
  }

  public DateTime getCreationDate() {
    return creationDate;
  }
}
