package com.ebs.dda.jpa.order;

public interface IIObOrderItemValueObject {
  String ITEM_CODE = "itemCode";
  String UNIT_OF_MEASURE_CODE = "unitOfMeasureCode";
  String PRICE = "price";
  String QUANTITY = "quantity";
}
