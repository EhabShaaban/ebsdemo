package com.ebs.dda.jpa.inventory.stocktransformation;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObStockTransformationDetails")
public class IObStockTransformationDetails extends DocumentHeader implements Serializable {

  private String newStockType;

  private Double transformedQty;

  private Long storeTransactionId;

  private Long transformationReasonId;

  private Long newStoreTransactionId;

  public String getNewStockType() {
    return newStockType;
  }

  public void setNewStockType(String newStockType) {
    this.newStockType = newStockType;
  }

  public Double getTransformedQty() {
    return transformedQty;
  }

  public void setTransformedQty(Double transformedQty) {
    this.transformedQty = transformedQty;
  }

  public Long getStoreTransactionId() {
    return storeTransactionId;
  }

  public void setStoreTransactionId(Long storeTransactionId) {
    this.storeTransactionId = storeTransactionId;
  }

  public Long getTransformationReasonId() {
    return transformationReasonId;
  }

  public void setTransformationReasonId(Long transformationReasonId) {
    this.transformationReasonId = transformationReasonId;
  }

  public Long getNewStoreTransactionId() {
    return newStoreTransactionId;
  }

  public void setNewStoreTransactionId(Long newStoreTransactionId) {
    this.newStoreTransactionId = newStoreTransactionId;
  }

  @Override
  public String getSysName() {
    return IIObStockTransformationDetails.SYS_NAME;
  }
}
