package com.ebs.dda.jpa.masterdata.plant;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "CObPlant")
@DiscriminatorValue("3")
@Inheritance(strategy = InheritanceType.JOINED)
@EntityInterface(ICObPlant.class)
public class CObPlant extends CObEnterprise {

  private static final long serialVersionUID = 1L;

  @Column(name = "companyId")
  private Long companyId;

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  @Override
  public String getSysName() {
    return ICObPlant.SYS_NAME;
  }

}
