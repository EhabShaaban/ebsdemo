package com.ebs.dda.jpa.order.salesorder;

public class DObSalesOrderUpdateStateValueObject {

  private String salesOrderCode;

  public String getSalesOrderCode() {
    return salesOrderCode;
  }

  public void setSalesOrderCode(String salesOrderCode) {
    this.salesOrderCode = salesOrderCode;
  }
}
