package com.ebs.dda.jpa.inventory.stocktransformation;

public class DObStockTransformationCreateValueObject {

    private String newStockType;

    private Double transformedQty;

    private String purchaseUnitCode;

    private String storeTransactionCode;

    private String transformationTypeCode;

    private String transformationReasonCode;

    public String getNewStockType() {
        return newStockType;
    }

    public Double getTransformedQty() {
        return transformedQty;
    }

    public String getPurchaseUnitCode() {
        return purchaseUnitCode;
    }

    public String getStoreTransactionCode() {
        return storeTransactionCode;
    }

    public String getTransformationTypeCode() {
        return transformationTypeCode;
    }

    public String getTransformationReasonCode() {
        return transformationReasonCode;
    }
}
