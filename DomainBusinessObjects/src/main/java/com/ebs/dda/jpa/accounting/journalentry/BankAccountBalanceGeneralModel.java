package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "BankAccountBalanceGeneralModel")
public class BankAccountBalanceGeneralModel extends BalanceGeneralModel {

  private String bankAccountNumber;

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }
}
