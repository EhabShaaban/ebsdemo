package com.ebs.dda.jpa.accounting;

public interface IIObAccountingDocumentActivationDetailsGeneralModel {

    String SYS_NAME = "AccountingDocument";

    String CODE = "code";
    String ACTIVATION_DATE = "activationDate";
    String MODIFIED_BY = "modificationInfo";
    String JOURNAL_ENTRY_CODE = "journalEntryCode";
    String CURRENCY_PRICE = "currencyPrice";
    String FIRST_CURRENCY_ISO = "firstCurrencyIso";
    String SECOND_CURRENCY_ISO = "secondCurrencyIso";
    String FIRST_VALUE = "firstValue";
    String EXCHANGE_RATE_CODE = "exchangeRateCode";
    String ACCOUNTANT = "accountant";
    String FISCAL_PERIOD = "fiscalPeriod";
}
