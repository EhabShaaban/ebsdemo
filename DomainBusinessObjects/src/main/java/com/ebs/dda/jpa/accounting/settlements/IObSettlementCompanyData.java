package com.ebs.dda.jpa.accounting.settlements;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

@Entity
@DiscriminatorValue(value = "Settlement")
public class IObSettlementCompanyData extends IObAccountingDocumentCompanyData {
	private static final long serialVersionUID = 1L;

}
