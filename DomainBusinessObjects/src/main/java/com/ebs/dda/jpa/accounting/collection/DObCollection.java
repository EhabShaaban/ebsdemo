package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "DObCollection")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@EntityInterface(IDObCollection.class)
public abstract class DObCollection extends DObAccountingDocument implements Serializable {
  private static final long serialVersionUID = 1L;

  private String collectionType;

  public String getCollectionType() {
    return collectionType;
  }

  public void setCollectionType(String collectionType) {
    this.collectionType = collectionType;
  }

  @Override
  public String getSysName() {
    return IDObCollection.SYS_NAME;
  }
}
