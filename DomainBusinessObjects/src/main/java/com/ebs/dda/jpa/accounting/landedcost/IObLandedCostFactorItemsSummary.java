package com.ebs.dda.jpa.accounting.landedcost;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObLandedCostFactorItemsSummary")
public class IObLandedCostFactorItemsSummary {

    @Id
    private Long id;
    private String code;
    private String companyCurrency;
    private BigDecimal estimateTotalAmount;
    private BigDecimal actualTotalAmount;
    private BigDecimal differenceTotalAmount;

    public String getCode() {
        return code;
    }

    public BigDecimal getEstimateTotalAmount() {
        return estimateTotalAmount;
    }

    public BigDecimal getActualTotalAmount() {
        return actualTotalAmount;
    }

    public BigDecimal getDifferenceTotalAmount() {
        return differenceTotalAmount;
    }

    public Long getId() {
        return id;
    }

    public String getCompanyCurrency() {
        return companyCurrency;
    }
}
