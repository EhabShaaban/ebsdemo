package com.ebs.dda.jpa.accounting;

public interface ICObFiscalYear {
    String SYS_NAME = "FiscalYear";
    String CODE = "userCode";
    String NAME = "name";
}
