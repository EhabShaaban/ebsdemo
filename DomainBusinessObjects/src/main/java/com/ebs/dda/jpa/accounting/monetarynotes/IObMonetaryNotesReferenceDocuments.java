package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "IObMonetaryNotesReferenceDocuments")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "documentType")
public abstract class IObMonetaryNotesReferenceDocuments extends DocumentLine {
  private BigDecimal amountToCollect;

  public BigDecimal getAmountToCollect() {
    return amountToCollect;
  }

  public void setAmountToCollect(BigDecimal amountToCollect) {
    this.amountToCollect = amountToCollect;
  }
}
