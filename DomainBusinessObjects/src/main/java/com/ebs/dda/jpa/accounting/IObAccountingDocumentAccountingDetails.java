package com.ebs.dda.jpa.accounting;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

@Entity
@Table(name = "IObAccountingDocumentAccountingDetails")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "subLedger")
@DiscriminatorValue("NONE")
public class IObAccountingDocumentAccountingDetails extends DocumentLine implements Serializable {

  private String accountingEntry;
  private Long glAccountId;
  @Transient
  private Long glSubAccountId;
  private BigDecimal amount;
  private String objectTypeCode;

  public String getAccountingEntry() {
    return accountingEntry;
  }

  public void setAccountingEntry(String accountingEntry) {
    this.accountingEntry = accountingEntry;
  }

  public Long getGlAccountId() {
    return glAccountId;
  }

  public void setGlAccountId(Long glAccountId) {
    this.glAccountId = glAccountId;
  }

  public Long getGlSubAccountId() {
    return glSubAccountId;
  }

  public void setGlSubAccountId(Long glSubAccountId) {
    this.glSubAccountId = glSubAccountId;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public void setObjectTypeCode(String objectTypeCode) {
    this.objectTypeCode = objectTypeCode;
  }

}
