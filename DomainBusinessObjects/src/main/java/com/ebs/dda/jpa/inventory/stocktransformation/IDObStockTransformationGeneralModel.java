package com.ebs.dda.jpa.inventory.stocktransformation;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObStockTransformationGeneralModel {
  String SYS_NAME = "StockTransformation";
  String STOCK_TRANSFORMATION_CODE = "userCode";
  String STOCK_TRANSFORMATION_TYPE_CODE = "stockTransformationTypeCode";
  String REF_STORE_TRANSACTION_CODE = "refStoreTransactionCode";
  String NEW_STORE_TRANSACTION_CODE = "newStoreTransactionCode";
  String TRANSFORMATION_REASON_CODE = "transformationReasonCode";
  String TRANSFORMATION_REASON_NAME = "transformationReasonName";
  String PURCHASEUNIT_NAME = "purchaseUnitName";
  String PURCHASEUNIT_CODE = "purchaseUnitCode";
  String PURCHASEUNIT_NAME_EN = "purchaseUnitNameEn";
  String CURRENT_STATES = "currentStates";
    String TRANSFORMED_QUANTITY = "transformedQty";

  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASEUNIT_NAME);
}
