package com.ebs.dda.jpa.accounting.salesinvoice;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dda.jpa.accounting.IObInvoiceTaxesGeneralModel;

@Entity
@Table(name = "IObSalesInvoiceTaxesGeneralModel")
public class IObSalesInvoiceTaxesGeneralModel extends IObInvoiceTaxesGeneralModel {
	@Override
	public String getSysName() {
		return IDObSalesInvoice.SYS_NAME;
	}
}
