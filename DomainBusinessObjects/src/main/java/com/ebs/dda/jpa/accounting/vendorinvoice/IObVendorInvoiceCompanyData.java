package com.ebs.dda.jpa.accounting.vendorinvoice;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

@Entity
@DiscriminatorValue(value = "VendorInvoice")
public class IObVendorInvoiceCompanyData extends IObAccountingDocumentCompanyData {

  private static final long serialVersionUID = 1L;

}

