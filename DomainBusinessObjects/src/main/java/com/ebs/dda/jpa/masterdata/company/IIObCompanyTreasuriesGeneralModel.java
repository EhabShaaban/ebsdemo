package com.ebs.dda.jpa.masterdata.company;

public interface IIObCompanyTreasuriesGeneralModel {

	String COMPANY_CODE = "companyCode";
	String COMPANY_NAME = "companyName";

	String TREASURY_CODE = "treasuryCode";
	String TREASURY_NAME = "treasuryName";
}
