package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobcustomerbusinessunit")
public class IObCustomerBusinessUnit extends InformationObject {

    private static final long serialVersionUID = 1L;

    @Column(name = "businessUnitId")
    private Long businessUnit;

    public Long getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(Long businessUnit) {
        this.businessUnit = businessUnit;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Override
    public String getSysName() {
        return "IObCustomerBusinessUnit";
    }
}
