package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "dobcollectionJournalEntry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("Collection")
public class DObCollectionJournalEntry extends DObJournalEntry {

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }
}
