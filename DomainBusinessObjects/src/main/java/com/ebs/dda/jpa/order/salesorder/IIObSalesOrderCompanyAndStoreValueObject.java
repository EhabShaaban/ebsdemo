package com.ebs.dda.jpa.order.salesorder;

public interface IIObSalesOrderCompanyAndStoreValueObject {

  String SALES_ORDER_CODE = "salesOrderCode";
  String STORE_CODE = "storeCode";
}
