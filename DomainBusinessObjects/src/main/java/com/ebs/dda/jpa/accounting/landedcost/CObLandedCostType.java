package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

@Entity
@Table(name = "CObLandedCostType")
public class CObLandedCostType extends ConfigurationObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ICObLandedCostType.SYS_NAME;
  }

}
