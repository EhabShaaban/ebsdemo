package com.ebs.dda.jpa.masterdata.measure;

public class UnitOfMeasureValueObject {

  private String alternativeUnitCode;
  private Float conversionFactor;
  private String oldItemNumber;

  public String getAlternativeUnitCode() {
    return alternativeUnitCode;
  }

  public void setAlternativeUnitCode(String alternativeUnitCode) {
    this.alternativeUnitCode = alternativeUnitCode;
  }

  public Float getConversionFactor() {
    return conversionFactor;
  }

  public void setConversionFactor(Float conversionFactor) {
    this.conversionFactor = conversionFactor;
  }

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  public void setOldItemNumber(String oldItemNumber) {
    this.oldItemNumber = oldItemNumber;
  }
}
