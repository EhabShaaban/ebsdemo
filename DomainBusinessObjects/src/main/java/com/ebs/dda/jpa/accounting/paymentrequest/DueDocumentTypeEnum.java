package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.ArrayList;
import java.util.Arrays;

@Embeddable
public class DueDocumentTypeEnum extends BusinessEnum {

  public static final String SYS_NAME = "DueDocumentType";
  public static final String INVOICE = "INVOICE";
  public static final String PURCHASEORDER = "PURCHASEORDER";
  public static final String NONE = "NONE";
  public static final String CREDITNOTE = "CREDITNOTE";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private DueDocumentType value;

  public DueDocumentTypeEnum() {}

  @Override
  public DueDocumentType getId() {
    return this.value;
  }

  public void setId(DueDocumentType id) {
    this.value = id;
  }

  public enum DueDocumentType {
    INVOICE,
    PURCHASEORDER,
    CREDITNOTE
  }

  public enum VendorPaymentDueDocumentType {
    INVOICE,
    PURCHASEORDER
  }

  public enum GeneralPaymentDueDocumentType {
    PURCHASEORDER
  }

  public enum CreditNotePaymentDueDocumentType {
    CREDITNOTE
  }
}
