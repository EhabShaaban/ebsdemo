package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObGoodsIssueSalesOrder {

    String SYS_NAME = "GoodsIssueSalesOrder";

    String REF_DOCUMENT_ATTR_NAME = "refDocument";
    CompositeAttribute<IObGoodsIssueSalesOrderData> salesOrderRefDocumentAttr =
            new CompositeAttribute<>(REF_DOCUMENT_ATTR_NAME, IObGoodsIssueSalesOrderData.class, false);

    String SO_ITEMS_ATTR_NAME = "salesorderitems";
    CompositeAttribute<IObGoodsIssueSalesOrderItem> salesOrderItemsAttr =
            new CompositeAttribute<>(SO_ITEMS_ATTR_NAME, IObGoodsIssueSalesOrderItem.class, true);
}
