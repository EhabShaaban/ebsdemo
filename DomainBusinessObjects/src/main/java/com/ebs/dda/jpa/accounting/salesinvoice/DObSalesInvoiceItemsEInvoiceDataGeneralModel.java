package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import lombok.Getter;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Entity
@Table(name = "DObSalesInvoiceItemsEInvoiceDataGeneralModel")
public class DObSalesInvoiceItemsEInvoiceDataGeneralModel extends InformationObject {

    private String itemUserCode;
    private String itemName;
    private String itemType;
    private String itemBrickCode;
    private String itemBaseUnit;
    private BigDecimal quantity;

    private String currencySoldIso;
    private BigDecimal amountSold;
    private BigDecimal currencyExchangeRate;

    private BigDecimal salesTotal;
    private BigDecimal valueDifference;
    private BigDecimal totalTaxableFees;
    private BigDecimal itemsDiscount;

    @Override public String getSysName() {
        return null;
    }
}
