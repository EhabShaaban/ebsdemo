package com.ebs.dda.jpa.inventory.goodsissue;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobgoodsissuesalesorderitem")
public class IObGoodsIssueSalesOrderItem extends IObGoodsIssueItem {
}
