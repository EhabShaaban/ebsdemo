package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/**
 * @author: Menna Sayed
 * @date: Jan 11, 2017
 */
public interface IIObMaterialRestriction extends IInformationObject {
  String SYS_NAME = "IObMaterialRestriction";
}
