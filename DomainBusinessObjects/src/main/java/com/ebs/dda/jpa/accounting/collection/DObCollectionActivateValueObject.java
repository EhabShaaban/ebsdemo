package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dda.jpa.IValueObject;

public class DObCollectionActivateValueObject implements IValueObject {

  private String journalEntryDate;
  private String collectionCode;
  private String collectionType;

  public String getCollectionCode() {
    return collectionCode;
  }

  public void setCollectionCode(String collectionCode) {
    this.collectionCode = collectionCode;
  }

  public String getCollectionType() {
    return collectionType;
  }

  public void setCollectionType(String collectionType) {
    this.collectionType = collectionType;
  }

  public String getJournalEntryDate() {
    return journalEntryDate;
  }

  public void setJournalEntryDate(String journalEntryDate) {
    this.journalEntryDate = journalEntryDate;
  }

  @Override
  public void userCode(String userCode) {
    this.collectionCode = userCode;
  }
}
