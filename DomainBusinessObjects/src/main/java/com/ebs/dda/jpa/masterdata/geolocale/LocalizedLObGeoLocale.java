package com.ebs.dda.jpa.masterdata.geolocale;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 28, 2016 11:18:33 AM
 */
@Entity
@Table(name = "LocalizedLObGeoLocale")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class LocalizedLObGeoLocale extends LocalizedLookupObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
