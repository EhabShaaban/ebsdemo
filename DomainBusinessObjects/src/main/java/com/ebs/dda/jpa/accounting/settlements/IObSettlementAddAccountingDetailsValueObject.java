package com.ebs.dda.jpa.accounting.settlements;

import java.math.BigDecimal;

public class IObSettlementAddAccountingDetailsValueObject {

  private String settlementCode;
  private String accountType;
  private String glAccountCode;
  private String glSubAccountCode;
  private BigDecimal amount;

  public String getSettlementCode() {
    return settlementCode;
  }

  public void setSettlementCode(String settlementCode) {
    this.settlementCode = settlementCode;
  }

  public String getAccountType() {
    return accountType;
  }

  public void setAccountType(String accountType) {
    this.accountType = accountType;
  }

  public String getGlAccountCode() {
    return glAccountCode;
  }

  public void setGlAccountCode(String glAccountCode) {
    this.glAccountCode = glAccountCode;
  }

  public String getGlSubAccountCode() {
    return glSubAccountCode;
  }

  public void setGlSubAccountCode(String glSubAccountCode) {
    this.glSubAccountCode = glSubAccountCode;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

}
