package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dda.jpa.inventory.stockavailabilities.IObItemQuantityGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObInitialStockUploadItemQuantityGeneralModel")
public class IObInitialStockUploadItemQuantityGeneralModel extends IObItemQuantityGeneralModel {

  private static final long serialVersionUID = 1L;

  private String stockType;

  public String getStockType() {
    return stockType;
  }

  @Override
  public String getSysName() {
    return "IObInitialStockUploadItemQuantityGeneralModel";
  }
}
