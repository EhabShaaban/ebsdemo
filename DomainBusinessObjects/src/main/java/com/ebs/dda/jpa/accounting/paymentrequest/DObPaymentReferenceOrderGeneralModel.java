package com.ebs.dda.jpa.accounting.paymentrequest;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObPaymentReferenceOrderGeneralModel")
public class DObPaymentReferenceOrderGeneralModel extends DObPaymentReferenceDocumentGeneralModel {
}
