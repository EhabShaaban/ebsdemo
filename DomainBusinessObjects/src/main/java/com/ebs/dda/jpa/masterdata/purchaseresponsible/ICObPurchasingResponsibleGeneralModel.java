package com.ebs.dda.jpa.masterdata.purchaseresponsible;

public interface ICObPurchasingResponsibleGeneralModel {

  String CODE = "userCode";
  String NAME = "name";
  String STATE = "currentStates";
}
