package com.ebs.dda.jpa.accounting.landedcost;

public interface IIObLandedCostFactorItemsSummaryGeneralModel {

    final String CODE = "code";
    final String ESTIMATE_TOTAL_AMOUNT = "estimateTotalAmount";
    final String ACTUAL_TOTAL_AMOUNT = "actualTotalAmount";
    final String DIFFERENCE_TOTAL_AMOUNT = "differenceTotalAmount";
    final String COMPANY_CURRENCY = "companyCurrency";


}
