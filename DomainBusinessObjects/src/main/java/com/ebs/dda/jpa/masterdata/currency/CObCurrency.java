package com.ebs.dda.jpa.masterdata.currency;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import java.io.Serializable;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObCurrency")
@EntityInterface(ICObCurrency.class)
public class CObCurrency extends ConfigurationObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  private String iso;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString subUnit;

  private short decimalPlaces;

  public String getIso() {
    return iso;
  }

  public void setIso(String iso) {
    this.iso = iso;
  }

  public LocalizedString getSubUnit() {
    return subUnit;
  }

  public void setSubUnit(LocalizedString subUnit) {
    this.subUnit = subUnit;
  }

  public short getDecimalPlaces() {
    return decimalPlaces;
  }

  public void setDecimalPlaces(short decimalPlaces) {
    this.decimalPlaces = decimalPlaces;
  }

  @Override
  public String getSysName() {
    return ICObCurrency.SYS_NAME;
  }
}
