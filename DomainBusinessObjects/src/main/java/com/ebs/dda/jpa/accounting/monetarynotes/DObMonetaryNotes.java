package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "DObMonetaryNotes")
@EntityInterface(IDObMonetaryNotes.class)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class DObMonetaryNotes extends DObAccountingDocument {

  @Override
  public String getSysName() {
    return null;
  }

}
