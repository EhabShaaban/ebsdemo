package com.ebs.dda.jpa.masterdata.chartofaccount;

import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountType;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;

public class HObChartOfAccountCreateValueObject {

  private String accountName;
  private String level;
  private String oldAccountCode;
  private String parentCode;
  private AccountType accountType;
  private AccountingEntry accountingEntry;
  private String mappedAccount;

  public String getLevel() {
    return level;
  }

  public String getParentCode() {
    return parentCode;
  }

  public AccountType getAccountType() {
    return accountType;
  }

  public AccountingEntry getAccountingEntry() {
    return accountingEntry;
  }

  public String getAccountName() {
    return accountName;
  }

  public String getOldAccountCode() {
    return oldAccountCode;
  }

  public String getMappedAccount() {
    return mappedAccount;
  }
}
