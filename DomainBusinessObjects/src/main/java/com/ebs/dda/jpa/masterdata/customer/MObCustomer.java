package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.buisnesspartner.MObBusinessPartner;

import javax.persistence.Convert;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "mobcustomer")
@DiscriminatorValue(value = "3")
@EntityInterface(IMObCustomer.class)
public class MObCustomer extends MObBusinessPartner {

  private Long kapId;
  private Double creditLimit;
  private Long currencyId;
  private String type;
  private String oldCustomerNumber;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString marketName;

  public void setKapId(Long kapId) {
    this.kapId = kapId;
  }

  public void setCreditLimit(Double creditLimit) {
    this.creditLimit = creditLimit;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setMarketName(LocalizedString marketName) {
    this.marketName = marketName;
  }

  public void setOldCustomerNumber(String oldCustomerNumber) {
    this.oldCustomerNumber = oldCustomerNumber;
  }

  @Override
  public String getSysName() {
    return IMObCustomer.SYS_NAME;
  }
}
