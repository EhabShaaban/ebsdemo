package com.ebs.dda.jpa.masterdata.productmanager;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("10")
@EntityInterface(ICObProductManager.class)
public class CObProductManager extends CObEnterprise {
  @Override
  public String getSysName() {
    return ICObProductManager.SYS_NAME;
  }
}
