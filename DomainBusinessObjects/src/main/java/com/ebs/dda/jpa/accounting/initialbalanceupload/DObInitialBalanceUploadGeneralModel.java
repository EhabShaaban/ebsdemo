package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObInitialBalanceUploadGeneralModel")
@EntityInterface(IDObInitialBalanceUploadGeneralModel.class)
public class DObInitialBalanceUploadGeneralModel extends StatefullBusinessObject<DefaultUserCode> {
  private static final long serialVersionUID = 1L;
  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String documentOwner;

  private String fiscalYear;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  public String getFiscalYear() {
    return fiscalYear;
  }

  @Override
  public String getSysName() {
    return IDObInitialBalanceUpload.SYS_NAME;
  }
}
