package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author: Menna Sayed
 * @date: Jan 11, 2017
 */
@Entity
@Table(name = "CObMaterial")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class CObMaterial extends ConfigurationObject<DefaultUserCode>
    implements IAggregateAwareEntity, Serializable {

  private static final long serialVersionUID = 1L;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  public CObMaterial() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }
}
