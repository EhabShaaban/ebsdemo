package com.ebs.dda.jpa.accounting.settlements;

public class DObSettlementCreationValueObject {
	private String type;
	private String businessUnitCode;
	private String companyCode;
	private String currencyCode;
	private Long documentOwnerId;

	public String getType() {
		return type;
	}

	public String getBusinessUnitCode() {
		return businessUnitCode;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public Long getDocumentOwnerId() {
		return documentOwnerId;
	}
}
