package com.ebs.dda.jpa.masterdata.chartofaccount;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "HObGLAccountGeneralModel")
public class HObChartOfAccountsGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString accountName;

  @Column(name = "type")
  private String accountType;

  private String level;

  private Long parentId;

  @Column(name = "creditdebit")
  private String creditDebit;

    private String leadger;

  @Column(name = "parentCode")
  private String parentCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "parentName")
  private LocalizedString parentName;

  private String parentCodeName;

  private String enLevel;

  private String arLevel;

  private String mappedAccount;

  private String oldAccountCode;

  public LocalizedString getAccountName() {
    return accountName;
  }

  public String getAccountType() {
    return accountType;
  }

  public String getAccountTypeEn() {
    return accountType;
  }

  public String getLevel() {
    return level;
  }

  public Long getParentId() {
    return parentId;
  }

  public String getCreditDebit() {
    return creditDebit;
  }

  public String getCreditDebitEn() {
    return creditDebit;
  }

    public String getLeadger() {
        return leadger;
    }

  public String getParentCode() {
    return parentCode;
  }

  public LocalizedString getParentName() {
    return parentName;
  }

  public String getParentCodeName() {
    return parentCodeName;
  }

  public String getEnLevel() {
    return enLevel;
  }

  public String getArLevel() {
    return arLevel;
  }

  public String getMappedAccount() {
    return mappedAccount;
  }

  public String getOldAccountCode() {
    return oldAccountCode;
  }

  @Override
  public String getSysName() {
    return IHObChartOfAccounts.SYS_NAME;
  }
}
