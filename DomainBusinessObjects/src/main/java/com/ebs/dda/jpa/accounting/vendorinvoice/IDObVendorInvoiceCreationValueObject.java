package com.ebs.dda.jpa.accounting.vendorinvoice;

public interface IDObVendorInvoiceCreationValueObject {

  public static final String PURCHESE_ORDER_CODE = "purchaseOrderCode";
  public static final String VENDOR_CODE = "vendorCode";
  public static final String INVOICE_TYPE = "invoiceType";
  public static final String DOCUMENT_OWNER = "documentOwnerId";


}
