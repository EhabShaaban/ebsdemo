package com.ebs.dda.jpa.masterdata.exchangerate;

/**
 * @author Ahmed Ali Elfeky
 * @since Dec 27, 2020
 */
public interface ICObExchangeRateType {

  static final String SYS_NAME = "CObExchangeRateType";

  static final String USER_DAILY_RATE = "USER_DAILY_RATE";
  static final String CENTRAL_BANK_OF_EGYPT = "CENTRAL_BANK_OF_EGYPT";

}
