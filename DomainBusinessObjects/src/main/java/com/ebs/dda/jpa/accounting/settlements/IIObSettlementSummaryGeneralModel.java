package com.ebs.dda.jpa.accounting.settlements;

public interface IIObSettlementSummaryGeneralModel {

    String SETTLEMENT_CODE = "settlementCode";
    String TOTAL_DEBIT = "totalDebit";
    String TOTAL_CREDIT = "totalCredit";
}
