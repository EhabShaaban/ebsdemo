package com.ebs.dda.jpa.accounting.salesinvoice;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "DObSalesInvoicePostPreparationGeneralModel")
public class DObSalesInvoicePostPreparationGeneralModel {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String code;

	private Long exchangeRateId;
	private BigDecimal currencyPrice;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public Long getExchangeRateId() {
		return exchangeRateId;
	}

	public BigDecimal getCurrencyPrice() {
		return currencyPrice;
	}
}
