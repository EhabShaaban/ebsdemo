package com.ebs.dda.jpa.accounting.salesinvoice;

public class DObSalesInvoiceReadyForDeliveringValueObject {

  private String salesInvoiceCode;

  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public void setSalesInvoiceCode(String salesInvoiceCode) {
    this.salesInvoiceCode = salesInvoiceCode;
  }

}
