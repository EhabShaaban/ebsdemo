package com.ebs.dda.jpa.accounting.initialbalanceupload;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel")
public class DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel {
	@Id
	private Long id;
	private String code;
	private Double debit;
	private Double credit;
	private BigDecimal currencyPrice;
	private Long glAccountId;
	private Long currencyId;
	private String subledger;
	private Long vendorId;

	public String getCode() {
		return code;
	}

	public Double getDebit() {
		return debit;
	}

	public Double getCredit() {
		return credit;
	}

	public BigDecimal getCurrencyPrice() {
		return currencyPrice;
	}

	public Long getGlAccountId() {
		return glAccountId;
	}

	public Long getCurrencyId() {
		return currencyId;
	}

	public String getSubledger() {
		return subledger;
	}

	public Long getVendorId() {
		return vendorId;
	}
}
