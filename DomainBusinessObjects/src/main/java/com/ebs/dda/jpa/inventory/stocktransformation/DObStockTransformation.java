package com.ebs.dda.jpa.inventory.stocktransformation;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.inventory.inventorydocument.DObInventoryDocument;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DObStockTransformation")
@EntityInterface(IDObStockTransformation.class)
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = IDObStockTransformation.STOCK_TRANSFORMATION_KEY)
public class DObStockTransformation extends DObInventoryDocument implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long transformationTypeId;

  public Long getTransformationTypeId() {
    return transformationTypeId;
  }

  public void setTransformationTypeId(Long transformationTypeId) {
    this.transformationTypeId = transformationTypeId;
  }

  @Override
  public String getSysName() {
    return IDObStockTransformation.SYS_NAME;
  }
}
