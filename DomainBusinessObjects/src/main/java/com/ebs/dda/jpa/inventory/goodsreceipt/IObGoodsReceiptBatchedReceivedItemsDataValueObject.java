package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dda.jpa.inventory.goodsreceipt.StockTypeEnum.StockType;

import java.math.BigDecimal;
import java.util.List;

public class IObGoodsReceiptBatchedReceivedItemsDataValueObject {

  private String goodsReceiptCode;
  private String itemCode;
  private StockType stockType;
  private BigDecimal deliveryQuantity;
  private List<IObGoodsReceiptItemBatchValueObject> batches;
}
