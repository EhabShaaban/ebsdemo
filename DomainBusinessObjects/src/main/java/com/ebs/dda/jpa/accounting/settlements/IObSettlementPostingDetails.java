package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Settlement")
public class IObSettlementPostingDetails extends IObAccountingDocumentActivationDetails {
    @Override
    public String getSysName() {
        return IDObVendorInvoice.SYS_NAME;
    }
}
