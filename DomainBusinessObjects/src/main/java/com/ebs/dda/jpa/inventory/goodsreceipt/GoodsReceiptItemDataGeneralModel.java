package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "goodsreceiptitemdatageneralmodel")
public class GoodsReceiptItemDataGeneralModel {

  @Id
  @Column(name = "id")
  private Long id;

  @Column(name = "itemcode")
  private String itemCode;

  @Column(name = "itemname")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;

  @Column(name = "vendorcode")
  private String vendorCode;

  @Column(name = "batched")
  private String isBatched;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  @Column(name = "baseunit")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString baseUnit;

  @Column(name = "baseUnitCode")
  private String baseUnitCode;

  public String getItemCode() {
    return itemCode;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getIsBatched() {
    return isBatched;
  }
}
