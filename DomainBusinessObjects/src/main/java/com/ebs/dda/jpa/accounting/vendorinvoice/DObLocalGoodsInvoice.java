package com.ebs.dda.jpa.accounting.vendorinvoice;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE)
public class DObLocalGoodsInvoice extends DObVendorInvoice implements Serializable {

}
