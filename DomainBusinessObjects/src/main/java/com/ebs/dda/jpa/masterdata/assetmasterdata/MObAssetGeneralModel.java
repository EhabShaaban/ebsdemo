package com.ebs.dda.jpa.masterdata.assetmasterdata;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringArrayConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "MObAssetGeneralModel")
@EntityInterface(IMObAssetGeneralModel.class)
public class MObAssetGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString assetName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString assetTypeName;

  private String assetTypeCode;

  private String businessUnitCodes;

  private String oldNumber;

  @Convert(converter = LocalizedStringArrayConverter.class)
  private List<LocalizedString> businessUnitNames;

  public LocalizedString getAssetName() {
    return assetName;
  }

  public LocalizedString getAssetTypeName() {
    return assetTypeName;
  }

  public String getAssetTypeCode() {
    return assetTypeCode;
  }

  public String getBusinessUnitCodes() {
    return businessUnitCodes;
  }

  public String getOldNumber() {
    return oldNumber;
  }

  public List<LocalizedString> getBusinessUnitNames() {
    return businessUnitNames;
  }

  @Override
  public String getSysName() {
    return IMObAsset.SYS_NAME;
  }
}
