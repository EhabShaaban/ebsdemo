package com.ebs.dda.jpa.masterdata.storehouse;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObStorehouseGeneralModel")
public class CObStorehouseGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "plantName")
  private LocalizedString plantName;

  @Column(name = "plantId")
  private Long plantId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private String plantCode;

  public LocalizedString getPlantName() {
    return plantName;
  }

  public void setPlantName(LocalizedString plantName) {
    this.plantName = plantName;
  }

  public Long getPlantId() {
    return plantId;
  }

  public void setPlantId(Long plantId) {
    this.plantId = plantId;
  }

  @Override
  public String getSysName() {
    return ICObStorehouse.SYS_NAME;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public void setPlantCode(String plantCode) {
    this.plantCode = plantCode;
  }
}
