/** */
package com.ebs.dda.jpa.masterdata.country;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dda.jpa.masterdata.geolocale.CObGeoLocale;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 27, 2016 10:06:17 AM
 */
@Entity
@DiscriminatorValue("1")
@Table(name = "SubdivisionDefinition")
@EntityInterface(ICObSubdivision.class)
public class CObSubdivision extends CObGeoLocale implements IAggregateAwareEntity, Serializable {

  private static final long serialVersionUID = 1L;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  @Column(name = "countryid")
  private Long country;

  @Column(name = "categoryid")
  private Long category;

  private Integer level;

  @Column(name = "parentid")
  private Long subdivisionParent;

  @Column(name = "languageid")
  private Long language;

  @Column(name = "standardTimezoneid")
  private Long standardTimezone;

  @Column(name = "dstTimezoneid")
  private Long dstTimezone;

  public CObSubdivision() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }


  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  @Override
  public String getSysName() {
    return ICObSubdivision.SYS_NAME;
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }
}
