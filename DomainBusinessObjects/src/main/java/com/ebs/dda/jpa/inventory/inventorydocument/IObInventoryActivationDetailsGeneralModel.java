package com.ebs.dda.jpa.inventory.inventorydocument;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObInventoryActivationDetailsGeneralModelGeneralModel")
public class IObInventoryActivationDetailsGeneralModel extends InformationObject {

  private String inventoryDocumentCode;

  private String activatedBy;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime activationDate;

  public String getActivatedBy() {
    return activatedBy;
  }

  public DateTime getActivationDate() {
    return activationDate;
  }

  public String getInventoryDocumentCode() {
    return inventoryDocumentCode;
  }

  @Override
  public String getSysName() {
    return IDObVendorInvoice.SYS_NAME;
  }
}
