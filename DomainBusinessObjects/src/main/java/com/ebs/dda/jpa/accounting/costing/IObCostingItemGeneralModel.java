package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;

@Entity
@Table(name = "IObCostingItemsGeneralModel")
public class IObCostingItemGeneralModel extends InformationObject {

  private String itemCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;
  private String userCode;
  private String uomCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString uomSymbol;
  private BigDecimal quantity;
  private String stockType;
  private BigDecimal toBeConsideredInCostingPer;
  private BigDecimal estimateUnitPrice;
  private BigDecimal actualUnitPrice;
  private BigDecimal estimateUnitLandedCost;
  private BigDecimal actualUnitLandedCost;
  @Transient private BigDecimal estimateItemTotalCost;
  @Transient private BigDecimal actualItemTotalCost;

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getUomCode() {
    return uomCode;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public String getStockType() {
    return stockType;
  }

  public BigDecimal getToBeConsideredInCostingPer() {
    return toBeConsideredInCostingPer;
  }

  public String getUserCode() {
    return userCode;
  }

  public LocalizedString getUomSymbol() {
    return uomSymbol;
  }

  public BigDecimal getEstimateUnitPrice() {
    return estimateUnitPrice;
  }

  public BigDecimal getActualUnitPrice() {
    return actualUnitPrice;
  }

  public BigDecimal getEstimateUnitLandedCost() {
    return estimateUnitLandedCost;
  }

  public BigDecimal getActualUnitLandedCost() {
    return actualUnitLandedCost;
  }

  public BigDecimal getEstimateItemTotalCost() {
    return estimateItemTotalCost;
  }

  public void setEstimateItemTotalCost(BigDecimal estimateItemTotalCost) {
    this.estimateItemTotalCost = estimateItemTotalCost;
  }

  public BigDecimal getActualItemTotalCost() {
    return actualItemTotalCost;
  }

  public void setActualItemTotalCost(BigDecimal actualItemTotalCost) {
    this.actualItemTotalCost = actualItemTotalCost;
  }

  @Override
  public String getSysName() {
    return "IObCostingItemGeneralModel";
  }
}
