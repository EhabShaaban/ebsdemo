package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

public interface IDObAccountingNote {

  String SYS_NAME = "AccountingNote";
  String COMPANY = "company";
  String DETAILS = "details";
  CompositeAttribute<IObAccountingDocumentCompanyData> companyAttr =
      new CompositeAttribute<>(COMPANY, IObAccountingDocumentCompanyData.class, false);
  CompositeAttribute<IObAccountingNoteDetails> detailsAttr =
      new CompositeAttribute<>(DETAILS, IObAccountingNoteDetails.class, false);
}
