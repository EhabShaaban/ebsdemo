package com.ebs.dda.jpa.masterdata.item;

import java.util.List;

public class MObItemGeneralDataValueObject {
  private String itemCode;

  private String itemName;

  private List<String> purchasingUnitCodes;

  private String marketName;

  private String productManagerCode;

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }

  public List<String> getPurchasingUnitCodes() {
    return purchasingUnitCodes;
  }

  public String getMarketName() {
    return marketName;
  }

  public void setPurchasingUnitCodes(List<String> purchasingUnitCodes) {
    this.purchasingUnitCodes = purchasingUnitCodes;
  }

  public String getProductManagerCode() {
    return productManagerCode;
  }
}
