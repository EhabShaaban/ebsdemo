package com.ebs.dda.jpa.accounting.monetarynotes;

public interface IIObMonetaryNotesReferenceDocumentsValueObject {

  String NOTES_RECEIVABLE_CODE = "notesReceivableCode";
  String DOCUMENT_TYPE = "documentType";
  String AMOUNT_TO_COLLECT = "amountToCollect";
  String REF_DOCUMENT_CODE = "refDocumentCode";
}
