package com.ebs.dda.jpa.masterdata.assetmasterdata;


import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "iobassetbusinessunit")
public class IObAssetBusinessUnit extends InformationObject implements Serializable {

  private Long businessUnitId;

  public Long getBusinessUnitId() {
    return businessUnitId;
  }

  public void setBusinessUnitId(Long businessUnitId) {
    this.businessUnitId = businessUnitId;
  }

  @Override
  public String getSysName() {
    return IMObAsset.SYS_NAME;
  }
}
