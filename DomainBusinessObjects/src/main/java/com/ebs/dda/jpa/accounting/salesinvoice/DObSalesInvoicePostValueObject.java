package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dda.jpa.IValueObject;

import java.math.BigDecimal;

public class DObSalesInvoicePostValueObject implements IValueObject {

  private String salesInvoiceCode;
  private String journalEntryDate;
  private BigDecimal currencyPrice;


  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public void setSalesInvoiceCode(String salesInvoiceCode) {
    this.salesInvoiceCode = salesInvoiceCode;
  }

  public String getJournalEntryDate() {
    return journalEntryDate;
  }

  public void setJournalEntryDate(String journalEntryDate) {
    this.journalEntryDate = journalEntryDate;
  }

  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }

  public void setCurrencyPrice(BigDecimal currencyPrice) {
    this.currencyPrice = currencyPrice;
  }

  @Override
  public void userCode(String userCode) {
    this.salesInvoiceCode = userCode;
  }
}
