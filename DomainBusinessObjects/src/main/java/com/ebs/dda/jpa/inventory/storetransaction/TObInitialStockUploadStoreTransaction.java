package com.ebs.dda.jpa.inventory.storetransaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TObInitialStockUploadStoreTransaction")
@DiscriminatorValue("ISU")
public class TObInitialStockUploadStoreTransaction extends TObStoreTransaction {

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }
}
