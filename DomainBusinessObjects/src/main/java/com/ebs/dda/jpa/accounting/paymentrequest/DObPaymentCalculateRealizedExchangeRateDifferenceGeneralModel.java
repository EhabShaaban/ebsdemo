package com.ebs.dda.jpa.accounting.paymentrequest;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel")
public class DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel implements Serializable {
    @Id
    private Long id;
    private String paymentCode;
    private BigDecimal paymentAmount;
    private Long localCurrencyId;
    private Long glAccountId;
    private Long glSubAccountId;
    private String subLedger;
    private BigDecimal paymentCurrencyPrice;
    private BigDecimal vendorInvoiceCurrencyPrice;


    public Long getId() {
        return id;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public Long getLocalCurrencyId() {
        return localCurrencyId;
    }

    public Long getGlAccountId() {
        return glAccountId;
    }

    public Long getGlSubAccountId() {
        return glSubAccountId;
    }

    public String getSubLedger() {
        return subLedger;
    }

    public BigDecimal getPaymentCurrencyPrice() {
        return paymentCurrencyPrice;
    }

    public BigDecimal getVendorInvoiceCurrencyPrice() {
        return vendorInvoiceCurrencyPrice;
    }
}
