package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.joda.time.DateTime;

@Entity
@Table(name = "IObVendorInvoiceDetailsGeneralModel")
@EntityInterface(IIObVendorInvoiceDetailsGeneralModel.class)
public class IObVendorInvoiceDetailsGeneralModel extends DocumentHeader {

	private String invoiceCode;
	private String invoiceNumber;
	private String purchaseOrderCode;
	private String vendorCode;
	private String vendorName;
	private String currencyISO;
	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString paymentTerm;
	private String downPayment;
	private String purchaseUnitCode;
	private String purchaseUnitNameEn;
	private String currencyCode;
	private String paymentTermCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString currencyName;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString purchaseUnitName;

	@Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
	@Column(updatable = false)
	private DateTime invoiceDate;

	public String getVendorCode() {
		return vendorCode;
	}

	public String getVendorName() {
		return vendorName;
	}

	public String getCurrencyISO() {
		return currencyISO;
	}

	public LocalizedString getCurrencyName() {
		return currencyName;
	}

	public LocalizedString getPaymentTerm() {
		return paymentTerm;
	}

	public String getPurchaseOrderCode() {
		return purchaseOrderCode;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public DateTime getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(DateTime invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getDownPayment() {
		return downPayment;
	}

	public String getInvoiceCode() {
		return invoiceCode;
	}

	public String getPurchaseUnitCode() {
		return purchaseUnitCode;
	}

	public String getPurchaseUnitNameEn() {
		return purchaseUnitNameEn;
	}

	public LocalizedString getPurchaseUnitName() {
		return purchaseUnitName;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}

	public String getPaymentTermCode() {
		return paymentTermCode;
	}

	@Override
	public String getSysName() {
		return IDObVendorInvoice.SYS_NAME;
	}

}
