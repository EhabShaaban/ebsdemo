package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dda.jpa.IValueObject;

public class DObVendorInvoiceActivateValueObject implements IValueObject {

  private String vendorInvoiceCode;
  private String journalDate;

  public String getJournalDate() {
    return journalDate;
  }

  public void setVendorInvoiceCode(String vendorInvoiceCode) {
    this.vendorInvoiceCode = vendorInvoiceCode;
  }

  public void setJournalDate(String journalDate) { this.journalDate = journalDate; }

  public String getVendorInvoiceCode() {
    return vendorInvoiceCode;
  }

  @Override
  public void userCode(String userCode) {
    this.vendorInvoiceCode = userCode;
  }
}
