package com.ebs.dda.jpa.order.salesorder;

public interface ICObSalesOrderType {

  String SYS_NAME = "SalesOrderType";

  String USER_CODE = "userCode";
  String NAME = "name";
}
