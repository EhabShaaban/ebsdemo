package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import java.math.BigDecimal;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 18, 2021
 */
@Entity
@Table(name = "IObLandedCostFactorItemsDetailsGeneralModel")
public class IObLandedCostFactorItemsDetailsGeneralModel extends InformationObject {

  private static final long serialVersionUID = 1L;

  private BigDecimal actualValue;
  private String documentInfoCode;
  private String documentInfoType;

  public BigDecimal getActualValue() {
    return actualValue;
  }

  public String getDocumentInfoCode() {
    return documentInfoCode;
  }

  public String getDocumentInfoType() {
    return documentInfoType;
  }

  @Override
  public String getSysName() {
    return IDObLandedCost.SYS_NAME;
  }

}
