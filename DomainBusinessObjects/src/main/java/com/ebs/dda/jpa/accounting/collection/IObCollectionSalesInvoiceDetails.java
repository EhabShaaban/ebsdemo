package com.ebs.dda.jpa.accounting.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObCollectionSalesInvoiceDetails")
@DiscriminatorValue("1")
public class IObCollectionSalesInvoiceDetails extends IObCollectionDetails
    implements Serializable {
  private Long salesInvoiceId;

  public Long getSalesInvoiceId() {
    return salesInvoiceId;
  }

  public void setSalesInvoiceId(Long salesInvoiceId) {
    this.salesInvoiceId = salesInvoiceId;
  }
}
