package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "DObCostingJournalEntry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("Costing")
public class DObCostingJournalEntry extends DObJournalEntry {

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }

  public Long getDocumentReferenceId() {
    return documentReferenceId;
  }
}
