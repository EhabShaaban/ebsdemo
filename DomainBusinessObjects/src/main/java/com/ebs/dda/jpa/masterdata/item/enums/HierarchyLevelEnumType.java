package com.ebs.dda.jpa.masterdata.item.enums;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class HierarchyLevelEnumType extends BusinessEnum {

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  @Column(name = "level")
  private HierarchyLevelEnum value; // this is the material group level (Root or

  public HierarchyLevelEnum getId() {
    return value;
  }
  // leaf ...)

  public void setId(HierarchyLevelEnum level) {
    this.value = level;
  }

  public enum HierarchyLevelEnum {
    ROOT,
    HIERARCHY,
    LEAF
  }
}
