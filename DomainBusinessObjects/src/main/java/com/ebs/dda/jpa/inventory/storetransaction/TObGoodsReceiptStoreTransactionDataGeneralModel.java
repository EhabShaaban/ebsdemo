package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.TransactionObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TObGoodsReceiptStoreTransactionDataGeneralModel")
@EntityInterface(ITObGoodsReceiptStoreTransactionDataGeneralModel.class)
public class TObGoodsReceiptStoreTransactionDataGeneralModel
    extends TransactionObject<DefaultUserCode> {

  private Long storehouseId;
  private Long plantId;
  private Long companyId;
  private Long purchaseUnitId;
  private Long itemId;
  private Long goodsReceiptId;
  private BigDecimal estimatedCostQty;
  private BigDecimal actualCostQty;
  private BigDecimal estimatedCostBatch;
  private BigDecimal actualCostBatch;
  private BigDecimal receivedQty;
  private Long receivedUoeId;
  private String receivedUoeCode;
  private String receivedStockType;
  private String batchNo;
  private BigDecimal batchQty;
  private Long batchUoeId;
  private String batchStockType;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString baseUnitSymbol;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;

  private String itemCode;
  private BigDecimal receivedQtyBase;
  private BigDecimal receivedBatchQtyBase;

  public LocalizedString getBaseUnitSymbol() {
    return baseUnitSymbol;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getItemCode() {
    return itemCode;
  }

  public BigDecimal getReceivedQtyBase() {
    return receivedQtyBase;
  }

  public BigDecimal getReceivedBatchQtyBase() {
    return receivedBatchQtyBase;
  }

  public Long getGoodsReceiptId() {
    return goodsReceiptId;
  }

  public Long getStorehouseId() {
    return storehouseId;
  }

  public Long getPlantId() {
    return plantId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public Long getItemId() {
    return itemId;
  }

  public BigDecimal getEstimatedCostQty() {
    return estimatedCostQty;
  }

  public BigDecimal getActualCostQty() {
    return actualCostQty;
  }

  public BigDecimal getEstimatedCostBatch() {
    return estimatedCostBatch;
  }

  public BigDecimal getActualCostBatch() {
    return actualCostBatch;
  }

  public BigDecimal getReceivedQty() {
    return receivedQty;
  }

  public Long getReceivedUoeId() {
    return receivedUoeId;
  }


  public String getReceivedStockType() {
    return receivedStockType;
  }

  public String getBatchNo() {
    return batchNo;
  }

  public BigDecimal getBatchQty() {
    return batchQty;
  }

  public Long getBatchUoeId() {
    return batchUoeId;
  }

   public String getBatchStockType() {
    return batchStockType;
  }

  public String getReceivedUoeCode() {
    return receivedUoeCode;
  }

  @Override
  public String getSysName() {
    return ITObStoreTransaction.SYS_NAME;
  }
}
