package com.ebs.dda.jpa.masterdata.vendor;

public class IObVendorPurchaseUnitValueObject {

  private String purchaseUnitCode;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public void setPurchaseUnitCode(String purchaseUnitCode) {
    this.purchaseUnitCode = purchaseUnitCode;
  }
}
