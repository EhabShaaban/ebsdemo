package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.api.annotations.Exclude;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "IObAccountingDocumentCompanyDataGeneralModel")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class IObAccountingDocumentCompanyDataGeneralModel extends InformationObject {

  private static final long serialVersionUID = 1L;

  private String documentCode;

  private Long purchaseUnitId;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  @Exclude private Long companyId;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private Long bankAccountId;

  private String bankAccountCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString bankAccountName;

  private String bankAccountNumber;

  public String getDocumentCode() {
    return documentCode;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public Long getBankAccountId() {
    return bankAccountId;
  }

  public String getBankAccountCode() {
    return bankAccountCode;
  }

  public LocalizedString getBankAccountName() {
    return bankAccountName;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }
}
