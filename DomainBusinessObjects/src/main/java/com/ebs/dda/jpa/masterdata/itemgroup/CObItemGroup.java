package com.ebs.dda.jpa.masterdata.itemgroup;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.item.enums.HierarchyLevelEnumType;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cobmaterialgroup")
public class CObItemGroup extends CodedBusinessObject implements Serializable {

  private static final long serialVersionUID = 1L;

    @Column(name = "parentid")
  private Long parentGroup;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString name;

    @Embedded
    private HierarchyLevelEnumType level;

  private String description;

    public static long getSerialVersionUID() {
        return serialVersionUID;
  }

    public LocalizedString getName() {
        return name;
  }

    public void setName(LocalizedString name) {
        this.name = name;
    }

  public HierarchyLevelEnumType getLevel() {
    return level;
  }

  public void setLevel(HierarchyLevelEnumType materialGroupLevel) {
    this.level = materialGroupLevel;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getSysName() {
    return IHObItemGroup.SYS_NAME;
  }

    public long getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(long parentGroup) {
        this.parentGroup = parentGroup;
    }
}
