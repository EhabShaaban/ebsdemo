package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(NotesReceivableTypeEnum.Values.NOTES_RECEIVABLE_AS_COLLECTION)
@EntityInterface(IDObMonetaryNotes.class)
public class DObNotesReceivableAsCollection extends DObMonetaryNotes {

  public static final String SYS_NAME = "NotesReceivableAsCollection";

  public DObNotesReceivableAsCollection() {
    setDocument(IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME);
  }

  @Override
  public String getSysName() {
    return SYS_NAME;
  }
}
