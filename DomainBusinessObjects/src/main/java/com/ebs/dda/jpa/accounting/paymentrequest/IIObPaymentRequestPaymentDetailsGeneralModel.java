package com.ebs.dda.jpa.accounting.paymentrequest;

public interface IIObPaymentRequestPaymentDetailsGeneralModel {

  String SYS_NAME = "PaymentRequest";

  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String DUE_DOCUMENT_TYPE = "dueDocumentType";
  String DUE_DOCUMENT_CODE = "dueDocumentCode";
  String NET_AMOUNT = "netAmount";
  String CURRENCY_ISO = "currencyISO";
  String CURRENCY_NAME = "currencyName";
  String CURRENCY_CODE = "currencyCode";
  String DESCRIPTION = "description";
  String COST_FACTOR_ITEM_CODE = "costFactorItemCode";
  String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
  String BANK_ACCOUNT_NAME = "bankAccountName";
  String TREASURY_CODE = "treasuryCode";
  String TREASURY_Name = "treasuryName";
  String COMPANY_BANK_ID = "companyBankId";
  String BANK_ACCOUNT_CODE = "bankAccountCode";
  String COMPANY_CODE = "companyCode";

  String EXPECTED_DUE_DATE = "expectedDueDate";
  String BANK_TRANS_REF = "bankTransRef";
}
