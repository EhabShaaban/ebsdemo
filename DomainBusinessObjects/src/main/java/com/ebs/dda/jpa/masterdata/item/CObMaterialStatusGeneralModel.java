package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.*;
import java.util.List;

/**
 * @author: Menna Sayed
 * @date: Jan 11, 2017
 */
@Entity
@Table(name = "CObMaterialStatusGeneralModel")
public class CObMaterialStatusGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  @Column(name = "description")
  private String description;

  @OneToMany
  @JoinColumn(name = "refinstanceid", referencedColumnName = "id")
  private List<IObMaterialRestrictionGeneralModel> materialRestrictions;

  public CObMaterialStatusGeneralModel() {}

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public List<IObMaterialRestrictionGeneralModel> getMaterialRestrictions() {
    return materialRestrictions;
  }

  public void setMaterialRestrictions(
      List<IObMaterialRestrictionGeneralModel> materialRestrictions) {
    this.materialRestrictions = materialRestrictions;
  }

  @Override
  public String getSysName() {
    return ICObMaterialStatus.SYS_NAME;
  }
}
