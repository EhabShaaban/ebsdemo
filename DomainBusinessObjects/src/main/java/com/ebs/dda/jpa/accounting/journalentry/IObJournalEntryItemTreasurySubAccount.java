package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "IObJournalEntryItemTreasurySubAccount")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Treasuries")
public class IObJournalEntryItemTreasurySubAccount extends IObJournalEntryItem {

  Long subAccountId;

  public void setSubAccountId(Long subAccountId) {
    this.subAccountId = subAccountId;
  }
}
