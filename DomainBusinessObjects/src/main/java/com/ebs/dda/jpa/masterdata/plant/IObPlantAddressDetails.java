package com.ebs.dda.jpa.masterdata.plant;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.IIObEnterpriseAddressDetails;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseAddressDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("3")
@EntityInterface(IIObEnterpriseAddressDetails.class)
public class IObPlantAddressDetails extends IObEnterpriseAddressDetails {

  private static final long serialVersionUID = 1L;
}
