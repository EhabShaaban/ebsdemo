package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObSettlementSummaryGeneralModel")
@EntityInterface(IIObSettlementSummaryGeneralModel.class)
public class IObSettlementSummaryGeneralModel extends DocumentHeader {

    private static final long serialVersionUID = 1L;

    private Double totalDebit;
    private Double totalCredit;
    private String settlementCode;
    private String currencyISO;
    private String currencyCode;

    public String getSettlementCode() {
        return settlementCode;
    }

    public String getCurrencyISO() {
        return currencyISO;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Double getTotalDebit() {
        return totalDebit;
    }

    public Double getTotalCredit() {
        return totalCredit;
    }
}
