package com.ebs.dda.jpa.masterdata.chartofaccount;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SubAccountGeneralModel")
public class SubAccountGeneralModel {

  @Id
  private Long id;

  private Long subaccountid;

  private String code;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private String ledger;

  public Long getId() {
    return id;
  }

  public Long getSubaccountid() {
    return subaccountid;
  }

  public String getCode() {
    return code;
  }

  public LocalizedString getName() {
    return name;
  }

  public String getLedger() {
    return ledger;
  }
}
