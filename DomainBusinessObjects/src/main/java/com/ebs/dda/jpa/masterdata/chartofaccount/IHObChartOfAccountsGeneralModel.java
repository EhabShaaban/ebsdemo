package com.ebs.dda.jpa.masterdata.chartofaccount;

public interface IHObChartOfAccountsGeneralModel {

  String ACCOUNT_ID = "id";

  String USER_CODE = "userCode";

  String ACCOUNT_NAME = "accountName";

  String ACCOUNT_TYPE = "accountType";

  String LEVEL = "level";

  String EN_LEVEL = "enLevel";

  String AR_LEVEL = "arLevel";

  String CREDIT_DEBIT = "creditDebit";

  String PARENT_CODE_NAME = "parentCodeName";

  String STATE = "currentStates";
}
