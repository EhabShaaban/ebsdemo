package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IIObGoodsReceiptReceivedItemsData {

    String SYS_NAME = "GoodsReceiptRecievedItems";
    String RECEIVED_ITEM_CODE = "itemCode";
    String STOCK_TYPE = "stockType";
    String ORDINARY_ITEMS_OBJECT_TYPE_CODE = "1";
    String BATCHED_ITEMS_OBJECT_TYPE_CODE = "2";
    String OBJECT_TYPE_CODE = "objectTypeCode";

    CompositeAttribute<IObGoodsReceiptPurchaseOrderItemQuantities>
            iObGoodsReceiptPurchaseOrderItemQuantities =
            new CompositeAttribute(
                    "iObGoodsReceiptPurchaseOrderItemQuantities",
                    IObGoodsReceiptPurchaseOrderItemQuantities.class,
                    true);
    CompositeAttribute<IObGoodsReceiptSalesReturnItemQuantities>
            iObGoodsReceiptSalesReturnItemQuantities =
            new CompositeAttribute(
                    "iObGoodsReceiptSalesReturnItemQuantities",
                    IObGoodsReceiptSalesReturnItemQuantities.class,
                    true);

    CompositeAttribute<IObGoodsReceiptItemBatches> iObGoodsReceiptItemBatches =
            new CompositeAttribute("iObGoodsReceiptItemBatches", IObGoodsReceiptItemBatches.class, true);
}
