package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObCompanyGeneralModel")
public class CObCompanyGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "companyName")
  private LocalizedString companyName;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "companyGroupName")
  private LocalizedString companyGroupName;

  @Column(name = "countryName")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString countryName;

  @Column(name = "cityName")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString cityName;

  private String currencyIso;

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public void setCompanyName(LocalizedString companyName) {
    this.companyName = companyName;
  }

  public LocalizedString getCompanyGroupName() {
    return companyGroupName;
  }

  public void setCompanyGroupName(LocalizedString companyGroupName) {
    this.companyGroupName = companyGroupName;
  }

  public LocalizedString getCountryName() {
    return countryName;
  }

  public void setCountryName(LocalizedString countryName) {
    this.countryName = countryName;
  }

  public LocalizedString getCityName() {
    return cityName;
  }

  public void setCityName(LocalizedString cityName) {
    this.cityName = cityName;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public void setCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
  }

  @Override
  public String getSysName() {
    return CObCompany.SYS_NAME;
  }
}
