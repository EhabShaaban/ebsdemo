package com.ebs.dda.jpa.accounting.journalbalance;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class JournalBalanceTypeEnum extends BusinessEnum {

  public static final String SYS_NAME = "JournalBalanceType";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private JournalBalanceType value;

  public JournalBalanceTypeEnum() {}

  @Override
  public JournalBalanceType getId() {
    return this.value;
  }

  public void setId(JournalBalanceType id) {
    this.value = id;
  }

  public enum JournalBalanceType {
    TREASURY,
    BANK
  }
}
