package com.ebs.dda.jpa.masterdata.salescoordinator;

public interface ICObSalesCoordinator {

  String SYS_NAME = "SalesCoordinator";

  String USER_CODE = "userCode";
  String NAME = "name";
}
