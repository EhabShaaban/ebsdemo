package com.ebs.dda.jpa.masterdata.item.enums;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 3:38:21 PM
 */
@Embeddable
public class PriceControlEnum extends BusinessEnum {

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private PriceControl value;

  public PriceControlEnum() {}

  public PriceControlEnum(PriceControl value) {
    this.value = value;
  }

  @Override
  public PriceControl getId() {
    return value;
  }

  public enum PriceControl {
    STANDARD,
    MOVING_AVERAGE
  }
}
