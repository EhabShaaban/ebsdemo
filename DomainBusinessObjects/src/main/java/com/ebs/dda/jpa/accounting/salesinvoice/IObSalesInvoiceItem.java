package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dda.jpa.accounting.IObInvoiceItem;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "iobsalesinvoiceitem")
public class IObSalesInvoiceItem extends IObInvoiceItem {

  private BigDecimal returnedQuantity;

  public BigDecimal getReturnedQuantity() {
    return returnedQuantity;
  }

  public void setReturnedQuantity(BigDecimal returnedQuantity) {
    this.returnedQuantity = returnedQuantity;
  }

  @Override
  public String getSysName() {
    return IIObSalesInvoiceItems.SYS_NAME;
  }
}
