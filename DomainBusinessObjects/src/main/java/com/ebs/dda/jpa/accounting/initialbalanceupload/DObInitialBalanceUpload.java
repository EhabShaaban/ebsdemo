package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.io.Serializable;

@Entity
@EntityInterface(IDObInitialBalanceUpload.class)
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = IDObInitialBalanceUpload.INITIAL_STOCK_UPLOAD_KEY)
public class DObInitialBalanceUpload extends DObAccountingDocument implements Serializable {
  private static final long serialVersionUID = 1L;
  private Long fiscalPeriodId;

  public Long getFiscalPeriodId() {
    return fiscalPeriodId;
  }

  public void setFiscalPeriodId(Long fiscalPeriodId) {
    this.fiscalPeriodId = fiscalPeriodId;
  }
  @Override
  public String getSysName() {
    return IDObInitialBalanceUpload.SYS_NAME;
  }
}
