package com.ebs.dda.jpa.accounting.initialbalanceupload;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DObInitialBalanceUploadJournalEntryPreparationGeneralModel")
public class DObInitialBalanceUploadJournalEntryPreparationGeneralModel {
	@Id
	private Long id;
	private String code;
	private Long businessUnitId;
	private Long companyId;
	private Long fiscalPeriodId;
	private Long companyCurrencyId;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public Long getBusinessUnitId() {
		return businessUnitId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public Long getFiscalPeriodId() {
		return fiscalPeriodId;
	}

	public Long getCompanyCurrencyId() {
		return companyCurrencyId;
	}
}
