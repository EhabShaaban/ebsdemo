package com.ebs.dda.jpa.accounting.taxes;

import com.ebs.dda.jpa.accounting.taxes.LObTaxInfo;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "LObVendorTaxes")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "VendorTaxes")
public class LObVendorTaxes extends LObTaxInfo implements Serializable {

  private Long vendorId;

  public Long getVendorId() {
    return vendorId;
  }

  public void setVendorId(Long vendorId) {
    this.vendorId = vendorId;
  }
}
