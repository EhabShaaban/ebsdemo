package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObSalesOrderCompanyStoreData")
public class IObSalesOrderCompanyStoreData extends DocumentHeader implements Serializable {

  private Long companyId;
  private Long StoreId;

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getStoreId() {
    return StoreId;
  }

  public void setStoreId(Long storeId) {
    StoreId = storeId;
  }
}
