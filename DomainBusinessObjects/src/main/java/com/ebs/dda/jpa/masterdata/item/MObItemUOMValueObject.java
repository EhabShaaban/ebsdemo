package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dda.jpa.masterdata.measure.UnitOfMeasureValueObject;

import java.util.List;

public class MObItemUOMValueObject {

  private String itemCode;
  private List<UnitOfMeasureValueObject> itemUnitOfMeasures;

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public List<UnitOfMeasureValueObject> getItemUnitOfMeasures() {
    return itemUnitOfMeasures;
  }

  public void setItemUnitOfMeasures(List<UnitOfMeasureValueObject> itemUnitOfMeasures) {
    this.itemUnitOfMeasures = itemUnitOfMeasures;
  }
}
