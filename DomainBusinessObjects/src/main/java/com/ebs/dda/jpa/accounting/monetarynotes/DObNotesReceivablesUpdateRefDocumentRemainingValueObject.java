package com.ebs.dda.jpa.accounting.monetarynotes;

public class DObNotesReceivablesUpdateRefDocumentRemainingValueObject
    extends RefDocumentRemainingValueObject {
  private String notesReceivablesCode;

  public String getNotesReceivablesCode() {
    return notesReceivablesCode;
  }

  public void setNotesReceivablesCode(String notesReceivablesCode) {
    this.notesReceivablesCode = notesReceivablesCode;
  }
}
