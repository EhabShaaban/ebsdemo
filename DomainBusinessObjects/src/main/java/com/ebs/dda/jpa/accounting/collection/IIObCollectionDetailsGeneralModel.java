package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObCollectionDetailsGeneralModel {

  String SYS_NAME = "Collection";

  String COLLECTION_CODE = "collectionCode";
  String REF_DOCUMENT_TYPE = "refDocumentType";
  String REF_DOCUMENT_CODE = "refDocumentCode";
  String CURRENCY_ISO = "currencyISO";
  String BANK_ACCOUNT_CODE = "bankAccountCode";
  String BANK_ACCOUNT_NAME = "bankAccountName";
  String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
  String TREASURY_CODE = "treasuryCode";
  String TREASURY_NAME = "treasuryName";
  String COLLECTION_METHOD = "collectionMethod";
  String AMOUNT = "amount";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASE_UNIT_NAME_ATTR = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
          new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR);
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
}
