package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorPurchaseUnitGeneralModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MObVendorPurchaseUnitGeneralModel")
@EntityInterface(IMObVendorPurchaseUnitGeneralModel.class)
public class MObVendorPurchaseUnitGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Column(name = "vendorcode")
  private String vendorCode;

  @Column(name = "purchaseunitcode")
  private String purchaseUnitCode;

  @Column(name = "purchaseunitname")
  private String purchaseUnitName;

  @Override
  public String getSysName() {
    return IMObVendor.SYS_NAME;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public void setPurchaseUnitCode(String purchaseUnitCode) {
    this.purchaseUnitCode = purchaseUnitCode;
  }

  public String getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public void setPurchaseUnitName(String purchaseUnitName) {
    this.purchaseUnitName = purchaseUnitName;
  }
}
