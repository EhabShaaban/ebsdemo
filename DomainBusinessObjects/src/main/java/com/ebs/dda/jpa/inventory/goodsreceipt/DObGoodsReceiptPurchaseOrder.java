package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dobgoodsreceiptpurchaseorder")
@Inheritance(strategy = InheritanceType.JOINED)
@EntityInterface(IDObGoodsReceiptPurchaseOrder.class)
@DiscriminatorValue(value = "GR_PO")
public class DObGoodsReceiptPurchaseOrder extends DObGoodsReceipt implements Serializable {

  private static final long serialVersionUID = 1L;
  private Long typeId;

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  @Override
  public String getSysName() {
    return IDObGoodsReceiptPurchaseOrder.SYS_NAME;
  }
}
