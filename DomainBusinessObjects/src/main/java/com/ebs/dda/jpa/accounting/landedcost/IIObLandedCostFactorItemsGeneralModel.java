package com.ebs.dda.jpa.accounting.landedcost;

public interface IIObLandedCostFactorItemsGeneralModel  {

    final String CODE = "code";
    final String ITEM_CODE = "itemCode";
    final String ITEM_NAME = "itemName";
    final String COMPANY_CURRENCY = "companyCurrency";
    final String ESTIMATED_VALUE = "estimatedValue";
    final String ACTUAL_VALUE = "actualValue";
    final String DIFFERENE = "difference";

    final String DETAILS = "details";

}
