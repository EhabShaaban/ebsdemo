package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.buisnesspartner.MObBusinessPartner;

import javax.persistence.*;

@Entity
@Table(name = "MObVendor")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "2")
@EntityInterface(IMObVendor.class)
public class MObVendor extends MObBusinessPartner implements IMObVendor {

    private static final long serialVersionUID = 1L;

    @Column
    private Long purchaseResponsibleId;

    @Column
    private Long typeId;

    @Column
    private String accountWithVendor;

    @Column
    private String oldVendorNumber;

    public Long getPurchaseResponsibleId() {
        return purchaseResponsibleId;
    }

    public void setPurchaseResponsibleId(Long purchaseResponsibleId) {
    this.purchaseResponsibleId = purchaseResponsibleId;
  }

  public String getAccountWithVendor() {
    return accountWithVendor;
  }

  public void setAccountWithVendor(String accountWithVendor) {
    this.accountWithVendor = accountWithVendor;
  }

    public String getOldVendorNumber() {
        return oldVendorNumber;
    }

    public void setOldVendorNumber(String oldVendorNumber) {
        this.oldVendorNumber = oldVendorNumber;
    }

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @Override
    public String getSysName() {
        return IMObVendor.SYS_NAME;
    }
}
