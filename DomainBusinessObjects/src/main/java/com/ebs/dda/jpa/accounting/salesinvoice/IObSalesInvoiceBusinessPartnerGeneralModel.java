package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.accounting.paymentrequest.IIObPaymentRequestPaymentDetailsGeneralModel;

import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobsalesinvoicebusinesspartnergeneralmodel")
@EntityInterface(IIObSalesInvoiceBusinessPartnerGeneralModel.class)
public class IObSalesInvoiceBusinessPartnerGeneralModel extends InformationObject {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  private String code;

  private String businessPartnerCode;
  private String businesspartnercodename;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  private String currencyCode;
  private String currencyIso;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString paymentTermName;

  private String paymentTermCode;
  private String paymentTermCodeName;

  private BigDecimal downPayment;
  private String salesOrder;

  @Convert(converter = StatefullBusinessObjectCurrentStatesJpaConverter.class)
  private Set<String> currentStates;

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public void setBusinessPartnerCode(String businessPartnerCode) {
    this.businessPartnerCode = businessPartnerCode;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(LocalizedString currencyName) {
    this.currencyName = currencyName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public void setPaymentTermCode(String paymentTermCode) {
    this.paymentTermCode = paymentTermCode;
  }

  public BigDecimal getDownPayment() {
    return downPayment;
  }

  public void setDownPayment(BigDecimal downPayment) {
    this.downPayment = downPayment;
  }

  public String getSalesOrder() {
    return salesOrder;
  }

  public void setSalesOrder(String salesOrder) {
    this.salesOrder = salesOrder;
  }

  public Set<String> getCurrentStates() {
    return currentStates;
  }

  public void setCurrentStates(Set<String> currentStates) {
    this.currentStates = currentStates;
  }

  public void setBusinessPartnerName(LocalizedString businessPartnerName) {
    this.businessPartnerName = businessPartnerName;
  }

  public void setBusinesspartnercodename(String businesspartnercodename) {
    this.businesspartnercodename = businesspartnercodename;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public void setCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
  }

  public void setPaymentTermName(LocalizedString paymentTermName) {
    this.paymentTermName = paymentTermName;
  }

  public void setPaymentTermCodeName(String paymentTermCodeName) {
    this.paymentTermCodeName = paymentTermCodeName;
  }

  public String getCode() {
    return code;
  }


  @Override
  public String getSysName() {
    return IIObPaymentRequestPaymentDetailsGeneralModel.SYS_NAME;
  }
}
