package com.ebs.dda.jpa.accounting;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "purchasedItemsTotalFinalCost")
public class PurchasedItemsTotalFinalCost implements Serializable {

  private static final long serialVersionUID = 1L;
  @Id private Long itemId;
  private String itemCode;
  private String unitOfEntryCode;
  private String goodsReceiptCode;
  private String landedCostCode;
  private String landedCostState;
  private Long purchaseOrderId;
  private String purchaseorderCode;
  private BigDecimal unrestrictedQty;

  @Column(name = "itemSingleUnitFinalCostWithDamagedItemsCost")
  private BigDecimal itemTotalFinalCost;

  public Long getItemId() {
    return itemId;
  }

  public String getItemCode() {
    return itemCode;
  }

  public BigDecimal getItemTotalFinalCost() {
    return itemTotalFinalCost;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public String getLandedCostState() {
    return landedCostState;
  }

  public String getLandedCostCode() {
    return landedCostCode;
  }

  public String getUnitOfEntryCode() {
    return unitOfEntryCode;
  }

  public Long getPurchaseOrderId() {
    return purchaseOrderId;
  }

  public String getPurchaseorderCode() {
    return purchaseorderCode;
  }

  public BigDecimal getUnrestrictedQty() {
    return unrestrictedQty;
  }
}
