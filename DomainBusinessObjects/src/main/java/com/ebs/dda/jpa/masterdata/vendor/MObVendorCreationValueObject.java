package com.ebs.dda.jpa.masterdata.vendor;

import java.util.List;

public class MObVendorCreationValueObject {

  private String vendorName;

  private String vendorType;

  private String purchaseResponsibleCode;

  private String oldVendorReference;

  private String accountWithVendor;

  private List<IObVendorPurchaseUnitValueObject> purchaseUnits;

  public String getVendorName() {
    return vendorName;
  }

  public String getPurchaseResponsibleCode() {
    return purchaseResponsibleCode;
  }

  public String getOldVendorReference() {
    return oldVendorReference;
  }

  public String getAccountWithVendor() {
    return accountWithVendor;
  }

  public List<IObVendorPurchaseUnitValueObject> getPurchaseUnits() {
    return purchaseUnits;
  }

  public void setPurchaseUnits(List<IObVendorPurchaseUnitValueObject> purchaseUnits) {
    this.purchaseUnits = purchaseUnits;
  }

  public void setVendorName(String vendorName) {
    this.vendorName = vendorName;
  }

  public void setVendorType(String vendorType) {
    this.vendorType = vendorType;
  }

  public void setPurchaseResponsibleCode(String purchaseResponsibleCode) {
    this.purchaseResponsibleCode = purchaseResponsibleCode;
  }

  public void setOldVendorReference(String oldVendorReference) {
    this.oldVendorReference = oldVendorReference;
  }

  public void setAccountWithVendor(String accountWithVendor) {
    this.accountWithVendor = accountWithVendor;
  }

  public String getVendorType() {
    return vendorType;
  }
}
