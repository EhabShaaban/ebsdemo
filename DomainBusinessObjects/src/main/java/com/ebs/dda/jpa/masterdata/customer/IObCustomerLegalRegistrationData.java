package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobcustomerlegalregistrationdata")
public class IObCustomerLegalRegistrationData extends InformationObject {

  private static final long serialVersionUID = 1L;

  private String registrationNumber;
  private String taxCardNumber;

  private Long issueFromId;
  private Long taxAdministrativeId;
  private String taxFileNumber;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  public String getTaxCardNumber() {
    return taxCardNumber;
  }

  public void setTaxCardNumber(String taxCard) {
    this.taxCardNumber = taxCard;
  }

  public Long getIssueFromId() {
    return issueFromId;
  }

  public void setIssueFromId(Long issueFrom) {
    this.issueFromId = issueFrom;
  }

  public Long getTaxAdministrativeId() {
    return taxAdministrativeId;
  }

  public void setTaxAdministrativeId(Long taxAdministrative) {
    this.taxAdministrativeId = taxAdministrative;
  }

  public String getTaxFileNumber() {
    return taxFileNumber;
  }

  public void setTaxFileNumber(String taxFileNumber) {
    this.taxFileNumber = taxFileNumber;
  }

  @Override
  public String getSysName() {
    return "IObCustomerLegalRegistrationData";
  }
}
