package com.ebs.dda.jpa.inventory.goodsissue;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObGoodsIssueSalesInvoiceItem")
public class IObGoodsIssueSalesInvoiceItem extends IObGoodsIssueItem {
}
