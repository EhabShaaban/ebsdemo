package com.ebs.dda.jpa.accounting.landedcost;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "DObLandedCost")
@EntityInterface(IDObLandedCost.class)
@DiscriminatorValue(value = "LandedCost")
public class DObLandedCost extends DObAccountingDocument implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long typeId;

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  @Override
  public String getSysName() {
    return IDObLandedCost.SYS_NAME;
  }
}
