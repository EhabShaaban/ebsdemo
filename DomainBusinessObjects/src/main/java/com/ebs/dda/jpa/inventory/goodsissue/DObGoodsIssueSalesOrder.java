package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dobgoodsissuesalesorder")
@Inheritance(strategy = InheritanceType.JOINED)
@EntityInterface(IDObGoodsIssueSalesOrder.class)
@DiscriminatorValue(value = "GI_SO")
public class DObGoodsIssueSalesOrder extends DObGoodsIssue implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long typeId;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @Override
    public String getSysName() {
        return IDObGoodsIssueSalesOrder.SYS_NAME;
    }
}
