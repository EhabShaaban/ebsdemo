package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

@Entity
@DiscriminatorValue("LandedCost")
public class IObLandedCostCompanyData extends IObAccountingDocumentCompanyData {

  private static final long serialVersionUID = 1L;

}
