package com.ebs.dda.jpa.masterdata.item;

public interface IMObItemUOMGeneralModel {
    String SYS_NAME = "ItemUOM";

    String ITEM_CODE = "itemCode";
    String USER_CODE = "userCode";
    String SYMBOL = "symbol";
}
