package com.ebs.dda.jpa.accounting.vendorinvoice;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

@Entity
@Table(name = "IObVendorInvoicePurchaseOrder")
@EntityInterface(IIObVendorInvoicePurchaseOrder.class)
public class IObVendorInvoicePurchaseOrder extends DocumentHeader implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long purchaseOrderId;
  private Long currencyId;
  private Long paymentTermId;

  public Long getPurchaseOrderId() {
    return purchaseOrderId;
  }

  public void setPurchaseOrderId(Long purchaseOrderId) {
    this.purchaseOrderId = purchaseOrderId;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public Long getPaymentTermId() {
    return paymentTermId;
  }

  public void setPaymentTermId(Long paymentTermId) {
    this.paymentTermId = paymentTermId;
  }

  @Override
  public String getSysName() {
    return IIObVendorInvoicePurchaseOrder.SYS_NAME;
  }
}
