package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObCostingDetailsGeneralModel {
  String GOODS_RECEIPT_CODE = "goodsReceiptCode";
  String CURRENCY_PRICE_ESTIMATE_TIME = "currencyPriceEstimateTime";
  String CURRENCY_PRICE_ACTUAL_TIME = "currencyPriceActualTime";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String LOCAL_CURRENCY_ISO = "localCurrencyIso";
  String CURRENT_STATES = "currentStates";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
          new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME);

}
