package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel")
@EntityInterface(IIObGoodsReceiptOrdinaryReceivedItemGeneralModel.class)
public class IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel {

  @Id private Long id;
  private Long ordinaryItemId;

  private BigDecimal receivedQtyUoE;
  private BigDecimal receivedQtyBase;
  private String stockType;
  private String notes;
  private String goodsReceiptCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString baseUnitSymbol;

  private String itemCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString unitOfEntry;

  private String unitOfEntryCode;

  private BigDecimal baseUnitFactor;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public LocalizedString getBaseUnitName() {
    return baseUnitSymbol;
  }

  public String getItemCode() {
    return itemCode;
  }

  public Long getId() {
    return id;
  }

  public BigDecimal getReceivedQtyUoE() {
    return receivedQtyUoE;
  }

  public BigDecimal getReceivedQtyBase() {
    return receivedQtyBase;
  }

  public String getStockType() {
    return stockType;
  }

  public String getNotes() {
    return notes;
  }

  public LocalizedString getUnitOfEntry() {
    return unitOfEntry;
  }

  public String getUnitOfEntryCode() {
    return unitOfEntryCode;
  }

  public Long getOrdinaryItemId() {
    return ordinaryItemId;
  }
}
