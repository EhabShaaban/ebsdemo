package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObSalesReturnOrderType")
@EntityInterface(ICObSalesReturnOrderType.class)
public class CObSalesReturnOrderType extends ConfigurationObject<DefaultUserCode> implements Serializable {

  @Override
  public String getSysName() {
    return ICObSalesReturnOrderType.SYS_NAME;
  }
}
