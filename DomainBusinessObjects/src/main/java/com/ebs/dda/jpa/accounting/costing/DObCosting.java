package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DObCosting")
@DiscriminatorValue(value = "Costing")
public class DObCosting extends DObAccountingDocument implements Serializable {
  @Override
  public String getSysName() {
    return IDObCosting.SYS_NAME;
  }
}
