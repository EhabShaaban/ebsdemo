package com.ebs.dda.jpa.inventory.inventorydocument;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DObInventoryDocument")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "inventoryDocumentType")
@EntityInterface(IDObInventoryDocument.class)
public abstract class DObInventoryDocument extends DocumentObject<DefaultUserCode>
        implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long purchaseUnitId;

    @Column(name = "inventoryDocumentType", insertable = false, updatable = false)
    private String inventoryDocumentType;

    public Long getPurchaseUnitId() {
        return purchaseUnitId;
    }

    public void setPurchaseUnitId(Long purchaseUnitId) {
        this.purchaseUnitId = purchaseUnitId;
    }

    public String getInventoryDocumentType() {
        return inventoryDocumentType;
    }
}
