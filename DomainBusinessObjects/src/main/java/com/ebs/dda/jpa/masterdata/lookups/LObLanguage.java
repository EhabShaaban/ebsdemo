/** */
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.geolocale.LocalizedLObGeoLocale;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 24, 2016 2:45:16 PM
 */
@Entity
@DiscriminatorValue("1")
public class LObLanguage extends LocalizedLObGeoLocale implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_LANGUAGE_SYS_NAME;
  }
}
