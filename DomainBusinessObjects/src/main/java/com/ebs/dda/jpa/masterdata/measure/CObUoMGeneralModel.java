package com.ebs.dda.jpa.masterdata.measure;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 24, 2016
 */
@Entity
@Table(name = "CObUoMGeneralModel")
public class CObUoMGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "symbol")
  private LocalizedString symbol;

  @Column(name = "isStandard")
  private Boolean isStandard;

  @Column(name = "internationalSystem")
  private Boolean internationalSystem;

  @Column(name = "dimension")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString dimension;

  @Column(name = "isoCode")
  private String isoCode;

  public CObUoMGeneralModel() {}

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public LocalizedString getSymbol() {
    return symbol;
  }

  public void setSymbol(LocalizedString symbol) {
    this.symbol = symbol;
  }

  public Boolean getStandard() {
    return isStandard;
  }

  public void setStandard(Boolean standard) {
    isStandard = standard;
  }

  public Boolean getInternationalSystem() {
    return internationalSystem;
  }

  public void setInternationalSystem(Boolean internationalSystem) {
    this.internationalSystem = internationalSystem;
  }

  public LocalizedString getDimension() {
    return dimension;
  }

  public void setDimension(LocalizedString dimension) {
    this.dimension = dimension;
  }

  public String getIsoCode() {
    return isoCode;
  }

  public void setIsoCode(String isoCode) {
    this.isoCode = isoCode;
  }

  @Override
  public String getSysName() {
    return "COb Standard UoM General Model";
  }
}
