package com.ebs.dda.jpa.accounting.initialbalanceupload;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "IObInitialBalanceUploadVendorItem")
@DiscriminatorValue(value = "Local_Vendors")
public class IObInitialBalanceUploadVendorItem extends IObInitialBalanceUploadItem
    implements Serializable {
  private Long vendorId;

  public Long getVendorId() {
    return vendorId;
  }

  public void setVendorId(Long vendorId) {
    this.vendorId = vendorId;
  }
}
