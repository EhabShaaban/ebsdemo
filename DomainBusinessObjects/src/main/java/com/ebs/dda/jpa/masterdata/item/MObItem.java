package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.MasterDataMinor;

import javax.persistence.*;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 30, 2017 , 2:27:00 PM
 */
@Entity
@Table(name = "mobitem")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "1")
@EntityInterface(IMObItem.class)
public class MObItem extends MasterDataMinor implements IMObItem {

  private static final long serialVersionUID = 1L;

  private String oldItemNumber;

  private Long basicUnitOfMeasure;

  private Long itemGroup;

  private Boolean batchManagementRequired;

  private Long typeId;

  private Long productManagerId;

  private String marketName;

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  public void setOldItemNumber(String oldItemNumber) {
    this.oldItemNumber = oldItemNumber;
  }

  public Long getBasicUnitOfMeasure() {
    return basicUnitOfMeasure;
  }

  public void setBasicUnitOfMeasure(Long basicUnitOfMeasure) {
    this.basicUnitOfMeasure = basicUnitOfMeasure;
  }

  public Long getItemGroup() {
    return itemGroup;
  }

  public void setItemGroup(Long itemGroup) {
    this.itemGroup = itemGroup;
  }

  public Boolean getBatchManagementRequired() {
    return batchManagementRequired;
  }

  public void setBatchManagementRequired(Boolean batchManagementRequired) {
    this.batchManagementRequired = batchManagementRequired;
  }

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public String getMarketName() {
    return marketName;
  }

  public void setMarketName(String marketName) {
    this.marketName = marketName;
  }

  public Long getProductManagerId() {
    return productManagerId;
  }

  public void setProductManagerId(Long productManagerId) {
    this.productManagerId = productManagerId;
  }

  @Override
  public String getSysName() {
    return SYS_NAME;
  }
}
