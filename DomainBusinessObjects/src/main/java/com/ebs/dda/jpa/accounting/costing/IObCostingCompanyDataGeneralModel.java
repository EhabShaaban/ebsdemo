package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Costing")
public class IObCostingCompanyDataGeneralModel
    extends IObAccountingDocumentCompanyDataGeneralModel {

  @Override
  public String getSysName() {
    return IDObCosting.SYS_NAME;
  }
}
