package com.ebs.dda.jpa.masterdata.item;

public interface IIObItemAccountingInfo {

  String SYS_NAME = "IObItemAccountingInfo";
  String ACCOUNT_CODE = "accountCode";
  String ACCOUNT_NAME = "accountName";
  String COST_FACTOR = "costFactor";

}
