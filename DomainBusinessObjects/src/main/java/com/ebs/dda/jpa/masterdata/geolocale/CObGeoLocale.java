package com.ebs.dda.jpa.masterdata.geolocale;

import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 27, 2016 9:26:24 AM
 */
@Entity
@Table(name = "CObGeoLocale")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class CObGeoLocale extends ConfigurationObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
