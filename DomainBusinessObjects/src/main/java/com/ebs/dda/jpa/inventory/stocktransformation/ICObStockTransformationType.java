package com.ebs.dda.jpa.inventory.stocktransformation;

public interface ICObStockTransformationType {
    String SYS_NAME = "StockTransformationType";
    String USER_CODE = "userCode";
    String NAME = "name";
}
