package com.ebs.dda.jpa.masterdata.chartofaccount.enums;

public enum MappedAccountsEnum {
	CashGLAccount, RealizedGLAccount, BankGLAccount, CustomersGLAccount, TaxesGLAccount,
	SalesGLAccount, ImportPurchasingGLAccount, LocalPurchasingGLAccount, PurchasingGLAccount,
	PurchaseOrderGLAccount, NotesReceivablesGLAccount, TreasuryGLAccount,
	UnrealizedCurrencyGainLossGLAccount,
}
