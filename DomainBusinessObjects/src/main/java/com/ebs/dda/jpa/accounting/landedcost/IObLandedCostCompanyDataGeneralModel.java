package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;

@Entity
@DiscriminatorValue(value = "LandedCost")
@EntityInterface(IIObAccountingDocumentCompanyDataGeneralModel.class)
public class IObLandedCostCompanyDataGeneralModel
    extends IObAccountingDocumentCompanyDataGeneralModel {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IDObLandedCost.SYS_NAME;
  }
}
