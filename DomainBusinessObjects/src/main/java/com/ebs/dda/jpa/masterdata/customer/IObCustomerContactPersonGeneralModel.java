package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IIObCustomerContactPersonGeneralModel.class)
@Table(name = "IObCustomerContactPersonGeneralModel")
public class IObCustomerContactPersonGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

    private static final long serialVersionUID = 1L;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString customerName;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString contactPersonName;

    public LocalizedString getContactPersonName() {
        return contactPersonName;
    }

    public LocalizedString getCustomerName() {
        return customerName;
    }


    @Override
    public String getSysName() {
        return IIObCustomerContactPersonGeneralModel.SYS_NAME;
    }
}
