package com.ebs.dda.jpa.inventory.storetransaction;

public interface ITObGoodsReceiptStoreTransactionGeneralModel {
    String QUANTITY = "quantity";
    String COST = "cost";
    String STOCK_TYPE = "stockType";
    String COMPANY_CODE = "companyCode";
    String COMPANY_NAME = "companyName";
    String STOREHOUSE_CODE = "storehouseCode";
    String STOREHOUSE_NAME = "storehouseName";
    String PLANT_CODE = "plantCode";
    String PLANT_NAME = "plantName";
    String PURCHASE_UNIT_CODE = "purchaseUnitCode";
    String PURCHASE_UNIT_NAME = "purchaseUnitName";
    String UNIT_OF_MEASURE_SYMBOL = "unitOfMeasureSymbol";
    String UNIT_OF_MEASURE_CODE = "unitOfMeasureCode";
    String REF_TRANSACTION_CODE = "refTransactionCode";
    String ITEM_NAME = "itemName";
    String ITEM_CODE = "itemCode";
    String GOODS_RECEIPT_CODE = "goodsReceiptCode";
}
