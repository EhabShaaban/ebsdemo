package com.ebs.dda.jpa.masterdata.purchaseresponsible;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/** @author xerix on Tue Nov 2017 at 09 : 53 */
@Entity
@DiscriminatorValue("6")
@EntityInterface(ICObPurchasingResponsible.class)
public class CObPurchasingResponsible extends CObEnterprise {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ICObPurchasingResponsible.SYS_NAME;
  }
}
