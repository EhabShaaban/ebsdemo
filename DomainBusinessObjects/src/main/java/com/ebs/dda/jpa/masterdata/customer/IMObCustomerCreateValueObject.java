package com.ebs.dda.jpa.masterdata.customer;

public interface IMObCustomerCreateValueObject {

  String LEGAL_NAME = "legalName";
  String MARKET_NAME = "marketName";
  String BUSINESS_UNIT_CODES = "businessUnitCodes";
  String CREDIT_LIMIT = "creditLimit";
  String CURRENCY_ISO = "currencyIso";
  String REGISTRATION_NUMBER = "registrationNumber";
  String ISSUED_FROM_CODE = "issuedFromCode";
  String TAX_ADMINISTRATION_CODE = "taxAdministrationCode";
  String TAX_CARD_NUMBER = "taxCardNumber";
  String TAX_FILE_NUMBER = "taxFileNumber";
  String OLD_CUSTOMER_NUMBER = "oldCustomerNumber";
}
