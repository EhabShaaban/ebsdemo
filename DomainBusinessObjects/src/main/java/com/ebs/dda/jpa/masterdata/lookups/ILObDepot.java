package com.ebs.dda.jpa.masterdata.lookups;

public interface ILObDepot {
  String SYS_NAME = "Depot";
  String USER_CODE = "userCode";
  String NAME = "name";
}
