package com.ebs.dda.jpa.accounting.paymentrequest;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObPaymentRequestActivatePreparationGeneralModel")
public class DObPaymentRequestActivatePreparationGeneralModel {
    @Id
    private Long id;
    private String paymentRequestCode;
    private String businessUnitCode;
    private String companyCode;
    private String currencyCode;
    private String bankCode;
    private String bankAccountNo;
    private String treasuryCode;
    private String paymentType;
    private String dueDocumentType;
    private String dueDocumentCode;
    private BigDecimal paymentNetAmount;
    private BigDecimal balance;
    private BigDecimal currencyPrice;
    private String exchangeRate;

    public String getPaymentRequestCode() {
        return paymentRequestCode;
    }

    public String getBusinessUnitCode() {
        return businessUnitCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getBankCode() {
        return bankCode;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public String getTreasuryCode() {
        return treasuryCode;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getDueDocumentType() {
        return dueDocumentType;
    }

    public String getDueDocumentCode() {
        return dueDocumentCode;
    }

    public BigDecimal getPaymentNetAmount() {
        return paymentNetAmount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public BigDecimal getCurrencyPrice() {
        return currencyPrice;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }
}
