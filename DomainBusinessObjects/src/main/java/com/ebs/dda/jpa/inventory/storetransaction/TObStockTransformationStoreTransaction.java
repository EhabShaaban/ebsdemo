package com.ebs.dda.jpa.inventory.storetransaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TObStockTransformationStoreTransaction")
@DiscriminatorValue("STK_TR")
public class TObStockTransformationStoreTransaction extends TObStoreTransaction {
  public TObStockTransformationStoreTransaction() {}

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }

  public Long getDocumentReferenceId() {
    return documentReferenceId;
  }
}
