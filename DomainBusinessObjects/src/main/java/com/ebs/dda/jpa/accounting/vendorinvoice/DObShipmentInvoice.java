package com.ebs.dda.jpa.accounting.vendorinvoice;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = VendorInvoiceTypeEnum.Values.SHIPMENT_INVOICE)
public class DObShipmentInvoice extends DObPurchaseServiceLocalInvoice implements Serializable {

}
