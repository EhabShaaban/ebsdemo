/** */
package com.ebs.dda.jpa.masterdata.country;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yara Ameen
 * @since Mar 27, 2016 9:22:23 AM
 */
@Entity
@Table(name = "cobcountrygeneralmodel")
public class CObCountryGeneralModel extends StatefullBusinessObject {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString language;

  private String standardTimezone;
  private String dstTimezone;

  private String isoCode;

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public LocalizedString getLanguage() {
    return language;
  }

  public void setLanguage(LocalizedString language) {
    this.language = language;
  }

  public String getStandardTimezone() {
    return standardTimezone;
  }

  public void setStandardTimezone(String standardTimezone) {
    this.standardTimezone = standardTimezone;
  }

  public String getDstTimezone() {
    return dstTimezone;
  }

  public void setDstTimezone(String dstTimezone) {
    this.dstTimezone = dstTimezone;
  }

  public String getIsoCode() {
    return isoCode;
  }

  public void setIsoCode(String isoCode) {
    this.isoCode = isoCode;
  }

  @Override
  public String getSysName() {
    return CObCountry.SYS_NAME;
  }
}
