package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IDObCollectionGeneralModel {

  String SYS_NAME = "Collection";
  String USER_CODE = "userCode";
  String CODE = "Code";
  String NAME = "Name";
  String TYPE = "Type";
  String TYPE_NAME = "TypeName";
  String SOURCE = "source";
  String BUSINESS_PARTNER = "businessPartner";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String BUSINESS_PARTNER_TYPE = "businessPartnerType";
  String CREATION_INFO = "creationInfo";
  String CREATION_DATE = "creationDate";
  String COLLECTION_TYPE = "collectionType";

  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASE_UNIT_NAME_ATTR = "purchaseUnitName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR);

  String DOCUMENT_OWNER = "documentOwner";
  String DOCUMENT_OWNER_USER_NAME = "documentOwnerUserName";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
  String REF_DOCUMENT = "refDocument";
  String REF_DOCUMENT_TYPE = "refDocumentType";
  String REF_DOCUMENT_TYPE_NAME = "refDocumentTypeName";
  String REF_DOCUMENT_TYPE_CODE = "refDocumentTypeCode";
  String REF_DOCUMENT_CODE = "refDocumentCode";
  String AMOUNT = "amount";
  String CURRENCY = "currency";
  String COMPANY_CURRENCY_ISO = "companyLocalCurrencyISO";

  String PURCHASE_UNIT = "purchaseUnitName";

  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";

  String CURRENT_STATES = "currentStates";

  String COMAPNY_CODE = "companyCode";

  String COMPANY_NAME = "companyName";

  String COMPANY_LOCAL_CURRENCY_CODE = "companyLocalCurrencyCode";

  String COMPANY_LOCAL_CURRENCY_NAME = "companyLocalCurrencyName";

}
