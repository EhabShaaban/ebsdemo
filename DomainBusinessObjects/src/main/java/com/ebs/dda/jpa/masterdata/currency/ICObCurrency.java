package com.ebs.dda.jpa.masterdata.currency;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;

public interface ICObCurrency extends IStatefullBusinessObject {

  String SYS_NAME = "Currency";

  String ISO_ATTR_NAME = "iso";
}
