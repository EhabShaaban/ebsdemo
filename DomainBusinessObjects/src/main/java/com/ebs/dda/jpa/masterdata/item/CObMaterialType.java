package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.item.enums.QuantityValueUpdatingEnum;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.embededobjects.ProcurementSettings;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.embededobjects.ValuationSetting;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 3:11:53 PM
 */
@Entity
@Table(name = "CObMaterialType")
@DiscriminatorValue("1")
@EntityInterface(ICObMaterialType.class)
public class CObMaterialType extends CObMaterial implements ICObMaterialType, Serializable {

  private static final long serialVersionUID = 1L;

  @Embedded
  @EmbeddedAttribute
  private ProcurementSettings procurementSettings;

  @Embedded
  @EmbeddedAttribute
  private ValuationSetting valuationSetting;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "quantityUpdating"))
  private QuantityValueUpdatingEnum quantityUpdatingEnum;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "valueUpdating"))
  private QuantityValueUpdatingEnum valueUpdatingEnum;

  public CObMaterialType() {}

  @Override
  public String getSysName() {
    return ICObMaterialType.SYS_NAME;
  }

  public ProcurementSettings getProcurementSettings() {
    if (procurementSettings == null) {
      procurementSettings = new ProcurementSettings();
    }
    return procurementSettings;
  }

  public void setProcurementSettings(ProcurementSettings procurementSettings) {
    this.procurementSettings = procurementSettings;
  }

  public ValuationSetting getValuationSetting() {
    if (valuationSetting == null) {
      valuationSetting = new ValuationSetting();
    }
    return valuationSetting;
  }

  public void setValuationSetting(ValuationSetting valuationSetting) {
    this.valuationSetting = valuationSetting;
  }

  public QuantityValueUpdatingEnum getQuantityUpdatingEnum() {
    if (quantityUpdatingEnum == null) {
      quantityUpdatingEnum = new QuantityValueUpdatingEnum();
    }
    return quantityUpdatingEnum;
  }

  public void setQuantityUpdatingEnum(QuantityValueUpdatingEnum quantityUpdatingEnum) {
    this.quantityUpdatingEnum = quantityUpdatingEnum;
  }

  public QuantityValueUpdatingEnum getValueUpdatingEnum() {
    if (valueUpdatingEnum == null) {
      valueUpdatingEnum = new QuantityValueUpdatingEnum();
    }
    return valueUpdatingEnum;
  }

  public void setValueUpdatingEnum(QuantityValueUpdatingEnum valueUpdatingEnum) {
    this.valueUpdatingEnum = valueUpdatingEnum;
  }
}
