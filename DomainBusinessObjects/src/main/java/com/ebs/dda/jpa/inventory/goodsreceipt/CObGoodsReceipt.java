package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObGoodsReceipt")
@EntityInterface(ICObGoodsReceipt.class)
public class CObGoodsReceipt extends ConfigurationObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ICObGoodsReceipt.SYS_NAME;
  }
}
