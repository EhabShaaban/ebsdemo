package com.ebs.dda.jpa.accounting.vendorinvoice;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = VendorInvoiceTypeEnum.Values.INSURANCE_INVOICE)
public class DObInsuranceInvoice extends DObPurchaseServiceLocalInvoice implements Serializable {

}
