package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IMObItemGeneralModel {
  String SYS_NAME = "Item";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String CODE = "userCode";
  String ITEM_NAME = "itemName";
  String ITEM_CODE = "itemCode";
  String BASIC_UOM_CODE = "basicUnitOfMeasureCode";
  String BASIC_UOM_SYMBOL = "basicUnitOfMeasureSymbol";
  String BASIC_UOM_NAME = "basicUnitOfMeasureName";
  String PURCHASE_UNIT_CODES = "purchasingUnitCodes";
  String MARKET_NAME = "marketName";
  String ITEM_TYPE_NAME = "typeName";
  String IS_BATCH_MANAGED = "isBatchManaged";
  String ITEM_TYPE_CODE = "typeCode";
  String ITEM_GROUP_CODE = "itemGroupCode";
  String ITEM_GROUP_NAME = "itemGroupName";
  String PRODUCT_MANAGER_CODE = "productManagerCode";
  String PRODUCT_MANAGER_NAME = "productManagerName";
  String OLD_ITEM_REFERENCE = "oldItemNumber";
  String PURCHASING_UNIT_NAMES = "purchasingUnitNames";
  String COST_FACTOR = "costFactor";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME);
}
