package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IIObVendorInvoiceDetailsGeneralModel {
	String PURCHASING_UNIT_NAME = "purchaseUnitNameEn";
	BasicAttribute purchaseUnitNameAttr = new BasicAttribute(
			PURCHASING_UNIT_NAME);

	String INVOICE_CODE = "invoiceCode";
	String INVOICE_NUMBER = "invoiceNumber";
	String VENDOR_CODE = "vendorCode";
	String VENDOR_NAME = "vendorName";
	String PURCHASE_ORDER_CODE = "purchaseOrderCode";
	String CURRENCY = "currency";
	String CURRENCY_ISO = "currencyISO";
	String PAYMENT_TERM = "paymentTerm";
	String INVOICE_DATE = "invoiceDate";
	String DOWN_PAYMENT = "downPayment";
	String PURCHASE_UNIT_CODE = "purchaseUnitCode";
	String PURCHASE_UNIT_NAME = "purchaseUnitName";
}
