package com.ebs.dda.jpa.inventory.storetransaction;

import javax.persistence.*;

@Entity
@Table(name = "TObGoodsReceiptStoreTransaction")
@DiscriminatorValue("GR")
public class TObGoodsReceiptStoreTransaction extends TObStoreTransaction {
  public TObGoodsReceiptStoreTransaction() {}

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }

  public Long getDocumentReferenceId() {
    return documentReferenceId;
  }
}
