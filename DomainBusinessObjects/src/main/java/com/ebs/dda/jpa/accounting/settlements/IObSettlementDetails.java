package com.ebs.dda.jpa.accounting.settlements;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

@Entity
@Table(name = "IObSettlementDetails")
public class IObSettlementDetails extends DocumentHeader {

  private static final long serialVersionUID = 1L;

  private Long currencyId;

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }
}
