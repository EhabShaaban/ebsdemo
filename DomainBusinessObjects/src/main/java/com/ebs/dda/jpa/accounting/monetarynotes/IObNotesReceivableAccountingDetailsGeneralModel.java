package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObNotesReceivableAccountingDetailsGeneralModel")
public class IObNotesReceivableAccountingDetailsGeneralModel
    extends IObAccountingDocumentAccountingDetailsGeneralModel {
  @Override
  public String getSysName() {
    return IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME;
  }
}
