package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(IDObInitialBalanceUpload.INITIAL_STOCK_UPLOAD_KEY)
public class IObInitialBalanceUploadActivationDetails extends IObAccountingDocumentActivationDetails {
}
