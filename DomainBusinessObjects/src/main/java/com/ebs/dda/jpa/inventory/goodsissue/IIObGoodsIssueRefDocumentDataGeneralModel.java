package com.ebs.dda.jpa.inventory.goodsissue;

public interface IIObGoodsIssueRefDocumentDataGeneralModel {
    String GOODS_ISSUE_CODE = "goodsIssueCode";

    String CUSTOMER_NAME = "customerName";
    String CUSTOMER_CODE = "customerCode";

    String ADDRESS = "address";

    String CONTACT_PERSON_NAME_EN = "contactPersonNameEn";

  String SALES_REPRESENTATIVE_CODE = "salesRepresentativeCode";
  String SALES_REPRESENTATIVE_NAME = "salesRepresentativeName";

  String REF_DOCUMENT = "refDocument";
  String KAP_CODE = "kapCode";
  String KAP_NAME = "kapName";

  String COMMENTS = "comments";
}
