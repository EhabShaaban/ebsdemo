package com.ebs.dda.jpa.accounting.landedcost;

public interface IIObLandedCostFactorItemValueObject {

  String ITEM_CODE = "itemCode";
  String ESTIMATED_VALUE = "value";
}
