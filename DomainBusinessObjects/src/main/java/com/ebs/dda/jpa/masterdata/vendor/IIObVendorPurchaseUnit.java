package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IIObVendorPurchaseUnit {

  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  BasicAttribute purchaseUnitNameAttr =
      new BasicAttribute(PURCHASING_UNIT_NAME);
}
