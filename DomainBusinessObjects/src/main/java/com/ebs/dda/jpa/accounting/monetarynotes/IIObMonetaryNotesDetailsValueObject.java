package com.ebs.dda.jpa.accounting.monetarynotes;

public interface IIObMonetaryNotesDetailsValueObject {

  String NOTES_RECEIVABLE_CODE = "notesReceivableCode";
  String NOTE_NUMBER = "noteNumber";
  String NOTE_BANK = "noteBank";
  String DUE_DATE = "dueDate";
  String DEPOT_CODE = "depotCode";
}
