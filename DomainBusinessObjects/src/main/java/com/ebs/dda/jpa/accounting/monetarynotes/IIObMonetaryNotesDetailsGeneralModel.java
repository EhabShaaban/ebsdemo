package com.ebs.dda.jpa.accounting.monetarynotes;

public interface IIObMonetaryNotesDetailsGeneralModel {

  String USER_CODE = "userCode";
  String OBJECT_TYPE_CODE = "objectTypeCode";
  String AMOUNT = "amount";
  String DUE_DATE = "dueDate";
  String NOTE_NUMBER = "noteNumber";
  String NOTE_BANK = "noteBank";
  String DEPOT_CODE = "depotCode";
  String DEPOT_NAME = "depotName";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String CURRENCY_CODE = "currencyCode";
  String CURRENCY_ISO = "currencyIso";
  String REMAINING = "remaining";
  String NOTE_FORM = "noteForm";
}
