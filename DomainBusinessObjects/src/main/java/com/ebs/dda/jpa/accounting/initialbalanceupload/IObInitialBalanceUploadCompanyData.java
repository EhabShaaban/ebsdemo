package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("InitialBalanceUpload")
public class IObInitialBalanceUploadCompanyData extends IObAccountingDocumentCompanyData {

  private static final long serialVersionUID = 1L;

}
