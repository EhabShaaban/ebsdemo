package com.ebs.dda.jpa.accounting.monetarynotes;

public interface IIObNotesReceivablesReferenceDocumentGeneralModel {

  String NOTES_RECEIVABLE_CODE = "notesReceivableCode";
  String OBJECT_TYPE_CODE = "objectTypeCode";
  String DOCUMENT_TYPE = "documentType";
  String AMOUNT_TO_COLLECT = "amountToCollect";
  String REF_DOCUMENT_CODE = "refDocumentCode";
  String REF_DOCUMENT_NAME = "refDocumentName";
}
