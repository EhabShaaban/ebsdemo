package com.ebs.dda.jpa.accounting.costing;

public interface IIObCostingAccountingDetailsGeneralModel {

  String ACCOUNTING_ENTRY="accountingEntry" ;

  String GL_ACCOUNT_CODE="glAccountCode";

  String GL_ACCOUNT_NAME="glAccountName" ;

  String GL_SUB_ACCOUNT_CODE="glSubAccountCode";

  String AMOUNT="amount" ;

  String CURRENCY_ISO = "currencyISO";

}