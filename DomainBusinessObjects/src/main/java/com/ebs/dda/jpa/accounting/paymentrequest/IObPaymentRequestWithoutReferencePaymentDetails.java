package com.ebs.dda.jpa.accounting.paymentrequest;


import java.io.Serializable;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue(DueDocumentTypeEnum.NONE)
public class IObPaymentRequestWithoutReferencePaymentDetails extends IObPaymentRequestPaymentDetails implements Serializable {

}
