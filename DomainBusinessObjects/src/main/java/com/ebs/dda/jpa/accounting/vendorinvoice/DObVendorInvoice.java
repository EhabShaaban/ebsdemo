package com.ebs.dda.jpa.accounting.vendorinvoice;


import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dobvendorInvoice")
@EntityInterface(IDObVendorInvoice.class)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class DObVendorInvoice extends DObAccountingDocument implements Serializable {

  @Override
  public String getSysName() {
    return IDObVendorInvoice.SYS_NAME;
  }

  @Transient
  public String getDiscriminatorValue() {
    return this.getClass().getAnnotation(DiscriminatorValue.class).value();
  }
}
