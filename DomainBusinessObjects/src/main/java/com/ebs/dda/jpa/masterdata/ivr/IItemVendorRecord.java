package com.ebs.dda.jpa.masterdata.ivr;

import com.ebs.dac.dbo.processing.BasicAttribute;
import com.ebs.dda.jpa.masterdata.item.ICObMaterial;

public interface IItemVendorRecord extends ICObMaterial {

  String SYS_NAME = "ItemVendorRecord";

  String PURCHASING_UNIT_NAME = "purchaseUnitName";

  BasicAttribute purchaseUnitNameAttr =
           new BasicAttribute(PURCHASING_UNIT_NAME);
}
