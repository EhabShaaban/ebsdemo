package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObSalesOrderCycleDates")
public class IObSalesOrderCycleDates extends DocumentHeader implements Serializable {

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime approvalDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime cancellationDate;

  public DateTime getApprovalDate() {
    return approvalDate;
  }

  public void setApprovalDate(DateTime approvalDate) {
    this.approvalDate = approvalDate;
  }

  public DateTime getCancellationDate() {
    return cancellationDate;
  }

  public void setCancellationDate(DateTime cancellationDate) {
    this.cancellationDate = cancellationDate;
  }
}
