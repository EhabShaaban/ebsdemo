package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObAccountingNoteDetails")
public class IObAccountingNoteDetails extends DocumentHeader {

  private Long businessPartnerId;
  private Long currencyId;
  private Long refDocumentId;
  private Double amount;

  public void setBusinessPartnerId(Long businessPartnerId) {
    this.businessPartnerId = businessPartnerId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public void setRefDocumentId(Long refDocumentId) {
    this.refDocumentId = refDocumentId;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  @Override
  public String getSysName() {
    return IDObAccountingNote.SYS_NAME;
  }
}
