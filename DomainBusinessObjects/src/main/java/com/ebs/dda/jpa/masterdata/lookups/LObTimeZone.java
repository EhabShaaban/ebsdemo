/** */
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.geolocale.LObGeoLocale;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 24, 2016 2:57:04 PM
 */
@Entity
@Table(name = "utcoffset")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("5")
public class LObTimeZone extends LObGeoLocale implements Serializable {

  public static final byte UTC_OFFSET_CODE = 10;
  public static final String UTC_OFFSET_NAME = "utcOffset";
  private static final long serialVersionUID = 1L;

  @Column(name = "utcOffset")
  private String utcOffset;

  public String getUtcOffset() {
    return utcOffset;
  }

  public void setUtcOffset(String utcOffset) {
    this.utcOffset = utcOffset;
  }

  @Override
  public String getSysName() {
    return "TZ";
  }
}
