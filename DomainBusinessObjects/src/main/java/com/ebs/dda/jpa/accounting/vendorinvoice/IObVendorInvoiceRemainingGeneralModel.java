package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObVendorInvoiceRemainingGeneralModel")
@EntityInterface(IIObVendorInvoiceRemainingGeneralModel.class)
public class IObVendorInvoiceRemainingGeneralModel extends DocumentObject<DefaultUserCode> {

    private String vendorCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString vendorName;

    private String vendorCodeName;

    private String invoiceType;

    private String purchaseUnitCode;
    private String purchaseUnitNameEn;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString purchaseUnitName;

    private String companyCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString companyName;

    private String currencyCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString currencyName;

    private String currencyISO;
    private String paymentTermCode;
    private String purchaseOrderCode;
    private String invoiceNumber;

    private BigDecimal remaining;

    public BigDecimal getRemaining() {
        return remaining;
    }

    @Override
    public String getSysName() {
        return IDObVendorInvoice.SYS_NAME;
    }
}
