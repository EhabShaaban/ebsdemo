package com.ebs.dda.jpa.accounting.journalentry;


import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class DObJournalEntryCreateValueObject implements IValueObject {

  private String userCode;
  private String journalEntryCode;

  public String getUserCode() {
    return userCode;
  }

  public void setUserCode(String userCode) {
    this.userCode = userCode;
  }

  public String getJournalEntryCode() {
    return journalEntryCode;
  }

  public void setJournalEntryCode(String journalEntryCode) {
    this.journalEntryCode = journalEntryCode;
  }
}
