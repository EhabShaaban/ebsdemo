package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCollectionAccountingDetailsGeneralModel")
public class IObCollectionAccountingDetailsGeneralModel
				extends IObAccountingDocumentAccountingDetailsGeneralModel {

	@Override
	public String getSysName() {
		return IDObCollection.SYS_NAME;
	}
}
