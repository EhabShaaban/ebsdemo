package com.ebs.dda.jpa.masterdata.lookups;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 4, 2018 10:09:28 AM
 */
public interface IMasterDataLookupsSysNames {

  String LOB_ADDRESS_FORMAT_SYS_NAME = "lobAddressFormat";
  String LOB_ATTACHMENT_TYPE_SYS_NAME = "lobAttachmentType";
  String LOB_BASIC_MATERIAL_SYS_NAME = "lobBasicMaterial";
  String LOB_CONTACT_TYPE_SYS_NAME = "lobContactType";
  String LOB_CONTAINER_REQUIRMENT_SYS_NAME = "ContainerType";
  String LOB_DATE_FORMAT_SYS_NAME = "lobDateFormat";
  String LOB_DECIMAL_FORMAT_SYS_NAME = "lobDecimalFormat";
  String LOB_DIVISION_SYS_NAME = "lobDivision";
  String LOB_DOWN_PAYMENT_DUE_OPTIONS_SYS_NAME = "lobDownPaymentDueOptions";
  String LOB_EXTERNAL_MATERIAL_GROUP_SYS_NAME = "lobExternalMaterialGroup";
  String LOB_INCOTERMS_SYS_NAME = "Incoterm";
  String LOB_LAB_DESIGN_OFFICE_SYS_NAME = "lobLabDesignOffice";
  String LOB_LANGUAGE_SYS_NAME = "lobLanguage";
  String LOB_MATERIAL_ABC_INDECATOR_SYS_NAME = "lobMaterialABCIndecator";
  String LOB_MATERIAL_BUSINESS_OPERATION_SYS_NAME = "lobMaterialBusinessOperation";
  String LOB_MATERIAL_VIEW_SYS_NAME = "lobMaterialView";
  String LOB_PAYMENT_METHOD_SYS_NAME = "lobPaymentMethod";
  String LOB_PAYMENT_TERMS_DEFAULT_BASE_SELINE_SYS_NAME = "lobPaymentTermsDefaultBaseLine";
  String LOB_PORT_SYS_NAME = "Port";
  String LOB_POSITION_SYS_NAME = "lobPosition";
  String LOB_STORAGE_CONDITION_SYS_NAME = "lobStorageCondition";
  String LOB_SUBDIVISION_CATEGORY_SYS_NAME = "lobSubdivisionCategory";
  String LOB_TEMPERATURE_CONDITION_SYS_NAME = "lobTemperatureCondition";
  String LOB_TIME_ZONE_SYS_NAME = "lobTimeZone";
  String LOB_UOM_DIMENSION_SYS_NAME = "lobUnitOfMeasureDimension";
  String LOB_VALUATION_AREA_SYS_NAME = "lobValuationArea";
  String LOB_ENTERPRISE_CONTACT_TYPE_SYS_NAME = "lobEnterpriseContactType";
  String LOB_REASON = "Reason";
  String LOB_DEPOT = "Depot";
  String LOB_ISSUE_FROM = "IssueFrom";
}
