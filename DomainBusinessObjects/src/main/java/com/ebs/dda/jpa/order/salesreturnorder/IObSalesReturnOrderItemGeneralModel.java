package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesReturnOrderItemGeneralModel")
public class IObSalesReturnOrderItemGeneralModel extends InformationObject {

  private String salesReturnOrderCode;

  private String itemCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;

  private String orderUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString orderUnitName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString orderUnitSymbol;

  private String returnReasonCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString returnReasonName;

  private BigDecimal returnQuantity;

  private BigDecimal maxReturnQuantity;

  private BigDecimal salesPrice;

  private BigDecimal totalAmount;

  public String getSalesReturnOrderCode() {
    return salesReturnOrderCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getOrderUnitCode() {
    return orderUnitCode;
  }

  public LocalizedString getOrderUnitName() {
    return orderUnitName;
  }

  public LocalizedString getOrderUnitSymbol() {
    return orderUnitSymbol;
  }

  public String getReturnReasonCode() {
    return returnReasonCode;
  }

  public LocalizedString getReturnReasonName() {
    return returnReasonName;
  }

  public BigDecimal getReturnQuantity() {
    return returnQuantity;
  }

  public BigDecimal getMaxReturnQuantity() {
    return maxReturnQuantity;
  }

  public BigDecimal getSalesPrice() {
    return salesPrice;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
