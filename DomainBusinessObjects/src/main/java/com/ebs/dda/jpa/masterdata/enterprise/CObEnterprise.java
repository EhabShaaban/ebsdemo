package com.ebs.dda.jpa.masterdata.enterprise;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "CObEnterprise")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class CObEnterprise extends ConfigurationObject<DefaultUserCode>
    implements IAggregateAwareEntity, Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "description")
  private String description;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  public CObEnterprise() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
