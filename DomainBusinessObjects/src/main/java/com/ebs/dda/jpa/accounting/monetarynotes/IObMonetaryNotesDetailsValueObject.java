package com.ebs.dda.jpa.accounting.monetarynotes;

public class IObMonetaryNotesDetailsValueObject {

  private String notesReceivableCode;
  private String noteNumber;
  private String noteBank;
  private String dueDate;
  private String depotCode;

  public String getNotesReceivableCode() {
    return notesReceivableCode;
  }

  public void setNotesReceivableCode(String notesReceivableCode) {
    this.notesReceivableCode = notesReceivableCode;
  }

  public String getNoteNumber() {
    return noteNumber;
  }

  public void setNoteNumber(String noteNumber) {
    this.noteNumber = noteNumber;
  }

  public String getNoteBank() {
    return noteBank;
  }

  public void setNoteBank(String noteBank) {
    this.noteBank = noteBank;
  }

  public String getDueDate() {
    return dueDate;
  }

  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

  public String getDepotCode() {
    return depotCode;
  }

  public void setDepotCode(String depotCode) {
    this.depotCode = depotCode;
  }
}
