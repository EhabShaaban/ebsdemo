package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptReceivedItemsDifferenceReasonValueObject {

  String DIFFERENCE_REASON = "differenceReason";
}
