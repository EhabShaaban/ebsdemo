package com.ebs.dda.jpa.masterdata.measure;

import com.ebs.dac.dbo.processing.EmbeddedAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 27, 2016
 */
public interface IHObMeasure {
  String NAME_ATTR = "name";
  byte NAME_CODE = 10;
  EmbeddedAttribute<LocalizedString> nameAttr =
      new EmbeddedAttribute<>(NAME_ATTR, LocalizedString.class);
}
