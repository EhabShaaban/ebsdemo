package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 2:16:51 PM
 */
@Entity
@Table(name = "LocalizedLObMaterial")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class LObMaterial extends LocalizedLookupObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
