package com.ebs.dda.jpa.accounting;

import java.math.BigDecimal;

public class DObInvoiceItemValueObject {

  private String invoiceCode;
  private Long itemId;
  private String itemCode;
  private BigDecimal quantityInOrderUnit;
  private String orderUnitCode;
  private BigDecimal price;

  public String getInvoiceCode() {
    return invoiceCode;
  }

  public void setInvoiceCode(String invoiceCode) {
    this.invoiceCode = invoiceCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public BigDecimal getQuantityInOrderUnit() {
    return quantityInOrderUnit;
  }

  public void setQuantityInOrderUnit(BigDecimal quantityInOrderUnit) {
    this.quantityInOrderUnit = quantityInOrderUnit;
  }

  public String getOrderUnitCode() {
    return orderUnitCode;
  }

  public void setOrderUnitCode(String orderUnitCode) {
    this.orderUnitCode = orderUnitCode;
  }

  public BigDecimal getUnitPrice() {
    return price;
  }

  public void setUnitPrice(BigDecimal unitPrice) {
    this.price = unitPrice;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }
}
