package com.ebs.dda.jpa.masterdata.enterprise;

public interface IIObContactDetails {

  String CONTACT_TYPE_ATTR = "contactTypeId";
  String CONTACT_VALUE_ATTR = "contactValue";
}
