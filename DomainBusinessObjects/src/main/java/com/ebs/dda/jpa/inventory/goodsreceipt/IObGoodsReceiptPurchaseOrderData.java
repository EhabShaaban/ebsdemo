package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;

@Entity
@EntityInterface(IIObGoodsReceiptPurchaseOrderData.class)
public class IObGoodsReceiptPurchaseOrderData extends DocumentHeader {

  private static final long serialVersionUID = 1L;
  private Long purchaseOrderId;
  private Long vendorId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString vendorName;

  private String deliveryNote;

  private Long purchaseResponsibleId;

  private String purchaseResponsibleName;

  private String comments;

  public Long getPurchaseOrderId() {
    return purchaseOrderId;
  }

  public void setPurchaseOrderId(Long purchaseOrderId) {
    this.purchaseOrderId = purchaseOrderId;
  }

  public Long getVendorId() {
    return vendorId;
  }

  public void setVendorId(Long vendorId) {
    this.vendorId = vendorId;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public void setVendorName(LocalizedString vendorName) {
    this.vendorName = vendorName;
  }

  public String getDeliveryNote() {
    return deliveryNote;
  }

  public void setDeliveryNote(String deliveryNote) {
    this.deliveryNote = deliveryNote;
  }

  public Long getPurchaseResponsibleId() {
    return purchaseResponsibleId;
  }

  public void setPurchaseResponsibleId(Long purchaseResponsibleId) {
    this.purchaseResponsibleId = purchaseResponsibleId;
  }

  public String getPurchaseResponsibleName() {
    return purchaseResponsibleName;
  }

  public void setPurchaseResponsibleName(String purchaseResponsibleName) {
    this.purchaseResponsibleName = purchaseResponsibleName;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  @Override
  public String getSysName() {
    return "IObGoodsReceiptPurchaseOrderData";
  }
}
