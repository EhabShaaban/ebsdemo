package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObVendorInvoiceDetailsDownPayment")
public class IObVendorInvoiceDetailsDownPayment extends DocumentLine
		implements
			Serializable {

	private Long downPaymentId;

	private BigDecimal downPaymentAmount;

	public Long getDownPaymentId() {
		return downPaymentId;
	}

	public void setDownPaymentId(Long downPaymentId) {
		this.downPaymentId = downPaymentId;
	}

	public BigDecimal getDownPaymentAmount() {
		return downPaymentAmount;
	}

	public void setDownPaymentAmount(BigDecimal downPaymentAmount) {
		this.downPaymentAmount = downPaymentAmount;
	}
}
