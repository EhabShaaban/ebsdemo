package com.ebs.dda.jpa.order.salesorder;

public class IObSalesOrderDataValueObject {
  private String salesOrderCode;
  private String paymentTermCode;
  private Long customerAddressId;
  private String currencyCode;
  private Long contactPersonId;
  private String expectedDeliveryDate;
  private String notes;

  public String getSalesOrderCode() {
    return salesOrderCode;
  }

  public void setSalesOrderCode(String salesOrderCode) {
    this.salesOrderCode = salesOrderCode;
  }

  public void setPaymentTermCode(String paymentTermCode) {
    this.paymentTermCode = paymentTermCode;
  }

  public void setCustomerAddressId(Long customerAddressId) {
    this.customerAddressId = customerAddressId;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public void setContactPersonId(Long contactPersonId) {
    this.contactPersonId = contactPersonId;
  }

  public void setExpectedDeliveryDate(String expectedDeliveryDate) {
    this.expectedDeliveryDate = expectedDeliveryDate;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public Long getCustomerAddressId() {
    return customerAddressId;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public Long getContactPersonId() {
    return contactPersonId;
  }

  public String getExpectedDeliveryDate() {
    return expectedDeliveryDate;
  }

  public String getNotes() {
    return notes;
  }
}
