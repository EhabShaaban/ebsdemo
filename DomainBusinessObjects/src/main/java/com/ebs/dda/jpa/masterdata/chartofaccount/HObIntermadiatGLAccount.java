package com.ebs.dda.jpa.masterdata.chartofaccount;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("2")
public class HObIntermadiatGLAccount extends HObGLAccount {
}
