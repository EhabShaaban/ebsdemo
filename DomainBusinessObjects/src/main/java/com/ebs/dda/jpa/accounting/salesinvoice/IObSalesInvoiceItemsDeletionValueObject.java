package com.ebs.dda.jpa.accounting.salesinvoice;

public class IObSalesInvoiceItemsDeletionValueObject {

  private String salesInvoiceCode;
  private Long itemId;

  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public void setSalesInvoiceCode(String salesInvoiceCode) {
    this.salesInvoiceCode = salesInvoiceCode;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }
}
