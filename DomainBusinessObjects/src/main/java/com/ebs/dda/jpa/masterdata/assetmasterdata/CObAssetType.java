package com.ebs.dda.jpa.masterdata.assetmasterdata;

import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObAssetType")
public class CObAssetType extends ConfigurationObject<DefaultUserCode> implements Serializable {

  @Override
  public String getSysName() {
    return IMObAsset.SYS_NAME;
  }
}
