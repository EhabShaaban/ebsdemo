package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.jpa.entities.businessobjects.SimpleLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;
import java.io.Serializable;

/** Created by yara on 11/2/17. */
@Entity
@Table(name = "lobsimplematerialdata")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class LObSimpleMaterial extends SimpleLookupObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
