package com.ebs.dda.jpa.accounting.journalentry;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObJournalEntry {

  static final String SYS_NAME = "JournalEntry";

  String JOURNAL_ITEM_ATTR_NAME = "journalItem";
  CompositeAttribute<IObJournalEntryItem> journalItemAttr =
      new CompositeAttribute<>(JOURNAL_ITEM_ATTR_NAME, IObJournalEntryItem.class,
          true);

}
