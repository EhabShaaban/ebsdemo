package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IMObVendorPurchaseUnitGeneralModel {

  String SYS_NAME = "Vendor";

  byte PURCHASING_UNIT_NAME_CODE = 10;
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  BasicAttribute purchaseUnitNameAttr =
      new BasicAttribute(PURCHASING_UNIT_NAME);
}
