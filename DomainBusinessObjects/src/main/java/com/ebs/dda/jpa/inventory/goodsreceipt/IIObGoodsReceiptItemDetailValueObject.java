package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptItemDetailValueObject {
  String ITEM_DETAIL = "itemDetail";
  String RECEIVED_QTY_UOE = "receivedQtyUoE";
    String NOTES = "notes";
    String STOCK_TYPE = "stockType";
  String UOE_CODE = "unitOfEntryCode";
  String BATCH_CODE = "batchCode";
  String PRODUCTION_DATE = "productionDate";
  String EXPIRATION_DATE = "expirationDate";
}
