package com.ebs.dda.jpa.order.salesreturnorder;

public interface IDObSalesReturnOrderCreateValueObject {
  String TYPE_CODE = "typeCode";
  String BUSINESS_UNIT_CODE = "businessUnitCode";
  String CUSTOMER_CODE = "customerCode";
  String SALES_INVOICE_CODE = "salesInvoiceCode";
  String DOCUMENT_OWNER_ID = "documentOwnerId";

}
