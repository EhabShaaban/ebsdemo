package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

@Entity
@Table(name = "IObLandedCostAccountingDetailsGeneralModel")
public class IObLandedCostAccountingDetailsGeneralModel
				extends IObAccountingDocumentAccountingDetailsGeneralModel {
	@Override
	public String getSysName() {
		return "LandedCostAccountingDetails";
	}
}
