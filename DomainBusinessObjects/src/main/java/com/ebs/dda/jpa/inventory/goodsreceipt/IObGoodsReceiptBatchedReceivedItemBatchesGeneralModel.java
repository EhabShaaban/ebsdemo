package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel")
@EntityInterface(IIObGoodsReceiptBatchedReceivedItemBatchesGeneralModel.class)
public class IObGoodsReceiptBatchedReceivedItemBatchesGeneralModel {

  @Id private Long id;
  private Long batchedItemId;
  private String batchCode;
  private BigDecimal receivedQtyUoE;
  private BigDecimal receivedQtyBase;
  private String stockType;
  private String notes;

  private String goodsReceiptCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString baseUnitSymbol;

  private String itemCode;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime productionDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime expirationDate;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString unitOfEntry;

  private String unitOfEntryCode;

  private BigDecimal baseUnitFactor;

  private String itemCodeAtVendor;

  private BigDecimal estimatedCost;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public LocalizedString getBaseUnitSymbol() {
    return baseUnitSymbol;
  }

  public String getItemCode() {
    return itemCode;
  }

  public String getBatchCode() {
    return batchCode;
  }

  public BigDecimal getReceivedQtyUoE() {
    return receivedQtyUoE;
  }

  public DateTime getProductionDate() {
    return productionDate;
  }

  public DateTime getExpirationDate() {
    return expirationDate;
  }

  public String getStockType() {
    return stockType;
  }

  public String getNotes() {
    return notes;
  }

  public LocalizedString getUnitOfEntry() {
    return unitOfEntry;
  }

  public BigDecimal getReceivedQtyBase() {
    return receivedQtyBase;
  }

  public Long getBatchedItemId() {
    return batchedItemId;
  }

  public String getUnitOfEntryCode() {
    return unitOfEntryCode;
  }

  public BigDecimal getEstimatedCost() {
    return estimatedCost;
  }
}
