package com.ebs.dda.jpa.order.salesreturnorder;

public interface IIObSalesReturnOrderCompanyDataGeneralModel {
  String BUSINESS_UNIT_CODE = "businessUnitCode";
  String BUSINESS_UNIT_NAME = "businessUnitName";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
}
