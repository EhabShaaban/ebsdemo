package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import java.io.Serializable;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.joda.time.DateTime;

@Entity
@Table(name = "IObVendorInvoiceDetails")
public class IObVendorInvoiceDetails extends DocumentHeader implements Serializable {

	private String invoiceNumber;

	@Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
	private DateTime invoiceDate;

	private Long vendorId;

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public DateTime getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(DateTime invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

}