package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author: Menna Sayed
 * @date: Jan 10, 2017
 */
@Embeddable
public class MaterialStatusRestrictionEnum extends BusinessEnum {
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  @Column(name = "restriction")
  private MaterialStatusRestriction value;

  public MaterialStatusRestrictionEnum() {}

  public MaterialStatusRestrictionEnum(MaterialStatusRestriction id) {
    this.value = id;
  }

  public MaterialStatusRestriction getId() {
    return value;
  }

  public void setId(MaterialStatusRestriction id) {
    this.value = id;
  }

  public enum MaterialStatusRestriction {
    ALLOWED_WITH_WARNING,
    NOT_ALLOWED
  }
}
