package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author: Menna Sayed
 * @date: Jan 11, 2017
 */
@Entity
@Table(name = "CObMaterialStatus")
@DiscriminatorValue("2")
@EntityInterface(ICObMaterialStatus.class)
public class CObMaterialStatus extends CObMaterial implements Serializable {

  private static final long serialVersionUID = 1L;

  private String description;

  public CObMaterialStatus() {}

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getSysName() {
    return ICObMaterialStatus.SYS_NAME;
  }
}
