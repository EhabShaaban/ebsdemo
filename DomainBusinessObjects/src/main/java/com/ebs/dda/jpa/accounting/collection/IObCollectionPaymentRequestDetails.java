package com.ebs.dda.jpa.accounting.collection;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObCollectionPaymentRequestDetails")
@DiscriminatorValue("5")
public class IObCollectionPaymentRequestDetails extends IObCollectionDetails implements Serializable {
  private Long paymentRequestId;

  public Long getPaymentRequestId() {
    return paymentRequestId;
  }

  public void setPaymentRequestId(Long paymentRequestId) {
    this.paymentRequestId = paymentRequestId;
  }
}
