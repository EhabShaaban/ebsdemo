package com.ebs.dda.jpa.accounting.vendorinvoice;

public interface IDObVendorInvoiceDetailsValueObject {
	  String INVOICE_NUMBER = "invoiceNumber";
	  String CURRENCY_CODE = "currencyCode";
	  String PAYMENT_TERM_CODE = "paymentTermCode";
	  String INVOICE_DATE = "invoiceDate";
	  String DOWN_PAYMENT = "downPayment";
	  String TAX = "tax";
}
