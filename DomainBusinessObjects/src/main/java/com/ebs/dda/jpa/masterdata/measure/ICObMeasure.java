package com.ebs.dda.jpa.masterdata.measure;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.dbo.processing.EmbeddedAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 27, 2016
 */
public interface ICObMeasure extends IStatefullBusinessObject {

  String SYS_NAME = "Measures";

  String SYMBOl_ATTR = "symbol";
  byte SYMBOl_CODE = 20;
  EmbeddedAttribute<LocalizedString> symbolAttr =
      new EmbeddedAttribute<>(SYMBOl_ATTR, LocalizedString.class);
}
