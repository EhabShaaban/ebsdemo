package com.ebs.dda.jpa.inventory.goodsreceipt;

public class DObGoodsReceiptCalculateEstimatedCostValueObject {

  private String goodsReceiptCode;

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }
}
