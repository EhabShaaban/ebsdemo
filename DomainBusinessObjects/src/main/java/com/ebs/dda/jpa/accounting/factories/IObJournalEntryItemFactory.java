package com.ebs.dda.jpa.accounting.factories;

import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItem;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemBankSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemCustomerSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemNotesReceivableSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemPurchaseOrderSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemTaxesSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemTreasurySubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemUnrealizedCurrencyGainLossSubAccount;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemVendorSubAccount;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;

public class IObJournalEntryItemFactory {

	public static final String PO = SubLedgers.PO.toString();
	public static final String BANKS = SubLedgers.Banks.toString();
	public static final String LOCAL_CUSTOMER = SubLedgers.Local_Customer.toString();
	public static final String TAXES = SubLedgers.Taxes.toString();
	public static final String LOCAL_VENDORS = SubLedgers.Local_Vendors.toString();
	public static final String IMPORT_VENDORS = SubLedgers.Import_Vendors.toString();
	public static final String NOTES_RECEIVABLE = SubLedgers.Notes_Receivables.toString();;
	public static final String TREASURIES = SubLedgers.Treasuries.toString();
	public static final String UNREALIZED_CURRENCY_GAIN_LOSS = SubLedgers.UnrealizedCurrencyGainLoss
					.toString();

	private IObJournalEntryItemFactory() {
	}

	public static IObJournalEntryItem initJournalEntryItemInstance(String ledger) {
		if (ledger == null || ledger.equals(SubLedgers.NONE.name())) {
			return new IObJournalEntryItem();
		}
		if (ledger.equals(BANKS)) {

			return new IObJournalEntryItemBankSubAccount();
		}
		if (ledger.equals(LOCAL_CUSTOMER)) {

			IObJournalEntryItemCustomerSubAccount customerSubAccount = new IObJournalEntryItemCustomerSubAccount();
			customerSubAccount.setType(SubLedgers.Local_Customer.name());
			return customerSubAccount;
		}
		if (ledger.equals(PO)) {
			return new IObJournalEntryItemPurchaseOrderSubAccount();
		}
		if (ledger.equals(TREASURIES)) {

			return new IObJournalEntryItemTreasurySubAccount();
		}
		if (ledger.equals(UNREALIZED_CURRENCY_GAIN_LOSS)) {
			return new IObJournalEntryItemUnrealizedCurrencyGainLossSubAccount();
		}
		if (ledger.equals(NOTES_RECEIVABLE)) {

			return new IObJournalEntryItemNotesReceivableSubAccount();
		}
		if (ledger.equals(LOCAL_VENDORS) || ledger.equals(IMPORT_VENDORS)) {
			IObJournalEntryItemVendorSubAccount journalEntryItemVendorSubAccount = new IObJournalEntryItemVendorSubAccount();
			journalEntryItemVendorSubAccount.setType(ledger);
			return journalEntryItemVendorSubAccount;
		}
		if (ledger.equals(TAXES)) {

			return new IObJournalEntryItemTaxesSubAccount();
		}
		throw new IllegalArgumentException(
						String.format("Ledger type '%s' is not supported", ledger));
	}

}
