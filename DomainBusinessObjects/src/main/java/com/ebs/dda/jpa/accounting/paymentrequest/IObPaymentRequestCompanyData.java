package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "PaymentRequest")
public class IObPaymentRequestCompanyData extends IObAccountingDocumentCompanyData {
    private static final long serialVersionUID = 1L;

}
