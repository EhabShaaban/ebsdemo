package com.ebs.dda.jpa.order.salesorder;

public class DObSalesOrderCreateValueObject {
  private String typeCode;
  private String businessUnitCode;
  private String companyCode;
  private String customerCode;
  private String salesResponsibleCode;

  public String getTypeCode() {
    return typeCode;
  }

  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public void setCustomerCode(String customerCode) {
    this.customerCode = customerCode;
  }

  public String getSalesResponsibleCode() {
    return salesResponsibleCode;
  }

  public void setSalesResponsibleCode(String salesResponsibleCode) {
    this.salesResponsibleCode = salesResponsibleCode;
  }
}
