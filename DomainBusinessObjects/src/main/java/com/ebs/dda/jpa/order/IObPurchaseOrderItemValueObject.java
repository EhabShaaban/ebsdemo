package com.ebs.dda.jpa.order;

import java.math.BigDecimal;

public class IObPurchaseOrderItemValueObject {
  private String purchaseOrderCode;
  private String itemCode;
  private String unitOfMeasureCode;
  private BigDecimal quantity;
  private BigDecimal price;

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public void setUnitOfMeasureCode(String unitOfMeasureCode) {
    this.unitOfMeasureCode = unitOfMeasureCode;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}
