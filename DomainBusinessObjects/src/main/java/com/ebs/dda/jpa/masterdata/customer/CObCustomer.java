package com.ebs.dda.jpa.masterdata.customer;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

@Entity
@Table(name = "cobcustomer")
public class CObCustomer extends ConfigurationObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final String SYS_NAME = "CObCustomer";


  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  @Override
  public String getSysName() {
    return SYS_NAME;
  }
}
