package com.ebs.dda.jpa.inventory.stockavailabilities;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;

import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class IObItemQuantityGeneralModel extends BusinessObject {

    private String userCode;

    private long itemId;
    private String itemCode;
    private long unitOfMeasureId;
    private String unitOfMeasureCode;
    private long purchaseUnitId;
    private String purchaseUnitCode;
    private long companyId;
    private String companyCode;
    private long storehouseId;
    private String storeHouseCode;
    private long plantId;
    private String plantCode;
    private BigDecimal quantity;

    public String getUserCode() {
        return userCode;
    }

    public long getItemId() {
        return itemId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public long getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    public String getUnitOfMeasureCode() {
        return unitOfMeasureCode;
    }

    public long getPurchaseUnitId() {
        return purchaseUnitId;
    }

    public String getPurchaseUnitCode() {
        return purchaseUnitCode;
    }

    public long getCompanyId() {
        return companyId;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public long getStorehouseId() {
        return storehouseId;
    }

    public String getStoreHouseCode() {
        return storeHouseCode;
    }

    public long getPlantId() {
        return plantId;
    }

    public String getPlantCode() {
        return plantCode;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public abstract String getStockType();

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    @Override
    public String getSysName() {
        return "StockAvailabilityUpdatePreparationGeneralModel";
    }
}
