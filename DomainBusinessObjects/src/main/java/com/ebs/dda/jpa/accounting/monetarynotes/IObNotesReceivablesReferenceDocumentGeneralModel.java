package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table(name = "IObNotesReceivablesReferenceDocumentGeneralModel")
@EntityInterface(IIObNotesReceivablesReferenceDocumentGeneralModel.class)
public class IObNotesReceivablesReferenceDocumentGeneralModel
      extends InformationObject {

  private String notesReceivableCode;
  private String objectTypeCode;
  private String documentType;
  private BigDecimal amountToCollect;
  private String refDocumentCode;
  private String currencyIso;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString refDocumentName;

  @Convert(converter = StatefullBusinessObjectCurrentStatesJpaConverter.class)
  private Set<String> currentStates;

  public String getNotesReceivableCode() {
    return notesReceivableCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getDocumentType() {
    return documentType;
  }

  public BigDecimal getAmountToCollect() {
    return amountToCollect;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public LocalizedString getRefDocumentName() {
    return refDocumentName;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public Set<String> getCurrentStates() {
    return currentStates;
  }

  @Override
  public String getSysName() {
    return IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME;
  }
}
