package com.ebs.dda.jpa.masterdata.chartofaccount.enums;

public enum AccountType {
  TYPE_01,
  TYPE_11,
  TYPE_02,
  TYPE_03,
  TYPE_04
}
