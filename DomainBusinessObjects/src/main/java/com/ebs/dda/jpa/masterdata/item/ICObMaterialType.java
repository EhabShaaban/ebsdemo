package com.ebs.dda.jpa.masterdata.item;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 3:47:40 PM
 */
public interface ICObMaterialType extends ICObMaterial {

  String SYS_NAME = "MaterialType";
}
