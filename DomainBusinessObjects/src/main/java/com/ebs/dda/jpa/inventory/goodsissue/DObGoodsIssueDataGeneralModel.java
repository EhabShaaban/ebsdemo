package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObGoodsIssueDataGeneralModel")
public class DObGoodsIssueDataGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

    private static final long serialVersionUID = 1L;
    private String purchaseUnitCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString purchaseUnitName;

    private String itemCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString itemName;

    private String unitOfMeasureCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString unitOfMeasureName;

    private BigDecimal quantity;
    private String batchNo;
    private BigDecimal price;
    private String goodsIssueTypeCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString goodsIssueTypeName;

    private String referenceDocumentCode;
    private String companyCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString companyName;

    private String plantCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString plantName;

    private String storeHouseCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString storeHouseName;

    private String storekeeperCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString storekeeperName;

    private String customerCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString customerName;

    private String address;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString contactPersonName;

    private String salesRepresentativeCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString salesRepresentativeName;

    private String comments;
    private String kapCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString kapName;

    private Float conversionfactor;

    private Long unitOfMeasureId;
    private Long itemId;
    private Long companyId;
    private Long plantId;
    private Long storeHouseId;
    private Long purchaseUnitId;
    private Long goodsIssueId;

    public String getPurchaseUnitCode() {
        return purchaseUnitCode;
    }

    public LocalizedString getPurchaseUnitName() {
        return purchaseUnitName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public LocalizedString getItemName() {
        return itemName;
    }

    public String getUnitOfMeasureCode() {
        return unitOfMeasureCode;
    }

    public LocalizedString getUnitOfMeasureName() {
        return unitOfMeasureName;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public String getGoodsIssueTypeCode() {
        return goodsIssueTypeCode;
    }

    public LocalizedString getGoodsIssueTypeName() {
        return goodsIssueTypeName;
    }

    public String getReferenceDocumentCode() {
        return referenceDocumentCode;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public LocalizedString getCompanyName() {
        return companyName;
    }

    public String getPlantCode() {
        return plantCode;
    }

    public LocalizedString getPlantName() {
        return plantName;
    }

    public String getStoreHouseCode() {
        return storeHouseCode;
    }

    public LocalizedString getStoreHouseName() {
        return storeHouseName;
    }

    public String getStorekeeperCode() {
        return storekeeperCode;
    }

    public LocalizedString getStorekeeperName() {
        return storekeeperName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public LocalizedString getCustomerName() {
        return customerName;
    }

    public String getAddress() {
        return address;
    }

    public LocalizedString getContactPersonName() {
        return contactPersonName;
    }

    public String getSalesRepresentativeCode() {
        return salesRepresentativeCode;
    }

    public LocalizedString getSalesRepresentativeName() {
        return salesRepresentativeName;
    }

    public String getComments() {
        return comments;
    }

    public String getKapCode() {
        return kapCode;
    }

    public LocalizedString getKapName() {
        return kapName;
    }

    public Float getConversionfactor() {
        return conversionfactor;
    }

    public Long getUnitOfMeasureId() {
        return unitOfMeasureId;
    }

    public Long getItemId() {
        return itemId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public Long getPlantId() {
        return plantId;
    }

    public Long getStoreHouseId() {
        return storeHouseId;
    }

    public Long getPurchaseUnitId() {
        return purchaseUnitId;
    }
    
  public Long getGoodsIssueId() {
    return goodsIssueId;
  }

  @Override
  public String getSysName() {
      return IDObGoodsIssue.SYS_NAME;
  }
}
