package com.ebs.dda.jpa;

public interface IBusinessObjectNames {

	String PURCHASE_ORDER = "purchaseorder";
	String ACCOUNTING_NOTE = "accountingnote";
	String SALES_RETURN = "salesreturn";
	String SALES_INVOICE = "salesinvoice";
	String SETTLEMENT = "settlement";
	String COLLECTION = "collection";
	String INITIAL_BALANCE_UPLOAD = "initialbalanceupload";
	String NOTES_RECEIVABLE = "notesreceivable";
	String PAYMENT_REQUEST = "paymentrequest";
	String VENDOR_INVOICE = "vendorinvoice";
	String LANDED_COST = "landedcost";

}
