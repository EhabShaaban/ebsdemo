package com.ebs.dda.jpa.accounting.journalentry;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObJournalEntryItemGeneralModel")
@EntityInterface(IIObJournalEntryItemGeneralModel.class)
public class IObJournalEntryItemGeneralModel extends DocumentLine {

  private String purchaseUnitCode;

  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String code;

  private String subLedger;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String companyCurrencyPrice;
  private String companyGroupCurrencyPrice;

  private String debit;

  private String credit;

  private String accountCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString accountName;

  private String subAccountCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString subAccountName;

  private String documentCurrencyCode;

  private String documentCurrencyName;

  private String companyCurrencyCode;

  private String companyCurrencyName;

  private String companyGroupCurrencyCode;

  private String companyGroupCurrencyName;

  private Long companyExchangeRateId;
  private Long companyGroupExchangeRateId;

  private String referenceType;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getCode() {
    return code;
  }

  public String getSubLedger() {
    return subLedger;
  }

  public DateTime getDueDate() {
    return dueDate;
  }

  public String getCompanyCurrencyPrice() {
    return companyCurrencyPrice;
  }

  public String getDebit() {
    return debit;
  }

  public String getCredit() {
    return credit;
  }

  public String getAccountCode() {
    return accountCode;
  }

  public LocalizedString getAccountName() {
    return accountName;
  }

  public String getSubAccountCode() {
    return subAccountCode;
  }

  public LocalizedString getSubAccountName() {
    return subAccountName;
  }

  public String getDocumentCurrencyCode() {
    return documentCurrencyCode;
  }

  public String getDocumentCurrencyName() {
    return documentCurrencyName;
  }

  public Long getCompanyExchangeRateId() {
    return companyExchangeRateId;
  }

  public String getReferenceType() {
    return referenceType;
  }

  public String getCompanyGroupCurrencyPrice() {
    return companyGroupCurrencyPrice;
  }

  public String getCompanyCurrencyCode() {
    return companyCurrencyCode;
  }

  public String getCompanyCurrencyName() {
    return companyCurrencyName;
  }

  public String getCompanyGroupCurrencyCode() {
    return companyGroupCurrencyCode;
  }

  public String getCompanyGroupCurrencyName() {
    return companyGroupCurrencyName;
  }

  public Long getCompanyGroupExchangeRateId() {
    return companyGroupExchangeRateId;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }
}
