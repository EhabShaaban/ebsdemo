package com.ebs.dda.jpa.accounting;
public interface IIObInvoiceItemsGeneralModel {

  String SYS_NAME = "InvoiceItem";

  String INVOICE_CODE = "invoiceCode";
  String QUANTITY_IN_ORDER_UNIT = "quantityInOrderUnit";
  String TOTAL_AMOUNT = "totalAmount";

  String ITEM_NAME = "itemName";
  String ITEM_CODE = "itemCode";
  String ITEM_PRICE= "price";
  String ITEM_ORDER_UNIT_PRICE= "orderUnitPrice";
  String ITEM_QUANTITY_IN_ORDER_UNIT = "quantityInOrderUnit";

  String ORDER_UNIT_NAME = "orderUnitName";
  String ORDER_UNIT_CODE = "orderUnitCode";
  String ORDER_UNIT_SYMBOL = "orderUnitSymbol";
  String BASE_UNIT_SYMBOL = "baseUnitSymbol";
  String BASE_UNIT_CODE = "baseUnitCode";
}
