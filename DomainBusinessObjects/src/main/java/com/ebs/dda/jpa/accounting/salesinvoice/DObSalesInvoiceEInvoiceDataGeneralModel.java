package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import lombok.Getter;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Entity
@Table(name = "DObSalesInvoiceEInvoiceDataGeneralModel")
public class DObSalesInvoiceEInvoiceDataGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {

    private String companyTaxRegistrationNumber;
    private String companyName;
    private String companyCountry;
    private String companyGovernate;
    private String companyRegion;
    private String companyStreet;
    private String companyBuildingNumber;
    private String companyPostalCode;

    private String customerTaxRegistrationNumber;
    private String customerName;
    private String customerType;
    private String customerCountry;
    private String customerGovernate;
    private String customerRegion;
    private String customerStreet;
    private String customerBuildingNumber;
    private String customerPostalCode;

    @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
    private DateTime journalDate;
    private String taxpayerActivityCode;
    private String invoiceSerialNumber;
    private BigDecimal extraDiscountAmount;

    @Override
    public String getSysName() {
        return IDObSalesInvoice.SYS_NAME;
    }

}
