package com.ebs.dda.jpa.inventory.storetransaction;

import javax.persistence.*;

@Entity
@Table(name = "TObGoodsIssueStoreTransaction")
@DiscriminatorValue("GI")
public class TObGoodsIssueStoreTransaction extends TObStoreTransaction {

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }
}
