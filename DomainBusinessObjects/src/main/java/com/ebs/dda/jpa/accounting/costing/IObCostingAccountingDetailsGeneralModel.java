package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCostingAccountingDetailsGeneralModel")
public class IObCostingAccountingDetailsGeneralModel
    extends IObAccountingDocumentAccountingDetailsGeneralModel {

  private String goodsReceiptCode;
  private String currencyCode;
  private String currencyISO;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  @Override
  public String getSysName() {
    return IDObCosting.SYS_NAME;
  }
}
