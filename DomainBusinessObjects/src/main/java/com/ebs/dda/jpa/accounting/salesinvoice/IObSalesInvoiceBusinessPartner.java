package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "iobsalesinvoicebusinesspartner")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
public class IObSalesInvoiceBusinessPartner extends DocumentHeader implements Serializable {

  private Long paymentTermId;
  private Long currencyId;
  private Long salesOrderid;
  private BigDecimal downPayment;

  public Long getPaymentTermId() {
    return paymentTermId;
  }

  public void setPaymentTermId(Long paymentTermId) {
    this.paymentTermId = paymentTermId;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public Long getSalesOrderid() {
    return salesOrderid;
  }

  public void setSalesOrderid(Long salesOrderid) {
    this.salesOrderid = salesOrderid;
  }

  public BigDecimal getDownPayment() {
    return downPayment;
  }

  public void setDownPayment(BigDecimal downPayment) {
    this.downPayment = downPayment;
  }

  @Override
  public String getSysName() {
    return IIObSalesInvoiceBusinessPartner.SYS_NAME;
  }
}
