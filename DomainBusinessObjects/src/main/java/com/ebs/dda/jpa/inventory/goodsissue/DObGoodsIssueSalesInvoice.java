package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dobgoodsissuesalesinvoice")
@Inheritance(strategy = InheritanceType.JOINED)
@EntityInterface(IDObGoodsIssueSalesInvoice.class)
@DiscriminatorValue(value = "GI_SI")
public class DObGoodsIssueSalesInvoice extends DObGoodsIssue implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long typeId;

    public Long getTypeId() {
        return typeId;
    }

    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @Override
    public String getSysName() {
        return IDObGoodsIssueSalesInvoice.SYS_NAME;
    }
}
