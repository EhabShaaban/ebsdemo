package com.ebs.dda.jpa.accounting.settlements;

import java.math.BigDecimal;

public class DObSettlementPostValueObject {

    private String settlementCode;
    private String dueDate;
    private BigDecimal currencyPrice;


    public String getSettlementCode() {
        return settlementCode;
    }

    public void setSettlementCode(String settlementCode) {
        this.settlementCode = settlementCode;
    }

    public BigDecimal getCurrencyPrice() {
        return currencyPrice;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public void setCurrencyPrice(BigDecimal currencyPrice) {
        this.currencyPrice = currencyPrice;
    }
}
