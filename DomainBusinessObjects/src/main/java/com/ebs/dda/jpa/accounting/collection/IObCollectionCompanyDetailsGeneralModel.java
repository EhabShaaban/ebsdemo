package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCollectionCompanyDetailsGeneralModel")
@EntityInterface(IIObCollectionCompanyDetailsGeneralModel.class)
public class IObCollectionCompanyDetailsGeneralModel extends InformationObject {

  private String companyCode;
  private String bankCode;
  private String accountNumber;
  private String collectionCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitCode;
  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString bankName;

  public String getCompanyCode() {
    return companyCode;
  }

  public String getBankCode() {
    return bankCode;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public LocalizedString getBankName() {
    return bankName;
  }

  public String getCollectionCode() {
    return collectionCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  @Override
  public String getSysName() {
    return IIObCollectionCompanyDetailsGeneralModel.SYS_NAME;
  }
}
