package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.inventory.inventorydocument.DObInventoryDocument;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import java.io.Serializable;

@Entity
@EntityInterface(IDObInitialStockUpload.class)
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = IDObInitialStockUpload.INITIAL_STOCK_UPLOAD_KEY)
public class DObInitialStockUpload extends DObInventoryDocument implements Serializable {
  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IDObInitialStockUpload.SYS_NAME;
  }
}
