package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dda.jpa.IValueObject;

public class DObPaymentRequestActivateValueObject implements IValueObject {

  private String code;
  private String dueDate;

  public String getDueDate() {
    return dueDate;
  }

  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

  public String getPaymentRequestCode() {
    return code;
  }

  public void setPaymentRequestCode(String paymentRequestCode) {
    this.code = paymentRequestCode;
  }

  @Override
  public void userCode(String userCode) {
  this.code = userCode;
  }
}
