package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObFiscalPeriod")
@EntityInterface(ICObFiscalYear.class)
public class CObFiscalYear extends ConfigurationObject<DefaultUserCode> implements Serializable {

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime fromDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime toDate;

  public DateTime getFromDate() {
    return fromDate;
  }

  public void setFromDate(DateTime fromDate) {
    this.fromDate = fromDate;
  }

  public DateTime getToDate() {
    return toDate;
  }

  public void setToDate(DateTime toDate) {
    this.toDate = toDate;
  }

  @Override
  public String getSysName() {
    return ICObFiscalYear.SYS_NAME;
  }
}
