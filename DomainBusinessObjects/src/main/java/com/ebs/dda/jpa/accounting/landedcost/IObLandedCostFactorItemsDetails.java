package com.ebs.dda.jpa.accounting.landedcost;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import java.math.BigDecimal;

/**
 * @author Ahmed Ali Elfeky
 * @since Mar 17, 2021
 */
@Entity
@Table(name = "IObLandedCostFactorItemsDetails")
public class IObLandedCostFactorItemsDetails extends DocumentLine {

  private static final long serialVersionUID = 1L;

  private Long documentInfoId;
  private BigDecimal actualValue;

  public Long getDocumentInfoId() {
    return documentInfoId;
  }

  public void setDocumentInfoId(Long documentInfoId) {
    this.documentInfoId = documentInfoId;
  }

  public BigDecimal getActualValue() {
    return actualValue;
  }

  public void setActualValue(BigDecimal actualValue) {
    this.actualValue = actualValue;
  }

}
