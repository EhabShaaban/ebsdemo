package com.ebs.dda.jpa.accounting.salesinvoice;


import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("SalesInvoice")
public class IObSalesInvoicePostingDetails extends IObAccountingDocumentActivationDetails {
}
