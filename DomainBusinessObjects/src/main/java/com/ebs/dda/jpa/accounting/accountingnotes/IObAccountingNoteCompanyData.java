package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("AccountingNote")
public class IObAccountingNoteCompanyData extends IObAccountingDocumentCompanyData {

  private static final long serialVersionUID = 1L;

}
