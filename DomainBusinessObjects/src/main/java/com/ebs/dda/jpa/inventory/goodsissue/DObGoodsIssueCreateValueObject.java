package com.ebs.dda.jpa.inventory.goodsissue;

public class DObGoodsIssueCreateValueObject {

  private String typeCode;
  private String purchaseUnitCode;
    private String refDocumentCode;
    private String companyCode;
  private String storeHouseCode;
  private String storeKeeperCode;

  public String getTypeCode() {
    return typeCode;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

    public String getStoreHouseCode() {
        return storeHouseCode;
    }

    public String getStoreKeeperCode() {
        return storeKeeperCode;
    }

    public String getRefDocumentCode() {
        return refDocumentCode;
    }
}
