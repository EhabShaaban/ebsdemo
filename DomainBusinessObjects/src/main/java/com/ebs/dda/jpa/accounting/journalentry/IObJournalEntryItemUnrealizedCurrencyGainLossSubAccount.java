package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "IObJournalEntryItemUnrealizedCurrencyGainLossSubAccount")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "UnrealizedCurrencyGainLoss")
public class IObJournalEntryItemUnrealizedCurrencyGainLossSubAccount extends IObJournalEntryItem {
}
