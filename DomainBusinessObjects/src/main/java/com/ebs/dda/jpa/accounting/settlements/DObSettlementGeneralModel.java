package com.ebs.dda.jpa.accounting.settlements;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

@Entity
@Table(name = "dobsettlementgeneralmodel")
@EntityInterface(IDObSettlementGeneralModel.class)
public class DObSettlementGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;
  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;
  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String companyNameEn;
  private String settlementType;
  private String currencyIso;
  private String activatedBy;
  private String documentOwner;

  public String getActivatedBy() {
    return activatedBy;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }


  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }


  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getSettlementType() {
    return settlementType;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getCompanyNameEn() {
    return companyNameEn;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  @Override
  public String getSysName() {
    return IDObSettlementGeneralModel.SYS_NAME;
  }
}
