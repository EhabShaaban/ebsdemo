package com.ebs.dda.jpa.accounting.monetarynotes;

public interface IDObNotesReceivablesCreateValueObject {

  String NOTE_FORM = "noteForm";
  String TYPE = "type";
  String BUSINESS_UNIT_CODE = "businessUnitCode";
  String COMPANY_CODE = "companyCode";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
  String AMOUNT = "amount";
  String CURRENCY_ISO = "currencyIso";
}
