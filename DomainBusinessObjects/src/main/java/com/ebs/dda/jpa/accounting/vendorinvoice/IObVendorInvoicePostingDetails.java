package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentActivationDetails;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("VendorInvoice")
public class IObVendorInvoicePostingDetails extends IObAccountingDocumentActivationDetails {
  @Override
  public String getSysName() {
    return IDObVendorInvoice.SYS_NAME;
  }
}
