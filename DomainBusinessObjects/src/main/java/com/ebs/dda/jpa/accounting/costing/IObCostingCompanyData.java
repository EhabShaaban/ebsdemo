package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyData;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "Costing")
public class IObCostingCompanyData extends IObAccountingDocumentCompanyData {
	private static final long serialVersionUID = 1L;

}
