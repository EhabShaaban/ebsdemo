package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;

public class AccountingNoteCreateValueObject {

  private AccountingEntry noteType;
  private String type;
  private String businessPartnerCode;
  private String companyCode;
  private String businessUnitCode;
  private Double amount;
  private String currencyIso;
  private String referenceDocumentCode;
  private Long documentOwnerId;

  public AccountingEntry getNoteType() {
    return noteType;
  }

  public void setNoteType(AccountingEntry noteType) {
    this.noteType = noteType;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public void setBusinessPartnerCode(String businessPartnerCode) {
    this.businessPartnerCode = businessPartnerCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public void setCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }

  public String getReferenceDocumentCode() {
    return referenceDocumentCode;
  }

  public void setReferenceDocumentCode(String referenceDocumentCode) {
    this.referenceDocumentCode = referenceDocumentCode;
  }
}
