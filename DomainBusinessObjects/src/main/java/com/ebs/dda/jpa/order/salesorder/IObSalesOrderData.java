package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObSalesOrderData")
public class IObSalesOrderData extends DocumentHeader implements Serializable {

  private Long customerId;
  private Long customerAddressId;
  private Long contactPersonId;
  private Long paymentTermId;
  private Long currencyId;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime expectedDeliveryDate;

  private String notes;

  public Long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  public Long getCustomerAddressId() {
    return customerAddressId;
  }

  public void setCustomerAddressId(Long customerAddressId) {
    this.customerAddressId = customerAddressId;
  }

  public Long getContactPersonId() {
    return contactPersonId;
  }

  public void setContactPersonId(Long contactPersonId) {
    this.contactPersonId = contactPersonId;
  }

  public Long getPaymentTermId() {
    return paymentTermId;
  }

  public void setPaymentTermId(Long paymentTermId) {
    this.paymentTermId = paymentTermId;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public DateTime getExpectedDeliveryDate() {
    return expectedDeliveryDate;
  }

  public void setExpectedDeliveryDate(DateTime expectedDeliveryDate) {
    this.expectedDeliveryDate = expectedDeliveryDate;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }
}
