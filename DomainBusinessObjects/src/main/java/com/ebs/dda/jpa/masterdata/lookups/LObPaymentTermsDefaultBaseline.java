package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("8")
public class LObPaymentTermsDefaultBaseline extends LObMaterial {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_PAYMENT_TERMS_DEFAULT_BASE_SELINE_SYS_NAME;
  }
}
