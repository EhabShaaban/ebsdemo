package com.ebs.dda.jpa.accounting.paymentrequest;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DObPaymentRequestSaveDetailsPreparationGeneralModel")
public class DObPaymentRequestSaveDetailsPreparationGeneralModel {
	@Id
	private Long id;
	private String code;
	private String paymentType;
	private String paymentForm;
	private String dueDocumentType;
	private String companyCode;
	private String businessUnitCode;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public String getPaymentForm() {
		return paymentForm;
	}

	public String getDueDocumentType() {
		return dueDocumentType;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public String getBusinessUnitCode() {
		return businessUnitCode;
	}
}
