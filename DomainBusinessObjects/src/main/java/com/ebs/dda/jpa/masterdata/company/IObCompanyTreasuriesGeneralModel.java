package com.ebs.dda.jpa.masterdata.company;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

@Entity
@Table(name = "IObCompanyTreasuriesGeneralModel")
public class IObCompanyTreasuriesGeneralModel extends InformationObject {

	String companyCode;
	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString companyName;

	String treasuryCode;

	@Convert(converter = LocalizedStringConverter.class)
	private LocalizedString treasuryName;

	public String getCompanyCode() {
		return companyCode;
	}

	public LocalizedString getCompanyName() {
		return companyName;
	}

	public String getTreasuryCode() {
		return treasuryCode;
	}

	public LocalizedString getTreasuryName() {
		return treasuryName;
	}

	@Override
	public String getSysName() {
		return IObCompanyTreasuriesGeneralModel.class.getSimpleName();
	}
}
