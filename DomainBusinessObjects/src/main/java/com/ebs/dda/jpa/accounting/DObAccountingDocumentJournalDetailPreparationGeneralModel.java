package com.ebs.dda.jpa.accounting;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class DObAccountingDocumentJournalDetailPreparationGeneralModel {
    @Id
    private Long id;
    private String code;
    private String objectTypeCode;
    private Long companyCurrencyId;
    private Long documentCurrencyId;
    private String currencyPrice;
    private Long exchangeRateId;
    private Long glAccountId;
    private Long glSubAccountId;
    private String accountLedger;
    private BigDecimal amount;

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getObjectTypeCode() {
        return objectTypeCode;
    }

    public Long getCompanyCurrencyId() {
        return companyCurrencyId;
    }

    public Long getDocumentCurrencyId() {
        return documentCurrencyId;
    }

    public String getCurrencyPrice() {
        return currencyPrice;
    }

    public Long getExchangeRateId() {
        return exchangeRateId;
    }

    public Long getGlAccountId() {
        return glAccountId;
    }

    public Long getGlSubAccountId() {
        return glSubAccountId;
    }

    public String getAccountLedger() {
        return accountLedger;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
