package com.ebs.dda.jpa.inventory.stocktransformation;

public interface ICObStockTransformationReason {
    String SYS_NAME = "StockTransformationReason";
    String USER_CODE = "userCode";
    String NAME = "name";
}
