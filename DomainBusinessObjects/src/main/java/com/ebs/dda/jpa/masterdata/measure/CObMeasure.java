package com.ebs.dda.jpa.masterdata.measure;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @Author : Menna Sayed
 *
 * @since : Mar 24, 2016
 */
@Entity
@Table(name = "CObMeasure")
@EntityInterface(ICObMeasure.class)
public class CObMeasure extends ConfigurationObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "symbol")
  private LocalizedString symbol;

  @Column(name = "isstandard")
  private Boolean isStandard;

  @Column(name = "isocode")
  private String isoCode;

  @Column(name = "dimensionid")
  private Long dimensionId;

  @Column(name = "internationalsystem")
  private Boolean internationalSystem;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public LocalizedString getSymbol() {
    return symbol;
  }

  public void setSymbol(LocalizedString symbol) {
    this.symbol = symbol;
  }

  public Boolean getStandard() {
    return isStandard;
  }

  public void setStandard(Boolean standard) {
    isStandard = standard;
  }

  public String getIsoCode() {
    return isoCode;
  }

  public void setIsoCode(String isoCode) {
    this.isoCode = isoCode;
  }

  public Long getDimensionId() {
    return dimensionId;
  }

  public void setDimensionId(Long dimensionId) {
    this.dimensionId = dimensionId;
  }

  public Boolean getInternationalSystem() {
    return internationalSystem;
  }

  public void setInternationalSystem(Boolean internationalSystem) {
    this.internationalSystem = internationalSystem;
  }

  @Override
  public String getSysName() {
    return ICObMeasure.SYS_NAME;
  }
}
