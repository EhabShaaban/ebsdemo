package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "dobaccountingnotegeneralmodel")
@EntityInterface(value = IDObAccountingNoteGeneralModel.class)
public class DObAccountingNoteGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private String type;
  private Double remaining;
  private String objectTypeCode;
  private Double amount;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerType;

  private String businessPartnerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString referenceDocumentType;
  private String referenceDocumentCode;
  private String currencyCode;
  private String currencyIso;
  private String documentOwnerName;

  private String purchaseUnitCode;
  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime activationDate;

  public String getType() {
    return type;
  }

  public Double getRemaining() {
    return remaining;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public Double getAmount() {
    return amount;
  }

  public LocalizedString getBusinessPartnerType() {
    return businessPartnerType;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public LocalizedString getBusinessPartnerName() {
    return businessPartnerName;
  }

  public LocalizedString getReferenceDocumentType() {
    return referenceDocumentType;
  }

  public String getReferenceDocumentCode() {
    return referenceDocumentCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public String getDocumentOwnerName() {
    return documentOwnerName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public DateTime getActivationDate() {
    return activationDate;
  }

  @Override
  public String getSysName() {
    return IDObAccountingNoteGeneralModel.SYS_NAME;
  }
}
