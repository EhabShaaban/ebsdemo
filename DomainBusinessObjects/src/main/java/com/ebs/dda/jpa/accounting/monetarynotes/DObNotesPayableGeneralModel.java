package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObNotesPayableGeneralModel")
public class DObNotesPayableGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private String objectTypeCode;
  private String businessPartnerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private BigDecimal remaining;
  private Long documentOwnerId;

  private String documentOwner;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private BigDecimal amount;

  private String currencyIso;
  private String noteForm;

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getBusinessPartnerCode() {
    return businessPartnerCode;
  }

  public LocalizedString getBusinessPartnerName() {
    return businessPartnerName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public DateTime getDueDate() {
    return dueDate;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public String getNoteForm() {
    return noteForm;
  }

  @Override
  public String getSysName() {
    return IDObMonetaryNotes.NOTES_PAYABLE_SYS_NAME;
  }
}
