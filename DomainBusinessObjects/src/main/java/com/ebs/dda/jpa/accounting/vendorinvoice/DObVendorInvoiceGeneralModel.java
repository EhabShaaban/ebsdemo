package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObVendorInvoiceGeneralModel")
@EntityInterface(IDObVendorInvoiceGeneralModel.class)
public class DObVendorInvoiceGeneralModel extends DocumentObject<DefaultUserCode> {

  private String vendorCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString vendorName;

  private String vendorCodeName;

  private String invoiceType;

  private String purchaseUnitCode;
  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String currencyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;
  private String currencyISO;
  private String paymentTermCode;
  private String purchaseOrderCode;
  private String invoiceNumber;
  private  String documentOwner;
  public String getVendorCode() {
    return vendorCode;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public String getVendorCodeName() {
    return vendorCodeName;
  }

  public String getInvoiceType() {
    return invoiceType;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getInvoiceNumber() {
    return invoiceNumber;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  @Override
  public String getSysName() {
    return IDObVendorInvoice.SYS_NAME;
  }
}
