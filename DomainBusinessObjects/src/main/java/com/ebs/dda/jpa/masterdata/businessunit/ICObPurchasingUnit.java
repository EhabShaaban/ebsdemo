package com.ebs.dda.jpa.masterdata.businessunit;

import com.ebs.dac.dbo.processing.BasicAttribute;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 1, 2018 5:51:06 PM
 */
public interface ICObPurchasingUnit{

  String SYS_NAME = "PurchasingUnit";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";

  BasicAttribute purchaseUnitNameAttr =
      new BasicAttribute(PURCHASING_UNIT_NAME);
}
