package com.ebs.dda.jpa.masterdata.geonames;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/** The persistent class for the geonames_alternatename database table. */
@Entity
@Table(name = "geonames_alternatename")
public class GeonamesAlternatename implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "alternatenameid", unique = true, nullable = false)
  private Integer id;

  @Column(length = 3000)
  private String alternatename;

  private Integer geonameid;

  private Boolean iscolloquial;

  private Boolean ishistoric;

  @Column(length = 70)
  private String isolanguage;

  private Boolean ispreferredname;

  private Boolean isshortname;

  public GeonamesAlternatename() {}

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getAlternatename() {
    return this.alternatename;
  }

  public void setAlternatename(String alternatename) {
    this.alternatename = alternatename;
  }

  public Integer getGeonameid() {
    return this.geonameid;
  }

  public void setGeonameid(Integer geonameid) {
    this.geonameid = geonameid;
  }

  public Boolean getIscolloquial() {
    return this.iscolloquial;
  }

  public void setIscolloquial(Boolean iscolloquial) {
    this.iscolloquial = iscolloquial;
  }

  public Boolean getIshistoric() {
    return this.ishistoric;
  }

  public void setIshistoric(Boolean ishistoric) {
    this.ishistoric = ishistoric;
  }

  public String getIsolanguage() {
    return this.isolanguage;
  }

  public void setIsolanguage(String isolanguage) {
    this.isolanguage = isolanguage;
  }

  public Boolean getIspreferredname() {
    return this.ispreferredname;
  }

  public void setIspreferredname(Boolean ispreferredname) {
    this.ispreferredname = ispreferredname;
  }

  public Boolean getIsshortname() {
    return this.isshortname;
  }

  public void setIsshortname(Boolean isshortname) {
    this.isshortname = isshortname;
  }
}
