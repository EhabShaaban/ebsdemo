package com.ebs.dda.jpa.inventory;

import com.ebs.dda.jpa.IValueObject;

public class DObInventoryDocumentActivateValueObject implements IValueObject {
    private String inventoryType;
    private String userCode;
    private String refDocumentCode;

    public String getInventoryType() {
        return inventoryType;
    }

    public void setInventoryType(String inventoryType) {
        this.inventoryType = inventoryType;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getRefDocumentCode() {
        return refDocumentCode;
    }

    public void setRefDocumentCode(String refDocumentCode) {
        this.refDocumentCode = refDocumentCode;
    }

    @Override
    public void userCode(String userCode) {
        this.userCode = userCode;
    }
}
