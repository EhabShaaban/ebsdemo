package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class TObStoreTransactionCreateGoodsRecieptValueObject implements IValueObject {

    private String goodsReceiptCode;
    private String goodsReceiptType;

    public String getGoodsReceiptCode() {
        return goodsReceiptCode;
    }

    public void setGoodsReceiptType(String goodsReceiptType) {
        this.goodsReceiptType = goodsReceiptType;
    }

    public String getGoodsReceiptType() {
        return goodsReceiptType;
    }

    public void setGoodsReceiptCode(String goodsReceiptCode) {
        this.goodsReceiptCode = goodsReceiptCode;
    }
}
