package com.ebs.dda.jpa.order;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObOrderDocument {

  String REQUIRED_DOCUMENTS_NAME = "requiredDocuments";
  CompositeAttribute<IObOrderDocumentRequiredDocuments> requiredDocumentsAttr =
      new CompositeAttribute<>(
          REQUIRED_DOCUMENTS_NAME, IObOrderDocumentRequiredDocuments.class, true);
}
