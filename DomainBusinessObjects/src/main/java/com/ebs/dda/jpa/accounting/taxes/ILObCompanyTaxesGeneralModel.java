package com.ebs.dda.jpa.accounting.taxes;

public interface ILObCompanyTaxesGeneralModel {
  String COMPANY_NAME = "companyName";
  String COMPANY_CODE = "companyCode";
  String NAME = "name";
  String USER_CODE = "userCode";
  String TAX_CODE = "taxCode";
  String TAX_NAME = "taxName";
  String TAX_PERCENTAGE = "percentage";
  String TAX_AMOUNT = "taxAmount";
}
