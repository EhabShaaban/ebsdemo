package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "iobsalesreturnorderdetailsGeneralModel")
public class IObSalesReturnOrderDetailsGeneralModel extends InformationObject {

  private String salesReturnOrderCode;
  private String customerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  private String salesInvoiceCode;

  private String currencyIso;

  public String getCustomerCode() {
    return customerCode;
  }

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public String getSalesReturnOrderCode() {
    return salesReturnOrderCode;
  }

  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
