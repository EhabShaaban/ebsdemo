package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.converters.StatefullBusinessObjectCurrentStatesJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.accounting.BasicGeneralModel;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "DobSalesInvoiceBasicGeneralModel")
@EntityInterface(IDObSalesInvoiceBasicGeneralModel.class)
public class DObSalesInvoiceBasicGeneralModel extends BasicGeneralModel<DefaultUserCode>
    implements Serializable {
  private static final long serialVersionUID = 1L;

  private String salesOrder;
  private String invoiceTypeCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString invoiceType;
  private String purchaseUnitCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;
  private String purchaseUnitNameEn;
  private String companyCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString company;
  private String customerCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;
  private String customerCodeName;
  private String documentOwnerUserName;



  public String getSalesOrder() {
    return salesOrder;
  }

  public String getInvoiceTypeCode() {
    return invoiceTypeCode;
  }

  public LocalizedString getInvoiceTypeName() {
    return invoiceType;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return company;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getCustomerCodeName() {
    return customerCodeName;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public LocalizedString getInvoiceType() {
    return invoiceType;
  }

  public LocalizedString getCompany() {
    return company;
  }

  public String getDocumentOwnerUserName() {
    return documentOwnerUserName;
  }
}
