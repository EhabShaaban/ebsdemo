package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.*;

@Entity
@Table(name = "iobcompanybankdetailsgeneralmodel")
public class IObCompanyBankDetailsGeneralModel {

  @Id private Long id;

  private String companyCode;
  private String code;
  private String bankAccountNumber;

  @Column(name = "accountno")
  private String accountNumber;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private String currencyCode;
  private String currencyISO;

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public String getCurrencyCode() { return currencyCode; }

  public String getCurrencyISO() { return currencyISO; }

  public void setCurrencyISO(String currencyISO) { this.currencyISO = currencyISO; }

  public String getSysName() {
    return "IObCompanyBankDetails General Model";
  }
}
