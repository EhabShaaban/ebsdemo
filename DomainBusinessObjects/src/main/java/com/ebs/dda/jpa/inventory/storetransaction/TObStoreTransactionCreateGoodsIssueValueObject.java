package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class TObStoreTransactionCreateGoodsIssueValueObject implements IValueObject {

    private String goodsIssueCode;
    private Long goodsIssueId;

    public String getGoodsIssueCode() {
        return goodsIssueCode;
    }

    public void setGoodsIssueCode(String goodsIssueCode) {
        this.goodsIssueCode = goodsIssueCode;
    }

    public Long getGoodsIssueId() {
        return goodsIssueId;
    }

    public void setGoodsIssueId(Long goodsIssueId) {
        this.goodsIssueId = goodsIssueId;
    }
}
