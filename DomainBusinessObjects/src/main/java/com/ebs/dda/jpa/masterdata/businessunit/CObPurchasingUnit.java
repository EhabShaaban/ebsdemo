package com.ebs.dda.jpa.masterdata.businessunit;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("5")
@EntityInterface(ICObPurchasingUnit.class)
public class CObPurchasingUnit extends CObEnterprise {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ICObPurchasingUnit.SYS_NAME;
  }
}
