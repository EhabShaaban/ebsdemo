package com.ebs.dda.jpa.accounting;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "IObAccountingDocumentBankAccountingDetails")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Banks")
public class IObAccountingDocumentBankAccountingDetails
				extends IObAccountingDocumentAccountingDetails implements Serializable {
	private Long glSubAccountId;

	@Override
	public Long getGlSubAccountId() {
		return glSubAccountId;
	}

	@Override
	public void setGlSubAccountId(Long glSubAccountId) {
		this.glSubAccountId = glSubAccountId;
	}
}
