package com.ebs.dda.jpa.accounting.monetarynotes;

import javax.persistence.*;

@Entity
@Table(name = "IObNotesReceivableReferenceDocumentNotesReceivable")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(NotesReceivableReferenceDocumentsTypeEnum.Values.NOTES_RECEIVABLE)
public class IObNotesReceivableReferenceDocumentNotesReceivable
    extends IObMonetaryNotesReferenceDocuments {

  private Long documentId;

  public Long getDocumentId() {
    return documentId;
  }

  public void setDocumentId(Long documentId) {
    this.documentId = documentId;
  }
}
