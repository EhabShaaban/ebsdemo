package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.jpa.accounting.salesinvoice.ICObSalesInvoice;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObItem")
@EntityInterface(ICObItem.class)
public class CObItem extends ConfigurationObject<DefaultUserCode> implements Serializable {

  @Override
  public String getSysName() {
    return ICObItem.SYS_NAME;
  }
}
