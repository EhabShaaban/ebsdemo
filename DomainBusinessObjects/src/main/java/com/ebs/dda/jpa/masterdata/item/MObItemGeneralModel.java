package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringArrayConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 31, 2017 , 9:57:36 AM
 */
@Entity
@Table(name = "MObItemGeneralModel")
@EntityInterface(IMObItem.class)
public class MObItemGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Column(name = "oldItemNumber")
  private String oldItemNumber;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString itemName;

  @Column(name = "basicUnitOfMeasure")
  private Long basicUnitOfMeasure;

  @Column(name = "basicunitofmeasurecode")
  private String basicUnitOfMeasureCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "basicunitofmeasuresymbol")
  private LocalizedString basicUnitOfMeasureSymbol;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "basicunitofmeasurename")
  private LocalizedString basicUnitOfMeasureName;

  @Column(name = "itemGroup")
  private Long itemGroup;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "itemgroupname")
  private LocalizedString itemGroupName;

  @Column(name = "itemgroupcode")
  private String itemGroupCode;

  @Convert(converter = LocalizedStringArrayConverter.class)
  @Column(name = "purchaseunitsnames")
  private List<LocalizedString> purchasingUnitNames;

  @Column(name = "purchaseunitscodes")
  private String purchasingUnitCodes;

  @Column(name = "batchmanagementrequired")
  private Boolean isBatchManaged;

  @Column(name = "marketname")
  private String marketName;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "typename")
  private LocalizedString typeName;

  @Column(name = "typecode")
  private String typeCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "productmanagername")
  private LocalizedString productManagerName;

  @Column(name = "productmanagercode")
  private String productManagerCode;

  private Boolean costFactor;

  @Override
  public String getSysName() {
    return IMObItem.SYS_NAME;
  }

  public String getOldItemNumber() {
    return oldItemNumber;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public void setItemName(LocalizedString name) {
    this.itemName = name;
  }

  public Long getBasicUnitOfMeasure() {
    return basicUnitOfMeasure;
  }

  public Long getItemGroup() {
    return itemGroup;
  }

  public LocalizedString getItemGroupName() {
    return this.itemGroupName;
  }

  public Boolean getIsBatchManaged() {
    return isBatchManaged;
  }

  public String getPurchasingUnitCodes() {
    return purchasingUnitCodes;
  }

  public String getBasicUnitOfMeasureCode() {
    return basicUnitOfMeasureCode;
  }

  public LocalizedString getBasicUnitOfMeasureSymbol() {
    return basicUnitOfMeasureSymbol;
  }

  public String getMarketName() {
    return marketName;
  }

  public LocalizedString getTypeName() {
    return typeName;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public LocalizedString getProductManagerName() {
    return productManagerName;
  }

  public String getProductManagerCode() {
    return productManagerCode;
  }

  public Boolean getCostFactor() {
    return costFactor;
  }
}
