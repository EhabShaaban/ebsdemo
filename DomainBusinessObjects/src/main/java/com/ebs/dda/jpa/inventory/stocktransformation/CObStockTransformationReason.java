package com.ebs.dda.jpa.inventory.stocktransformation;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObStockTransformationReason")
@EntityInterface(ICObStockTransformationReason.class)
public class CObStockTransformationReason extends ConfigurationObject<DefaultUserCode>
    implements Serializable {

  private Long typeId;
  private String fromStockType;
  private String toStockType;

  public Long getTypeId() {
    return typeId;
  }

  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }

  public String getFromStockType() {
    return fromStockType;
  }

  public void setFromStockType(String fromStockType) {
    this.fromStockType = fromStockType;
  }

  public String getToStockType() {
    return toStockType;
  }

  public void setToStockType(String toStockType) {
    this.toStockType = toStockType;
  }

  @Override
  public String getSysName() {
    return IDObStockTransformation.SYS_NAME;
  }
}
