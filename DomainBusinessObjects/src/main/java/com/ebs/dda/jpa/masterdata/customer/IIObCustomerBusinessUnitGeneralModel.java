package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IIObCustomerBusinessUnitGeneralModel {
  String SYS_NAME = "Customer";
  String CUSTOMER_NAME = "customerName";
  String CUSTOMER_CODE = "userCode";

  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME);
}
