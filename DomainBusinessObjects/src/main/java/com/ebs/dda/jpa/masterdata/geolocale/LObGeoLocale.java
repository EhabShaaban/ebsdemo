/** */
package com.ebs.dda.jpa.masterdata.geolocale;

import com.ebs.dac.dbo.jpa.entities.businessobjects.SimpleLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Yara Ameen
 * @since Mar 24, 2016 2:24:56 PM
 */
@Entity
@Table(name = "LObGeoLocale")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class LObGeoLocale extends SimpleLookupObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
