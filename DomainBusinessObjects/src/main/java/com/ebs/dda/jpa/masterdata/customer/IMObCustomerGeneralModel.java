package com.ebs.dda.jpa.masterdata.customer;

public interface IMObCustomerGeneralModel {

  String USER_CODE = "userCode";
  String CURRENT_STATES = "currentStates";
  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";
  String MARKET_NAME = "marketName";
  String LEGAL_NAME = "legalName";
  String TYPE = "type";
  String KAP_CODE = "kapCode";
  String KAP_NAME = "kapName";
  String BUSINESS_UNIT_CODES = "businessUnitCodes";
  String BUSINESS_UNIT_NAMES = "businessUnitNames";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String OLD_CUSTOMER_NUMBER = "oldCustomerNumber";
}
