package com.ebs.dda.jpa.accounting.taxes;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "lobtaxinfo")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
public class LObTaxInfo extends LocalizedLookupObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  private BigDecimal percentage;

  public BigDecimal getPercentage() { return percentage; }

  public void setPercentage(BigDecimal percentage) { this.percentage = percentage; }

  @Override
  public String getSysName() {
    return null;
  }
}
