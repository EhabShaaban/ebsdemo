package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptOrdinaryReceivedItemGeneralModel {
    String ITEM_CODE_AT_VENDOR = "itemCodeAtVendor";
    String RECEIVED_QTY_UOE = "receivedQtyUoE";
    String RECEIVED_QTY_BASE = "receivedQtyBase";
    String UOE = "unitOfEntry";
    String UOE_CODE = "unitOfEntryCode";
    String BASE_UNIT_SYMBOL = "baseUnitSymbol";
    String DEFECT_DESCRIPTION = "defectDescription";
    String STOCK_TYPE = "stockType";
}
