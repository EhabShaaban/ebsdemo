package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;
import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IIObVendorContact extends IInformationObject {

  String TYPE_ATTR = "contactType";

  String VALUE_ATTR = "contactValue";
  BasicAttribute contactValueAttr = new BasicAttribute( VALUE_ATTR);
}
