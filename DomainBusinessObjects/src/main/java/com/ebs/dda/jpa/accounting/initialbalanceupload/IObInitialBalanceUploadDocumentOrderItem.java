package com.ebs.dda.jpa.accounting.initialbalanceupload;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "IObInitialBalanceUploadDocumentOrderItem")
@DiscriminatorValue(value = "PO")
public class IObInitialBalanceUploadDocumentOrderItem extends IObInitialBalanceUploadItem
    implements Serializable {
}
