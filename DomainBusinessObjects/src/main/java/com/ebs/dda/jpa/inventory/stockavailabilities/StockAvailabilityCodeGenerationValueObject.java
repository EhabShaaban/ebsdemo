package com.ebs.dda.jpa.inventory.stockavailabilities;

public class StockAvailabilityCodeGenerationValueObject {
  String businessUnitCode;
  String companyCode;
  String plantCode;
  String storehouseCode;
  String itemCode;
  String unitOfMeasureCode;
  String stockType;


  public StockAvailabilityCodeGenerationValueObject fromItemQuantityGeneralModel(
          IObItemQuantityGeneralModel item) {
    this.companyCode = item.getCompanyCode();
    this.businessUnitCode = item.getPurchaseUnitCode();
    this.plantCode = item.getPlantCode();
    this.storehouseCode = item.getStoreHouseCode();
    this.itemCode = item.getItemCode();
    this.stockType = item.getStockType();
    this.unitOfMeasureCode = item.getUnitOfMeasureCode();
    return this;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public void setCompanyCode(String companyCode) {
    this.companyCode = companyCode;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public void setPlantCode(String plantCode) {
    this.plantCode = plantCode;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public void setStorehouseCode(String storehouseCode) {
    this.storehouseCode = storehouseCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public void setUnitOfMeasureCode(String unitOfMeasureCode) {
    this.unitOfMeasureCode = unitOfMeasureCode;
  }

  public String getStockType() {
    return stockType;
  }

  public void setStockType(String stockType) {
    this.stockType = stockType;
  }
}
