package com.ebs.dda.jpa.inventory.initialstockupload;

import org.springframework.web.multipart.MultipartFile;

public class DObInitialStockUploadCreateValueObject {

  private String purchaseUnitCode;
  private String companyCode;
  private String storeHouseCode;
  private String storeKeeperCode;
  private MultipartFile attachment;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getStoreHouseCode() {
    return storeHouseCode;
  }

  public String getStoreKeeperCode() {
    return storeKeeperCode;
  }

  public MultipartFile getAttachment() {
    return attachment;
  }

  public void setAttachment(MultipartFile attachment) {
    this.attachment = attachment;
  }
}
