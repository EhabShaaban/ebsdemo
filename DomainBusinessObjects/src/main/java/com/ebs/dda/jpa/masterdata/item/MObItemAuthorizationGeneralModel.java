package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.IMObItemGeneralModel;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MObItemAuthorizationGeneralModel")
@EntityInterface(IMObItemGeneralModel.class)
public class MObItemAuthorizationGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "itemname")
  private LocalizedString itemName;

  @Column(name = "purchaseunitname")
  private String purchaseUnitName;

  @Column(name = "itemid")
  private Long itemId;

  @Override
  public String getSysName() {
    return IMObItem.SYS_NAME;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public void setItemName(LocalizedString itemName) {
    this.itemName = itemName;
  }

  public String getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public void setPurchaseUnitName(String purchaseUnitName) {
    this.purchaseUnitName = purchaseUnitName;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }
}
