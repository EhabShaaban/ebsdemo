package com.ebs.dda.jpa.accounting.landedcost;

public interface ICObLandedCostType {

  static final String SYS_NAME = "LandedCostType";

  static final String IMPORT = "IMPORT";
  static final String LOCAL = "LOCAL";

}
