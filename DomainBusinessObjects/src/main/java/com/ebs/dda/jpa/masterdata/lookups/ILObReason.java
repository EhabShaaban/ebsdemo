package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface ILObReason {
  String SYS_NAME = "Reason";
  String USER_CODE = "userCode";
  String NAME = "name";

  String SRO_REASON = "SRO_REASON";

  String OBJECT_TYPE_CODE = "type";
  BasicAttribute typeAttr = new BasicAttribute(OBJECT_TYPE_CODE);
}
