package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObGoodsIssueSalesInvoice {

    String SYS_NAME = "GoodsIssueSalesInvoice";

    String REF_DOCUMENT_ATTR_NAME = "refDocument";
    CompositeAttribute<IObGoodsIssueSalesInvoiceData> salesInvoiceRefDocumentAttr =
            new CompositeAttribute<>(REF_DOCUMENT_ATTR_NAME, IObGoodsIssueSalesInvoiceData.class, false);

    String SI_ITEMS_ATTR_NAME = "salesinvoiceitems";
    CompositeAttribute<IObGoodsIssueSalesInvoiceItem> salesInvoiceItemsAttr =
            new CompositeAttribute<>(SI_ITEMS_ATTR_NAME, IObGoodsIssueSalesInvoiceItem.class, true);
}
