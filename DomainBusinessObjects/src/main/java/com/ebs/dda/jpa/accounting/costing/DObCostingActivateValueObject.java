package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dda.jpa.IValueObject;

public class DObCostingActivateValueObject implements IValueObject {

  private String userCode;
  private String postingDate;
  private String dueDate;

  public String getUserCode() {
    return userCode;
  }

  public void setUserCode(String userCode) {
    this.userCode = userCode;
  }

  public String getPostingDate() {
    return postingDate;
  }

  public void setPostingDate(String postingDate) {
    this.postingDate = postingDate;
  }

  public String getDueDate() {
    return dueDate;
  }

  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

  @Override
  public void userCode(String userCode) {
    this.userCode = userCode;
  }
}
