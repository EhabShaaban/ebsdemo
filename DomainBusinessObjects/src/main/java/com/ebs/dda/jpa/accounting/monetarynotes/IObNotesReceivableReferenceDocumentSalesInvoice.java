package com.ebs.dda.jpa.accounting.monetarynotes;

import javax.persistence.*;

@Entity
@Table(name = "IObNotesReceivableReferenceDocumentSalesInvoice")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(NotesReceivableReferenceDocumentsTypeEnum.Values.SALES_INVOICE)
public class IObNotesReceivableReferenceDocumentSalesInvoice
    extends IObMonetaryNotesReferenceDocuments {

  private Long documentId;

  public Long getDocumentId() {
    return documentId;
  }

  public void setDocumentId(Long documentId) {
    this.documentId = documentId;
  }
}
