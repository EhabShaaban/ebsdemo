package com.ebs.dda.jpa.inventory.goodsreceipt;

public interface IIObGoodsReceiptReceivedItemsGeneralModel {
    String ITEM_NAME = "itemName";
    String BASE_UNIT = "baseUnit";
    String BASE_UNIT_CODE = "baseUnitCode";
    String QTY_IN_DN = "quantityInDn";
    String RETURNED_QTY_BASE = "returnedQuantityBase";
    String DIFFERENCE_REASON = "differenceReason";
}
