package com.ebs.dda.jpa.accounting.salesinvoice;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;

@Entity
@DiscriminatorValue(value = "SalesInvoice")
public class IObSalesInvoiceCompanyDataGeneralModel
    extends IObAccountingDocumentCompanyDataGeneralModel {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IDObSalesInvoice.SYS_NAME;
  }
}
