package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObGoodsIssueGeneralModel {
  String SYS_NAME = "GoodsIssue";
  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String PURCHASING_UNIT = "purchaseUnit";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME_EN);

  String CREATION_INFO = "creationInfo";
  BasicAttribute creationInfoAttr = new BasicAttribute(CREATION_INFO);

  String USER_CODE = "userCode";
  String TYPE_ID = "typeId";
  String TYPE_NAME_EN = "typeNameEn";
  String TYPE_NAME = "typeName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String CURRENT_STATES = "currentStates";
  String CREATION_DATE = "creationDate";
  String STORE_HOUSE_NAME = "storehouseName";
  String STOREHOUSE_CODE = "storehouseCode";
  String CUSTOMER_NAME = "customerName";
  String CUSTOMER = "customer";


  String STOREKEEPER_NAME = "storekeeperName";
  BasicAttribute storehouseCodeAttr = new BasicAttribute(STOREHOUSE_CODE);

}
