package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;

@Entity
public class IObGoodsReceiptSalesReturnData extends DocumentHeader {

  private static final long serialVersionUID = 1L;

  private Long salesReturnId;
  private Long customerId;
  private Long salesRepresentativeId;
  private String comments;


  public Long getSalesReturnId() {
    return salesReturnId;
  }

  public void setSalesReturnId(Long salesReturnId) {
    this.salesReturnId = salesReturnId;
  }

  public Long getCustomerId() {
    return customerId;
  }

  public void setCustomerId(Long customerId) {
    this.customerId = customerId;
  }

  public Long getSalesRepresentativeId() {
    return salesRepresentativeId;
  }

  public void setSalesRepresentativeId(Long salesRepresentativeId) {
    this.salesRepresentativeId = salesRepresentativeId;
  }

  public String getComments() {
    return comments;
  }

  public void setComments(String comments) {
    this.comments = comments;
  }

  @Override
  public String getSysName() {
    return IDObGoodsReceiptSalesReturn.SYS_NAME;
  }
}
