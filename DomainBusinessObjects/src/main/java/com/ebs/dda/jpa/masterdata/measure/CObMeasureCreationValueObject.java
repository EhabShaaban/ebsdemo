package com.ebs.dda.jpa.masterdata.measure;

public class CObMeasureCreationValueObject {

  private String unitOfMeasureName;
  private Boolean isStandard;

  public String getUnitOfMeasureName() {
    return unitOfMeasureName;
  }

  public void setUnitOfMeasureName(String unitOfMeasureName) {
    this.unitOfMeasureName = unitOfMeasureName;
  }

  public Boolean getIsStandard() {
    return isStandard;
  }

  public void setIsStandard(Boolean isStandard) {
    this.isStandard = isStandard;
  }
}
