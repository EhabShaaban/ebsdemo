package com.ebs.dda.jpa.inventory.goodsreceipt;

public class DObGoodsReceiptCreationValueObject {

  private String companyCode;
  private String storeKeeperCode;
  private String storehouseCode;
  private String refDocumentCode;
  private String type;

  public String getCompanyCode() {
    return companyCode;
  }

  public String getStoreKeeperCode() {
    return storeKeeperCode;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public String getType() {
    return type;
  }
}
