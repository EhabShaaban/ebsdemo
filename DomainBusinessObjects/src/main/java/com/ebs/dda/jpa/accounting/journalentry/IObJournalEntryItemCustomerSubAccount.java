package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "iobjournalentryitemcustomersubaccount")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Customers")
public class IObJournalEntryItemCustomerSubAccount extends IObJournalEntryItem {

  private Long subAccountId;
  private String type;

  public void setSubAccountId(Long subAccountId) {
    this.subAccountId = subAccountId;
    super.setSubAccountId(subAccountId);
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
