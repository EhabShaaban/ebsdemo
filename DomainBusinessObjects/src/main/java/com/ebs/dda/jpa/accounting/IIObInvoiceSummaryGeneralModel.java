package com.ebs.dda.jpa.accounting;

public interface IIObInvoiceSummaryGeneralModel {

  String SI_CODE = "salesInvoiceCode";
  String AMOUNT_BEFORE_TAXES = "totalAmountBeforeTaxes";
  String TAX_AMOUNT = "taxAmount";
  String NET_AMOUNT = "netAmount";
  String REMAINING_AMOUNT = "totalRemaining";
  String DOWN_PAYMENT = "downpayment";
}

