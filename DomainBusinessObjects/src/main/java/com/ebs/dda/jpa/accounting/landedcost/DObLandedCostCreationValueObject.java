package com.ebs.dda.jpa.accounting.landedcost;

public class DObLandedCostCreationValueObject {

  private String landedCostType;
  private String purchaseOrderCode;
  private Long documentOwnerId;

  public String getLandedCostType() {
    return landedCostType;
  }

  public void setLandedCostType(String landedCostType) {
    this.landedCostType = landedCostType;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }
}
