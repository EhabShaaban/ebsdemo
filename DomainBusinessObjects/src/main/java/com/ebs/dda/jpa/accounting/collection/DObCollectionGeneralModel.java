package com.ebs.dda.jpa.accounting.collection;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObCollectionGeneralModel")
@EntityInterface(IDObCollectionGeneralModel.class)
public class DObCollectionGeneralModel extends DocumentObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitCode;
  private String purchaseUnitNameEn;

  private String collectionType;
  private String companyCode;
  private String refDocumentCode;
  private String refDocumentTypeCode;
  private String collectionDocumentType;

  private String documentOwner;

  private String documentOwnerUserName;

  private Long documentOwnerId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString refDocumentTypeName;

  private BigDecimal amount;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString businessPartnerType;

  private String businessPartnerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyLocalCurrencyName;

  private String companyLocalCurrencyCode;

  private String companyLocalCurrencyISO;

  private String collectionMethod;

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getCollectionType() {
    return collectionType;
  }

  public LocalizedString getbusinessPartnerName() {
    return businessPartnerName;
  }

  public String getbusinessPartnerCode() {
    return businessPartnerCode;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public String getRefDocumentTypeCode() {
    return refDocumentTypeCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getCompanyLocalCurrencyCode() {
    return companyLocalCurrencyCode;
  }

  public LocalizedString getCompanyLocalCurrencyName() {
    return companyLocalCurrencyName;
  }

  public String getCompanyLocalCurrencyIso() {
    return companyLocalCurrencyISO;
  }

  public LocalizedString getRefDocumentTypeName() {
    return refDocumentTypeName;
  }

  public String getCollectionDocumentType() {
    return collectionDocumentType;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  public String getDocumentOwnerUserName() {
    return documentOwnerUserName;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public LocalizedString getBusinessPartnerType() {
    return businessPartnerType;
  }

  public String getCollectionMethod() {
    return collectionMethod;
  }

  @Override
  public String getSysName() {
    return IDObCollectionGeneralModel.SYS_NAME;
  }
}
