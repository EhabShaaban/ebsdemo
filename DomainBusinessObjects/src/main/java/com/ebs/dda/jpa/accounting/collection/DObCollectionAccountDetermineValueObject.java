package com.ebs.dda.jpa.accounting.collection;

public class DObCollectionAccountDetermineValueObject {

  private String collectionCode;
  private String collectionType;

  public String getCollectionCode() {
    return collectionCode;
  }

  public void setCollectionCode(String collectionCode) {
    this.collectionCode = collectionCode;
  }

  public String getCollectionType() {
    return collectionType;
  }

  public void setCollectionType(String collectionType) {
    this.collectionType = collectionType;
  }
}
