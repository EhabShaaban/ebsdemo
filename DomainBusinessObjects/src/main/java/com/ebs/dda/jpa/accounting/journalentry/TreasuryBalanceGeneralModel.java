package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "TreasuryBalanceGeneralModel")
public class TreasuryBalanceGeneralModel extends BalanceGeneralModel {

  private String treasuryCode;

  public String getTreasuryCode() {
    return treasuryCode;
  }
}
