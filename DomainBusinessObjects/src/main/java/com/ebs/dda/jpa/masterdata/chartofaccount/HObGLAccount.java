package com.ebs.dda.jpa.masterdata.chartofaccount;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.HierarchyObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.*;

@Entity
@Table(name = "HObGLAccount")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "level", discriminatorType = DiscriminatorType.STRING)
@EntityInterface(IHObChartOfAccounts.class)
public abstract class HObGLAccount extends HierarchyObject<DefaultUserCode> {

    private Long parentId;
    private String oldAccountCode;

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setOldAccountCode(String oldAccountCode) {
        this.oldAccountCode = oldAccountCode;
    }

    @Override
    public String getSysName() {
        return IHObChartOfAccounts.SYS_NAME;
    }
}
