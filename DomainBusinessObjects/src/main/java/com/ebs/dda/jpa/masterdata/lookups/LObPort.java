package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.*;
import java.io.Serializable;

/** @author xerix on Wed Nov 2017 at 15 : 55 */
@Entity
@Table(name = "portsCountry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("34")
public class LObPort extends LObMaterial implements Serializable {

  public static final byte COUNTRY_ATTR_CODE = 10;
  public static final String COUNTRY_ATTR_NAME = "country";
  public static final byte COUNTRY_NAME_ATTR_CODE = 20;
  public static final String COUNTRY_NAME_ATTR_NAME = "countryName";
  private static final long serialVersionUID = 1L;

  @Column(name = "countryid")
  private Long country;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "countryName")
  private LocalizedString countryName;

  public Long getCountry() {
    return country;
  }

  public void setCountry(Long country) {
    this.country = country;
  }

  public LocalizedString getCountryName() {
    return countryName;
  }

  public void setCountryName(LocalizedString countryName) {
    this.countryName = countryName;
  }

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_PORT_SYS_NAME;
  }
}
