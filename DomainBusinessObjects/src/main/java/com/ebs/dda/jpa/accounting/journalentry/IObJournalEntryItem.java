package com.ebs.dda.jpa.accounting.journalentry;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "IObJournalEntryItem")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "type")
@DiscriminatorValue("NONE")
public class IObJournalEntryItem extends DocumentLine implements Serializable {

  private Long accountId;
  private Long documentCurrencyId;
  private Long companyCurrencyId;
  private Long companyGroupCurrencyId;
  private BigDecimal credit;
  private BigDecimal debit;
  private BigDecimal companyCurrencyPrice;
  private Long companyExchangeRateId;
  private BigDecimal companyGroupCurrencyPrice;
  private Long companyGroupExchangeRateId;
  @Transient private Long subAccountId;

  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public Long getDocumentCurrencyId() {
    return documentCurrencyId;
  }

  public void setDocumentCurrencyId(Long currency) {
    this.documentCurrencyId = currency;
  }

  public Long getCompanyCurrencyId() {
    return companyCurrencyId;
  }

  public void setCompanyCurrencyId(Long companyCurrency) {
    this.companyCurrencyId = companyCurrency;
  }

  public BigDecimal getCredit() {
    return credit;
  }

  public void setCredit(BigDecimal credit) {
    this.credit = credit;
  }

  public BigDecimal getDebit() {
    return debit;
  }

  public void setDebit(BigDecimal debit) {
    this.debit = debit;
  }

  public BigDecimal getCompanyCurrencyPrice() {
    return companyCurrencyPrice;
  }

  public void setCompanyCurrencyPrice(BigDecimal currencyPrice) {
    this.companyCurrencyPrice = currencyPrice;
  }

  public Long getCompanyExchangeRateId() {
    return companyExchangeRateId;
  }

  public void setCompanyExchangeRateId(Long exchangeRateId) {
    this.companyExchangeRateId = exchangeRateId;
  }

  public Long getSubAccountId() {
    return subAccountId;
  }

  public void setSubAccountId(Long subAccountId) {
    this.subAccountId = subAccountId;
  }

  public Long getCompanyGroupCurrencyId() {
    return companyGroupCurrencyId;
  }

  public void setCompanyGroupCurrencyId(Long companyGroupCurrencyId) {
    this.companyGroupCurrencyId = companyGroupCurrencyId;
  }

  public BigDecimal getCompanyGroupCurrencyPrice() {
    return companyGroupCurrencyPrice;
  }

  public void setCompanyGroupCurrencyPrice(BigDecimal companyGroupCurrencyPrice) {
    this.companyGroupCurrencyPrice = companyGroupCurrencyPrice;
  }

  public Long getCompanyGroupExchangeRateId() {
    return companyGroupExchangeRateId;
  }

  public void setCompanyGroupExchangeRateId(Long companyGroupExchangeRateId) {
    this.companyGroupExchangeRateId = companyGroupExchangeRateId;
  }
}
