package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DObAccountingJournalEntryPreparationGeneralModel")
public class DObAccountingJournalEntryPreparationGeneralModel {
    @Id
    private Long id;
    private String code;
    private String objectTypeCode;
    private Long businessUnitId;
    private Long companyId;
    private Long fiscalPeriodId;
    @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
    private DateTime dueDate;

    public Long getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public Long getBusinessUnitId() {
        return businessUnitId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public Long getFiscalPeriodId() {
        return fiscalPeriodId;
    }

    public String getObjectTypeCode() {
        return objectTypeCode;
    }

    public DateTime getDueDate() {
        return dueDate;
    }
}
