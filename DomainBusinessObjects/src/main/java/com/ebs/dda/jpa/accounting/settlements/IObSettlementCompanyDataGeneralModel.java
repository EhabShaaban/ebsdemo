package com.ebs.dda.jpa.accounting.settlements;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;

@Entity
@DiscriminatorValue(value = "Settlement")
public class IObSettlementCompanyDataGeneralModel
				extends IObAccountingDocumentCompanyDataGeneralModel {

	@Override
	public String getSysName() {
		return IDObSettlement.SYS_NAME;
	}
}
