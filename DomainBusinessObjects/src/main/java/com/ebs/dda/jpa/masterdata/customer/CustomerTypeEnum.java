package com.ebs.dda.jpa.masterdata.customer;

public enum CustomerTypeEnum {
  LOCAL(Values.LOCAL);

  private CustomerTypeEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }

  public static class Values {
    public static final String LOCAL = "LOCAL";
  }
}
