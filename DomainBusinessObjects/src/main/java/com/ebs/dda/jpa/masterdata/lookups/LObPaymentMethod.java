package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("5")
public class LObPaymentMethod extends LObMaterial {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_PAYMENT_METHOD_SYS_NAME;
  }
}
