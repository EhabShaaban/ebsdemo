package com.ebs.dda.jpa.accounting.journalentry;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObJournalEntryGeneralModel {

  String CODE = "userCode";
  String CREATION_DATE = "creationDate";
  String CREATED_BY = "creationInfo";
  String TYPE = "type";
  String DUE_DATE = "dueDate";
  String COMPANY_NAME = "companyName";
  String DOCUMENT_REFERENCE = "documentReference";
  String DOCUMENT_TYPE = "Type";
  String DOCUMENT_CODE = "Code";
  String DOCUMENT_REFERENCE_TYPE = "documentReferenceType";
  String DOCUMENT_REFERENCE_CODE = "documentReferenceCode";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASE_UNIT_NAME_EN);
  String FISCAL_PERIOD = "fiscalPeriod";
}
