package com.ebs.dda.jpa.inventory.inventorydocument;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.inventory.goodsissue.IIObInventoryCompanyGeneralModel;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IObInventoryCompanyGeneralModel")
@EntityInterface(IIObInventoryCompanyGeneralModel.class)
public class IObInventoryCompanyGeneralModel {

  @Id private String id;

  private String inventoryDocumentCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storehouseName;

  private String storehouseCode;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private String inventoryDocumentType;

  public String getInventoryDocumentCode() {
    return inventoryDocumentCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getStorehouseName() {
    return storehouseName;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getInventoryDocumentType() {
    return inventoryDocumentType;
  }
}
