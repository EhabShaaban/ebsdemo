package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@EntityInterface(IIObSalesOrderDataGeneralModel.class)
@Table(name = "IObSalesOrderDataGeneralModel")
public class IObSalesOrderDataGeneralModel extends InformationObject {

  private String salesOrderCode;
  private String customerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  private Long customerAddressId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerAddressName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerAddress;

  @Column(name = "customerContactPersonId")
  private Long contactPersonId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString contactPersonName;

  private String paymentTermCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString paymentTermName;

  private String currencyIso;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  private String currencyCode;

  private BigDecimal creditLimit;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime expectedDeliveryDate;

  private String notes;

  public String getSalesOrderCode() {
    return salesOrderCode;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public Long getCustomerAddressId() {
    return customerAddressId;
  }

  public LocalizedString getCustomerAddressName() {
    return customerAddressName;
  }

  public LocalizedString getCustomerAddress() {
    return customerAddress;
  }

  public LocalizedString getContactPersonName() {
    return contactPersonName;
  }

    public Long getContactPersonId() {
        return contactPersonId;
    }

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public LocalizedString getPaymentTermName() {
    return paymentTermName;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public BigDecimal getCreditLimit() {
    return creditLimit;
  }

  public DateTime getExpectedDeliveryDate() {
    return expectedDeliveryDate;
  }

  public String getNotes() {
    return notes;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  @Override
  public String getSysName() {
    return IDObSalesOrder.SYS_NAME;
  }

  public void setSalesOrderCode(String salesOrderCode) {
    this.salesOrderCode = salesOrderCode;
  }

  public void setCustomerCode(String customerCode) {
    this.customerCode = customerCode;
  }
}
