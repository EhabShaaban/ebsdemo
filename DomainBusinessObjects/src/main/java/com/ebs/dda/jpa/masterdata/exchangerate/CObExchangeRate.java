package com.ebs.dda.jpa.masterdata.exchangerate;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.joda.time.DateTime;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

/** @author xerix on Wed Nov 2017 at 14 : 09 */
@Entity
@Table(name = "cobexchangerate")
@EntityInterface(ICObExchangeRate.class)
public class CObExchangeRate extends ConfigurationObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "typeId")
  private Long typeId;

  @Column(name = "secondcurrencyid")
  private Long secondCurrencyId;

  @Column(name = "firstcurrencyid")
  private Long firstCurrencyId;

  @Column(name = "secondvalue")
  private BigDecimal secondValue;

  @Column(name = "firstvalue")
  private BigDecimal firstValue;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(name = "validfrom")
  private DateTime validFrom;

  public CObExchangeRate() {}

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Long getTypeId() {
    return typeId;
  }


  public void setTypeId(Long typeId) {
    this.typeId = typeId;
  }


  public Long getSecondCurrencyId() {
    return secondCurrencyId;
  }

  public void setSecondCurrencyId(Long secondCurrencyId) {
    this.secondCurrencyId = secondCurrencyId;
  }

  public Long getFirstCurrencyId() {
    return firstCurrencyId;
  }

  public void setFirstCurrencyId(Long firstCurrencyId) {
    this.firstCurrencyId = firstCurrencyId;
  }

  public BigDecimal getSecondValue() {
    return secondValue;
  }

  public void setSecondValue(BigDecimal secondValue) {
    this.secondValue = secondValue;
  }

  public BigDecimal getFirstValue() {
    return firstValue;
  }

  public void setFirstValue(BigDecimal firstValue) {
    this.firstValue = firstValue;
  }


  public DateTime getValidFrom() {
    return validFrom;
  }

  public void setValidFrom(DateTime validFrom) {
    this.validFrom = validFrom;
  }

  @Override
  public String getSysName() {
    return ICObExchangeRate.SYS_NAME;
  }
}
