package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class PaymentFormEnum extends BusinessEnum {

  public static final String SYS_NAME = "PaymentForm";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private PaymentForm value;

  public PaymentFormEnum() {}

  @Override
  public PaymentForm getId() {
    return this.value;
  }

  public void setId(PaymentForm id) {
    this.value = id;
  }

  public enum PaymentForm {
    CASH,
    BANK
  }
}
