package com.ebs.dda.jpa.masterdata.customer;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IIObCustomerAddressGeneralModel.class)
@Table(name = "IObCustomerAddressGeneralModel")
public class IObCustomerAddressGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString addressName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString addressDescription;

  public LocalizedString getAddressName() {
    return addressName;
  }

  public LocalizedString getAddressDescription() {
    return addressDescription;
  }

  @Override
  public String getSysName() {
    return IIObCustomerAddressGeneralModel.SYS_NAME;
  }
}
