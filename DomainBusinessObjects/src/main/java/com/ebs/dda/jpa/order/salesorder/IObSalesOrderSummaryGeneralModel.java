package com.ebs.dda.jpa.order.salesorder;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesOrderSummaryGeneralModel")
public class IObSalesOrderSummaryGeneralModel {

  @Id
  private Long id;

  private String orderCode;

  private BigDecimal totalAmountBeforeTaxes;
  private BigDecimal totalTaxes;
  private BigDecimal totalAmountAfterTaxes;

  public String getOrderCode() {
    return orderCode;
  }

  public BigDecimal getTotalAmountBeforeTaxes() {
    return totalAmountBeforeTaxes;
  }

  public BigDecimal getTotalTaxes() {
    return totalTaxes;
  }

  public BigDecimal getTotalAmountAfterTaxes() {
    return totalAmountAfterTaxes;
  }
}
