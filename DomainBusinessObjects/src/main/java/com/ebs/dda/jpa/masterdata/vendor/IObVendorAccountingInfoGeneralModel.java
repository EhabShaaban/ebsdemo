package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObVendorAccountingInfoGeneralModel")
public class IObVendorAccountingInfoGeneralModel extends InformationObject {
    private String accountCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString accountName;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString vendorName;

    private String vendorCode;

    private String vendorPurchaseUnitCodes;

    private String accountLeadger;

    @Override
    public String getSysName() {
        return "VendorAccountingInfo";
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public LocalizedString getAccountName() {
        return accountName;
    }

    public void setAccountName(LocalizedString accountName) {
        this.accountName = accountName;
    }

    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    public LocalizedString getVendorName() {
        return vendorName;
    }

    public String getAccountLeadger() {
        return accountLeadger;
    }

    public String getVendorPurchaseUnitCodes() {
        return vendorPurchaseUnitCodes;
    }
}
