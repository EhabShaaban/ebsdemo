package com.ebs.dda.jpa.order.salesreturnorder;

public interface IIObSalesReturnOrderItemValueObject {
  String RETURN_REASON_CODE = "returnReasonCode";
  String RETURN_QUANTITY = "returnQuantity";
}
