package com.ebs.dda.jpa.accounting.salesinvoice;


import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObSalesInvoiceGeneralModel {

  String SYS_NAME = "SalesInvoice";

  String INVOICE_TYPE_CODE = "invoiceTypeCode";
  String INVOICE_TYPE = "invoiceType";
  String INVOICE_TYPE_NAME_EN = "invoiceType";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_UNIT = "purchaseUnitName";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String DOCUMENT_OWNER = "documentOwner";
  String DOCUMENT_OWNER_USER_NAME = "documentOwnerUserName";
  String CURRENT_STATES = "currentStates";
  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";
  String INVOICE_CODE = "userCode";
  String SALES_ORDER = "salesOrder";
  String COMPANY = "company";
  String COMPANY_CODE_NAME = "companyCodeName";
  String CUSTOMER = "customer";
  String CREATION_INFO_NAME = "creationInfoName";
  String CUSTOMER_CODE_NAME = "customerCodeName";

  String CUSTOMER_CODE = "customerCode";
  String CUSTOMER_NAME = "customerName";
  String INVOICE_TYPE_NAME = "invoiceTypeName";
  String PURCHASE_UNIT_NAME = "purchaseUnitName";
  String PURCHASING_UNIT_NAME = "purchaseUnitNameEn";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String COLLECTION_RESPONSIBLE_NAME = "collectionResponsibleName";
  String COMPANY_TAXES = "CompanyTaxes";
  String CURRENCY_ISO = "currencyIso";
  String CURRENCY_NAME = "currencyName";
  String LOGGED_IN_USER = "LoggedInUser";

  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASE_UNIT);
  BasicAttribute invoiceTypeCodeAttr = new BasicAttribute(INVOICE_TYPE_CODE);
  BasicAttribute documentOwnerAttr = new BasicAttribute(DOCUMENT_OWNER);
  BasicAttribute documentOwnerUserNameAttr = new BasicAttribute(DOCUMENT_OWNER_USER_NAME);


  String REMAINING = "remaining";
}
