package com.ebs.dda.jpa.accounting.costing;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IDObCostingGeneralModel {

    String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
    String PURCHASE_UNIT_NAME_ATTR = "purchaseUnitName";
    String PURCHASE_UNIT_CODE = "purchaseUnitCode";
    LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
            new LocalizedTextAttribute<>(PURCHASE_UNIT_NAME_ATTR);

}
