package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.*;

@Entity
@Table(name = "DObLandedCostJournalEntry")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue("LandedCost")
public class DObLandedCostJournalEntry extends DObJournalEntry {

  private Long documentReferenceId;

  public void setDocumentReferenceId(Long documentReferenceId) {
    this.documentReferenceId = documentReferenceId;
  }
}
