package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class IObGoodsIssueItem extends DocumentLine implements Serializable {

  private static final long serialVersionUID = 1L;

  Long itemId;
  Long unitOfMeasureId;
  BigDecimal quantity;
  String batchNo;
  BigDecimal price;

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public void setUnitOfMeasureId(Long unitOfMeasureId) {
    this.unitOfMeasureId = unitOfMeasureId;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public void setBatchNo(String batchNo) {
    this.batchNo = batchNo;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}
