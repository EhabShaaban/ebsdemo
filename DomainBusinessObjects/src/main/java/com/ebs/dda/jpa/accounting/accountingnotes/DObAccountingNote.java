package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "DObAccountingNote")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@EntityInterface(IDObAccountingNote.class)
public class DObAccountingNote extends DObAccountingDocument {

  private String type;

  private Double remaining;

  public void setType(String type) {
    this.type = type;
  }

  public Double getRemaining() {
    return remaining;
  }

  public void setRemaining(Double remaining) {
    this.remaining = remaining;
  }

  @Override
  public String getSysName() {
    return IDObAccountingNote.SYS_NAME;
  }
}
