package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.processing.CompositeAttribute;

/**
 * @author Ahmed Ali Elfeky
 * @since Oct 30, 2017 , 3:11:55 PM
 */
public interface IMObItem {
  String SYS_NAME = "Item";
  String MARKET_NAME = "marketName";
  String PRODUCT_MANAGER_CODE = "productManagerCode";


  String PURCHASE_UNIT_ATTR_NAME = "purchaseunit";
  CompositeAttribute<IObItemPurchaseUnit> purchseUnitAttr =
      new CompositeAttribute<>( PURCHASE_UNIT_ATTR_NAME, IObItemPurchaseUnit.class, true);

  String ALTERNATIVE_UOM_ATTR_NAME = "alternativeuom";
  CompositeAttribute<IObAlternativeUoM> alternativeUOMAttr =
      new CompositeAttribute<>( ALTERNATIVE_UOM_ATTR_NAME, IObAlternativeUoM.class, true);

    String ACCOUNT_INFO_NAME = "itemaccountinfo";
    CompositeAttribute<IObItemAccountingInfo> itemAccountInfoAttr =
            new CompositeAttribute<>(ACCOUNT_INFO_NAME, IObItemAccountingInfo.class, false);
}
