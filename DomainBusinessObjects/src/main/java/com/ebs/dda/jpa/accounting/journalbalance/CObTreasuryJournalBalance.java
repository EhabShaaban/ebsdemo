package com.ebs.dda.jpa.accounting.journalbalance;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObTreasuryJournalBalance")
@DiscriminatorValue(value = "TREASURY")
public class CObTreasuryJournalBalance extends CObJournalBalance {
  private Long treasuryId;

  public void setTreasuryId(Long treasuryId) {
    this.treasuryId = treasuryId;
  }
}
