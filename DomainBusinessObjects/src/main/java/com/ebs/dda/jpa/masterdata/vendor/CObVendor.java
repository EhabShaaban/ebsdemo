package com.ebs.dda.jpa.masterdata.vendor;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "CObVendor")
@EntityInterface(ICObVendor.class)
public class CObVendor extends ConfigurationObject<DefaultUserCode> implements Serializable {

    @Override
    public String getSysName() {
        return ICObVendor.SYS_NAME;
    }
}
