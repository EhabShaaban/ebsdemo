package com.ebs.dda.jpa.order.salesreturnorder;

import java.math.BigDecimal;

public class IObSalesReturnOrderItemValueObject {
  private String salesReturnOrderCode;
  private Long itemId;
  private String returnReasonCode;
  private BigDecimal returnQuantity;

  public IObSalesReturnOrderItemValueObject(String salesReturnOrderCode) {
    setSalesReturnOrderCode(salesReturnOrderCode);
  }

  public String getSalesReturnOrderCode() {
    return salesReturnOrderCode;
  }

  public void setSalesReturnOrderCode(String salesReturnOrderCode) {
    this.salesReturnOrderCode = salesReturnOrderCode;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public String getReturnReasonCode() {
    return returnReasonCode;
  }

  public void setReturnReasonCode(String returnReasonCode) {
    this.returnReasonCode = returnReasonCode;
  }

  public BigDecimal getReturnQuantity() {
    return returnQuantity;
  }

  public void setReturnQuantity(BigDecimal returnQuantity) {
    this.returnQuantity = returnQuantity;
  }
}
