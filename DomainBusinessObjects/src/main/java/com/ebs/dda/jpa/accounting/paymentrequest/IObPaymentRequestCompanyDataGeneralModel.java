package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "PaymentRequest")
public class IObPaymentRequestCompanyDataGeneralModel extends
    IObAccountingDocumentCompanyDataGeneralModel {

  @Override
  public String getSysName() {
    return IDObPaymentRequest.SYS_NAME;
  }
}
