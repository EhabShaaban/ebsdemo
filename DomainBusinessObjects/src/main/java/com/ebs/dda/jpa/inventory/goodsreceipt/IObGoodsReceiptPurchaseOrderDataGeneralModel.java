package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

// Created By Hossam Hassan @ 7/24/18
@Entity
@Table(name = "IObGoodsReceiptPurchaseOrderDataGeneralModel")
@EntityInterface(IIObGoodsReceiptPurchaseOrderDataGeneralModel.class)
public class IObGoodsReceiptPurchaseOrderDataGeneralModel {
  @Id private Long id;

  private Long refInstanceId;

  private String goodsReceiptCode;

  private String purchaseOrderCode;
  private String vendorCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString vendorName;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  @EmbeddedAttribute
  private LocalizedString purchaseUnitName;

  private String purchaseResponsibleName;

  private String deliveryNote;

  private String comments;

  private String costingDocumentCode;

  public String getCostingDocumentCode() {
    return costingDocumentCode;
  }

  public Long getId() {
    return id;
  }

  public Long getRefInstanceId() {
    return refInstanceId;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getDeliveryNote() {
    return deliveryNote;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseResponsibleName() {
    return purchaseResponsibleName;
  }

  public String getComments() {
    return comments;
  }
}
