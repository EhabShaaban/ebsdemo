package com.ebs.dda.jpa.accounting.accountingnotes;

public interface IAccountingNoteCreateValueObject {
  String NOTE_TYPE = "noteType";
  String TYPE = "type";
  String BUSINESS_PARTNER_CODE = "businessPartnerCode";
  String COMPANY_CODE = "companyCode";
  String BUSINESS_UNIT_CODE = "businessUnitCode";
  String AMOUNT = "amount";
  String CURRENCY_ISO = "currencyIso";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
  String REFERENCE_DOCUMENT_CODE = "referenceDocumentCode";
}
