package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "dobsettlement")
@DiscriminatorValue(value = "Settlement")
public class DObSettlement extends DObAccountingDocument implements Serializable {
    private static final long serialVersionUID = 1L;

    private String settlementType;

    public void setSettlementType(String settlementType) {
        this.settlementType = settlementType;
    }

    @Override
    public String getSysName() {
        return IDObSettlement.SYS_NAME;
    }
}
