package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface ICObCompany{

  String SYS_NAME = "Company";
  String COMPANY_LOGO_ATTR_NAME = "companyLogo";
  CompositeAttribute<IObCompanyLogoDetails> companyLogoAttr =
      new CompositeAttribute<>( COMPANY_LOGO_ATTR_NAME, IObCompanyLogoDetails.class, false);

}
