package com.ebs.dda.jpa.accounting.landedcost;

public class IObLandedCostFactorItemDeleteValueObject {

	private String landedCostCode;
	private Long itemId;

	public String getLandedCostCode() {
		return landedCostCode;
	}

	public void setLandedCostCode(String landedCostCode) {
		this.landedCostCode = landedCostCode;
	}

	public Long getItemId() {
		return itemId;
	}

	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
}
