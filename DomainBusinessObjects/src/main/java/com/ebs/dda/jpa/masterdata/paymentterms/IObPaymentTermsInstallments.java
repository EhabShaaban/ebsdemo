package com.ebs.dda.jpa.masterdata.paymentterms;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObPaymentTermsInstallments")
public class IObPaymentTermsInstallments extends InformationObject {

  private static final long serialVersionUID = 1L;

  private Long paymentMethod;

  private Short paymentPercentage;

  private Short creditPeriod;

  private Long defaultBaselineDate;

  private Short baselineFixedDate;

  private Short baselineAdditionalMonths;


  public Short getPaymentPercentage() {
    return paymentPercentage;
  }

  public void setPaymentPercentage(Short paymentPercentage) {
    this.paymentPercentage = paymentPercentage;
  }

  public Short getCreditPeriod() {
    return creditPeriod;
  }

  public void setCreditPeriod(Short creditPeriod) {
    this.creditPeriod = creditPeriod;
  }

  public Short getBaselineFixedDate() {
    return baselineFixedDate;
  }

  public void setBaselineFixedDate(Short alternativeBaselineFixedDate) {
    this.baselineFixedDate = alternativeBaselineFixedDate;
  }

  public Short getBaselineAdditionalMonths() {
    return baselineAdditionalMonths;
  }

  public void setBaselineAdditionalMonths(Short alternativeBaselineAddMonths) {
    this.baselineAdditionalMonths = alternativeBaselineAddMonths;
  }

  @Override
  public String getSysName() {
    return "IObPaymentTermsInstallments";
  }
}
