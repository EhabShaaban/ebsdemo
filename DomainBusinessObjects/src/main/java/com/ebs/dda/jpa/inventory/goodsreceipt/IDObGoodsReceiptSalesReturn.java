package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObGoodsReceiptSalesReturn {

    String SYS_NAME = "GoodsReceiptSalesReturn";

    String REF_DOCUMENT_ATTR_NAME = "refDocument";
    CompositeAttribute<IObGoodsReceiptSalesReturnData> salesReturnRefDocumentAttr =
            new CompositeAttribute<>(REF_DOCUMENT_ATTR_NAME, IObGoodsReceiptSalesReturnData.class, false);

    String SR_ORDINARY_ITEMS_ATTR_NAME = "salesReturnOrdinaryItems";
    CompositeAttribute<IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData> salesReturnOrdinaryItems =
            new CompositeAttribute<>(
                    SR_ORDINARY_ITEMS_ATTR_NAME,
                    IObGoodsReceiptSalesReturnOrdinaryReceivedItemsData.class,
                    true);
}
