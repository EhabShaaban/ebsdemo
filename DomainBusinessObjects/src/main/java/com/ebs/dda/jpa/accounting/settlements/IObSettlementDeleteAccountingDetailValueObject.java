package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IValueObject;

public class IObSettlementDeleteAccountingDetailValueObject implements IValueObject {
	String settlementCode;
	Long accountingDetailId;

	public String getSettlementCode() {
		return settlementCode;
	}

	public void setSettlementCode(String settlementCode) {
		this.settlementCode = settlementCode;
	}

	public Long getAccountingDetailId() {
		return accountingDetailId;
	}

	public void setAccountingDetailId(Long accountingDetailId) {
		this.accountingDetailId = accountingDetailId;
	}
}
