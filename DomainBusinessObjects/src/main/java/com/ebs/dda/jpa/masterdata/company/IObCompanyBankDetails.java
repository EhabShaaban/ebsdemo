package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dda.jpa.masterdata.bank.BankDetails;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCompanyBankDetails")
public class IObCompanyBankDetails extends BankDetails {

  private static final long serialVersionUID = 1L;

  private Long currencyId;

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  @Override
  public String getSysName() {
    return IIObCompanyBankDetails.SYS_NAME;
  }
}
