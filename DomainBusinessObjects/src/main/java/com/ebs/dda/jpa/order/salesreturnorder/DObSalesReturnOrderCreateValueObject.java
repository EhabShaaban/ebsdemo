package com.ebs.dda.jpa.order.salesreturnorder;

public class DObSalesReturnOrderCreateValueObject {
  private String typeCode;
  private Long documentOwnerId;
  private String salesInvoiceCode;

  public String getTypeCode() {
    return typeCode;
  }

  public void setTypeCode(String typeCode) {
    this.typeCode = typeCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }

  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public void setSalesInvoiceCode(String salesInvoiceCode) {
    this.salesInvoiceCode = salesInvoiceCode;
  }
}
