package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObGoodsReceiptGeneralModel {
  String USER_CODE = "userCode";
  String CREATION_DATE = "creationDate";
  String CREATION_INFO = "creationInfo";
  String STATE = "currentStates";
  String TYPE = "type";
  String STOREKEEPER_CODE = "storekeeperCode";
  String STOREKEEPER_NAME = "storekeeperName";
  String STOREHOUSE_CODE = "storehouseCode";
  String STOREHOUSE_NAME = "storehouseName";
  String ACTIVATION_DATE = "activationDate";
  String PURCHASE_UNIT_CODE = "purchaseunitcode";
  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";
  String BUSINESS_PARTNER_NAME = "businessPartnerName";
  String BUSINESS_PARTNER = "businessPartner";
  String TYPE_NAME = "typeName";

  BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME);
  BasicAttribute storehouseCodeAttr = new BasicAttribute(STOREHOUSE_CODE);
}
