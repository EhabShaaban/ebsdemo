package com.ebs.dda.jpa.masterdata.salescoordinator;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("11")
@EntityInterface(ICObSalesCoordinator.class)
public class CObSalesCoordinator extends CObEnterprise {

  @Override
  public String getSysName() {
    return ICObSalesCoordinator.SYS_NAME;
  }
}
