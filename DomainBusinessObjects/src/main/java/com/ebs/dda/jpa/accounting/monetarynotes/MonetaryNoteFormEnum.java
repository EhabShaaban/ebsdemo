package com.ebs.dda.jpa.accounting.monetarynotes;

public enum MonetaryNoteFormEnum {
  CHEQUE(Values.CHEQUE),
  BILL_OF_EXCHANGE(Values.BILL_OF_EXCHANGE),
  TRUST_RECEIPT(Values.TRUST_RECEIPT);


  private MonetaryNoteFormEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }

  public static class Values {
    public static final String CHEQUE = "CHEQUE";
    public static final String BILL_OF_EXCHANGE = "BILL_OF_EXCHANGE";
    public static final String TRUST_RECEIPT = "TRUST_RECEIPT";
  }
}
