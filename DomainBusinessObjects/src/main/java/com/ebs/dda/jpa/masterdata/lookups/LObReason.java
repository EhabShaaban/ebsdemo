package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "lobreason")
@EntityInterface(ILObReason.class)
public class LObReason extends LocalizedLookupObject<DefaultUserCode> implements Serializable {

  private static final long serialVersionUID = 1L;
  private String type;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_REASON;
  }
}
