package com.ebs.dda.jpa.masterdata.storehouse;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.masterdata.enterprise.CObEnterprise;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "CObStorehouse")
@DiscriminatorValue("4")
@Inheritance(strategy = InheritanceType.JOINED)
@EntityInterface(ICObStorehouse.class)
public class CObStorehouse extends CObEnterprise {

  private static final long serialVersionUID = 1L;

  @Column(name = "plantId")
  private Long plantId;

  @Override
  public String getSysName() {
    return ICObStorehouse.SYS_NAME;
  }

  public Long getPlantId() {
    return plantId;
  }

  public void setPlantId(Long plantId) {
    this.plantId = plantId;
  }
}
