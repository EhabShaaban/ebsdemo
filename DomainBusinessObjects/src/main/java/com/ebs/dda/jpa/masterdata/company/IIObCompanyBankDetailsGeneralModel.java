package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

public interface IIObCompanyBankDetailsGeneralModel extends IInformationObject {
  String BANK_ACCOUNT_NUMBER = "bankAccountNumber";


}
