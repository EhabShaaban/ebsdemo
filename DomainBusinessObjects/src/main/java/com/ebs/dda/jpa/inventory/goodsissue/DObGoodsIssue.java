package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.inventory.inventorydocument.DObInventoryDocument;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
@EntityInterface(IDObGoodsIssue.class)
public abstract class DObGoodsIssue extends DObInventoryDocument implements Serializable {

  private static final long serialVersionUID = 1L;
}
