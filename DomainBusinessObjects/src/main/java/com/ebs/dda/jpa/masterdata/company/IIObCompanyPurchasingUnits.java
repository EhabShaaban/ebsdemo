package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

public interface IIObCompanyPurchasingUnits extends IInformationObject {

  byte PURCHASING_UNIT_CODE = 10;
  String PURCHASING_UNIT_NAME = "purchasingUnit";
}
