package com.ebs.dda.jpa.masterdata.customer;

public interface IIObCustomerContactPersonGeneralModel {
    String SYS_NAME = "Customer";
    String CUSTOMER_NAME = "customerName";
    String CUSTOMER_CODE = "userCode";
    String CONTACT_PERSON_NAME = "contactPersonName";
}
