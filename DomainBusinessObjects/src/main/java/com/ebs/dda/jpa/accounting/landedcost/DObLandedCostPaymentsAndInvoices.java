package com.ebs.dda.jpa.accounting.landedcost;

import java.util.List;

public class DObLandedCostPaymentsAndInvoices {
    private List<DObLandedCostItemsInvoicesGeneralModel> itemsInvoices;
    private List<DObLandedCostItemsPaymentRequestsGeneralModel> itemsPaymentRequests;

    public List<DObLandedCostItemsInvoicesGeneralModel> getItemsInvoices() {
        return itemsInvoices;
    }

    public void setItemsInvoices(List<DObLandedCostItemsInvoicesGeneralModel> itemsInvoices) {
        this.itemsInvoices = itemsInvoices;
    }

    public List<DObLandedCostItemsPaymentRequestsGeneralModel> getItemsPaymentRequests() {
        return itemsPaymentRequests;
    }

    public void setItemsPaymentRequests(List<DObLandedCostItemsPaymentRequestsGeneralModel> itemsPaymentRequests) {
        this.itemsPaymentRequests = itemsPaymentRequests;
    }
}
