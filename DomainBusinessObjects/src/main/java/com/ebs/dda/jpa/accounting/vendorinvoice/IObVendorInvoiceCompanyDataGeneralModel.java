package com.ebs.dda.jpa.accounting.vendorinvoice;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.IIObAccountingDocumentCompanyDataGeneralModel;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentCompanyDataGeneralModel;


@Entity
@DiscriminatorValue(value = "VendorInvoice")
@EntityInterface(IIObAccountingDocumentCompanyDataGeneralModel.class)
public class IObVendorInvoiceCompanyDataGeneralModel
    extends IObAccountingDocumentCompanyDataGeneralModel {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IDObVendorInvoice.SYS_NAME;
  }
}
