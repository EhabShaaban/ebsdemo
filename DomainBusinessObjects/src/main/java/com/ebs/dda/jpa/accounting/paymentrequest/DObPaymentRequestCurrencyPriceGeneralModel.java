package com.ebs.dda.jpa.accounting.paymentrequest;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "DObPaymentRequestCurrencyPriceGeneralModel")
public class DObPaymentRequestCurrencyPriceGeneralModel {

  @Id private Long id;
  private String userCode;
  private BigDecimal netAmount;
  private BigDecimal currencyPrice;
  private String dueDocumentType;
  private String referenceDocumentCode;
  private String paymentType;

  public String getUserCode() {
    return userCode;
  }

  public BigDecimal getNetAmount() {
    return netAmount;
  }

  public String getDueDocumentType() {
    return dueDocumentType;
  }

  public String getReferenceDocumentCode() {
    return referenceDocumentCode;
  }

  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }

  public String getPaymentType() {
    return paymentType;
  }
}
