package com.ebs.dda.jpa;

import java.math.BigDecimal;

public class DocumentObjectUpdateRemainingValueObject {

    private String userCode;
    private BigDecimal amount;

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
