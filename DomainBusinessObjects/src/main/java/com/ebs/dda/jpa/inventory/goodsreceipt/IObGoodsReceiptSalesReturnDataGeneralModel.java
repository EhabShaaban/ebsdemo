package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IObGoodsReceiptSalesReturnDataGeneralModel")
public class IObGoodsReceiptSalesReturnDataGeneralModel {
  @Id
  private Long id;

  private Long refInstanceId;

  private String goodsReceiptCode;

  private String salesReturnOrderCode;
  private String customerCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  @EmbeddedAttribute
  private LocalizedString purchaseUnitName;

  private String salesRepresentativeName;

  private String salesRepresentativeId;

  private String comments;

  public Long getId() {
    return id;
  }

  public Long getRefInstanceId() {
    return refInstanceId;
  }

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getSalesReturnOrderCode() {
    return salesReturnOrderCode;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public String getSalesRepresentativeName() {
    return salesRepresentativeName;
  }

  public String getSalesRepresentativeId() {
    return salesRepresentativeId;
  }

  public String getComments() {
    return comments;
  }
}
