package com.ebs.dda.jpa.accounting.collection;

public class DObCollectionCreateValueObject {
  private String collectionType;
  private String collectionMethod;
  private String refDocumentCode;
  private Long documentOwnerId;

  public String getCollectionType() {
    return collectionType;
  }

  public void setCollectionType(String collectionType) {
    this.collectionType = collectionType;
  }

  public String getCollectionMethod() {
    return collectionMethod;
  }

  public void setCollectionMethod(String collectionMethod) {
    this.collectionMethod = collectionMethod;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public void setRefDocumentCode(String refDocumentCode) {
    this.refDocumentCode = refDocumentCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }
}
