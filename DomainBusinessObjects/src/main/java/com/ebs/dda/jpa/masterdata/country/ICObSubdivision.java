/** */
package com.ebs.dda.jpa.masterdata.country;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;

/**
 * @author Yara Ameen
 * @since Mar 27, 2016 9:49:39 AM
 */
public interface ICObSubdivision extends IStatefullBusinessObject {

  String SYS_NAME = "Subdivision";
}
