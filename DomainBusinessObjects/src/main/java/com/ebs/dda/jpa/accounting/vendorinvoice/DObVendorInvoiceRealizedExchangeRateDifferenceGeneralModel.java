package com.ebs.dda.jpa.accounting.vendorinvoice;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel")
public class DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel implements Serializable {
  @Id private Long id;
  private String paymentRequestUserCode;
  private String dueDocumentType;
  private String referenceDocumentCode;
  private BigDecimal paymentCurrencyPrice;
  private String paymentType;
  private String vendorInvoiceCode;
  private String vendorObjectTypeCode;
  private BigDecimal paymentRequestNetAmount;
  private Long companyCurrencyId;
  private Long vendorInvoiceCurrencyId;
  private BigDecimal vendorInvoiceCurrencyPrice;
  private Long exchangeRateId;
  private Long vendorInvoiceGlAccountId;
  private Long vendorInvoiceGlSubAccountId;
  private String vendorInvoiceAccountLedger;

  public Long getId() {
    return id;
  }

  public String getPaymentRequestUserCode() {
    return paymentRequestUserCode;
  }

  public String getDueDocumentType() {
    return dueDocumentType;
  }

  public String getReferenceDocumentCode() {
    return referenceDocumentCode;
  }

  public BigDecimal getPaymentCurrencyPrice() {
    return paymentCurrencyPrice;
  }

  public String getPaymentType() {
    return paymentType;
  }

  public String getVendorInvoiceCode() {
    return vendorInvoiceCode;
  }

  public String getVendorObjectTypeCode() {
    return vendorObjectTypeCode;
  }

  public Long getCompanyCurrencyId() {
    return companyCurrencyId;
  }

  public Long getVendorInvoiceCurrencyId() {
    return vendorInvoiceCurrencyId;
  }

  public BigDecimal getVendorInvoiceCurrencyPrice() {
    return vendorInvoiceCurrencyPrice;
  }

  public Long getExchangeRateId() {
    return exchangeRateId;
  }

  public Long getVendorInvoiceGlAccountId() {
    return vendorInvoiceGlAccountId;
  }

  public Long getVendorInvoiceGlSubAccountId() {
    return vendorInvoiceGlSubAccountId;
  }

  public String getVendorInvoiceAccountLedger() {
    return vendorInvoiceAccountLedger;
  }

  public BigDecimal getPaymentRequestNetAmount() {
    return paymentRequestNetAmount;
  }
}
