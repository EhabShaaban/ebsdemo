package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
public abstract class IObInvoiceItem extends DocumentLine {

    private Long itemId;

    private Long baseUnitOfMeasureId;

    private Long orderUnitOfMeasureId;

    @Column(name = "quantityinorderunit")
    private BigDecimal qunatityInOrderUnit;

    private BigDecimal price;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public Long getBaseUnitOfMeasureId() {
        return baseUnitOfMeasureId;
    }

    public void setBaseUnitOfMeasureId(Long baseUnitOfMeasureId) {
        this.baseUnitOfMeasureId = baseUnitOfMeasureId;
    }

    public Long getOrderUnitOfMeasureId() {
        return orderUnitOfMeasureId;
    }

    public void setOrderUnitOfMeasureId(Long orderUnitOfMeasureId) {
        this.orderUnitOfMeasureId = orderUnitOfMeasureId;
    }

    public BigDecimal getQunatityInOrderUnit() {
        return qunatityInOrderUnit;
    }

    public void setQunatityInOrderUnit(BigDecimal qunatityInOrderUnit) {
        this.qunatityInOrderUnit = qunatityInOrderUnit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
