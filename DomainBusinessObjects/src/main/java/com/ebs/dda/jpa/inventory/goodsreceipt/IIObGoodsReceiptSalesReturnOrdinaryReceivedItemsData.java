package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IIObGoodsReceiptSalesReturnOrdinaryReceivedItemsData {

    String SYS_NAME = "GoodsReceiptSalesReturnOrdinaryReceivedItems";
    String ORDINARY_ITEMS_OBJECT_TYPE_CODE = "1";

    CompositeAttribute<IObGoodsReceiptSalesReturnItemQuantities> iObGoodsReceiptItemQuantities =
            new CompositeAttribute(
                    "iObGoodsReceiptItemQuantities", IObGoodsReceiptSalesReturnItemQuantities.class, true);
}
