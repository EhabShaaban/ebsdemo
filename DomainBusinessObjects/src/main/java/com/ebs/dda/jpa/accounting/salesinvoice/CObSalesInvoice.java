package com.ebs.dda.jpa.accounting.salesinvoice;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CObSalesInvoice")
@EntityInterface(ICObSalesInvoice.class)
public class CObSalesInvoice extends ConfigurationObject<DefaultUserCode> implements Serializable {

  @Override
  public String getSysName() {
    return ICObSalesInvoice.SYS_NAME;
  }
}
