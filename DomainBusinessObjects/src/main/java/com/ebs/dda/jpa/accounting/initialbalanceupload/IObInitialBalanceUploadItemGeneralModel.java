package com.ebs.dda.jpa.accounting.initialbalanceupload;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObInitialBalanceUploadItemGeneralModel")
public class IObInitialBalanceUploadItemGeneralModel extends InformationObject {

  private String initialBalanceUploadCode;
  private Double debit;
  private Double credit;
  private String currencyISO;
  private Double currencyPrice;
  private String glAccountCode;
  private String glAccountName;
  private String subLedger;
  private String glSubAccountCode;
  private String glSubAccountName;

  public String getInitialBalanceUploadCode() {
    return initialBalanceUploadCode;
  }

  public Double getDebit() {
    return debit;
  }

  public Double getCredit() {
    return credit;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public Double getCurrencyPrice() {
    return currencyPrice;
  }

  public String getGlAccountCode() {
    return glAccountCode;
  }

  public String getSubLedger() {
    return subLedger;
  }

  public String getGlSubAccountCode() {
    return glSubAccountCode;
  }

  public String getGlAccountName() {
    return glAccountName;
  }

  public String getGlSubAccountName() {
    return glSubAccountName;
  }

  @Override
  public String getSysName() {
    return IDObInitialBalanceUpload.SYS_NAME;
  }
}
