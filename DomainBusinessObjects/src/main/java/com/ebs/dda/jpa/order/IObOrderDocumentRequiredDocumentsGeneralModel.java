package com.ebs.dda.jpa.order;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderDocumentRequiredDocumentsGeneralModel")
@EntityInterface(value = IObOrderDocumentRequiredDocumentsGeneralModel.class)
public class IObOrderDocumentRequiredDocumentsGeneralModel
    extends CodedBusinessObject<DefaultUserCode> {

  @Column(name = "numberofcopies")
  private Integer numberOfCopies;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "attachmentname")
  private LocalizedString attachmentName;

  private String attachmentCode;

  private String objectTypeCode;

  public String getAttachmentCode() {
    return attachmentCode;
  }

  public LocalizedString getAttachmentName() {
    return attachmentName;
  }

  public Integer getNumberOfCopies() {
    return numberOfCopies;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
