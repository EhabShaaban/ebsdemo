package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.DObAccountingDocumentJournalDetailPreparationGeneralModel;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObVendorInvoiceJournalDetailPreparationGeneralModel")
public class DObVendorInvoiceJournalDetailPreparationGeneralModel extends DObAccountingDocumentJournalDetailPreparationGeneralModel {
}
