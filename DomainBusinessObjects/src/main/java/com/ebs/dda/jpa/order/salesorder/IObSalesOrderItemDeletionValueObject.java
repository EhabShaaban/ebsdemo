package com.ebs.dda.jpa.order.salesorder;

public class IObSalesOrderItemDeletionValueObject {

  private String salesOrderCode;
  private Long salesOrderItemId;

  public String getSalesOrderCode() {
    return salesOrderCode;
  }

  public void setSalesOrderCode(String salesOrderCode) {
    this.salesOrderCode = salesOrderCode;
  }

  public Long getSalesOrderItemId() {
    return salesOrderItemId;
  }

  public void setSalesOrderItemId(Long salesOrderItemId) {
    this.salesOrderItemId = salesOrderItemId;
  }
}
