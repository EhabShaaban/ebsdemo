package com.ebs.dda.jpa.masterdata.item.enums;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 3:18:17 PM
 */
@Embeddable
public class ProcurementRestrictionEnum extends BusinessEnum {

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private ProcurementRestriction value;

  public ProcurementRestrictionEnum() {}

  public ProcurementRestrictionEnum(ProcurementRestriction value) {
    this.value = value;
  }

  @Override
  public ProcurementRestriction getId() {
    return value;
  }

  public enum ProcurementRestriction {
    ALLOWED,
    ALLOWED_WITH_WARRING,
    NOT_ALLOWED
  }
}
