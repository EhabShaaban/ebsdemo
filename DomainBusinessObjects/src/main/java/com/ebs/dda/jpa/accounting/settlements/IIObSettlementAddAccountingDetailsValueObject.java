package com.ebs.dda.jpa.accounting.settlements;

public interface IIObSettlementAddAccountingDetailsValueObject {

  String SETTLEMENT_CODE = "settlementCode";
  String ACCOUNT_TYPE = "accountType";
  String GL_ACCOUNT = "glAccountCode";
  String GL_SUB_ACCOUNT = "glSubAccountCode";
  String AMOUNT = "amount";
}
