package com.ebs.dda.jpa.masterdata.ivr;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IItemVendorRecord.class)
@Table(name = "ItemVendorRecordGeneralModel")
public class ItemVendorRecordGeneralModel extends BusinessObject {

	private static final long serialVersionUID = 1L;

	@Column(name = "usercode")
	private String userCode;

	@Convert(converter = LocalizedStringConverter.class)
	@Column(name = "vendorname")
	private LocalizedString vendorName;

	@Column(name = "vendorcode")
	private String vendorCode;

	@Convert(converter = LocalizedStringConverter.class)
	@Column(name = "itemname")
	private LocalizedString itemName;

	@Convert(converter = LocalizedStringConverter.class)
	@Column(name = "uomsymbol")
	private LocalizedString uomSymbol;

	@Column(name = "uomcode")
	private String uomCode;

	@Column(name = "itemcode")
	private String itemCode;

	@Convert(converter = LocalizedStringConverter.class)
	@Column(name = "purchaseunitname")
	private LocalizedString purchaseUnitName;

	@Column(name = "purchaseUnitNameEn")
	private String purchaseUnitNameEn;

	@Column(name = "purchaseunitcode")
	private String purchasingUnitCode;

	@Column(name = "itemvendorcode")
	private String itemVendorCode;

	@Column(name = "olditemnumber")
	private String oldItemNumber;

	@Convert(converter = LocalizedStringConverter.class)
	@Column(name = "currencyname")
	private LocalizedString currencyName;

	@Column(name = "currencycode")
	private String currencyCode;

	@Column(name = "currencysymbol")
	private String currencySymbol;

	@Column(name = "price")
	private Double price;

	public ItemVendorRecordGeneralModel() {
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public LocalizedString getVendorName() {
		return vendorName;
	}

	public void setVendorName(LocalizedString vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public LocalizedString getItemName() {
		return itemName;
	}

	public void setItemName(LocalizedString itemName) {
		this.itemName = itemName;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getPurchaseUnitNameEn() {
		return purchaseUnitNameEn;
	}

	public LocalizedString getPurchaseUnitName() {
		return purchaseUnitName;
	}

	public void setPurchaseUnitName(LocalizedString purchasingUnitName) {
		this.purchaseUnitName = purchasingUnitName;
	}

	public String getPurchasingUnitCode() {
		return purchasingUnitCode;
	}

	public void setPurchasingUnitCode(String purchasingUnitCode) {
		this.purchasingUnitCode = purchasingUnitCode;
	}

	public String getItemVendorCode() {
		return itemVendorCode;
	}

	public void setItemVendorCode(String itemVendorCode) {
		this.itemVendorCode = itemVendorCode;
	}

	public String getOldItemNumber() {
		return oldItemNumber;
	}

	public void setOldItemNumber(String oldItemNumber) {
		this.oldItemNumber = oldItemNumber;
	}

	public LocalizedString getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(LocalizedString currencyName) {
		this.currencyName = currencyName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCurrencySymbol() {
		return currencySymbol;
	}

	public void setCurrencySymbol(String currencySymbol) {
		this.currencySymbol = currencySymbol;
	}

	public LocalizedString getUomSymbol() {
		return uomSymbol;
	}

	public void setUomSymbol(LocalizedString uomSymbol) {
		this.uomSymbol = uomSymbol;
	}

	public String getUomCode() {
		return uomCode;
	}

	public void setUomCode(String uomCode) {
		this.uomCode = uomCode;
	}

	@Override
	public String getSysName() {
		return "ItemVendorRecord";
	}
}
