package com.ebs.dda.jpa.masterdata.enterprise;

import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.*;

@Entity
@Table(name = "IObEnterpriseBasicData")
public class IObEnterpriseBasicData extends InformationObject {

  private static final long serialVersionUID = 1L;

  private Long currencyId;

  @Override
  public String getSysName() {
    return IIObEnterpriseBasicData.SYS_NAME;
  }

  public Long getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }
}
