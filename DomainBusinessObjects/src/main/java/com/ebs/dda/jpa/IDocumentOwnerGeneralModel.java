package com.ebs.dda.jpa;

public interface IDocumentOwnerGeneralModel {

  String SYS_NAME = "DocumentOwner";
  String USER_ID = "userId";
  String USER_NAME = "userName";
}
