package com.ebs.dda.jpa.accounting.landedcost;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IIObLandedCostFactorItems {

    static final String LANDED_COST_COST_FACTOR_ITEMS_DETAILS_ATTR_NAME = "landedCostCostFactorItemsDetails";
    CompositeAttribute<IObLandedCostFactorItemsDetails> costFactorItemsDetails =
            new CompositeAttribute<IObLandedCostFactorItemsDetails>(LANDED_COST_COST_FACTOR_ITEMS_DETAILS_ATTR_NAME,
                    IObLandedCostFactorItemsDetails.class, true);
}
