package com.ebs.dda.jpa.masterdata.item;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 4:42:37 PM
 */
@Entity
@Table(name = "IObMaterialAllowedView")
@EntityInterface(IIObMaterialAllowedView.class)
public class IObMaterialAllowedView extends InformationObject implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "materialView")
  private Long materialView;

  @Override
  public String getSysName() {
    return IIObMaterialAllowedView.SYSTEM_NAME;
  }
}
