package com.ebs.dda.jpa.accounting.paymentrequest;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.accounting.DObAccountingDocument;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "dobpaymentrequest")
@EntityInterface(IDObPaymentRequest.class)
@DiscriminatorValue(value = "PaymentRequest")
public class DObPaymentRequest extends DObAccountingDocument implements Serializable {
  private static final long serialVersionUID = 1L;

  private String paymentType;
  private BigDecimal remaining;

  public String getPaymentType() {
    return paymentType;
  }

  public void setPaymentType(String paymentType) {
    this.paymentType = paymentType;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }

  public void setRemaining(BigDecimal remaining) {
    this.remaining = remaining;
  }

  @Override
  public String getSysName() {
    return IDObPaymentRequest.SYS_NAME;
  }
}
