package com.ebs.dda.jpa.accounting;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObAccountingDocumentActivationDetailsGeneralModel")
@EntityInterface(IIObAccountingDocumentActivationDetailsGeneralModel.class)
public class IObAccountingDocumentActivationDetailsGeneralModel extends InformationObject {

  private String code;
  private String accountant;
  private String documentType;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime activationDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private String exchangeRateCode;
  private Long exchangeRateId;
  private String journalEntryCode;
  private BigDecimal currencyPrice;
  private BigDecimal firstValue;
  private BigDecimal secondValue;
  private String firstCurrencyIso;
  private String secondCurrencyIso;
  private String objectTypeCode;

  private Long fiscalPeriodId;
  private String fiscalPeriod;

  public BigDecimal getFirstValue() {
    return firstValue;
  }

  public BigDecimal getSecondValue() {
    return secondValue;
  }

  public String getFirstCurrencyIso() {
    return firstCurrencyIso;
  }

  public String getSecondCurrencyIso() {
    return secondCurrencyIso;
  }

  public String getAccountant() {
    return accountant;
  }

  public DateTime getActivationDate() {
    return activationDate;
  }

  public DateTime getDueDate() {
    return dueDate;
  }

  public String getExchangeRateCode() {
    return exchangeRateCode;
  }

  public BigDecimal getCurrencyPrice() {
    return currencyPrice;
  }

  public String getCode() {
    return code;
  }

  public String getDocumentType() {
    return documentType;
  }

  public Long getExchangeRateId() {
    return exchangeRateId;
  }

  public String getJournalEntryCode() {
    return journalEntryCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public Long getFiscalPeriodId() {
    return fiscalPeriodId;
  }

  public void setFiscalPeriodId(Long fiscalPeriodId) {
    this.fiscalPeriodId = fiscalPeriodId;
  }

  @Override
  public String getSysName() {
    return IIObAccountingDocumentActivationDetailsGeneralModel.SYS_NAME;
  }
}
