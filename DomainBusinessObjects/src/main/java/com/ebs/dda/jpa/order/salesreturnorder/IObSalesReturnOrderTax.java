package com.ebs.dda.jpa.order.salesreturnorder;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObSalesReturnOrderTax")
public class IObSalesReturnOrderTax extends DocumentLine {

  private String code;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private BigDecimal percentage;

  public void setCode(String code) {
    this.code = code;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public void setPercentage(BigDecimal percentage) {
    this.percentage = percentage;
  }

  @Override
  public String getSysName() {
    return IDObSalesReturnOrder.SYS_NAME;
  }
}
