package com.ebs.dda.jpa.masterdata.assetmasterdata;

import com.ebs.dac.dbo.processing.CompositeAttribute;


public interface IMObAsset {
  String SYS_NAME = "AssetMasterData";
  String OLD_NUMBER = "oldNumber";
  String ASSET_TYPE_ID = "assetTypeId";


  String BUSINESS_UNIT_ATTR_NAME = "businessunit";
  CompositeAttribute<IObAssetBusinessUnit> businessUnitAttr =
      new CompositeAttribute<>( BUSINESS_UNIT_ATTR_NAME, IObAssetBusinessUnit.class, true);
}
