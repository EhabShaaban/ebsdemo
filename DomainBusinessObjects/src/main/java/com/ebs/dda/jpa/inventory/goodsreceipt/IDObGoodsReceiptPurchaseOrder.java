package com.ebs.dda.jpa.inventory.goodsreceipt;

import com.ebs.dac.dbo.api.processing.ICompositeAttribute;
import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObGoodsReceiptPurchaseOrder {

    String SYS_NAME = "GoodsReceiptPurchaseOrder";

    CompositeAttribute<IObGoodsReceiptPurchaseOrderReceivedItemsData>
            iObGoodsReceiptPurchaseOrderItems =
            new CompositeAttribute(
                    "iObGoodsReceiptPurchaseOrderItems",
                    IObGoodsReceiptPurchaseOrderReceivedItemsData.class,
                    true);

    ICompositeAttribute<IObGoodsReceiptPurchaseOrderData> IOB_PURCHASING_DATA =
            new CompositeAttribute("iobPurchasingData", IObGoodsReceiptPurchaseOrderData.class, false);
}
