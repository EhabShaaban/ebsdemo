package com.ebs.dda.jpa.masterdata.item;

public interface IMObItemAccountingInfoValueObject {
    String ITEM_CODE = "itemCode";
    String ACCOUNT_CODE = "accountCode";
    String COST_FACTOR = "costFactor";
}
