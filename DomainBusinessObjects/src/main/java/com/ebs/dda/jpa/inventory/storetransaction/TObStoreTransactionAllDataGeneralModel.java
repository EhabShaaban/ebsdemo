package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.TransactionObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import org.joda.time.DateTime;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "TObStoreTransactionAllDataGeneralModel")
public class TObStoreTransactionAllDataGeneralModel extends TransactionObject<DefaultUserCode> {

  private BigDecimal quantity;
    private BigDecimal estimateCost;
    private String stockType;
    private BigDecimal actualCost;
    private String batchNo;
  private BigDecimal remainingQuantity;
  private String transactionOperation;
  private String companyCode;

  private String companyName;

  private String storehouseCode;

  private String storehouseName;

  private String plantCode;

  private String plantName;

  private String purchaseUnitCode;

  private String purchaseUnitName;

  private String unitofmeasurename;

  private String unitOfMeasureSymbol;

  private String unitOfMeasureCode;

  private String refTransactionCode;

  private String itemCode;

  private String itemName;

    private String documentCode;

    @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
    @Column(updatable = false)
    private DateTime originalAddingDate;

    public BigDecimal getQuantity() {
        return quantity;
    }

    public BigDecimal getEstimateCost() {
        return estimateCost;
    }

    public String getStockType() {
        return stockType;
    }

    public BigDecimal getActualCost() {
        return actualCost;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public BigDecimal getRemainingQuantity() {
        return remainingQuantity;
    }

  public String getTransactionOperation() {
    return transactionOperation;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getCompanyName() {
    return companyName;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public String getStorehouseName() {
    return storehouseName;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public String getPlantName() {
    return plantName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getUnitofmeasurename() {
    return unitofmeasurename;
  }

  public String getUnitOfMeasureSymbol() {
    return unitOfMeasureSymbol;
  }

  public String getUnitOfMeasureCode() {
    return unitOfMeasureCode;
  }

  public String getRefTransactionCode() {
    return refTransactionCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public String getItemName() {
    return itemName;
  }

  public String getDocumentCode() {
    return documentCode;
  }

  public DateTime getOriginalAddingDate() {
    return originalAddingDate;
  }

  @Override
  public String getSysName() {
    return ITObStoreTransaction.SYS_NAME;
  }
}
