package com.ebs.dda.jpa.order.salesorder;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.SalesTypeEnum;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "DObSalesOrder")
@DiscriminatorValue(SalesTypeEnum.Values.SALES_ORDER)
@EntityInterface(IDObSalesOrder.class)
public class DObSalesOrder extends DObOrderDocument implements Serializable {

    private Long salesOrderTypeId;
    private Long salesResponsibleId;
    private Long businessUnitId;
    private BigDecimal remaining;
    private BigDecimal totalAmount;

    public Long getSalesOrderTypeId() {
        return salesOrderTypeId;
    }

    public void setSalesOrderTypeId(Long salesOrderTypeId) {
        this.salesOrderTypeId = salesOrderTypeId;
    }

    public Long getSalesResponsibleId() {
        return salesResponsibleId;
    }

    public void setSalesResponsibleId(Long salesResponsibleId) {
        this.salesResponsibleId = salesResponsibleId;
    }

    public Long getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(Long businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public BigDecimal getRemaining() {
        return remaining;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public void setRemaining(BigDecimal remaining) {
        this.remaining = remaining;
    }

    @Override
    public String getSysName() {
        return IDObSalesOrder.SYS_NAME;
    }
}
