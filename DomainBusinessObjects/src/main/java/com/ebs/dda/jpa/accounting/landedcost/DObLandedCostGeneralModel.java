package com.ebs.dda.jpa.accounting.landedcost;

import com.ebs.dac.dbo.api.annotations.Exclude;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObLandedCostGeneralModel")
@EntityInterface(IDObLandedCostGeneralModel.class)
public class DObLandedCostGeneralModel extends DocumentObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  private String typeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString typeName;

  private String purchaseOrderCode;
  private String documentOwner;

  @Exclude
  private Long purchaseOrderId;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public LocalizedString getTypeName() {
    return typeName;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getDocumentOwner() {
    return documentOwner;
  }

  public Long getPurchaseOrderId() {
    return purchaseOrderId;
  }

  @Override
  public String getSysName() {
    return IDObLandedCost.SYS_NAME;
  }
}
