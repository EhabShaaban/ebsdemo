package com.ebs.dda.jpa.accounting.monetarynotes;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "IObMonetaryNotesDetails")
public class IObMonetaryNotesDetails extends DocumentHeader {

  private BigDecimal amount;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime dueDate;

  private String noteNumber;
  private String noteBank;
  private Long depotId;
  private Long businessPartnerId;
  private Long currencyId;
  private BigDecimal remaining;
  private String noteForm;

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public void setDueDate(DateTime dueDate) {
    this.dueDate = dueDate;
  }

  public void setNoteNumber(String noteNumber) {
    this.noteNumber = noteNumber;
  }

  public void setNoteBank(String noteBank) {
    this.noteBank = noteBank;
  }

  public void setDepotId(Long depotId) {
    this.depotId = depotId;
  }

  public void setBusinessPartnerId(Long businessPartnerId) {
    this.businessPartnerId = businessPartnerId;
  }

  public void setCurrencyId(Long currencyId) {
    this.currencyId = currencyId;
  }

  public void setRemaining(BigDecimal remaining) {
    this.remaining = remaining;
  }

  public void setNoteForm(String noteForm) {
    this.noteForm = noteForm;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }
}
