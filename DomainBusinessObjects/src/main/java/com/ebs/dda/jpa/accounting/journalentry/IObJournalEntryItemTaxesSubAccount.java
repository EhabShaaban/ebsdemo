package com.ebs.dda.jpa.accounting.journalentry;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "iobjournalentryitemtaxsubaccount")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorValue(value = "Taxes")
public class IObJournalEntryItemTaxesSubAccount extends IObJournalEntryItem {

  private Long subAccountId;

  public void setSubAccountId(Long subAccountId) {
    this.subAccountId = subAccountId;
  }
}
