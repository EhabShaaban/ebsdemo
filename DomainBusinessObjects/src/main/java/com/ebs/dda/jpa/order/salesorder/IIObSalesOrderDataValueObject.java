package com.ebs.dda.jpa.order.salesorder;

public interface IIObSalesOrderDataValueObject {

  String SALES_ORDER_CODE = "salesOrderCode";
  String PAYMENT_TERM_CODE = "paymentTermCode";
  String CUSTOMER_ADDRESS_ID = "customerAddressId";
  String CURRENCY_CODE = "currencyCode";
  String CONTACT_PERSON_ID = "contactPersonId";
  String EXPECTED_DELIVERY_DATE = "expectedDeliveryDate";
  String NOTES = "notes";
}
