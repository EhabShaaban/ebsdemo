package com.ebs.dda.jpa.masterdata.enterprise;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class AddressDetails extends InformationObject {

  private static final long serialVersionUID = 1L;

  private Long countryId;

  private Long cityId;

  private String postalCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString addressLine;

  public Long getCountryId() {
    return countryId;
  }

  public void setCountryId(Long countryId) {
    this.countryId = countryId;
  }

  public Long getCityId() {
    return cityId;
  }

  public void setCityId(Long cityId) {
    this.cityId = cityId;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public LocalizedString getAddressLine() {
    return addressLine;
  }

  public void setAddressLine(LocalizedString addressLine) {
    this.addressLine = addressLine;
  }

  @Override
  public String getSysName() {
    return IIObEnterpriseAddressDetails.SYS_NAME;
  }
}
