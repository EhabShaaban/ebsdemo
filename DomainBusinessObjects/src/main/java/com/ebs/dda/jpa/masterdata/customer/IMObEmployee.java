package com.ebs.dda.jpa.masterdata.customer;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 5, 2021
 */
public interface IMObEmployee {
  String SYS_NAME = "Employee";

  String CODE = "userCode";
  String NAME = "name";
}
