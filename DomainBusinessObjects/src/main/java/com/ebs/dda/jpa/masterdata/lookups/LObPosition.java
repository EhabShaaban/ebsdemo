package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dda.jpa.masterdata.item.LObMaterial;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/** @author xerix on Tue Oct 2017 at 15 : 45 */
@Entity
@DiscriminatorValue("4")
public class LObPosition extends LObMaterial implements Serializable {
  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IMasterDataLookupsSysNames.LOB_POSITION_SYS_NAME;
  }
}
