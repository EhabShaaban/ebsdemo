package com.ebs.dda.jpa.accounting.paymentrequest;

public interface IDObPaymentRequestCreateValueObject {

  String PAYMENT_TYPE = "paymentType";
  String PAYMENT_FORM = "paymentForm";
  String BUSINESSUNIT_CODE = "businessUnitCode";
  String BUSINESSPARTNER_CODE = "businessPartnerCode";
  String DUE_DOCUMENT_TYPE = "dueDocumentType";
  String DUE_DOCUMENT_CODE = "dueDocumentCode";
  String COMPANY_CODE = "companyCode";
  String DOCUMENT_OWNER = "documentOwnerId";
}
