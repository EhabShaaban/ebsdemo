package com.ebs.dda.jpa.inventory.goodsreceipt;

import java.util.List;

public class IObGoodsReceiptReceivedItemsDataValueObject {

  private String goodsReceiptCode;
  private String itemCode;
  private Integer qtyInDn;

  private List<IObGoodsReceiptItemQuantityValueObject> quantities;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public List<IObGoodsReceiptItemQuantityValueObject> getQuantities() {
    return quantities;
  }
}
