package com.ebs.dda.jpa.accounting.settlements;

import com.ebs.dda.jpa.IValueObject;

public class DObSettlementActivateValueObject implements IValueObject {

  private String settlementCode;
  private String journalEntryDate;

  public String getSettlementCode() {
    return settlementCode;
  }

  public void setSettlementCode(String settlementCode) {
    this.settlementCode = settlementCode;
  }

  public String getJournalEntryDate() {
    return journalEntryDate;
  }

  public void setJournalEntryDate(String journalEntryDate) {
    this.journalEntryDate = journalEntryDate;
  }

  @Override
  public void userCode(String userCode) {
    this.settlementCode = userCode;
  }
}
