package com.ebs.dda.jpa.accounting.accountingnotes;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(DebitNoteTypeEnum.Values.DEBIT_NOTE_BASED_ON_COMMISSION)
@EntityInterface(IDObAccountingNote.class)
public class DObDebitNoteCommission extends DObAccountingNote {

  public DObDebitNoteCommission() {}

  @Override
  public String getSysName() {
    return IDObAccountingNote.SYS_NAME;
  }
}
