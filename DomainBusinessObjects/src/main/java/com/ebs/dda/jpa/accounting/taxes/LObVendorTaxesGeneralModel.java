package com.ebs.dda.jpa.accounting.taxes;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

@Entity
@Table(name = "LObVendorTaxesGeneralModel")
public class LObVendorTaxesGeneralModel extends LObTaxInfoGeneralModel {
	String vendorCode;
	@Convert(converter = LocalizedStringConverter.class)
	LocalizedString vendorName;

	public String getVendorCode() {
		return vendorCode;
	}

	public LocalizedString getVendorName() {
		return vendorName;
	}
}
