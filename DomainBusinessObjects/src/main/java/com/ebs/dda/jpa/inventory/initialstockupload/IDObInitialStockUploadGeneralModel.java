package com.ebs.dda.jpa.inventory.initialstockupload;

import com.ebs.dac.dbo.processing.BasicAttribute;

public interface IDObInitialStockUploadGeneralModel {
    String PURCHASING_UNIT_NAME = "purchaseUnitName";
    String CREATION_INFO = "creationInfo";
    String USER_CODE = "userCode";
    String PURCHASE_UNIT_CODE = "purchaseUnitCode";
    String CURRENT_STATES = "currentStates";
    String CREATION_DATE = "creationDate";
    String STORE_HOUSE_NAME = "storehouseName";
    String PURCHASING_UNIT_NAME_EN = "purchaseUnitNameEn";

    BasicAttribute purchaseUnitNameAttr = new BasicAttribute(PURCHASING_UNIT_NAME_EN);
}
