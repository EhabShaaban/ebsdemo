package com.ebs.dda.jpa.accounting.initialbalanceupload;

public interface IDObInitialBalanceUploadCreateValueObject {

    String PURCHASE_UNIT = "purchaseUnitCode";
    String COMPANY = "companyCode";
    String DOCUMENT_OWNER_ID = "documentOwnerId";
    String FISCALPERIODCODE = "fiscalPeriodCode";
}
