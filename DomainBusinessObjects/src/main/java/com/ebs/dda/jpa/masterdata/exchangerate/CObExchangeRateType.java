package com.ebs.dda.jpa.masterdata.exchangerate;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

/**
 * @author Ahmed Ali Elfeky
 * @since Dec 27, 2020
 */
@Entity
@Table(name = "CObExchangeRateType")
@EntityInterface(ICObExchangeRateType.class)
public class CObExchangeRateType extends ConfigurationObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  private Boolean isDefault;

  @Override
  public String getSysName() {
    return ICObExchangeRateType.SYS_NAME;
  }

  public Boolean getIsDefault() {
    return isDefault;
  }

  public void setIsDefault(Boolean isDefault) {
    this.isDefault = isDefault;
  }

}
