package com.ebs.dda.jpa.masterdata.vendor;

public class MObVendorAccountingInfoValueObject {

  private String accountCode;
  private String vendorCode;


  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public String getAccountCode() {
    return accountCode;
  }

  public void setAccountCode(String accountCode) {
    this.accountCode = accountCode;
  }

}
