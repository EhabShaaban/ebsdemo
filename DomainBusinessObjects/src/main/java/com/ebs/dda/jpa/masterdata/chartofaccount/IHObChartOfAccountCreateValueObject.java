package com.ebs.dda.jpa.masterdata.chartofaccount;

public interface IHObChartOfAccountCreateValueObject {

  String ACCOUNT_NAME = "accountName";
  String LEVEL = "level";
  String OLD_ACCOUNT_CODE = "oldAccountCode";
  String PARENT = "parentCode";
  String ACCOUNT_TYPE = "accountType";
  String ACCOUNTING_ENTRY = "accountingEntry";
  String MAPPED_ACCOUNT = "mappedAccount";
}
