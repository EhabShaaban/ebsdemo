package com.ebs.dda.jpa.masterdata.exchangerate;

import java.math.BigDecimal;

public class CObExchangeRateValueObject {


    private BigDecimal secondValue;
    private String firstCurrencyCode;


    public BigDecimal getSecondValue() {
        return secondValue;
    }

    public void setSecondValue(BigDecimal secondValue) {
        this.secondValue = secondValue;
    }

    public String getFirstCurrencyCode() {
        return firstCurrencyCode;
    }

    public void setFirstCurrencyCode(String firstCurrencyCode) {
        this.firstCurrencyCode = firstCurrencyCode;
    }
}
