
package com.ebs.dda.jpa.masterdata.lookups;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "lobcontactperson")
public class LObContactPerson extends LocalizedLookupObject<DefaultUserCode> implements Serializable {

    private static final long serialVersionUID = 1L;

    private String title;
    private String email;
    private String department;
    private String telephone;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }


    @Override
    public String getSysName() {
        return null;
    }
}
