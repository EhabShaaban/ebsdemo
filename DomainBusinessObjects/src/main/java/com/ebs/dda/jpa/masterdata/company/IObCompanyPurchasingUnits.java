package com.ebs.dda.jpa.masterdata.company;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObCompanyPurchasingUnits")
@EntityInterface(IIObCompanyPurchasingUnits.class)
public class IObCompanyPurchasingUnits extends InformationObject {

  private static final long serialVersionUID = 1L;

  @Column(name = "purchasingUnitId")
  private Long purchasingUnit;


  @Override
  public String getSysName() {
    return "IObCompanyPurchasingUnits";
  }
}
