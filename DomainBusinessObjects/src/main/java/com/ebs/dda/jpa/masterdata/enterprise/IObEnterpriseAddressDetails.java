package com.ebs.dda.jpa.masterdata.enterprise;

import com.ebs.dac.dbo.api.processing.EntityInterface;

import javax.persistence.*;

@Entity
@Table(name = "IObEnterpriseAddress")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "objectTypeCode")
@EntityInterface(IIObEnterpriseAddressDetails.class)
public class IObEnterpriseAddressDetails extends AddressDetails {

  private static final long serialVersionUID = 1L;
}
