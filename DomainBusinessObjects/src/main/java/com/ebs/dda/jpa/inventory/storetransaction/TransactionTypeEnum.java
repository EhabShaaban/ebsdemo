package com.ebs.dda.jpa.inventory.storetransaction;

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class TransactionTypeEnum extends BusinessEnum {

  public static final String SYS_NAME = "StoreTransactions";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private TransactionType value;

  public TransactionTypeEnum() {
  }

  @Override
  public TransactionType getId() {
    return this.value;
  }

  public void setId(TransactionType id) {
    this.value = id;
  }

  public enum TransactionType {
    ADD, TAKE, TRANSFER_TO_STORE, CHANGE_STOCK_TYPE, BEGINNING_INVENTORY
  }
}
