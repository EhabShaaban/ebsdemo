package com.ebs.dda.jpa.accounting.vendorinvoice;

import com.ebs.dda.jpa.accounting.IObInvoiceItem;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObVendorInvoiceItem")
public class IObVendorInvoiceItem extends IObInvoiceItem {

  @Override
  public String getSysName() {
    return IIObVendorInvoiceItems.SYS_NAME;
  }
}
