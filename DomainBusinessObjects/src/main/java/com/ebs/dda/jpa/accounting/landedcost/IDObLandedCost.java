package com.ebs.dda.jpa.accounting.landedcost;

import com.ebs.dac.dbo.processing.CompositeAttribute;

public interface IDObLandedCost {

  String SYS_NAME = "LandedCost";

  static final String LANDED_COST_DETAILS_ATTR_NAME = "landedCostDetails";
  CompositeAttribute<IObLandedCostDetails> landedCostDetails =
      new CompositeAttribute<IObLandedCostDetails>(LANDED_COST_DETAILS_ATTR_NAME,
          IObLandedCostDetails.class, false);


  static final String LANDED_COST_COMPANY_DATA_ATTR_NAME = "landedCostCompanyData";
  CompositeAttribute<IObLandedCostCompanyData> companyData =
      new CompositeAttribute<IObLandedCostCompanyData>(LANDED_COST_COMPANY_DATA_ATTR_NAME,
          IObLandedCostCompanyData.class, false);

  static final String LANDED_COST_COST_FACTOR_ITEMS_ATTR_NAME = "landedCostCostFactorItems";
  CompositeAttribute<IObLandedCostFactorItems> costFactorItems =
      new CompositeAttribute<IObLandedCostFactorItems>(LANDED_COST_COST_FACTOR_ITEMS_ATTR_NAME,
          IObLandedCostFactorItems.class, true);
}

