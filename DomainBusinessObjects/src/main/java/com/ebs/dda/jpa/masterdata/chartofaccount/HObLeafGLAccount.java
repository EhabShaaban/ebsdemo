package com.ebs.dda.jpa.masterdata.chartofaccount;

import javax.persistence.*;

@Entity
@Table(name = "leafglaccount")
@DiscriminatorValue("3")
@Inheritance(strategy = InheritanceType.JOINED)
public class HObLeafGLAccount extends HObGLAccount {

    @Column(name = "type")
    private String accountType;

    private String creditDebit;

    private String leadger;

    private String mappedAccount;

    public String getAccountType() {
        return accountType;
    }

    public String getCreditDebit() {
        return creditDebit;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setCreditDebit(String creditDebit) {
        this.creditDebit = creditDebit;
    }

    public String getLeadger() {
        return leadger;
    }

    public void setLeadger(String leadger) {
        this.leadger = leadger;
    }

    public void setMappedAccount(String mappedAccount) {
        this.mappedAccount = mappedAccount;
    }
}
