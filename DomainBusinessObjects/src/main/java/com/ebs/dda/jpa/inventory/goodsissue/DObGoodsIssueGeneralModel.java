package com.ebs.dda.jpa.inventory.goodsissue;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObGoodsIssueGeneralModel")
@EntityInterface(IDObGoodsIssueGeneralModel.class)
public class DObGoodsIssueGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;
  private String inventoryDocumentType;
  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString typeName;

  private String typeNameEn;

  private String typeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storehouseName;

  private String storehouseCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString customerName;

  private String customerCode;

  private String companyCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String plantCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString plantName;

  private String storekeeperCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString storekeeperName;

  public String getTypeNameEn() {
    return typeNameEn;
  }

  public String getInventoryDocumentType() {
    return inventoryDocumentType;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public LocalizedString getStorehouseName() {
    return storehouseName;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public LocalizedString getPlantName() {
    return plantName;
  }

  public String getStorekeeperCode() {
    return storekeeperCode;
  }

  public LocalizedString getStorekeeperName() {
    return storekeeperName;
  }

  public LocalizedString getCustomerName() {
    return customerName;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public boolean isGoodsIssueSalesInvoice() {
    return inventoryDocumentType.equals("GI_SI");
  }

  @Override
  public String getSysName() {
    return IDObGoodsIssue.SYS_NAME;
  }
}
