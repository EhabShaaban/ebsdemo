package com.ebs.dda.dbo.jpa.entities.lookups.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.LookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LObGlobalGLAccountConfigGeneralModel")
@EntityInterface(ILObGlobalGLAccountConfig.class)
public class LObGlobalGLAccountConfigGeneralModel extends LookupObject<DefaultUserCode> {

  private Long glAccountId;

  private String accountCode;

  public Long getGlAccountId() {
    return glAccountId;
  }

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString accountName;

  public String getAccountCode() {
    return accountCode;
  }

  public LocalizedString getAccountName() {
    return accountName;
  }

  @Override
  public String getSysName() {
    return ILObGlobalGLAccountConfig.SYS_NAME;
  }

}
