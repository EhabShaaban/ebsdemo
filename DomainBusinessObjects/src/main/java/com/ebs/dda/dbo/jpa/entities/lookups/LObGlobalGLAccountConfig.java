package com.ebs.dda.dbo.jpa.entities.lookups;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.LookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.dbo.jpa.entities.lookups.apis.ILObGlobalGLAccountConfig;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LObGlobalGLAccountConfig")
@EntityInterface(ILObGlobalGLAccountConfig.class)
public class LObGlobalGLAccountConfig extends LookupObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long accountId;

  private Boolean sysFlag;

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }

  public void setSysFlag(Boolean sysFlag) {
    this.sysFlag = sysFlag;
  }

  @Override
  public String getSysName() {
    return ILObGlobalGLAccountConfig.SYS_NAME;
  }

}
