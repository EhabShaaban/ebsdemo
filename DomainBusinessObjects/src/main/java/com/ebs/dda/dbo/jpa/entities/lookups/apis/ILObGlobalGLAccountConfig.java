package com.ebs.dda.dbo.jpa.entities.lookups.apis;

public interface ILObGlobalGLAccountConfig {

  String SYS_NAME = "LObGlobalGLAccountConfig";

  static String CASH_GL_ACCOUNT = "CashGLAccount";
  static String REALIZED_GLA_CCOUNT = "RealizedGLAccount";
  static String BANK_GL_ACCOUNT = "BankGLAccount";
  static String CUSTOMERS_GL_ACCOUNT = "CustomersGLAccount";
  static String TAXES_GL_ACCOUNT = "TaxesGLAccount";
  static String SALES_GL_ACCOUNT = "SalesGLAccount";
  static String IMPORT_PURCHASING_GL_ACCOUNT = "ImportPurchasingGLAccount";
  static String LOCAL_PURCHASING_GL_ACCOUNT = "LocalPurchasingGLAccount";
  static String PURCHASING_GL_ACCOUNT = "PurchasingGLAccount";
  static String PURCHASE_ORDER_GL_ACCOUNT = "PurchaseOrderGLAccount";
  static String NOTES_RECEIVABLES_GL_ACCOUNT = "NotesReceivablesGLAccount";
  static String TREASURY_GL_ACCOUNT = "TreasuryGLAccount";
  static String UNREALIZED_ACCOUNT = "UnrealizedCurrencyGainLossGLAccount";
}
