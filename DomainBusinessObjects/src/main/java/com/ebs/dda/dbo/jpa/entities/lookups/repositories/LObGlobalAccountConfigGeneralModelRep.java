package com.ebs.dda.dbo.jpa.entities.lookups.repositories;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LookupObjectRep;
import com.ebs.dda.dbo.jpa.entities.lookups.generalmodels.LObGlobalGLAccountConfigGeneralModel;

public interface LObGlobalAccountConfigGeneralModelRep extends
    LookupObjectRep<LObGlobalGLAccountConfigGeneralModel, DefaultUserCode> {
    LObGlobalGLAccountConfigGeneralModel findOneByAccountCode(String accountCode);

}
