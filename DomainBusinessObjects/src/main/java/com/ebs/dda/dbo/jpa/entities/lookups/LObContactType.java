package com.ebs.dda.dbo.jpa.entities.lookups;

import com.ebs.dac.dbo.jpa.entities.businessobjects.SharedLocalizedLookup;
import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class LObContactType extends SharedLocalizedLookup implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return "LObContactType";
  }
}
