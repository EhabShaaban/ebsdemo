package com.ebs.dda.sales.dbo.jpa.entities.GeneralModels;

public interface IItemsLastSalesPriceGeneralModel {

  String SALES_INVOICE_CODE = "salesInvoiceCode";
  String UOM_CODE_NAME = "uomCodeName";
  String ITEM_CODE_NAME = "itemCodeName";
  String CUSTOMER_CODE_NAME = "customerCodeName";
  String PURCHASE_UNIT_CODE_NAME = "purchaseUnitCodeName";
  String SALES_PRICE = "salesPrice";
  String SALES_INVOICE_POSTING_DATE = "salesInvoicePostingDate";

}
