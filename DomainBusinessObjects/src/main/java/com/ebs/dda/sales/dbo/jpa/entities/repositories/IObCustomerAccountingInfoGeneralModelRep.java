package com.ebs.dda.sales.dbo.jpa.entities.repositories;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.jpa.accounting.salesinvoice.IObCustomerAccountingInfoGeneralModel;

public interface IObCustomerAccountingInfoGeneralModelRep
    extends InformationObjectRep<IObCustomerAccountingInfoGeneralModel> {

  IObCustomerAccountingInfoGeneralModel findOneByCustomerCode(String customerCode);
}
