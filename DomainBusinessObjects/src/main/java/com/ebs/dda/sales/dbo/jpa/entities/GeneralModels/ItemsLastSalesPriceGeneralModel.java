package com.ebs.dda.sales.dbo.jpa.entities.GeneralModels;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.joda.time.DateTime;

@Entity
@Table(name = "ItemsLastSalesPriceGeneralModel")
public class ItemsLastSalesPriceGeneralModel implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String salesInvoiceCode;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(updatable = false)
  private DateTime salesInvoicePostingDate;

  private String uomCode;
  private String uomCodeName;
  private String itemCode;
  private String itemCodeName;
  private String customerCode;
  private String customerCodeName;
  private String purchaseUnitCode;
  private String purchaseUnitCodeName;
  private BigDecimal salesPrice;
  private BigDecimal orderUnitPrice;

  public String getSalesInvoiceCode() {
    return salesInvoiceCode;
  }

  public DateTime getSalesInvoicePostingDate() {
    return salesInvoicePostingDate;
  }

  public String getUomCode() {
    return uomCode;
  }

  public String getUomCodeName() {
    return uomCodeName;
  }

  public String getItemCode() {
    return itemCode;
  }

  public String getItemCodeName() {
    return itemCodeName;
  }

  public String getCustomerCode() {
    return customerCode;
  }

  public String getCustomerCodeName() {
    return customerCodeName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPurchaseUnitCodeName() {
    return purchaseUnitCodeName;
  }

  public BigDecimal getSalesPrice() {
    return salesPrice;
  }

  public BigDecimal getOrderUnitPrice() {
    return orderUnitPrice;
  }
}
