package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.generalmodels;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "LObAttachmentType")
public class LObAttachmentTypeGeneralModel {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String code;

  @Column(name = "name")
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;
}
