package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.informationobjects.apis;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 15, 2017 , 3:04:48 PM
 */
public interface HasValuationArea<LObValuationArea extends LocalizedLookupObject<DefaultUserCode>> {

}
