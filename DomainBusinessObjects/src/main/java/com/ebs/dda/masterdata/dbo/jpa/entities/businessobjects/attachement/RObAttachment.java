package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement;

import com.ebs.dac.dbo.jpa.entities.businessobjects.ResourceObject;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis.IRObAttachment;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Zyad Ghorab AND Islam Mostafa
 * @date Oct 13, 2018
 */
@Entity
@Table(name = "RObAttachment")
public class RObAttachment extends ResourceObject {
  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IRObAttachment.SYS_NAME;
  }
}
