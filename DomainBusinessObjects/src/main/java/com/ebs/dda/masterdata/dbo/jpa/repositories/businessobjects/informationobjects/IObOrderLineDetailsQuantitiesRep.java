package com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetailsQuantities;

public interface IObOrderLineDetailsQuantitiesRep
    extends InformationObjectRep<IObOrderLineDetailsQuantities> {
  IObOrderLineDetailsQuantities findOneById(Long id);

}
