package com.ebs.dda.masterdata.specifications;

import com.ebs.dda.jpa.inventory.goodsreceipt.GoodsReceiptItemDataGeneralModel;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class ItemVendorRecordSpecification {

  public static Specification<GoodsReceiptItemDataGeneralModel> withVendorCode(String vendorCode) {
    return new Specification<GoodsReceiptItemDataGeneralModel>() {

      @Override
      public Predicate toPredicate(
          Root<GoodsReceiptItemDataGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder builder) {
        if (vendorCode != null) {
          Path<String> expression = root.get("vendorCode");
          return builder.equal(expression, vendorCode);
        }

        return null;
      }
    };
  }
}
