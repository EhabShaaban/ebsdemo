package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.embededobjects;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.item.enums.PriceControlEnum;

import javax.persistence.*;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 3:33:05 PM
 */
@Embeddable
public class ValuationSetting {

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "valuationMethod"))
  private PriceControlEnum valuationMethod;

  @Transient private LocalizedString localizedValuationMethod;

  public PriceControlEnum getValuationMethod() {
    if (valuationMethod == null) {
      valuationMethod = new PriceControlEnum();
    }
    return valuationMethod;
  }

  public void setValuationMethod(PriceControlEnum valuationMethod) {
    this.valuationMethod = valuationMethod;
  }

  public LocalizedString getLocalizedValuationMethod() {
    if (localizedValuationMethod == null) {
      localizedValuationMethod = new LocalizedString();
    }
    return localizedValuationMethod;
  }

  public void setLocalizedValuationMethod(LocalizedString localizedValuationMethod) {
    this.localizedValuationMethod = localizedValuationMethod;
  }
}
