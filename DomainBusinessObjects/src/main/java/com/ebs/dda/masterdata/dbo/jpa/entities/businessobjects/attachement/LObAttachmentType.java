package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement;

import com.ebs.dac.dbo.jpa.entities.businessobjects.LocalizedLookupObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis.ILObAttachmentType;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Zyad Ghorab AND Islam Mostafa
 * @date Oct 13, 2018
 */
@Entity
@Table(name = "LObAttachmentType")
public class LObAttachmentType extends LocalizedLookupObject<DefaultUserCode>
    implements Serializable {
  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return ILObAttachmentType.SYS_NAME;
  }
}
