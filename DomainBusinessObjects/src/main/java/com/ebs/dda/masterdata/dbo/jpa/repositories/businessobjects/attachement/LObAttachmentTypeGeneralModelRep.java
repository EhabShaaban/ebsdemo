package com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement;

import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.generalmodels.LObAttachmentTypeGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LObAttachmentTypeGeneralModelRep
    extends JpaRepository<LObAttachmentTypeGeneralModel, Long> {}
