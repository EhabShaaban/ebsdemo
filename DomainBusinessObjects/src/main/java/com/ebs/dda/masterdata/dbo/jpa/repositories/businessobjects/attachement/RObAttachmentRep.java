package com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.ResourceObjectRep;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.RObAttachment;

/**
 * @author Zyad Ghorab AND Islam Mostafa
 * @date Oct 13, 2018
 */
public interface RObAttachmentRep extends ResourceObjectRep<RObAttachment> {}
