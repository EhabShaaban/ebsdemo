package com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.LObAttachmentType;

/**
 * @author Zyad Ghorab AND Islam Mostafa
 * @date Oct 13, 2018
 */
public interface LObAttachmentTypeRep
    extends LocalizedLookupRep<LObAttachmentType, DefaultUserCode> {
  @Override
  LObAttachmentType findOneByUserCode(String userCode);
}
