package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis;

/**
 * @author Zyad Ghorab AND Islam Mostafa
 * @date Oct 13, 2018
 */
public interface ILObAttachmentType {
  String SYS_NAME = "DocumentType";
}
