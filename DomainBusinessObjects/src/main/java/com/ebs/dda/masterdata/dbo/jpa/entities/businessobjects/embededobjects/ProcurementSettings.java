package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.embededobjects;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.item.enums.ProcurementRestrictionEnum;

import javax.persistence.*;

/**
 * @author Ahmed Ali Elfeky
 * @since Jan 8, 2017 , 3:15:37 PM
 */
@Embeddable
public class ProcurementSettings {

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "internalProcurement"))
  private ProcurementRestrictionEnum internalProcurement;

  @Transient private LocalizedString localizedInternalProcurement;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "externalProcurement"))
  private ProcurementRestrictionEnum externalProcurement;

  @Transient private LocalizedString localizedExternalProcurement;

  public ProcurementRestrictionEnum getInternalProcurement() {
    if (internalProcurement == null) {
      internalProcurement = new ProcurementRestrictionEnum();
    }
    return internalProcurement;
  }

  public void setInternalProcurement(ProcurementRestrictionEnum internalProcurement) {
    this.internalProcurement = internalProcurement;
  }

  public ProcurementRestrictionEnum getExternalProcurement() {
    if (externalProcurement == null) {
      externalProcurement = new ProcurementRestrictionEnum();
    }
    return externalProcurement;
  }

  public void setExternalProcurement(ProcurementRestrictionEnum externalProcurement) {
    this.externalProcurement = externalProcurement;
  }

  public LocalizedString getLocalizedInternalProcurement() {
    if (localizedInternalProcurement == null) {
      localizedInternalProcurement = new LocalizedString();
    }
    return localizedInternalProcurement;
  }

  public void setLocalizedInternalProcurement(LocalizedString localizedInternalProcurement) {
    this.localizedInternalProcurement = localizedInternalProcurement;
  }

  public LocalizedString getLocalizedExternalProcurement() {
    if (localizedExternalProcurement == null) {
      localizedExternalProcurement = new LocalizedString();
    }
    return localizedExternalProcurement;
  }

  public void setLocalizedExternalProcurement(LocalizedString localizedExternalProcurement) {
    this.localizedExternalProcurement = localizedExternalProcurement;
  }
}
