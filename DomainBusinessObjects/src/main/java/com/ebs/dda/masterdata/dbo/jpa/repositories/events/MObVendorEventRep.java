package com.ebs.dda.masterdata.dbo.jpa.repositories.events;

import com.ebs.dda.masterdata.dbo.jpa.entities.events.CreatedMObVendorEvent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MObVendorEventRep extends JpaRepository<CreatedMObVendorEvent, Long> {

}
