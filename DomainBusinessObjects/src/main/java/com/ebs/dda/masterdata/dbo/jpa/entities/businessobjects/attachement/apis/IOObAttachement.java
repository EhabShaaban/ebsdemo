package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IResourceObject;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 5, 2018 3:10:05 PM
 */
public interface IOObAttachement extends IResourceObject {

  String SYS_NAME = "Attachment";
}
