package com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IResourceObject;

/**
 * @author Zyad Ghorab AND Islam Mostafa
 * @date Oct 13, 2018
 */
public interface IRObAttachment extends IResourceObject {
  String SYS_NAME = "RObAttachment";
}
