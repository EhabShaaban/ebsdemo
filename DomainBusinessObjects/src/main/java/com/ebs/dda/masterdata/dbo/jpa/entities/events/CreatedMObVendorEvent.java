package com.ebs.dda.masterdata.dbo.jpa.entities.events;

import com.ebs.dac.dbo.jpa.converters.events.MObVendorEventJsonConverter;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorCreationValueObject;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class CreatedMObVendorEvent {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @Column
  private Timestamp eventCreationDateTime;
  @Column
  private String commandName;
  @Column
  private String vendorCode;
  @Column
  @Convert(converter = MObVendorEventJsonConverter.class)
  private MObVendorCreationValueObject vendorCreationValueObject;

  public Long getId() {
    return id;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public Timestamp getEventCreationDateTime() {
    return eventCreationDateTime;
  }

  public String getCommandName() {
    return commandName;
  }

  public void setCommandName(String commandName) {
    this.commandName = commandName;
  }

  public MObVendorCreationValueObject getVendorCreationValueObject() {
    return vendorCreationValueObject;
  }

  public void setVendorCreationValueObject(
      MObVendorCreationValueObject vendorCreationValueObject) {
    this.vendorCreationValueObject = vendorCreationValueObject;
  }
}
