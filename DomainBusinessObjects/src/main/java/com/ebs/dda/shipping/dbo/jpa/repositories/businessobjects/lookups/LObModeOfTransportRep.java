package com.ebs.dda.shipping.dbo.jpa.repositories.businessobjects.lookups;

import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.LObModeOfTransport;
import com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects.LObShippingRep;

public interface LObModeOfTransportRep extends LObShippingRep<LObModeOfTransport> {}
