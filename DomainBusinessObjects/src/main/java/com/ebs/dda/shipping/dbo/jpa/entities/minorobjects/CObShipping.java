package com.ebs.dda.shipping.dbo.jpa.entities.minorobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.ConfigurationObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "CObShipping")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn(name = "objectTypeCode")
public abstract class CObShipping extends ConfigurationObject<DefaultUserCode>
    implements Serializable {

  private static final long serialVersionUID = 1L;
}
