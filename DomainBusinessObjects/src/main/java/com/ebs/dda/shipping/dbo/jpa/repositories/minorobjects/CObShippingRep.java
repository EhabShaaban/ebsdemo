package com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.ConfigurationObjectRep;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.CObShipping;

/**
 * @author Ahmed Ali Elfeky
 * @since Dec 19, 2017 1:52:59 PM
 */
public interface CObShippingRep<T extends CObShipping>
    extends ConfigurationObjectRep<T, DefaultUserCode> {
  T findOneByUserCode(String userCode);
}
