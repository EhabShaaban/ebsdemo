package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.informationobjects.apis.IIObModeOfTransport;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
@Entity
@Table(name = "IObModeOfTransport")
@EntityInterface(IIObModeOfTransport.class)
public class IObModeOfTransport extends InformationObject implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "modeOfTransportId")
  private Long modeOfTransport;

  @Override
  public String getSysName() {
    return IIObModeOfTransport.SYS_NAME;
  }
}
