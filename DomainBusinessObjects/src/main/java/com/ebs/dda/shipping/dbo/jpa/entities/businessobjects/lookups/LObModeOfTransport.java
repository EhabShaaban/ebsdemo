package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups;

import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.apis.IShippingLookupsSysNames;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.LObShipping;
import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class LObModeOfTransport extends LObShipping implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IShippingLookupsSysNames.LOB_MODE_OF_TRANSPORT_SYS_NAME;
  }
}
