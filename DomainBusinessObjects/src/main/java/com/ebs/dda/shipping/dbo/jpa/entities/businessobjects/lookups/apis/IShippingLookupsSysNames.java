package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.apis;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 5, 2018 10:10:14 AM
 */
public interface IShippingLookupsSysNames {

  String LOB_MATERIAL_NATURE_SYS_NAME = "lobMaterialNature";
  String LOB_MODE_OF_TRANSPORT_SYS_NAME = "ModeOfTransport";
}
