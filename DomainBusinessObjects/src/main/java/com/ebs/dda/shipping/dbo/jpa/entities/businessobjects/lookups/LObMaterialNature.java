package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups;

import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.apis.IShippingLookupsSysNames;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.LObShipping;
import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
@Entity
@DiscriminatorValue("1")
public class LObMaterialNature extends LObShipping implements Serializable {

  private static final long serialVersionUID = 1L;

  @Override
  public String getSysName() {
    return IShippingLookupsSysNames.LOB_MATERIAL_NATURE_SYS_NAME;
  }
}
