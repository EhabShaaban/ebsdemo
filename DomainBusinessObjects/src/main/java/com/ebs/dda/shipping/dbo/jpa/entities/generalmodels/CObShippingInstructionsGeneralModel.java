package com.ebs.dda.shipping.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.configurationobjects.apis.ICObShippingInstructions;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
@Entity
@Table(name = "CObShippingInstructionsGeneralModel")
public class CObShippingInstructionsGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "instructiontext")
  private LocalizedString instructionText;

  @Transient private List<Long> materialsNature;

  @Transient private List<Long> modesOfTransport;

  public LocalizedString getInstructionText() {
    return instructionText;
  }

  public void setInstructionText(LocalizedString instructionText) {
    this.instructionText = instructionText;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  @Override
  public String getSysName() {
    return ICObShippingInstructions.SYS_NAME;
  }
}
