package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.configurationobjects.apis;

import com.ebs.dac.dbo.api.processing.IEmbeddedAttribute;
import com.ebs.dac.dbo.processing.EmbeddedAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
public interface ICObShippingInstructions {
  String SYS_NAME = "ShippingInstructions";

  byte INSTRUCTION_TEXT_CODE = 20;
  String INSTRUCTION_TEXT_ATTR_NAME = "instructionText";
  IEmbeddedAttribute<LocalizedString> instructionTextAttr =
      new EmbeddedAttribute<>(
          INSTRUCTION_TEXT_ATTR_NAME, LocalizedString.class);

  /** ******************************** PayLoad Attributes ********************************** */
  String MODE_OF_TRANSPORT_NAME = "modesOfTransport";
}
