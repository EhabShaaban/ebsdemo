package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.informationobjects.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
public interface IIObModeOfTransport extends IInformationObject {
  String SYS_NAME = "IObModeOfTransport";

  String MODE_OF_TRANSPORT_NAME = "modeOfTransport";
}
