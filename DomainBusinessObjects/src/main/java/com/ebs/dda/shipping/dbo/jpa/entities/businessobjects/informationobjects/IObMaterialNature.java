package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.informationobjects.apis.IIObMaterialNature;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
@Entity
@Table(name = "IObMaterialNature")
@EntityInterface(IIObMaterialNature.class)
public class IObMaterialNature extends InformationObject implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "materialNatureId")
  private Long materialNature;

  @Override
  public String getSysName() {
    return IIObMaterialNature.SYS_NAME;
  }
}
