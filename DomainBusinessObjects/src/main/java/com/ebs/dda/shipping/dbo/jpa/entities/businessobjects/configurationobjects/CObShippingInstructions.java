package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.configurationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.configurationobjects.apis.ICObShippingInstructions;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.CObShipping;
import java.io.Serializable;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
@Entity
@Table(name = "CObShippingInstructions")
@DiscriminatorValue("2")
@EntityInterface(ICObShippingInstructions.class)
public class CObShippingInstructions extends CObShipping
    implements Serializable, IAggregateAwareEntity {
  private static final long serialVersionUID = 1L;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString instructionText;

  public CObShippingInstructions() {
    aggregateBusinessObject = new AggregateBusinessObject();
    instructionText = new LocalizedString();
  }

  public LocalizedString getInstructionText() {
    if (instructionText == null) {
      instructionText = new LocalizedString();
    }
    return instructionText;
  }

  public void setInstructionText(LocalizedString instructionText) {
    this.instructionText = instructionText;
  }

  @Override
  public String getSysName() {
    return ICObShippingInstructions.SYS_NAME;
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }
}
