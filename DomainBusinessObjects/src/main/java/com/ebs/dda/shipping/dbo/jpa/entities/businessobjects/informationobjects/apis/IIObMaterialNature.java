package com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.informationobjects.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/**
 * @author Maged Zakzouk
 *     <p>Oct 22, 2017
 */
public interface IIObMaterialNature extends IInformationObject {
  String SYS_NAME = "IObMaterialNature";

  String MATERIAL_NATURE_NAME = "materialNature";
}
