package com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.LocalizedLookupRep;
import com.ebs.dda.shipping.dbo.jpa.entities.minorobjects.LObShipping;

/**
 * @author Ahmed Ali Elfeky
 * @since Dec 19, 2017 1:52:59 PM
 */
public interface LObShippingRep<T extends LObShipping>
    extends LocalizedLookupRep<T, DefaultUserCode> {}
