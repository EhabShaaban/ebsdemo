package com.ebs.dda.accounting.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.jpa.masterdata.chartofaccount.SubAccountGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubAccountGeneralModelRep extends JpaRepository<SubAccountGeneralModel, Long> {

  List<SubAccountGeneralModel> findAllByLedger(String ledger);

  SubAccountGeneralModel findOneByCodeAndLedger(String code, String ledger);

}
