package com.ebs.dda.accounting.dbo.jpa.valueobjects;

public class IObBusinessPartnerValueObject {
    private String salesInvoiceCode;
    private String currencyCode;
    private String paymentTermCode;

    public String getSalesInvoiceCode() {
        return salesInvoiceCode;
    }

    public void setSalesInvoiceCode(String salesInvoiceCode) {
        this.salesInvoiceCode = salesInvoiceCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPaymentTermCode() {
        return paymentTermCode;
    }

    public void setPaymentTermCode(String paymentTermCode) {
        this.paymentTermCode = paymentTermCode;
    }

}
