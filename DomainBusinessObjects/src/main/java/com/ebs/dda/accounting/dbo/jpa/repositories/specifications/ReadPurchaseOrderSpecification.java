package com.ebs.dda.accounting.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadPurchaseOrderSpecification {

    public Specification<DObPurchaseOrderGeneralModel> isAuthorizedPurchaseUnit(
            List<String> purchaseUnitNames) {
        return new Specification<DObPurchaseOrderGeneralModel>() {
            @Override
            public Predicate toPredicate(
                    Root<DObPurchaseOrderGeneralModel> root,
                    CriteriaQuery<?> query,
                    CriteriaBuilder criteriaBuilder) {

                if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
                    Expression<String> purchaseUnitEnglishValue =
                            criteriaBuilder.function(
                                    "json_extract_path_text",
                                    String.class,
                                    root.<String>get("purchaseUnitName"),
                                    criteriaBuilder.literal("en"));
                    return purchaseUnitEnglishValue.in(purchaseUnitNames);
                }
                return null;
            }
        };
    }

    public Specification<DObPurchaseOrderGeneralModel> isAllowedPurchaseOrder(
            String invoiceTypeCode, String vendorCode) {
        return new Specification<DObPurchaseOrderGeneralModel>() {
            @Override
            public Predicate toPredicate(
                    Root<DObPurchaseOrderGeneralModel> root,
                    CriteriaQuery<?> query,
                    CriteriaBuilder criteriaBuilder) {
                return isAllowedPurchaseOrder(root, criteriaBuilder, invoiceTypeCode, vendorCode);
            }
        };
    }

    private Predicate isAllowedPurchaseOrder(
            Root<DObPurchaseOrderGeneralModel> root,
            CriteriaBuilder criteriaBuilder,
            String invoiceTypeCode,
            String vendorCode) {
        Predicate vendor = whenVendorIs(root, criteriaBuilder, vendorCode);
        Predicate servicePurchaseOrderType = whenPurchaseOrderTypeIs(root, criteriaBuilder, OrderTypeEnum.SERVICE_PO.name());
        Predicate localPurchaseOrderType = whenPurchaseOrderTypeIs(root, criteriaBuilder, OrderTypeEnum.LOCAL_PO.name());
        Predicate importPurchaseOrderType = whenPurchaseOrderTypeIs(root, criteriaBuilder, OrderTypeEnum.IMPORT_PO.name());
        Predicate isConfirmed = whenStateIs(root, criteriaBuilder, "Confirmed");
        Predicate isFinishedProduction = whenStateIs(root, criteriaBuilder, "FinishedProduction");
        Predicate isShipped = whenStateIs(root, criteriaBuilder, "Shipped");
        Predicate isArrived = whenStateIs(root, criteriaBuilder, "Arrived");
        Predicate isCleared = whenStateIs(root, criteriaBuilder, "Cleared");
        Predicate isDeliveryComplete = whenStateIs(root, criteriaBuilder, "DeliveryComplete");
        Predicate allowedStates =
                criteriaBuilder.or(
                        isConfirmed, isFinishedProduction, isShipped, isArrived, isCleared, isDeliveryComplete);
        if (VendorInvoiceTypeEnum.VendorInvoiceType.isPurchaseServiceLocalInvoice(invoiceTypeCode))
            return criteriaBuilder.and(isConfirmed, vendor, servicePurchaseOrderType);
        else if (invoiceTypeCode.equals(VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE))
            return criteriaBuilder.and(allowedStates, vendor, localPurchaseOrderType);
        else if (invoiceTypeCode.equals(VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE))
            return criteriaBuilder.and(allowedStates, vendor, importPurchaseOrderType);
        return criteriaBuilder.and();
    }

    private Predicate whenStateIs(
            Root<DObPurchaseOrderGeneralModel> root,
            CriteriaBuilder criteriaBuilder,
            String state) {
        return criteriaBuilder.like(
                root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
    }

    private Predicate whenVendorIs(
            Root<DObPurchaseOrderGeneralModel> root,
            CriteriaBuilder criteriaBuilder,
            String vendorCode) {
        return criteriaBuilder.equal(root.get(IDObPurchaseOrderGeneralModel.VENDOR_CODE), vendorCode);
    }
    private Predicate whenPurchaseOrderTypeIs(
            Root<DObPurchaseOrderGeneralModel> root,
            CriteriaBuilder criteriaBuilder,
            String purchaseOrderType) {
        return criteriaBuilder.equal(root.get(IDObPurchaseOrderGeneralModel.OBJECT_TYPE_CODE), purchaseOrderType);
    }
}
