package com.ebs.dda.accounting.dbo.jpa.entities.businessobject.enums;

import com.ebs.dac.dbo.types.BusinessEnum;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class SettlementTypeEnum extends BusinessEnum {

  public static final String SYS_NAME = "SettlementType";

  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private SettlementType value;

  public SettlementTypeEnum() {}

  @Override
  public SettlementType getId() {
    return this.value;
  }

  public void setId(SettlementType id) {
    this.value = id;
  }

  public enum SettlementType {
    SETTLEMENTS
  }
}
