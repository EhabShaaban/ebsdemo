package com.ebs.dda.accounting.dbo.jpa.repositories.specifications;

import java.util.List;

import javax.persistence.criteria.*;

import org.springframework.data.jpa.domain.Specification;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;

public class ReadSalesOrderSpecification {

	public Specification<DObSalesOrderGeneralModel> isAuthorizedPurchaseUnit(
					List<String> purchaseUnitNames) {
		return new Specification<DObSalesOrderGeneralModel>() {
			@Override
			public Predicate toPredicate(Root<DObSalesOrderGeneralModel> root,
							CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

				if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
					Expression<String> purchaseUnitEnglishValue = criteriaBuilder.function(
									"json_extract_path_text", String.class,
									root.<String>get("purchaseUnitName"),
									criteriaBuilder.literal("en"));
					return purchaseUnitEnglishValue.in(purchaseUnitNames);
				}
				return null;
			}
		};
	}

	public Specification<DObSalesOrderGeneralModel> isAllowedSalesOrder(String businessUnitCode) {
		return new Specification<DObSalesOrderGeneralModel>() {
			@Override
			public Predicate toPredicate(Root<DObSalesOrderGeneralModel> root,
							CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return isAllowedPurchaseOrder(root, criteriaBuilder, businessUnitCode);
			}
		};
	}

	private Predicate isAllowedPurchaseOrder(Root<DObSalesOrderGeneralModel> root,
					CriteriaBuilder criteriaBuilder, String businessUnitCode) {
		Predicate businessUnit = whenBusinessUnitCodeIs(root, criteriaBuilder, businessUnitCode);
		Predicate isApproved = whenStateIs(root, criteriaBuilder, "Approved");
		Predicate isGoodsIssued = whenStateIs(root, criteriaBuilder, "GoodsIssueActivated");
		Predicate allowedStates = criteriaBuilder.or(isApproved, isGoodsIssued);
		return criteriaBuilder.and(allowedStates, businessUnit);
	}

	private Predicate whenStateIs(Root<DObSalesOrderGeneralModel> root,
					CriteriaBuilder criteriaBuilder, String state) {
		return criteriaBuilder.like(root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME),
						"%" + state + "%");
	}

	private Predicate whenBusinessUnitCodeIs(Root<DObSalesOrderGeneralModel> root,
					CriteriaBuilder criteriaBuilder, String businessUnitCode) {
		return criteriaBuilder.equal(root.get(IDObSalesOrderGeneralModel.PURCHASE_UNIT_CODE),
						businessUnitCode);
	}
}
