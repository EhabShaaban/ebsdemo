package com.ebs.dda.accounting.dbo.jpa.valueobjects;

public interface IIObBusinessPartnerValueObject {
  String CURRENY_CODE = "currencyCode";
  String PAYMENT_TERM_CODE = "paymentTermCode";
}
