package com.ebs.dda.accounting.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadPaymentRequestSpecification {

  public static final String PAYMENT_DONE = "PaymentDone";
  public static final String UNDER_SETTLEMENT = "UNDER_SETTLEMENT";

  public Specification<DObPaymentRequestGeneralModel> isAuthorizedBusinessUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObPaymentRequestGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObPaymentRequestGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObPaymentRequestGeneralModel> isUnderSettlement() {
    return (root, query, criteriaBuilder) -> getUnderSettlementPredicate(root, criteriaBuilder);
  }

  private Predicate getUnderSettlementPredicate(
      Root<DObPaymentRequestGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.equal(
        root.get(IDObPaymentRequestGeneralModel.PAYMENT_TYPE), UNDER_SETTLEMENT);
  }

  public Specification<DObPaymentRequestGeneralModel> isPaymentDone() {
    return (root, query, criteriaBuilder) -> getPaymentDonePredicate(root, criteriaBuilder);
  }

  private Predicate getPaymentDonePredicate(
      Root<DObPaymentRequestGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return whenStateIs(root, criteriaBuilder, PAYMENT_DONE);
  }

  public Specification<DObPaymentRequestGeneralModel> remainingIsGreaterThanZero() {
    return (root, query, criteriaBuilder) ->
        getRemainingGreaterThanZeroPredicate(root, criteriaBuilder);
  }

  private Predicate getRemainingGreaterThanZeroPredicate(
      Root<DObPaymentRequestGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(root.get(IDObPaymentRequestGeneralModel.REMAINING), 0.0);
  }

  private Predicate whenStateIs(
      Root<DObPaymentRequestGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
