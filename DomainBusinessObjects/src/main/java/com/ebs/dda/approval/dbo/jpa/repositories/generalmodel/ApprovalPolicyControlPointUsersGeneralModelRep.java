package com.ebs.dda.approval.dbo.jpa.repositories.generalmodel;

import com.ebs.dda.approval.dbo.jpa.entities.generalmodel.ApprovalPolicyControlPointUsersGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApprovalPolicyControlPointUsersGeneralModelRep
    extends JpaRepository<ApprovalPolicyControlPointUsersGeneralModel, Long> {

  List<ApprovalPolicyControlPointUsersGeneralModel>
      findByApprovalPolicyCodeAndIsMainTrueOrderByControlPointCodeAsc(String approvalPolicyCode);
}
