package com.ebs.dda.approval.dbo.jpa.entities.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dda.approval.dbo.jpa.entities.informationobjects.apis.IIObApprovalPolicies;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObApprovalPolicyControlPoint")
@EntityInterface(IIObApprovalPolicies.class)
public class IObApprovalPolicyControlPoint extends InformationObject {

  private static final long serialVersionUID = 1L;

  @Column(name = "controlpointid")
  private Long controlPointId;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }
  
  public Long getControlPointId() {
    return controlPointId;
  }

  public void setControlPointId(Long controlPointId) {
    this.controlPointId = controlPointId;
  }

  @Override
  public String getSysName() {
    return IIObApprovalPolicies.SYS_NAME;
  }
}
