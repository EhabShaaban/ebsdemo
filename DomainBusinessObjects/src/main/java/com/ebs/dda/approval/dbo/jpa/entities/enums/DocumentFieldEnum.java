package com.ebs.dda.approval.dbo.jpa.entities.enums;
// Created By xerix - 3/12/18 - 8:32 AM

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class DocumentFieldEnum extends BusinessEnum {
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private ConditionDocumentFields value;

  public DocumentFieldEnum() {}

  public ConditionDocumentFields getValue() {
    return value;
  }

  public void setValue(ConditionDocumentFields value) {
    this.value = value;
  }

  @Override
  public ConditionDocumentFields getId() {
    return this.value;
  }

  public void setId(ConditionDocumentFields id) {
    this.value = id;
  }

  public enum ConditionDocumentFields {
    PRICE,
    COMPANY
  }
}
