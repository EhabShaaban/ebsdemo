package com.ebs.dda.approval.dbo.jpa.entities.enums;
// Created By xerix - 3/12/18 - 8:34 AM

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class ConditionDocumentFieldValuesEnum extends BusinessEnum {
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private ConditionDocumentFieldValue value;

  public ConditionDocumentFieldValuesEnum() {}

  public ConditionDocumentFieldValue getValue() {
    return value;
  }

  public void setValue(ConditionDocumentFieldValue value) {
    this.value = value;
  }

  @Override
  public ConditionDocumentFieldValue getId() {
    return this.value;
  }

  public void setId(ConditionDocumentFieldValue id) {
    this.value = id;
  }

  public enum ConditionDocumentFieldValue {
    DIGI_PRO,
    NUMBER
  }
}
