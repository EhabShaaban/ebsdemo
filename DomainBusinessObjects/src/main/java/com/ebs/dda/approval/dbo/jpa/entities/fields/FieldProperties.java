package com.ebs.dda.approval.dbo.jpa.entities.fields;

import java.io.Serializable;
import java.util.Map;

// Created By Hossam Hassan @2018/3/12

public class FieldProperties implements Serializable {

  private Long id;

  private Map<String, String> value;

  private String entityAttributeName;

  private String type;

  public String getEntityAttributeName() {
    return entityAttributeName;
  }

  public void setEntityAttributeName(String entityAttributeName) {
    this.entityAttributeName = entityAttributeName;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Map<String, String> getValue() {
    return value;
  }

  public void setValue(Map<String, String> value) {
    this.value = value;
  }
}
