package com.ebs.dda.approval.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.approval.dbo.jpa.entities.businessobjects.apis.ICObControlPoint;
import java.io.Serializable;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

// Created By xerix - 2/19/18 - 9:05 AM
@Entity
@Table(name = "cobcontrolpoint")
@EntityInterface(ICObControlPoint.class)
public class CObControlPoint extends CodedBusinessObject<DefaultUserCode>
    implements IAggregateAwareEntity, Serializable {
  private static final long serialVersionUID = 1L;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  public CObControlPoint() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  public AggregateBusinessObject getAggregateBusinessObject() {
    return aggregateBusinessObject;
  }

  public void setAggregateBusinessObject(AggregateBusinessObject aggregateBusinessObject) {
    this.aggregateBusinessObject = aggregateBusinessObject;
  }

  public LocalizedString getName() {
    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }

  @Override
  public String getSysName() {
    return ICObControlPoint.SYS_NAME;
  }
}
