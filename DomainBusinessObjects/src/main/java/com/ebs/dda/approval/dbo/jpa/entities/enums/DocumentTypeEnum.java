package com.ebs.dda.approval.dbo.jpa.entities.enums;

public enum DocumentTypeEnum {
    IMPORT_PO,
    LOCAL_PO,
    ALL_PO
  }
