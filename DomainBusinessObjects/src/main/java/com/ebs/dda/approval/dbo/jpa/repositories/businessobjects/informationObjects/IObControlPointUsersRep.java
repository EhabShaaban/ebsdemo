package com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.informationObjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.approval.dbo.jpa.entities.informationobjects.IObControlPointUsers;

public interface IObControlPointUsersRep extends InformationObjectRep<IObControlPointUsers> {}
