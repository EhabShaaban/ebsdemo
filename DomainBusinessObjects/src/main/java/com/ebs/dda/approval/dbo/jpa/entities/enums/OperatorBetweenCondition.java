package com.ebs.dda.approval.dbo.jpa.entities.enums;
// Created By xerix - 3/12/18 - 8:19 AM

public enum OperatorBetweenCondition {
    AND,
    OR,
    END
}
