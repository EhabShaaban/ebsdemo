package com.ebs.dda.approval.dbo.jpa.entities.fields;

import com.ebs.dda.approval.dbo.jpa.entities.converter.ObjectConditionFieldsVarArrayConverter;
import com.ebs.dda.approval.dbo.jpa.entities.enums.DocumentTypeEnum;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;


// Created By Hossam Hassan @2018/3/12
@Entity
@Table(name = "objectconditionfields")
public class ObjectConditionFields {
  @Id private Long id;

  @Enumerated(EnumType.STRING)
  private DocumentTypeEnum documentType;

  @Column(name = "vararray")
  @Convert(converter = ObjectConditionFieldsVarArrayConverter.class)
  private List<FieldProperties> varArray;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public DocumentTypeEnum getDocumentType() {
    return documentType;
  }

  public void setDocumentType(DocumentTypeEnum documentType) {
    this.documentType = documentType;
  }

  public List<FieldProperties> getVarArray() {
    return varArray;
  }

  public void setVarArray(List<FieldProperties> varArray) {
    this.varArray = varArray;
  }
}
