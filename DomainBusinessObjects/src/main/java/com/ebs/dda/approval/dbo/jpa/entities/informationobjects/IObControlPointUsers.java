package com.ebs.dda.approval.dbo.jpa.entities.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dda.approval.dbo.jpa.entities.informationobjects.apis.IIObControlPointUsers;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

// Created By xerix - 2/19/18 - 9:06 AM
@Entity
@Table(name = "iobcontrolpointusers")
@EntityInterface(IIObControlPointUsers.class)
public class IObControlPointUsers extends InformationObject {
  private static final long serialVersionUID = 1L;

  @Column(name = "ismain")
  private Boolean isMain;

  @Column(name = "userid")
  private Long userId;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Boolean getIsMain() {
    return isMain;
  }

  public void setIsMain(Boolean main) {
    isMain = main;
  }

  @Override
  public String getSysName() {
    return IIObControlPointUsers.SYS_NAME;
  }
}
