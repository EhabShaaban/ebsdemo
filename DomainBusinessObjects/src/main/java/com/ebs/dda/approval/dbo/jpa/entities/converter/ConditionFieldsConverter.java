package com.ebs.dda.approval.dbo.jpa.entities.converter;
// Created By xerix - 3/15/18 - 11:41 AM

import com.ebs.dda.approval.dbo.jpa.entities.fields.ConditionFields;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Converter
public class ConditionFieldsConverter
    implements AttributeConverter<List<ConditionFields>, PGobject> {

  private static final Logger logger = LoggerFactory.getLogger(ConditionFields.class);

  private final Type dataValueType = (new TypeToken<List<ConditionFields>>() {}).getType();

  private String verifyValue(String value, String valueType) {
    if (valueType.equals("double")) {
      if (!value.contains(".")) {
        value += ".0";
      }
    } else if (valueType.equals("long")) {
      if (value.indexOf(".") > 0) {
        value = value.substring(0, value.indexOf("."));
      }
    }
    return value;
  }

  @Override
  public PGobject convertToDatabaseColumn(List<ConditionFields> conditionFields) {
    List<ConditionFields> modifiedConditionFields = modifyList(conditionFields);
    PGobject dbData = new PGobject();
    dbData.setType("json");
    Gson gson = new Gson();
    String json = gson.toJson(conditionFields);
    try {
      dbData.setValue(json);
    } catch (SQLException e) {
      logger.error("", e);
    }
    return dbData;
  }

  private List<ConditionFields> modifyList(List<ConditionFields> conditionFields) {
    List<ConditionFields> modifiedConditionList = new ArrayList<>();
    for (ConditionFields conditionField : conditionFields) {
      conditionField.setValue(
          verifyValue(conditionField.getValue(), conditionField.getValueType()));
      modifiedConditionList.add(conditionField);
    }
    return modifiedConditionList;
  }

  @Override
  public List<ConditionFields> convertToEntityAttribute(PGobject dbData) {
    if (dbData != null) {
      Gson gson = new Gson();
      return gson.fromJson(dbData.getValue(), dataValueType);
    }
    return null;
  }
}
