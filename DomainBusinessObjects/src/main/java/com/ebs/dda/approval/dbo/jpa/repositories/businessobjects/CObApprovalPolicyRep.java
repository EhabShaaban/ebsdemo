package com.ebs.dda.approval.dbo.jpa.repositories.businessobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.approval.dbo.jpa.entities.businessobjects.CObApprovalPolicy;
import com.ebs.dda.approval.dbo.jpa.entities.enums.DocumentTypeEnum;
import java.util.List;

public interface CObApprovalPolicyRep extends CodedBusinessObjectRep<CObApprovalPolicy> {

  List<CObApprovalPolicy> findByDocumentType(
      DocumentTypeEnum documentType);
}
