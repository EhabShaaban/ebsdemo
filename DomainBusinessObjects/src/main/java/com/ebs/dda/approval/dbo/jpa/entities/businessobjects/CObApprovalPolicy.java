package com.ebs.dda.approval.dbo.jpa.entities.businessobjects;

import com.ebs.dac.dbo.api.objects.IAggregateAwareEntity;
import com.ebs.dac.dbo.api.objects.IAggregateBusinessObject;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.AggregateBusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.approval.dbo.jpa.entities.businessobjects.apis.ICObApprovalPolicies;
import com.ebs.dda.approval.dbo.jpa.entities.enums.DocumentTypeEnum;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

// Created By xerix - 2/19/18 - 8:58 AM
@Entity
@Table(name = "CObApprovalPolicy")
@EntityInterface(ICObApprovalPolicies.class)
public class CObApprovalPolicy extends CodedBusinessObject<DefaultUserCode>
    implements IAggregateAwareEntity, Serializable {

  @Transient private AggregateBusinessObject aggregateBusinessObject;

  @Enumerated(EnumType.STRING)
  private DocumentTypeEnum documentType;

  private String condition;

  public CObApprovalPolicy() {
    aggregateBusinessObject = new AggregateBusinessObject();
  }

  @Override
  public <Detail> IAggregateBusinessObject<Detail> aggregateBusinessObject() {
    return (IAggregateBusinessObject<Detail>) aggregateBusinessObject;
  }

  public AggregateBusinessObject getAggregateBusinessObject() {
    return aggregateBusinessObject;
  }

  public void setAggregateBusinessObject(AggregateBusinessObject aggregateBusinessObject) {
    this.aggregateBusinessObject = aggregateBusinessObject;
  }

  public DocumentTypeEnum getDocumentType() {
    return documentType;
  }

  public void setDocumentType(DocumentTypeEnum documentType) {
    this.documentType = documentType;
  }

  public String getCondition() {
    return condition;
  }

  @Override
  public String getSysName() {
    return ICObApprovalPolicies.SYS_NAME;
  }
}
