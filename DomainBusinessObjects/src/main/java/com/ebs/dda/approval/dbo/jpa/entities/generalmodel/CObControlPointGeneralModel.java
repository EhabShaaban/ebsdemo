package com.ebs.dda.approval.dbo.jpa.entities.generalmodel;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

// Created By xerix - 2/19/18 - 9:11 AM
@Entity
@Table(name = "cobcontrolpointgeneralmodel")
public class CObControlPointGeneralModel extends BusinessObject {
  private static final long serialVersionUID = 1L;

  @Column(name = "code")
  private String code;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  public CObControlPointGeneralModel() {}

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public LocalizedString getName() {

    return name;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  @Override
  public String getSysName() {
    return "CObControlPointGeneralModel";
  }
}
