package com.ebs.dda.approval.dbo.jpa.entities.enums;
// Created By xerix - 2/21/18 - 12:09 PM

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Embeddable
public class DocumentNameEnum extends BusinessEnum {
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private DocumentName value;

  public DocumentNameEnum() {}

  public DocumentName getValue() {
    return value;
  }

  public void setValue(DocumentName value) {
    this.value = value;
  }

  @Override
  public DocumentName getId() {
    return this.value;
  }

  public void setId(DocumentName id) {
    this.value = id;
  }

  public enum DocumentName {
    PURCHASE_ORDER
  }
}
