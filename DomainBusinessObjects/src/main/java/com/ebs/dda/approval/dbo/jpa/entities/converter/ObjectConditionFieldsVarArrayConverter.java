package com.ebs.dda.approval.dbo.jpa.entities.converter;

import com.ebs.dda.approval.dbo.jpa.entities.fields.FieldProperties;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.List;
import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import org.postgresql.util.PGobject;

// Created By Hossam Hassan @2018/3/12
@Converter
public class ObjectConditionFieldsVarArrayConverter
    implements AttributeConverter<List<FieldProperties>, PGobject> {

  private final Type dataValueType = (new TypeToken<List<FieldProperties>>() {}).getType();

  @Override
  public PGobject convertToDatabaseColumn(List<FieldProperties> fieldProperties) {
    return null;
  }

  @Override
  public List<FieldProperties> convertToEntityAttribute(PGobject dbData) {
    if (dbData != null) {
      Gson gson = new Gson();
      FieldProperties fp;
      return gson.fromJson(dbData.getValue(), dataValueType);
    }
    return null;
  }
}
