package com.ebs.dda.approval.dbo.jpa.entities.generalmodel;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ApprovalPolicyControlPointUsersGeneralModel")
public class ApprovalPolicyControlPointUsersGeneralModel {

  @Id private Long id;
  private String approvalPolicyCode;
  private String controlPointCode;
  private Long controlPointId;
  private Long userId;
  private Boolean isMain;

  public String getApprovalPolicyCode() {
    return approvalPolicyCode;
  }

  public String getControlPointCode() {
    return controlPointCode;
  }

  public Long getControlPointId() {
    return controlPointId;
  }

  public Long getUserId() {
    return userId;
  }

  public Boolean getIsMain() {
    return isMain;
  }
}
