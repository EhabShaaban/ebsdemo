package com.ebs.dda.approval.dbo.jpa.repositories.businessobjects;
// Created By xerix - 2/19/18 - 12:56 PM

import com.ebs.dac.dbo.jpa.repositories.businessobjects.CodedBusinessObjectRep;
import com.ebs.dda.approval.dbo.jpa.entities.businessobjects.CObControlPoint;

public interface CObControlPointRep extends CodedBusinessObjectRep<CObControlPoint> {}
