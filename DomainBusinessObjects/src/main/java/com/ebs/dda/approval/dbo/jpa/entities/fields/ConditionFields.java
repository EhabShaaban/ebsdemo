package com.ebs.dda.approval.dbo.jpa.entities.fields;
// Created By xerix - 3/15/18 - 11:37 AM

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.approval.dbo.jpa.entities.enums.ConditionOperator;
import com.ebs.dda.approval.dbo.jpa.entities.enums.OperatorBetweenCondition;
import java.io.Serializable;
import javax.persistence.Convert;

public class ConditionFields implements Serializable {

  private static final long serialVersionUID = 1L;
  // variable id which corresponding to id at ObjectConditionFields table
  private Long id;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString variable;

  private ConditionOperator operator;
  private String value;
  private String valueType;
  private OperatorBetweenCondition conditionOperator;
  private String entityAttributeName;

  public ConditionFields() {}

  public ConditionFields(
      LocalizedString variable,
      ConditionOperator operator,
      String value,
      String valueType,
      String entityAttributeName) {
    this.variable = variable;
    this.operator = operator;
    this.value = value;
    this.valueType = valueType;
    this.entityAttributeName = entityAttributeName;
  }

  public ConditionFields(
      LocalizedString variable,
      ConditionOperator operator,
      String value,
      String valueType,
      String entityAttributeName,
      OperatorBetweenCondition conditionOperator) {
    this(variable, operator, value, valueType, entityAttributeName);
    this.conditionOperator = conditionOperator;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getEntityAttributeName() {
    return entityAttributeName;
  }

  public void setEntityAttributeName(String entityAttributeName) {
    this.entityAttributeName = entityAttributeName;
  }

  public LocalizedString getVariable() {
    return variable;
  }

  public void setVariable(LocalizedString variable) {
    this.variable = variable;
  }

  public String getEnglishVariable() {
    return variable.getValue("en");
  }

  public ConditionOperator getOperator() {
    return operator;
  }

  public void setOperator(ConditionOperator operator) {
    this.operator = operator;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getValueType() {
    return valueType;
  }

  public void setValueType(String valueType) {
    this.valueType = valueType;
  }

  public OperatorBetweenCondition getConditionOperator() {
    return conditionOperator;
  }

  public void setConditionOperator(OperatorBetweenCondition conditionOperator) {
    this.conditionOperator = conditionOperator;
  }

  @Override
  public String toString() {
    String str = "[" + variable + " " + operator + " " + value + "]";
    str +=
        (conditionOperator == null
                || conditionOperator.toString().equals(OperatorBetweenCondition.END.toString()))
            ? ""
            : " " + conditionOperator.toString();
    return str;
  }
}
