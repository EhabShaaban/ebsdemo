package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class CreatedBySpecification<T extends BusinessObject> implements Specification<T> {

  private String loggedInUser;
  private String key;

  public CreatedBySpecification(String loggedInUser) {
    this.loggedInUser = loggedInUser;
    this.key = "creationInfo";
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    if (loggedInUser != null) {
      Path<String> expression = root.get(this.key);
      return builder.equal(expression, loggedInUser);
    }
    return null;
  }
}
