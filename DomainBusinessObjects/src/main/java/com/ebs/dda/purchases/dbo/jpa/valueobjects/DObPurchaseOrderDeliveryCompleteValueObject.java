package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import org.joda.time.DateTime;

public class DObPurchaseOrderDeliveryCompleteValueObject {

  private DateTime deliveryCompleteDateTime;

  public DateTime getDeliveryCompleteDateTime() {
    return deliveryCompleteDateTime;
  }

  public void setDeliveryCompleteDateTime(DateTime deliveryCompleteDateTime) {
    this.deliveryCompleteDateTime = deliveryCompleteDateTime;
  }
}
