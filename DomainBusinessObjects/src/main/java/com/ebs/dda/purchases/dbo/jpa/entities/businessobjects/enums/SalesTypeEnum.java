package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums;

public enum SalesTypeEnum {
  SALES_RETURN(Values.SALES_RETURN),
  SALES_ORDER(Values.SALES_ORDER);

  private SalesTypeEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }

  public static class Values {
    public static final String SALES_RETURN = "SALES_RETURN";
    public static final String SALES_ORDER = "SALES_ORDER";
  }
}
