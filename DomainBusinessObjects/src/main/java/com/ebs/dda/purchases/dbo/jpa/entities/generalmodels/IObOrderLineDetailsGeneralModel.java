package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.apis.IIObOrderLineDetailsGeneralModel;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@EntityInterface(IIObOrderLineDetailsGeneralModel.class)
@Table(name = "ioborderlinedetailsgeneralmodel")
public class IObOrderLineDetailsGeneralModel extends BusinessObject {

  private String orderCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "name")
  private LocalizedString name;

  private String itemCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "unitsymbol")
  private LocalizedString unitSymbol;

  @Column(name = "quantityInDn")
  private BigDecimal qtyInDn;

  private BigDecimal itemQuantity;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "itemgroupname")
  private LocalizedString itemGroupName;

  @Transient private List<IObOrderLineDetailsQuantitiesGeneralModel> quantities;

  @Transient
  private List<IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel> quantitiesWithoutPrices;

  public String getOrderCode() {
    return orderCode;
  }

  @Override
  public String getSysName() {
    return "IObOrderLineDetailsGeneralModel";
  }

  public LocalizedString getName() {
    return name;
  }

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getUnitSymbol() {
    return unitSymbol;
  }

  public BigDecimal getQtyInDn() {
    return qtyInDn;
  }

  public LocalizedString getItemGroupName() {
    return itemGroupName;
  }

  public List<IObOrderLineDetailsQuantitiesGeneralModel> getQuantities() {
    return quantities;
  }

  public void setQuantities(List<IObOrderLineDetailsQuantitiesGeneralModel> quantities) {
    this.quantities = quantities;
  }

  public List<IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel> getQuantitiesWithoutPrices() {
    return quantitiesWithoutPrices;
  }

  public void setQuantitiesWithoutPrices(
      List<IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel> quantitiesWithoutPrices) {
    this.quantitiesWithoutPrices = quantitiesWithoutPrices;
  }
}
