package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderAttachmentGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObPurchaseOrderAttachmentGeneralModelRep
    extends JpaRepository<DObPurchaseOrderAttachmentGeneralModel, Long> {

  List<DObPurchaseOrderAttachmentGeneralModel> findAllByUserCodeOrderByIdAsc(
      String purchaseOrderCode);
}
