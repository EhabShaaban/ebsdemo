package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderConsigneeGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadPurchaseOrderForGoodsReceiptSpecification {

  public Specification<DObPurchaseOrderConsigneeGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return (root, query, criteriaBuilder) -> {
      if (!purchaseUnitNames.isEmpty() && !purchaseUnitNames.contains("*")) {
        Expression<String> purchaseUnitEnglishValue =
            criteriaBuilder.function(
                "json_extract_path_text",
                String.class,
                root.<String>get("purchaseUnitName"),
                criteriaBuilder.literal("en"));
        return purchaseUnitEnglishValue.in(purchaseUnitNames);
      }
      return null;
    };
  }

  public Specification<DObPurchaseOrderConsigneeGeneralModel> isValidPurchaseOrder() {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.or(
            isImportClearedAndNotReceived(root, criteriaBuilder),
            isLocalShippedAndNotReceived(root, criteriaBuilder));
  }

  private Predicate isImportClearedAndNotReceived(
      Root<DObPurchaseOrderConsigneeGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isImport = whenPurchaseOrderObjectTypeCodeIs(root, criteriaBuilder, "IMPORT_PO");
    Predicate isCleared = whenStateIs(root, criteriaBuilder, "Cleared");
    Predicate isNotReceived = whenStateIs(root, criteriaBuilder, "NotReceived");
    return criteriaBuilder.and(isImport, isCleared, isNotReceived);
  }

  private Predicate isLocalShippedAndNotReceived(
      Root<DObPurchaseOrderConsigneeGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isLocal = whenPurchaseOrderObjectTypeCodeIs(root, criteriaBuilder, "LOCAL_PO");
    Predicate isShipped = whenStateIs(root, criteriaBuilder, "Shipped");
    Predicate isNotReceived = whenStateIs(root, criteriaBuilder, "NotReceived");
    return criteriaBuilder.and(isLocal, isShipped, isNotReceived);
  }

  private Predicate whenStateIs(
      Root<DObPurchaseOrderConsigneeGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }

  private Predicate whenPurchaseOrderObjectTypeCodeIs(
      Root<DObPurchaseOrderConsigneeGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      String objectTypeCode) {
    return criteriaBuilder.equal(root.get("objectTypeCode"), objectTypeCode);
  }
}
