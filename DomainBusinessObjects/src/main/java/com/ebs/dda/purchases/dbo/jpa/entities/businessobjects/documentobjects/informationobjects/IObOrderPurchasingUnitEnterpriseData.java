package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderPurchasingUnitEnterpriseData;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("PurchasingUnit")
@EntityInterface(IIObOrderPurchasingUnitEnterpriseData.class)
public class IObOrderPurchasingUnitEnterpriseData extends IObEnterpriseData {

  private static final long serialVersionUID = 1L;

  @Transient
  @EmbeddedAttribute
  private LocalizedString purchaseUnitName;

  public LocalizedString getPurchaseUnitName() {
    this.purchaseUnitName = super.getEnterpriseName();
    return purchaseUnitName;
  }

  public void setPurchaseUnitName(LocalizedString purchaseUnitName) {
    this.purchaseUnitName = purchaseUnitName;
  }
}
