package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObOrderPaymentGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class ReadPurchaseOrderForPaymentRequestSpecification {

  public Specification<DObOrderPaymentGeneralModel> isAuthorizedPurchaseUnit(
      String purchaseUnitName) {
    return new Specification<DObOrderPaymentGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObOrderPaymentGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return criteriaBuilder.equal(purchaseUnitEnglishValue, purchaseUnitName);
      }
    };
  }

  private Predicate whenStateIs(
      Root<DObOrderPaymentGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }

  private Predicate whenVendorCodeIs(
      Root<DObOrderPaymentGeneralModel> root, CriteriaBuilder criteriaBuilder, String vendorCode) {
    Path<String> expression = root.get(IDObPurchaseOrderGeneralModel.VENDOR_CODE);
    return criteriaBuilder.equal(expression, vendorCode);
  }

  private Predicate whenBusinessUnitCodeIs(
      Root<DObOrderPaymentGeneralModel> root, CriteriaBuilder criteriaBuilder, String businessUnitCode) {
    Path<String> expression = root.get(IDObPurchaseOrderGeneralModel.PURCHASE_UNIT_CODE);
    return criteriaBuilder.equal(expression, businessUnitCode);
  }
  private Predicate whenObjectTypeCodeIs(
          Root<DObOrderPaymentGeneralModel> root, CriteriaBuilder criteriaBuilder, String objectTypeCode) {
    Path<String> expression = root.get(IDObPurchaseOrderGeneralModel.OBJECT_TYPE_CODE);
    return criteriaBuilder.equal(expression, objectTypeCode);
  }
  public Specification<DObOrderPaymentGeneralModel> isAllowedDobOrder(String vendorCode, String paymentType, String businessUnitCode) {
    return new Specification<DObOrderPaymentGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObOrderPaymentGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {
        Predicate isApprovedOrder = whenStateIs(root, criteriaBuilder, "Approved");
        Predicate isShippedOrder = whenStateIs(root, criteriaBuilder, "Shipped");
        Predicate isArrivedOrder = whenStateIs(root, criteriaBuilder, "Arrived");
        Predicate isFinishedProductionOrder =
            whenStateIs(root, criteriaBuilder, "FinishedProduction");
        Predicate isConfirmedOrder = whenStateIs(root, criteriaBuilder, "Confirmed");
        Predicate isDeliveryCompleteOrder =
            whenStateIs(root, criteriaBuilder, "DeliveryComplete");
        Predicate isClearedOrder = whenStateIs(root, criteriaBuilder, "Cleared");
        Predicate isAllowedState =
            criteriaBuilder.or(
                isApprovedOrder,
                isShippedOrder,
                isArrivedOrder,
                isFinishedProductionOrder,
                isConfirmedOrder,
                isDeliveryCompleteOrder,
                isClearedOrder);
        Predicate orderBusinessUnitCode = whenBusinessUnitCodeIs(root, criteriaBuilder, businessUnitCode);
        if(paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())){
          Predicate orderVendorCode = whenVendorCodeIs(root, criteriaBuilder, vendorCode);
          return criteriaBuilder.and(isAllowedState, orderVendorCode, orderBusinessUnitCode);
        }
        return criteriaBuilder.and(isAllowedState, orderBusinessUnitCode);
      }
    };
  }
}
