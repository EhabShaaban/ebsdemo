package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.types.LocalizedString;
import java.io.Serializable;
import java.math.BigDecimal;

public class IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel implements Serializable {

  private final Long id;

  private final Long refInstanceId;

  private final BigDecimal quantity;

  private final LocalizedString orderUnitSymbol;

  private final BigDecimal quantityInBase;

  private final String itemCodeAtVendor;

  public IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel(
      Long id,
      Long refInstanceId,
      BigDecimal quantity,
      LocalizedString orderUnitSymbol,
      BigDecimal quantityInBase,
      String itemCodeAtVendor) {
    this.id = id;
    this.refInstanceId = refInstanceId;
    this.quantity = quantity;
    this.orderUnitSymbol = orderUnitSymbol;
    this.quantityInBase = quantityInBase;
    this.itemCodeAtVendor = itemCodeAtVendor;
  }

  public Long getId() {
    return id;
  }

  public Long getRefInstanceId() {
    return refInstanceId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public LocalizedString getOrderUnitSymbol() {
    return orderUnitSymbol;
  }

  public BigDecimal getQuantityInBase() {
    return quantityInBase;
  }

  public String getItemCodeAtVendor() {
    return itemCodeAtVendor;
  }
}
