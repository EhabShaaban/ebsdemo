package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderPaymentDetailsGeneralModel")
@EntityInterface(IIObOrderPaymentDetailsGeneralModel.class)
public class IObOrderPaymentDetailsGeneralModel extends InformationObject {

    private String orderCode;

    private String purchaseUnitCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString purchaseUnitName;

    private String purchaseUnitNameEn;
    private String objectTypeCode;

    private String paymentTermCode;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString paymentTermName;

    private String currencyCode;

    private String currencyIso;

    @Convert(converter = LocalizedStringConverter.class)
    private LocalizedString currencyName;

    public String getPaymentTermCode() {
        return paymentTermCode;
    }

    public LocalizedString getPaymentTermName() {
        return paymentTermName;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public String getPurchaseUnitCode() {
        return purchaseUnitCode;
    }

    public LocalizedString getPurchaseUnitName() {
        return purchaseUnitName;
    }

    public String getPurchaseUnitNameEn() {
        return purchaseUnitNameEn;
    }

    public String getObjectTypeCode() {
        return objectTypeCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public LocalizedString getCurrencyName() {
        return currencyName;
    }

    public String getCurrencyIso() {
        return currencyIso;
    }

    @Override
    public String getSysName() {
        return IDObPurchaseOrder.SYS_NAME;
    }
}
