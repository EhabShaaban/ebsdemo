package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDobPurchaseOrderCompanyValueObject {
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String COMPANY_CODE = "companyCode";
  String BANK_ACCOUNT_ID = "bankAccountId";
}
