package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderLineDetails;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Table;

/** @author Yara Ameen */
@Entity
@Table(name = "IObOrderLineDetails")
@EntityInterface(IIObOrderLineDetails.class)
public class IObOrderLineDetails extends DocumentLine implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long itemId;

  private BigDecimal quantityInDn;

  private Long unitOfMeasureId;

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public BigDecimal getQuantityInDn() {
    return quantityInDn;
  }

  public void setQuantityInDn(BigDecimal quantityInDn) {
    this.quantityInDn = quantityInDn;
  }

  public Long getUnitOfMeasureId() {
    return unitOfMeasureId;
  }

  public void setUnitOfMeasureId(Long unitOfMeasureId) {
    this.unitOfMeasureId = unitOfMeasureId;
  }

  @Override
  public String getSysName() {
    return IIObOrderLineDetails.SYS_NAME;
  }
}
