package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderPaymentDetailsGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderPaymentDetailsGeneralModelRep
        extends JpaRepository<IObOrderPaymentDetailsGeneralModel, Long> {
    IObOrderPaymentDetailsGeneralModel findOneByOrderCode(String code);
}
