package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObLocalPurchaseOrder;

public interface DObLocalPurchasingOrderRep
    extends DocumentObjectRep<DObLocalPurchaseOrder, DefaultUserCode> {}
