package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObPurchaseOrderAttachmentGeneralModel")
@EntityInterface(value = IDObPurchaseOrderGeneralModel.class)
public class DObPurchaseOrderAttachmentGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {

  private String objectTypeCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "attachmentTypeName")
  private LocalizedString attachmentTypeName;

  @Column(name = "attachmentTypeCode")
  private String attachmentTypeCode;

  @Column(name = "format")
  private String format;

  @Column(name = "attachmentName")
  private String attachmentName;

  public String getAttachmentName() {
    return attachmentName;
  }

  public void setAttachmentName(String attachmentName) {
    this.attachmentName = attachmentName;
  }

  public LocalizedString getAttachmentTypeName() {
    return attachmentTypeName;
  }

  public void setAttachmentTypeName(LocalizedString attachmentTypeName) {
    this.attachmentTypeName = attachmentTypeName;
  }

  public String getAttachmentTypeCode() {
    return attachmentTypeCode;
  }

  public void setAttachmentTypeCode(String attachmentTypeCode) {
    this.attachmentTypeCode = attachmentTypeCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
