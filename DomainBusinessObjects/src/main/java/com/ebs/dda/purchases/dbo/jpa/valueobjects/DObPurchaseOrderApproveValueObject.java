package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class DObPurchaseOrderApproveValueObject {

  private String purchaseOrderCode;
  private String notes;

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getNotes() {
    return notes;
  }

}
