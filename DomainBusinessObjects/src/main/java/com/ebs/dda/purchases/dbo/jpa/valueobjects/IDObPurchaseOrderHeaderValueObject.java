package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderHeaderValueObject {
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String PURCHASE_RESPONSIBLE_CODE = "purchaseResponsibleCode";
}
