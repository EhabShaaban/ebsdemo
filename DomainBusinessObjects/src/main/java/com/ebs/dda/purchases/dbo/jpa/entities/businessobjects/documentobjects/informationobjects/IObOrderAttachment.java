package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderAttachment;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderAttachment")
@EntityInterface(IIObOrderAttachment.class)
public class IObOrderAttachment extends DocumentLine implements Serializable {

  @Column(name = "robAttachmentId")
  private Long robAttachmentId;

  @Column(name = "lobAttachmentId")
  private Long lobAttachmentId;

  public Long getRobAttachmentId() {
    return robAttachmentId;
  }

  public void setRobAttachmentId(Long robAttachmentId) {
    this.robAttachmentId = robAttachmentId;
  }

  public Long getLobAttachmentId() {
    return lobAttachmentId;
  }

  public void setLobAttachmentId(Long lobAttachmentId) {
    this.lobAttachmentId = lobAttachmentId;
  }

  @Override
  public String getSysName() {
    return IIObOrderAttachment.SYS_NAME;
  }
}
