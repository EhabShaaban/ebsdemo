package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

public interface IIObOrderPlantEnterpriseData extends IIObEnterpriseData {

  String SYS_NAME = "Plant";

  byte PLANT_NAME_ATTR_CODE = 10;
  String PLANT_NAME_ATTR_NAME = "plant";
}
