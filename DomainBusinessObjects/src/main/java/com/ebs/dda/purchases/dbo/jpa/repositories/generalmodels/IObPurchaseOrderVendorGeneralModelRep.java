package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObPurchaseOrderVendorGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObPurchaseOrderVendorGeneralModelRep
    extends JpaRepository<IObPurchaseOrderVendorGeneralModel, Long> {
  IObPurchaseOrderVendorGeneralModel findOneByUserCode(String code);
  List<IObPurchaseOrderVendorGeneralModel> findAllByReferencePOCodeAndObjectTypeCode(String code, String objectTypeCode);
}
