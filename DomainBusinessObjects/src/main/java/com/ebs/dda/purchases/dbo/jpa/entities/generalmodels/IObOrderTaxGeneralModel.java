package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderTaxGeneralModel")
public class IObOrderTaxGeneralModel extends InformationObject {

  private String orderCode;
  private String objectTypeCode;
  private String code;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString name;

  private BigDecimal percentage;

  private BigDecimal amount;

  public String getOrderCode() {
    return orderCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getCode() {
    return code;
  }

  public LocalizedString getName() {
    return name;
  }

  public BigDecimal getPercentage() {
    return percentage;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
