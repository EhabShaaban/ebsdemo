package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class DObPurchaseOrderAssignMeToApproveValueObject {

  private String purchaseOrderCode;
  private String mainUser;
  private String controlPointCode;


  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getControlPointCode() {
    return controlPointCode;
  }

}
