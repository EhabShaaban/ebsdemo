package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import java.math.BigDecimal;

public class IObOrderLineDetailsValueObject implements IValueObject {
  String purchaseOrderCode;
  String itemCode;
  BigDecimal qtyInDn;

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public BigDecimal getQtyInDn() {
    return qtyInDn;
  }

  public void setQtyInDn(BigDecimal qtyInDn) {
    this.qtyInDn = qtyInDn;
  }
}
