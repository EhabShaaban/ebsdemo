package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderApproverGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderApproverGeneralModelRep
    extends JpaRepository<IObOrderApproverGeneralModel, Long> {

  List<IObOrderApproverGeneralModel> findByPurchaseOrderCodeAndApprovalCycleNumOrderByControlPointCode(
      String purchaseOrderCode, Long approvalCycleNumber);
}
