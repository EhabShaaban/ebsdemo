package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderPlantEnterpriseData;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("Plant")
@EntityInterface(IIObEnterpriseData.class)
public class IObOrderPlantEnterpriseData extends IObEnterpriseData {

  private static final long serialVersionUID = 1L;

  @Transient
  @EmbeddedAttribute
  private LocalizedString plant;

  public LocalizedString getPlant() {
    this.plant = super.getEnterpriseName();
    return plant;
  }

  public void setPlant(LocalizedString plant) {
    this.plant = plant;
  }

  @Override
  public String getSysName() {
    return IIObOrderPlantEnterpriseData.SYS_NAME;
  }
}
