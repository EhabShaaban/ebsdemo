package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderConsigneeEnterpriseData;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("Consignee")
@EntityInterface(IIObOrderConsigneeEnterpriseData.class)
public class IObOrderConsigneeEnterpriseData extends IObEnterpriseData {

  private static final long serialVersionUID = 1L;

  @Transient
  @EmbeddedAttribute
  private LocalizedString consignee;

  public LocalizedString getConsignee() {
    this.consignee = super.getEnterpriseName();
    return consignee;
  }

  public void setConsignee(LocalizedString consignee) {
    this.consignee = consignee;
  }
}
