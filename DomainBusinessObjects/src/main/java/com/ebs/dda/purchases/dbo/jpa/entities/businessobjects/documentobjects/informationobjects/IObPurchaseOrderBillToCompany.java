package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = "BILL_TO")
public class IObPurchaseOrderBillToCompany extends IObOrderCompany {
  public Long bankAccountId;

  public void setBankAccountId(Long backAccountId) {
    this.bankAccountId = backAccountId;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
