package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadSalesOrderForCollectionSpecification {

  public Specification<DObSalesOrderGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObSalesOrderGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObSalesOrderGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObSalesOrderGeneralModel> salesOrdersAllowedRemaining() {
    return (Specification<DObSalesOrderGeneralModel>)
        (root, query, criteriaBuilder) -> isSalesOrderRamainingAllowed(root, criteriaBuilder);
  }

  private Predicate isSalesOrderRamainingAllowed(Root<DObSalesOrderGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(
            root.get(IDObSalesOrderGeneralModel.REMAINING), 0);
  }

  public Specification<DObSalesOrderGeneralModel> salesOrdersInAllowedState() {
    return (Specification<DObSalesOrderGeneralModel>)
        (root, query, criteriaBuilder) -> isSalesOrderInAllowedState(root, criteriaBuilder);
  }

  private Predicate isSalesOrderInAllowedState(
      Root<DObSalesOrderGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isGoodsIssueActivated = whenStateIs(root, criteriaBuilder, "GoodsIssueActivated");
    Predicate isApproved = whenStateIs(root, criteriaBuilder, "Approved");
    return criteriaBuilder.or(isGoodsIssueActivated, isApproved);
  }

  private Predicate whenStateIs(
      Root<DObSalesOrderGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
