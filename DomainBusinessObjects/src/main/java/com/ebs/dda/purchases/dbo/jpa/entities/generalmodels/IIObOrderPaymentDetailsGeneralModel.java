package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObOrderPaymentDetailsGeneralModel {
    String PURCHASING_UNIT_NAME_ATTR_NAME = "purchaseUnitName";
    LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
            new LocalizedTextAttribute<>(PURCHASING_UNIT_NAME_ATTR_NAME);

    String PAYMENT_TERM_CODE = "paymentTermCode";
    String PAYMENT_TERM_NAME = "paymentTermName";
    String CURRENCY_ISO = "currencyIso";
}
