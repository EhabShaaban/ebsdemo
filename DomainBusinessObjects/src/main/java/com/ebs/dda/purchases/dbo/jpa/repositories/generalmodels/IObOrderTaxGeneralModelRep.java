package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderTaxGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderTaxGeneralModelRep extends JpaRepository<IObOrderTaxGeneralModel, Long> {

  List<IObOrderTaxGeneralModel> findByOrderCodeOrderByCodeAsc(String purchaseOrderCode);
}
