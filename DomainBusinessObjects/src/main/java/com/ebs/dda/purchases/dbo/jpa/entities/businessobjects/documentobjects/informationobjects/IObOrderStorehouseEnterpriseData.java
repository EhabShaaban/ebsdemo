package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObEnterpriseData;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderStorehouseEnterpriseData;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("Storehouse")
@EntityInterface(IIObEnterpriseData.class)
public class IObOrderStorehouseEnterpriseData extends IObEnterpriseData {

  private static final long serialVersionUID = 1L;

  @Transient
  @EmbeddedAttribute
  private LocalizedString storehouse;

  public LocalizedString getStorehouse() {
    this.storehouse = super.getEnterpriseName();
    return storehouse;
  }

  public void setStorehouse(LocalizedString storehouse) {
    this.storehouse = storehouse;
  }

  @Override
  public String getSysName() {
    return IIObOrderStorehouseEnterpriseData.SYS_NAME;
  }
}
