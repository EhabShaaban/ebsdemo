package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class DObPurchaseOrderAttachmentValueObject {
  private String purchaseOrderCode;
  private String attachmentTypeCode;
  private byte[] attachmentContent;
  private String attachmentName;
  private String attachmentFormat;

  public String getAttachmentFormat() {
    return attachmentFormat;
  }

  public void setAttachmentFormat(String attachmentFormat) {
    this.attachmentFormat = attachmentFormat;
  }

  public byte[] getAttachmentContent() {
    return attachmentContent;
  }

  public void setAttachmentContent(byte[] attachmentContent) {
    this.attachmentContent = attachmentContent;
  }

  public String getAttachmentName() {
    return attachmentName;
  }

  public void setAttachmentName(String attachmentName) {
    this.attachmentName = attachmentName;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public String getAttachmentTypeCode() {
    return attachmentTypeCode;
  }

  public void setAttachmentTypeCode(String attachmentTypeCode) {
    this.attachmentTypeCode = attachmentTypeCode;
  }
}
