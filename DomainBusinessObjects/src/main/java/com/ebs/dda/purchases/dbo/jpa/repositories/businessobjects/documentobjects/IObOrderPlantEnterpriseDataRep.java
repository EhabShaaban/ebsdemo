package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderPlantEnterpriseData;

public interface IObOrderPlantEnterpriseDataRep
    extends IObEnterpriseDataRep<IObOrderPlantEnterpriseData> {
  IObOrderPlantEnterpriseData findOneByRefInstanceId(Long id);
}
