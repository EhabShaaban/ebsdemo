package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadSalesInvoiceForCollectionSpecification {

  public Specification<DObSalesInvoiceGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObSalesInvoiceGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObSalesInvoiceGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObSalesInvoiceGeneralModel> salesInvoiceAllowedRemaining() {
    return (Specification<DObSalesInvoiceGeneralModel>)
            (root, query, criteriaBuilder) -> isSalesInvoiceRemainingAllowed(root, criteriaBuilder);
  }

  private Predicate isSalesInvoiceRemainingAllowed(Root<DObSalesInvoiceGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(
            root.get(IDObSalesInvoiceGeneralModel.REMAINING), 0);
  }

  public Specification<DObSalesInvoiceGeneralModel> salesInvoicesInAllowedState() {
    return (Specification<DObSalesInvoiceGeneralModel>)
        (root, query, criteriaBuilder) -> isSalesInvoiceInAllowedState(root, criteriaBuilder);
  }

  private Predicate isSalesInvoiceInAllowedState(
      Root<DObSalesInvoiceGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isOpened = whenStateIs(root, criteriaBuilder, "Opened");
    Predicate isActive = whenStateIs(root, criteriaBuilder, "Active");
    return criteriaBuilder.and(isOpened, isActive);
  }

  private Predicate whenStateIs(
      Root<DObSalesInvoiceGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
