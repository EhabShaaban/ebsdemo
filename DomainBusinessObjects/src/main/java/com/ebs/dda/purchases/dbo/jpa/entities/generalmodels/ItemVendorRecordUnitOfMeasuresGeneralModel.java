package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "itemvendorrecordunitofmeasuresgeneralmodel")
public class ItemVendorRecordUnitOfMeasuresGeneralModel {

  @Id
  private Long id;

  @Column(name = "alternativeunitofmeasurecode")
  private String alternativeUnitOfMeasureCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString alternativeUnitOfMeasureName;

  @Column(name = "conversionFactor")
  private Float conversionFactor;

  @Column(name = "price")
  private Double price;

  @Column(name = "vendorcode")
  private String vendorCode;

  @Column(name = "itemcode")
  private String itemCode;

  @Column(name = "itemcodeatvendor")
  private String itemCodeAtVendor;

  @Column(name = "purchaseunitcode")
  private String purchasingUnitCode;

  @Column(name = "itemvendorrecordid")
  private Long itemvendorRecordId;

  public void setAlternativeUnitOfMeasureCode(String alternativeUnitOfMeasureCode) {
    this.alternativeUnitOfMeasureCode = alternativeUnitOfMeasureCode;
  }

  public void setAlternativeUnitOfMeasureName(
      LocalizedString alternativeUnitOfMeasureName) {
    this.alternativeUnitOfMeasureName = alternativeUnitOfMeasureName;
  }

  public void setConversionFactor(Float conversionFactor) {
    this.conversionFactor = conversionFactor;
  }

  public String getAlternativeUnitOfMeasureCode() {
    return alternativeUnitOfMeasureCode;
  }

  public Long getId() {
    return id;
  }

  public Long getItemvendorRecordId() {
    return itemvendorRecordId;
  }

  public void setItemCodeAtVendor(String itemCodeAtVendor) {
    this.itemCodeAtVendor = itemCodeAtVendor;
  }
}
