package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;

import javax.persistence.*;

@Entity
@Table(name = "DObPurchaseOrderHeaderPDFData")
public class DObPurchaseOrderHeaderPDFData {

  @Id private Long id;

  private String purchaseOrderCode;
  
  private String purchaseResponsibleName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyAddress;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyCountryName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyCityName;

  private String companyPostalCode;

  private String companyTelephone;

  private String companyFax;

  private byte[] companyLogo;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString consigneeName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString consigneeAddress;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString consigneeCountryName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString consigneeCityName;

  private String consigneePostalCode;

  private String consigneeTelephone;

  private String consigneeFax;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString vendorName;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "modeoftransportname")
  private LocalizedString modeOfTransportName;

  private String incotermName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString dischargePortName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString paymentTermName;

  private String collectionDate;

  private Integer numberOfDaysAfterConfirmation;
  
  private String currencyISO;
  
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString shippingInstructionsText;
}
