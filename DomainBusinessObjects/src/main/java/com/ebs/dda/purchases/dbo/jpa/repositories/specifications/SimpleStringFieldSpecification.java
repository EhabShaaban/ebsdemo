package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class SimpleStringFieldSpecification<T extends BusinessObject>
        implements Specification<T> {

    private FilterCriteria criteria;

    public SimpleStringFieldSpecification(FilterCriteria filterCriteria) {
        this.criteria = filterCriteria;
    }

    @Override
    public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        if (criteria != null) {
            Path<String> expression = root.get(criteria.getKey());
            String filterValue = this.criteria.getValue();
            if (criteria.getOperation().equalsIgnoreCase("contains")) {
                return builder.like(builder.lower(expression),
                        "%" + filterValue.toLowerCase() + "%");
            } else if (criteria.getOperation().equalsIgnoreCase("equals")) {
                return builder.equal(expression, filterValue);
            }
        }
        return null;
    }
}
