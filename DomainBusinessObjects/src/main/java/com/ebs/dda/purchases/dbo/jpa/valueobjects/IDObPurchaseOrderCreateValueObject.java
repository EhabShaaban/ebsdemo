package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderCreateValueObject {
  String TYPE = "type";
  String REFERENCE_PURCHASE_ORDER_CODE = "referencePurchaseOrderCode";
  String BUSINESS_UNIT_CODE = "businessUnitCode";
  String VENDOR_CODE = "vendorCode";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
}
