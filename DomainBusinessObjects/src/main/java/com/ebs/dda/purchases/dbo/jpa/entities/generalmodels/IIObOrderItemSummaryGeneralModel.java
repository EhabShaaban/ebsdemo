package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

public interface IIObOrderItemSummaryGeneralModel {
    String ORDER_CODE = "orderCode";
    String OBJECT_TYPE_CODE = "objectTypeCode";
    String TOTAL_AMOUNT_BEFORE_TAXES = "totalAmountBeforeTaxes";
    String TOTAL_AMOUNT_AFTER_TAXES = "totalAmountAfterTaxes";
    String TOTAL_TAXES = "totalTaxes";
}
