package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import org.joda.time.DateTime;

public class DObPurchaseOrderMarkAsShippedValueObject {

  private String purchaseOrderCode;
  private DateTime shippingDate;

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public DateTime getShippingDate() {
    return shippingDate;
  }

  public void setShippingDate(DateTime shippingDate) {
    this.shippingDate = shippingDate;
  }
}
