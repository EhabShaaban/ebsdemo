package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IObOrderDocumentRequiredDocumentsValueObject {
  private String purchaseOrderCode;
  private List<IObPurchaseOrderDocumentValueObject> purchaseOrderDocuments;

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public List<IObPurchaseOrderDocumentValueObject> getPurchaseOrderDocuments() {
    return purchaseOrderDocuments;
  }

  public void setPurchaseOrderDocuments(
      List<IObPurchaseOrderDocumentValueObject> purchaseOrderDocuments) {
    this.purchaseOrderDocuments = purchaseOrderDocuments;
  }

  public boolean hasEmptyList() {
    return purchaseOrderDocuments.isEmpty();
  }

  public boolean hasDuplicate() {
    Set<String> appeared = new HashSet<>();
    for (IObPurchaseOrderDocumentValueObject item : purchaseOrderDocuments) {
      if (!appeared.add(item.getDocumentTypeCode())) {
        return true;
      }
    }
    return false;
  }
}
