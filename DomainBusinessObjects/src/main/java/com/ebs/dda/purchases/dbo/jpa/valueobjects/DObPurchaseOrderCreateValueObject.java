package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;

public class DObPurchaseOrderCreateValueObject {

  private OrderTypeEnum type;
  private String referencePurchaseOrderCode;
  private String businessUnitCode;
  private String vendorCode;
  private Long documentOwnerId;

  public OrderTypeEnum getType() {
    return type;
  }

  public void setType(OrderTypeEnum type) {
    this.type = type;
  }

  public String getReferencePurchaseOrderCode() {
    return referencePurchaseOrderCode;
  }

  public void setReferencePurchaseOrderCode(String referencePurchaseOrderCode) {
    this.referencePurchaseOrderCode = referencePurchaseOrderCode;
  }

  public String getBusinessUnitCode() {
    return businessUnitCode;
  }

  public void setBusinessUnitCode(String businessUnitCode) {
    this.businessUnitCode = businessUnitCode;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public void setVendorCode(String vendorCode) {
    this.vendorCode = vendorCode;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  public void setDocumentOwnerId(Long documentOwnerId) {
    this.documentOwnerId = documentOwnerId;
  }
}
