package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.approval.dbo.jpa.entities.enums.DocumentNameEnum.DocumentName;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/** @author Niveen Magdy */
@Entity
@DiscriminatorValue(OrderTypeEnum.Values.LOCAL_PO)
@EntityInterface(IDObPurchaseOrder.class)
public class DObLocalPurchaseOrder extends DObPurchaseOrder {
  public static final String SYS_NAME = "LocalPurchaseOrder";

  private static final long serialVersionUID = 1L;

  public DObLocalPurchaseOrder() {
    setDocument(DocumentName.PURCHASE_ORDER.toString());
  }

  @Override
  public String getSysName() {
    return SYS_NAME;
  }
}
