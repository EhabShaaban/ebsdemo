package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IDObPurchaseOrderGeneralModel {

  String PURCHASING_UNIT_NAME_ATTR_NAME = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASING_UNIT_NAME_ATTR_NAME);
  String SYS_NAME = "LocalPurchaseOrder";
  String IMPORT_SYS_NAME = "ImportPurchaseOrder";
  String LOCAL_SYS_NAME = "LocalPurchaseOrder";
  String SERVICE_SYS_NAME = "ServicePurchaseOrder";
  String PO_SYS_NAME = "PurchaseOrder";

  String VENDOR_CODE = "vendorCode";
  String VENDOR_NAME = "vendorName";
  String CODE = "userCode";
  String CURRENCY_ISO = "currencyISO";
  String CURRENCY_NAME = "currencyName";
  String PURCHASE_UNIT_NAME_EN = "purchaseUnitNameEn";
  String OBJECT_TYPE_CODE = "objectTypeCode";
  String COMPANY_CODE = "companyCode";
  String COMPANY_NAME = "companyName";
  String ORDER_TYPE_NAME_EN = "orderTypeNameEn";
  String REFERENCE_PO_CODE = "referencePOCode";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String BANK_NAME = "bankName";
  String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
  String DOCUMENT_OWNER_ID = "documentOwnerId";
  String CURRENT_STATES = "currentStates";
  String REF_DOCUMENT_CODE = "refDocumentCode";
  String REF_DOCUMENT_TYPE = "refDocumentType";
  String REF_DOCUMENT = "refDocument";
  String TYPE = "Type";


}
