package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

public interface IIObOrderTaxGeneralModel {
  String ORDER_CODE = "orderCode";
  String OBJECT_TYPE_CODE = "objectTypeCode";
  String CODE = "code";
  String NAME = "name";
  String PERCENTAGE = "percentage";
  String AMOUNT = "amount";
}
