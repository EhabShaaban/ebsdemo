package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IPurchaseOrderValueObject {
  String ORDER_TYPE_CODE = "orderTypeCode";
  String VENDOR_CODE = "vendorCode";
  String PURCHASE_UNIT_CODE = "purchaseUnitCode";
  String PURCHASE_RESPONSIBLE_CODE = "purchaseResponsibleCode";
}
