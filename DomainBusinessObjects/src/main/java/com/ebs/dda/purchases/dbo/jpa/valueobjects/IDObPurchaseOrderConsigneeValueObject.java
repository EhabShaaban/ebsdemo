package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderConsigneeValueObject {
  String CONSIGNEEID = "consigneeId";
  String PLANTID = "plantId";
  String STOREHOUSEID = "storehouseId";
  String CONSIGNEE_CODE = "consigneeCode";
  String PLANT_CODE = "plantCode";
  String STOREHOUSE_CODE = "storehouseCode";
}
