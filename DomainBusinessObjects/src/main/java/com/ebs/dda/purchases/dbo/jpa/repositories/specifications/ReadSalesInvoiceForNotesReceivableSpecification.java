package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadSalesInvoiceForNotesReceivableSpecification {

  public Specification<DObSalesInvoiceGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObSalesInvoiceGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObSalesInvoiceGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObSalesInvoiceGeneralModel> salesInvoicesAllowedBusinessUnit(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObSalesInvoiceGeneralModel>)
        (root, query, criteriaBuilder) ->
            isSalesInvoiceBusinessUnitAllowed(root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  public Specification<DObSalesInvoiceGeneralModel> salesInvoicesAllowedCompany(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObSalesInvoiceGeneralModel>)
        (root, query, criteriaBuilder) ->
            isSalesInvoiceCompanyAllowed(root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  public Specification<DObSalesInvoiceGeneralModel> salesInvoicesAllowedBusinessPartner(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObSalesInvoiceGeneralModel>)
        (root, query, criteriaBuilder) ->
            isSalesInvoiceBusinessPartnerAllowed(
                root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  public Specification<DObSalesInvoiceGeneralModel> salesInvoicesAllowedRemaining() {
    return (Specification<DObSalesInvoiceGeneralModel>)
        (root, query, criteriaBuilder) -> isSalesInvoiceRemainingAllowed(root, criteriaBuilder);
  }

  private Predicate isSalesInvoiceBusinessUnitAllowed(
      Root<DObSalesInvoiceGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
        root.get(IDObSalesInvoiceGeneralModel.PURCHASE_UNIT_CODE),
        notesReceivablesGeneralModel.getPurchaseUnitCode());
  }

  private Predicate isSalesInvoiceCompanyAllowed(
      Root<DObSalesInvoiceGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
        root.get(IDObSalesInvoiceGeneralModel.COMPANY_CODE),
        notesReceivablesGeneralModel.getCompanyCode());
  }

  private Predicate isSalesInvoiceBusinessPartnerAllowed(
      Root<DObSalesInvoiceGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
        root.get(IDObSalesInvoiceGeneralModel.CUSTOMER_CODE),
        notesReceivablesGeneralModel.getBusinessPartnerCode());
  }

  private Predicate isSalesInvoiceRemainingAllowed(
      Root<DObSalesInvoiceGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(root.get(IDObSalesInvoiceGeneralModel.REMAINING), 0);
  }

  public Specification<DObSalesInvoiceGeneralModel> salesInvoicesInAllowedState() {
    return (Specification<DObSalesInvoiceGeneralModel>)
        (root, query, criteriaBuilder) -> isSalesInvoiceInAllowedState(root, criteriaBuilder);
  }

  private Predicate isSalesInvoiceInAllowedState(
      Root<DObSalesInvoiceGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isActivated = whenStateIs(root, criteriaBuilder, "Active");
    return criteriaBuilder.or(isActivated);
  }

  private Predicate whenStateIs(
      Root<DObSalesInvoiceGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
