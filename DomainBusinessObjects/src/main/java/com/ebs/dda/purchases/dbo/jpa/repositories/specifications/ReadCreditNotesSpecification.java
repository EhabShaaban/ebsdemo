package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNoteGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNoteGeneralModel;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class ReadCreditNotesSpecification {

  public Specification<DObAccountingNoteGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObAccountingNoteGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObAccountingNoteGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObAccountingNoteGeneralModel> creditNotesAllowedRemaining() {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> isCreditNoteRamainingAllowed(root, criteriaBuilder);
  }

  private Predicate isCreditNoteRamainingAllowed(Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(
            root.get(IDObAccountingNoteGeneralModel.REMAINING), 0);
  }

  public Specification<DObAccountingNoteGeneralModel> accountingNoteIsCredit() {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> isCreditAccoutingNote(root, criteriaBuilder);
  }

  private Predicate isCreditAccoutingNote(
          Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.like(root.get(IDObAccountingNoteGeneralModel.OBJECT_TYPE_CODE), "%CREDIT_NOTE%");
  }

  public Specification<DObAccountingNoteGeneralModel> creditNoteInAllowedState() {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> isCreditNoteInAllowedState(root, criteriaBuilder);
  }

  public Specification<DObAccountingNoteGeneralModel> BusinessUnitIs(String businessUnitCode) {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> whenBusinessUnitIs(root, criteriaBuilder, businessUnitCode);
  }

  public Specification<DObAccountingNoteGeneralModel> BusinessPartnerIs(String businessPartnerCode) {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> whenBusinessPartnerIs(root, criteriaBuilder, businessPartnerCode);
  }

  private Predicate isCreditNoteInAllowedState(
      Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return whenStateIs(root, criteriaBuilder, "Posted");
  }

  private Predicate whenStateIs(
      Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
  private Predicate whenBusinessUnitIs(
      Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder, String businessUnitCode) {
    return criteriaBuilder.equal(
        root.get(IDObAccountingNoteGeneralModel.PURCHASE_UNIT_CODE), businessUnitCode);
  }
  private Predicate whenBusinessPartnerIs(
      Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder, String businessPartnerCode) {
    return criteriaBuilder.equal(
        root.get(IDObAccountingNoteGeneralModel.BUSINESS_PARTNER_CODE), businessPartnerCode);
  }
}
