package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

public interface IDObOrderDocumentGeneralModel {

  String OBJECT_TYPE_CODE = "objectTypeCode";
  String DOCUMENT_OWNER_NAME = "documentOwnerName";
  String CREATION_DATE = "creationDate";
  String CURRENT_STATES = "currentStates";
  String USER_CODE = "userCode";
  String CREATION_INFO = "creationInfo";
}
