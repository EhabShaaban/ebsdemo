package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class DObPurchaseOrderPaymentAndDeliveryValueObject {

  private String purchaseOrderCode;
  private String incoterm;
  private String currency;
  private String paymentTerms;
  private String shippingInstructions;
  private String containersType;
  private String collectionDate;
  private String modeOfTransport;
  private String loadingPort;
  private String dischargePort;
  private String containersNo;
  private String numberOfDays;

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public String getIncoterm() {
    return incoterm;
  }

  public void setIncoterm(String incoterm) {
    this.incoterm = incoterm;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getPaymentTerms() {
    return paymentTerms;
  }

  public void setPaymentTerms(String paymentTerms) {
    this.paymentTerms = paymentTerms;
  }

  public String getShippingInstructions() {
    return shippingInstructions;
  }

  public void setShippingInstructions(String shippingInstructions) {
    this.shippingInstructions = shippingInstructions;
  }

  public String getContainersNo() {
    return containersNo;
  }

  public void setContainersNo(String containersNo) {
    this.containersNo = containersNo;
  }

  public String getNumberOfDays() {
    return numberOfDays;
  }

  public void setNumberOfDays(String numberOfDays) {
    this.numberOfDays = numberOfDays;
  }

  public String getContainersType() {
    return containersType;
  }

  public void setContainersType(String containersType) {
    this.containersType = containersType;
  }

  public String getCollectionDate() {
    return collectionDate;
  }

  public void setCollectionDate(String collectionDate) {
    this.collectionDate = collectionDate;
  }

  public String getModeOfTransport() {
    return modeOfTransport;
  }

  public void setModeOfTransport(String modeOfTransport) {
    this.modeOfTransport = modeOfTransport;
  }

  public String getLoadingPort() {
    return loadingPort;
  }

  public void setLoadingPort(String loadingPort) {
    this.loadingPort = loadingPort;
  }

  public String getDischargePort() {
    return dischargePort;
  }

  public void setDischargePort(String dischargePort) {
    this.dischargePort = dischargePort;
  }

  public boolean isDeliveryDetailsEmpty() {
    boolean result =
        isEmpty(incoterm, shippingInstructions, modeOfTransport, loadingPort, dischargePort);
    result &= isCollectionDateEmpty(numberOfDays, collectionDate);
    return result;
  }

  public boolean isPaymentTermDetailsEmpty() {
    return isEmpty(paymentTerms, currency);
  }

  public boolean isContainerDetailsEmpty() {
    return isEmpty(containersType, containersNo);
  }

  private boolean isEmpty(String... attrs) {
    boolean all = true;
    for (String attr : attrs) {
      if (attr == null) {
        all &= true;
      } else {
        all &= (attr.trim().isEmpty());
      }
    }
    return all;
  }

  private boolean isCollectionDateEmpty(String numberOfDays, String collectionDate) {
    return isNullOrEmpty(numberOfDays) && isNullOrEmpty(collectionDate);
  }

  private boolean isNullOrEmpty(String field) {
    return (field == null || field.trim().isEmpty());
  }
}
