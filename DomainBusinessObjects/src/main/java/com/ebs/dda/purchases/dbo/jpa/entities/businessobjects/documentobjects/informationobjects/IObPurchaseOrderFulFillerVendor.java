package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "IObPurchaseOrderFulFillerVendor")
public class IObPurchaseOrderFulFillerVendor extends DocumentHeader implements Serializable {

    private Long vendorId;
    private Long referencePoId;

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public Long getReferencePoId() {
        return referencePoId;
    }

    public void setReferencePoId(Long referencePoId) {
        this.referencePoId = referencePoId;
    }

    @Override
    public String getSysName() {
        return null;
    }
}
