package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;

public interface IIObOrderApprovalCycles {
	String APPROVERS_NAME = "approvers";
	  CompositeAttribute<IObOrderApprover> approversAttr =
	      new CompositeAttribute<>( APPROVERS_NAME, IObOrderApprover.class, true);
}
