package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import org.joda.time.DateTime;

public class DobPurchaseOrderMarkAsClearedValueObject {

  private DateTime clearanceDateTime;

  public DateTime getClearanceDateTime() {
    return clearanceDateTime;
  }

  public void setClearanceDateTime(DateTime clearanceDateTime) {
    this.clearanceDateTime = clearanceDateTime;
  }
}
