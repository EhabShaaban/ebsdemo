package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis; // Created By Hossam Hassan @ 23/5

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObOrderConsigneeEnterpriseData extends IIObEnterpriseData {
  String CONSIGNEE_NAME_ATTR_NAME = "consignee";
  LocalizedTextAttribute<LocalizedString> consigneeAttr =
      new LocalizedTextAttribute<>(CONSIGNEE_NAME_ATTR_NAME);

  String CONSIGNEE_CODE = "consigneeCode";
  String PLANT_CODE = "plantCode";
  String STOREHOUSE_CODE = "storehouseCode";
}
