package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.QueryHint;
import java.util.List;

import static org.eclipse.persistence.config.QueryHints.JDBC_MAX_ROWS;

public interface DObPurchaseOrderGeneralModelRep
    extends StatefullBusinessObjectRep<DObPurchaseOrderGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObPurchaseOrderGeneralModel> {

  @Override
  Page<DObPurchaseOrderGeneralModel> findAll(
      Specification<DObPurchaseOrderGeneralModel> specification, Pageable page);

  @Query(
      value =
          "SELECT  e FROM DObPurchaseOrderGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) > :code "
              + "AND e.purchaseUnitNameEn IN :purchaseUnitNames "
              + "ORDER BY CAST(e.userCode AS BIGINT) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObPurchaseOrderGeneralModel findNextByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT e FROM DObPurchaseOrderGeneralModel e "
              + "WHERE CAST (e.userCode AS BIGINT) > :code "
              + "ORDER BY CAST (e.userCode AS BIGINT) ASC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObPurchaseOrderGeneralModel findNextByUserCodeUnconditionalRead(Long code);

  @Query(
      value =
          "SELECT  e FROM DObPurchaseOrderGeneralModel e "
              + "WHERE CAST(e.userCode AS BIGINT) < :code "
              + "AND e.purchaseUnitNameEn IN :purchaseUnitNames "
              + "ORDER BY CAST(e.userCode AS BIGINT) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObPurchaseOrderGeneralModel findPreviousByUserCode(
      @Param("code") Long code, @Param("purchaseUnitNames") List<String> purchaseUnitNames);

  @Query(
      value =
          "SELECT e FROM DObPurchaseOrderGeneralModel e "
              + "WHERE CAST (e.userCode AS BIGINT) < :code "
              + "ORDER BY CAST (e.userCode AS BIGINT) DESC")
  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObPurchaseOrderGeneralModel findPreviousByUserCodeUnconditionalRead(Long code);

  @Query(
      "SELECT COUNT(e) FROM DObPurchaseOrderGeneralModel e WHERE e.purchaseUnitNameEn IN :purchaseUnitNames")
  long countByPurchaseUnitNameEnIn(@Param("purchaseUnitNames") List<String> purchaseUnitName);
}
