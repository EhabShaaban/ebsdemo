package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderTotalAmountGeneralModel;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObPurchaseOrderTotalAmountGeneralModelRep
    extends StatefullBusinessObjectRep<DObPurchaseOrderTotalAmountGeneralModel, DefaultUserCode>,
        JpaSpecificationExecutor<DObPurchaseOrderTotalAmountGeneralModel> {}
