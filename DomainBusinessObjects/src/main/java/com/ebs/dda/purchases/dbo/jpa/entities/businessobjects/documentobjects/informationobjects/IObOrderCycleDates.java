/** */
package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderCycleDates;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.joda.time.DateTime;

/**
 * @auther: Eslam Mostafa
 * @date: Jan 10, 2018 4:58:18 PM
 */
@Entity
@Table(name = "IObOrderCycleDates")
@EntityInterface(IIObOrderCycleDates.class)
public class IObOrderCycleDates extends DocumentHeader implements Serializable {
  private static final long serialVersionUID = 1L;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(name = "actualPIRequestedDate")
  private DateTime actualPIRequestedDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime actualConfirmationDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime actualProductionFinishedDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime actualShippingDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime actualArrivalDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime actualClearanceDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime actualDeliveryCompleteDate;

  public DateTime getActualConfirmationDate() {
    return actualConfirmationDate;
  }

  public void setActualConfirmationDate(DateTime actualConfirmationDate) {
    this.actualConfirmationDate = actualConfirmationDate;
  }

  public DateTime getActualPIRequestedDate() {
    return actualPIRequestedDate;
  }

  public void setActualPIRequestedDate(DateTime actualPIRequestedDate) {
    this.actualPIRequestedDate = actualPIRequestedDate;
  }

  public DateTime getActualProductionFinishedDate() {
    return actualProductionFinishedDate;
  }

  public void setActualProductionFinishedDate(DateTime actualProductionFinishedDate) {
    this.actualProductionFinishedDate = actualProductionFinishedDate;
  }

  public DateTime getActualShippingDate() {
    return actualShippingDate;
  }

  public void setActualShippingDate(DateTime actualShippingDate) {
    this.actualShippingDate = actualShippingDate;
  }

  public DateTime getActualArrivalDate() {
    return actualArrivalDate;
  }

  public void setActualArrivalDate(DateTime actualArrivalDate) {
    this.actualArrivalDate = actualArrivalDate;
  }

  public DateTime getActualClearanceDate() {
    return actualClearanceDate;
  }

  public void setActualClearanceDate(DateTime actualClearanceDate) {
    this.actualClearanceDate = actualClearanceDate;
  }

  public DateTime getActualDeliveryCompleteDate() {
    return actualDeliveryCompleteDate;
  }

  public void setActualDeliveryCompleteDate(DateTime actualDeliveryCompleteDate) {
    this.actualDeliveryCompleteDate = actualDeliveryCompleteDate;
  }

  @Override
  public String getSysName() {
    return IIObOrderCycleDates.SYS_NAME;
  }
}
