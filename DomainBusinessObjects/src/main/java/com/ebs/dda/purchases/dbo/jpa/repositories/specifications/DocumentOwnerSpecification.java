package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoiceGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class DocumentOwnerSpecification<T extends BusinessObject> implements Specification<T> {

  private String loggedInUser;
  private String key;

  public DocumentOwnerSpecification(String loggedInUser) {
    this.loggedInUser = loggedInUser;
    this.key = IDObSalesInvoiceGeneralModel.DOCUMENT_OWNER_USER_NAME;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    if (loggedInUser != null) {
      Path<String> expression = root.get(this.key);
      return builder.equal(expression, loggedInUser);
    }
    return null;
  }
}
