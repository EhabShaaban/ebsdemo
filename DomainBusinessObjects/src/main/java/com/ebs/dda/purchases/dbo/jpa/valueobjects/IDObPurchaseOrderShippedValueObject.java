package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderShippedValueObject {
  String SHIPPING_DATE_TIME = "shippingDate";
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
}
