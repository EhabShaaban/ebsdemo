package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderArrivalValueObject {

  String ARRIVAL_DATE_TIME = "arrivalDateTime";
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
}
