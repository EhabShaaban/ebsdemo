package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class ConditionalFilterSpecification<T extends BusinessObject> implements Specification<T> {

  private String loggedInUser;
  private String key;

  public ConditionalFilterSpecification(String loggedInUser, String key) {
    this.loggedInUser = loggedInUser;
    this.key = key;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    if (loggedInUser != null) {
      Path<String> expression = root.get(this.key);
      return builder.equal(expression, loggedInUser);
    }
    return null;
  }
}
