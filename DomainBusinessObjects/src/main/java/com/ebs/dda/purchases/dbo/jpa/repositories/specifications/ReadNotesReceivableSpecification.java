package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObNotesReceivablesGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadNotesReceivableSpecification {

  public Specification<DObNotesReceivablesGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObNotesReceivablesGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObNotesReceivablesGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObNotesReceivablesGeneralModel> notesReceivablesAllowedBusinessUnit(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObNotesReceivablesGeneralModel>)
        (root, query, criteriaBuilder) ->
            isNotesReceivableBusinessUnitAllowed(
                root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  private Predicate isNotesReceivableBusinessUnitAllowed(
          Root<DObNotesReceivablesGeneralModel> root,
          CriteriaBuilder criteriaBuilder,
          DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
            root.get(IDObNotesReceivablesGeneralModel.PURCHASE_UNIT_CODE),
            notesReceivablesGeneralModel.getPurchaseUnitCode());
  }

  public Specification<DObNotesReceivablesGeneralModel> notesReceivablesAllowedCompany(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObNotesReceivablesGeneralModel>)
        (root, query, criteriaBuilder) ->
            isNotesReceivableCompanyAllowed(root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  private Predicate isNotesReceivableCompanyAllowed(
          Root<DObNotesReceivablesGeneralModel> root,
          CriteriaBuilder criteriaBuilder,
          DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
            root.get(IDObNotesReceivablesGeneralModel.COMPANY_CODE),
            notesReceivablesGeneralModel.getCompanyCode());
  }

  public Specification<DObNotesReceivablesGeneralModel> notesReceivablesAllowedBusinessPartner(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObNotesReceivablesGeneralModel>)
        (root, query, criteriaBuilder) ->
            isNotesReceivableBusinessPartnerAllowed(
                root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  private Predicate isNotesReceivableBusinessPartnerAllowed(
          Root<DObNotesReceivablesGeneralModel> root,
          CriteriaBuilder criteriaBuilder,
          DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
            root.get(IDObNotesReceivablesGeneralModel.BUSINESS_PARTNER_CODE),
            notesReceivablesGeneralModel.getBusinessPartnerCode());
  }

  public Specification<DObNotesReceivablesGeneralModel> notesReceivablesAllowedRemaining() {
    return (Specification<DObNotesReceivablesGeneralModel>)
        (root, query, criteriaBuilder) -> isNotesReceivableRemainingAllowed(root, criteriaBuilder);
  }

  private Predicate isNotesReceivableRemainingAllowed(
      Root<DObNotesReceivablesGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(root.get(IDObNotesReceivablesGeneralModel.REMAINING), 0);
  }

  public Specification<DObNotesReceivablesGeneralModel> notesReceivablesInAllowedState() {
    return (Specification<DObNotesReceivablesGeneralModel>)
        (root, query, criteriaBuilder) -> isNotesReceivableInAllowedState(root, criteriaBuilder);
  }

  private Predicate isNotesReceivableInAllowedState(
      Root<DObNotesReceivablesGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return whenStateIs(root, criteriaBuilder, "Active");
  }

  private Predicate whenStateIs(
      Root<DObNotesReceivablesGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
