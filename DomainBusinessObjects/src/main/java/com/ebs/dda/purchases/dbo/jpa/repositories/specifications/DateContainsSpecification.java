package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.joda.time.DateTime;
import org.springframework.data.jpa.domain.Specification;

public class DateContainsSpecification<T extends BusinessObject> implements Specification<T> {

  private FilterCriteria criteria;

  public DateContainsSpecification(FilterCriteria filterCriteria) {
    this.criteria = filterCriteria;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    if (criteria != null) {

      if (criteria.getOperation().equalsIgnoreCase("equals")) {
        Expression<DateTime> expression = root.get(criteria.getKey()).as(DateTime.class);
        DateTime dateTime = new DateTime(Long.parseLong(criteria.getValue()));
        return builder.equal(expression, dateTime);
      } else if (criteria.getOperation().equalsIgnoreCase("contains")) {
        Expression<DateTime> expression = root.get(criteria.getKey()).as(DateTime.class);
        DateTime dateTime = new DateTime(Long.parseLong(criteria.getValue()));
        DateTime endingDate = dateTime.plusDays(1);

        Predicate greaterThanPredicate = builder.greaterThan(expression, dateTime);
        Predicate equalPredicate = builder.equal(expression, dateTime);
        Predicate greaterThanOrEqual = builder.or(greaterThanPredicate, equalPredicate);
        Predicate lessThanPredicate = builder.lessThan(expression, endingDate);
        return builder.and(greaterThanOrEqual, lessThanPredicate);
      } else if (criteria.getOperation().equalsIgnoreCase("before")) {
        Expression<DateTime> expression = root.get(criteria.getKey()).as(DateTime.class);
        DateTime dateTime = new DateTime(Long.parseLong(criteria.getValue()));

        Predicate lessThanPredicate = builder.lessThan(expression, dateTime);
        Predicate equalPredicate = builder.equal(expression, dateTime);
        return builder.or(lessThanPredicate, equalPredicate);

      } else if (criteria.getOperation().equalsIgnoreCase("after")) {
        Expression<DateTime> expression = root.get(criteria.getKey()).as(DateTime.class);
        DateTime dateTime = new DateTime(Long.parseLong(criteria.getValue()));

        Predicate greaterThanPredicate = builder.greaterThan(expression, dateTime);
        Predicate equalPredicate = builder.equal(expression, dateTime);
        return builder.or(greaterThanPredicate, equalPredicate);
      }
    }
    return null;
  }
}
