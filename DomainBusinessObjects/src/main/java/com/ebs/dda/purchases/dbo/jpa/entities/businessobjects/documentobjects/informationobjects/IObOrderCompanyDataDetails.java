package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderCompanyDataDetails;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/** @author Yara Ameen */
@Entity
@Table(name = "IObOrderCompanyDataDetails")
@EntityInterface(IIObOrderCompanyDataDetails.class)
public class IObOrderCompanyDataDetails extends DocumentHeader implements Serializable {

  private static final long serialVersionUID = 1L;

  private String bankAccount;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString bankName;

  private String bankAddress;

  @Column(name = "payerBankDetailsId")
  private Long payerBankDetails;

  public Long getPayerBankDetails() {
    return payerBankDetails;
  }

  public void setPayerBankDetails(Long payerBankDetails) {
    this.payerBankDetails = payerBankDetails;
  }

  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount;
  }

  public LocalizedString getBankName() {
    if (this.bankName != null) return bankName;
    else return new LocalizedString();
  }

  public void setBankName(LocalizedString bankName) {
    this.bankName = bankName;
  }

  public String getBankAddress() {
    return bankAddress;
  }

  public void setBankAddress(String bankAddress) {
    this.bankAddress = bankAddress;
  }

  @Override
  public String getSysName() {
    return IIObOrderCompanyDataDetails.SYS_NAME;
  }
}
