package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.jpa.order.IObOrderDocumentRequiredDocumentsGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderDocumentRequiredDocumentsGeneralModelRep
    extends JpaRepository<IObOrderDocumentRequiredDocumentsGeneralModel, Long> {
  List<IObOrderDocumentRequiredDocumentsGeneralModel> findByUserCodeOrderByAttachmentCode(
      String code);

  IObOrderDocumentRequiredDocumentsGeneralModel findByUserCodeAndAttachmentCode(
      String code, String attachmentCode);

  List<IObOrderDocumentRequiredDocumentsGeneralModel> findByUserCodeOrderById(
      String purchaseOrderCode);
}
