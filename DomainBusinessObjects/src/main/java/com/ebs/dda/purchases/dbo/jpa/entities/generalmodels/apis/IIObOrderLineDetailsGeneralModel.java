package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.apis;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesGeneralModel;

public interface IIObOrderLineDetailsGeneralModel {

  String SYS_NAME = "OrderLineGeneralModel";

  CompositeAttribute<IObOrderLineDetailsQuantitiesGeneralModel>
      iObOrderLineDetailsQuantitiesGeneralModel =
          new CompositeAttribute(
              "iObOrderLineDetailsQuantitiesGeneralModel",
              IObOrderLineDetailsQuantitiesGeneralModel.class,
              true);
}
