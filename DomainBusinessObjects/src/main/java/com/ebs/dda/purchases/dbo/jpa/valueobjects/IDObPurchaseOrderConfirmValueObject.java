package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderConfirmValueObject {

  String CONFIRMATION_DATE_TIME = "confirmationDateTime";
}
