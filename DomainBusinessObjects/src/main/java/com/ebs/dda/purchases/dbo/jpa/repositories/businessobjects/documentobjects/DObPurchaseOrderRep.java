package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/** @author Yara Ameen */
public interface DObPurchaseOrderRep extends DocumentObjectRep<DObPurchaseOrder, DefaultUserCode> {
  DObPurchaseOrder findOneByUserCode(String purchaseOrderCode);

  @Query("SELECT O.id FROM DObPurchaseOrder O WHERE O.userCode = :userCode")
  Long findOrderIdByUserCode(@Param("userCode") String userCode);

}
