package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderLineDetailsSummaryGeneralModel")
@EntityInterface(IObOrderLineDetailsSummaryGeneralModel.class)
public class IObOrderLineDetailsSummaryGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {

  private String objectTypeCode;

  private BigDecimal totalAmount;
  private BigDecimal totalDiscount;

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public BigDecimal getTotalAmount() {
    return totalAmount == null ? BigDecimal.ZERO : totalAmount;
  }

  public BigDecimal getTotalDiscount() {
    return totalDiscount == null ? BigDecimal.ZERO : totalDiscount;
  }

  public BigDecimal getTotalAmountAfterDiscount() {
    return getTotalAmount().subtract(getTotalDiscount());
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
