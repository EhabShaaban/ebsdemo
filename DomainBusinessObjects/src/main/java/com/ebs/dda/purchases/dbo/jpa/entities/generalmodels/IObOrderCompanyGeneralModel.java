package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderCompanyGeneralModel")
@EntityInterface(value = IDObPurchaseOrderGeneralModel.class)
public class IObOrderCompanyGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString bankName;

  private String bankAccountNumber;

  private Long bankAccountId;
  private Long companyId;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String companyCode;
  private Long purchaseUnitId;
  private String purchaseUnitCode;

  private String bankCode;

  private String objectTypeCode;

  private String partyRole;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String currencyCode;
  private String currencyIso;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  public LocalizedString getBankName() {
    return bankName;
  }

  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public String getBankCode() {
    return bankCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public Long getBankAccountId() {
    return bankAccountId;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getPartyRole() {
    return partyRole;
  }

  public Long getCompanyId() {
    return companyId;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
