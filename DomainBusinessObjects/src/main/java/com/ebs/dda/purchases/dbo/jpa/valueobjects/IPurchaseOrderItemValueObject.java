package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IPurchaseOrderItemValueObject {

  String ITEM_CODE = "itemCode";
  String QUANTITY_IN_DN = "qtyInDn";

  //	Item quantities Attributes names
  String QUANTITY = "quantity";
  String ORDER_UNIT_SYMBOL = "orderUnitSymbol";
  String PRICE = "price";
  String DISCOUNT_PERCENTAGE = "discountpercentage";
}
