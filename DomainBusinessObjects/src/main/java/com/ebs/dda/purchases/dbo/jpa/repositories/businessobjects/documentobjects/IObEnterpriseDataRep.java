package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObEnterpriseData;

/** @author Yara Ameen */
public interface IObEnterpriseDataRep<T extends IObEnterpriseData>
    extends InformationObjectRep<T> {}
