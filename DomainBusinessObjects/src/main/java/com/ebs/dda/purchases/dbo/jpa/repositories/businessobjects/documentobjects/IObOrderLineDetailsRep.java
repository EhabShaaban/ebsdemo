package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;

public interface IObOrderLineDetailsRep extends InformationObjectRep<IObOrderLineDetails> {

  IObOrderLineDetails findOneById(Long id);

}
