package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderPurchasingResponsibleEnterpriseData;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("PurchasingResponsible")
@EntityInterface(IIObOrderPurchasingResponsibleEnterpriseData.class)
public class IObOrderPurchasingResponsibleEnterpriseData extends IObEnterpriseData {

  private static final long serialVersionUID = 1L;

  @Transient
  @EmbeddedAttribute
  private LocalizedString purchasingResponsible;

  public LocalizedString getPurchasingResponsible() {
    this.purchasingResponsible = super.getEnterpriseName();
    return purchasingResponsible;
  }

  public void setPurchasingResponsible(LocalizedString purchasingResponsible) {
    this.purchasingResponsible = purchasingResponsible;
  }
}
