package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderPaymentDetails")
public class IObOrderPaymentDetails extends DocumentHeader {

    private Long currencyId;
    private Long paymentTermId;

    public void setCurrencyId(Long currencyId) {
        this.currencyId = currencyId;
    }

    public void setPaymentTermId(Long paymentTermId) {
        this.paymentTermId = paymentTermId;
    }

    @Override
    public String getSysName() {
        return null;
    }
}
