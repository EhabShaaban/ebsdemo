package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderStorehouseEnterpriseData;

public interface IObOrderStorehouseEnterpriseDataRep
    extends IObEnterpriseDataRep<IObOrderStorehouseEnterpriseData> {
  IObOrderStorehouseEnterpriseData findOneByRefInstanceId(Long id);
}
