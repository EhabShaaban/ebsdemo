package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IIObServicePurchaseOrderPaymentDetailsValueObject {

  String PAYMENT_TERM_CODE = "paymentTermCode";
  String CURRENY_CODE = "currencyCode";
}
