package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadSalesReturnForGoodsReceiptSpecification {

  public Specification<DObSalesReturnOrderGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObSalesReturnOrderGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObSalesReturnOrderGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                      String.class,
                      root.<String>get("purchaseUnitName"),
                      criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObSalesReturnOrderGeneralModel> salesReturnsInAllowedState() {
    return (Specification<DObSalesReturnOrderGeneralModel>)
            (root, query, criteriaBuilder) -> isSalesReturnInAllowedState(root, criteriaBuilder);
  }

  private Predicate isSalesReturnInAllowedState(
          Root<DObSalesReturnOrderGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isShipped = whenStateIs(root, criteriaBuilder, "Shipped");
    Predicate isCreditNoteActivated = whenStateIs(root, criteriaBuilder, "CreditNoteActivated");
    return criteriaBuilder.or(isShipped, isCreditNoteActivated);
  }

  private Predicate whenStateIs(
          Root<DObSalesReturnOrderGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
            root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
