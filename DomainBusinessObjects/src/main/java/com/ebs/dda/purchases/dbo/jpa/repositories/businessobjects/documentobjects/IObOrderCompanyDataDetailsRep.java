package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCompanyDataDetails;

/** @author Yara Ameen */
public interface IObOrderCompanyDataDetailsRep
    extends InformationObjectRep<IObOrderCompanyDataDetails> {

  IObOrderCompanyDataDetails findOneByRefInstanceId(Long refInstanceId);
}
