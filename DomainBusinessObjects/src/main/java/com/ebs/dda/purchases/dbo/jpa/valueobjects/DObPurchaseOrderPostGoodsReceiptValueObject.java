package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class DObPurchaseOrderPostGoodsReceiptValueObject implements IValueObject {

  private String goodsReceiptCode;

  public String getGoodsReceiptCode() {
    return goodsReceiptCode;
  }

  public void setGoodsReceiptCode(String goodsReceiptCode) {
    this.goodsReceiptCode = goodsReceiptCode;
  }
}
