package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.approval.dbo.jpa.entities.enums.DocumentNameEnum.DocumentName;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/** @author Yara Ameen */
@Entity
@DiscriminatorValue(OrderTypeEnum.Values.IMPORT_PO)
@EntityInterface(IDObPurchaseOrder.class)
public class DObImportPurchaseOrder extends DObPurchaseOrder {
  public static final String SYS_NAME = "ImportPurchaseOrder";

  private static final long serialVersionUID = 1L;

  public DObImportPurchaseOrder() {
    setDocument(DocumentName.PURCHASE_ORDER.toString());
  }

  @Override
  public String getSysName() {
    return SYS_NAME;
  }
}
