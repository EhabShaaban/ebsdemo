package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderHeaderPDFData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObPurchaseOrderHeaderPDFDataRep
    extends JpaRepository<DObPurchaseOrderHeaderPDFData, Long> {
  DObPurchaseOrderHeaderPDFData findOneByPurchaseOrderCode(String purchaseOrderCode);
}
