package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCompanyEnterpriseData;

public interface IObOrderCompanyEnterpriseDataRep
    extends IObEnterpriseDataRep<IObOrderCompanyEnterpriseData> {

  IObOrderCompanyEnterpriseData findIObOrderCompanyEnterpriseDataByRefInstanceId(
      Long refInstanceId);
}
