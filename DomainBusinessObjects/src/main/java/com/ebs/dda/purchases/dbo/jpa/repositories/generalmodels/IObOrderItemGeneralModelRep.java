package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemGeneralModel;

import java.util.List;

public interface IObOrderItemGeneralModelRep
    extends InformationObjectRep<IObOrderItemGeneralModel> {

  List<IObOrderItemGeneralModel> findByOrderCodeOrderByIdDesc(String purchaseOrderCode);

  List<IObOrderItemGeneralModel> findAllByOrderCodeAndItemCode(
      String purchaseOrderCode, String itemCode);

  IObOrderItemGeneralModel findAllByOrderCodeAndItemCodeAndOrderUnitCode(
      String purchaseOrderCode, String itemCode, String orderUnitCode);
}
