package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class IObServicePurchaseOrderPaymentDetailsValueObject {
  private String paymentTermCode;
  private String currencyCode;
  private String purchaseOrderCode;

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }
}
