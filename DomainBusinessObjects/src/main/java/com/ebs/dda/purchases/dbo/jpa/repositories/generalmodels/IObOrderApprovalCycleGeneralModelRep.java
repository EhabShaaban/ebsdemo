package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderApprovalCycleGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderApprovalCycleGeneralModelRep
    extends JpaRepository<IObOrderApprovalCycleGeneralModel, Long> {

  List<IObOrderApprovalCycleGeneralModel> findByPurchaseOrderCodeOrderByApprovalCycleNumDesc(
      String purchaseOrderCode);
}
