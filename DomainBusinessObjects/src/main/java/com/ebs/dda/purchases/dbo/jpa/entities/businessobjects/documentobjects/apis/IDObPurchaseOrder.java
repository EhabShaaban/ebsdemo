package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis;

import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.*;

public interface IDObPurchaseOrder {
  String SYS_NAME = "PurchaseOrder";

  String CONTAINERS_ATTR_NAME = "containers";
  CompositeAttribute<IObOrderContainersDetails> containersAttr =
      new CompositeAttribute<>(CONTAINERS_ATTR_NAME, IObOrderContainersDetails.class, true);

  String PAYMENT_TERMS_ATTR_NAME = "paymentTerms";
  CompositeAttribute<IObOrderPaymentTermsDetails> paymentTermsAttr =
      new CompositeAttribute<>(PAYMENT_TERMS_ATTR_NAME, IObOrderPaymentTermsDetails.class, false);

  String PAYMENT_DETAILS_ATTR_NAME = "paymentDetails";
  CompositeAttribute<IObOrderPaymentDetails> paymentDetailsAttr =
      new CompositeAttribute<>(PAYMENT_DETAILS_ATTR_NAME, IObOrderPaymentDetails.class, false);

  String DELIVERY_ATTR_NAME = "delivery";

  CompositeAttribute<IObOrderDeliveryDetails> deliveryAttr =
      new CompositeAttribute<>(DELIVERY_ATTR_NAME, IObOrderDeliveryDetails.class, false);

  String COMPANY_DATA_ATTR_NAME = "payerBankDetails";
  CompositeAttribute<IObOrderCompanyDataDetails> payerBankDetailsAttr =
      new CompositeAttribute<>(COMPANY_DATA_ATTR_NAME, IObOrderCompanyDataDetails.class, false);

  String COMPANY_ENTERPRISE_DATA_ATTR_NAME = "companyData";
  CompositeAttribute<IObOrderCompanyEnterpriseData> companyDataAttr =
      new CompositeAttribute<>(
          COMPANY_ENTERPRISE_DATA_ATTR_NAME, IObOrderCompanyEnterpriseData.class, false);

  String CONSIGNEE_ENTERPRISE_DATA_ATTR_NAME = "consigneeData";
  CompositeAttribute<IObOrderConsigneeEnterpriseData> consigneeDataAttr =
      new CompositeAttribute<>(
          CONSIGNEE_ENTERPRISE_DATA_ATTR_NAME, IObOrderConsigneeEnterpriseData.class, false);

  String STOREHOUSE_ENTERPRISE_DATA_ATTR_NAME = "storehouseData";
  CompositeAttribute<IObOrderStorehouseEnterpriseData> storehouseDataAttr =
      new CompositeAttribute<>(
          STOREHOUSE_ENTERPRISE_DATA_ATTR_NAME, IObOrderStorehouseEnterpriseData.class, false);

  String PLANT_ENTERPRISE_DATA_ATTR_NAME = "plantData";
  CompositeAttribute<IObOrderPlantEnterpriseData> plantDataAttr =
      new CompositeAttribute<>(
          PLANT_ENTERPRISE_DATA_ATTR_NAME, IObOrderPlantEnterpriseData.class, false);

  String ORDER_CYCLE_DATES_ATTR_NAME = "cycleDates";
  CompositeAttribute<IObOrderCycleDates> cycleDatesAttr =
      new CompositeAttribute<>(ORDER_CYCLE_DATES_ATTR_NAME, IObOrderCycleDates.class, false);

  String LINE_ATTR_NAME = "line";
  CompositeAttribute<IObOrderLineDetails> lineAttr =
      new CompositeAttribute<>(LINE_ATTR_NAME, IObOrderLineDetails.class, true);

  String ATTACHMENT_NAME = "attachment";
  CompositeAttribute<IObOrderAttachment> attachmentDataAttr =
      new CompositeAttribute<>(ATTACHMENT_NAME, IObOrderAttachment.class, true);

  String PURCHASING_UNIT_NAME = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASING_UNIT_NAME);

  String APPROVAL_CYCLES_NAME = "approvalCycles";
  CompositeAttribute<IObOrderApprovalCycle> approvalCyclesAttr =
      new CompositeAttribute<>(APPROVAL_CYCLES_NAME, IObOrderApprovalCycle.class, true);

  String VENDOR_ATTR_NAME = "vendor";
  CompositeAttribute<IObPurchaseOrderFulFillerVendor> vendorAttr =
      new CompositeAttribute<>(VENDOR_ATTR_NAME, IObPurchaseOrderFulFillerVendor.class, false);

  String COMPANY_ATTR_NAME = "company";
  CompositeAttribute<IObPurchaseOrderBillToCompany> companyAttr =
      new CompositeAttribute<>(COMPANY_ATTR_NAME, IObPurchaseOrderBillToCompany.class, false);

  String TAX_ATTR_NAME = "tax";
  CompositeAttribute<IObOrderTax> taxAttr =
      new CompositeAttribute<>(TAX_ATTR_NAME, IObOrderTax.class, true);

  String PURCHASE_ORDER_ITEMS_ATTR_NAME = "ItemsData";
  CompositeAttribute<IObOrderItem> purchaseOrderItemsAttr =
      new CompositeAttribute<>(PURCHASE_ORDER_ITEMS_ATTR_NAME, IObOrderItem.class, true);
}
