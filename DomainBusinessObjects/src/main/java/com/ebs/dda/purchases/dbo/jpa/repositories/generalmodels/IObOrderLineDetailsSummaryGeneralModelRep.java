package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsSummaryGeneralModel;

public interface IObOrderLineDetailsSummaryGeneralModelRep
    extends StatefullBusinessObjectRep<IObOrderLineDetailsSummaryGeneralModel, DefaultUserCode> {

  IObOrderLineDetailsSummaryGeneralModel findOneByUserCode(String purchaseOrderCode);
}
