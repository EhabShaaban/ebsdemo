package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

@Entity
@EntityInterface(IDObPurchaseOrderGeneralModel.class)
@Table(name = "IObPurchaseOrderVendorGeneralModel")
public class IObPurchaseOrderVendorGeneralModel extends CodedBusinessObject<DefaultUserCode> {

  private Long vendorId;

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String vendorCode;

  private String objectTypeCode;
  private String purchaseUnitNameEn;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString vendorName;

  private Long referencePOId;

  private String referencePOCode;

  public Long getVendorId() {
    return vendorId;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public String getReferencePOCode() {
    return referencePOCode;
  }

  public Long getReferencePOId() {
    return referencePOId;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
