package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadNotesReceivableForCollectionSpecification {

  public Specification<DObNotesReceivablesGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObNotesReceivablesGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObNotesReceivablesGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObNotesReceivablesGeneralModel> notesReceivablesInAllowedState() {
    return (Specification<DObNotesReceivablesGeneralModel>)
        (root, query, criteriaBuilder) -> isNotesReceivablesInAllowedState(root, criteriaBuilder);
  }

  private Predicate isNotesReceivablesInAllowedState(
      Root<DObNotesReceivablesGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isActive = whenStateIs(root, criteriaBuilder, "Active");
    return criteriaBuilder.and(isActive);
  }

  private Predicate whenStateIs(
      Root<DObNotesReceivablesGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
