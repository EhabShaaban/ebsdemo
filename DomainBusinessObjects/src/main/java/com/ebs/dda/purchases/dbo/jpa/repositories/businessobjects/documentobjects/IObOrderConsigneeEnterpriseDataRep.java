package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderConsigneeEnterpriseData;

public interface IObOrderConsigneeEnterpriseDataRep
    extends IObEnterpriseDataRep<IObOrderConsigneeEnterpriseData> {
  IObOrderConsigneeEnterpriseData findOneByRefInstanceId(Long id);
}
