package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderConsigneeGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DObPurchaseOrderConsigneeGeneralModelRep
    extends JpaRepository<DObPurchaseOrderConsigneeGeneralModel, Long> , JpaSpecificationExecutor<DObPurchaseOrderConsigneeGeneralModel> {

  DObPurchaseOrderConsigneeGeneralModel findByUserCode(String code);
}
