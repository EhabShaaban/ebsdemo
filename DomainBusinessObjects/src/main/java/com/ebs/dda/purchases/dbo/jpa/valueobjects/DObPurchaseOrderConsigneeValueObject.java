package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class DObPurchaseOrderConsigneeValueObject {

  private String purchaseOrderCode;

  private String consigneeCode;
  private String plantCode;
  private String storehouseCode;

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }

  public String getConsigneeCode() {
    return consigneeCode;
  }

  public void setConsigneeCode(String consigneeCode) {
    this.consigneeCode = consigneeCode;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public void setPlantCode(String plantCode) {
    this.plantCode = plantCode;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public void setStorehouseCode(String storehouseCode) {
    this.storehouseCode = storehouseCode;
  }
}
