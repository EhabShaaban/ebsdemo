package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderLineDetails;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/** @author Hossam Hassan, Zyad Ghorab */
@Entity
@Table(name = "ioborderlinedetailsquantities")
@EntityInterface(IIObOrderLineDetails.class)
public class IObOrderLineDetailsQuantities extends DocumentLine {

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString orderUnitSymbol;

  private Long orderUnitId;
  private BigDecimal price;
  private BigDecimal quantity;
  private BigDecimal discountPercentage;
  private Long itemVendorRecordId;

  public LocalizedString getOrderUnitSymbol() {
    return orderUnitSymbol;
  }

  public void setOrderUnitSymbol(LocalizedString orderUnitSymbol) {
    this.orderUnitSymbol = orderUnitSymbol;
  }

  public Long getOrderUnitId() {
    return orderUnitId;
  }

  public void setOrderUnitId(Long orderUnitId) {
    this.orderUnitId = orderUnitId;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getDiscountPercentage() {
    return discountPercentage;
  }

  public void setDiscountPercentage(BigDecimal discountPercentage) {
    this.discountPercentage = discountPercentage;
  }

  public Long getItemVendorRecordId() {
    return itemVendorRecordId;
  }

  public void setItemVendorRecordId(Long itemVendorRecordId) {
    this.itemVendorRecordId = itemVendorRecordId;
  }
}
