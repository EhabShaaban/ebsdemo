package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.joda.time.DateTime;

@Entity
@Table(name = "IObOrderApproverGeneralModel")
public class IObOrderApproverGeneralModel {

  @Id private Long id;

  private Long approvalCycleNum;

  private String purchaseOrderCode;

  private String controlPointCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString controlPointName;

  private boolean isMainApprover;

  private String approverName;

  private String decision;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime decisionDatetime;

  private String notes;

  public Long getApprovalCycleNum() {
    return approvalCycleNum;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public String getControlPointCode() {
    return controlPointCode;
  }

  public LocalizedString getControlPointName() {
    return controlPointName;
  }

  public String getApproverName() {
    return approverName;
  }

  public String getDecision() {
    return decision;
  }

  public String getNotes() {
    return notes;
  }
}
