package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderDeliveryCompleteValueObject {

  String DELIVERY_COMPLETE_DATE_TIME = "deliveryCompleteDateTime";
}
