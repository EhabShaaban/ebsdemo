package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import java.math.BigDecimal;

public class IObOrderLineDetailsQuantitiesValueObject implements IValueObject {

  private BigDecimal quantity;
  private String orderUnitCode;
  private BigDecimal discountPercentage;
  private BigDecimal price;
  private String itemCode;
  private String orderCode;
  private Long quantityId;

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getOrderUnitCode() {
    return orderUnitCode;
  }

  public void setOrderUnitCode(String orderUnitCode) {
    this.orderUnitCode = orderUnitCode;
  }

  public BigDecimal getDiscountPercentage() {
    return discountPercentage;
  }

  public void setDiscountPercentage(BigDecimal discountPercentage) {
    this.discountPercentage = discountPercentage;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }

  public String getOrderCode() {
    return orderCode;
  }

  public void setOrderCode(String orderCode) {
    this.orderCode = orderCode;
  }

  public Long getQuantityId() {
    return quantityId;
  }

  public void setQuantityId(Long quantityId) {
    this.quantityId = quantityId;
  }
}
