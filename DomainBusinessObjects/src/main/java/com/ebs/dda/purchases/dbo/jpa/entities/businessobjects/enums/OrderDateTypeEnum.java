package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums;

import com.ebs.dac.dbo.types.BusinessEnum;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/** @author Yara Ameen */
@Embeddable
public class OrderDateTypeEnum extends BusinessEnum {
  private static final long serialVersionUID = 1L;

  @Enumerated(EnumType.STRING)
  private OrderDateType value;

  public OrderDateTypeEnum() {}

  @Override
  public OrderDateType getId() {
    return this.value;
  }

  public void setId(OrderDateType id) {
    this.value = id;
  }

  public enum OrderDateType {
    ORDER_DATE_CONFIRMATION,
    DOWN_PAYMENT_DATE
  }
}
