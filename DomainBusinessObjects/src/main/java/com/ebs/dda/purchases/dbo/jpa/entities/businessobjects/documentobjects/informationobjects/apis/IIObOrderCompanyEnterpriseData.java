package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

/** * @author : Menna Sayed * @Date : Mar 21, 2018 */
public interface IIObOrderCompanyEnterpriseData extends IIObEnterpriseData {

  String SYS_NAME = "Company";

  String COMPANY_NAME_ATTR_NAME = "company";
  LocalizedTextAttribute<LocalizedString> companyAttr =
      new LocalizedTextAttribute<>(COMPANY_NAME_ATTR_NAME);
}
