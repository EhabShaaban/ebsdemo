package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums;

public enum OrderTypeEnum {
  IMPORT_PO(Values.IMPORT_PO),
  LOCAL_PO(Values.LOCAL_PO),
  SERVICE_PO(Values.SERVICE_PO);

  private OrderTypeEnum(String val) {
    // force equality between name of enum instance, and value of constant
    if (!this.name().equals(val)) throw new IllegalArgumentException("Incorrect use of ELanguage");
  }

  public static class Values {
    public static final String IMPORT_PO = "IMPORT_PO";
    public static final String LOCAL_PO = "LOCAL_PO";
    public static final String SERVICE_PO = "SERVICE_PO";
  }
}
