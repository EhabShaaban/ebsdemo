package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class DObOrderDocumentGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {

  private String objectTypeCode;
  private String purchaseUnitNameEn;
  private String documentOwnerName;
  private Long documentOwnerId;

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getDocumentOwnerName() {
    return documentOwnerName;
  }

  public Long getDocumentOwnerId() {
    return documentOwnerId;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
