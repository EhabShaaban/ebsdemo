package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;

public interface IObOrderApprovalCycleRep extends InformationObjectRep<IObOrderApprovalCycle> {
  IObOrderApprovalCycle findByRefInstanceIdAndFinalDecisionIsNull(Long purchaseOrderId);
  IObOrderApprovalCycle findTopByRefInstanceIdOrderByIdDesc(Long purchaseOrderId);
}
