package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderCompanyEnterpriseData;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

@Entity
@DiscriminatorValue("Company")
@EntityInterface(IIObOrderCompanyEnterpriseData.class)
public class IObOrderCompanyEnterpriseData extends IObEnterpriseData {

  private static final long serialVersionUID = 1L;

  @Transient
  @EmbeddedAttribute
  private LocalizedString company;

  public LocalizedString getCompany() {
    this.company = super.getEnterpriseName();
    return company;
  }

  public void setCompany(LocalizedString company) {
    this.company = company;
  }

  @Override
  public String getSysName() {
    return IIObOrderCompanyEnterpriseData.SYS_NAME;
  }
}
