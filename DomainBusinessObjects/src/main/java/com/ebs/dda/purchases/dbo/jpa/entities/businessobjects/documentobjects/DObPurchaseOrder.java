package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "DObPurchaseOrder")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@EntityInterface(IDObPurchaseOrder.class)
public abstract class DObPurchaseOrder extends DObOrderDocument implements Serializable {

  private BigDecimal remaining = BigDecimal.ZERO;

  public void setRemaining(BigDecimal remaining) {
    this.remaining = remaining;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
