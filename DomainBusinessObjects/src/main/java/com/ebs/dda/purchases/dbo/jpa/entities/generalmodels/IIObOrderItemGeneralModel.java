package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

public interface IIObOrderItemGeneralModel {

  String ORDER_CODE = "orderCode";
  String OBJECT_TYPE_CODE = "objectTypeCode";
  String ITEM_CODE = "itemCode";
  String ITEM_NAME = "itemName";
  String ITEM_TYPE_CODE = "itemTypeCode";
  String ITEM_TYPE_NAME = "itemTypeName";
  String ORDER_UNIT_CODE = "orderUnitCode";
  String UNIT_OF_MEASURE_CODE = "unitOfMeasureCode";
  String ORDER_UNIT_NAME = "orderUnitName";
  String ORDER_UNIT_SYMBOL = "orderUnitSymbol";
  String QUANTITY = "quantity";
  String PRICE = "price";
  String ITEM_TOTAL_AMOUNT = "itemTotalAmount";
}
