package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

public interface IIObOrderStorehouseEnterpriseData extends IIObEnterpriseData {

  String SYS_NAME = "Storehouse";

  byte STOREHOUSE_NAME_ATTR_CODE = 10;
  String STOREHOUSE_NAME_ATTR_NAME = "storehouse";
}
