package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import org.joda.time.DateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IDObPurchaseOrderGeneralModel.class)
@Table(name = "DObPurchaseOrderCycleDatesGeneralModel")
public class DObPurchaseOrderCycleDatesGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  private String objectTypeCode;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime pIRequestedDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime confirmationDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime productionFinishedDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime shippingDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime arrivalDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime clearanceDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime deliveryCompleteDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime approvalDate;

  public DateTime getConfirmationDate() {
    return confirmationDate;
  }

  public void setConfirmationDate(DateTime confirmationDate) {
    this.confirmationDate = confirmationDate;
  }

  public DateTime getShippingDate() {
    return shippingDate;
  }

  public void setShippingDate(DateTime shippingDate) {
    this.shippingDate = shippingDate;
  }

  public DateTime getArrivalDate() {
    return arrivalDate;
  }

  public void setArrivalDate(DateTime arrivalDate) {
    this.arrivalDate = arrivalDate;
  }

  public DateTime getClearanceDate() {
    return clearanceDate;
  }

  public void setClearanceDate(DateTime clearanceDate) {
    this.clearanceDate = clearanceDate;
  }

  public DateTime getDeliveryCompleteDate() {
    return deliveryCompleteDate;
  }

  public void setDeliveryCompleteDate(DateTime deliveryCompleteDate) {
    this.deliveryCompleteDate = deliveryCompleteDate;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public DateTime getProductionFinishedDate() {
    return productionFinishedDate;
  }

  public void setProductionFinishedDate(DateTime productionFinishedDate) {
    this.productionFinishedDate = productionFinishedDate;
  }

  public DateTime getpIRequestedDate() {
    return pIRequestedDate;
  }

  public void setpIRequestedDate(DateTime pIRequestedDate) {
    this.pIRequestedDate = pIRequestedDate;
  }

  public DateTime getApprovalDate() {
    return approvalDate;
  }

  public void setApprovalDate(DateTime approvalDate) {
    this.approvalDate = approvalDate;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
