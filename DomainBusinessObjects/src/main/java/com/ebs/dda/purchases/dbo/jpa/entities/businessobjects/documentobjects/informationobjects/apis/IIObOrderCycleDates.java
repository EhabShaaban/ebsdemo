/** */
package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.processing.BasicAttribute;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 4:58:35 PM
 */
public interface IIObOrderCycleDates {
  String SYS_NAME = "IIObOrderCycleDates";

  String ORDER_DATE_NAME = "actualPIRequestedDate";

  BasicAttribute orderDateAttr = new BasicAttribute(ORDER_DATE_NAME);
}
