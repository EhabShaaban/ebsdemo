package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.apis;

import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderPaymentAndDeliveryValueObject;

public interface IDObPaymentAndDeliveryGeneralModel {
  String ORDER_CODE = IDObPurchaseOrderPaymentAndDeliveryValueObject.ORDER_CODE;
  String INCOTERM = "incotermCode";
  String CURRENCY = "currencyCode";
  String PAYMENT_TERMS = "paymentTermCode";
  String SHIPPING_INSTRUCTIONS = "shippingInstructionsCode";
  String CONTAINERS_TYPE = "containerCode";
  String COLLECTION_DATE = IDObPurchaseOrderPaymentAndDeliveryValueObject.COLLECTION_DATE;
  String MODE_OF_TRANSPORT = "transportModeCode";
  String LOADING_PORT = "loadingPortCode";
  String DISCHARGE_PORT = "dischargePortCode";
  String CONTAINERS_NUMBER = IDObPurchaseOrderPaymentAndDeliveryValueObject.CONTAINERS_NUMBER;
  String NUMBER_OF_DAYS = IDObPurchaseOrderPaymentAndDeliveryValueObject.NUMBER_OF_DAYS;
}
