package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderLineDetailsGeneralModelRep
    extends JpaRepository<IObOrderLineDetailsGeneralModel, Long> {

  List<IObOrderLineDetailsGeneralModel> findByOrderCode(String purchaseOrderCode);

  List<IObOrderLineDetailsGeneralModel> findByOrderCodeOrderByIdDesc(String purchaseOrderCode);

  IObOrderLineDetailsGeneralModel findByOrderCodeAndItemCode(
      String purchaseOrderCode, String itemCode);
}
