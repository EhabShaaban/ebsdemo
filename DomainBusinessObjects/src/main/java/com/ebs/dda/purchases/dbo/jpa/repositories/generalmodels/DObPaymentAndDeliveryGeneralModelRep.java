package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPaymentAndDeliveryGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DObPaymentAndDeliveryGeneralModelRep
    extends JpaRepository<DObPaymentAndDeliveryGeneralModel, Long> {

  DObPaymentAndDeliveryGeneralModel findByUserCode(String code);
}
