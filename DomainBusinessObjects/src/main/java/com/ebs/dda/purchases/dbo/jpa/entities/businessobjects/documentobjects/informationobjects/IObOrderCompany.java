package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;

import javax.persistence.*;

@Entity
@Table(name = "IObOrderCompany")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "partyRole")
public class IObOrderCompany extends DocumentHeader {

  private Long companyId;
  private Long businessUnitId;

  public Long getCompanyId() {
    return companyId;
  }

  public void setCompanyId(Long companyId) {
    this.companyId = companyId;
  }

  public Long getBusinessUnitId() {
    return businessUnitId;
  }

  public void setBusinessUnitId(Long businessUnitId) {
    this.businessUnitId = businessUnitId;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
