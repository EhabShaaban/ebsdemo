/** */
package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderCycleDates;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 5:31:05 PM
 */
public interface IObOrderCycleDatesRep extends InformationObjectRep<IObOrderCycleDates> {
  IObOrderCycleDates findOneByRefInstanceId(Long id);
}
