/** */
package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 2:57:20 PM
 */
public interface IIObOrderRequiredDocuments extends IInformationObject {
  String SYS_NAME = "IObOrderRequiredDocuments";

}
