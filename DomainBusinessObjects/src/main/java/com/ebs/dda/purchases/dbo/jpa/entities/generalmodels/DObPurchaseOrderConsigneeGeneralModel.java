package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObPurchaseOrderConsigneeSectionGeneralModel")
@EntityInterface(value = IDObPurchaseOrderGeneralModel.class)
public class DObPurchaseOrderConsigneeGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {
  private static final long serialVersionUID = 1L;

  @Column(name = "purchasingunitcode")
  private String purchasingUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "purchasingunitname")
  private LocalizedString purchaseUnitName;

  private String objectTypeCode;

  @Column(name = "consigneecode")
  private String consigneeCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "consigneename")
  private LocalizedString consigneeName;

  @Column(name = "plantcode")
  private String plantCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "plantname")
  private LocalizedString plantName;

  @Column(name = "storehousecode")
  private String storehouseCode;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "storehousename")
  private LocalizedString storehouseName;

  public String getPurchasingUnitCode() {
    return purchasingUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getConsigneeCode() {
    return consigneeCode;
  }

  public void setConsigneeCode(String consigneeCode) {
    this.consigneeCode = consigneeCode;
  }

  public LocalizedString getConsigneeName() {
    return consigneeName;
  }

  public void setConsigneeName(LocalizedString consigneeName) {
    this.consigneeName = consigneeName;
  }

  public String getPlantCode() {
    return plantCode;
  }

  public void setPlantCode(String plantCode) {
    this.plantCode = plantCode;
  }

  public LocalizedString getPlantName() {
    return plantName;
  }

  public void setPlantName(LocalizedString plantName) {
    this.plantName = plantName;
  }

  public String getStorehouseCode() {
    return storehouseCode;
  }

  public void setStorehouseCode(String storehouseCode) {
    this.storehouseCode = storehouseCode;
  }

  public LocalizedString getStorehouseName() {
    return storehouseName;
  }

  public void setStorehouseName(LocalizedString storehouseName) {
    this.storehouseName = storehouseName;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
