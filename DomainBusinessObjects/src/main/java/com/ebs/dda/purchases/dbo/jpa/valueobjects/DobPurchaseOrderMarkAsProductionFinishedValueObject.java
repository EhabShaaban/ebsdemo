package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import org.joda.time.DateTime;

public class DobPurchaseOrderMarkAsProductionFinishedValueObject {
  private DateTime productionFinishedDate;
  private String purchaseOrderCode;

  public DateTime getProductionFinishedDate() {
    return productionFinishedDate;
  }

  public void setProductionFinishedDate(DateTime productionFinishedDate) {
    this.productionFinishedDate = productionFinishedDate;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }
}
