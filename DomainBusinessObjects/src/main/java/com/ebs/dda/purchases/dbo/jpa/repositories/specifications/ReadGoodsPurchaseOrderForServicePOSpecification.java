package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class ReadGoodsPurchaseOrderForServicePOSpecification {

  private Predicate whenStateIs(
      Root<DObPurchaseOrderGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }

  private Predicate whenObjectTypeCodeIs(
      Root<DObPurchaseOrderGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      String objectTypeCode) {
    Path<String> expression = root.get(IDObPurchaseOrderGeneralModel.OBJECT_TYPE_CODE);
    return criteriaBuilder.equal(expression, objectTypeCode);
  }

  public Specification<DObPurchaseOrderGeneralModel> isAllowedDobOrder() {
    return new Specification<DObPurchaseOrderGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObPurchaseOrderGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        Predicate isConfirmedOrder = whenStateIs(root, criteriaBuilder, "Confirmed");
        Predicate isFinishedProductionOrder =
            whenStateIs(root, criteriaBuilder, "FinishedProduction");
        Predicate isShippedOrder = whenStateIs(root, criteriaBuilder, "Shipped");
        Predicate isArrivedOrder = whenStateIs(root, criteriaBuilder, "Arrived");
        Predicate isClearedOrder = whenStateIs(root, criteriaBuilder, "Cleared");
        Predicate isAllowedState =
            criteriaBuilder.or(
                isConfirmedOrder,
                isFinishedProductionOrder,
                isShippedOrder,
                isArrivedOrder,
                isClearedOrder);

        Predicate isLocalPO = whenObjectTypeCodeIs(root, criteriaBuilder, "IMPORT_PO");
        Predicate isImportPO = whenObjectTypeCodeIs(root, criteriaBuilder, "LOCAL_PO");
        Predicate isAllowedPoType = criteriaBuilder.or(isLocalPO, isImportPO);

        return criteriaBuilder.and(isAllowedState, isAllowedPoType);
      }
    };
  }
}
