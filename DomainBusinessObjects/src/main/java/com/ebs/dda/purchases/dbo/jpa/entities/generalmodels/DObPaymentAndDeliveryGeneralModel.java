package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringJsonConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "PaymentAndDeliveryGeneralModel")
@EntityInterface(value = IDObPurchaseOrderGeneralModel.class)
public class DObPaymentAndDeliveryGeneralModel extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Column(name = "purchaseunitcode")
  private String purchaseUnitCode;

  @Column(name = "incotermcode")
  private String incotermCode;

  @Column(name = "currencycode")
  private String currencyCode;

  @Column(name = "currencyiso")
  private String currencyIso;

  @Column(name = "paymenttermcode")
  private String paymentTermCode;

  @Column(name = "shippinginstructionscode")
  private String shippingInstructionsCode;

  @Column(name = "containercode")
  private String containerCode;

  @Column(name = "modeoftransportcode")
  private String transportModeCode;

  @Column(name = "loadingportcode")
  private String loadingPortCode;

  @Column(name = "dischargeportcode")
  private String dischargePortCode;

  @Column(name = "collectiondate")
  private String collectionDate;

  @Column(name = "numberofdays")
  private Integer numberOfDays;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "purchaseUnitName")
  private LocalizedString purchaseUnitName;

  private String objectTypeCode;

  @Column(name = "incotermname")
  private String incotermName;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "modeoftransportname")
  private LocalizedString modeOfTransport;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "shippinginstructionsname")
  private LocalizedString shippingInstructions;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "loadingcountry")
  private LocalizedString loadingCountry;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "loadingportname")
  private LocalizedString loadingPortName;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "dischargecountry")
  private LocalizedString dischargeCountry;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "dischargeportname")
  private LocalizedString dischargePortName;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "currency")
  private LocalizedString currency;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "paymenttermname")
  private LocalizedString paymentTermName;

  @Convert(converter = LocalizedStringJsonConverter.class)
  @Column(name = "containername")
  private LocalizedString containerName;

  @Column(name = "containerno")
  private Integer containersNo;

  public Integer getNumberOfDays() {
    return numberOfDays;
  }

  public String getCollectionDate() {
    return collectionDate;
  }

  public Integer getContainersNo() {
    return containersNo;
  }

  public String getIncotermCode() {
    return incotermCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public String getShippingInstructionsCode() {
    return shippingInstructionsCode;
  }

  public String getContainerCode() {
    return containerCode;
  }

  public String getTransportModeCode() {
    return transportModeCode;
  }

  public String getLoadingPortCode() {
    return loadingPortCode;
  }

  public String getDischargePortCode() {
    return dischargePortCode;
  }

  public String getCurrencyIso() {
    return currencyIso;
  }

  @Override
  public Set<String> getCurrentStates() {

    return super.getCurrentStates();
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
