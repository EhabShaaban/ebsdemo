package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderLineDetailsQuantitiesGeneralModelRep
    extends JpaRepository<IObOrderLineDetailsQuantitiesGeneralModel, Long> {

  List<IObOrderLineDetailsQuantitiesGeneralModel> findByRefInstanceIdOrderByIdDesc(Long id);

  List<IObOrderLineDetailsQuantitiesWithoutPricesGeneralModel> findOrderQuantitiesWithoutPricesGeneralModelByRefInstanceId(
      Long refInstanceId);

  IObOrderLineDetailsQuantitiesGeneralModel findByRefInstanceIdAndOrderUnitCode(Long refinstanceId,
      String orderUnitCode);
}
