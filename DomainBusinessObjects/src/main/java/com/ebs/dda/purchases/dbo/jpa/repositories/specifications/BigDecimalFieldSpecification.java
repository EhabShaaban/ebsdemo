package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.math.BigDecimal;

public class BigDecimalFieldSpecification<T extends BusinessObject> implements Specification<T> {

  private FilterCriteria criteria;

  public BigDecimalFieldSpecification(FilterCriteria filterCriteria) {
    this.criteria = filterCriteria;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    if (criteria != null) {
      if (criteria.getOperation().equalsIgnoreCase("equals")) {
        Path<String> expression = root.get(criteria.getKey());
        String filterStringValue = this.criteria.getValue();
        BigDecimal filterDoubleValue = BigDecimal.ZERO;
        try {
          filterDoubleValue = new BigDecimal(filterStringValue);
          return builder.equal(expression, filterDoubleValue);
        } catch (NumberFormatException exception) {
          return builder.equal(expression, -1.0);
        }
      }
    }
    return null;
  }
}
