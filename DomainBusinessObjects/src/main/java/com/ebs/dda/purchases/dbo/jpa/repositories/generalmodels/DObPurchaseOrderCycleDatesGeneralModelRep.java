package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderCycleDatesGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.QueryHints;

import javax.persistence.QueryHint;

import static org.eclipse.persistence.config.QueryHints.JDBC_MAX_ROWS;

public interface DObPurchaseOrderCycleDatesGeneralModelRep
    extends JpaRepository<DObPurchaseOrderCycleDatesGeneralModel, Long> {

  DObPurchaseOrderCycleDatesGeneralModel findOneByUserCode(String purchaseOrderCode);

  @QueryHints(@QueryHint(name = JDBC_MAX_ROWS, value = "1"))
  DObPurchaseOrderCycleDatesGeneralModel findOneByUserCodeOrderByIdDesc(String userCode);


}
