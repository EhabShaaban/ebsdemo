package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

public interface IIObPaymentAndDelivery {

  String INCOTERM_ID = "incotermRef";
  String CURRENCY_ID = "currencyRef";
  String PAYMENT_TERMS_ID = "paymentTermRef";
  String SHIPPING_INSTRUCTIONS_ID = "shippingInstructionsRef";
  String COLLECTION_DATE = "collectionDate";
  String MODE_OF_TRANSPORT_ID = "modeOfTransportRef";
  String CONTAINERS_NUMBER = "quantity";
  String CONTAINERS_TYPE_ID = "containerType";
  String LOADING_PORT_ID = "loadingPortRef";
  String DISCHARGE_PORT_ID = "dischargePortRef";
}
