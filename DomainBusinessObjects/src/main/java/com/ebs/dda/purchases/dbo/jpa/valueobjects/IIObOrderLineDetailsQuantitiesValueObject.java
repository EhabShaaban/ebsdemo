package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IIObOrderLineDetailsQuantitiesValueObject {

  String QUANTITY = "quantity";
  String ORDER_UNIT_CODE = "orderUnitCode";
  String PRICE = "price";
  String DISCOUNT_PERCENTAGE = "discountPercentage";
}
