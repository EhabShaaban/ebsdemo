package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDobPurchaseOrderProductionFinishedValueObject {
	  String PRODUCTION_FINISHED_DATE_TIME = "productionFinishedDate";

}
