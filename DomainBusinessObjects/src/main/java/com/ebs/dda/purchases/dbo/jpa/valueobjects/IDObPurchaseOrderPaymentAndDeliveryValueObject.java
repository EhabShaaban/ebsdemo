package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderPaymentAndDeliveryValueObject {

  String ORDER_CODE = "purchaseOrderCode";
  String INCOTERM = "incoterm";
  String CURRENCY = "currency";
  String PAYMENT_TERMS = "paymentTerms";
  String SHIPPING_INSTRUCTIONS = "shippingInstructions";
  String CONTAINERS_TYPE = "containersType";
  String COLLECTION_DATE = "collectionDate";
  String MODE_OF_TRANSPORT = "modeOfTransport";
  String LOADING_PORT = "loadingPort";
  String DISCHARGE_PORT = "dischargePort";
  String CONTAINERS_NUMBER = "containersNo";
  String NUMBER_OF_DAYS = "numberOfDays";
}
