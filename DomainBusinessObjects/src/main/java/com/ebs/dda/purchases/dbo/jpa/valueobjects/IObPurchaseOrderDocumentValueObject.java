package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public class IObPurchaseOrderDocumentValueObject {
  private String documentTypeCode;
  private Integer numberOfCopies;

  public String getDocumentTypeCode() {
    return documentTypeCode;
  }

  public void setDocumentTypeCode(String documentTypeCode) {
    this.documentTypeCode = documentTypeCode;
  }

  public Integer getNumberOfCopies() {
    return numberOfCopies;
  }

  public void setNumberOfCopies(Integer numberOfCopies) {
    this.numberOfCopies = numberOfCopies;
  }
}
