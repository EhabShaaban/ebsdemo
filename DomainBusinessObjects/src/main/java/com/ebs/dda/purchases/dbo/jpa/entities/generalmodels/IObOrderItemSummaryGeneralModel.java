package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderItemSummaryGeneralModel")
public class IObOrderItemSummaryGeneralModel {
  @Id private Long id;
  private String orderCode;
  private String objectTypeCode;

  private BigDecimal totalAmountBeforeTaxes;
  private BigDecimal totalTaxes;
  private BigDecimal totalAmountAfterTaxes;

  public BigDecimal getTotalAmountBeforeTaxes() {
    return totalAmountBeforeTaxes;
  }

  public BigDecimal getTotalTaxes() {
    return totalTaxes;
  }

  public BigDecimal getTotalAmountAfterTaxes() {
    return totalAmountAfterTaxes;
  }

  public String getOrderCode() {
    return orderCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }
}
