package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObOrderPaymentGeneralModel;

public interface DObOrderPaymentGeneralModelRep
				extends StatefullBusinessObjectRep<DObOrderPaymentGeneralModel, DefaultUserCode>,
				JpaSpecificationExecutor<DObOrderPaymentGeneralModel> {

	@Override
	Page<DObOrderPaymentGeneralModel> findAll(
					Specification<DObOrderPaymentGeneralModel> specification, Pageable page);
}