/** */
package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderPaymentTermsDetails;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 5:31:48 PM
 */
public interface IObOrderPaymentTermsDetailsRep
    extends InformationObjectRep<IObOrderPaymentTermsDetails> {}
