package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import org.joda.time.DateTime;

public class DObPurchaseOrderConfirmValueObject {

  DateTime confirmationDateTime;

  public DateTime getConfirmationDateTime() {
    return confirmationDateTime;
  }

  public void setConfirmationDateTime(DateTime confirmationDateTime) {
    this.confirmationDateTime = confirmationDateTime;
  }
}
