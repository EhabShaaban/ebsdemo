package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import org.joda.time.DateTime;

public class DobPurchaseOrderMarkAsPIRequestedValueObject {
  private DateTime pIRequestedDate;
  private String purchaseOrderCode;

  public DateTime getpIRequestedDate() {
    return pIRequestedDate;
  }

  public void setpIRequestedDate(DateTime pIRequestedDate) {
    this.pIRequestedDate = pIRequestedDate;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }
}
