package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

/**
 * @author Ahmed Ali Elfeky
 * @since Feb 28, 2018 11:27:09 AM
 */
public interface IIObOrderPurchasingUnitEnterpriseData extends IIObEnterpriseData {

  String PURCHASING_UNIT_NAME_ATTR_NAME = "purchaseUnitName";
  LocalizedTextAttribute<LocalizedString> purchaseUnitNameAttr =
      new LocalizedTextAttribute<>(PURCHASING_UNIT_NAME_ATTR_NAME);
}
