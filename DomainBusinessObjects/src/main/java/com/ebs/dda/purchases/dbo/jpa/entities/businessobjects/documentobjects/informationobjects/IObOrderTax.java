package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringJsonConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dac.dbo.types.LocalizedString;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderTax")
public class IObOrderTax extends DocumentLine {

  private String code;

  @Convert(converter = LocalizedStringJsonConverter.class)
  private LocalizedString name;

  private BigDecimal percentage;

  public void setCode(String code) {
    this.code = code;
  }

  public void setName(LocalizedString name) {
    this.name = name;
  }

  public void setPercentage(BigDecimal percentage) {
    this.percentage = percentage;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
