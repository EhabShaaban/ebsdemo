package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.ApprovalDecision;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import org.joda.time.DateTime;

/** CreatedBy : Nancy Shoukry , 02-12-2018 */
// IObOrderApprovalCycle >> 144397762565570560
// IObOrderApprover >> 144397762565636096
@Entity
@Table(name = "IObOrderApprovalCycle")
public class IObOrderApprovalCycle extends DocumentLine implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column private Long approvalCycleNum;

  @Column
  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime startDate;

  @Column
  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime endDate;

  @Enumerated(EnumType.STRING)
  private ApprovalDecision finalDecision;

  public Long getApprovalCycleNum() {
    return approvalCycleNum;
  }

  public void setApprovalCycleNum(Long approvalCycleNum) {
    this.approvalCycleNum = approvalCycleNum;
  }

  public DateTime getStartDate() {
    return startDate;
  }

  public void setStartDate(DateTime startDate) {
    this.startDate = startDate;
  }

  public DateTime getEndDate() {
    return endDate;
  }

  public void setEndDate(DateTime endDate) {
    this.endDate = endDate;
  }

  public ApprovalDecision getFinalDecision() {
    return finalDecision;
  }

  public void setFinalDecision(ApprovalDecision finalDecision) {
    this.finalDecision = finalDecision;
  }
}
