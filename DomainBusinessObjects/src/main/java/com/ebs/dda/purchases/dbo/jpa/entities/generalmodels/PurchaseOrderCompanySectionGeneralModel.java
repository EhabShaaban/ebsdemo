package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CompanySectionGeneralModel")
public class PurchaseOrderCompanySectionGeneralModel {

  @Id private Long purchaseOrderId;
  private Long companyId;
  private Long consigneeId;
  private Long purchaseUnitId;
  private Long purchaseResponsibleId;
  private Long plantId;
  private Long storehouseId;
  private Long payerId;
  private Long payerBankAccountId;

  public Long getPurchaseOrderId() {
    return purchaseOrderId;
  }

  public Long getCompanyId() {
    return companyId;
  }

  public Long getConsigneeId() {
    return consigneeId;
  }

  public Long getPurchaseUnitId() {
    return purchaseUnitId;
  }

  public Long getPurchaseResponsibleId() {
    return purchaseResponsibleId;
  }

  public Long getPlantId() {
    return plantId;
  }

  public Long getStorehouseId() {
    return storehouseId;
  }

  public Long getPayerId() {
    return payerId;
  }

  public Long getPayerBankAccountId() {
    return payerBankAccountId;
  }
}
