package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums;

public enum ApprovalDecision {
  APPROVED,
  REJECTED,
  CANCELLED
}
