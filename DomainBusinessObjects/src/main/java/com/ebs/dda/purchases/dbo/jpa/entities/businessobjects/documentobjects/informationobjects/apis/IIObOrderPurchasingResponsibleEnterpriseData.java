package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis; // Created By Hossam Hassan @ 23/5

import com.ebs.dac.dbo.processing.LocalizedTextAttribute;
import com.ebs.dac.dbo.types.LocalizedString;

public interface IIObOrderPurchasingResponsibleEnterpriseData extends IIObEnterpriseData {
  String PURCHASING_RESPONSIBLE_NAME_ATTR_NAME = "purchasingResponsible";
  LocalizedTextAttribute<LocalizedString> purchasingResponsibleAttr =
      new LocalizedTextAttribute<>(
          PURCHASING_RESPONSIBLE_NAME_ATTR_NAME);
}
