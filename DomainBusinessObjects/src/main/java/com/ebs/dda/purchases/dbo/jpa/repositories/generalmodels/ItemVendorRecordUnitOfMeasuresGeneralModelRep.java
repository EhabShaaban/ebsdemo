package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemVendorRecordUnitOfMeasuresGeneralModelRep
    extends JpaRepository<ItemVendorRecordUnitOfMeasuresGeneralModel, Long> {

  List<ItemVendorRecordUnitOfMeasuresGeneralModel>
      findAllByItemCodeAndVendorCodeAndPurchasingUnitCodeOrderById(
          String itemCode, String vendorcode, String purchaseUnitCode);

  ItemVendorRecordUnitOfMeasuresGeneralModel
      findOneByItemCodeAndVendorCodeAndPurchasingUnitCodeAndAlternativeUnitOfMeasureCode(
          String itemCode, String vendorcode, String purchaseUnitCode, String measureCode);
}
