package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

public class JsonFieldSpecification<T extends BusinessObject> implements Specification<T> {

  private String locale;
  private FilterCriteria criteria;

  public JsonFieldSpecification(FilterCriteria filterCriteria, String locale) {
    this.criteria = filterCriteria;
    this.locale = locale;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

    if (criteria != null) {
      if (criteria.getOperation().equalsIgnoreCase("contains")) {
        Expression<String> expression =
                builder.function(
                        "json_extract_path_text",
                        String.class,
                        root.<String>get(this.criteria.getKey()),
                        builder.literal(this.locale));
        String filterValue = criteria.getValue();
        return builder.like(builder.lower(expression), "%" + filterValue.toLowerCase() + "%");
      } else if (criteria.getOperation().equalsIgnoreCase("equals")) {
        Expression<String> expression =
                builder.function(
                        "json_extract_path_text",
                        String.class,
                        root.<String>get(this.criteria.getKey()),
                        builder.literal(this.locale));
        String filterValue = criteria.getValue();
        return builder.equal(expression, filterValue);
      }
    }
    return null;
  }
}
