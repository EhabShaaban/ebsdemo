package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderContainersDetails;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/** @author Yara Ameen */
@Entity
@Table(name = "IObOrderContainersDetails")
@EntityInterface(IIObOrderContainersDetails.class)
public class IObOrderContainersDetails extends DocumentLine implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "containerId")
  private Long containerType;

  private Integer quantity;

  public Long getContainerType() {
    return containerType;
  }

  public void setContainerType(Long containerType) {
    this.containerType = containerType;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  @Override
  public String getSysName() {
    return IIObOrderContainersDetails.SYS_NAME;
  }
}
