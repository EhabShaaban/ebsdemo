package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNoteGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNoteGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadDebitNotesForCollectionSpecification {

  public Specification<DObAccountingNoteGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObAccountingNoteGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObAccountingNoteGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObAccountingNoteGeneralModel> debitNotesAllowedRemaining() {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> isDebitNoteRamainingAllowed(root, criteriaBuilder);
  }

  private Predicate isDebitNoteRamainingAllowed(Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(
            root.get(IDObAccountingNoteGeneralModel.REMAINING), 0);
  }

  public Specification<DObAccountingNoteGeneralModel> accountingNoteIsDebit() {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> isDebitAccoutingNote(root, criteriaBuilder);
  }

  private Predicate isDebitAccoutingNote(
          Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.like(root.get(IDObAccountingNoteGeneralModel.OBJECT_TYPE_CODE), "%DEBIT_NOTE%");
  }

  public Specification<DObAccountingNoteGeneralModel> debitNoteInAllowedState() {
    return (Specification<DObAccountingNoteGeneralModel>)
        (root, query, criteriaBuilder) -> isDebitNoteInAllowedState(root, criteriaBuilder);
  }

  private Predicate isDebitNoteInAllowedState(
      Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return whenStateIs(root, criteriaBuilder, "Posted");
  }

  private Predicate whenStateIs(
      Root<DObAccountingNoteGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
