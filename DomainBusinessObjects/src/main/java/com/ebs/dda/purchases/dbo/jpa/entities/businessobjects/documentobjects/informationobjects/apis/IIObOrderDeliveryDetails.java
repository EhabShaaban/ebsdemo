package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 2:41:58 PM
 */
public interface IIObOrderDeliveryDetails extends IInformationObject {
  String SYS_NAME = "IObOrderDeliveryDetails";
}
