package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrderGeneralModel;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;

public class ReadSalesOrderForNotesReceivableSpecification {

  public Specification<DObSalesOrderGeneralModel> isAuthorizedPurchaseUnit(
      List<String> purchaseUnitNames) {
    return new Specification<DObSalesOrderGeneralModel>() {
      @Override
      public Predicate toPredicate(
          Root<DObSalesOrderGeneralModel> root,
          CriteriaQuery<?> query,
          CriteriaBuilder criteriaBuilder) {

        if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
          Expression<String> purchaseUnitEnglishValue =
              criteriaBuilder.function(
                  "json_extract_path_text",
                  String.class,
                  root.<String>get("purchaseUnitName"),
                  criteriaBuilder.literal("en"));
          return purchaseUnitEnglishValue.in(purchaseUnitNames);
        }
        return null;
      }
    };
  }

  public Specification<DObSalesOrderGeneralModel> salesOrdersAllowedBusinessUnit(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObSalesOrderGeneralModel>)
        (root, query, criteriaBuilder) ->
            isSalesOrderBusinessUnitAllowed(root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  public Specification<DObSalesOrderGeneralModel> salesOrdersAllowedCompany(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObSalesOrderGeneralModel>)
        (root, query, criteriaBuilder) ->
            isSalesOrderCompanyAllowed(root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  public Specification<DObSalesOrderGeneralModel> salesOrdersAllowedBusinessPartner(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return (Specification<DObSalesOrderGeneralModel>)
        (root, query, criteriaBuilder) ->
            isSalesOrderBusinessPartnerAllowed(root, criteriaBuilder, notesReceivablesGeneralModel);
  }

  public Specification<DObSalesOrderGeneralModel> salesOrdersAllowedRemaining() {
    return (Specification<DObSalesOrderGeneralModel>)
        (root, query, criteriaBuilder) -> isSalesOrderRemainingAllowed(root, criteriaBuilder);
  }

  private Predicate isSalesOrderBusinessUnitAllowed(
      Root<DObSalesOrderGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
        root.get(IDObSalesOrderGeneralModel.PURCHASE_UNIT_CODE),
        notesReceivablesGeneralModel.getPurchaseUnitCode());
  }

  private Predicate isSalesOrderCompanyAllowed(
      Root<DObSalesOrderGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
        root.get(IDObSalesOrderGeneralModel.COMPANY_CODE),
        notesReceivablesGeneralModel.getCompanyCode());
  }

  private Predicate isSalesOrderBusinessPartnerAllowed(
      Root<DObSalesOrderGeneralModel> root,
      CriteriaBuilder criteriaBuilder,
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return criteriaBuilder.equal(
        root.get(IDObSalesOrderGeneralModel.CUSTOMER_CODE),
        notesReceivablesGeneralModel.getBusinessPartnerCode());
  }

  private Predicate isSalesOrderRemainingAllowed(
      Root<DObSalesOrderGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    return criteriaBuilder.greaterThan(root.get(IDObSalesOrderGeneralModel.REMAINING), 0);
  }

  public Specification<DObSalesOrderGeneralModel> salesOrdersInAllowedState() {
    return (Specification<DObSalesOrderGeneralModel>)
        (root, query, criteriaBuilder) -> isSalesOrderInAllowedState(root, criteriaBuilder);
  }

  private Predicate isSalesOrderInAllowedState(
      Root<DObSalesOrderGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Predicate isGoodsIssueActivated = whenStateIs(root, criteriaBuilder, "GoodsIssueActivated");
    Predicate isApproved = whenStateIs(root, criteriaBuilder, "Approved");
    return criteriaBuilder.or(isGoodsIssueActivated, isApproved);
  }

  private Predicate whenStateIs(
      Root<DObSalesOrderGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
        root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }
}
