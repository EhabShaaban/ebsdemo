package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.jpa.order.salesorder.IObSalesOrderSummaryGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObSalesOrderSummaryGeneralModelRep
    extends JpaRepository<IObSalesOrderSummaryGeneralModel, Long> {

  IObSalesOrderSummaryGeneralModel findOneByOrderCode(String salesOrderCode);
}
