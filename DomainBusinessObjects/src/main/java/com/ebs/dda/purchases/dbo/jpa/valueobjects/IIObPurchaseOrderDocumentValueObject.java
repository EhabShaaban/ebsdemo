package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IIObPurchaseOrderDocumentValueObject {
  String DOCUMENT_TYPE_CODE = "documentTypeCode";
  String DOCUMENT_NUMBER_OF_COPIES = "numberOfCopies";
}
