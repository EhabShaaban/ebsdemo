package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import java.util.List;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.joda.time.DateTime;

@Entity
@Table(name = "IObOrderApprovalCycleGeneralModel")
public class IObOrderApprovalCycleGeneralModel {

  @Id private Long id;

  private Long approvalCycleNum;

  private String purchaseOrderCode;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime startDate;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime endDate;

  private String finalDecision;

  @Transient private List<IObOrderApproverGeneralModel> approvers;

  public Long getApprovalCycleNum() {
    return approvalCycleNum;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public DateTime getStartDate() {
    return startDate;
  }

  public DateTime getEndDate() {
    return endDate;
  }

  public String getFinalDecision() {
    return finalDecision;
  }

  public List<IObOrderApproverGeneralModel> getApprovers() {
    return approvers;
  }

  public void setApprovers(List<IObOrderApproverGeneralModel> approvers) {
    this.approvers = approvers;
  }
}
