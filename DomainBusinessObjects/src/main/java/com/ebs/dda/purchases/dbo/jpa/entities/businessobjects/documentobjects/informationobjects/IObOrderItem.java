package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderItem")
public class IObOrderItem extends DocumentLine {

  private Long itemId;
  private Long orderUnitId;
  private BigDecimal quantity;
  private BigDecimal price;

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Long getOrderUnitId() {
    return orderUnitId;
  }

  public void setOrderUnitId(Long orderUnitId) {
    this.orderUnitId = orderUnitId;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
