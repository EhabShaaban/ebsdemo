package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderClearanceValueObject {

  String CLEARANCE_DATE_TIME = "clearanceDateTime";
}
