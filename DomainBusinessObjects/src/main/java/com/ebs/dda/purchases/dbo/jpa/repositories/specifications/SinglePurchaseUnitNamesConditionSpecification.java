package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class SinglePurchaseUnitNamesConditionSpecification<T extends BusinessObject>
				implements Specification<T> {

  private List<String> purchaseUnitNames;
  private String key;

  public SinglePurchaseUnitNamesConditionSpecification(List<String> purchaseUnitNames, String key) {
    this.purchaseUnitNames = purchaseUnitNames;
    this.key = key;
  }

  @Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
		if (purchaseUnitNames.size() > 0 && !purchaseUnitNames.contains("*")) {
      return root.get(key).in(purchaseUnitNames);
    }

    return null;
  }
}
