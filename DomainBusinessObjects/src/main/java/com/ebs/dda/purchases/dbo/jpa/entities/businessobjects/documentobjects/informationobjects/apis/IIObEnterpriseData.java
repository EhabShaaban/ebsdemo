package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;

/** @author Yara Ameen */
public interface IIObEnterpriseData extends IInformationObject {

  String SYS_NAME = "EnterpriseData";
  }
