package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.DocumentObjectRep;
import com.ebs.dda.jpa.order.DObOrderDocument;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/** @author Yara Ameen */
public interface DObOrderDocumentRep extends DocumentObjectRep<DObOrderDocument, DefaultUserCode> {
  DObOrderDocument findOneByUserCode(String purchaseOrderCode);

  @Query("SELECT O.id FROM DObOrderDocument O WHERE O.userCode = :userCode")
  Long findOrderIdByUserCode(@Param("userCode") String userCode);

}
