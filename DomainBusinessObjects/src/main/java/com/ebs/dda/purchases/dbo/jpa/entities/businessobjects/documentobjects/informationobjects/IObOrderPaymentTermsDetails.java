package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringJsonConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderPaymentTermsDetails;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 2:41:58 PM
 */
@Entity
@Table(name = "IObOrderPaymentTermsDetails")
@EntityInterface(IIObOrderPaymentTermsDetails.class)
public class IObOrderPaymentTermsDetails extends DocumentHeader implements Serializable {
  private static final long serialVersionUID = 1L;

  @Column(name = "paymentTermId")
  private Long paymentTermRef;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringJsonConverter.class)
  private LocalizedString paymentTermName;

  @Column(name = "currencyId")
  private Long currencyRef;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringJsonConverter.class)
  private LocalizedString currencyName;

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public void setCurrencyName(LocalizedString currencyName) {
    this.currencyName = currencyName;
  }

  public Long getCurrencyRef() {
    return currencyRef;
  }

  public void setCurrencyRef(Long currencyRef) {
    this.currencyRef = currencyRef;
  }

  public Long getPaymentTermRef() {
    return paymentTermRef;
  }

  public void setPaymentTermRef(Long paymentTermRef) {
    this.paymentTermRef = paymentTermRef;
  }

  public LocalizedString getPaymentTermName() {
    if (paymentTermName == null) {
      paymentTermName = new LocalizedString();
    }
    return paymentTermName;
  }

  public void setPaymentTermName(LocalizedString paymentTermName) {
    this.paymentTermName = paymentTermName;
  }

  @Override
  public String getSysName() {
    return IIObOrderPaymentTermsDetails.SYS_NAME;
  }
}
