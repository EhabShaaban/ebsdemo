package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentLine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.ApprovalDecision;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import org.joda.time.DateTime;

/** CreatedBy : Nancy Shoukry , 02-12-2018 */

// IObOrderApprovalCycle >> 144397762565570560
// IObOrderApprover >> 144397762565636096
@Entity
@Table(name = "IObOrderApprover")
public class IObOrderApprover extends DocumentLine implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column private Long controlPointId;

  @Column private Long approverId;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  private DateTime decisionDatetime;

  @Enumerated(EnumType.STRING)
  private ApprovalDecision decision;

  @Column private String notes;

  public Long getControlPointId() {
    return controlPointId;
  }

  public void setControlPointId(Long controlPointId) {
    this.controlPointId = controlPointId;
  }

  public Long getApproverId() {
    return approverId;
  }

  public void setApproverId(Long approverId) {
    this.approverId = approverId;
  }

  public DateTime getDecisionDatetime() {
    return decisionDatetime;
  }

  public void setDecisionDatetime(DateTime dicisionDatetime) {
    this.decisionDatetime = dicisionDatetime;
  }

  public ApprovalDecision getDecision() {
    return decision;
  }

  public void setDecision(ApprovalDecision decision) {
    this.decision = decision;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }
}
