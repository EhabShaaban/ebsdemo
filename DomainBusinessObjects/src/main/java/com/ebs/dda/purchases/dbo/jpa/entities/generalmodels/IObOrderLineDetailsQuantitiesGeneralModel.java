package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ioborderlinedetailsquantitiesgeneralmodel")
public class IObOrderLineDetailsQuantitiesGeneralModel {

  @Id private Long id;

  private Long refInstanceId;

  private BigDecimal quantity;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "orderUnitSymbol")
  private LocalizedString orderUnitSymbol;

  private String orderUnitCode;

  private BigDecimal price;

  private BigDecimal discountPercentage;

  private BigDecimal quantityInBase;

  private String itemCodeAtVendor;

  public BigDecimal getQuantityInBase() {
    return quantityInBase;
  }

  public void setQuantityInBase(BigDecimal quantityInBase) {
    this.quantityInBase = quantityInBase;
  }

  public Long getId() {
    return id;
  }

  public LocalizedString getOrderUnitSymbol() {
    return orderUnitSymbol;
  }

  public void setOrderUnitSymbol(LocalizedString orderUnitSymbol) {
    this.orderUnitSymbol = orderUnitSymbol;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public BigDecimal getDiscountPercentage() {
    return discountPercentage;
  }

  public String getItemCodeAtVendor() {
    return itemCodeAtVendor;
  }

  public void setItemCodeAtVendor(String itemCodeAtVendor) {
    this.itemCodeAtVendor = itemCodeAtVendor;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public void setQuantity(BigDecimal quantity) {
    this.quantity = quantity;
  }

  public String getOrderUnitCode() {
    return orderUnitCode;
  }
}
