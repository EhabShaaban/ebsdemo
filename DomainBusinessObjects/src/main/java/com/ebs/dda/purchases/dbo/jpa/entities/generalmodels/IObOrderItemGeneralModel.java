package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.InformationObject;
import com.ebs.dac.dbo.types.LocalizedString;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "IObOrderItemGeneralModel")
public class IObOrderItemGeneralModel extends InformationObject {

  private String orderCode;
  private String objectTypeCode;
  private String itemCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemName;

  private String itemTypeCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString itemTypeName;

  private String orderUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString orderUnitName;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString orderUnitSymbol;

  private BigDecimal quantity;
  private BigDecimal price;
  private BigDecimal itemTotalAmount;

  public String getOrderCode() {
    return orderCode;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public String getItemCode() {
    return itemCode;
  }

  public LocalizedString getItemName() {
    return itemName;
  }

  public String getItemTypeCode() {
    return itemTypeCode;
  }

  public LocalizedString getItemTypeName() {
    return itemTypeName;
  }

  public String getOrderUnitCode() {
    return orderUnitCode;
  }

  public LocalizedString getOrderUnitName() {
    return orderUnitName;
  }

  public LocalizedString getOrderUnitSymbol() {
    return orderUnitSymbol;
  }

  public BigDecimal getQuantity() {
    return quantity;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public BigDecimal getItemTotalAmount() {
    return itemTotalAmount;
  }

  @Override
  public String getSysName() {
    return null;
  }
}
