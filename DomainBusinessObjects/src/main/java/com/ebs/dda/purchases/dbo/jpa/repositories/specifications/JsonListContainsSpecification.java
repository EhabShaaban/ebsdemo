package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class JsonListContainsSpecification <T extends BusinessObject>implements Specification<T> {
	private List<String> multipleValueFieldValues;
	private String multipleValueFieldName;

	public JsonListContainsSpecification(String multipleValueFieldName,
					List<String> multipleValueFieldValues) {
		this.multipleValueFieldName = multipleValueFieldName;
		this.multipleValueFieldValues = multipleValueFieldValues;
	}

	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> criteriaQuery,
					CriteriaBuilder builder) {
		if (multipleValueFieldValues.size() > 0 && !multipleValueFieldValues.contains("*")) {
			Predicate[] predicates = new Predicate[multipleValueFieldValues.size()];
			for (int i = 0; i < predicates.length; i++) {
				Expression<String> stringExpression = builder.function("convertjsontostring",
								String.class, root.get(multipleValueFieldName));

				predicates[i] = builder
								.function("substring", String.class, stringExpression,
												builder.literal(multipleValueFieldValues.get(i)))
								.isNotNull();
			}
			return builder.or(predicates);
		}
		return null;
	}
}