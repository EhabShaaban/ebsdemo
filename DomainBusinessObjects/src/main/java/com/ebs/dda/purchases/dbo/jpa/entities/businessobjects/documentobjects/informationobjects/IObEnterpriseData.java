package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseAddressDetails;
import com.ebs.dda.jpa.masterdata.enterprise.IObEnterpriseContactDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObEnterpriseData;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/** @author Yara Ameen */
@Entity
@Table(name = "IObEnterpriseData")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "enterpriseType")
@EntityInterface(IIObEnterpriseData.class)
public class IObEnterpriseData extends DocumentHeader implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "enterpriseId")
  private Long enterpriseId;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString enterpriseName;

  private String enterpriseAddress;

  @OneToMany
  @JoinColumn(name = "refinstanceid", referencedColumnName = "enterpriseId")
  private List<IObEnterpriseAddressDetails> addressDetails;

  @OneToMany
  @JoinColumn(name = "refinstanceid", referencedColumnName = "enterpriseId")
  private List<IObEnterpriseContactDetails> contactsDetails;

  public List<IObEnterpriseContactDetails> getContactsDetails() {
    return contactsDetails;
  }

  public void setContactsDetails(List<IObEnterpriseContactDetails> contactsDetails) {
    this.contactsDetails = contactsDetails;
  }

  public List<IObEnterpriseAddressDetails> getAddressDetails() {
    return addressDetails;
  }

  public void setAddressDetails(List<IObEnterpriseAddressDetails> addressDetails) {
    this.addressDetails = addressDetails;
  }

  public Long getEnterpriseId() {
    return enterpriseId;
  }

  public void setEnterpriseId(Long enterpriseId) {
    this.enterpriseId = enterpriseId;
  }

  public LocalizedString getEnterpriseName() {
    if (this.enterpriseName != null) return enterpriseName;
    else return new LocalizedString();
  }

  public void setEnterpriseName(LocalizedString enterpriseName) {
    this.enterpriseName = enterpriseName;
  }

  public String getEnterpriseAddress() {
    return enterpriseAddress;
  }

  public void setEnterpriseAddress(String enterpriseAddress) {
    this.enterpriseAddress = enterpriseAddress;
  }

  @Override
  public String getSysName() {
    return IIObEnterpriseData.SYS_NAME;
  }
}
