package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dda.purchases.dbo.jpa.filterentities.FilterCriteria;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

public class LongFieldSpecification<T extends BusinessObject>
    implements Specification<T> {

  private FilterCriteria criteria;

  public LongFieldSpecification(FilterCriteria filterCriteria) {
    this.criteria = filterCriteria;
  }

  @Override
  public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
    if (criteria != null) {
      if (criteria.getOperation().equalsIgnoreCase("equals")) {
        Path<String> expression = root.get(criteria.getKey());
        String filterValue = this.criteria.getValue();
        return builder.equal(expression, Long.parseLong(filterValue));
      }
    }
    return null;
  }

}
