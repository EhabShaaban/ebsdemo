package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
@Entity
@Table(name = "DObOrderPaymentGeneralModel")
public class DObOrderPaymentGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {

  private String purchaseUnitCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String vendorCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString vendorName;

  private String currencyCode;
  private String currencyISO;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  private String companyCode;
  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString companyName;

  private String objectTypeCode;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public String getCompanyCode() {
    return companyCode;
  }

  public LocalizedString getCompanyName() {
    return companyName;
  }

  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
