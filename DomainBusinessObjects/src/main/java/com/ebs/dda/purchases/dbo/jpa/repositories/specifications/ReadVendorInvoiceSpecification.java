package com.ebs.dda.purchases.dbo.jpa.repositories.specifications;

import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IIObVendorInvoiceRemainingGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceRemainingGeneralModel;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class ReadVendorInvoiceSpecification {

  public Specification<IObVendorInvoiceRemainingGeneralModel> isAuthorizedPurchaseUnit(
          String purchaseUnitName) {
    return new Specification<IObVendorInvoiceRemainingGeneralModel>() {
      @Override
      public Predicate toPredicate(
              Root<IObVendorInvoiceRemainingGeneralModel> root,
              CriteriaQuery<?> query,
              CriteriaBuilder criteriaBuilder) {

        Expression<String> purchaseUnitEnglishValue =
                criteriaBuilder.function(
                        "json_extract_path_text",
                        String.class,
                        root.<String>get("purchaseUnitName"),
                        criteriaBuilder.literal("en"));
        return criteriaBuilder.equal(purchaseUnitEnglishValue, purchaseUnitName);
      }
    };
  }

  private Predicate whenStateIs(
          Root<IObVendorInvoiceRemainingGeneralModel> root, CriteriaBuilder criteriaBuilder, String state) {
    return criteriaBuilder.like(
            root.get(IStatefullBusinessObject.CURRENT_STATES_ATTR_NAME), "%" + state + "%");
  }

  private Predicate whenVendorCodeIs(
          Root<IObVendorInvoiceRemainingGeneralModel> root,
          CriteriaBuilder criteriaBuilder,
          String vendorCode) {
    Path<String> expression = root.get(IDObVendorInvoiceGeneralModel.VENDOR_CODE);
    return criteriaBuilder.equal(expression, vendorCode);
  }


  private Predicate wheBusinessUnitCodeIs(
          Root<IObVendorInvoiceRemainingGeneralModel> root,
          CriteriaBuilder criteriaBuilder,
          String businessUnitCode) {
    Path<String> expression = root.get(IDObVendorInvoiceGeneralModel.PURCHASE_UNIT_CODE);
    return criteriaBuilder.equal(expression, businessUnitCode);
  }

  private Predicate whenRemainingIsGreaterThanZero(
          Root<IObVendorInvoiceRemainingGeneralModel> root, CriteriaBuilder criteriaBuilder) {
    Path<Double> expression = root.get(IIObVendorInvoiceRemainingGeneralModel.REMAINING);
    return criteriaBuilder.greaterThan(expression, 0.0);
  }

  public Specification<IObVendorInvoiceRemainingGeneralModel> isAllowedDobInvoice(String vendorCode, String businessUnitCode) {
    return new Specification<IObVendorInvoiceRemainingGeneralModel>() {
      @Override
      public Predicate toPredicate(
              Root<IObVendorInvoiceRemainingGeneralModel> root,
              CriteriaQuery<?> query,
              CriteriaBuilder criteriaBuilder) {
        Predicate isPostedInvoice = whenStateIs(root, criteriaBuilder, "Posted");
        Predicate invoiceVendorCode = whenVendorCodeIs(root, criteriaBuilder, vendorCode);
        Predicate invoiceBusinessUnitCode = wheBusinessUnitCodeIs(root, criteriaBuilder, businessUnitCode);
        Predicate remainingGreaterThanZero = whenRemainingIsGreaterThanZero(root, criteriaBuilder);
        return criteriaBuilder.and(isPostedInvoice, invoiceVendorCode, remainingGreaterThanZero, invoiceBusinessUnitCode);
      }
    };
  }
}
