package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis;

import com.ebs.dac.dbo.jpa.entities.interfaces.IInformationObject;
import com.ebs.dac.dbo.processing.CompositeAttribute;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetailsQuantities;

/** @author Yara Ameen */
public interface IIObOrderLineDetails extends IInformationObject {

  String SYS_NAME = "OrderLine";

  String ITEM_QUANTITIES_ATTR_NAME = "itemQuantities";
  CompositeAttribute<IObOrderLineDetailsQuantities> itemQuantitiesAttr =
      new CompositeAttribute<>(
          ITEM_QUANTITIES_ATTR_NAME, IObOrderLineDetailsQuantities.class, true);
}
