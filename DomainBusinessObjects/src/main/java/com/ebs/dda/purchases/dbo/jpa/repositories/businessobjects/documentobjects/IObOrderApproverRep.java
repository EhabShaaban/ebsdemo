package com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.InformationObjectRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import java.util.List;

public interface IObOrderApproverRep extends InformationObjectRep<IObOrderApprover> {

  List<IObOrderApprover> findByRefInstanceIdOrderByIdAsc(Long refInstanceId);

}
