package com.ebs.dda.purchases.dbo.jpa.valueobjects;

import org.joda.time.DateTime;

public class DObPurchaseOrderMarkAsArrivedValueObject {
  private DateTime arrivalDateTime;
  private String purchaseOrderCode;

  public DateTime getArrivalDateTime() {
    return arrivalDateTime;
  }

  public void setArrivalDateTime(DateTime arrivalDateTime) {
    this.arrivalDateTime = arrivalDateTime;
  }

  public String getPurchaseOrderCode() {
    return purchaseOrderCode;
  }

  public void setPurchaseOrderCode(String purchaseOrderCode) {
    this.purchaseOrderCode = purchaseOrderCode;
  }
}
