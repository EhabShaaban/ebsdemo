package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface IObOrderCompanyGeneralModelRep
        extends JpaRepository<IObOrderCompanyGeneralModel, Long>,
        JpaSpecificationExecutor<IObOrderCompanyGeneralModel> {

  IObOrderCompanyGeneralModel findByUserCode(String code);

  IObOrderCompanyGeneralModel findByUserCodeAndPartyRole(String code, String partyRole);

}
