package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IDObPurchaseOrderAssignMeToApproveValueObject {


  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String MAIN_USER = "mainUser";
  String CONTROL_POINT_CODE = "controlPointCode";

}
