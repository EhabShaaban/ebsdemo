package com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemSummaryGeneralModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IObOrderItemSummaryGeneralModelRep
        extends JpaRepository<IObOrderItemSummaryGeneralModel, Long> {

    IObOrderItemSummaryGeneralModel findOneByOrderCode(String purchaseOrderCode);
}
