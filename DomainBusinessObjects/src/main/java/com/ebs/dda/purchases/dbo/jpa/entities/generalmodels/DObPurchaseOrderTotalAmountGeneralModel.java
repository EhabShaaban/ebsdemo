package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DObPurchaseOrderTotalAmountGeneralModel")
public class DObPurchaseOrderTotalAmountGeneralModel
    extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "orderTypeName")
  private LocalizedString orderTypeName;

  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "vendorName")
  private LocalizedString vendorName;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringConverter.class)
  @Column(name = "purchaseUnitName")
  private LocalizedString purchaseUnitName;

  @Column(name = "vendorCode")
  private String vendorCode;

  @Column(name = "purchaseUnitNameEn")
  private String purchaseUnitNameEn;

  @Column(name = "purchaseUnitCode")
  private String purchaseUnitCode;

  private String currencyCode;
  private String currencyISO;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  public String getPurchaseUnitNameEn() {
    return purchaseUnitNameEn;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public LocalizedString getOrderTypeName() {
    return orderTypeName;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public void setPurchaseUnitName(LocalizedString purchaseUnitName) {
    this.purchaseUnitName = purchaseUnitName;
  }

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  @Override
  public String getSysName() {
    return IDObPurchaseOrder.SYS_NAME;
  }
}
