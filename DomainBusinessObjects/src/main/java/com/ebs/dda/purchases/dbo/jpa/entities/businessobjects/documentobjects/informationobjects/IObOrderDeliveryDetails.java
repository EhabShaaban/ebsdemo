package com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects;

import com.ebs.dac.dbo.api.annotations.EmbeddedAttribute;
import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.JodaTimeSQLTimestampJpaConverter;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringJsonConverter;
import com.ebs.dac.dbo.jpa.entities.businessobjects.DocumentHeader;
import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.apis.IIObOrderDeliveryDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderDateTypeEnum;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @auther: Maged Zakzouk
 * @date: Nov 27, 2017 2:41:58 PM
 */
@Entity
@Table(name = "IObOrderDeliveryDetails")
@EntityInterface(IIObOrderDeliveryDetails.class)
public class IObOrderDeliveryDetails extends DocumentHeader implements Serializable {
  private static final long serialVersionUID = 1L;

  @Convert(converter = JodaTimeSQLTimestampJpaConverter.class)
  @Column(name = "collectionDate")
  private DateTime collectionDate;

  @Column(name = "numberOfDays")
  private Integer numberOfDays;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "deliveryDate"))
  private OrderDateTypeEnum deliveryDate;

  @Column(name = "incotermId")
  private Long incotermRef;

  private String incotermName;

  @Column(name = "modeOfTransportId")
  private Long modeOfTransportRef;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringJsonConverter.class)
  private LocalizedString modeOfTransportName;

  @Column(name = "shippingInstructionsId")
  private Long shippingInstructionsRef;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringJsonConverter.class)
  private LocalizedString shippingInstructionsName;

  @Column(name = "loadingPortId")
  private Long loadingPortRef;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringJsonConverter.class)
  private LocalizedString loadingPortName;

  @Column(name = "dischargePortId")
  private Long dischargePortRef;

  @EmbeddedAttribute
  @Convert(converter = LocalizedStringJsonConverter.class)
  private LocalizedString dischargePortName;

  public IObOrderDeliveryDetails() {
    deliveryDate = new OrderDateTypeEnum();

    shippingInstructionsName = new LocalizedString();

    modeOfTransportName = new LocalizedString();

    dischargePortName = new LocalizedString();

    loadingPortName = new LocalizedString();
  }

  public DateTime getCollectionDate() {
    return collectionDate;
  }

  public void setCollectionDate(DateTime collectionDate) {
    this.collectionDate = collectionDate;
  }

  public Integer getNumberOfDays() {
    return numberOfDays;
  }

  public void setNumberOfDays(Integer numberOfDays) {
    this.numberOfDays = numberOfDays;
  }

  public Long getModeOfTransportRef() {
    return modeOfTransportRef;
  }

  public void setModeOfTransportRef(Long modeOfTransportRef) {
    this.modeOfTransportRef = modeOfTransportRef;
  }

  public Long getShippingInstructionsRef() {
    return shippingInstructionsRef;
  }

  public void setShippingInstructionsRef(Long shippingInstructionsRef) {
    this.shippingInstructionsRef = shippingInstructionsRef;
  }

  public Long getLoadingPortRef() {
    return loadingPortRef;
  }

  public void setLoadingPortRef(Long loadingPortRef) {
    this.loadingPortRef = loadingPortRef;
  }

  public Long getDischargePortRef() {
    return dischargePortRef;
  }

  public void setDischargePortRef(Long dischargePortRef) {
    this.dischargePortRef = dischargePortRef;
  }

  public OrderDateTypeEnum getDeliveryDate() {
    if (this.deliveryDate == null) {
      deliveryDate = new OrderDateTypeEnum();
    }
    return deliveryDate;
  }

  public void setDeliveryDate(OrderDateTypeEnum deliveryDate) {
    this.deliveryDate = deliveryDate;
  }

  public Long getIncotermRef() {
    return incotermRef;
  }

  public void setIncotermRef(Long incotermRef) {
    this.incotermRef = incotermRef;
  }

  public String getIncotermName() {

    return incotermName;
  }

  public void setIncotermName(String incotermName) {
    this.incotermName = incotermName;
  }

  public LocalizedString getModeOfTransportName() {
    if (modeOfTransportName == null) {
      modeOfTransportName = new LocalizedString();
    }
    return modeOfTransportName;
  }

  public void setModeOfTransportName(LocalizedString modeOfTransportName) {
    this.modeOfTransportName = modeOfTransportName;
  }

  public LocalizedString getShippingInstructionsName() {
    if (shippingInstructionsName == null) {
      shippingInstructionsName = new LocalizedString();
    }
    return shippingInstructionsName;
  }

  public void setShippingInstructionsName(LocalizedString shippingInstructionsName) {
    this.shippingInstructionsName = shippingInstructionsName;
  }

  public LocalizedString getLoadingPortName() {
    if (loadingPortName == null) {
      loadingPortName = new LocalizedString();
    }
    return loadingPortName;
  }

  public void setLoadingPortName(LocalizedString loadingPortName) {
    // if (loadingPortName == null) {
    // this.loadingPortName = null;
    // return;
    // }
    this.loadingPortName = loadingPortName;
  }

  public LocalizedString getDischargePortName() {
    if (dischargePortName == null) {
      dischargePortName = new LocalizedString();
    }
    return dischargePortName;
  }

  public void setDischargePortName(LocalizedString dischargePortName) {
    // if (dischargePortName == null) {
    // this.dischargePortName = null;
    // return;
    // }
    this.dischargePortName = dischargePortName;
  }

  @Override
  public String getSysName() {
    return IIObOrderDeliveryDetails.SYS_NAME;
  }
}
