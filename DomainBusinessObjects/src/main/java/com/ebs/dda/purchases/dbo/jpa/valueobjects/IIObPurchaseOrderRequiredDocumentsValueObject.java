package com.ebs.dda.purchases.dbo.jpa.valueobjects;

public interface IIObPurchaseOrderRequiredDocumentsValueObject {
  String PURCHASE_ORDER_CODE = "purchaseOrderCode";
  String PURCHASE_ORDER_DOCUMENTS = "purchaseOrderDocuments";
}
