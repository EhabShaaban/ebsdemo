package com.ebs.dda.purchases.dbo.jpa.entities.generalmodels;

import com.ebs.dac.dbo.api.processing.EntityInterface;
import com.ebs.dac.dbo.jpa.converters.LocalizedStringConverter;
import com.ebs.dac.dbo.types.LocalizedString;
import java.math.BigDecimal;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EntityInterface(IDObPurchaseOrderGeneralModel.class)
@Table(name = "DObPurchaseOrderGeneralModel")
public class DObPurchaseOrderGeneralModel extends DObOrderDocumentGeneralModel {

  private String purchaseUnitCode;
  private String refDocumentCode;
  private String refDocumentType;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString purchaseUnitName;

  private String vendorCode;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString vendorName;

  private String currencyCode;
  private String currencyISO;

  @Convert(converter = LocalizedStringConverter.class)
  private LocalizedString currencyName;

  private BigDecimal remaining;

  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  public LocalizedString getPurchaseUnitName() {
    return purchaseUnitName;
  }

  public String getVendorCode() {
    return vendorCode;
  }

  public LocalizedString getVendorName() {
    return vendorName;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public String getCurrencyISO() {
    return currencyISO;
  }

  public LocalizedString getCurrencyName() {
    return currencyName;
  }

  public String getRefDocumentCode() {
    return refDocumentCode;
  }

  public String getRefDocumentType() {
    return refDocumentType;
  }

  public BigDecimal getRemaining() {
    return remaining;
  }
}
