package com.ebs.dac.infrastructure.jpa.repositories;

import org.springframework.data.repository.Repository;

/** Created by marisoft on 11/6/16. */
public interface IJpaRepositoryBuilder<BO> {
  <R extends Repository<T, Long>, T extends BO> R createBusinessObjectRepository(
      Class<R> rClass, Class<T> tClass);

  <R extends Repository<T, Long>, T extends BO> R createRepository(
      Class<R> rClass, Class<T> tClass);
}
