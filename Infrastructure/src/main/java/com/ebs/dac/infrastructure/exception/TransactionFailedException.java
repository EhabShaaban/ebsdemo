package com.ebs.dac.infrastructure.exception;

public class TransactionFailedException extends RuntimeException implements IExceptionResponse {

  private static final long serialVersionUID = 1L;

  public static final String INTERNAL_SERVER_ERROR = "Gen-msg-55";

  public TransactionFailedException(Exception e) {
    super(e);
  }

  @Override
  public String getResponseCode() {
    return INTERNAL_SERVER_ERROR;
  }

}
