package com.ebs.dac.infrastructure.tm;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import com.ebs.dac.infrastructure.exception.TransactionFailedException;

public class TransactionManagerDelegate {
  private TransactionTemplate transactionTemplate;
  private PlatformTransactionManager platformTransactionManager;

  public void setTransactionManager(PlatformTransactionManager transactionManager) {
    this.platformTransactionManager = transactionManager;
  }

  public void executeTransaction(TransactionCallback transactionCallback) {
    transactionTemplate = new TransactionTemplate(platformTransactionManager);

    transactionTemplate.execute(new TransactionCallbackWithoutResult() {
      @Override
      protected void doInTransactionWithoutResult(TransactionStatus status) {
        try {
          transactionCallback.doTransaction();
        } catch (Exception e) {
          status.setRollbackOnly();
          throw new TransactionFailedException(e);
        }
      }
    });
  }

}


