package com.ebs.dac.infrastructure.jpa.repositories;

import javax.persistence.EntityManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IBusinessObjectRepository<T> extends JpaRepository<T, Long> {
  EntityManager getEntityManager();

  void detach(T obj) throws Exception;

  T create(T obj) throws Exception;

  T update(T obj) throws Exception;

  T updateAndClear(T obj) throws Exception;
}
