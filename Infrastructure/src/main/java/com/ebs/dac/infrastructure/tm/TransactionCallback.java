package com.ebs.dac.infrastructure.tm;

public abstract class TransactionCallback {

  protected abstract void doTransaction() throws Exception;
}
