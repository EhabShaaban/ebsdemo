package com.ebs.dac.infrastructure.exception;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 25, 2018 3:23:09 PM
 */
public interface IExceptionResponse {
  int SECURITY_VIOLATION_HTTP_STATUS_CODE = 460;
  int CONDITIONAL_AUTH_HTTP_STATUS_CODE = 403;
  int DEFAULT_EXCEPTION_HTTP_STATUS_CODE = 400;

  String getResponseCode();

  default int getHttpStatusCode() {
    return 400;
  }
}
