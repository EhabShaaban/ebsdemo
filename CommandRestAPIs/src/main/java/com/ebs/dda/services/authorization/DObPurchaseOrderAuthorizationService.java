package com.ebs.dda.services.authorization;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.lookups.IMasterDataLookupsSysNames;
import com.ebs.dda.jpa.masterdata.paymentterms.ICObPaymentTerms;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.configurationobjects.apis.ICObShippingInstructions;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.apis.IShippingLookupsSysNames;

public class DObPurchaseOrderAuthorizationService extends AuthorizationService {

  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;

  public void applyAuthorizationOnSavePaymentAndDelivery(String purchaseOrderCode)
      throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);
    applyAuthorizationOnPaymentAndeDeliverySectionFields();
  }

  private void applyAuthorizationOnPaymentAndeDeliverySectionFields() {
    authorizationManager.authorizeAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObShippingInstructions.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObPaymentTerms.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        IMasterDataLookupsSysNames.LOB_INCOTERMS_SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        IMasterDataLookupsSysNames.LOB_CONTAINER_REQUIRMENT_SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        IMasterDataLookupsSysNames.LOB_PORT_SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        IShippingLookupsSysNames.LOB_MODE_OF_TRANSPORT_SYS_NAME, IActionsNames.READ_ALL);
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

}
