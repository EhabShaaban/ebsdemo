package com.ebs.dda.services.authorization;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;

public class AuthorizationService {

  protected AuthorizationManager authorizationManager;

  public void setAuthorizationManager(
      AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void checkAuthorizeForActionOnBusinessObject(
      BusinessObject orderGeneralModel, String actionName) throws Exception {
    try {
      checkIfEntityIsNull(orderGeneralModel);
      authorizationManager.authorizeAction(orderGeneralModel, actionName);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException();
      authorizationException.initCause(ex);
      throw authorizationException;
    }
  }

  private void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }


}
