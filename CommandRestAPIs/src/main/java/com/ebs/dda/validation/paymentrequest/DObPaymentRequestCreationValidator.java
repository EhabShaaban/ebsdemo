package com.ebs.dda.validation.paymentrequest;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
public abstract class DObPaymentRequestCreationValidator {

	protected ValidationResult validationResult;
	private CObPurchasingUnitGeneralModelRep purchasingUnitRep;
	private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;

	public ValidationResult validate(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		validationResult = new ValidationResult();
		checkDocumentOwner(creationValueObject.getDocumentOwnerId());
		checkPurchaseUnitData(creationValueObject.getBusinessUnitCode());
		return validationResult;
	}

	private void checkDocumentOwner(Long documentOwnerId) {
		checkIfObjectIsNull(documentOwnerId);
		DocumentOwnerGeneralModel documentOwner = documentOwnerGeneralModelRep
						.findOneByUserIdAndObjectName(documentOwnerId, IDObPaymentRequest.SYS_NAME);
		if (documentOwner == null) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkPurchaseUnitData(String purchaseUnitCode) {
		CObPurchasingUnitGeneralModel purchaseUnit = purchasingUnitRep
						.findOneByUserCode(purchaseUnitCode);
		if (purchaseUnit == null) {
			throw new ArgumentViolationSecurityException();
		}
	}

	public void setPurchasingUnitRep(CObPurchasingUnitGeneralModelRep purchasingUnitRep) {
		this.purchasingUnitRep = purchasingUnitRep;
	}
	public void setDocumentOwnerGeneralModelRep(
					DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
		this.documentOwnerGeneralModelRep = documentOwnerGeneralModelRep;
	}
}
