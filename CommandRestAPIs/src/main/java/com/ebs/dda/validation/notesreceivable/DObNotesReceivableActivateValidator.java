package com.ebs.dda.validation.notesreceivable;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableActivateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesReferenceDocumentsValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.util.List;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

public class DObNotesReceivableActivateValidator
    implements IActivateBusinessRulesValidator<DObNotesReceivableActivateValueObject> {

  private ValidationResult validationResult = new ValidationResult();
  private IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep;
  private CObFiscalYearRep fiscalYearRep;

  private IObNotesReceivableRefDocumentBusinessValidator
      notesReceivableRefDocumentBusinessValidator;

  @Override
  public ValidationResult validateBusinessRules(DObNotesReceivableActivateValueObject valueObject)
      throws RuntimeException {
    final boolean isSavedRefDocBefore = true;

    String notesReceivableCode = valueObject.getNotesReceivableCode();

    validateRefDocuments(isSavedRefDocBefore, notesReceivableCode);
    checkMandatoriesExist(valueObject);
    checkThatJournalEntryDateInActiveFiscalYear(valueObject.getJournalEntryDate());
    return validationResult;
  }

  private void validateRefDocuments(boolean isSavedRefDocBefore, String notesReceivableCode) {
    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocumentGeneralModels =
        monetaryNotesReferenceDocumentsRep.findByNotesReceivableCodeOrderByIdAsc(
                notesReceivableCode);

    for (IObNotesReceivablesReferenceDocumentGeneralModel referenceDocumentGeneralModel :
        referenceDocumentGeneralModels) {

      IObMonetaryNotesReferenceDocumentsValueObject refDocumentvalueObject = getMonetaryNotesReferenceDocumentsValueObject(notesReceivableCode, referenceDocumentGeneralModel);

      notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
          refDocumentvalueObject, isSavedRefDocBefore, validationResult);
    }
  }

  private IObMonetaryNotesReferenceDocumentsValueObject getMonetaryNotesReferenceDocumentsValueObject(String notesReceivableCode, IObNotesReceivablesReferenceDocumentGeneralModel referenceDocumentGeneralModel) {
    IObMonetaryNotesReferenceDocumentsValueObject refDocumentvalueObject =
        new IObMonetaryNotesReferenceDocumentsValueObject();
    refDocumentvalueObject.setNotesReceivableCode(notesReceivableCode);
    refDocumentvalueObject.setRefDocumentCode(referenceDocumentGeneralModel.getRefDocumentCode());
    refDocumentvalueObject.setDocumentType(referenceDocumentGeneralModel.getDocumentType());
    refDocumentvalueObject.setAmountToCollect(referenceDocumentGeneralModel.getAmountToCollect());
    return refDocumentvalueObject;
  }

  private void checkThatJournalEntryDateInActiveFiscalYear(String journalEntryDate) {
    DateTime dueDate = getDateTime(journalEntryDate);

    boolean isValidDueDate =
        fiscalYearRep.findByCurrentStatesLike("%" + BasicStateMachine.ACTIVE_STATE + "%").stream()
            .anyMatch(year -> isJournalDateInActiveFiscalYear(year, dueDate));

    if (!isValidDueDate) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private boolean isJournalDateInActiveFiscalYear(CObFiscalYear year, DateTime dueDate) {
    return dueDate.isAfter(year.getFromDate()) && dueDate.isBefore(year.getToDate());
  }

  private void checkMandatoriesExist(DObNotesReceivableActivateValueObject valueObject)
      throws ArgumentViolationSecurityException {
    checkIfObjectIsNull(valueObject.getJournalEntryDate());
  }

  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

  public void setNotesReceivableRefDocumentBusinessValidator(
      IObNotesReceivableRefDocumentBusinessValidator notesReceivableRefDocumentBusinessValidator) {
    this.notesReceivableRefDocumentBusinessValidator = notesReceivableRefDocumentBusinessValidator;
  }

  public void setMonetaryNotesReferenceDocumentsRep(
      IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep) {
    this.monetaryNotesReferenceDocumentsRep = monetaryNotesReferenceDocumentsRep;
  }
}
