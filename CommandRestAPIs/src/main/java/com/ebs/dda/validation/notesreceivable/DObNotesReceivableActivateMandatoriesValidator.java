package com.ebs.dda.validation.notesreceivable;

import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesSectionNames;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.accounting.notesreceivables.validation.apis.INotesReceivableDetailsMandatoryAttributes;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableTypeEnum;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.*;

public class DObNotesReceivableActivateMandatoriesValidator implements IMissingFieldsValidator {

    private IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsRep;
    private IObNotesReceivablesReferenceDocumentGeneralModelRep notesReceivablesReferenceDocumentRep;
    private final MandatoryValidatorUtils mandatoryValidatorUtils;

    public DObNotesReceivableActivateMandatoriesValidator() {
        this.mandatoryValidatorUtils = new MandatoryValidatorUtils();
    }

    public  List<String> validateAllSectionsForState(String nrCode, String state)
            throws RuntimeException {
        List<String> sectionsMissingFields = new LinkedList<>();
        List<String> detailsMissingFields = validateGeneralModelMandatories(nrCode, state);
        if (!detailsMissingFields.isEmpty()) {
            sectionsMissingFields.addAll(detailsMissingFields);
        }
        if (!notesReceivablesHasReferenceDocument(nrCode)) {
            sectionsMissingFields.addAll(
                    Arrays.asList(IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION));
        }
        return sectionsMissingFields;
    }

    public List<String> validateGeneralModelMandatories(String nrCode, String state) throws RuntimeException {
        IObMonetaryNotesDetailsGeneralModel monetaryNotesDetails = monetaryNotesDetailsRep
                .findByUserCodeAndObjectTypeCode(nrCode, NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION.name());
        String[] mandatoryAttributes = mandatoryValidatorUtils.getMandatoryAttributesByState(
                INotesReceivableDetailsMandatoryAttributes.MANDATORIES, state);
        return mandatoryValidatorUtils.validateMandatoriesForGeneralModel(mandatoryAttributes, monetaryNotesDetails);
    }

    public boolean notesReceivablesHasReferenceDocument(String nrCode) {
        List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments =
                notesReceivablesReferenceDocumentRep.findByNotesReceivableCodeOrderByIdAsc(nrCode);
        return (referenceDocuments.size() > 0);
    }

    @Override
    public Map<String, Object> validateMissingFields(String userCode, String state) throws RuntimeException {
        Map<String, Object> missingFields = new HashMap<>();
        mandatoryValidatorUtils.populateSectionMissingFieldstoResult(missingFields, DObNotesReceivablesStateMachine.ACTIVE_STATE,
                validateAllSectionsForState(userCode, DObNotesReceivablesStateMachine.ACTIVE_STATE));
        return missingFields;
    }

    public void setMonetaryNotesDetailsRep(IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsRep) {
        this.monetaryNotesDetailsRep = monetaryNotesDetailsRep;
    }

    public void setNotesReceivablesReferenceDocumentRep(IObNotesReceivablesReferenceDocumentGeneralModelRep notesReceivablesReferenceDocumentRep) {
        this.notesReceivablesReferenceDocumentRep = notesReceivablesReferenceDocumentRep;
    }
}
