package com.ebs.dda.validation.paymentrequest;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRep;

public abstract class DObPaymentForVendorCreationValidator extends DObPaymentRequestCreationValidator {

	private MObVendorGeneralModelRep vendorGeneralModelRep;
	private MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep;

	public ValidationResult validate(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		super.validate(creationValueObject);
		checkBusinessPartner(creationValueObject);
		checkDueDocumentType(creationValueObject);
		checkReferenceDocument(creationValueObject);
		return validationResult;
	}
	private void checkReferenceDocument(DObPaymentRequestCreateValueObject creationValueObject) {
		String purchaseOrderCode = creationValueObject.getDueDocumentCode();
		checkIfObjectIsNull(purchaseOrderCode);
	}
	private void checkDueDocumentType(DObPaymentRequestCreateValueObject creationValueObject) {
		checkIfObjectIsNull(creationValueObject.getDueDocumentType());
	}

	private void checkBusinessPartner(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		checkIfObjectIsNull(creationValueObject.getBusinessPartnerCode());
		checkIfBusinessPartnerCodeIsCorrect(creationValueObject);
	}

	private void checkIfBusinessPartnerCodeIsCorrect(DObPaymentRequestCreateValueObject creationValueObject) throws Exception {
	  String businessPartnerCode = creationValueObject.getBusinessPartnerCode();
		MObVendorGeneralModel vendor = vendorGeneralModelRep.findOneByUserCode(businessPartnerCode);
		if (vendor != null) {
			checkIfVendorHasBusinessUnitAllowed(creationValueObject);
		} else {
			validationResult.bindError(IDObPaymentRequestCreateValueObject.BUSINESSPARTNER_CODE,
							IExceptionsCodes.INCORRECT_FIELD_INPUT);
		}
	}

	private void checkIfVendorHasBusinessUnitAllowed(
					DObPaymentRequestCreateValueObject creationValueObject) {
		String businessPartnerCode = creationValueObject.getBusinessPartnerCode();
		String businessUnitCode = creationValueObject.getBusinessUnitCode();
		MObVendorPurchaseUnitGeneralModel vendorPurchaseUnitGeneralModel = vendorPurchaseUnitGeneralModelRep
						.findOneByUserCodeAndPurchaseUnitCode(businessPartnerCode,
										businessUnitCode);
		if (vendorPurchaseUnitGeneralModel == null)
			throw new AuthorizationException();
	}

	public void setVendorGeneralModelRep(MObVendorGeneralModelRep vendorGeneralModelRep) {
		this.vendorGeneralModelRep = vendorGeneralModelRep;
	}

	public void setVendorPurchaseUnitGeneralModelRep(
					MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep) {
		this.vendorPurchaseUnitGeneralModelRep = vendorPurchaseUnitGeneralModelRep;
	}
}
