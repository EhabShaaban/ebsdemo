package com.ebs.dda.validation.vendorinvoice;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import com.ebs.dda.accounting.vendorinvoice.exceptions.DObVendorInvoiceMissingItemAccountException;
import com.ebs.dda.accounting.vendorinvoice.exceptions.DObVendorInvoiceMissingVendorAccountException;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceActivateValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.IObItemAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import com.ebs.dda.validation.ValidationUtils;
import org.joda.time.DateTime;

import java.util.List;

public abstract class DObVendorInvoiceActivateValidator implements IActivateBusinessRulesValidator<DObVendorInvoiceActivateValueObject> {

  protected ValidationResult validationResult;
  protected IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep;
  protected IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep;
  protected IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoGeneralModelRep;
  protected IObItemAccountingInfoGeneralModelRep itemAccountingInfoGeneralModelRep;
  protected CObFiscalYearRep fiscalYearRep;
  protected ValidationUtils utils;
  protected IObVendorInvoiceDetailsGeneralModel invoiceDetails;

  public DObVendorInvoiceActivateValidator(){
    utils = new ValidationUtils();
  }
  public void checkIfVendorOfInvoiceHasAccount(String vendorCode) {
    IObVendorAccountingInfoGeneralModel accountingInfoGeneralModel =
        vendorAccountingInfoGeneralModelRep.findByVendorCode(vendorCode);
    if (accountingInfoGeneralModel == null) {
      throw new DObVendorInvoiceMissingVendorAccountException(validationResult);
    }
  }

  public void checkIfItemOfInvoiceHasAccount(String itemCode) {
    IObItemAccountingInfoGeneralModel itemAccountingInfoGeneralModel =
        itemAccountingInfoGeneralModelRep.findByItemCode(itemCode);
    if (itemAccountingInfoGeneralModel == null) {
      throw new DObVendorInvoiceMissingItemAccountException(validationResult);

    }
  }

  public void setInvoiceItemsGeneralModelRep(
      IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep) {
    this.invoiceItemsGeneralModelRep = invoiceItemsGeneralModelRep;
  }

  public void setInvoiceDetailsGeneralModelRep(
      IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep) {
    this.invoiceDetailsGeneralModelRep = invoiceDetailsGeneralModelRep;
  }

  public void setVendorAccountingInfoGeneralModelRep(
      IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoGeneralModelRep) {
    this.vendorAccountingInfoGeneralModelRep = vendorAccountingInfoGeneralModelRep;
  }

  public void setItemAccountingInfoGeneralModelRep(
      IObItemAccountingInfoGeneralModelRep itemAccountingInfoGeneralModelRep) {
    this.itemAccountingInfoGeneralModelRep = itemAccountingInfoGeneralModelRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

  @Override
  public ValidationResult validateBusinessRules(DObVendorInvoiceActivateValueObject valueObject) throws RuntimeException {
    validationResult = new ValidationResult();
    invoiceDetails = invoiceDetailsGeneralModelRep.findByInvoiceCode(valueObject.getVendorInvoiceCode());
    checkIfVendorOfInvoiceHasAccount(invoiceDetails.getVendorCode());

    List<IObVendorInvoiceItemsGeneralModel> invoiceItems =
            invoiceItemsGeneralModelRep
                    .findByInvoiceCodeOrderByIdAsc(valueObject.getVendorInvoiceCode());

    for (IObVendorInvoiceItemsGeneralModel invoiceItem : invoiceItems) {
      checkIfItemOfInvoiceHasAccount(invoiceItem.getItemCode());
    }
    checkThatJournalDateIsValid(valueObject);
    return validationResult;  }

  private void checkThatJournalDateIsValid(DObVendorInvoiceActivateValueObject valueObject) {
    String journalDate = valueObject.getJournalDate();
    DateTime dueDate = utils.getDateTime(journalDate);
    String dueDateYear = Integer.toString(dueDate.getYear());
    List<CObFiscalYear> activeFiscalYears = fiscalYearRep.findByCurrentStatesLike("%Active%");
    CObFiscalYear fiscalYear = activeFiscalYears.stream()
            .filter(activeFiscalYear -> dueDateYear.equals(activeFiscalYear.getName().getValue(ISysDefaultLocales.ENGLISH_LANG_KEY)))
            .findAny()
            .orElse(null);

    if (fiscalYear == null ){
      throw new ArgumentViolationSecurityException();
    }

  }
}
