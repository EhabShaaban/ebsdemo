package com.ebs.dda.validation.salesorder;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemValueObject;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderItemsGeneralModelRep;

public class IObSalesOrderItemSaveAddValidator {
  private ValidationResult validationResult;
  private IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep;

  public ValidationResult validate(IObSalesOrderItemValueObject valueObject) throws Exception {
    validationResult = new ValidationResult();

    validateThatItemAndUnitOfMeasureWillBeAddedOnce(valueObject);

    return validationResult;
  }

  private void validateThatItemAndUnitOfMeasureWillBeAddedOnce(
      IObSalesOrderItemValueObject valueObject) throws ArgumentViolationSecurityException {

    IObSalesOrderItemGeneralModel salesOrderItem =
        salesOrderItemsGeneralModelRep.findOneBySalesOrderCodeAndItemCodeAndUnitOfMeasureCode(
            valueObject.getSalesOrderCode(),
            valueObject.getItemCode(),
            valueObject.getUnitOfMeasureCode());

    if (salesOrderItem != null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setSalesOrderItemsGeneralModelRep(
      IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep) {
    this.salesOrderItemsGeneralModelRep = salesOrderItemsGeneralModelRep;
  }
}
