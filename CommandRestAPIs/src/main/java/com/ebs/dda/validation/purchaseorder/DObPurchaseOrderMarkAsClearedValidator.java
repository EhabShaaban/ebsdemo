package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderCycleDatesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderClearanceValueObject;
import java.util.Arrays;
import org.joda.time.DateTime;

public class DObPurchaseOrderMarkAsClearedValidator
    extends DObPurchaseOrderStateTransitionCommonValidator {

  private ValidationResult validationResult;

  public ValidationResult validate(String purchaseOrderCode, DateTime clearanceDateTime)
      throws Exception {
    validationResult = new ValidationResult();

    DObPurchaseOrder dObPurchaseOrder = getPurchaseOrderRep().findOneByUserCode(purchaseOrderCode);

    if (dObPurchaseOrder == null) {
      throw new ArgumentViolationSecurityException();
    }

    if (dObPurchaseOrder.getCurrentStates().contains("Draft")) {
      throw new ArgumentViolationSecurityException();
    }

    DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel =
        getpurchaseOrderCycleDatesGeneralModelRep().findOneByUserCode(purchaseOrderCode);

    if (purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getArrivalDate() != null
        && !isValidTransitionDateTime(
            purchaseOrderCycleDatesGeneralModel.getArrivalDate(), clearanceDateTime)) {

      validationResult.bindErrorWithValues(
          IDObPurchaseOrderClearanceValueObject.CLEARANCE_DATE_TIME,
          IExceptionsCodes.INVALID_STATE_DATE,
          Arrays.asList(
              getDateWithUserDefaultTimeZone(
                  purchaseOrderCycleDatesGeneralModel.getArrivalDate())));
    }

    return validationResult;
  }
}
