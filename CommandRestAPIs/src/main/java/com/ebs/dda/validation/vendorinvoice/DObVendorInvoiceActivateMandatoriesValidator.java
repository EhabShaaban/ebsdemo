package com.ebs.dda.validation.vendorinvoice;

import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.accounting.vendorinvoice.validation.apis.IIObInvoiceDetailsMandatoryAttributes;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.*;

public class DObVendorInvoiceActivateMandatoriesValidator implements IMissingFieldsValidator {

    private IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep;
    private IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep;
    private final MandatoryValidatorUtils mandatoryValidatorUtils;

    public DObVendorInvoiceActivateMandatoriesValidator() {
        this.mandatoryValidatorUtils = new MandatoryValidatorUtils();
    }

    public  List<String> validateAllSectionsForState(String invoiceCode, String state)
            throws RuntimeException {
        List<String> sectionsMissingFields = new LinkedList<>();
        List<String> detailsMissingFields = validateGeneralModelMandatories(invoiceCode, state);
        if (!detailsMissingFields.isEmpty()) {
            sectionsMissingFields.addAll(detailsMissingFields);
        }
        if (!invoiceHasItems(invoiceCode)) {
            sectionsMissingFields.addAll(
                    Arrays.asList(IDObVendorInvoiceSectionNames.ITEMS_SECTION));
        }
        return sectionsMissingFields;
    }

    public List<String> validateGeneralModelMandatories(String invoiceCode, String state)
            throws RuntimeException {
        IObVendorInvoiceDetailsGeneralModel invoiceDetailsGeneralModel =
                invoiceDetailsGeneralModelRep.findByInvoiceCode(invoiceCode);
        String[] mandatoryAttributes =
                mandatoryValidatorUtils.getMandatoryAttributesByState(
                        IIObInvoiceDetailsMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL, state);
        return mandatoryValidatorUtils.validateMandatoriesForGeneralModel(
                mandatoryAttributes, invoiceDetailsGeneralModel);
    }

    public boolean invoiceHasItems(String invoiceCode) {
        List<IObVendorInvoiceItemsGeneralModel> invoiceItems =
                invoiceItemsGeneralModelRep.findByInvoiceCodeOrderByIdAsc(invoiceCode);
        return (invoiceItems.size() > 0);
    }

    public void setInvoiceItemsGeneralModelRep(
            IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep) {
        this.invoiceItemsGeneralModelRep = invoiceItemsGeneralModelRep;
    }
    public void setInvoiceDetailsGeneralModelRep(
            IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep) {
        this.invoiceDetailsGeneralModelRep = invoiceDetailsGeneralModelRep;
    }

    @Override
    public Map<String, Object> validateMissingFields(String userCode, String state) throws RuntimeException {
        Map<String, Object> missingFields = new HashMap<>();
        mandatoryValidatorUtils.populateSectionMissingFieldstoResult(missingFields, DObVendorInvoiceStateMachine.POSTED_STATE,
                validateAllSectionsForState(userCode, DObVendorInvoiceStateMachine.POSTED_STATE));
        return missingFields;
    }
}
