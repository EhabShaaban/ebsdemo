package com.ebs.dda.validation.salesorder;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.masterdata.storehouse.CObStorehouseGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCompanyAndStoreValueObject;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

public class IObSalesOrderCompanyAndStoreDataMandatoryValidator {

  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private CObStorehouseGeneralModelRep storehouseGeneralModelRep;

  public void validate(IObSalesOrderCompanyAndStoreValueObject valueObject)
      throws ArgumentViolationSecurityException {

    String salesOrderCode = valueObject.getSalesOrderCode();
    String storeCode = valueObject.getStoreCode();

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    if (isContainsState(salesOrderGeneralModel, DObSalesOrderStateMachine.DRAFT_STATE)
        && storeCode != null) {
      checkIfStorehouseExists(storeCode);
    }

    if (isContainsState(salesOrderGeneralModel, DObSalesOrderStateMachine.REJECTED)) {
      checkIfStorehouseExists(storeCode);
    }
  }

  private void checkIfStorehouseExists(String storeCode) throws ArgumentViolationSecurityException {
    CObStorehouseGeneralModel storehouse = storehouseGeneralModelRep.findOneByUserCode(storeCode);
    if (storehouse == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private boolean isContainsState(DObSalesOrderGeneralModel salesOrderGeneralModel, String state) {
    return salesOrderGeneralModel.getCurrentStates().contains(state);
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setStorehouseGeneralModelRep(CObStorehouseGeneralModelRep storehouseGeneralModelRep) {
    this.storehouseGeneralModelRep = storehouseGeneralModelRep;
  }
}
