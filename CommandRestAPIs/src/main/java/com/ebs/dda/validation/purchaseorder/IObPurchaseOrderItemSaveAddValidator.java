package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.ValidationUtils;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObUoMGeneralModel;
import com.ebs.dda.jpa.order.IIObOrderItemValueObject;
import com.ebs.dda.jpa.order.IObPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObUoMGeneralModelRep;

import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MISSING_FIELD_INPUT;

public class IObPurchaseOrderItemSaveAddValidator {
  private ValidationResult validationResult;
  private IObOrderItemGeneralModelRep orderItemGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private CObUoMGeneralModelRep uomGeneralModelRep;

  public ValidationResult validate(IObPurchaseOrderItemValueObject valueObject)
      throws ArgumentViolationSecurityException {
    validationResult = new ValidationResult();

    boolean itemExists = checkIfItemExists(valueObject.getItemCode());
    boolean orderUnitExists = checkIfOrderUnitExists(valueObject.getUnitOfMeasureCode());

    if(itemExists && orderUnitExists){
      validateThatItemAndUnitOfMeasureWillBeAddedOnce(valueObject);
      validateThatItemTypeIsService(valueObject);
    }
    return validationResult;
  }

  public void validateMissingMandatoryFields(IObPurchaseOrderItemValueObject valueObject)
          throws Exception {
    ValidationUtils.checkIfFieldValueIsMissing(valueObject.getItemCode());
    ValidationUtils.checkIfFieldValueIsMissing(valueObject.getUnitOfMeasureCode());
    ValidationUtils.checkIfFieldValueIsMissing(valueObject.getPrice());
    ValidationUtils.checkIfFieldValueIsMissing(valueObject.getQuantity());
  }

  private boolean checkIfOrderUnitExists(String unitOfMeasureCode) throws ArgumentViolationSecurityException {
    CObUoMGeneralModel orderUnit = uomGeneralModelRep.findOneByUserCode(unitOfMeasureCode);
    if (orderUnit == null) {
      throw new ArgumentViolationSecurityException();
    }
    return true;
  }

  private boolean checkIfItemExists(String itemCode) {
    MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
    if (item == null) {
      validationResult.bindError(IIObOrderItemValueObject.ITEM_CODE, General_MISSING_FIELD_INPUT);
      return false;
    }
    return true;
  }

  private void validateThatItemAndUnitOfMeasureWillBeAddedOnce(
      IObPurchaseOrderItemValueObject valueObject) throws ArgumentViolationSecurityException {

    IObOrderItemGeneralModel purchaseOrderItem =
        orderItemGeneralModelRep.findAllByOrderCodeAndItemCodeAndOrderUnitCode(
            valueObject.getPurchaseOrderCode(),
            valueObject.getItemCode(),
            valueObject.getUnitOfMeasureCode());

    if (purchaseOrderItem != null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateThatItemTypeIsService(IObPurchaseOrderItemValueObject valueObject)
      throws ArgumentViolationSecurityException {
    MObItemGeneralModel itemGeneralMode =
        itemGeneralModelRep.findByUserCode(valueObject.getItemCode());
    if (!itemGeneralMode.getTypeCode().equals("SERVICE")) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setPurchaseOrderItemsGeneralModelRep(
      IObOrderItemGeneralModelRep orderItemGeneralModelRep) {
    this.orderItemGeneralModelRep = orderItemGeneralModelRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setUomGeneralModelRep(CObUoMGeneralModelRep uomGeneralModelRep) {
    this.uomGeneralModelRep = uomGeneralModelRep;
  }
}
