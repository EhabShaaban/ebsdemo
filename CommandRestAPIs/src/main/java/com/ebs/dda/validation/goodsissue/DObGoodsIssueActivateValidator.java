package com.ebs.dda.validation.goodsissue;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import com.ebs.dda.inventory.goodsissue.apis.IGoodsIssueSectionNames;
import com.ebs.dda.inventory.goodsissue.exceptions.ItemQuantityMoreThanInStoreException;
import com.ebs.dda.inventory.goodsissue.exceptions.ReferenceDocumentStateInvalidException;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IIObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.stockavailability.DObStockAvailabilityGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DObGoodsIssueActivateValidator
    implements IActivateBusinessRulesValidator<DObInventoryDocumentActivateValueObject> {

  private static final String UNRESTRICTED_STOCK_TYPE = "UNRESTRICTED_USE";
  private static final String GOODS_ISSUE_SALES_ORDER_TYPE = "BASED_ON_SALES_ORDER";
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep;
  private IObGoodsIssueItemGeneralModelRep goodsIssueItemsGeneralModelRep;
  private DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep;
  private DObGoodsIssueGeneralModelRep goodsIssueGeneralModelRep;

  private ValidationResult validationResult;

  public DObGoodsIssueActivateValidator() {}

  public ValidationResult validateBusinessRules(
      DObInventoryDocumentActivateValueObject valueObject) {
    String userCode = valueObject.getUserCode();
    DObGoodsIssueGeneralModel goodsIssueGeneralModel = goodsIssueGeneralModelRep.findOneByUserCode(userCode);
    validationResult = new ValidationResult();
    String goodsIssueGeneralModelTypeNameEn = goodsIssueGeneralModel.getTypeCode();
    if (goodsIssueGeneralModelTypeNameEn.equals(GOODS_ISSUE_SALES_ORDER_TYPE)) {
      IObGoodsIssueRefDocumentDataGeneralModel goodsIssueRefDocumentDataGeneralModel =
          goodsIssueRefDocumentDataGeneralModelRep.findOneByGoodsIssueCode(
              goodsIssueGeneralModel.getUserCode());
      validateSalesOrderStateIsValid(goodsIssueRefDocumentDataGeneralModel.getRefDocument());
    }

    List<LocalizedString> localizedItemExceptions = new ArrayList<>();

    List<IObGoodsIssueItemGeneralModel> goodsIssueItems =
        goodsIssueItemsGeneralModelRep.findByGoodsIssueCodeOrderByIdDesc(
            goodsIssueGeneralModel.getUserCode());

    for (IObGoodsIssueItemGeneralModel goodsIssueItem : goodsIssueItems) {

      CObStockAvailabilitiesGeneralModel itemStockAvailability =
          getItemRemainingAmount(goodsIssueGeneralModel, goodsIssueItem);

      BigDecimal itemRemainingAmount =
          itemStockAvailability == null
              ? BigDecimal.ZERO
              : itemStockAvailability.getAvailableQuantity();

      if (isRemainingLessThanQuantity(goodsIssueItem, itemRemainingAmount)) {
        LocalizedString localizedItemException = toLocalizedException(goodsIssueItem);
        localizedItemExceptions.add(localizedItemException);
      }
    }

    if (!localizedItemExceptions.isEmpty()) {
      bindItemHasQtyMoreThnaRemainingError(localizedItemExceptions);
    }
    return validationResult;
  }

  private LocalizedString toLocalizedException(IObGoodsIssueItemGeneralModel goodsIssueItem) {
    LocalizedString localizedItem = new LocalizedString();
    localizedItem.setValue(ISysDefaultLocales.ENGLISH_LOCALE, ( "Item: "
            + goodsIssueItem.getItemCode()
            + " - "
            + goodsIssueItem.getItemName().getValue(ISysDefaultLocales.ENGLISH_LANG_KEY)
            + " - "
            + goodsIssueItem.getUnitOfMeasureSymbol().getValue(ISysDefaultLocales.ENGLISH_LANG_KEY)));
    String textString = "الصنف: "
            + goodsIssueItem.getItemCode()
            + " - "
            + goodsIssueItem.getItemName().getValue(ISysDefaultLocales.ARABIC_LANG_KEY)
            + " - "
            + goodsIssueItem.getUnitOfMeasureSymbol().getValue(ISysDefaultLocales.ARABIC_LANG_KEY);
    localizedItem.setValue(ISysDefaultLocales.ARABIC_LOCALE, textString);
    return localizedItem;
  }

  private boolean isRemainingLessThanQuantity(
      IObGoodsIssueItemGeneralModel goodsIssueItem, BigDecimal itemRemainingAmount) {
    return itemRemainingAmount.compareTo(goodsIssueItem.getQuantity()) < 0;
  }

  private void bindItemHasQtyMoreThnaRemainingError(List<LocalizedString> items) {
    validationResult.bindErrorWithLocalizedValues(
        IGoodsIssueSectionNames.ITEMS_SECTION,
        IExceptionsCodes.ITEM_HAS_QUANTITY_MORE_THAN_REMAINING_IN_STORE,
        items);
    throw new ItemQuantityMoreThanInStoreException(validationResult);
  }

  private void validateSalesOrderStateIsValid(String refDocumentCode) {
    DObSalesOrderGeneralModel approvedSalesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCodeAndCurrentStatesLike(
            refDocumentCode, "%" + DObSalesOrderStateMachine.APPROVED + "%");
    DObSalesOrderGeneralModel salesInvoiceActivatedSalesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCodeAndCurrentStatesLike(
            refDocumentCode, "%" + DObSalesOrderStateMachine.SALES_INVOICE_ACTIVATED + "%");
    if (approvedSalesOrderGeneralModel == null
        && salesInvoiceActivatedSalesOrderGeneralModel == null) {
      validationResult.bindError(
          IIObGoodsIssueRefDocumentDataGeneralModel.REF_DOCUMENT,
          IExceptionsCodes.REF_DOCUMENT_STATE_IS_INVALID);
      throw new ReferenceDocumentStateInvalidException(validationResult);
    }
  }

  private CObStockAvailabilitiesGeneralModel getItemRemainingAmount(
      DObGoodsIssueGeneralModel goodsIssueGeneralModel,
      IObGoodsIssueItemGeneralModel goodsIssueItem) {
    String companyCode = goodsIssueGeneralModel.getCompanyCode();
    String plantCode = goodsIssueGeneralModel.getPlantCode();
    String storehouseCode = goodsIssueGeneralModel.getStorehouseCode();
    String businessUnitCode = goodsIssueGeneralModel.getPurchaseUnitCode();
    String UnitOfMeasureCode = goodsIssueItem.getUnitOfMeasureCode();

    CObStockAvailabilitiesGeneralModel itemStockAvailability =
        stockAvailabilityGeneralModelRep
            .findByItemCodeAndCompanyCodeAndPlantCodeAndStorehouseCodeAndPurchaseUnitCodeAndStockTypeAndUnitOfMeasureCode(
                goodsIssueItem.getItemCode(),
                companyCode,
                plantCode,
                storehouseCode,
                businessUnitCode,
                UNRESTRICTED_STOCK_TYPE,
                UnitOfMeasureCode);

    return itemStockAvailability;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setGoodsIssueRefDocumentDataGeneralModelRep(
      IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep) {
    this.goodsIssueRefDocumentDataGeneralModelRep = goodsIssueRefDocumentDataGeneralModelRep;
  }

  public void setGoodsIssueItemsGeneralModelRep(
      IObGoodsIssueItemGeneralModelRep goodsIssueItemsGeneralModelRep) {
    this.goodsIssueItemsGeneralModelRep = goodsIssueItemsGeneralModelRep;
  }

  public void setStockAvailabilityGeneralModelRep(
      DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep) {
    this.stockAvailabilityGeneralModelRep = stockAvailabilityGeneralModelRep;
  }

  public void setGoodsIssueGeneralModelRep(DObGoodsIssueGeneralModelRep goodsIssueGeneralModelRep) {
    this.goodsIssueGeneralModelRep = goodsIssueGeneralModelRep;
  }
}
