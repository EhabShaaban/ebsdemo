package com.ebs.dda.validation.paymentrequest;

import java.util.Set;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.validation.ValidationUtils;

public class DObPaymentAgainstVendorInvoiceCreationValidator
				extends DObPaymentForVendorCreationValidator {

	private DObVendorInvoiceGeneralModelRep vendorInvoiceRep;

	public ValidationResult validate(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		super.validate(creationValueObject);
		checkVendorInvoiceReference(creationValueObject);
		return validationResult;
	}

	private void checkVendorInvoiceReference(
					DObPaymentRequestCreateValueObject creationValueObject) {
		String vendorInvoiceCode = creationValueObject.getDueDocumentCode();
		DObVendorInvoiceGeneralModel vendorInvoice = vendorInvoiceRep
						.findOneByUserCode(vendorInvoiceCode);
		ValidationUtils.checkIfObjectIsNull(vendorInvoice);
		checkPurchaseOrderIsTheSelectedBusinessUnit(vendorInvoice, creationValueObject);
		checkPurchaseOrderIsTheSelectedBusinessPartner(vendorInvoice, creationValueObject);
		checkVendorInvoiceHasAllowedState(vendorInvoice.getCurrentStates());
	}

	private void checkPurchaseOrderIsTheSelectedBusinessPartner(DObVendorInvoiceGeneralModel vendorInvoice, DObPaymentRequestCreateValueObject creationValueObject) {
		String businessPartnerCode = creationValueObject.getBusinessPartnerCode();
		if (!businessPartnerCode.equals(vendorInvoice.getVendorCode())) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkPurchaseOrderIsTheSelectedBusinessUnit(
					DObVendorInvoiceGeneralModel vendorInvoice,
					DObPaymentRequestCreateValueObject creationValueObject) {
		String businessUnit = creationValueObject.getBusinessUnitCode();
		if (!businessUnit.equals(vendorInvoice.getPurchaseUnitCode())) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkVendorInvoiceHasAllowedState(Set<String> currentStates) {
		if (!currentStates.contains(DObVendorInvoiceStateMachine.POSTED_STATE)) {
			throw new ArgumentViolationSecurityException();
		}
	}

	public void setVendorInvoiceRep(DObVendorInvoiceGeneralModelRep vendorInvoiceRep) {
		this.vendorInvoiceRep = vendorInvoiceRep;
	}
}
