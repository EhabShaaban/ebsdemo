package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentHasNoEstimatedLandedCostException;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.OrderPaymentsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.OrderPaymentsGeneralModelRep;

public class DObPaymentGeneralForPOActivationValidator
        extends DObPaymentRequestActivationValidator {

    private OrderPaymentsGeneralModelRep orderPaymentsGeneralModelRep;
    private DObLandedCostGeneralModelRep landedCostGeneralModelRep;

    @Override
    public ValidationResult validateBusinessRules(DObPaymentRequestActivateValueObject valueObject)
            throws RuntimeException {
        super.validateBusinessRules(valueObject);
        String paymentRequestCode = valueObject.getPaymentRequestCode();
        OrderPaymentsGeneralModel orderPayment = orderPaymentsGeneralModelRep.findOneByPaymentCode(paymentRequestCode);
        String purchaseOrderCode = getPurchaseOrder(orderPayment);
        checkThatEstimatedLandedCostExistForPurchaseOrder(purchaseOrderCode);

        return validationResult;
    }

    private void checkThatEstimatedLandedCostExistForPurchaseOrder(String purchaseOrderCode) {
        DObLandedCostGeneralModel landedCostGeneralModel =
                landedCostGeneralModelRep.findByPurchaseOrderCodeAndCurrentStatesLike
                        (purchaseOrderCode, '%'+DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED+'%');
        if (landedCostGeneralModel == null) {
            throw new DObPaymentHasNoEstimatedLandedCostException(validationResult);
        }
    }

    private String getPurchaseOrder(OrderPaymentsGeneralModel orderPayment) {
        if (orderPayment.getOrderType().equals(OrderTypeEnum.Values.SERVICE_PO)) {
            return orderPayment.getReferenceOrderCode();
        }
        return orderPayment.getOrderCode();
    }

    public void setOrderPaymentsGeneralModelRep(OrderPaymentsGeneralModelRep orderPaymentsGeneralModelRep) {
        this.orderPaymentsGeneralModelRep = orderPaymentsGeneralModelRep;
    }

    public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
        this.landedCostGeneralModelRep = landedCostGeneralModelRep;
    }
}
