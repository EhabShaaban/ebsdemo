package com.ebs.dda.validation.paymentrequest;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNotNull;
import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

import java.util.Set;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequestCreateValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.validation.ValidationUtils;

public class DObGeneralPaymentCreationValidator extends DObPaymentRequestCreationValidator {

	private DObPurchaseOrderGeneralModelRep purchaseOrderRep;
	private IObOrderCompanyGeneralModelRep poComapnyGMRep;

	public ValidationResult validate(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		super.validate(creationValueObject);
		checkBusinessPartner(creationValueObject);
		checkDueDocumentType(creationValueObject);
		checkReferenceDocument(creationValueObject);
		return validationResult;
	}

	private void checkReferenceDocument(DObPaymentRequestCreateValueObject creationValueObject) {
		String purchaseOrderCode = creationValueObject.getDueDocumentCode();
		checkIfObjectIsNull(purchaseOrderCode);
		checkPurchaseOrderReference(creationValueObject);
	}

	private void checkDueDocumentType(DObPaymentRequestCreateValueObject creationValueObject) {
		checkIfDocumentTypeIsPurchaseOrder(creationValueObject.getDueDocumentType());
	}

	private void checkBusinessPartner(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		checkIfObjectIsNotNull(creationValueObject.getBusinessPartnerCode());
	}

	private void checkIfDocumentTypeIsPurchaseOrder(Object object) {
		if (!DueDocumentTypeEnum.PURCHASEORDER.equals(String.valueOf(object))) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkPurchaseOrderReference(
					DObPaymentRequestCreateValueObject creationValueObject) {
		String purchaseOrderCode = creationValueObject.getDueDocumentCode();
		DObPurchaseOrderGeneralModel purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
		ValidationUtils.checkIfObjectIsNull(purchaseOrder);
		checkPurchaseOrderIsTheSelectedBusinessUnit(purchaseOrder, creationValueObject);
		checkIfPurchaseOrderIsNotInPreviousState(purchaseOrder.getCurrentStates());
		checkIfPurchaseOrderHasValidState(purchaseOrder.getCurrentStates());
		checkIfPurchaseOrderHasCompany(purchaseOrder);
	}

	private void checkIfPurchaseOrderHasCompany(DObPurchaseOrderGeneralModel purchaseOrder) {
		IObOrderCompanyGeneralModel poCompanyGM =
			poComapnyGMRep.findByUserCode(purchaseOrder.getUserCode());

		if (poCompanyGM == null || poCompanyGM.getCompanyId() == null) {
			validationResult.bindError(
				IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_CODE,
				IExceptionsCodes.INVOICE_PO_HAS_NO_COMPANY);
		}
	}

	private void checkPurchaseOrderIsTheSelectedBusinessUnit(
					DObPurchaseOrderGeneralModel purchaseOrder,
					DObPaymentRequestCreateValueObject creationValueObject) {
		String businessUnit = creationValueObject.getBusinessUnitCode();
		if (!businessUnit.equals(purchaseOrder.getPurchaseUnitCode())) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkIfPurchaseOrderIsNotInPreviousState(Set<String> currentStates) {
		if (checkIfPurchaseOrderStateIsPrevious(currentStates)) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkIfPurchaseOrderHasValidState(Set<String> currentStates) {
		if (!checkIfPurchaseOrderStateIsInvalid(currentStates)) {
			validationResult.bindError(
							IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_CODE,
							IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE);
		}
	}

	private boolean checkIfPurchaseOrderStateIsPrevious(Set<String> currentStates) {
		return currentStates.contains(DObImportPurchaseOrderStateMachine.DRAFT_STATE);
	}

	private boolean checkIfPurchaseOrderStateIsInvalid(Set<String> currentStates) {
		return currentStates.contains(DObImportPurchaseOrderStateMachine.CLEARED_STATE)
						|| currentStates.contains(
										DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE)
						|| currentStates.contains(DObImportPurchaseOrderStateMachine.SHIPPED_STATE)
						|| currentStates.contains(DObImportPurchaseOrderStateMachine.APPROVED_STATE)
						|| currentStates.contains(
										DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE)
						|| currentStates.contains(DObImportPurchaseOrderStateMachine.ARRIVED_STATE)
						|| currentStates.contains(
										DObImportPurchaseOrderStateMachine.CONFIRMED_STATE);
	}

	public void setPurchaseOrderRep(DObPurchaseOrderGeneralModelRep purchaseOrderRep) {
		this.purchaseOrderRep = purchaseOrderRep;
	}

	public void setPoComapnyGMRep(IObOrderCompanyGeneralModelRep poComapnyGMRep) {
		this.poComapnyGMRep = poComapnyGMRep;
	}
}
