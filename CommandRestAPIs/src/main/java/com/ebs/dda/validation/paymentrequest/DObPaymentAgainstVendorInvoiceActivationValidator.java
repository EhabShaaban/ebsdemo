package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentAmountGreaterThanVendorInvoiceException;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;

import java.math.BigDecimal;

public class DObPaymentAgainstVendorInvoiceActivationValidator extends DObPaymentRequestActivationValidator {

    private IObInvoiceSummaryGeneralModelRep iObInvoiceSummaryGeneralModelRep;

    @Override
    public ValidationResult validateBusinessRules(DObPaymentRequestActivateValueObject valueObject) throws RuntimeException {
        super.validateBusinessRules(valueObject);
        BigDecimal paymentRequestNetAmount =
                activatePreparationGeneralModel.getPaymentNetAmount();
        String dueDocumentCode = activatePreparationGeneralModel.getDueDocumentCode();
        checkAgainstVendorInvoiceRemaining(paymentRequestNetAmount, dueDocumentCode);
        return validationResult;
    }

    private void checkAgainstVendorInvoiceRemaining(
        BigDecimal paymentRequestNetAmount, String dueDocumentCode) {
        IObInvoiceSummaryGeneralModel invoiceSummary =
                iObInvoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
                        dueDocumentCode, IDocumentTypes.VENDOR_INVOICE);
        BigDecimal invoiceSummaryTotalRemaining = new BigDecimal(invoiceSummary.getTotalRemaining());
        if (paymentRequestNetAmount.compareTo(invoiceSummaryTotalRemaining) > 0) {
            throw new DObPaymentAmountGreaterThanVendorInvoiceException(validationResult);
        }
    }

    public void setiObInvoiceSummaryGeneralModelRep(IObInvoiceSummaryGeneralModelRep iObInvoiceSummaryGeneralModelRep) {
        this.iObInvoiceSummaryGeneralModelRep = iObInvoiceSummaryGeneralModelRep;
    }
}
