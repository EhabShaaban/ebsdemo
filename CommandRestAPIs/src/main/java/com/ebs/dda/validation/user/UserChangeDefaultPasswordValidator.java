package com.ebs.dda.validation.user;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.jpa.entities.User;
import com.ebs.dac.security.jpa.repositories.UserRepository;
import com.ebs.dac.security.jpa.valueobjects.IUserPasswordValueObject;
import com.ebs.dac.security.jpa.valueobjects.UserPasswordValueObject;
import com.ebs.dac.security.shiro.models.EBSPrincipal;
import com.ebs.dac.security.usermanage.utils.BDKUserUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

public class UserChangeDefaultPasswordValidator {

  private UserRepository userRepository;

  public ValidationResult validate(UserPasswordValueObject userPasswordValueObject)
      throws Exception {
    ValidationResult validationResult = new ValidationResult();
    checkThatObjectNotEmpty(userPasswordValueObject.getCurrentPassword());
    checkThatObjectNotEmpty(userPasswordValueObject.getNewPassword());
    checkThatObjectNotEmpty(userPasswordValueObject.getConfirmedNewPassword());
    checkThatNewPasswordMatchesTheConfirmedPassword(
        userPasswordValueObject.getNewPassword(),
        userPasswordValueObject.getConfirmedNewPassword());
    checkThatCurrentPasswordEqualsDefaultPassword(
        userPasswordValueObject.getCurrentPassword(), validationResult);
    checkThatCurrentPasswordNotEqualsNewPassword(
        userPasswordValueObject.getNewPassword(), validationResult);
    return validationResult;
  }

  private void checkThatCurrentPasswordEqualsDefaultPassword(
      String currentPassword, ValidationResult validationResult) {
    Subject subject = SecurityUtils.getSubject();
    EBSPrincipal principal = (EBSPrincipal) subject.getPrincipal();
    User user = userRepository.findOneById(principal.getUserId());
    Boolean isSamePassword = BDKUserUtils.checkPassword(currentPassword, user.getPassword());

    if (!isSamePassword) {
      validationResult.bindError(
          IUserPasswordValueObject.CURRENT_PASSWORD,
          IExceptionsCodes.CURRENT_PASSWORD_IS_DIFFERENT_TO_DEFAULT_PASSWORD);
    }
  }

  private void checkThatCurrentPasswordNotEqualsNewPassword(
      String newPassword, ValidationResult validationResult) {
    Subject subject = SecurityUtils.getSubject();
    EBSPrincipal principal = (EBSPrincipal) subject.getPrincipal();
    User user = userRepository.findOneById(principal.getUserId());
    Boolean isSamePassword = BDKUserUtils.checkPassword(newPassword, user.getPassword());

    if (isSamePassword) {
      validationResult.bindError(
          IUserPasswordValueObject.NEW_PASSWORD,
          IExceptionsCodes.CURRENT_PASSWORD_IS_SAME_TO_NEW_PASSWORD);
    }
  }

  private void checkThatObjectNotEmpty(String password) throws Exception {
    if (password == null || password.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatNewPasswordMatchesTheConfirmedPassword(
      String newPassword, String confirmedNewPassword) throws Exception {
    if (!newPassword.equals(confirmedNewPassword)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setUserRepository(UserRepository userRepository) {
    this.userRepository = userRepository;
  }
}
