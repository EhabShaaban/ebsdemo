package com.ebs.dda.validation.collection;

import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;

public class DObCollectionMonetaryNoteActivateValidator extends DObCollectionActivateValidator {

  @Override
  protected void validateRefDocument(DObCollectionGeneralModel collectionGeneralModel) {}

}
