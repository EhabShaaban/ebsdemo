package com.ebs.dda.validation.collection;

public class IDObCollectionCreationValidator {

   static final String SALES_INVOICE_TYPE = "SALES_INVOICE";
   static final String NOTES_RECEIVABLE_TYPE = "NOTES_RECEIVABLE";
   static final String SALES_ORDER_TYPE = "SALES_ORDER";
}
