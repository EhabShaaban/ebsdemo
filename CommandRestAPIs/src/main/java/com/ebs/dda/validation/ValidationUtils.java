package com.ebs.dda.validation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class ValidationUtils {
  public static void checkIfObjectIsNull(Object object) throws ArgumentViolationSecurityException {
    if (object == null || object.toString().trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public static void checkIfObjectIsNotNull(Object object) throws Exception {
    if (object != null) {
      throw new ArgumentViolationSecurityException();
    }
  }
  public DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }
}
