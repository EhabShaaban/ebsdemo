package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApprovalCycleRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApproverRep;
import com.ebs.dda.purchases.exceptions.ApproverNotInApprovalCycleException;
import com.ebs.dda.purchases.exceptions.ApproverNotInCorrectOrderException;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;

import java.util.List;

public class DObPurchaseOrderApproveAndRejectValidator {

  private IUserAccountSecurityService userAccountSecurityService;
  private IObOrderApprovalCycleRep approvalCycleRep;
  private IObOrderApproverRep approverRep;
  private DObPurchaseOrderRep purchaseOrderRep;

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setApprovalCycleRep(IObOrderApprovalCycleRep approvalCycleRep) {
    this.approvalCycleRep = approvalCycleRep;
  }

  public void setApproverRep(IObOrderApproverRep approverRep) {
    this.approverRep = approverRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void validate(DObPurchaseOrderGeneralModel purchaseOrder, String approverAction) throws Exception {
    IBDKUser loggedInUser = userAccountSecurityService.getLoggedInUser();
    IObOrderApprovalCycle currentUndecidedCycle =
        approvalCycleRep.findByRefInstanceIdAndFinalDecisionIsNull(purchaseOrder.getId());
    checkIfPurchaseOrderIsDraft(purchaseOrder);
    checkIfActionIsAllowedInCurrentState(purchaseOrder, approverAction);
    checkIfUserExistsInApprovalCycle(currentUndecidedCycle, loggedInUser);
    checkIfUserInCorrectOrder(currentUndecidedCycle, loggedInUser);
  }

  private void checkIfActionIsAllowedInCurrentState(DObPurchaseOrderGeneralModel purchaseOrder, String approverAction)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    purchaseOrderStateMachine.isAllowedAction(approverAction);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrderGeneralModel purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory = new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private void checkIfPurchaseOrderIsDraft(DObPurchaseOrderGeneralModel purchaseOrder) {
    if (purchaseOrder.getCurrentStates().contains(DObImportPurchaseOrderStateMachine.DRAFT_STATE)) {
      throw new AuthorizationException("Purhcase order with draft state");
    }
  }

  private void checkIfUserInCorrectOrder(
      IObOrderApprovalCycle currentUndecidedCycle, IBDKUser loggedInUser)
          throws ApproverNotInCorrectOrderException {

    List<IObOrderApprover> approversList =
        approverRep.findByRefInstanceIdOrderByIdAsc(currentUndecidedCycle.getId());

    for (IObOrderApprover approver : approversList) {
      if (approver.getDecision() == null) {
        if (approver.getApproverId().equals(loggedInUser.getUserId())) {
          return;
        }
        throw new ApproverNotInCorrectOrderException();
      }
    }
  }

  private IObOrderApprover getCurrentApprover(
          IObOrderApprovalCycle currentUndecidedCycle, IBDKUser loggedInUser) {

    List<IObOrderApprover> approversList =
            approverRep.findByRefInstanceIdOrderByIdAsc(currentUndecidedCycle.getId());

    for (IObOrderApprover approver : approversList) {
      if (approver.getDecision() == null) {
        if (approver.getApproverId().equals(loggedInUser.getUserId())) {
          return approver;
        }
      }
    }
    return null;
  }

  private void checkIfUserExistsInApprovalCycle(
      IObOrderApprovalCycle currentUndecidedCycle, IBDKUser loggedInUser)
      throws ApproverNotInApprovalCycleException {

    IObOrderApprover approver = getCurrentApprover(currentUndecidedCycle, loggedInUser);
    if (approver == null) {
      throw new ApproverNotInApprovalCycleException();
    }
  }
}
