package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderPaymentDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConfirmValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderConfirmValueObject;
import com.ebs.dda.purchases.statemachines.DObServicePurchaseOrderStateMachine;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IItemMandatoryAttributes;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IPurchaseOrderPaymentDetailsMandatoryAttributes;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import org.joda.time.DateTime;

import java.util.*;

public class DObPurchaseOrderConfirmationValidator
    extends DObPurchaseOrderStateTransitionCommonValidator {
  private ValidationResult validationResult;
  private CommandUtils commandUtils;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  private IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep;
  private IObOrderItemGeneralModelRep orderItemGeneralModelRep;
  private MandatoryValidatorUtils validatorUtils;

  public DObPurchaseOrderConfirmationValidator() {
    commandUtils = new CommandUtils();
    validatorUtils = new MandatoryValidatorUtils();
  }

  public Map<String, Object> validateMandatories(
      DObPurchaseOrderGeneralModel orderGeneralModel,
      DObPurchaseOrderConfirmValueObject confirmValueObject)
      throws Exception {

    DateTime confirmationDateTime = getConfirmationDateTime(confirmValueObject);

    if (confirmationDateTime == null) {
      throw new ArgumentViolationSecurityException();
    }

    Map<String, Object> sectionsMissingFields = new HashMap<>();
    if (isServicePurchaseOrder(orderGeneralModel)) {

      List<String> paymentDetailsMissingFields =
          validatePaymentDetailsMandatories(orderGeneralModel.getUserCode());
      if (!paymentDetailsMissingFields.isEmpty()) {
        sectionsMissingFields.put(
            IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION, paymentDetailsMissingFields);
      }

      List<IObOrderItemGeneralModel> itemGeneralModels =
          orderItemGeneralModelRep.findByOrderCodeOrderByIdDesc(orderGeneralModel.getUserCode());
      HashSet<String> result = new HashSet<>();
      if (itemGeneralModels.isEmpty()) {
        result.add(IItemMandatoryAttributes.EMPTY_SECTION_DATA);
        sectionsMissingFields.put(
            IPurchaseOrderSectionNames.ITEMS_SECTION, new ArrayList<>(result));
      }
    }
    return sectionsMissingFields;
  }

  public List<String> validatePaymentDetailsMandatories(String purchaseOrderCode) throws Exception {
    IObOrderPaymentDetailsGeneralModel paymentDetailsGeneralModel =
        orderPaymentDetailsGeneralModelRep.findOneByOrderCode(purchaseOrderCode);
    if (paymentDetailsGeneralModel == null) {
      paymentDetailsGeneralModel = new IObOrderPaymentDetailsGeneralModel();
    }
    String[] mandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(
            IPurchaseOrderPaymentDetailsMandatoryAttributes.MANDATORIES,
            DObServicePurchaseOrderStateMachine.CONFIRMED_STATE);
    return validatorUtils.validateMandatoriesForGeneralModel(
        mandatoryAttributes, paymentDetailsGeneralModel);
  }

  public ValidationResult validate(
      DObPurchaseOrderGeneralModel orderGeneralModel,
      DObPurchaseOrderConfirmValueObject confirmValueObject)
      throws Exception {
    validationResult = new ValidationResult();

    if (orderGeneralModel == null) {
      throw new ArgumentViolationSecurityException();
    }

    if (isDraftPurchaseOrder(orderGeneralModel) && !isServicePurchaseOrder(orderGeneralModel)) {
      throw new ArgumentViolationSecurityException();
    }

    DateTime confirmationDateTime = getConfirmationDateTime(confirmValueObject);

    compareApprovalDataWithConfirmationDateIfNotServicePurchaseOrder(
        orderGeneralModel, confirmationDateTime);
    checkIfRelatedPOStateNotCancelIfServicePO(orderGeneralModel);

    return validationResult;
  }

  private DateTime getConfirmationDateTime(DObPurchaseOrderConfirmValueObject confirmValueObject) {
    return confirmValueObject.getConfirmationDateTime();
  }

  private void compareApprovalDataWithConfirmationDateIfNotServicePurchaseOrder(
      DObPurchaseOrderGeneralModel orderGeneralModel, DateTime confirmationDateTime) {
    DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel =
        getpurchaseOrderCycleDatesGeneralModelRep()
            .findOneByUserCodeOrderByIdDesc(orderGeneralModel.getUserCode());

    if (!isServicePurchaseOrder(orderGeneralModel)
        && purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getApprovalDate() != null
        && !isValidTransitionDateTime(
            purchaseOrderCycleDatesGeneralModel.getApprovalDate(), confirmationDateTime)) {
      validationResult.bindErrorWithValues(
          IDObPurchaseOrderConfirmValueObject.CONFIRMATION_DATE_TIME,
          IExceptionsCodes.INVALID_STATE_DATE,
          Arrays.asList(
              getDateWithUserDefaultTimeZone(
                  purchaseOrderCycleDatesGeneralModel.getApprovalDate())));
    }
  }

  private void checkIfRelatedPOStateNotCancelIfServicePO(
      DObPurchaseOrderGeneralModel orderGeneralModel) {
    if (isServicePurchaseOrder(orderGeneralModel)) {
      DObPurchaseOrderGeneralModel relatedPO =
          purchaseOrderGeneralModelRep.findOneByUserCode(orderGeneralModel.getRefDocumentCode());
      if (isCancelledPurchaseOrder(relatedPO)) {
        validationResult.bindError(
            IDObPurchaseOrderGeneralModel.REFERENCE_PO_CODE,
            IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH);
      }
    }
  }

  private boolean isDraftPurchaseOrder(DObPurchaseOrderGeneralModel dObPurchaseOrder) {
    return dObPurchaseOrder.getCurrentStates().contains("Draft");
  }

  private boolean isCancelledPurchaseOrder(DObPurchaseOrderGeneralModel dObPurchaseOrder) {
    return dObPurchaseOrder.getCurrentStates().contains("Cancelled");
  }

  private boolean isServicePurchaseOrder(DObPurchaseOrderGeneralModel dObPurchaseOrder) {
    return dObPurchaseOrder.getObjectTypeCode().equals("SERVICE_PO");
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

  public void setOrderPaymentDetailsGeneralModelRep(
      IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep) {
    this.orderPaymentDetailsGeneralModelRep = orderPaymentDetailsGeneralModelRep;
  }

  public void setOrderItemGeneralModelRep(IObOrderItemGeneralModelRep orderItemGeneralModelRep) {
    this.orderItemGeneralModelRep = orderItemGeneralModelRep;
  }
}
