package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsValueObject;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;

public class IObUnderSettlementPaymentDetailsSaveValidator extends IObPaymentRequestPaymentDetailsSaveValidator{

    private CObCurrencyRep currencyRep;

    @Override
    public ValidationResult validate(IObPaymentRequestPaymentDetailsValueObject valueObject) throws Exception {
        super.validate(valueObject);
        checkCurrencyExists(valueObject.getCurrencyCode());

        return validationResult;
    }

    private void checkCurrencyExists(String currencyCode) throws Exception {
        if (currencyCode != null) {
            CObCurrency currency = currencyRep.findOneByUserCode(currencyCode);
            if (currency == null) {
                throw new ArgumentViolationSecurityException();
            }
        }
    }

    public void setCurrencyRep(CObCurrencyRep currencyRep) {
        this.currencyRep = currencyRep;
    }

}
