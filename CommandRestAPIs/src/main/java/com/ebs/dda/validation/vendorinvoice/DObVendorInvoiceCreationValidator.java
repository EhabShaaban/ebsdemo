package com.ebs.dda.validation.vendorinvoice;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;

import java.util.Arrays;
import java.util.List;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

public class DObVendorInvoiceCreationValidator {

  private final String SERVICE_PO_CONFIRMED_STATE = DObImportPurchaseOrderStateMachine.CONFIRMED_STATE;
  private ValidationResult validationResult;
  private IObOrderCompanyGeneralModelRep purchaseOrderCompanySectionGeneralModelRep;
  private DObPurchaseOrderRep orderRep;
  private MObVendorRep vendorRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;

  public ValidationResult validate(DObVendorInvoiceCreationValueObject creationValueObject)
          throws Exception {
    validationResult = new ValidationResult();
    checkIfObjectIsNull(creationValueObject.getInvoiceType());
    checkIfObjectIsNull(creationValueObject.getVendorCode());
    checkIfObjectIsNull(creationValueObject.getPurchaseOrderCode());
    checkIfObjectIsNull(creationValueObject.getDocumentOwnerId());
    checkThatVendorExists(
            creationValueObject.getVendorCode());
    checkThatPurchaseOrderExists(creationValueObject.getPurchaseOrderCode());
    checkThatDocumentOwnerExists(creationValueObject.getDocumentOwnerId());
    checkThatPurchaseOrderIsRelatedToTheSelectedVendor(
            creationValueObject.getPurchaseOrderCode(),
            creationValueObject.getVendorCode(),
            creationValueObject.getInvoiceType());
    checkInvoiceTypeAndPOType(creationValueObject);
    checkPurchaseOrderState(creationValueObject);
    checkIfPurchaseOrderHasCompany(creationValueObject.getPurchaseOrderCode());
    return validationResult;
  }

  private void checkThatDocumentOwnerExists(Long documentOwnerId) {
    documentOwnerGeneralModelRep.findOneByUserIdAndObjectName(documentOwnerId, IDObVendorInvoice.SYS_NAME);
  }

  private void checkInvoiceTypeAndPOType(DObVendorInvoiceCreationValueObject creationValueObject)
          throws Exception {
    DObPurchaseOrder order = orderRep.findOneByUserCode(creationValueObject.getPurchaseOrderCode());
    if (VendorInvoiceTypeEnum.VendorInvoiceType.isPurchaseServiceLocalInvoice(creationValueObject.getInvoiceType())
            && !order.getObjectTypeCode().equals(OrderTypeEnum.Values.SERVICE_PO)) {
      throw new ArgumentViolationSecurityException();
    } else if (creationValueObject.getInvoiceType().equals(VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE)
            && !order.getObjectTypeCode().equals(OrderTypeEnum.Values.LOCAL_PO)) {
      throw new ArgumentViolationSecurityException();
    }
    else if (creationValueObject.getInvoiceType().equals(VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE)
            && !order.getObjectTypeCode().equals(OrderTypeEnum.Values.IMPORT_PO)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatPurchaseOrderExists(String purchaseOrderCode) throws Exception {
    DObPurchaseOrder order = orderRep.findOneByUserCode(purchaseOrderCode);
    if (order == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatVendorExists(String vendorCode) throws Exception {
    MObVendor vendor = vendorRep.findOneByUserCode(vendorCode);
    if (vendor == null) {
        validationResult.bindError(
                IDObVendorInvoiceCreationValueObject.VENDOR_CODE,
                IExceptionsCodes.General_MISSING_FIELD_INPUT);

    }
  }
  private void checkThatPurchaseOrderIsRelatedToTheSelectedVendor(
          String purchaseOrderCode, String vendorCode, String invoiceTypeCode) throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
            purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    if (VendorInvoiceTypeEnum.VendorInvoiceType.isGoodsInvoice(invoiceTypeCode)
            && !purchaseOrderGeneralModel.getVendorCode().equals(vendorCode)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfPurchaseOrderHasCompany(String purchaseOrderCode) {
    IObOrderCompanyGeneralModel dObPurchaseOrderCompanySectionGeneralModel =
            purchaseOrderCompanySectionGeneralModelRep.findByUserCode(purchaseOrderCode);
    if (dObPurchaseOrderCompanySectionGeneralModel != null) {
      String companyCode = dObPurchaseOrderCompanySectionGeneralModel.getCompanyCode();
      if (companyCode == null) {
        validationResult.bindError(
                IDObVendorInvoiceCreationValueObject.PURCHESE_ORDER_CODE,
                IExceptionsCodes.INVOICE_PO_HAS_NO_COMPANY);
      }
    }
    if (dObPurchaseOrderCompanySectionGeneralModel == null) {
      validationResult.bindError(
              IDObVendorInvoiceCreationValueObject.PURCHESE_ORDER_CODE,
              IExceptionsCodes.INVOICE_PO_HAS_NO_COMPANY);
    }
  }

  public void setPurchaseOrderCompanySectionGeneralModelRep(
          IObOrderCompanyGeneralModelRep purchaseOrderCompanySectionGeneralModelRep) {
    this.purchaseOrderCompanySectionGeneralModelRep = purchaseOrderCompanySectionGeneralModelRep;
  }

  public void setOrderRep(DObPurchaseOrderRep orderRep) {
    this.orderRep = orderRep;
  }

  public void setVendorRep(MObVendorRep vendorRep) {
    this.vendorRep = vendorRep;
  }
  public void setPurchaseOrderGeneralModelRep(
          DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

  public void setDocumentOwnerGeneralModelRep(DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
    this.documentOwnerGeneralModelRep = documentOwnerGeneralModelRep;
  }

  private void checkPurchaseOrderState(DObVendorInvoiceCreationValueObject valueObject)
          throws Exception {
    DObPurchaseOrder order = orderRep.findOneByUserCode(valueObject.getPurchaseOrderCode());
    Boolean isStateAllowed = false;
    if (order != null && order.getCurrentState() != null) {
      for (String state : order.getCurrentStates()) {
        isStateAllowed =
                checkOrderStateBasedOnCreatedInvoiceType(valueObject, isStateAllowed, state);
        if (isStateAllowed == null) {
          break;
        }
      }
      if (!isStateAllowed) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private Boolean checkOrderStateBasedOnCreatedInvoiceType(
          DObVendorInvoiceCreationValueObject valueObject, Boolean isStateAllowed, String state)
          {
    if (VendorInvoiceTypeEnum.VendorInvoiceType.isPurchaseServiceLocalInvoice(valueObject.getInvoiceType())) {
      if (state.equals(SERVICE_PO_CONFIRMED_STATE)) {
        isStateAllowed = true;
      }
    } else {
      if (getPossibleStates().contains(state)) {
        isStateAllowed = true;
      }
    }
    return isStateAllowed;
  }

  private List<String> getPossibleStates() {
    return Arrays.asList(
            DObImportPurchaseOrderStateMachine.CONFIRMED_STATE,
            DObImportPurchaseOrderStateMachine.FINISHED_PRODUCTION_STATE,
            DObImportPurchaseOrderStateMachine.SHIPPED_STATE,
            DObImportPurchaseOrderStateMachine.ARRIVED_STATE,
            DObImportPurchaseOrderStateMachine.CLEARED_STATE,
            DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE);
  }
}
