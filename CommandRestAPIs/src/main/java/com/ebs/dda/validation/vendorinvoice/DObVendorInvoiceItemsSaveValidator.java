package com.ebs.dda.validation.vendorinvoice;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.DObInvoiceItemValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoiceItemValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.CObUoMGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNotNull;
import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

public class DObVendorInvoiceItemsSaveValidator {

  private ValidationResult validationResult;

  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private DObVendorInvoiceGeneralModelRep invoiceGeneralModelRep;
  private CObUoMGeneralModelRep uomGeneralModelRep;
  private IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep;

  public ValidationResult validate(DObInvoiceItemValueObject invoiceItemValueObject)
          throws Exception {
    validationResult = new ValidationResult();
    checkMandatories(invoiceItemValueObject);
    checkIfItemExists(invoiceItemValueObject.getItemCode());
    checkIfUnitOfMeasureExists(invoiceItemValueObject);
    checkIfItemVendorRecordExists(invoiceItemValueObject);
    checkIfItemIsAddedBefore(invoiceItemValueObject);
    return validationResult;
  }

  private void checkIfItemIsAddedBefore(DObInvoiceItemValueObject invoiceItemValueObject) throws Exception {
    String invoiceCode = invoiceItemValueObject.getInvoiceCode();
    String itemCode = invoiceItemValueObject.getItemCode();
    String orderUnitCode = invoiceItemValueObject.getOrderUnitCode();
    IObVendorInvoiceItemsGeneralModel vendorInvoiceItem = vendorInvoiceItemsGeneralModelRep.findOneByInvoiceCodeAndItemCodeAndOrderUnitCode(invoiceCode, itemCode, orderUnitCode);
    checkIfObjectIsNotNull(vendorInvoiceItem);
  }

  private void checkMandatories(DObInvoiceItemValueObject invoiceItemValueObject) {
    checkIfObjectIsNull(invoiceItemValueObject.getItemCode());
    checkIfObjectIsNull(invoiceItemValueObject.getOrderUnitCode());
  }

  private void checkIfUnitOfMeasureExists(DObInvoiceItemValueObject invoiceItemValueObject){
    CObUoMGeneralModel cObUoMGeneralModel =
            uomGeneralModelRep.findOneByUserCode(invoiceItemValueObject.getOrderUnitCode());
    if (cObUoMGeneralModel == null) {
      validationResult.bindError(
              IDObVendorInvoiceItemValueObject.ITEM_CODE, IExceptionsCodes.General_MISSING_FIELD_INPUT);
      validationResult.bindError(
              IDObVendorInvoiceItemValueObject.ORDER_UNIT_CODE,
              IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkIfItemVendorRecordExists(DObInvoiceItemValueObject invoiceItemValueObject){
    
    DObVendorInvoiceGeneralModel invoiceGeneralModel =
            invoiceGeneralModelRep.findOneByUserCode(invoiceItemValueObject.getInvoiceCode());
    ItemVendorRecordGeneralModel itemVendorRecord =
            itemVendorRecordGeneralModelRep
                    .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                            invoiceItemValueObject.getItemCode(),
                            invoiceGeneralModel.getVendorCode(),
                            invoiceItemValueObject.getOrderUnitCode(),
                            invoiceGeneralModel.getPurchaseUnitCode());

    if (itemVendorRecord == null) {
      validationResult.bindError(
              IDObVendorInvoiceItemValueObject.ITEM_CODE, IExceptionsCodes.General_MISSING_FIELD_INPUT);
      validationResult.bindError(
              IDObVendorInvoiceItemValueObject.ORDER_UNIT_CODE,
              IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkIfItemExists(String itemCode) {
    MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
    checkIfObjectIsNull(item);
  }

  public void setItemVendorRecordGeneralModelRep(
          ItemVendorRecordGeneralModelRep itemVendorRecordRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setInvoiceGeneralModelRep(DObVendorInvoiceGeneralModelRep invoiceGeneralModelRep) {
    this.invoiceGeneralModelRep = invoiceGeneralModelRep;
  }

  public void setUomGeneralModelRep(CObUoMGeneralModelRep uomGeneralModelRep) {
    this.uomGeneralModelRep = uomGeneralModelRep;
  }

  public void setVendorInvoiceItemsGeneralModelRep(IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep) {
    this.vendorInvoiceItemsGeneralModelRep = vendorInvoiceItemsGeneralModelRep;
  }
}
