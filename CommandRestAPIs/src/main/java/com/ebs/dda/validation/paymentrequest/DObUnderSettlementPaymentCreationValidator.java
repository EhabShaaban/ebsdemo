package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;

public class DObUnderSettlementPaymentCreationValidator extends DObPaymentRequestCreationValidator {

	public ValidationResult validate(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		super.validate(creationValueObject);
		return validationResult;
	}
}
