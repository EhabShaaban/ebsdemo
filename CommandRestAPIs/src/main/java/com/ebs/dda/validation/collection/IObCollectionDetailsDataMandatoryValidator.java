package com.ebs.dda.validation.collection;

import com.ebs.dda.jpa.accounting.collection.CollectionMethodEnum;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.ISectionMandatoryValidator;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.validation.collection.apis.IIObCollectionDetailsMandatoryAttributes;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.List;
import java.util.Map;

public class IObCollectionDetailsDataMandatoryValidator implements ISectionMandatoryValidator {

  private MandatoryValidatorUtils validatorUtils;
  private IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep;

  public IObCollectionDetailsDataMandatoryValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  public void setCollectionDetailsGeneralModelRep(
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep) {
    this.collectionDetailsGeneralModelRep = collectionDetailsGeneralModelRep;
  }

  @Override
  public void validateValueObjectMandatories(Object valueObject) {}

  @Override
  public List<String> validateGeneralModelMandatories(String collectionCode, String state)
      throws RuntimeException {
    Map<String, String[]> mandatories = null;

    IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
        collectionDetailsGeneralModelRep.findByCollectionCode(collectionCode);
    if (collectionDetailsGeneralModel
        .getCollectionMethod()
        .equals(CollectionMethodEnum.CollectionMethod.BANK.name())) {
      mandatories = IIObCollectionDetailsMandatoryAttributes.MANDATORIES_FOR_BANK;
    } else {
      mandatories = IIObCollectionDetailsMandatoryAttributes.MANDATORIES_FOR_CASH;
    }
    String[] mandatoryAttributes = validatorUtils.getMandatoryAttributesByState(mandatories, state);
    return validatorUtils.validateMandatoriesForGeneralModel(
        mandatoryAttributes, collectionDetailsGeneralModel);
  }
}
