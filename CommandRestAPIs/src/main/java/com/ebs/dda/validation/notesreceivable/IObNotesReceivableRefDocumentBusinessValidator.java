package com.ebs.dda.validation.notesreceivable;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;

import java.math.BigDecimal;
import java.util.List;

public class IObNotesReceivableRefDocumentBusinessValidator {

  private ValidationResult validationResult;
  private IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsGeneralModelRep;
  private IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;

  private IObNotesReceivableRefDocumentValidator notesReceivableRefDocumentValidator;

  public void validateBusinessRules(
      IObMonetaryNotesReferenceDocumentsValueObject valueObject,
      boolean isSavedRefDocBefore,
      ValidationResult validationResult)
      throws RuntimeException {

    String notesReceivableCode = valueObject.getNotesReceivableCode();
    String refDocumentCode = valueObject.getRefDocumentCode();
    String refDocumentType = valueObject.getDocumentType();
    BigDecimal amountToCollect = valueObject.getAmountToCollect();

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocumentGeneralModels =
        monetaryNotesReferenceDocumentsRep.findByNotesReceivableCodeOrderByIdAsc(
            notesReceivableCode);

    IObMonetaryNotesDetailsGeneralModel notesDetailsGeneralModel =
        monetaryNotesDetailsGeneralModelRep.findOneByRefInstanceId(
            notesReceivablesGeneralModel.getId());

    BigDecimal totalOfAmountToCollect = getTotalOfAmountToCollect(referenceDocumentGeneralModels);

    this.validationResult = validationResult;

    validateTotalOfAmountToCollectComparedWithNRAmount(
        amountToCollect, isSavedRefDocBefore, notesDetailsGeneralModel, totalOfAmountToCollect);

    notesReceivableRefDocumentValidator.validateRefDocument(
        refDocumentCode,
        refDocumentType,
        amountToCollect,
        notesReceivablesGeneralModel,
        validationResult);

    validateRefDocumentIsUnique(
        notesReceivableCode, refDocumentCode, refDocumentType, isSavedRefDocBefore);
  }

  private void validateTotalOfAmountToCollectComparedWithNRAmount(
      BigDecimal amountToCollect,
      boolean isSavedRefDocBefore,
      IObMonetaryNotesDetailsGeneralModel notesDetailsGeneralModel,
      BigDecimal totalOfAmountToCollect) {
    if (isSavedRefDocBefore) {
      checkIfTotalOfAmountToCollectEqualsNRAmount(
          totalOfAmountToCollect, notesDetailsGeneralModel.getAmount());
    } else {
      checkIfTotalOfAmountToCollectLessThanOrEqualsNRAmount(
          totalOfAmountToCollect.add(amountToCollect), notesDetailsGeneralModel.getAmount());
    }
  }

  private void validateRefDocumentIsUnique(
      String notesReceivableCode,
      String refDocumentCode,
      String refDocumentType,
      boolean isSavedRefDocBefore) {
    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments =
        monetaryNotesReferenceDocumentsRep
            .findByNotesReceivableCodeAndRefDocumentCodeAndDocumentTypeOrderByIdAsc(
                notesReceivableCode, refDocumentCode, refDocumentType);

    if (isSavedRefDocBefore) {
      checkIfSavedRefDocumentRecordExistMoreThanOne(referenceDocuments);
    } else {
      checkIfRequestToSaveRefDocumentRecordThatExistsOnceBefore(referenceDocuments);
    }
  }

  private void checkIfRequestToSaveRefDocumentRecordThatExistsOnceBefore(
      List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments) {
    if (referenceDocuments.size() > 0) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfSavedRefDocumentRecordExistMoreThanOne(
      List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments) {
    if (referenceDocuments.size() > 1) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private BigDecimal getTotalOfAmountToCollect(
      List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocumentGeneralModels) {

    if (!referenceDocumentGeneralModels.isEmpty()) {
      return referenceDocumentGeneralModels.stream()
          .map(IObNotesReceivablesReferenceDocumentGeneralModel::getAmountToCollect)
          .reduce(BigDecimal::add)
          .get();
    }
    return BigDecimal.ZERO;
  }

  private void checkIfTotalOfAmountToCollectEqualsNRAmount(
      BigDecimal totalOfAmountToCollect, BigDecimal amount) {
    if (totalOfAmountToCollect.compareTo(amount) != 0) {
      validationResult.bindError(
          IIObMonetaryNotesDetailsGeneralModel.AMOUNT,
          IExceptionsCodes.NR_AMOUNT_TO_COLLECT_NOT_EQUALS_NR_AMOUNT);
    }
  }

  private void checkIfTotalOfAmountToCollectLessThanOrEqualsNRAmount(
      BigDecimal totalOfAmountToCollect, BigDecimal amount) {
    if (totalOfAmountToCollect.compareTo(amount) > 0) {
      validationResult.bindError(
          IIObMonetaryNotesDetailsGeneralModel.AMOUNT,
          IExceptionsCodes.NR_AMOUNT_TO_COLLECT_NOT_EQUALS_NR_AMOUNT);
    }
  }

  public void setMonetaryNotesDetailsGeneralModelRep(
      IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsGeneralModelRep) {
    this.monetaryNotesDetailsGeneralModelRep = monetaryNotesDetailsGeneralModelRep;
  }

  public void setMonetaryNotesReferenceDocumentsRep(
      IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep) {
    this.monetaryNotesReferenceDocumentsRep = monetaryNotesReferenceDocumentsRep;
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setNotesReceivableRefDocumentValidator(
      IObNotesReceivableRefDocumentValidator notesReceivableRefDocumentValidator) {
    this.notesReceivableRefDocumentValidator = notesReceivableRefDocumentValidator;
  }
}
