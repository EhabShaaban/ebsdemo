package com.ebs.dda.validation;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.IValueObject;

public interface IActivateBusinessRulesValidator<V> {

  ValidationResult validateBusinessRules(V valueObject) throws RuntimeException;

}
