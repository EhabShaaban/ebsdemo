package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.paymentrequest.*;

import com.ebs.dda.jpa.masterdata.company.CObTreasury;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.company.IObCompanyTreasuriesGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestSaveDetailsPreparationGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyTreasuriesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import com.ebs.dda.validation.ValidationUtils;

public abstract class IObPaymentRequestPaymentDetailsSaveValidator {

  private static final String BANK_PAYMENT_FORM = PaymentFormEnum.PaymentForm.BANK.name();
  private static final String CASH_PAYMENT_FORM = PaymentFormEnum.PaymentForm.CASH.name();

  private IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep;
  private IObCompanyTreasuriesGeneralModelRep companyTreasuriesGeneralModelRep;
  private CObTreasuryRep treasuryRep;
  private DObPaymentRequestSaveDetailsPreparationGeneralModelRep paymentDetailsGeneralModelRep;
  protected ValidationResult validationResult;
  protected DObPaymentRequestSaveDetailsPreparationGeneralModel paymentRequest;
  public ValidationResult validate(IObPaymentRequestPaymentDetailsValueObject valueObject)
      throws Exception {
    validationResult = new ValidationResult();
     paymentRequest =
            paymentDetailsGeneralModelRep.findOneByCode(valueObject.getPaymentRequestCode());
    checkBankAccountClientBypassing(valueObject);
    checkTreasuryAccountClientBypassing(valueObject);
    checkBankData(valueObject);
    checkTreasuryData(valueObject);
    return validationResult;
  }

  private void checkTreasuryAccountClientBypassing(
          IObPaymentRequestPaymentDetailsValueObject valueObject) {
    checkTreasuryBelongsToCompany(valueObject, paymentRequest);
    checkTreasuryValueWhenPaymentIsNotCash(valueObject, paymentRequest);
  }

  private void checkTreasuryValueWhenPaymentIsNotCash(
          IObPaymentRequestPaymentDetailsValueObject valueObject,
          DObPaymentRequestSaveDetailsPreparationGeneralModel paymentDetailsGeneralModel){
    if (!paymentDetailsGeneralModel.getPaymentForm().equals(CASH_PAYMENT_FORM)
        && valueObject.getTreasuryCode() != null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkTreasuryBelongsToCompany(
          IObPaymentRequestPaymentDetailsValueObject valueObject,
          DObPaymentRequestSaveDetailsPreparationGeneralModel paymentDetailsGeneralModel)
      {
        if (valueObject.getTreasuryCode() != null) {
          IObCompanyTreasuriesGeneralModel companyTreasuries =
            companyTreasuriesGeneralModelRep.findOneByCompanyCodeAndTreasuryCode(
                    paymentDetailsGeneralModel.getCompanyCode(), valueObject.getTreasuryCode());
      ValidationUtils.checkIfObjectIsNull(companyTreasuries);
    }
  }
  private void checkBankData(IObPaymentRequestPaymentDetailsValueObject valueObject) {
    if (valueObject.getCompanyBankId() != null) {
      checkIfBankAccountExists(valueObject);
    }
  }

  private void checkTreasuryData(IObPaymentRequestPaymentDetailsValueObject valueObject) {
    if (valueObject.getTreasuryCode() != null) {
      checkIfTreasuryExists(valueObject);
    }
  }

  private void checkIfBankAccountExists(IObPaymentRequestPaymentDetailsValueObject valueObject) {
    IObCompanyBankDetailsGeneralModel companyBankDetails =
        companyBankDetailsGeneralModelRep.getOne(valueObject.getCompanyBankId());

    if (companyBankDetails == null) {
      validationResult.bindError(
          IIObPaymentRequestPaymentDetailsGeneralModel.COMPANY_BANK_ID,
          IExceptionsCodes.INCORRECT_FIELD_INPUT);
    }
  }

  private void checkIfTreasuryExists(IObPaymentRequestPaymentDetailsValueObject valueObject) {
    CObTreasury treasury = treasuryRep.findOneByUserCode(valueObject.getTreasuryCode());

    if (treasury == null) {
      validationResult.bindError(
          IIObPaymentRequestPaymentDetailsGeneralModel.TREASURY_CODE,
          IExceptionsCodes.INCORRECT_FIELD_INPUT);
    }
  }

  private void checkBankAccountClientBypassing(
          IObPaymentRequestPaymentDetailsValueObject valueObject){
    checkBankBelongsToCompany(valueObject);
    checkBankAccountValueWhenPaymentIsNotBank(valueObject);
  }

	private void checkBankBelongsToCompany(IObPaymentRequestPaymentDetailsValueObject valueObject) {
		if (valueObject.getCompanyBankId() != null) {
			IObCompanyBankDetailsGeneralModel companyBankDetails = companyBankDetailsGeneralModelRep
							.findOneByIdAndCompanyCode(valueObject.getCompanyBankId(),
											paymentRequest.getCompanyCode());
          ValidationUtils.checkIfObjectIsNull(companyBankDetails);
        }
	}

  private void checkBankAccountValueWhenPaymentIsNotBank(
          IObPaymentRequestPaymentDetailsValueObject valueObject) {
    if (!paymentRequest.getPaymentForm().equals(BANK_PAYMENT_FORM)
        && valueObject.getCompanyBankId() != null) {
      throw new ArgumentViolationSecurityException();
    }
  }
  public void setCompanyBankDetailsGeneralModelRep(
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep) {
    this.companyBankDetailsGeneralModelRep = companyBankDetailsGeneralModelRep;
  }

  public void setCompanyTreasuriesGeneralModelRep(
      IObCompanyTreasuriesGeneralModelRep companyTreasuriesGeneralModelRep) {
    this.companyTreasuriesGeneralModelRep = companyTreasuriesGeneralModelRep;
  }

  public void setTreasuryRep(CObTreasuryRep treasuryRep) {
    this.treasuryRep = treasuryRep;
  }

  public void setPaymentDetailsGeneralModelRep(DObPaymentRequestSaveDetailsPreparationGeneralModelRep paymentDetailsGeneralModelRep) {
    this.paymentDetailsGeneralModelRep = paymentDetailsGeneralModelRep;
  }
}
