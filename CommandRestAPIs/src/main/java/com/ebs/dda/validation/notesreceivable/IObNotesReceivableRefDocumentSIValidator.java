package com.ebs.dda.validation.notesreceivable;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.IDocumentTypes;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IObNotesReceivableRefDocumentSIValidator {
  Map<String, List<String>> validationErrors;

  private IObInvoiceSummaryGeneralModelRep salesInvoiceSummaryRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  public void validateRefDocument(
      String refDocumentCode,
      String refDocumentType,
      BigDecimal amountToCollect,
      DObNotesReceivablesGeneralModel dObNotesReceivablesGeneralModel,
      Map<String, List<String>> validationErrors) {

    this.validationErrors = validationErrors;

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(refDocumentCode);

    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        salesInvoiceSummaryRep.findOneByInvoiceCodeAndInvoiceType(
            refDocumentCode, IDocumentTypes.SALES_INVOICE);

    checkThatAmountToCollectForSIRefDocumentLessThanOrEqualItsRemaining(
        invoiceSummaryGeneralModel, refDocumentCode, refDocumentType, amountToCollect);

    checkSalesInvoiceState(salesInvoiceGeneralModel.getCurrentStates());

    checkBusinessPartner(
        salesInvoiceGeneralModel.getCustomerCode(),
        dObNotesReceivablesGeneralModel.getBusinessPartnerCode());

    checkBusinessUnit(
        salesInvoiceGeneralModel.getPurchaseUnitCode(),
        dObNotesReceivablesGeneralModel.getPurchaseUnitCode());

    checkCompany(
        salesInvoiceGeneralModel.getCompanyCode(),
        dObNotesReceivablesGeneralModel.getCompanyCode());
  }

  private void checkThatAmountToCollectForSIRefDocumentLessThanOrEqualItsRemaining(
      IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel,
      String refDocumentCode,
      String refDocumentType,
      BigDecimal amountToCollect) {
    String totalRemaining = invoiceSummaryGeneralModel.getTotalRemaining();
    if (amountToCollect.compareTo(new BigDecimal(totalRemaining)) > 0) {
      bindAmountToCollectError(refDocumentCode, refDocumentType);
    }
  }

  private void bindAmountToCollectError(String refDocumentCode, String refDocumentType) {
    validationErrors.computeIfAbsent(
        IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT,
        k -> new ArrayList<>());

    validationErrors
        .get(IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT)
        .add(refDocumentType + " " + refDocumentCode);
  }

  private void checkSalesInvoiceState(Set<String> currentStates)
      throws ArgumentViolationSecurityException {
    if (!currentStates.contains(DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkBusinessPartner(String refDocumentBusinessPartner, String nrBusinessPartner)
      throws ArgumentViolationSecurityException {
    if (!refDocumentBusinessPartner.equals(nrBusinessPartner)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkBusinessUnit(String refDocumentBusinessUnit, String nrBusinessUnit)
      throws ArgumentViolationSecurityException {
    if (!refDocumentBusinessUnit.equals(nrBusinessUnit)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkCompany(String refDocumentCompany, String nrCompany)
      throws ArgumentViolationSecurityException {
    if (!refDocumentCompany.equals(nrCompany)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setSalesInvoiceSummaryRep(IObInvoiceSummaryGeneralModelRep salesInvoiceSummaryRep) {
    this.salesInvoiceSummaryRep = salesInvoiceSummaryRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }
}
