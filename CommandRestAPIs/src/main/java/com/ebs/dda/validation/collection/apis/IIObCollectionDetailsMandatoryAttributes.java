package com.ebs.dda.validation.collection.apis;

import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import com.ebs.dda.jpa.accounting.collection.IIObCollectionDetailsGeneralModel;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public interface IIObCollectionDetailsMandatoryAttributes {

  String[] BANK_ACTIVE_STATE_MANDATORIES = {
    IIObCollectionDetailsGeneralModel.AMOUNT, IIObCollectionDetailsGeneralModel.BANK_ACCOUNT_CODE
  };
  String[] CASH_ACTIVE_STATE_MANDATORIES = {
    IIObCollectionDetailsGeneralModel.AMOUNT, IIObCollectionDetailsGeneralModel.TREASURY_CODE
  };
  Map<String, String[]> MANDATORIES_FOR_BANK =
      ImmutableMap.<String, String[]>builder()
          .put(DObCollectionStateMachine.ACTIVE, BANK_ACTIVE_STATE_MANDATORIES)
          .build();

  Map<String, String[]> MANDATORIES_FOR_CASH =
      ImmutableMap.<String, String[]>builder()
          .put(DObCollectionStateMachine.ACTIVE, CASH_ACTIVE_STATE_MANDATORIES)
          .build();
}
