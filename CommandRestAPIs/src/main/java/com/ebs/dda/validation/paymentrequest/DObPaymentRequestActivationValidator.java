package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.exceptions.data.MissingSystemDataException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentRequestAmountGreaterThanBalanceException;
import com.ebs.dda.commands.CommandUtils;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivatePreparationGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObPaymentRequestActivatePreparationGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;

public class DObPaymentRequestActivationValidator
  implements IActivateBusinessRulesValidator<DObPaymentRequestActivateValueObject> {

  private final CommandUtils commandUtils;
  protected ValidationResult validationResult;
  private DObPaymentRequestActivatePreparationGeneralModelRep
    paymentRequestActivatePreparationGeneralModelRep;
  protected DObPaymentRequestActivatePreparationGeneralModel activatePreparationGeneralModel;
  protected CObFiscalYearRep fiscalYearRep;

  public DObPaymentRequestActivationValidator() {
    commandUtils = new CommandUtils();
  }

  private void checkIfPaymentRequestAmountIsAvailableInBankOrTreasury(
    DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel) {
    checkBalanceExists(paymentRequestActivatePreparationGeneralModel);
    checkThatPaymentAmountGreaterThanBalance(paymentRequestActivatePreparationGeneralModel);
  }

  private void checkThatPaymentAmountGreaterThanBalance(
    DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel) {
    if (paymentRequestActivatePreparationGeneralModel.getPaymentNetAmount()
      .compareTo(paymentRequestActivatePreparationGeneralModel.getBalance()) > 0) {
      throw new DObPaymentRequestAmountGreaterThanBalanceException(validationResult);
    }
  }

  private void checkBalanceExists(
    DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel) {
    if (paymentRequestActivatePreparationGeneralModel.getBalance() == null) {
      throw new MissingSystemDataException("Balance does not exist!");
    }
  }

  @Override
  public ValidationResult validateBusinessRules(DObPaymentRequestActivateValueObject valueObject)
    throws RuntimeException {
    validationResult = new ValidationResult();

    activatePreparationGeneralModel = paymentRequestActivatePreparationGeneralModelRep
      .findOneByPaymentRequestCode(valueObject.getPaymentRequestCode());
    checkIfJournalDateFiscalYearIsActive(valueObject.getDueDate());
    checkIfFutureDateIsInserted(valueObject);
    checkIfPaymentRequestAmountIsAvailableInBankOrTreasury(activatePreparationGeneralModel);
    return validationResult;
  }

  private void checkIfJournalDateFiscalYearIsActive(String journalDate) {
    DateTime dueDate = getDateTime(journalDate);

    boolean isValidDueDate = fiscalYearRep
      .findByCurrentStatesLike("%" + BasicStateMachine.ACTIVE_STATE + "%")
      .stream()
      .anyMatch(year -> isJournalDateInActiveFiscalYear(year, dueDate));

    if (!isValidDueDate) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private boolean isJournalDateInActiveFiscalYear(CObFiscalYear year, DateTime dueDate) {
    return dueDate.isAfter(year.getFromDate()) && dueDate.isBefore(year.getToDate());
  }

  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }

  private void checkIfFutureDateIsInserted(DObPaymentRequestActivateValueObject valueObject) {
    DateTime currentDate = new DateTime();
    DateTime requestDate = commandUtils.getDateTime(valueObject.getDueDate());
    if (requestDate.isAfter(currentDate)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setPaymentRequestActivatePreparationGeneralModelRep(
    DObPaymentRequestActivatePreparationGeneralModelRep paymentRequestActivatePreparationGeneralModelRep) {
    this.paymentRequestActivatePreparationGeneralModelRep =
      paymentRequestActivatePreparationGeneralModelRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }
}
