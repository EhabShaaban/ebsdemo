package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsValueObject;

public class IObDownPaymentDetailsSaveValidator extends IObPaymentRequestPaymentDetailsSaveValidator{

    @Override
    public ValidationResult validate(IObPaymentRequestPaymentDetailsValueObject valueObject) throws Exception {
        super.validate(valueObject);
        return validationResult;
    }
}
