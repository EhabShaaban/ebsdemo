package com.ebs.dda.validation.actualcost;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.actualcost.ActualCostCalculateValueObject;
import com.ebs.dda.jpa.accounting.actualcost.IActualCostCalculateValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;

public class ActualCostCalculateValidator {

  private ValidationResult validationResult;
  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep
      goodsReceiptPurchaseOrderDataGeneralModelRep;
  private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  private DObPurchaseOrderRep orderRep;

  public ValidationResult validate(ActualCostCalculateValueObject actualCostCalculateValueObject)
      throws Exception {
    validationResult = new ValidationResult();
    DObPurchaseOrder validatableOrder = getPurchaseOrder(actualCostCalculateValueObject);
    checkOrderType(validatableOrder);
    return validationResult;
  }

  private DObPurchaseOrder getPurchaseOrder(
      ActualCostCalculateValueObject actualCostCalculateValueObject) {
    DObGoodsReceiptGeneralModel goodsReceipt =
        goodsReceiptGeneralModelRep.findOneByUserCode(
            actualCostCalculateValueObject.getGoodsReceiptCode());
    IObGoodsReceiptPurchaseOrderDataGeneralModel goodsReceiptPurchaseOrderData =
        goodsReceiptPurchaseOrderDataGeneralModelRep.findByGoodsReceiptCode(
            goodsReceipt.getUserCode());
    DObPurchaseOrder order =
        orderRep.findOneByUserCode(goodsReceiptPurchaseOrderData.getPurchaseOrderCode());
    return order;
  }

  private void checkOrderType(DObPurchaseOrder order) throws Exception {
    chekIfGoodsInvoiceExistForPO(order);

    if (order.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name())
        && validationResult.isValid()) {
      chekIfCustomTrusteeInvoiceExistForPO(order);
    }
  }

  private void chekIfGoodsInvoiceExistForPO(DObPurchaseOrder order) {
    String invoiceType = getInvoiceType(order.getObjectTypeCode());
    DObVendorInvoiceGeneralModel invoice =
        vendorInvoiceGeneralModelRep
            .findOneByPurchaseOrderCodeAndInvoiceTypeAndAndCurrentStatesLike(
                order.getUserCode(),
                    invoiceType,
                IActualCostCalculateValidator.POSTED_STATE);
    if (invoice == null) {
      validationResult.bindError(
          IActualCostCalculateValueObject.INVOICE, IExceptionsCodes.GOODS_INVOICE_NOT_EXIST);
    }
  }

  private String getInvoiceType(String objectTypeCode) {
    if(objectTypeCode.equals(OrderTypeEnum.Values.IMPORT_PO))
      return VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE;
    if(objectTypeCode.equals(OrderTypeEnum.Values.LOCAL_PO))
      return VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE;
    throw new IllegalArgumentException("Invalid Purchase Order Type");
  }

  private void chekIfCustomTrusteeInvoiceExistForPO(DObPurchaseOrder order) {
    DObVendorInvoiceGeneralModel invoice =
        vendorInvoiceGeneralModelRep
            .findOneByPurchaseOrderCodeAndInvoiceTypeAndAndCurrentStatesLike(
                order.getUserCode(),
                    VendorInvoiceTypeEnum.Values.CUSTOM_TRUSTEE_INVOICE,
                IActualCostCalculateValidator.POSTED_STATE);
    if (invoice == null) {
      validationResult.bindError(
          IActualCostCalculateValueObject.INVOICE, IExceptionsCodes.CUSTOM_TRUSTEE_NOT_EXIST);
    }
  }

  public void setGoodsReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }

  public void setGoodsReceiptPurchaseOrderDataGeneralModelRep(
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          goodsReceiptPurchaseOrderDataGeneralModelRep) {
    this.goodsReceiptPurchaseOrderDataGeneralModelRep =
        goodsReceiptPurchaseOrderDataGeneralModelRep;
  }

  public void setVendorInvoiceGeneralModelRep(
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
  }

  public void setOrderRep(DObPurchaseOrderRep orderRep) {
    this.orderRep = orderRep;
  }
}
