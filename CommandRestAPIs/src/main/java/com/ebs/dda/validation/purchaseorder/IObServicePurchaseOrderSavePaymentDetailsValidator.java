package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObServicePurchaseOrderPaymentDetailsValueObject;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;

public class IObServicePurchaseOrderSavePaymentDetailsValidator {

  private CObPaymentTermsRep paymentTermsRep;
  private CObCurrencyRep currencyRep;

  public void validate(
      IObServicePurchaseOrderPaymentDetailsValueObject valueObject)
      throws ArgumentViolationSecurityException {

    checkIfPaymentTermExists(valueObject);
    checkIfCurrencyExists(valueObject);
  }

  private void checkIfPaymentTermExists(
      IObServicePurchaseOrderPaymentDetailsValueObject valueObject)
      throws ArgumentViolationSecurityException {
    if (valueObject.getPaymentTermCode() != null) {
      CObPaymentTerms paymentTerm = paymentTermsRep
          .findOneByUserCode(valueObject.getPaymentTermCode());
      if (paymentTerm == null) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkIfCurrencyExists(IObServicePurchaseOrderPaymentDetailsValueObject valueObject)
      throws ArgumentViolationSecurityException {
    if (valueObject.getCurrencyCode() != null) {
      CObCurrency currency = currencyRep.findOneByUserCode(valueObject.getCurrencyCode());
      if (currency == null) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  public void setPaymentTermsRep(
      CObPaymentTermsRep paymentTermsRep) {
    this.paymentTermsRep = paymentTermsRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }
}
