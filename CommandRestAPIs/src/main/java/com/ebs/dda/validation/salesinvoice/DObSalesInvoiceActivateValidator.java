package com.ebs.dda.validation.salesinvoice;

import com.ebs.dac.foundation.exceptions.operational.accessability.ObjectStateNotValidException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.apis.IIObSalesInvoiceDetailsMandatoryAttributes;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceDeliverToCustomerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoicePostValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObSalesInvoiceActivateValidator
        implements IMissingFieldsValidator, IActivateBusinessRulesValidator<DObSalesInvoicePostValueObject> {

  private ValidationResult validationResult;
  private IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGMRep;
  private MandatoryValidatorUtils validatorUtils;
  private DObSalesInvoiceAuthorizationGeneralModelRep salesInvoiceGMRep;
  private CObFiscalYearRep fiscalYearRep;

  public DObSalesInvoiceActivateValidator() { validatorUtils = new MandatoryValidatorUtils(); }

  @Override
  public ValidationResult validateBusinessRules(DObSalesInvoicePostValueObject valueObject) throws RuntimeException {
    DObSalesInvoiceDeliverToCustomerGeneralModel salesInvoiceGeneralModel =
            salesInvoiceGMRep.findOneByUserCode(valueObject.getSalesInvoiceCode());
    return validate(salesInvoiceGeneralModel, valueObject);
  }

  @Deprecated
  public ValidationResult validate(
      DObSalesInvoiceDeliverToCustomerGeneralModel salesInvoiceGeneralModel,
      DObSalesInvoicePostValueObject valueObject) throws RuntimeException {

    validationResult = new ValidationResult();
    checkIfJournalDateFiscalYearIsActive(valueObject.getJournalEntryDate());

    if (isSalesInvoiceBasedOnSalesOrder(salesInvoiceGeneralModel)) {
      checkSalesOrderState(salesInvoiceGeneralModel);
    }
    return validationResult;
  }

  private boolean isSalesInvoiceBasedOnSalesOrder(DObSalesInvoiceDeliverToCustomerGeneralModel salesInvoiceGeneralModel) {
    return salesInvoiceGeneralModel.getInvoiceTypeCode().equals(DObSalesInvoiceFactory.ORDER_BASED_TYPE);
  }

  private void checkIfJournalDateFiscalYearIsActive(String journalDate) {
    DateTime dueDate = getDateTime(journalDate);

    boolean isValidDueDate = fiscalYearRep
        .findByCurrentStatesLike("%" + BasicStateMachine.ACTIVE_STATE + "%")
        .stream()
        .anyMatch(year -> isJournalDateInActiveFiscalYear(year, dueDate));

    if (!isValidDueDate) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }

  private boolean isJournalDateInActiveFiscalYear(CObFiscalYear year, DateTime dueDate) {
    return dueDate.isAfter(year.getFromDate()) && dueDate.isBefore(year.getToDate());
  }

  private void checkSalesOrderState(
      DObSalesInvoiceDeliverToCustomerGeneralModel salesInvoiceGeneralModel) throws RuntimeException {
    DObSalesOrderGeneralModel salesOrder = new DObSalesOrderGeneralModel();
    salesOrder.setCurrentStates(salesInvoiceGeneralModel.getSalesOrderCurrentStates());
    if (salesOrder != null) {
      checkIfTransitionIsAllowedBasedOnSalesOrderStateInStateMachine(salesOrder);
    }
  }

  private void checkIfTransitionIsAllowedBasedOnSalesOrderStateInStateMachine(
      DObSalesOrderGeneralModel salesOrder) throws ObjectStateNotValidException {
    try {
      DObSalesOrderStateMachine salesOrderStateMachine = new DObSalesOrderStateMachine();
      salesOrderStateMachine.initObjectState(salesOrder);
      if (!salesOrderStateMachine.canFireEvent(IDObSalesOrderActionNames.POST_INVOICE)) {
        throw new ObjectStateNotValidException();
      }
    } catch (Exception exception) {
      throw new ObjectStateNotValidException();
    }
  }

  public Map<String, Object> validateMissingFields(String salesInvoiceCode, String state) throws RuntimeException {
    Map<String, Object> salesInvoiceMissingFields = new HashMap<>();

    validatorUtils.populateSectionMissingFieldstoResult(salesInvoiceMissingFields,
            IDObSalesInvoiceSectionNames.SALES_INVOICE_DETAILS,
            validateSalesInvoiceDetailsMandatories(salesInvoiceCode));

    return salesInvoiceMissingFields;
  }

  public List<String> validateSalesInvoiceDetailsMandatories(String salesInvoiceCode) throws RuntimeException {
    IObSalesInvoiceBusinessPartnerGeneralModel salesInvoiceDetails =
            salesInvoiceBusinessPartnerGMRep.findOneByCode(salesInvoiceCode);
    String[] mandatoryAttributes =
            validatorUtils.getMandatoryAttributesByState(
                    IIObSalesInvoiceDetailsMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL,
                    DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE);
    return validatorUtils.validateMandatoriesForGeneralModel(
            mandatoryAttributes, salesInvoiceDetails);
  }

  public void setSalesInvoiceBusinessPartnerGeneralModelRep(IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep) {
    this.salesInvoiceBusinessPartnerGMRep = salesInvoiceBusinessPartnerGeneralModelRep;
  }

  public void setSalesInvoiceAuthorizationGeneralModelRep(DObSalesInvoiceAuthorizationGeneralModelRep salesInvoiceAuthorizationGeneralModelRep) {
    this.salesInvoiceGMRep = salesInvoiceAuthorizationGeneralModelRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }
}
