package com.ebs.dda.validation.masterdata.customer;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomerCreateValueObject;
import com.ebs.dda.jpa.masterdata.customer.MObCustomer;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerCreateValueObject;
import com.ebs.dda.jpa.masterdata.lookups.LObIssueFrom;
import com.ebs.dda.jpa.masterdata.lookups.LObTaxAdministrative;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIssueFromRep;
import com.ebs.dda.repositories.masterdata.lookups.LObTaxAdministrativeRep;

import java.util.List;

public class MObCustomerCreateMandatoryValidator {

  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private LObIssueFromRep issueFromRep;
  private LObTaxAdministrativeRep taxAdministrativeRep;
  private CObCurrencyRep currencyRep;
  private MObCustomerRep customerRep;
  private ValidationResult validationResult;


  public void validate(MObCustomerCreateValueObject valueObject)
      throws ArgumentViolationSecurityException {

    validateOnFieldIfNullOrEmpty(valueObject.getLegalName());
    validateOnFieldIfNullOrEmpty(valueObject.getMarketName());
    validateOnBusinessUnitCodesAreNotEmptyList(valueObject.getBusinessUnitCodes());
    validateOnCreditLimitIfNull(valueObject.getCreditLimit());
    validateOnFieldIfNullOrEmpty(valueObject.getCurrencyIso());
    validateOnFieldIfNullOrEmpty(valueObject.getRegistrationNumber());
    validateOnFieldIfNullOrEmpty(valueObject.getIssuedFromCode());
    validateOnFieldIfNullOrEmpty(valueObject.getTaxAdministrationCode());
    validateOnFieldIfNullOrEmpty(valueObject.getTaxCardNumber());
    validateOnFieldIfNullOrEmpty(valueObject.getTaxFileNumber());
    validateOnFieldIfNullOrEmpty(valueObject.getOldCustomerNumber());

    checkThatBusinessUnitsExist(valueObject.getBusinessUnitCodes());
    checkThatIssuedFromExists(valueObject.getIssuedFromCode());
    checkThatTaxAdministrationExists(valueObject.getTaxAdministrationCode());
    checkThatCurrencyExists(valueObject.getCurrencyIso());
  }

  public ValidationResult validateOnBusiness(MObCustomerCreateValueObject valueObject)
          throws Exception {
    validationResult = new ValidationResult();

    checkThatOldCustomerNumberIsUnique(valueObject.getOldCustomerNumber());
    return validationResult;
  }
  private void checkThatOldCustomerNumberIsUnique(String oldCustomerNumber) {
    MObCustomer customer = customerRep.findOneByOldCustomerNumber(oldCustomerNumber);
    if (customer != null) {
      validationResult.bindError(
              IMObCustomerCreateValueObject.OLD_CUSTOMER_NUMBER,
              IExceptionsCodes.CUSTOMER_OLD_NUMBER_EXIST);
    }
  }

  private void validateOnCreditLimitIfNull(Double creditLimit) throws ArgumentViolationSecurityException {
    if (creditLimit == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateOnBusinessUnitCodesAreNotEmptyList(List<String> businessUnitCodes)
      throws ArgumentViolationSecurityException {
    if (businessUnitCodes == null || businessUnitCodes.size() == 0) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateOnFieldIfNullOrEmpty(String fieldName) throws ArgumentViolationSecurityException {
    if (fieldName == null || fieldName.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatBusinessUnitsExist(List<String> purchasingUnitCodes)
      throws ArgumentViolationSecurityException {
    for (String purchasingUnitCode : purchasingUnitCodes) {
      checkThatBusinessUnitExists(purchasingUnitCode);
    }
  }

  private void checkThatBusinessUnitExists(String purchasingUnitCode)
      throws ArgumentViolationSecurityException {
    CObPurchasingUnitGeneralModel purchasingUnit =
        purchasingUnitGeneralModelRep.findOneByUserCode(purchasingUnitCode);
    if (purchasingUnit == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatIssuedFromExists(String issuedFromCode)
      throws ArgumentViolationSecurityException {
    LObIssueFrom issueFrom = issueFromRep.findOneByUserCode(issuedFromCode);
    if (issueFrom == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatTaxAdministrationExists(String taxAdministrationCode)
      throws ArgumentViolationSecurityException {
    LObTaxAdministrative taxAdministrative =
        taxAdministrativeRep.findOneByUserCode(taxAdministrationCode);
    if (taxAdministrative == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatCurrencyExists(String currencyIso)
      throws ArgumentViolationSecurityException {
    CObCurrency currency = currencyRep.findOneByIso(currencyIso);
    if (currency == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setIssueFromRep(LObIssueFromRep issueFromRep) {
    this.issueFromRep = issueFromRep;
  }

  public void setTaxAdministrativeRep(LObTaxAdministrativeRep taxAdministrativeRep) {
    this.taxAdministrativeRep = taxAdministrativeRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }
  public void setCustomerRep(MObCustomerRep customerRep) {
    this.customerRep = customerRep;
  }
}
