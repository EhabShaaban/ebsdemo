package com.ebs.dda.validation.salesorder;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerAddressGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerContactPersonGeneralModel;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataValueObject;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerAddressGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerContactPersonGeneralModelRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;

import java.util.Optional;

public class IObSalesOrderDataSaveMandatoryValidator {
  private IObCustomerAddressGeneralModelRep customerAddressGeneralModelRep;
  private IObCustomerContactPersonGeneralModelRep customerContactPersonGeneralModelRep;
  private IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep;
  private CObCurrencyRep currencyRep;
  private CObPaymentTermsRep paymentTermsRep;

  private Optional<IObCustomerAddressGeneralModel> customerAddressGeneralModel;
  private Optional<IObCustomerContactPersonGeneralModel> customerContactPersonGeneralModel;
  private IObSalesOrderDataGeneralModel salesOrderDataGeneralModel;
  private CObCurrency currencyRow;

  public void validate(IObSalesOrderDataValueObject valueObject)
      throws ArgumentViolationSecurityException {
    validateOnFieldIfNullOrEmpty(valueObject.getCurrencyCode());
    validateOnFieldIfEqualSpaces(valueObject.getPaymentTermCode());
    validateOnFieldIfEqualSpaces(valueObject.getExpectedDeliveryDate());

    setSalesOrderDataBySalesOrderCode(valueObject.getSalesOrderCode());
    checkThatCustomerAddressExist(valueObject.getCustomerAddressId());
    checkThatCustomerAddressIsRelatedToThatCustomer(valueObject.getCustomerAddressId());
    checkThatCustomerContactPersonExist(valueObject.getContactPersonId());
    checkThatCustomerContactPersonIsRelatedToThatCustomer(valueObject.getContactPersonId());
    checkThatPaymentTermExist(valueObject.getPaymentTermCode());
    setCurrencyRowByCurrencyCode(valueObject.getCurrencyCode());
    checkThatCurrencyExist();
    checkThatCurrencyIsEGPForLocalSalesOrder();
  }

  private void validateOnFieldIfNullOrEmpty(String fieldName)
      throws ArgumentViolationSecurityException {
    if (fieldName == null || fieldName.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateOnFieldIfEqualSpaces(String fieldName)
      throws ArgumentViolationSecurityException {
    if (fieldName != null && fieldName.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatPaymentTermExist(String paymentTermCode)
      throws ArgumentViolationSecurityException {
    if (paymentTermCode != null) {
      CObPaymentTerms paymentTerm = paymentTermsRep.findOneByUserCode(paymentTermCode);
      if (paymentTerm == null) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkThatCustomerAddressExist(Long customerAddressId)
      throws ArgumentViolationSecurityException {
    if (customerAddressId != null) {
      customerAddressGeneralModel = customerAddressGeneralModelRep.findById(customerAddressId);
      if (!customerAddressGeneralModel.isPresent()) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkThatCustomerAddressIsRelatedToThatCustomer(Long customerAddressId)
      throws ArgumentViolationSecurityException {
    if (customerAddressId != null) {
      String customerCode = salesOrderDataGeneralModel.getCustomerCode();
      String actualCustomerCode = customerAddressGeneralModel.get().getUserCode();
      if (!actualCustomerCode.equals(customerCode)) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkThatCustomerContactPersonExist(Long customerContactPersonId)
      throws ArgumentViolationSecurityException {
    if (customerContactPersonId != null) {
      customerContactPersonGeneralModel =
          customerContactPersonGeneralModelRep.findById(customerContactPersonId);
      if (!customerContactPersonGeneralModel.isPresent()) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void checkThatCustomerContactPersonIsRelatedToThatCustomer(Long customerContactPersonId)
      throws ArgumentViolationSecurityException {
    if (customerContactPersonId != null) {
      String customerCode = salesOrderDataGeneralModel.getCustomerCode();
      String actualCustomerCode = customerContactPersonGeneralModel.get().getUserCode();
      if (!actualCustomerCode.equals(customerCode)) {
        throw new ArgumentViolationSecurityException();
      }
    }
  }

  private void setSalesOrderDataBySalesOrderCode(String salesOrderCode) {
    salesOrderDataGeneralModel =
        salesOrderDataGeneralModelRep.findOneBySalesOrderCode(salesOrderCode);
  }

  private void setCurrencyRowByCurrencyCode(String currencyCode) {
    currencyRow = currencyRep.findOneByUserCode(currencyCode);
  }

  private void checkThatCurrencyExist() throws ArgumentViolationSecurityException {
    if (currencyRow == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkThatCurrencyIsEGPForLocalSalesOrder()
      throws ArgumentViolationSecurityException {
    String currencyISO = currencyRow.getIso();
    if (!currencyISO.equals("EGP")) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setCustomerAddressGeneralModelRep(
      IObCustomerAddressGeneralModelRep customerAddressGeneralModelRep) {
    this.customerAddressGeneralModelRep = customerAddressGeneralModelRep;
  }

  public void setCustomerContactPersonGeneralModelRep(
      IObCustomerContactPersonGeneralModelRep customerContactPersonGeneralModelRep) {
    this.customerContactPersonGeneralModelRep = customerContactPersonGeneralModelRep;
  }

  public void setSalesOrderDataGeneralModelRep(
      IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep) {
    this.salesOrderDataGeneralModelRep = salesOrderDataGeneralModelRep;
  }

  public void setCurrencyRep(CObCurrencyRep currencyRep) {
    this.currencyRep = currencyRep;
  }

  public void setPaymentTermsRep(CObPaymentTermsRep paymentTermsRep) {
    this.paymentTermsRep = paymentTermsRep;
  }
}
