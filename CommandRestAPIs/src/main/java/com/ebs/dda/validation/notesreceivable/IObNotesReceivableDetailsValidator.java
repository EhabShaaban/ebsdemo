package com.ebs.dda.validation.notesreceivable;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsValueObject;
import com.ebs.dda.jpa.masterdata.lookups.LObDepot;
import com.ebs.dda.repositories.masterdata.lookups.LObDepotRep;
import com.ebs.dda.validation.ValidationUtils;

public class IObNotesReceivableDetailsValidator {

  private ValidationResult validationResult;
  private LObDepotRep depotRep;

  public ValidationResult validate(
      IObMonetaryNotesDetailsValueObject monetaryNotesDetailsValueObject){
    validationResult = new ValidationResult();
    validateDepotIsExists(monetaryNotesDetailsValueObject);
    return validationResult;
  }

  private void validateDepotIsExists(
      IObMonetaryNotesDetailsValueObject monetaryNotesDetailsValueObject){
    String depotCode = monetaryNotesDetailsValueObject.getDepotCode();
    if (depotCode != null) {
      LObDepot depot = depotRep.findOneByUserCode(depotCode);
      ValidationUtils.checkIfObjectIsNull(depot);
    }
  }

  public void setDepotRep(LObDepotRep depotRep) {
    this.depotRep = depotRep;
  }
}
