package com.ebs.dda.validation.vendorinvoice;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.accounting.vendorinvoice.exceptions.DObVendorInvoiceHasNoEstimatedLandedCostException;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceActivateValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObPurchaseOrderVendorGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObPurchaseOrderVendorGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;

public class DObVendorInvoiceActivateServiceInvoiceValidator extends DObVendorInvoiceActivateValidator {

    private IObPurchaseOrderVendorGeneralModelRep orderVendorGeneralModelRep;
    private DObLandedCostGeneralModelRep landedCostGeneralModelRep;

    private void checkThatEstimatedLandedCostExistForPurchaseOrder(String servicePurchaseOrderCode) {
        IObPurchaseOrderVendorGeneralModel orderVendorGeneralModel =
                orderVendorGeneralModelRep.findOneByUserCode(servicePurchaseOrderCode);
        String purchaseOrderCode = orderVendorGeneralModel.getReferencePOCode();
        if (purchaseOrderCode == null || purchaseOrderCode.isEmpty()) {
            return;
        }
        DObLandedCostGeneralModel landedCostGeneralModel =
                landedCostGeneralModelRep.findByPurchaseOrderCodeAndCurrentStatesLike
                        (purchaseOrderCode, '%' + DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED + '%');
        if (landedCostGeneralModel == null) {
            throw new DObVendorInvoiceHasNoEstimatedLandedCostException(validationResult);
        }
    }

    public void setOrderVendorGeneralModelRep(IObPurchaseOrderVendorGeneralModelRep orderVendorGeneralModelRep) {
        this.orderVendorGeneralModelRep = orderVendorGeneralModelRep;
    }

    public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
        this.landedCostGeneralModelRep = landedCostGeneralModelRep;
    }

    @Override
    public ValidationResult validateBusinessRules(DObVendorInvoiceActivateValueObject valueObject) throws RuntimeException {
        super.validateBusinessRules(valueObject);
        checkThatEstimatedLandedCostExistForPurchaseOrder(invoiceDetails.getPurchaseOrderCode());
        return validationResult;
    }
}


