package com.ebs.dda.validation.collection;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionCreateValueObject;
import com.ebs.dda.jpa.accounting.collection.IDObCollection;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionCreateValueObject;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

import java.math.BigDecimal;

import static com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes.General_MISSING_FIELD_INPUT;
import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

public class DObCollectionCreationValidator {

  private ValidationResult validationResult;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;

  private DocumentOwnerGeneralModelRep documentOwnerRep;

  public ValidationResult validate(DObCollectionCreateValueObject collectionCreateValueObject)
          throws Exception {
    validationResult = new ValidationResult();
    checkMandatoriesExist(collectionCreateValueObject);
    checkDataBasedOnDocumentRef(collectionCreateValueObject);
    validateDocumentOwner(collectionCreateValueObject.getDocumentOwnerId());
    return validationResult;
  }

  private void checkMandatoriesExist(DObCollectionCreateValueObject collectionCreateValueObject)
          throws Exception {
    checkIfObjectIsNull(collectionCreateValueObject.getCollectionMethod());
    checkIfObjectIsNull(collectionCreateValueObject.getCollectionType());
    checkIfObjectIsNull(collectionCreateValueObject.getRefDocumentCode());
    checkIfObjectIsNull(collectionCreateValueObject.getDocumentOwnerId());
  }

  private void checkDataBasedOnDocumentRef(
          DObCollectionCreateValueObject collectionCreateValueObject) throws Exception {
    if (collectionCreateValueObject
            .getCollectionType()
            .equals(IDObCollectionCreationValidator.SALES_INVOICE_TYPE)) {
      checkSalesInvoiceData(collectionCreateValueObject);
    } else if (collectionCreateValueObject
            .getCollectionType()
            .equals(IDObCollectionCreationValidator.NOTES_RECEIVABLE_TYPE)) {
      checkNotesReceivableData(collectionCreateValueObject);
    } else if (collectionCreateValueObject
            .getCollectionType()
            .equals(IDObCollectionCreationValidator.SALES_ORDER_TYPE)) {
      checkSalesOrderData(collectionCreateValueObject);
    }
  }

  private void checkSalesOrderData(DObCollectionCreateValueObject collectionCreateValueObject)
          throws Exception {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
            salesOrderGeneralModelRep.findOneByUserCode(
                    collectionCreateValueObject.getRefDocumentCode());

    checkIfRefDocumentExist(salesOrderGeneralModel);
    checkSalesOrderState(salesOrderGeneralModel);
    checkSalesOrderRemaining(salesOrderGeneralModel);
  }

  private void checkSalesOrderRemaining(DObSalesOrderGeneralModel salesOrderGeneralModel) throws Exception {
    if (salesOrderGeneralModel.getRemaining().compareTo(BigDecimal.ZERO) == 0){
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkSalesOrderState(DObSalesOrderGeneralModel salesOrderGeneralModel)
          throws Exception {
    if (salesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.READY_FOR_DELIVERY)
            || salesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.SALES_INVOICE_ACTIVATED)) {
      this.validationResult.bindError(
              IDObCollectionGeneralModel.REF_DOCUMENT_CODE, IExceptionsCodes.REF_DOCUMENT_INVALID_STATE);
    } else if (!(salesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.APPROVED)
            || salesOrderGeneralModel
            .getCurrentStates()
            .contains(DObSalesOrderStateMachine.GOODS_ISSUE_ACTIVATED))) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkSalesInvoiceData(DObCollectionCreateValueObject collectionCreateValueObject)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(
            collectionCreateValueObject.getRefDocumentCode());
    checkIfRefDocumentExist(salesInvoiceGeneralModel);
    checkSaleInvoiceState(salesInvoiceGeneralModel);
    checkSalesInvoiceRemaining(salesInvoiceGeneralModel);
  }

  private void checkSalesInvoiceRemaining(DObSalesInvoiceGeneralModel salesInvoiceGeneralModel) throws Exception {
    if (salesInvoiceGeneralModel.getRemaining().equals(BigDecimal.ZERO)){
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkSaleInvoiceState(DObSalesInvoiceGeneralModel salesInvoiceGeneralModel)
          throws Exception {
    if (!salesInvoiceGeneralModel
            .getCurrentStates()
            .contains(DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE)) {
        throw new ArgumentViolationSecurityException();
    }
  }

  private void checkNotesReceivableState(
          DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) throws Exception {
    boolean isStateAllowed = false;
    for (String state : notesReceivablesGeneralModel.getCurrentStates()) {
      if (state.equals(DObNotesReceivablesStateMachine.DRAFT_STATE)) {
        throw new ArgumentViolationSecurityException();
      }

      if (notesReceivablesGeneralModel
              .getCurrentStates()
              .contains(DObNotesReceivablesStateMachine.ACTIVE_STATE)) {
        isStateAllowed = true;
        break;
      }
    }
    if (!isStateAllowed) {
      validationResult.bindError(
          IDObCollectionCreateValueObject.REF_DOCUMENT_CODE,
          IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE);
    }
  }

  private void checkNotesReceivableData(DObCollectionCreateValueObject collectionCreateValueObject)
      throws Exception {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
            notesReceivablesGeneralModelRep.findOneByUserCode(
                    collectionCreateValueObject.getRefDocumentCode());

    checkIfRefDocumentExist(notesReceivablesGeneralModel);
    checkNotesReceivableState(notesReceivablesGeneralModel);
  }

  private void checkIfRefDocumentExist(Object refDocumentGeneralModel) throws Exception {
    if (refDocumentGeneralModel == null) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void validateDocumentOwner(Long documentOwnerId) throws Exception {
    checkThatDocumnetOwnerIsExist(documentOwnerId);
  }

  private void checkThatDocumnetOwnerIsExist(Long documentOwnerId) {
    DocumentOwnerGeneralModel docOwner =
        documentOwnerRep.findOneByUserIdAndObjectName(documentOwnerId, IDObCollection.SYS_NAME);
    if (docOwner == null) {
      validationResult.bindError(
          IDObCollectionCreateValueObject.DOCUMENT_OWNER_ID, General_MISSING_FIELD_INPUT);
    }
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setDocumentOwnerRep(DocumentOwnerGeneralModelRep documentOwnerRep) {
    this.documentOwnerRep = documentOwnerRep;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }
}
