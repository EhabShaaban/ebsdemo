package com.ebs.dda.validation;

import java.util.Map;

public interface IMissingFieldsValidator {

  Map<String, Object> validateMissingFields(String userCode, String state);
}
