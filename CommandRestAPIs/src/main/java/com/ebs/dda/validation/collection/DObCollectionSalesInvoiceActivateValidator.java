package com.ebs.dda.validation.collection;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.collection.exceptions.CollectionAmountGreaterThanSalesInvoiceRemainingException;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;

public class DObCollectionSalesInvoiceActivateValidator extends DObCollectionActivateValidator {

  private DObSalesInvoiceGeneralModelRep salesInvoiceGMRep;

  @Override
  protected void validateRefDocument(DObCollectionGeneralModel collection) {

    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGMRep.findOneByUserCode(collection.getRefDocumentCode());

    checkSalesInvoiceState(salesInvoice);

    assertThatCollectionAmountNotGreaterThanRemaining(collection, salesInvoice);
  }

  private void checkSalesInvoiceState(DObSalesInvoiceGeneralModel salesInvoiceGeneralModel)
      throws ArgumentViolationSecurityException {
    if (!salesInvoiceGeneralModel.getCurrentStates()
        .contains(DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void assertThatCollectionAmountNotGreaterThanRemaining(
      DObCollectionGeneralModel collection, DObSalesInvoiceGeneralModel salesInvoice) {

    if (collection.getAmount().compareTo(salesInvoice.getRemaining()) > 0 ) {

      validationResult.bindError(IDObCollectionGeneralModel.AMOUNT,
          IExceptionsCodes.C_AMOUNT_GREATER_THAN_SALES_INVOICE_REMAINING);

      throw new CollectionAmountGreaterThanSalesInvoiceRemainingException(validationResult);
    }
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGMRep = salesInvoiceGeneralModelRep;
  }

}
