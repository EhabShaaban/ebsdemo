package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;

public class DObPaymentAgainstCreditNoteCreationValidator
				extends DObPaymentRequestCreationValidator {

	public ValidationResult validate(DObPaymentRequestCreateValueObject creationValueObject)
					throws Exception {
		super.validate(creationValueObject);
		checkDueDocumentType(creationValueObject);
		return validationResult;
	}

	private void checkDueDocumentType(DObPaymentRequestCreateValueObject creationValueObject) {
		checkIfDocumentTypeIsAccountingNote(creationValueObject.getDueDocumentType());
	}

	private void checkIfDocumentTypeIsAccountingNote(Object object) {
		if (!DueDocumentTypeEnum.CREDITNOTE.equals(String.valueOf(object))) {
			throw new ArgumentViolationSecurityException();
		}
	}
}
