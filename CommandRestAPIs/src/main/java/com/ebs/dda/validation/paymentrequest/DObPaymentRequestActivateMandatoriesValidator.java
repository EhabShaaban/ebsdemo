package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentMissingDueDocumentDataException;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentRequestNoExchangeRateException;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.accounting.paymentrequest.validation.DObPaymentRequestPaymentDetailsMandatoryValidator;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivatePreparationGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObPaymentRequestActivatePreparationGeneralModelRep;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObPaymentRequestActivateMandatoriesValidator implements IMissingFieldsValidator {

    private final ValidationResult validationResult;
    private final MandatoryValidatorUtils mandatoryValidatorUtils;
    private DObPaymentRequestPaymentDetailsMandatoryValidator paymentDetailsMandatoryValidator;
    private DObPaymentRequestActivatePreparationGeneralModelRep paymentRequestActivatePreparationGeneralModelRep;

    public DObPaymentRequestActivateMandatoriesValidator() {
        mandatoryValidatorUtils = new MandatoryValidatorUtils();
        validationResult = new ValidationResult();
    }

    private void checkIfPaymentRequestHasCompany(DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel) {
        if (paymentRequestActivatePreparationGeneralModel.getCompanyCode() == null) {
            throw new DObPaymentMissingDueDocumentDataException(validationResult);
        }
    }

    private void checkIfPaymentRequestHasCurrency(DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel) {
        if (paymentRequestActivatePreparationGeneralModel.getCurrencyCode() == null) {
            throw new DObPaymentMissingDueDocumentDataException(validationResult);
        }
    }

    private void checkIfPaymentRequestHasExchangeRate(DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel) {
        if (paymentRequestActivatePreparationGeneralModel.getExchangeRate()
                == null) {
            throw new DObPaymentRequestNoExchangeRateException(validationResult);
        }
    }

    private List<String> validatePaymentDetailsSection(String paymentRequestCode, String state)
            throws RuntimeException {
        return paymentDetailsMandatoryValidator.validateGeneralModelMandatories(
                paymentRequestCode, state);
    }

    public void setPaymentDetailsMandatoryValidator(
            DObPaymentRequestPaymentDetailsMandatoryValidator paymentDetailsMandatoryValidator) {
        this.paymentDetailsMandatoryValidator = paymentDetailsMandatoryValidator;
    }

    public void setPaymentRequestActivatePreparationGeneralModelRep(DObPaymentRequestActivatePreparationGeneralModelRep paymentRequestActivatePreparationGeneralModelRep) {
        this.paymentRequestActivatePreparationGeneralModelRep = paymentRequestActivatePreparationGeneralModelRep;
    }

    @Override
    public Map<String, Object> validateMissingFields(String paymentCode, String state) throws RuntimeException {
        Map<String, Object> missingFields = new HashMap<>();
        mandatoryValidatorUtils.populateSectionMissingFieldstoResult(missingFields,
                DObPaymentRequestStateMachine.PAYMENT_DONE_STATE,
                validatePaymentDetailsSection(paymentCode, DObPaymentRequestStateMachine.PAYMENT_DONE_STATE));
        if (!missingFields.isEmpty()) {
            return missingFields;
        }
        DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel = paymentRequestActivatePreparationGeneralModelRep.findOneByPaymentRequestCode(paymentCode);
        checkIfPaymentRequestHasCompany(paymentRequestActivatePreparationGeneralModel);
        checkIfPaymentRequestHasCurrency(paymentRequestActivatePreparationGeneralModel);
        checkIfPaymentRequestHasExchangeRate(paymentRequestActivatePreparationGeneralModel);
        return missingFields;
    }
}
