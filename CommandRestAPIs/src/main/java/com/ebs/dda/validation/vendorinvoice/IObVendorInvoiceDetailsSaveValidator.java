package com.ebs.dda.validation.vendorinvoice;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsValueObject;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;

public class IObVendorInvoiceDetailsSaveValidator {

    private ValidationResult validationResult;
    private CObCurrencyRep currencyRep;
    private CObPaymentTermsRep paymentTermsRep;

    private IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsRep;

    public ValidationResult validate(IObVendorInvoiceDetailsValueObject valueObject) throws Exception {
        validationResult = new ValidationResult();
        checkCurrencyBypassing(valueObject);
        checkPaymentTermBypassing(valueObject);
        return validationResult;
    }

    private void checkPaymentTermBypassing(IObVendorInvoiceDetailsValueObject valueObject)
            throws Exception {
        if (valueObject.getPaymentTermCode() != null) {
            CObPaymentTerms paymentTerms =
                    paymentTermsRep.findOneByUserCode(valueObject.getPaymentTermCode());
            if (paymentTerms == null) {
                throw new ArgumentViolationSecurityException();
            }
        }
    }

    private void checkCurrencyBypassing(IObVendorInvoiceDetailsValueObject valueObject) throws Exception {
        if (valueObject.getCurrencyCode() != null) {
            CObCurrency currency = currencyRep.findOneByUserCode(valueObject.getCurrencyCode());
            if (currency == null) {
                throw new ArgumentViolationSecurityException();
            }
    }
    }

    public void setCurrencyRep(CObCurrencyRep currencyRep) {
        this.currencyRep = currencyRep;
    }

    public void setcObPaymentTermsRep(CObPaymentTermsRep cObPaymentTermsRep) {
        this.paymentTermsRep = cObPaymentTermsRep;
    }

    public void setInvoiceDetailsRep(IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsRep) {
        this.invoiceDetailsRep = invoiceDetailsRep;
    }
}
