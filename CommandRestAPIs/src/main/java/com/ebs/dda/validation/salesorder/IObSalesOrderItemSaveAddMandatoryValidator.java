package com.ebs.dda.validation.salesorder;

import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemValueObject;
import com.ebs.dda.order.salesorder.ISalesOrderItemsMandatoryAttributes;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

public class IObSalesOrderItemSaveAddMandatoryValidator {
  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;

  public void validate(IObSalesOrderItemValueObject valueObject) throws Exception {
    mandatoryValidatorUtils = new MandatoryValidatorUtils();

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(valueObject.getSalesOrderCode());

    mandatoryValidatorUtils.validateMandatoriesForValueObjectAsObjectState(
        ISalesOrderItemsMandatoryAttributes.MANDATORIES,
        salesOrderGeneralModel.getCurrentState(),
        valueObject);
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }
}
