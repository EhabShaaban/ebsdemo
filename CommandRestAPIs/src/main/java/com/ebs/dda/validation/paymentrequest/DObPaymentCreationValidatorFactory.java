package com.ebs.dda.validation.paymentrequest;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestCreateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;

@Component
public class DObPaymentCreationValidatorFactory implements ApplicationContextAware {
	private ApplicationContext applicationContext;

	public DObPaymentRequestCreationValidator getValidatorInstance(
					DObPaymentRequestCreateValueObject creationValueObject) {
		PaymentTypeEnum.PaymentType paymentType = creationValueObject.getPaymentType();
		if (paymentType != null && !paymentType.toString().isEmpty()) {
			if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR)) {
				return getPaymentToVendorValidatorBean(creationValueObject);
			}
			if (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)) {
				return applicationContext.getBean(DObGeneralPaymentCreationValidator.class);
			}
			if (paymentType.equals(PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE)) {
				return applicationContext
								.getBean(DObPaymentAgainstCreditNoteCreationValidator.class);
			}
			if (paymentType.equals(PaymentTypeEnum.PaymentType.UNDER_SETTLEMENT)) {
				return applicationContext.getBean(DObUnderSettlementPaymentCreationValidator.class);
			}
		}
		throw new ArgumentViolationSecurityException();
	}

	private DObPaymentForVendorCreationValidator getPaymentToVendorValidatorBean(DObPaymentRequestCreateValueObject creationValueObject) {
		DueDocumentTypeEnum.DueDocumentType dueDocumentType = creationValueObject
				.getDueDocumentType();
		if (dueDocumentType != null && !dueDocumentType.toString().isEmpty()) {
			if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE)) {
				return applicationContext.getBean(
								DObPaymentAgainstVendorInvoiceCreationValidator.class);
			}
			if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)) {
				return applicationContext.getBean(DObDownPaymentCreationValidator.class);
			}
		}
		throw new ArgumentViolationSecurityException();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
