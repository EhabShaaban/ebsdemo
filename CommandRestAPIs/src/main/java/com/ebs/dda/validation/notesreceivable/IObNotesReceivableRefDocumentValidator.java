package com.ebs.dda.validation.notesreceivable;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableReferenceDocumentsTypeEnum;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IObNotesReceivableRefDocumentValidator {
  private IObNotesReceivableRefDocumentNRValidator refDocumentNRValidator;
  private IObNotesReceivableRefDocumentSOValidator refDocumentSOValidator;
  private IObNotesReceivableRefDocumentSIValidator refDocumentSIValidator;
  private ValidationResult validationResult;
  private Map<String, String> errorsMessageCode;
  private Map<String, List<String>> validationErrors;

  private void setErrorsMessageCode() {
    errorsMessageCode = new HashMap<>();
    errorsMessageCode.put(
        IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT,
        IExceptionsCodes.NR_AMOUNT_TO_COLLECT_REF_DOCUMENT_GREATER_THAN_ITS_REMAINING);

    errorsMessageCode.put(
        IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE,
        IExceptionsCodes.NR_SALES_ORDER_REF_DOCUMENT_INVALID_STATE);
  }

  public void validateRefDocument(
      String refDocumentCode,
      String refDocumentType,
      BigDecimal amountToCollect,
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel,
      ValidationResult validationResult) {
    this.validationResult = validationResult;
    validationErrors = new HashMap<>();

    if (isNotesReceivable(refDocumentType)) {
      refDocumentNRValidator.validateRefDocument(
          refDocumentCode,
          refDocumentType,
          amountToCollect,
          notesReceivablesGeneralModel,
          validationErrors);
    } else if (isSalesOrder(refDocumentType)) {
      refDocumentSOValidator.validateRefDocument(
          refDocumentCode,
          refDocumentType,
          amountToCollect,
          notesReceivablesGeneralModel,
          validationErrors);
    } else if (isSalesInvoice(refDocumentType)) {
      refDocumentSIValidator.validateRefDocument(
          refDocumentCode,
          refDocumentType,
          amountToCollect,
          notesReceivablesGeneralModel,
          validationErrors);
    }

    setValidationResult();
  }

  private void setValidationResult() {
    setErrorsMessageCode();
    for (Map.Entry<String, List<String>> validationError : validationErrors.entrySet()) {
      validationResult.appendBindErrorWithValues(
          validationError.getKey(),
          errorsMessageCode.get(validationError.getKey()),
          validationError.getValue());
    }
  }

  private boolean isNotesReceivable(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.NOTES_RECEIVABLE.name());
  }

  private boolean isSalesOrder(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_ORDER.name());
  }

  private boolean isSalesInvoice(String type) {
    return type.equals(NotesReceivableReferenceDocumentsTypeEnum.SALES_INVOICE.name());
  }

  public void setRefDocumentNRValidator(
      IObNotesReceivableRefDocumentNRValidator refDocumentNRValidator) {
    this.refDocumentNRValidator = refDocumentNRValidator;
  }

  public void setRefDocumentSOValidator(
      IObNotesReceivableRefDocumentSOValidator refDocumentSOValidator) {
    this.refDocumentSOValidator = refDocumentSOValidator;
  }

  public void setRefDocumentSIValidator(
      IObNotesReceivableRefDocumentSIValidator refDocumentSIValidator) {
    this.refDocumentSIValidator = refDocumentSIValidator;
  }
}
