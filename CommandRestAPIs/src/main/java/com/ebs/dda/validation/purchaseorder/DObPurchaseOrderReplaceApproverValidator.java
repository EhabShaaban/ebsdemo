package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dda.approval.dbo.jpa.entities.businessobjects.CObControlPoint;
import com.ebs.dda.approval.dbo.jpa.entities.informationobjects.IObControlPointUsers;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.CObControlPointRep;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.informationObjects.IObControlPointUsersRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprovalCycle;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderApprover;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApprovalCycleRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApproverRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderAssignMeToApproveValueObject;
import com.ebs.dda.purchases.exceptions.ControlPointApproverHasAlreadyApprovedException;
import com.ebs.dda.purchases.exceptions.ControlPointApproverHasAlreadyAssignedException;
import com.ebs.dda.purchases.exceptions.ReplacedApproverNotInControlPointUsersException;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import java.util.ArrayList;
import java.util.List;

public class DObPurchaseOrderReplaceApproverValidator {

  private IUserAccountSecurityService userAccountSecurityService;
  private IObOrderApprovalCycleRep approvalCycleRep;
  private IObOrderApproverRep approverRep;
  private DObPurchaseOrderRep purchaseOrderRep;
  private CObControlPointRep controlPointRep;
  private IObControlPointUsersRep controlPointUsersRep;

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setApprovalCycleRep(IObOrderApprovalCycleRep approvalCycleRep) {
    this.approvalCycleRep = approvalCycleRep;
  }

  public void setControlPointRep(CObControlPointRep controlPointRep) {
    this.controlPointRep = controlPointRep;
  }

  public void setControlPointUsersRep(IObControlPointUsersRep controlPointUsersRep) {
    this.controlPointUsersRep = controlPointUsersRep;
  }

  public void setApproverRep(IObOrderApproverRep approverRep) {
    this.approverRep = approverRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void validate(
      String purchaseOrderCode,
      DObPurchaseOrderAssignMeToApproveValueObject valueObject,
      String approverAction)
      throws Exception {
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    IBDKUser loggedInUser = userAccountSecurityService.getLoggedInUser();
    IObOrderApprovalCycle currentUndecidedCycle =
        approvalCycleRep.findByRefInstanceIdAndFinalDecisionIsNull(purchaseOrder.getId());

    checkIfPurchaseOrderIsDraft(purchaseOrder);
    checkIfActionIsAllowedInCurrentState(purchaseOrder, approverAction);
    checkIfLoggedInUserExistsForControlPoint(loggedInUser, valueObject.getControlPointCode());
    checkIfControlPointUserAlreadyApprovedOrAssigned(
        currentUndecidedCycle.getId(), valueObject.getControlPointCode(), loggedInUser);
  }

  private void checkIfLoggedInUserExistsForControlPoint(
      IBDKUser loggedInUser, String controlPointCode)
      throws ReplacedApproverNotInControlPointUsersException {
    CObControlPoint controlPoint = controlPointRep.findOneByUserCode(controlPointCode);

    List<IObControlPointUsers> controlPointUsers =
        controlPointUsersRep.findByRefInstanceId(controlPoint.getId());

    if (!isLoggedInUserCanAssignHimselfForControlPoint(loggedInUser, controlPointUsers)) {
      throw new ReplacedApproverNotInControlPointUsersException();
    }
  }

  private boolean isLoggedInUserCanAssignHimselfForControlPoint(
      IBDKUser loggedInUser, List<IObControlPointUsers> controlPointUsers) {
    Long loggedInUserId = loggedInUser.getUserId();
    List<Long> controlPointUsersIds = new ArrayList<>();
    for (IObControlPointUsers controlPointUser : controlPointUsers) {
      controlPointUsersIds.add(controlPointUser.getUserId());
    }
    return controlPointUsersIds.contains(loggedInUserId);
  }

  private void checkIfActionIsAllowedInCurrentState(DObPurchaseOrder purchaseOrder, String approverAction)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    purchaseOrderStateMachine.isAllowedAction(approverAction);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory = new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private void checkIfPurchaseOrderIsDraft(DObPurchaseOrder purchaseOrder) {
    if (purchaseOrder.getCurrentStates().contains(DObImportPurchaseOrderStateMachine.DRAFT_STATE)) {
      throw new AuthorizationException("Purchase order with draft state");
    }
  }

  private IObOrderApprover getControlPointCurrentApprover(
      Long approvalCycleId, Long controlPointId) {

    List<IObOrderApprover> approversList =
        approverRep.findByRefInstanceIdOrderByIdAsc(approvalCycleId);

    for (IObOrderApprover approver : approversList) {
      if (approver.getDecision() == null && approver.getControlPointId().equals(controlPointId)) {
        return approver;
      }
    }
    return null;
  }

  private void checkIfControlPointUserAlreadyApprovedOrAssigned(
      Long currentUndecidedCycleId, String controlPointCode, IBDKUser loggedInUser)
      throws ControlPointApproverHasAlreadyApprovedException,
      ControlPointApproverHasAlreadyAssignedException {

    CObControlPoint controlPoint = controlPointRep.findOneByUserCode(controlPointCode);

    IObOrderApprover approver =
        getControlPointCurrentApprover(currentUndecidedCycleId, controlPoint.getId());
    if (approver == null) {
      throw new ControlPointApproverHasAlreadyApprovedException();
    }

    if (approver.getApproverId().equals(loggedInUser.getUserId())) {
      throw new ControlPointApproverHasAlreadyAssignedException();
    }
  }
}
