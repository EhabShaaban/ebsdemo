package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderCycleDatesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDobPurchaseOrderProductionFinishedValueObject;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import java.util.Arrays;
import org.joda.time.DateTime;

public class DObPurchaseOrderMarkAsProductionFinishedValidator
    extends DObPurchaseOrderStateTransitionCommonValidator {

  private ValidationResult validationResult;

  public ValidationResult validate(String purchaseOrderCode, DateTime valueObjectProductionDate)
      throws Exception {
    validationResult = new ValidationResult();

    DObPurchaseOrder purchaseOrder = getPurchaseOrderRep().findOneByUserCode(purchaseOrderCode);
    checkIfPurchaseOrderIsDraft(purchaseOrder);
    checkIfTransitionIsAllowedInCurrentState(purchaseOrder);

    checkThatProductionFinishedDateIsAfterConfirmationDate(
        purchaseOrderCode, valueObjectProductionDate);
    return validationResult;
  }

  private void checkThatProductionFinishedDateIsAfterConfirmationDate(
      String purchaseOrderCode, DateTime valueObjectProductionDate) {
    DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel =
        getpurchaseOrderCycleDatesGeneralModelRep().findOneByUserCode(purchaseOrderCode);

    if (purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getConfirmationDate() != null
        && !isValidTransitionDateTime(
            purchaseOrderCycleDatesGeneralModel.getConfirmationDate(), valueObjectProductionDate)) {

      validationResult.bindErrorWithValues(
          IDobPurchaseOrderProductionFinishedValueObject.PRODUCTION_FINISHED_DATE_TIME,
          IExceptionsCodes.INVALID_STATE_DATE,
          Arrays.asList(
              getDateWithUserDefaultTimeZone(
                  purchaseOrderCycleDatesGeneralModel.getConfirmationDate())));
    }
  }

  private void checkIfTransitionIsAllowedInCurrentState(DObPurchaseOrder purchaseOrder) throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    purchaseOrderStateMachine.isAllowedAction(
        IPurchaseOrderActionNames.MARK_AS_PRODUCTION_FINISHED);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory = new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }
}
