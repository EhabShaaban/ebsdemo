package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.ValidationUtils;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCreateValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;

/**
 * @author Niveen Magdy
 * @since Agust 27, 2018
 */
public class DObPurchaseOrderCreateValidator {

  private ValidationResult validationResult;
  private MObVendorGeneralModelRep vendorRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;

  public ValidationResult validateBusinessRules(DObPurchaseOrderCreateValueObject valueObject)
      throws Exception {
    validationResult = new ValidationResult();

    checkIfBusinessUnitExists(valueObject.getBusinessUnitCode());
    if (valueObject.getType().name().equals(OrderTypeEnum.Values.SERVICE_PO)) {
      checkIfRefPurchaseOrderExists(valueObject.getReferencePurchaseOrderCode());
      checkIfBusinessUnitIsRelatedToRefPurchaseOrder(valueObject);
      checkIfRefPurchaseOrderIsInInvalidState(valueObject.getReferencePurchaseOrderCode());
    }
    checkIfVendorInstanceExist(valueObject);
    checkIfDocumentOwnerIsValid(valueObject);

    return validationResult;
  }

  private void checkIfBusinessUnitIsRelatedToRefPurchaseOrder(
      DObPurchaseOrderCreateValueObject valueObject) throws Exception {
    String businessUnitCode = valueObject.getBusinessUnitCode();
    String refPurchaseOrderCode = valueObject.getReferencePurchaseOrderCode();

    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderGeneralModelRep.findOneByUserCode(refPurchaseOrderCode);

    if (!businessUnitCode.equals(purchaseOrder.getPurchaseUnitCode())) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void validateMissingMandatoryFields(DObPurchaseOrderCreateValueObject creationValueObject)
      throws Exception {
    ValidationUtils.checkIfFieldValueIsMissing(creationValueObject.getType());
    if (creationValueObject.getType().name().equals(OrderTypeEnum.Values.SERVICE_PO)) {
      ValidationUtils.checkIfFieldValueIsMissing(
          creationValueObject.getReferencePurchaseOrderCode());
    }
    ValidationUtils.checkIfFieldValueIsMissing(creationValueObject.getBusinessUnitCode());
    ValidationUtils.checkIfFieldValueIsMissing(creationValueObject.getVendorCode());
    ValidationUtils.checkIfFieldValueIsMissing(creationValueObject.getDocumentOwnerId());
  }

  private void checkIfBusinessUnitExists(String businessUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel businessUnit =
        purchasingUnitGeneralModelRep.findOneByUserCode(businessUnitCode);
    if (null == businessUnit) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfRefPurchaseOrderExists(String refPurchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderGeneralModelRep.findOneByUserCode(refPurchaseOrderCode);
    if (null == purchaseOrder) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkIfRefPurchaseOrderIsInInvalidState(String refPurchaseOrderCode) {
    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderGeneralModelRep.findOneByUserCode(refPurchaseOrderCode);
    if (isInvalidPurchaseOrder(purchaseOrder, DObImportPurchaseOrderStateMachine.CANCELLED_STATE)
        || isInvalidPurchaseOrder(
            purchaseOrder, DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE)) {
      validationResult.bindError(
          IDObPurchaseOrderGeneralModel.REFERENCE_PO_CODE,
          IExceptionsCodes.General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH);
    }
  }

  private boolean isInvalidPurchaseOrder(DObPurchaseOrderGeneralModel purchaseOrder, String state) {
    return purchaseOrder.getCurrentStates().contains(state);
  }

  private void checkIfVendorInstanceExist(DObPurchaseOrderCreateValueObject creationValueObject) {
    MObVendorGeneralModel vendor = vendorRep.findOneByUserCode(creationValueObject.getVendorCode());
    if (null == vendor) {
      validationResult.bindError(
          IDObPurchaseOrderGeneralModel.VENDOR_CODE, IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  private void checkIfDocumentOwnerIsValid(DObPurchaseOrderCreateValueObject creationValueObject) {
    DocumentOwnerGeneralModel documentOwner =
        documentOwnerGeneralModelRep.findOneByUserIdAndObjectName(
            creationValueObject.getDocumentOwnerId(), IDObPurchaseOrder.SYS_NAME);
    if (null == documentOwner) {
      validationResult.bindError(
          IDObPurchaseOrderGeneralModel.DOCUMENT_OWNER_ID,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  public void setVendorRep(MObVendorGeneralModelRep vendorRep) {
    this.vendorRep = vendorRep;
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

  public void setDocumentOwnerGeneralModelRep(
      DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
    this.documentOwnerGeneralModelRep = documentOwnerGeneralModelRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }
}
