package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsQuantitiesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsQuantitiesGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsQuantitiesValueObject;

public class DObPurchaseOrderSaveItemQuantityValidator {
  private ValidationResult validationResult;
  private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;
  private IObOrderLineDetailsQuantitiesGeneralModelRep orderLineDetailsQuantitiesGeneralModelRep;

  public ValidationResult validate(IObOrderLineDetailsQuantitiesValueObject valueObject)
      throws Exception {
    validationResult = new ValidationResult();
    checkIfOrderUnitAlreadyExistsInItemInPurchaseOrder(valueObject);
    checkIfMeasureIsValid(valueObject);
    return validationResult;
  }

  private void checkIfOrderUnitAlreadyExistsInItemInPurchaseOrder(
      IObOrderLineDetailsQuantitiesValueObject valueObject) throws Exception {

    IObOrderLineDetailsQuantitiesGeneralModel orderLineDetailsQuantities =
        getOrderLineDetailsQuantities(valueObject);

    if (orderLineDetailsQuantities != null) {
      if (valueObject.getQuantityId() != null
          && valueObject.getQuantityId().equals(orderLineDetailsQuantities.getId())) {
        return;
      }
      throw new ArgumentViolationSecurityException();
    }
  }

  private IObOrderLineDetailsQuantitiesGeneralModel getOrderLineDetailsQuantities(
      IObOrderLineDetailsQuantitiesValueObject valueObject) {
    IObOrderLineDetailsGeneralModel orderLineDetails = getOrderLineDetails(valueObject);
    return orderLineDetailsQuantitiesGeneralModelRep.findByRefInstanceIdAndOrderUnitCode(
        orderLineDetails.getId(), valueObject.getOrderUnitCode());
  }

  private IObOrderLineDetailsGeneralModel getOrderLineDetails(
      IObOrderLineDetailsQuantitiesValueObject valueObject) {
    return orderLineDetailsGeneralModelRep.findByOrderCodeAndItemCode(
        valueObject.getOrderCode(), valueObject.getItemCode());
  }

  private void checkIfMeasureIsValid(IObOrderLineDetailsQuantitiesValueObject valueObject) {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(valueObject.getOrderCode());

    ItemVendorRecordGeneralModel itemVendorRecordUnitOfMeasure =
        itemVendorRecordGeneralModelRep
            .findOneByItemCodeAndVendorCodeAndUomCodeAndPurchasingUnitCode(
                valueObject.getItemCode(),
                orderGeneralModel.getVendorCode(),
                valueObject.getOrderUnitCode(),
                orderGeneralModel.getPurchaseUnitCode());

    if (itemVendorRecordUnitOfMeasure == null) {
      validationResult.bindError(
          IIObOrderLineDetailsQuantitiesValueObject.ORDER_UNIT_CODE,
          IExceptionsCodes.PO_MEASURE_DOES_NOT_HAVE_IVR);
    }
  }

  public void setItemVendorRecordGeneralModelRep(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    this.itemVendorRecordGeneralModelRep = itemVendorRecordGeneralModelRep;
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }

  public void setOrderLineDetailsGeneralModelRep(
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep) {
    this.orderLineDetailsGeneralModelRep = orderLineDetailsGeneralModelRep;
  }

  public void setOrderLineDetailsQuantitiesGeneralModelRep(
      IObOrderLineDetailsQuantitiesGeneralModelRep orderLineDetailsQuantitiesGeneralModelRep) {
    this.orderLineDetailsQuantitiesGeneralModelRep = orderLineDetailsQuantitiesGeneralModelRep;
  }
}
