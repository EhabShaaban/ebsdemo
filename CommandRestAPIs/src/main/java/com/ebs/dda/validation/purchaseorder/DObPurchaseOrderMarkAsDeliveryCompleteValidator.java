package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModel;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderCycleDatesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderDeliveryCompleteValueObject;
import com.ebs.dda.purchases.exceptions.PurchaseOrderHasNoPostedGoodsReceiptException;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrderRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;

public class DObPurchaseOrderMarkAsDeliveryCompleteValidator
    extends DObPurchaseOrderStateTransitionCommonValidator {

  private static final String LOCAL = "Local";
  private static final String IMPORT = "Import";
  private DObGoodsReceiptPurchaseOrderRep goodsReceiptRep;
  private IObGoodsReceiptPurchaseOrderDataGeneralModelRep
      goodsReceiptPurchaseOrderDataGeneralModelRep;

  private ValidationResult validationResult;

  private Map<String, List<String>> allowedStatesWithType = new HashMap<String, List<String>>();

  public ValidationResult validate(String purchaseOrderCode, DateTime deliveryCompleteDate)
      throws Exception {
    fillAllowedStatesWithType();

    ValidationResult validationResult =
        validatePurchaseOrder(purchaseOrderCode, deliveryCompleteDate);
    if (validationResult.isValid()) {
      validateGoodsReceipt(purchaseOrderCode);
    }
    return validationResult;
  }

  private void validateGoodsReceipt(String purchaseOrderCode) throws Exception {
    List<IObGoodsReceiptPurchaseOrderDataGeneralModel> goodsReceiptPurchaseOrderDataGeneralModels =
        goodsReceiptPurchaseOrderDataGeneralModelRep.findByPurchaseOrderCode(purchaseOrderCode);

    if (goodsReceiptPurchaseOrderDataGeneralModels.isEmpty()) {
      throw new PurchaseOrderHasNoPostedGoodsReceiptException();
    }
    boolean hasPostedGoodsReceipt =
        checkIfGoodsRecieptsHasPostedState(goodsReceiptPurchaseOrderDataGeneralModels);
    if (!hasPostedGoodsReceipt) {
      throw new PurchaseOrderHasNoPostedGoodsReceiptException();
    }
  }

  private boolean checkIfGoodsRecieptsHasPostedState(
      List<IObGoodsReceiptPurchaseOrderDataGeneralModel>
          goodsReceiptPurchaseOrderDataGeneralModels) {
    boolean hasPostedGoodsReceipt = false;

    for (IObGoodsReceiptPurchaseOrderDataGeneralModel goodsReceiptPurchaseOrderDataGeneralModel :
        goodsReceiptPurchaseOrderDataGeneralModels) {
      DObGoodsReceipt goodsReceipt =
          goodsReceiptRep.findOneByUserCode(
              goodsReceiptPurchaseOrderDataGeneralModel.getGoodsReceiptCode());
      if (goodsReceipt.getCurrentStates().contains(DObGoodsReceiptStateMachine.ACTIVE_STATE)) {
        hasPostedGoodsReceipt = true;
        break;
      }
    }

    return hasPostedGoodsReceipt;
  }

  private ValidationResult validatePurchaseOrder(
      String purchaseOrderCode, DateTime deliveryCompleteDateTime) throws Exception {
    validationResult = new ValidationResult();

    DObPurchaseOrder purchaseOrder = getPurchaseOrderRep().findOneByUserCode(purchaseOrderCode);
    checkIfPurchaseOrderIsValidState(purchaseOrder);
    checkIfTransitionIsAllowedInCurrentState(purchaseOrder);

    DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel =
        getpurchaseOrderCycleDatesGeneralModelRep().findOneByUserCode(purchaseOrderCode);

    String PurchaseOrderType = getPurchaseOrderType(purchaseOrder);
    List<String> lastDate = new ArrayList<>();
    if ((PurchaseOrderType.equals(IMPORT)
            && !isValidImportPODeliveryCompleteDate(
                purchaseOrderCycleDatesGeneralModel, deliveryCompleteDateTime, lastDate))
        || (PurchaseOrderType.equals(LOCAL)
            && !isValidLocalPODeliveryCompleteDate(
                purchaseOrderCycleDatesGeneralModel, deliveryCompleteDateTime, lastDate))) {
      validationResult.bindErrorWithValues(
          IDObPurchaseOrderDeliveryCompleteValueObject.DELIVERY_COMPLETE_DATE_TIME,
          IExceptionsCodes.INVALID_STATE_DATE,
          lastDate);
    }
    return validationResult;
  }

  private boolean isValidLocalPODeliveryCompleteDate(
      DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel,
      DateTime deliveryCompleteDate,
      List<String> lastDate) {
    if (purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getShippingDate() != null) {
      lastDate.add(
          getDateWithUserDefaultTimeZone(purchaseOrderCycleDatesGeneralModel.getShippingDate()));
      return isValidTransitionDateTime(
          purchaseOrderCycleDatesGeneralModel.getShippingDate(), deliveryCompleteDate);
    }
    return false;
  }

  private boolean isValidImportPODeliveryCompleteDate(
      DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel,
      DateTime deliveryCompleteDate,
      List<String> lastDate) {
    if (purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getClearanceDate() != null) {
      lastDate.add(
          getDateWithUserDefaultTimeZone(purchaseOrderCycleDatesGeneralModel.getClearanceDate()));
      return isValidTransitionDateTime(
          purchaseOrderCycleDatesGeneralModel.getClearanceDate(), deliveryCompleteDate);
    }
    return false;
  }

  private void fillAllowedStatesWithType() {
    fillAllowedStatesForLocal();
    fillAllowedStatesForImport();
  }

  private void fillAllowedStatesForLocal() {
    List<String> allowedStatesForLocalPO = new ArrayList<String>();
    allowedStatesForLocalPO.add(DObImportPurchaseOrderStateMachine.SHIPPED_STATE);
    allowedStatesForLocalPO.add(DObImportPurchaseOrderStateMachine.CANCELLED_STATE);
    allowedStatesForLocalPO.add(DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE);
    allowedStatesWithType.put(LOCAL, allowedStatesForLocalPO);
  }

  private void fillAllowedStatesForImport() {
    List<String> allowedStatesForImportPO = new ArrayList<String>();
    allowedStatesForImportPO.add(DObImportPurchaseOrderStateMachine.CLEARED_STATE);
    allowedStatesForImportPO.add(DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE);

    allowedStatesWithType.put(IMPORT, allowedStatesForImportPO);
  }

  private void checkIfTransitionIsAllowedInCurrentState(DObPurchaseOrder purchaseOrder)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    purchaseOrderStateMachine.isAllowedAction(IPurchaseOrderActionNames.MARK_AS_DELIVERY_COMPLETE);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  private void checkIfPurchaseOrderIsValidState(DObPurchaseOrder purchaseOrder) {
    boolean isPurchaseOrderInValidState = false;

    List<String> allowedStates = allowedStatesWithType.get(getPurchaseOrderType(purchaseOrder));

    for (String state : allowedStates) {
      if (purchaseOrder.getCurrentStates().contains(state)) {
        isPurchaseOrderInValidState = true;
        break;
      }
    }
    if (!isPurchaseOrderInValidState) {
      throw new AuthorizationException(
          "Mark As Production Finished purhcase order with "
              + purchaseOrder.getCurrentStates()
              + " state");
    }
  }

  private String getPurchaseOrderType(DObPurchaseOrder purchaseOrder) {
    if (purchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name())) {
      return IMPORT;
    } else {
      return LOCAL;
    }
  }

  public void setGoodsReceiptPurchaseOrderDataGeneralModelRep(
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          goodsReceiptPurchaseOrderDataGeneralModelRep) {
    this.goodsReceiptPurchaseOrderDataGeneralModelRep =
        goodsReceiptPurchaseOrderDataGeneralModelRep;
  }

  public void setGoodsReceiptRep(DObGoodsReceiptPurchaseOrderRep goodsReceiptRep) {
    this.goodsReceiptRep = goodsReceiptRep;
  }
}
