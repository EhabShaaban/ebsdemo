package com.ebs.dda.validation.chartofaccounts;

import java.util.List;

import com.ebs.dac.foundation.exceptions.operational.accessability.DependentInstanceException;
import com.ebs.dda.jpa.accounting.journalentry.IObJournalEntryItemGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;

public class HObChartOfAccountDeleteValidator {

	private HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep;
	private IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep;

	public void validate(String glAccountCode) throws Exception {
		checkThatAccountHasChilds(glAccountCode);
		checkThatAccountIsAssignedToJournalItem(glAccountCode);
	}

	private void checkThatAccountIsAssignedToJournalItem(String glAccountCode) throws Exception {
		List<IObJournalEntryItemGeneralModel> journalItems = journalEntryItemsGeneralModelRep
						.findAllByAccountCode(glAccountCode);
		if (!journalItems.isEmpty()) {
			throw new DependentInstanceException();
		}
	}

	private void checkThatAccountHasChilds(String glAccountCode) throws Exception {
		List<HObChartOfAccountsGeneralModel> childs = glAccountsGeneralModelRep
						.findAllByParentCode(glAccountCode);
		if (!childs.isEmpty()) {
			throw new DependentInstanceException();
		}
	}

	public void setGlAccountsGeneralModelRep(
					HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep) {
		this.glAccountsGeneralModelRep = glAccountsGeneralModelRep;
	}

	public void setJournalEntryItemsGeneralModelRep(
					IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep) {
		this.journalEntryItemsGeneralModelRep = journalEntryItemsGeneralModelRep;
	}
}
