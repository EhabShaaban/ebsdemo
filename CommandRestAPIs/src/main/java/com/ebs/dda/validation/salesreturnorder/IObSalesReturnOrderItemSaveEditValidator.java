package com.ebs.dda.validation.salesreturnorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.lookups.ILObReason;
import com.ebs.dda.jpa.masterdata.lookups.LObReason;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemValueObject;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemValueObject;
import com.ebs.dda.repositories.masterdata.lookups.LObReasonRep;

public class IObSalesReturnOrderItemSaveEditValidator {
  private LObReasonRep lObReasonRep;
  private ValidationResult validationResult;

  public ValidationResult validate(IObSalesReturnOrderItemValueObject valueObject)
      throws Exception {
    validationResult = new ValidationResult();

    checkIfReturnReasonExists(valueObject);


    return validationResult;
  }

  private void checkIfReturnReasonExists(IObSalesReturnOrderItemValueObject valueObject) {
    String returnReasonCode = valueObject.getReturnReasonCode();

    if (returnReasonCode == null) {
      return;
    }

    LObReason reason =
        lObReasonRep.findOneByUserCodeAndType(returnReasonCode, ILObReason.SRO_REASON);

    if (reason == null) {
      validationResult.bindError(
          IIObSalesReturnOrderItemValueObject.RETURN_REASON_CODE,
          IExceptionsCodes.General_MISSING_FIELD_INPUT);
    }
  }

  public void setLObReasonRep(LObReasonRep lObReasonRep) {
    this.lObReasonRep = lObReasonRep;
  }
}
