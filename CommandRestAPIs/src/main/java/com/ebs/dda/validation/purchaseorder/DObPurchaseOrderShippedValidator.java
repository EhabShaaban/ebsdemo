package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderCycleDatesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderShippedValueObject;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;

public class DObPurchaseOrderShippedValidator
    extends DObPurchaseOrderStateTransitionCommonValidator {

  private static final String LOCAL = "Local";
  private static final String IMPORT = "Import";
  private ValidationResult validationResult;

  public ValidationResult validate(String purchaseOrderCode, DateTime shippingDateTime)
      throws Exception {
    validationResult = new ValidationResult();

    DObPurchaseOrder dObPurchaseOrder = getPurchaseOrderRep().findOneByUserCode(purchaseOrderCode);

    if (dObPurchaseOrder == null) {
      throw new ArgumentViolationSecurityException();
    }

    if (dObPurchaseOrder.getCurrentStates().contains("Draft")) {
      throw new ArgumentViolationSecurityException();
    }

    DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel =
        getpurchaseOrderCycleDatesGeneralModelRep().findOneByUserCode(purchaseOrderCode);
    String PurchaseOrderType = getPurchaseOrderType(dObPurchaseOrder);

    List<String> lastDate = new ArrayList<>();
    if ((PurchaseOrderType.equals(IMPORT)
            && !isValidImportPOShippingDate(
                purchaseOrderCycleDatesGeneralModel, shippingDateTime, lastDate))
        || (PurchaseOrderType.equals(LOCAL)
            && !isValidLocalPOShippingDate(
                purchaseOrderCycleDatesGeneralModel, shippingDateTime, lastDate))) {
      validationResult.bindErrorWithValues(
          IDObPurchaseOrderShippedValueObject.SHIPPING_DATE_TIME,
          IExceptionsCodes.INVALID_STATE_DATE,
          lastDate);
    }
    return validationResult;
  }

  private String getPurchaseOrderType(DObPurchaseOrder purchaseOrder) {
    if (purchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name())) {
      return IMPORT;
    } else {
      return LOCAL;
    }
  }

  private boolean isValidLocalPOShippingDate(
      DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel,
      DateTime shippingDateTime,
      List<String> lastDate) {
    if (purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getConfirmationDate() != null) {
      lastDate.add(
          getDateWithUserDefaultTimeZone(
              purchaseOrderCycleDatesGeneralModel.getConfirmationDate()));
      return isValidTransitionDateTime(
          purchaseOrderCycleDatesGeneralModel.getConfirmationDate(), shippingDateTime);
    }
    return false;
  }

  private boolean isValidImportPOShippingDate(
      DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel,
      DateTime shippingDateTime,
      List<String> lastDate) {
    if (purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getProductionFinishedDate() != null) {
      lastDate.add(
          getDateWithUserDefaultTimeZone(
              purchaseOrderCycleDatesGeneralModel.getProductionFinishedDate()));
      return isValidTransitionDateTime(
          purchaseOrderCycleDatesGeneralModel.getProductionFinishedDate(), shippingDateTime);
    }
    return false;
  }
}
