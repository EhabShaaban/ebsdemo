package com.ebs.dda.validation.paymentrequest;

import java.util.Arrays;
import java.util.List;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestSaveDetailsPreparationGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsValueObject;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.validation.ValidationUtils;

public class IObGeneralPaymentDetailsSaveValidator
				extends IObPaymentRequestPaymentDetailsSaveValidator {
	private static final String SERVICE_ITEM_TYPE = "SERVICE";

	private CObCurrencyRep currencyRep;
	private MObItemGeneralModelRep itemGMRep;

	@Override
	public ValidationResult validate(IObPaymentRequestPaymentDetailsValueObject valueObject)
					throws Exception {
		super.validate(valueObject);
		checkCurrencyExists(valueObject.getCurrencyCode());
		checkCostFactorItem(valueObject, paymentRequest);
		return validationResult;
	}

	private void checkCurrencyExists(String currencyCode) {
		if (currencyCode != null) {
			CObCurrency currency = currencyRep.findOneByUserCode(currencyCode);
			if (currency == null) {
				throw new ArgumentViolationSecurityException();
			}
		}
	}

	private void checkCostFactorItem(IObPaymentRequestPaymentDetailsValueObject valueObject,
					DObPaymentRequestSaveDetailsPreparationGeneralModel paymentRequest) {

		if (valueObject.getCostFactorItemCode() == null) {
			return;
		}
		MObItemGeneralModel item = itemGMRep.findByUserCode(valueObject.getCostFactorItemCode());
		ValidationUtils.checkIfObjectIsNull(item);
		checkItemIsSameBusinessUnitAsSelected(paymentRequest, item);
		checkItemInActiveState(item);
		checkItemIsServiceItem(item);
		checkItemIsCostFactorItem(item);
	}

	private void checkItemIsCostFactorItem(MObItemGeneralModel item) {
		if (!item.getCostFactor()) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkItemIsServiceItem(MObItemGeneralModel item) {
		if (!item.getTypeCode().equals(SERVICE_ITEM_TYPE)) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkItemInActiveState(MObItemGeneralModel item) {
		if (!item.getCurrentStates().contains(BasicStateMachine.ACTIVE_STATE)) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkItemIsSameBusinessUnitAsSelected(
					DObPaymentRequestSaveDetailsPreparationGeneralModel paymentRequest,
					MObItemGeneralModel item) {
		String paymentPUCode = paymentRequest.getBusinessUnitCode();
		List<String> itemPUCodes = Arrays.asList(item.getPurchasingUnitCodes().split(","));
		if (!itemPUCodes.contains(paymentPUCode)) {
			throw new ArgumentViolationSecurityException();
		}
	}

	public void setCurrencyRep(CObCurrencyRep currencyRep) {
		this.currencyRep = currencyRep;
	}

	public void setItemGMRep(MObItemGeneralModelRep itemGMRep) {
		this.itemGMRep = itemGMRep;
	}

}
