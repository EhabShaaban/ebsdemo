package com.ebs.dda.validation.collection;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.collection.apis.IDObCollectionSectionNames;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.collection.DObCollectionActivateValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

public abstract class DObCollectionActivateValidator
    implements IMissingFieldsValidator,
        IActivateBusinessRulesValidator<DObCollectionActivateValueObject> {

  protected ValidationResult validationResult;
  protected DObCollectionGeneralModelRep collectionGeneralModelRep;
  protected IObCollectionDetailsDataMandatoryValidator collectionDetailsDataMandatoryValidator;
  private MandatoryValidatorUtils validatorUtils;
  private CObFiscalYearRep fiscalYearRep;

  public DObCollectionActivateValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  @Override
  public ValidationResult validateBusinessRules(DObCollectionActivateValueObject valueObject)
      throws RuntimeException {

    DObCollectionGeneralModel collectionGeneralModel =
        collectionGeneralModelRep.findOneByUserCode(valueObject.getCollectionCode());

    validationResult = new ValidationResult();
    checkMandatoriesExist(valueObject);
    validateRefDocument(collectionGeneralModel);
    checkThatJournalEntryDateInActiveFiscalYear(valueObject.getJournalEntryDate());
    return validationResult;
  }

  private void checkThatJournalEntryDateInActiveFiscalYear(String journalEntryDate) {
    DateTime date = getDateTime(journalEntryDate);
    List<CObFiscalYear> fiscalYears = fiscalYearRep.findByCurrentStatesLike("%Active%");
    boolean correctFiscalYear = false;
    for (CObFiscalYear fiscalYear : fiscalYears) {
      if (date.isAfter(fiscalYear.getFromDate()) && date.isBefore(fiscalYear.getToDate())) {
        correctFiscalYear = true;
        break;
      }
    }
    if (!correctFiscalYear) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }

  protected abstract void validateRefDocument(DObCollectionGeneralModel collectionGeneralModel);

  private void checkMandatoriesExist(DObCollectionActivateValueObject collectionActivateValueObject)
      throws ArgumentViolationSecurityException {
    checkIfObjectIsNull(collectionActivateValueObject.getJournalEntryDate());
  }

  public Map<String, Object> validateMissingFields(String collectionCode, String state)
      throws RuntimeException {
    Map<String, Object> collectionMissingFields = new HashMap<>();

    validatorUtils.populateSectionMissingFieldstoResult(
        collectionMissingFields,
        IDObCollectionSectionNames.COLLECTION_DETAILS,
        validateCollectionDetailsSection(collectionCode, state));

    return collectionMissingFields;
  }

  private List<String> validateCollectionDetailsSection(String collectionCode, String state)
      throws RuntimeException {
    return collectionDetailsDataMandatoryValidator.validateGeneralModelMandatories(
        collectionCode, state);
  }

  public void setCollectionGeneralModelRep(DObCollectionGeneralModelRep collectionGeneralModelRep) {
    this.collectionGeneralModelRep = collectionGeneralModelRep;
  }

  public void setCollectionDetailsDataMandatoryValidator(
      IObCollectionDetailsDataMandatoryValidator collectionDetailsDataMandatoryValidator) {
    this.collectionDetailsDataMandatoryValidator = collectionDetailsDataMandatoryValidator;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }
}
