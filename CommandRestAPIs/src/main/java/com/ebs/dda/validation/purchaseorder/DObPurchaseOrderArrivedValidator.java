package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderCycleDatesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IDObPurchaseOrderArrivalValueObject;
import org.joda.time.DateTime;

import java.util.Arrays;

public class DObPurchaseOrderArrivedValidator
    extends DObPurchaseOrderStateTransitionCommonValidator {

  private ValidationResult validationResult;

  public ValidationResult validate(DObPurchaseOrderGeneralModel purchaseOrderGeneralModel, DateTime valueObjectArrivalDate)
      throws Exception {
    validationResult = new ValidationResult();

    if (purchaseOrderGeneralModel == null) {
      throw new ArgumentViolationSecurityException();
    }

    if (purchaseOrderGeneralModel.getCurrentStates().contains("Draft")) {
      throw new ArgumentViolationSecurityException();
    }
    DObPurchaseOrderCycleDatesGeneralModel purchaseOrderCycleDatesGeneralModel =
        getpurchaseOrderCycleDatesGeneralModelRep().findOneByUserCode(purchaseOrderGeneralModel.getUserCode());

    if (purchaseOrderCycleDatesGeneralModel != null
        && purchaseOrderCycleDatesGeneralModel.getShippingDate() != null
        && !isValidTransitionDateTime(
            purchaseOrderCycleDatesGeneralModel.getShippingDate(), valueObjectArrivalDate)) {

      validationResult.bindErrorWithValues(
          IDObPurchaseOrderArrivalValueObject.ARRIVAL_DATE_TIME,
          IExceptionsCodes.INVALID_STATE_DATE,
          Arrays.asList(
              getDateWithUserDefaultTimeZone(
                  purchaseOrderCycleDatesGeneralModel.getShippingDate())));
    }
    return validationResult;
  }
}
