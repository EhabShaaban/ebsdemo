package com.ebs.dda.validation.collection;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.operational.accessability.ObjectStateNotValidException;
import com.ebs.dda.accounting.collection.exceptions.CollectionAmountGreaterThanSalesOrderRemainingException;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IDObCollectionGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

import java.util.Set;

public class DObCollectionSalesOrderActivateValidator extends DObCollectionActivateValidator {

    private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;

    @Override
    protected void validateRefDocument(DObCollectionGeneralModel collection) {
        DObSalesOrderGeneralModel salesOrder =
                salesOrderGeneralModelRep.findOneByUserCode(collection.getRefDocumentCode());
        checkSalesOrderState(salesOrder);

        assertThatCollectionAmountNotGreaterThanRemaining(collection, salesOrder);
    }

    private void checkSalesOrderState(DObSalesOrderGeneralModel salesOrderGeneralModel)
            throws ObjectStateNotValidException {
        Set<String> salesOrderState = salesOrderGeneralModel.getCurrentStates();
        if (!(salesOrderState.contains(DObSalesOrderStateMachine.APPROVED)
                || salesOrderState.contains(DObSalesOrderStateMachine.GOODS_ISSUE_ACTIVATED))) {
            throw new ObjectStateNotValidException();
        }
    }

    private void assertThatCollectionAmountNotGreaterThanRemaining(
            DObCollectionGeneralModel collection, DObSalesOrderGeneralModel salesOrder) {

        if (isAmountGreaterThanRemaining(collection, salesOrder)) {

            validationResult.bindError(IDObCollectionGeneralModel.AMOUNT,
                    IExceptionsCodes.C_AMOUNT_GREATER_THAN_SALES_ORDER_REMAINING);

            throw new CollectionAmountGreaterThanSalesOrderRemainingException(validationResult);
        }
    }

    private boolean isAmountGreaterThanRemaining(DObCollectionGeneralModel collection, DObSalesOrderGeneralModel salesOrder) {
        return collection.getAmount().compareTo(salesOrder.getRemaining()) > 0;
    }

    public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
        this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
    }
}
