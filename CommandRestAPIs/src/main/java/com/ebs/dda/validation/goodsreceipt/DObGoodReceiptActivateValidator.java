package com.ebs.dda.validation.goodsreceipt;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.operational.accessability.ObjectStateNotValidException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.exceptions.PurchaseOrderHasInvalidLandedCostException;
import com.ebs.dda.inventory.goodsreceipt.validation.DObGoodsReceiptGeneralDataSectionValidator;
import com.ebs.dda.inventory.goodsreceipt.validation.DObGoodsReceiptItemsSectionValidator;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IIObGoodsReceiptPurchaseOrderDataGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderStateMachine;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DObGoodReceiptActivateValidator
    implements IMissingFieldsValidator,
        IActivateBusinessRulesValidator<DObInventoryDocumentActivateValueObject> {
  private static final String GR_SR = "GR_SR";
  private static final String DRAFT_STATE = "%Draft%";

  private MandatoryValidatorUtils validatorUtils;

  private ValidationResult validationResult;

  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private DObGoodsReceiptGeneralDataSectionValidator generalDataSectionValidator;
  private DObGoodsReceiptItemsSectionValidator itemsSectionValidator;
  private DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep;
  private DObLandedCostGeneralModelRep landedCostGeneralModelRep;

  public DObGoodReceiptActivateValidator() {
    this.validatorUtils = new MandatoryValidatorUtils();
  }

  @Override
  public ValidationResult validateBusinessRules(DObInventoryDocumentActivateValueObject valueObject)
      throws RuntimeException {
    validationResult = new ValidationResult();

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(valueObject.getUserCode());

    String refDocumentCode = goodsReceiptGeneralModel.getRefDocumentCode();

    if (goodsReceiptGeneralModel.getInventoryDocumentType().equals(GR_SR)) {
      validateSalesReturnOrderState(refDocumentCode);
    } else {
      validatePurchaseOrderHasValidLandedCostDocument(refDocumentCode);
    }
    return validationResult;
  }

  private void validatePurchaseOrderHasValidLandedCostDocument(String purchaseOrderCode) {
    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findByPurchaseOrderCodeAndCurrentStatesNotLike(
            purchaseOrderCode, DRAFT_STATE);

    if (landedCostGeneralModel == null) {
      validationResult.bindError(
          IIObGoodsReceiptPurchaseOrderDataGeneralModel.PURCHASE_ORDER_CODE,
          IExceptionsCodes.GOODS_RECEIPT_REF_DOCUMENT_HAS_INVALID_LANDED_COST);
      throw new PurchaseOrderHasInvalidLandedCostException(validationResult);
    }
  }

  private void validateSalesReturnOrderState(String refDocumentCode) {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(refDocumentCode);
    if (!isValidSalesReturnState(salesReturnOrderGeneralModel)) {
      throw new ObjectStateNotValidException();
    }
  }

  private boolean isValidSalesReturnState(
      DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel) {
    return salesReturnOrderGeneralModel
            .getCurrentState()
            .equals(DObSalesReturnOrderStateMachine.SHIPPED_STATE)
        || salesReturnOrderGeneralModel
            .getCurrentState()
            .equals(DObSalesReturnOrderStateMachine.CREDIT_NOTE_ACTIVATED_STATE);
  }

  @Override
  public Map<String, Object> validateMissingFields(String userCode, String state) {
    Map<String, Object> missingFieldsMap = new HashMap<>();

    validatorUtils.populateSectionMissingFieldstoResult(
        missingFieldsMap,
        IGoodsReceiptSectionNames.HEADER_SECTION,
        getGeneralDataMissingFields(userCode, state));

    validatorUtils.populateSectionMissingFieldstoResult(
        missingFieldsMap,
        IGoodsReceiptSectionNames.ITEMS_SECTION,
        getItemsMissingFields(userCode, state));

    return missingFieldsMap;
  }

  private List<String> getGeneralDataMissingFields(String userCode, String state) {
    return generalDataSectionValidator.validateGeneralModelMandatories(userCode, state);
  }

  private Object getItemsMissingFields(String userCode, String state) {
    return itemsSectionValidator.validateGeneralModelMandatories(userCode, state);
  }

  public void setSalesReturnOrderGeneralModelRep(
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep) {
    this.salesReturnOrderGeneralModelRep = salesReturnOrderGeneralModelRep;
  }

  public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }

  public void setGeneralDataSectionValidator(
      DObGoodsReceiptGeneralDataSectionValidator generalDataSectionValidator) {
    this.generalDataSectionValidator = generalDataSectionValidator;
  }

  public void setItemsSectionValidator(DObGoodsReceiptItemsSectionValidator itemsSectionValidator) {
    this.itemsSectionValidator = itemsSectionValidator;
  }

  public void setGoodsReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }
}
