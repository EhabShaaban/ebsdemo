package com.ebs.dda.validation.actualcost;

import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;

public interface IActualCostCalculateValidator {

  String IMPORT_PO = "0001";
   String POSTED_STATE = "%"+ DObVendorInvoiceStateMachine.POSTED_STATE+"%";

}
