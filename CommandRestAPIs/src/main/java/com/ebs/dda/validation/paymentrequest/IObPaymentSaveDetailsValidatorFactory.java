package com.ebs.dda.validation.paymentrequest;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;

@Component
public class IObPaymentSaveDetailsValidatorFactory implements ApplicationContextAware {
	private ApplicationContext applicationContext;

	public IObPaymentRequestPaymentDetailsSaveValidator getValidatorInstance(
					DObPaymentRequestGeneralModel paymentRequest) {
		String paymentType = paymentRequest.getPaymentType();
		if (paymentType != null && !paymentType.isEmpty()) {
			if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())) {
				return getPaymentToVendorValidatorBean(paymentRequest);
			}
			if (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.name())) {
				return applicationContext.getBean(IObGeneralPaymentDetailsSaveValidator.class);
			}
			if (paymentType.equals(PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE.name())) {
				return applicationContext
								.getBean(IObPaymentAgainstCreditNoteDetailsSaveValidator.class);
			}
			if (paymentType.equals(PaymentTypeEnum.PaymentType.UNDER_SETTLEMENT.name())) {
				return applicationContext
								.getBean(IObUnderSettlementPaymentDetailsSaveValidator.class);
			}
		}
		throw new ArgumentViolationSecurityException();
	}

	private IObPaymentRequestPaymentDetailsSaveValidator getPaymentToVendorValidatorBean(
					DObPaymentRequestGeneralModel paymentRequest) {
		String dueDocumentType = paymentRequest.getDueDocumentType();
		if (dueDocumentType != null && !dueDocumentType.isEmpty()) {
			if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE.name())) {
				return applicationContext
								.getBean(IObPaymentAgainstVendorInvoiceDetailsSaveValidator.class);
			}
			if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name())) {
				return applicationContext.getBean(IObDownPaymentDetailsSaveValidator.class);
			}
		}
		throw new ArgumentViolationSecurityException();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
}
