package com.ebs.dda.validation.purchaseorder;

import com.ebs.dda.jpa.order.IObPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.editability.IIObServicePurchaseOrderItemsMandatoryAttributes;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

public class IObPurchaseOrderItemSaveAddMandatoryValidator {
  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;

  public void validate(IObPurchaseOrderItemValueObject valueObject) throws Exception {
    mandatoryValidatorUtils = new MandatoryValidatorUtils();

    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(valueObject.getPurchaseOrderCode());

    mandatoryValidatorUtils.validateMandatoriesForValueObjectAsObjectState(
        IIObServicePurchaseOrderItemsMandatoryAttributes.MANDATORIES,
        purchaseOrderGeneralModel.getCurrentState(),
        valueObject);
  }

  public void setPurchaseOrderGeneralModelRep(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    this.purchaseOrderGeneralModelRep = purchaseOrderGeneralModelRep;
  }
}
