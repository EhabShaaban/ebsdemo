package com.ebs.dda.validation.notesreceivable;

import com.ebs.dac.foundation.exceptions.operational.accessability.ObjectStateNotValidException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IObNotesReceivableRefDocumentSOValidator {
  Map<String, List<String>> validationErrors;

  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;

  public void validateRefDocument(
      String refDocumentCode,
      String refDocumentType,
      BigDecimal amountToCollect,
      DObNotesReceivablesGeneralModel dObNotesReceivablesGeneralModel,
      Map<String, List<String>> validationErrors) {
    this.validationErrors = validationErrors;

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(refDocumentCode);

    checkThatAmountToCollectForSORefDocumentLessThanOrEqualItsRemaining(
        salesOrderGeneralModel, refDocumentCode, refDocumentType, amountToCollect);

    checkSalesOrderState(salesOrderGeneralModel.getCurrentStates(), refDocumentCode);

    checkBusinessPartner(
        salesOrderGeneralModel.getCustomerCode(),
        dObNotesReceivablesGeneralModel.getBusinessPartnerCode());

    checkBusinessUnit(
        salesOrderGeneralModel.getPurchaseUnitCode(),
        dObNotesReceivablesGeneralModel.getPurchaseUnitCode());

    checkCompany(
        salesOrderGeneralModel.getCompanyCode(), dObNotesReceivablesGeneralModel.getCompanyCode());
  }

  private void checkThatAmountToCollectForSORefDocumentLessThanOrEqualItsRemaining(
      DObSalesOrderGeneralModel salesOrderGeneralModel,
      String refDocumentCode,
      String refDocumentType,
      BigDecimal amountToCollect) {
    if (isAmountGreaterThanRemaining(salesOrderGeneralModel, amountToCollect)) {
      bindAmountToCollectError(refDocumentCode, refDocumentType);
    }
  }

  private boolean isAmountGreaterThanRemaining(
      DObSalesOrderGeneralModel salesOrderGeneralModel, BigDecimal amountToCollect) {
    return amountToCollect.compareTo(salesOrderGeneralModel.getRemaining()) > 0;
  }

  private void bindAmountToCollectError(String refDocumentCode, String refDocumentType) {
    validationErrors.computeIfAbsent(
        IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT,
        k -> new ArrayList<>());
    validationErrors
        .get(IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT)
        .add(refDocumentType + " " + refDocumentCode);
  }

  private void checkSalesOrderState(Set<String> currentState, String salesOrderCode)
      throws ObjectStateNotValidException {
    if (currentState.contains(DObSalesOrderStateMachine.DRAFT_STATE)) {
      throw new ArgumentViolationSecurityException();
    } else if (!(currentState.contains(DObSalesOrderStateMachine.APPROVED)
        || currentState.contains(DObSalesOrderStateMachine.GOODS_ISSUE_ACTIVATED))) {
      bindSOStateError(salesOrderCode);
    }
  }

  private void bindSOStateError(String salesOrderCode) {
    validationErrors.computeIfAbsent(
        IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE,
        k -> new ArrayList<>());
    validationErrors
        .get(IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE)
        .add(salesOrderCode);
  }

  private void checkBusinessPartner(String refDocumentBusinessPartner, String nrBusinessPartner)
      throws ArgumentViolationSecurityException {
    if (!refDocumentBusinessPartner.equals(nrBusinessPartner)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkBusinessUnit(String refDocumentBusinessUnit, String nrBusinessUnit)
      throws ArgumentViolationSecurityException {
    if (!refDocumentBusinessUnit.equals(nrBusinessUnit)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkCompany(String refDocumentCompany, String nrCompany)
      throws ArgumentViolationSecurityException {
    if (!refDocumentCompany.equals(nrCompany)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }
}
