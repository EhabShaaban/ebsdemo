package com.ebs.dda.validation.notesreceivable;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IObNotesReceivableRefDocumentNRValidator {

  Map<String, List<String>> validationErrors;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private IObMonetaryNotesDetailsGeneralModelRep notesDetailsGeneralModelRep;

  public void validateRefDocument(
      String refDocumentCode,
      String refDocumentType,
      BigDecimal amountToCollect,
      DObNotesReceivablesGeneralModel dObNotesReceivablesGeneralModel,
      Map<String, List<String>> validationErrors) {
    this.validationErrors = validationErrors;

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(refDocumentCode);
    IObMonetaryNotesDetailsGeneralModel monetaryNotesDetailsGeneralModel =
        notesDetailsGeneralModelRep.findOneByRefInstanceId(notesReceivablesGeneralModel.getId());

    checkThatAmountToCollectForNRRefDocumentLessThanOrEqualItsRemaining(
        monetaryNotesDetailsGeneralModel, refDocumentCode, refDocumentType, amountToCollect);

    checkNotesReceivableState(notesReceivablesGeneralModel.getCurrentStates());

    checkBusinessPartner(
        notesReceivablesGeneralModel.getBusinessPartnerCode(),
        dObNotesReceivablesGeneralModel.getBusinessPartnerCode());

    checkBusinessUnit(
        notesReceivablesGeneralModel.getPurchaseUnitCode(),
        dObNotesReceivablesGeneralModel.getPurchaseUnitCode());

    checkCompany(
        notesReceivablesGeneralModel.getCompanyCode(),
        dObNotesReceivablesGeneralModel.getCompanyCode());
  }

  private void checkThatAmountToCollectForNRRefDocumentLessThanOrEqualItsRemaining(
      IObMonetaryNotesDetailsGeneralModel monetaryNotesDetailsGeneralModel,
      String refDocumentCode,
      String refDocumentType,
      BigDecimal amountToCollect) {
    BigDecimal remaining = monetaryNotesDetailsGeneralModel.getRemaining();
    if (amountToCollect.compareTo(remaining) > 0) {
      bindAmountToCollectError(refDocumentCode, refDocumentType);
    }
  }

  private void bindAmountToCollectError(String refDocumentCode, String refDocumentType) {
    validationErrors.computeIfAbsent(
        IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT,
        k -> new ArrayList<>());
    validationErrors
        .get(IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT)
        .add(refDocumentType + " " + refDocumentCode);
  }

  private void checkNotesReceivableState(Set<String> currentStates)
      throws ArgumentViolationSecurityException {
    if (!currentStates.contains(DObNotesReceivablesStateMachine.ACTIVE_STATE)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkBusinessPartner(String refDocumentBusinessPartner, String nrBusinessPartner)
      throws ArgumentViolationSecurityException {
    if (!refDocumentBusinessPartner.equals(nrBusinessPartner)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkBusinessUnit(String refDocumentBusinessUnit, String nrBusinessUnit)
      throws ArgumentViolationSecurityException {
    if (!refDocumentBusinessUnit.equals(nrBusinessUnit)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  private void checkCompany(String refDocumentCompany, String nrCompany)
      throws ArgumentViolationSecurityException {
    if (!refDocumentCompany.equals(nrCompany)) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setNotesDetailsGeneralModelRep(
      IObMonetaryNotesDetailsGeneralModelRep notesDetailsGeneralModelRep) {
    this.notesDetailsGeneralModelRep = notesDetailsGeneralModelRep;
  }
}
