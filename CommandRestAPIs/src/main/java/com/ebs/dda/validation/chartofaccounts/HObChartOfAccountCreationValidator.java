package com.ebs.dda.validation.chartofaccounts;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNotNull;
import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountCreateValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccountCreateValueObject;
import com.ebs.dda.dbo.jpa.entities.lookups.LObGlobalGLAccountConfig;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigRep;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;

public class HObChartOfAccountCreationValidator {

	private ValidationResult validationResult;
	private HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep;
	private LObGlobalAccountConfigRep globalAccountConfigRep;

	public ValidationResult validate(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		validationResult = new ValidationResult();
		checkMandatories(creationValueObject);
		checkPassingDisabledFields(creationValueObject);
		checkInvalidInputs(creationValueObject);

		return validationResult;
	}

	private void checkPassingDisabledFields(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		checkPassingRootDisabledFields(creationValueObject);
		checkPassingIntermediateDisabledFields(creationValueObject);
	}

	private void checkPassingIntermediateDisabledFields(
					HObChartOfAccountCreateValueObject creationValueObject) throws Exception {
		if ((creationValueObject.getLevel().equals(IHObChartOfAccounts.INTERMEDIATE))) {
			checkIfObjectIsNotNull(creationValueObject.getAccountType());
			checkIfObjectIsNotNull(creationValueObject.getAccountingEntry());
			checkIfObjectIsNotNull(creationValueObject.getMappedAccount());
		}
	}

	private void checkPassingRootDisabledFields(
					HObChartOfAccountCreateValueObject creationValueObject) throws Exception {
		if ((creationValueObject.getLevel().equals(IHObChartOfAccounts.ROOT))) {
			checkIfObjectIsNotNull(creationValueObject.getParentCode());
			checkIfObjectIsNotNull(creationValueObject.getAccountType());
			checkIfObjectIsNotNull(creationValueObject.getAccountingEntry());
			checkIfObjectIsNotNull(creationValueObject.getMappedAccount());

		}
	}

	private void checkInvalidInputs(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		checkParentIsValid(creationValueObject);
		checkThatMappedAccountIsNotAssignedToAnotherAccount(creationValueObject);
	}

	private void checkThatMappedAccountIsNotAssignedToAnotherAccount(
					HObChartOfAccountCreateValueObject creationValueObject) throws Exception {
		LObGlobalGLAccountConfig globalAccountConfig = globalAccountConfigRep
						.findOneByUserCode(creationValueObject.getMappedAccount());
		checkIfObjectIsNotNull(globalAccountConfig);
	}

	private void checkParentIsValid(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		HObChartOfAccountsGeneralModel parentAccount = chartOfAccountsGeneralModelRep
						.findOneByUserCode(creationValueObject.getParentCode());
		checkIfParentIsOneLevelBeforeChild(creationValueObject, parentAccount);
		checkIfParentExists(creationValueObject, parentAccount);

	}

	private void checkIfParentExists(HObChartOfAccountCreateValueObject creationValueObject,
					HObChartOfAccountsGeneralModel parentAccount) {
		if (((creationValueObject.getLevel().equals(IHObChartOfAccounts.LEAF)
						|| (creationValueObject.getLevel()
										.equals(IHObChartOfAccounts.INTERMEDIATE)))
						&& parentAccount == null)) {
			validationResult.bindError(IHObChartOfAccountCreateValueObject.PARENT,
							IExceptionsCodes.General_MISSING_FIELD_INPUT);
		}

	}

	private void checkIfParentIsOneLevelBeforeChild(
					HObChartOfAccountCreateValueObject creationValueObject,
					HObChartOfAccountsGeneralModel parentAccount) throws Exception {
		if (parentAccount != null && Integer.parseInt(creationValueObject.getLevel())
						- Integer.parseInt(parentAccount.getLevel()) != 1) {
			throw new ArgumentViolationSecurityException();
		}
	}

	private void checkMandatories(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		checkCommonMandatories(creationValueObject);
		checkChildLevelMandatories(creationValueObject);
		checkChildLevelMandatories(creationValueObject);
		checkLeafLevelMandatories(creationValueObject);
	}

	private void checkCommonMandatories(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		checkIfObjectIsNull(creationValueObject.getAccountName());
		checkIfObjectIsNull(creationValueObject.getLevel());
		checkIfObjectIsNull(creationValueObject.getOldAccountCode());
	}

	private void checkChildLevelMandatories(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		if ((creationValueObject.getLevel().equals(IHObChartOfAccounts.LEAF) || (creationValueObject
						.getLevel().equals(IHObChartOfAccounts.INTERMEDIATE)))) {
			checkIfObjectIsNull(creationValueObject.getParentCode());
		}
	}

	private void checkLeafLevelMandatories(HObChartOfAccountCreateValueObject creationValueObject)
					throws Exception {
		if ((creationValueObject.getLevel().equals(IHObChartOfAccounts.LEAF))) {
			checkIfObjectIsNull(creationValueObject.getAccountType());
			checkIfObjectIsNull(creationValueObject.getAccountingEntry());
			checkIfObjectIsNull(creationValueObject.getMappedAccount());
		}
	}

	public ValidationResult getValidationResult() {
		return validationResult;
	}

	public void setValidationResult(ValidationResult validationResult) {
		this.validationResult = validationResult;
	}

	public HObChartOfAccountsGeneralModelRep getChartOfAccountsGeneralModelRep() {
		return chartOfAccountsGeneralModelRep;
	}

	public void setGlobalAccountConfigRep(LObGlobalAccountConfigRep globalAccountConfigRep) {
		this.globalAccountConfigRep = globalAccountConfigRep;
	}

	public void setChartOfAccountsGeneralModelRep(
					HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
		this.chartOfAccountsGeneralModelRep = chartOfAccountsGeneralModelRep;
	}
}
