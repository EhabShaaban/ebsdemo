package com.ebs.dda.validation.collection;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.accounting.collection.CollectionMethodEnum;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsValueObject;
import com.ebs.dda.jpa.masterdata.company.CObTreasury;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.company.IObCompanyTreasuriesGeneralModel;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyTreasuriesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import com.ebs.dda.validation.ValidationUtils;

public class IObCollectionDetailsValidator {

  private ValidationResult validationResult;
  private IObCompanyTreasuriesGeneralModelRep companyTreasuriesGeneralModelRep;
  private IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep;
  private CObTreasuryRep treasuryRep;

  public ValidationResult validate(
      DObCollectionGeneralModel collectionGeneralModel,
      IObCollectionDetailsValueObject collectionDetailsValueObject)
      throws Exception {
    validationResult = new ValidationResult();

    if (collectionGeneralModel
        .getCollectionMethod()
        .equals(CollectionMethodEnum.CollectionMethod.CASH.name())) {
      validateTreasuryIsCorrect(
          collectionDetailsValueObject, collectionGeneralModel.getCompanyCode());
    }else {
      checkIfBankIsRelatedToCompany(collectionDetailsValueObject.getBankAccountNumber(), collectionGeneralModel.getCompanyCode());
    }
    return validationResult;
  }

  private void validateTreasuryIsCorrect(
      IObCollectionDetailsValueObject collectionDetailsValueObject, String companyCode)
      throws Exception {
    String treasuryCode = collectionDetailsValueObject.getTreasuryCode();
    if (treasuryCode != null) {
      CObTreasury treasury = treasuryRep.findOneByUserCode(treasuryCode);
      ValidationUtils.checkIfObjectIsNull(treasury);
      checkIfTreasuryIsRelatedToCompany(treasuryCode, companyCode);
    }
  }

  private void checkIfTreasuryIsRelatedToCompany(String treasuryCode, String companyCode)
      throws Exception {
    IObCompanyTreasuriesGeneralModel companyTreasury =
        companyTreasuriesGeneralModelRep.findOneByCompanyCodeAndTreasuryCode(
            companyCode, treasuryCode);
    ValidationUtils.checkIfObjectIsNull(companyTreasury);
  }

  private void checkIfBankIsRelatedToCompany(String bankAccountNumber, String companyCode)
      throws Exception {
    if (bankAccountNumber != null) {
      IObCompanyBankDetailsGeneralModel companyBank =
          companyBankDetailsGeneralModelRep.findOneByCompanyCodeAndAccountNumber(
              companyCode, bankAccountNumber);
      ValidationUtils.checkIfObjectIsNull(companyBank);
    }
  }

  public void setCompanyTreasuriesGeneralModelRep(IObCompanyTreasuriesGeneralModelRep companyTreasuriesGeneralModelRep) {
    this.companyTreasuriesGeneralModelRep = companyTreasuriesGeneralModelRep;
  }

  public void setCompanyBankDetailsGeneralModelRep(IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep) {
    this.companyBankDetailsGeneralModelRep = companyBankDetailsGeneralModelRep;
  }

  public void setTreasuryRep(CObTreasuryRep treasuryRep) {
    this.treasuryRep = treasuryRep;
  }
}
