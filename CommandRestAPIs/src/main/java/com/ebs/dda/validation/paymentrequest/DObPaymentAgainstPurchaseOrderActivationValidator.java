package com.ebs.dda.validation.paymentrequest;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentAmountGreaterThanPurchaseOrderException;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;

import java.math.BigDecimal;

public class DObPaymentAgainstPurchaseOrderActivationValidator
    extends DObPaymentRequestActivationValidator {

  private DObPurchaseOrderRep purchaseOrderRep;

  @Override
  public ValidationResult validateBusinessRules(DObPaymentRequestActivateValueObject valueObject)
      throws RuntimeException {
    super.validateBusinessRules(valueObject);
    BigDecimal paymentRequestNetAmount = activatePreparationGeneralModel.getPaymentNetAmount();
    String dueDocumentCode = activatePreparationGeneralModel.getDueDocumentCode();
    checkAgainstPurchaseOrderAmount(paymentRequestNetAmount, dueDocumentCode);
    return validationResult;
  }

  private void checkAgainstPurchaseOrderAmount(
    BigDecimal paymentRequestNetAmount, String dueDocumentCode) {

    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(dueDocumentCode);

    if (paymentRequestNetAmount.compareTo(purchaseOrder.getRemaining()) > 0) {
      throw new DObPaymentAmountGreaterThanPurchaseOrderException(validationResult);
    }
  }

  public void setDObPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }
}
