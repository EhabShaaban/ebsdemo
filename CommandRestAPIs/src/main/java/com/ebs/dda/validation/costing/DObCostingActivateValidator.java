package com.ebs.dda.validation.costing;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.costing.exceptions.DObCostingHasNoActualLandedCostException;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.costing.DObCostingGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.ValidationUtils;
import org.joda.time.DateTime;

import java.util.List;
import java.util.Map;

import static com.ebs.dda.validation.ValidationUtils.checkIfObjectIsNull;

public class DObCostingActivateValidator
    implements IMissingFieldsValidator,
        IActivateBusinessRulesValidator<DObCostingActivateValueObject> {

  private ValidationResult validationResult;
  private DObCostingGeneralModelRep costingGeneralModelRep;
  private CObFiscalYearRep fiscalYearRep;
  private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
  private IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep;
  private final ValidationUtils validationUtils;

  public DObCostingActivateValidator() {
    validationUtils = new ValidationUtils();
  }

  @Override
  public ValidationResult validateBusinessRules(
      DObCostingActivateValueObject costingActivateValueObject) throws RuntimeException {
    String costingCode = costingActivateValueObject.getUserCode();

    DObCostingGeneralModel costingGeneralModel =
        costingGeneralModelRep.findOneByUserCode(costingCode);

    validationResult = new ValidationResult();

    checkIfObjectIsNull(costingGeneralModel);
    checkMandatoriesExist(costingActivateValueObject);

    checkDueDateInValidFiscalYear(costingActivateValueObject);
    checkThatCostingHasActualLandedCost(costingCode);

    return validationResult;
  }

  private void checkDueDateInValidFiscalYear(
      DObCostingActivateValueObject costingActivateValueObject) {
    List<CObFiscalYear> activeFiscalYears = fiscalYearRep.findByCurrentStatesLike("%Active%");

    DateTime dueDate = validationUtils.getDateTime(costingActivateValueObject.getDueDate());

    boolean isValidDueDate = false;
    for (CObFiscalYear fiscalYear : activeFiscalYears) {
      if (dueDate.isAfter(fiscalYear.getFromDate()) && dueDate.isBefore(fiscalYear.getToDate())) {
        isValidDueDate = true;
        continue;
      }
    }
    if (!isValidDueDate) {
      throw new ArgumentViolationSecurityException();
    }
  }

  @Override
  public Map<String, Object> validateMissingFields(String userCode, String state) {
    return null;
  }

  private void checkMandatoriesExist(DObCostingActivateValueObject costingActivateValueObject)
      throws ArgumentViolationSecurityException {
    checkIfObjectIsNull(costingActivateValueObject.getDueDate());
  }

  private void checkThatCostingHasActualLandedCost(String costingCode) {
    IObCostingDetailsGeneralModel costingDetailsGeneralModel =
        getIObCostingDetailsGeneralModel(costingCode);

    DObLandedCostGeneralModel landedCostGeneralModel =
        getActualLandedCostGeneralModel(costingDetailsGeneralModel);

    if (landedCostGeneralModel == null) {
      throw new DObCostingHasNoActualLandedCostException(validationResult);
    }
  }

  private DObLandedCostGeneralModel getActualLandedCostGeneralModel(
      IObCostingDetailsGeneralModel costingDetailsGeneralModel) {
    return landedCostGeneralModelRep.findByPurchaseOrderCodeAndCurrentStatesLike(
        costingDetailsGeneralModel.getPurchaseOrderCode(),
        '%' + DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED + '%');
  }

  private IObCostingDetailsGeneralModel getIObCostingDetailsGeneralModel(String costingCode) {
    return costingDetailsGeneralModelRep.findOneByUserCode(costingCode);
  }

  public void setCostingGeneralModelRep(DObCostingGeneralModelRep costingGeneralModelRep) {
    this.costingGeneralModelRep = costingGeneralModelRep;
  }

  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

  public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }

  public void setCostingDetailsGeneralModelRep(
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep) {
    this.costingDetailsGeneralModelRep = costingDetailsGeneralModelRep;
  }
}
