package com.ebs.dda.validation.settlement;

import com.ebs.dac.foundation.exceptions.data.MissingSystemDataException;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementSectionNames;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.accounting.settlement.validation.api.IIObSettlementDetailsMandatoryAttributes;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.IObAccountingDocumentAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementActivateValueObject;
import com.ebs.dda.jpa.accounting.settlements.IIObSettlementAccountingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementDetailsGeneralModelRep;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import com.ebs.dda.validation.IMissingFieldsValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class DObSettlementActivationValidator
    implements IMissingFieldsValidator, IActivateBusinessRulesValidator<DObSettlementActivateValueObject> {
  private ValidationResult validationResult;
  private MandatoryValidatorUtils validatorUtils;
  private DObSettlementGeneralModelRep settlementRep;
  private IObSettlementAccountDetailsGeneralModelRep settlementAccountDetailsGeneralModelRep;
  private IObSettlementDetailsGeneralModelRep settlementDetailsGeneralModelRep;
  private CObFiscalYearRep fiscalYearRep;

  public DObSettlementActivationValidator() {
    validatorUtils = new MandatoryValidatorUtils();
  }

  @Override
  public ValidationResult validateBusinessRules(DObSettlementActivateValueObject settlementActivateValueObject) throws RuntimeException {
    validationResult = new ValidationResult();
    List<IObSettlementAccountDetailsGeneralModel> settlementAccountingDetails =
        settlementAccountDetailsGeneralModelRep.findAllByDocumentCode(settlementActivateValueObject.getSettlementCode());
    checkIfCreditAndDebitAreEquals(settlementAccountingDetails);

    List<IObSettlementAccountDetailsGeneralModel> creditAccountingDetailsCaseCashOrBank =
        getCreditAccountingDetailsCaseCashOrBank(settlementAccountingDetails);

    checkIfSettlementAmountIsAvailableInBankOrTreasury(creditAccountingDetailsCaseCashOrBank);
    checkThatJournalEntryDateInActiveFiscalYear(settlementActivateValueObject.getJournalEntryDate());
    return validationResult;
  }

  private void checkThatJournalEntryDateInActiveFiscalYear(String journalEntryDate) {
    DateTime dueDate = getDateTime(journalEntryDate);

    boolean isValidDueDate = fiscalYearRep
            .findByCurrentStatesLike("%" + BasicStateMachine.ACTIVE_STATE + "%")
            .stream()
            .anyMatch(year -> isJournalDateInActiveFiscalYear(year, dueDate));

    if (!isValidDueDate) {
      throw new ArgumentViolationSecurityException();
    }
  }
  private DateTime getDateTime(String dateTime) {
    DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
    return formatter.parseDateTime(dateTime);
  }
  private boolean isJournalDateInActiveFiscalYear(CObFiscalYear year, DateTime dueDate) {
    System.out.println(year);
    System.out.println(dueDate);
    return dueDate.isAfter(year.getFromDate()) && dueDate.isBefore(year.getToDate());
  }
  private List<IObSettlementAccountDetailsGeneralModel> getCreditAccountingDetailsCaseCashOrBank(
      List<IObSettlementAccountDetailsGeneralModel> settlementAccountingDetails) {
    List<IObSettlementAccountDetailsGeneralModel> settlementAccountDetailsGeneralModel =
        settlementAccountingDetails.stream()
            .filter(detail -> detail.getAccountingEntry().equals(AccountingEntry.CREDIT.name()))
            .filter(
                detail ->
                    detail.getSubLedger().equals(SubLedgers.Treasuries.name())
                        || detail.getSubLedger().equals(SubLedgers.Banks.name()))
            .collect(Collectors.toList());
    return settlementAccountDetailsGeneralModel;
  }

  private void checkIfSettlementAmountIsAvailableInBankOrTreasury(
      List<IObSettlementAccountDetailsGeneralModel> creditAccountingDetailsCaseCashOrBank) {
    checkBalanceExists(creditAccountingDetailsCaseCashOrBank);
    checkThatSettlementAmountGreaterThanBalance(creditAccountingDetailsCaseCashOrBank);
  }

  private void checkThatSettlementAmountGreaterThanBalance(
      List<IObSettlementAccountDetailsGeneralModel> creditAccountingDetailsCaseCashOrBankList) {
    List<String> validationErrorValues = new ArrayList<>();
    for (IObSettlementAccountDetailsGeneralModel creditAccountingDetailsCaseCashOrBank :
        creditAccountingDetailsCaseCashOrBankList) {
      if (creditAccountingDetailsCaseCashOrBank
              .getAmount()
              .compareTo(creditAccountingDetailsCaseCashOrBank.getBalance())
          > 0) {
        validationErrorValues.add(
            creditAccountingDetailsCaseCashOrBank.getGlSubAccountCode()
                + " - "
                + creditAccountingDetailsCaseCashOrBank.getGlSubAccountName().getValue("en"));
      }
    }
    setValidationError(validationErrorValues);
  }

  private void setValidationError(List<String> validationErrorValues) {
    if (validationErrorValues.size() != 0) {
      validationResult.bindErrorWithValues(
          IIObSettlementAccountingDetailsGeneralModel.AMOUNT,
          IExceptionsCodes.SETTLEMENTS_AMOUNT_GREATER_THAN_BALANCE,
          validationErrorValues);
    }
  }

  private void checkBalanceExists(
      List<IObSettlementAccountDetailsGeneralModel> creditAccountingDetailsCaseCashOrBankList) {
    for (IObSettlementAccountDetailsGeneralModel creditAccountingDetailsCaseCashOrBank :
        creditAccountingDetailsCaseCashOrBankList) {

      if (creditAccountingDetailsCaseCashOrBank.getBalance() == null) {
        throw new MissingSystemDataException("Balance does not exist!");
      }
    }
  }

  private void checkIfCreditAndDebitAreEquals(
      List<IObSettlementAccountDetailsGeneralModel> settlementAccountingDetails) {
    checkAtLeastOneCreditAndOneDebit(settlementAccountingDetails);
    BigDecimal creditAmount =
        settlementAccountingDetails.stream()
            .filter(
                (settlementDetail) ->
                    settlementDetail.getAccountingEntry().equals(AccountingEntry.CREDIT.name()))
            .map(IObAccountingDocumentAccountingDetailsGeneralModel::getAmount)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    BigDecimal debitAmount =
        settlementAccountingDetails.stream()
            .filter(
                (settlementDetail) ->
                    settlementDetail.getAccountingEntry().equals(AccountingEntry.DEBIT.name()))
            .map(IObAccountingDocumentAccountingDetailsGeneralModel::getAmount)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    if (creditAmount.compareTo(debitAmount) != 0) {
      bindAccountingDetailsSectionError();
    }
  }

  private void bindAccountingDetailsSectionError() {
    validationResult.bindError(
        IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION,
        IExceptionsCodes.SETTLEMENTS_ACCOUNTING_DETAILS_NOT_EQUALS);
  }

  private void checkAtLeastOneCreditAndOneDebit(
      List<IObSettlementAccountDetailsGeneralModel> settlementAccountingDetails) {
    List<IObSettlementAccountDetailsGeneralModel> debitSettlementAccountDetailsGeneralModels =
        settlementAccountingDetails.stream()
            .filter(
                accountingDetails ->
                    accountingDetails.getAccountingEntry().equals(AccountingEntry.DEBIT.name()))
            .collect(toList());
    if (debitSettlementAccountDetailsGeneralModels.equals(settlementAccountingDetails)
        || debitSettlementAccountDetailsGeneralModels.isEmpty()) {
      bindAccountingDetailsSectionError();
    }
  }

  @Override
  public Map<String, Object> validateMissingFields(String settlementCode, String state)
      throws RuntimeException {

    Map<String, Object> sectionsMissingFields = new HashMap<>();
    List<String> settlementDetailsMissingFields = validateGeneralModelMandatories(settlementCode);
    if (!settlementDetailsMissingFields.isEmpty()) {
      sectionsMissingFields.put(
          IDObSettlementSectionNames.SETTLEMENT_DETAILS_SECTION, settlementDetailsMissingFields);
    }
    if (!settlementHasAccountingDetails(settlementCode)) {
      sectionsMissingFields.put(
          IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION,
          Arrays.asList(IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION));
    }
    return sectionsMissingFields;
  }

  public List<String> validateGeneralModelMandatories(String settlementCode)
      throws RuntimeException {
    IObSettlementDetailsGeneralModel settlementDetailsGeneralModel =
        settlementDetailsGeneralModelRep.findOneBySettlementCode(settlementCode);
    String[] mandatoryAttributes =
        validatorUtils.getMandatoryAttributesByState(
            IIObSettlementDetailsMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL,
            DObSettlementStateMachine.POSTED_STATE);
    return validatorUtils.validateMandatoriesForGeneralModel(
        mandatoryAttributes, settlementDetailsGeneralModel);
  }

  public boolean settlementHasAccountingDetails(String settlementCode) {
    List<IObSettlementAccountDetailsGeneralModel> settlementDetails =
        settlementAccountDetailsGeneralModelRep.findAllByDocumentCode(settlementCode);
    return (settlementDetails == null ? false : settlementDetails.size() > 0);
  }

  public void setSettlementRep(DObSettlementGeneralModelRep settlementRep) {
    this.settlementRep = settlementRep;
  }

  public void setSettlementAccountDetailsGeneralModelRep(
      IObSettlementAccountDetailsGeneralModelRep settlementAccountDetailsGeneralModelRep) {
    this.settlementAccountDetailsGeneralModelRep = settlementAccountDetailsGeneralModelRep;
  }

  public void setSettlementDetailsGeneralModelRep(
      IObSettlementDetailsGeneralModelRep settlementDetailsGeneralModelRep) {
    this.settlementDetailsGeneralModelRep = settlementDetailsGeneralModelRep;
  }
  public void setFiscalYearRep(CObFiscalYearRep fiscalYearRep) {
    this.fiscalYearRep = fiscalYearRep;
  }

}
