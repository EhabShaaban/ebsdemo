package com.ebs.dda.validation.purchaseorder;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderCycleDatesGeneralModelRep;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

public class DObPurchaseOrderStateTransitionCommonValidator {

  private DObPurchaseOrderRep purchaseOrderRep;
  private DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep;

  public DObPurchaseOrderRep getPurchaseOrderRep() {
    return this.purchaseOrderRep;
  }

  public void setPurchaseOrderRep(DObPurchaseOrderRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  public void setPurchaseOrderCycleDatesGeneralModelRep(
      DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep) {
    this.purchaseOrderCycleDatesGeneralModelRep = purchaseOrderCycleDatesGeneralModelRep;
  }

  public DObPurchaseOrderCycleDatesGeneralModelRep getpurchaseOrderCycleDatesGeneralModelRep() {
    return this.purchaseOrderCycleDatesGeneralModelRep;
  }

  public void checkIfPurchaseOrderIsDraft(DObPurchaseOrder purchaseOrder) throws Exception {
    if (purchaseOrder.getCurrentStates().contains(DObImportPurchaseOrderStateMachine.DRAFT_STATE)) {
      throw new ArgumentViolationSecurityException(
          "User action on Purchase order code "
              + purchaseOrder.getUserCode()
              + " with draft state.");
    }
  }

  public void checkIfActionIsAllowedInCurrentState(DObPurchaseOrder purchaseOrder, String approverAction)
      throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(purchaseOrder);
    purchaseOrderStateMachine.isAllowedAction(approverAction);
  }

  public void validateActionOnNonDraftPurchaseOrder(String purchaseOrderCode, String actionName)
      throws Exception {
    DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    checkIfPurchaseOrderIsDraft(purchaseOrder);
    checkIfActionIsAllowedInCurrentState(purchaseOrder, actionName);
  }

  public void validateActionOnNonDraftPurchaseOrder(DObPurchaseOrder purchaseOrder, String actionName)
      throws Exception {
    checkIfPurchaseOrderIsDraft(purchaseOrder);
    checkIfActionIsAllowedInCurrentState(purchaseOrder, actionName);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(DObPurchaseOrder purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory = new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    return purchaseOrderStateMachine;
  }

  protected Boolean isValidTransitionDateTime(
      DateTime pastStateDateTime, DateTime nextStateDateTime) {
    LocalDate pastStateDate = pastStateDateTime.toLocalDate();
    LocalTime pastStateTime = pastStateDateTime.toLocalTime();
    LocalDate nextStateDate = nextStateDateTime.toLocalDate();
    LocalTime nextStateTime = nextStateDateTime.toLocalTime();
    Integer dateDifference = pastStateDate.compareTo(nextStateDate);
    Integer timeDifference = pastStateTime.compareTo(nextStateTime);

    if (dateDifference > 0 || (dateDifference == 0 && timeDifference >= 0)) {
      return false;
    }
    return true;
  }

  protected String getDateWithUserDefaultTimeZone(DateTime dateTime) {

    Long millis = dateTime.withZone(DateTimeZone.forID("Africa/Cairo")).getMillis();

    int offset = DateTimeZone.forID("Africa/Cairo").getOffset(millis);
    dateTime = dateTime.plusMillis(offset);

    return dateTime.toString();
  }
}
