package com.ebs.dda.validation.masterdata.unitofmeasure;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.masterdata.measure.CObMeasureCreationValueObject;

public class CObMeasureCreateMandatoryValidator {

  public void validate(CObMeasureCreationValueObject valueObject)
      throws ArgumentViolationSecurityException {
    String unitOfMeasureName = valueObject.getUnitOfMeasureName();
    Boolean standard = valueObject.getIsStandard();

    if (unitOfMeasureName == null || standard == null || unitOfMeasureName.trim().isEmpty()) {
      throw new ArgumentViolationSecurityException();
    }
  }
}
