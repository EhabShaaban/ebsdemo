package com.ebs.dda.rest.masterdata.itemgroup;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.masterdata.itemGroup.ItemGroupCreateCommand;
import com.ebs.dda.commands.masterdata.itemGroup.ItemGroupDeleteCommand;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.itemgroup.HObItemGroupValueObject;
import com.ebs.dda.jpa.masterdata.itemgroup.IHObItemGroup;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.masterdata.Itemgroup.ItemGroupValidator;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rest.masterdata.utils.MasterDataControllerUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/command/itemGroup")
public class ItemGroupCommandController implements InitializingBean {

  private final String RESPONSE_STATUS = "status";
  private final String CODE_RESPONSE_KEY = "code";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String SAVING_SUCCESSFUL_CODE = "Gen-msg-04";
  private final String CREATION_SUCCESSFUL_CODE = "Gen-msg-11";
  private final String USER_CODE_KEY = "userCode";
  private final String DELETION_SUCCESSFUL_CODE = "Gen-msg-12";
    private final String DELETION_FAILUER_CODE = "Gen-msg-01";
  private MasterDataControllerUtils masterDataControllerUtils;
  @Autowired
  private AuthorizationManager authorizationManager;
  @Autowired
  private ItemGroupValidator itemGroupValidator;
  @Autowired
  private ItemGroupCreateCommand itemGroupCreateCommand;
  @Autowired
  private ItemGroupDeleteCommand itemGroupDeleteCommand;
  private CommonControllerUtils commonControllerUtils;

  @RequestMapping(value = "/create", method = RequestMethod.POST)
  public @ResponseBody
  String createItem(@RequestBody String value) throws Exception {

    String userCode = null;

    commonControllerUtils.applySchemaValidation(HObItemGroupValueObject.class, value);
    HObItemGroupValueObject valueObject = new Gson().fromJson(value, HObItemGroupValueObject.class);
    applyAuthorizationForCreateAction();

    ValidationResult validationResult = itemGroupValidator.validateCreation(valueObject);

    if (!validationResult.isValid()) {
      return masterDataControllerUtils.getFailureResponse(validationResult);
    } else {
      itemGroupCreateCommand.executeCommand(valueObject);
      return commonControllerUtils.getSuccessCreationResponse(userCode);
    }
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{itemGroupCode}")
  public @ResponseBody
  String deleteItemGroup(@PathVariable String itemGroupCode) throws Exception {
    itemGroupDeleteCommand.checkIfInstanceExists(itemGroupCode);

    applyAuthorizationForDeleteAction();

    itemGroupValidator.validateDeletion(itemGroupCode);

    itemGroupDeleteCommand.executeCommand(itemGroupCode);
    return getSuccessResponseWithUserCode(DELETION_SUCCESSFUL_CODE, itemGroupCode);
  }

  public String getSuccessResponseWithUserCode(String msgCode, String userCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }

  protected void applyAuthorizationForCreateAction() {
    authorizationManager.authorizeAction(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IHObItemGroup.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(MObItem.SYS_NAME, IActionsNames.CREATE);
  }

  protected void applyAuthorizationForDeleteAction() {
    authorizationManager.authorizeAction(IHObItemGroup.SYS_NAME, IActionsNames.DELETE);
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    masterDataControllerUtils = new MasterDataControllerUtils(authorizationManager);
  }

}
