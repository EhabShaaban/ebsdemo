package com.ebs.dda.rest.accounting.accountingnotes;

import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.accounting.accountingnote.AccountingNoteCreateCommand;
import com.ebs.dda.jpa.accounting.accountingnotes.AccountingNoteCreateValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/accounting/accountingnotes")
public class DObAccountingNotesCommandController implements InitializingBean {

  private DObAccountingNoteControllerUtils utils;
  @Autowired private AccountingNoteCreateCommand accountingNoteCreateCommand;
  @Autowired private AuthorizationManager authorizationManager;

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String create(@RequestBody String accountingNoteCreationData)
      throws Exception {

    AccountingNoteCreateValueObject valueObject =
        (AccountingNoteCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                accountingNoteCreationData, AccountingNoteCreateValueObject.class);

    final StringBuilder userCode = new StringBuilder();
    Observable<String> codeObservable = accountingNoteCreateCommand.executeCommand(valueObject);
    codeObservable.subscribe(code -> userCode.append(code));

    return utils.getSuccessCreationResponse(userCode.toString());
  }

  @Override
  public void afterPropertiesSet() {
    utils = new DObAccountingNoteControllerUtils(authorizationManager);
  }
}
