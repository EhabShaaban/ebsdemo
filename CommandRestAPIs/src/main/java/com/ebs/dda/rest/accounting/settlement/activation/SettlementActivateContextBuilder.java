package com.ebs.dda.rest.accounting.settlement.activation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementActionNames;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementActivateValueObject;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlement;

public class SettlementActivateContextBuilder {

  public static SettlementActivateContext newContext() {
    return new SettlementActivateContext();
  }

  public static class SettlementActivateContext
      implements ActivateContext,
          AuthorizeContext,
          FindEntityContext,
          LockContext,
          DeserializeValueObjectContext<DObSettlementActivateValueObject>,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<DObSettlementActivateValueObject>,
          ExecuteCommandContext<DObSettlementActivateValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext {

    private String objectCode;
    private String activateData;
    private String response;
    private EntitiesOfInterestContainer entitiesOfInstrestContainer;
    private String journalEntryUserCode;
    private DObSettlementActivateValueObject valueObject;

    public SettlementActivateContext() {
      this.entitiesOfInstrestContainer =
          new EntitiesOfInterestContainer(DObSettlementGeneralModel.class);
    }

    @Override
    public String state() {
      return DObSettlementStateMachine.POSTED_STATE;
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return journalEntryUserCode;
    }

    public SettlementActivateContext objectCode(String objectCode) {
      this.objectCode = objectCode;
      return this;
    }

    @Override
    public String activateData() {
      return activateData;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return IDObSettlement.SYS_NAME;
    }

    @Override
    public String actionName() {
      return IDObSettlementActionNames.POST;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInstrestContainer;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      this.journalEntryUserCode = userCode;
    }
    public SettlementActivateContextBuilder.SettlementActivateContext valueObject(String valueObject) {
      this.activateData = valueObject;
      return this;
    }
    @Override
    public Class<DObSettlementActivateValueObject> valueObjectClass() {
      return DObSettlementActivateValueObject.class;
    }

    @Override
    public void valueObject(DObSettlementActivateValueObject valueObject) {
      this.valueObject = valueObject;
    }

    @Override
    public DObSettlementActivateValueObject valueObject() {
      return this.valueObject;
    }
  }
}
