package com.ebs.dda.rest.accounting.collection;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionCreateValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.ICObCollectionType;
import com.ebs.dda.jpa.accounting.collection.IDObCollection;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;

public class DObCollectionCommandControllerUtils extends CommonControllerUtils {

  static final String SALES_INVOICE_TYPE = "SALES_INVOICE";
  static final String NOTES_RECEIVABLE_TYPE = "NOTES_RECEIVABLE";
  static final String SALES_ORDER_TYPE = "SALES_ORDER";

  private DObCollectionGeneralModelRep collectionGeneralModelRep;

  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;

  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  private DObNotesReceivablesGeneralModelRep notesReceivableGeneralModelRep;

  public DObCollectionCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public void authorizeActionOnObject(BusinessObject businessObject, String actionName)
      throws Exception {
    try {
      this.authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void checkIfDeleteIsAllowedInCurrentState(DObCollectionGeneralModel collectionGeneralModel)
      throws Exception {
    DObCollectionStateMachine collectionStateMachine = new DObCollectionStateMachine();
    collectionStateMachine.initObjectState(collectionGeneralModel);
    try {
      collectionStateMachine.isAllowedAction(IActionsNames.DELETE);
    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException(e);
    }
  }

  public DObCollectionGeneralModel getCollectionGeneralModel(String collectionCode)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel =
        collectionGeneralModelRep.findOneByUserCode(collectionCode);
    checkIfEntityIsNull(collectionGeneralModel);
    return collectionGeneralModel;
  }

  public void setCollectionGeneralModelRep(DObCollectionGeneralModelRep collectionGeneralModelRep) {
    this.collectionGeneralModelRep = collectionGeneralModelRep;
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }

  public void setNotesReceivableGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivableGeneralModelRep) {
    this.notesReceivableGeneralModelRep = notesReceivableGeneralModelRep;
  }

  public void applyAuthorizationForCreation(
      DObCollectionCreateValueObject collectionCreateValueObject) throws Exception {

    authorizationManager.authorizeAction(IDObCollection.SYS_NAME, IActionsNames.CREATE);
    authorizationManager.authorizeAction(ICObCollectionType.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        DocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL);
    checkRefDocumentAuthorization(collectionCreateValueObject);
  }

  private void checkRefDocumentAuthorization(
      DObCollectionCreateValueObject collectionCreateValueObject) throws Exception {
    if (collectionCreateValueObject.getCollectionType().equals(SALES_INVOICE_TYPE)) {
      checkSalesInvoiceAuthCondition(collectionCreateValueObject.getRefDocumentCode());
    } else if (collectionCreateValueObject.getCollectionType().equals(NOTES_RECEIVABLE_TYPE)) {
      checkNotesReceivableAuthCondition(collectionCreateValueObject.getRefDocumentCode());
    } else if (collectionCreateValueObject.getCollectionType().equals(SALES_ORDER_TYPE)) {
      checkSalesOrderAuthCondition(collectionCreateValueObject.getRefDocumentCode());
    }
  }

  private void checkSalesOrderAuthCondition(String code) throws Exception {
    DObSalesOrderGeneralModel salesOrder = salesOrderGeneralModelRep.findOneByUserCode(code);
    if (salesOrder != null) {
      try {
        authorizationManager.authorizeAction(salesOrder, IActionsNames.READ_ALL);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  private void checkSalesInvoiceAuthCondition(String code) throws Exception {
    DObSalesInvoiceGeneralModel salesInvoice = salesInvoiceGeneralModelRep.findOneByUserCode(code);
    if (salesInvoice != null) {
      try {
        authorizationManager.authorizeAction(salesInvoice, IActionsNames.READ_ALL);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  private void checkNotesReceivableAuthCondition(String code) throws Exception {
    DObNotesReceivablesGeneralModel notesReceivable =
        notesReceivableGeneralModelRep.findOneByUserCode(code);
    if (notesReceivable != null) {
      try {
        authorizationManager.authorizeAction(notesReceivable, IActionsNames.READ_ALL);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }
}
