package com.ebs.dda.rest.inventory.goodsissue.activation;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.executequery.inventory.goodsreceipt.FindStockAvailabilitiesContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IDObGoodsIssue;
import com.ebs.dda.rest.inventory.goodsissue.IGoodsIssueResponseMessages;

import java.util.List;

public class GoodsIssueActivateContextBuilder {
  private GoodsIssueActivateContextBuilder() {}

  public static GoodsIssueActivateContext newContext() {
    return new GoodsIssueActivateContext();
  }

  public static class GoodsIssueActivateContext
      implements ActivateContext,
          AuthorizeContext,
          FindEntityContext,
          ExecuteCommandContext<DObInventoryDocumentActivateValueObject>,
          SerializeActivateResponseContext,
          FindStockAvailabilitiesContext,
          LockContext,
          ValidateIfActionIsAllowedContext,
          ValidateBusinessRulesContext<DObInventoryDocumentActivateValueObject> {
    private String objectCode;
    private String valueObjectString;
    private String response;
    private DObInventoryDocumentActivateValueObject valueObject;
    private List<String> stockAvailabilityCodes;
    private EntitiesOfInterestContainer entitiesOfInterestContainer;

    public GoodsIssueActivateContext() {
      this.entitiesOfInterestContainer =
          new EntitiesOfInterestContainer(DObGoodsIssueGeneralModel.class);
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IGoodsIssueResponseMessages.SUCCESSFUL_POST_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return null;
    }

    @Override
    public String activateData() {
      return valueObjectString;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    public GoodsIssueActivateContext objectCode(String goodsIssueCode) {
      this.objectCode = goodsIssueCode;
      return this;
    }

    public GoodsIssueActivateContext valueObject(String valueObject) {
      this.valueObjectString = valueObject;
      return this;
    }

    @Override
    public DObInventoryDocumentActivateValueObject valueObject() {
      if (this.valueObject == null) {
        createActivateValueObject();
      }
      return this.valueObject;
    }

    private void createActivateValueObject() {
      DObGoodsIssueGeneralModel goodsIssueGeneralModel =
          this.entitiesOfInterestContainer().getMainEntity();
      this.valueObject = new DObInventoryDocumentActivateValueObject();
      valueObject.setUserCode(goodsIssueGeneralModel.getUserCode());
      valueObject.setInventoryType(goodsIssueGeneralModel.getInventoryDocumentType());
    }

    @Override
    public void stockAvailabilityCodes(List<String> stockAvailabilityCodes) {
      this.stockAvailabilityCodes = stockAvailabilityCodes;
    }

    public List<String> stockAvailabilityCodes() {
      return this.stockAvailabilityCodes;
    }

    @Override
    public String objectSysName() {
      return IDObGoodsIssue.SYS_NAME;
    }

    @Override
    public String actionName() {
      return IActionsNames.ACTIVATE;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInterestContainer;
    }
  }
}
