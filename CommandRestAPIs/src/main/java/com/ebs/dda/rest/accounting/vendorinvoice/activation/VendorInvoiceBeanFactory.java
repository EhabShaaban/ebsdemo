package com.ebs.dda.rest.accounting.vendorinvoice.activation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.vendorinvoice.VendorInvoiceTypeEnum;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class VendorInvoiceBeanFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ActivateVendorInvoice getBean(String vendorInvoiceType) {
        if (vendorInvoiceType.equals(VendorInvoiceTypeEnum.Values.LOCAL_GOODS_INVOICE)) {
            return applicationContext.getBean("LocalGoodsInvoiceActivate", ActivateVendorInvoice.class);
        }
        if (vendorInvoiceType.equals(VendorInvoiceTypeEnum.Values.IMPORT_GOODS_INVOICE)) {
            return applicationContext.getBean("ImportGoodsInvoiceActivate", ActivateVendorInvoice.class);
        }
        if (VendorInvoiceTypeEnum.VendorInvoiceType.isPurchaseServiceLocalInvoice(vendorInvoiceType)) {
            return applicationContext.getBean("PurchaseServiceLocalInvoiceActivate", ActivateVendorInvoice.class);
        }

        throw new ArgumentViolationSecurityException();
    }
}
