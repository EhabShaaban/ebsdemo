package com.ebs.dda.rest.accounting.vendorinvoice.items;

import com.ebs.dac.dbo.events.vendorinvoice.VendorInvoiceSaveItemSuccessEvent;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.DObInvoiceAuthorizationManager;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObInvoiceItemsMandatoryAttributes;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.editiablity.DObVendorInvoiceAddItemEditabilityManager;
import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceItemSaveCommand;
import com.ebs.dda.commands.accounting.vendorinvoice.IObVendorInvoiceItemsDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.DObInvoiceItemValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsDeletionValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import com.ebs.dda.validation.vendorinvoice.DObVendorInvoiceItemsSaveValidator;
import com.google.gson.Gson;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

@RestController
@RequestMapping(value = "command/accounting/vendorinvoice")
public class DObVendorInvoiceItemsCommandController implements InitializingBean {

  private CommonControllerUtils commonControllerUtils;
  private DObVendorInvoiceItemsCommandControllerUtils dObInvoiceItemsCommandControllerUtils;

  @Autowired private DObInvoiceAuthorizationManager invoiceAuthorizationManager;
  @Autowired private EntityLockCommand<DObVendorInvoice> entityLockCommand;

  @Autowired private DObVendorInvoiceRep dObInvoiceRep;
  @Autowired private DObVendorInvoiceGeneralModelRep invoiceGeneralModelRep;
  @Autowired private IObVendorInvoiceItemsDeleteCommand invoiceItemDeleteCommand;
  @Autowired private IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep;
  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private DObVendorInvoiceAddItemEditabilityManager editabilityManager;
  @Autowired private DObVendorInvoiceItemSaveCommand dObInvoiceItemSaveCommand;
  @Autowired private DObVendorInvoiceItemsSaveValidator invoiceItemsSaveValidator;
	@Autowired
	private IObVendorInvoiceItemsGeneralModelRep itemsGeneralModelRep;
    @Autowired private ObservablesEventBus observablesEventBus;

  @RequestMapping(method = RequestMethod.DELETE, value = "/{invoiceCode}/items/{itemId}")
  public @ResponseBody String deleteInvoiceItem(
      @PathVariable String invoiceCode, @PathVariable String itemId) throws Exception {

    Long itemIdPathVariable = Long.valueOf(itemId);

    DObVendorInvoiceGeneralModel invoice = invoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    dObInvoiceItemsCommandControllerUtils.checkIfEntityIsNull(invoice);
    invoiceAuthorizationManager.authorizeDeleteItems(invoice);

    IObVendorInvoiceItemsDeletionValueObject invoiceItemsDeletionValueObject =
        new IObVendorInvoiceItemsDeletionValueObject();
    invoiceItemsDeletionValueObject.setInvoiceCode(invoiceCode);
    invoiceItemsDeletionValueObject.setItemId(itemIdPathVariable);

    IObVendorInvoiceItemsGeneralModel invoiceItemsGeneralModel =
        invoiceItemsGeneralModelRep.findOneByInvoiceCodeAndId(invoiceCode, itemIdPathVariable);

    dObInvoiceItemsCommandControllerUtils.checkIfInvoiceItemExists(invoiceItemsGeneralModel);

    DObVendorInvoice invoiceData = dObInvoiceRep.findOneByUserCode(invoiceCode);

    dObInvoiceItemsCommandControllerUtils.checkIfActionIsAllowedInCurrentState(
        invoiceData, IActionsNames.DELETE);
      try {
          entityLockCommand.lockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
          entityLockCommand.lockSection(invoiceCode, IDObVendorInvoiceSectionNames.SUMMARY_SECTION);
          Observable<DObVendorInvoice> invoiceItemsObservable = invoiceItemDeleteCommand.executeCommand(invoiceItemsDeletionValueObject);
          invoiceItemsObservable.subscribe(this::consumeInvoiceUpdateEvent);
      } finally {
          entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
          entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.SUMMARY_SECTION);
      }

    return getSuccessResponse(CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{invoiceCode}/lock/items")
  public @ResponseBody String requestToAddItem(@PathVariable String invoiceCode) throws Exception {

    DObVendorInvoiceGeneralModel invoice = invoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    dObInvoiceItemsCommandControllerUtils.checkIfEntityIsNull(invoice);
		invoiceAuthorizationManager.authorizeUpdateItem(invoice);

    entityLockCommand.lockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);

		List<String> authorizedReads = getInvoiceRequestLockItemAuthorizedReads();

    String[] mandatories = getItemMandatoryAttributesByState(invoice.getCurrentState());

    Set<String> editabilities =
        editabilityManager.getEnabledAttributesConfig().get(invoice.getCurrentState());

    SectionEditConfig sectionEditConfig = new SectionEditConfig();
    sectionEditConfig.setAuthorizedReads(authorizedReads);
    sectionEditConfig.setMandatories(mandatories);
    sectionEditConfig.setEnabledAttributes(editabilities);

    return commonControllerUtils.serializeEditConfigurationsResponse(sectionEditConfig);
  }

  @RequestMapping(method = RequestMethod.POST, value = "{invoiceCode}/update/items")
  public @ResponseBody String saveInvoiceItem(
      @PathVariable String invoiceCode, @RequestBody String invoiceItem) throws Exception {
    commonControllerUtils.applySchemaValidation(DObInvoiceItemValueObject.class, invoiceItem);

    applyAuthorizationForSave(invoiceCode);
    DObInvoiceItemValueObject itemValueObject =
        new Gson().fromJson(invoiceItem, DObInvoiceItemValueObject.class);
    itemValueObject.setInvoiceCode(invoiceCode);
    entityLockCommand.checkIfEntityExists(invoiceCode);

      entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
      try {
          ValidationResult validationResult = invoiceItemsSaveValidator.validate(itemValueObject);
          if (!validationResult.isValid()) {
              return commonControllerUtils.getFailureResponse(validationResult);
          }
      } catch (ArgumentViolationSecurityException e) {
          entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
          throw e;
      }
      try {
          Observable<DObVendorInvoice> invoiceItemsObservable = dObInvoiceItemSaveCommand.executeCommand(itemValueObject);
          invoiceItemsObservable.subscribe(this::consumeInvoiceUpdateEvent);


      } finally {
          entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
      }
      return getSuccessResponse(CommonControllerUtils.ADDING_ITEM_SUCCESSFUL_CODE);
  }

    private void consumeInvoiceUpdateEvent(DObVendorInvoice invoice) {
        VendorInvoiceSaveItemSuccessEvent event = new VendorInvoiceSaveItemSuccessEvent(invoice);
        observablesEventBus.consumeEvent(event);
    }

  @RequestMapping(method = RequestMethod.GET, value = "{invoiceCode}/unlock/items")
  public @ResponseBody String cancelRequestToAddItem(@PathVariable String invoiceCode)
      throws Exception {

    DObVendorInvoiceGeneralModel invoice = invoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    dObInvoiceItemsCommandControllerUtils.checkIfEntityIsNull(invoice);
		invoiceAuthorizationManager.authorizeUpdateItem(invoice);

    entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);

    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

	private List<String> getInvoiceRequestLockItemAuthorizedReads() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(IMObItem.SYS_NAME, IActionsNames.READ_ALL, "ReadItems");
    permissionsConfig.addPermissionConfig(
        ICObMeasure.SYS_NAME, IActionsNames.READ_ALL, "ReadItemUoM");

    return commonControllerUtils.getAuthorizedReads(permissionsConfig);
  }

  @RequestMapping(method = RequestMethod.POST, value = "{invoiceCode}/unlock/lockedsections")
  public @ResponseBody String closeAllSectionsInInvoice(
      @PathVariable String invoiceCode, @RequestBody List<String> lockedSections) throws Exception {
    entityLockCommand.unlockAllResourceLockedSectionsByUser(invoiceCode, lockedSections);
    return dObInvoiceItemsCommandControllerUtils.getSuccessResponseWithUserCode(
        invoiceCode, CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  public String[] getItemMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IDObInvoiceItemsMandatoryAttributes.INVOICE_ITEMS_MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

	@RequestMapping(method = RequestMethod.GET, value = "{invoiceCode}/lock/items/{itemId}")
	public @ResponseBody
	String requestEditItem(
			@PathVariable String invoiceCode, @PathVariable String itemId) throws Exception {
		DObVendorInvoiceGeneralModel invoice = invoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    dObInvoiceItemsCommandControllerUtils.checkIfEntityIsNull(invoice);
		invoiceAuthorizationManager.authorizeUpdateItem(invoice);
		IObVendorInvoiceItemsGeneralModel itemGeneralModel =
				itemsGeneralModelRep.findOneByInvoiceCodeAndId(invoiceCode, Long.parseLong(itemId));
		if (itemGeneralModel == null) {
			throw new RecordOfInstanceNotExistException(
					IObVendorInvoiceItemsGeneralModel.class.getSimpleName());
		} else {
			entityLockCommand.lockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
			List<String> authorizedReads = getInvoiceRequestLockItemAuthorizedReads();
			Set<String> editabilities =
					editabilityManager
							.getEnabledAttributesConfigForRequestEdit()
							.get(invoice.getCurrentState());

			SectionEditConfig sectionEditConfig = new SectionEditConfig();
			sectionEditConfig.setAuthorizedReads(authorizedReads);
			sectionEditConfig.setEnabledAttributes(editabilities);

			return commonControllerUtils.serializeEditConfigurationsResponse(sectionEditConfig);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "{invoiceCode}/unlock/items/{itemId}")
	public @ResponseBody
	String cancelRequestToEditItem(
			@PathVariable String invoiceCode, @PathVariable String itemId) throws Exception {
		DObVendorInvoiceGeneralModel invoice = invoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    dObInvoiceItemsCommandControllerUtils.checkIfEntityIsNull(invoice);
		invoiceAuthorizationManager.authorizeUpdateItem(invoice);
		IObVendorInvoiceItemsGeneralModel itemGeneralModel =
				itemsGeneralModelRep.findOneByInvoiceCodeAndId(invoiceCode, Long.parseLong(itemId));
		if (itemGeneralModel == null) {
			throw new RecordOfInstanceNotExistException(
					IObVendorInvoiceItemsGeneralModel.class.getSimpleName());
		} else {
			entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
			return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
		}
	}

	@RequestMapping(method = RequestMethod.PUT, value = "{invoiceCode}/update/item/{itemId}")
	public @ResponseBody
	String saveEditInvoiceItem(
			@PathVariable String invoiceCode, @PathVariable String itemId,
			@RequestBody String invoiceItem) throws Exception {
		DObInvoiceItemValueObject itemValueObject =
				new Gson().fromJson(invoiceItem, DObInvoiceItemValueObject.class);
		itemValueObject.setInvoiceCode(invoiceCode);
		itemValueObject.setItemId(Long.parseLong(itemId));
		try {
            Observable<DObVendorInvoice> invoiceItemsObservable = dObInvoiceItemSaveCommand.executeCommand(itemValueObject);
            invoiceItemsObservable.subscribe(this::consumeInvoiceUpdateEvent);

        } finally {
			entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.ITEMS_SECTION);
		}
		return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
	}

  private void applyAuthorizationForSave(String invoiceCode) throws Exception {
    DObVendorInvoiceGeneralModel invoice = invoiceGeneralModelRep.findOneByUserCode(invoiceCode);
    dObInvoiceItemsCommandControllerUtils.checkIfEntityIsNull(invoice);

		invoiceAuthorizationManager.authorizeUpdateItem(invoice);
    invoiceAuthorizationManager.applyAuthorizatioinOnReads(invoice);
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils =
        new CommonControllerUtils(invoiceAuthorizationManager.getAuthorizationManager());
    dObInvoiceItemsCommandControllerUtils = new DObVendorInvoiceItemsCommandControllerUtils();
    editabilityManager = new DObVendorInvoiceAddItemEditabilityManager();
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }
}
