package com.ebs.dda.rest.accounting.salesinvoice.activation;

public interface ActivateSalesInvoice {
  String activate(SalesInvoiceActivateContextBuilder.SalesInvoiceActivateContext context) throws Exception;
}
