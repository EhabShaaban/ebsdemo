package com.ebs.dda.rest.accounting.paymentrequest.activation;

public interface ActivatePayment {
    String activate(PaymentActivateContextBuilder.PaymentActivateContext context) throws Exception;
}
