package com.ebs.dda.rest.accounting.paymentrequest.activation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestJsonSchema;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.functions.validate.ValidateSchemaContext;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;

public class PaymentActivateContextBuilder {

  private PaymentActivateContextBuilder() {}

  public static PaymentActivateContext newContext() {
    return new PaymentActivateContext();
  }

  public static class PaymentActivateContext
      implements ActivateContext,
          AuthorizeContext,
          FindEntityContext,
          ValidateSchemaContext,
          LockContext,
          DeserializeValueObjectContext<DObPaymentRequestActivateValueObject>,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<DObPaymentRequestActivateValueObject>,
          ExecuteCommandContext<DObPaymentRequestActivateValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext {

    private String objectCode;
    private String activateData;
    private DObPaymentRequestActivateValueObject valueObject;
    private String response;
    private String journalEntryUserCode;
    private EntitiesOfInterestContainer entitiesOfInstrestContainer;

    public PaymentActivateContext() {
      this.entitiesOfInstrestContainer =
          new EntitiesOfInterestContainer(DObPaymentRequestGeneralModel.class);
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public String activateData() {
      return activateData;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return IDObPaymentRequest.SYS_NAME;
    }

    @Override
    public String actionName() {
      return IActionsNames.ACTIVATE;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      this.journalEntryUserCode = userCode;
    }

    @Override
    public String state() {
      return BasicStateMachine.ACTIVE_STATE;
    }

    public PaymentActivateContext objectCode(String paymentCode) {
      this.objectCode = paymentCode;
      return this;
    }

    public PaymentActivateContext valueObject(String valueObject) {
      this.activateData = valueObject;
      return this;
    }

    @Override
    public String schema() {
      return IDObPaymentRequestJsonSchema.PAYMENT_ACTIVATE_SCHEMA;
    }

    @Override
    public Class<DObPaymentRequestActivateValueObject> valueObjectClass() {
      return DObPaymentRequestActivateValueObject.class;
    }

    @Override
    public void valueObject(DObPaymentRequestActivateValueObject valueObject) {
      this.valueObject = valueObject;
    }

    @Override
    public DObPaymentRequestActivateValueObject valueObject() {
      return this.valueObject;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return journalEntryUserCode;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInstrestContainer;
    }
  }
}
