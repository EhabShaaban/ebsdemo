package com.ebs.dda.rest.order.purchaseorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.order.IObPurchaseOrderDeleteItemValueObject;
import com.ebs.dda.masterdata.item.IMObItemActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemGeneralModelRep;
import com.ebs.dda.purchases.editability.IIObServicePurchaseOrderItemsMandatoryAttributes;
import com.ebs.dda.purchases.editability.IObServicePurchaseOrderItemsSectionEditabilityManager;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.Set;

public class IObItemsCommandControllerUtils extends DObPurchaseOrderControllerUtils {

  private IObServicePurchaseOrderItemsSectionEditabilityManager editabilityManager;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObItemsCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public AuthorizedPermissionsConfig getServicePurchaseOrderItemsAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(IMObItem.SYS_NAME, IActionsNames.READ_ALL, "ReadItems");
    permissionsConfig.addPermissionConfig(
        IMObItem.SYS_NAME, IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION, "ReadItemUoM");
    return permissionsConfig;
  }

  public Set<String> getItemsEnableAttributesByState(Set<String> states) {
    return editabilityManager.getEnabledAttributesInGivenStates(states);
  }

  public String[] getItemsMandatoryAttributesByState(String currentState) {
    return mandatoryValidatorUtils.getMandatoryAttributesByState(
        IIObServicePurchaseOrderItemsMandatoryAttributes.MANDATORIES, currentState);
  }

  public void setEditabilityManager(
      IObServicePurchaseOrderItemsSectionEditabilityManager editabilityManager) {
    this.editabilityManager = editabilityManager;
  }

  public void checkIfDeleteItemIsAllowed(DObPurchaseOrderGeneralModel purchaseOrder)
      throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(purchaseOrder);
    checkIfDeleteDetailActionIsAllowedInCurrentState(
        purchaseOrderStateMachine, purchaseOrder, IActionsNames.DELETE);
  }

  public void checkIfItemIsNull(Long itemId, IObOrderItemGeneralModelRep itemGeneralModelRep)
      throws Exception {
    IObOrderItemGeneralModel item = itemGeneralModelRep.findOneById(itemId);
    CommonControllerUtils.checkIfDetailIsNull(item, IObOrderItemGeneralModel.class.getName());
  }

  public IObPurchaseOrderDeleteItemValueObject constructDeleteItemValueObject(
      String purchaseOrderCode, Long itemId) {
    IObPurchaseOrderDeleteItemValueObject valueObject = new IObPurchaseOrderDeleteItemValueObject();
    valueObject.setPurchaseOrderCode(purchaseOrderCode);
    valueObject.setItemId(itemId);
    return valueObject;
  }

  public void applyAuthorizationForSave(DObPurchaseOrderGeneralModel purchaseOrderGeneralModel)
      throws Exception {
    authorizationManager.authorizeActionOnObject(
        purchaseOrderGeneralModel, IActionsNames.UPDATE_ITEM);

    authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        IMObItem.SYS_NAME, IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION);
  }
}
