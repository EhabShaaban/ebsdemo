package com.ebs.dda.rest.masterdata.utils;

import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.List;

public class MasterDataControllerUtils extends CommonControllerUtils {

  private static final String RESPONSE_STATUS = "status";
  private static final String CODE_RESPONSE_KEY = "code";
  private static final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private static final String USER_CODE_KEY = "userCode";

  public MasterDataControllerUtils(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public static String serializeEditConfigurationsResponse(List<String> authorizedReads) {
    JsonObject successResponse = new JsonObject();
    JsonObject editablesAndMandatories = new JsonObject();
    Gson gson = new Gson();
    editablesAndMandatories.add(
        "authorizedReads", gson.toJsonTree(authorizedReads).getAsJsonArray());

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);

    successResponse.add("configuration", editablesAndMandatories);

    return successResponse.toString();
  }
}
