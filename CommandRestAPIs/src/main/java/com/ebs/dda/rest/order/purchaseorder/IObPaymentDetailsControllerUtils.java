package com.ebs.dda.rest.order.purchaseorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.ICObPaymentTerms;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.editability.IObPurchaseOrderPaymentDetailsEditabilityManager;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IPurchaseOrderPaymentDetailsMandatoryAttributes;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.Set;

public class IObPaymentDetailsControllerUtils extends DObPurchaseOrderControllerUtils {

  private EntityLockCommand<DObOrderDocument> entityLockCommand;
  private IObPurchaseOrderPaymentDetailsEditabilityManager
      purchaseOrderPaymentDetailsEditabilityManager;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObPaymentDetailsControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObOrderDocument> entityLockCommand) {
    super(authorizationManager);
    this.entityLockCommand = entityLockCommand;
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void unlockSection(String sectionName, String salesOrderCode) throws Exception {
    entityLockCommand.unlockSection(salesOrderCode, sectionName);
  }

  public Set<String> getEnableAttributes(DObPurchaseOrderGeneralModel purchaseOrderGeneralModel) {
    return purchaseOrderPaymentDetailsEditabilityManager
        .getEnabledAttributesConfig()
        .get(purchaseOrderGeneralModel.getCurrentState());
  }

  public String[] getMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IPurchaseOrderPaymentDetailsMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public void setPurchaseOrderPaymentDetailsEditabilityManager(
      IObPurchaseOrderPaymentDetailsEditabilityManager
          purchaseOrderPaymentDetailsEditabilityManager) {
    this.purchaseOrderPaymentDetailsEditabilityManager =
        purchaseOrderPaymentDetailsEditabilityManager;
  }

  public AuthorizedPermissionsConfig getEditPaymentDetailsAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(
        ICObPaymentTerms.SYS_NAME, IActionsNames.READ_ALL, "ReadPaymentTerms");
    permissionsConfig.addPermissionConfig(
        ICObCurrency.SYS_NAME, IActionsNames.READ_ALL, "ReadCurrency");
    return permissionsConfig;
  }

  public void applyAuthorizationForSave(DObPurchaseOrderGeneralModel purchaseOrder)
      throws Exception {
    authorizationManager.authorizeActionOnObject(
        purchaseOrder, IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);
    authorizationManager.authorizeAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObPaymentTerms.SYS_NAME, IActionsNames.READ_ALL);
  }
}
