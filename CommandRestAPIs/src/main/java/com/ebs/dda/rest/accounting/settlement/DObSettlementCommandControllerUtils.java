package com.ebs.dda.rest.accounting.settlement;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;

/**
 * @auther Ahmed Ali Elfeky
 * @since Feb 17, 2021
 */
@Component
public class DObSettlementCommandControllerUtils extends CommonControllerUtils {

  private static final String READ_ACCOUNTS_PERMESSION = "ReadAccounts";

  @Autowired private DObSettlementGeneralModelRep settlementGeneralModelRep;
  @Autowired
  private EntityLockCommand<DObSettlement> entityLockCommand;


  @Autowired
  public DObSettlementCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public DObSettlementGeneralModel checkExistanceAndAuthority(String settlementCode,
      String actionName) throws Exception {

    DObSettlementGeneralModel settlementGeneralModel =
        settlementGeneralModelRep.findOneByUserCode(settlementCode);
    if (settlementGeneralModel == null) {
      throw new InstanceNotExistException();
    }

    try {
      this.authorizationManager.authorizeAction(settlementGeneralModel, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
    return settlementGeneralModel;
  }


  public void lockSectionForEdit(DObSettlementGeneralModel settlementGeneralModel,
      String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(settlementGeneralModel.getUserCode(), sectionName);
    try {
      checkIfEditIsAllowedInCurrentState(settlementGeneralModel, actionName);
    } catch (EditNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(settlementGeneralModel.getUserCode(), sectionName);
      throw e;
    }
  }

  public void unlockSection(String settlementCode, String accountingDetailsSection)
      throws Exception {
    entityLockCommand.unlockSection(settlementCode, accountingDetailsSection);
  }

  public void checkIfEditIsAllowedInCurrentState(DObSettlementGeneralModel settlementGeneralModel,
      String actionName) throws Exception {
    DObSettlementStateMachine stateMachine = new DObSettlementStateMachine();
    stateMachine.initObjectState(settlementGeneralModel);
    try {
      stateMachine.isAllowedAction(actionName);
    } catch (ActionNotAllowedPerStateException e) {
      throw new EditNotAllowedPerStateException(e);
    }
  }

  public List<String> getAddingAccountingDetailsAuthorizedReads() {

    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL,
        READ_ACCOUNTS_PERMESSION);

    return getAuthorizedReads(permissionsConfig);
  }

}
