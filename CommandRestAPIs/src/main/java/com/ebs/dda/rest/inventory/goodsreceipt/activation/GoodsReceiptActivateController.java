package com.ebs.dda.rest.inventory.goodsreceipt.activation;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/goodsreceipt")
public class GoodsReceiptActivateController {

  private final ActivateGoodsReceipt activateGoodsReceipt;

  public GoodsReceiptActivateController(ActivateGoodsReceipt activateGoodsReceipt) {
    this.activateGoodsReceipt = activateGoodsReceipt;
  }

  @PutMapping("/{goodsReceiptCode}/activate")
  public @ResponseBody String activate(@PathVariable String goodsReceiptCode) throws Exception {
    return activateGoodsReceipt.apply(
        GoodsReceiptActivateContextBuilder.newContext().objectCode(goodsReceiptCode));
  }
}
