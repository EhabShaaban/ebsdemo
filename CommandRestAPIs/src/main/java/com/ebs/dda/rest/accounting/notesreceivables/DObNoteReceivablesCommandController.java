package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivableCreationValidator;
import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivablesValidator;
import com.ebs.dda.commands.accounting.notesreceivables.DObNotesReceivablesCreateCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesCreateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ValidationException;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "command/accounting/notesreceivables")
public class DObNoteReceivablesCommandController implements InitializingBean {

  private CommonControllerUtils commonControllerUtils;
  private DObNoteReceivablesControllerUtils utils;

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObNotesReceivablesCreateCommand notesReceivablesCreateCommand;

  @Autowired private DObNotesReceivablesValidator validator;
  @Autowired private DObNotesReceivableCreationValidator CreationValidator;

  @Autowired private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  @Autowired private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;

  @Autowired private EntityLockCommand<DObMonetaryNotes> entityLockCommand;

  @Autowired private DObNotesReceivableCreationValidator creationValidator;

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    utils = new DObNoteReceivablesControllerUtils(authorizationManager, entityLockCommand);
    utils.setValidator(validator);
    utils.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    utils.setCustomerBusinessUnitGeneralModelRep(customerBusinessUnitGeneralModelRep);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  public @ResponseBody String createNotesReceivables(@RequestBody String notesReceivableData)
      throws Exception {

    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME, IActionsNames.CREATE);

    CommonControllerUtils.applySchemaValidationForString(
      IDObNotesReceivableJsonSchema.CREATE_SCHEMA,
      notesReceivableData
    );

    DObNotesReceivablesCreateValueObject notesReceivableValueObject =
        (DObNotesReceivablesCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                notesReceivableData, DObNotesReceivablesCreateValueObject.class);

    utils.applyAuthorizationForCreateValueObjectValues(notesReceivableValueObject);

    final StringBuilder userCode = new StringBuilder();

    creationValidator.validate(notesReceivableValueObject);

    notesReceivablesCreateCommand.executeCommand(notesReceivableValueObject)
      .subscribe(code -> userCode.append(code));
    return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(
      method = RequestMethod.POST,
      value = "/unlock/lockedsections/{notesReceivableCode}")
  public @ResponseBody String closeAllSectionsInNotesReceivable(
      @PathVariable String notesReceivableCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(notesReceivableCode, lockedSections);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @GetMapping
  public @ResponseBody String getCreateConfiguration() {
    authorizationManager.authorizeAction(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = utils.constructCreateConfiguration();

    List<String> authorizedReads =
        commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

    return CommonControllerUtils.serializeEditConfigurationsResponse(
        new ArrayList<>(new HashSet<>(authorizedReads)));
  }
}

class ValidationError extends ValidationException {
  private ValidationResult validationResult;

  public ValidationError(ValidationResult validationResult) {
    super();
    this.validationResult = validationResult;
  }

  public ValidationResult getValidationResult() {
    return validationResult;
  }
}
