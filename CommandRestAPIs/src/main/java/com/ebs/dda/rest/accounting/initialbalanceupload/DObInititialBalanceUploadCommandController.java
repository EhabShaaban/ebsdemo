package com.ebs.dda.rest.accounting.initialbalanceupload;

import com.ebs.dac.dbo.events.initialbalanceupload.InitialBalanceUploadActivateEvent;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObInitialBalanceUploadAuthorizationManager;
import com.ebs.dda.commands.accounting.initialbalanceupload.DObInitialBalanceUploadActivateCommand;
import com.ebs.dda.commands.accounting.initialbalanceupload.DObInitialBalanceUploadCreateCommand;
import com.ebs.dda.jpa.accounting.initialbalanceupload.DObInitialBalanceUploadCreateValueObject;
import com.ebs.dda.jpa.accounting.initialbalanceupload.IDObInitialBalanceUpload;
import com.ebs.dda.repositories.accounting.DObInitialBalanceUploadGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.google.gson.Gson;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping(value = "command/initialbalanceupload")
public class DObInititialBalanceUploadCommandController implements InitializingBean {

  @Autowired DObInitialBalanceUploadCreateCommand initialBalanceUploadCreateCommand;
  private CommonControllerUtils commonControllerUtils;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObInitialBalanceUploadAuthorizationManager initialBalanceUploadAuthorizationManager;
  @Autowired private DObInitialBalanceUploadGeneralModelRep initialBalanceUploadGeneralModelRep;
  @Autowired private DObInitialBalanceUploadActivateCommand initialBalanceUploadActivateCommand;
  @Autowired private ObservablesEventBus observablesEventBus;

  @PostMapping(value = "/")
  public String createInitialBalanceUpload(@RequestParam("valueObject") String valueObject,
      @RequestParam("file") MultipartFile file)
      throws Exception {

    DObInitialBalanceUploadCreateValueObject initialBalanceUploadCreateValueObject =
        new Gson().fromJson(valueObject, DObInitialBalanceUploadCreateValueObject.class);

    initialBalanceUploadCreateValueObject.setAttachment(file);

    initialBalanceUploadAuthorizationManager.applyAuthorizationOnCreateAction(initialBalanceUploadCreateValueObject);

    Observable<String> codeObservable =
        initialBalanceUploadCreateCommand.executeCommand(initialBalanceUploadCreateValueObject);
    final StringBuilder userCode = new StringBuilder();
    codeObservable.subscribe(userCode::append);
    return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  @GetMapping("/")
  public @ResponseBody String createInitialBalanceUploadConfiguration() {
    authorizationManager.authorizeAction(IDObInitialBalanceUpload.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig =
            initialBalanceUploadAuthorizationManager.constructCreateConfiguration();
    List<String> authorizedReads =
            commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);

    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }
  @RequestMapping(method = RequestMethod.PUT, value = "/{initialBalanceUploadCode}/activate")
  public @ResponseBody String activateInitialBalanceUpload(
          @PathVariable String initialBalanceUploadCode) throws Exception {
    initialBalanceUploadActivateCommand.executeCommand(initialBalanceUploadCode);
    consumeInitialBalanceUploadActivateEvent(
        initialBalanceUploadCode);
    return CommonControllerUtils.getSuccessResponse(
            IInitialBalanceUploadResponseMessages.SUCCESSFUL_ACTIVATE);
  }

  private void consumeInitialBalanceUploadActivateEvent(String userCode) {
    InitialBalanceUploadActivateEvent event = new InitialBalanceUploadActivateEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }


  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }
}
