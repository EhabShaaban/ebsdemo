package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dac.dbo.events.sales.SalesInvoiceUpdateItemsEvent;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserInSameSessionException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObSalesInvoiceAuthorizationManager;
import com.ebs.dda.accounting.electronicinvoice.DObSalesInvoiceEInvoiceData;
import com.ebs.dda.accounting.salesinvoice.editability.SalesInvoiceBusinessPartnerEditabilityManger;
import com.ebs.dda.accounting.salesinvoice.validation.DObSalesInvoiceCreationValidator;
import com.ebs.dda.commands.accounting.electronicinvoice.EInvoiceSubmitCommand;
import com.ebs.dda.commands.accounting.salesinvoice.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.*;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.CObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.google.gson.Gson;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.ebs.dda.rest.CommonControllerUtils.*;

@RestController
@RequestMapping(value = "command/salesinvoice")
public class DObSalesInvoiceCommandController implements InitializingBean {

  @Autowired
  private AuthorizationManager authorizationManager;
  @Autowired
  private EntityLockCommand<DObSalesInvoice> entityLockCommand;
  private DObSalesInvoiceCommandControllerUtils dObSalesInvoiceCommandControllerUtils;
  @Autowired
  private DObSalesInvoiceAuthorizationManager salesInvoiceAuthorizationManager;
  @Autowired
  private DObSalesInvoiceCreateCommand dObSalesInvoiceCreateCommand;
  @Autowired
  private DObSalesInvoiceDeleteCommand salesInvoiceDeleteCommand;
  @Autowired
  private DObSalesInvoiceMarkAsReadyForDeliveringCommand salesInvoiceReadyForDeliveringCommand;
  @Autowired
  private EInvoiceSubmitCommand<DObSalesInvoiceEInvoiceData> salesInvoiceSubmitEInvoice;

  @Autowired
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  @Autowired
  private DObSalesInvoiceRep saleInvoiceRep;

  @Autowired
  private SalesInvoiceBusinessPartnerEditabilityManger salesInvoiceBusinessPartnerEditabilityManger;

  @Autowired
  private DObSalesInvoicePdfGeneratorCommand salesInvoicePdfGeneratorCommand;

  private DObSalesInvoiceCreationValidator validator;
  @Autowired
  private CObSalesInvoiceRep salesInvoiceTypeRep;
  @Autowired
  private DObSalesOrderRep salesOrderRep;
  @Autowired
  private CObPurchasingUnitRep businessUnitRep;
  @Autowired
  private DocumentOwnerGeneralModelRep documentOwnerRep;
  @Autowired
  private ObservablesEventBus observablesEventBus;
  private CommonControllerUtils commonControllerUtils;


  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createSalesInvoice(@RequestBody String salesInvoiceData)
      throws Exception {
    CommonControllerUtils.applySchemaValidation(DObSalesInvoiceCreationValueObject.class,
        salesInvoiceData);

    DObSalesInvoiceCreationValueObject valueObject =
        new Gson().fromJson(salesInvoiceData, DObSalesInvoiceCreationValueObject.class);

    initCreationValidator();
    validator.checkMandatories(valueObject);

    salesInvoiceAuthorizationManager.applyAuthorizationForCreation(valueObject);

    ValidationResult validationResult = validator.validateBusinessRules(valueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }

    final StringBuilder userCode = new StringBuilder();
    Observable<String> codeObservable = dObSalesInvoiceCreateCommand.executeCommand(valueObject);
    codeObservable.subscribe((code) -> {
      userCode.append(code);
    });
    codeObservable.subscribe(this::consumeSalesInvoiceUpdateItemsEvent);

    return dObSalesInvoiceCommandControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  private void initCreationValidator() {
    validator = new DObSalesInvoiceCreationValidator();
    validator.setSalesInvoiceTypeRep(salesInvoiceTypeRep);
    validator.setSalesOrderRep(salesOrderRep);
    validator.setBusinessUnitRep(businessUnitRep);
    validator.setDocumentOwnerRep(documentOwnerRep);
  }

  private void consumeSalesInvoiceUpdateItemsEvent(String userCode) {
    SalesInvoiceUpdateItemsEvent event = new SalesInvoiceUpdateItemsEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }

  @GetMapping("/")
  public @ResponseBody String getCreateConfiguration() {
    authorizationManager.authorizeAction(IDObSalesInvoice.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();

    List<String> authorizedReads =
        commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

    SectionEditConfig createConfig = new SectionEditConfig();

    createConfig.setAuthorizedReads(authorizedReads);

    return commonControllerUtils.serializeEditConfigurationsResponse(createConfig);
  }

  private AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(ICObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL,
        "ReadSalesInvoiceTypes");

    configuration.addPermissionConfig(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL,
        "ReadBusinessUnits");

    configuration.addPermissionConfig(IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL,
        "ReadSalesOrders");

    configuration.addPermissionConfig(IMObCustomer.SYS_NAME, IActionsNames.READ_ALL,
        "ReadCustomers");

    configuration.addPermissionConfig(DocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL,
        "ReadDocumentOwners");

    configuration.addPermissionConfig(ICObCompany.SYS_NAME, IActionsNames.READ_ALL,
        "ReadCompanies");

    return configuration;
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{salesInvoiceCode}")
  public @ResponseBody String deleteSalesInvoice(@PathVariable String salesInvoiceCode)
      throws Exception {
    salesInvoiceDeleteCommand.checkIfInstanceExists(salesInvoiceCode);

    DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    LockDetails lockDetails = null;

    try {
      lockDetails = entityLockCommand.lockAllSections(salesInvoiceCode);

      salesInvoiceAuthorizationManager.authorizeActionOnSection(dObSalesInvoiceGeneralModel,
          IActionsNames.DELETE);
      dObSalesInvoiceCommandControllerUtils
          .checkIfDeleteIsAllowedInCurrentState(dObSalesInvoiceGeneralModel, IActionsNames.DELETE);

      salesInvoiceDeleteCommand.executeCommand(salesInvoiceCode);
      entityLockCommand.unlockAllSections(salesInvoiceCode);
    } catch (ActionNotAllowedPerStateException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(salesInvoiceCode);
      }
      DeleteNotAllowedPerStateException deleteNotAllowedPerStateException =
          new DeleteNotAllowedPerStateException();
      deleteNotAllowedPerStateException.initCause(e);
      throw deleteNotAllowedPerStateException;
    } catch (ResourceAlreadyLockedBySameUserException ex) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(salesInvoiceCode);
      }
      throw new ResourceAlreadyLockedBySameUserInSameSessionException();
    }
    return CommonControllerUtils.getSuccessResponseWithUserCode(salesInvoiceCode,
        CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{salesInvoiceCode}")
  public @ResponseBody String closeAllSectionsInSalesInvoice(@PathVariable String salesInvoiceCode,
      HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(salesInvoiceCode, lockedSections);
    return dObSalesInvoiceCommandControllerUtils.getSuccessResponseWithUserCode(salesInvoiceCode,
        CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{salesInvoiceCode}/readyfordelivering")
  public @ResponseBody String markSalesInvoiceAsReadyForDelivering(
      @PathVariable String salesInvoiceCode) throws Exception {

    DObSalesInvoiceReadyForDeliveringValueObject readyForDeliveringValueObject =
        new DObSalesInvoiceReadyForDeliveringValueObject();
    readyForDeliveringValueObject.setSalesInvoiceCode(salesInvoiceCode);
    entityLockCommand.lockAllSections(salesInvoiceCode);
    try {
      salesInvoiceReadyForDeliveringCommand.executeCommand(readyForDeliveringValueObject);
    } finally {
      entityLockCommand.unlockAllSections(salesInvoiceCode);
    }
    return CommonControllerUtils.getSuccessResponseWithUserCode(salesInvoiceCode, "SI-msg-03");
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/generatepdf")
  public @ResponseBody String generateSalesInvoicePdf(@PathVariable String salesInvoiceCode)
      throws Exception {
    entityLockCommand.lockAllSections(salesInvoiceCode);
    StringBuilder pdfStringBuilder = new StringBuilder();
    try {
      salesInvoicePdfGeneratorCommand.executeCommand(salesInvoiceCode)
          .subscribe(commandPdf -> pdfStringBuilder.append(commandPdf));
    } finally {
      entityLockCommand.unlockAllSections(salesInvoiceCode);
    }
    return getSuccessResponse(pdfStringBuilder.toString());
  }

  @PostMapping("/{salesInvoiceCode}/submit-electronic-invoice")
  public @ResponseBody String submitEInvoice(@PathVariable String salesInvoiceCode)
      throws Exception {

    // TODO: apply basic authorization

    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    commonControllerUtils.checkIfEntityIsNull(salesInvoice);

    // TODO: apply conditional authorization
    // TODO: validate action is allowed per state

    salesInvoiceSubmitEInvoice.executeCommand(salesInvoiceCode);

    return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);

    dObSalesInvoiceCommandControllerUtils = new DObSalesInvoiceCommandControllerUtils(
        salesInvoiceAuthorizationManager.getAuthorizationManager());
    dObSalesInvoiceCommandControllerUtils.setEntityLockCommand(entityLockCommand);
    dObSalesInvoiceCommandControllerUtils.setSaleInvoiceRep(saleInvoiceRep);
    dObSalesInvoiceCommandControllerUtils.setSalesInvoiceBusinessPartnerEditabilityManger(
        salesInvoiceBusinessPartnerEditabilityManger);
  }
}
