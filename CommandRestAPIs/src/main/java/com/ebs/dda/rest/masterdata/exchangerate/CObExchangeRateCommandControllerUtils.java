package com.ebs.dda.rest.masterdata.exchangerate;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.CObExchangeRateAuthorizationManager;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.rest.CommonControllerUtils;

import java.util.ArrayList;
import java.util.List;

public class CObExchangeRateCommandControllerUtils extends CommonControllerUtils {

    private CObExchangeRateAuthorizationManager authorizationManager;

    protected AuthorizedPermissionsConfig constructCreateConfiguration() {
        AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
        configuration.addPermissionConfig(
                ICObCurrency.SYS_NAME, IActionsNames.READ_ALL, "ReadCurrency");
        return configuration;
    }

    @Override
    public List<String> getAuthorizedReads(AuthorizedPermissionsConfig authorizedPermissionsConfig) {
        List<AuthorizedPermissionsConfig.AuthorizedPermissionConfig> permissionsConfig =
                authorizedPermissionsConfig.getPermissionsConfig();
        List<String> authorizedReads = new ArrayList<>();
        for (AuthorizedPermissionsConfig.AuthorizedPermissionConfig config : permissionsConfig) {
            try {
                authorizationManager.authorizeReadAction(
                        config.getEntitySysName(), config.getPermissionToCheck());
                authorizedReads.add(config.getPermissionToReturn());
            } catch (AuthorizationException | ConditionalAuthorizationException exception) {
            }
        }
        return authorizedReads;
    }

    public void setAuthorizationManager(CObExchangeRateAuthorizationManager authorizationManager) {
        this.authorizationManager = authorizationManager;
    }
}
