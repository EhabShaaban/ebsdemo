package com.ebs.dda.rest.accounting.salesinvoice;

import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import com.ebs.dac.dbo.events.sales.SalesInvoiceUpdateItemsEvent;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.DObSalesInvoiceAuthorizationManager;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.editability.SalesInvoiceItemEditabilityManger;
import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceItemSaveCommand;
import com.ebs.dda.commands.accounting.salesinvoice.IObSalesInvoiceItemsDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.DObInvoiceItemValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItem;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItemsDeletionValueObject;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.google.gson.Gson;
import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/salesinvoice")
public class IObItemsCommandController implements InitializingBean {

  @Autowired private EntityLockCommand<DObSalesInvoice> entityLockCommand;
  private IObItemsCommandControllerUtils iObItemsCommandControllerUtils;
  @Autowired private DObSalesInvoiceAuthorizationManager salesInvoiceAuthorizationManager;
  @Autowired private IObSalesInvoiceItemsDeleteCommand salesInvoiceItemDeleteCommand;
  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired private IObSalesInvoiceItemsRep iObSalesInvoiceItemsRep;
  @Autowired private DObSalesInvoiceRep saleInvoiceRep;
  @Autowired private DObSalesInvoiceItemSaveCommand salesInvoiceItemSaveCommand;
  @Autowired private ObservablesEventBus observablesEventBus;


  @Autowired private SalesInvoiceItemEditabilityManger salesInvoiceItemEditabilityManger;

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/lock/items")
  public @ResponseBody String requestAddItem(@PathVariable String salesInvoiceCode)
      throws Exception {
    iObItemsCommandControllerUtils.checkIfEntityIsNull(salesInvoiceCode);

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.UPDATE_item);
    iObItemsCommandControllerUtils.lockSalesInvoiceSectionForEdit(
        salesInvoiceGeneralModel,
        IDObSalesInvoiceSectionNames.ITEM_SECTION,
        IDObSalesInvoiceActionNames.UPDATE_item);

    Set<String> stateEnabledAttributes =
        iObItemsCommandControllerUtils.getEnableAttributesForAddItem(salesInvoiceGeneralModel);

    String[] stateMandatories =
        iObItemsCommandControllerUtils.getItemsMandatoryAttributesByState(
            salesInvoiceGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        iObItemsCommandControllerUtils.getِAddAuthorizedPermissionsConfig();
    List<String> authorizedReads =
        iObItemsCommandControllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(stateMandatories);
    return iObItemsCommandControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{salesInvoiceCode}/unlock/items")
  public @ResponseBody String unlockItemsSection(@PathVariable String salesInvoiceCode)
      throws Exception {

    DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    iObItemsCommandControllerUtils.checkIfEntityIsNull(dObSalesInvoiceGeneralModel);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        dObSalesInvoiceGeneralModel, IDObSalesInvoiceActionNames.UPDATE_item);

    entityLockCommand.unlockSection(salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);

    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{salesInvoiceCode}/items/{itemId}")
  public @ResponseBody String deleteSalesInvoiceItem(
      @PathVariable String salesInvoiceCode, @PathVariable String itemId) throws Exception {

    Long itemIdPathVariable = Long.valueOf(itemId);

    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    iObItemsCommandControllerUtils.checkIfEntityIsNull(salesInvoice);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoice, IDObSalesInvoiceActionNames.UPDATE_item);

    IObSalesInvoiceItemsDeletionValueObject iObInvoiceItemsDeletionValueObject =
        new IObSalesInvoiceItemsDeletionValueObject();
    iObInvoiceItemsDeletionValueObject.setSalesInvoiceCode(salesInvoiceCode);
    iObInvoiceItemsDeletionValueObject.setItemId(itemIdPathVariable);

    IObSalesInvoiceItem iObSalesInvoiceItem =
        iObSalesInvoiceItemsRep.findOneById(itemIdPathVariable);

    iObItemsCommandControllerUtils.checkIfSalesInvoiceItemExists(iObSalesInvoiceItem);

    iObItemsCommandControllerUtils.checkIfDeleteIsAllowedInCurrentState(
        salesInvoice, IActionsNames.DELETE);

    entityLockCommand.lockSection(salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);

    Observable<String> salesInvoiceObservable  = salesInvoiceItemDeleteCommand.executeCommand(iObInvoiceItemsDeletionValueObject);
    salesInvoiceObservable.subscribe(this::consumeSalesInvoiceUpdateItemsEvent);
    entityLockCommand.unlockSection(salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);

    return getSuccessResponse(CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.POST, value = "{salesInvoiceCode}/update/items")
  public @ResponseBody String saveSalesInvoiceItem(
      @PathVariable String salesInvoiceCode, @RequestBody String salesInvoiceItem)
      throws Exception {

    DObInvoiceItemValueObject itemValueObject =
        new Gson().fromJson(salesInvoiceItem, DObInvoiceItemValueObject.class);
    itemValueObject.setInvoiceCode(salesInvoiceCode);

    entityLockCommand.checkIfEntityExists(salesInvoiceCode);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);
    try {
      Observable<String> salesInvoiceObservable = salesInvoiceItemSaveCommand.executeCommand(itemValueObject);
      salesInvoiceObservable.subscribe(this::consumeSalesInvoiceUpdateItemsEvent);
    } finally {
      entityLockCommand.unlockSection(salesInvoiceCode, IDObSalesInvoiceSectionNames.ITEM_SECTION);
    }
    return getSuccessResponse(CommonControllerUtils.ADDING_ITEM_SUCCESSFUL_CODE);
  }
  private void consumeSalesInvoiceUpdateItemsEvent(String userCode) {
    SalesInvoiceUpdateItemsEvent event = new SalesInvoiceUpdateItemsEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }
  @Override
  public void afterPropertiesSet() {
    iObItemsCommandControllerUtils =
        new IObItemsCommandControllerUtils(
            salesInvoiceAuthorizationManager.getAuthorizationManager());
    iObItemsCommandControllerUtils.setEntityLockCommand(entityLockCommand);
    iObItemsCommandControllerUtils.setSaleInvoiceRep(saleInvoiceRep);
    iObItemsCommandControllerUtils.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    iObItemsCommandControllerUtils.setSalesInvoiceItemEditabilityManger(
        salesInvoiceItemEditabilityManger);
  }
}
