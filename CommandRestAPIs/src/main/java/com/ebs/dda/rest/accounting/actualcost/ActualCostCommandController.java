package com.ebs.dda.rest.accounting.actualcost;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.accounting.actualcost.ActualCostCalculateCommand;
import com.ebs.dda.jpa.accounting.actualcost.ActualCostCalculateValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.actualcost.ActualCostCalculateValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/accounting/actualcost")
public class ActualCostCommandController implements InitializingBean {

  private final String ACTUAL_COST_SUCCESS_MSG = "ActualCost-msg-03";
  @Autowired
  private ActualCostCalculateValidator actualCostCalculateValidator;
  @Autowired
  private AuthorizationManager authorizationManager;
  private CommonControllerUtils commonControllerUtils;
  @Autowired
  private ActualCostCalculateCommand actualCostCalculateCommand;

  @RequestMapping(method = RequestMethod.POST, value = "/{goodsReceiptsCode}/calculate")
  public @ResponseBody String calculateActualCost(@PathVariable String goodsReceiptsCode)
      throws Exception {
    ActualCostCalculateValueObject actualCostCalculateValueObject =
        new ActualCostCalculateValueObject();
    actualCostCalculateValueObject.setGoodsReceiptCode(goodsReceiptsCode);
    ValidationResult validationResult =
        actualCostCalculateValidator.validate(actualCostCalculateValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      StringBuilder userCode = new StringBuilder();
      actualCostCalculateCommand
          .executeCommand(goodsReceiptsCode)
          .subscribe(code -> userCode.append(code));
      return commonControllerUtils.getSuccessResponse(ACTUAL_COST_SUCCESS_MSG);
    }
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }
}
