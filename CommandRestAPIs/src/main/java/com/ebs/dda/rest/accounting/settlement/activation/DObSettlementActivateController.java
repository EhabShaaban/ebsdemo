package com.ebs.dda.rest.accounting.settlement.activation;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/command/settlement")
public class DObSettlementActivateController {

  private final ActivateSettlement activateSettlement;

  public DObSettlementActivateController(ActivateSettlement activateSettlement) {
    this.activateSettlement = activateSettlement;
  }

  @PutMapping("/{settlementCode}/post")
  public @ResponseBody String activate(
      @PathVariable String settlementCode, @RequestBody String valueObject) throws Exception {

    return activateSettlement.activate(
        SettlementActivateContextBuilder.newContext()
            .objectCode(settlementCode)
            .valueObject(valueObject));
  }
}
