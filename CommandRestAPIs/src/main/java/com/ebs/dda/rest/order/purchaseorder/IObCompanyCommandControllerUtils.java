package com.ebs.dda.rest.order.purchaseorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.editability.DObPurchaseOrderCompanySectionEditabilityManager;
import java.util.Set;

public class IObCompanyCommandControllerUtils extends DObPurchaseOrderControllerUtils {

  private DObPurchaseOrderCompanySectionEditabilityManager
      purchaseOrderCompanySectionEditabilityManager;

  public IObCompanyCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public Set<String> getEnableAttributes(DObPurchaseOrderGeneralModel orderGeneralModel) {
    return purchaseOrderCompanySectionEditabilityManager.getEnabledAttributesConfigByTypeAndState(
        orderGeneralModel.getObjectTypeCode(), orderGeneralModel.getCurrentStates());
  }

  public String[] getMandatoryAttributesByState(String currentState) {
    return new String[] {};
  }

  public AuthorizedPermissionsConfig getEditCompanyAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(
        ICObCompany.SYS_NAME, IActionsNames.READ_BANK_DETAILS, "ReadBankAccountNumber");
    permissionsConfig.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME, IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER, "ReadBusinessUnits");
    permissionsConfig.addPermissionConfig(
            ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadAllCompany");
    return permissionsConfig;
  }

  public void setPurchaseOrderCompanyEditabilityManager(
      DObPurchaseOrderCompanySectionEditabilityManager
          purchaseOrderCompanySectionEditabilityManager) {
    this.purchaseOrderCompanySectionEditabilityManager =
        purchaseOrderCompanySectionEditabilityManager;
  }
}
