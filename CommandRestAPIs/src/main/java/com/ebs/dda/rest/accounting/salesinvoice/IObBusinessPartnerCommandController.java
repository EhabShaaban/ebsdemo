package com.ebs.dda.rest.accounting.salesinvoice;

import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.DObSalesInvoiceAuthorizationManager;
import com.ebs.dda.accounting.dbo.jpa.valueobjects.IObBusinessPartnerValueObject;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.editability.SalesInvoiceBusinessPartnerEditabilityManger;
import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceDeleteCommand;
import com.ebs.dda.commands.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerSaveCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.rest.CommonControllerUtils;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/salesinvoice")
public class IObBusinessPartnerCommandController implements InitializingBean {

  @Autowired private EntityLockCommand<DObSalesInvoice> entityLockCommand;
  private DObSalesInvoiceCommandControllerUtils dObSalesInvoiceCommandControllerUtils;
  @Autowired private DObSalesInvoiceAuthorizationManager salesInvoiceAuthorizationManager;
  @Autowired private DObSalesInvoiceDeleteCommand salesInvoiceDeleteCommand;
  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired private DObSalesInvoiceRep saleInvoiceRep;

  @Autowired
  private SalesInvoiceBusinessPartnerEditabilityManger salesInvoiceBusinessPartnerEditabilityManger;

  @Autowired private IObSalesInvoiceBusinessPartnerSaveCommand businessPartnerSaveCommand;

  @RequestMapping(method = RequestMethod.GET, value = "/{salesInvoiceCode}/lock/businesspartner")
  public @ResponseBody String openBusinessPartnerEditMode(@PathVariable String salesInvoiceCode)
      throws Exception {
    salesInvoiceDeleteCommand.checkIfInstanceExists(salesInvoiceCode);

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoiceGeneralModel, IDObSalesInvoiceActionNames.EDIT_BUSINESS_PARTNER);
    dObSalesInvoiceCommandControllerUtils.lockSalesInvoiceSectionForEdit(
        salesInvoiceGeneralModel,
        IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION,
        IDObSalesInvoiceActionNames.EDIT_BUSINESS_PARTNER);

    Set<String> stateEnabledAttributes =
        dObSalesInvoiceCommandControllerUtils.getEnableAttributes(salesInvoiceGeneralModel);
    AuthorizedPermissionsConfig permissionsConfig =
        dObSalesInvoiceCommandControllerUtils.getEditAuthorizedPermissionsConfig();
    List<String> authorizedReads =
        dObSalesInvoiceCommandControllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    return dObSalesInvoiceCommandControllerUtils.serializeEditConfigurationsResponse(
        editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{salesInvoiceCode}/unlock/businesspartner")
  public @ResponseBody String unlockBusinessPartnerSection(@PathVariable String salesInvoiceCode)
      throws Exception {

    DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    dObSalesInvoiceCommandControllerUtils.checkIfEntityIsNull(dObSalesInvoiceGeneralModel);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        dObSalesInvoiceGeneralModel, IDObSalesInvoiceActionNames.EDIT_BUSINESS_PARTNER);

    entityLockCommand.unlockSection(
        salesInvoiceCode, IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION);

    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "{salesInvoiceCode}/update/businesspartner")
  public @ResponseBody String updateBusinessPartner(
      @PathVariable String salesInvoiceCode, @RequestBody String businessPartnerJsonData)
      throws Exception {
    IObBusinessPartnerValueObject businessPartnerValueObject =
        (IObBusinessPartnerValueObject)
            CommonControllerUtils.deserializeRequestBody(
                businessPartnerJsonData, IObBusinessPartnerValueObject.class);

    businessPartnerValueObject.setSalesInvoiceCode(salesInvoiceCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        salesInvoiceCode, IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION);
    businessPartnerSaveCommand.executeCommand(businessPartnerValueObject);
    entityLockCommand.unlockSection(
        salesInvoiceCode, IDObSalesInvoiceSectionNames.BUSINESS_PARTNER_SECTION);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    dObSalesInvoiceCommandControllerUtils =
        new DObSalesInvoiceCommandControllerUtils(
            salesInvoiceAuthorizationManager.getAuthorizationManager());
    dObSalesInvoiceCommandControllerUtils.setEntityLockCommand(entityLockCommand);
    dObSalesInvoiceCommandControllerUtils.setSaleInvoiceRep(saleInvoiceRep);
    dObSalesInvoiceCommandControllerUtils.setSalesInvoiceBusinessPartnerEditabilityManger(
        salesInvoiceBusinessPartnerEditabilityManger);
  }
}
