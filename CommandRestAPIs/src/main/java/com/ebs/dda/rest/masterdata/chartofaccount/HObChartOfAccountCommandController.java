package com.ebs.dda.rest.masterdata.chartofaccount;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.masterdata.chartofaccount.HObChartOfAccountCreateCommand;
import com.ebs.dda.commands.masterdata.chartofaccount.HObChartOfAccountDeleteCommand;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountCreateValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountDeleteValueObject;
import com.ebs.dda.jpa.masterdata.chartofaccount.HObChartOfAccountsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;
import com.ebs.dda.masterdata.chartofaccounts.IHObChartOfAccountJsonSchema;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.chartofaccounts.HObChartOfAccountCreationValidator;
import com.ebs.dda.validation.chartofaccounts.HObChartOfAccountDeleteValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/masterdata/chartofaccount")
public class HObChartOfAccountCommandController implements InitializingBean {

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private HObChartOfAccountCreateCommand chartOfAccountCreateCommand;
  @Autowired private HObChartOfAccountCreationValidator chartOfAccountCreationValidator;
  @Autowired private HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep;
  @Autowired private IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep;
  @Autowired private HObChartOfAccountDeleteCommand deleteCommand;
  private CommonControllerUtils commonControllerUtils;
  @Autowired private HObChartOfAccountDeleteValidator deleteValidator;

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createChartOfAccount(@RequestBody String chartOfAccountData)
      throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
        IHObChartOfAccountJsonSchema.CREATION_SCHEMA, chartOfAccountData);
    HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject =
        (HObChartOfAccountCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                chartOfAccountData, HObChartOfAccountCreateValueObject.class);

    applyAuthorization(chartOfAccountCreateValueObject);

    ValidationResult validationResult =
        chartOfAccountCreationValidator.validate(chartOfAccountCreateValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      final StringBuilder userCode = new StringBuilder();
      chartOfAccountCreateCommand
          .executeCommand(chartOfAccountCreateValueObject)
          .subscribe(code -> userCode.append(code));
      return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
    }
  }

  private void applyAuthorization(
      HObChartOfAccountCreateValueObject chartOfAccountCreateValueObject) {
    authorizationManager.authorizeAction(IHObChartOfAccounts.SYS_NAME, IActionsNames.CREATE);
    if (!chartOfAccountCreateValueObject.getLevel().equals(IHObChartOfAccounts.ROOT)) {
      authorizationManager.authorizeAction(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL);
    }
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{glAccountCode}")
  public @ResponseBody String deleteGLAccount(@PathVariable String glAccountCode) throws Exception {

    HObChartOfAccountsGeneralModel glAccountsGeneralModel =
        glAccountsGeneralModelRep.findOneByUserCode(glAccountCode);
    CommonControllerUtils.checkIfEntityIsNull(glAccountsGeneralModel);
    authorizationManager.authorizeAction(glAccountsGeneralModel, IActionsNames.DELETE);
    deleteValidator.validate(glAccountCode);
    HObChartOfAccountDeleteValueObject valueObject = new HObChartOfAccountDeleteValueObject();
    valueObject.setGlAccountCode(glAccountCode);
    deleteCommand.executeCommand(valueObject);
    return CommonControllerUtils.getSuccessResponseWithUserCode(
        glAccountCode, CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @Override
  public void afterPropertiesSet() throws Exception {

    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }
}
