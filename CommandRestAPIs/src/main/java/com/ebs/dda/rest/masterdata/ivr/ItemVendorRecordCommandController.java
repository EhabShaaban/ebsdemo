package com.ebs.dda.rest.masterdata.ivr;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecordCreateCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecordDeleteCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecordSaveCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecoredGeneralDataEditCommand;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.ivr.IItemVendorRecord;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralDataValueObject;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordGeneralModel;
import com.ebs.dda.jpa.masterdata.ivr.ItemVendorRecordValueObject;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.masterdata.ivr.IItemVendorRecordSectionNames;
import com.ebs.dda.masterdata.ivr.validation.ItemVendorRecordCreationValidator;
import com.ebs.dda.masterdata.ivr.validation.ItemVendorRecordGeneralDataValidator;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "command/itemvendorrecord")
public class ItemVendorRecordCommandController implements InitializingBean {

  private final String RESPONSE_STATUS = "status";
  private final String CODE_RESPONSE_KEY = "code";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String CREATION_SUCCESSFUL_CODE = "Gen-msg-11";
  private final String DELETION_SUCCESSFUL_CODE = "Gen-msg-12";
  private final String SAVE_SUCCESSFUL_CODE = "Gen-msg-04";
  private final String USER_CODE_KEY = "userCode";

    @Autowired
    private ItemVendorRecordCreateCommand itemVendorRecordCreateCommand;
    @Autowired
    private ItemVendorRecordSaveCommand itemVendorRecordSaveCommand;

    @Autowired
    private ItemVendorRecordDeleteCommand itemVendorRecordDeleteCommand;

    @Autowired
    private AuthorizationManager authorizationManager;

    @Autowired
    private ItemVendorRecordCreationValidator itemVendorRecordCreationValidator;

    @Autowired
    private ItemVendorRecordGeneralDataValidator itemVendorRecordGeneralDataValidator;

    @Autowired
    private ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep;

  private ItemVendorRecordControllerUtils itemVendorRecordControllerUtils;
    @Autowired
    private EntityLockCommand<ItemVendorRecord> entityLockCommand;

  @Autowired
  private ItemVendorRecoredGeneralDataEditCommand itemVendorRecoredGeneralDataEditCommand;

  @Override
  public void afterPropertiesSet() {
    itemVendorRecordControllerUtils = new ItemVendorRecordControllerUtils(authorizationManager);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/createconfiguration")
  public @ResponseBody
  String getCreateIVRConfiguration() {
    return itemVendorRecordControllerUtils.serializeEditConfigurationsResponse(
        getCreateAuthorizedReads());
  }

  @RequestMapping(value = "/create", method = RequestMethod.POST)
  public @ResponseBody
  String createItemVendorRecord(@RequestBody String creationData)
      throws Exception {
    itemVendorRecordControllerUtils.applySchemaValidation(
        ItemVendorRecordValueObject.class, creationData);

    ItemVendorRecordValueObject valueObject =
        (ItemVendorRecordValueObject)
            deserializeRequestBody(creationData, ItemVendorRecordValueObject.class);

    applyAuthorizationForCreateAction();

    ValidationResult validationResult = itemVendorRecordCreationValidator.validate(valueObject);
    if (!validationResult.isValid()) {
      return itemVendorRecordControllerUtils.getFailureResponse(validationResult);
    }
    final StringBuilder userCode = new StringBuilder();
    Observable<String> codeObservable = itemVendorRecordCreateCommand.executeCommand(valueObject);
    codeObservable.subscribe(code -> userCode.append(code));
    return getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{itemVendorRecordCode}")
  public @ResponseBody
  String deleteIVR(@PathVariable String itemVendorRecordCode)
      throws Exception {

    authorizeActionOnItemVendorRecord(itemVendorRecordCode, IActionsNames.DELETE);
    itemVendorRecordDeleteCommand.executeCommand(itemVendorRecordCode);
    return getSuccessDeletionResponse();
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/saveGeneralData/{ivrCode}")
  public @ResponseBody
  String saveGeneralData(
      @PathVariable String ivrCode, @RequestBody String generalDataJsonObject) throws Exception {

    itemVendorRecordControllerUtils.applySchemaValidation(
        ItemVendorRecordGeneralDataValueObject.class, generalDataJsonObject);

    authorizeActionOnItemVendorRecord(ivrCode, IActionsNames.UPDATE_GENERAL_DATA);
    itemVendorRecordControllerUtils.applyAuthorizationOnGeneralDataSectionFields();
    entityLockCommand.checkIfEntityExists(ivrCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        ivrCode, IItemVendorRecordSectionNames.GENERAL_DATA_SECTION);
    ItemVendorRecordGeneralDataValueObject generalDataValueObject =
        new Gson().fromJson(generalDataJsonObject, ItemVendorRecordGeneralDataValueObject.class);
    ValidationResult validationResult =
        itemVendorRecordGeneralDataValidator.validate(generalDataValueObject);
    if (!validationResult.isValid()) {
      return itemVendorRecordControllerUtils.getFailureResponse(validationResult);
    }
    itemVendorRecordSaveCommand.setUserCode(ivrCode);
    itemVendorRecordSaveCommand.executeCommand(generalDataValueObject);
    entityLockCommand.unlockSection(ivrCode, IItemVendorRecordSectionNames.GENERAL_DATA_SECTION);
    return getSuccessResponseWithUserCode(SAVE_SUCCESSFUL_CODE, ivrCode);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/lock/generaldatasection/{itemVendorRecordCode}")
  public @ResponseBody
  String openGeneralDataSection(@PathVariable String itemVendorRecordCode)
      throws Exception {
    authorizeActionOnItemVendorRecord(itemVendorRecordCode, IActionsNames.UPDATE_GENERAL_DATA);
    itemVendorRecoredGeneralDataEditCommand.lockSection(itemVendorRecordCode);

    List<String> authorizedReads = prepareGeneralDataAuthorizedReads();

    return itemVendorRecordControllerUtils.serializeEditConfigurationsResponse(authorizedReads);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/generaldatasection/{itemVendorRecordCode}")
  public @ResponseBody
  String closeGeneralDataSection(@PathVariable String itemVendorRecordCode)
      throws Exception {
    authorizeActionOnItemVendorRecord(itemVendorRecordCode, IActionsNames.UPDATE_GENERAL_DATA);
    itemVendorRecoredGeneralDataEditCommand.unlockSection(itemVendorRecordCode);
    return getSuccessResponseWithUserCode(SUCCESS_RESPONSE_KEY, itemVendorRecordCode);
  }

  @RequestMapping(
      method = RequestMethod.POST,
      value = "/unlock/lockedsections/{itemVendorRecordCode}")
  public @ResponseBody
  String closeAllSectionsInItemVendorRecord(
      @PathVariable String itemVendorRecordCode, HttpServletRequest request) throws Exception{
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(itemVendorRecordCode, lockedSections);
    return getSuccessResponseWithUserCode(SUCCESS_RESPONSE_KEY, itemVendorRecordCode);
  }

  private void authorizeActionOnItemVendorRecord(String itemVendorRecordCode, String actionName)
      throws Exception {
    ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
        getItemVendorRecordGeneralModel(itemVendorRecordCode);
    try {
      authorizationManager.authorizeAction(itemVendorRecordGeneralModel, actionName);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException();
      authorizationException.initCause(ex);
      throw authorizationException;
    }
  }

  private ItemVendorRecordGeneralModel getItemVendorRecordGeneralModel(String itemVendorRecordCode)
      throws InstanceNotExistException {
    ItemVendorRecordGeneralModel itemVendorRecordGeneralModel =
        itemVendorRecordGeneralModelRep.findByUserCode(itemVendorRecordCode);
    if (itemVendorRecordGeneralModel == null) {
      throw new InstanceNotExistException();
    }
    return itemVendorRecordGeneralModel;
  }

  private Object deserializeRequestBody(String valueObject, Class<?> valueObjectClass) {
    Gson gson = new Gson();
    Object serializedValueObject = gson.fromJson(valueObject, valueObjectClass);
    return serializedValueObject;
  }

  private String getSuccessDeletionResponse() {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(CODE_RESPONSE_KEY, DELETION_SUCCESSFUL_CODE);
    return successResponse.toString();
  }

  private String getSuccessCreationResponse(String userCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    String USER_CODE_KEY = "userCode";
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, CREATION_SUCCESSFUL_CODE);
    return successResponse.toString();
  }

  private List<String> getCreateAuthorizedReads() {
    List<String> authorizedReads = new ArrayList<>();
    boolean isReadItemAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            IMObItem.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadVendorAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            IMObVendor.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadReadItemUOMsAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadCurrenciesAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadPurchasingUnitAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadItemAllowed) {
      authorizedReads.add("ReadItems");
    }
    if (isReadVendorAllowed) {
      authorizedReads.add("ReadVendors");
    }
    if (isReadReadItemUOMsAllowed) {
      authorizedReads.add("ReadItemUOMs");
    }
    if (isReadPurchasingUnitAllowed) {
      authorizedReads.add("ReadPurchasingUnit");
    }
    if (isReadCurrenciesAllowed) {
      authorizedReads.add("ReadCurrencies");
    }
    return authorizedReads;
  }

  protected void applyAuthorizationForCreateAction() {
    authorizationManager.authorizeAction(MObItem.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(MObVendor.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IItemVendorRecord.SYS_NAME, IActionsNames.CREATE);
  }

  private List<String> prepareGeneralDataAuthorizedReads() throws Exception {
    List<String> authorizedReads = new ArrayList<>();

    boolean isReadItemAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            IMObItem.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadVendorAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            IMObVendor.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadReadItemUOMsAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadCurrenciesAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadPurchasingUnitAllowed =
        itemVendorRecordControllerUtils.isReadPermissionAllowed(
            ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadItemAllowed) {
      authorizedReads.add("ReadItems");
    }
    if (isReadVendorAllowed) {
      authorizedReads.add("ReadVendors");
    }
    if (isReadReadItemUOMsAllowed) {
      authorizedReads.add("ReadItemUOMs");
    }
    if (isReadPurchasingUnitAllowed) {
      authorizedReads.add("ReadPurchasingUnit");
    }
    if (isReadCurrenciesAllowed) {
      authorizedReads.add("ReadCurrencies");
    }

    return authorizedReads;
  }

  public String getSuccessResponseWithUserCode(String msgCode, String userCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }
}
