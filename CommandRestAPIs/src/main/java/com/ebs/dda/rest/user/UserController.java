package com.ebs.dda.rest.user;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.commands.UserChangePasswordCommand;
import com.ebs.dac.security.jpa.valueobjects.UserPasswordValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.user.UserChangeDefaultPasswordValidator;
import com.google.gson.Gson;
import io.reactivex.Observable;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/user")
public class UserController implements InitializingBean {

  private final String REQUESTED_URL = "RequestedURL";

  @Autowired
  private UserChangePasswordCommand userChangePasswordCommand;

  @Autowired
  private UserChangeDefaultPasswordValidator userChangeDefaultPasswordValidator;

  @PostMapping("/update/password")
  public @ResponseBody
  String changeDefaultPassword(
      @RequestBody String userData, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    CommonControllerUtils.applySchemaValidation(UserPasswordValueObject.class, userData);
    UserPasswordValueObject userPasswordValueObject =
        new Gson().fromJson(userData, UserPasswordValueObject.class);

    ValidationResult validationResult =
        userChangeDefaultPasswordValidator.validate(userPasswordValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    }
    Observable<String> commandObservable =
        userChangePasswordCommand.executeCommand(userPasswordValueObject);
    commandObservable.subscribe(
        status -> {
          Subject subject = SecurityUtils.getSubject();
          subject.logout();
          try {
            response.setHeader("Location", "logout");
            removeSavedUrl(request, response);
          } catch (Exception e) {
          }
        });

    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  private void removeSavedUrl(HttpServletRequest request, HttpServletResponse response) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().trim().equals(REQUESTED_URL)) {
          cookie.setValue("");
          cookie.setPath("/");
          cookie.setMaxAge(0);
          response.addCookie(cookie);
        }
      }
    }
  }
  @Override
  public void afterPropertiesSet() {
  }
}
