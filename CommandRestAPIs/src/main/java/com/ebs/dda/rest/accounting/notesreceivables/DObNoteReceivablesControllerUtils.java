package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivablesValidator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesCreateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DObNoteReceivablesControllerUtils extends CommonControllerUtils {

  private EntityLockCommand<DObMonetaryNotes> entityLockCommand;

  @Autowired private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;
  private DObNotesReceivablesValidator validator;

  public DObNoteReceivablesControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObMonetaryNotes> entityLockCommand) {
    super(authorizationManager);
    this.entityLockCommand = entityLockCommand;
  }

  public AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();

    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL, "ReadBusinessUnits");

    configuration.addPermissionConfig(ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadCompany");

    configuration.addPermissionConfig(
        IMObCustomer.SYS_NAME, IActionsNames.READ_ALL, "ReadCustomers");

    configuration.addPermissionConfig(
        ICObCurrency.SYS_NAME, IActionsNames.READ_ALL, "ReadCurrency");

    configuration.addPermissionConfig(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME,
        IActionsNames.READ_ALL,
        "ReadNotesReceivables");

    configuration.addPermissionConfig(
        IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL, "ReadDocumentOwners");

    return configuration;
  }

  public void checkIfUpdateActionIsAllowedInNotesReceivableCurrentState(
      String notesReceivableCode, String sectionName, String actionName) throws Exception {
    try {
      checkIfActionIsAllowedInCurrentStateFromStateMachine(notesReceivableCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(notesReceivableCode, sectionName);
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
  }

  private void checkIfActionIsAllowedInCurrentStateFromStateMachine(
      String notesReceivableCode, String actionName) throws Exception {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);
    DObNotesReceivablesStateMachine notesReceivablesStateMachine =
        new DObNotesReceivablesStateMachine();
    checkIfActionIsAllowedInCurrentState(
        notesReceivablesStateMachine, notesReceivablesGeneralModel, actionName);
  }


  public void applyAuthorizationForCreateValueObjectValues(
      DObNotesReceivablesCreateValueObject valueObject) throws Exception {
    AuthorizedPermissionsConfig permissionsConfig = constructCreateConfiguration();
    applyAuthorizationForValueObjectAuthorizedReads(permissionsConfig);

    applyAuthorizationForBusinessUnitCode(valueObject.getBusinessUnitCode());
    applyAuthorizationForCustomerCode(valueObject.getBusinessPartnerCode());
  }

  private void applyAuthorizationForBusinessUnitCode(String purchaseUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
            purchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    if (purchasingUnitGeneralModel != null) {
      try {
        authorizationManager.authorizeAction(
                purchasingUnitGeneralModel, IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  private void applyAuthorizationForCustomerCode(String customerCode) {
    List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnits =
        customerBusinessUnitGeneralModelRep.findAllByUserCode(customerCode);
    if (!customerBusinessUnits.isEmpty()) {
      authorizeActionOnCustomer(customerBusinessUnits, IActionsNames.READ_ALL);
    }
  }

  public void authorizeActionOnCustomer(
      List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnitsList, String actionName) {
    boolean isAuthorized = false;
    Exception exception = null;
    for (IObCustomerBusinessUnitGeneralModel customerBusinessUnit : customerBusinessUnitsList) {
      try {
        authorizationManager.authorizeAction(customerBusinessUnit, actionName);
        isAuthorized = true;
        break;
      } catch (Exception e) {
        exception = e;
      }
    }
    if (!isAuthorized) {
      throw new AuthorizationException(exception);
    }
  }

  public void setValidator(DObNotesReceivablesValidator validator) {
    this.validator = validator;
  }

  public void setNotesReceivablesGeneralModelRep(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    this.notesReceivablesGeneralModelRep = notesReceivablesGeneralModelRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setCustomerBusinessUnitGeneralModelRep(
      IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep) {
    this.customerBusinessUnitGeneralModelRep = customerBusinessUnitGeneralModelRep;
  }
}
