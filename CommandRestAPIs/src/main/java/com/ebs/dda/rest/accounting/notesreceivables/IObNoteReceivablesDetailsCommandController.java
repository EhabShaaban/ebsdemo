package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableDetailsEditabilityManager;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesActionNames;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesSectionNames;
import com.ebs.dda.commands.accounting.notesreceivables.IObNoteReceivablesDetailsUpdateCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsValueObject;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.notesreceivable.IObNotesReceivableDetailsValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "command/accounting/notesreceivables")
public class IObNoteReceivablesDetailsCommandController implements InitializingBean {

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private EntityLockCommand<DObMonetaryNotes> entityLockCommand;
  @Autowired private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  @Autowired private IObNoteReceivablesDetailsUpdateCommand noteReceivablesDetailsUpdateCommand;

  @Autowired
  private IObNotesReceivableDetailsEditabilityManager notesReceivableDetailsEditabilityManager;

  @Autowired private IObNotesReceivableDetailsValidator  notesReceivableDetailsValidator;


  private IObNoteReceivablesDetailsControllerUtils controllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/lock/notesdetails")
  public @ResponseBody String openNoteDetailsSectionEditMode(
      @PathVariable String notesReceivableCode) throws Exception {

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.UPDATE_NOTES_DETAILS_SECTION);

    controllerUtils.lockNotesReceivableSectionForEdit(
        notesReceivableCode,
        IDObNotesReceivablesSectionNames.NOTES_DETAILS_SECTION,
        IDObNotesReceivablesActionNames.UPDATE_NOTES_DETAILS_SECTION);

    Set<String> stateEnabledAttributes =
        controllerUtils.getEnableAttributes(notesReceivablesGeneralModel);

    String[] mandatories =
        controllerUtils.getMandatoryAttributesByState(
            notesReceivablesGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getEditNotesDetailsAuthorizedPermissionsConfig();

    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(mandatories);
    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{notesReceivableCode}/unlock/notesdetails")
  public @ResponseBody String cancelNoteDetailsEditMode(@PathVariable String notesReceivableCode)
      throws Exception {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.UPDATE_NOTES_DETAILS_SECTION);

    controllerUtils.unlockSection(
        IDObNotesReceivablesSectionNames.NOTES_DETAILS_SECTION, notesReceivableCode);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{notesReceivableCode}/update/notesdetails")
  public @ResponseBody String updateNotesDetails(
      @PathVariable String notesReceivableCode, @RequestBody String notesDetailsJsonData)
      throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
            IDObNotesReceivableJsonSchema.NOTE_DETAILS_SCHEMA, notesDetailsJsonData);

    IObMonetaryNotesDetailsValueObject notesDetailsValueObject =
        (IObMonetaryNotesDetailsValueObject)
            CommonControllerUtils.deserializeRequestBody(
                notesDetailsJsonData, IObMonetaryNotesDetailsValueObject.class);

    notesDetailsValueObject.setNotesReceivableCode(notesReceivableCode);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        notesReceivableCode, IDObNotesReceivablesSectionNames.NOTES_DETAILS_SECTION);
    notesReceivableDetailsValidator.validate(notesDetailsValueObject);
    try {
      noteReceivablesDetailsUpdateCommand.executeCommand(notesDetailsValueObject);
    } finally {
      entityLockCommand.unlockSection(
          notesReceivableCode, IDObNotesReceivablesSectionNames.NOTES_DETAILS_SECTION);
    }

    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils =
        new IObNoteReceivablesDetailsControllerUtils(authorizationManager, entityLockCommand);
    controllerUtils.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    controllerUtils.setNotesReceivableDetailsEditabilityManager(
        notesReceivableDetailsEditabilityManager);
  }
}
