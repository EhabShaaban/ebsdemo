package com.ebs.dda.rest.masterdata.company;

import com.ebs.dda.commands.masterdata.company.CObCompanyLogoCreationCommand;
import com.ebs.dda.jpa.masterdata.company.CObCompanyLogoCreationValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.Gson;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/company")
public class CObCompanyCommandController implements InitializingBean {

  @Autowired
  private CObCompanyLogoCreationCommand companyLogoCreationCommand;

  private CommonControllerUtils commonControllerUtils;

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils();
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/uploadlogo")
  public @ResponseBody
  String uploadCompanyLogo(@RequestBody String logoData) throws Exception {

    CObCompanyLogoCreationValueObject companyLogoCreationValueObject =
        new Gson().fromJson(logoData, CObCompanyLogoCreationValueObject.class);

    companyLogoCreationCommand.executeCommand(companyLogoCreationValueObject);

    return commonControllerUtils.getSuccessCreationResponse(
        companyLogoCreationValueObject.getCompanyCode());
  }
}
