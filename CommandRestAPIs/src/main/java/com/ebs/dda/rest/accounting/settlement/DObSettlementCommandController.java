package com.ebs.dda.rest.accounting.settlement;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObSettlementAuthorizationManager;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementJsonSchema;
import com.ebs.dda.accounting.settlement.validation.DObSettlementCreationValidator;
import com.ebs.dda.commands.accounting.settlement.DObSettlementCreationCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementCreationValueObject;
import com.ebs.dda.jpa.accounting.settlements.ICObSettlementType;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlement;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.Gson;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/command/settlement")
public class DObSettlementCommandController implements InitializingBean {

	@Autowired
	private DObSettlementCreationCommand creationCommand;
	@Autowired
	private EntityLockCommand<DObSettlement> entityLockCommand;

	private CommonControllerUtils commonControllerUtils;

	@Autowired
	private DObSettlementCreationValidator creationValidator;
	@Autowired private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;
	@Autowired private AuthorizationManager authorizationManager;
	@Autowired private DObSettlementAuthorizationManager settlementAuthorizationManager;

	@Override
	public void afterPropertiesSet() {
		commonControllerUtils = new CommonControllerUtils(authorizationManager);
	}

	@PostMapping
	public @ResponseBody String createSettlement(@RequestBody String requestBody) throws Exception {
		CommonControllerUtils.applySchemaValidationForString(
						IDObSettlementJsonSchema.CREATION_SCHEMA, requestBody);
		DObSettlementCreationValueObject valueObject = new Gson().fromJson(requestBody,
						DObSettlementCreationValueObject.class);
		applyAuthorizationForCreate(valueObject);
		final StringBuilder userCode = new StringBuilder();
		ValidationResult validationResult = creationValidator.validate(valueObject);
		if (!validationResult.isValid()) {
			return CommonControllerUtils.getFailureResponse(validationResult);
		}
		Observable<String> codeObservable = creationCommand.executeCommand(valueObject);
		codeObservable.subscribe((code) -> {
			userCode.append(code);
		});

		return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
	}

	private void applyAuthorizationForCreate(DObSettlementCreationValueObject valueObject) throws Exception {
		settlementAuthorizationManager.applyAuthorizationForCreation(valueObject);
		applyAuthorizationForPurchaseUnit(valueObject.getBusinessUnitCode());
	}

	private void applyAuthorizationForPurchaseUnit(String purchaseUnitCode) throws Exception {
		CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
				cObPurchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
		if (purchasingUnitGeneralModel != null) {
			try {
				authorizationManager.authorizeAction(
						purchasingUnitGeneralModel, IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER);
			} catch (ConditionalAuthorizationException ex) {
				throw new AuthorizationException(ex);
			}
		}
	}

	@GetMapping
	public @ResponseBody String getCreateConfiguration() {
		authorizationManager.authorizeAction(IDObSettlement.SYS_NAME, IActionsNames.CREATE);

		AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();

		List<String> authorizedReads =
				commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

		return CommonControllerUtils.serializeEditConfigurationsResponse(
				new ArrayList<String>(new HashSet<>(authorizedReads)));
	}

	private AuthorizedPermissionsConfig constructCreateConfiguration() {
		AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
		configuration.addPermissionConfig(
				ICObSettlementType.SYS_NAME, IActionsNames.READ_ALL, "ReadSettlementTypes");
		return configuration;
	}

	@PostMapping("/unlock/lockedsections/{settlementCode}")
	public @ResponseBody String closeAllSectionsInSettlement(@PathVariable String settlementCode,
					HttpServletRequest request) throws Exception {

		BufferedReader reader = request.getReader();
		String requestData = reader.lines().collect(Collectors.joining());
		String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");

		List<String> lockedSections = Arrays.asList(splitedSections);
		entityLockCommand.unlockAllResourceLockedSectionsByUser(settlementCode, lockedSections);

		return CommonControllerUtils.getSuccessResponseWithUserCode(settlementCode,
						CommonControllerUtils.SUCCESS_RESPONSE_KEY);
	}
}
