package com.ebs.dda.rest.masterdata.item;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.accessability.UpdateDependentInstanceException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.masterdata.item.*;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;
import com.ebs.dda.jpa.masterdata.item.*;
import com.ebs.dda.jpa.masterdata.itemgroup.IHObItemGroup;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.productmanager.ICObProductManager;
import com.ebs.dda.masterdata.item.*;
import com.ebs.dda.repositories.masterdata.item.MObItemAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rest.masterdata.utils.MasterDataControllerUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.util.*;
import java.util.stream.Collectors;

// Created By Hossam Hassan @ 10/10/18
@RestController
@RequestMapping(value = "command/item")
public class MObItemCommandController implements InitializingBean {

  public final String DELETION_SUCCESSFUL_CODE = "Gen-msg-12";
  private final String RESPONSE_STATUS = "status";
  private final String CODE_RESPONSE_KEY = "code";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String SAVING_SUCCESSFUL_CODE = "Gen-msg-04";
  private final String CREATION_SUCCESSFUL_CODE = "Gen-msg-11";
  private final String USER_CODE_KEY = "userCode";

  private MasterDataControllerUtils masterDataControllerUtils;
  @Autowired
  private MObItemCreateCommand itemCreateCommand;
  @Autowired
  private MObItemUOMEditCommand uomEditCommand;
  @Autowired
  private MObItemCreationValidator itemCreationValidator;
  @Autowired
  private MObItemAccountingDetailsValidator itemAccountingDetailsValidator;
  @Autowired
  private MObItemUOMValidator itemUOMValidator;
  @Autowired
  private AuthorizationManager authorizationManager;
  @Autowired
  private MObItemDeleteCommand mObItemDeleteCommand;
  @Autowired
  private MObItemAuthorizationGeneralModelRep generalModelRep;
  @Autowired
  private MObItemGeneralModelRep mobItemGeneralModelRep;
  @Autowired
  private MObItemGeneralDataEditCommand mobItemGeneralDataEditCommand;
  @Autowired
  private EntityLockCommand<MObItem> entityLockCommand;
  @Autowired
  private MObItemAccountingDetailsEditCommand itemAccountingDetailsEditCommand;

  @Autowired
  private MObItemGeneralDataMandatoryValidator itemGeneralDataValidator;

  @RequestMapping(method = RequestMethod.PUT, value = "/update/unitofmeasure")
  public @ResponseBody String updateUnitOfMeasureSection(@RequestBody String generaldataJsonObject)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(MObItemUOMValueObject.class, generaldataJsonObject);
    MObItemUOMValueObject itemUOMValueObjects =
        (MObItemUOMValueObject)
            deserializeRequestBody(generaldataJsonObject, MObItemUOMValueObject.class);
    String itemCode = itemUOMValueObjects.getItemCode();

    applyAuthorizationForItemUnitOfMeasures(itemCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        itemCode, IMObItemSectionNames.UOM_SECTION);
    ValidationResult validationResult = itemUOMValidator.validate(itemUOMValueObjects);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      try {

        uomEditCommand.executeCommand(itemUOMValueObjects);
      } catch (TransactionSystemException e) {
        throw new UpdateDependentInstanceException();
      } finally {
        uomEditCommand.unlockSection(itemCode);
      }
      return getSuccessResponseWithUserCode(SAVING_SUCCESSFUL_CODE, itemCode);
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/lock/unitofmeasure/{itemCode}")
  public @ResponseBody String lockItemUOMs(@PathVariable String itemCode) throws Exception {
    applyAuthorizationForAction(itemCode, IMObItemActionNames.EDIT_UOM_SECTION);
    uomEditCommand.lock(itemCode);

    List<String> authorizedReads = prepareUOMAuthorizedReads();

    return MasterDataControllerUtils.serializeEditConfigurationsResponse(authorizedReads);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unlock/unitofmeasure/{itemCode}")
  public @ResponseBody String unlockItemUOMs(@PathVariable String itemCode) throws Exception {
    applyAuthorizationForAction(itemCode, IMObItemActionNames.EDIT_UOM_SECTION);
    uomEditCommand.unlockSection(itemCode);
    return getSuccessResponseWithUserCode(SUCCESS_RESPONSE_KEY, itemCode);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{itemCode}")
  public @ResponseBody String deleteItem(@PathVariable String itemCode) throws Exception {
    mObItemDeleteCommand.checkIfInstanceExists(itemCode);
    try {
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
          generalModelRep.findAllByUserCode(itemCode);
      applyAuthorization(itemPurchaseUnits, IActionsNames.DELETE);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
    mObItemDeleteCommand.executeCommand(itemCode);
    return getSuccessResponseWithUserCode(DELETION_SUCCESSFUL_CODE, itemCode);
  }

  @RequestMapping(value = "/create", method = RequestMethod.POST)
  public @ResponseBody String createItem(@RequestBody String creationData) throws Exception {

    CommonControllerUtils.applySchemaValidation(MObItemCreationValueObject.class, creationData);

    MObItemCreationValueObject valueObject =
        (MObItemCreationValueObject)
            deserializeRequestBody(creationData, MObItemCreationValueObject.class);
    applyAuthorizationForCreateAction();

    ValidationResult validationResult = null;
    validationResult = itemCreationValidator.validate(valueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      final StringBuilder userCode = new StringBuilder();
      Observable<String> codeObservable = itemCreateCommand.executeCommand(valueObject);
      codeObservable.subscribe(
              (code) -> {
                userCode.append(code);
              });
      return getSuccessCreationResponse(userCode.toString());
    }
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{itemCode}")
  public @ResponseBody
  String closeAllSectionsInItem(
          @PathVariable String itemCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(itemCode, lockedSections);
    return getSuccessResponseWithUserCode(SUCCESS_RESPONSE_KEY, itemCode);
  }

  public void applyAuthorization(
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits, String actionName)
      throws Exception {
    if (itemPurchaseUnits == null || itemPurchaseUnits.isEmpty()) {
      throw new InstanceNotExistException();
    }
    applyAuthorizationAction(itemPurchaseUnits, actionName);
  }

  public String getSuccessResponseWithUserCode(String msgCode, String userCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/lock/generaldatasection/{itemCode}")
  public @ResponseBody String openGeneralDataSectionInItem(@PathVariable String itemCode)
      throws Exception {
    applyAuthorizationForAction(itemCode, IActionsNames.UPDATE_GENERAL_DATA);
    mobItemGeneralDataEditCommand.lock(itemCode);

    List<String> authorizedReads = prepareGeneralDataAuthorizedReads();

    return MasterDataControllerUtils.serializeEditConfigurationsResponse(authorizedReads);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unlock/generaldatasection/{itemCode}")
  public @ResponseBody
  String closeGeneralDataSectionInItem(@PathVariable String itemCode)
          throws Exception {
    applyAuthorizationForAction(itemCode, IActionsNames.UPDATE_GENERAL_DATA);
    mobItemGeneralDataEditCommand.unlock(itemCode);
    return getSuccessResponseWithUserCode(SUCCESS_RESPONSE_KEY, itemCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/lock/accountingdetails/{itemCode}")
  public @ResponseBody
  String openAccountingDetailsSectionEditMode(
          @PathVariable String itemCode, HttpServletResponse response) throws Exception {

    applyAuthorizationForAction(itemCode, IMObItemActionNames.EDIT_ACCOUNTING_DETAILS_SECTION);

    itemAccountingDetailsEditCommand.lock(itemCode);
    AuthorizedPermissionsConfig editRequestPermissionsConfig =
            constructEditAccountingDetailsConfiguration();
    List<String> authorizedReads =
            masterDataControllerUtils.getAuthorizedReads(editRequestPermissionsConfig);

    String[] mandatories = {IIObItemAccountingInfo.ACCOUNT_CODE};
    MObItemGeneralModel mobItemGeneralModel = mobItemGeneralModelRep.findByUserCode(itemCode);
    Set<String> editables = new HashSet<>();
    editables.add(IIObItemAccountingInfo.ACCOUNT_CODE);
    if(mobItemGeneralModel.getTypeCode().equals("SERVICE"))
      editables.add(IIObItemAccountingInfo.COST_FACTOR);
    return masterDataControllerUtils.serializeEditConfigurationsResponse(
            editables, mandatories, authorizedReads);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unlock/accountingdetails/{itemCode}")
  public @ResponseBody
  String closeAccountingDetailsSectionEditMode(
          @PathVariable String itemCode, HttpServletResponse response) throws Exception {

    applyAuthorizationForAction(itemCode, IMObItemActionNames.EDIT_ACCOUNTING_DETAILS_SECTION);

    itemAccountingDetailsEditCommand.unlockSection(itemCode);
    return getSuccessResponseWithUserCode(itemCode, SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/update/accountingdetails/{itemCode}")
  public @ResponseBody
  String updateAccountingDetailsSection(
          @PathVariable String itemCode, @RequestBody String accountingDetailsJsonObject)
          throws Exception {

    CommonControllerUtils.applySchemaValidation(
            MObItemAccountingInfoValueObject.class, accountingDetailsJsonObject);

    applyAuthorizationForSaveAccountingDetailsAction(itemCode);

    entityLockCommand.checkIfEntityExists(itemCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
            itemCode, IMObItemSectionNames.ACCOUNTING_DETAILS_SECTION);

    MObItemAccountingInfoValueObject itemAccountingInfoValueObject =
            (MObItemAccountingInfoValueObject)
                    deserializeRequestBody(
                            accountingDetailsJsonObject, MObItemAccountingInfoValueObject.class);
    itemAccountingInfoValueObject.setItemCode(itemCode);

    ValidationResult validationResult =
            itemAccountingDetailsValidator.validate(itemAccountingInfoValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      itemAccountingDetailsEditCommand.executeCommand(itemAccountingInfoValueObject);
      itemAccountingDetailsEditCommand.unlockSection(itemCode);
      return getSuccessResponseWithUserCode(SAVING_SUCCESSFUL_CODE, itemCode);
    }
  }

  private AuthorizedPermissionsConfig constructEditAccountingDetailsConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
            IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL_LIMITED, "ReadAccounts");
    return configuration;
  }

  private void applyAuthorizationForItemUnitOfMeasures(String itemCode) throws Exception {
    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(MObItem.SYS_NAME, IMObItemActionNames.EDIT_UOM_SECTION);
    applyAuthorizationForAction(itemCode, IMObItemActionNames.EDIT_UOM_SECTION);
  }

  private Object deserializeRequestBody(String valueObject, Class<?> valueObjectClass) {
    Gson gson = new Gson();
    Object serializedValueObject = gson.fromJson(valueObject, valueObjectClass);
    return serializedValueObject;
  }

  private String getSuccessCreationResponse(String userCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, CREATION_SUCCESSFUL_CODE);
    return successResponse.toString();
  }

  protected void applyAuthorizationForCreateAction() {
    authorizationManager.authorizeAction(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(IHObItemGroup.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObItem.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(MObItem.SYS_NAME, IActionsNames.CREATE);
  }

  private void checkPurchaseUnitsNotEmpty(List<MObItemAuthorizationGeneralModel> itemPurchaseUnits)
      throws InstanceNotExistException {
    if (itemPurchaseUnits == null || itemPurchaseUnits.isEmpty()) {
      throw new InstanceNotExistException();
    }
  }

  private void applyAuthorizationAction(
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits, String actionName)
      throws Exception {
    boolean isAuthorized = false;
    ConditionalAuthorizationException conditionalAuthorizationException = null;
    for (MObItemAuthorizationGeneralModel itemPurchaseUnit : itemPurchaseUnits) {
      try {
        authorizationManager.authorizeAction(itemPurchaseUnit, actionName);
        isAuthorized = true;
        break;
      } catch (ConditionalAuthorizationException e) {
        conditionalAuthorizationException = e;
      }
    }
    if (!isAuthorized) throw conditionalAuthorizationException;
  }

  private void applyAuthorizationForSaveAccountingDetailsAction(String itemCode) throws Exception {
    authorizationManager.authorizeAction(
            IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL_LIMITED);
    applyAuthorizationForAction(itemCode, IMObItemActionNames.EDIT_ACCOUNTING_DETAILS_SECTION);
  }

  private void applyAuthorizationForAction(String itemCode, String permissionName)
      throws Exception {
    try {
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
          generalModelRep.findAllByUserCode(itemCode);
      checkPurchaseUnitsNotEmpty(itemPurchaseUnits);
      applyAuthorizationAction(itemPurchaseUnits, permissionName);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  private List<String> prepareGeneralDataAuthorizedReads() throws Exception {
    List<String> authorizedReads = new ArrayList<>();

    boolean isReadPurchasingUnitAllowed =
        masterDataControllerUtils.isReadPermissionAllowed(
            ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadProductManagerAllowed =
        masterDataControllerUtils.isReadPermissionAllowed(
            ICObProductManager.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadPurchasingUnitAllowed) {
      authorizedReads.add("ReadPurchasingUnit");
    }

    if (isReadProductManagerAllowed) {
      authorizedReads.add("ReadProductManager");
    }
    return authorizedReads;
  }

  private List<String> prepareUOMAuthorizedReads() throws Exception {

    List<String> authorizedReads = new ArrayList<>();

    boolean isReadMeasuresAllowed =
        masterDataControllerUtils.isReadPermissionAllowed(
            ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadMeasuresAllowed) {
      authorizedReads.add("ReadMeasures");
    }
    return authorizedReads;
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/update/generaldata/{itemCode}")
  public @ResponseBody String updateGeneralDataSection(
      @PathVariable String itemCode, @RequestBody String generaldataJsonObject) throws Exception {

    CommonControllerUtils.applySchemaValidation(
        MObItemGeneralDataValueObject.class, generaldataJsonObject);
    applyAuthorizationForGeneralDataSection(itemCode);

    entityLockCommand.checkIfEntityExists(itemCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        itemCode, IMObItemSectionNames.GENERAL_DATA_SECTION);

    MObItemGeneralDataValueObject itemGeneralDataValueObject =
        (MObItemGeneralDataValueObject)
            deserializeRequestBody(generaldataJsonObject, MObItemGeneralDataValueObject.class);

    ValidationResult validationResult =
        itemGeneralDataValidator.validate(itemGeneralDataValueObject);

    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      mobItemGeneralDataEditCommand.executeCommand(itemGeneralDataValueObject);
      mobItemGeneralDataEditCommand.unlock(itemCode);
      return getSuccessResponseWithUserCode(SAVING_SUCCESSFUL_CODE, itemCode);
    }
  }

  private void applyAuthorizationForGeneralDataSection(String itemCode) throws Exception {
    authorizationManager.authorizeAction(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObProductManager.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(MObItem.SYS_NAME, IActionsNames.UPDATE_GENERAL_DATA);
    applyAuthorizationForAction(itemCode, IActionsNames.UPDATE_GENERAL_DATA);
  }

  @Override
  public void afterPropertiesSet() {
    masterDataControllerUtils = new MasterDataControllerUtils(authorizationManager);
  }
}
