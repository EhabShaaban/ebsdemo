package com.ebs.dda.rest;

import com.ebs.dac.common.utils.calendar.DateFormats;
import com.ebs.dac.common.utils.json.adapters.BigDecimalAdapter;
import com.ebs.dac.common.utils.json.adapters.JodaTimeJsonAdapter;
import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig.AuthorizedPermissionConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.RecordDeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidator;
import com.ebs.dac.foundation.realization.validation.validators.JsonSchemaValidatorFactory;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.fasterxml.jackson.core.JsonParseException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.joda.time.DateTime;

public class CommonControllerUtils {

  public static final String CODE_RESPONSE_KEY = "code";
  public static final String RESPONSE_STATUS = "status";
  public static final String CREATION_SUCCESSFUL_CODE = "Gen-msg-11";
  public static final String USER_CODE_KEY = "userCode";
  public static final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  public static final String RECORD_DELETION_SUCCESSFUL_CODE = "Gen-msg-18";
  public static final String FAIL_RESPONSE_KEY = "FAIL";
  public static final String MISSING_CORRECT_DATA_ERROR_CODE = "Gen-msg-05";
  public static final String DELETION_SUCCESSFUL_MESSAGE_CODE = "Gen-msg-12";
  public static final String SUCCESSFUL_ACTIVATION_CODE = "Gen-msg-47";

  public static final String SAVING_SUCCESSFUL_CODE = "Gen-msg-04";
  public static final String ADDING_ITEM_SUCCESSFUL_CODE = "Gen-msg-16";
  public static final String MISSING_FIELDS_KEY = "missingFields";
  public static final String MISSING_FIELDS_RESPONSE_CODE = "Gen-msg-40";
  public static final String STATE_CHANGED_SUCCESSFULLY = "LC-msg-54";

  protected AuthorizationManager authorizationManager;

  public CommonControllerUtils() {}

  public CommonControllerUtils(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public static String getSuccessResponse(String msgCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }

  public static String getSuccessResponseWithUserCode(String userCode, String msgCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }

  public static String serializeEditConfigurationsResponse(List<String> authorizedReads) {
    JsonObject successResponse = new JsonObject();
    JsonObject editablesAndMandatories = new JsonObject();
    Gson gson = new Gson();
    editablesAndMandatories.add("authorizedReads",
        gson.toJsonTree(authorizedReads).getAsJsonArray());

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);

    successResponse.add("configuration", editablesAndMandatories);

    return successResponse.toString();
  }

  public static String serializeEditConfigurationsResponse(String[] mandatories,
      List<String> authorizedReads) {
    JsonObject successResponse = new JsonObject();
    JsonObject mandatoriesObject = new JsonObject();
    Gson gson = new Gson();
    mandatoriesObject.add("mandatories", gson.toJsonTree(mandatories).getAsJsonArray());
    mandatoriesObject.add("authorizedReads", gson.toJsonTree(authorizedReads).getAsJsonArray());

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);

    successResponse.add("configuration", mandatoriesObject);

    return successResponse.toString();
  }

  public static String getFailureResponse(ValidationResult validationResult) {
    return getResponse(validationResult, RESPONSE_STATUS, FAIL_RESPONSE_KEY, CODE_RESPONSE_KEY,
        MISSING_CORRECT_DATA_ERROR_CODE);
  }

  public static String getFailureResponse(ValidationResult validationResult, String errorCode) {
    return getResponse(validationResult, RESPONSE_STATUS, FAIL_RESPONSE_KEY, CODE_RESPONSE_KEY,
        errorCode);
  }

  public static String getResponse(ValidationResult validationResult, String response_status,
      String response_key, String code_response_key, String error_code) {
    JsonParser parser = new JsonParser();
    JsonObject response = (JsonObject) parser.parse(new Gson().toJson(validationResult));
    response.addProperty(response_status, response_key);
    response.addProperty(code_response_key, error_code);
    return response.toString();
  }

  @Deprecated
  public static void applySchemaValidation(Class<?> valueObjectClass, String requestData)
      throws Exception {
    JsonSchemaValidator schemaValidator = new JsonSchemaValidator()
        .jsonSchemaFactory(new JsonSchemaValidatorFactory(valueObjectClass));
    schemaValidator.validateSchema(requestData);
  }

  public static void applySchemaValidationForString(String valueObjectSchema, String requestData)
      throws Exception {
    try {
      JsonSchemaValidator schemaValidator = new JsonSchemaValidator()
          .jsonSchemaFactory(new JsonSchemaValidatorFactory(valueObjectSchema));
      schemaValidator.validateSchemaForString(requestData);
    } catch (JsonParseException ex) {
      throw new ArgumentViolationSecurityException();
    }
  }

  public static Object deserializeRequestBody(String jsonObject, Class<?> valueObjectClass) {
    Gson gson = new Gson();
    Object serializedValueObject = gson.fromJson(jsonObject, valueObjectClass);
    return serializedValueObject;
  }

  public static String getFailureResponse(String code) {
    JsonObject response = new JsonObject();
    response.addProperty(RESPONSE_STATUS, FAIL_RESPONSE_KEY);
    response.addProperty(CODE_RESPONSE_KEY, code);
    return response.toString();
  }

  public static String getFailureResponseWithMissingFields(Map<String, Object> missingFields,
      String responseCode) {
    JsonParser parser = new JsonParser();
    JsonObject response = new JsonObject();
    response.add(MISSING_FIELDS_KEY, parser.parse(new Gson().toJson(missingFields)));
    response.addProperty(RESPONSE_STATUS, FAIL_RESPONSE_KEY);
    response.addProperty(CODE_RESPONSE_KEY, responseCode);
    return response.toString();
  }

  public static void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }

  public static void checkIfDetailIsNull(Object object, String objectClassName) throws Exception {
    if (object == null) {
      throw new RecordOfInstanceNotExistException(objectClassName);
    }
  }

  public boolean isReadPermissionAllowed(String sysName, String readPermission) {
    boolean isReadPermissionAllowed = false;
    try {
      authorizationManager.authorizeAction(sysName, readPermission);
      isReadPermissionAllowed = true;
    } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      return isReadPermissionAllowed;
    }

    return isReadPermissionAllowed;
  }

  public String serializeEditConfigurationsResponse(Set<String> enabledAttributes,
      String[] mandatories, List<String> authorizedReads) {

    JsonObject successResponse = new JsonObject();
    JsonObject editablesAndMandatories = new JsonObject();
    Gson gson = new Gson();
    editablesAndMandatories.add("editables", gson.toJsonTree(enabledAttributes).getAsJsonArray());
    editablesAndMandatories.add("mandatories", gson.toJsonTree(mandatories).getAsJsonArray());
    editablesAndMandatories.add("authorizedReads",
        gson.toJsonTree(authorizedReads).getAsJsonArray());

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);

    successResponse.add("configuration", editablesAndMandatories);

    return successResponse.toString();
  }

  public static String serializeEditConfigurationsResponse(SectionEditConfig editConfiguration) {

    JsonObject successResponse = new JsonObject();
    JsonObject editablesAndMandatories = new JsonObject();
    Gson gson = new Gson();
    editablesAndMandatories.add("editables",
        gson.toJsonTree(editConfiguration.getEnabledAttributes()).getAsJsonArray());
    editablesAndMandatories.add("mandatories",
        gson.toJsonTree(editConfiguration.getMandatories()).getAsJsonArray());
    editablesAndMandatories.add("authorizedReads",
        gson.toJsonTree(editConfiguration.getAuthorizedReads()).getAsJsonArray());

    successResponse.addProperty(CODE_RESPONSE_KEY, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);

    successResponse.add("configuration", editablesAndMandatories);

    return successResponse.toString();
  }

  public void checkAuthorizeForActionOnBusinessObject(BusinessObject businessObject,
      String actionName) throws Exception {
    try {
      checkIfEntityIsNull(businessObject);
      authorizationManager.authorizeAction(businessObject, actionName);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
  }

  public void applyAuthorizationForValueObjectAuthorizedReads(AuthorizedPermissionsConfig createPermissionsConfig) {
    for (AuthorizedPermissionConfig config : createPermissionsConfig.getPermissionsConfig()) {
      authorizationManager.authorizeAction(
          config.getEntitySysName(), config.getPermissionToCheck());
    }
  }

  public List<String> getAuthorizedReads(AuthorizedPermissionsConfig authorizedPermissionsConfig) {
    List<AuthorizedPermissionConfig> permissionsConfig =
        authorizedPermissionsConfig.getPermissionsConfig();
    List<String> authorizedReads = new ArrayList<>();
    for (AuthorizedPermissionConfig config : permissionsConfig) {
      try {
        authorizationManager.authorizeAction(config.getEntitySysName(),
            config.getPermissionToCheck());
        authorizedReads.add(config.getPermissionToReturn());
      } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      }
    }
    return authorizedReads;
  }

  public String getSuccessCreationResponse(String userCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, CREATION_SUCCESSFUL_CODE);
    return successResponse.toString();
  }

  public static void checkIfActionIsAllowedInCurrentState(AbstractStateMachine stateMachine,
      StatefullBusinessObject object, String actionName) throws Exception {
    stateMachine.initObjectState(object);
    stateMachine.isAllowedAction(actionName);
  }

  public void checkIfDeleteDetailActionIsAllowedInCurrentState(AbstractStateMachine stateMachine,
      StatefullBusinessObject object, String actionName) throws Exception {
    stateMachine.initObjectState(object);
    try {
      stateMachine.isAllowedAction(actionName);
    } catch (ActionNotAllowedPerStateException exception) {
      throw new RecordDeleteNotAllowedPerStateException();
    }
  }

  public String getSuccessDeletionResponse() {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(CODE_RESPONSE_KEY, DELETION_SUCCESSFUL_MESSAGE_CODE);
    return successResponse.toString();
  }

  public Gson getGson() {
    return initGsonBuilder().create();
  }

  private GsonBuilder initGsonBuilder() {
    GsonBuilder gsonBuilder = new GsonBuilder();

    gsonBuilder.setDateFormat(DateFormats.DATE_TIME_WITH_TIMEZONE);
    gsonBuilder.registerTypeAdapter(DateTime.class, new JodaTimeJsonAdapter());
    gsonBuilder.registerTypeAdapter(BigDecimal.class, new BigDecimalAdapter());
    gsonBuilder.enableComplexMapKeySerialization().setPrettyPrinting();

    return gsonBuilder;
  }

}
