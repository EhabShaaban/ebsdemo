package com.ebs.dda.rest.accounting.landedcost;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.landedcost.editability.LandedCostDetailsEditabilityManger;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;

import java.util.Set;

public class DObLandedCostCommandControllerUtils extends CommonControllerUtils {

  private EntityLockCommand<DObLandedCost> entityLockCommand;
  private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
  private LandedCostDetailsEditabilityManger landedCostDetailsEditabilityManger;

  public DObLandedCostCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public void checkIfActionIsAllowedInCurrentState(
      StatefullBusinessObject landedCost, String actionName) throws Exception {
    DObLandedCostStateMachine landedCostStateMachine = new DObLandedCostStateMachine();
    landedCostStateMachine.initObjectState(landedCost);
    landedCostStateMachine.isAllowedAction(actionName);
  }

  public void checkIfDeleteIsAllowedInCurrentState(
      DObLandedCostGeneralModel dObLandedCostGeneralModel, String actionName) throws Exception {
    DObLandedCostStateMachine landedCostStateMachine = new DObLandedCostStateMachine();
    landedCostStateMachine.initObjectState(dObLandedCostGeneralModel);
    try {
      landedCostStateMachine.isAllowedAction(actionName);
    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException(e);
    }
  }

  public void checkIfEditIsAllowedInCurrentState(
      DObLandedCostGeneralModel landedCostGeneralModel, String actionName) throws Exception {
    DObLandedCostStateMachine landedCostStateMachine = new DObLandedCostStateMachine();
    landedCostStateMachine.initObjectState(landedCostGeneralModel);
    try {
      landedCostStateMachine.isAllowedAction(actionName);
    } catch (ActionNotAllowedPerStateException e) {
      throw new EditNotAllowedPerStateException(e);
    }
  }

  public void lockLandedCostSectionForEdit(
      DObLandedCostGeneralModel landedCostGeneralModel, String sectionName, String actionName)
      throws Exception {
    entityLockCommand.lockSection(landedCostGeneralModel.getUserCode(), sectionName);
    try {
      checkIfEditIsAllowedInCurrentState(landedCostGeneralModel, actionName);
    } catch (EditNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(landedCostGeneralModel.getUserCode(), sectionName);
      throw e;
    }
  }

  protected Set<String> getEnableAttributes(DObLandedCostGeneralModel landedCostGeneralModel) {
    return landedCostDetailsEditabilityManger
        .getEnabledAttributesConfig()
        .get(landedCostGeneralModel.getCurrentState());
  }

  protected AuthorizedPermissionsConfig getEditAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    return permissionsConfig;
  }

  public void setEntityLockCommand(EntityLockCommand<DObLandedCost> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setLandedCostDetailsEditabilityManger(
      LandedCostDetailsEditabilityManger landedCostDetailsEditabilityManger) {
    this.landedCostDetailsEditabilityManger = landedCostDetailsEditabilityManger;
  }

  public void checkIfEntityIsNull(DObLandedCostGeneralModel landedCostGeneralModel)
      throws InstanceNotExistException {
    if (landedCostGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }

  public void checkIfEntityIsNull(String landedCostCode) throws InstanceNotExistException {
    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    if (landedCostGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }

  public DObLandedCostGeneralModel getLandedCostInstance(String landedCostCode)
      throws InstanceNotExistException {
    DObLandedCostGeneralModel landedCost =
        landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
    if (landedCost == null) {
      throw new InstanceNotExistException();
    }
    return landedCost;
  }

  public void setLandedCostGeneralModelRep(DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }
}
