package com.ebs.dda.rest.inventory.initialstockupload;

import com.ebs.dac.dbo.events.initialstockupload.InitialStockUploadActivateEvent;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObInitialStockUploadAuthorizationManager;
import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.initialstockupload.DObInitialStockUploadCreateCommand;
import com.ebs.dda.commands.inventory.inventorydocument.DObInventoryDocumentActivateCommand;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUploadCreateValueObject;
import com.ebs.dda.jpa.inventory.initialstockupload.DObInitialStockUploadGeneralModel;
import com.ebs.dda.jpa.inventory.initialstockupload.IDObInitialStockUpload;
import com.ebs.dda.jpa.inventory.initialstockupload.IObInitialStockUploadItemQuantityGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;
import com.ebs.dda.jpa.inventory.stockavailabilities.StockAvailabilityCodeGenerationValueObject;
import com.ebs.dda.repositories.inventory.initialstockupload.DObInitialStockUploadGeneralModelRep;
import com.ebs.dda.repositories.inventory.initialstockupload.IObInitialStockUploadItemQuantityGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.google.gson.Gson;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "command/initialstockupload")
public class DObInitialStockUploadCommandController implements InitializingBean {

  private CommonControllerUtils commonControllerUtils;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObInitialStockUploadCreateCommand initialstockuploadCreateCommand;

  @Autowired
  private DObInitialStockUploadAuthorizationManager initialStockUploadAuthorizationManager;

  @Autowired private DObInventoryDocumentActivateCommand dObInventoryDocumentActivateCommand;
  @Autowired private DObInitialStockUploadGeneralModelRep initialStockUploadGeneralModelRep;
  @Autowired private IObInitialStockUploadItemQuantityGeneralModelRep initialStockUploadItemQuantitiesGeneralModelRep;
  @Autowired private StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator;
  @Autowired
  @Qualifier("stockAvailabilityLockCommand")
  private EntityLockCommand<CObStockAvailability> stockAvailabilityEntityLockCommand;

  @Autowired private ObservablesEventBus observablesEventBus;
  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }

  @PostMapping(value = "/")
  public String createInitialStockUpload(
      @RequestParam("file") MultipartFile file, @RequestParam("valueObject") String valueObject)
      throws Exception {

    DObInitialStockUploadCreateValueObject initialStockUploadCreateValueObject =
        new Gson().fromJson(valueObject, DObInitialStockUploadCreateValueObject.class);
    initialStockUploadCreateValueObject.setAttachment(file);

    initialStockUploadAuthorizationManager.applyAuthorizationOnCreateAction(
        initialStockUploadCreateValueObject);

    Observable<String> codeObservable =
        initialstockuploadCreateCommand.executeCommand(initialStockUploadCreateValueObject);
    final StringBuilder userCode = new StringBuilder();
    codeObservable.subscribe(userCode::append);
    return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{initialStockUploadCode}/activate")
  public @ResponseBody String activateInitialStockUpload(
      @PathVariable String initialStockUploadCode) throws Exception {
    DObInitialStockUploadGeneralModel initialStockUploadGeneralModel =
        initialStockUploadGeneralModelRep.findOneByUserCode(initialStockUploadCode);
    List<IObInitialStockUploadItemQuantityGeneralModel> initialStockUploadItems =
        initialStockUploadItemQuantitiesGeneralModelRep.findAllByUserCode(
            initialStockUploadCode);
    List<String> refDocumentCodes = lockRefDocument(initialStockUploadItems);
    DObInventoryDocumentActivateValueObject inventoryDocumentActivateValueObject =
        getInventoryDocumentActivateValueObject(initialStockUploadCode);
    try {
      dObInventoryDocumentActivateCommand.executeCommand(inventoryDocumentActivateValueObject);
      consumeInitialStockUploadActivateEvent(
          initialStockUploadCode, initialStockUploadGeneralModel.getId());
    } finally {
      for (String refDocumentCode : refDocumentCodes) {
        stockAvailabilityEntityLockCommand.unlockAllSections(refDocumentCode);
      }
    }
    return CommonControllerUtils.getSuccessResponse(
        IInitialStockUploadResponseMessages.SUCCESSFUL_ACTIVATE);
  }

  private List<String> lockRefDocument(List<IObInitialStockUploadItemQuantityGeneralModel> items)
          throws Exception {
    List<String> stockAvailabilityCodes = new ArrayList<String>();
    for (IObInitialStockUploadItemQuantityGeneralModel item : items) {
      StockAvailabilityCodeGenerationValueObject stockAvailabilityCodeGenerationValueObject =
              new StockAvailabilityCodeGenerationValueObject().fromItemQuantityGeneralModel(item);
      String stockAvailabilityCode =
              stockAvailabilityCodeGenerator.generateUserCode(
                      stockAvailabilityCodeGenerationValueObject);
      stockAvailabilityEntityLockCommand.lockAllSectionsOfNonExistingEntity(stockAvailabilityCode);
      stockAvailabilityCodes.add(stockAvailabilityCode);
    }
    return stockAvailabilityCodes;
  }

  private void consumeInitialStockUploadActivateEvent(String userCode, Long initialStockUploadId) {
    InitialStockUploadActivateEvent event = new InitialStockUploadActivateEvent(userCode, initialStockUploadId);
    observablesEventBus.consumeEvent(event);
  }

  private DObInventoryDocumentActivateValueObject getInventoryDocumentActivateValueObject(
      String initialStockUploadCode) {
    DObInventoryDocumentActivateValueObject inventoryDocumentActivateValueObject =
        new DObInventoryDocumentActivateValueObject();
    inventoryDocumentActivateValueObject.setInventoryType(
        IDObInitialStockUpload.INITIAL_STOCK_UPLOAD_KEY);
    inventoryDocumentActivateValueObject.setUserCode(initialStockUploadCode);
    return inventoryDocumentActivateValueObject;
  }

  @GetMapping("/")
  public @ResponseBody String createInitialStockUploadConfiguration() {
    authorizationManager.authorizeAction(IDObInitialStockUpload.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig =
        initialStockUploadAuthorizationManager.constructCreateConfiguration();
    List<String> authorizedReads =
        commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);

    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }
}
