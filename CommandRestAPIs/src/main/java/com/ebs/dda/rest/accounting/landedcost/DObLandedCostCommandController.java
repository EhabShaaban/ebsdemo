package com.ebs.dda.rest.accounting.landedcost;

import com.ebs.dac.dbo.events.landedCost.LandedCostConvertToActualEvent;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserInSameSessionException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObLandedCostAuthorizationManager;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostActionNames;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostJsonSchema;
import com.ebs.dda.accounting.landedcost.validation.*;
import com.ebs.dda.commands.accounting.landedcost.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.landedcost.*;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.google.gson.Gson;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static com.ebs.dda.rest.CommonControllerUtils.STATE_CHANGED_SUCCESSFULLY;
import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;

@RestController
@RequestMapping(value = "command/landedcost")
public class DObLandedCostCommandController implements InitializingBean {

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private EntityLockCommand<DObLandedCost> entityLockCommand;
  private DObLandedCostCommandControllerUtils utils;
  @Autowired private DObLandedCostAuthorizationManager landedCostAuthorizationManager;
  @Autowired private DObLandedCostDeleteCommand landedCostDeleteCommand;
  @Autowired private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
  @Autowired private IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep;
  @Autowired private DObLandedCostCreationCommand creationCommand;
  @Autowired private IObOrderCompanyGeneralModelRep purhcaseOrderGMRep;
  @Autowired private DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep;
  @Autowired private DObLandedCostConvertToEstimateCommand landedCostConvertToEstimateCommand;
  @Autowired private DObLandedCostConvertToActualCommand landedCostConvertToActualCommand;
  @Autowired private IObLandedCostFactorItemsGeneralModelRep landedCostFactorItemsGeneralModelRep;
  @Autowired private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModel;
  @Autowired
  private ObservablesEventBus observablesEventBus;


  private DObLandedCostCreationValidator creationValidator;
  private DObLandedCostConvertToEstimateValidator convertToEstimateValidator;
  private DObLandedCostConvertToActualValidator convertToActualValidator;
  private CommonControllerUtils commonControllerUtils;

  @RequestMapping(method = RequestMethod.DELETE, value = "/{landedCostCode}")
  public @ResponseBody String deleteLandedCost(@PathVariable String landedCostCode)
      throws Exception {

    DObLandedCostGeneralModel landedCostGeneralModel = utils.getLandedCostInstance(landedCostCode);
    LockDetails lockDetails = null;

    try {
      lockDetails = entityLockCommand.lockAllSections(landedCostCode);

      landedCostAuthorizationManager.authorizeActionOnSection(
          landedCostGeneralModel, IActionsNames.DELETE);
      utils.checkIfDeleteIsAllowedInCurrentState(landedCostGeneralModel, IActionsNames.DELETE);

      landedCostDeleteCommand.executeCommand(landedCostCode);
    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException();
    } catch (ResourceAlreadyLockedBySameUserException ex) {
      throw new ResourceAlreadyLockedBySameUserInSameSessionException();
    } finally {
      if (lockDetails != null) entityLockCommand.unlockAllSections(landedCostCode);
    }
    return CommonControllerUtils.getSuccessResponseWithUserCode(
        landedCostCode, CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{landedCostCode}")
  public @ResponseBody String closeAllSectionsInLandedCost(
      @PathVariable String landedCostCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(landedCostCode, lockedSections);
    return utils.getSuccessResponseWithUserCode(
        landedCostCode, CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @PutMapping("/{landedCostCode}/convert-to-estimate")
  public @ResponseBody String convertToEstimateLandedCost(@PathVariable String landedCostCode)
      throws Exception {

    DObLandedCostGeneralModel landedCost = utils.getLandedCostInstance(landedCostCode);
    landedCostAuthorizationManager.authorizeConvertToEstimateAction(landedCost);
    utils.checkIfActionIsAllowedInCurrentState(
        landedCost, IDObLandedCostActionNames.CONVERT_TO_ESTIMATE);
    entityLockCommand.lockAllSections(landedCostCode);

    String response = null;
    try {

      initConvertToEstimateValidator();
      ValidationResult validationResult =
          convertToEstimateValidator.validateBusinessRules(landedCostCode);

      if (!validationResult.isValid()) {
        response = CommonControllerUtils.getFailureResponse(validationResult);
      } else {
        landedCostConvertToEstimateCommand.executeCommand(landedCostCode);
        response =
            CommonControllerUtils.getSuccessResponseWithUserCode(
                landedCostCode, STATE_CHANGED_SUCCESSFULLY);
      }

    } finally {
      entityLockCommand.unlockAllSections(landedCostCode);
    }

    return response;
  }

  private void initConvertToEstimateValidator() {
    convertToEstimateValidator = new DObLandedCostConvertToEstimateValidator();
    convertToEstimateValidator.setLandedCostFactorItemsGeneralModelRep(
        landedCostFactorItemsGeneralModelRep);
    convertToEstimateValidator.setLandedCostGMRep(landedCostGeneralModelRep);
    convertToEstimateValidator.setPurhcaseOrderGMRep(purhcaseOrderGMRep);
  }

  private void initConvertToActualValidator() {
    convertToActualValidator = new DObLandedCostConvertToActualValidator();
    convertToActualValidator.setLandedCostGeneralModelMRep(landedCostGeneralModelRep);
    convertToActualValidator.setLandedCostDetailsGeneralModelRep(landedCostDetailsGeneralModelRep);
    convertToActualValidator.setVendorInvoiceGeneralModelRep(vendorInvoiceGeneralModel);
  }

  @PutMapping("/{landedCostCode}/convert-to-actual")
  public @ResponseBody String convertToActualLandedCost(@PathVariable String landedCostCode)
      throws Exception {

    DObLandedCostGeneralModel landedCost = utils.getLandedCostInstance(landedCostCode);
    landedCostAuthorizationManager.authorizeConvertToActualAction(landedCost);
    utils.checkIfActionIsAllowedInCurrentState(
        landedCost, IDObLandedCostActionNames.CONVERT_TO_ACTUAL);
    entityLockCommand.lockAllSections(landedCostCode);

    try {
      initConvertToActualValidator();
      ValidationResult validationResult = convertToActualValidator.validate(landedCostCode);
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      }
      landedCostConvertToActualCommand.executeCommand(landedCostCode);
      consumeLandedCostPostEvent(landedCostCode);
    } finally {
      entityLockCommand.unlockAllSections(landedCostCode);
    }
    return CommonControllerUtils.getSuccessResponseWithUserCode(
        landedCostCode, STATE_CHANGED_SUCCESSFULLY);
  }
  private void consumeLandedCostPostEvent(String userCode) {
    LandedCostConvertToActualEvent event = new LandedCostConvertToActualEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }

  @PostMapping
  public @ResponseBody String createLandedCost(@RequestBody String requestBody) throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
        IDObLandedCostJsonSchema.CREATION_SCHEMA, requestBody);

    DObLandedCostCreationValueObject valueObject =
        new Gson().fromJson(requestBody, DObLandedCostCreationValueObject.class);

    initCreationValidator();
    creationValidator.checkMandatories(valueObject);

    landedCostAuthorizationManager.applyAuthorizationForCreation(valueObject);

    ValidationResult validationResult = creationValidator.validateBusinessRules(valueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }

    final StringBuilder userCode = new StringBuilder();
    Observable<String> codeObservable = creationCommand.executeCommand(valueObject);
    codeObservable.subscribe(
        (code) -> {
          userCode.append(code);
        });

    return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  private void initCreationValidator() {
    creationValidator = new DObLandedCostCreationValidator();
    creationValidator.setPurhcaseOrderGMRep(purhcaseOrderGMRep);
    creationValidator.setLandedCostGMRep(landedCostGeneralModelRep);
    creationValidator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
  }

  @GetMapping
  public @ResponseBody String getCreateConfiguration() {
    authorizationManager.authorizeAction(IDObLandedCost.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();

    List<String> authorizedReads =
        commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

    return CommonControllerUtils.serializeEditConfigurationsResponse(
        new ArrayList<String>(new HashSet<>(authorizedReads)));
  }

  private AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
        ICObLandedCost.SYS_NAME, IActionsNames.READ_ALL, "ReadLandedCostTypes");
    configuration.addPermissionConfig(
        IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadPurchaseOrders");
  configuration.addPermissionConfig(
        IDObLandedCost.SYS_NAME, IActionsNames.READ_ALL, "ReadDocumentOwners");

    return configuration;
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    utils =
        new DObLandedCostCommandControllerUtils(
            landedCostAuthorizationManager.getAuthorizationManager());
    utils.setEntityLockCommand(entityLockCommand);
    utils.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
  }
}
