package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dda.commands.accounting.notesreceivables.IObNotesReceivableRefDocumentUpdateRemainingCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableUpdateRefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UpdateNotesReceivableReferenceDocumentsRemaining
    implements Observer<ExecuteActivateDependanciesContext> {

  private IObNotesReceivableRefDocumentUpdateRemainingCommand updateRemainingCommand;

  public UpdateNotesReceivableReferenceDocumentsRemaining(
      IObNotesReceivableRefDocumentUpdateRemainingCommand updateRemainingCommand) {
    this.updateRemainingCommand = updateRemainingCommand;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      updateRemainingCommand.executeCommand(
          initValueObject(context.entitiesOfInterestContainer().getMainEntity()));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObNotesReceivableUpdateRefDocumentRemainingValueObject initValueObject(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {

    DObNotesReceivableUpdateRefDocumentRemainingValueObject valueObject =
        new DObNotesReceivableUpdateRefDocumentRemainingValueObject();

    valueObject.setNotesReceivableCode(notesReceivablesGeneralModel.getUserCode());

    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
