package com.ebs.dda.rest.order.purchaseorder.paymentanddelivery;

import static com.ebs.dda.rest.order.purchaseorder.paymentanddelivery.IObPaymentAndDeliveryCommandControllerUtils.applySchemaValidation;
import static com.ebs.dda.rest.order.purchaseorder.paymentanddelivery.IObPaymentAndDeliveryCommandControllerUtils.deserializeRequestBody;
import static com.ebs.dda.rest.order.purchaseorder.paymentanddelivery.IObPaymentAndDeliveryCommandControllerUtils.getFailureResponse;
import static com.ebs.dda.rest.order.purchaseorder.paymentanddelivery.IObPaymentAndDeliveryCommandControllerUtils.getSuccessResponse;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderEditPaymentAndDeliveryCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderPaymentAndDeliveryValueObject;
import com.ebs.dda.purchases.editability.DObPurchasePaymentAndDeliverySectionEditabilityManager;
import com.ebs.dda.purchases.validation.mandatoryvalidator.DObPurchaseOrderPaymentAndDeliveryMandatoryValidator;
import com.ebs.dda.purchases.validation.validator.DObPurchaseOrderPaymentAndDeliveryValidator;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.services.authorization.DObPurchaseOrderAuthorizationService;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/purchaseorder")
public class IObPaymentAndDeliveryCommandController implements InitializingBean {

  @Autowired
  DObPurchasePaymentAndDeliverySectionEditabilityManager
      paymentAndDeliverySectionEditabilityManager;

  @Autowired
  DObPurchaseOrderPaymentAndDeliveryMandatoryValidator
      paymentAndDeliveryValueObjectMandatoryValidator;

  @Autowired
  DObPurchaseOrderPaymentAndDeliveryValidator dObPurchaseOrderPaymentAndDeliveryValidator;

  @Autowired
  private DObPurchaseOrderEditPaymentAndDeliveryCommand purchaseOrderEditPaymentAndDeliveryCommand;

  private IObPaymentAndDeliveryCommandControllerUtils paymentAndDeliveryCommandControllerUtils;

  @Autowired private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  @Autowired private EntityLockCommand<DObOrderDocument> entityLockCommand;

  @Autowired private AuthorizationManager authorizationManager;

  private DObPurchaseOrderAuthorizationService purchaseOrderAuthorizationService;

  @Override
  public void afterPropertiesSet() {
    paymentAndDeliveryCommandControllerUtils =
        new IObPaymentAndDeliveryCommandControllerUtils(authorizationManager);
    paymentAndDeliveryCommandControllerUtils.setEntityLockCommand(entityLockCommand);
    paymentAndDeliveryCommandControllerUtils.setOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    purchaseOrderAuthorizationService = new DObPurchaseOrderAuthorizationService();
    purchaseOrderAuthorizationService.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    purchaseOrderAuthorizationService.setAuthorizationManager(authorizationManager);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/lock/paymentanddeliverysection/{purchaseOrderCode}")
  public @ResponseBody String openPaymentAndDeliveryInPurchaseOrder(
      @PathVariable String purchaseOrderCode) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    paymentAndDeliveryCommandControllerUtils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);

    paymentAndDeliveryCommandControllerUtils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION,
        IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);

    DObPurchaseOrderGeneralModel order =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    Set<String> enabledAttributes =
        paymentAndDeliverySectionEditabilityManager.getEnabledAttributesInGivenStates(
            order.getCurrentStates());
    return paymentAndDeliveryCommandControllerUtils.getPaymentAndDeliveryEditConfiguration(
        order, enabledAttributes);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/paymentanddeliverysection/{purchaseOrderCode}")
  public @ResponseBody String closePaymentAndDeliverySectionInPurchaseOrder(
      @PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    paymentAndDeliveryCommandControllerUtils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);
    purchaseOrderEditPaymentAndDeliveryCommand.unlockSection(purchaseOrderCode);
    return paymentAndDeliveryCommandControllerUtils.getSuccessResponse(
        CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/savepaymentanddelivery/{purchaseOrderCode}")
  public @ResponseBody String savePaymentAndDelivery(
      @PathVariable String purchaseOrderCode, @RequestBody String paymentAndDeliveryJsonObject)
      throws Exception {
    applySchemaValidation(
        DObPurchaseOrderPaymentAndDeliveryValueObject.class, paymentAndDeliveryJsonObject);
    purchaseOrderAuthorizationService.applyAuthorizationOnSavePaymentAndDelivery(purchaseOrderCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION);
    DObPurchaseOrderPaymentAndDeliveryValueObject paymentAndDeliveryValueObject =
        (DObPurchaseOrderPaymentAndDeliveryValueObject)
            deserializeRequestBody(
                paymentAndDeliveryJsonObject, DObPurchaseOrderPaymentAndDeliveryValueObject.class);
    try {
      paymentAndDeliveryValueObjectMandatoryValidator.validateValueObjectMandatories(
          paymentAndDeliveryValueObject);
      ValidationResult validationResult =
          dObPurchaseOrderPaymentAndDeliveryValidator.validate(paymentAndDeliveryValueObject);
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      }
      DObPurchaseOrderPaymentAndDeliveryValueObject newPaymentAndDeliveryValueObject =
          dObPurchaseOrderPaymentAndDeliveryValidator.getNewPaymentAndDeliveryValueObject();
      purchaseOrderEditPaymentAndDeliveryCommand.executeCommand(newPaymentAndDeliveryValueObject);
    } finally {
      entityLockCommand.unlockSection(
          paymentAndDeliveryValueObject.getPurchaseOrderCode(),
          IPurchaseOrderSectionNames.PAYMENT_DELIVERY_SECTION);
    }
    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }
}
