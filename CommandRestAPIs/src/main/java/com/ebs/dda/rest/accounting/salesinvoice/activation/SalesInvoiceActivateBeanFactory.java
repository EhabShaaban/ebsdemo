package com.ebs.dda.rest.accounting.salesinvoice.activation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.salesinvoice.SalesInvoiceTypeEnum;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SalesInvoiceActivateBeanFactory implements ApplicationContextAware {

  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  public ActivateSalesInvoice getBean(String salesInvoiceType) {
    if (salesInvoiceType.equals(SalesInvoiceTypeEnum.SalesInvoiceType.SALES_INVOICE_FOR_SALES_ORDER.toString())) {
      return applicationContext.getBean("SalesInvoiceSalesOrderActivate", ActivateSalesInvoice.class);
    }
    if (salesInvoiceType.equals(SalesInvoiceTypeEnum.SalesInvoiceType.SALES_INVOICE_WITHOUT_REFERENCE.toString())) {
      return applicationContext.getBean("SalesInvoiceWithoutReferenceActivate", ActivateSalesInvoice.class);
    }
    throw new ArgumentViolationSecurityException();
  }
}
