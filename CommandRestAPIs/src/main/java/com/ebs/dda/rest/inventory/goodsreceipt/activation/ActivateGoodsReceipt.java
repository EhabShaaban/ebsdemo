package com.ebs.dda.rest.inventory.goodsreceipt.activation;

public interface ActivateGoodsReceipt {
  String apply(GoodsReceiptActivateContextBuilder.GoodsReceiptActivateContext context)
      throws Exception;
}
