package com.ebs.dda.rest.accounting.vendorinvoice;

import com.ebs.dac.dbo.events.vendorinvoice.VendorInvoiceCreateSuccessEvent;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserInSameSessionException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObInvoiceAuthorizationManager;
import com.ebs.dda.accounting.vendorinvoice.apis.IDOVendorInvoiceSchema;
import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceCreateCommand;
import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceCreateCommandFactory;
import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.*;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.ebs.dda.validation.vendorinvoice.DObVendorInvoiceCreationValidator;
import com.google.gson.Gson;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "command/accounting/vendorinvoice")
public class DObVendorInvoiceCommandController implements InitializingBean {

  private CommonControllerUtils commonControllerUtils;
  private DObVendorInvoiceCommandControllerUtils dObInvoiceCommandControllerUtils;
  @Autowired private DObVendorInvoiceCreateCommandFactory createCommandFactory;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObInvoiceAuthorizationManager invoiceAuthorizationManager;
  @Autowired private DObVendorInvoiceCreationValidator creationValidator;
  @Autowired private EntityLockCommand<DObVendorInvoice> entityLockCommand;
  @Autowired private DObVendorInvoiceGeneralModelRep invoiceGeneralModelRep;
  @Autowired
  private ObservablesEventBus observablesEventBus;
  @Autowired private DObVendorInvoiceDeleteCommand deleteCommand;

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createInvoice(@RequestBody String invoiceData) throws Exception {
    CommonControllerUtils.applySchemaValidationForString(
            IDOVendorInvoiceSchema.CREATION_SCHEMA, invoiceData);

    DObVendorInvoiceCreationValueObject invoiceCreationValueObject =
        new Gson().fromJson(invoiceData, DObVendorInvoiceCreationValueObject.class);
    invoiceAuthorizationManager.applyAuthorizationForCreate(invoiceCreationValueObject);
    ValidationResult validationResult = creationValidator.validate(invoiceCreationValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    }
    final StringBuilder userCode = new StringBuilder();
    DObVendorInvoiceCreateCommand dObInvoiceCreateCommand = createCommandFactory.getVendorInvoiceCreateCommandInstance(invoiceCreationValueObject.getInvoiceType());
    Observable<DObVendorInvoice> vendorInvoiceObservable =
            dObInvoiceCreateCommand.executeCommand(invoiceCreationValueObject);
    vendorInvoiceObservable.subscribe(
        (vendorInvoice) -> {
          userCode.append(vendorInvoice.getUserCode());
        });
    vendorInvoiceObservable.subscribe(this::consumeVendorInvoiceCreateEvent);
    return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  private void consumeVendorInvoiceCreateEvent(DObVendorInvoice vendorInvoice) {
    VendorInvoiceCreateSuccessEvent event = new VendorInvoiceCreateSuccessEvent(vendorInvoice);
    observablesEventBus.consumeEvent(event);
  }
  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String getCreateConfiguration() {
    applyAuthorizationForInvoiceCreate();
    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();
    List<String> authorizedReads =
            commonControllerUtils.getAuthorizedReads(createPermissionsConfig);
    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return commonControllerUtils.serializeEditConfigurationsResponse(createConfig);
  }

  private AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(IMObVendor.SYS_NAME, IActionsNames.READ_ALL, "ReadVendors");
    configuration.addPermissionConfig(
            IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadImportPurchaseOrders");
    configuration.addPermissionConfig(
            IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadLocalPurchaseOrders");
    configuration.addPermissionConfig(
            IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL, "ReadDocumentOwners");
    return configuration;
  }

  private void applyAuthorizationForInvoiceCreate() {
    authorizationManager.authorizeAction(IDObVendorInvoice.SYS_NAME, IActionsNames.CREATE);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{vendorInvoiceCode}")
  public @ResponseBody String deleteVendorInvoice(@PathVariable String vendorInvoiceCode)
      throws Exception {

    DObVendorInvoiceGeneralModel invoiceGeneralModel =
        invoiceGeneralModelRep.findOneByUserCode(vendorInvoiceCode);
    LockDetails lockDetails = null;

    try {
      lockDetails = entityLockCommand.lockAllSections(vendorInvoiceCode);

      invoiceAuthorizationManager.authorizeActionOnSection(
          invoiceGeneralModel, IActionsNames.DELETE);
      dObInvoiceCommandControllerUtils.checkIfDeleteIsAllowedInCurrentState(
          invoiceGeneralModel, IActionsNames.DELETE);

      deleteCommand.executeCommand(vendorInvoiceCode);
    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException();
    } catch (ResourceAlreadyLockedBySameUserException ex) {
      throw new ResourceAlreadyLockedBySameUserInSameSessionException();
    } finally {
      if (lockDetails != null) entityLockCommand.unlockAllSections(vendorInvoiceCode);
    }
    return CommonControllerUtils.getSuccessResponseWithUserCode(
        vendorInvoiceCode, CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    dObInvoiceCommandControllerUtils = new DObVendorInvoiceCommandControllerUtils();
  }
}
