package com.ebs.dda.rest.masterdata.itemgroup;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.itemgroup.CObItemGroup;
import com.ebs.dda.rest.masterdata.utils.MasterDataControllerUtils;

public class ItemGroupCommandControllerUtils extends MasterDataControllerUtils {
    public ItemGroupCommandControllerUtils(AuthorizationManager authorizationManager) {
        super(authorizationManager);
    }

    public void checkIfItemGroupExists(CObItemGroup itemGroup) throws Exception {
        if (itemGroup == null) {
          throw new InstanceNotExistException();
        }
    }
}
