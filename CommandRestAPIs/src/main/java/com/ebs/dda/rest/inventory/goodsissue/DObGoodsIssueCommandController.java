package com.ebs.dda.rest.inventory.goodsissue;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObGoodsIssueAuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.goodsissue.DObGoodsIssueCreateCommand;
import com.ebs.dda.commands.inventory.goodsissue.DObGoodsIssueDeleteCommand;
import com.ebs.dda.commands.inventory.goodsissue.DObGoodsIssuePdfGeneratorCommand;
import com.ebs.dda.inventory.goodsissue.validation.DObGoodsIssueCreateValidator;
import com.ebs.dda.jpa.inventory.goodsissue.*;
import com.ebs.dda.jpa.inventory.goodsreceipt.ICObStorekeeper;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ebs.dda.rest.CommonControllerUtils.*;

@RestController
@RequestMapping(value = "command/goodsissue")
public class DObGoodsIssueCommandController implements InitializingBean {

  @Autowired
  DObGoodsIssuePdfGeneratorCommand goodsIssuePdfGeneratorCommand;
  private CommonControllerUtils commonControllerUtils;
  @Autowired
  private DObGoodsIssueAuthorizationManager goodsIssueAuthorizationManager;
  @Autowired
  private AuthorizationManager authorizationManager;
  @Autowired
  private DObGoodsIssueCreateCommand goodsIssueCreateCommand;
  @Autowired
  private DObGoodsIssueCreateValidator validator;
  @Autowired
  private DObGoodsIssueGeneralModelRep goodsIssueGeneralModelRep;
  @Autowired
  private DObGoodsIssueDeleteCommand goodsIssueDeleteCommand;
  @Autowired
  private EntityLockCommand<DObGoodsIssue> entityLockCommand;


  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody
  String getCreateConfiguration() {
    authorizationManager.authorizeAction(IDObGoodsIssue.SYS_NAME, IActionsNames.CREATE);
    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();
    List<String> authorizedReads =
            commonControllerUtils.getAuthorizedReads(createPermissionsConfig);
    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return commonControllerUtils.serializeEditConfigurationsResponse(createConfig);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  public @ResponseBody String createGoodsIssue(@RequestBody String goodsIssueData)
      throws Exception {
    CommonControllerUtils.applySchemaValidation(
            DObGoodsIssueCreateValueObject.class, goodsIssueData);

    authorizationManager.authorizeAction(IDObGoodsIssue.SYS_NAME, IActionsNames.CREATE);
    authorizeActionOnReadingDropdowns();

    DObGoodsIssueCreateValueObject dObGoodsIssueCreateValueObject =
            (DObGoodsIssueCreateValueObject)
                    CommonControllerUtils.deserializeRequestBody(
                            goodsIssueData, DObGoodsIssueCreateValueObject.class);
    goodsIssueAuthorizationManager.checkCreateValueObjectValuesAuthorization(
            dObGoodsIssueCreateValueObject);

    ValidationResult validationResult = validator.validate(dObGoodsIssueCreateValueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }
    final StringBuilder userCode = new StringBuilder();
    goodsIssueCreateCommand
            .executeCommand(dObGoodsIssueCreateValueObject)
            .subscribe(code -> userCode.append(code));
    return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{goodsIssueCode}")
  public @ResponseBody
  String deleteGoodsIssue(@PathVariable String goodsIssueCode)
          throws Exception {

    goodsIssueAuthorizationManager.authorizeActionOnSection(
            getGoodsIssueGeneralModel(goodsIssueCode), IActionsNames.DELETE);
    goodsIssueDeleteCommand.executeCommand(goodsIssueCode);
    return commonControllerUtils.getSuccessDeletionResponse();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{goodsIssueCode}/generatepdf")
  public @ResponseBody String generateGoodsIssuePdf(@PathVariable String goodsIssueCode)
          throws Exception {
    entityLockCommand.lockAllSections(goodsIssueCode);
    StringBuilder pdfStringBuilder = new StringBuilder();
    goodsIssuePdfGeneratorCommand
            .executeCommand(goodsIssueCode)
            .subscribe(commandPdf -> pdfStringBuilder.append(commandPdf));
    entityLockCommand.unlockAllSections(goodsIssueCode);
    return getSuccessResponse(pdfStringBuilder.toString());
  }

  private DObGoodsIssueGeneralModel getGoodsIssueGeneralModel(String goodsIssueCode)
          throws Exception {
    DObGoodsIssueGeneralModel goodsIssueGeneralModel =
            goodsIssueGeneralModelRep.findOneByUserCode(goodsIssueCode);
    checkIfEntityIsNull(goodsIssueGeneralModel);
    return goodsIssueGeneralModel;
  }

  private AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
            ICObGoodsIssue.SYS_NAME, IActionsNames.READ_ALL, "ReadGITypes");
    configuration.addPermissionConfig(
            ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL, "ReadBusinessUnits");
    configuration.addPermissionConfig(
            IMObCustomer.SYS_NAME, IActionsNames.READ_ALL, "ReadCustomers");
    configuration.addPermissionConfig(
            IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadSalesOrders");
    configuration.addPermissionConfig(
            ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL, "ReadStorehouses");
    configuration.addPermissionConfig(
            ICObStorekeeper.SYS_NAME, IActionsNames.READ_ALL, "ReadStorekeepers");
    configuration.addPermissionConfig(
            ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadCompanies");
    return configuration;
  }

  private void authorizeActionOnReadingDropdowns() {
    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();
    for (AuthorizedPermissionsConfig.AuthorizedPermissionConfig config :
            createPermissionsConfig.getPermissionsConfig()) {
      try {
        authorizationManager.authorizeAction(
                config.getEntitySysName(), config.getPermissionToCheck());
      } catch (AuthorizationException | ConditionalAuthorizationException exception) {
        throw new AuthorizationException(exception);
      }
    }
  }
}
