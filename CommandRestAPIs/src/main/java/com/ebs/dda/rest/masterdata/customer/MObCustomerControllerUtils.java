package com.ebs.dda.rest.masterdata.customer;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerCreateValueObject;
import com.ebs.dda.masterdata.customer.ICustomerActionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;

import java.util.List;

public class MObCustomerControllerUtils extends CommonControllerUtils {

  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;

  public MObCustomerControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();

    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL, "ReadBusinessUnit");
    configuration.addPermissionConfig(
        ICObCurrency.SYS_NAME, IActionsNames.READ_ALL, "ReadCurrency");

    configuration.addPermissionConfig(
        IMObCustomer.SYS_NAME, ICustomerActionNames.READ_ISSUE_FROM, "ReadIssuedFrom");

    configuration.addPermissionConfig(
        IMObCustomer.SYS_NAME,
        ICustomerActionNames.READ_TAX_ADMINISTRATION,
        "ReadTaxAdministration");
    return configuration;
  }

  public void applyAuthorizationForCreateAction() {
    authorizationManager.authorizeAction(IMObCustomer.SYS_NAME, IActionsNames.CREATE);
  }

  public void applyAuthorizationForValueObjectValues(
      MObCustomerCreateValueObject customerCreateValueObject) throws Exception {
    applyAuthorizationForBusinessUnits(customerCreateValueObject.getBusinessUnitCodes());
    authorizeCurrency();
    authorizeCustomerIssuedFrom();
    authorizeCustomerTaxAdministration();
  }

  private void applyAuthorizationForBusinessUnits(List<String> businessUnitCodes) throws Exception {
    for (String businessUnitCode : businessUnitCodes) {
      applyAuthorizationForBusinessUnit(businessUnitCode);
    }
  }

  private void applyAuthorizationForBusinessUnit(String businessUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(businessUnitCode);
    if (purchasingUnitGeneralModel != null) {
      authorizationManager.authorizeActionOnObject(
          purchasingUnitGeneralModel, IActionsNames.READ_ALL);
    }
  }

  private void authorizeCurrency() {
    authorizationManager.authorizeAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
  }

  private void authorizeCustomerIssuedFrom() {
    authorizationManager.authorizeAction(
        IMObCustomer.SYS_NAME, ICustomerActionNames.READ_ISSUE_FROM);
  }

  private void authorizeCustomerTaxAdministration() {
    authorizationManager.authorizeAction(
        IMObCustomer.SYS_NAME, ICustomerActionNames.READ_TAX_ADMINISTRATION);
  }

  public void authorizeActionOnCustomer(
          List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnitsList, String actionName) {
    boolean isAuthorized = false;
    Exception exception = null;
    for (IObCustomerBusinessUnitGeneralModel customerBusinessUnit : customerBusinessUnitsList) {
      try {
        authorizationManager.authorizeAction(customerBusinessUnit, actionName);
        isAuthorized = true;
        break;
      } catch (Exception e) {
        exception = e;
      }
    }
    if (!isAuthorized) {
      throw new AuthorizationException(exception);
    }
  }


  public void setPurchasingUnitGeneralModelRep(
          CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }
}
