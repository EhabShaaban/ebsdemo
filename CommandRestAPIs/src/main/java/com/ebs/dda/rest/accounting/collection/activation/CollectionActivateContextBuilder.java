package com.ebs.dda.rest.accounting.collection.activation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.accounting.collection.apis.IDObCollectionJsonSchema;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.functions.validate.ValidateSchemaContext;
import com.ebs.dda.jpa.accounting.collection.DObCollectionActivateValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IDObCollection;

public class CollectionActivateContextBuilder {

  private CollectionActivateContextBuilder() {}

  public static CollectionActivateContext newContext() {
    return new CollectionActivateContext();
  }

  public static class CollectionActivateContext
      implements ActivateContext,
          AuthorizeContext,
          FindEntityContext,
          ValidateSchemaContext,
          LockContext,
          DeserializeValueObjectContext<DObCollectionActivateValueObject>,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<DObCollectionActivateValueObject>,
          ExecuteCommandContext<DObCollectionActivateValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext {

    private String objectCode;
    private String activateData;
    private DObCollectionActivateValueObject valueObject;
    private String response;
    private String journalEntryUserCode;
    private EntitiesOfInterestContainer entitiesOfInstrestContainer;

    private CollectionActivateContext() {
      this.entitiesOfInstrestContainer =
          new EntitiesOfInterestContainer(DObCollectionGeneralModel.class);
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public String activateData() {
      return activateData;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return IDObCollection.SYS_NAME;
    }

    @Override
    public String actionName() {
      return IActionsNames.ACTIVATE;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      this.journalEntryUserCode = userCode;
    }

    @Override
    public String state() {
      return BasicStateMachine.ACTIVE_STATE;
    }

    public CollectionActivateContext objectCode(String collectionCode) {
      this.objectCode = collectionCode;
      return this;
    }

    public CollectionActivateContext valueObject(String valueObject) {
      this.activateData = valueObject;
      return this;
    }

    @Override
    public String schema() {
      return IDObCollectionJsonSchema.ACTIVATION_SCHEMA;
    }

    @Override
    public Class<DObCollectionActivateValueObject> valueObjectClass() {
      return DObCollectionActivateValueObject.class;
    }

    @Override
    public void valueObject(DObCollectionActivateValueObject valueObject) {
      this.valueObject = valueObject;
    }

    @Override
    public DObCollectionActivateValueObject valueObject() {
      return this.valueObject;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return journalEntryUserCode;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInstrestContainer;
    }
  }
}
