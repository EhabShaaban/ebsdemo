package com.ebs.dda.rest.accounting.costing.activation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dda.accounting.costing.apis.IDObCostingActionNames;
import com.ebs.dda.accounting.costing.apis.IDObCostingJsonSchema;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.functions.validate.ValidateSchemaContext;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IDObCosting;

public class CostingActivateContextBuilder {

  private CostingActivateContextBuilder() {}

  public static CostingActivateContext newContext() {
    return new CostingActivateContext();
  }

  public static class CostingActivateContext
      implements ActivateContext,
          AuthorizeContext,
          ValidateSchemaContext,
          FindEntityContext,
          DeserializeValueObjectContext<DObCostingActivateValueObject>,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<DObCostingActivateValueObject>,
          ExecuteCommandContext<DObCostingActivateValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext {

    private String objectCode;
    private String activateData;
    private DObCostingActivateValueObject valueObject;
    private String response;
    private String journalEntryUserCode;
    private EntitiesOfInterestContainer entitiesOfInstrestContainer;

    private CostingActivateContext() {
      this.entitiesOfInstrestContainer =
          new EntitiesOfInterestContainer(DObCostingGeneralModel.class);
    }

    @Override
    public String activateData() {
      return activateData;
    }

    @Override
    public String schema() {
      return IDObCostingJsonSchema.ACTIVATION_SCHEMA;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return IDObCosting.SYS_NAME;
    }

    @Override
    public String actionName() {
      return IDObCostingActionNames.ACTIVATE;
    }

    @Override
    public String state() {
      return BasicStateMachine.ACTIVE_STATE;
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    public CostingActivateContext objectCode(String userCode) {
      this.objectCode = userCode;
      return this;
    }

    public CostingActivateContext valueObject(String valueObject) {
      this.activateData = valueObject;
      return this;
    }

    @Override
    public Class<DObCostingActivateValueObject> valueObjectClass() {
      return DObCostingActivateValueObject.class;
    }

    @Override
    public void valueObject(DObCostingActivateValueObject valueObject) {
      this.valueObject = valueObject;
    }

    @Override
    public DObCostingActivateValueObject valueObject() {
      return this.valueObject;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return journalEntryUserCode;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return entitiesOfInstrestContainer;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      journalEntryUserCode = userCode;
    }
  }
}
