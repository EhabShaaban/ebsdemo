package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.salesinvoice.editability.SalesInvoiceItemEditabilityManger;
import com.ebs.dda.accounting.salesinvoice.validation.apis.ISalesInvoiceItemsMandatoryAttributes;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceItem;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.Set;

public class IObItemsCommandControllerUtils extends DObSalesInvoiceCommandControllerUtils {

  private SalesInvoiceItemEditabilityManger salesInvoiceItemEditabilityManger;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObItemsCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  protected AuthorizedPermissionsConfig getِAddAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(IMObItem.SYS_NAME, IActionsNames.READ_ALL, "ReadItems");
    permissionsConfig.addPermissionConfig(
        ICObMeasure.SYS_NAME, IActionsNames.READ_ALL, "ReadItemUoM");
    return permissionsConfig;
  }

  protected Set<String> getEnableAttributesForAddItem(DObSalesInvoiceGeneralModel salesInvoice) {
    return salesInvoiceItemEditabilityManger
        .getEnabledAttributesConfig()
        .get(salesInvoice.getCurrentState());
  }

  public String[] getItemsMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            ISalesInvoiceItemsMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public void setSalesInvoiceItemEditabilityManger(
      SalesInvoiceItemEditabilityManger salesInvoiceItemEditabilityManger) {
    this.salesInvoiceItemEditabilityManger = salesInvoiceItemEditabilityManger;
  }

  public void checkIfSalesInvoiceItemExists(IObSalesInvoiceItem salesInvoiceItem)
      throws RecordOfInstanceNotExistException {
    if (salesInvoiceItem == null) {
      throw new RecordOfInstanceNotExistException(IObSalesInvoiceItem.class.getName());
    }
  }
}
