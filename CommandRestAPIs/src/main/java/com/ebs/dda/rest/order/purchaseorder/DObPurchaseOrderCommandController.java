package com.ebs.dda.rest.order.purchaseorder;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;
import static com.ebs.dda.rest.order.purchaseorder.IPurchaseOrderMessages.APPROVER_HAS_BEEN_REPLACED_SUCCESSFULLY;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.accessability.ObjectStateNotValidException;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.RecordDeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderApproveCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderAssignMeToApproveCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderCancelCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderConfirmCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderEditConsigneeCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderMarkAsArrivedCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderMarkAsClearedCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderMarkAsDeliveryCompleteCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderOpenForUpdatesCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderRejectCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderSaveItemCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderSaveItemQtyInDnPlCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderSaveItemQuantityCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderSubmitForApprovalCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderUploadAttachmentCommand;
import com.ebs.dda.commands.order.purchaseorder.IObOrderDocumentRequiredDocumentsUpdateCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderCompanyEditCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderCreateCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderDeleteCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderDeleteItemCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderDeleteItemQuantityCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderEditItemCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderMarkAsPIRequestedCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderMarkAsProductionFinishedCommand;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderMarkAsShippedCommand;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.plant.ICObPlant;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis.ILObAttachmentType;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects.IObOrderLineDetailsQuantitiesRep;
import com.ebs.dda.purchases.apis.IDObPurchaseOrderJsonSchema;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderApproveValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderAssignMeToApproveValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderAttachmentValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCompanyValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConfirmValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConsigneeValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCreateValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderDeliveryCompleteValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderMarkAsArrivedValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderMarkAsShippedValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DobPurchaseOrderMarkAsClearedValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DobPurchaseOrderMarkAsPIRequestedValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DobPurchaseOrderMarkAsProductionFinishedValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderDocumentRequiredDocumentsValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObOrderLineDetailsValueObject;
import com.ebs.dda.purchases.editability.DObPurchaseOrderCompanySectionEditabilityManager;
import com.ebs.dda.purchases.editability.DObPurchaseOrderItemQuantitySectionEditabilityManager;
import com.ebs.dda.purchases.editability.DObPurchaseOrderItemsQtyInDnPlEditabilityManager;
import com.ebs.dda.purchases.editability.DObPurchaseOrderItemsSectionEditabilityManager;
import com.ebs.dda.purchases.editability.IObServicePurchaseOrderItemsSectionEditabilityManager;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.validation.mandatoryvalidator.DObPurchaseOrderConsigneeMandatoryValidator;
import com.ebs.dda.purchases.validation.mandatoryvalidator.DObPurchaseOrderGeneralModelsMandatoryValidator;
import com.ebs.dda.purchases.validation.mandatoryvalidator.DObPurchaseOrderItemsMandatoryValidator;
import com.ebs.dda.purchases.validation.mandatoryvalidator.DObPurchaseOrderPaymentAndDeliveryMandatoryValidator;
import com.ebs.dda.purchases.validation.mandatoryvalidator.IObOrderDocumentRequiredDocumentsMandatoryValidator;
import com.ebs.dda.purchases.validation.validator.DObPurchaseOrderCompanyValidator;
import com.ebs.dda.purchases.validation.validator.DObPurchaseOrderConsigneeValidator;
import com.ebs.dda.purchases.validation.validator.DObPurchaseOrderPaymentAndDeliveryValidator;
import com.ebs.dda.purchases.validation.validator.IObOrderDocumentRequiredDocumentsValidator;
import com.ebs.dda.purchases.validation.validator.IObOrderLineDetailesValidator;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderApproveAndRejectValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderArrivedValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderConfirmationValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderCreateValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderMarkAsClearedValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderMarkAsDeliveryCompleteValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderMarkAsProductionFinishedValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderReplaceApproverValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderSaveItemQuantityValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderShippedValidator;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderStateTransitionCommonValidator;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.reactivex.Observable;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "command/purchaseorder")
public class DObPurchaseOrderCommandController implements InitializingBean {

  private final String SAVING_SUCCESSFUL_CODE = "Gen-msg-04";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  @Autowired IObOrderDocumentRequiredDocumentsValidator orderDocumentRequiredDocumentsValidator;

  @Autowired
  DObPurchaseOrderPaymentAndDeliveryValidator dObPurchaseOrderPaymentAndDeliveryValidator;

  @Autowired
  DObPurchaseOrderPaymentAndDeliveryMandatoryValidator
      paymentAndDeliveryValueObjectMandatoryValidator;

  @Autowired
  IObOrderDocumentRequiredDocumentsMandatoryValidator
      requiredDocumentsValueObjectMandatoryValidator;

  @Autowired private PurchaseOrderDeleteCommand purchaseOrderDeleteCommand;
  @Autowired private PurchaseOrderCompanyEditCommand purchaseOrderCompanyEditCommand;
  @Autowired private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  @Autowired private DObPurchaseOrderRep purchaseOrderRep;

  @Autowired private DObPurchaseOrderCompanyValidator purchaseOrderCompanyValidator;
  @Autowired private DObPurchaseOrderEditConsigneeCommand purchaseOrderEditConsigneeCommand;
  @Autowired private DObPurchaseOrderConsigneeValidator purchaseOrderConsigneeValidator;

  @Autowired
  private IObOrderDocumentRequiredDocumentsUpdateCommand purchaseOrderEditRequiredDocumentsCommand;

  @Autowired private EntityLockCommand<DObOrderDocument> entityLockCommand;
  @Autowired private DObPurchaseOrderUploadAttachmentCommand purchaseOrderUploadAttachmentCommand;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private PurchaseOrderCreateCommand purchaseOrderCreateCommand;
  @Autowired private DObPurchaseOrderCreateValidator createValidator;
  @Autowired private PurchaseOrderDeleteItemCommand purchaseOrderDeleteItemCommand;
  @Autowired private IObOrderLineDetailsRep orderLineDetailsRep;
  @Autowired private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;
  @Autowired private PurchaseOrderEditItemCommand purchaseOrderEditItemCommand;

  @Autowired
  private DObPurchaseOrderItemsSectionEditabilityManager
      purchaseOrderItemsSectionEditabilityManager;

  @Autowired
  private DObPurchaseOrderItemsQtyInDnPlEditabilityManager itemsQtyInDnPlEditabilityManager;

  @Autowired private DObPurchaseOrderApproveCommand purchaseOrderApproveCommand;
  @Autowired private DObPurchaseOrderAssignMeToApproveCommand assignMeToApproveCommand;
  @Autowired private PurchaseOrderMarkAsPIRequestedCommand markAsPIRequestedCommand;
  @Autowired private DObPurchaseOrderConfirmCommand confirmCommand;
  @Autowired private DObPurchaseOrderApproveAndRejectValidator approveValidator;
  @Autowired private DObPurchaseOrderReplaceApproverValidator replaceApproverValidator;

  @Autowired private PurchaseOrderMarkAsProductionFinishedCommand markAsProductionFinishedCommand;

  @Autowired
  private DObPurchaseOrderConsigneeMandatoryValidator ConsigneeValueObjectMandatoryValidator;

  @Autowired
  private DObPurchaseOrderMarkAsProductionFinishedValidator
      purchaseOrderMarkAsProductionFinishedValidator;

  @Autowired private DObPurchaseOrderRejectCommand purchaseOrderRejectCommand;
  @Autowired private DObPurchaseOrderConfirmationValidator purchaseOrderConfirmationValidator;
  @Autowired private DObPurchaseOrderMarkAsArrivedCommand arrivedCommand;
  @Autowired private DObPurchaseOrderArrivedValidator purchaseOrderArrivedValidator;

  @Autowired
  private DObPurchaseOrderGeneralModelsMandatoryValidator orderGeneralModelsMandatoryValidator;

  @Autowired private PurchaseOrderMarkAsShippedCommand shippedCommand;
  @Autowired private DObPurchaseOrderShippedValidator shippedValidator;
  @Autowired private DObPurchaseOrderMarkAsClearedValidator purchaseOrderMarkAsClearedValidator;
  @Autowired private DObPurchaseOrderMarkAsClearedCommand markAsClearedCommand;
  @Autowired private DObPurchaseOrderMarkAsDeliveryCompleteCommand markAsDeliveryCompleteCommand;

  @Autowired
  private DObPurchaseOrderMarkAsDeliveryCompleteValidator
      purchaseOrderMarkAsDeliveryCompleteValidator;

  @Autowired private DObPurchaseOrderCancelCommand cancelCommand;
  @Autowired private PurchaseOrderDeleteItemQuantityCommand deleteItemQuantityCommand;
  @Autowired private DObPurchaseOrderSubmitForApprovalCommand submitForApprovalCommand;

  @Autowired
  @Qualifier("purchaseOrderStateTransitionCommonValidator")
  private DObPurchaseOrderStateTransitionCommonValidator stateTransitionCommonValidator;

  @Autowired private DObPurchaseOrderSaveItemQtyInDnPlCommand saveItemQtyInDnPlCommand;
  @Autowired private DObPurchaseOrderSaveItemCommand orderSaveItemCommand;
  @Autowired private DObPurchaseOrderOpenForUpdatesCommand openForUpdatesCommand;
  @Autowired private DObPurchaseOrderItemsMandatoryValidator orderItemsMandatoryValidator;
  @Autowired private IObOrderLineDetailesValidator orderItemsValidator;
  @Autowired private DObPurchaseOrderSaveItemQuantityCommand saveItemQuantityCommand;
  @Autowired private DObPurchaseOrderSaveItemQuantityValidator saveItemQuantityValidator;
  @Autowired private IObOrderLineDetailsQuantitiesRep orderLineDetailsQuantitiesRep;

  @Autowired
  private DObPurchaseOrderItemQuantitySectionEditabilityManager
      itemQuantitySectionEditabilityManager;

  @Autowired private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;
  @Autowired private ObservablesEventBus observablesEventBus;
  @Autowired private MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep;

  @Autowired
  private DObPurchaseOrderCompanySectionEditabilityManager
      purchaseOrderCompanySectionEditabilityManager;

  @Autowired
  private IObServicePurchaseOrderItemsSectionEditabilityManager
      servicePurchaseOrderItemsSectionEditabilityManager;

  private CommonControllerUtils commonUtils;
  private DObPurchaseOrderControllerUtils utils;
  private IObCompanyCommandControllerUtils companyControllerUtils;
  private IObItemsCommandControllerUtils itemsControllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String getCreateConfiguration() {

    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = utils.constructCreateConfiguration();
    List<String> authorizedReads = commonUtils.getAuthorizedReads(createPermissionsConfig);

    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return CommonControllerUtils.serializeEditConfigurationsResponse(createConfig);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String create(@RequestBody String orderCreationData) throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.CREATE, orderCreationData);

    DObPurchaseOrderCreateValueObject valueObject =
        (DObPurchaseOrderCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                orderCreationData, DObPurchaseOrderCreateValueObject.class);

    createValidator.validateMissingMandatoryFields(valueObject);

    utils.applyAuthorizationForCreateAction(valueObject);
    utils.applyAuthorizationForValueObjectValues(valueObject);

    ValidationResult validationResult = createValidator.validateBusinessRules(valueObject);

    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }

    final StringBuilder userCode = new StringBuilder();
    Observable<String> codeObservable = purchaseOrderCreateCommand.executeCommand(valueObject);
    codeObservable.subscribe(code -> userCode.append(code));

    return utils.getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{purchaseOrderCode}")
  public @ResponseBody String deletePurchaseOrder(@PathVariable String purchaseOrderCode)
      throws Exception {
    LockDetails lockDetails = null;

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, IActionsNames.DELETE);

    try {
      lockDetails = entityLockCommand.lockAllSections(purchaseOrderCode);
      utils.checkIfActionIsAllowedInCurrentState(purchaseOrderCode, IActionsNames.DELETE);
      Observable<String> deleteObservable =
          purchaseOrderDeleteCommand.executeCommand(purchaseOrderCode);
      deleteObservable.subscribe(
          (msg) -> {
            observablesEventBus.registerObservable(PurchaseOrderDeleteCommand.class);
          });
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    } catch (ActionNotAllowedPerStateException e) {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(purchaseOrderCode);
      }
      DeleteNotAllowedPerStateException deleteNotAllowedPerStateException =
          new DeleteNotAllowedPerStateException(e);
      throw deleteNotAllowedPerStateException;
    }

    return CommonControllerUtils.getSuccessResponse(utils.DELETION_SUCCESSFUL_CODE);
  }

  @RequestMapping(
      method = RequestMethod.DELETE,
      value = "/delete/item/{purchaseOrderCode}/{itemCode}")
  public @ResponseBody String deleteItemInPurchaseOrder(
      @PathVariable String purchaseOrderCode, @PathVariable String itemCode) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, IActionsNames.UPDATE_ITEM);
    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);
    IObOrderLineDetails orderLineDetails =
        utils.readOrderLineByPOAndItemCode(purchaseOrderCode, itemCode);
    utils.checkPOItemsExist(orderLineDetails);
    LockDetails lockDetails =
        entityLockCommand.lockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    try {
      utils.checkIfActionIsAllowedInCurrentState(
          purchaseOrderCode, IPurchaseOrderActionNames.UPDATE_ITEM);
      purchaseOrderDeleteItemCommand.executeCommand(purchaseOrderCode, itemCode);
    } catch (ActionNotAllowedPerStateException actionNotAllowedPerStateException) {
      throw new RecordDeleteNotAllowedPerStateException();
    } finally {
      if (lockDetails != null) {
        entityLockCommand.unlockSection(
            purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      }
    }
    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.RECORD_DELETION_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{purchaseOrderCode}/lock/items")
  public @ResponseBody String requestAddItemInServicePurchaseOrder(
      @PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, IActionsNames.UPDATE_ITEM);
    try {
      utils.checkIfActionIsAllowedInCurrentState(purchaseOrderCode, IActionsNames.UPDATE_ITEM);
    } catch (ActionNotAllowedPerStateException actionNotAllowedPerStateException) {
      throw new EditNotAllowedPerStateException();
    }
    entityLockCommand.lockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);

    AuthorizedPermissionsConfig permissionsConfig =
        itemsControllerUtils.getServicePurchaseOrderItemsAuthorizedPermissionsConfig();
    List<String> authorizedReads = companyControllerUtils.getAuthorizedReads(permissionsConfig);

    String[] stateMandatories =
        itemsControllerUtils.getItemsMandatoryAttributesByState(
            orderGeneralModel.getCurrentState());

    Set<String> stateEnabledAttributes =
        itemsControllerUtils.getItemsEnableAttributesByState(orderGeneralModel.getCurrentStates());

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setMandatories(stateMandatories);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{purchaseOrderCode}/unlock/items")
  public @ResponseBody String requestCancelItemsInServicePurchaseOrder(
      @PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, IActionsNames.UPDATE_ITEM);
    entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    return CommonControllerUtils.getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/add/item/{purchaseOrderCode}")
  public @ResponseBody String requestAddItemInPurchaseOrder(@PathVariable String purchaseOrderCode)
      throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, IActionsNames.UPDATE_ITEM);
    utils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.ITEMS_SECTION,
        IPurchaseOrderActionNames.UPDATE_ITEM);
    return utils.getItemEditConfiguration(
        purchaseOrderGeneralModelRep,
        purchaseOrderItemsSectionEditabilityManager,
        purchaseOrderCode);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/lock/edit/itemquantity/{purchaseOrderCode}/{itemCode}/{quantityId}")
  public @ResponseBody String requestEditItemQuantityForItemInPurchaseOrder(
      @PathVariable String purchaseOrderCode,
      @PathVariable String itemCode,
      @PathVariable Long quantityId)
      throws Exception {
    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);

    IObOrderLineDetails orderLineDetails =
        utils.readOrderLineByPOAndItemCode(purchaseOrderCode, itemCode);
    DObPurchaseOrderGeneralModel order =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkPOExists(order);
    utils.checkPOItemsExist(orderLineDetails);
    utils.checkIfQuantityExists(quantityId);

    utils.checkAuthorizeForActionOnBusinessObject(order, IActionsNames.UPDATE_ITEM);

    utils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.ITEMS_SECTION,
        IPurchaseOrderActionNames.UPDATE_ITEM);

    return utils.getItemQuantityEditConfiguration(
        purchaseOrderGeneralModelRep, itemQuantitySectionEditabilityManager, purchaseOrderCode);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/edit/itemquantity/{purchaseOrderCode}/{itemCode}/{quantityId}")
  public @ResponseBody String requestCancelItemQuantityForItemInPurchaseOrder(
      @PathVariable String purchaseOrderCode,
      @PathVariable String itemCode,
      @PathVariable Long quantityId)
      throws Exception {

    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);

    IObOrderLineDetails orderLineDetails =
        utils.readOrderLineByPOAndItemCode(purchaseOrderCode, itemCode);
    DObPurchaseOrderGeneralModel order =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkPOExists(order);
    utils.checkPOItemsExist(orderLineDetails);
    utils.checkIfQuantityExists(quantityId);

    utils.checkAuthorizeForActionOnBusinessObject(order, IActionsNames.UPDATE_ITEM);

    entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/lock/orderitems/itemquantity/{purchaseOrderCode}/{itemCode}")
  public @ResponseBody String requestAddItemQuantityForItemInPurchaseOrder(
      @PathVariable String purchaseOrderCode, @PathVariable String itemCode) throws Exception {
    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);

    IObOrderLineDetails orderLineDetails =
        utils.readOrderLineByPOAndItemCode(purchaseOrderCode, itemCode);
    DObPurchaseOrderGeneralModel order =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkPOExists(order);
    utils.checkPOItemsExist(orderLineDetails);

    utils.checkAuthorizeForActionOnBusinessObject(order, IActionsNames.UPDATE_ITEM);

    utils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.ITEMS_SECTION,
        IPurchaseOrderActionNames.UPDATE_ITEM);

    return utils.getItemQuantityEditConfiguration(
        purchaseOrderGeneralModelRep, itemQuantitySectionEditabilityManager, purchaseOrderCode);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/orderitems/itemquantity/{purchaseOrderCode}/{itemCode}")
  public @ResponseBody String requestCancelItemQuantityForItemInPurchaseOrder(
      @PathVariable String purchaseOrderCode, @PathVariable String itemCode) throws Exception {

    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);

    IObOrderLineDetails orderLineDetails =
        utils.readOrderLineByPOAndItemCode(purchaseOrderCode, itemCode);
    DObPurchaseOrderGeneralModel order =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkPOExists(order);
    utils.checkPOItemsExist(orderLineDetails);

    utils.checkAuthorizeForActionOnBusinessObject(order, IActionsNames.UPDATE_ITEM);

    entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/edit/item/{purchaseOrderCode}/{itemCode}")
  public @ResponseBody String openItemSectionEditMode(
      @PathVariable String purchaseOrderCode, @PathVariable String itemCode) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, IActionsNames.UPDATE_ITEM);

    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);

    IObOrderLineDetails orderLineDetails =
        utils.readOrderLineByPOAndItemCode(purchaseOrderCode, itemCode);

    utils.checkPOItemsExist(orderLineDetails);

    utils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.ITEMS_SECTION,
        IPurchaseOrderActionNames.UPDATE_ITEM);
    return utils.getItemEditConfiguration(
        purchaseOrderGeneralModelRep,
        purchaseOrderItemsSectionEditabilityManager,
        purchaseOrderCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unlock/itemsection/{purchaseOrderCode}")
  public @ResponseBody String closePurchaseOrderItemEditMode(@PathVariable String purchaseOrderCode)
      throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, IActionsNames.UPDATE_ITEM);
    purchaseOrderEditItemCommand.unlockSection(purchaseOrderCode);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/update/consignee/{purchaseOrderCode}")
  public @ResponseBody String updateConsigneeSection(
      @PathVariable String purchaseOrderCode, @RequestBody String consigneeSectionData)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DObPurchaseOrderConsigneeValueObject.class, consigneeSectionData);

    try {
      utils.applyAuthorization(
          purchaseOrderGeneralModelRep,
          purchaseOrderCode,
          IPurchaseOrderActionNames.EDIT_CONSIGNEE_SECTION);
      utils.applyAuthorizationOnConsigneeSectionFields();
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }

    Gson gson = new Gson();
    DObPurchaseOrderConsigneeValueObject purchaseOrderConsigneeValueObject =
        gson.fromJson(consigneeSectionData, DObPurchaseOrderConsigneeValueObject.class);

    purchaseOrderConsigneeValueObject.setPurchaseOrderCode(purchaseOrderCode);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        purchaseOrderCode, IPurchaseOrderSectionNames.CONSIGNEE_SECTION);

    try {
      ConsigneeValueObjectMandatoryValidator.validateValueObjectMandatories(
          purchaseOrderConsigneeValueObject);
      ValidationResult validationResult =
          purchaseOrderConsigneeValidator.validate(purchaseOrderConsigneeValueObject);
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      } else {
        purchaseOrderEditConsigneeCommand.executeCommand(purchaseOrderConsigneeValueObject);
        purchaseOrderEditConsigneeCommand.unlockSection(purchaseOrderCode);
        return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
      }
    } catch (ArgumentViolationSecurityException argumentViolationSecurityException) {
      purchaseOrderEditConsigneeCommand.unlockSection(purchaseOrderCode);
      throw argumentViolationSecurityException;
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{purchaseOrderCode}/update/company")
  public @ResponseBody String updateCompanySection(
      @PathVariable String purchaseOrderCode, @RequestBody String companySectionData)
      throws Exception {
    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.Save_Company, companySectionData);

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    authorizationManager.authorizeActionOnObject(
        orderGeneralModel, IPurchaseOrderActionNames.EDIT_COMPANY_SECTION);

    utils.applyAuthorizationOnCompanySectionFields(orderGeneralModel);

    DObPurchaseOrderCompanyValueObject purchaseOrderCompanyValueObject =
        (DObPurchaseOrderCompanyValueObject)
            CommonControllerUtils.deserializeRequestBody(
                companySectionData, DObPurchaseOrderCompanyValueObject.class);
    purchaseOrderCompanyValueObject.setPurchaseOrderCode(purchaseOrderCode);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        purchaseOrderCode, IPurchaseOrderSectionNames.COMPANY_SECTION);

    ValidationResult validationResult =
        purchaseOrderCompanyValidator.validate(purchaseOrderCompanyValueObject);

    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    } else {
      purchaseOrderCompanyEditCommand.executeCommand(purchaseOrderCompanyValueObject);
      purchaseOrderCompanyEditCommand.unlockSection(purchaseOrderCode);
      return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
    }
  }

  @RequestMapping(
      method = RequestMethod.PUT,
      value = "/update/requireddocuments/{purchaseOrderCode}")
  public @ResponseBody String updateRequiredDocumentsSection(
      @PathVariable String purchaseOrderCode, @RequestBody String requiredDocumentsJsonObject)
      throws Exception {

    IObOrderDocumentRequiredDocumentsValueObject purchaseOrderRequiredDocumentsValueObject =
        (IObOrderDocumentRequiredDocumentsValueObject)
            CommonControllerUtils.deserializeRequestBody(
                requiredDocumentsJsonObject, IObOrderDocumentRequiredDocumentsValueObject.class);
    try {

      CommonControllerUtils.applySchemaValidation(
          IObOrderDocumentRequiredDocumentsValueObject.class, requiredDocumentsJsonObject);

      DObPurchaseOrderGeneralModel orderGeneralModel =
          purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
      utils.checkAuthorizeForActionOnBusinessObject(
          orderGeneralModel, IPurchaseOrderActionNames.EDIT_REQUIRED_DOCUMENTS_SECTION);
      utils.applyAuthorizationOnRequiredDocumentsSectionFields();

      entityLockCommand.checkIfEntityExists(purchaseOrderCode);
      entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
          purchaseOrderCode, IPurchaseOrderSectionNames.REQUIRED_DOCUMENTS_SECTION);

      requiredDocumentsValueObjectMandatoryValidator.validateValueObjectMandatories(
          purchaseOrderRequiredDocumentsValueObject);

      ValidationResult validationResult =
          orderDocumentRequiredDocumentsValidator.validate(
              purchaseOrderRequiredDocumentsValueObject);

      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      }
      purchaseOrderEditRequiredDocumentsCommand.executeCommand(
          purchaseOrderRequiredDocumentsValueObject);
      purchaseOrderEditRequiredDocumentsCommand.unlockSection(purchaseOrderCode);
      return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
    } catch (ArgumentViolationSecurityException argumentViolationSecurityException) {
      purchaseOrderEditRequiredDocumentsCommand.unlockSection(purchaseOrderCode);
      throw argumentViolationSecurityException;
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/lock/companysection/{purchaseOrderCode}")
  public @ResponseBody String openCompanySectionInPurchaseOrder(
      @PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.EDIT_COMPANY_SECTION);

    utils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.COMPANY_SECTION,
        IPurchaseOrderActionNames.EDIT_COMPANY_SECTION);

    Set<String> stateEnabledAttributes =
        companyControllerUtils.getEnableAttributes(orderGeneralModel);

    String[] mandatories =
        companyControllerUtils.getMandatoryAttributesByState(orderGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        companyControllerUtils.getEditCompanyAuthorizedPermissionsConfig();

    List<String> authorizedReads = companyControllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(mandatories);

    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/consignee/lockheader/{purchaseOrderCode}")
  public @ResponseBody String openConsigneeSectionEditMode(@PathVariable String purchaseOrderCode)
      throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.EDIT_CONSIGNEE_SECTION);

    utils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.CONSIGNEE_SECTION,
        IPurchaseOrderActionNames.EDIT_CONSIGNEE_SECTION);

    List<String> authorizedReads = prepareConsigneeAuthorizedReads();
    return CommonControllerUtils.serializeEditConfigurationsResponse(authorizedReads);
  }

  private List<String> prepareConsigneeAuthorizedReads() {
    List<String> authorizedReads = new ArrayList<>();

    boolean isReadConsigneeAllowed =
        utils.isReadPermissionAllowed(CObCompany.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadPlantAllowed =
        utils.isReadPermissionAllowed(ICObPlant.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadStorehouseAllowed =
        utils.isReadPermissionAllowed(ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadConsigneeAllowed) {
      authorizedReads.add("ReadAllCompany");
    }
    if (isReadPlantAllowed) {
      authorizedReads.add("ReadAllPlant");
    }
    if (isReadStorehouseAllowed) {
      authorizedReads.add("ReadAllStorehouse");
    }
    return authorizedReads;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/consignee/unlockheader/{purchaseOrderCode}")
  public @ResponseBody String closeConsigneeSectionEditMode(@PathVariable String purchaseOrderCode)
      throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.EDIT_CONSIGNEE_SECTION);
    purchaseOrderEditConsigneeCommand.unlockSection(purchaseOrderCode);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unlock/companysection/{purchaseOrderCode}")
  public @ResponseBody String closeCompanySectionInPurchaseOrder(
      @PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.EDIT_COMPANY_SECTION);
    purchaseOrderCompanyEditCommand.unlockSection(purchaseOrderCode);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/lock/requireddocuments/{purchaseOrderCode}")
  public @ResponseBody String openRequiredDocumentsSectionInPurchaseOrderEditMode(
      @PathVariable String purchaseOrderCode) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.EDIT_REQUIRED_DOCUMENTS_SECTION);
    utils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.REQUIRED_DOCUMENTS_SECTION,
        IPurchaseOrderActionNames.EDIT_REQUIRED_DOCUMENTS_SECTION);
    List<String> authorizedReads = prepareRequiredDocumnetsAuthorizedReads();
    String[] mandatoriesResponse =
        utils.getRequiredDocumentsMandatoryAttributesByState(orderGeneralModel);

    return CommonControllerUtils.serializeEditConfigurationsResponse(
        mandatoriesResponse, authorizedReads);
  }

  private List<String> prepareRequiredDocumnetsAuthorizedReads() {
    List<String> authorizedReads = new ArrayList<>();

    boolean isReadDocumentTypeAllowed =
        utils.isReadPermissionAllowed(ILObAttachmentType.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadDocumentTypeAllowed) {
      authorizedReads.add("ReadAllDocumentTypes");
    }
    return authorizedReads;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/requireddocuments/{purchaseOrderCode}")
  public @ResponseBody String closeRequiredDocumentsSectionInPurchaseOrderEditMode(
      @PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.EDIT_REQUIRED_DOCUMENTS_SECTION);
    purchaseOrderEditRequiredDocumentsCommand.unlockSection(purchaseOrderCode);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/attachment/save/{purchaseOrderCode}")
  public @ResponseBody String createAttachment(
      @RequestParam("file") MultipartFile file,
      @RequestParam("documentTypeCode") String documentTypeCode,
      @PathVariable String purchaseOrderCode)
      throws Exception {

    DObPurchaseOrderAttachmentValueObject attachmentValueObject =
        initializeAttachmentValueObject(file, documentTypeCode, purchaseOrderCode);

    purchaseOrderUploadAttachmentCommand.executeCommand(attachmentValueObject);

    return utils.getSuccessUploadResponse();
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{purchaseOrderCode}")
  public @ResponseBody String closeLockedSectionsInPurchaseOrder(
      @PathVariable String purchaseOrderCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(purchaseOrderCode, lockedSections);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/markAsPIRequested/{purchaseOrderCode}")
  public @ResponseBody String markAsPIRequested(
      @PathVariable String purchaseOrderCode, @RequestBody String markAsPIData) throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DobPurchaseOrderMarkAsPIRequestedValueObject.class, markAsPIData);

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_PI_REQUESTED);

    DobPurchaseOrderMarkAsPIRequestedValueObject markAsPIRequestedValueObject =
        utils.getGson().fromJson(markAsPIData, DobPurchaseOrderMarkAsPIRequestedValueObject.class);
    markAsPIRequestedValueObject.setPurchaseOrderCode(purchaseOrderCode);
    entityLockCommand.lockAllSections(purchaseOrderCode);
    try {
      utils.checkIfActionIsAllowedInCurrentState(
          purchaseOrderCode, IActionsNames.MARK_AS_PI_REQUESTED);
      Map<String, List<String>> missingFields =
          orderGeneralModelsMandatoryValidator.validateAllSectionsForState(
              purchaseOrderCode, DObImportPurchaseOrderStateMachine.OPEN_FOR_UPDATES);
      if (!missingFields.isEmpty()) {
        return utils.getFailureResponseWithValidationErrorsAndCode(
            missingFields, utils.MISSING_FIELDS_CODE);
      }
      markAsPIRequestedCommand.executeCommand(markAsPIRequestedValueObject);
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    }
    return getSuccessResponse(IPurchaseOrderMessages.PI_REQUESTED_SUCCESSFUL);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/markas/shipped")
  public @ResponseBody String markAsShipped(@RequestBody String markAsShippedJsonData)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DObPurchaseOrderMarkAsShippedValueObject.class, markAsShippedJsonData);

    DObPurchaseOrderMarkAsShippedValueObject purchaseOrderValueObject =
        utils
            .getGson()
            .fromJson(markAsShippedJsonData, DObPurchaseOrderMarkAsShippedValueObject.class);
    String purchaseOrderCode = purchaseOrderValueObject.getPurchaseOrderCode();

    try {
      DObPurchaseOrderGeneralModel orderGeneralModel =
          purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
      utils.checkAuthorizeForActionOnBusinessObject(
          orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_SHIPPED);
    } catch (InstanceNotExistException exception) {
      AuthorizationException authorizationException = new AuthorizationException(exception);
      throw authorizationException;
    }
    LockDetails lockDetails = null;
    ValidationResult validationResult = null;
    try {
      lockDetails =
          entityLockCommand.lockAllSections(purchaseOrderValueObject.getPurchaseOrderCode());
      validationResult =
          shippedValidator.validate(purchaseOrderCode, purchaseOrderValueObject.getShippingDate());
      utils.checkIfActionIsAllowedInCurrentState(purchaseOrderCode, IActionsNames.MARK_AS_SHIPPED);
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      } else {
        shippedCommand.executeCommand(purchaseOrderValueObject);
        return getSuccessResponse(IPurchaseOrderMessages.SHIPPED_SUCCESSFUL);
      }
    } finally {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(purchaseOrderValueObject.getPurchaseOrderCode());
      }
    }
  }

  @RequestMapping(method = RequestMethod.POST, value = "/replaceapprover")
  public @ResponseBody String assignMeToApprovePurchaseOrder(@RequestBody String assignMeData)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DObPurchaseOrderAssignMeToApproveValueObject.class, assignMeData);

    DObPurchaseOrderAssignMeToApproveValueObject assignMeToApproveValueObject =
        new Gson().fromJson(assignMeData, DObPurchaseOrderAssignMeToApproveValueObject.class);

    String purchaseOrderCode = assignMeToApproveValueObject.getPurchaseOrderCode();

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.REPLACE_APPROVER_PO);

    replaceApproverValidator.validate(
        purchaseOrderCode,
        assignMeToApproveValueObject,
        IPurchaseOrderActionNames.REPLACE_APPROVER_PO);

    entityLockCommand.lockAllSections(purchaseOrderCode);

    try {
      assignMeToApproveCommand.executeCommand(assignMeToApproveValueObject);
    } finally {
      entityLockCommand.unlockAllSections(orderGeneralModel.getUserCode());
    }
    return getSuccessResponse(APPROVER_HAS_BEEN_REPLACED_SUCCESSFULLY);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/approve")
  public @ResponseBody String approvePurchaseOrder(@RequestBody String approvalData)
      throws Exception {
    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.APPROVED, approvalData);
    DObPurchaseOrderApproveValueObject approveValueObject =
        new Gson().fromJson(approvalData, DObPurchaseOrderApproveValueObject.class);
    String purchaseOrderCode = approveValueObject.getPurchaseOrderCode();
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    try {
      utils.checkAuthorizeForActionOnBusinessObject(
          orderGeneralModel, IPurchaseOrderActionNames.APPROVE_PO);
    } catch (InstanceNotExistException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
    approveValidator.validate(orderGeneralModel, IPurchaseOrderActionNames.APPROVE_PO);
    purchaseOrderApproveCommand.executeCommand(approveValueObject);
    String msgCode =
        purchaseOrderApproveCommand.isLastApprover()
            ? IPurchaseOrderMessages.SUCCESS_APPROVE_LAST_APPROVER
            : IPurchaseOrderMessages.SUCCESS_APPROVE_NOT_LAST_APPROVER;
    return getSuccessResponse(msgCode);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/reject")
  public @ResponseBody String rejectPurchaseOrder(@RequestBody String approvalData)
      throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.REJECT, approvalData);
    DObPurchaseOrderApproveValueObject approveValueObject =
        new Gson().fromJson(approvalData, DObPurchaseOrderApproveValueObject.class);
    String purchaseOrderCode = approveValueObject.getPurchaseOrderCode();
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    try {
      utils.checkAuthorizeForActionOnBusinessObject(
          orderGeneralModel, IPurchaseOrderActionNames.REJECT_PO);
    } catch (InstanceNotExistException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
    approveValidator.validate(orderGeneralModel, IPurchaseOrderActionNames.REJECT_PO);
    purchaseOrderRejectCommand.executeCommand(approveValueObject);
    return getSuccessResponse(IPurchaseOrderMessages.SUCCESS_REJECT);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/confirm/{purchaseOrderCode}")
  public @ResponseBody String confirmPurchaseOrder(
      @PathVariable String purchaseOrderCode, @RequestBody String confirmationData)
      throws Exception {
    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.Confirm, confirmationData);

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_CONFIRMED);

    DObPurchaseOrderConfirmValueObject confirmValueObject =
        utils.getGson().fromJson(confirmationData, DObPurchaseOrderConfirmValueObject.class);

    entityLockCommand.lockAllSections(purchaseOrderCode);

    Map<String, Object> missingFields =
        purchaseOrderConfirmationValidator.validateMandatories(
            orderGeneralModel, confirmValueObject);

    try {
      ValidationResult validationResult =
          purchaseOrderConfirmationValidator.validate(orderGeneralModel, confirmValueObject);
      String purchaseOrderErrorCode =
          validationResult.getErrorCode(IDObPurchaseOrderGeneralModel.REFERENCE_PO_CODE);
      if (!validationResult.isValid()) {
        if (purchaseOrderErrorCode == null) {
          return getFailureResponse(validationResult);
        } else {
          throw new ObjectStateNotValidException();
        }
      } else {

        if (!missingFields.isEmpty()) {
          return CommonControllerUtils.getFailureResponseWithMissingFields(
              missingFields, IExceptionsCodes.General_UC_Failure_MISSING_MANDATORIES);
        }

        utils.checkIfActionIsAllowedInCurrentState(
            orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_CONFIRMED);

        confirmCommand.executeCommand(purchaseOrderCode, confirmValueObject);
        String msgCode = IPurchaseOrderMessages.SUCCESSFUL_CONFIRMATION;
        return getSuccessResponse(msgCode);
      }
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    }
  }

  private DObPurchaseOrderAttachmentValueObject initializeAttachmentValueObject(
      MultipartFile file, String documentTypeCode, String purchaseOrderCode) throws IOException {
    DObPurchaseOrderAttachmentValueObject attachmentValueObject =
        new DObPurchaseOrderAttachmentValueObject();
    attachmentValueObject.setAttachmentContent(file.getBytes());
    attachmentValueObject.setAttachmentName(file.getOriginalFilename());
    attachmentValueObject.setAttachmentTypeCode(documentTypeCode);
    attachmentValueObject.setPurchaseOrderCode(purchaseOrderCode);
    int extensionIndex = file.getOriginalFilename().indexOf('.');
    String attachmentExtension = file.getOriginalFilename().substring(extensionIndex);
    attachmentValueObject.setAttachmentFormat(attachmentExtension);
    return attachmentValueObject;
  }

  private String getSuccessResponse(String msgCode) {
    JsonObject successResponse = new JsonObject();
    String RESPONSE_STATUS = "status";
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    String CODE_RESPONSE_KEY = "code";
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }

  @RequestMapping(method = RequestMethod.POST, value = "/arrival/")
  public @ResponseBody String markAsArrived(@RequestBody String arrivalData) throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.ARRIVAL, arrivalData);

    DObPurchaseOrderMarkAsArrivedValueObject arrivedValueObject =
        utils.getGson().fromJson(arrivalData, DObPurchaseOrderMarkAsArrivedValueObject.class);

    String purchaseOrderCode = arrivedValueObject.getPurchaseOrderCode();
    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    authorizeActionAndLockAllSectionsForNonDraftPurchaseOrder(
        orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_ARRIVED);
    ValidationResult validationResult = null;
    try {
      validationResult =
          purchaseOrderArrivedValidator.validate(
              orderGeneralModel, arrivedValueObject.getArrivalDateTime());
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      } else {

        arrivedCommand.executeCommand(arrivedValueObject);
        return getSuccessResponse(IPurchaseOrderMessages.MARK_AS_ARRIVED_SUCCESSFUL);
      }
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    }
  }

  @RequestMapping(
      method = RequestMethod.PUT,
      value = "/markasproductionfinished/{purchaseOrderCode}")
  public @ResponseBody String markAsProductionsFinished(
      @PathVariable String purchaseOrderCode, @RequestBody String markAsProductionFinishedData)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DobPurchaseOrderMarkAsProductionFinishedValueObject.class, markAsProductionFinishedData);
    try {
      DObPurchaseOrderGeneralModel orderGeneralModel =
          purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
      utils.checkAuthorizeForActionOnBusinessObject(
          orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_PRODUCTION_FINISHED);
    } catch (InstanceNotExistException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }

    DobPurchaseOrderMarkAsProductionFinishedValueObject
        purchaseOrderMarkAsProductionFinishedValueObject =
            utils
                .getGson()
                .fromJson(
                    markAsProductionFinishedData,
                    DobPurchaseOrderMarkAsProductionFinishedValueObject.class);

    purchaseOrderMarkAsProductionFinishedValueObject.setPurchaseOrderCode(purchaseOrderCode);
    entityLockCommand.lockAllSections(purchaseOrderCode);
    try {
      ValidationResult validationResult =
          purchaseOrderMarkAsProductionFinishedValidator.validate(
              purchaseOrderCode,
              purchaseOrderMarkAsProductionFinishedValueObject.getProductionFinishedDate());
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      } else {
        utils.checkIfActionIsAllowedInCurrentState(
            purchaseOrderCode, IPurchaseOrderActionNames.MARK_AS_PRODUCTION_FINISHED);

        markAsProductionFinishedCommand.executeCommand(
            purchaseOrderMarkAsProductionFinishedValueObject);
        return getSuccessResponse(IPurchaseOrderMessages.PRODUCTION_FINISHED_SUCCESSFUL);
      }
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/markascleared/{purchaseOrderCode}")
  public @ResponseBody String markPurchaseOrderAsCleared(
      @PathVariable String purchaseOrderCode, @RequestBody String confirmationData)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DobPurchaseOrderMarkAsClearedValueObject.class, confirmationData);

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    try {
      utils.checkAuthorizeForActionOnBusinessObject(
          orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_CLEARED);
    } catch (InstanceNotExistException exception) {
      AuthorizationException authorizationException = new AuthorizationException(exception);
      throw authorizationException;
    }

    LockDetails lockDetails = null;
    try {
      lockDetails = entityLockCommand.lockAllSections(purchaseOrderCode);

      DobPurchaseOrderMarkAsClearedValueObject clearValueObject =
          utils
              .getGson()
              .fromJson(confirmationData, DobPurchaseOrderMarkAsClearedValueObject.class);

      ValidationResult validationResult =
          purchaseOrderMarkAsClearedValidator.validate(
              purchaseOrderCode, clearValueObject.getClearanceDateTime());

      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      }

      utils.checkIfActionIsAllowedInCurrentState(
          purchaseOrderCode, IPurchaseOrderActionNames.MARK_AS_CLEARED);

      Map<String, List<String>> missingFields =
          orderGeneralModelsMandatoryValidator.validateAllSectionsForState(
              purchaseOrderCode, DObImportPurchaseOrderStateMachine.CLEARED_STATE);

      if (utils.arrivedPOHasMissingFields(missingFields, orderGeneralModel)) {
        return utils.getFailureResponseWithValidationErrorsAndCode(
            missingFields, utils.MISSING_FIELDS_CODE);
      }

      markAsClearedCommand.executeCommand(purchaseOrderCode, clearValueObject);

      return getSuccessResponse(IPurchaseOrderMessages.SUCCESSFUL_CLEARANCE);
    } finally {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(purchaseOrderCode);
      }
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/markasdeliverycomplete/{purchaseOrderCode}")
  public @ResponseBody String markPurchaseOrderAsDeliveryComplete(
      @PathVariable String purchaseOrderCode, @RequestBody String deliveryCompleteValueObject)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DObPurchaseOrderDeliveryCompleteValueObject.class, deliveryCompleteValueObject);
    try {
      DObPurchaseOrderGeneralModel orderGeneralModel =
          purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
      utils.checkAuthorizeForActionOnBusinessObject(
          orderGeneralModel, IPurchaseOrderActionNames.MARK_AS_DELIVERY_COMPLETE);
    } catch (InstanceNotExistException exception) {
      AuthorizationException authorizationException = new AuthorizationException(exception);
      throw authorizationException;
    }
    LockDetails lockDetails = null;

    try {
      lockDetails = entityLockCommand.lockAllSections(purchaseOrderCode);

      DObPurchaseOrderDeliveryCompleteValueObject deliveryComplete =
          utils
              .getGson()
              .fromJson(
                  deliveryCompleteValueObject, DObPurchaseOrderDeliveryCompleteValueObject.class);

      ValidationResult validationResult =
          purchaseOrderMarkAsDeliveryCompleteValidator.validate(
              purchaseOrderCode, deliveryComplete.getDeliveryCompleteDateTime());
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      } else {
        Map<String, List<String>> missingFields =
            orderGeneralModelsMandatoryValidator.validateAllSectionsForState(
                purchaseOrderCode, DObImportPurchaseOrderStateMachine.DELIVERY_COMPLETE_STATE);

        if (!missingFields.isEmpty()) {
          return utils.getFailureResponseWithValidationErrorsAndCode(
              missingFields, utils.MISSING_FIELDS_CODE);
        }

        utils.checkIfActionIsAllowedInCurrentState(
            purchaseOrderCode, IPurchaseOrderActionNames.MARK_AS_DELIVERY_COMPLETE);

        markAsDeliveryCompleteCommand.executeCommand(purchaseOrderCode, deliveryComplete);
        return getSuccessResponse(IPurchaseOrderMessages.SUCCESSFUL_DELIVERY_COMPLETE);
      }
    } finally {
      if (lockDetails != null) {
        entityLockCommand.unlockAllSections(purchaseOrderCode);
      }
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/cancel/{purchaseOrderCode}")
  public @ResponseBody String cancelPurchaseOrder(@PathVariable String purchaseOrderCode)
      throws Exception {

    authorizeActionAndLockAllSectionsForNonDraftPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.CANCEL);
    try {
      stateTransitionCommonValidator.validateActionOnNonDraftPurchaseOrder(
          purchaseOrderCode, IPurchaseOrderActionNames.CANCEL);
      cancelCommand.executeCommand(purchaseOrderCode);
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    }
    return getSuccessResponse(IPurchaseOrderMessages.PO_CANCELLED_SUCCESSFULLY);
  }

  @RequestMapping(
      method = RequestMethod.DELETE,
      value = "/delete/quantity/{orderCode}/{itemCode}/{quantityId}")
  public @ResponseBody String deleteItemQuantityInPurchaseOrder(
      @PathVariable String orderCode, @PathVariable String itemCode, @PathVariable Long quantityId)
      throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(orderCode);
    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.UPDATE_ITEM);
    entityLockCommand.checkIfEntityExists(orderCode);

    try {

      utils.checkIfActionIsAllowedInCurrentState(orderCode, IPurchaseOrderActionNames.UPDATE_ITEM);
    } catch (ActionNotAllowedPerStateException actionNotAllowedPerStateException) {
      throw new RecordDeleteNotAllowedPerStateException();
    }

    entityLockCommand.lockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    try {
      deleteItemQuantityCommand.executeCommand(orderCode, itemCode, quantityId);
    } catch (RecordOfInstanceNotExistException e) {
      entityLockCommand.unlockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      throw e;
    }
    entityLockCommand.unlockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);

    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.RECORD_DELETION_SUCCESSFUL_CODE);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/lock/itemsection/qtyindnpl/{orderCode}/{itemCode}")
  public @ResponseBody String requestEditQtyInDnPl(
      @PathVariable String orderCode, @PathVariable String itemCode) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(orderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.ADD_QTY_IN_DN_PL);

    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);

    IObOrderLineDetails orderLineDetails = utils.readOrderLineByPOAndItemCode(orderCode, itemCode);

    utils.checkPOItemsExist(orderLineDetails);

    try {
      utils.checkIfActionIsAllowedInCurrentState(
          orderCode, IPurchaseOrderActionNames.ADD_QTY_IN_DN_PL);
    } catch (ActionNotAllowedPerStateException actionNotAllowedPerStateException) {
      throw new EditNotAllowedPerStateException();
    }

    entityLockCommand.lockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);

    return utils.getItemEditConfiguration(
        purchaseOrderGeneralModelRep, itemsQtyInDnPlEditabilityManager, orderCode);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/save/qtyindnpl/{orderCode}/{itemCode}")
  public @ResponseBody String saveQtyInDnPl(
      @PathVariable String orderCode,
      @PathVariable String itemCode,
      @RequestBody String qtyInDnPlJsonObject)
      throws Exception {
    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.SAVE_ITEM_QUANTITY_IN_DN_PL, qtyInDnPlJsonObject);

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(orderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.ADD_QTY_IN_DN_PL);

    utils.initOrderLineDetailsReps(
        orderLineDetailsRep,
        orderLineDetailsGeneralModelRep,
        purchaseOrderGeneralModelRep,
        orderLineDetailsQuantitiesRep);

    IObOrderLineDetailsValueObject orderLineDetailsValueObject =
        new Gson().fromJson(qtyInDnPlJsonObject, IObOrderLineDetailsValueObject.class);
    orderLineDetailsValueObject.setItemCode(itemCode);
    orderLineDetailsValueObject.setPurchaseOrderCode(orderCode);

    orderItemsMandatoryValidator.validateValueObjectMandatories(orderLineDetailsValueObject);

    IObOrderLineDetails orderLineDetails = utils.readOrderLineByPOAndItemCode(orderCode, itemCode);

    utils.checkPOItemsExist(orderLineDetails);

    utils.checkIfActionIsAllowedInCurrentState(
        orderCode, IPurchaseOrderActionNames.ADD_QTY_IN_DN_PL);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);

    try {
      saveItemQtyInDnPlCommand.executeCommand(orderLineDetailsValueObject);
    } finally {
      entityLockCommand.unlockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    }
    return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/itemsection/qtyindnpl/{orderCode}/{itemCode}")
  public @ResponseBody String requestCancelQtyInDnPl(
      @PathVariable String orderCode, @PathVariable String itemCode) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(orderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        orderGeneralModel, IPurchaseOrderActionNames.ADD_QTY_IN_DN_PL);

    IObOrderLineDetails orderLineDetails = utils.readOrderLineByPOAndItemCode(orderCode, itemCode);

    utils.checkPOItemsExist(orderLineDetails);

    entityLockCommand.unlockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);

    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/submitforapproval/{purchaseOrderCode}")
  public @ResponseBody String submitForApproval(@PathVariable String purchaseOrderCode)
      throws Exception {

    authorizeActionAndLockAllSectionsForNonDraftPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.SUBMIT_FOR_APPROVAL);
    try {
      stateTransitionCommonValidator.validateActionOnNonDraftPurchaseOrder(
          purchaseOrderCode, IPurchaseOrderActionNames.SUBMIT_FOR_APPROVAL);
      Map<String, List<String>> missingFields =
          orderGeneralModelsMandatoryValidator.validateAllSectionsForState(
              purchaseOrderCode, DObImportPurchaseOrderStateMachine.WAITING_APPROVAL);
      if (!missingFields.isEmpty()) {
        return utils.getFailureResponseWithValidationErrorsAndCode(
            missingFields, utils.MISSING_FIELDS_CODE);
      }
      submitForApprovalCommand.executeCommand(purchaseOrderCode);
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    }
    return getSuccessResponse(IPurchaseOrderMessages.SUBMITTED_FOR_APPROVAL_SUCCESSFULLY);
  }

  private void authorizeActionAndLockAllSectionsForNonDraftPurchaseOrder(
      String purchaseOrderCode, String actionName) throws Exception {
    try {
      DObPurchaseOrderGeneralModel orderGeneralModel =
          purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
      utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, actionName);
      entityLockCommand.lockAllSections(purchaseOrderCode);
    } catch (InstanceNotExistException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  private void authorizeActionAndLockAllSectionsForNonDraftPurchaseOrder(
      DObPurchaseOrderGeneralModel orderGeneralModel, String actionName) throws Exception {
    try {
      utils.checkAuthorizeForActionOnBusinessObject(orderGeneralModel, actionName);
      entityLockCommand.lockAllSections(orderGeneralModel.getUserCode());
    } catch (InstanceNotExistException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/update/OrderItems/{purchaseOrderCode}")
  public @ResponseBody String updateOrderItemsSection(
      @PathVariable String purchaseOrderCode, @RequestBody String orderItemJsonObject)
      throws Exception {
    try {
      CommonControllerUtils.applySchemaValidationForString(
          IDObPurchaseOrderJsonSchema.SAVE_ITEM_QUANTITY_IN_DN_PL, orderItemJsonObject);

      utils.applyAuthorization(
          purchaseOrderGeneralModelRep, purchaseOrderCode, IPurchaseOrderActionNames.UPDATE_ITEM);
      utils.applyAuthorizationOnItemSectionFields();

      entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
          purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);

      IObOrderLineDetailsValueObject orderLineDetailsValueObject =
          (IObOrderLineDetailsValueObject)
              CommonControllerUtils.deserializeRequestBody(
                  orderItemJsonObject, IObOrderLineDetailsValueObject.class);
      orderLineDetailsValueObject.setPurchaseOrderCode(purchaseOrderCode);

      orderItemsMandatoryValidator.validateValueObjectMandatories(orderLineDetailsValueObject);
      ValidationResult validationResult = orderItemsValidator.validate(orderLineDetailsValueObject);
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      }

      orderSaveItemCommand.executeCommand(orderLineDetailsValueObject);
      entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      return getSuccessResponse(SAVING_SUCCESSFUL_CODE);

    } catch (ArgumentViolationSecurityException argumentViolationSecurityException) {
      entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      throw argumentViolationSecurityException;
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/openforupdates/{purchaseOrderCode}")
  public @ResponseBody String openForUpdates(@PathVariable String purchaseOrderCode)
      throws Exception {

    authorizeActionAndLockAllSectionsForNonDraftPurchaseOrder(
        purchaseOrderCode, IPurchaseOrderActionNames.OPEN_FOR_UPDATES);
    try {
      DObPurchaseOrder purchaseOrder = purchaseOrderRep.findOneByUserCode(purchaseOrderCode);

      stateTransitionCommonValidator.validateActionOnNonDraftPurchaseOrder(
          purchaseOrder, IPurchaseOrderActionNames.OPEN_FOR_UPDATES);
      openForUpdatesCommand.executeCommand(purchaseOrder);
    } finally {
      entityLockCommand.unlockAllSections(purchaseOrderCode);
    }
    return getSuccessResponse(IPurchaseOrderMessages.OPENED_FOR_UPDATES_SUCCESSFULLY);
  }

  @RequestMapping(
      method = RequestMethod.PUT,
      value = "/save/quantity/{orderCode}/{itemCode}/{quantityId}")
  public @ResponseBody String saveItemQuantity(
      @PathVariable String orderCode,
      @PathVariable String itemCode,
      @PathVariable Long quantityId,
      @RequestBody String quantityJsonObject)
      throws Exception {
    try {
      CommonControllerUtils.applySchemaValidationForString(
          IDObPurchaseOrderJsonSchema.SAVE_ITEM_QUANTITY, quantityJsonObject);

      utils.applyAuthorization(
          purchaseOrderGeneralModelRep, orderCode, IPurchaseOrderActionNames.UPDATE_ITEM);
      utils.applyAuthorizationOnItemQuantitySectionFields();

      IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject =
          new Gson().fromJson(quantityJsonObject, IObOrderLineDetailsQuantitiesValueObject.class);
      quantitiesValueObject.setItemCode(itemCode);
      quantitiesValueObject.setOrderCode(orderCode);
      quantitiesValueObject.setQuantityId(quantityId);

      utils.initOrderLineDetailsReps(
          orderLineDetailsRep,
          orderLineDetailsGeneralModelRep,
          purchaseOrderGeneralModelRep,
          orderLineDetailsQuantitiesRep);

      IObOrderLineDetails orderLineDetails =
          utils.readOrderLineByPOAndItemCode(orderCode, itemCode);

      orderItemsMandatoryValidator.validateQuantityValueObjectMandatories(quantitiesValueObject);

      utils.checkPOItemsExist(orderLineDetails);
      entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
          orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      utils.checkIfActionIsAllowedInCurrentState(orderCode, IPurchaseOrderActionNames.UPDATE_ITEM);
      try {
        ValidationResult validationResult =
            saveItemQuantityValidator.validate(quantitiesValueObject);
        if (!validationResult.isValid()) {
          return getFailureResponse(validationResult);
        }
      } catch (ArgumentViolationSecurityException argumentViolationSecurityException) {
        entityLockCommand.unlockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
        throw argumentViolationSecurityException;
      }

      try {
        saveItemQuantityCommand.executeCommand(quantitiesValueObject);
      } finally {
        entityLockCommand.unlockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      }
      return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/add/quantity/{orderCode}/{itemCode}")
  public @ResponseBody String addItemQuantity(
      @PathVariable String orderCode,
      @PathVariable String itemCode,
      @RequestBody String quantityJsonObject)
      throws Exception {
    try {

      CommonControllerUtils.applySchemaValidationForString(
          IDObPurchaseOrderJsonSchema.SAVE_ITEM_QUANTITY, quantityJsonObject);

      utils.applyAuthorization(
          purchaseOrderGeneralModelRep, orderCode, IPurchaseOrderActionNames.UPDATE_ITEM);
      utils.applyAuthorizationOnItemQuantitySectionFields();

      IObOrderLineDetailsQuantitiesValueObject quantitiesValueObject =
          new Gson().fromJson(quantityJsonObject, IObOrderLineDetailsQuantitiesValueObject.class);
      quantitiesValueObject.setItemCode(itemCode);
      quantitiesValueObject.setOrderCode(orderCode);
      utils.initOrderLineDetailsReps(
          orderLineDetailsRep,
          orderLineDetailsGeneralModelRep,
          purchaseOrderGeneralModelRep,
          orderLineDetailsQuantitiesRep);

      IObOrderLineDetails orderLineDetails =
          utils.readOrderLineByPOAndItemCode(orderCode, itemCode);

      orderItemsMandatoryValidator.validateQuantityValueObjectMandatories(quantitiesValueObject);

      utils.checkPOItemsExist(orderLineDetails);
      entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
          orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      utils.checkIfActionIsAllowedInCurrentState(orderCode, IPurchaseOrderActionNames.UPDATE_ITEM);

      ValidationResult validationResult = saveItemQuantityValidator.validate(quantitiesValueObject);
      if (!validationResult.isValid()) {
        return getFailureResponse(validationResult);
      }
      try {
        saveItemQuantityCommand.executeCommand(quantitiesValueObject);
      } finally {
        entityLockCommand.unlockSection(orderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
      }
      return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @Override
  public void afterPropertiesSet() {
    companyControllerUtils = new IObCompanyCommandControllerUtils(authorizationManager);
    companyControllerUtils.setPurchaseOrderCompanyEditabilityManager(
        purchaseOrderCompanySectionEditabilityManager);
    utils = new DObPurchaseOrderControllerUtils(authorizationManager);
    utils.setOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    utils.setPurchasingUnitGeneralModelRep(cObPurchasingUnitGeneralModelRep);
    utils.setVendorPurchaseUnitGeneralModelRep(vendorPurchaseUnitGeneralModelRep);
    utils.setEntityLockCommand(entityLockCommand);
    commonUtils = new CommonControllerUtils(authorizationManager);
    itemsControllerUtils = new IObItemsCommandControllerUtils(authorizationManager);
    itemsControllerUtils.setEditabilityManager(servicePurchaseOrderItemsSectionEditabilityManager);
  }
}
