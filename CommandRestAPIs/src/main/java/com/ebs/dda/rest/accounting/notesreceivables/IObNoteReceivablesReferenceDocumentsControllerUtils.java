package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableReferenceDocumentsEditabilityManager;
import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableReferenceDocumentsValueObjectMandatoryAttributes;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesActionNames;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesSectionNames;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNote;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.Set;

public class IObNoteReceivablesReferenceDocumentsControllerUtils
    extends DObNoteReceivablesControllerUtils {

  private EntityLockCommand<DObMonetaryNotes> entityLockCommand;
  private IObNotesReceivableReferenceDocumentsEditabilityManager
      notesReceivableReferenceDocumentsEditabilityManager;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObNoteReceivablesReferenceDocumentsControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObMonetaryNotes> entityLockCommand) {
    super(authorizationManager, entityLockCommand);
    this.entityLockCommand = entityLockCommand;
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void lockNotesReceivableSectionForEdit(
      String notesReceivableCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(notesReceivableCode, sectionName);
    checkIfUpdateActionIsAllowedInNotesReceivableCurrentState(
        notesReceivableCode, sectionName, actionName);
  }

  public void unlockSection(String sectionName, String notesReceivableCode) throws Exception {
    entityLockCommand.unlockSection(notesReceivableCode, sectionName);
  }

  public Set<String> getEnableAttributes(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return notesReceivableReferenceDocumentsEditabilityManager
        .getEnabledAttributesConfig()
        .get(notesReceivablesGeneralModel.getCurrentState());
  }

  public String[] getMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IObNotesReceivableReferenceDocumentsValueObjectMandatoryAttributes.MANDATORIES,
            currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public AuthorizedPermissionsConfig getAddReferenceDocumentsAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME,
        IDObNotesReceivablesActionNames.READ_REFERENCE_DOCUMENTS,
        "ReadReferenceDocumentTypes");
    permissionsConfig.addPermissionConfig(
        IDObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL, "ReadSalesInvoices");
    permissionsConfig.addPermissionConfig(
        IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadSalesOrders");
    permissionsConfig.addPermissionConfig(
        IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME,
        IActionsNames.READ_ALL,
        "ReadNotesReceivables");
    permissionsConfig.addPermissionConfig(
        IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL, "ReadDebitNotes");
    return permissionsConfig;
  }

  public IObNotesReceivableReferenceDocumentDeletionValueObject
      constructNotesReceivableReferenceDocumentDeletionValueObject(
          String notesReceivableCode, Long referenceDocumentIdPathVariable, String referenceDocumentType) {
    IObNotesReceivableReferenceDocumentDeletionValueObject valueObject =
        new IObNotesReceivableReferenceDocumentDeletionValueObject();
    valueObject.setNotesReceivableCode(notesReceivableCode);
    valueObject.setReferenceDocumentId(referenceDocumentIdPathVariable);
    valueObject.setReferenceDocumentType(referenceDocumentType);
    return valueObject;
  }

  public void checkIfReferenceDocumentExists(
      String notesReceivableCode,
      IObNotesReceivablesReferenceDocumentGeneralModel
          notesReceivablesReferenceDocumentGeneralModel)
      throws Exception {
    if (notesReceivablesReferenceDocumentGeneralModel == null) {
      entityLockCommand.unlockSection(notesReceivableCode, IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION);
      throw new RecordOfInstanceNotExistException(IObMonetaryNotesReferenceDocuments.class.getName());
    }
  }

  public void setNotesReceivableReferenceDocumentsEditabilityManager(
      IObNotesReceivableReferenceDocumentsEditabilityManager
          notesReceivableReferenceDocumentsEditabilityManager) {
    this.notesReceivableReferenceDocumentsEditabilityManager =
        notesReceivableReferenceDocumentsEditabilityManager;
  }
}
