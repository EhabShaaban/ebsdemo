package com.ebs.dda.rest.accounting.paymentrequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.DObPaymentRequestAuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class IObAccountingDetailsControllerUtils extends CommonControllerUtils {

	public final String MISSING_FIELDS_CODE = "Gen-msg-40";
	private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
	private final String MISSING_FIELDS_KEY = "missingFields";
	private EntityLockCommand<DObPaymentRequest> entityLockCommand;
	private DObPaymentRequestRep paymentRequestRep;
	private MandatoryValidatorUtils mandatoryValidatorUtils;
	private DObPaymentRequestAuthorizationManager authorizationManager;

	public IObAccountingDetailsControllerUtils() {
		mandatoryValidatorUtils = new MandatoryValidatorUtils();
	}

	@Override
	public List<String> getAuthorizedReads(
					AuthorizedPermissionsConfig authorizedPermissionsConfig) {
		List<AuthorizedPermissionsConfig.AuthorizedPermissionConfig> permissionsConfig = authorizedPermissionsConfig
						.getPermissionsConfig();
		List<String> authorizedReads = new ArrayList<>();
		for (AuthorizedPermissionsConfig.AuthorizedPermissionConfig config : permissionsConfig) {
			try {
				authorizationManager.authorizeReadAction(config.getEntitySysName(),
								config.getPermissionToCheck());
				authorizedReads.add(config.getPermissionToReturn());
			} catch (AuthorizationException | ConditionalAuthorizationException exception) {
			}
		}
		return authorizedReads;
	}

	protected String unlockSection(String sectionName, String resourceCode) throws Exception {
		entityLockCommand.unlockSection(resourceCode, sectionName);
		return getSuccessResponse(SUCCESS_RESPONSE_KEY);
	}

	public void setEntityLockCommand(EntityLockCommand<DObPaymentRequest> entityLockCommand) {
		this.entityLockCommand = entityLockCommand;
	}

	public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
		this.paymentRequestRep = paymentRequestRep;
	}

	public void setAuthorizationManager(
					DObPaymentRequestAuthorizationManager authorizationManager) {
		this.authorizationManager = authorizationManager;
	}

	//TODO: Remove it and use CommonControllerUtils.getFailureResponseWithMissingFields instead
	public String getFailureResponseWithValidationErrorsAndCode(
					Map<String, List<String>> validationErrors, String code) {
		JsonParser parser = new JsonParser();
		JsonObject response = new JsonObject();
		response.add(MISSING_FIELDS_KEY, parser.parse(new Gson().toJson(validationErrors)));
		response.addProperty(RESPONSE_STATUS, FAIL_RESPONSE_KEY);
		response.addProperty(CODE_RESPONSE_KEY, code);
		return response.toString();
	}
}
