package com.ebs.dda.rest.accounting.notesreceivables.activation;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/accounting/notesreceivables")
public class DObNotesReceivableActivationController {

  private final ActivateNotesReceivable activateNotesReceivable;


  public DObNotesReceivableActivationController(ActivateNotesReceivable activateNotesReceivable) {
    this.activateNotesReceivable = activateNotesReceivable;
  }
  
  @PutMapping("/{notesReceivableCode}/activate")
  public @ResponseBody String activateNotesReceivable(@PathVariable String notesReceivableCode,
      @RequestBody String valueObject) throws Exception {


    return activateNotesReceivable.activate(
        NotesReceivableActivateContextBuilder
          .newContext()
          .objectCode(notesReceivableCode)
          .valueObject(valueObject)
        );
  }

}
