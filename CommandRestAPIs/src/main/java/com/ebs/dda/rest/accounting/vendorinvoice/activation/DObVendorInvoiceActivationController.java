package com.ebs.dda.rest.accounting.vendorinvoice.activation;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/accounting/vendorinvoice")
public class DObVendorInvoiceActivationController {

    @Autowired
    private VendorInvoiceBeanFactory activateBeanFactory;

    @PutMapping("/{vendorInvoiceCode}/activate")
    public @ResponseBody
    String activateVendorInvoice(@PathVariable String vendorInvoiceCode,
                                 @RequestBody String valueObject,
                                 @RequestParam(name = "type") String vendorInvoiceType) throws Exception {

        ActivateVendorInvoice activateVendorInvoice = activateBeanFactory.getBean(vendorInvoiceType);

        return activateVendorInvoice.activate(VendorInvoiceActivateContextBuilder.newContext()
                .objectCode(vendorInvoiceCode).valueObject(valueObject));
    }

}
