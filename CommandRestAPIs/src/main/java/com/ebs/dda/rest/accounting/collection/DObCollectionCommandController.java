package com.ebs.dda.rest.accounting.collection;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserException;
import com.ebs.dac.foundation.exceptions.operational.locking.ResourceAlreadyLockedBySameUserInSameSessionException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObCollectionAuthorizationManager;
import com.ebs.dda.accounting.collection.apis.IDObCollectionJsonSchema;
import com.ebs.dda.commands.accounting.collection.DObCollectionCreateCommand;
import com.ebs.dda.commands.accounting.collection.DObCollectionDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.DObCollectionCreateValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.ICObCollectionType;
import com.ebs.dda.jpa.accounting.collection.IDObCollection;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.collection.DObCollectionCreationValidator;

@RestController
@RequestMapping(value = "command/accounting/collection")
public class DObCollectionCommandController implements InitializingBean {
  @Autowired
  DObCollectionAuthorizationManager collectionAuthorizationManager;
  private CommonControllerUtils commonControllerUtils;
  private DObCollectionCommandControllerUtils utils;
  @Autowired
  private AuthorizationManager authorizationManager;
  @Autowired
  private DObCollectionCreationValidator collectionCreationValidator;
  @Autowired
  private DObCollectionCreateCommand collectionCreateCommand;
  @Autowired
  private DObCollectionDeleteCommand collectionDeleteCommand;
  @Autowired
  private DObCollectionGeneralModelRep collectionGeneralModelRep;
  @Autowired
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  @Autowired
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired
  private DObNotesReceivablesGeneralModelRep notesReceivableGeneralModelRep;
  @Autowired
  private EntityLockCommand<DObCollection> collectionEntityLockCommand;

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createCollection(@RequestBody String collectionData)
      throws Exception {
    CommonControllerUtils.applySchemaValidationForString(IDObCollectionJsonSchema.CREATION_SCHEMA,
        collectionData);

    DObCollectionCreateValueObject collectionCreateValueObject =
        (DObCollectionCreateValueObject) CommonControllerUtils
            .deserializeRequestBody(collectionData, DObCollectionCreateValueObject.class);

    utils.applyAuthorizationForCreation(collectionCreateValueObject);

    ValidationResult validationResult =
        collectionCreationValidator.validate(collectionCreateValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      final StringBuilder userCode = new StringBuilder();

      collectionCreateCommand.executeCommand(collectionCreateValueObject)
          .subscribe(code -> userCode.append(code));
      return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
    }
  }

  @DeleteMapping(value = "/{collectionCode}")
  public @ResponseBody String deleteCollection(@PathVariable String collectionCode)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel =
        utils.getCollectionGeneralModel(collectionCode);
    LockDetails lockDetails = null;

    try {
      lockDetails = collectionEntityLockCommand.lockAllSections(collectionCode);

      utils.authorizeActionOnObject(collectionGeneralModel, IActionsNames.DELETE);
      utils.checkIfDeleteIsAllowedInCurrentState(collectionGeneralModel);
      collectionDeleteCommand.executeCommand(collectionCode);

      return commonControllerUtils.getSuccessDeletionResponse();
    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException();
    } catch (ResourceAlreadyLockedBySameUserException ex) {
      throw new ResourceAlreadyLockedBySameUserInSameSessionException();
    } finally {
      if (lockDetails != null)
        collectionEntityLockCommand.unlockAllSections(collectionCode);
    }
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{collectionCode}")
  public @ResponseBody String closeAllSectionsInCollection(@PathVariable String collectionCode,
      HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    collectionEntityLockCommand.unlockAllResourceLockedSectionsByUser(collectionCode,
        lockedSections);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.CREATION_SUCCESSFUL_CODE);
  }

  @GetMapping
  public @ResponseBody String getCreateConfiguration() {
    authorizationManager.authorizeAction(IDObCollection.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();

    List<String> authorizedReads =
        commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

    return CommonControllerUtils
        .serializeEditConfigurationsResponse(new ArrayList<String>(new HashSet<>(authorizedReads)));
  }

  private AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(ICObCollectionType.SYS_NAME, IActionsNames.READ_ALL,
        "ReadType");
    configuration.addPermissionConfig(DocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL,
        "ReadDocumentOwners");
    configuration.addPermissionConfig(IDObSalesOrder.SYS_NAME, IActionsNames.READ_ALL,
        "ReadSalesOrders");
    configuration.addPermissionConfig(IDObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL,
        "ReadSalesInvoices");
    configuration.addPermissionConfig(IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME,
        IActionsNames.READ_ALL, "ReadNotesReceivable");

    return configuration;
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    utils = new DObCollectionCommandControllerUtils(authorizationManager);
    utils.setCollectionGeneralModelRep(collectionGeneralModelRep);
    utils.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    utils.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    utils.setNotesReceivableGeneralModelRep(notesReceivableGeneralModelRep);
  }
}
