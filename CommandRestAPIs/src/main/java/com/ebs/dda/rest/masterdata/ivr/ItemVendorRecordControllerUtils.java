package com.ebs.dda.rest.masterdata.ivr;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.rest.masterdata.utils.MasterDataControllerUtils;

public class ItemVendorRecordControllerUtils extends MasterDataControllerUtils {

  public ItemVendorRecordControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public void applyAuthorizationOnGeneralDataSectionFields() {
    authorizationManager.authorizeAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
  }
}
