package com.ebs.dda.rest.accounting.paymentrequest.activation;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class PaymentActivateBeanFactory implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ActivatePayment getBean(String paymentType, String method, String dueDocument) {
        if (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.name()) && method.equals(PaymentFormEnum.PaymentForm.BANK.name())) {
            return applicationContext.getBean("BankGeneralPaymentForPOActivate", ActivatePayment.class);
        }
        if (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.name()) && method.equals(PaymentFormEnum.PaymentForm.CASH.name())) {
            return applicationContext.getBean("CashGeneralPaymentForPOActivate", ActivatePayment.class);
        }
        if (paymentType.equals(PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE.name()) && method.equals(PaymentFormEnum.PaymentForm.BANK.name())) {
            return applicationContext.getBean("BankGeneralPaymentBasedOnCreditNoteActivate", ActivatePayment.class);
        }
        if (paymentType.equals(PaymentTypeEnum.PaymentType.BASED_ON_CREDIT_NOTE.name()) && method.equals(PaymentFormEnum.PaymentForm.CASH.name())) {
            return applicationContext.getBean("CashGeneralPaymentBasedOnCreditNoteActivate", ActivatePayment.class);
        }
        if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
                && method.equals(PaymentFormEnum.PaymentForm.BANK.name())
                && dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE.name())) {
            return applicationContext.getBean("BankPaymentForVendorAgainstVendorInvoiceActivate", ActivatePayment.class);
        }
        if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
                && method.equals(PaymentFormEnum.PaymentForm.CASH.name())
                && dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE.name())) {
            return applicationContext.getBean("CashPaymentForVendorAgainstVendorInvoiceActivate", ActivatePayment.class);
        }
        if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
                && method.equals(PaymentFormEnum.PaymentForm.BANK.name())
                && dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name())) {
            return applicationContext.getBean("BankPaymentForVendorAgainstPurchaseOrderActivate", ActivatePayment.class);
        }
        if (paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
                && method.equals(PaymentFormEnum.PaymentForm.CASH.name())
                && dueDocument.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name())) {
            return applicationContext.getBean("CashPaymentForVendorAgainstPurchaseOrderActivate", ActivatePayment.class);
        }
        throw new ArgumentViolationSecurityException();
    }
}
