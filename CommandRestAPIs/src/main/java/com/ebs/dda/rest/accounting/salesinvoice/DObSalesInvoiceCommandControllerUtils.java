package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.salesinvoice.editability.SalesInvoiceBusinessPartnerEditabilityManger;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceFactory;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.ICObPaymentTerms;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.rest.CommonControllerUtils;
import java.util.Set;

public class DObSalesInvoiceCommandControllerUtils extends CommonControllerUtils {

  private EntityLockCommand<DObSalesInvoice> entityLockCommand;
  private DObSalesInvoiceRep saleInvoiceRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private SalesInvoiceBusinessPartnerEditabilityManger salesInvoiceBusinessPartnerEditabilityManger;

  public DObSalesInvoiceCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public void checkIfActionIsAllowedInCurrentState(
      StatefullBusinessObject invoice, String actionName) throws Exception {
    DObSalesInvoiceFactory salesInvoiceStateMachine = new DObSalesInvoiceFactory();
    salesInvoiceStateMachine.createSalesInvoiceStateMachine(invoice);
    salesInvoiceStateMachine.isAllowedAction(invoice, actionName);
  }

  public void checkIfDeleteIsAllowedInCurrentState(
      DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel, String actionName) throws Exception {
    DObSalesInvoiceFactory invoiceStateMachine = new DObSalesInvoiceFactory();
    invoiceStateMachine.createSalesInvoiceStateMachine(dObSalesInvoiceGeneralModel);
    try {
      invoiceStateMachine.isAllowedAction(dObSalesInvoiceGeneralModel, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException(e);
    }
  }

  public void checkIfEditIsAllowedInCurrentState(
      DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel, String actionName) throws Exception {
    DObSalesInvoiceFactory invoiceStateMachine = new DObSalesInvoiceFactory();
    invoiceStateMachine.createSalesInvoiceStateMachine(dObSalesInvoiceGeneralModel);
    try {
      invoiceStateMachine.isAllowedAction(dObSalesInvoiceGeneralModel, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      throw new EditNotAllowedPerStateException(e);
    }
  }

  public void lockSalesInvoiceSectionForEdit(
      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel, String sectionName, String actionName)
      throws Exception {
    entityLockCommand.lockSection(salesInvoiceGeneralModel.getUserCode(), sectionName);
    try {
      checkIfEditIsAllowedInCurrentState(salesInvoiceGeneralModel, actionName);
    } catch (EditNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(salesInvoiceGeneralModel.getUserCode(), sectionName);
      throw e;
    }
  }

  protected Set<String> getEnableAttributes(DObSalesInvoiceGeneralModel salesInvoice) {
    return salesInvoiceBusinessPartnerEditabilityManger
        .getEnabledAttributesConfig()
        .get(salesInvoice.getCurrentState());
  }

  protected AuthorizedPermissionsConfig getEditAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(
        ICObPaymentTerms.SYS_NAME, IActionsNames.READ_ALL, "ReadPaymentTerm");
    permissionsConfig.addPermissionConfig(
        ICObCurrency.SYS_NAME, IActionsNames.READ_ALL, "ReadCurrency");
    return permissionsConfig;
  }

  public void setEntityLockCommand(EntityLockCommand<DObSalesInvoice> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setSaleInvoiceRep(DObSalesInvoiceRep saleInvoiceRep) {
    this.saleInvoiceRep = saleInvoiceRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }

  public void setSalesInvoiceBusinessPartnerEditabilityManger(
      SalesInvoiceBusinessPartnerEditabilityManger salesInvoiceBusinessPartnerEditabilityManger) {
    this.salesInvoiceBusinessPartnerEditabilityManger =
        salesInvoiceBusinessPartnerEditabilityManger;
  }

  public void checkIfEntityIsNull(DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel)
      throws InstanceNotExistException {
    if (dObSalesInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }

  public void checkIfEntityIsNull(String salesInvoiceCode) throws InstanceNotExistException {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);
    if (salesInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }
}
