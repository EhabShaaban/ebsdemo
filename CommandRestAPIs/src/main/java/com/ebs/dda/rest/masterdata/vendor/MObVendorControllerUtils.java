package com.ebs.dda.rest.masterdata.vendor;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;

import java.util.List;

public class MObVendorControllerUtils {

  private AuthorizationManager authorizationManager;

  public MObVendorControllerUtils(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void applyAuthorization(
      List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits, String actionName)
      throws Exception {

    if (vendorPurchaseUnits == null || vendorPurchaseUnits.isEmpty()) {
      throw new InstanceNotExistException();
    }
    authorizeActionOnVendor(vendorPurchaseUnits, actionName);
  }

  private void authorizeActionOnVendor(
      List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits, String actionName)
      throws Exception {
    boolean isAuthorized = false;
    ConditionalAuthorizationException conditionalAuthorizationException = null;
    for (MObVendorPurchaseUnitGeneralModel vendorPurchaseUnit : vendorPurchaseUnits) {
      try {
        authorizationManager.authorizeAction(vendorPurchaseUnit, actionName);
        isAuthorized = true;
        break;
      } catch (ConditionalAuthorizationException e) {
        conditionalAuthorizationException = e;
      }
    }
    if (!isAuthorized) throw conditionalAuthorizationException;
  }
}
