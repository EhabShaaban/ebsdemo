package com.ebs.dda.rest.accounting.settlement.activation;

@FunctionalInterface
public interface ActivateSettlement {
  String activate(SettlementActivateContextBuilder.SettlementActivateContext context)
    throws Exception;
}
