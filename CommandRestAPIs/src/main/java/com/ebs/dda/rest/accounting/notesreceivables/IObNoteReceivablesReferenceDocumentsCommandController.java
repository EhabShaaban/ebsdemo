package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableReferenceDocumentsEditabilityManager;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesActionNames;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivablesSectionNames;
import com.ebs.dda.commands.accounting.notesreceivables.IObNoteReceivablesReferenceDocumentSaveCommand;
import com.ebs.dda.commands.accounting.notesreceivables.IObNotesReceivableReferenceDocumentDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.notesreceivable.IObNotesReceivableRefDocumentBusinessValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static com.ebs.dda.rest.CommonControllerUtils.deserializeRequestBody;

@RestController
@RequestMapping(value = "command/accounting/notesreceivables")
public class IObNoteReceivablesReferenceDocumentsCommandController implements InitializingBean {

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private EntityLockCommand<DObMonetaryNotes> entityLockCommand;
  @Autowired private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;

  @Autowired
  private IObNotesReceivableReferenceDocumentDeleteCommand
      notesReceivableReferenceDocumentDeleteCommand;

  @Autowired
  private IObNotesReceivableRefDocumentBusinessValidator
      notesReceivableRefDocumentBusinessValidator;

  @Autowired
  private IObNotesReceivablesReferenceDocumentGeneralModelRep
      notesReceivablesReferenceDocumentGeneralModelRep;

  @Autowired
  private IObNotesReceivableReferenceDocumentsEditabilityManager
      notesReceivableReferenceDocumentsEditabilityManager;

  @Autowired
  private IObNoteReceivablesReferenceDocumentSaveCommand
      noteReceivablesReferenceDocumentSaveCommand;

  private IObNoteReceivablesReferenceDocumentsControllerUtils controllerUtils;

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{notesReceivableCode}/lock/referencedocuments")
  public @ResponseBody String openReferenceDocumentsSectionEditMode(
      @PathVariable String notesReceivableCode) throws Exception {

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.ADD_REFERENCE_DOCUMENT);

    controllerUtils.lockNotesReceivableSectionForEdit(
        notesReceivableCode,
        IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION,
        IDObNotesReceivablesActionNames.ADD_REFERENCE_DOCUMENT);

    Set<String> stateEnabledAttributes =
        controllerUtils.getEnableAttributes(notesReceivablesGeneralModel);

    String[] mandatories =
        controllerUtils.getMandatoryAttributesByState(
            notesReceivablesGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getAddReferenceDocumentsAuthorizedPermissionsConfig();

    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(mandatories);
    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{notesReceivableCode}/unlock/referencedocuments")
  public @ResponseBody String cancelReferenceDocumentsEditMode(
      @PathVariable String notesReceivableCode) throws Exception {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.ADD_REFERENCE_DOCUMENT);

    controllerUtils.unlockSection(
        IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION, notesReceivableCode);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(
      method = RequestMethod.PUT,
      value = "/{notesReceivableCode}/update/referencedocuments")
  public @ResponseBody String addReferenceDocument(
      @PathVariable String notesReceivableCode, @RequestBody String referenceDocumentJsonData)
      throws Exception {

    final boolean isSavedRefDocBefore = false;
    ValidationResult validationResult = new ValidationResult();

    CommonControllerUtils.applySchemaValidationForString(
            IDObNotesReceivableJsonSchema.SAVE_ADD_REF_DOCUMENT_SCHEMA, referenceDocumentJsonData);

    IObMonetaryNotesReferenceDocumentsValueObject valueObject =
        (IObMonetaryNotesReferenceDocumentsValueObject)
            deserializeRequestBody(
                referenceDocumentJsonData, IObMonetaryNotesReferenceDocumentsValueObject.class);

    valueObject.setNotesReceivableCode(notesReceivableCode);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        notesReceivableCode, IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION);

    notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
        valueObject, isSavedRefDocBefore, validationResult);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    }

    noteReceivablesReferenceDocumentSaveCommand.executeCommand(valueObject);

    entityLockCommand.unlockSection(
        notesReceivableCode, IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(
      method = RequestMethod.DELETE,
      value =
          "/{notesReceivableCode}/delete/referencedocuments/{referenceDocumentId}/{referenceDocumentType}")
  public @ResponseBody String requestDeleteReferenceDocument(
      @PathVariable String notesReceivableCode,
      @PathVariable String referenceDocumentId,
      @PathVariable String referenceDocumentType)
      throws Exception {

    Long referenceDocumentIdPathVariable = Long.valueOf(referenceDocumentId);
    IObNotesReceivableReferenceDocumentDeletionValueObject valueObject =
        controllerUtils.constructNotesReceivableReferenceDocumentDeletionValueObject(
            notesReceivableCode, referenceDocumentIdPathVariable, referenceDocumentType);

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode);

    authorizationManager.authorizeActionOnObject(
        notesReceivablesGeneralModel, IDObNotesReceivablesActionNames.DELETE_REFERENCE_DOCUMENT);

    controllerUtils.lockNotesReceivableSectionForEdit(
        notesReceivableCode,
        IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION,
        IDObNotesReceivablesActionNames.DELETE_REFERENCE_DOCUMENT);

    IObNotesReceivablesReferenceDocumentGeneralModel notesReceivablesReferenceDocumentGeneralModel =
        notesReceivablesReferenceDocumentGeneralModelRep.findOneById(
            valueObject.getReferenceDocumentId());

    controllerUtils.checkIfReferenceDocumentExists(
        notesReceivableCode, notesReceivablesReferenceDocumentGeneralModel);

    notesReceivableReferenceDocumentDeleteCommand.executeCommand(valueObject);

    entityLockCommand.unlockSection(
        notesReceivableCode, IDObNotesReceivablesSectionNames.REFERENCE_DOCUMENTS_SECTION);

    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils =
        new IObNoteReceivablesReferenceDocumentsControllerUtils(
            authorizationManager, entityLockCommand);
    controllerUtils.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    controllerUtils.setNotesReceivableReferenceDocumentsEditabilityManager(
        notesReceivableReferenceDocumentsEditabilityManager);
  }
}
