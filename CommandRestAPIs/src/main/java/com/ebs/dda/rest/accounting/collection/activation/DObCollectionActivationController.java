package com.ebs.dda.rest.accounting.collection.activation;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/accounting/collection")
public class DObCollectionActivationController {

  private CollectionActivateBeanFactory activateBeanFactory;

  public DObCollectionActivationController(CollectionActivateBeanFactory activateBeanFactory) {
    this.activateBeanFactory = activateBeanFactory;
  }
  
  @PutMapping("/{collectionCode}/activate")
  public @ResponseBody String activateCollection(@PathVariable String collectionCode,
      @RequestBody String valueObject,
      @RequestParam(name = "type", required = true) String collectionType) throws Exception {

    ActivateCollection activateCollection = activateBeanFactory.getBean(collectionType);

    return activateCollection.activate(
        CollectionActivateContextBuilder
          .newContext()
          .objectCode(collectionCode)
          .valueObject(valueObject)
        );
  }

}
