package com.ebs.dda.rest.order.purchaseorder;

import static com.ebs.dda.rest.CommonControllerUtils.deserializeRequestBody;
import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.purchaseorder.IObPurchaseOrderDeleteItemCommand;
import com.ebs.dda.commands.order.purchaseorder.IObPurchaseOrderItemsSaveCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.jpa.order.IObPurchaseOrderDeleteItemValueObject;
import com.ebs.dda.jpa.order.IObPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.apis.IDObPurchaseOrderJsonSchema;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.purchaseorder.IObPurchaseOrderItemSaveAddMandatoryValidator;
import com.ebs.dda.validation.purchaseorder.IObPurchaseOrderItemSaveAddValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/purchaseorder")
public class IObPurchaseOrderItemsCommandController implements InitializingBean {

  @Autowired private IObPurchaseOrderDeleteItemCommand purchaseOrderDeleteItemCommand;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private EntityLockCommand<DObOrderDocument> entityLockCommand;
  @Autowired private IObPurchaseOrderItemsSaveCommand purchaseOrderItemsSaveCommand;
  @Autowired private IObOrderItemGeneralModelRep itemGeneralModelRep;
  @Autowired private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;

  @Autowired
  private IObPurchaseOrderItemSaveAddMandatoryValidator purchaseOrderItemSaveAddMandatoryValidator;

  @Autowired private IObPurchaseOrderItemSaveAddValidator purchaseOrderItemSaveAddValidator;

  private IObItemsCommandControllerUtils itemsControllerUtils;

  @RequestMapping(method = RequestMethod.DELETE, value = "{purchaseOrderCode}/items/{itemId}")
  public @ResponseBody String deleteItem(
      @PathVariable String purchaseOrderCode, @PathVariable Long itemId) throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    authorizationManager.authorizeActionOnObject(
        purchaseOrder, IPurchaseOrderActionNames.UPDATE_ITEM);

    IObPurchaseOrderDeleteItemValueObject valueObject =
        itemsControllerUtils.constructDeleteItemValueObject(purchaseOrderCode, itemId);

    itemsControllerUtils.checkIfItemIsNull(itemId, itemGeneralModelRep);
    itemsControllerUtils.checkIfDeleteItemIsAllowed(purchaseOrder);

    entityLockCommand.lockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    try {
      purchaseOrderDeleteItemCommand.executeCommand(valueObject);
    } finally {
      entityLockCommand.unlockSection(purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);
    }

    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{purchaseOrderCode}/update/items")
  public @ResponseBody String addItem(
      @PathVariable String purchaseOrderCode, @RequestBody String itemJsonData) throws Exception {

    DObPurchaseOrderGeneralModel orderGeneralModel =
            purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    itemsControllerUtils.applyAuthorizationForSave(orderGeneralModel);

    CommonControllerUtils.applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.SAVE_ADD_SERVICE_ITEM, itemJsonData);

    IObPurchaseOrderItemValueObject valueObject =
        (IObPurchaseOrderItemValueObject)
            deserializeRequestBody(itemJsonData, IObPurchaseOrderItemValueObject.class);
    valueObject.setPurchaseOrderCode(purchaseOrderCode);

    purchaseOrderItemSaveAddValidator.validateMissingMandatoryFields(valueObject);

    itemsControllerUtils.checkIfActionIsAllowedInCurrentState(
        orderGeneralModel, IActionsNames.UPDATE_ITEM);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        purchaseOrderCode, IPurchaseOrderSectionNames.ITEMS_SECTION);

    ValidationResult validationResult = purchaseOrderItemSaveAddValidator.validate(valueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    }

    try {
      purchaseOrderItemSaveAddMandatoryValidator.validate(valueObject);
      purchaseOrderItemsSaveCommand.executeCommand(valueObject);
    } finally {
      entityLockCommand.unlockSection(
          valueObject.getPurchaseOrderCode(), IPurchaseOrderSectionNames.ITEMS_SECTION);
    }
    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    itemsControllerUtils = new IObItemsCommandControllerUtils(authorizationManager);
  }
}
