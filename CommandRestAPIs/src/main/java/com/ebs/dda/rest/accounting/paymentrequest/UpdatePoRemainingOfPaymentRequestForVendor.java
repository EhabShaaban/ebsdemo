package com.ebs.dda.rest.accounting.paymentrequest;

import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import java.math.BigDecimal;

public class UpdatePoRemainingOfPaymentRequestForVendor
    implements Observer<ExecuteActivateDependanciesContext> {

  private DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand updateRemainingCommand;

  public UpdatePoRemainingOfPaymentRequestForVendor(
      DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand updateRemainingCommand) {
    this.updateRemainingCommand = updateRemainingCommand;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      updateRemainingCommand.executeCommand(
          initValueObject(context.entitiesOfInterestContainer().getMainEntity()));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject initValueObject(
      DObPaymentRequestGeneralModel paymentRequestGeneralModel) {

    DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject valueObject =
        new DObPaymentRequestForVendorAgainstPoUpdateRemainingValueObject();

    valueObject.setPaymentRequestCode(paymentRequestGeneralModel.getUserCode());
    valueObject.setAmountValue(paymentRequestGeneralModel.getAmountValue());

    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
