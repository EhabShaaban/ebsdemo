package com.ebs.dda.rest.accounting.vendorinvoice.taxes;

import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.DObInvoiceAuthorizationManager;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.commands.accounting.vendorinvoice.IObVendorInvoiceTaxesDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesDeletionValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/accounting/vendorinvoice")
public class IObVendorInvoiceTaxesCommandController implements InitializingBean {

  private CommonControllerUtils commonControllerUtils;
  @Autowired private EntityLockCommand<DObVendorInvoice> entityLockCommand;
  private IObVendorInvoiceTaxesCommandControllerUtils iObTaxesCommandControllerUtils;
  @Autowired private DObInvoiceAuthorizationManager invoiceAuthorizationManager;
  @Autowired private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  @Autowired private IObVendorInvoiceTaxesDeleteCommand vendorInvoiceTaxesDeleteCommand;
  @Autowired private IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep;

  @RequestMapping(method = RequestMethod.DELETE, value = "/{vendorInvoiceCode}/taxes/{taxCode}")
  public @ResponseBody String deleteVendorInvoiceItem(
      @PathVariable String vendorInvoiceCode, @PathVariable String taxCode) throws Exception {
    DObVendorInvoiceGeneralModel vendorInvoice = getAndAuthorizationVendorInvoice(vendorInvoiceCode);

    IObVendorInvoiceTaxesDeletionValueObject vendorInvoiceTaxesDeletionValueObject =
        initTaxDeletionValueObject(vendorInvoiceCode,taxCode);

    IObVendorInvoiceTaxesGeneralModel taxesGeneralModel =
        getAndAuthorizationVendorInvoiceTaxes(vendorInvoiceCode, taxCode, vendorInvoice);

    entityLockCommand.lockSection(vendorInvoiceCode, IDObVendorInvoiceSectionNames.TAXES_SECTION);
    try {
      vendorInvoiceTaxesDeleteCommand.executeCommand(vendorInvoiceTaxesDeletionValueObject);
    } finally {
      entityLockCommand.unlockSection(vendorInvoiceCode, IDObVendorInvoiceSectionNames.TAXES_SECTION);
    }
    return getSuccessResponse(CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  private IObVendorInvoiceTaxesDeletionValueObject initTaxDeletionValueObject(
      String vendorInvoiceCode, String taxCode) {
    IObVendorInvoiceTaxesDeletionValueObject vendorInvoiceTaxesDeletionValueObject=
        new IObVendorInvoiceTaxesDeletionValueObject();
    vendorInvoiceTaxesDeletionValueObject.setVendorInvoiceCode(vendorInvoiceCode);
    vendorInvoiceTaxesDeletionValueObject.setTaxCode(taxCode);
    return vendorInvoiceTaxesDeletionValueObject;
  }

  private DObVendorInvoiceGeneralModel getAndAuthorizationVendorInvoice(String vendorInvoiceCode)
      throws Exception {
    DObVendorInvoiceGeneralModel vendorInvoice =
        vendorInvoiceGeneralModelRep.findOneByUserCode(vendorInvoiceCode);
    iObTaxesCommandControllerUtils.checkIfEntityIsNull(vendorInvoice);
    invoiceAuthorizationManager.authorizeDeleteTaxes(vendorInvoice);
    return vendorInvoice;
  }

  private IObVendorInvoiceTaxesGeneralModel getAndAuthorizationVendorInvoiceTaxes(
      String vendorInvoiceCode, String taxCode, DObVendorInvoiceGeneralModel vendorInvoice)
      throws Exception {
    IObVendorInvoiceTaxesGeneralModel taxesGeneralModel =
        vendorInvoiceTaxesGeneralModelRep.findOneByInvoiceCodeAndTaxCode(vendorInvoiceCode, taxCode);
    iObTaxesCommandControllerUtils.checkIfVendorInvoiceTaxExists(taxesGeneralModel);
    iObTaxesCommandControllerUtils.checkIfDeleteIsAllowedInCurrentState(vendorInvoice,
        IActionsNames.DELETE);
    return taxesGeneralModel;
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils =
        new CommonControllerUtils(invoiceAuthorizationManager.getAuthorizationManager());
    iObTaxesCommandControllerUtils = new IObVendorInvoiceTaxesCommandControllerUtils();
    iObTaxesCommandControllerUtils.setEntityLockCommand(entityLockCommand);
    iObTaxesCommandControllerUtils.setVendorInvoiceGeneralModelRep(vendorInvoiceGeneralModelRep);
  }
}
