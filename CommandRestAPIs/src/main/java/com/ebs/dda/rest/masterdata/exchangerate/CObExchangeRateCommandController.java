package com.ebs.dda.rest.masterdata.exchangerate;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.CObExchangeRateAuthorizationManager;
import com.ebs.dda.commands.masterdata.exchagerate.CObExchangeRateCreateCommand;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.exchangerate.CObExchangeRateValueObject;
import com.ebs.dda.jpa.masterdata.exchangerate.ICObExchangeRate;
import com.ebs.dda.masterdata.exchangerate.CObExchangeRateCreateValidator;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "command/masterdata/exchangerate")
public class CObExchangeRateCommandController implements InitializingBean {

    @Autowired
    private CObExchangeRateCreateCommand exchangeRateCreateCommand;
    @Autowired
    private CObExchangeRateCreateValidator cObExchangeRateCreateValidator;
    @Autowired
    private CObExchangeRateAuthorizationManager authorizationManager;

  private CObExchangeRateCommandControllerUtils utils;

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody
  String createExchangeRate(@RequestBody String exchangeRateData)
      throws Exception {
    CObExchangeRateCommandControllerUtils.applySchemaValidation(
            CObExchangeRateValueObject.class, exchangeRateData);

    CObExchangeRateValueObject exchangeRateValueObject =
        (CObExchangeRateValueObject)
                CObExchangeRateCommandControllerUtils.deserializeRequestBody(
                exchangeRateData, CObExchangeRateValueObject.class);
      applyAuthorizationForCreate();
    ValidationResult validationResult =
            cObExchangeRateCreateValidator.validate(exchangeRateValueObject);
    if (!validationResult.isValid()) {
      return CObExchangeRateCommandControllerUtils.getFailureResponse(validationResult);
    }

    Observable codeObservable = exchangeRateCreateCommand.executeCommand(exchangeRateValueObject);
    final StringBuilder userCode = new StringBuilder();
    codeObservable.subscribe(code -> userCode.append(code));

    return utils.getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody
  String getCreateConfiguration() {
      applyAuthorizationForRequestCreate();
      AuthorizedPermissionsConfig createPermissionsConfig = utils.constructCreateConfiguration();
    List<String> authorizedReads = utils.getAuthorizedReads(createPermissionsConfig);
    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return utils.serializeEditConfigurationsResponse(createConfig);
  }

    private void applyAuthorizationForCreate() {
        authorizationManager.authorizeReadAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
        authorizationManager.authorizeReadAction(ICObExchangeRate.SYS_NAME, IActionsNames.CREATE);
    }

    private void applyAuthorizationForRequestCreate() {
        authorizationManager.authorizeReadAction(ICObExchangeRate.SYS_NAME, IActionsNames.CREATE);
    }

  @Override
  public void afterPropertiesSet() {
    utils = new CObExchangeRateCommandControllerUtils();
    utils.setAuthorizationManager(authorizationManager);
  }
}
