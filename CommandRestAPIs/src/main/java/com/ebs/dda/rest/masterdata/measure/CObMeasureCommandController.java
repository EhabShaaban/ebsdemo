package com.ebs.dda.rest.masterdata.measure;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.masterdata.measure.CObMeasureCreateCommand;
import com.ebs.dda.jpa.masterdata.measure.CObMeasureCreationValueObject;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.masterdata.unitofmeasure.CObMeasureJsonSchema;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.masterdata.unitofmeasure.CObMeasureCreateMandatoryValidator;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/measure")
public class CObMeasureCommandController implements InitializingBean {

  @Autowired
  CObMeasureCreateMandatoryValidator validator;
  @Autowired private CObMeasureCreateCommand measureCreateCommand;
  private CObMeasureControllerUtils measureControllerUtils;
  @Autowired private AuthorizationManager authorizationManager;

  @RequestMapping(value = "/", method = RequestMethod.POST)
  public @ResponseBody String createUnitOfMeasure(@RequestBody String creationData)
      throws Exception {

    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.CREATE);

    CommonControllerUtils.applySchemaValidationForString(CObMeasureJsonSchema.CREATE, creationData);

    CObMeasureCreationValueObject valueObject =
        (CObMeasureCreationValueObject)
            CommonControllerUtils.deserializeRequestBody(
                creationData, CObMeasureCreationValueObject.class);
    validator.validate(valueObject);
    final StringBuilder userCode = new StringBuilder();
    Observable<String> codeObservable = measureCreateCommand.executeCommand(valueObject);
    codeObservable.subscribe(
        (code) -> {
          userCode.append(code);
        });
    return measureControllerUtils.getSuccessCreationResponse(userCode.toString());
  }

  @Override
  public void afterPropertiesSet() {
    measureControllerUtils = new CObMeasureControllerUtils(authorizationManager);
  }
}
