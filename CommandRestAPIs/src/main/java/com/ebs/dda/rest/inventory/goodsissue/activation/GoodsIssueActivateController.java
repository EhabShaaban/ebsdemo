package com.ebs.dda.rest.inventory.goodsissue.activation;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/goodsissue")
public class GoodsIssueActivateController {
  private final ActivateGoodsIssue activateGoodsIssue;

  public GoodsIssueActivateController(ActivateGoodsIssue activateGoodsIssue) {
    this.activateGoodsIssue = activateGoodsIssue;
  }

  @PutMapping("/{goodsIssueCode}/activate")
  public @ResponseBody String activate(@PathVariable String goodsIssueCode) throws Exception {
    return activateGoodsIssue.apply(
        GoodsIssueActivateContextBuilder.newContext().objectCode(goodsIssueCode));
  }
}
