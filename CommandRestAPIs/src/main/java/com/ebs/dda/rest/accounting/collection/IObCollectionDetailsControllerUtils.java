package com.ebs.dda.rest.accounting.collection;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.DObCollectionAuthorizationManager;
import com.ebs.dda.accounting.collection.apis.IDObCollectionActionNames;
import com.ebs.dda.accounting.collection.editability.IObCollectionDetailsEditabilityManager;
import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.collection.CollectionMethodEnum;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class IObCollectionDetailsControllerUtils extends CommonControllerUtils {

  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private EntityLockCommand<DObCollection> entityLockCommand;
  private DObCollectionGeneralModelRep collectionGeneralModelRep;
  private IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep;
  private DObCollectionAuthorizationManager authorizationManager;
  private IObCollectionDetailsEditabilityManager collectionDetailsEditabilityManager;

  public IObCollectionDetailsControllerUtils() {}

  public void lockCollectionSectionForEdit(
      String collectionCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(collectionCode, sectionName);
    try {
      checkIfActionIsAllowedInCurrentState(collectionCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(collectionCode, sectionName);
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
  }

  private void checkIfActionIsAllowedInCurrentState(String collectionCode, String actionName)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel =
        collectionGeneralModelRep.findOneByUserCode(collectionCode);
    DObCollectionStateMachine collecStateMachine = new DObCollectionStateMachine();
    collecStateMachine.initObjectState(collectionGeneralModel);
    collecStateMachine.isAllowedAction(actionName);
  }

  @Override
  public List<String> getAuthorizedReads(AuthorizedPermissionsConfig authorizedPermissionsConfig) {
    List<AuthorizedPermissionsConfig.AuthorizedPermissionConfig> permissionsConfig =
        authorizedPermissionsConfig.getPermissionsConfig();
    List<String> authorizedReads = new ArrayList<>();
    for (AuthorizedPermissionsConfig.AuthorizedPermissionConfig config : permissionsConfig) {
      try {
        authorizationManager.authorizeReadAction(
            config.getEntitySysName(), config.getPermissionToCheck());
        authorizedReads.add(config.getPermissionToReturn());
      } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      }
    }
    return authorizedReads;
  }

  protected AuthorizedPermissionsConfig getEditCollectionDetailsAuthorizedPermissionsConfig(
      String collectionCode) {

    IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
        collectionDetailsGeneralModelRep.findByCollectionCode(collectionCode);
    String collectionMethod = collectionDetailsGeneralModel.getCollectionMethod();

    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    if (collectionMethod.equals(CollectionMethodEnum.CollectionMethod.BANK.name())) {
      permissionsConfig.addPermissionConfig(
          ICObCompany.SYS_NAME, IActionsNames.READ_BANK_DETAILS, "ReadCompanyBankDetails");
    } else {
      permissionsConfig.addPermissionConfig(
          ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadTreasuries");
    }

    return permissionsConfig;
  }

  protected void applyAuthorizationForSave(
          DObCollectionGeneralModel collectionGeneralModel)
      throws Exception {
    authorizationManager.authorizeReadAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
    try {
      authorizationManager.authorizeActionOnSection(
              collectionGeneralModel, IDObCollectionActionNames.UPDATE_COLLECTION_DETAILS);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public Set<String> getEnableAttributesByMethod(
      IObCollectionDetailsGeneralModel collectionDetailsGeneralModel) {
    return collectionDetailsEditabilityManager
        .getEnabledAttributesConfig()
        .get(collectionDetailsGeneralModel.getCollectionMethod());
  }

  protected String unlockSection(String sectionName, String resourceCode) throws Exception {
    entityLockCommand.unlockSection(resourceCode, sectionName);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  public void setEntityLockCommand(EntityLockCommand<DObCollection> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setCollectionGeneralModelRep(DObCollectionGeneralModelRep collectionGeneralModelRep) {
    this.collectionGeneralModelRep = collectionGeneralModelRep;
  }

  public void setAuthorizationManager(DObCollectionAuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void setCollectionDetailsGeneralModelRep(
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep) {
    this.collectionDetailsGeneralModelRep = collectionDetailsGeneralModelRep;
  }

  public void setCollectionDetailsEditabilityManager(
      IObCollectionDetailsEditabilityManager collectionDetailsEditabilityManager) {
    this.collectionDetailsEditabilityManager = collectionDetailsEditabilityManager;
  }
}
