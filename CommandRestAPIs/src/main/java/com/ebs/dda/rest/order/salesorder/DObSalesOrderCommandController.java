package com.ebs.dda.rest.order.salesorder;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderApproveCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderCreateCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderDeleteCommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderCreateValueObject;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderDeletionValueObject;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IDObSalesOrder;
import com.ebs.dda.order.salesorder.DObSalesOrderCreateValidator;
import com.ebs.dda.order.salesorder.DObSalesOrderMandatoryValidator;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/order/salesorder")
public class DObSalesOrderCommandController implements InitializingBean {

  @Autowired private EntityLockCommand<DObSalesOrder> entityLockCommand;
  @Autowired private DObSalesOrderCreateCommand salesOrderCreateCommand;
  @Autowired private DObSalesOrderDeleteCommand salesOrderDeleteCommand;
  @Autowired private DObSalesOrderApproveCommand salesOrderApproveCommand;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  @Autowired private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  @Autowired private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;
  @Autowired private DObSalesOrderCreateValidator validator;
  @Autowired private DObSalesOrderMandatoryValidator salesOrderMandatoryValidator;

  private DObSalesOrderControllerUtils utils;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String getCreateConfiguration() {

    authorizationManager.authorizeAction(IDObSalesOrder.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = utils.constructCreateConfiguration();
    List<String> authorizedReads = utils.getAuthorizedReads(createPermissionsConfig);

    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return CommonControllerUtils.serializeEditConfigurationsResponse(createConfig);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createSalesOrder(@RequestBody String salesOrderData)
      throws Exception {

    authorizationManager.authorizeAction(IDObSalesOrder.SYS_NAME, IActionsNames.CREATE);

    CommonControllerUtils.applySchemaValidation(
        DObSalesOrderCreateValueObject.class, salesOrderData);
    DObSalesOrderCreateValueObject valueObject =
        (DObSalesOrderCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                salesOrderData, DObSalesOrderCreateValueObject.class);

    validator.validateMandatoryFields(valueObject);

    utils.applyAuthorizationForCreateValueObjectValues(valueObject);

    ValidationResult validationResult = validator.validate(valueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }
    final StringBuilder userCode = new StringBuilder();
    salesOrderCreateCommand.executeCommand(valueObject).subscribe(code -> userCode.append(code));
    return utils.getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{salesOrderCode}")
  public @ResponseBody String closeAllSectionsInSalesOrder(
      @PathVariable String salesOrderCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(salesOrderCode, lockedSections);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{salesOrderCode}")
  public @ResponseBody String deleteSalesOrder(@PathVariable String salesOrderCode)
      throws Exception {

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(salesOrderGeneralModel, IActionsNames.DELETE);

    utils.checkIfDeleteActionIsAllowedInSalesOrderCurrentState(
        salesOrderCode, IActionsNames.DELETE);

    DObSalesOrderDeletionValueObject valueObject = new DObSalesOrderDeletionValueObject();
    valueObject.setSalesOrderCode(salesOrderCode);

    entityLockCommand.lockAllSections(salesOrderCode);

    salesOrderDeleteCommand.executeCommand(valueObject);

    entityLockCommand.unlockAllSections(salesOrderCode);

    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{salesOrderCode}/approve")
  public @ResponseBody String approveSalesOrder(@PathVariable String salesOrderCode)
      throws Exception {

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.APPROVE);

    utils.checkIfActionIsAllowedInCurrentState(
        new DObSalesOrderStateMachine(), salesOrderGeneralModel, IDObSalesOrderActionNames.APPROVE);

    DObSalesOrderDeletionValueObject valueObject = new DObSalesOrderDeletionValueObject();
    valueObject.setSalesOrderCode(salesOrderCode);

    entityLockCommand.lockAllSections(salesOrderCode);

    try {
      Map<String, Object> missingFields =
          salesOrderMandatoryValidator.validateAllSectionsForState(
              salesOrderCode, DObSalesOrderStateMachine.APPROVED);

      if (!missingFields.isEmpty()) {
        return CommonControllerUtils.getFailureResponseWithMissingFields(
            missingFields, CommonControllerUtils.MISSING_FIELDS_RESPONSE_CODE);
      }

      salesOrderApproveCommand.executeCommand(valueObject);
    } finally {
      entityLockCommand.unlockAllSections(salesOrderCode);
    }

    return CommonControllerUtils.getSuccessResponse(
        ISalesOrderMessages.SUCCESS_APPROVE_LAST_APPROVER);
  }

  @Override
  public void afterPropertiesSet() {
    utils = new DObSalesOrderControllerUtils(authorizationManager, entityLockCommand);
    utils.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    utils.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    utils.setCustomerBusinessUnitGeneralModelRep(customerBusinessUnitGeneralModelRep);
  }
}
