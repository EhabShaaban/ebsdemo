package com.ebs.dda.rest.inventory.goodsreceipt.activation;

import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.executequery.inventory.goodsreceipt.FindStockAvailabilitiesContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IDObGoodsReceipt;
import com.ebs.dda.rest.inventory.goodsreceipt.IGoodsReceiptResponseMessages;
import java.util.List;

public class GoodsReceiptActivateContextBuilder {

  private GoodsReceiptActivateContextBuilder() {}

  public static GoodsReceiptActivateContext newContext() {
    return new GoodsReceiptActivateContext();
  }

  public static class GoodsReceiptActivateContext
      implements ActivateContext,
          AuthorizeContext,
          FindEntityContext,
          ExecuteCommandContext<DObInventoryDocumentActivateValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext,
          FindStockAvailabilitiesContext,
          LockContext,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<DObInventoryDocumentActivateValueObject> {

    private String objectCode;
    private String valueObjectString;
    private String response;
    private DObInventoryDocumentActivateValueObject valueObject;
    private List<String> stockAvailabilityCodes;
    private String journalEntryCode;
    private EntitiesOfInterestContainer entitiesOfInterestContainer;

    public GoodsReceiptActivateContext() {
      this.entitiesOfInterestContainer =
          new EntitiesOfInterestContainer(DObGoodsReceiptGeneralModel.class);
    }

    @Override
    public String state() {
      return BasicStateMachine.ACTIVE_STATE;
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IGoodsReceiptResponseMessages.SUCCESSFUL_ACTIVATE;
    }

    @Override
    public String journalEntryUserCode() {
      return this.journalEntryCode;
    }

    @Override
    public String activateData() {
      return valueObjectString;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return IDObGoodsReceipt.SYS_NAME;
    }

    @Override
    public String actionName() {
      return IActionsNames.ACTIVATE;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      this.journalEntryCode = userCode;
    }

    public GoodsReceiptActivateContext objectCode(String goodsReceiptCode) {
      this.objectCode = goodsReceiptCode;
      return this;
    }

    public GoodsReceiptActivateContext valueObject(String valueObject) {
      this.valueObjectString = valueObject;
      return this;
    }

    @Override
    public DObInventoryDocumentActivateValueObject valueObject() {
      if (this.valueObject == null) {
        createActivateValueObject();
      }
      return this.valueObject;
    }

    private void createActivateValueObject() {
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
          this.entitiesOfInterestContainer.getMainEntity();
      this.valueObject = new DObInventoryDocumentActivateValueObject();
      valueObject.setUserCode(goodsReceiptGeneralModel.getUserCode());
      valueObject.setInventoryType(goodsReceiptGeneralModel.getInventoryDocumentType());
      valueObject.setRefDocumentCode(goodsReceiptGeneralModel.getRefDocumentCode());
    }

    @Override
    public void stockAvailabilityCodes(List<String> stockAvailabilityCodes) {
      this.stockAvailabilityCodes = stockAvailabilityCodes;
    }

    public List<String> stockAvailabilityCodes() {
      return this.stockAvailabilityCodes;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInterestContainer;
    }
  }
}
