package com.ebs.dda.rest.accounting.collection.activation;

@FunctionalInterface
public interface ActivateCollection {
  String activate(CollectionActivateContextBuilder.CollectionActivateContext context)
      throws Exception;
}
