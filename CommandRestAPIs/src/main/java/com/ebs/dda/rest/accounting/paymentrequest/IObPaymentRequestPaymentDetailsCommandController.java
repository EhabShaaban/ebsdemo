package com.ebs.dda.rest.accounting.paymentrequest;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;

import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import com.ebs.dda.validation.paymentrequest.IObPaymentSaveDetailsValidatorFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.DObPaymentRequestAuthorizationManager;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestActionNames;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestJsonSchema;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestSectionNames;
import com.ebs.dda.accounting.paymentrequest.editability.IObPaymentRequestPaymentDetailsEditabilityManager;
import com.ebs.dda.commands.accounting.paymentrequest.IObPaymentRequestPaymentDetailsSaveCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemAuthorizationGeneralModel;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemAuthorizationGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.paymentrequest.IObPaymentRequestPaymentDetailsSaveValidator;

@RestController
@RequestMapping(value = "command/accounting/paymentrequest")
public class IObPaymentRequestPaymentDetailsCommandController implements InitializingBean {

  @Autowired EntityLockCommand<DObPaymentRequest> entityLockCommand;
  @Autowired IObPaymentRequestPaymentDetailsSaveCommand paymentDetailsSaveCommand;
  @Autowired private DObPaymentRequestAuthorizationManager authorizationManager;
  @Autowired private DObPaymentRequestRep paymentRequestRep;
  @Autowired private DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;
  @Autowired private IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsGeneralModelRep;
  @Autowired private MObItemAuthorizationGeneralModelRep itemAuthorizationGeneralModelRep;

  private IObPaymentDetailsControllerUtils controllerUtils;

  @Autowired
  private IObPaymentRequestPaymentDetailsEditabilityManager
      paymentRequestPaymentDetailsEditabilityManager;

  @Autowired private IObPaymentSaveDetailsValidatorFactory paymentSaveDetailsValidatorFactory;

  @RequestMapping(method = RequestMethod.GET, value = "/{prCode}/lock/paymentdetails")
  public @ResponseBody String openPaymentDetailsSectionEditMode(
      @PathVariable String prCode, HttpServletResponse response) throws Exception {
    DObPaymentRequest paymentRequest = checkIfInstanceExists(prCode);
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(prCode);
    authorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IDObPaymentRequestActionNames.Edit_PAYMENT_DETAILS);

    controllerUtils.lockPaymentRequestSectionForEdit(
        prCode,
        IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION,
        IDObPaymentRequestActionNames.Edit_PAYMENT_DETAILS);

    Set<String> stateEnabledAttributes =
        paymentRequestPaymentDetailsEditabilityManager.getEnabledAttributesInGivenStates(
            paymentRequest.getCurrentStates());
    Set<String> paymentTypesEnabledAttributes =
        controllerUtils.getEditPaymentDetailsEnabledAttributes(paymentRequest.getPaymentType());
    stateEnabledAttributes.retainAll(paymentTypesEnabledAttributes);
    Set<String> paymentFormEnabledAttributes =
        controllerUtils.getEnableAttributesByForm(paymentRequestGeneralModel);
    stateEnabledAttributes.addAll(paymentFormEnabledAttributes);

    String[] stateMandatories =
        controllerUtils.getPaymentDetailsMandatoryAttributesByState(
            paymentRequest.getCurrentState());
    String[] paymentTypeMandatories =
        controllerUtils.getPaymentDetailsMandatoryAttributesByKey(paymentRequest.getPaymentType());
    String[] mandatories =
        controllerUtils.getInterSectionBetweenTwoArrays(paymentTypeMandatories, stateMandatories);

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getEditPaymentDetailsAuthorizedPermissionsConfig();
    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(mandatories);
    return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  public DObPaymentRequest checkIfInstanceExists(String userCode) throws InstanceNotExistException {
    DObPaymentRequest paymentRequest = paymentRequestRep.findOneByUserCode(userCode);
    if (paymentRequest == null) {
      throw new InstanceNotExistException();
    }
    return paymentRequest;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{prCode}/unlock/paymentdetails")
  public @ResponseBody String cancelPaymentDetailsEditMode(@PathVariable String prCode)
      throws Exception {
    checkIfInstanceExists(prCode);
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(prCode);
    authorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IDObPaymentRequestActionNames.Edit_PAYMENT_DETAILS);
    return controllerUtils.unlockSection(
        IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION, prCode);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{paymentRequestCode}/update/paymentdetails")
  public @ResponseBody String updatePaymentDetails(
      @PathVariable String paymentRequestCode, @RequestBody String paymentDetailsJsonData)
      throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
        IDObPaymentRequestJsonSchema.PAYMENT_DETAILS_SCHEMA, paymentDetailsJsonData);

    IObPaymentRequestPaymentDetailsValueObject paymentDetailsValueObject =
        (IObPaymentRequestPaymentDetailsValueObject)
            CommonControllerUtils.deserializeRequestBody(
                paymentDetailsJsonData, IObPaymentRequestPaymentDetailsValueObject.class);

    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
            getPaymentRequestGeneralModel(paymentRequestCode);

    applyAuthorizationForSave(paymentRequestGeneralModel, paymentDetailsValueObject);

    paymentDetailsValueObject.setPaymentRequestCode(paymentRequestCode);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        paymentRequestCode, IDObPaymentRequestSectionNames.PAYMENT_DETAILS_SECTION);

   IObPaymentRequestPaymentDetailsSaveValidator validator = paymentSaveDetailsValidatorFactory.getValidatorInstance(paymentRequestGeneralModel);
    ValidationResult validationResult =
            validator.validate(paymentDetailsValueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }

    try {
      paymentDetailsSaveCommand.executeCommand(paymentDetailsValueObject);
    } finally {
      paymentDetailsSaveCommand.unlockSection(paymentRequestCode);
    }
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  private void applyAuthorizationForSave(
          DObPaymentRequestGeneralModel paymentRequestGeneralModel, IObPaymentRequestPaymentDetailsValueObject valueObject)
      throws Exception {

    applyAuthorizationForReads(paymentRequestGeneralModel);

    checkCostFactorItemAuthoriztion(valueObject.getCostFactorItemCode());
    try {
      authorizationManager.authorizeActionOnSection(
              paymentRequestGeneralModel, IActionsNames.EDIT_PAYMENT_DETAILS);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  private DObPaymentRequestGeneralModel getPaymentRequestGeneralModel(String PaymentRequestCode)
      throws InstanceNotExistException {
    DObPaymentRequestGeneralModel dObPaymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(PaymentRequestCode);
    if (dObPaymentRequestGeneralModel == null) {
      throw new InstanceNotExistException();
    }
    return dObPaymentRequestGeneralModel;
  }

	private void applyAuthorizationForReads(
					DObPaymentRequestGeneralModel paymentDetailsGeneralModel) {
		authorizationManager.authorizeReadAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
		String paymentType = paymentDetailsGeneralModel.getPaymentType();
		if (!paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())) {
			authorizationManager.authorizeReadAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
		}
		if (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.name())) {
			authorizationManager.authorizeReadAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
		}
	}

  private void checkCostFactorItemAuthoriztion(String costFactorItemCode) throws Exception {

    if (costFactorItemCode == null) {
      return;
    }
    try {
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits =
          itemAuthorizationGeneralModelRep.findAllByUserCode(costFactorItemCode);
      if (itemPurchaseUnits == null || itemPurchaseUnits.isEmpty()) {
        return;
      }
      applyAuthorizationAction(itemPurchaseUnits, IActionsNames.READ_ALL);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  private void applyAuthorizationAction(
      List<MObItemAuthorizationGeneralModel> itemPurchaseUnits, String actionName)
      throws Exception {
    boolean isAuthorized = false;
    ConditionalAuthorizationException conditionalAuthorizationException = null;
    for (MObItemAuthorizationGeneralModel itemPurchaseUnit : itemPurchaseUnits) {
      try {
        authorizationManager.authorizeAction(itemPurchaseUnit, actionName);
        isAuthorized = true;
        break;
      } catch (ConditionalAuthorizationException e) {
        conditionalAuthorizationException = e;
      }
    }
    if (!isAuthorized) throw conditionalAuthorizationException;
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils = new IObPaymentDetailsControllerUtils();
    controllerUtils.setEntityLockCommand(entityLockCommand);
    controllerUtils.setPaymentRequestRep(paymentRequestRep);
    controllerUtils.setPaymentDetailsGeneralModelRep(paymentDetailsGeneralModelRep);
    controllerUtils.setAuthorizationManager(authorizationManager);
    controllerUtils.setPaymentRequestPaymentDetailsEditabilityManager(
        paymentRequestPaymentDetailsEditabilityManager);
  }
}
