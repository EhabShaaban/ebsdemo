package com.ebs.dda.rest.accounting.salesinvoice;

import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.DObSalesInvoiceAuthorizationManager;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.commands.accounting.salesinvoice.IObSalesInvoiceTaxesDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxes;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesDeletionValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceTaxesRep;
import com.ebs.dda.rest.CommonControllerUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/salesinvoice")
public class IObTaxesCommandController implements InitializingBean {

  @Autowired private EntityLockCommand<DObSalesInvoice> entityLockCommand;
  private IObTaxesCommandControllerUtils iObTaxesCommandControllerUtils;
  @Autowired private DObSalesInvoiceAuthorizationManager salesInvoiceAuthorizationManager;
  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired private IObSalesInvoiceTaxesDeleteCommand salesInvoiceTaxesDeleteCommand;
  @Autowired private IObSalesInvoiceTaxesRep salesInvoiceTaxesRep;
  @Autowired private IObSalesInvoiceTaxesGeneralModelRep salesInvoiceTaxesGeneralModelRep;

  @RequestMapping(method = RequestMethod.DELETE, value = "/{salesInvoiceCode}/taxes/{taxCode}")
  public @ResponseBody String deleteSalesInvoiceItem(
      @PathVariable String salesInvoiceCode, @PathVariable String taxCode) throws Exception {
    DObSalesInvoiceGeneralModel salesInvoice =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    iObTaxesCommandControllerUtils.checkIfEntityIsNull(salesInvoice);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        salesInvoice, IDObSalesInvoiceActionNames.UPDATE_TAXES);

    IObSalesInvoiceTaxesDeletionValueObject salesInvoiceTaxesDeletionValueObject=
        new IObSalesInvoiceTaxesDeletionValueObject();
    salesInvoiceTaxesDeletionValueObject.setSalesInvoiceCode(salesInvoiceCode);
    salesInvoiceTaxesDeletionValueObject.setTaxCode(taxCode);

    IObSalesInvoiceTaxesGeneralModel taxesGeneralModel =
        salesInvoiceTaxesGeneralModelRep.findOneByInvoiceCodeAndTaxCode(salesInvoiceCode, taxCode);

    iObTaxesCommandControllerUtils.checkIfSalesInvoiceTaxExists(taxesGeneralModel);

    iObTaxesCommandControllerUtils.checkIfDeleteIsAllowedInCurrentState(salesInvoice,
        IActionsNames.DELETE);

    entityLockCommand.lockSection(salesInvoiceCode, IDObSalesInvoiceSectionNames.TAXES_SECTION);
    try {
      salesInvoiceTaxesDeleteCommand.executeCommand(salesInvoiceTaxesDeletionValueObject);
    } finally {
      entityLockCommand.unlockSection(salesInvoiceCode, IDObSalesInvoiceSectionNames.TAXES_SECTION);
    }
    return getSuccessResponse(CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    iObTaxesCommandControllerUtils =
        new IObTaxesCommandControllerUtils(salesInvoiceAuthorizationManager.getAuthorizationManager());
    iObTaxesCommandControllerUtils.setEntityLockCommand(entityLockCommand);
    iObTaxesCommandControllerUtils.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
  }
}
