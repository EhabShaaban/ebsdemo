package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderDataUpdateCommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataValueObject;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.order.salesorder.IDObSalesOrderSectionNames;
import com.ebs.dda.order.salesorder.IObSalesOrderDataEditabilityManager;
import com.ebs.dda.order.salesorder.IObSalesOrderDataJsonSchema;
import com.ebs.dda.order.salesorder.IObSalesOrderDataMandatoryValidator;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.salesorder.IObSalesOrderDataSaveMandatoryValidator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/order/salesorder")
public class IObSalesOrderDataCommandController implements InitializingBean {

  @Autowired private IObSalesOrderDataMandatoryValidator salesOrderDataMandatoryValidator;
  @Autowired private EntityLockCommand<DObSalesOrder> entityLockCommand;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  @Autowired private IObSalesOrderDataEditabilityManager salesOrderDataEditabilityManager;
  @Autowired private IObSalesOrderDataUpdateCommand salesOrderDataUpdateCommand;
  @Autowired private IObSalesOrderDataSaveMandatoryValidator salesOrderDataSaveMandatoryValidator;
  private IObSalesOrderDataControllerUtils controllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/lock/salesorderdata")
  public @ResponseBody String openSalesOrderDataSectionEditMode(@PathVariable String salesOrderCode)
      throws Exception {

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_SALES_ORDER_DATA);

    controllerUtils.lockSalesOrderSectionForEdit(
        salesOrderCode,
        IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION,
        IDObSalesOrderActionNames.UPDATE_SALES_ORDER_DATA);

    Set<String> stateEnabledAttributes =
        controllerUtils.getEnableAttributes(salesOrderGeneralModel);

    String[] mandatories =
        controllerUtils.getMandatoriesAttributes(salesOrderGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getEditSalesOrderDataAuthorizedPermissionsConfig();

    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(mandatories);

    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/unlock/salesorderdata")
  public @ResponseBody String cancelSalesOrderDataEditMode(@PathVariable String salesOrderCode)
      throws Exception {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_SALES_ORDER_DATA);

    controllerUtils.unlockSection(
        IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION, salesOrderCode);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{salesOrderCode}/update/salesorderdata")
  public @ResponseBody String updateSalesOrderData(
      @PathVariable String salesOrderCode, @RequestBody String salesOrderJsonData)
      throws Exception {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_SALES_ORDER_DATA);

    controllerUtils.applyAuthorizationForSave();

    CommonControllerUtils.applySchemaValidationForString(
        IObSalesOrderDataJsonSchema.SAVE, salesOrderJsonData);

    IObSalesOrderDataValueObject salesOrderDataValueObject =
        (IObSalesOrderDataValueObject)
            CommonControllerUtils.deserializeRequestBody(
                salesOrderJsonData, IObSalesOrderDataValueObject.class);

    salesOrderDataValueObject.setSalesOrderCode(salesOrderCode);

    salesOrderDataMandatoryValidator.validateValueObjectMandatories(salesOrderDataValueObject);
    salesOrderDataSaveMandatoryValidator.validate(salesOrderDataValueObject);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        salesOrderCode, IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION);

    salesOrderDataUpdateCommand.executeCommand(salesOrderDataValueObject);

    entityLockCommand.unlockSection(
        salesOrderCode, IDObSalesOrderSectionNames.SALES_ORDER_DATA_SECTION);

    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils = new IObSalesOrderDataControllerUtils(authorizationManager, entityLockCommand);
    controllerUtils.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    controllerUtils.setSalesOrderDataEditabilityManager(salesOrderDataEditabilityManager);
  }
}
