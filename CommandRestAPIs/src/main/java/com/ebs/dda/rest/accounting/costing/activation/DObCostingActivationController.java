package com.ebs.dda.rest.accounting.costing.activation;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.exceptions.AuthorizationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/accounting/costing")
public class DObCostingActivationController {
  @Autowired ActivateCosting activateCosting;

  @PutMapping("/{userCode}/activate")
  public @ResponseBody String activateCostingDocument(
      @PathVariable String userCode, @RequestBody String valueObject) throws Exception {
    try {
      return activateCosting.activate(
          CostingActivateContextBuilder.newContext().objectCode(userCode).valueObject(valueObject));
    } catch (InstanceNotExistException exception) {
      AuthorizationException authorizationException = new AuthorizationException(exception);
      throw authorizationException;
    }
  }
}
