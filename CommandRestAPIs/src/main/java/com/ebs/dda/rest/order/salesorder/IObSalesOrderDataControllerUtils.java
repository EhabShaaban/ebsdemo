package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.masterdata.paymentterms.ICObPaymentTerms;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.IObSalesOrderDataEditabilityManager;
import com.ebs.dda.order.salesorder.ISalesOrderDataMandatoryAttributes;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.Set;

public class IObSalesOrderDataControllerUtils extends DObSalesOrderControllerUtils {

  private EntityLockCommand<DObSalesOrder> entityLockCommand;

  private IObSalesOrderDataEditabilityManager salesOrderDataEditabilityManager;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObSalesOrderDataControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObSalesOrder> entityLockCommand) {
    super(authorizationManager, entityLockCommand);
    this.entityLockCommand = entityLockCommand;
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void lockSalesOrderSectionForEdit(
      String salesOrderCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(salesOrderCode, sectionName);
    checkIfUpdateActionIsAllowedInSalesOrderCurrentState(salesOrderCode, sectionName, actionName);
  }

  public void unlockSection(String sectionName, String salesOrderCode) throws Exception {
    entityLockCommand.unlockSection(salesOrderCode, sectionName);
  }

  public Set<String> getEnableAttributes(DObSalesOrderGeneralModel salesOrderGeneralModel) {
    return salesOrderDataEditabilityManager
        .getEnabledAttributesConfig()
        .get(salesOrderGeneralModel.getCurrentState());
  }

  public String[] getMandatoriesAttributes(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            ISalesOrderDataMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public AuthorizedPermissionsConfig getEditSalesOrderDataAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(
        IMObCustomer.SYS_NAME, IActionsNames.READ_ADDRESS, "ReadAddress");
    permissionsConfig.addPermissionConfig(
        IMObCustomer.SYS_NAME, IActionsNames.READ_CONTACT_PERSON, "ReadContactPerson");
    permissionsConfig.addPermissionConfig(
        ICObPaymentTerms.SYS_NAME, IActionsNames.READ_ALL, "ReadPaymentTerms");
    permissionsConfig.addPermissionConfig(
        ICObCurrency.SYS_NAME, IActionsNames.READ_ALL, "ReadCurrency");
    return permissionsConfig;
  }

  public void setSalesOrderDataEditabilityManager(
      IObSalesOrderDataEditabilityManager salesOrderDataEditabilityManager) {
    this.salesOrderDataEditabilityManager = salesOrderDataEditabilityManager;
  }

  public void applyAuthorizationForSave() {
    AuthorizedPermissionsConfig permissionsConfig =
        getEditSalesOrderDataAuthorizedPermissionsConfig();
    applyAuthorizationForValueObjectAuthorizedReads(permissionsConfig);
  }
}
