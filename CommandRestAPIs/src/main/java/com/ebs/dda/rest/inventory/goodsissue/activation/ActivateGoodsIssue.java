package com.ebs.dda.rest.inventory.goodsissue.activation;

public interface ActivateGoodsIssue {
  String apply(GoodsIssueActivateContextBuilder.GoodsIssueActivateContext context) throws Exception;
}
