package com.ebs.dda.rest.order.purchaseorder.paymentanddelivery;

import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.lookups.IMasterDataLookupsSysNames;
import com.ebs.dda.jpa.masterdata.paymentterms.ICObPaymentTerms;
import com.ebs.dda.purchases.apis.IPurchaseOrderPaymentAndDeliveryActions;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IPaymentAndDeliveryMandatoryAttributes;
import com.ebs.dda.rest.order.purchaseorder.DObPurchaseOrderControllerUtils;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.configurationobjects.apis.ICObShippingInstructions;
import com.ebs.dda.shipping.dbo.jpa.entities.businessobjects.lookups.apis.IShippingLookupsSysNames;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class IObPaymentAndDeliveryCommandControllerUtils extends DObPurchaseOrderControllerUtils {


  public IObPaymentAndDeliveryCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  private String[] getPaymentAndDeliveryMandatoryAttributesByState(
      DObPurchaseOrderGeneralModel dObPurchaseOrder) {
    String[] mandatories;
    if (isImport(dObPurchaseOrder)) {
      mandatories =
          mandatoryValidatorUtils
              .getMandatoryAttributesByState(
                  IPaymentAndDeliveryMandatoryAttributes.IMPORT_MANDATORIES_FOR_VALUE_OBJECT,
                  dObPurchaseOrder.getCurrentStates())
              .toArray(new String[0]);
    } else {
      mandatories =
          mandatoryValidatorUtils
              .getMandatoryAttributesByState(
                  IPaymentAndDeliveryMandatoryAttributes.LOCAL_MANDATORIES_FOR_VALUE_OBJECT,
                  dObPurchaseOrder.getCurrentStates())
              .toArray(new String[0]);
    }
    return mandatories != null ? mandatories : new String[]{};
  }

  public String getPaymentAndDeliveryEditConfiguration(
      DObPurchaseOrderGeneralModel dObPurchaseOrder, Set<String> enabledAttributes) {

    String[] mandatoriesResponse = getPaymentAndDeliveryMandatoryAttributesByState(dObPurchaseOrder);
    List<String> paymentAndDeliveryAuthorizedReads = getPaymentAndDeliveryAuthorizedReads();

    return serializeEditConfigurationsResponse(
        enabledAttributes, mandatoriesResponse, paymentAndDeliveryAuthorizedReads);
  }

  private List<String> getPaymentAndDeliveryAuthorizedReads() {
    List<String> authReads = new ArrayList<>();
    isAuthorizedReadPermission(
        ICObCurrency.SYS_NAME, IPurchaseOrderPaymentAndDeliveryActions.READ_CURRENCY, authReads);
    isAuthorizedReadPermission(
        ICObShippingInstructions.SYS_NAME,
        IPurchaseOrderPaymentAndDeliveryActions.READ_SHIPPING_INSTRUCTIONS,
        authReads);
    isAuthorizedReadPermission(
        ICObPaymentTerms.SYS_NAME,
        IPurchaseOrderPaymentAndDeliveryActions.READ_PAYMENT_TERMS,
        authReads);
    isAuthorizedReadPermission(
        IMasterDataLookupsSysNames.LOB_INCOTERMS_SYS_NAME,
        IPurchaseOrderPaymentAndDeliveryActions.READ_INCOTERMS,
        authReads);
    isAuthorizedReadPermission(
        IMasterDataLookupsSysNames.LOB_CONTAINER_REQUIRMENT_SYS_NAME,
        IPurchaseOrderPaymentAndDeliveryActions.READ_CONTAINER_TYPES,
        authReads);
    isAuthorizedReadPermission(
        IMasterDataLookupsSysNames.LOB_PORT_SYS_NAME,
        IPurchaseOrderPaymentAndDeliveryActions.READ_PORTS,
        authReads);
    isAuthorizedReadPermission(
        IShippingLookupsSysNames.LOB_MODE_OF_TRANSPORT_SYS_NAME,
        IPurchaseOrderPaymentAndDeliveryActions.READ_TRANSPORT_MODES,
        authReads);
    return authReads;
  }


}
