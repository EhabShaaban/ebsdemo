package com.ebs.dda.rest.accounting.vendorinvoice.items;

import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItem;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.rest.accounting.vendorinvoice.DObVendorInvoiceCommandControllerUtils;

public class DObVendorInvoiceItemsCommandControllerUtils extends DObVendorInvoiceCommandControllerUtils {

  public void checkIfInvoiceItemExists(IObVendorInvoiceItemsGeneralModel invoiceItems)
      throws RecordOfInstanceNotExistException {
    if (invoiceItems == null) {
      throw new RecordOfInstanceNotExistException(IObVendorInvoiceItem.class.getName());
    }
  }

}
