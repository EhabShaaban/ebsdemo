package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.order.salesorder.IObSalesOrderCompanyStoreEditabilityManager;
import com.ebs.dda.order.salesorder.ISalesOrderCompanyAndStoreDataMandatoryAttributes;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import java.util.Set;

public class IObSalesOrderCompanyStoreControllerUtils extends DObSalesOrderControllerUtils {

  private EntityLockCommand<DObSalesOrder> entityLockCommand;
  private IObSalesOrderCompanyStoreEditabilityManager salesOrderCompanyStoreEditabilityManager;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObSalesOrderCompanyStoreControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObSalesOrder> entityLockCommand) {
    super(authorizationManager, entityLockCommand);
    this.entityLockCommand = entityLockCommand;
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void lockSalesOrderSectionForEdit(
      String salesOrderCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(salesOrderCode, sectionName);
    checkIfUpdateActionIsAllowedInSalesOrderCurrentState(salesOrderCode, sectionName, actionName);
  }

  public void unlockSection(String sectionName, String salesOrderCode) throws Exception {
    entityLockCommand.unlockSection(salesOrderCode, sectionName);
  }

  public Set<String> getEnableAttributes(DObSalesOrderGeneralModel salesOrderGeneralModel) {
    return salesOrderCompanyStoreEditabilityManager
        .getEnabledAttributesConfig()
        .get(salesOrderGeneralModel.getCurrentState());
  }

  public String[] getMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            ISalesOrderCompanyAndStoreDataMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public AuthorizedPermissionsConfig getEditCompanyStoreAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(
        ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL, "ReadAllStorehouse");
    return permissionsConfig;
  }

  public void setSalesOrderCompanyStoreEditabilityManager(
      IObSalesOrderCompanyStoreEditabilityManager salesOrderCompanyStoreEditabilityManager) {
    this.salesOrderCompanyStoreEditabilityManager = salesOrderCompanyStoreEditabilityManager;
  }

  public void applyAuthorizationForSaveValueObjectValues() {
    AuthorizedPermissionsConfig permissionsConfig =
        getEditCompanyStoreAuthorizedPermissionsConfig();
    applyAuthorizationForValueObjectAuthorizedReads(permissionsConfig);
  }
}
