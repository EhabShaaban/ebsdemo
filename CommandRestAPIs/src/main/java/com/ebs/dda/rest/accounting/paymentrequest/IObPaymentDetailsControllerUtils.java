package com.ebs.dda.rest.accounting.paymentrequest;

import java.util.*;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.DObPaymentRequestAuthorizationManager;
import com.ebs.dda.accounting.paymentrequest.editability.IObPaymentRequestPaymentDetailsEditabilityManager;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.accounting.paymentrequest.validation.apis.IPaymentRequestPaymentDetailsMandatoryAttributes;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.masterdata.company.CObCompany;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

public class IObPaymentDetailsControllerUtils extends CommonControllerUtils {

  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private EntityLockCommand<DObPaymentRequest> entityLockCommand;
  private DObPaymentRequestRep paymentRequestRep;
  private IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsGeneralModelRep;
  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private DObPaymentRequestAuthorizationManager authorizationManager;
  private IObPaymentRequestPaymentDetailsEditabilityManager
      paymentRequestPaymentDetailsEditabilityManager;

  public IObPaymentDetailsControllerUtils() {
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void lockPaymentRequestSectionForEdit(
      String paymentRequestCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(paymentRequestCode, sectionName);
    try {
      checkIfActionIsAllowedInCurrentState(paymentRequestCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(paymentRequestCode, sectionName);
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
  }

  private void checkIfActionIsAllowedInCurrentState(String paymentRequestCode, String actionName)
      throws Exception {
    DObPaymentRequest paymentRequest = paymentRequestRep.findOneByUserCode(paymentRequestCode);
    DObPaymentRequestStateMachine paymentRequestStateMachine = new DObPaymentRequestStateMachine();
    paymentRequestStateMachine.initObjectState(paymentRequest);
    paymentRequestStateMachine.isAllowedAction(actionName);
  }

  public String[] getPaymentDetailsMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IPaymentRequestPaymentDetailsMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public String[] getPaymentDetailsMandatoryAttributesByKey(String keyName) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByKey(
            IPaymentRequestPaymentDetailsMandatoryAttributes.MANDATORIES, keyName);
    return mandatories != null ? mandatories : new String[] {};
  }

  @Override
  public List<String> getAuthorizedReads(AuthorizedPermissionsConfig authorizedPermissionsConfig) {
    List<AuthorizedPermissionsConfig.AuthorizedPermissionConfig> permissionsConfig =
        authorizedPermissionsConfig.getPermissionsConfig();
    List<String> authorizedReads = new ArrayList<>();
    for (AuthorizedPermissionsConfig.AuthorizedPermissionConfig config : permissionsConfig) {
      try {
        authorizationManager.authorizeReadAction(
            config.getEntitySysName(), config.getPermissionToCheck());
        authorizedReads.add(config.getPermissionToReturn());
      } catch (AuthorizationException | ConditionalAuthorizationException exception) {
      }
    }
    return authorizedReads;
  }

  protected String[] getInterSectionBetweenTwoArrays(String[] firstArray, String[] secondArray) {
    Set<String> interSectionSet = new HashSet<>(Arrays.asList(firstArray));
    interSectionSet.retainAll(Arrays.asList(secondArray));
    return Arrays.copyOf(interSectionSet.toArray(), interSectionSet.size(), String[].class);
  }

  protected AuthorizedPermissionsConfig getEditPaymentDetailsAuthorizedPermissionsConfig() {

		AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

		permissionsConfig.addPermissionConfig(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL,
						"ReadCurrency");
		permissionsConfig.addPermissionConfig(IMObItem.SYS_NAME, IActionsNames.READ_ALL,
						"ReadItems");
		permissionsConfig.addPermissionConfig(CObCompany.SYS_NAME, IActionsNames.READ_BANK_DETAILS,
						"ReadCompanyBankDetails");
		permissionsConfig.addPermissionConfig(CObCompany.SYS_NAME, IActionsNames.READ_ALL,
						"ReadTreasuries");
    return permissionsConfig;
  }

  protected Set<String> getEditPaymentDetailsEnabledAttributes(String currentState) {
    return paymentRequestPaymentDetailsEditabilityManager
        .getEnabledAttributesConfig()
        .get(currentState);
  }

  public Set<String> getEnableAttributesByForm(DObPaymentRequestGeneralModel paymentRequest) {
    return paymentRequestPaymentDetailsEditabilityManager
        .getEnabledAttributesConfig()
        .get(paymentRequest.getPaymentForm());
  }

  protected String unlockSection(String sectionName, String resourceCode) throws Exception {
    entityLockCommand.unlockSection(resourceCode, sectionName);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  public void setEntityLockCommand(EntityLockCommand<DObPaymentRequest> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
    this.paymentRequestRep = paymentRequestRep;
  }

  public void setAuthorizationManager(DObPaymentRequestAuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  public void setPaymentDetailsGeneralModelRep(
      IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsGeneralModelRep) {
    this.paymentDetailsGeneralModelRep = paymentDetailsGeneralModelRep;
  }

  public void setPaymentRequestPaymentDetailsEditabilityManager(
      IObPaymentRequestPaymentDetailsEditabilityManager
          paymentRequestPaymentDetailsEditabilityManager) {
    this.paymentRequestPaymentDetailsEditabilityManager =
        paymentRequestPaymentDetailsEditabilityManager;
  }
}
