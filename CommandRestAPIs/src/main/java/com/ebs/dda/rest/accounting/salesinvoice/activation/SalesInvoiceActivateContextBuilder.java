package com.ebs.dda.rest.accounting.salesinvoice.activation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSchema;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.functions.validate.ValidateSchemaContext;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoicePostValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;

public class SalesInvoiceActivateContextBuilder {

  private SalesInvoiceActivateContextBuilder() {}

  public static SalesInvoiceActivateContext newContext() {
    return new SalesInvoiceActivateContext();
  }

  public static class SalesInvoiceActivateContext
      implements ActivateContext,
          AuthorizeContext,
          FindEntityContext,
          ValidateSchemaContext,
          LockContext,
          DeserializeValueObjectContext<DObSalesInvoicePostValueObject>,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<DObSalesInvoicePostValueObject>,
          ExecuteCommandContext<DObSalesInvoicePostValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext {

    private String objectCode;
    private String activateData;
    private DObSalesInvoicePostValueObject valueObject;
    private String response;
    private RuntimeException exception;
    private String journalEntryCode;
    private EntitiesOfInterestContainer entitiesOfInstrestContainer;

    public SalesInvoiceActivateContext() {
      this.entitiesOfInstrestContainer =
          new EntitiesOfInterestContainer(DObSalesInvoiceGeneralModel.class);
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public String activateData() {
      return activateData;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return IDObSalesInvoice.SYS_NAME;
    }

    @Override
    public String actionName() {
      return IActionsNames.ACTIVATE;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      this.journalEntryCode = userCode;
    }

    @Override
    public String state() {
      return BasicStateMachine.ACTIVE_STATE;
    }

    public SalesInvoiceActivateContext objectCode(String salesInvoiceCode) {
      this.objectCode = salesInvoiceCode;
      return this;
    }

    public SalesInvoiceActivateContext valueObject(String valueObject) {
      this.activateData = valueObject;
      return this;
    }

    @Override
    public String schema() {
      return IDObSalesInvoiceSchema.salesInvoiceDeliverToCustomerSchema;
    }

    @Override
    public Class<DObSalesInvoicePostValueObject> valueObjectClass() {
      return DObSalesInvoicePostValueObject.class;
    }

    @Override
    public void valueObject(DObSalesInvoicePostValueObject valueObject) {
      this.valueObject = valueObject;
    }

    @Override
    public DObSalesInvoicePostValueObject valueObject() {
      return this.valueObject;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return this.journalEntryCode;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInstrestContainer;
    }
  }
}
