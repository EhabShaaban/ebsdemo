package com.ebs.dda.rest.accounting.salesinvoice;

import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.DObSalesInvoiceAuthorizationManager;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceActionNames;
import com.ebs.dda.accounting.salesinvoice.apis.IDObSalesInvoiceSectionNames;
import com.ebs.dda.accounting.salesinvoice.editiablity.DObSalesInvoiceEditCompanyDataEditabilityManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/salesinvoice")
public class IObCompanyDataCommandController implements InitializingBean {

  @Autowired private EntityLockCommand<DObSalesInvoice> entityLockCommand;
  private DObSalesInvoiceCommandControllerUtils dObSalesInvoiceCommandControllerUtils;
  @Autowired private DObSalesInvoiceAuthorizationManager salesInvoiceAuthorizationManager;
  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private DObSalesInvoiceEditCompanyDataEditabilityManager editabilityManager;
  private IObCompanyDataCommandControllerUtils controllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "{salesInvoiceCode}/lock/companydata/")
  public @ResponseBody String lockCompanyDataSection(@PathVariable String salesInvoiceCode)
      throws Exception {

    DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    dObSalesInvoiceCommandControllerUtils.checkIfEntityIsNull(dObSalesInvoiceGeneralModel);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        dObSalesInvoiceGeneralModel, IDObSalesInvoiceActionNames.UPDATE_COMPANY);

    dObSalesInvoiceCommandControllerUtils.lockSalesInvoiceSectionForEdit(
        dObSalesInvoiceGeneralModel,
        IDObSalesInvoiceSectionNames.COMPANY_DATA_SECTION,
        IDObSalesInvoiceActionNames.UPDATE_COMPANY);

    List<String> authorizedReads =
        controllerUtils.getSalesInvoiceRequestEditCompanyAuthorizedReads();

    Set<String> editabilities =
        editabilityManager
            .getEnabledAttributesConfig()
            .get(dObSalesInvoiceGeneralModel.getCurrentState());

    SectionEditConfig sectionEditConfig = new SectionEditConfig();
    sectionEditConfig.setAuthorizedReads(authorizedReads);
    sectionEditConfig.setEnabledAttributes(editabilities);

    return dObSalesInvoiceCommandControllerUtils.serializeEditConfigurationsResponse(
        sectionEditConfig);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{salesInvoiceCode}/unlock/companydata")
  public @ResponseBody String unlockCompanyDataSection(@PathVariable String salesInvoiceCode)
      throws Exception {

    DObSalesInvoiceGeneralModel dObSalesInvoiceGeneralModel =
        salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode);

    dObSalesInvoiceCommandControllerUtils.checkIfEntityIsNull(dObSalesInvoiceGeneralModel);

    salesInvoiceAuthorizationManager.authorizeActionOnSection(
        dObSalesInvoiceGeneralModel, IDObSalesInvoiceActionNames.UPDATE_COMPANY);

    entityLockCommand.unlockSection(
        salesInvoiceCode, IDObSalesInvoiceSectionNames.COMPANY_DATA_SECTION);

    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils =
        new IObCompanyDataCommandControllerUtils(
            salesInvoiceAuthorizationManager.getAuthorizationManager());
    dObSalesInvoiceCommandControllerUtils =
        new DObSalesInvoiceCommandControllerUtils(
            salesInvoiceAuthorizationManager.getAuthorizationManager());
    editabilityManager = new DObSalesInvoiceEditCompanyDataEditabilityManager();
    dObSalesInvoiceCommandControllerUtils.setEntityLockCommand(entityLockCommand);
  }
}
