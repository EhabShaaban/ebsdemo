package com.ebs.dda.rest.order.purchaseorder;

import static com.ebs.dda.rest.CommonControllerUtils.applySchemaValidationForString;
import static com.ebs.dda.rest.CommonControllerUtils.deserializeRequestBody;
import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.purchaseorder.IObServicePurchaseOrderPaymentDetailsUpdateCommand;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.apis.IDObPurchaseOrderJsonSchema;
import com.ebs.dda.purchases.apis.IPurchaseOrderActionNames;
import com.ebs.dda.purchases.apis.IPurchaseOrderSectionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IObServicePurchaseOrderPaymentDetailsValueObject;
import com.ebs.dda.purchases.editability.IObPurchaseOrderPaymentDetailsEditabilityManager;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.purchaseorder.IObServicePurchaseOrderSavePaymentDetailsValidator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/purchaseorder")
public class IObPurchaseOrderPaymentDetailsCommandController implements InitializingBean {

  @Autowired private EntityLockCommand<DObOrderDocument> entityLockCommand;
  @Autowired private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private IObServicePurchaseOrderPaymentDetailsUpdateCommand paymentDetailsUpdateCommand;

  @Autowired
  private IObPurchaseOrderPaymentDetailsEditabilityManager
      purchaseOrderPaymentDetailsEditabilityManager;

  @Autowired private IObServicePurchaseOrderSavePaymentDetailsValidator validator;

  private IObPaymentDetailsControllerUtils controllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseOrderCode}/lock/paymentdetails")
  public @ResponseBody String openPaymentDetailsSectionEditMode(
      @PathVariable String purchaseOrderCode) throws Exception {

    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    authorizationManager.authorizeActionOnObject(
        purchaseOrderGeneralModel, IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);

    controllerUtils.lockPurchaseOrderSectionForEdit(
        purchaseOrderCode,
        IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION,
        IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);

    Set<String> stateEnabledAttributes =
        controllerUtils.getEnableAttributes(purchaseOrderGeneralModel);

    String[] mandatories =
        controllerUtils.getMandatoryAttributesByState(purchaseOrderGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getEditPaymentDetailsAuthorizedPermissionsConfig();

    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(mandatories);
    return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{purchaseOrderCode}/unlock/paymentdetails")
  public @ResponseBody String cancelPaymentDetailsSectionEditMode(
      @PathVariable String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    authorizationManager.authorizeActionOnObject(
        purchaseOrderGeneralModel, IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);

    controllerUtils.unlockSection(
        IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION, purchaseOrderCode);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{purchaseOrderCode}/update/paymentdetails")
  public @ResponseBody String updatePaymentDetails(
      @PathVariable String purchaseOrderCode, @RequestBody String paymentDetailsJsonData)
      throws Exception {

    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);

    controllerUtils.applyAuthorizationForSave(purchaseOrder);

    applySchemaValidationForString(
        IDObPurchaseOrderJsonSchema.SAVE_PAYMENT_DETAILS, paymentDetailsJsonData);

    IObServicePurchaseOrderPaymentDetailsValueObject paymentDetailsValueObject =
        (IObServicePurchaseOrderPaymentDetailsValueObject)
            deserializeRequestBody(
                paymentDetailsJsonData, IObServicePurchaseOrderPaymentDetailsValueObject.class);
    paymentDetailsValueObject.setPurchaseOrderCode(purchaseOrderCode);

    controllerUtils.checkIfActionIsAllowedInCurrentState(
        purchaseOrderCode, IPurchaseOrderActionNames.UPDATE_PAYMENT_TERMS);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION);

    validator.validate(paymentDetailsValueObject);

    paymentDetailsUpdateCommand.executeCommand(paymentDetailsValueObject);

    entityLockCommand.unlockSection(
        purchaseOrderCode, IPurchaseOrderSectionNames.PAYMENT_DETAILS_SECTION);

    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils = new IObPaymentDetailsControllerUtils(authorizationManager, entityLockCommand);
    controllerUtils.setEntityLockCommand(entityLockCommand);
    controllerUtils.setOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    controllerUtils.setPurchaseOrderPaymentDetailsEditabilityManager(
        purchaseOrderPaymentDetailsEditabilityManager);
  }
}
