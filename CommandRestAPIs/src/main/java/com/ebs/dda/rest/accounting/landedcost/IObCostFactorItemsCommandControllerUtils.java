package com.ebs.dda.rest.accounting.landedcost;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.landedcost.apis.ILandedCostFactorItemsMandatoryAttributes;
import com.ebs.dda.accounting.landedcost.editability.LandedCostFactorItemEditabilityManger;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.Set;

public class IObCostFactorItemsCommandControllerUtils extends DObLandedCostCommandControllerUtils {

    private LandedCostFactorItemEditabilityManger landedCOstFactorItemEditabilityManger;
    private MandatoryValidatorUtils mandatoryValidatorUtils;

    public IObCostFactorItemsCommandControllerUtils(AuthorizationManager authorizationManager) {
        super(authorizationManager);
        mandatoryValidatorUtils = new MandatoryValidatorUtils();
    }

    protected AuthorizedPermissionsConfig getِAddAuthorizedPermissionsConfig() {
        AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
        permissionsConfig.addPermissionConfig(IMObItem.SYS_NAME, IActionsNames.READ_ALL, "ReadItems");
        return permissionsConfig;
    }

    protected Set<String> getEnableAttributesForAddItem(DObLandedCostGeneralModel landedCost) {
        return landedCOstFactorItemEditabilityManger
                .getEnabledAttributesConfig()
                .get(landedCost.getCurrentState());
    }

    public String[] getItemsMandatoryAttributesByState(String currentState) {
        String[] mandatories;
        mandatories =
                mandatoryValidatorUtils.getMandatoryAttributesByState(
                        ILandedCostFactorItemsMandatoryAttributes.MANDATORIES, currentState);
        return mandatories != null ? mandatories : new String[]{};
    }

    public void setLandedCOstFactorItemEditabilityManger(
            LandedCostFactorItemEditabilityManger landedCOstFactorItemEditabilityManger) {
        this.landedCOstFactorItemEditabilityManger = landedCOstFactorItemEditabilityManger;
    }


}
