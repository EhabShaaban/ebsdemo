package com.ebs.dda.rest.masterdata.vendor;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorAccountingDetailsEditCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorCreateCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorDeleteCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorGeneralDataEditCommand;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.ICObPurchasingResponsible;
import com.ebs.dda.jpa.masterdata.vendor.*;
import com.ebs.dda.masterdata.vendor.*;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rest.masterdata.utils.MasterDataControllerUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.util.*;
import java.util.stream.Collectors;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;

@RestController
@RequestMapping(value = "command/vendor")
public class MObVendorCommandController implements InitializingBean {

  private final String RESPONSE_STATUS = "status";
  private final String CODE_RESPONSE_KEY = "code";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String SAVING_SUCCESSFUL_CODE = "Gen-msg-04";
  private final String CREATION_SUCCESSFUL_CODE = "Gen-msg-11";
  private final String DELETION_SUCCESSFUL_CODE = "Gen-msg-12";
  private final String USER_CODE_KEY = "userCode";
  private MObVendorControllerUtils vendorControllerUtils;
  private MasterDataControllerUtils masterDataControllerUtils;
  @Autowired private MObVendorCreateCommand vendorCreateCommand;
  @Autowired private MObVendorCreationValidator mObVendorCreationValidator;
  @Autowired private MObVendorGeneralDataValidator vendorGeneralDataValidator;
  @Autowired private MObVendorAccountingDetailsValidator vendorAccountingDetailsValidator;

  @Autowired private MObVendorDeleteCommand vendorDeleteCommand;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitRep;
  @Autowired private MObVendorGeneralDataEditCommand vendorGeneralDataEditCommand;
  @Autowired private MObVendorAccountingDetailsEditCommand vendorAccountingDetailsEditCommand;
  @Autowired private EntityLockCommand<MObVendor> entityLockCommand;
  private CommonControllerUtils commonControllerUtils;

  @RequestMapping(value = "/create", method = RequestMethod.POST)
  public @ResponseBody String createVendor(@RequestBody String creationData) throws Exception {

    applyAuthorizationForCreateAction();

    CommonControllerUtils.applySchemaValidationForString(MObVendorJsonSchema.CREATE, creationData);

    MObVendorCreationValueObject valueObject =
        (MObVendorCreationValueObject)
            CommonControllerUtils.deserializeRequestBody(
                creationData, MObVendorCreationValueObject.class);

    mObVendorCreationValidator.validate(valueObject);

    ValidationResult validationResult = mObVendorCreationValidator.validateOnBusiness(valueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }

    StringBuilder userCode = new StringBuilder();
    vendorCreateCommand.executeCommand(valueObject).subscribe(code -> userCode.append(code));
    return commonControllerUtils.getSuccessCreationResponse(String.valueOf(userCode));
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{vendorCode}")
  public @ResponseBody String deleteVendor(@PathVariable String vendorCode) throws Exception {
    vendorDeleteCommand.checkIfInstanceExists(vendorCode);
    try {
      List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits =
          vendorPurchaseUnitRep.findAllByUserCode(vendorCode);
      vendorControllerUtils.applyAuthorization(vendorPurchaseUnits, IActionsNames.DELETE);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
    vendorDeleteCommand.executeCommand(vendorCode);
    return getSuccessResponseWithUserCode(vendorCode, DELETION_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/lockgeneraldata/{vendorCode}")
  public @ResponseBody String openGeneralDataSectionEditMode(
      @PathVariable String vendorCode, HttpServletResponse response) throws Exception {

    applyAuthorizationForEditGeneralDataAction(vendorCode);

    vendorGeneralDataEditCommand.lock(vendorCode);

    List<String> authorizedReads = prepareAuthorizedReads();

    return MasterDataControllerUtils.serializeEditConfigurationsResponse(authorizedReads);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/lockaccountingdetails/{vendorCode}")
  public @ResponseBody String openAccountingDetailsSectionEditMode(
      @PathVariable String vendorCode, HttpServletResponse response) throws Exception {

    applyAuthorizationForEditAccountingDetailsAction(vendorCode);

    vendorAccountingDetailsEditCommand.lock(vendorCode);
    AuthorizedPermissionsConfig editRequestPermissionsConfig =
        constructEditAccountingDetailsConfiguration();
    List<String> authorizedReads =
        masterDataControllerUtils.getAuthorizedReads(editRequestPermissionsConfig);

    String[] mandatories = {IIObVendorAccountingInfo.ACCOUNT_CODE};
    Set<String> editables = new HashSet<>();
    editables.add(IIObVendorAccountingInfo.ACCOUNT_CODE);
    return masterDataControllerUtils.serializeEditConfigurationsResponse(
        editables, mandatories, authorizedReads);
  }

  private AuthorizedPermissionsConfig constructEditAccountingDetailsConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
        IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL_LIMITED, "ReadAccounts");

    return configuration;
  }

  private List<String> prepareAuthorizedReads() throws Exception {
    List<String> authorizedReads = new ArrayList<>();

    boolean isReadPurchasingUnitAllowed =
        masterDataControllerUtils.isReadPermissionAllowed(
            ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);

    boolean isReadPurchasingResponsibleAllowed =
        masterDataControllerUtils.isReadPermissionAllowed(
            ICObPurchasingResponsible.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadPurchasingUnitAllowed) {
      authorizedReads.add("ReadPurchasingUnit");
    }
    if (isReadPurchasingResponsibleAllowed) {
      authorizedReads.add("ReadPurchasingResponsible");
    }
    return authorizedReads;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unlockgeneraldata/{vendorCode}")
  public @ResponseBody String closeGeneralDataSectionEditMode(
      @PathVariable String vendorCode, HttpServletResponse response) throws Exception {

    applyAuthorizationForEditGeneralDataAction(vendorCode);

    vendorGeneralDataEditCommand.unlockSection(vendorCode);
    return getSuccessResponseWithUserCode(vendorCode, SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/unlockaccountingdetails/{vendorCode}")
  public @ResponseBody String closeAccountingDetailsSectionEditMode(
      @PathVariable String vendorCode, HttpServletResponse response) throws Exception {

    applyAuthorizationForEditAccountingDetailsAction(vendorCode);

    vendorAccountingDetailsEditCommand.unlockSection(vendorCode);
    return getSuccessResponseWithUserCode(vendorCode, SUCCESS_RESPONSE_KEY);
  }

  private void applyAuthorizationForEditGeneralDataAction(String vendorCode) throws Exception {
    List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits =
        vendorPurchaseUnitRep.findAllByUserCode(vendorCode);
    try {
      vendorControllerUtils.applyAuthorization(
          vendorPurchaseUnits, IMObVendorActionNames.EDIT_GENERAL_DATA_SECTION);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  private void applyAuthorizationForEditAccountingDetailsAction(String vendorCode)
      throws Exception {
    List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnits =
        vendorPurchaseUnitRep.findAllByUserCode(vendorCode);
    try {
      vendorControllerUtils.applyAuthorization(
          vendorPurchaseUnits, IMObVendorActionNames.EDIT_ACCOUNTING_DETAILS_SECTION);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/update/generaldata/{vendorCode}")
  public @ResponseBody String updateGeneralDataSection(
      @PathVariable String vendorCode, @RequestBody String generaldataJsonObject) throws Exception {

    CommonControllerUtils.applySchemaValidation(
        MObVendorGeneralDataValueObject.class, generaldataJsonObject);

    applyAuthorizationForSaveGeneralDataAction(vendorCode);

    entityLockCommand.checkIfEntityExists(vendorCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        vendorCode, IMObVendorSectionNames.GENERAL_DATA_SECTION);

    MObVendorGeneralDataValueObject vendorGeneralDataValueObject =
        (MObVendorGeneralDataValueObject)
            deserializeRequestBody(generaldataJsonObject, MObVendorGeneralDataValueObject.class);

    ValidationResult validationResult =
        vendorGeneralDataValidator.validate(vendorGeneralDataValueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    } else {
      vendorGeneralDataEditCommand.executeCommand(vendorGeneralDataValueObject);
      vendorGeneralDataEditCommand.unlockSection(vendorCode);
      return getSuccessResponseWithUserCode(vendorCode, SAVING_SUCCESSFUL_CODE);
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/update/accountdetails/{vendorCode}")
  public @ResponseBody String updateAccountDetailsSection(
      @PathVariable String vendorCode, @RequestBody String AccountInfoJsonObject) throws Exception {

    CommonControllerUtils.applySchemaValidation(
        MObVendorAccountingInfoValueObject.class, AccountInfoJsonObject);

    applyAuthorizationForSaveAccountingDetailsAction(vendorCode);

    entityLockCommand.checkIfEntityExists(vendorCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        vendorCode, IMObVendorSectionNames.ACCOUNTING_DETAILS_SECTION);

    MObVendorAccountingInfoValueObject vendorAccountingInfoValueObject =
        (MObVendorAccountingInfoValueObject)
            deserializeRequestBody(AccountInfoJsonObject, MObVendorAccountingInfoValueObject.class);
    vendorAccountingInfoValueObject.setVendorCode(vendorCode);

    ValidationResult validationResult =
        vendorAccountingDetailsValidator.validate(vendorAccountingInfoValueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    } else {
      vendorAccountingDetailsEditCommand.executeCommand(vendorAccountingInfoValueObject);
      vendorAccountingDetailsEditCommand.unlockSection(vendorCode);
      return getSuccessResponseWithUserCode(vendorCode, SAVING_SUCCESSFUL_CODE);
    }
  }

  private void applyAuthorizationForSaveGeneralDataAction(String vendorCode) throws Exception {
    authorizationManager.authorizeAction(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);
    applyAuthorizationForEditGeneralDataAction(vendorCode);
  }

  private void applyAuthorizationForSaveAccountingDetailsAction(String vendorCode)
      throws Exception {
    authorizationManager.authorizeAction(
        IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL_LIMITED);
    applyAuthorizationForEditAccountingDetailsAction(vendorCode);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{vendorCode}")
  public @ResponseBody String closeAllSectionsInVendor(
      @PathVariable String vendorCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(vendorCode, lockedSections);
    return getSuccessResponseWithUserCode(vendorCode, SUCCESS_RESPONSE_KEY);
  }

  private Object deserializeRequestBody(String valueObject, Class<?> valueObjectClass) {
    Gson gson = new Gson();
    Object serializedValueObject = gson.fromJson(valueObject, valueObjectClass);
    return serializedValueObject;
  }

  public String getSuccessResponseWithUserCode(String userCode, String msgCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }

  protected void applyAuthorizationForCreateAction() {
    authorizationManager.authorizeAction(ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(
        ICObPurchasingResponsible.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(MObVendor.SYS_NAME, IActionsNames.CREATE);
    authorizationManager.authorizeAction(ICObVendor.SYS_NAME, IMObVendorActionNames.READ_ALL);
  }

  @Override
  public void afterPropertiesSet() {
    vendorControllerUtils = new MObVendorControllerUtils(authorizationManager);
    masterDataControllerUtils = new MasterDataControllerUtils(authorizationManager);
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }
}
