package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.masterdata.company.ICompanyActionNames;

import java.util.List;

public class IObCompanyDataCommandControllerUtils extends DObSalesInvoiceCommandControllerUtils {

  public IObCompanyDataCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  protected List<String> getSalesInvoiceRequestEditCompanyAuthorizedReads() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(
        ICObCompany.SYS_NAME, ICompanyActionNames.READ_TAXES, "ReadCompanyTaxes");

    return getAuthorizedReads(permissionsConfig);
  }
}
