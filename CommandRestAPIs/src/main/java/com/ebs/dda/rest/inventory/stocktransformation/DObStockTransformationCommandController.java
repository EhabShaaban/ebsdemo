package com.ebs.dda.rest.inventory.stocktransformation;

import com.ebs.dac.dbo.events.stocktrasformation.*;
import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.inventorydocument.DObInventoryDocumentActivateCommand;
import com.ebs.dda.commands.inventory.stocktransformation.DObStockTransformationCreateCommand;
import com.ebs.dda.inventory.stocktransformation.IDObStockTransformationActionNames;
import com.ebs.dda.inventory.stocktransformation.statemachines.DObStockTransformationStateMachine;
import com.ebs.dda.inventory.stocktransformation.validation.DObStockTransformationCreationValidator;
import com.ebs.dda.inventory.stocktransformation.validation.DObStockTransformationPostValidator;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformation;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationCreateValueObject;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformationGeneralModel;
import com.ebs.dda.jpa.inventory.stocktransformation.IDObStockTransformation;
import com.ebs.dda.repositories.inventory.stocktransformation.DObStockTransformationGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;

@RestController
@RequestMapping(value = "command/stocktransformation")
public class DObStockTransformationCommandController implements InitializingBean {

  @Autowired DObStockTransformationCreationValidator stockTransformationCreationValidator;
  private CommonControllerUtils commonControllerUtils;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObStockTransformationCreateCommand dObStockTransformationCreateCommand;
  @Autowired private DObInventoryDocumentActivateCommand dObInventoryDocumentActivateCommand;
  @Autowired private EntityLockCommand<DObStockTransformation> entityLockCommand;
  @Autowired private DObStockTransformationGeneralModelRep dObStockTransformationGeneralModelRep;
  @Autowired private DObStockTransformationPostValidator stockTransformationValidator;
  @Autowired private ObservablesEventBus observablesEventBus;

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }

  @RequestMapping(value = "/", method = RequestMethod.POST)
  public @ResponseBody String createStockTransformation(@RequestBody String stockTransformationData)
      throws Exception {
    CommonControllerUtils.applySchemaValidation(
        DObStockTransformationCreateValueObject.class, stockTransformationData);
    DObStockTransformationCreateValueObject dObStockTransformationCreateValueObject =
        (DObStockTransformationCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                stockTransformationData, DObStockTransformationCreateValueObject.class);
    ValidationResult validationResult =
        stockTransformationCreationValidator.validate(dObStockTransformationCreateValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      final StringBuilder userCode = new StringBuilder();
      dObStockTransformationCreateCommand
          .executeCommand(dObStockTransformationCreateValueObject)
          .subscribe(code -> userCode.append(code));
      return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
    }
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{stockTransformationCode}/activate")
  public @ResponseBody String activateStockTransformation(
      @PathVariable String stockTransformationCode) throws Exception {
    DObStockTransformationGeneralModel dObStockTransformationGeneralModel =
        dObStockTransformationGeneralModelRep.findOneByUserCode(stockTransformationCode);

    if (dObStockTransformationGeneralModel == null) {
      throw new InstanceNotExistException();
    }
    commonControllerUtils.checkIfActionIsAllowedInCurrentState(
        new DObStockTransformationStateMachine(),
        dObStockTransformationGeneralModel,
        IDObStockTransformationActionNames.ACTIVATE);

    applyAuthorizationForActivate(dObStockTransformationGeneralModel);

    ValidationResult validationResult =
        stockTransformationValidator.validate(dObStockTransformationGeneralModel);
    if (!validationResult.isValid()) {
      return getFailureResponse(
          validationResult, IExceptionsCodes.STKTR_TRANSFORMED_QTY_GREATER_THAN_ST_REMAINING);
    }

    entityLockCommand.lockAllSections(stockTransformationCode);
    final StringBuilder restResponse = new StringBuilder();
    try {

      DObInventoryDocumentActivateValueObject inventoryDocumentActivateValueObject =
          getInventoryDocumentActivateValueObject(stockTransformationCode);

      dObInventoryDocumentActivateCommand.executeCommand(inventoryDocumentActivateValueObject);
      consumeStockTransformationActivateEvent(stockTransformationCode);
      observablesEventBus
          .getObservableObject(StoreTransactionUpdateSuccessEvent.class)
          .subscribe(
              (event) -> {
                consumeStockTransformationCreateStoreTransactionEvent(stockTransformationCode);
              });
      observablesEventBus
          .getObservableObject(StockTransformationCreateStoreTransactionSuccessEvent.class)
          .subscribe(
              (event) -> {
                consumeStockTransformationUpdateEvent(stockTransformationCode);
              });
      observablesEventBus
          .getObservableObject(StockTransformationUpdateSuccessEvent.class)
          .subscribe(
              (event) -> {
                handleSuccessResponse(stockTransformationCode, restResponse);
              });
    } finally {
      entityLockCommand.unlockAllSections(stockTransformationCode);
    }
    return restResponse.toString();
  }

  private void consumeStockTransformationActivateEvent(String userCode) {
    StockTransformationActivateSuccessEvent event =
        new StockTransformationActivateSuccessEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }

  private void consumeStockTransformationUpdateEvent(String userCode) {
    StockTransformationUpdateEvent event = new StockTransformationUpdateEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }

  private void consumeStockTransformationCreateStoreTransactionEvent(String userCode) {
    StockTransformationCreateStoreTransactionEvent event =
        new StockTransformationCreateStoreTransactionEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }

  protected void applyAuthorizationForActivate(
      DObStockTransformationGeneralModel dObStockTransformationGeneralModel) throws Exception {
    try {
      authorizationManager.authorizeAction(
          dObStockTransformationGeneralModel, IDObStockTransformationActionNames.ACTIVATE);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException();
      authorizationException.initCause(ex);
      throw authorizationException;
    }
  }

  private void handleSuccessResponse(String stockTransformationCode, StringBuilder restResponse) {
    restResponse.append(
        CommonControllerUtils.getSuccessResponseWithUserCode(
            stockTransformationCode, IStockTransformationResponseMessages.SUCCESSFUL_POST_CODE));
  }

  private DObInventoryDocumentActivateValueObject getInventoryDocumentActivateValueObject(
      String stockTransformationCode) {
    DObInventoryDocumentActivateValueObject inventoryDocumentActivateValueObject =
        new DObInventoryDocumentActivateValueObject();
    inventoryDocumentActivateValueObject.setInventoryType(
        IDObStockTransformation.STOCK_TRANSFORMATION_KEY);
    inventoryDocumentActivateValueObject.setUserCode(stockTransformationCode);
    return inventoryDocumentActivateValueObject;
  }
}
