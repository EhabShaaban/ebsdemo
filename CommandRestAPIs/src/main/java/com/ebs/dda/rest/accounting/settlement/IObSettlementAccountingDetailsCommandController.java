package com.ebs.dda.rest.accounting.settlement;

import static com.ebs.dda.accounting.settlement.apis.IDObSettlementActionNames.UPDATE_SETTLEMENT_ACCOUNTING_DETAILS;
import static com.ebs.dda.accounting.settlement.apis.IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION;
import static com.ebs.dda.accounting.settlement.apis.IIObSettlementAccountingDetailsAttributesConfig.EDITABLE_ATTRIBUTES;
import static com.ebs.dda.accounting.settlement.apis.IIObSettlementAccountingDetailsAttributesConfig.MANDATORIES;
import static com.ebs.dda.rest.CommonControllerUtils.SAVING_SUCCESSFUL_CODE;
import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;
import static com.ebs.dda.rest.CommonControllerUtils.serializeEditConfigurationsResponse;

import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.settlement.apis.IIObSettlementAccountingDetailsJsonSchema;
import com.ebs.dda.accounting.settlement.validation.IObSettlementAddAccountingDetailsValidator;
import com.ebs.dda.jpa.masterdata.chartofaccount.IHObChartOfAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.DObSettlementAuthorizationManager;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementSectionNames;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.commands.accounting.settlement.IObSettlementAccountingDetailDeleteCommand;
import com.ebs.dda.commands.accounting.settlement.IObSettlementSaveAccountingDetailsCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAddAccountingDetailsValueObject;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDeleteAccountingDetailValueObject;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "/command/settlement")
public class IObSettlementAccountingDetailsCommandController {

  @Autowired
  private DObSettlementCommandControllerUtils utils;

  @Autowired
  private DObSettlementGeneralModelRep settlementGeneralModelRep;
  @Autowired
  private DObSettlementAuthorizationManager authorizationManager;
  @Autowired
  private IObSettlementAccountDetailsGeneralModelRep accountDetailsGeneralModelRep;
  @Autowired
  private EntityLockCommand<DObSettlement> entityLockCommand;

  @Autowired
  private IObSettlementSaveAccountingDetailsCommand saveAccountingDetailsCmd;

  @Autowired
  private IObSettlementAccountingDetailDeleteCommand deleteCommand;
  @Autowired
  private IObSettlementAddAccountingDetailsValidator addAccountingDetailsValidator;

  @GetMapping("/{settlementCode}/lock/accountingdetails")
  public @ResponseBody String requestAddAccountingDetails(@PathVariable String settlementCode)
      throws Exception {

    DObSettlementGeneralModel settlementGM =
        utils.checkExistanceAndAuthority(settlementCode, UPDATE_SETTLEMENT_ACCOUNTING_DETAILS);

    utils.lockSectionForEdit(settlementGM, ACCOUNTING_DETAILS_SECTION,
        UPDATE_SETTLEMENT_ACCOUNTING_DETAILS);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(utils.getAddingAccountingDetailsAuthorizedReads());
    editConfiguration.setEnabledAttributes(EDITABLE_ATTRIBUTES.get(settlementGM.getCurrentState()));
    editConfiguration.setMandatories(MANDATORIES.get(settlementGM.getCurrentState()));

    return serializeEditConfigurationsResponse(editConfiguration);
  }

  @GetMapping("{settlementCode}/unlock/accountingdetails")
  public @ResponseBody String unlockItemsSection(@PathVariable String settlementCode)
      throws Exception {

    utils.checkExistanceAndAuthority(settlementCode, UPDATE_SETTLEMENT_ACCOUNTING_DETAILS);
    utils.unlockSection(settlementCode, ACCOUNTING_DETAILS_SECTION);
    return getSuccessResponse(SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.DELETE,
      value = "{settlementCode}/accountingdetail/{accountingDetailId}")
  public @ResponseBody String deleteAccountingDetail(@PathVariable String settlementCode,
      @PathVariable Long accountingDetailId) throws Exception {

    IObSettlementDeleteAccountingDetailValueObject valueObject =
        new IObSettlementDeleteAccountingDetailValueObject();
    valueObject.setSettlementCode(settlementCode);
    valueObject.setAccountingDetailId(accountingDetailId);

    DObSettlementGeneralModel settlementGeneralModel =
        settlementGeneralModelRep.findOneByUserCode(settlementCode);
    DObSettlementCommandControllerUtils.checkIfEntityIsNull(settlementGeneralModel);

    authorizationManager.authorizeActionOnSection(settlementGeneralModel,
        UPDATE_SETTLEMENT_ACCOUNTING_DETAILS);

    utils.checkIfDeleteDetailActionIsAllowedInCurrentState(new DObSettlementStateMachine(),
        settlementGeneralModel, UPDATE_SETTLEMENT_ACCOUNTING_DETAILS);

    IObSettlementAccountDetailsGeneralModel accountDetailsGeneralModel =
        accountDetailsGeneralModelRep.findOneById(accountingDetailId);

    DObSettlementCommandControllerUtils.checkIfDetailIsNull(accountDetailsGeneralModel,
        IObSettlementAccountDetailsGeneralModel.class.getName());

    entityLockCommand.lockSection(settlementCode, ACCOUNTING_DETAILS_SECTION);

    deleteCommand.executeCommand(valueObject);

    entityLockCommand.unlockSection(settlementCode, ACCOUNTING_DETAILS_SECTION);

    return CommonControllerUtils
        .getSuccessResponse(CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @PostMapping("/{settlementCode}/update/accountingdetails")
  public @ResponseBody String saveAccountingDetails(@PathVariable String settlementCode,
      @RequestBody String accountingDetailsReqBody) throws Exception {
    CommonControllerUtils.applySchemaValidationForString(
            IIObSettlementAccountingDetailsJsonSchema.SAVE_ACCOUNTING_DETAILS_SCHEMA, accountingDetailsReqBody);
    DObSettlementGeneralModel settlementGeneralModel =
            settlementGeneralModelRep.findOneByUserCode(settlementCode);

    authorizationManager.authorizeActionOnSection(settlementGeneralModel,
            UPDATE_SETTLEMENT_ACCOUNTING_DETAILS);
    authorizationManager.authorizeReadAction(IHObChartOfAccounts.SYS_NAME, IActionsNames.READ_ALL);

    IObSettlementAddAccountingDetailsValueObject accDetailsValueObject = new Gson()
        .fromJson(accountingDetailsReqBody, IObSettlementAddAccountingDetailsValueObject.class);

    accDetailsValueObject.setSettlementCode(settlementCode);

    entityLockCommand.checkIfEntityExists(settlementCode);
    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(settlementCode,
        IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION);
    try {
      ValidationResult validationResult = addAccountingDetailsValidator.validate(accDetailsValueObject);
      if (!validationResult.isValid()) {
        return CommonControllerUtils.getFailureResponse(validationResult);
      }
      saveAccountingDetailsCmd.executeCommand(accDetailsValueObject);
    } finally {
      entityLockCommand.unlockSection(settlementCode,
          IDObSettlementSectionNames.ACCOUNTING_DETAILS_SECTION);
    }

    return getSuccessResponse(CommonControllerUtils.ADDING_ITEM_SUCCESSFUL_CODE);
  }
}
