package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.RecordDeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.salesresponsible.ICObSalesResponsible;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderCreateValueObject;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.ICObSalesOrderType;
import com.ebs.dda.order.salesorder.DObSalesOrderStateMachine;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import java.util.List;

public class DObSalesOrderControllerUtils extends CommonControllerUtils {

  private EntityLockCommand<DObSalesOrder> entityLockCommand;

  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;

  public DObSalesOrderControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObSalesOrder> entityLockCommand) {
    super(authorizationManager);
    this.entityLockCommand = entityLockCommand;
  }

  public void checkIfUpdateActionIsAllowedInSalesOrderCurrentState(
      String salesOrderCode, String sectionName, String actionName) throws Exception {
    try {
      checkIfActionIsAllowedInCurrentStateFromStateMachine(salesOrderCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(salesOrderCode, sectionName);
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
  }

  public void checkIfRecordDeleteActionIsAllowedInSalesOrderCurrentState(
      String salesOrderCode, String sectionName, String actionName) throws Exception {
    try {
      checkIfActionIsAllowedInCurrentStateFromStateMachine(salesOrderCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(salesOrderCode, sectionName);
      RecordDeleteNotAllowedPerStateException recordDeleteNotAllowedPerStateException =
          new RecordDeleteNotAllowedPerStateException();
      recordDeleteNotAllowedPerStateException.initCause(e);
      throw recordDeleteNotAllowedPerStateException;
    }
  }

  public void checkIfDeleteActionIsAllowedInSalesOrderCurrentState(
      String salesOrderCode, String actionName) throws Exception {
    try {
      checkIfActionIsAllowedInCurrentStateFromStateMachine(salesOrderCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      DeleteNotAllowedPerStateException deleteNotAllowedPerStateException =
          new DeleteNotAllowedPerStateException();
      deleteNotAllowedPerStateException.initCause(e);
      throw deleteNotAllowedPerStateException;
    }
  }

  private void checkIfActionIsAllowedInCurrentStateFromStateMachine(
      String salesOrderCode, String actionName) throws Exception {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);
    DObSalesOrderStateMachine salesOrderStateMachine = new DObSalesOrderStateMachine();
    checkIfActionIsAllowedInCurrentState(
        salesOrderStateMachine, salesOrderGeneralModel, actionName);
  }

  public void setSalesOrderGeneralModelRep(DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    this.salesOrderGeneralModelRep = salesOrderGeneralModelRep;
  }

  public AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();

    configuration.addPermissionConfig(
        ICObSalesOrderType.SYS_NAME, IActionsNames.READ_ALL, "ReadSalesOrderType");

    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL, "ReadBusinessUnit");

    configuration.addPermissionConfig(
        IMObCustomer.SYS_NAME, IActionsNames.READ_ALL, "ReadCustomer");

    configuration.addPermissionConfig(
        ICObSalesResponsible.SYS_NAME, IActionsNames.READ_ALL, "ReadSalesResponsible");

    configuration.addPermissionConfig(ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadCompany");

    return configuration;
  }

  public void applyAuthorizationForCreateValueObjectValues(DObSalesOrderCreateValueObject valueObject)
      throws Exception {
    AuthorizedPermissionsConfig permissionsConfig = constructCreateConfiguration();
    applyAuthorizationForValueObjectAuthorizedReads(permissionsConfig);

    applyAuthorizationForBusinessUnitCode(valueObject.getBusinessUnitCode());
    applyAuthorizationForCustomerCode(valueObject.getCustomerCode());
  }

  private void applyAuthorizationForBusinessUnitCode(String businessUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel businessUnit =
        purchasingUnitGeneralModelRep.findOneByUserCode(businessUnitCode);
    if (businessUnit != null) {
      authorizationManager.authorizeActionOnObject(businessUnit, IActionsNames.READ_ALL);
    }
  }

  private void applyAuthorizationForCustomerCode(String customerCode) {
    List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnits =
        customerBusinessUnitGeneralModelRep.findAllByUserCode(customerCode);
    if (!customerBusinessUnits.isEmpty()) {
      authorizeActionOnCustomer(customerBusinessUnits, IActionsNames.READ_ALL);
    }
  }

  public void authorizeActionOnCustomer(
      List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnitsList, String actionName) {
    boolean isAuthorized = false;
    Exception exception = null;
    for (IObCustomerBusinessUnitGeneralModel customerBusinessUnit : customerBusinessUnitsList) {
      try {
        authorizationManager.authorizeAction(customerBusinessUnit, actionName);
        isAuthorized = true;
        break;
      } catch (Exception e) {
        exception = e;
      }
    }
    if (!isAuthorized) {
      throw new AuthorizationException(exception);
    }
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setCustomerBusinessUnitGeneralModelRep(
      IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep) {
    this.customerBusinessUnitGeneralModelRep = customerBusinessUnitGeneralModelRep;
  }
}
