package com.ebs.dda.rest.accounting.notesreceivables.activation;

@FunctionalInterface
public interface ActivateNotesReceivable {
  String activate(NotesReceivableActivateContextBuilder.NotesReceivableActivateContext context)
      throws Exception;
}
