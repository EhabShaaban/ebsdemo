package com.ebs.dda.rest.accounting.costing.activation;

public interface ActivateCosting {
    String activate(CostingActivateContextBuilder.CostingActivateContext context) throws Exception;
}
