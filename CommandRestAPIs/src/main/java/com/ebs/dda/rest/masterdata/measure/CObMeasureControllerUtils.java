package com.ebs.dda.rest.masterdata.measure;

import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.rest.CommonControllerUtils;

public class CObMeasureControllerUtils extends CommonControllerUtils {

  public CObMeasureControllerUtils(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

}
