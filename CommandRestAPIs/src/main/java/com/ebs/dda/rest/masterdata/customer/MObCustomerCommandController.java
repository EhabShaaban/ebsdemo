package com.ebs.dda.rest.masterdata.customer;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.masterdata.customer.MObCustomerCreateCommand;
import com.ebs.dda.commands.masterdata.customer.MObCustomerDeleteCommand;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.masterdata.customer.IObCustomerBusinessUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerCreateValueObject;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerDeletionValueObject;
import com.ebs.dda.masterdata.customer.MObCustomerJsonSchema;
import com.ebs.dda.masterdata.customer.MObCustomerStateMachine;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.masterdata.customer.MObCustomerCreateMandatoryValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;

@RestController
@RequestMapping(value = "command/customer")
public class MObCustomerCommandController implements InitializingBean {

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private MObCustomerCreateCommand customerCreateCommand;
  @Autowired private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  @Autowired private MObCustomerCreateMandatoryValidator validator;
  @Autowired private MObCustomerGeneralModelRep customerGeneralModelRep;
  @Autowired private MObCustomerDeleteCommand customerDeleteCommand;
  @Autowired private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;

  private CommonControllerUtils commonControllerUtils;
  private MObCustomerControllerUtils utils;

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String getCreateConfiguration() {

    authorizationManager.authorizeAction(IMObCustomer.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = utils.constructCreateConfiguration();
    List<String> authorizedReads =
        commonControllerUtils.getAuthorizedReads(createPermissionsConfig);

    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return CommonControllerUtils.serializeEditConfigurationsResponse(createConfig);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createCustomer(@RequestBody String customerData) throws Exception {

    utils.applyAuthorizationForCreateAction();

    CommonControllerUtils.applySchemaValidationForString(
        MObCustomerJsonSchema.CREATE, customerData);

    MObCustomerCreateValueObject customerCreateValueObject =
        (MObCustomerCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                customerData, MObCustomerCreateValueObject.class);

    validator.validate(customerCreateValueObject);
    ValidationResult validationResult = validator.validateOnBusiness(customerCreateValueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }

    utils.applyAuthorizationForValueObjectValues(customerCreateValueObject);

    StringBuilder userCode = new StringBuilder();
    customerCreateCommand
        .executeCommand(customerCreateValueObject)
        .subscribe(code -> userCode.append(code));
    return commonControllerUtils.getSuccessCreationResponse(String.valueOf(userCode));
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{customerCode}")
  public @ResponseBody String deleteCustomer(@PathVariable String customerCode) throws Exception {
    customerDeleteCommand.checkIfInstanceExists(customerCode);

    List<IObCustomerBusinessUnitGeneralModel> customerBusinessUnitsList =
        customerBusinessUnitGeneralModelRep.findAllByUserCode(customerCode);
    utils.authorizeActionOnCustomer(customerBusinessUnitsList, IActionsNames.DELETE);

    utils.checkIfActionIsAllowedInCurrentState(
        new MObCustomerStateMachine(), customerBusinessUnitsList.get(0), IActionsNames.DELETE);

    MObCustomerDeletionValueObject valueObject = new MObCustomerDeletionValueObject();
    valueObject.setCustomerCode(customerCode);

    customerDeleteCommand.executeCommand(valueObject);

    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    utils = new MObCustomerControllerUtils(authorizationManager);
    utils.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
  }
}
