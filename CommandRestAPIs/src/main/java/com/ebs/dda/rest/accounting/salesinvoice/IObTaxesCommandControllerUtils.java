package com.ebs.dda.rest.accounting.salesinvoice;

import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxes;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;

public class IObTaxesCommandControllerUtils extends DObSalesInvoiceCommandControllerUtils {


  public IObTaxesCommandControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public void checkIfSalesInvoiceTaxExists(IObSalesInvoiceTaxesGeneralModel salesInvoiceTaxes)
      throws RecordOfInstanceNotExistException {
    if (salesInvoiceTaxes == null) {
      throw new RecordOfInstanceNotExistException(IObSalesInvoiceTaxes.class.getName());
    }
  }
}
