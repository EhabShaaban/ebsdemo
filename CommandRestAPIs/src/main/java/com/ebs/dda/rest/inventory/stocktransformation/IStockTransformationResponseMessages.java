package com.ebs.dda.rest.inventory.stocktransformation;

public interface IStockTransformationResponseMessages {
  String SUCCESSFUL_POST_CODE = "TRAN-MSG-03";
}
