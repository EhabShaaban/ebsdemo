package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.order.salesorder.*;
import com.ebs.dda.masterdata.item.IMObItemActionNames;
import com.ebs.dda.order.salesorder.IDObSalesOrderSectionNames;
import com.ebs.dda.order.salesorder.IObSalesOrderItemsEditabilityManger;
import com.ebs.dda.order.salesorder.ISalesOrderItemsMandatoryAttributes;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.Set;

public class IObSalesOrderItemsControllerUtils extends DObSalesOrderControllerUtils {

  private IObSalesOrderItemsEditabilityManger salesOrderItemsEditabilityManger;
  private EntityLockCommand<DObSalesOrder> entityLockCommand;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObSalesOrderItemsControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObSalesOrder> entityLockCommand) {
    super(authorizationManager, entityLockCommand);
    this.entityLockCommand = entityLockCommand;
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void lockSalesOrderSectionForEdit(
      String salesOrderCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(salesOrderCode, sectionName);
    checkIfUpdateActionIsAllowedInSalesOrderCurrentState(salesOrderCode, sectionName, actionName);
  }

  public void unlockSection(String sectionName, String salesOrderCode) throws Exception {
    entityLockCommand.unlockSection(salesOrderCode, sectionName);
  }

  protected Set<String> getEnableAttributesForAddItem(
      DObSalesOrderGeneralModel salesOrderGeneralModel) {
    return salesOrderItemsEditabilityManger
        .getEnabledAttributesConfig()
        .get(salesOrderGeneralModel.getCurrentState());
  }

  protected AuthorizedPermissionsConfig getSalesOrderِAddItemAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(IMObItem.SYS_NAME, IActionsNames.READ_ALL, "ReadItems");
    permissionsConfig.addPermissionConfig(
            IMObItem.SYS_NAME, IMObItemActionNames.VIEW_UNIT_OF_MEASURE_SECTION, "ReadItemUoM");
    return permissionsConfig;
  }

  public void setSalesOrderItemEditabilityManger(
      IObSalesOrderItemsEditabilityManger salesOrderItemsEditabilityManger) {
    this.salesOrderItemsEditabilityManger = salesOrderItemsEditabilityManger;
  }

  public String[] getItemsMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            ISalesOrderItemsMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public void checkIfSalesOrderItemExists(
      String salesOrderCode, IObSalesOrderItemGeneralModel salesOrderItemGeneralModel)
      throws Exception {
    if (salesOrderItemGeneralModel == null) {
      entityLockCommand.unlockSection(salesOrderCode, IDObSalesOrderSectionNames.ITEMS_SECTION);
      throw new RecordOfInstanceNotExistException(IObSalesOrderItem.class.getName());
    }
  }

  public IObSalesOrderItemDeletionValueObject constructSalesOrderItemDeletionValueObject(
      String salesOrderCode, Long itemIdPathVariable) {
    IObSalesOrderItemDeletionValueObject valueObject = new IObSalesOrderItemDeletionValueObject();
    valueObject.setSalesOrderCode(salesOrderCode);
    valueObject.setSalesOrderItemId(itemIdPathVariable);
    return valueObject;
  }

  public void applyAuthorizationForSaveValueObjectValues() {
    AuthorizedPermissionsConfig permissionsConfig =
        getSalesOrderِAddItemAuthorizedPermissionsConfig();
    applyAuthorizationForValueObjectAuthorizedReads(permissionsConfig);
  }
}
