package com.ebs.dda.rest.accounting.notesreceivables.activation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.functions.validate.ValidateSchemaContext;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableActivateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IDObMonetaryNotes;

public class NotesReceivableActivateContextBuilder {

  private NotesReceivableActivateContextBuilder() {}

  public static NotesReceivableActivateContext newContext() {
    return new NotesReceivableActivateContext();
  }

  public static class NotesReceivableActivateContext
      implements ActivateContext,
          AuthorizeContext,
          ValidateSchemaContext,
          FindEntityContext,
          LockContext,
          DeserializeValueObjectContext<DObNotesReceivableActivateValueObject>,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<DObNotesReceivableActivateValueObject>,
          ExecuteCommandContext<DObNotesReceivableActivateValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext {

    private String objectCode;
    private String activateData;
    private DObNotesReceivableActivateValueObject valueObject;
    private String response;
    private String journalEntryUserCode;
    private EntitiesOfInterestContainer entitiesOfInstrestContainer;

    private NotesReceivableActivateContext() {
      this.entitiesOfInstrestContainer =
          new EntitiesOfInterestContainer(DObNotesReceivablesGeneralModel.class);
    }

    @Override
    public String state() { return BasicStateMachine.ACTIVE_STATE; }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public String activateData() {
      return activateData;
    }

    @Override
    public String schema() {
      return IDObNotesReceivableJsonSchema.ACTIVATION_SCHEMA;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return IDObMonetaryNotes.NOTES_RECEIVABLE_SYS_NAME;
    }

    @Override
    public String actionName() {
      return IActionsNames.ACTIVATE;
    }

    public NotesReceivableActivateContext objectCode(String notesReceivableCode) {
      this.objectCode = notesReceivableCode;
      return this;
    }

    public NotesReceivableActivateContext valueObject(String valueObject) {
      this.activateData = valueObject;
      return this;
    }

    @Override
    public Class<DObNotesReceivableActivateValueObject> valueObjectClass() {
      return DObNotesReceivableActivateValueObject.class;
    }

    @Override
    public void valueObject(DObNotesReceivableActivateValueObject valueObject) {
      this.valueObject = valueObject;
    }

    @Override
    public DObNotesReceivableActivateValueObject valueObject() {
      return this.valueObject;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return journalEntryUserCode;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInstrestContainer;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      this.journalEntryUserCode = userCode;
    }
  }
}
