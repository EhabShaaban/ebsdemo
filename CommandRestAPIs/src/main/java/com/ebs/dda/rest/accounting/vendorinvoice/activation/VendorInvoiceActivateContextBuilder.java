package com.ebs.dda.rest.accounting.vendorinvoice.activation;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dda.accounting.vendorinvoice.apis.IDOVendorInvoiceSchema;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObInvoiceActionNames;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.functions.validate.ValidateSchemaContext;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceActivateValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;

public class VendorInvoiceActivateContextBuilder {

    private VendorInvoiceActivateContextBuilder() {
    }

    public static VendorInvoiceActivateContext newContext() {
        return new VendorInvoiceActivateContext();
    }

    public static class VendorInvoiceActivateContext
            implements ActivateContext,
            AuthorizeContext,
            FindEntityContext,
            ValidateSchemaContext,
            LockContext,
            DeserializeValueObjectContext<DObVendorInvoiceActivateValueObject>,
            ValidateIfActionIsAllowedContext,
            ValidateMissingFieldsForStateContext,
            ValidateBusinessRulesContext<DObVendorInvoiceActivateValueObject>,
            ExecuteCommandContext<DObVendorInvoiceActivateValueObject>,
            ExecuteActivateDependanciesContext,
            SerializeActivateResponseContext {

        private String objectCode;
        private String activateData;
        private DObVendorInvoiceActivateValueObject valueObject;
        private String response;
        private String journalEntryUserCode;
        private EntitiesOfInterestContainer entitiesOfInstrestContainer;

        public VendorInvoiceActivateContext() {
            this.entitiesOfInstrestContainer =
                    new EntitiesOfInterestContainer(DObVendorInvoiceGeneralModel.class);
        }

        @Override
        public String objectCode() {
            return objectCode;
        }

        @Override
        public String activateData() {
            return activateData;
        }

        @Override
        public String serializeResponse() {
            return response;
        }

        @Override
        public String objectSysName() {
            return IDObVendorInvoice.SYS_NAME;
        }

        @Override
        public String actionName() {
            return IDObInvoiceActionNames.CREATE_JOURNAL_ENTRY_INVOICE;
        }

        @Override
        public void journalEntryUserCode(String userCode) {
            this.journalEntryUserCode = userCode;
        }

        @Override
        public String state() {
            return BasicStateMachine.ACTIVE_STATE;
        }

        public VendorInvoiceActivateContext objectCode(String vendorInvoiceCode) {
            this.objectCode = vendorInvoiceCode;
            return this;
        }

        public VendorInvoiceActivateContext valueObject(String valueObject) {
            this.activateData = valueObject;
            return this;
        }

        @Override
        public String schema() {
            return IDOVendorInvoiceSchema.ACTIVATION_SCHEMA;
        }

        @Override
        public Class<DObVendorInvoiceActivateValueObject> valueObjectClass() {
            return DObVendorInvoiceActivateValueObject.class;
        }

        @Override
        public void valueObject(DObVendorInvoiceActivateValueObject valueObject) {
            this.valueObject = valueObject;
        }

        @Override
        public DObVendorInvoiceActivateValueObject valueObject() {
            return this.valueObject;
        }

        @Override
        public void response(String response) {
            this.response = response;
        }

        @Override
        public String successMessage() {
            return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
        }

        @Override
        public String journalEntryUserCode() {
            return journalEntryUserCode;
        }

        @Override
        public EntitiesOfInterestContainer entitiesOfInterestContainer() {
            return this.entitiesOfInstrestContainer;
        }
    }
}
