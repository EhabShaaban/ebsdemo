package com.ebs.dda.rest.accounting.collection.activation;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum;

@Component
public class CollectionActivateBeanFactory implements ApplicationContextAware {

  private ApplicationContext applicationContext;

  @Override
  public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
    this.applicationContext = applicationContext;
  }

  public ActivateCollection getBean(String collectionType) {
    if (collectionType.equals(RefDocumentTypeEnum.SALES_INVOICE)) {
      return applicationContext.getBean("CollectionSalesInvoiceActivate", ActivateCollection.class);
    }
    if (collectionType.equals(RefDocumentTypeEnum.SALES_ORDER)) {
      return applicationContext.getBean("CollectionSalesOrderActivate", ActivateCollection.class);
    }
    if (collectionType.equals(RefDocumentTypeEnum.DEBIT_NOTE)) {
      return applicationContext.getBean("CollectionAccountingNoteActivate", ActivateCollection.class);
    }
    if (collectionType.equals(RefDocumentTypeEnum.NOTES_RECEIVABLE)) {
      return applicationContext.getBean("CollectionMonetaryNotesActivate", ActivateCollection.class);
    }
    throw new ArgumentViolationSecurityException();
  }
}
