package com.ebs.dda.rest.order.purchaseorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.editability.AbstractEditabilityManager;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.plant.ICObPlant;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.masterdata.dbo.jpa.entities.businessobjects.attachement.apis.ILObAttachmentType;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects.IObOrderLineDetailsQuantitiesRep;
import com.ebs.dda.purchases.apis.IPurchaseOrderItemActions;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetails;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.informationobjects.IObOrderLineDetailsQuantities;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderLineDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderCreateValueObject;
import com.ebs.dda.purchases.statemachines.DObImportPurchaseOrderStateMachine;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IItemMandatoryAttributes;
import com.ebs.dda.purchases.validation.mandatoryvalidator.apis.IRequiredDocumentsMandatoryAttributes;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DObPurchaseOrderControllerUtils extends CommonControllerUtils {

  private static final String RESPONSE_STATUS = "status";
  private static final String CODE_RESPONSE_KEY = "code";
  public final String DELETION_SUCCESSFUL_CODE = "Gen-msg-12";
  public final String MISSING_FIELDS_CODE = "Gen-msg-33";
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  private final String USER_CODE_KEY = "userCode";
  private final String CREATION_SUCCESSFUL_CODE = "Gen-msg-11";
  private final String UPLOAD_SUCCESSFUL_CODE = "Gen-msg-23";
  private final String MISSING_FIELDS_KEY = "missingFields";
  protected MandatoryValidatorUtils mandatoryValidatorUtils;
  private IObOrderLineDetailsRep orderLineDetailsRep;
  private IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep;
  private DObPurchaseOrderGeneralModelRep orderGeneralModelRep;
  private EntityLockCommand<DObOrderDocument> entityLockCommand;
  private IObOrderLineDetailsQuantitiesRep orderLineDetailsQuantitiesRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep;

  public DObPurchaseOrderControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  void initOrderLineDetailsReps(
      IObOrderLineDetailsRep orderLineDetailsRep,
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep,
      DObPurchaseOrderGeneralModelRep orderGeneralModelRep,
      IObOrderLineDetailsQuantitiesRep orderLineDetailsQuantitiesRep) {
    this.orderLineDetailsRep = orderLineDetailsRep;
    this.orderLineDetailsGeneralModelRep = orderLineDetailsGeneralModelRep;
    this.orderGeneralModelRep = orderGeneralModelRep;
    this.orderLineDetailsQuantitiesRep = orderLineDetailsQuantitiesRep;
  }

  void applyAuthorizationForCreateAction(DObPurchaseOrderCreateValueObject valueObject) {
    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.CREATE);
  }

  void applyAuthorization(
      DObPurchaseOrderGeneralModelRep orderGeneralModelRep, String code, String actionName)
      throws Exception {
    DObPurchaseOrderGeneralModel order = orderGeneralModelRep.findOneByUserCode(code);
    if (order == null) {
      throw new InstanceNotExistException();
    }
    applyAuthorization(order, actionName);
  }

  void checkPOItemsExist(IObOrderLineDetails orderLineDetails)
      throws RecordOfInstanceNotExistException {
    if (orderLineDetails == null) {
      throw new RecordOfInstanceNotExistException(IObOrderLineDetails.class.getName());
    }
  }

  void checkPOExists(DObPurchaseOrderGeneralModel order) throws Exception {
    if (order == null) {
      throw new InstanceNotExistException();
    }
  }

  IObOrderLineDetails readOrderLineByPOAndItemCode(String purchaseOrderCode, String itemCode) {
    IObOrderLineDetailsGeneralModel itemOrderGeneralModel =
        orderLineDetailsGeneralModelRep.findByOrderCodeAndItemCode(purchaseOrderCode, itemCode);
    return (itemOrderGeneralModel != null)
        ? orderLineDetailsRep.findOneById(itemOrderGeneralModel.getId())
        : null;
  }

  public void applyAuthorizationForPlant(String actionName) {
    authorizationManager.authorizeAction(ICObPlant.SYS_NAME, actionName);
  }

  public void applyAuthorizationForStorehouse(String actionName) {
    authorizationManager.authorizeAction(ICObStorehouse.SYS_NAME, actionName);
  }

  public void applyAuthorizationOnConsigneeSectionFields() {
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
    applyAuthorizationForPlant(IActionsNames.READ_ALL);
    applyAuthorizationForStorehouse(IActionsNames.READ_ALL);
  }

  public void applyAuthorizationOnCompanySectionFields(
      DObPurchaseOrderGeneralModel orderGeneralModel) {
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
  }

  public void applyAuthorizationForDocumentTypeList(String actionName) {
    authorizationManager.authorizeAction(ILObAttachmentType.SYS_NAME, actionName);
  }

  public void applyAuthorizationOnRequiredDocumentsSectionFields() {
    applyAuthorizationForDocumentTypeList(IActionsNames.READ_ALL);
  }

  private void applyAuthorization(DObPurchaseOrderGeneralModel order, String permissions)
      throws Exception {
    authorizationManager.authorizeAction(order, permissions);
  }

  // TODO: Remove it and use CommonControllerUtils.getFailureResponseWithMissingFields instead
  public String getFailureResponseWithValidationErrorsAndCode(
      Map<String, List<String>> validationErrors, String code) {
    JsonParser parser = new JsonParser();
    JsonObject response = new JsonObject();
    response.add(MISSING_FIELDS_KEY, parser.parse(new Gson().toJson(validationErrors)));
    response.addProperty(RESPONSE_STATUS, FAIL_RESPONSE_KEY);
    response.addProperty(CODE_RESPONSE_KEY, code);
    return response.toString();
  }

  protected String getSuccessUploadResponse() {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(CODE_RESPONSE_KEY, UPLOAD_SUCCESSFUL_CODE);
    return successResponse.toString();
  }

  protected boolean isImport(DObPurchaseOrderGeneralModel dObPurchaseOrder) {
    return dObPurchaseOrder.getObjectTypeCode().equals(OrderTypeEnum.IMPORT_PO.name());
  }

  public String getItemEditConfiguration(
      DObPurchaseOrderGeneralModelRep purchaseOrderRep,
      AbstractEditabilityManager editabilityManager,
      String purchaseOrderCode) {

    DObPurchaseOrderGeneralModel dObPurchaseOrder =
        purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    String[] mandatoriesResponse = getMandatoriesResponse(dObPurchaseOrder);
    Set<String> enabledAttributes =
        editabilityManager.getEnabledAttributesInGivenStates(dObPurchaseOrder.getCurrentStates());
    List<String> itemAuthorizedReads = getItemAuthorizedReads();

    return serializeEditConfigurationsResponse(
        enabledAttributes, mandatoriesResponse, itemAuthorizedReads);
  }

  public String getItemQuantityEditConfiguration(
      DObPurchaseOrderGeneralModelRep purchaseOrderRep,
      AbstractEditabilityManager editabilityManager,
      String purchaseOrderCode) {

    DObPurchaseOrderGeneralModel dObPurchaseOrder =
        purchaseOrderRep.findOneByUserCode(purchaseOrderCode);
    String[] mandatoriesResponse = getQuantityMandatoriesResponse(dObPurchaseOrder);
    Set<String> enabledAttributes =
        editabilityManager.getEnabledAttributesInGivenStates(dObPurchaseOrder.getCurrentStates());
    List<String> itemAuthorizedReads = getPurchaseOrderItemQuantitySectionAuthorizedReads();

    return serializeEditConfigurationsResponse(
        enabledAttributes, mandatoriesResponse, itemAuthorizedReads);
  }

  private String[] getMandatoriesResponse(DObPurchaseOrderGeneralModel dObPurchaseOrder) {
    return mandatoryValidatorUtils
        .getMandatoryAttributesByState(
            IItemMandatoryAttributes.ITEM_MANDATORIES, dObPurchaseOrder.getCurrentStates())
        .toArray(new String[0]);
  }

  private String[] getQuantityMandatoriesResponse(DObPurchaseOrderGeneralModel dObPurchaseOrder) {
    return mandatoryValidatorUtils
        .getMandatoryAttributesByState(
            IItemMandatoryAttributes.ITEM_QUANTITES_MANDATORIES,
            dObPurchaseOrder.getCurrentStates())
        .toArray(new String[0]);
  }

  private List<String> getItemAuthorizedReads() {
    List<String> authReads = new ArrayList<>();
    isAuthorizedReadPermission(IMObItem.SYS_NAME, IPurchaseOrderItemActions.READ_ITEMS, authReads);
    return authReads;
  }

  private List<String> getPurchaseOrderItemQuantitySectionAuthorizedReads() {
    List<String> authorizedReads = new ArrayList<>();
    boolean isReadMeasuresAllowed =
        isReadPermissionAllowed(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);

    if (isReadMeasuresAllowed) {
      authorizedReads.add("ReadAllMeasures");
    }
    return authorizedReads;
  }

  public String[] getRequiredDocumentsMandatoryAttributesByState(
      DObPurchaseOrderGeneralModel dObOrder) {

    String[] mandatoriesResponse =
        mandatoryValidatorUtils
            .getMandatoryAttributesByState(
                IRequiredDocumentsMandatoryAttributes.VALUE_OBJECT_MANDATORIES,
                dObOrder.getCurrentStates())
            .toArray(new String[0]);
    return mandatoriesResponse != null ? mandatoriesResponse : new String[] {};
  }

  protected void isAuthorizedReadPermission(
      String objectSysName, String authReadKey, List<String> authReads) {
    try {
      authorizationManager.authorizeAction(objectSysName, IActionsNames.READ_ALL);
      authReads.add(authReadKey);
    } catch (AuthorizationException exception) {
    }
  }

  public void setOrderGeneralModelRep(DObPurchaseOrderGeneralModelRep orderGeneralModelRep) {
    this.orderGeneralModelRep = orderGeneralModelRep;
  }

  public void checkIfActionIsAllowedInCurrentState(String purchaseOrderCode, String actionName)
      throws Exception {
    DObPurchaseOrderGeneralModel orderDocument =
        orderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(orderDocument);
    purchaseOrderStateMachine.isAllowedAction(actionName);
  }

  public void checkIfActionIsAllowedInCurrentState(
      DObPurchaseOrderGeneralModel orderDocument, String actionName) throws Exception {
    AbstractStateMachine purchaseOrderStateMachine = getPurchaseOrderStateMachine(orderDocument);
    purchaseOrderStateMachine.isAllowedAction(actionName);
  }

  private AbstractStateMachine getPurchaseOrderStateMachine(
      DObPurchaseOrderGeneralModel orderDocument) throws Exception {
    DObPurchaseOrderStateMachineFactory purchaseOrderFactory =
        new DObPurchaseOrderStateMachineFactory();
    AbstractStateMachine purchaseOrderStateMachine =
        purchaseOrderFactory.createPurchaseOrderStateMachine(orderDocument);
    return purchaseOrderStateMachine;
  }

  public void setEntityLockCommand(EntityLockCommand<DObOrderDocument> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void lockPurchaseOrderSectionForEdit(
      String purchaseOrderCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(purchaseOrderCode, sectionName);
    try {
      checkIfActionIsAllowedInCurrentState(purchaseOrderCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(purchaseOrderCode, sectionName);
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
  }

  public void applyAuthorizationOnItemSectionFields() {
    authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
  }

  public void applyAuthorizationOnItemQuantitySectionFields() {
    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
  }

  void checkIfQuantityExists(Long quantityId) throws RecordOfInstanceNotExistException {

    IObOrderLineDetailsQuantities orderLineDetailsQuantities =
        orderLineDetailsQuantitiesRep.findOneById(quantityId);
    if (orderLineDetailsQuantities == null)
      throw new RecordOfInstanceNotExistException(IObOrderLineDetailsQuantities.class.getName());
  }

  public boolean arrivedPOHasMissingFields(
      Map<String, List<String>> missingFields, DObPurchaseOrderGeneralModel orderGeneralModel) {
    return !missingFields.isEmpty() && purchaseOrderIsArrived(orderGeneralModel);
  }

  private boolean purchaseOrderIsArrived(DObPurchaseOrderGeneralModel orderGeneralModel) {
    return orderGeneralModel
        .getCurrentStates()
        .contains(DObImportPurchaseOrderStateMachine.ARRIVED_STATE);
  }

  public AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();

    configuration.addPermissionConfig(
        IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadPOTypes");

    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL, "ReadBusinessUnits");
    configuration.addPermissionConfig(IMObVendor.SYS_NAME, IActionsNames.READ_ALL, "ReadVendors");

    configuration.addPermissionConfig(
        IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadReferenceDocuments");

    configuration.addPermissionConfig(
        IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL, "ReadDocumentOwners");
    return configuration;
  }

  public void applyAuthorizationForValueObjectValues(DObPurchaseOrderCreateValueObject valueObject)
      throws Exception {

    authorizePurchaseOrderType();
    authorizeDocumentOwner();
    applyAuthorizationForBusinessUnit(valueObject.getBusinessUnitCode());
    applyAuthorizationForVendor(valueObject.getVendorCode());
    authorizeReferencePurchaseOrder(valueObject.getReferencePurchaseOrderCode());
  }

  private void authorizeDocumentOwner() {
    authorizationManager.authorizeAction(
        DocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL);
  }

  private void authorizePurchaseOrderType() {
    authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL);
  }

  private void applyAuthorizationForVendor(String vendorCode) throws Exception {
    List<MObVendorPurchaseUnitGeneralModel> vendorPurchaseUnitGeneralModeList =
        vendorPurchaseUnitGeneralModelRep.findAllByUserCode(vendorCode);
    for (MObVendorPurchaseUnitGeneralModel vendor : vendorPurchaseUnitGeneralModeList) {
      try {
        authorizationManager.authorizeAction(vendor, IActionsNames.READ_ALL);
        break;
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  private void applyAuthorizationForBusinessUnit(String businessUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        purchasingUnitGeneralModelRep.findOneByUserCode(businessUnitCode);
    if (purchasingUnitGeneralModel != null) {
      try {
        authorizationManager.authorizeAction(
            purchasingUnitGeneralModel, IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  private void authorizeReferencePurchaseOrder(String referencePO) throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrder =
        orderGeneralModelRep.findOneByUserCode(referencePO);
    if (purchaseOrder != null) {
      try {
        authorizationManager.authorizeAction(purchaseOrder, IActionsNames.READ_ALL);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setVendorPurchaseUnitGeneralModelRep(
      MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep) {
    this.vendorPurchaseUnitGeneralModelRep = vendorPurchaseUnitGeneralModelRep;
  }
}
