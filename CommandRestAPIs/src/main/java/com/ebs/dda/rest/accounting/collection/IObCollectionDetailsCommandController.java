package com.ebs.dda.rest.accounting.collection;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.security.services.DObCollectionAuthorizationManager;
import com.ebs.dda.accounting.collection.apis.IDObCollectionActionNames;
import com.ebs.dda.accounting.collection.apis.IDObCollectionSectionNames;
import com.ebs.dda.accounting.collection.apis.IIObCollectionDetailsJsonSchema;
import com.ebs.dda.accounting.collection.editability.IObCollectionDetailsEditabilityManager;
import com.ebs.dda.commands.accounting.collection.IObCollectionSaveCollectionDetailsCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsValueObject;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.collection.IObCollectionDetailsValidator;
import com.google.gson.Gson;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "command/accounting/collection")
public class IObCollectionDetailsCommandController implements InitializingBean {

  @Autowired EntityLockCommand<DObCollection> dObCollectionLockCommand;
  @Autowired private DObCollectionAuthorizationManager authorizationManager;
  @Autowired private DObCollectionRep collectionRep;
  @Autowired private DObCollectionGeneralModelRep collectionGeneralModelRep;
  @Autowired private IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep;
  @Autowired private IObCollectionDetailsEditabilityManager collectionDetailsEditabilityManager;

  @Autowired
  private IObCollectionSaveCollectionDetailsCommand collectionSaveCollectionDetailsCommand;

  @Autowired private EntityLockCommand<DObCollection> collectionEntityLockCommand;
  @Autowired private IObCollectionDetailsValidator collectionDetailsValidator;

  private IObCollectionDetailsControllerUtils controllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "/{collectionCode}/lock/collectiondetails")
  public @ResponseBody String openCollectionDetailsSectionEditMode(
      @PathVariable String collectionCode, HttpServletResponse response) throws Exception {
    DObCollectionGeneralModel collectionGeneralModel = checkIfInstanceExists(collectionCode);
    authorizationManager.authorizeActionOnSection(
        collectionGeneralModel, IDObCollectionActionNames.UPDATE_COLLECTION_DETAILS);

    IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
        collectionDetailsGeneralModelRep.findByCollectionCode(collectionCode);

    controllerUtils.lockCollectionSectionForEdit(
        collectionCode,
        IDObCollectionSectionNames.COLLECTION_DETAILS,
        IDObCollectionActionNames.UPDATE_COLLECTION_DETAILS);

    Set<String> stateEnabledAttributes =
        collectionDetailsEditabilityManager.getEnabledAttributesInGivenStates(
            collectionGeneralModel.getCurrentStates());
    Set<String> collectionMethodEnabledAttributes =
        controllerUtils.getEnableAttributesByMethod(collectionDetailsGeneralModel);
    stateEnabledAttributes.addAll(collectionMethodEnabledAttributes);

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getEditCollectionDetailsAuthorizedPermissionsConfig(collectionCode);
    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  public DObCollectionGeneralModel checkIfInstanceExists(String userCode)
      throws InstanceNotExistException {
    DObCollectionGeneralModel collection = collectionGeneralModelRep.findOneByUserCode(userCode);
    if (collection == null) {
      throw new InstanceNotExistException();
    }
    return collection;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{collectionCode}/unlock/collectiondetails")
  public @ResponseBody String cancelCollectionDetailsEditMode(@PathVariable String collectionCode)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel = checkIfInstanceExists(collectionCode);
    authorizationManager.authorizeActionOnSection(
        collectionGeneralModel, IDObCollectionActionNames.UPDATE_COLLECTION_DETAILS);
    return controllerUtils.unlockSection(
        IDObCollectionSectionNames.COLLECTION_DETAILS, collectionCode);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "{collectionCode}/update/collectiondetails")
  public @ResponseBody String saveCollectionDetails(
      @PathVariable String collectionCode, @RequestBody String collectionDetails) throws Exception {

    CommonControllerUtils.applySchemaValidationForString(
        IIObCollectionDetailsJsonSchema.COLLECTION_DETAILS_SCHEMA, collectionDetails);

    DObCollectionGeneralModel collectionGeneralModel = checkIfInstanceExists(collectionCode);

    controllerUtils.applyAuthorizationForSave(collectionGeneralModel);

    IObCollectionDetailsValueObject collectionDetailsValueObject =
        new Gson().fromJson(collectionDetails, IObCollectionDetailsValueObject.class);
    collectionDetailsValueObject.setCollectionCode(collectionCode);

    collectionEntityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        collectionCode, IDObCollectionSectionNames.COLLECTION_DETAILS);
    collectionDetailsValidator.validate(collectionGeneralModel, collectionDetailsValueObject);
    try {
      collectionSaveCollectionDetailsCommand.executeCommand(collectionDetailsValueObject);
    } finally {
      collectionEntityLockCommand.unlockSection(
          collectionCode, IDObCollectionSectionNames.COLLECTION_DETAILS);
    }
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils = new IObCollectionDetailsControllerUtils();
    controllerUtils.setEntityLockCommand(dObCollectionLockCommand);
    controllerUtils.setCollectionGeneralModelRep(collectionGeneralModelRep);
    controllerUtils.setCollectionDetailsGeneralModelRep(collectionDetailsGeneralModelRep);
    controllerUtils.setAuthorizationManager(authorizationManager);
    controllerUtils.setCollectionDetailsEditabilityManager(collectionDetailsEditabilityManager);
  }
}
