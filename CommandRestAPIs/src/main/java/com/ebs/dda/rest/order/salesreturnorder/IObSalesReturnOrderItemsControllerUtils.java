package com.ebs.dda.rest.order.salesreturnorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObSalesReturnOrderAuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.masterdata.lookups.ILObReason;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IIObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderActionNames;
import com.ebs.dda.order.salesreturnorder.IIObSalesReturnOrderItemsMandatoryAttributes;
import com.ebs.dda.order.salesreturnorder.IObSalesReturnOrderItemsEditabilityManger;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.List;
import java.util.Set;

public class IObSalesReturnOrderItemsControllerUtils extends DObSalesReturnOrderControllerUtils {

  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private IObSalesReturnOrderItemsEditabilityManger salesReturnOrderItemEditabilityManger;
  private DObSalesReturnOrderAuthorizationManager salesReturnOrderAuthorizationManager;

  public IObSalesReturnOrderItemsControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObSalesReturnOrder> entityLockCommand) {
    super(authorizationManager);
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void checkIfItemExistsInSalesReturnOrder(
      IObSalesReturnOrderItemGeneralModel itemGeneralModel)
      throws RecordOfInstanceNotExistException {
    if (itemGeneralModel == null) {
      throw new RecordOfInstanceNotExistException(
          IIObSalesReturnOrderItemGeneralModel.class.getSimpleName());
    }
  }

  public List<String> getEditItemAuthorizedReads() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(ILObReason.SYS_NAME, IActionsNames.READ_ALL, "ReadReturnReason");
    permissionsConfig.addPermissionConfig(
        ICObMeasure.SYS_NAME, IActionsNames.READ_ALL, "ReadItemUoM");

    return getAuthorizedReads(permissionsConfig);
  }

  public String[] getEditItemsMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IIObSalesReturnOrderItemsMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public Set<String> getEditItemsEnabledAttributes(
      DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel) {
    return salesReturnOrderItemEditabilityManger
        .getEnabledAttributesConfig()
        .get(salesReturnOrderGeneralModel.getCurrentState());
  }

  public void applyAuthorizationForSave(String salesReturnCode) throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
            salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);

    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ILObReason.SYS_NAME, IActionsNames.READ_ALL);

    try {
      salesReturnOrderAuthorizationManager.authorizeActionOnSection(
              salesReturnOrderGeneralModel, IDObSalesReturnOrderActionNames.UPDATE_ITEMS);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException(ex);
    }
  }

  public void setSalesReturnOrderGeneralModelRep(
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep) {
    this.salesReturnOrderGeneralModelRep = salesReturnOrderGeneralModelRep;
  }

  public void setSalesReturnOrderItemEditabilityManger(
      IObSalesReturnOrderItemsEditabilityManger salesReturnOrderItemEditabilityManger) {
    this.salesReturnOrderItemEditabilityManger = salesReturnOrderItemEditabilityManger;
  }

  public void setSalesReturnOrderAuthorizationManager(
      DObSalesReturnOrderAuthorizationManager salesReturnOrderAuthorizationManager) {
    this.salesReturnOrderAuthorizationManager = salesReturnOrderAuthorizationManager;
  }
}
