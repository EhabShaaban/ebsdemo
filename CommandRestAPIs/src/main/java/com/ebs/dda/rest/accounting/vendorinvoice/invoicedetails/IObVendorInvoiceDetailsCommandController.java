package com.ebs.dda.rest.accounting.vendorinvoice.invoicedetails;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObInvoiceActionNames;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObInvoiceDetailsMandatoryAttributes;
import com.ebs.dda.accounting.vendorinvoice.apis.IDObVendorInvoiceSectionNames;
import com.ebs.dda.accounting.vendorinvoice.editiablity.IObVendorInvoiceDetailsEditabilityManager;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceDetailsEditCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.IDObPaymentRequest;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsValueObject;
import com.ebs.dda.jpa.masterdata.currency.ICObCurrency;
import com.ebs.dda.jpa.masterdata.paymentterms.ICObPaymentTerms;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.vendorinvoice.IObVendorInvoiceDetailsSaveValidator;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

@RestController
@RequestMapping(value = "command/accounting/vendorinvoice")
public class IObVendorInvoiceDetailsCommandController implements InitializingBean {

  public final String DELETION_SUCCESSFUL_CODE = "Gen-msg-12";
  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private CommonControllerUtils commonControllerUtils;

  @Autowired
  private EntityLockCommand<DObVendorInvoice> entityLockCommand;
  @Autowired
  private AuthorizationManager authorizationManager;
  @Autowired
  private IObVendorInvoiceDetailsGeneralModelRep iObInvoiceDetailsGeneralModelRep;
  @Autowired
  private DObVendorInvoiceRep dObInvoiceRep;
  @Autowired
  private DObVendorInvoiceDetailsEditCommand invoiceDetailsEditCommand;
  @Autowired
  private IObVendorInvoiceDetailsSaveValidator invoiceDetailsValidator;
  @Autowired
  private IObVendorInvoiceDetailsEditabilityManager invoiceDetailsEditabilityManager;

  @RequestMapping(method = RequestMethod.GET, value = "{invoiceCode}/lock/invoicedetails/")
  public @ResponseBody
  String openInvoiceDetailsSectionEditMode(
          @PathVariable String invoiceCode, HttpServletResponse response) throws Exception {

      DObVendorInvoice invoice = dObInvoiceRep.findOneByUserCode(invoiceCode);
    entityLockCommand.checkIfEntityExists(invoiceCode);

      commonControllerUtils.checkIfActionIsAllowedInCurrentState(
              new DObVendorInvoiceStateMachine(), invoice, IDObInvoiceActionNames.POST_INVOICE);
    applyAuthorizationForEditInvoiceDetailsAction(invoiceCode);

    entityLockCommand.lockSection(invoiceCode, IDObVendorInvoiceSectionNames.INVOICE_DETAILS);

    SectionEditConfig editConfiguration = prepareInvoiceDetailsEditConfiguration(invoiceCode);

    return commonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{invoiceCode}/unlock/invoicedetails/")
  public @ResponseBody
  String closeInvoiceDetailsSectionEditMode(
          @PathVariable String invoiceCode, HttpServletResponse response) throws Exception {

    applyAuthorizationForEditInvoiceDetailsAction(invoiceCode);

    entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.INVOICE_DETAILS);
    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{invoiceCode}")
  public @ResponseBody
  String closeAllSectionsInItem(
          @PathVariable String invoiceCode, HttpServletRequest request) throws Exception{
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(invoiceCode, lockedSections);
    return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "{invoiceCode}/update/invoicedetails/")
  public @ResponseBody
  String saveInvoiceDetailsSection(
          @PathVariable String invoiceCode, @RequestBody String invoiceDetailsJsonObject)
          throws Exception {
    commonControllerUtils.applySchemaValidation(
            IObVendorInvoiceDetailsValueObject.class, invoiceDetailsJsonObject);

    applyAuthorizationForSave(invoiceCode);

    entityLockCommand.checkIfEntityExists(invoiceCode);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
            invoiceCode, IDObVendorInvoiceSectionNames.INVOICE_DETAILS);

    IObVendorInvoiceDetailsValueObject invoiceDetailsValueObject =
            (IObVendorInvoiceDetailsValueObject)
                    CommonControllerUtils.deserializeRequestBody(
                            invoiceDetailsJsonObject, IObVendorInvoiceDetailsValueObject.class);
    invoiceDetailsValueObject.setInvoiceCode(invoiceCode);


      ValidationResult validationResult = invoiceDetailsValidator.validate(invoiceDetailsValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      final StringBuilder userCode = new StringBuilder();

      invoiceDetailsEditCommand
              .executeCommand(invoiceDetailsValueObject)
              .subscribe(code -> userCode.append(code));
      entityLockCommand.unlockSection(invoiceCode, IDObVendorInvoiceSectionNames.INVOICE_DETAILS);
      return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
    }
  }

  private void applyAuthorizationForSave(String invoiceCode) throws Exception {
    applyAuthorizationForEditInvoiceDetailsAction(invoiceCode);
    applyAuthorizatioinOnReads();
  }

  private void applyAuthorizatioinOnReads() {
    try {
      authorizationManager.authorizeAction(ICObPaymentTerms.SYS_NAME, IActionsNames.READ_ALL);
      authorizationManager.authorizeAction(ICObCurrency.SYS_NAME, IActionsNames.READ_ALL);
      authorizationManager.authorizeAction(IDObPaymentRequest.SYS_NAME, IActionsNames.READ_ALL);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  private AuthorizedPermissionsConfig constructEditInvoiceDetailsConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
            ICObPaymentTerms.SYS_NAME, IActionsNames.READ_ALL, "ReadPaymentTerms");

    configuration.addPermissionConfig(
            ICObCurrency.SYS_NAME, IActionsNames.READ_ALL, "ReadCurrency");

    configuration.addPermissionConfig(
            IDObPaymentRequest.SYS_NAME, IActionsNames.READ_ALL, "ReadPaymentRequest");

    return configuration;
  }

  private void applyAuthorizationForEditInvoiceDetailsAction(String invoiceCode) throws Exception {
    IObVendorInvoiceDetailsGeneralModel invoiceDetailsGeneralModel =
            iObInvoiceDetailsGeneralModelRep.findByInvoiceCode(invoiceCode);
    try {
      commonControllerUtils.checkAuthorizeForActionOnBusinessObject(
              invoiceDetailsGeneralModel, IDObInvoiceActionNames.EDIT_INVOICE_DETAILS);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
  }

  public String[] getInvoiceMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
            mandatoryValidatorUtils.getMandatoryAttributesByState(
                    IDObInvoiceDetailsMandatoryAttributes.INVOICE_DETAILS_MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[]{};
  }

  private SectionEditConfig prepareInvoiceDetailsEditConfiguration(String invoiceCode) {
    DObVendorInvoice dObInvoice = dObInvoiceRep.findOneByUserCode(invoiceCode);
    String currentState = dObInvoice.getCurrentState();
    Set<String> currentStates = dObInvoice.getCurrentStates();
    List<String> authorizedReads = getInvoiceDetailsAuthorizedReads();
    String[] mandatories = getInvoiceDetailsMandatories(currentState);
    Set<String> enabledAttributes =
            invoiceDetailsEditabilityManager.getEnabledAttributesInGivenStates(currentStates);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setMandatories(mandatories);
    editConfiguration.setEnabledAttributes(enabledAttributes);
    return editConfiguration;
  }

  private String[] getInvoiceDetailsMandatories(String currentState) {
    String[] mandatories = getInvoiceMandatoryAttributesByState(currentState);
    return mandatories;
  }

  private List<String> getInvoiceDetailsAuthorizedReads() {
    AuthorizedPermissionsConfig editRequestPermissionsConfig =
            constructEditInvoiceDetailsConfiguration();
    List<String> authorizedReads =
            commonControllerUtils.getAuthorizedReads(editRequestPermissionsConfig);
    return authorizedReads;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }
}
