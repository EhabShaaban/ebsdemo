package com.ebs.dda.rest.accounting.salesinvoice.activation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/salesinvoice")
public class DObSalesInvoiceActivationController {

    @Autowired
    private SalesInvoiceActivateBeanFactory activateBeanFactory;

    @PutMapping("/{salesInvoiceCode}/activate")
    public @ResponseBody
    String activateSalesInvoice(@PathVariable String salesInvoiceCode,
                              @RequestBody String valueObject,
    @RequestParam(name = "type", required = true) String salesInvoiceType) throws Exception {
        ActivateSalesInvoice activateSalesInvoice = activateBeanFactory.getBean(salesInvoiceType);

        return activateSalesInvoice.activate(SalesInvoiceActivateContextBuilder.newContext()
                .objectCode(salesInvoiceCode).valueObject(valueObject));
    }

}
