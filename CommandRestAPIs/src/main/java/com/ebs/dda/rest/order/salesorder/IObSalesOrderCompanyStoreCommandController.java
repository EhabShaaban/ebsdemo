package com.ebs.dda.rest.order.salesorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderCompanyStoreUpdateCommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCompanyAndStoreValueObject;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.order.salesorder.IDObSalesOrderSectionNames;
import com.ebs.dda.order.salesorder.IObSalesOrderCompanyAndStoreDataJsonSchema;
import com.ebs.dda.order.salesorder.IObSalesOrderCompanyStoreEditabilityManager;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.salesorder.IObSalesOrderCompanyAndStoreDataMandatoryValidator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/order/salesorder")
public class IObSalesOrderCompanyStoreCommandController implements InitializingBean {

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private EntityLockCommand<DObSalesOrder> entityLockCommand;
  @Autowired private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  @Autowired private IObSalesOrderCompanyAndStoreDataMandatoryValidator validator;

  @Autowired
  private IObSalesOrderCompanyStoreEditabilityManager salesOrderCompanyStoreEditabilityManager;

  @Autowired private IObSalesOrderCompanyStoreUpdateCommand companyStoreDataUpdateCommand;

  private IObSalesOrderCompanyStoreControllerUtils controllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/lock/companystoredata")
  public @ResponseBody String openCompanyStoreSectionEditMode(@PathVariable String salesOrderCode)
      throws Exception {

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_COMPANY_AND_STORE_DATA);

    controllerUtils.lockSalesOrderSectionForEdit(
        salesOrderCode,
        IDObSalesOrderSectionNames.COMPANY_AND_STORE_SECTION,
        IDObSalesOrderActionNames.UPDATE_COMPANY_AND_STORE_DATA);

    Set<String> stateEnabledAttributes =
        controllerUtils.getEnableAttributes(salesOrderGeneralModel);

    String[] mandatories =
        controllerUtils.getMandatoryAttributesByState(salesOrderGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getEditCompanyStoreAuthorizedPermissionsConfig();

    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(mandatories);
    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/unlock/companystoredata")
  public @ResponseBody String cancelCompanyStoreEditMode(@PathVariable String salesOrderCode)
      throws Exception {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_COMPANY_AND_STORE_DATA);

    controllerUtils.unlockSection(
        IDObSalesOrderSectionNames.COMPANY_AND_STORE_SECTION, salesOrderCode);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{salesOrderCode}/update/companystoredata")
  public @ResponseBody String updateCompanyAndStoreData(
      @PathVariable String salesOrderCode, @RequestBody String companyStoreJsonData)
      throws Exception {

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_COMPANY_AND_STORE_DATA);

    CommonControllerUtils.applySchemaValidationForString(
        IObSalesOrderCompanyAndStoreDataJsonSchema.SAVE, companyStoreJsonData);

    IObSalesOrderCompanyAndStoreValueObject companyStoreValueObject =
        (IObSalesOrderCompanyAndStoreValueObject)
            CommonControllerUtils.deserializeRequestBody(
                companyStoreJsonData, IObSalesOrderCompanyAndStoreValueObject.class);
    companyStoreValueObject.setSalesOrderCode(salesOrderCode);

    controllerUtils.applyAuthorizationForSaveValueObjectValues();

    validator.validate(companyStoreValueObject);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        salesOrderCode, IDObSalesOrderSectionNames.COMPANY_AND_STORE_SECTION);

    companyStoreDataUpdateCommand.executeCommand(companyStoreValueObject);

    entityLockCommand.unlockSection(
        salesOrderCode, IDObSalesOrderSectionNames.COMPANY_AND_STORE_SECTION);

    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils =
        new IObSalesOrderCompanyStoreControllerUtils(authorizationManager, entityLockCommand);
    controllerUtils.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    controllerUtils.setSalesOrderCompanyStoreEditabilityManager(
        salesOrderCompanyStoreEditabilityManager);
  }
}
