package com.ebs.dda.rest.accounting.vendorinvoice.taxes;

import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxes;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceTaxesGeneralModel;
import com.ebs.dda.rest.accounting.vendorinvoice.DObVendorInvoiceCommandControllerUtils;

public class IObVendorInvoiceTaxesCommandControllerUtils extends DObVendorInvoiceCommandControllerUtils {

  public void checkIfVendorInvoiceTaxExists(IObVendorInvoiceTaxesGeneralModel vendorInvoiceTaxes)
      throws RecordOfInstanceNotExistException {
    if (vendorInvoiceTaxes == null) {
      throw new RecordOfInstanceNotExistException(IObSalesInvoiceTaxes.class.getName());
    }
  }
}
