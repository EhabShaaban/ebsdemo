package com.ebs.dda.rest.accounting.landedcost;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;
import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

import java.util.List;
import java.util.Set;

import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostJsonSchema;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.DObLandedCostAuthorizationManager;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostActionNames;
import com.ebs.dda.accounting.landedcost.apis.IDObLandedCostSectionNames;
import com.ebs.dda.accounting.landedcost.editability.LandedCostFactorItemEditabilityManger;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.accounting.landedcost.validation.DObLandedCostSaveItemValidator;
import com.ebs.dda.commands.accounting.landedcost.IObLandedCostFactorItemDeleteCommand;
import com.ebs.dda.commands.accounting.landedcost.IObLandedCostFactorItemSaveCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.landedcost.*;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.Gson;

@RestController
@RequestMapping(value = "command/landedcost")
public class IObCostFactorItemsCommandController implements InitializingBean {

	@Autowired
	private EntityLockCommand<DObLandedCost> entityLockCommand;
	private IObCostFactorItemsCommandControllerUtils iObLandedCostItemsCommandControllerUtils;
	@Autowired
	private DObLandedCostAuthorizationManager landedCostAuthorizationManager;
	@Autowired
	private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
	@Autowired
	private IObLandedCostFactorItemsGeneralModelRep itemsGeneralModelRep;

	@Autowired
	private LandedCostFactorItemEditabilityManger landedCostItemEditabilityManger;
	@Autowired
	private IObLandedCostFactorItemDeleteCommand deleteCommand;
	@Autowired
	private IObLandedCostFactorItemSaveCommand landedCostFactorItemSaveCommand;
	@Autowired
	private DObLandedCostSaveItemValidator validator;

	@RequestMapping(method = RequestMethod.GET, value = "/{landedCostCode}/lock/items")
	public @ResponseBody String requestAddItem(@PathVariable String landedCostCode)
					throws Exception {

		iObLandedCostItemsCommandControllerUtils.checkIfEntityIsNull(landedCostCode);
		DObLandedCostGeneralModel landedCostGeneralModel = landedCostGeneralModelRep
						.findOneByUserCode(landedCostCode);
		landedCostAuthorizationManager.authorizeActionOnSection(landedCostGeneralModel,
						IDObLandedCostActionNames.UPDATE_COST_FACTOR_ITEMS);

		iObLandedCostItemsCommandControllerUtils.lockLandedCostSectionForEdit(
						landedCostGeneralModel, IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION,
						IDObLandedCostActionNames.UPDATE_COST_FACTOR_ITEMS);

		Set<String> stateEnabledAttributes = iObLandedCostItemsCommandControllerUtils
						.getEnableAttributesForAddItem(landedCostGeneralModel);

		String[] stateMandatories = iObLandedCostItemsCommandControllerUtils
						.getItemsMandatoryAttributesByState(
										landedCostGeneralModel.getCurrentState());

		AuthorizedPermissionsConfig permissionsConfig = iObLandedCostItemsCommandControllerUtils
						.getِAddAuthorizedPermissionsConfig();
		List<String> authorizedReads = iObLandedCostItemsCommandControllerUtils
						.getAuthorizedReads(permissionsConfig);

		SectionEditConfig editConfiguration = new SectionEditConfig();
		editConfiguration.setAuthorizedReads(authorizedReads);
		editConfiguration.setEnabledAttributes(stateEnabledAttributes);
		editConfiguration.setMandatories(stateMandatories);
		return iObLandedCostItemsCommandControllerUtils
						.serializeEditConfigurationsResponse(editConfiguration);
	}

	@RequestMapping(method = RequestMethod.GET, value = "{landedCostCode}/unlock/items")
	public @ResponseBody String unlockItemsSection(@PathVariable String landedCostCode)
					throws Exception {

		DObLandedCostGeneralModel landedCostGeneralModel = landedCostGeneralModelRep
						.findOneByUserCode(landedCostCode);

		iObLandedCostItemsCommandControllerUtils.checkIfEntityIsNull(landedCostGeneralModel);

		landedCostAuthorizationManager.authorizeActionOnSection(landedCostGeneralModel,
						IDObLandedCostActionNames.UPDATE_COST_FACTOR_ITEMS);

		entityLockCommand.unlockSection(landedCostCode,
						IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);

		return getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "{landedCostCode}/costfactoritems/{itemId}")
	public @ResponseBody String deleteItem(@PathVariable String landedCostCode,
					@PathVariable Long itemId) throws Exception {

		IObLandedCostFactorItemDeleteValueObject valueObject = new IObLandedCostFactorItemDeleteValueObject();
		valueObject.setLandedCostCode(landedCostCode);
		valueObject.setItemId(itemId);

		DObLandedCostGeneralModel landedCostGeneralModel = landedCostGeneralModelRep
						.findOneByUserCode(landedCostCode);
		iObLandedCostItemsCommandControllerUtils.checkIfEntityIsNull(landedCostGeneralModel);

		landedCostAuthorizationManager.authorizeActionOnSection(landedCostGeneralModel,
						IDObLandedCostActionNames.UPDATE_COST_FACTOR_ITEMS);

		iObLandedCostItemsCommandControllerUtils.checkIfDeleteDetailActionIsAllowedInCurrentState(
						new DObLandedCostStateMachine(), landedCostGeneralModel,
						IDObLandedCostActionNames.UPDATE_COST_FACTOR_ITEMS);

		IObLandedCostFactorItemsGeneralModel itemsGeneralModel = itemsGeneralModelRep
						.findOneById(itemId);

		iObLandedCostItemsCommandControllerUtils.checkIfDetailIsNull(itemsGeneralModel,
						IObLandedCostFactorItems.class.getName());

		entityLockCommand.lockSection(landedCostCode,
						IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);

		deleteCommand.executeCommand(valueObject);

		entityLockCommand.unlockSection(landedCostCode,
						IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);

		return CommonControllerUtils
						.getSuccessResponse(CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
	}

	@RequestMapping(method = RequestMethod.POST, value = "{landedCostCode}/update/items")
	public @ResponseBody String saveLandedCostItem(@PathVariable String landedCostCode,
					@RequestBody String landedCostItem) throws Exception {
		CommonControllerUtils.applySchemaValidationForString(IDObLandedCostJsonSchema.COST_FACTOR_ITEMS_SCHEMA, landedCostItem);
		applyAuthorizationForSave(landedCostCode);
		IObAddLandedCostFactorItemValueObject itemValueObject = new Gson().fromJson(landedCostItem,
						IObAddLandedCostFactorItemValueObject.class);
		itemValueObject.setLandedCostCode(landedCostCode);
		entityLockCommand.checkIfEntityExists(landedCostCode);

		entityLockCommand.checkIfResourceIsLockedByLoggedInUser(landedCostCode,
						IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);
		try {
			ValidationResult validationResult = validator.validate(itemValueObject);
			if (!validationResult.isValid()) {
				return getFailureResponse(validationResult);
			}
			landedCostFactorItemSaveCommand.executeCommand(itemValueObject);
		} finally {
			entityLockCommand.unlockSection(landedCostCode,
							IDObLandedCostSectionNames.COST_FACTOR_ITEM_SECTION);
		}
		return getSuccessResponse(CommonControllerUtils.ADDING_ITEM_SUCCESSFUL_CODE);
	}

	private void applyAuthorizationForSave(String landedCostCode) throws Exception {
		DObLandedCostGeneralModel landedCost = landedCostGeneralModelRep.findOneByUserCode(landedCostCode);
		if (landedCost != null) {
			landedCostAuthorizationManager.authorizeUpdateItem(landedCost);
			landedCostAuthorizationManager.applyAuthorizatioinOnReads(landedCost);
		}
	}

	@Override
	public void afterPropertiesSet() {
		iObLandedCostItemsCommandControllerUtils = new IObCostFactorItemsCommandControllerUtils(
						landedCostAuthorizationManager.getAuthorizationManager());
		iObLandedCostItemsCommandControllerUtils.setEntityLockCommand(entityLockCommand);
		iObLandedCostItemsCommandControllerUtils
						.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
		iObLandedCostItemsCommandControllerUtils
						.setLandedCOstFactorItemEditabilityManger(landedCostItemEditabilityManger);
	}
}
