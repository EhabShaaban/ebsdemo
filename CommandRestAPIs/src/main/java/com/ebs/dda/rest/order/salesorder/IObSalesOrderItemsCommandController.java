package com.ebs.dda.rest.order.salesorder;

import static com.ebs.dda.rest.CommonControllerUtils.deserializeRequestBody;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderItemsDeleteCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderItemsSaveCommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemDeletionValueObject;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderItemValueObject;
import com.ebs.dda.order.salesorder.IDObSalesOrderActionNames;
import com.ebs.dda.order.salesorder.IDObSalesOrderSectionNames;
import com.ebs.dda.order.salesorder.IObSalesOrderItemsEditabilityManger;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderItemsGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.salesorder.IObSalesOrderItemSaveAddMandatoryValidator;
import com.ebs.dda.validation.salesorder.IObSalesOrderItemSaveAddValidator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/order/salesorder")
public class IObSalesOrderItemsCommandController implements InitializingBean {

  @Autowired private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private EntityLockCommand<DObSalesOrder> entityLockCommand;
  @Autowired private IObSalesOrderItemsEditabilityManger salesOrderItemsEditabilityManger;

  @Autowired
  private IObSalesOrderItemSaveAddMandatoryValidator salesOrderItemSaveAddMandatoryValidator;

  @Autowired private IObSalesOrderItemSaveAddValidator salesOrderItemSaveAddValidator;

  @Autowired private IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep;
  @Autowired private IObSalesOrderItemsSaveCommand iObSalesOrderItemsSaveCommand;
  @Autowired private IObSalesOrderItemsDeleteCommand iObSalesOrderItemsDeleteCommand;

  private IObSalesOrderItemsControllerUtils controllerUtils;

  @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/lock/items")
  public @ResponseBody String requestAddItem(@PathVariable String salesOrderCode) throws Exception {

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_ITEMS);

    controllerUtils.lockSalesOrderSectionForEdit(
        salesOrderCode,
        IDObSalesOrderSectionNames.ITEMS_SECTION,
        IDObSalesOrderActionNames.UPDATE_ITEMS);

    Set<String> stateEnabledAttributes =
        controllerUtils.getEnableAttributesForAddItem(salesOrderGeneralModel);

    String[] stateMandatories =
        controllerUtils.getItemsMandatoryAttributesByState(
            salesOrderGeneralModel.getCurrentState());

    AuthorizedPermissionsConfig permissionsConfig =
        controllerUtils.getSalesOrderِAddItemAuthorizedPermissionsConfig();

    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    editConfiguration.setEnabledAttributes(stateEnabledAttributes);
    editConfiguration.setMandatories(stateMandatories);
    return CommonControllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesOrderCode}/unlock/items")
  public @ResponseBody String cancelAddSalesOrderItem(@PathVariable String salesOrderCode)
      throws Exception {
    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_ITEMS);

    controllerUtils.unlockSection(IDObSalesOrderSectionNames.ITEMS_SECTION, salesOrderCode);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{salesOrderCode}/update/items")
  public @ResponseBody String addItem(
      @PathVariable String salesOrderCode, @RequestBody String itemJsonData) throws Exception {

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_ITEMS);

    CommonControllerUtils.applySchemaValidation(IObSalesOrderItemValueObject.class, itemJsonData);

    IObSalesOrderItemValueObject valueObject =
        (IObSalesOrderItemValueObject)
            deserializeRequestBody(itemJsonData, IObSalesOrderItemValueObject.class);
    valueObject.setSalesOrderCode(salesOrderCode);

    controllerUtils.applyAuthorizationForSaveValueObjectValues();

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        salesOrderCode, IDObSalesOrderSectionNames.ITEMS_SECTION);

    controllerUtils.checkIfUpdateActionIsAllowedInSalesOrderCurrentState(
        salesOrderCode,
        IDObSalesOrderSectionNames.ITEMS_SECTION,
        IDObSalesOrderActionNames.UPDATE_ITEMS);
    try {
      salesOrderItemSaveAddMandatoryValidator.validate(valueObject);

      salesOrderItemSaveAddValidator.validate(valueObject);

      iObSalesOrderItemsSaveCommand.executeCommand(valueObject);
    } finally {
      entityLockCommand.unlockSection(
          valueObject.getSalesOrderCode(), IDObSalesOrderSectionNames.ITEMS_SECTION);
    }
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{salesOrderCode}/items/{itemId}")
  public @ResponseBody String requestDeleteItem(
      @PathVariable String salesOrderCode, @PathVariable String itemId) throws Exception {

    Long itemIdPathVariable = Long.valueOf(itemId);
    IObSalesOrderItemDeletionValueObject valueObject =
        controllerUtils.constructSalesOrderItemDeletionValueObject(
            salesOrderCode, itemIdPathVariable);

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode);

    authorizationManager.authorizeActionOnObject(
        salesOrderGeneralModel, IDObSalesOrderActionNames.UPDATE_ITEMS);

    entityLockCommand.lockSection(salesOrderCode, IDObSalesOrderSectionNames.ITEMS_SECTION);
    controllerUtils.checkIfRecordDeleteActionIsAllowedInSalesOrderCurrentState(
        salesOrderCode,
        IDObSalesOrderSectionNames.ITEMS_SECTION,
        IDObSalesOrderActionNames.UPDATE_ITEMS);

    IObSalesOrderItemGeneralModel itemGeneralModel =
        salesOrderItemsGeneralModelRep.findOneById(valueObject.getSalesOrderItemId());

    controllerUtils.checkIfSalesOrderItemExists(salesOrderCode, itemGeneralModel);

    iObSalesOrderItemsDeleteCommand.executeCommand(valueObject);

    entityLockCommand.unlockSection(salesOrderCode, IDObSalesOrderSectionNames.ITEMS_SECTION);

    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    controllerUtils =
        new IObSalesOrderItemsControllerUtils(authorizationManager, entityLockCommand);
    controllerUtils.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    controllerUtils.setSalesOrderItemEditabilityManger(salesOrderItemsEditabilityManger);
  }
}
