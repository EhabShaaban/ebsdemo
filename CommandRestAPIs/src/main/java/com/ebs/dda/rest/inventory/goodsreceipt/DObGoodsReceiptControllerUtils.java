package com.ebs.dda.rest.inventory.goodsreceipt;

import com.ebs.dac.foundation.exceptions.operational.accessability.RecordOfInstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.inventory.goodsreceipt.validation.apis.IGoodsReceiptItemsDifferenceReasonMandatoryAttributes;
import com.ebs.dda.inventory.goodsreceipt.validation.apis.IGoodsReceiptReceivedItemMandatoryAttributes;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.Optional;

public class DObGoodsReceiptControllerUtils extends CommonControllerUtils {

  public static final String GR_PO = "GR_PO";
  private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;
  private IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptRecievedItemsGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptRecievedItemsRep;
  private MandatoryValidatorUtils mandatoryValidatorUtils;
  private IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep;
  private MObItemGeneralModelRep itemGeneralModelRep;
  private IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderItemQuantitiesRep;
  private IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep;
  private IObGoodsReceiptItemBatchesRep batchesRep;
  private EntityLockCommand<DObGoodsReceipt> entityLockCommand;

  private IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptReceivedItemsGeneralModelRep;

  public DObGoodsReceiptControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void setGoodsReceiptRecievedItemsRep(
      IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptRecievedItemsRep) {
    this.goodsReceiptRecievedItemsRep = goodsReceiptRecievedItemsRep;
  }

  public void setGoodsReceiptRecievedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptRecievedItemsGeneralModelRep) {
    this.goodsReceiptRecievedItemsGeneralModelRep = goodsReceiptRecievedItemsGeneralModelRep;
  }

  public void setGoodsReceiptGeneralModelRep(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    this.goodsReceiptGeneralModelRep = goodsReceiptGeneralModelRep;
  }

  public void setReceivedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep) {
    this.receivedItemsGeneralModelRep = receivedItemsGeneralModelRep;
  }

  public void setItemGeneralModelRep(MObItemGeneralModelRep itemGeneralModelRep) {
    this.itemGeneralModelRep = itemGeneralModelRep;
  }

  public void setGoodsReceiptPurchaseOrderItemQuantitiesRep(
      IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderItemQuantitiesRep) {
    this.goodsReceiptPurchaseOrderItemQuantitiesRep = goodsReceiptPurchaseOrderItemQuantitiesRep;
  }

  public void setGoodsReceiptSalesReturnItemQuantitiesRep(
      IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep) {
    this.goodsReceiptSalesReturnItemQuantitiesRep = goodsReceiptSalesReturnItemQuantitiesRep;
  }

  public void setBatchesRep(IObGoodsReceiptItemBatchesRep batchesRep) {
    this.batchesRep = batchesRep;
  }

  protected void checkIfActionIsAllowedInCurrentState(String goodsReceiptCode, String actionName)
      throws Exception {
    DObGoodsReceiptGeneralModel goodsReceipt =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    DObGoodsReceiptStateMachine goodsReceiptStateMachine = new DObGoodsReceiptStateMachine();
    checkIfActionIsAllowedInCurrentState(goodsReceiptStateMachine, goodsReceipt, actionName);
  }

  public void setEntityLockCommand(EntityLockCommand<DObGoodsReceipt> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void setGoodsReceiptReceivedItemsGeneralModelRep(
      IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptReceivedItemsGeneralModelRep) {
    this.goodsReceiptReceivedItemsGeneralModelRep = goodsReceiptReceivedItemsGeneralModelRep;
  }

  public void lockGoodsReceiptSectionForEdit(
      String goodsReceiptCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(goodsReceiptCode, sectionName);
    try {
      checkIfActionIsAllowedInCurrentState(goodsReceiptCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      entityLockCommand.unlockSection(goodsReceiptCode, sectionName);
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
  }

  public void checkIfItemExists(String goodsReceiptCode, String itemCode) throws Exception {
    IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel =
        receivedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(goodsReceiptCode, itemCode);
    checkIfGoodsReceiptItemExists(receivedItemGeneralModel);
  }

  private void checkIfGoodsReceiptItemExists(
      IObGoodsReceiptReceivedItemsGeneralModel receivedItemGeneralModel) throws Exception {
    if (receivedItemGeneralModel == null) {
      throw new RecordOfInstanceNotExistException(
          IObGoodsReceiptReceivedItemsGeneralModel.class.getName());
    }
  }

  public void checkIfItemDetailExists(String inventoryDocumentType, String itemCode, Long detailId)
      throws Exception {
    MObItemGeneralModel item = itemGeneralModelRep.findByUserCode(itemCode);
    boolean isBatched = item.getIsBatchManaged();

    if (isBatched) {
      checkIfBatchExists(detailId);
    } else {
      checkIfQuantityExists(inventoryDocumentType, detailId);
    }
  }

  private void checkIfQuantityExists(String inventoryDocumentType, Long detailId) throws Exception {
    if (inventoryDocumentType.equals(GR_PO)) {
      Optional<IObGoodsReceiptPurchaseOrderItemQuantities> quantity =
          goodsReceiptPurchaseOrderItemQuantitiesRep.findById(detailId);

      if (quantity.equals(Optional.empty())) {
        throw new RecordOfInstanceNotExistException(
            IObGoodsReceiptPurchaseOrderItemQuantities.class.getName());
      }
    } else {
      Optional<IObGoodsReceiptSalesReturnItemQuantities> quantity =
          goodsReceiptSalesReturnItemQuantitiesRep.findById(detailId);
      if (quantity.equals(Optional.empty())) {
        throw new RecordOfInstanceNotExistException(
            IObGoodsReceiptSalesReturnItemQuantities.class.getName());
      }
    }
  }

  private void checkIfBatchExists(Long detailId) throws Exception {
    Optional<IObGoodsReceiptItemBatches> batch = batchesRep.findById(detailId);

    if (batch.equals(Optional.empty())) {
      throw new RecordOfInstanceNotExistException(
          IObGoodsReceiptReceivedItemsGeneralModel.class.getName());
    }
  }

  public String[] getItemMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IGoodsReceiptReceivedItemMandatoryAttributes.ITEM_GENERAL_MODEL_MANDATORIES,
            currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public String[] getItemDetailMandatoryAttributesByState(
      String currentState, Boolean isBatchManaged) {

    String[] mandatories;
    if (isBatchManaged) {
      mandatories =
          mandatoryValidatorUtils.getMandatoryAttributesByState(
              IGoodsReceiptReceivedItemMandatoryAttributes.ITEM_BACTHES_GENERAL_MODEL_MANDATORIES,
              currentState);
    } else {
      mandatories =
          mandatoryValidatorUtils.getMandatoryAttributesByState(
              IGoodsReceiptReceivedItemMandatoryAttributes
                  .ITEM_QUANTITIES_GENERAL_MODEL_MANDATORIES,
              currentState);
    }
    return mandatories != null ? mandatories : new String[] {};
  }

  public String[] getItemDifferenceReasonMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IGoodsReceiptItemsDifferenceReasonMandatoryAttributes.MANDATORIES_FOR_GENERAL_MODEL,
            currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public IObGoodsReceiptPurchaseOrderReceivedItemsData readGRItemByGRCodeAndItemCode(
      String goodsReceiptCode, String itemCode) {
    IObGoodsReceiptReceivedItemsGeneralModel goodsReceiptRecievedItemsGeneralModel =
        goodsReceiptRecievedItemsGeneralModelRep.findByGoodsReceiptCodeAndItemCode(
            goodsReceiptCode, itemCode);
    return (goodsReceiptRecievedItemsGeneralModel != null)
        ? goodsReceiptRecievedItemsRep.findOneById(goodsReceiptRecievedItemsGeneralModel.getGRItemId())
        : null;
  }

  public void checkGRItemsExist(
      IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptRecievedItemsData)
      throws RecordOfInstanceNotExistException {
    if (goodsReceiptRecievedItemsData == null) {
      throw new RecordOfInstanceNotExistException(
          IObGoodsReceiptPurchaseOrderReceivedItemsData.class.getName());
    }
  }
}
