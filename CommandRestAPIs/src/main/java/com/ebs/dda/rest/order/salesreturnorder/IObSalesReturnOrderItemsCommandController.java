package com.ebs.dda.rest.order.salesreturnorder;

import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObSalesReturnOrderAuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesreturnorder.IObSalesReturnOrderDeleteItemCommand;
import com.ebs.dda.commands.order.salesreturnorder.IObSalesReturnOrderSaveItemCommand;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderDeleteItemValueObject;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemValueObject;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderActionNames;
import com.ebs.dda.order.salesreturnorder.IDObSalesReturnOrderSectionNames;
import com.ebs.dda.order.salesreturnorder.IObSalesReturnOrderItemsEditabilityManger;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.salesreturnorder.IObSalesReturnOrderItemSaveEditValidator;
import com.google.gson.Gson;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "command/order/salesreturnorder")
public class IObSalesReturnOrderItemsCommandController implements InitializingBean {

  @Autowired private DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep;
  @Autowired private IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemGeneralModelRep;
  @Autowired private EntityLockCommand<DObSalesReturnOrder> entityLockCommand;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private IObSalesReturnOrderDeleteItemCommand salesReturnOrderDeleteItemCommand;
  @Autowired private IObSalesReturnOrderSaveItemCommand salesReturnOrderItemSaveCommand;
  @Autowired private DObSalesReturnOrderAuthorizationManager salesReturnOrderAuthorizationManager;
  @Autowired private IObSalesReturnOrderItemSaveEditValidator itemSaveEditValidator;

  @Autowired
  private IObSalesReturnOrderItemsEditabilityManger salesReturnOrderItemsEditabilityManger;

  private CommonControllerUtils commonControllerUtils;
  private IObSalesReturnOrderItemsControllerUtils utils;

  @RequestMapping(method = RequestMethod.GET, value = "{salesReturnOrderCode}/lock/items/{itemId}")
  public @ResponseBody String requestEditItem(
      @PathVariable String salesReturnOrderCode, @PathVariable String itemId) throws Exception {

    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    commonControllerUtils.checkAuthorizeForActionOnBusinessObject(
        salesReturnOrderGeneralModel, IDObSalesReturnOrderActionNames.UPDATE_ITEMS);

    utils.checkIfUpdateActionIsAllowedInSalesReturnOrderCurrentState(
        salesReturnOrderCode, IDObSalesReturnOrderActionNames.UPDATE_ITEMS);

    IObSalesReturnOrderItemGeneralModel salesReturnOrderItemGeneralModel =
        salesReturnOrderItemGeneralModelRep.findOneBySalesReturnOrderCodeAndId(
            salesReturnOrderCode, Long.parseLong(itemId));

    utils.checkIfItemExistsInSalesReturnOrder(salesReturnOrderItemGeneralModel);

    entityLockCommand.lockSection(
        salesReturnOrderCode, IDObSalesReturnOrderSectionNames.ITEMS_SECTION);

    List<String> authorizedReads = utils.getEditItemAuthorizedReads();
    Set<String> stateEnabledAttributes =
        utils.getEditItemsEnabledAttributes(salesReturnOrderGeneralModel);

    String[] stateMandatories =
        utils.getEditItemsMandatoryAttributesByState(
            salesReturnOrderGeneralModel.getCurrentState());
    SectionEditConfig sectionEditConfig = new SectionEditConfig();
    sectionEditConfig.setAuthorizedReads(authorizedReads);
    sectionEditConfig.setMandatories(stateMandatories);
    sectionEditConfig.setEnabledAttributes(stateEnabledAttributes);

    return commonControllerUtils.serializeEditConfigurationsResponse(sectionEditConfig);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/{salesReturnOrderCode}/unlock/items/{itemId}")
  public @ResponseBody String cancelEditItem(
      @PathVariable String salesReturnOrderCode, @PathVariable String itemId) throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    commonControllerUtils.checkAuthorizeForActionOnBusinessObject(
        salesReturnOrderGeneralModel, IDObSalesReturnOrderActionNames.UPDATE_ITEMS);

    IObSalesReturnOrderItemGeneralModel salesReturnOrderItemGeneralModel =
        salesReturnOrderItemGeneralModelRep.findOneBySalesReturnOrderCodeAndId(
            salesReturnOrderCode, Long.parseLong(itemId));

    utils.checkIfItemExistsInSalesReturnOrder(salesReturnOrderItemGeneralModel);

    entityLockCommand.unlockSection(
        salesReturnOrderCode, IDObSalesReturnOrderSectionNames.ITEMS_SECTION);

    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "{salesReturnOrderCode}/items/{itemId}")
  public @ResponseBody String deleteItem(
      @PathVariable String salesReturnOrderCode, @PathVariable String itemId) throws Exception {

    IObSalesReturnOrderDeleteItemValueObject valueObject =
        new IObSalesReturnOrderDeleteItemValueObject();
    valueObject.setSalesReturnOrderCode(salesReturnOrderCode);
    valueObject.setItemId(Long.valueOf(itemId));

    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    commonControllerUtils.checkAuthorizeForActionOnBusinessObject(
        salesReturn, IDObSalesReturnOrderActionNames.UPDATE_ITEMS);

    utils.checkIfRecordDeleteActionIsAllowedInSalesOrderCurrentState(
        salesReturnOrderCode, IDObSalesReturnOrderActionNames.UPDATE_ITEMS);

    IObSalesReturnOrderItemGeneralModel salesReturnOrderItemGeneralModel =
        salesReturnOrderItemGeneralModelRep.findOneBySalesReturnOrderCodeAndId(
            salesReturnOrderCode, Long.parseLong(itemId));

    utils.checkIfItemExistsInSalesReturnOrder(salesReturnOrderItemGeneralModel);

    entityLockCommand.lockSection(
        salesReturnOrderCode, IDObSalesReturnOrderSectionNames.ITEMS_SECTION);

    salesReturnOrderDeleteItemCommand.executeCommand(valueObject);

    entityLockCommand.unlockSection(
        salesReturnOrderCode, IDObSalesReturnOrderSectionNames.ITEMS_SECTION);

    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.GET, value = "{salesReturnOrderCode}/lock/items")
  public @ResponseBody String requestTOLockItemSection(@PathVariable String salesReturnOrderCode)
      throws Exception {

    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    entityLockCommand.lockSection(
        salesReturnOrderCode, IDObSalesReturnOrderSectionNames.ITEMS_SECTION);

    List<String> authorizedReads = utils.getEditItemAuthorizedReads();
    Set<String> stateEnabledAttributes =
        utils.getEditItemsEnabledAttributes(salesReturnOrderGeneralModel);

    String[] stateMandatories =
        utils.getEditItemsMandatoryAttributesByState(
            salesReturnOrderGeneralModel.getCurrentState());
    SectionEditConfig sectionEditConfig = new SectionEditConfig();
    sectionEditConfig.setAuthorizedReads(authorizedReads);
    sectionEditConfig.setMandatories(stateMandatories);
    sectionEditConfig.setEnabledAttributes(stateEnabledAttributes);

    return commonControllerUtils.serializeEditConfigurationsResponse(sectionEditConfig);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "{salesReturnCode}/update/item/{itemId}")
  public @ResponseBody String saveEditSalesReturnOrderItem(
      @PathVariable String salesReturnCode,
      @PathVariable String itemId,
      @RequestBody String sroItem)
      throws Exception {
    CommonControllerUtils.applySchemaValidation(IObSalesReturnOrderItemValueObject.class, sroItem);

    IObSalesReturnOrderItemValueObject itemValueObject =
        new Gson().fromJson(sroItem, IObSalesReturnOrderItemValueObject.class);
    itemValueObject.setSalesReturnOrderCode(salesReturnCode);
    itemValueObject.setItemId(Long.parseLong(itemId));

    DObSalesReturnOrderGeneralModel salesReturn =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnCode);

    commonControllerUtils.checkAuthorizeForActionOnBusinessObject(
        salesReturn, IDObSalesReturnOrderActionNames.UPDATE_ITEMS);

    utils.applyAuthorizationForSave(salesReturnCode);

    IObSalesReturnOrderItemGeneralModel salesReturnOrderItemGeneralModel =
        salesReturnOrderItemGeneralModelRep.findOneBySalesReturnOrderCodeAndId(
            salesReturnCode, Long.parseLong(itemId));

    utils.checkIfItemExistsInSalesReturnOrder(salesReturnOrderItemGeneralModel);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        salesReturnCode, IDObSalesReturnOrderSectionNames.ITEMS_SECTION);

    ValidationResult validationResult = itemSaveEditValidator.validate(itemValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    }

    salesReturnOrderItemSaveCommand.executeCommand(itemValueObject);
    entityLockCommand.unlockSection(
        salesReturnCode, IDObSalesReturnOrderSectionNames.ITEMS_SECTION);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
    utils = new IObSalesReturnOrderItemsControllerUtils(authorizationManager, entityLockCommand);
    utils.setSalesReturnOrderGeneralModelRep(salesReturnOrderGeneralModelRep);
    utils.setSalesReturnOrderItemEditabilityManger(salesReturnOrderItemsEditabilityManger);
    utils.setSalesReturnOrderAuthorizationManager(salesReturnOrderAuthorizationManager);
  }
}
