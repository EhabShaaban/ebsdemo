package com.ebs.dda.rest.accounting.accountingnotes;

import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;
import com.ebs.dda.rest.CommonControllerUtils;

public class DObAccountingNoteControllerUtils extends CommonControllerUtils {

  private EntityLockCommand<DObAccountingNote> entityLockCommand;

  public DObAccountingNoteControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
    this.entityLockCommand = entityLockCommand;
  }

  public void setEntityLockCommand(EntityLockCommand<DObAccountingNote> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }
}
