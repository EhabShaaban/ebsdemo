package com.ebs.dda.rest.accounting.paymentrequest.activation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "command/accounting/paymentrequest")
public class DObPaymentActivationController {

    @Autowired
    private PaymentActivateBeanFactory activateBeanFactory;

    @PutMapping("/{paymentCode}/activate")
    public  @ResponseBody
    String activatePayment(@PathVariable String paymentCode,
                              @RequestBody String valueObject,
                              @RequestParam(name = "type") String paymentType,
                              @RequestParam(name = "method") String method,
                              @RequestParam(name = "dueDocument", required = false) String dueDocument) throws Exception {

        ActivatePayment activatePayment = activateBeanFactory.getBean(paymentType, method, dueDocument);

        return activatePayment.activate(PaymentActivateContextBuilder.newContext()
                .objectCode(paymentCode).valueObject(valueObject));
    }

}
