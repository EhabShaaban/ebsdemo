package com.ebs.dda.rest.accounting.notesreceivables;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableDetailsEditabilityManager;
import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableDetailsValueObjectMandatoryAttributes;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.masterdata.lookups.ILObDepot;
import com.ebs.dda.validation.utils.MandatoryValidatorUtils;

import java.util.Set;

public class IObNoteReceivablesDetailsControllerUtils extends DObNoteReceivablesControllerUtils {

  private EntityLockCommand<DObMonetaryNotes> entityLockCommand;
  private IObNotesReceivableDetailsEditabilityManager notesReceivableDetailsEditabilityManager;
  private MandatoryValidatorUtils mandatoryValidatorUtils;

  public IObNoteReceivablesDetailsControllerUtils(
      AuthorizationManager authorizationManager,
      EntityLockCommand<DObMonetaryNotes> entityLockCommand) {
    super(authorizationManager, entityLockCommand);
    this.entityLockCommand = entityLockCommand;
    mandatoryValidatorUtils = new MandatoryValidatorUtils();
  }

  public void lockNotesReceivableSectionForEdit(
      String notesReceivableCode, String sectionName, String actionName) throws Exception {
    entityLockCommand.lockSection(notesReceivableCode, sectionName);
    checkIfUpdateActionIsAllowedInNotesReceivableCurrentState(
        notesReceivableCode, sectionName, actionName);
  }

  public void unlockSection(String sectionName, String salesOrderCode) throws Exception {
    entityLockCommand.unlockSection(salesOrderCode, sectionName);
  }

  public Set<String> getEnableAttributes(
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel) {
    return notesReceivableDetailsEditabilityManager
        .getEnabledAttributesConfig()
        .get(notesReceivablesGeneralModel.getCurrentState());
  }

  public String[] getMandatoryAttributesByState(String currentState) {
    String[] mandatories;
    mandatories =
        mandatoryValidatorUtils.getMandatoryAttributesByState(
            IObNotesReceivableDetailsValueObjectMandatoryAttributes.MANDATORIES, currentState);
    return mandatories != null ? mandatories : new String[] {};
  }

  public AuthorizedPermissionsConfig getEditNotesDetailsAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();

    permissionsConfig.addPermissionConfig(
        ILObDepot.SYS_NAME, IActionsNames.READ_ALL, "ReadAllDepots");
    return permissionsConfig;
  }

  public void setNotesReceivableDetailsEditabilityManager(
      IObNotesReceivableDetailsEditabilityManager notesReceivableDetailsEditabilityManager) {
    this.notesReceivableDetailsEditabilityManager = notesReceivableDetailsEditabilityManager;
  }
}
