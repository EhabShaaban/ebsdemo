package com.ebs.dda.rest.accounting.vendorinvoice;

import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.util.Map;

public class DObVendorInvoiceCommandControllerUtils extends CommonControllerUtils {

  private DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep;
  private EntityLockCommand<DObVendorInvoice> entityLockCommand;

  public void checkIfActionIsAllowedInCurrentState(DObVendorInvoice invoice, String actionName)
      throws Exception {
    DObVendorInvoiceStateMachine invoiceStateMachine = new DObVendorInvoiceStateMachine();
    invoiceStateMachine.initObjectState(invoice);
    invoiceStateMachine.isAllowedAction(actionName);
  }

  public void checkIfActionIsAllowedInCurrentState(DObVendorInvoiceGeneralModel invoice, String actionName)
      throws Exception {
    DObVendorInvoiceStateMachine invoiceStateMachine = new DObVendorInvoiceStateMachine();
    invoiceStateMachine.initObjectState(invoice);
    invoiceStateMachine.isAllowedAction(actionName);
  }

  public void checkIfDeleteIsAllowedInCurrentState(DObVendorInvoiceGeneralModel invoice, String actionName)
      throws Exception {
    DObVendorInvoiceStateMachine invoiceStateMachine = new DObVendorInvoiceStateMachine();
    invoiceStateMachine.initObjectState(invoice);
    try {
    invoiceStateMachine.isAllowedAction(actionName);
    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException(e);
    }
  }


  //TODO: Remove it and use CommonControllerUtils.getFailureResponseWithMissingFields instead
  public String getFailureResponseWithValidationErrorsAndCode(
          Map<String, Object> validationErrors, String code) {
    JsonParser parser = new JsonParser();
    JsonObject response = new JsonObject();
    response.add("missingFields", parser.parse(new Gson().toJson(validationErrors)));
    response.addProperty(RESPONSE_STATUS, FAIL_RESPONSE_KEY);
    response.addProperty(CODE_RESPONSE_KEY, code);
    return response.toString();
  }

  public void setVendorInvoiceGeneralModelRep(
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {
    this.vendorInvoiceGeneralModelRep = vendorInvoiceGeneralModelRep;
  }

  public void setEntityLockCommand(
      EntityLockCommand<DObVendorInvoice> entityLockCommand) {
    this.entityLockCommand = entityLockCommand;
  }

  public void checkIfEntityIsNull(DObVendorInvoiceGeneralModel dObVendorInvoiceGeneralModel)
      throws InstanceNotExistException {
    if (dObVendorInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }

  public void checkIfEntityIsNull(String vendorInvoiceCode) throws InstanceNotExistException {
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        vendorInvoiceGeneralModelRep.findOneByUserCode(vendorInvoiceCode);
    if (vendorInvoiceGeneralModel == null) {
      throw new InstanceNotExistException();
    }
  }
}
