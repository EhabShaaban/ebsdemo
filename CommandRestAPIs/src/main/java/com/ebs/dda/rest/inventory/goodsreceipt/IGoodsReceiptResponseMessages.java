package com.ebs.dda.rest.inventory.goodsreceipt;

public interface IGoodsReceiptResponseMessages {

  String SAVING_SUCCESSFUL_CODE = "Gen-msg-04";
  String CREATION_SUCCESSFUL_CODE = "Gen-msg-11";
  String DELETE_GR_SUCCESS_MSG = "Gen-msg-12";
  String DELETE_ITEM_SUCCESS_MSG = "Gen-msg-12";
  String SUCCESSFUL_ACTIVATE = "GR-msg-12";
}
