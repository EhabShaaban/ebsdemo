package com.ebs.dda.rest.inventory.goodsreceipt;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.RecordDeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.goodsreceipt.*;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptActionNames;
import com.ebs.dda.inventory.goodsreceipt.apis.IGoodsReceiptSectionNames;
import com.ebs.dda.inventory.goodsreceipt.editability.DObGoodsReceiptAddItemEditabilityManager;
import com.ebs.dda.inventory.goodsreceipt.editability.DObGoodsReceiptItemBatchEditabilityManager;
import com.ebs.dda.inventory.goodsreceipt.editability.DObGoodsReceiptItemQuantityEditabilityManager;
import com.ebs.dda.inventory.goodsreceipt.editability.IObGoodsReceiptItemDifferenceReasonEditabilityManager;
import com.ebs.dda.inventory.goodsreceipt.validation.DObGoodsReceiptCreationValidator;
import com.ebs.dda.inventory.goodsreceipt.validation.DObGoodsReceiptDetailValidator;
import com.ebs.dda.inventory.goodsreceipt.validation.DObGoodsReceiptDifferenceReasonValidator;
import com.ebs.dda.inventory.goodsreceipt.validation.DObGoodsReceiptItemsSectionValidator;
import com.ebs.dda.jpa.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.item.IMObItem;
import com.ebs.dda.jpa.masterdata.item.MObItemGeneralModel;
import com.ebs.dda.jpa.masterdata.measure.ICObMeasure;
import com.ebs.dda.jpa.masterdata.storehouse.ICObStorehouse;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.IDObSalesReturnOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ebs.dda.rest.CommonControllerUtils.*;

@RestController
@RequestMapping(value = "command/goodsreceipt")
public class DObGoodsReceiptCommandController implements InitializingBean {

  @Autowired private DObGoodsReceiptCreationValidator creationValidator;
  @Autowired private DObGoodsReceiptCreateCommand goodsReceiptCreateCommand;
  @Autowired private DObGoodsReceiptDeleteCommand goodsReceiptDeleteCommand;
  @Autowired private IObGoodsReceiptOrdinaryItemAddCommand goodsReceiptItemAddCommand;
  @Autowired private IObGoodsReceiptBatchedItemAddCommand goodsReceiptBatchedItemAddCommand;
  @Autowired private DObGoodsReceiptItemBatchEditabilityManager batchEditabilityManager;
  @Autowired private DObGoodsReceiptItemQuantityEditabilityManager quantityEditabilityManager;
  @Autowired
  private IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptItemDataGeneralModelRep;

  @Autowired private IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptRecievedItemsRep;

  @Autowired
  @Qualifier("goodsReceiptLockCommand")
  private EntityLockCommand<DObGoodsReceipt> entityLockCommand;

  @Autowired private DObPurchaseOrderGeneralModelRep purchasingOrderGeneralModelRep;
  @Autowired private DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep;
  @Autowired private DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep;

  @Autowired
  private IObGoodsReceiptReceivedItemsGeneralModelRep goodsReceiptReceivedItemsGeneralModelRep;

  @Autowired private DObGoodsReceiptPurchaseOrderRep goodsReceiptRep;
  @Autowired private IObGoodsReceiptDeleteItemCommand iObGoodsReceiptDeleteItemCommand;

  @Autowired
  private IObGoodsReceiptItemDifferenceReasonSaveCommand
      goodsReceiptItemDifferenceReasonSaveCommand;

  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObGoodsReceiptDeleteDetailCommand deleteDetailCommand;
  @Autowired private IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep;
  @Autowired private MObItemGeneralModelRep itemGeneralModelRep;
  @Autowired private IObGoodsReceiptItemBatchesRep batchesRep;

  @Autowired
  private IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderItemQuantitiesRep;

  @Autowired
  private IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep;

  @Autowired private DObGoodsReceiptItemsSectionValidator goodsReceiptItemsSectionValidator;
  @Autowired private DObGoodsReceiptSaveDetailCommand saveDetailCommand;
  @Autowired private DObGoodsReceiptDetailValidator detailValidator;

  @Autowired
  private IObGoodsReceiptItemDifferenceReasonEditabilityManager
      grItemDifferenceReasonEditabilityManager;

  @Autowired private DObGoodsReceiptDifferenceReasonValidator grItemDifferenceReasonValidator;
  @Autowired private DObGoodsReceiptAddItemEditabilityManager addItemEditabilityManager;
  private DObGoodsReceiptControllerUtils controllerUtils;

  @Override
  public void afterPropertiesSet() {
    controllerUtils = new DObGoodsReceiptControllerUtils(authorizationManager);
    controllerUtils.setEntityLockCommand(entityLockCommand);
    controllerUtils.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    controllerUtils.setGoodsReceiptReceivedItemsGeneralModelRep(
        goodsReceiptReceivedItemsGeneralModelRep);
    initItemDetailRepositories();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/createconfiguration")
  public @ResponseBody String getCreateGoodsReceiptConfiguration() {
    authorizationManager.authorizeAction(IDObGoodsReceipt.SYS_NAME, IActionsNames.CREATE);

    AuthorizedPermissionsConfig createPermissionsConfig = getCreatePermissionsConfig();
    List<String> authorizedReads = controllerUtils.getAuthorizedReads(createPermissionsConfig);
    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(value = "/create", method = RequestMethod.POST)
  public @ResponseBody String createGoodsReceipt(@RequestBody String creationData)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DObGoodsReceiptCreationValueObject.class, creationData);
    DObGoodsReceiptCreationValueObject valueObject =
        new Gson().fromJson(creationData, DObGoodsReceiptCreationValueObject.class);
    applyAuthorizationOnCreateAction(valueObject);

    ValidationResult validationResult = creationValidator.validate(valueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    }
    Observable<String> codeObservable = goodsReceiptCreateCommand.executeCommand(valueObject);
    final StringBuilder userCode = new StringBuilder();
    codeObservable.subscribe(
        (code) -> {
          userCode.append(code);
        });
    return getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{goodsReceiptCode}")
  public @ResponseBody String deleteGoodsReceipt(@PathVariable String goodsReceiptCode)
      throws Exception {
    applyAuthorization(goodsReceiptCode, IActionsNames.DELETE);
    goodsReceiptDeleteCommand.executeCommand(goodsReceiptCode);
    return getSuccessResponse(IGoodsReceiptResponseMessages.DELETE_GR_SUCCESS_MSG);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{goodsReceiptCode}/delete/{itemCode}")
  public @ResponseBody String deleteGoodsReceiptItem(
      @PathVariable String goodsReceiptCode, @PathVariable String itemCode) throws Exception {
    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
    iObGoodsReceiptDeleteItemCommand.executeCommand(itemCode, goodsReceiptCode);
    return getSuccessResponse(IGoodsReceiptResponseMessages.DELETE_ITEM_SUCCESS_MSG);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/saveitem/{goodsReceiptCode}")
  public @ResponseBody String saveItemSection(
      @PathVariable String goodsReceiptCode, @RequestBody String itemSectionData) throws Exception {
    try {
      CommonControllerUtils.applySchemaValidation(
          IObGoodsReceiptReceivedItemsDataValueObject.class, itemSectionData);

      applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
      applyAuthorizationOnItemSectionFields();

      IObGoodsReceiptReceivedItemsDataValueObject goodsReceiptOrdinaryReceivedItemsDataValueObject =
          (IObGoodsReceiptReceivedItemsDataValueObject)
              deserializeRequestBody(
                  itemSectionData, IObGoodsReceiptReceivedItemsDataValueObject.class);

      goodsReceiptOrdinaryReceivedItemsDataValueObject.setGoodsReceiptCode(goodsReceiptCode);

      ValidationResult validationResult =
          goodsReceiptItemsSectionValidator.validate(
              goodsReceiptOrdinaryReceivedItemsDataValueObject);
      if (!validationResult.isValid()) {
        return CommonControllerUtils.getFailureResponse(validationResult);
      } else {
        MObItemGeneralModel itemGeneralModelRepByCode =
            itemGeneralModelRep.findByUserCode(
                goodsReceiptOrdinaryReceivedItemsDataValueObject.getItemCode());
        if (itemGeneralModelRepByCode.getIsBatchManaged()) {
          goodsReceiptBatchedItemAddCommand.executeCommand(
              goodsReceiptOrdinaryReceivedItemsDataValueObject);
          entityLockCommand.unlockSection(
              goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
          return getSuccessResponse(IGoodsReceiptResponseMessages.SAVING_SUCCESSFUL_CODE);

        } else {
          goodsReceiptItemAddCommand.executeCommand(
              goodsReceiptOrdinaryReceivedItemsDataValueObject);
          entityLockCommand.unlockSection(
              goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
          return getSuccessResponse(IGoodsReceiptResponseMessages.SAVING_SUCCESSFUL_CODE);
        }
      }

    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    } catch (ArgumentViolationSecurityException argumentViolationSecurityException) {
      entityLockCommand.unlockSection(goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
      throw argumentViolationSecurityException;
    }
  }

  @RequestMapping(method = RequestMethod.GET, value = "/cancelrequestadditem/{goodsReceiptCode}")
  public @ResponseBody String cancelRequestAddItem(@PathVariable String goodsReceiptCode)
      throws Exception {
    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
    return unlockSection(IGoodsReceiptSectionNames.ITEMS_SECTION, goodsReceiptCode);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/requestadditem/{goodsReceiptCode}")
  public @ResponseBody String requestToAddItem(@PathVariable String goodsReceiptCode)
      throws Exception {
    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
    DObGoodsReceipt goodsReceipt = goodsReceiptRep.findOneByUserCode(goodsReceiptCode);
    controllerUtils.lockGoodsReceiptSectionForEdit(
        goodsReceiptCode,
        IGoodsReceiptSectionNames.ITEMS_SECTION,
        IGoodsReceiptActionNames.UPDATE_ITEM);
    AuthorizedPermissionsConfig permissionsConfig = getAddItemAuthorizedPermissionsConfig();
    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);
    SectionEditConfig editConfiguration = new SectionEditConfig();
    editConfiguration.setAuthorizedReads(authorizedReads);
    Set<String> enabledAttributes = getAddItemEnabledAttributes(goodsReceipt.getCurrentState());
    editConfiguration.setEnabledAttributes(enabledAttributes);
    String[] mandatories =
        controllerUtils.getItemMandatoryAttributesByState(goodsReceipt.getCurrentState());
    editConfiguration.setMandatories(mandatories);
    return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "unlock/add/itemdetail/{goodsReceiptCode}/{itemCode}")
  public @ResponseBody String cancelRequestAddItemDetail(
      @PathVariable String goodsReceiptCode, @PathVariable String itemCode) throws Exception {

    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
    DObGoodsReceiptGeneralModel goodsReceipt =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);

    checkIfEntityIsNull(goodsReceipt);
    controllerUtils.checkIfItemExists(goodsReceiptCode, itemCode);

    return unlockSection(IGoodsReceiptSectionNames.ITEMS_SECTION, goodsReceiptCode);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "lock/add/itemdetail/{goodsReceiptCode}/{itemCode}")
  public @ResponseBody String requestToAddItemDetail(
      @PathVariable String goodsReceiptCode, @PathVariable String itemCode) throws Exception {

    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
    DObGoodsReceiptGeneralModel goodsReceipt =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);

    checkIfEntityIsNull(goodsReceipt);
    controllerUtils.checkIfItemExists(goodsReceiptCode, itemCode);

    controllerUtils.lockGoodsReceiptSectionForEdit(
        goodsReceiptCode,
        IGoodsReceiptSectionNames.ITEMS_SECTION,
        IGoodsReceiptActionNames.UPDATE_ITEM);

    SectionEditConfig editConfiguration = new SectionEditConfig();

    Set<String> enabledAttributes =
        getItemDetailEnabledAttributes(itemCode, goodsReceipt.getCurrentState());
    editConfiguration.setEnabledAttributes(enabledAttributes);

    AuthorizedPermissionsConfig permissionsConfig = getItemDetailsAuthorizedPermissionsConfig();
    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);
    editConfiguration.setAuthorizedReads(authorizedReads);

    String[] mandatories =
        controllerUtils.getItemDetailMandatoryAttributesByState(
            goodsReceipt.getCurrentState(), isItemBatchManaged(itemCode));
    editConfiguration.setMandatories(mandatories);

    return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/lock/itemssection/differencereason/{goodsReceiptCode}/{itemCode}")
  public @ResponseBody String itemSectionDifferenceReasonEditMode(
      @PathVariable String goodsReceiptCode, @PathVariable String itemCode) throws Exception {

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    controllerUtils.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    controllerUtils.checkAuthorizeForActionOnBusinessObject(
        goodsReceiptGeneralModel, IGoodsReceiptActionNames.Edit_DIFFERENCE_REASON);

    controllerUtils.setGoodsReceiptRecievedItemsRep(goodsReceiptRecievedItemsRep);
    controllerUtils.setGoodsReceiptRecievedItemsGeneralModelRep(
        goodsReceiptItemDataGeneralModelRep);
    IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptReceivedItemsData =
        controllerUtils.readGRItemByGRCodeAndItemCode(goodsReceiptCode, itemCode);

    controllerUtils.checkGRItemsExist(goodsReceiptReceivedItemsData);

    try {
      controllerUtils.checkIfActionIsAllowedInCurrentState(
          goodsReceiptCode, IGoodsReceiptActionNames.Edit_DIFFERENCE_REASON);
    } catch (ActionNotAllowedPerStateException actionNotAllowedPerStateException) {
      throw new EditNotAllowedPerStateException(actionNotAllowedPerStateException);
    }

    entityLockCommand.lockSection(goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);

    SectionEditConfig editConfiguration = new SectionEditConfig();
    Set<String> enabledAttributes =
        getItemDifferenceReasonEnabledAttributes(goodsReceiptGeneralModel.getCurrentState());
    editConfiguration.setEnabledAttributes(enabledAttributes);

    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    List<String> authorizedReads = controllerUtils.getAuthorizedReads(permissionsConfig);
    editConfiguration.setAuthorizedReads(authorizedReads);
    String[] mandatories =
        controllerUtils.getItemDifferenceReasonMandatoryAttributesByState(
            goodsReceiptGeneralModel.getCurrentState());
    editConfiguration.setMandatories(mandatories);

    return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/itemssection/differencereason/{goodsReceiptCode}/{itemCode}")
  public @ResponseBody String itemSectionDifferenceReasonCancelEdit(
      @PathVariable String goodsReceiptCode, @PathVariable String itemCode) throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);

    controllerUtils.setGoodsReceiptRecievedItemsRep(goodsReceiptRecievedItemsRep);
    controllerUtils.setGoodsReceiptRecievedItemsGeneralModelRep(
        goodsReceiptItemDataGeneralModelRep);

    controllerUtils.checkAuthorizeForActionOnBusinessObject(
        goodsReceiptGeneralModel, IGoodsReceiptActionNames.Edit_DIFFERENCE_REASON);

    IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptReceivedItemsData =
        controllerUtils.readGRItemByGRCodeAndItemCode(goodsReceiptCode, itemCode);

    controllerUtils.checkGRItemsExist(goodsReceiptReceivedItemsData);

    entityLockCommand.unlockSection(goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);

    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(
      method = RequestMethod.PUT,
      value = "/differencereason/{goodsReceiptCode}/{itemCode}")
  public @ResponseBody String saveDifferenceReasonItemSection(
      @PathVariable String goodsReceiptCode,
      @PathVariable String itemCode,
      @RequestBody String itemSectionData)
      throws Exception {
    controllerUtils.setGoodsReceiptRecievedItemsRep(goodsReceiptRecievedItemsRep);
    controllerUtils.setGoodsReceiptRecievedItemsGeneralModelRep(
        goodsReceiptItemDataGeneralModelRep);
    CommonControllerUtils.applySchemaValidation(
        IObGoodsReceiptReceivedItemsDifferenceReasonValueObject.class, itemSectionData);

    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.Edit_DIFFERENCE_REASON);

    IObGoodsReceiptPurchaseOrderReceivedItemsData goodsReceiptReceivedItemsData =
        controllerUtils.readGRItemByGRCodeAndItemCode(goodsReceiptCode, itemCode);

    controllerUtils.checkGRItemsExist(goodsReceiptReceivedItemsData);

    entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
        goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);

    IObGoodsReceiptReceivedItemsDifferenceReasonValueObject valueObject =
        (IObGoodsReceiptReceivedItemsDifferenceReasonValueObject)
            deserializeRequestBody(
                itemSectionData, IObGoodsReceiptReceivedItemsDifferenceReasonValueObject.class);
    CommonControllerUtils.applySchemaValidation(
        IObGoodsReceiptReceivedItemsDifferenceReasonValueObject.class, itemSectionData);
    valueObject.setGoodsReceiptCode(goodsReceiptCode);
    valueObject.setItemCode(itemCode);

    ValidationResult validationResult = grItemDifferenceReasonValidator.validate(valueObject);
    valueObject = grItemDifferenceReasonEditabilityManager.removeUnEditableFields(valueObject);

    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      try {
        goodsReceiptItemDifferenceReasonSaveCommand.executeCommand(valueObject);
      } finally {
        entityLockCommand.unlockSection(goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
      }
      return getSuccessResponse(IGoodsReceiptResponseMessages.SAVING_SUCCESSFUL_CODE);
    }
  }

  private AuthorizedPermissionsConfig getAddItemAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(IMObItem.SYS_NAME, IActionsNames.READ_ALL, "ReadItems");
    permissionsConfig.addPermissionConfig(
        StockTypeEnum.SYS_NAME, IActionsNames.READ_ALL, "ReadStockTypes");
    return permissionsConfig;
  }

  private AuthorizedPermissionsConfig getItemDetailsAuthorizedPermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(
        ICObMeasure.SYS_NAME, IActionsNames.READ_ALL, "ReadMeasures");
    permissionsConfig.addPermissionConfig(
        StockTypeEnum.SYS_NAME, IActionsNames.READ_ALL, "ReadStockTypes");
    return permissionsConfig;
  }

  private Object deserializeRequestBody(String valueObject, Class<?> valueObjectClass) {
    Gson gson = new Gson();
    Object serializedValueObject = gson.fromJson(valueObject, valueObjectClass);
    return serializedValueObject;
  }

  private void applyAuthorizationOnCreateAction(DObGoodsReceiptCreationValueObject valueObject)
      throws Exception {
    authorizationManager.authorizeAction(IDObGoodsReceipt.SYS_NAME, IActionsNames.CREATE);
    authorizationManager.authorizeAction(ICObGoodsReceipt.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObStorekeeper.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
    if (valueObject.getType().equals(IDObGoodsReceiptCreationValueObject.PURCHASE_ORDER)) {
      authorizeReadActionOnPurchaseOrder(valueObject.getRefDocumentCode());
    } else {
      authorizeReadActionOnSalesReturnOrder(valueObject.getRefDocumentCode());
    }
  }

  private void authorizeReadActionOnPurchaseOrder(String purchaseOrderCode) throws Exception {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        purchasingOrderGeneralModelRep.findOneByUserCode(purchaseOrderCode);
    try {
      checkIfEntityIsNull(purchaseOrderGeneralModel);
      authorizationManager.authorizeAction(purchaseOrderGeneralModel, IActionsNames.READ_ALL);
    } catch (ConditionalAuthorizationException | InstanceNotExistException exception) {
      throw new AuthorizationException();
    }
  }

  private void authorizeReadActionOnSalesReturnOrder(String salesReturnOrderCode) throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);
    try {
      checkIfEntityIsNull(salesReturnOrderGeneralModel);
      authorizationManager.authorizeAction(salesReturnOrderGeneralModel, IActionsNames.READ_ALL);
    } catch (ConditionalAuthorizationException | InstanceNotExistException exception) {
      throw new AuthorizationException();
    }
  }

  private String getSuccessCreationResponse(String userCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(
        CODE_RESPONSE_KEY, IGoodsReceiptResponseMessages.CREATION_SUCCESSFUL_CODE);
    return successResponse.toString();
  }

  private String unlockSection(String sectionName, String resourceCode) throws Exception {
    entityLockCommand.unlockSection(resourceCode, sectionName);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  @RequestMapping(method = RequestMethod.POST, value = "/unlock/lockedsections/{goodsReceiptCode}")
  public @ResponseBody String closeAllSectionsInGoodsReceipt(
      @PathVariable String goodsReceiptCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(goodsReceiptCode, lockedSections);
    return getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  private String getSuccessResponse(String msgCode) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(CODE_RESPONSE_KEY, msgCode);
    return successResponse.toString();
  }

  private DObGoodsReceiptGeneralModel applyAuthorization(String goodsReceiptCode, String actionName)
      throws Exception {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);

    try {
      authorizationManager.authorizeAction(goodsReceiptGeneralModel, actionName);
    } catch (ConditionalAuthorizationException ex) {
      AuthorizationException authorizationException = new AuthorizationException(ex);
      throw authorizationException;
    }
    return goodsReceiptGeneralModel;
  }

  private void checkIfEntityIsNull(Object object) throws Exception {
    if (object == null) {
      throw new InstanceNotExistException();
    }
  }

  private AuthorizedPermissionsConfig getCreatePermissionsConfig() {
    AuthorizedPermissionsConfig permissionsConfig = new AuthorizedPermissionsConfig();
    permissionsConfig.addPermissionConfig(
        ICObGoodsReceipt.SYS_NAME, IActionsNames.READ_ALL, "ReadGRType");
    permissionsConfig.addPermissionConfig(
        ICObStorehouse.SYS_NAME, IActionsNames.READ_ALL, "ReadStorehouse");
    permissionsConfig.addPermissionConfig(
        ICObStorekeeper.SYS_NAME, IActionsNames.READ_ALL, "ReadStorekeeper");
    permissionsConfig.addPermissionConfig(
        IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadLocalPurchaseOrders");
    permissionsConfig.addPermissionConfig(
        IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadImportPurchaseOrders");
    permissionsConfig.addPermissionConfig(
        ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadCompany");
    permissionsConfig.addPermissionConfig(
        IDObSalesReturnOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadSalesReturnOrders");
    return permissionsConfig;
  }

  @RequestMapping(
      method = RequestMethod.DELETE,
      value = "/delete/{goodsReceiptCode}/{itemCode}/{detailId}")
  public @ResponseBody String deleteGoodsReceiptDetail(
      @PathVariable String goodsReceiptCode,
      @PathVariable String itemCode,
      @PathVariable Long detailId)
      throws Exception {

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
    checkIfEntityIsNull(goodsReceiptGeneralModel);
    authorizeUpdateItemDetailPermission(goodsReceiptGeneralModel);
    controllerUtils.checkIfItemExists(goodsReceiptCode, itemCode);

    controllerUtils.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);

    LockDetails lockDetails =
        entityLockCommand.lockSection(goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
    try {
      controllerUtils.checkIfActionIsAllowedInCurrentState(
          goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);

      deleteDetailCommand.executeCommand(
          goodsReceiptCode, goodsReceiptGeneralModel.getType(), itemCode, detailId);
    } catch (ActionNotAllowedPerStateException actionNotAllowedPerStateException) {
      throw new RecordDeleteNotAllowedPerStateException();
    } finally {
      if (lockDetails != null)
        entityLockCommand.unlockSection(goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);
    }
    return getSuccessResponse(CommonControllerUtils.RECORD_DELETION_SUCCESSFUL_CODE);
  }

  private void authorizeUpdateItemDetailPermission(
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel) throws Exception {
    try {
      authorizationManager.authorizeAction(
          goodsReceiptGeneralModel, IGoodsReceiptActionNames.UPDATE_ITEM);
    } catch (ConditionalAuthorizationException | InstanceNotExistException exception) {
      throw new AuthorizationException();
    }
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/lock/edit/itemdetail/{goodsReceiptCode}/{itemCode}/{detailId}")
  public @ResponseBody String requestEditItemDetail(
      @PathVariable String goodsReceiptCode,
      @PathVariable String itemCode,
      @PathVariable Long detailId)
      throws Exception {

    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);

    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);

    controllerUtils.checkIfItemExists(goodsReceiptCode, itemCode);
    controllerUtils.checkIfItemDetailExists(
        goodsReceiptGeneralModel.getInventoryDocumentType(), itemCode, detailId);

    try {
      controllerUtils.lockGoodsReceiptSectionForEdit(
          goodsReceiptCode,
          IGoodsReceiptSectionNames.ITEMS_SECTION,
          IGoodsReceiptActionNames.UPDATE_ITEM);
      SectionEditConfig editConfiguration = new SectionEditConfig();
      Set<String> enabledAttributes =
          getItemDetailEnabledAttributes(itemCode, goodsReceiptGeneralModel.getCurrentState());
      editConfiguration.setEnabledAttributes(enabledAttributes);
      List<String> authorizedReads =
          controllerUtils.getAuthorizedReads(getItemDetailsAuthorizedPermissionsConfig());
      editConfiguration.setAuthorizedReads(authorizedReads);
      String[] mandatories =
          controllerUtils.getItemDetailMandatoryAttributesByState(
              goodsReceiptGeneralModel.getCurrentState(), isItemBatchManaged(itemCode));
      editConfiguration.setMandatories(mandatories);
      return controllerUtils.serializeEditConfigurationsResponse(editConfiguration);
    } catch (ActionNotAllowedPerStateException actionNotAllowedPerStateException) {
      throw new EditNotAllowedPerStateException();
    }
  }

  @RequestMapping(
      method = RequestMethod.GET,
      value = "/unlock/edit/itemdetail/{goodsReceiptCode}/{itemCode}/{detailId}")
  public @ResponseBody String requestCancelItemDetail(
      @PathVariable String goodsReceiptCode,
      @PathVariable String itemCode,
      @PathVariable Long detailId)
      throws Exception {

    applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);

    controllerUtils.checkIfItemExists(goodsReceiptCode, itemCode);
    controllerUtils.checkIfItemDetailExists(
        goodsReceiptGeneralModel.getInventoryDocumentType(), itemCode, detailId);

    return unlockSection(IGoodsReceiptSectionNames.ITEMS_SECTION, goodsReceiptCode);
  }

  @RequestMapping(
      method = RequestMethod.PUT,
      value = "/save/itemdetail/{goodsReceiptCode}/{itemCode}/{detailId}")
  public @ResponseBody String saveItemDetail(
      @PathVariable String goodsReceiptCode,
      @PathVariable String itemCode,
      @PathVariable Long detailId,
      @RequestBody String itemDetail)
      throws Exception {
    try {
      applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
      authorizeItemDetailReads();

      Boolean isBatched = isItemBatchManaged(itemCode);
      IObGoodsReceiptItemDetailValueObject valueObject = new IObGoodsReceiptItemDetailValueObject();
      if (isBatched) {
        CommonControllerUtils.applySchemaValidation(
            IObGoodsReceiptItemBatchValueObject.class, itemDetail);
        valueObject = new Gson().fromJson(itemDetail, IObGoodsReceiptItemBatchValueObject.class);
        valueObject.setDetailType("Batch");
      } else {
        CommonControllerUtils.applySchemaValidation(
            IObGoodsReceiptItemQuantityValueObject.class, itemDetail);
        valueObject = new Gson().fromJson(itemDetail, IObGoodsReceiptItemQuantityValueObject.class);
        valueObject.setDetailType("Quantity");
      }
      valueObject.setId(detailId);
      valueObject.setGoodsReceiptCode(goodsReceiptCode);
      valueObject.setItemCode(itemCode);

      DObGoodsReceiptGeneralModel goodsReceipt =
          goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
      checkIfEntityIsNull(goodsReceipt);

      valueObject.setGoodsReceiptType(goodsReceipt.getType());

      controllerUtils.checkIfItemExists(goodsReceiptCode, itemCode);
      controllerUtils.checkIfItemDetailExists(
          goodsReceipt.getInventoryDocumentType(), itemCode, detailId);

      entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
          goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);

      ValidationResult validationResult = detailValidator.validate(valueObject);
      if (!validationResult.isValid()) {
        return CommonControllerUtils.getFailureResponse(validationResult);
      }

      try {
        saveDetailCommand.executeCommand(valueObject);
      } finally {
        unlockSection(IGoodsReceiptSectionNames.ITEMS_SECTION, goodsReceiptCode);
      }
      return getSuccessResponse(IGoodsReceiptResponseMessages.SAVING_SUCCESSFUL_CODE);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
  }

  @RequestMapping(
      method = RequestMethod.PUT,
      value = "/add/itemdetail/{goodsReceiptCode}/{itemCode}")
  public @ResponseBody String addItemDetail(
      @PathVariable String goodsReceiptCode,
      @PathVariable String itemCode,
      @RequestBody String itemDetail)
      throws Exception {

    try {
      applyAuthorization(goodsReceiptCode, IGoodsReceiptActionNames.UPDATE_ITEM);
      authorizeItemDetailReads();

      Boolean isBatched = isItemBatchManaged(itemCode);
      IObGoodsReceiptItemDetailValueObject valueObject = new IObGoodsReceiptItemDetailValueObject();
      if (isBatched) {
        CommonControllerUtils.applySchemaValidation(
            IObGoodsReceiptItemBatchValueObject.class, itemDetail);
        valueObject = new Gson().fromJson(itemDetail, IObGoodsReceiptItemBatchValueObject.class);
        valueObject.setDetailType("Batch");
      } else {
        CommonControllerUtils.applySchemaValidation(
            IObGoodsReceiptItemQuantityValueObject.class, itemDetail);
        valueObject = new Gson().fromJson(itemDetail, IObGoodsReceiptItemQuantityValueObject.class);
        valueObject.setDetailType("Quantity");
      }
      valueObject.setGoodsReceiptCode(goodsReceiptCode);
      valueObject.setItemCode(itemCode);

      DObGoodsReceiptGeneralModel goodsReceipt =
          goodsReceiptGeneralModelRep.findOneByUserCode(goodsReceiptCode);
      checkIfEntityIsNull(goodsReceipt);

      valueObject.setGoodsReceiptType(goodsReceipt.getType());

      controllerUtils.checkIfItemExists(goodsReceiptCode, itemCode);

      entityLockCommand.checkIfResourceIsLockedByLoggedInUser(
          goodsReceiptCode, IGoodsReceiptSectionNames.ITEMS_SECTION);

      ValidationResult validationResult = detailValidator.validate(valueObject);
      if (!validationResult.isValid()) {
        return CommonControllerUtils.getFailureResponse(validationResult);
      }

      try {
        saveDetailCommand.executeCommand(valueObject);
      } finally {
        unlockSection(IGoodsReceiptSectionNames.ITEMS_SECTION, goodsReceiptCode);
      }
      return getSuccessResponse(IGoodsReceiptResponseMessages.SAVING_SUCCESSFUL_CODE);
    } catch (ConditionalAuthorizationException ex) {
      throw new AuthorizationException();
    }
  }

  private void authorizeItemDetailReads() {
    authorizationManager.authorizeAction(ICObMeasure.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(StockTypeEnum.SYS_NAME, IActionsNames.READ_ALL);
  }

  private void initItemDetailRepositories() {
    controllerUtils.setReceivedItemsGeneralModelRep(receivedItemsGeneralModelRep);
    controllerUtils.setItemGeneralModelRep(itemGeneralModelRep);
    controllerUtils.setGoodsReceiptPurchaseOrderItemQuantitiesRep(
        goodsReceiptPurchaseOrderItemQuantitiesRep);
    controllerUtils.setGoodsReceiptSalesReturnItemQuantitiesRep(
        goodsReceiptSalesReturnItemQuantitiesRep);
    controllerUtils.setBatchesRep(batchesRep);
  }

  private Set<String> getAddItemEnabledAttributes(String currentState) {
    return addItemEditabilityManager.getEnabledAttributesConfig().get(currentState);
  }

  private Set<String> getItemDetailEnabledAttributes(String itemCode, String currentState) {
    boolean isBatched = isItemBatchManaged(itemCode);
    if (isBatched) {
      return batchEditabilityManager.getEnabledAttributesConfig().get(currentState);
    } else {
      return quantityEditabilityManager.getEnabledAttributesConfig().get(currentState);
    }
  }

  private Set<String> getItemDifferenceReasonEnabledAttributes(String currentState) {
    return grItemDifferenceReasonEditabilityManager.getEnabledAttributesConfig().get(currentState);
  }

  private Boolean isItemBatchManaged(String itemCode) {
    return itemGeneralModelRep.findByUserCode(itemCode).getIsBatchManaged();
  }

  public void applyAuthorizationOnItemSectionFields() {
    authorizationManager.authorizeAction(IMObItem.SYS_NAME, IActionsNames.READ_ALL);
    authorizationManager.authorizeAction(StockTypeEnum.SYS_NAME, IActionsNames.READ_ALL);
  }
}
