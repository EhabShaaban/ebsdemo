package com.ebs.dda.rest.accounting.paymentrequest;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.apis.concurrency.model.LockDetails;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObPaymentRequestAuthorizationManager;
import com.ebs.dda.accounting.paymentrequest.apis.IDObPaymentRequestJsonSchema;
import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentCreateCommandFactory;
import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentRequestCreateCommand;
import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentRequestDeleteCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.accountingnotes.IDObAccountingNote;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum.PaymentType;
import com.ebs.dda.jpa.accounting.vendorinvoice.IDObVendorInvoice;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.company.ICObCompany;
import com.ebs.dda.jpa.masterdata.customer.IMObEmployee;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendor;
import com.ebs.dda.masterdata.buisnessunit.IPurchaseUnitActionNames;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.apis.IDObPurchaseOrder;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.paymentrequest.DObPaymentCreationValidatorFactory;
import com.ebs.dda.validation.paymentrequest.DObPaymentRequestCreationValidator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "command/accounting/paymentrequest")
public class DObPaymentRequestCommandController implements InitializingBean {
  private final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  @Autowired EntityLockCommand<DObPaymentRequest> entityLockCommand;
  @Autowired private DObPaymentCreateCommandFactory paymentCreateCommandFactory;
  @Autowired private DObPaymentCreationValidatorFactory paymentCreationValidatorFactory;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObPaymentRequestAuthorizationManager paymentRequestAuthorizationManager;

  @Autowired private CObPurchasingUnitGeneralModelRep cObPurchasingUnitGeneralModelRep;


  private CommonControllerUtils commonControllerUtils;
  @Autowired private DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;
  @Autowired private DObPaymentRequestDeleteCommand paymentRequestDeleteCommand;

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createPaymentRequest(@RequestBody String paymentRequestData)
      throws Exception {
    CommonControllerUtils.applySchemaValidationForString(
            IDObPaymentRequestJsonSchema.PAYMENT_CREATE_SCHEMA, paymentRequestData);
    DObPaymentRequestCreateValueObject paymentRequestCreateValueObject =
        (DObPaymentRequestCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                paymentRequestData, DObPaymentRequestCreateValueObject.class);
    applyAuthorizationForCreate(paymentRequestCreateValueObject);
    DObPaymentRequestCreationValidator validator = paymentCreationValidatorFactory.getValidatorInstance(paymentRequestCreateValueObject);
    ValidationResult validationResult =
            validator.validate(paymentRequestCreateValueObject);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(validationResult);
    } else {
      final StringBuilder userCode = new StringBuilder();
    DObPaymentRequestCreateCommand  paymentRequestCreateCommand = paymentCreateCommandFactory.getCommandInstance(paymentRequestCreateValueObject.getDueDocumentType(), paymentRequestCreateValueObject.getPaymentType());
      paymentRequestCreateCommand
          .executeCommand(paymentRequestCreateValueObject)
          .subscribe(code -> userCode.append(code));
      return commonControllerUtils.getSuccessCreationResponse(userCode.toString());
    }
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{paymentRequestCode}")
  public @ResponseBody String deletePaymentRequest(@PathVariable String paymentRequestCode)
      throws Exception {
    paymentRequestDeleteCommand.checkIfInstanceExists(paymentRequestCode);

    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);

    paymentRequestAuthorizationManager.authorizeActionOnSection(
        paymentRequestGeneralModel, IActionsNames.DELETE);

    LockDetails lockDetails = entityLockCommand.lockAllSections(paymentRequestCode);

    try {
      paymentRequestDeleteCommand.checkIfAllowedAction(
          paymentRequestGeneralModel, IActionsNames.DELETE);

      paymentRequestDeleteCommand.executeCommand(paymentRequestCode);

    } catch (ActionNotAllowedPerStateException e) {
      throw new DeleteNotAllowedPerStateException();

    } finally {
      if (lockDetails != null) entityLockCommand.unlockAllSections(paymentRequestCode);
    }

    return CommonControllerUtils.getSuccessResponseWithUserCode(
        paymentRequestCode, CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String getCreateConfiguration() {
    applyAuthorizationForRequestCreate();
    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();
    List<String> authorizedReads =
        commonControllerUtils.getAuthorizedReads(createPermissionsConfig);
    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return commonControllerUtils.serializeEditConfigurationsResponse(createConfig);
  }

  private void applyAuthorizationForRequestCreate() {
    authorizationManager.authorizeAction(IDObPaymentRequest.SYS_NAME, IActionsNames.CREATE);
  }

  @RequestMapping(
      method = RequestMethod.POST,
      value = "/unlock/lockedsections/{paymentRequestCode}")
  public @ResponseBody String closeAllSectionsInPaymentRequest(
      @PathVariable String paymentRequestCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(paymentRequestCode, lockedSections);
    return CommonControllerUtils.getSuccessResponse(SUCCESS_RESPONSE_KEY);
  }

  private void applyAuthorizationForCreate(DObPaymentRequestCreateValueObject valueObject)
      throws Exception {
    authorizationManager.authorizeAction(IDObPaymentRequest.SYS_NAME, IActionsNames.CREATE);
    applyAuthorizationForPurchaseUnit(valueObject.getBusinessUnitCode());
    applyAuthorizationForBusinessPartner(valueObject.getPaymentType());
    applyAuthorizationForDocumentOwner();
    applyAuthorizationForCompany();
    applyAuthorizationForRefDocument(valueObject.getDueDocumentType());
  }

  private void applyAuthorizationForRefDocument(DueDocumentTypeEnum.DueDocumentType dueDocumentType) {
    if (dueDocumentType != null) {
      if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE)) {
        authorizationManager.authorizeAction(IDObVendorInvoice.SYS_NAME,
                IActionsNames.READ_ALL);
      } else if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)) {
        authorizationManager.authorizeAction(IDObPurchaseOrder.SYS_NAME,
                IActionsNames.READ_ALL);
      }
      if (dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.CREDITNOTE)) {
        authorizationManager.authorizeAction(IDObAccountingNote.SYS_NAME,
                IActionsNames.READ_ALL);
      }
    }
  }

  private void applyAuthorizationForCompany() {
    authorizationManager.authorizeAction(ICObCompany.SYS_NAME, IActionsNames.READ_ALL);
  }

  private void applyAuthorizationForPurchaseUnit(String purchaseUnitCode) throws Exception {
    CObPurchasingUnitGeneralModel purchasingUnitGeneralModel =
        cObPurchasingUnitGeneralModelRep.findOneByUserCode(purchaseUnitCode);
    if (purchasingUnitGeneralModel != null) {
      try {
        authorizationManager.authorizeAction(
            purchasingUnitGeneralModel, IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER);
      } catch (ConditionalAuthorizationException ex) {
        throw new AuthorizationException(ex);
      }
    }
  }

  private void applyAuthorizationForBusinessPartner(PaymentType paymentType){
    if (paymentType.equals(PaymentType.VENDOR)) {
      authorizationManager.authorizeAction(IMObVendor.SYS_NAME, IActionsNames.READ_ALL);
    }
    if (paymentType.equals(PaymentType.UNDER_SETTLEMENT)) {
      authorizationManager.authorizeAction(IMObEmployee.SYS_NAME, IActionsNames.READ_ALL);
    }
  }

  private void applyAuthorizationForDocumentOwner() throws Exception {
      authorizationManager.authorizeAction(IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL);

  }

  private AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME,
        IPurchaseUnitActionNames.READ_ALL_FOR_PURCHASE_ORDER,
        "ReadPurchasingUnits");
    configuration.addPermissionConfig(IMObVendor.SYS_NAME, IActionsNames.READ_ALL, "ReadVendors");
    configuration.addPermissionConfig(IMObEmployee.SYS_NAME, IActionsNames.READ_ALL, "ReadEmployees");
    configuration.addPermissionConfig(ICObCompany.SYS_NAME, IActionsNames.READ_ALL, "ReadCompanies");
    configuration.addPermissionConfig(IDObPurchaseOrder.SYS_NAME, IActionsNames.READ_ALL, "ReadPurchaseOrders");
    configuration.addPermissionConfig(IDObVendorInvoice.SYS_NAME, IActionsNames.READ_ALL, "ReadVendorInvoices");
    configuration.addPermissionConfig(IDObAccountingNote.SYS_NAME, IActionsNames.READ_ALL, "ReadAccountingNotes");
    configuration.addPermissionConfig(IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL, "ReadDocumentOwners");
    return configuration;
  }

  @Override
  public void afterPropertiesSet() {
    commonControllerUtils = new CommonControllerUtils(authorizationManager);
  }
}
