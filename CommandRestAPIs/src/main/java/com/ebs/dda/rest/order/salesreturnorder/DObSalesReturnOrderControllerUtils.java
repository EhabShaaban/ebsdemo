package com.ebs.dda.rest.order.salesreturnorder;

import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig.AuthorizedPermissionConfig;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.DeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.EditNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.operational.other.RecordDeleteNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.exceptions.ConditionalAuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.jpa.IDocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.IDObSalesInvoice;
import com.ebs.dda.jpa.masterdata.businessunit.ICObPurchasingUnit;
import com.ebs.dda.jpa.masterdata.customer.IMObCustomer;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderCreateValueObject;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrderGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.ICObSalesReturnOrderType;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderStateMachine;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;

public class DObSalesReturnOrderControllerUtils extends CommonControllerUtils {

  protected DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  public DObSalesReturnOrderControllerUtils(AuthorizationManager authorizationManager) {
    super(authorizationManager);
  }

  public void checkIfDeleteActionIsAllowedInSalesReturnOrderCurrentState(
      String salesReturnOrderCode, String actionName) throws Exception {
    try {
      checkIfActionIsAllowedInCurrentStateFromStateMachine(salesReturnOrderCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      DeleteNotAllowedPerStateException deleteNotAllowedPerStateException =
          new DeleteNotAllowedPerStateException();
      deleteNotAllowedPerStateException.initCause(e);
      throw deleteNotAllowedPerStateException;
    }
  }

  public void checkIfUpdateActionIsAllowedInSalesReturnOrderCurrentState(
      String salesReturnOrderCode, String actionName) throws Exception {
    try {
      checkIfActionIsAllowedInCurrentStateFromStateMachine(salesReturnOrderCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      EditNotAllowedPerStateException editNotAllowedPerStateException =
          new EditNotAllowedPerStateException();
      editNotAllowedPerStateException.initCause(e);
      throw editNotAllowedPerStateException;
    }
  }

  public void checkIfRecordDeleteActionIsAllowedInSalesOrderCurrentState(
      String salesOrderCode, String actionName) throws Exception {
    try {
      checkIfActionIsAllowedInCurrentStateFromStateMachine(salesOrderCode, actionName);
    } catch (ActionNotAllowedPerStateException e) {
      RecordDeleteNotAllowedPerStateException recordDeleteNotAllowedPerStateException =
          new RecordDeleteNotAllowedPerStateException();
      recordDeleteNotAllowedPerStateException.initCause(e);
      throw recordDeleteNotAllowedPerStateException;
    }
  }

  protected void checkIfActionIsAllowedInCurrentStateFromStateMachine(
      String salesReturnOrderCode, String actionName) throws Exception {
    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        salesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);
    DObSalesReturnOrderStateMachine salesReturnOrderStateMachine =
        new DObSalesReturnOrderStateMachine();
    checkIfActionIsAllowedInCurrentState(
        salesReturnOrderStateMachine, salesReturnOrderGeneralModel, actionName);
  }

  public AuthorizedPermissionsConfig constructCreateConfiguration() {
    AuthorizedPermissionsConfig configuration = new AuthorizedPermissionsConfig();
    configuration.addPermissionConfig(
        ICObSalesReturnOrderType.SYS_NAME, IActionsNames.READ_ALL, "ReadSROTypes");
    configuration.addPermissionConfig(
        ICObPurchasingUnit.SYS_NAME, IActionsNames.READ_ALL, "ReadBusinessUnits");
    configuration.addPermissionConfig(
        IMObCustomer.SYS_NAME, IActionsNames.READ_ALL, "ReadCustomers");
    configuration.addPermissionConfig(
        IDObSalesInvoice.SYS_NAME, IActionsNames.READ_ALL, "ReadSalesInvoices");
    configuration.addPermissionConfig(
        IDocumentOwnerGeneralModel.SYS_NAME, IActionsNames.READ_ALL, "ReadDocumentOwners");
    return configuration;
  }

  public void authorizeActionOnReadingDropdowns() {
    AuthorizedPermissionsConfig createPermissionsConfig = constructCreateConfiguration();
    for (AuthorizedPermissionConfig config : createPermissionsConfig.getPermissionsConfig()) {
      try {
        authorizationManager.authorizeAction(
            config.getEntitySysName(), config.getPermissionToCheck());
      } catch (AuthorizationException | ConditionalAuthorizationException exception) {
        throw new AuthorizationException(exception);
      }
    }
  }

  public void authorizeActionOnDropdownsValues(DObSalesReturnOrderCreateValueObject valueObject)
      throws Exception {
    try {
      DObSalesInvoiceGeneralModel salesInvoice =
          salesInvoiceGeneralModelRep.findOneByUserCode(valueObject.getSalesInvoiceCode());
      authorizeOnSalesInvoice(salesInvoice);

    } catch (ConditionalAuthorizationException exception) {
      throw new AuthorizationException(exception);
    }
  }

  private void authorizeOnSalesInvoice(DObSalesInvoiceGeneralModel salesInvoice) throws Exception {
    if (salesInvoice == null) {
      return;
    }
    authorizationManager.authorizeAction(salesInvoice, IActionsNames.READ_ALL);
  }

  public void setSalesReturnOrderGeneralModelRep(
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep) {
    this.salesReturnOrderGeneralModelRep = salesReturnOrderGeneralModelRep;
  }

  public void setPurchasingUnitGeneralModelRep(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    this.purchasingUnitGeneralModelRep = purchasingUnitGeneralModelRep;
  }

  public void setCustomerBusinessUnitGeneralModelRep(
      IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep) {
    this.customerBusinessUnitGeneralModelRep = customerBusinessUnitGeneralModelRep;
  }

  public void setSalesInvoiceGeneralModelRep(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    this.salesInvoiceGeneralModelRep = salesInvoiceGeneralModelRep;
  }
}
