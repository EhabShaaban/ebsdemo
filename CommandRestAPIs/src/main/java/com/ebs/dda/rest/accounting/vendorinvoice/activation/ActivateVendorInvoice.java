package com.ebs.dda.rest.accounting.vendorinvoice.activation;

public interface ActivateVendorInvoice {
    String activate(VendorInvoiceActivateContextBuilder.VendorInvoiceActivateContext context) throws Exception;
}
