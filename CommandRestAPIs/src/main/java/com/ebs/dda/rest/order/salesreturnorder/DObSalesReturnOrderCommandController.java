package com.ebs.dda.rest.order.salesreturnorder;

import com.ebs.dac.dbo.events.order.salesreturnorder.SalesReturnOrderMarkAsShippedSuccessEvent;
import com.ebs.dac.edit.config.AuthorizedPermissionsConfig;
import com.ebs.dac.edit.config.SectionEditConfig;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.DObSalesReturnOrderAuthorizationManager;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesreturnorder.DObSalesReturnOrderCreateCommand;
import com.ebs.dda.commands.order.salesreturnorder.DObSalesReturnOrderDeleteCommand;
import com.ebs.dda.commands.order.salesreturnorder.DObSalesReturnOrderMarkAsShippedCommand;
import com.ebs.dda.commands.order.salesreturnorder.DObSalesReturnPdfGeneratorCommand;
import com.ebs.dda.jpa.order.salesreturnorder.*;
import com.ebs.dda.order.salesreturnorder.*;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rx.ObservablesEventBus;
import io.reactivex.Observable;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ebs.dda.rest.CommonControllerUtils.getFailureResponse;
import static com.ebs.dda.rest.CommonControllerUtils.getSuccessResponse;

@RestController
@RequestMapping(value = "command/order/salesreturnorder")
public class DObSalesReturnOrderCommandController implements InitializingBean {

  @Autowired private DObSalesReturnOrderCreateCommand salesReturnOrderCreateCommand;
  @Autowired private DObSalesReturnOrderMarkAsShippedCommand salesReturnOrderMarkAsShippedCommand;
  @Autowired private EntityLockCommand<DObSalesReturnOrder> entityLockCommand;
  @Autowired private AuthorizationManager authorizationManager;
  @Autowired private DObSalesReturnOrderAuthorizationManager sroAuthorizationManager;
  @Autowired private DObSalesReturnOrderGeneralModelRep dobSalesReturnOrderGeneralModelRep;
  @Autowired private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  @Autowired private IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep;
  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired private DObSalesReturnOrderDeleteCommand salesReturnOrderDeleteCommand;
  @Autowired private DObSalesReturnOrderCreateValidator validator;
  @Autowired private DObSalesReturnOrderMandatoryValidator salesReturnOrderMandatoryValidator;
  @Autowired private DObSalesReturnOrderValidator salesReturnOrderValidator;
  @Autowired private ObservablesEventBus observablesEventBus;
  @Autowired private DObSalesReturnPdfGeneratorCommand salesReturnPdfGeneratorCommand;

  private CommonControllerUtils commonUtils;
  private DObSalesReturnOrderControllerUtils utils;

  @RequestMapping(method = RequestMethod.POST, value = "/")
  public @ResponseBody String createSalesReturnOrder(@RequestBody String salesReturnOrderData)
      throws Exception {

    CommonControllerUtils.applySchemaValidation(
        DObSalesReturnOrderCreateValueObject.class, salesReturnOrderData);

    authorizationManager.authorizeAction(IDObSalesReturnOrder.SYS_NAME, IActionsNames.CREATE);
    utils.authorizeActionOnReadingDropdowns();

    DObSalesReturnOrderCreateValueObject valueObject =
        (DObSalesReturnOrderCreateValueObject)
            CommonControllerUtils.deserializeRequestBody(
                salesReturnOrderData, DObSalesReturnOrderCreateValueObject.class);

    utils.authorizeActionOnDropdownsValues(valueObject);

    ValidationResult validationResult = validator.validate(valueObject);
    if (!validationResult.isValid()) {
      return getFailureResponse(validationResult);
    }
    final StringBuilder userCode = new StringBuilder();
    salesReturnOrderCreateCommand
        .executeCommand(valueObject)
        .subscribe(code -> userCode.append(code));
    return commonUtils.getSuccessCreationResponse(userCode.toString());
  }

  @RequestMapping(method = RequestMethod.GET, value = "/")
  public @ResponseBody String getCreateConfiguration() {
    authorizationManager.authorizeAction(IDObSalesReturnOrder.SYS_NAME, IActionsNames.CREATE);
    AuthorizedPermissionsConfig createPermissionsConfig = utils.constructCreateConfiguration();
    List<String> authorizedReads = commonUtils.getAuthorizedReads(createPermissionsConfig);
    SectionEditConfig createConfig = new SectionEditConfig();
    createConfig.setAuthorizedReads(authorizedReads);
    return commonUtils.serializeEditConfigurationsResponse(createConfig);
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{salesReturnOrderCode}")
  public @ResponseBody String deleteSalesReturnOrder(@PathVariable String salesReturnOrderCode)
      throws Exception {

    DObSalesReturnOrderGeneralModel salesReturnOrderGeneralModel =
        dobSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(
        salesReturnOrderGeneralModel, IActionsNames.DELETE);

    utils.checkIfDeleteActionIsAllowedInSalesReturnOrderCurrentState(
        salesReturnOrderCode, IActionsNames.DELETE);

    DObSalesReturnOrderDeletionValueObject valueObject =
        new DObSalesReturnOrderDeletionValueObject();
    valueObject.setSalesReturnOrderCode(salesReturnOrderCode);

    entityLockCommand.lockAllSections(salesReturnOrderCode);

    salesReturnOrderDeleteCommand.executeCommand(valueObject);

    entityLockCommand.unlockAllSections(salesReturnOrderCode);

    return CommonControllerUtils.getSuccessResponse(
        CommonControllerUtils.DELETION_SUCCESSFUL_MESSAGE_CODE);
  }

  @RequestMapping(
      method = RequestMethod.POST,
      value = "/{salesReturnOrderCode}/unlock/lockedsections")
  public @ResponseBody String closeAllSectionsInSalesReturnOrder(
      @PathVariable String salesReturnOrderCode, HttpServletRequest request) throws Exception {
    BufferedReader reader = request.getReader();
    String requestData = reader.lines().collect(Collectors.joining());
    String[] splitedSections = requestData.replaceAll("[\\[\\]\\s\"]", "").split(",");
    List<String> lockedSections = Arrays.asList(splitedSections);
    entityLockCommand.unlockAllResourceLockedSectionsByUser(salesReturnOrderCode, lockedSections);
    return CommonControllerUtils.getSuccessResponse(CommonControllerUtils.SAVING_SUCCESSFUL_CODE);
  }

  @RequestMapping(method = RequestMethod.PUT, value = "/{salesReturnOrderCode}/markasshipped")
  public @ResponseBody String markSalesReturnOrderAsShipped(
      @PathVariable String salesReturnOrderCode) throws Exception {

    DObSalesReturnOrderGeneralModel salesReturnOrder =
        dobSalesReturnOrderGeneralModelRep.findOneByUserCode(salesReturnOrderCode);

    utils.checkAuthorizeForActionOnBusinessObject(salesReturnOrder, IActionsNames.MARK_AS_SHIPPED);

    utils.checkIfActionIsAllowedInCurrentStateFromStateMachine(
        salesReturnOrderCode, IActionsNames.MARK_AS_SHIPPED);

    DObSalesReturnOrderMarkAsShippedValueObject valueObject =
        new DObSalesReturnOrderMarkAsShippedValueObject();
    valueObject.setSalesReturnOrderCode(salesReturnOrderCode);

    Map<String, Object> missingFields =
        salesReturnOrderMandatoryValidator.validateAllSectionsForState(
            salesReturnOrderCode, DObSalesReturnOrderStateMachine.SHIPPED_STATE);

    if (!missingFields.isEmpty()) {
      return CommonControllerUtils.getFailureResponseWithMissingFields(
          missingFields, CommonControllerUtils.MISSING_FIELDS_RESPONSE_CODE);
    }

    ValidationResult validationResult = salesReturnOrderValidator.validate(salesReturnOrderCode);
    if (!validationResult.isValid()) {
      return CommonControllerUtils.getFailureResponse(
          validationResult, ISalesReturnOrderMessages.FAILURE_MARK_AS_SHIPPED);
    }

    entityLockCommand.lockAllSections(salesReturnOrderCode);

    Observable<String> salesReturnOrderMarkAsShippedObservable =
        salesReturnOrderMarkAsShippedCommand.executeCommand(valueObject);
    salesReturnOrderMarkAsShippedObservable.subscribe(
        this::consumeSalesReturnOrderMarkAsShippedEvent);

    entityLockCommand.unlockAllSections(salesReturnOrderCode);

    return CommonControllerUtils.getSuccessResponse(
        ISalesReturnOrderMessages.SUCCESS_MARK_AS_SHIPPED);
  }

  private void consumeSalesReturnOrderMarkAsShippedEvent(String userCode) {
    SalesReturnOrderMarkAsShippedSuccessEvent event =
        new SalesReturnOrderMarkAsShippedSuccessEvent(userCode);
    observablesEventBus.consumeEvent(event);
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{salesReturnCode}/generatepdf")
  public @ResponseBody String generateSalesReturnPdf(@PathVariable String salesReturnCode)
      throws Exception {
    entityLockCommand.lockAllSections(salesReturnCode);
    StringBuilder pdfStringBuilder = new StringBuilder();
    try {
      salesReturnPdfGeneratorCommand
          .executeCommand(salesReturnCode)
          .subscribe(commandPdf -> pdfStringBuilder.append(commandPdf));
    } finally {
      entityLockCommand.unlockAllSections(salesReturnCode);
    }
    return getSuccessResponse(pdfStringBuilder.toString());
  }

  @Override
  public void afterPropertiesSet() {
    commonUtils = new CommonControllerUtils(authorizationManager);
    utils = new DObSalesReturnOrderControllerUtils(authorizationManager);
    utils.setSalesReturnOrderGeneralModelRep(dobSalesReturnOrderGeneralModelRep);
    utils.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    utils.setCustomerBusinessUnitGeneralModelRep(customerBusinessUnitGeneralModelRep);
    utils.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
  }
}
