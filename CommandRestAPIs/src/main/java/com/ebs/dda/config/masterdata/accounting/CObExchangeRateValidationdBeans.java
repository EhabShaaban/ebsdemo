package com.ebs.dda.config.masterdata.accounting;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.ebs.dda.masterdata.exchangerate.CObExchangeRateCreateValidator;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;

@Configuration
public class CObExchangeRateValidationdBeans {

    @Bean
    public CObExchangeRateCreateValidator cObExchangeRateCreateValidator(
            CObCurrencyRep cObCurrencyRep, CObExchangeRateGeneralModelRep cObExchangeRateRep) {
        CObExchangeRateCreateValidator cObExchangeRateCreateValidator =
                new CObExchangeRateCreateValidator();
        cObExchangeRateCreateValidator.setcObCurrencyRep(cObCurrencyRep);
        cObExchangeRateCreateValidator.setcObExchangeRateRep(cObExchangeRateRep);
        return cObExchangeRateCreateValidator;
    }
}
