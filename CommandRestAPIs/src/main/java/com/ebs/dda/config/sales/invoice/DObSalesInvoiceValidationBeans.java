package com.ebs.dda.config.sales.invoice;

import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceAuthorizationGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.validation.salesinvoice.DObSalesInvoiceActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObSalesInvoiceValidationBeans {

    @Bean(value = "DObSalesInvoiceActivateValidator")
    public DObSalesInvoiceActivateValidator createActivateValidator(
            IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep,
            DObSalesInvoiceAuthorizationGeneralModelRep salesInvoiceAuthorizationGeneralModelRep,
            CObFiscalYearRep fiscalYearRep) {
        DObSalesInvoiceActivateValidator validator = new DObSalesInvoiceActivateValidator();
        validator.setSalesInvoiceBusinessPartnerGeneralModelRep(salesInvoiceBusinessPartnerGeneralModelRep);
        validator.setSalesInvoiceAuthorizationGeneralModelRep(salesInvoiceAuthorizationGeneralModelRep);
        validator.setFiscalYearRep(fiscalYearRep);
        return validator;
    }
}
