package com.ebs.dda.config.purchaseorder;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.concurrency.IEditTokenValidityPolicy;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.concurrency.policies.TimeoutEditTokenValidityPolicy;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.CObApprovalPolicyRep;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.CObControlPointRep;
import com.ebs.dda.approval.dbo.jpa.repositories.generalmodel.ApprovalPolicyControlPointUsersGeneralModelRep;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.purchaseorder.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnit;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.LObAttachmentTypeRep;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.RObAttachmentRep;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects.IObOrderLineDetailsQuantitiesRep;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.*;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderNewStateMachineFactory;
import com.ebs.dda.purchases.statemachines.DObPurchaseOrderStateMachineFactory;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.enterprise.CObEnterpriseRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.lookups.LObContainerRequirementRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIncotermsRep;
import com.ebs.dda.repositories.masterdata.lookups.LObPortRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.shipping.dbo.jpa.repositories.businessobjects.lookups.LObModeOfTransportRep;
import com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects.CObShippingRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class DObPurchaseOrderCommandBeans {

  @Bean
  public PurchaseOrderCreateCommand purchaseOrderCreateCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      DObPurchaseOrderRep purchaseOrderRep,
      IUserAccountSecurityService userAccountSecurityService,
      MObVendorGeneralModelRep vendorGeneralModelRep,
      CObEnterpriseRep<CObPurchasingUnit> enterpriseRep,
      IObOrderCompanyGeneralModelRep orderCompanyGeneralModelRep,
      LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep) {
    DObPurchaseOrderStateMachineFactory stateMachineFactory =
        new DObPurchaseOrderStateMachineFactory();
    PurchaseOrderCreateCommand purchaseOrderCreateCommand =
        new PurchaseOrderCreateCommand(documentObjectCodeGenerator, stateMachineFactory);
    purchaseOrderCreateCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderCreateCommand.setEnterpriseRep(enterpriseRep);
    purchaseOrderCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderCreateCommand.setVendorGeneralModelRep(vendorGeneralModelRep);
    purchaseOrderCreateCommand.setCompanyTaxesGeneralModelRep(companyTaxesGeneralModelRep);
    purchaseOrderCreateCommand.setOrderCompanyGeneralModelRep(orderCompanyGeneralModelRep);
    return purchaseOrderCreateCommand;
  }

  @Bean
  public PurchaseOrderDeleteCommand purchaseOrderDeleteCommand(
      DObOrderDocumentRep orderDocumentRep) {
    PurchaseOrderDeleteCommand purchaseOrderDeleteCommand = new PurchaseOrderDeleteCommand();
    purchaseOrderDeleteCommand.setOrderDocumentRep(orderDocumentRep);
    return purchaseOrderDeleteCommand;
  }

  @Bean
  public OrderDocumentDeleteCommand orderDocumentDeleteCommand(
      DObOrderDocumentRep orderDocumentRep) {
    OrderDocumentDeleteCommand purchaseOrderDeleteCommand = new OrderDocumentDeleteCommand();
    purchaseOrderDeleteCommand.setOrderDocumentRep(orderDocumentRep);
    return purchaseOrderDeleteCommand;
  }

  @Bean
  public PurchaseOrderMarkAsPIRequestedCommand purchaseOrderMarkAsPIRequestedCommand(
      DObPurchaseOrderRep purchaseOrderRep,
      IObOrderCycleDatesRep cycleDatesRep,
      IUserAccountSecurityService securityService) {
    PurchaseOrderMarkAsPIRequestedCommand purchaseOrderMarkAsPIRequestedCommand =
        new PurchaseOrderMarkAsPIRequestedCommand();
    purchaseOrderMarkAsPIRequestedCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderMarkAsPIRequestedCommand.setUserAccountSecurityService(securityService);
    purchaseOrderMarkAsPIRequestedCommand.setOrderCycleDatesRep(cycleDatesRep);
    return purchaseOrderMarkAsPIRequestedCommand;
  }

  @Bean
  public PurchaseOrderMarkAsShippedCommand purchaseOrderMarkAsShippedCommand(
      DObPurchaseOrderRep purchaseOrderRep,
      IObOrderCycleDatesRep cycleDatesRep,
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      IUserAccountSecurityService userAccountSecurityService) {
    PurchaseOrderMarkAsShippedCommand purchaseOrderMarkAsShippedCommand =
        new PurchaseOrderMarkAsShippedCommand();
    purchaseOrderMarkAsShippedCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderMarkAsShippedCommand.setOrderCycleDatesRep(cycleDatesRep);
    purchaseOrderMarkAsShippedCommand.setUserAccountSecurityService(userAccountSecurityService);
    return purchaseOrderMarkAsShippedCommand;
  }

  @Bean
  public PurchaseOrderDeleteItemCommand purchaseOrderDeleteItemCommand(
      IObOrderLineDetailsGeneralModelRep itemOrderGeneralModelRep,
      IObOrderLineDetailsRep itemOrderRep,
      DObOrderDocumentRep orderDocumentRep,
      IUserAccountSecurityService userAccountSecurityService) {
    PurchaseOrderDeleteItemCommand purchaseOrderDeleteItemCommand =
        new PurchaseOrderDeleteItemCommand();
    purchaseOrderDeleteItemCommand.setItemOrderGeneralModelRep(itemOrderGeneralModelRep);
    purchaseOrderDeleteItemCommand.setItemOrderRep(itemOrderRep);
    purchaseOrderDeleteItemCommand.setOrderDocumentRep(orderDocumentRep);
    purchaseOrderDeleteItemCommand.setUserAccountSecurityService(userAccountSecurityService);
    return purchaseOrderDeleteItemCommand;
  }

  @Bean
  public PurchaseOrderDeleteItemQuantityCommand purchaseOrderDeleteItemQuantityCommand(
      IObOrderLineDetailsQuantitiesRep orderLineDetailsQuantitiesRep,
      DObPurchaseOrderRep orderRep,
      IObOrderLineDetailsRep orderLineDetailsRep,
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep,
      PlatformTransactionManager transactionManager,
      IUserAccountSecurityService userAccountSecurityService) {
    PurchaseOrderDeleteItemQuantityCommand deleteItemQuantityCommand =
        new PurchaseOrderDeleteItemQuantityCommand();
    deleteItemQuantityCommand.setOrderLineDetailsQuantitiesRep(orderLineDetailsQuantitiesRep);
    deleteItemQuantityCommand.setOrderRep(orderRep);
    deleteItemQuantityCommand.setOrderLineDetailsRep(orderLineDetailsRep);
    deleteItemQuantityCommand.setOrderLineDetailsGeneralModelRep(orderLineDetailsGeneralModelRep);
    deleteItemQuantityCommand.setTransactionManager(transactionManager);
    deleteItemQuantityCommand.setUserAccountSecurityService(userAccountSecurityService);
    return deleteItemQuantityCommand;
  }

  @Bean
  public PurchaseOrderEditItemCommand purchaseOrderEditItemCommand(
      EntityLockCommand<DObOrderDocument> entityLockCommand) {
    PurchaseOrderEditItemCommand purchaseOrderEditItemCommand = new PurchaseOrderEditItemCommand();
    purchaseOrderEditItemCommand.setEntityLockCommand(entityLockCommand);
    return purchaseOrderEditItemCommand;
  }

  @Bean
  public PurchaseOrderCompanyEditCommand purchaseOrderCompanyEditCommand(
      DObPurchaseOrderRep purchaseOrderRep,
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      CObCompanyGeneralModelRep cObCompanyGeneralModelRep,
      IObPurchaseOrderBillToCompanyRep billToCompanyRep,
      IUserAccountSecurityService securityService) {
    PurchaseOrderCompanyEditCommand purchaseOrderCompanyEditCommand =
        new PurchaseOrderCompanyEditCommand();
    purchaseOrderCompanyEditCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderCompanyEditCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderCompanyEditCommand.setcObCompanyGeneralModelRep(cObCompanyGeneralModelRep);
    purchaseOrderCompanyEditCommand.setOrderCompanyRep(billToCompanyRep);
    purchaseOrderCompanyEditCommand.setUserAccountSecurityService(securityService);
    return purchaseOrderCompanyEditCommand;
  }

  @Bean("purchaseOrderTimeoutValidityPolicyMBean")
  public ObjectInstance createTimeoutValidityJMXBean(
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool) throws Exception {
    IConcurrentDataAccessManager concurrentDataAccessManager =
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObPurchaseOrder.class.getName());
    IEditTokenValidityPolicy validityStrategy = concurrentDataAccessManager.getValidityStrategy();
    TimeoutEditTokenValidityPolicy editTokenValidityPolicy =
        (TimeoutEditTokenValidityPolicy) validityStrategy;
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName(
            "com.ebs.dac.foundation.realization.concurrency.policies:type=TimeoutEditTokenValidityPolicy");
    ObjectInstance registerMBean = mbs.registerMBean(editTokenValidityPolicy, name);
    return registerMBean;
  }

  @Bean("purchaseOrderLockCommand")
  public EntityLockCommand<DObOrderDocument> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObOrderDocumentRep orderDocumentRep) {

    EntityLockCommand<DObOrderDocument> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObOrderDocument.class.getName()));
    entityLockCommand.setRepository(orderDocumentRep);
    return entityLockCommand;
  }

  @Bean
  public DObPurchaseOrderEditPaymentAndDeliveryCommand purchaseOrderPaymentAndDeliveryEditCommand(
      DObPurchaseOrderRep purchaseOrderRep,
      CObPaymentTermsRep cObPaymentTermsRep,
      IObOrderPaymentTermsDetailsRep iObOrderPaymentTermsDetailsRep,
      LObIncotermsRep lObIncotermsRep,
      IObOrderDeliveryDetailsRep iObOrderDeliveryDetailsRep,
      LObModeOfTransportRep lObModeOfTransportRep,
      CObShippingRep cObShippingRep,
      LObPortRep lObPortRep,
      CObCurrencyRep cObCurrencyRep,
      LObContainerRequirementRep lObContainerRequirementRep,
      IObOrderContainersDetailsRep iObOrderContainersDetailsRep,
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      IUserAccountSecurityService userAccountSecurityService) {

    DObPurchaseOrderEditPaymentAndDeliveryCommand purchaseOrderEditPaymentAndDeliveryCommand =
        new DObPurchaseOrderEditPaymentAndDeliveryCommand();
    purchaseOrderEditPaymentAndDeliveryCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setCobPaymentTermsRep(cObPaymentTermsRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderEditPaymentAndDeliveryCommand.setIObOrderPaymentTermsDetailsRep(
        iObOrderPaymentTermsDetailsRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setLObIncotermsRep(lObIncotermsRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setIObOrderDeliveryDetailsRep(
        iObOrderDeliveryDetailsRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setLObModeOfTransportRep(lObModeOfTransportRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setCObShippingRep(cObShippingRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setLObPortRep(lObPortRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setCObCurrencyRep(cObCurrencyRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setLObContainerRequirementRep(
        lObContainerRequirementRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setIObOrderContainersDetailsRep(
        iObOrderContainersDetailsRep);
    purchaseOrderEditPaymentAndDeliveryCommand.setUserAccountSecurityService(
        userAccountSecurityService);

    return purchaseOrderEditPaymentAndDeliveryCommand;
  }

  @Bean
  public DObPurchaseOrderEditConsigneeCommand creatPurchaseOrderEditConsigneeCommand(
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      DObPurchaseOrderRep purchaseOrderRep,
      IObOrderConsigneeEnterpriseDataRep iObOrderConsigneeEnterpriseDataRep,
      IObOrderPlantEnterpriseDataRep iObOrderPlantEnterpriseDataRep,
      IObOrderStorehouseEnterpriseDataRep iObOrderStorehouseEnterpriseDataRep,
      CObCompanyGeneralModelRep cObCompanyGeneralModelRep,
      CObPlantGeneralModelRep cObPlantGeneralModelRep,
      CObStorehouseGeneralModelRep cObStorehouseGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    DObPurchaseOrderEditConsigneeCommand purchaseOrderEditConsigneeCommand =
        new DObPurchaseOrderEditConsigneeCommand();
    purchaseOrderEditConsigneeCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderEditConsigneeCommand.setPurchaseOrderRep(purchaseOrderRep);

    purchaseOrderEditConsigneeCommand.setOrderConsigneeEnterpriseDataRep(
        iObOrderConsigneeEnterpriseDataRep);
    purchaseOrderEditConsigneeCommand.setOrderPlantEnterpriseDataRep(
        iObOrderPlantEnterpriseDataRep);
    purchaseOrderEditConsigneeCommand.setOrderStorehouseEnterpriseDataRep(
        iObOrderStorehouseEnterpriseDataRep);

    purchaseOrderEditConsigneeCommand.setCObCompanyGeneralModelRep(cObCompanyGeneralModelRep);
    purchaseOrderEditConsigneeCommand.setCObPlantGeneralModelRep(cObPlantGeneralModelRep);
    purchaseOrderEditConsigneeCommand.setCObStorehouseGeneralModelRep(cObStorehouseGeneralModelRep);
    purchaseOrderEditConsigneeCommand.setIUserAccountSecurityService(userAccountSecurityService);

    return purchaseOrderEditConsigneeCommand;
  }

  @Bean
  public IObOrderDocumentRequiredDocumentsUpdateCommand
      createPurchaseOrderEditRequiredDocumentsCommand(
          EntityLockCommand<DObOrderDocument> entityLockCommand,
          DObOrderDocumentRep purchaseOrderRep,
          IObOrderDocumentRequiredDocumentsGeneralModelRep orderRequiredDocumentsGeneralModelRep,
          LObAttachmentTypeRep attachmentTypeRep,
          IUserAccountSecurityService userAccountSecurityService) {
    IObOrderDocumentRequiredDocumentsUpdateCommand purchaseOrderEditRequiredDocumentsCommand =
        new IObOrderDocumentRequiredDocumentsUpdateCommand();
    purchaseOrderEditRequiredDocumentsCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderEditRequiredDocumentsCommand.setOrderDocumentRep(purchaseOrderRep);
    purchaseOrderEditRequiredDocumentsCommand.setOrderRequiredDocumentsGeneralModelRep(
        orderRequiredDocumentsGeneralModelRep);
    purchaseOrderEditRequiredDocumentsCommand.setAttachmentTypeRep(attachmentTypeRep);
    purchaseOrderEditRequiredDocumentsCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return purchaseOrderEditRequiredDocumentsCommand;
  }

  @Bean("purchaseOrderLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("purchaseOrderLockCommand") EntityLockCommand<DObOrderDocument> entityLockCommand)
      throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=purchaseOrderLockCommandJMXBean");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public DObPurchaseOrderUploadAttachmentCommand dObPurchaseOrderUploadAttachmentCommand(
      RObAttachmentRep robAttachmentRep,
      LObAttachmentTypeRep lObAttachmentTypeRep,
      DObPurchaseOrderRep orderRep,
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      IUserAccountSecurityService userAccountSecurityService) {
    DObPurchaseOrderUploadAttachmentCommand purchaseOrderUploadAttachmentCommand =
        new DObPurchaseOrderUploadAttachmentCommand();

    purchaseOrderUploadAttachmentCommand.setRobAttachmentRep(robAttachmentRep);
    purchaseOrderUploadAttachmentCommand.setlObAttachmentTypeRep(lObAttachmentTypeRep);
    purchaseOrderUploadAttachmentCommand.setOrderRep(orderRep);
    purchaseOrderUploadAttachmentCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderUploadAttachmentCommand.setUserAccountSecurityService(userAccountSecurityService);

    return purchaseOrderUploadAttachmentCommand;
  }

  @Bean
  public DObPurchaseOrderAssignMeToApproveCommand createPurchaseOrderAssignMeToApproveCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObPurchaseOrderRep orderRep,
      IObOrderApprovalCycleRep approvalCycleRep,
      IObOrderApproverRep approverRep,
      CObControlPointRep controlPointRep,
      PlatformTransactionManager transactionManager) {
    DObPurchaseOrderAssignMeToApproveCommand assignMeToApproveCommand =
        new DObPurchaseOrderAssignMeToApproveCommand();
    assignMeToApproveCommand.setUserAccountSecurityService(userAccountSecurityService);
    assignMeToApproveCommand.setPurchaseOrderRep(orderRep);
    assignMeToApproveCommand.setApprovalCycleRep(approvalCycleRep);
    assignMeToApproveCommand.setApproverRep(approverRep);
    assignMeToApproveCommand.setControlPointRep(controlPointRep);
    assignMeToApproveCommand.setTransactionManager(transactionManager);
    return assignMeToApproveCommand;
  }

  @Bean
  public DObPurchaseOrderApproveCommand createPurchaseOrderApproveCommand(
      IUserAccountSecurityService userAccountSecurityService,
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      DObPurchaseOrderRep orderRep,
      IObOrderApprovalCycleRep approvalCycleRep,
      IObOrderApproverRep approverRep,
      IObOrderLineDetailsSummaryGeneralModelRep orderLineDetailsSummaryGeneralModelRep,
      PlatformTransactionManager transactionManager,
      DObPurchaseOrderNewStateMachineFactory purchaseOrderFactory) {
    DObPurchaseOrderApproveCommand purchaseOrderApproveCommand =
        new DObPurchaseOrderApproveCommand();
    purchaseOrderApproveCommand.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderApproveCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderApproveCommand.setPurchaseOrderRep(orderRep);
    purchaseOrderApproveCommand.setApprovalCycleRep(approvalCycleRep);
    purchaseOrderApproveCommand.setApproverRep(approverRep);
    purchaseOrderApproveCommand.setOrderLineDetailsSummaryGeneralModelRep(
        orderLineDetailsSummaryGeneralModelRep);
    purchaseOrderApproveCommand.setTransactionManager(transactionManager);
    purchaseOrderApproveCommand.setPurchaseOrderFactory(purchaseOrderFactory);
    return purchaseOrderApproveCommand;
  }

  @Bean
  public DObPurchaseOrderRejectCommand createPurchaseOrderRejectCommand(
      IUserAccountSecurityService userAccountSecurityService,
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      DObPurchaseOrderRep orderRep,
      IObOrderApprovalCycleRep approvalCycleRep,
      IObOrderApproverRep approverRep,
      PlatformTransactionManager transactionManager,
      DObPurchaseOrderNewStateMachineFactory purchaseOrderFactory) {
    DObPurchaseOrderRejectCommand purchaseOrderRejectCommand = new DObPurchaseOrderRejectCommand();
    purchaseOrderRejectCommand.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderRejectCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderRejectCommand.setPurchaseOrderRep(orderRep);
    purchaseOrderRejectCommand.setApprovalCycleRep(approvalCycleRep);
    purchaseOrderRejectCommand.setApproverRep(approverRep);
    purchaseOrderRejectCommand.setTransactionManager(transactionManager);
    purchaseOrderRejectCommand.setPurchaseOrderFactory(purchaseOrderFactory);
    return purchaseOrderRejectCommand;
  }

  @Bean
  public PurchaseOrderMarkAsProductionFinishedCommand
      createPurchaseOrderMarkAsProductionFinishedCommand(
          IUserAccountSecurityService userAccountSecurityService,
          DObPurchaseOrderRep orderRep,
          IObOrderCycleDatesRep cycleDatesRep) {
    PurchaseOrderMarkAsProductionFinishedCommand purchaseOrderMarkAsProductionFinishedCommand =
        new PurchaseOrderMarkAsProductionFinishedCommand();
    purchaseOrderMarkAsProductionFinishedCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    purchaseOrderMarkAsProductionFinishedCommand.setPurchaseOrderRep(orderRep);
    purchaseOrderMarkAsProductionFinishedCommand.setOrderCycleDatesRep(cycleDatesRep);
    return purchaseOrderMarkAsProductionFinishedCommand;
  }

  @Bean
  public DObPurchaseOrderConfirmCommand createPurchaseOrderConfirmCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObPurchaseOrderRep orderRep,
      IObOrderCycleDatesRep orderCycleDatesRep,
      IObOrderItemSummaryGeneralModelRep orderSummaryGeneralModelRep,
      DObPurchaseOrderNewStateMachineFactory purchaseOrderFactory) {
    DObPurchaseOrderConfirmCommand purchaseOrderConfirmCommand =
        new DObPurchaseOrderConfirmCommand();
    purchaseOrderConfirmCommand.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderConfirmCommand.setPurchaseOrderRep(orderRep);
    purchaseOrderConfirmCommand.setOrderCycleDatesRep(orderCycleDatesRep);
    purchaseOrderConfirmCommand.setOrderSummaryGeneralModelRep(orderSummaryGeneralModelRep);
    purchaseOrderConfirmCommand.setPurchaseOrderFactory(purchaseOrderFactory);
    return purchaseOrderConfirmCommand;
  }

  @Bean
  public DObPurchaseOrderMarkAsArrivedCommand createPurchaseOrderArrivalCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObPurchaseOrderRep orderRep,
      IObOrderCycleDatesRep orderCycleDatesRep) {
    DObPurchaseOrderMarkAsArrivedCommand purchaseOrderArrivalCommand =
        new DObPurchaseOrderMarkAsArrivedCommand();
    purchaseOrderArrivalCommand.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderArrivalCommand.setPurchaseOrderRep(orderRep);
    purchaseOrderArrivalCommand.setOrderCycleDatesRep(orderCycleDatesRep);
    return purchaseOrderArrivalCommand;
  }

  @Bean
  public DObPurchaseOrderMarkAsClearedCommand createDObPurchaseOrderMarkAsClearedCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObPurchaseOrderRep orderRep,
      IObOrderCycleDatesRep orderCycleDatesRep) {
    DObPurchaseOrderMarkAsClearedCommand purchaseOrderClearCommand =
        new DObPurchaseOrderMarkAsClearedCommand();
    purchaseOrderClearCommand.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderClearCommand.setOrderRep(orderRep);
    purchaseOrderClearCommand.setOrderCycleDatesRep(orderCycleDatesRep);
    return purchaseOrderClearCommand;
  }

  @Bean
  public DObPurchaseOrderMarkAsDeliveryCompleteCommand
      createDObPurchaseOrderMarkAsDeliveryCompleteCommand(
          IUserAccountSecurityService userAccountSecurityService,
          DObPurchaseOrderRep orderRep,
          IObOrderCycleDatesRep orderCycleDatesRep) {
    DObPurchaseOrderMarkAsDeliveryCompleteCommand purchaseOrderMarkAsDeliveryCompleteCommand =
        new DObPurchaseOrderMarkAsDeliveryCompleteCommand();
    purchaseOrderMarkAsDeliveryCompleteCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    purchaseOrderMarkAsDeliveryCompleteCommand.setOrderRep(orderRep);
    purchaseOrderMarkAsDeliveryCompleteCommand.setOrderCycleDatesRep(orderCycleDatesRep);
    return purchaseOrderMarkAsDeliveryCompleteCommand;
  }

  @Bean
  public DObPurchaseOrderCancelCommand createDObPurchaseOrderCancelCommand(
      IUserAccountSecurityService userAccountSecurityService, DObPurchaseOrderRep orderRep) {
    DObPurchaseOrderCancelCommand purchaseOrderCancelCommand = new DObPurchaseOrderCancelCommand();
    purchaseOrderCancelCommand.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderCancelCommand.setPurchaseOrderRep(orderRep);
    return purchaseOrderCancelCommand;
  }

  @Bean
  public DObPurchaseOrderSubmitForApprovalCommand createDObPurchaseOrderSubmitForApprovalCommand(
      IUserAccountSecurityService userAccountSecurityService,
      CObApprovalPolicyRep approvalPolicyRep,
      IObOrderApprovalCycleRep approvalCycleRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep,
      ApprovalPolicyControlPointUsersGeneralModelRep approvalPolicyControlPointUsersRep,
      DObPurchaseOrderRep purchaseOrderRep) {
    DObPurchaseOrderSubmitForApprovalCommand submitForApprovalCommand =
        new DObPurchaseOrderSubmitForApprovalCommand();
    submitForApprovalCommand.setUserAccountSecurityService(userAccountSecurityService);
    submitForApprovalCommand.setApprovalPolicyRep(approvalPolicyRep);
    submitForApprovalCommand.setApprovalCycleRep(approvalCycleRep);
    submitForApprovalCommand.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    submitForApprovalCommand.setPurchaseOrderRep(purchaseOrderRep);
    submitForApprovalCommand.setApprovalPolicyControlPointUsersRep(
        approvalPolicyControlPointUsersRep);
    return submitForApprovalCommand;
  }

  @Bean
  public DObPurchaseOrderSaveItemCommand purchaseOrderSaveItemCommand(
      DObPurchaseOrderRep purchaseOrderRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      EntityLockCommand<DObOrderDocument> entityLockCommand,
      IUserAccountSecurityService userAccountSecurityService) {
    DObPurchaseOrderSaveItemCommand purchaseOrderSaveItemCommand =
        new DObPurchaseOrderSaveItemCommand();
    purchaseOrderSaveItemCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderSaveItemCommand.setItemGeneralModelRep(itemGeneralModelRep);
    purchaseOrderSaveItemCommand.setEntityLockCommand(entityLockCommand);
    purchaseOrderSaveItemCommand.setUserAccountSecurityService(userAccountSecurityService);
    return purchaseOrderSaveItemCommand;
  }

  @Bean
  public DObPurchaseOrderOpenForUpdatesCommand createDObPurchaseOrderOpenForUpdatesCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObPurchaseOrderRep orderRep,
      IObOrderApprovalCycleRep approvalCycleRep) {
    DObPurchaseOrderOpenForUpdatesCommand openForUpdatesCommand =
        new DObPurchaseOrderOpenForUpdatesCommand();
    openForUpdatesCommand.setUserAccountSecurityService(userAccountSecurityService);
    openForUpdatesCommand.setPurchaseOrderRep(orderRep);
    openForUpdatesCommand.setApprovalCycleRep(approvalCycleRep);
    return openForUpdatesCommand;
  }

  @Bean
  public DObPurchaseOrderSaveItemQtyInDnPlCommand saveItemQtyInDnPlCommand(
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep,
      IObOrderLineDetailsRep orderLineDetailsRep,
      DObPurchaseOrderRep orderRep,
      IUserAccountSecurityService userAccountSecurityService,
      PlatformTransactionManager transactionManager) {
    DObPurchaseOrderSaveItemQtyInDnPlCommand saveItemQtyInDnPlCommand =
        new DObPurchaseOrderSaveItemQtyInDnPlCommand();
    saveItemQtyInDnPlCommand.setOrderLineDetailsGeneralModelRep(orderLineDetailsGeneralModelRep);
    saveItemQtyInDnPlCommand.setOrderLineDetailsRep(orderLineDetailsRep);
    saveItemQtyInDnPlCommand.setOrderRep(orderRep);
    saveItemQtyInDnPlCommand.setUserAccountSecurityService(userAccountSecurityService);
    saveItemQtyInDnPlCommand.setTransactionManager(transactionManager);
    return saveItemQtyInDnPlCommand;
  }

  @Bean
  public DObPurchaseOrderSaveItemQuantityCommand saveItemQuantityCommand(
      IObOrderLineDetailsQuantitiesRep quantitiesRep,
      CObMeasureRep measureRep,
      IObOrderLineDetailsRep orderLineDetailsRep,
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep,
      DObPurchaseOrderRep orderRep,
      ItemVendorRecordUnitOfMeasuresGeneralModelRep itemVendorRecordUnitOfMeasuresGeneralModelRep,
      DObPurchaseOrderGeneralModelRep orderHeaderGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService,
      PlatformTransactionManager transactionManager,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {
    DObPurchaseOrderSaveItemQuantityCommand saveItemQuantityCommand =
        new DObPurchaseOrderSaveItemQuantityCommand();
    saveItemQuantityCommand.setUserAccountSecurityService(userAccountSecurityService);
    saveItemQuantityCommand.setTransactionManager(transactionManager);
    saveItemQuantityCommand.setQuantitiesRep(quantitiesRep);
    saveItemQuantityCommand.setMeasureRep(measureRep);
    saveItemQuantityCommand.setOrderLineDetailsRep(orderLineDetailsRep);
    saveItemQuantityCommand.setPurchaseOrderRep(orderRep);
    saveItemQuantityCommand.setOrderLineDetailsGeneralModelRep(orderLineDetailsGeneralModelRep);
    saveItemQuantityCommand.setItemVendorRecordUnitOfMeasuresGeneralModelRep(
        itemVendorRecordUnitOfMeasuresGeneralModelRep);
    saveItemQuantityCommand.setOrderHeaderGeneralModelRep(orderHeaderGeneralModelRep);
    saveItemQuantityCommand.setItemVendorRecordGeneralModelRep(itemVendorRecordGeneralModelRep);
    return saveItemQuantityCommand;
  }

  @Bean
  public DObPurchaseOrderPostGoodsReceiptCommand createPurchaseOrderPostGoodsReceipt(
      DObPurchaseOrderRep dObOrderRep,
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          iObGoodsReceiptPurchaseOrderDataGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    DObPurchaseOrderPostGoodsReceiptCommand dObPurchaseOrderPostGoodsReceiptCommand =
        new DObPurchaseOrderPostGoodsReceiptCommand();
    dObPurchaseOrderPostGoodsReceiptCommand.setdObOrderRep(dObOrderRep);
    dObPurchaseOrderPostGoodsReceiptCommand.setiObGoodsReceiptPurchaseOrderDataGeneralModelRep(
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep);
    dObPurchaseOrderPostGoodsReceiptCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return dObPurchaseOrderPostGoodsReceiptCommand;
  }

  @Bean
  public IObPurchaseOrderDeleteItemCommand createPurchaseOrderDeleteItemCommand(
      IObOrderItemRep iobOrderItemRep,
      DObOrderDocumentRep orderDocumentRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObPurchaseOrderDeleteItemCommand purchaseOrderDeleteItemCommand =
        new IObPurchaseOrderDeleteItemCommand();
    purchaseOrderDeleteItemCommand.setIObOrderItemRep(iobOrderItemRep);
    purchaseOrderDeleteItemCommand.setOrderDocumentRep(orderDocumentRep);
    purchaseOrderDeleteItemCommand.setUserAccountSecurityService(userAccountSecurityService);
    return purchaseOrderDeleteItemCommand;
  }

  @Bean
  public IObPurchaseOrderItemsSaveCommand createPurchaseOrderItemsSaveCommand(
      DObPurchaseOrderRep purchaseOrderRep,
      MObItemRep itemRep,
      CObMeasureRep measureRep,
      UserAccountSecurityService securityService) {
    IObPurchaseOrderItemsSaveCommand purchaseOrderItemsSaveCommand =
        new IObPurchaseOrderItemsSaveCommand();

    purchaseOrderItemsSaveCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderItemsSaveCommand.setItemRep(itemRep);
    purchaseOrderItemsSaveCommand.setMeasureRep(measureRep);
    purchaseOrderItemsSaveCommand.setUserAccountSecurityService(securityService);

    return purchaseOrderItemsSaveCommand;
  }

  @Bean
  public IObServicePurchaseOrderPaymentDetailsUpdateCommand
      createPurchaseOrderPaymentDetailsUpdateCommand(
          DObPurchaseOrderRep purchaseOrderRep,
          IObOrderPaymentDetailsRep paymentDetailsRep,
          CObPaymentTermsRep paymentTermsRep,
          CObCurrencyRep currencyRep,
          UserAccountSecurityService securityService) {
    IObServicePurchaseOrderPaymentDetailsUpdateCommand purchaseOrderPaymentDetailsUpdateCommand =
        new IObServicePurchaseOrderPaymentDetailsUpdateCommand();

    purchaseOrderPaymentDetailsUpdateCommand.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderPaymentDetailsUpdateCommand.setCurrencyRep(currencyRep);
    purchaseOrderPaymentDetailsUpdateCommand.setPaymentTermsRep(paymentTermsRep);
    purchaseOrderPaymentDetailsUpdateCommand.setPaymentDetailsRep(paymentDetailsRep);
    purchaseOrderPaymentDetailsUpdateCommand.setUserAccountSecurityService(securityService);

    return purchaseOrderPaymentDetailsUpdateCommand;
  }
}
