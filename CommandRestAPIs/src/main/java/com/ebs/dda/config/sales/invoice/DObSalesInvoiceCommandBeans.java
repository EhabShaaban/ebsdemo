package com.ebs.dda.config.sales.invoice;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.accounting.electronicinvoice.DObSalesInvoiceEInvoiceData;
import com.ebs.dda.accounting.electronicinvoice.DObSalesInvoiceEInvoiceDataFactory;
import com.ebs.dda.accounting.electronicinvoice.EInvoiceTaxAuthorityClient;
import com.ebs.dda.accounting.salesinvoice.editability.SalesInvoiceBusinessPartnerEditabilityManger;
import com.ebs.dda.accounting.salesinvoice.editability.SalesInvoiceItemEditabilityManger;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.electronicinvoice.EInvoiceSubmitCommand;
import com.ebs.dda.commands.accounting.invoice.IObInvoiceTaxCopyCommand;
import com.ebs.dda.commands.accounting.salesinvoice.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxes;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.*;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.collectionresponsible.CObCollectionResponsibleRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.order.salesorder.*;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.sql.DataSource;
import java.lang.management.ManagementFactory;

@Configuration
public class DObSalesInvoiceCommandBeans {

  @Bean
  public DObSalesInvoiceCreateCommand createSalesInvoiceCreateCommand(
      DObSalesInvoiceRep dObSalesInvoiceRep,
      CObSalesInvoiceRep cObSalesInvoiceRep,
      CObCollectionResponsibleRep cObCollectionResponsibleRep,
      MObCustomerRep mObCustomerRep,
      CObCompanyRep cObCompanyRep,
      CObPurchasingUnitRep purchasingUnitRep,
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep,
      IObSalesOrderTaxGeneralModelRep salesOrderTaxGeneralModelRep,
      DObSalesOrderGeneralModelRep dObSalesOrderGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService,
      IObSalesOrderDataRep iObSalesOrderDataRep,
      IObSalesOrderCompanyStoreDataRep iObSalesOrderCompanyRep,
      IObSalesOrderCompanyAndStoreDataGeneralModelRep iObSalesOrderCompanyGeneralModelRep,
      IObSalesOrderItemsRep salesOrderItemsRep,
      MObItemRep itemRep,
      IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep)
      throws Exception {
    DObSalesInvoiceCreateCommand command =
        new DObSalesInvoiceCreateCommand(documentObjectCodeGenerator);
    command.setcObSalesInvoiceRep(cObSalesInvoiceRep);
    command.setcObSalesInvoiceRep(cObSalesInvoiceRep);
    command.setPurchasingUnitRep(purchasingUnitRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setdObSalesInvoiceRep(dObSalesInvoiceRep);
    command.setcObCollectionResponsibleRep(cObCollectionResponsibleRep);
    command.setmObCustomerRep(mObCustomerRep);
    command.setcObCompanyRep(cObCompanyRep);
    command.setCompanyTaxesGeneralModelRep(companyTaxesGeneralModelRep);
    command.setSalesOrderTaxGeneralModelRep(salesOrderTaxGeneralModelRep);
    command.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setdObSalesOrderGeneralModelRep(dObSalesOrderGeneralModelRep);
    command.setiObSalesOrderDataRep(iObSalesOrderDataRep);
    command.setiObSalesOrderCompanyRep(iObSalesOrderCompanyRep);
    command.setiObSalesOrderCompanyGeneralModelRep(iObSalesOrderCompanyGeneralModelRep);
    command.setSalesOrderItemsRep(salesOrderItemsRep);
    command.setItemRep(itemRep);
    command.setAlternativeUoMGeneralModelRep(alternativeUoMGeneralModelRep);

    return command;
  }

  @Bean("dObSalesInvoiceLockCommand")
  public EntityLockCommand<DObSalesInvoice> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObSalesInvoiceRep salesInvoiceRep) {
    EntityLockCommand<DObSalesInvoice> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObSalesInvoice.class.getName()));
    entityLockCommand.setRepository(salesInvoiceRep);
    return entityLockCommand;
  }

  @Bean("SalesInvoiceLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("dObSalesInvoiceLockCommand") EntityLockCommand entityLockCommand)
      throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=SalesInvoiceEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public DObSalesInvoiceDeleteCommand createDObSalesInvoiceDeleteCommand(
      DObSalesInvoiceRep dObSalesInvoiceRep) {
    DObSalesInvoiceDeleteCommand salesInvoiceDeleteCommand = new DObSalesInvoiceDeleteCommand();
    salesInvoiceDeleteCommand.setdObSalesInvoiceRep(dObSalesInvoiceRep);
    return salesInvoiceDeleteCommand;
  }

  @Bean
  public SalesInvoiceBusinessPartnerEditabilityManger
      createSalesInvoiceBusinessPartnerEditabilityManger() {
    SalesInvoiceBusinessPartnerEditabilityManger salesInvoiceBusinessPartnerEditabilityManger =
        new SalesInvoiceBusinessPartnerEditabilityManger();
    return salesInvoiceBusinessPartnerEditabilityManger;
  }

  @Bean
  public SalesInvoiceItemEditabilityManger createSalesInvoiceItemEditabilityManger() {
    SalesInvoiceItemEditabilityManger salesInvoiceItemEditabilityManger =
        new SalesInvoiceItemEditabilityManger();
    return salesInvoiceItemEditabilityManger;
  }

  @Bean
  public DObSalesInvoicePostCommand createDObSalesInvoicePostCommand(
      DObSalesInvoiceRep salesInvoiceRep,
      DObSalesInvoicePostPreparationGeneralModelRep postPreparationGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService,
      IObSalesInvoiceCompanyDataRep salesInvoiceCompanyRep,
      CObFiscalYearRep cObFiscalYearRep,
      IObSalesInvoiceBusinessPartnerRep businessPartnerRep,
      DObSalesOrderRep salesOrderRep,
      DObCollectionGeneralModelRep collectionRep,
      IObNotesReceivablesReferenceDocumentGeneralModelRep notesReceivablesReferenceDocumentRep,
      IObSalesInvoiceSummaryRep summaryRep) {
    DObSalesInvoicePostCommand salesInvoicePostCommand = new DObSalesInvoicePostCommand();
    salesInvoicePostCommand.setdObSalesInvoiceRep(salesInvoiceRep);
    salesInvoicePostCommand.setPostPreparationGeneralModelRep(postPreparationGeneralModelRep);
    salesInvoicePostCommand.setUserAccountSecurityService(userAccountSecurityService);
    salesInvoicePostCommand.setcObFiscalYearRep(cObFiscalYearRep);
    salesInvoicePostCommand.setBusinessPartnerRep(businessPartnerRep);
    salesInvoicePostCommand.setSalesOrderRep(salesOrderRep);
    salesInvoicePostCommand.setCollectionRep(collectionRep);
    salesInvoicePostCommand.setNotesReceivablesReferenceDocumentRep(
        notesReceivablesReferenceDocumentRep);
    salesInvoicePostCommand.setSummaryRep(summaryRep);
    return salesInvoicePostCommand;
  }

  @Bean
  public DObSalesInvoiceMarkAsReadyForDeliveringCommand
      createDObSalesInvoiceReadyForDeliveringCommand(
          DObSalesInvoiceRep salesInvoiceRep,
          IUserAccountSecurityService userAccountSecurityService) {
    DObSalesInvoiceMarkAsReadyForDeliveringCommand salesInvoiceReadyForDeliveringCommand =
        new DObSalesInvoiceMarkAsReadyForDeliveringCommand();
    salesInvoiceReadyForDeliveringCommand.setdObSalesInvoiceRep(salesInvoiceRep);
    salesInvoiceReadyForDeliveringCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesInvoiceReadyForDeliveringCommand;
  }

  @Bean
  public IObSalesInvoiceBusinessPartnerSaveCommand createIObBusinessPartnerSaveCommand(
      IObSalesInvoiceBusinessPartnerRep salesInvoiceBusinessPartnerRep,
      DObSalesInvoiceRep salesInvoiceRep,
      CObPaymentTermsRep paymentTermRep,
      CObCurrencyRep currencyRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSalesInvoiceBusinessPartnerSaveCommand businessPartnerSaveCommand =
        new IObSalesInvoiceBusinessPartnerSaveCommand();
    businessPartnerSaveCommand.setSalesInvoiceBusinessPartnerRep(salesInvoiceBusinessPartnerRep);
    businessPartnerSaveCommand.setSalesInvoiceRep(salesInvoiceRep);
    businessPartnerSaveCommand.setPaymentTermRep(paymentTermRep);
    businessPartnerSaveCommand.setCurrencyRep(currencyRep);
    businessPartnerSaveCommand.setUserAccountSecurityService(userAccountSecurityService);

    return businessPartnerSaveCommand;
  }

  @Bean
  public DObSalesInvoiceItemSaveCommand salesInvoiceItemSaveCommand(
      DObSalesInvoiceRep salesInvoiceRep,
      MObItemGeneralModelRep mObItemGeneralModelRep,
      CObMeasureRep cObMeasureRep,
      IUserAccountSecurityService userAccountSecurityService) {
    DObSalesInvoiceItemSaveCommand invoiceItemSaveCommand = new DObSalesInvoiceItemSaveCommand();
    invoiceItemSaveCommand.setSalesInvoiceRep(salesInvoiceRep);
    invoiceItemSaveCommand.setcObMeasureRep(cObMeasureRep);
    invoiceItemSaveCommand.setmObItemGeneralModelRep(mObItemGeneralModelRep);
    invoiceItemSaveCommand.setUserAccountSecurityService(userAccountSecurityService);
    return invoiceItemSaveCommand;
  }

  @Bean
  public IObSalesInvoiceItemsDeleteCommand createSalesInvoiceDeleteItemCommand(
      IObSalesInvoiceItemsRep itemsRep,
      DObSalesInvoiceRep invoiceRep,
      UserAccountSecurityService userAccountSecurityService) {
    IObSalesInvoiceItemsDeleteCommand iObSalesInvoiceItemsDeleteCommand =
        new IObSalesInvoiceItemsDeleteCommand();
    iObSalesInvoiceItemsDeleteCommand.setSalesInvoiceItemsRep(itemsRep);
    iObSalesInvoiceItemsDeleteCommand.setSalesInvoiceRep(invoiceRep);
    iObSalesInvoiceItemsDeleteCommand.setUserAccountSecurityService(userAccountSecurityService);
    return iObSalesInvoiceItemsDeleteCommand;
  }

  @Bean
  public IObSalesInvoiceTaxesDeleteCommand createSalesInvoiceDeleteTaxCommand(
      IObSalesInvoiceTaxesRep salesInvoiceTaxesRep,
      DObSalesInvoiceRep salesInvoiceRep,
      IObSalesInvoiceTaxesGeneralModelRep salesInvoiceTaxesGeneralModelRep,
      UserAccountSecurityService userAccountSecurityService,
      IObSalesInvoiceSummaryRep summaryRep,
      EntityLockCommand<DObSalesInvoice> entityLockCommand) {
    IObSalesInvoiceTaxesDeleteCommand iObSalesInvoiceTaxesDeleteCommand =
        new IObSalesInvoiceTaxesDeleteCommand();
    iObSalesInvoiceTaxesDeleteCommand.setSalesInvoiceTaxesRep(salesInvoiceTaxesRep);
    iObSalesInvoiceTaxesDeleteCommand.setSalesInvoiceRep(salesInvoiceRep);
    iObSalesInvoiceTaxesDeleteCommand.setSalesInvoiceTaxesGeneralModelRep(
        salesInvoiceTaxesGeneralModelRep);
    iObSalesInvoiceTaxesDeleteCommand.setUserAccountSecurityService(userAccountSecurityService);
    iObSalesInvoiceTaxesDeleteCommand.setSummaryRep(summaryRep);
    iObSalesInvoiceTaxesDeleteCommand.setEntityLockCommand(entityLockCommand);
    return iObSalesInvoiceTaxesDeleteCommand;
  }

  @Bean
  public DObSalesInvoicePdfGeneratorCommand createSalesInvoiceGeneratePdfCommand(
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep, DataSource dataSource) {
    DObSalesInvoicePdfGeneratorCommand salesInvoicePdfGeneratorCommand =
        new DObSalesInvoicePdfGeneratorCommand();
    salesInvoicePdfGeneratorCommand.setSalesInvoiceItemsGeneralModelRep(
        salesInvoiceItemsGeneralModelRep);
    salesInvoicePdfGeneratorCommand.setDataSource(dataSource);
    return salesInvoicePdfGeneratorCommand;
  }

  @Bean
  public DObSalesInvoiceUpdateStateToClosedCommand createDObSalesInvoiceUpdateStateToClosedCommand(
      DObSalesInvoiceRep salesInvoiceRep,
      IObInvoiceSummaryGeneralModelRep salesInvoiceItemSummaryGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    DObSalesInvoiceUpdateStateToClosedCommand salesInvoiceUpdateStateToClosedCommand =
        new DObSalesInvoiceUpdateStateToClosedCommand();
    salesInvoiceUpdateStateToClosedCommand.setSalesInvoiceRep(salesInvoiceRep);
    salesInvoiceUpdateStateToClosedCommand.setSalesInvoiceItemSummaryGeneralModelRep(
        salesInvoiceItemSummaryGeneralModelRep);
    salesInvoiceUpdateStateToClosedCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return salesInvoiceUpdateStateToClosedCommand;
  }

  @Bean
  public IObSalesInvoiceTaxesUpdateCommand createIObSalesInvoiceTaxesUpdateCommand(
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep,
      IObSalesInvoiceTaxesRep taxesRep,
      IObSalesInvoiceTaxesGeneralModelRep taxesGeneralModelRep,
      UserAccountSecurityService userAccountSecurityService) {
    IObSalesInvoiceTaxesUpdateCommand taxesUpdateCommand = new IObSalesInvoiceTaxesUpdateCommand();
    taxesUpdateCommand.setSalesInvoiceItemsGeneralModelRep(salesInvoiceItemsGeneralModelRep);
    taxesUpdateCommand.setTaxesRep(taxesRep);
    taxesUpdateCommand.setTaxesGeneralModelRep(taxesGeneralModelRep);
    taxesUpdateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return taxesUpdateCommand;
  }

  @Bean
  public IObInvoiceTaxCopyCommand<DObSalesInvoice, IObSalesInvoiceTaxes>
      createIObSalesInvoiceTaxCopyCommand(
          IObSalesInvoiceTaxesRep salesInvoiceTaxesRep, LObCompanyTaxesRep taxesInfoRep) {

    IObInvoiceTaxCopyCommand<DObSalesInvoice, IObSalesInvoiceTaxes> command =
        new IObInvoiceTaxCopyCommand<>();
    command.setInvoiceTaxesRep(salesInvoiceTaxesRep);
    command.setTaxesInfoRep(taxesInfoRep);

    return command;
  }

  @Bean
  public IObSalesInvoiceUpdateReturnedItemsCommand salesInvoiceUpdateReturnedItemsCommand(
      UserAccountSecurityService userAccountSecurityService,
      IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemRep,
      DObSalesReturnOrderGeneralModelRep salesReturnOrderRep,
      IObSalesInvoiceItemsRep salesInvoiceItemsRep,
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep) {
    IObSalesInvoiceUpdateReturnedItemsCommand updateReturnedItemsCommand =
        new IObSalesInvoiceUpdateReturnedItemsCommand();
    updateReturnedItemsCommand.setUserAccountSecurityService(userAccountSecurityService);
    updateReturnedItemsCommand.setSalesReturnOrderItemRep(salesReturnOrderItemRep);
    updateReturnedItemsCommand.setSalesReturnOrderRep(salesReturnOrderRep);
    updateReturnedItemsCommand.setSalesInvoiceItemsRep(salesInvoiceItemsRep);
    updateReturnedItemsCommand.setSalesInvoiceItemsGeneralModelRep(
        salesInvoiceItemsGeneralModelRep);
    return updateReturnedItemsCommand;
  }

  @Bean
  public IObSalesInvoiceUpdateRemainingCommand createUpdateRemainingCommand(
      DObCollectionGeneralModelRep collectionGMRep,
      DObSalesInvoiceRep salesInvoiceRep,
      IObSalesInvoiceSummaryRep summaryRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSalesInvoiceUpdateRemainingCommand command = new IObSalesInvoiceUpdateRemainingCommand();

    command.setCollectionGMRep(collectionGMRep);
    command.setSalesInvoiceRep(salesInvoiceRep);
    command.setSummaryRep(summaryRep);
    command.setUserAccountSecurityService(userAccountSecurityService);

    return command;
  }

  @Bean
  public DObSalesInvoiceAccountDeterminationCommand
      createDObSalesInvoiceAccountDeterminationCommand(
          DObSalesInvoiceRep salesInvoiceRep,
          DObSalesInvoiceJournalEntryPreparationGeneralModelRep salesInvoicePreparationRep,
          IObSalesInvoiceJournalEntryItemsPreparationGeneralModelRep
              salesInvoiceItemsPreparationRep,
          IUserAccountSecurityService userAccountSecurityService) {
    DObSalesInvoiceAccountDeterminationCommand command =
        new DObSalesInvoiceAccountDeterminationCommand();
    command.setSalesInvoiceRep(salesInvoiceRep);
    command.setSalesInvoicePreparationRep(salesInvoicePreparationRep);
    command.setSalesInvoiceItemsPreparationRep(salesInvoiceItemsPreparationRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    return command;
  }


  @Bean
  public EInvoiceSubmitCommand<DObSalesInvoiceEInvoiceData> createDObSalesInvoiceSubmitEInvoiceCommand(
      DObSalesInvoiceEInvoiceDataGeneralModelRep eInvoiceDataGMRep,
      DObSalesInvoiceItemsEInvoiceDataGeneralModelRep invoiceItemsEInvoiceDataGMRep,
      IObSalesInvoiceTaxesGeneralModelRep invoiceTaxesGMRep
  ) {

    DObSalesInvoiceEInvoiceDataFactory factory = new DObSalesInvoiceEInvoiceDataFactory();
    factory.setEInvoiceDataGMRep(eInvoiceDataGMRep);
    factory.setInvoiceItemsEInvoiceDataGMRep(invoiceItemsEInvoiceDataGMRep);
    factory.setInvoiceTaxesGMRep(invoiceTaxesGMRep);

    EInvoiceSubmitCommand<DObSalesInvoiceEInvoiceData> command = new EInvoiceSubmitCommand<>(
        DObSalesInvoiceEInvoiceData.class);
    command.setEInvoiceDataFactory(factory);
    command.setTaxAuthorityClient(new EInvoiceTaxAuthorityClient());

    return command;
  }
}
