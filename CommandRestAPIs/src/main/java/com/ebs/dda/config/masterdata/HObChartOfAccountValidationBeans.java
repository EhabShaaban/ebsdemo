package com.ebs.dda.config.masterdata;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigRep;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.validation.chartofaccounts.HObChartOfAccountCreationValidator;
import com.ebs.dda.validation.chartofaccounts.HObChartOfAccountDeleteValidator;

@Configuration
public class HObChartOfAccountValidationBeans {

	@Bean
	public HObChartOfAccountCreationValidator chartOfAccountCreationValidator(
					HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep,
					LObGlobalAccountConfigRep globalAccountConfigRep) {
		HObChartOfAccountCreationValidator chartOfAccountCreationValidator = new HObChartOfAccountCreationValidator();
		chartOfAccountCreationValidator
						.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
		chartOfAccountCreationValidator.setGlobalAccountConfigRep(globalAccountConfigRep);
		return chartOfAccountCreationValidator;
	}

	@Bean
	public HObChartOfAccountDeleteValidator chartOfAccountDeleteValidator(
					HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep,
					IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep) {
		HObChartOfAccountDeleteValidator validator = new HObChartOfAccountDeleteValidator();
		validator.setGlAccountsGeneralModelRep(glAccountsGeneralModelRep);
		validator.setJournalEntryItemsGeneralModelRep(journalEntryItemsGeneralModelRep);
		return validator;
	}

}
