package com.ebs.dda.config.inventory.inventorydocument;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dda.commands.inventory.inventorydocument.DObInventoryDocumentActivateCommand;
import com.ebs.dda.repositories.inventory.DObInventoryDocumentRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObInventoryDocumentCommandBeans {
    @Bean
    public DObInventoryDocumentActivateCommand createInventoryDocumentPostCommand(
            IUserAccountSecurityService userAccountSecurityService,
            DObInventoryDocumentRep dObInventoryDocumentRep,
            TransactionManagerDelegate transactionManagerDelegate) {
        DObInventoryDocumentActivateCommand command = new DObInventoryDocumentActivateCommand();
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setDObInventoryDocumentRep(dObInventoryDocumentRep);
        return command;
    }
}
