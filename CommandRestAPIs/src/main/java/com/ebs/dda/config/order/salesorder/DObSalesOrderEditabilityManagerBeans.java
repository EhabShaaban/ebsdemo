package com.ebs.dda.config.order.salesorder;

import com.ebs.dda.order.salesorder.IObSalesOrderCompanyStoreEditabilityManager;
import com.ebs.dda.order.salesorder.IObSalesOrderDataEditabilityManager;
import com.ebs.dda.order.salesorder.IObSalesOrderItemsEditabilityManger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObSalesOrderEditabilityManagerBeans {
  @Bean
  public IObSalesOrderCompanyStoreEditabilityManager
      createSalesOrderCompanyStoreEditabilityManager() {
    IObSalesOrderCompanyStoreEditabilityManager salesOrderCompanyStoreEditabilityManager =
        new IObSalesOrderCompanyStoreEditabilityManager();
    return salesOrderCompanyStoreEditabilityManager;
  }

  @Bean
  public IObSalesOrderDataEditabilityManager createSalesOrderDataEditabilityManager() {
    IObSalesOrderDataEditabilityManager salesOrderDataEditabilityManager =
        new IObSalesOrderDataEditabilityManager();
    return salesOrderDataEditabilityManager;
  }
  @Bean
  public IObSalesOrderItemsEditabilityManger createSalesOrderItemsEditabilityManager() {
    IObSalesOrderItemsEditabilityManger salesOrderItemsEditabilityManger =
            new IObSalesOrderItemsEditabilityManger();
    return salesOrderItemsEditabilityManger;
  }
}
