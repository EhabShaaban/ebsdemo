package com.ebs.dda.config.accounting.landedcost;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.landedcost.factories.DObLandedCostUpdateActualCostValueObjectByPaymentFactory;
import com.ebs.dda.accounting.landedcost.factories.DObLandedCostUpdateActualCostValueObjectByServiceInvoceFactory;
import com.ebs.dda.accounting.landedcost.factories.DObLandedCostUpdateActualCostValueObjectFactory;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.landedcost.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.*;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class DObLandedCostCommandBeans {

  @Bean
  public DObLandedCostDeleteCommand createDObLandedCostDeleteCommand(
      DObLandedCostRep landedCostRep) {
    DObLandedCostDeleteCommand landedCostDeleteCommand = new DObLandedCostDeleteCommand();
    landedCostDeleteCommand.setLandedCostRep(landedCostRep);
    return landedCostDeleteCommand;
  }

  @Bean("dObLandedCostLockCommand")
  public EntityLockCommand<DObLandedCost> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObLandedCostRep landedCostRep) {
    EntityLockCommand<DObLandedCost> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObLandedCost.class.getName()));
    entityLockCommand.setRepository(landedCostRep);
    return entityLockCommand;
  }

  @Bean("LandedCostLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("dObLandedCostLockCommand") EntityLockCommand entityLockCommand) throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=LandedCostEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public DObLandedCostCreationCommand initCreationCommand(
      DocumentObjectCodeGenerator codeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      CObLandedCostTypeRep landedCostTypeRep,
      DObLandedCostRep landedCostRep,
      IObOrderCompanyGeneralModelRep poCompanyGMRep,
      CObCompanyRep companyRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderGMRep,
      CObPurchasingUnitRep purchasingUnitRep,
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep) {

    DObLandedCostCreationCommand creationCommand = new DObLandedCostCreationCommand();

    creationCommand.setCodeGenerator(codeGenerator);
    creationCommand.setUserAccountSecurityService(userAccountSecurityService);
    creationCommand.setLandedCostTypeRep(landedCostTypeRep);
    creationCommand.setLandedCostRep(landedCostRep);
    creationCommand.setPoCompanyGMRep(poCompanyGMRep);
    creationCommand.setCompanyRep(companyRep);
    creationCommand.setPurchaseOrderGMRep(purchaseOrderGMRep);
    creationCommand.setPurchasingUnitRep(purchasingUnitRep);
    creationCommand.setVendorInvoiceGeneralModelRep(vendorInvoiceGeneralModelRep);
    return creationCommand;
  }

  @Bean
  public DObLandedCostConvertToEstimateCommand convertToEstimateCommand(
      IUserAccountSecurityService userAccountSecurityService, DObLandedCostRep landedCostRep) {

    DObLandedCostConvertToEstimateCommand convertToEstimateCommand =
        new DObLandedCostConvertToEstimateCommand();

    convertToEstimateCommand.setUserAccountSecurityService(userAccountSecurityService);
    convertToEstimateCommand.setLandedCostRep(landedCostRep);
    return convertToEstimateCommand;
  }

  @Bean
  public DObLandedCostConvertToActualCommand convertToActualCommand(
      IUserAccountSecurityService userAccountSecurityService, DObLandedCostRep landedCostRep) {

    DObLandedCostConvertToActualCommand convertToActualCommand =
        new DObLandedCostConvertToActualCommand();

    convertToActualCommand.setUserAccountSecurityService(userAccountSecurityService);
    convertToActualCommand.setLandedCostRep(landedCostRep);
    return convertToActualCommand;
  }

  @Bean
  public IObLandedCostFactorItemSaveCommand landedCostFactorItemSaveCommand(
      DObLandedCostRep landedCostRep,
      MObItemGeneralModelRep mObItemGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObLandedCostFactorItemSaveCommand landedCostFactorItemSaveCommand =
        new IObLandedCostFactorItemSaveCommand();
    landedCostFactorItemSaveCommand.setLandedCostRep(landedCostRep);
    landedCostFactorItemSaveCommand.setmObItemGeneralModelRep(mObItemGeneralModelRep);
    landedCostFactorItemSaveCommand.setUserAccountSecurityService(userAccountSecurityService);
    return landedCostFactorItemSaveCommand;
  }

  @Bean
  public IObLandedCostFactorItemDeleteCommand createLandedCostFactorItemDeleteCommand(
      IObLandedCostFactorItemsRep itemsRep,
      IUserAccountSecurityService userAccountSecurityService,
      DObLandedCostRep landedCostRep) {
    IObLandedCostFactorItemDeleteCommand landedCostFactorItemDeleteCommand =
        new IObLandedCostFactorItemDeleteCommand();
    landedCostFactorItemDeleteCommand.setItemsRep(itemsRep);
    landedCostFactorItemDeleteCommand.setLandedCostRep(landedCostRep);
    landedCostFactorItemDeleteCommand.setUserAccountSecurityService(userAccountSecurityService);
    return landedCostFactorItemDeleteCommand;
  }

  @Bean
  public IObLandedCostUpdateGoodsInvoiceCommand createILandedCostUpdateGoodsInvoiceCommand(
      IObVendorInvoiceDetailsGeneralModelRep vendorInvoiceRep,
      IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep,
      IObLandedCostDetailsRep landedCostDetailsRep,
      IUserAccountSecurityService userAccountSecurityService,
      DObLandedCostRep landedCostRep) {
    IObLandedCostUpdateGoodsInvoiceCommand UpdateGoodsInvoiceCommand =
        new IObLandedCostUpdateGoodsInvoiceCommand();

    UpdateGoodsInvoiceCommand.setVendorInvoiceRep(vendorInvoiceRep);
    UpdateGoodsInvoiceCommand.setLandedCostDetailsGeneralModelRep(landedCostDetailsGeneralModelRep);
    UpdateGoodsInvoiceCommand.setLandedCostDetailsRep(landedCostDetailsRep);
    UpdateGoodsInvoiceCommand.setLandedCostRep(landedCostRep);
    UpdateGoodsInvoiceCommand.setUserAccountSecurityService(userAccountSecurityService);

    return UpdateGoodsInvoiceCommand;
  }

  @Bean
  public DObLandedCostUpdateActualCostValueObjectFactory
      createDObLandedCostUpdateActualCostValueObjectFactory(
          IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
          DObPurchaseOrderGeneralModelRep purchaseOrderRep,
          DObVendorInvoiceGeneralModelRep serviceInvoiceRep,
          IObVendorInvoiceItemsGeneralModelRep serviceInvoiceItemsRep) {

    DObLandedCostUpdateActualCostValueObjectByPaymentFactory paymentFactory =
        new DObLandedCostUpdateActualCostValueObjectByPaymentFactory(
            paymentDetailsRep, purchaseOrderRep);

    DObLandedCostUpdateActualCostValueObjectByServiceInvoceFactory serviceInvoiceFactory =
        new DObLandedCostUpdateActualCostValueObjectByServiceInvoceFactory(
            serviceInvoiceRep, serviceInvoiceItemsRep, purchaseOrderRep);

    DObLandedCostUpdateActualCostValueObjectFactory factory =
        new DObLandedCostUpdateActualCostValueObjectFactory(paymentFactory, serviceInvoiceFactory);
    return factory;
  }
}
