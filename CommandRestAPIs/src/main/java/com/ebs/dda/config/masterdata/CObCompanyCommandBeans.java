package com.ebs.dda.config.masterdata;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.masterdata.company.CObCompanyLogoCreationCommand;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyLogoDetailsRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CObCompanyCommandBeans {

  @Bean
  public CObCompanyLogoCreationCommand createCompanyLogoCreateCommand(
      IUserAccountSecurityService userAccountSecurityService,
      CObCompanyRep companyRep,
      IObCompanyLogoDetailsRep companyLogoDetailsRep) {
    CObCompanyLogoCreationCommand command = new CObCompanyLogoCreationCommand();
    command.setIUserAccountSecurityService(userAccountSecurityService);
    command.setCObCompanyRep(companyRep);
    command.setIObCompanyLogoDetailsRep(companyLogoDetailsRep);
    return command;
  }
}
