package com.ebs.dda.config.accounting.journalentry;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.accounting.journalentry.create.paymentrequest.*;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.DObPaymentRequestJournalEntryRep;
import com.ebs.dda.repositories.accounting.paymentrequest.*;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObLeafInChartOfAccountsRep;

public class PaymentRequestJournalEntryCreateCommandFactory {

  DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep;
  DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep;
  DObPaymentRequestRep paymentRequestRep;
  IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep;
  IObPaymentRequestAccountDetailsGeneralModelRep paymentRequestAccountingDetailsGeneralModelRep;
  IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep;
  IUserAccountSecurityService userAccountSecurityService;
  IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
      paymentRequestPaymentDetailsRep;
  HObLeafInChartOfAccountsRep leafInChartOfAccountsRep;
  IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestPaymentDetailsGeneralModelRep;
  IObAccountingDocumentActivationDetailsGeneralModelRep
          accountingDocumentActivationDetailsGeneralModelRep;

  private String paymentForm;
  private String paymentType;
  private String dueDocumentType;

  public DObJournalEntryCreatePaymentRequestCommand
      getPaymentRequestCreateJournalEntryCommandInstance(String paymentRequestCode)
          throws Exception {
    initializeTypeVariables(paymentRequestCode);
    return getInstanceBasedOnType();
  }

  private DObJournalEntryCreatePaymentRequestCommand getInstanceBasedOnType() throws Exception {
    if (isBankVendorInvoicePayment()) {
      return getBankVendorInvoiceCommand();
    }
    if (isBankGeneralPayment()) {
      return getBankGeneralCommand();
    }

    if (isCashGeneralPayment()) {
      return getCashGeneralCommand();
    }

    if (isCashVendorInvoicePayment()) {
      return getCashVendorInvoiceCommand();
    }

    if (isBankVendorDownpaymentPayment()) {
      return getBankVendorDownpaymentCommand();
    }

    if (isCashVendorDownPaymentPayment()) {
      return getCashVendorDownPaymentCommand();
    }

    throw new IllegalAccessException("Not A Valid Type");
  }

  private void setJournalEntryCommonParameters(
      DObJournalEntryCreatePaymentRequestCommand journalEntryCreatePaymentRequestCommand) {
    journalEntryCreatePaymentRequestCommand.setPaymentRequestJournalEntryRep(
        paymentRequestJournalEntryRep);
    journalEntryCreatePaymentRequestCommand.setPaymentRequestRep(paymentRequestRep);
    journalEntryCreatePaymentRequestCommand.setPaymentRequestCompanyDataRep(
        paymentRequestCompanyDataRep);
    journalEntryCreatePaymentRequestCommand.setPaymentRequestPostingDetailsRep(
        paymentRequestPostingDetailsRep);
    journalEntryCreatePaymentRequestCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    journalEntryCreatePaymentRequestCommand.setPaymentRequestPaymentDetailsRep(
        paymentRequestPaymentDetailsRep);
    journalEntryCreatePaymentRequestCommand.setPaymentRequestAccountingDetailsGeneralModelRep(
        paymentRequestAccountingDetailsGeneralModelRep);
    journalEntryCreatePaymentRequestCommand.setPaymentRequestPaymentDetailsGeneralModelRep(
        paymentRequestPaymentDetailsGeneralModelRep);
    journalEntryCreatePaymentRequestCommand.setAccountingDocumentActivationDetailsGeneralModelRep(
            accountingDocumentActivationDetailsGeneralModelRep);
  }

  private DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand
      getCashVendorDownPaymentCommand() {
    DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand
        paymentRequestCashVendorDownPaymentCommand =
            new DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand();
    setJournalEntryCommonParameters(paymentRequestCashVendorDownPaymentCommand);
    paymentRequestCashVendorDownPaymentCommand.setLeafInChartOfAccountsRep(
        leafInChartOfAccountsRep);
    return paymentRequestCashVendorDownPaymentCommand;
  }

  private DObJournalEntryCreatePaymentRequestBankVendorDownPaymentCommand
      getBankVendorDownpaymentCommand() {
    DObJournalEntryCreatePaymentRequestBankVendorDownPaymentCommand
        paymentRequestBankVendorDownpaymentCommand =
            new DObJournalEntryCreatePaymentRequestBankVendorDownPaymentCommand();
    setJournalEntryCommonParameters(paymentRequestBankVendorDownpaymentCommand);
    paymentRequestBankVendorDownpaymentCommand.setLeafInChartOfAccountsRep(
        leafInChartOfAccountsRep);
    return paymentRequestBankVendorDownpaymentCommand;
  }

  private DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand
      getCashVendorInvoiceCommand() {
    DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand paymentRequestCashVendorCommand =
        new DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand();
    setJournalEntryCommonParameters(paymentRequestCashVendorCommand);
    return paymentRequestCashVendorCommand;
  }

  private DObJournalEntryCreatePaymentRequestCashGeneralCommand getCashGeneralCommand() {
    DObJournalEntryCreatePaymentRequestCashGeneralCommand paymentRequestCashGeneralCommand =
        new DObJournalEntryCreatePaymentRequestCashGeneralCommand();
    setJournalEntryCommonParameters(paymentRequestCashGeneralCommand);
    paymentRequestCashGeneralCommand.setLeafInChartOfAccountsRep(leafInChartOfAccountsRep);
    return paymentRequestCashGeneralCommand;
  }

  private DObJournalEntryCreatePaymentRequestBankGeneralCommand getBankGeneralCommand() {
    DObJournalEntryCreatePaymentRequestBankGeneralCommand paymentRequestBankGeneralCommand =
        new DObJournalEntryCreatePaymentRequestBankGeneralCommand();
    setJournalEntryCommonParameters(paymentRequestBankGeneralCommand);
    paymentRequestBankGeneralCommand.setLeafInChartOfAccountsRep(leafInChartOfAccountsRep);
    return paymentRequestBankGeneralCommand;
  }

  private DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand
      getBankVendorInvoiceCommand() {
    DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand paymentRequestBankVendorCommand =
        new DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand();
    setJournalEntryCommonParameters(paymentRequestBankVendorCommand);
    return paymentRequestBankVendorCommand;
  }

  private boolean isBankGeneralPayment() {
    return paymentForm.equals(PaymentFormEnum.PaymentForm.BANK.name())
        && (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.name()));
  }

  private boolean isBankVendorInvoicePayment() {
    return paymentForm.equals(PaymentFormEnum.PaymentForm.BANK.name())
        && paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
        && dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE.name());
  }

  private boolean isCashVendorInvoicePayment() {
    return paymentForm.equals(PaymentFormEnum.PaymentForm.CASH.name())
        && paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
        && dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.INVOICE.name());
  }

  private boolean isCashGeneralPayment() {
    return paymentForm.equals(PaymentFormEnum.PaymentForm.CASH.name())
        && (paymentType.equals(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE.name()));
  }

  private boolean isBankVendorDownpaymentPayment() {
    return paymentForm.equals(PaymentFormEnum.PaymentForm.BANK.name())
        && paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
        && dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name());
  }

  private boolean isCashVendorDownPaymentPayment() {
    return paymentForm.equals(PaymentFormEnum.PaymentForm.CASH.name())
        && paymentType.equals(PaymentTypeEnum.PaymentType.VENDOR.name())
        && dueDocumentType.equals(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER.name());
  }

  private void initializeTypeVariables(String paymentRequestCode) {
    DObPaymentRequestGeneralModel paymentRequestGeneralModel =
        paymentRequestGeneralModelRep.findOneByUserCode(paymentRequestCode);
    IObPaymentRequestPaymentDetailsGeneralModel paymentDetailsGeneralModel =
        paymentRequestPaymentDetailsGeneralModelRep.findOneByUserCode(paymentRequestCode);
    paymentForm = paymentRequestGeneralModel.getPaymentForm();
    paymentType = paymentRequestGeneralModel.getPaymentType();
    dueDocumentType = paymentDetailsGeneralModel.getDueDocumentType();
  }

  public void setPaymentRequestJournalEntryRep(
      DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep) {
    this.paymentRequestJournalEntryRep = paymentRequestJournalEntryRep;
  }

  public void setPaymentRequestGeneralModelRep(
      DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep) {
    this.paymentRequestGeneralModelRep = paymentRequestGeneralModelRep;
  }

  public void setPaymentRequestAccountingDetailsGeneralModelRep(
      IObPaymentRequestAccountDetailsGeneralModelRep
          paymentRequestAccountingDetailsGeneralModelRep) {
    this.paymentRequestAccountingDetailsGeneralModelRep =
        paymentRequestAccountingDetailsGeneralModelRep;
  }

  public void setPaymentRequestPostingDetailsRep(
      IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep) {
    this.paymentRequestPostingDetailsRep = paymentRequestPostingDetailsRep;
  }

  public void setUserAccountSecurityService(
      IUserAccountSecurityService userAccountSecurityService) {
    this.userAccountSecurityService = userAccountSecurityService;
  }

  public void setPaymentRequestPaymentDetailsRep(
      IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
          paymentRequestPaymentDetailsRep) {
    this.paymentRequestPaymentDetailsRep = paymentRequestPaymentDetailsRep;
  }

  public void setLeafInChartOfAccountsRep(HObLeafInChartOfAccountsRep leafInChartOfAccountsRep) {
    this.leafInChartOfAccountsRep = leafInChartOfAccountsRep;
  }

  public void setPaymentRequestPaymentDetailsGeneralModelRep(
      IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestPaymentDetailsGeneralModelRep) {
    this.paymentRequestPaymentDetailsGeneralModelRep = paymentRequestPaymentDetailsGeneralModelRep;
  }

  public void setPaymentRequestRep(DObPaymentRequestRep paymentRequestRep) {
    this.paymentRequestRep = paymentRequestRep;
  }

  public void setPaymentRequestCompanyDataRep(
      IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep) {
    this.paymentRequestCompanyDataRep = paymentRequestCompanyDataRep;
  }
  public void setAccountingDocumentActivationDetailsGeneralModelRep(IObAccountingDocumentActivationDetailsGeneralModelRep accountingDocumentActivationDetailsGeneralModelRep){
    this.accountingDocumentActivationDetailsGeneralModelRep = accountingDocumentActivationDetailsGeneralModelRep;
  }
}
