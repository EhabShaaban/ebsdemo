package com.ebs.dda.config.accounting.journalentry;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.journalentry.create.DObJournalEntryCreateInintialBalanceUploadCommand;
import com.ebs.dda.commands.accounting.journalentry.create.DObJournalEntryCreateSettlementCommand;
import com.ebs.dda.commands.accounting.journalentry.create.collection.DObJournalEntryItemsCollectionCreateCommand;
import com.ebs.dda.commands.accounting.journalentry.create.paymentrequest.DObJournalEntrySettlePaymentRealizedExchangeRateCommand;
import com.ebs.dda.commands.accounting.journalentry.create.salesinvoice.DObJournalEntryCreateSalesInvoiceCommand;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetails;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetails;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionAccountingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionCompanyDetailsRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsRep;
import com.ebs.dda.repositories.accounting.initialbalanceupload.DObInitialBalanceUploadJournalEntryItemPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.initialbalanceupload.DObInitialBalanceUploadJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.*;
import com.ebs.dda.repositories.accounting.paymentrequest.*;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceAccountingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObLeafInChartOfAccountsRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.enterprise.IObEnterpriseBasicDataRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObJournalEntryCommandBeans {

  @Bean
  public PaymentRequestJournalEntryCreateCommandFactory
      createPaymentRequestJournalEntryCreateCommandFactory(
          DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep,
          DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep,
          IObPaymentRequestAccountDetailsGeneralModelRep
              paymentRequestAccountingDetailsGeneralModelRep,
          IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
          IUserAccountSecurityService userAccountSecurityService,
          IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
              paymentRequestPaymentDetailsRep,
          HObLeafInChartOfAccountsRep leafInChartOfAccountsRep,
          IObPaymentRequestPaymentDetailsGeneralModelRep
              paymentRequestPaymentDetailsGeneralModelRep,
          DObPaymentRequestRep paymentRequestRep,
          IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep,
          IObAccountingDocumentActivationDetailsGeneralModelRep
              accountingDocumentActivationDetailsGeneralModelRep) {
    PaymentRequestJournalEntryCreateCommandFactory paymentRequestJournalEntryCreateCommandFactory =
        new PaymentRequestJournalEntryCreateCommandFactory();
    paymentRequestJournalEntryCreateCommandFactory.setPaymentRequestJournalEntryRep(
        paymentRequestJournalEntryRep);
    paymentRequestJournalEntryCreateCommandFactory.setPaymentRequestGeneralModelRep(
        paymentRequestGeneralModelRep);
    paymentRequestJournalEntryCreateCommandFactory.setPaymentRequestPostingDetailsRep(
        paymentRequestPostingDetailsRep);
    paymentRequestJournalEntryCreateCommandFactory.setUserAccountSecurityService(
        userAccountSecurityService);
    paymentRequestJournalEntryCreateCommandFactory.setPaymentRequestPaymentDetailsRep(
        paymentRequestPaymentDetailsRep);
    paymentRequestJournalEntryCreateCommandFactory
        .setPaymentRequestAccountingDetailsGeneralModelRep(
            paymentRequestAccountingDetailsGeneralModelRep);
    paymentRequestJournalEntryCreateCommandFactory.setLeafInChartOfAccountsRep(
        leafInChartOfAccountsRep);
    paymentRequestJournalEntryCreateCommandFactory.setPaymentRequestPaymentDetailsGeneralModelRep(
        paymentRequestPaymentDetailsGeneralModelRep);
    paymentRequestJournalEntryCreateCommandFactory.setPaymentRequestRep(paymentRequestRep);
    paymentRequestJournalEntryCreateCommandFactory.setPaymentRequestCompanyDataRep(
        paymentRequestCompanyDataRep);
    paymentRequestJournalEntryCreateCommandFactory
        .setAccountingDocumentActivationDetailsGeneralModelRep(
            accountingDocumentActivationDetailsGeneralModelRep);
    return paymentRequestJournalEntryCreateCommandFactory;
  }

  @Bean
  public DObJournalEntryCreateSalesInvoiceCommand createDObJournalEntryCreateSalesInvoiceCommand(
          IUserAccountSecurityService userAccountSecurityService,
          DocumentObjectCodeGenerator documentObjectCodeGenerator,
          DObSalesInvoiceJournalEntryPreparationGeneralModelRep
          salesInvoiceJournalEntryPreparationGeneralModelRep,
          DObSalesInvoiceJournalEntryRep salesInvoiceJournalEntryRep,
          IObSalesInvoiceAccountingDetailsGeneralModelRep salesInvoiceAccountingDetailsGeneralModelRep) {

    DObJournalEntryCreateSalesInvoiceCommand salesInvoiceCommand =
        new DObJournalEntryCreateSalesInvoiceCommand(documentObjectCodeGenerator);

    salesInvoiceCommand.setUserAccountSecurityService(userAccountSecurityService);
    salesInvoiceCommand.setSalesInvoiceJournalEntryPreparationGeneralModelRep(
        salesInvoiceJournalEntryPreparationGeneralModelRep);
    salesInvoiceCommand.setSalesInvoiceJournalEntryRep(salesInvoiceJournalEntryRep);
    salesInvoiceCommand.setAccountingDetailsGeneralModelRep(salesInvoiceAccountingDetailsGeneralModelRep);
    return salesInvoiceCommand;
  }

  @Bean
  public DObJournalEntryItemsCollectionCreateCommand
      createJournalEntryCollectionNotesReceivableCreateCommand(
          IUserAccountSecurityService userAccountSecurityService,
          DocumentObjectCodeGenerator documentObjectCodeGenerator,
          IObAccountingDocumentActivationDetailsGeneralModelRep
              dObCollectionPostingDetailsGeneralModelRep,
          DObCollectionGeneralModelRep dObCollectionGeneralModelRep,
          CObCompanyGeneralModelRep companyGeneralModelRep,
          CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
          DObCollectionJournalEntryRep collectionJournalEntryRep,
          IObCollectionAccountingDetailsGeneralModelRep collectionAccountingDetailsGeneralModelRep,
          LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
          IObCollectionCompanyDetailsRep collectionCompanyDetailsRep,
          IObEnterpriseBasicDataRep companyBasicDataRep,
          IObCollectionDetailsRep<IObCollectionDetails> collectionDetailsRep) {
    DObJournalEntryItemsCollectionCreateCommand collectionCommand =
        new DObJournalEntryItemsCollectionCreateCommand();

    collectionCommand.setUserAccountSecurityService(userAccountSecurityService);
    collectionCommand.setCompanyGeneralModelRep(companyGeneralModelRep);
    collectionCommand.setAccountingDocumentActivationDetailsGeneralModelRep(
        dObCollectionPostingDetailsGeneralModelRep);
    collectionCommand.setdObCollectionGeneralModelRep(dObCollectionGeneralModelRep);
    collectionCommand.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    collectionCommand.setCollectionJournalEntryRep(collectionJournalEntryRep);
    collectionCommand.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
    collectionCommand.setCollectionAccountingDetailsGeneralModelRep(
        collectionAccountingDetailsGeneralModelRep);
    collectionCommand.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
    collectionCommand.setCollectionCompanyDetailsRep(collectionCompanyDetailsRep);
    collectionCommand.setCompanyBasicDataRep(companyBasicDataRep);
    collectionCommand.setCollectionDetailsRep(collectionDetailsRep);
    return collectionCommand;
  }

  @Bean
  public DObJournalEntryCreateSettlementCommand createSettlementJournalEntryCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObSettlementJournalEntryRep journalEntryRep,
      DObSettlementJournalEntryPreparationGeneralModelRep journalEntryPreparationGeneralModelRep,
      IObSettlementAccountDetailsGeneralModelRep accountDetailsGeneralModelRep) {

    DObJournalEntryCreateSettlementCommand journalEntryCreateSettlementCommand =
        new DObJournalEntryCreateSettlementCommand();
    journalEntryCreateSettlementCommand.setUserAccountSecurityService(userAccountSecurityService);
    journalEntryCreateSettlementCommand.setJournalEntryRep(journalEntryRep);
    journalEntryCreateSettlementCommand.setJournalEntryPreparationGeneralModelRep(
        journalEntryPreparationGeneralModelRep);
    journalEntryCreateSettlementCommand.setAccountDetailsGeneralModelRep(
        accountDetailsGeneralModelRep);
    return journalEntryCreateSettlementCommand;
  }

  @Bean
  public DObJournalEntryCreateInintialBalanceUploadCommand
      createInintialBalanceUploadJournalEntryCommand(
          DocumentObjectCodeGenerator codeGenerator,
          IUserAccountSecurityService userAccountSecurityService,
          DObInitialBalanceUploadJournalEntryRep journalEntryRep,
          DObInitialBalanceUploadJournalEntryPreparationGeneralModelRep
              journalEntryPreparationGeneralModelRep,
          DObInitialBalanceUploadJournalEntryItemPreparationGeneralModelRep
              journalEntryItemPreparationGeneralModelRep) {

    DObJournalEntryCreateInintialBalanceUploadCommand
        journalEntryCreateInitialBalanceUploadCommand =
            new DObJournalEntryCreateInintialBalanceUploadCommand(codeGenerator);
    journalEntryCreateInitialBalanceUploadCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    journalEntryCreateInitialBalanceUploadCommand.setJournalEntryRep(journalEntryRep);
    journalEntryCreateInitialBalanceUploadCommand.setJournalEntryPreparationGeneralModelRep(
        journalEntryPreparationGeneralModelRep);
    journalEntryCreateInitialBalanceUploadCommand.setJournalEntryItemPreparationGeneralModelRep(
        journalEntryItemPreparationGeneralModelRep);
    return journalEntryCreateInitialBalanceUploadCommand;
  }

  @Bean
  public DObJournalEntrySettlePaymentRealizedExchangeRateCommand
      createJournalEntrySettlePaymentRealizedExchangeRateCommand(
          DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModelRep
              exchangeRateDifferenceGeneralModelRep,
          LObGlobalAccountConfigGeneralModelRep accountConfigGeneralModelRep,
          DObPaymentRequestJournalEntryRep journalEntryRep,
          IUserAccountSecurityService userAccountSecurityService) {
    DObJournalEntrySettlePaymentRealizedExchangeRateCommand command =
        new DObJournalEntrySettlePaymentRealizedExchangeRateCommand();
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setAccountConfigGeneralModelRep(accountConfigGeneralModelRep);
    command.setExchangeRateDifferenceGeneralModelRep(exchangeRateDifferenceGeneralModelRep);
    command.setJournalEntryRep(journalEntryRep);
    return command;
  }
}
