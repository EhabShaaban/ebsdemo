package com.ebs.dda.config.accounting.costing;

import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.commands.accounting.costing.DObCostingActivateCommand;
import com.ebs.dda.commands.accounting.costing.DObCostingClosingJournalCreateCommand;
import com.ebs.dda.commands.accounting.costing.DObCostingSetActivationDetailsCommand;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.inventory.goodsreceipt.CreateCostingClosingJournalEntry;
import com.ebs.dda.functions.executecommand.inventory.goodsreceipt.SetCostingActivationDetails;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateSchema;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.repositories.accounting.costing.DObCostingGeneralModelRep;
import com.ebs.dda.rest.accounting.costing.activation.ActivateCosting;
import com.ebs.dda.validation.costing.DObCostingActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CostingFunctionBeans {
  @Bean("CostingActivate")
  public ActivateCosting activateCosting(
      AuthorizationManager authorizationManager,
      SerializionUtils serializionUtils,
      SchemaValidator schemaValidator,
      DObCostingGeneralModelRep costingGeneralModelRep,
      DObCostingActivateValidator validator,
      TransactionManagerDelegate transactionManagerDelegate,
      DObCostingActivateCommand activateCommand,
      DObCostingClosingJournalCreateCommand costingClosingJournalCreateCommand,
      DObCostingSetActivationDetailsCommand costingSetActivationDetailsCommand) {

    return context -> {
      new ActivateDocument()
          .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
          .validateSchema(new ValidateSchema(schemaValidator))
          .findEntitiesOfInterest(new FindEntityByUserCode<>(costingGeneralModelRep))
          .deserializeValueObject(
              new DeserializeValueObject<DObCostingActivateValueObject>(serializionUtils))
          .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObCostingStateMachine()))
          .validateBusinessRules(new ValidateBusinessRules<>(validator))
          .transactionManager(transactionManagerDelegate)
          .executeCommand(new ExecuteCommand<>(activateCommand))
          .executeActivateDependencies(
              new CreateCostingClosingJournalEntry(costingClosingJournalCreateCommand),
              new SetCostingActivationDetails(costingSetActivationDetailsCommand))
          .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
          .activate(context);
      return context.serializeResponse();
    };
  }
}
