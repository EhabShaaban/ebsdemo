package com.ebs.dda.config.accounting.collection;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.collection.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetails;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.*;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceCompanyDataRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.customer.MObEmployeeRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class DObCollectionCommandBeans {
  @Bean
  public DObCollectionCreateCommand createCommandCollection(
      IUserAccountSecurityService userAccountSecurityService,
      DocumentObjectCodeGenerator documentObjectCodeGenerator, DObCollectionRep collectionRep,
      CObPurchasingUnitRep businessUnitRep,
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
      IObSalesInvoiceBusinessPartnerRep salesInvoiceBusinessPartnerRep,
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
      CObCurrencyRep currencyRep, CObCompanyRep companyRep,
      IObSalesInvoiceCompanyDataRep salesInvoiceCompanyRep, MObCustomerRep customerRep,
      DObSalesOrderGeneralModelRep salesOrderGeneralModelRep,
      IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep,
      CObCollectionTypeRep collectionTypeRep,
      DObAccountingNoteGeneralModelRep accountingNoteGeneralModelRep, MObVendorRep vendorRep,
      DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep,
      IObPaymentRequestCompanyDataGeneralModelRep paymentRequestCompanyDataGeneralModelRep,
      MObEmployeeRep employeeRep) throws Exception {
    DObCollectionCreateCommand collectionCreateCommand =
        new DObCollectionCreateCommand(documentObjectCodeGenerator);
    collectionCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    collectionCreateCommand.setCollectionRep(collectionRep);
    collectionCreateCommand.setBusinessUnitRep(businessUnitRep);
    collectionCreateCommand.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    collectionCreateCommand.setSalesInvoiceCompanyRep(salesInvoiceCompanyRep);
    collectionCreateCommand.setSalesInvoiceBusinessPartnerRep(salesInvoiceBusinessPartnerRep);
    collectionCreateCommand.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    collectionCreateCommand.setCurrencyRep(currencyRep);
    collectionCreateCommand.setCompanyRep(companyRep);
    collectionCreateCommand.setCustomerRep(customerRep);
    collectionCreateCommand.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    collectionCreateCommand.setSalesOrderDataGeneralModelRep(salesOrderDataGeneralModelRep);
    collectionCreateCommand.setCollectionTypeRep(collectionTypeRep);
    collectionCreateCommand.setAccountingNoteGeneralModelRep(accountingNoteGeneralModelRep);
    collectionCreateCommand.setVendorRep(vendorRep);
    collectionCreateCommand.setPaymentRequestGeneralModelRep(paymentRequestGeneralModelRep);
    collectionCreateCommand
        .setPaymentRequestCompanyDataGeneralModelRep(paymentRequestCompanyDataGeneralModelRep);
    collectionCreateCommand.setEmployeeRep(employeeRep);
    return collectionCreateCommand;
  }

  @Bean
  public DObCollectionActivateCommand createActivateCollectionCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep,
      CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep,
      DObCollectionRep<DObCollection> collectionRep, CObFiscalYearRep fiscalYearRep) {
    DObCollectionActivateCommand collectionActivateCommand = new DObCollectionActivateCommand();
    collectionActivateCommand.setUserAccountSecurityService(userAccountSecurityService);
    collectionActivateCommand.setCollectionDetailsGeneralModelRep(collectionDetailsGeneralModelRep);
    collectionActivateCommand.setCollectionRep(collectionRep);
    collectionActivateCommand.setExchangeRateGeneralModelRep(exchangeRateGeneralModelRep);
    collectionActivateCommand.setFiscalYearRep(fiscalYearRep);

    return collectionActivateCommand;
  }

  @Bean
  public DObCollectionDeleteCommand createDeleteCollectionCommand(
      DObCollectionRep<DObCollection> collectionRep) throws Exception {
    DObCollectionDeleteCommand collectionDeleteCommand = new DObCollectionDeleteCommand();
    collectionDeleteCommand.setCollectionRep(collectionRep);

    return collectionDeleteCommand;
  }

  @Bean("dObCollectionLockCommand")
  public EntityLockCommand<DObCollection> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObCollectionRep collectionRep) {
    EntityLockCommand<DObCollection> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(concurrentDataAccessManagersPool
        .getConcurrentDataAccessManager(DObCollection.class.getName()));
    entityLockCommand.setRepository(collectionRep);
    return entityLockCommand;
  }

  @Bean
  public IObCollectionSaveCollectionDetailsCommand createIObCollectionSaveCollectionDetailsCommand(
      DObCollectionRep collectionRep, DObCollectionGeneralModelRep collectionGeneralModelRep,
      IObCollectionDetailsRep<IObCollectionDetails> collectionDetailsRep,
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep,
      CObTreasuryRep treasuryRep, IUserAccountSecurityService userAccountSecurityService) {
    IObCollectionSaveCollectionDetailsCommand collectionDetailsCommand =
        new IObCollectionSaveCollectionDetailsCommand();
    collectionDetailsCommand.setCollectionRep(collectionRep);
    collectionDetailsCommand.setCollectionGeneralModelRep(collectionGeneralModelRep);
    collectionDetailsCommand.setCollectionDetailsRep(collectionDetailsRep);
    collectionDetailsCommand
        .setCompanyBankDetailsGeneralModelRep(companyBankDetailsGeneralModelRep);
    collectionDetailsCommand.setTreasuryRep(treasuryRep);
    collectionDetailsCommand.setUserAccountSecurityService(userAccountSecurityService);
    return collectionDetailsCommand;
  }

  @Bean
  public DObCollectionSalesInvoiceAndOrderAccountDeterminationCommand createCollectionSalesOrderAccountDeterminationCommand(
      IUserAccountSecurityService userAccountSecurityService, DObCollectionRep collectionRep,
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep,
      MObCustomerGeneralModelRep customerGeneralModelRep, CObTreasuryRep treasuryRep,
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep,
      IObCollectionCompanyDetailsGeneralModelRep collectionCompanyDetailsGeneralModelRep,
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) throws Exception {

    DObCollectionSalesInvoiceAndOrderAccountDeterminationCommand command =
        new DObCollectionSalesInvoiceAndOrderAccountDeterminationCommand();

    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setCollectionDetailsGeneralModelRep(collectionDetailsGeneralModelRep);
    command.setCustomerGeneralModelRep(customerGeneralModelRep);
    command.setTreasuryRep(treasuryRep);
    command.setCollectionCompanyDetailsGeneralModelRep(collectionCompanyDetailsGeneralModelRep);
    command.setCompanyBankDetailsGeneralModelRep(companyBankDetailsGeneralModelRep);
    command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
    command.setCollectionRep(collectionRep);

    return command;
  }

  @Bean
  public DObCollectionDebitNoteAccountDeterminationCommand createCollectionAccountingNoteAccountDeterminationCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep,
      MObCustomerGeneralModelRep customerGeneralModelRep, CObTreasuryRep treasuryRep,
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep,
      IObCollectionCompanyDetailsGeneralModelRep collectionCompanyDetailsGeneralModelRep,
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
      DObCollectionRep collectionRep,
      IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoRep,
      HObChartOfAccountRep chartOfAccountRep) throws Exception {

    DObCollectionDebitNoteAccountDeterminationCommand command =
        new DObCollectionDebitNoteAccountDeterminationCommand();

    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setCollectionDetailsGeneralModelRep(collectionDetailsGeneralModelRep);
    command.setCustomerGeneralModelRep(customerGeneralModelRep);
    command.setTreasuryRep(treasuryRep);
    command.setCollectionCompanyDetailsGeneralModelRep(collectionCompanyDetailsGeneralModelRep);
    command.setCompanyBankDetailsGeneralModelRep(companyBankDetailsGeneralModelRep);
    command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
    command.setCollectionRep(collectionRep);
    command.setVendorAccountingInfoRep(vendorAccountingInfoRep);
    command.setChartOfAccountRep(chartOfAccountRep);

    return command;
  }

  @Bean
  public DObCollectionMonetaryNoteAccountDeterminationCommand createCollectionMonetaryNoteAccountDeterminationCommand(
      IUserAccountSecurityService userAccountSecurityService, DObCollectionRep collectionRep,
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep,
      MObCustomerGeneralModelRep customerGeneralModelRep,
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
      CObTreasuryRep treasuryRep,
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep,
      IObCollectionCompanyDetailsGeneralModelRep collectionCompanyDetailsGeneralModelRep,
      LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) throws Exception {

    DObCollectionMonetaryNoteAccountDeterminationCommand command =
        new DObCollectionMonetaryNoteAccountDeterminationCommand();

    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setCollectionDetailsGeneralModelRep(collectionDetailsGeneralModelRep);
    command.setCustomerGeneralModelRep(customerGeneralModelRep);
    command.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    command.setTreasuryRep(treasuryRep);
    command.setCollectionCompanyDetailsGeneralModelRep(collectionCompanyDetailsGeneralModelRep);
    command.setCompanyBankDetailsGeneralModelRep(companyBankDetailsGeneralModelRep);
    command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
    command.setCollectionRep(collectionRep);
    return command;
  }

  @Bean("DObCollectionLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("dObCollectionLockCommand") EntityLockCommand entityLockCommand) throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=CollectionEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }
}
