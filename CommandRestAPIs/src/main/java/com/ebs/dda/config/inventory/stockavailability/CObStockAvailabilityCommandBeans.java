package com.ebs.dda.config.inventory.stockavailability;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.stockavailability.*;
import com.ebs.dda.commands.inventory.stockavailability.factory.UpdateStockAvailabilityStrategyFactory;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsIssueStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByGoodsReceiptStrategy;
import com.ebs.dda.commands.inventory.stockavailability.strategies.UpdateStockAvailabilityByInitialStockUploadStrategy;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModelRep;
import com.ebs.dda.repositories.inventory.initialstockupload.IObInitialStockUploadItemQuantityGeneralModelRep;
import com.ebs.dda.repositories.inventory.stockavailability.CObStockAvailabilityRep;
import org.springframework.context.annotation.Bean;

public class CObStockAvailabilityCommandBeans {
  @Bean("stockAvailabilityLockCommand")
  public EntityLockCommand<CObStockAvailability> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      CObStockAvailabilityRep stockAvailabilityRep) {

    EntityLockCommand<CObStockAvailability> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            CObStockAvailability.class.getName()));
    entityLockCommand.setRepository(stockAvailabilityRep);
    return entityLockCommand;
  }

  @Bean("UpdateStockAvailabilityByGoodsReceiptStrategy")
  public UpdateStockAvailabilityByGoodsReceiptStrategy
      createUpdateStockAvailabilityByGoodsReceiptStrategy(
          IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantitiesGeneralModelRep,
          UpdateStockAvailabilityService cObStockAvailabilityManipulation) {
    UpdateStockAvailabilityByGoodsReceiptStrategy updateStockAvailabilityStrategy =
        new UpdateStockAvailabilityByGoodsReceiptStrategy();
    updateStockAvailabilityStrategy.setGoodsReceiptItemQuantityGeneralModelRep(
        goodsReceiptItemQuantitiesGeneralModelRep);
    updateStockAvailabilityStrategy.setStockAvailabilityUpdateService(
        cObStockAvailabilityManipulation);
    return updateStockAvailabilityStrategy;
  }

  @Bean("UpdateStockAvailabilityByGoodsIssueStrategy")
  public UpdateStockAvailabilityByGoodsIssueStrategy
  createUpdateStockAvailabilityByGoodsIssueStrategy(
          IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep,
          UpdateStockAvailabilityService stockAvailabilityUpdateService) {
    UpdateStockAvailabilityByGoodsIssueStrategy updateStockAvailabilityStrategy =
        new UpdateStockAvailabilityByGoodsIssueStrategy();
    updateStockAvailabilityStrategy.setGoodsIssueItemQuantityGeneralModelRep(
        goodsIssueItemQuantityGeneralModelRep);
    updateStockAvailabilityStrategy.setStockAvailabilityUpdateService(
        stockAvailabilityUpdateService);
    return updateStockAvailabilityStrategy;
  }

  @Bean("UpdateStockAvailabilityByInitialStockUploadStrategy")
  public UpdateStockAvailabilityByInitialStockUploadStrategy
  createUpdateStockAvailabilityByInitialStockUploadStrategy(
          IObInitialStockUploadItemQuantityGeneralModelRep
                  initialStockUploadItemQuantityGeneralModelRep,
          UpdateStockAvailabilityService stockAvailabilityUpdateService) {
    UpdateStockAvailabilityByInitialStockUploadStrategy updateStockAvailabilityStrategy =
            new UpdateStockAvailabilityByInitialStockUploadStrategy();
    updateStockAvailabilityStrategy.setInitialStockUploadItemQuantityGeneralModelRep(
            initialStockUploadItemQuantityGeneralModelRep);
    updateStockAvailabilityStrategy.setStockAvailabilityUpdateService(
            stockAvailabilityUpdateService);
    return updateStockAvailabilityStrategy;
  }

  @Bean("UpdateStockAvailabilityStrategyFactory")
  public UpdateStockAvailabilityStrategyFactory createUpdateStockAvailabilityStrategyFactory(
      UpdateStockAvailabilityByGoodsReceiptStrategy byGoodsReceiptStrategy,
      UpdateStockAvailabilityByGoodsIssueStrategy byGoodsIssueStrategy,
      UpdateStockAvailabilityByInitialStockUploadStrategy byInitialStockUploadStrategy) {
    UpdateStockAvailabilityStrategyFactory updateStockAvailabilityStrategyFactory =
        new UpdateStockAvailabilityStrategyFactory();
    updateStockAvailabilityStrategyFactory.setByGoodsIssueStrategy(byGoodsIssueStrategy);
    updateStockAvailabilityStrategyFactory.setByGoodsReceiptStrategy(byGoodsReceiptStrategy);
    updateStockAvailabilityStrategyFactory.setByInitialStockUploadStrategy(byInitialStockUploadStrategy);
    return updateStockAvailabilityStrategyFactory;
  }

  @Bean("UpdateStockAvailabilityService")
  public UpdateStockAvailabilityService createUpdateStockAvailabilityService(
      CObStockAvailabilityRep stockAvailabilityRep,
      IUserAccountSecurityService userAccountSecurityService,
      StockAvailabilityCodeGenerator codeGenerator) {
    UpdateStockAvailabilityService updateStockAvailabilityService =
        new UpdateStockAvailabilityService();
    updateStockAvailabilityService.setStockAvailabilityRep(stockAvailabilityRep);
    updateStockAvailabilityService.setCodeGenerator(codeGenerator);
    updateStockAvailabilityService.setUserAccountSecurityService(
        userAccountSecurityService);
    return updateStockAvailabilityService;
  }

  @Bean("CObStockAvailabilityUpdateCommand")
  public CObStockAvailabilityUpdateCommand createCObStockAvailabilityUpdateCommand(
          UpdateStockAvailabilityStrategyFactory updateFactory) {
    CObStockAvailabilityUpdateCommand cObStockAvailabilityUpdateCommand =
            new CObStockAvailabilityUpdateCommand();
   cObStockAvailabilityUpdateCommand.setStockAvailabilityUpdateFactory(updateFactory);
    return cObStockAvailabilityUpdateCommand;
  }
}
