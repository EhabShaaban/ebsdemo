package com.ebs.dda.config.inventory.initialstockupload;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.inventory.initialstockupload.DObInitialStockUploadCreateCommand;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.repositories.inventory.initialstockupload.DObInitialStockUploadRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.ivr.ItemVendorRecordRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObInitialStockUploadCommandBeans {

  @Bean
  public DObInitialStockUploadCreateCommand createCommandInitialStockUpload(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      CObPurchasingUnitRep purchasingUnitRep,
      CObStorehouseGeneralModelRep storehouseRep,
      CObCompanyRep companyRep,
      CObPlantGeneralModelRep plantRep,
      DObInitialStockUploadRep initialStockUploadRep,
      StoreKeeperRep storeKeeperRep,
      ItemVendorRecordRep itemVendorRecordRep)
      throws Exception {

    DObInitialStockUploadCreateCommand initialStockUploadCreateCommand =
        new DObInitialStockUploadCreateCommand(documentObjectCodeGenerator);
    initialStockUploadCreateCommand.setPurchasingUnitRep(purchasingUnitRep);
    initialStockUploadCreateCommand.setStorehouseRep(storehouseRep);
    initialStockUploadCreateCommand.setStoreKeeperRep(storeKeeperRep);
    initialStockUploadCreateCommand.setCompanyRep(companyRep);
    initialStockUploadCreateCommand.setPlantRep(plantRep);
    initialStockUploadCreateCommand.setItemVendorRecordRep(itemVendorRecordRep);
    initialStockUploadCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    initialStockUploadCreateCommand.setInitialStockUploadRep(initialStockUploadRep);
    return initialStockUploadCreateCommand;
  }
}
