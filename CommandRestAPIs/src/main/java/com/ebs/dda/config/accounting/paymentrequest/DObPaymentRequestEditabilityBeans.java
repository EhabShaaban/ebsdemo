package com.ebs.dda.config.accounting.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.editability.IObPaymentRequestPaymentDetailsEditabilityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObPaymentRequestEditabilityBeans {
    @Bean
    public IObPaymentRequestPaymentDetailsEditabilityManager createPaymentRequestPaymentDetailsEditabilityManager (){
        IObPaymentRequestPaymentDetailsEditabilityManager paymentRequestPaymentDetailsEditabilityManager =
                new IObPaymentRequestPaymentDetailsEditabilityManager();
        return paymentRequestPaymentDetailsEditabilityManager;
    }

}
