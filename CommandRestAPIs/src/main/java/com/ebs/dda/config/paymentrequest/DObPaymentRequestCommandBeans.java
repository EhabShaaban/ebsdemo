package com.ebs.dda.config.paymentrequest;

import java.lang.management.ManagementFactory;
import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

import com.ebs.dda.commands.accounting.paymentrequest.*;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObPaymentRequestActivatePreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.accounting.landedcost.DObLandedCostUpdateActualCostCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsRep;
import com.ebs.dda.repositories.accounting.paymentrequest.*;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObEmployeeRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;

@Configuration
public class DObPaymentRequestCommandBeans {
  @Bean("paymentRequestLockCommand")
  public EntityLockCommand<DObPaymentRequest> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObPaymentRequestRep paymentRequestRep) {

    EntityLockCommand<DObPaymentRequest> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObPaymentRequest.class.getName()));
    entityLockCommand.setRepository(paymentRequestRep);
    return entityLockCommand;
  }

  @Bean("PaymentRequestEntityLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("paymentRequestLockCommand") EntityLockCommand entityLockCommand)
      throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=PaymentRequestEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  @DependsOn({"paymentRequestLockCommand"})
  public IObPaymentRequestPaymentDetailsSaveCommand createIObPaymentDetailsSaveCommand(
      DObPaymentRequestRep paymentRequestRep,
      @Qualifier("IObPaymentRequestPaymentDetailsRep")
          IObPaymentRequestPaymentDetailsRep paymentDetailsRep,
      CObCurrencyRep currencyRep,
      IObCompanyBankDetailsRep companyBankDetailsRep,
      IUserAccountSecurityService userAccountSecurityService,
      MObItemGeneralModelRep itemGMRep,
      CObTreasuryRep treasuryRep,
      EntityLockCommand<DObPaymentRequest> entityLockCommand) {
    IObPaymentRequestPaymentDetailsSaveCommand paymentDetailsSaveCommand =
        new IObPaymentRequestPaymentDetailsSaveCommand();
    paymentDetailsSaveCommand.setPaymentRequestRep(paymentRequestRep);
    paymentDetailsSaveCommand.setPaymentDetailsRep(paymentDetailsRep);
    paymentDetailsSaveCommand.setCurrencyRep(currencyRep);
    paymentDetailsSaveCommand.setCompanyBankDetailsRep(companyBankDetailsRep);
    paymentDetailsSaveCommand.setUserAccountSecurityService(userAccountSecurityService);
    paymentDetailsSaveCommand.setEntityLockCommand(entityLockCommand);
    paymentDetailsSaveCommand.setItemGMRep(itemGMRep);
    paymentDetailsSaveCommand.setTreasuryRep(treasuryRep);
    return paymentDetailsSaveCommand;
  }

  @Bean
  DObPaymentRequestDeleteCommand deletePaymentRequestCommand(
      DObPaymentRequestRep paymentRequestRep,
      @Qualifier("paymentRequestLockCommand") EntityLockCommand entityLockCommand) {
    DObPaymentRequestDeleteCommand paymentRequestDeleteCommand =
        new DObPaymentRequestDeleteCommand();
    paymentRequestDeleteCommand.setPaymentRequestRep(paymentRequestRep);
    return paymentRequestDeleteCommand;
  }

  @Bean
  public DObPaymentRequestActivationCommand createDObPaymentRequestPostCommand(
    DObPaymentRequestRep paymentRequestRep,
    IUserAccountSecurityService userAccountSecurityService,
    CObExchangeRateRep cObExchangeRateRep,
    DObPaymentRequestActivatePreparationGeneralModelRep activatePreparationGMRep,
    CObFiscalYearRep fiscalYearRep) {

    DObPaymentRequestActivationCommand command = new DObPaymentRequestActivationCommand();
    command.setPaymentRequestRep(paymentRequestRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setcObExchangeRateRep(cObExchangeRateRep);
    command.setPaymentActivatePreparationGMRep(activatePreparationGMRep);
    command.setFiscalYearRep(fiscalYearRep);

    return command;
  }

    @Bean
    public DObBankPaymentForPurchaseOrderAccountDeterminationCommand createBankPaymentForPurchaseOrderAccountDeterminationCommand(
            DObPaymentRequestRep paymentRequestRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObAccountingDocumentAccountingDetailsRep accountingDetailsRep,
            DObPurchaseOrderRep purchaseOrderRep,
            LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
        DObBankPaymentForPurchaseOrderAccountDeterminationCommand command = new DObBankPaymentForPurchaseOrderAccountDeterminationCommand();
        command.setPaymentRequestRep(paymentRequestRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(paymentDetailsRep);
        command.setAccountingDetailsRep(accountingDetailsRep);
        command.setPurchaseOrderRep(purchaseOrderRep);
        command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
        return command;
    }

    @Bean
    public DObCashPaymentForPurchaseOrderAccountDeterminationCommand createCashPaymentForPurchaseOrderAccountDeterminationCommand(
            DObPaymentRequestRep paymentRequestRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObAccountingDocumentAccountingDetailsRep accountingDetailsRep,
            DObPurchaseOrderRep purchaseOrderRep,
            LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep) {
        DObCashPaymentForPurchaseOrderAccountDeterminationCommand command = new DObCashPaymentForPurchaseOrderAccountDeterminationCommand();
        command.setPaymentRequestRep(paymentRequestRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(paymentDetailsRep);
        command.setAccountingDetailsRep(accountingDetailsRep);
        command.setPurchaseOrderRep(purchaseOrderRep);
        command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
        return command;
    }

    @Bean
    public DObBankPaymentForVendorAccountDeterminationCommand createBankPaymentForVendorAccountDeterminationCommand(
            DObPaymentRequestRep paymentRequestRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObAccountingDocumentAccountingDetailsRep accountingDetailsRep,
            MObVendorRep vendorRep,
            IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoRep,
            LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
            HObChartOfAccountRep chartOfAccountRep) {
        DObBankPaymentForVendorAccountDeterminationCommand command = new DObBankPaymentForVendorAccountDeterminationCommand();
        command.setPaymentRequestRep(paymentRequestRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(paymentDetailsRep);
        command.setAccountingDetailsRep(accountingDetailsRep);
        command.setVendorRep(vendorRep);
        command.setVendorAccountingInfoRep(vendorAccountingInfoRep);
        command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
        command.setChartOfAccountRep(chartOfAccountRep);
        return command;
    }

    @Bean
    public DObCashPaymentForVendorAccountDeterminationCommand createCashPaymentForVendorAccountDeterminationCommand(
            DObPaymentRequestRep paymentRequestRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObAccountingDocumentAccountingDetailsRep accountingDetailsRep,
            MObVendorRep vendorRep,
            IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoRep,
            LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
            HObChartOfAccountRep chartOfAccountRep) {
        DObCashPaymentForVendorAccountDeterminationCommand command = new DObCashPaymentForVendorAccountDeterminationCommand();
        command.setPaymentRequestRep(paymentRequestRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(paymentDetailsRep);
        command.setAccountingDetailsRep(accountingDetailsRep);
        command.setVendorRep(vendorRep);
        command.setVendorAccountingInfoRep(vendorAccountingInfoRep);
        command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
        command.setChartOfAccountRep(chartOfAccountRep);
        return command;
    }

  @Bean
  public DObLandedCostUpdateActualCostCommand createDObPaymentRequestUpdateLandedCostCommand(
      DObLandedCostGeneralModelRep landedCostGeneralModelRep,
      DObLandedCostRep landedCostRep,
      MObItemRep itemRep,
      IObLandedCostFactorItemsRep landedCostFactorItemsRep,
      IUserAccountSecurityService userAccountSecurityService) {
    DObLandedCostUpdateActualCostCommand command = new DObLandedCostUpdateActualCostCommand();
    command.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
    command.setLandedCostRep(landedCostRep);
    command.setItemRep(itemRep);
    command.setLandedCostFactorItemsRep(landedCostFactorItemsRep);
    command.setUserAccountSecurityService(userAccountSecurityService);

    return command;
  }

  @Bean
  public DObPaymentUpdateVendorInvoiceRemainingCommand
      createPaymentUpdateVendorInvoiceRemainingCommand(
          IObPaymentRequestPaymentDetailsGeneralModelRep
              paymentRequestPaymentDetailsGeneralModelRep,
          DObVendorInvoiceRep vendorInvoiceRep,
          IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep,
          IUserAccountSecurityService userAccountSecurityService) {
    DObPaymentUpdateVendorInvoiceRemainingCommand command =
        new DObPaymentUpdateVendorInvoiceRemainingCommand();
    command.setPaymentRequestPaymentDetailsGeneralModelRep(
        paymentRequestPaymentDetailsGeneralModelRep);
    command.setVendorInvoiceRep(vendorInvoiceRep);
    command.setVendorInvoiceSummaryRep(vendorInvoiceSummaryRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    return command;
  }

  @Bean
  public DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand
      createDObPaymentRequestForVendorAgainstPoUpdateRemainingCommand(
          DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep,
          DObPurchaseOrderRep purchaseOrderRep) {
    DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand
        paymentRequestForVendorAgainstPoUpdateRemainingCommand =
            new DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand();
    paymentRequestForVendorAgainstPoUpdateRemainingCommand.setDObPaymentRequestGeneralModelRep(
        paymentRequestGeneralModelRep);
    paymentRequestForVendorAgainstPoUpdateRemainingCommand.setDObPurchaseOrderRep(purchaseOrderRep);
    return paymentRequestForVendorAgainstPoUpdateRemainingCommand;
  }
}
