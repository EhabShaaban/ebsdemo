package com.ebs.dda.config.accounting.settlement;

import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.codegenerator.JournalBalanceCodeGenerator;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceDecreaseBalanceCommand;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceIncreaseBalanceCommand;
import com.ebs.dda.commands.accounting.journalentry.create.DObJournalEntryCreateSettlementCommand;
import com.ebs.dda.commands.accounting.settlement.DObSettlementPostCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.activate.GenerateJournalEntryCode;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.accounting.settlement.CreateSettlementJournalEntry;
import com.ebs.dda.functions.executecommand.accounting.settlement.UpdateSettlementJournalBalance;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.executequery.accounting.settlement.ConstructSettlementJournalBalanceUpdateValueObject;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.settlement.SettlementLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.jpa.accounting.journalentry.DObSettlementJournalEntry;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.rest.accounting.settlement.activation.ActivateSettlement;
import com.ebs.dda.validation.settlement.DObSettlementActivationValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SettlementFunctionBeans {

  @Bean
  public ActivateSettlement activateSettlement(
      AuthorizationManager authorizationManager,
      DObSettlementGeneralModelRep settlementGMRep,
      IObSettlementAccountDetailsGeneralModelRep accDetailsGMRep,
      IObSettlementDetailsGeneralModelRep detailsGMRep,
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGMRep,
      CObJournalBalanceGeneralModelRep journalBalanceGMRep,
      JournalBalanceCodeGenerator journalBalanceCodeGenerator,
      DocumentObjectCodeGenerator codeGenerator,
      EntityLockCommand<DObSettlement> settlementLockCmd,
      CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
      SerializionUtils serializionUtils,
      DObSettlementActivationValidator validator,
      TransactionManagerDelegate transactionManagerDelegate,
      DObSettlementPostCommand activateCommand,
      CObJournalBalanceIncreaseBalanceCommand journalBalanceIncreaseBalanceCommand,
      CObJournalBalanceDecreaseBalanceCommand journalBalanceDecreaseBalanceCommand,
      DObJournalEntryCreateSettlementCommand createJournalEntryCommand) {

    return context -> {
      SettlementLockActivateEntitiesOfInterest lockManager =
          new SettlementLockActivateEntitiesOfInterest(
              settlementLockCmd, journalBalanceLockCommandBeanFactory);

      new ActivateDocument()
          .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
          .findEntitiesOfInterest(
              new FindEntityByUserCode(settlementGMRep),
              new ConstructSettlementJournalBalanceUpdateValueObject(
                  accDetailsGMRep,
                  detailsGMRep,
                  companyBankDetailsGMRep,
                  journalBalanceGMRep,
                  journalBalanceCodeGenerator),
              new GenerateJournalEntryCode(codeGenerator, DObSettlementJournalEntry.class))
          .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
          .lockEntitiesOfInterest(new LockEntitiesOfInterest(lockManager))
          .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
          .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObSettlementStateMachine()))
          .validateMissingFieldsForState(new ValidateMissingFieldsForState(validator))
          .validateBusinessRules(new ValidateBusinessRules(validator))
          .transactionManager(transactionManagerDelegate)
          .executeCommand(new ExecuteCommand(activateCommand))
          .executeActivateDependencies(
              new UpdateSettlementJournalBalance(
                  journalBalanceIncreaseBalanceCommand, journalBalanceDecreaseBalanceCommand),
              new CreateSettlementJournalEntry(createJournalEntryCommand))
          .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(lockManager))
          .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
          .activate(context);

      return context.serializeResponse();
    };
  }
}
