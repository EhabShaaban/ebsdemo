package com.ebs.dda.config.masterdata;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.exchangerates.CBEExchangeRateClient;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.masterdata.exchagerate.CObExchangeRateCreateCommand;
import com.ebs.dda.commands.masterdata.exchagerate.CObExchangeRatesUpdateCommandRemotly;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateTypeRep;

@Configuration
public class CObExchangeRateCommandBeans {
  private static final String EXCHANGERATE_SERVER_DOMAIN = "exchangerate.server.domain";

  @Bean
  public CObExchangeRateCreateCommand exchangeRateCreateCommand(
      IUserAccountSecurityService userAccountSecurityService, CObCurrencyRep currencyRep,
      CObExchangeRateRep exchangeRateRep, DocumentObjectCodeGenerator codeGenrator,
      CObExchangeRateTypeRep exchangeRateTypeRep) throws Exception {

    CObExchangeRateCreateCommand command = new CObExchangeRateCreateCommand(codeGenrator);

    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setExchangeRep(exchangeRateRep);
    command.setCurrencyRep(currencyRep);
    command.setExchangeRateTypeRep(exchangeRateTypeRep);

    return command;
  }

  @Bean
  public CObExchangeRatesUpdateCommandRemotly createRemoteExchangeRatesServerUpdateCommand(
      CObCurrencyRep currencyRep, CObExchangeRateRep exchangeRateRep,
      MasterDataCodeGenrator codeGenrator, CObExchangeRateTypeRep exchangeRateTypeRep,
      CBEExchangeRateClient cbeClient) throws Exception {

    CObExchangeRatesUpdateCommandRemotly command = new CObExchangeRatesUpdateCommandRemotly();

    command.setCbeClient(cbeClient);
    command.setCodeGenrator(codeGenrator);
    command.setCurrencyRep(currencyRep);
    command.setExchangeRateRep(exchangeRateRep);
    command.setExchangeRateTypeRep(exchangeRateTypeRep);

    return command;
  }

  @Bean
  public CBEExchangeRateClient createCBEExchangeRateClient(Environment env) {
    String domain = env.getProperty(EXCHANGERATE_SERVER_DOMAIN);
    CBEExchangeRateClient client = new CBEExchangeRateClient(domain);
    return client;
  }
}
