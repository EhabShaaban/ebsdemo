package com.ebs.dda.config.inventory.storetransaction;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.inventory.storetransaction.*;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService;
import com.ebs.dda.commands.inventory.storetransaction.services.GoodsReceiptPurchaseOrderCreateStoreTransactionService;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.PurchasedItemTotalFinalCostRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptSalesReturnDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.stocktransformation.DObStockTransformationGeneralModelRep;
import com.ebs.dda.repositories.inventory.storetransaction.*;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderDetailsGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TObStoreTransactionCommandBeans {

  @Bean
  public TObStoreTransactionCreateGoodsReceiptCommand
      createTObStoreTransactionCreateGoodsReceiptCommand(
          TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep,
          GoodsReceiptPurchaseOrderCreateStoreTransactionService
              goodsReceiptPurchaseOrderCreateStoreTransactionService,
          GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService
              goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService,
          DocumentObjectCodeGenerator documentObjectCodeGenerator,
          IUserAccountSecurityService userAccountSecurityService,
          IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
              goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep,
          IObGoodsReceiptSalesReturnDataGeneralModelRep goodsReceiptSalesReturnDataGeneralModelRep,
          IObSalesReturnOrderDetailsGeneralModelRep salesReturnOrderDetailsGeneralModelRep,
          IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep,
          IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep,
          TObGoodsIssueStoreTransactionGeneralModelRep goodsIssueStoreTransactionGeneralModelRep,
          TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep,
          DObGoodsReceiptDataGeneralModelRep goodsReceiptDataGeneralModelRep,
          MObItemGeneralModelRep itemGeneralModelRep,
          CObMeasureRep measureRep) {

    TObStoreTransactionCreateGoodsReceiptCommand storeTransactionCreateGoodsReceiptCommand =
        new TObStoreTransactionCreateGoodsReceiptCommand(documentObjectCodeGenerator);
    storeTransactionCreateGoodsReceiptCommand
        .setGoodsReceiptPurchaseOrderCreateStoreTransactionService(
            goodsReceiptPurchaseOrderCreateStoreTransactionService);
    storeTransactionCreateGoodsReceiptCommand.setGoodsReceiptStoreTransactionRep(
        goodsReceiptStoreTransactionRep);
    storeTransactionCreateGoodsReceiptCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    storeTransactionCreateGoodsReceiptCommand
        .setGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep(
            goodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setGoodsReceiptSalesReturnDataGeneralModelRep(
        goodsReceiptSalesReturnDataGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setSalesReturnOrderDetailsGeneralModelRep(
        salesReturnOrderDetailsGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setSalesInvoiceBusinessPartnerGeneralModelRep(
        salesInvoiceBusinessPartnerGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setGoodsIssueRefDocumentDataGeneralModelRep(
        goodsIssueRefDocumentDataGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setGoodsIssueStoreTransactionGeneralModelRep(
        goodsIssueStoreTransactionGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setStoreTransactionGeneralModelRep(
        storeTransactionGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setGoodsReceiptDataGeneralModelRep(
        goodsReceiptDataGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setItemGeneralModelRep(itemGeneralModelRep);
    storeTransactionCreateGoodsReceiptCommand.setMeasureRep(measureRep);
    storeTransactionCreateGoodsReceiptCommand.setGoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService);

    return storeTransactionCreateGoodsReceiptCommand;
  }

  @Bean
  public TObStoreTransactionCreateGoodsIssueCommand
      createTObStoreTransactionCreateGoodsIssueCommand(
          TObStoreTransactionRep tObStoreTransactionRep,
          TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep,
          TObGoodsIssueStoreTransactionRep goodsIssueStoreTransactionRep,
          DObGoodsIssueDataGeneralModelRep goodsIssueDataGeneralModelRep,
          DocumentObjectCodeGenerator documentObjectCodeGenerator,
          IUserAccountSecurityService userAccountSecurityService) {

    TObStoreTransactionCreateGoodsIssueCommand storeTransactionCreateGoodsIssueCommand =
        new TObStoreTransactionCreateGoodsIssueCommand(documentObjectCodeGenerator);
    storeTransactionCreateGoodsIssueCommand.settObStoreTransactionRep(tObStoreTransactionRep);
    storeTransactionCreateGoodsIssueCommand.settObStoreTransactionGeneralModelRep(
        tObStoreTransactionGeneralModelRep);
    storeTransactionCreateGoodsIssueCommand.setGoodsIssueStoreTransactionRep(
        goodsIssueStoreTransactionRep);
    storeTransactionCreateGoodsIssueCommand.setGoodsIssueDataGeneralModelRep(
        goodsIssueDataGeneralModelRep);
    storeTransactionCreateGoodsIssueCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return storeTransactionCreateGoodsIssueCommand;
  }

  @Bean
  public TObStoreTransactionCreateInitialStockUploadCommand
      createTObStoreTransactionCreateInitialStockUploadCommand(
          TObInitialStockUploadStoreTransactionRep initialStockUploadStoreTransactionRep,
          TObInitialStockUploadStoreTransactionDataGeneralModelRep
              initialStockUploadStoreTransactionDataGeneralModelRep,
          DocumentObjectCodeGenerator documentObjectCodeGenerator,
          IUserAccountSecurityService userAccountSecurityService) {

    TObStoreTransactionCreateInitialStockUploadCommand
        storeTransactionCreateInitialStockUploadCommand =
            new TObStoreTransactionCreateInitialStockUploadCommand(documentObjectCodeGenerator);
    storeTransactionCreateInitialStockUploadCommand.setInitialStockUploadStoreTransactionRep(
        initialStockUploadStoreTransactionRep);
    storeTransactionCreateInitialStockUploadCommand
        .setInitialStockUploadStoreTransactionDataGeneralModelRep(
            initialStockUploadStoreTransactionDataGeneralModelRep);
    storeTransactionCreateInitialStockUploadCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return storeTransactionCreateInitialStockUploadCommand;
  }

  @Bean
  public TObGoodsReceiptStoreTransactionUpdateCommand
      createTObGoodsReceiptStoreTransactionUpdateCommand(
          TObStoreTransactionRep storeTransactionRep,
          DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep,
          IUserAccountSecurityService userAccountSecurityService) {

    TObGoodsReceiptStoreTransactionUpdateCommand goodsReceiptStoreTransactionUpdateCommand =
        new TObGoodsReceiptStoreTransactionUpdateCommand();
    goodsReceiptStoreTransactionUpdateCommand.setStoreTransactionRep(storeTransactionRep);
    goodsReceiptStoreTransactionUpdateCommand.setStockTransformationGeneralModelRep(
        stockTransformationGeneralModelRep);
    goodsReceiptStoreTransactionUpdateCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return goodsReceiptStoreTransactionUpdateCommand;
  }

  @Bean
  public TObStoreTransactionCreateStockTransformationCommand
      createTObStoreTransactionCreateStockTransformationCommand(
          DocumentObjectCodeGenerator documentObjectCodeGenerator,
          TObStockTransformationStoreTransactionRep tObStockTransformationStoreTransactionRep,
          IUserAccountSecurityService userAccountSecurityService,
          TObStoreTransactionRep storeTransactionRep,
          DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep) {

    TObStoreTransactionCreateStockTransformationCommand
        storeTransactionCreateStockTransformationCommand =
            new TObStoreTransactionCreateStockTransformationCommand(documentObjectCodeGenerator);

    storeTransactionCreateStockTransformationCommand.setTObStockTransformationStoreTransactionRep(
        tObStockTransformationStoreTransactionRep);
    storeTransactionCreateStockTransformationCommand.setUserAccountSecurityService(
        userAccountSecurityService);

    storeTransactionCreateStockTransformationCommand.setStoreTransactionRep(storeTransactionRep);
    storeTransactionCreateStockTransformationCommand.setStockTransformationGeneralModelRep(
        stockTransformationGeneralModelRep);
    return storeTransactionCreateStockTransformationCommand;
  }

  @Bean
  public TObGoodsReceiptStoreTransactionRecostingCommand
      createTObStoreTransactionGoodsReceiptRecostingCommand(
          TObGoodsReceiptStoreTransactionRep goodsReceiptStoreTransactionRep,
          TObStoreTransactionRep storeTransactionRep,
          DObLandedCostGeneralModelRep landedCostGeneralModelRep,
          DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep,
          GoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService,
          IUserAccountSecurityService userAccountSecurityService) {

    TObGoodsReceiptStoreTransactionRecostingCommand storeTransactionGoodsReceiptRecostingCommand =
        new TObGoodsReceiptStoreTransactionRecostingCommand();

    storeTransactionGoodsReceiptRecostingCommand.setGoodsReceiptStoreTransactionRep(
        goodsReceiptStoreTransactionRep);
    storeTransactionGoodsReceiptRecostingCommand.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    storeTransactionGoodsReceiptRecostingCommand.setGoodsReceiptPurchaseOrderAddActualAndOrEstimateCostService(goodsReceiptPurchaseOrderAddActualAndOrEstimateCostService);
    storeTransactionGoodsReceiptRecostingCommand.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
    storeTransactionGoodsReceiptRecostingCommand.setStoreTransactionRep(storeTransactionRep);
    storeTransactionGoodsReceiptRecostingCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return storeTransactionGoodsReceiptRecostingCommand;
  }
}
