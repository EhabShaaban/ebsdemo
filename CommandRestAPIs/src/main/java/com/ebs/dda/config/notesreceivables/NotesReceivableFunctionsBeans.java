package com.ebs.dda.config.notesreceivables;

import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.notesreceivables.statemachines.DObNotesReceivablesStateMachine;
import com.ebs.dda.commands.accounting.notesreceivables.DObJournalEntryCreateNotesReceivableCommand;
import com.ebs.dda.commands.accounting.notesreceivables.DObNotesReceivableAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.notesreceivables.DObNotesReceivableActivateCommand;
import com.ebs.dda.commands.accounting.notesreceivables.IObNotesReceivableRefDocumentUpdateRemainingCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.accounting.monetarynote.CreateNotesReceivableJournalEntry;
import com.ebs.dda.functions.executecommand.accounting.monetarynote.DetermineNotesReceivableAccount;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.executequery.accounting.notesreceivable.FindReferenceDocumentsForNotesReceivable;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.notesreceivable.NotesReceivableLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.functions.validate.ValidateSchema;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.rest.accounting.notesreceivables.UpdateNotesReceivableReferenceDocumentsRemaining;
import com.ebs.dda.rest.accounting.notesreceivables.activation.ActivateNotesReceivable;
import com.ebs.dda.validation.notesreceivable.DObNotesReceivableActivateMandatoriesValidator;
import com.ebs.dda.validation.notesreceivable.DObNotesReceivableActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NotesReceivableFunctionsBeans {

  @Bean
  public ActivateNotesReceivable activateNotesReceivableDocument(
      AuthorizationManager authorizationManager,
      SchemaValidator schemaValidator,
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
      EntityLockCommand<DObMonetaryNotes> notesReceivableLockCommand,
      DObNotesReceivableRefDocumentLockCommandFactory refDocumentLockCommandFactory,
      SerializionUtils serializionUtils,
      TransactionManagerDelegate transactionManagerDelegate,
      DObNotesReceivableActivateCommand notesReceivableActivateCommand,
      DObNotesReceivableAccountDeterminationCommand notesReceivableAccountDeterminationCommand,
      IObNotesReceivableRefDocumentUpdateRemainingCommand refDocUpdateRemainingCommand,
      IObNotesReceivablesReferenceDocumentGeneralModelRep
          notesReceivablesReferenceDocumentGeneralModelRep,
      DObNotesReceivableActivateValidator validator,
      DObNotesReceivableActivateMandatoriesValidator mandatoriesValidator,
      DObJournalEntryCreateNotesReceivableCommand journalEntryCreateCommand) {

    return context -> {
      NotesReceivableLockActivateEntitiesOfInterest notesReceivableLockManager =
          new NotesReceivableLockActivateEntitiesOfInterest(
              notesReceivableLockCommand, refDocumentLockCommandFactory);

      new ActivateDocument()
          .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
          .validateSchema(new ValidateSchema(schemaValidator))
          .findEntitiesOfInterest(
              new FindEntityByUserCode<>(notesReceivablesGeneralModelRep),
              new FindReferenceDocumentsForNotesReceivable(
                  notesReceivablesReferenceDocumentGeneralModelRep))
          .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
          .lockEntitiesOfInterest(new LockEntitiesOfInterest(notesReceivableLockManager))
          .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
          .validateIfActionIsAllowed(
              new ValidateIfActionIsAllowed(new DObNotesReceivablesStateMachine()))
          .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
          .validateBusinessRules(new ValidateBusinessRules<>(validator))
          .transactionManager(transactionManagerDelegate)
          .executeCommand(new ExecuteCommand<>(notesReceivableActivateCommand))
          .executeActivateDependencies(
              new UpdateNotesReceivableReferenceDocumentsRemaining(refDocUpdateRemainingCommand),
              new DetermineNotesReceivableAccount(notesReceivableAccountDeterminationCommand),
              new CreateNotesReceivableJournalEntry(journalEntryCreateCommand))
          .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(notesReceivableLockManager))
          .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
          .activate(context);
      return context.serializeResponse();
    };
  }
}
