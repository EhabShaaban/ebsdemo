package com.ebs.dda.config.inventory.goodsreceipt;

import com.ebs.dda.inventory.goodsreceipt.editability.*;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObGoodsReceiptEditabilityBeans {

  @Bean
  public DObGoodsReceiptItemBatchEditabilityManager
      createDObGoodsReceiptItemBatchEditabilityManager() {
    DObGoodsReceiptItemBatchEditabilityManager batchEditabilityManager =
        new DObGoodsReceiptItemBatchEditabilityManager();
    return batchEditabilityManager;
  }

  @Bean
  public DObGoodsReceiptItemQuantityEditabilityManager
      createDObGoodsReceiptItemQuantityEditabilityManager() {
    DObGoodsReceiptItemQuantityEditabilityManager quantityEditabilityManager =
        new DObGoodsReceiptItemQuantityEditabilityManager();
    return quantityEditabilityManager;
  }
  @Bean
  public DObGoodsReceiptAddItemEditabilityManager createDObGoodsReceiptAddItemEditabilityManager() {
    DObGoodsReceiptAddItemEditabilityManager addItemEditabilityManager =
        new DObGoodsReceiptAddItemEditabilityManager();
    return addItemEditabilityManager;
  }

  @Bean
  public IObGoodsReceiptItemDifferenceReasonEditabilityManager
      createIObGoodsReceiptItemDifferenceReasonEditabilityManager(
          DObGoodsReceiptGeneralModelRep goodsReceiptHeaderGeneralModelRep) {
    return new IObGoodsReceiptItemDifferenceReasonEditabilityManager(
        goodsReceiptHeaderGeneralModelRep);
  }
}
