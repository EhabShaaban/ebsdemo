package com.ebs.dda.config.masterdata;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.ConfigurationObjectCodeGenrator;
import com.ebs.dda.commands.masterdata.measure.CObMeasureCreateCommand;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CObMeasureCommandBeans {

  @Bean
  public CObMeasureCreateCommand createCObMeasureCreateCommand(
      ConfigurationObjectCodeGenrator configurationObjectCodeGenrator,
      IUserAccountSecurityService userAccountSecurityService,
      CObMeasureRep masureRep) {
    CObMeasureCreateCommand command = new CObMeasureCreateCommand(configurationObjectCodeGenrator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setMeasureRep(masureRep);
    return command;
  }
}
