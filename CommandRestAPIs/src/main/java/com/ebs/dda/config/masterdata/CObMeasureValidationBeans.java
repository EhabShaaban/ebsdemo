package com.ebs.dda.config.masterdata;

import com.ebs.dda.validation.masterdata.unitofmeasure.CObMeasureCreateMandatoryValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CObMeasureValidationBeans {

  @Bean
  public CObMeasureCreateMandatoryValidator createCObMeasureMandatoryValidator() {
    CObMeasureCreateMandatoryValidator measureMandatoryValidator = new CObMeasureCreateMandatoryValidator();
    return measureMandatoryValidator;
  }
}
