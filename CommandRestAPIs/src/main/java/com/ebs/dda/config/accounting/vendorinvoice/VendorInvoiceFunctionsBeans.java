package com.ebs.dda.config.accounting.vendorinvoice;

import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.landedcost.factories.DObLandedCostUpdateActualCostValueObjectFactory;
import com.ebs.dda.accounting.vendorinvoice.statemachines.DObVendorInvoiceStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand;
import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.DObJournalEntryCreateVendorInvoiceCommand;
import com.ebs.dda.commands.accounting.landedcost.DObLandedCostUpdateActualCostCommand;
import com.ebs.dda.commands.accounting.landedcost.IObLandedCostUpdateGoodsInvoiceCommand;
import com.ebs.dda.commands.accounting.vendorinvoice.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.activate.GenerateJournalEntryCode;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.accounting.landedcost.UpdateLandedCostActualCost;
import com.ebs.dda.functions.executecommand.accounting.vendorinvoice.*;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.executequery.accounting.landedCost.FindEstimatedLandedCostByPurchaseOrder;
import com.ebs.dda.functions.executequery.accounting.vendorinvoice.FindVendorInvoicePurchaseOrder;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.vendorinvoice.ServiceInvoiceLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.vendorinvoice.VendorInvoiceLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.functions.validate.ValidateSchema;
import com.ebs.dda.jpa.accounting.journalentry.DObVendorInvoiceJournalEntry;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.rest.accounting.vendorinvoice.activation.ActivateVendorInvoice;
import com.ebs.dda.validation.vendorinvoice.DObVendorInvoiceActivateMandatoriesValidator;
import com.ebs.dda.validation.vendorinvoice.DObVendorInvoiceActivateServiceInvoiceValidator;
import com.ebs.dda.validation.vendorinvoice.DObVendorInvoiceActivateGoodsInvoiceValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VendorInvoiceFunctionsBeans {

    @Bean("LocalGoodsInvoiceActivate")
    public ActivateVendorInvoice activateLocalGoodsInvoice(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
            EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
            EntityLockCommand<DObOrderDocument> orderLockCommand,
            SerializionUtils serializionUtils,
            DObVendorInvoiceActivateGoodsInvoiceValidator validator,
            DObVendorInvoiceActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObVendorInvoiceActivateCommand activationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            DObLocalGoodsInvoiceAccountDeterminationCommand accountDeterminationCommand,
            DObVendorInvoiceUpdateDownPaymentCommand updateDownPaymentCommand,
            IObVendorInvoiceSummaryUpdateCommand updateSummaryCommand,
            IObLandedCostUpdateGoodsInvoiceCommand updateLandedCostDetailsCommand,
            DObJournalEntryCreateVendorInvoiceCommand journalEntryCreateCommand) {

        return context -> {
            VendorInvoiceLockActivateEntitiesOfInterest vendorInvoiceLockManager =
                    new VendorInvoiceLockActivateEntitiesOfInterest(
                            vendorInvoiceLockCommand, orderLockCommand);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(vendorInvoiceGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObVendorInvoiceJournalEntry.class))
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(vendorInvoiceLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObVendorInvoiceStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(activationCommand))
                    .executeActivateDependencies(
                            new DetermineVendorInvoiceAccount(accountDeterminationCommand),
                            new UpdateDownPayment(updateDownPaymentCommand),
                            new UpdateVendorInvoiceSummary(updateSummaryCommand),
                            new UpdateLandedCostDetails(updateLandedCostDetailsCommand),
                            new CreateVendorInvoiceJournalEntry(journalEntryCreateCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(vendorInvoiceLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }
    @Bean("ImportGoodsInvoiceActivate")
    public ActivateVendorInvoice activateImportGoodsInvoice(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
            EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
            EntityLockCommand<DObOrderDocument> orderLockCommand,
            SerializionUtils serializionUtils,
            DObVendorInvoiceActivateGoodsInvoiceValidator validator,
            DObVendorInvoiceActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObVendorInvoiceActivateCommand activationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            DObImportGoodsInvoiceAccountDeterminationCommand accountDeterminationCommand,
            IObVendorInvoiceSummaryUpdateCommand updateSummaryCommand,
            DObVendorInvoiceUpdateDownPaymentCommand updateDownPaymentCommand,
            IObLandedCostUpdateGoodsInvoiceCommand updateLandedCostDetailsCommand,
            DObJournalEntryCreateVendorInvoiceCommand journalEntryCreateCommand,
            DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand realizedExchangeRateCommand) {

        return context -> {
            VendorInvoiceLockActivateEntitiesOfInterest vendorInvoiceLockManager =
                    new VendorInvoiceLockActivateEntitiesOfInterest(
                            vendorInvoiceLockCommand, orderLockCommand);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(vendorInvoiceGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObVendorInvoiceJournalEntry.class))
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(vendorInvoiceLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObVendorInvoiceStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(activationCommand))
                    .executeActivateDependencies(
                            new DetermineVendorInvoiceAccount(accountDeterminationCommand),
                            new UpdateDownPayment(updateDownPaymentCommand),
                            new UpdateVendorInvoiceSummary(updateSummaryCommand),
                            new UpdateLandedCostDetails(updateLandedCostDetailsCommand),
                            new CreateVendorInvoiceJournalEntry(journalEntryCreateCommand),
                            new CreateImportVendorInvoiceRealizedExchangeRateDifferenceJournalEntry(realizedExchangeRateCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(vendorInvoiceLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }
    @Bean("PurchaseServiceLocalInvoiceActivate")
    public ActivateVendorInvoice activatePurchaseServiceLocal(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
            DObLandedCostGeneralModelRep landedCostRep,
            DObPurchaseOrderGeneralModelRep purchaseOrderRep,
            EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
            EntityLockCommand<DObOrderDocument> orderLockCommand,
            EntityLockCommand<DObLandedCost> landedCostLockCommand,
            SerializionUtils serializionUtils,
            DObVendorInvoiceActivateServiceInvoiceValidator validator,
            DObVendorInvoiceActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObVendorInvoiceActivateCommand activationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            DObPurchaseServiceLocalInvoiceAccountDeterminationCommand accountDeterminationCommand,
            DObVendorInvoiceUpdateDownPaymentCommand updateDownPaymentCommand,
            IObVendorInvoiceSummaryUpdateCommand updateSummaryCommand,
            DObJournalEntryCreateVendorInvoiceCommand journalEntryCreateCommand, 
            DObLandedCostUpdateActualCostCommand updateLandedCostCommand, 
            DObLandedCostUpdateActualCostValueObjectFactory landedCostUpdateActualCostvalueObjectFactory) {

        return context -> {
          ServiceInvoiceLockActivateEntitiesOfInterest vendorInvoiceLockManager =
                    new ServiceInvoiceLockActivateEntitiesOfInterest(
                            vendorInvoiceLockCommand, orderLockCommand, landedCostLockCommand);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(
                        new FindEntityByUserCode<>(vendorInvoiceGeneralModelRep),
                        new FindVendorInvoicePurchaseOrder(purchaseOrderRep),
                        new FindEstimatedLandedCostByPurchaseOrder(landedCostRep),
                        new GenerateJournalEntryCode(codeGenerator, DObVendorInvoiceJournalEntry.class))
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(vendorInvoiceLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObVendorInvoiceStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(activationCommand))
                    .executeActivateDependencies(
                        new DetermineVendorInvoiceAccount(accountDeterminationCommand),
                        new UpdateDownPayment(updateDownPaymentCommand),
                        new UpdateVendorInvoiceSummary(updateSummaryCommand),
                        new UpdateLandedCostActualCost(updateLandedCostCommand, landedCostUpdateActualCostvalueObjectFactory),
                        new CreateVendorInvoiceJournalEntry(journalEntryCreateCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(vendorInvoiceLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }


}
