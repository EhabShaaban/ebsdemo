package com.ebs.dda.config.inventory.goodsissue;

import com.ebs.dac.foundation.realization.concurrency.ConcurrentDataAccessManagersPool;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.commands.inventory.inventorydocument.DObInventoryDocumentActivateCommand;
import com.ebs.dda.commands.inventory.stockavailability.CObStockAvailabilityUpdateCommand;
import com.ebs.dda.commands.inventory.storetransaction.TObStoreTransactionCreateGoodsIssueCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderIssuedCommand;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.inventory.goodsissue.SalesOrderIssued;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.executequery.inventory.goodsissue.FindSalesOrderByGoodsIssueCode;
import com.ebs.dda.functions.executequery.inventory.goodsissue.FindStockAvailabilitiesForGoodsIssue;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.inventory.goodsissue.GoodsIssueActivateLockManager;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModelRep;
import com.ebs.dda.rest.inventory.goodsissue.activation.ActivateGoodsIssue;
import com.ebs.dda.validation.goodsissue.DObGoodsIssueActivateValidator;
import org.apache.commons.scxml2.model.ModelException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoodsIssueFunctionBeans {
  @Bean
  public ActivateGoodsIssue activateGoodsIssueDocument(
      AuthorizationManager authorizationManager,
      DObGoodsIssueActivateValidator activateValidator,
      DObGoodsIssueGeneralModelRep goodsIssueGeneralModelRep,
      DObInventoryDocumentActivateCommand dObInventoryDocumentActivateCommand,
      TransactionManagerDelegate transactionManagerDelegate,
      TObStoreTransactionCreateGoodsIssueCommand storeTransactionCreateGoodsIssueCommand,
      DObSalesOrderIssuedCommand salesOrderIssuedCommand,
      CObStockAvailabilityUpdateCommand stockAvailabilityUpdateCommand,
      SerializionUtils serializationUtils,
      IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep,
      DObGoodsIssueDataGeneralModelRep goodsIssueDataGeneralModelRep,
      StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator,
      ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool,
      UserAccountSecurityService userAccountSecurityService) {
    return context -> {
      createActivateDocumentFunction(
              authorizationManager,
              activateValidator,
              goodsIssueGeneralModelRep,
              dObInventoryDocumentActivateCommand,
              transactionManagerDelegate,
              storeTransactionCreateGoodsIssueCommand,
              salesOrderIssuedCommand,
              stockAvailabilityUpdateCommand,
              serializationUtils,
              goodsIssueItemQuantityGeneralModelRep,
              goodsIssueDataGeneralModelRep,
              stockAvailabilityCodeGenerator,
              concurrentDataAccessManagersPool,
              userAccountSecurityService)
          .activate(context);
      return context.serializeResponse();
    };
  }

  private ActivateDocument createActivateDocumentFunction(
      AuthorizationManager authorizationManager,
      DObGoodsIssueActivateValidator activateValidator,
      DObGoodsIssueGeneralModelRep goodsIssueGeneralModelRep,
      DObInventoryDocumentActivateCommand inventoryDocumentActivateCommand,
      TransactionManagerDelegate transactionManagerDelegate,
      TObStoreTransactionCreateGoodsIssueCommand storeTransactionCreateGoodsIssueCommand,
      DObSalesOrderIssuedCommand salesOrderIssuedCommand,
      CObStockAvailabilityUpdateCommand stockAvailabilityUpdateCommand,
      SerializionUtils serializionUtils,
      IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep,
      DObGoodsIssueDataGeneralModelRep goodsIssueDataGeneralModelRep,
      StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator,
      ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool,
      UserAccountSecurityService userAccountSecurityService)
      throws ModelException {
    GoodsIssueActivateLockManager goodsIssueActivateLockManager =
        new GoodsIssueActivateLockManager(
            concurrentDataAccessManagersPool, userAccountSecurityService);
    return new ActivateDocument()
        .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
        .findEntitiesOfInterest(
            new FindEntityByUserCode<>(goodsIssueGeneralModelRep),
            new FindStockAvailabilitiesForGoodsIssue(
                goodsIssueItemQuantityGeneralModelRep, stockAvailabilityCodeGenerator))
        .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
        .lockEntitiesOfInterest(new LockEntitiesOfInterest(goodsIssueActivateLockManager))
        .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObGoodsIssueStateMachine()))
        .validateBusinessRules(new ValidateBusinessRules(activateValidator))
        .transactionManager(transactionManagerDelegate)
        .executeCommand(new ExecuteCommand<>(inventoryDocumentActivateCommand))
        .executeActivateDependencies(
            new FindSalesOrderByGoodsIssueCode(goodsIssueDataGeneralModelRep),
            new SalesOrderIssued(salesOrderIssuedCommand),
            new ExecuteCommand<>(storeTransactionCreateGoodsIssueCommand),
            new ExecuteCommand<>(stockAvailabilityUpdateCommand))
        .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(goodsIssueActivateLockManager))
        .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils));
  }
}
