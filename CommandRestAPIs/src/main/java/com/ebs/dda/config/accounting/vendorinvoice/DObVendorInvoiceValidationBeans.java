package com.ebs.dda.config.accounting.vendorinvoice;

import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObPurchaseOrderVendorGeneralModelRep;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.IObItemAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.validation.vendorinvoice.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObVendorInvoiceValidationBeans {

  @Bean
  public DObVendorInvoiceCreationValidator createDObInvoiceCreationValidator(
          IObOrderCompanyGeneralModelRep purchaseOrderCompanySectionGeneralModelRep,
          DObPurchaseOrderRep orderRep,
          MObVendorRep vendorRep,
          DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep,
          DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
    DObVendorInvoiceCreationValidator creationValidator = new DObVendorInvoiceCreationValidator();
    creationValidator.setPurchaseOrderCompanySectionGeneralModelRep(
        purchaseOrderCompanySectionGeneralModelRep);
    creationValidator.setOrderRep(orderRep);
    creationValidator.setVendorRep(vendorRep);
    creationValidator.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    creationValidator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    return creationValidator;
  }

    @Bean
    public IObVendorInvoiceDetailsSaveValidator invoiceDetailsSaveValidator(
            CObCurrencyRep currencyRep,
            CObPaymentTermsRep paymentTermsRep,
            IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsRep) {
        IObVendorInvoiceDetailsSaveValidator invoiceDetailsSaveValidator =
                new IObVendorInvoiceDetailsSaveValidator();
        invoiceDetailsSaveValidator.setCurrencyRep(currencyRep);
        invoiceDetailsSaveValidator.setcObPaymentTermsRep(paymentTermsRep);
        invoiceDetailsSaveValidator.setInvoiceDetailsRep(invoiceDetailsRep);

        return invoiceDetailsSaveValidator;
    }

  @Bean(name = "DObVendorInvoiceActivateGoodsInvoiceValidator")
  public DObVendorInvoiceActivateGoodsInvoiceValidator createActivateGoodsInvoiceValidator(
      IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep,
      IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep,
      IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoGeneralModelRep,
      IObItemAccountingInfoGeneralModelRep itemAccountingInfoGeneralModelRep,
      CObFiscalYearRep fiscalYearRep
      ) {
    DObVendorInvoiceActivateGoodsInvoiceValidator sectionsMandatoryValidator = new DObVendorInvoiceActivateGoodsInvoiceValidator();
    sectionsMandatoryValidator.setInvoiceDetailsGeneralModelRep(invoiceDetailsGeneralModelRep);
    sectionsMandatoryValidator.setInvoiceItemsGeneralModelRep(invoiceItemsGeneralModelRep);
    sectionsMandatoryValidator.setItemAccountingInfoGeneralModelRep(
        itemAccountingInfoGeneralModelRep);
    sectionsMandatoryValidator.setVendorAccountingInfoGeneralModelRep(
        vendorAccountingInfoGeneralModelRep);
    sectionsMandatoryValidator.setFiscalYearRep(fiscalYearRep);
    return sectionsMandatoryValidator;
  }

  @Bean(name = "DObVendorInvoiceActivateServiceInvoiceValidator")
  public DObVendorInvoiceActivateServiceInvoiceValidator createInvoiceActivateServiceInvoiceValidator(
          IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep,
          IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep,
          IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoGeneralModelRep,
          IObItemAccountingInfoGeneralModelRep itemAccountingInfoGeneralModelRep,
          IObPurchaseOrderVendorGeneralModelRep orderVendorGeneralModelRep,
          DObLandedCostGeneralModelRep landedCostGeneralModelRep,
          CObFiscalYearRep fiscalYearRep) {
    DObVendorInvoiceActivateServiceInvoiceValidator validator = new DObVendorInvoiceActivateServiceInvoiceValidator();
    validator.setInvoiceDetailsGeneralModelRep(invoiceDetailsGeneralModelRep);
    validator.setInvoiceItemsGeneralModelRep(invoiceItemsGeneralModelRep);
    validator.setItemAccountingInfoGeneralModelRep(itemAccountingInfoGeneralModelRep);
    validator.setVendorAccountingInfoGeneralModelRep(vendorAccountingInfoGeneralModelRep);
    validator.setOrderVendorGeneralModelRep(orderVendorGeneralModelRep);
    validator.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean
  public DObVendorInvoiceActivateMandatoriesValidator createVendorInvoiceActivateMandatoriesValidator(
          IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep,
          IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep) {
    DObVendorInvoiceActivateMandatoriesValidator validator = new DObVendorInvoiceActivateMandatoriesValidator();
    validator.setInvoiceDetailsGeneralModelRep(invoiceDetailsGeneralModelRep);
    validator.setInvoiceItemsGeneralModelRep(invoiceItemsGeneralModelRep);
    return validator;
  }

  @Bean
	public DObVendorInvoiceItemsSaveValidator invoiceItemsSaveValidator(
					ItemVendorRecordGeneralModelRep itemVendorRecordRep,
					MObItemGeneralModelRep itemGeneralModelRep,
					DObVendorInvoiceGeneralModelRep invoiceGeneralModel,
					CObUoMGeneralModelRep uomGeneralModelRep,
					IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep) {
		DObVendorInvoiceItemsSaveValidator invoiceItemsSaveValidator = new DObVendorInvoiceItemsSaveValidator();
		invoiceItemsSaveValidator.setItemVendorRecordGeneralModelRep(itemVendorRecordRep);
		invoiceItemsSaveValidator.setItemGeneralModelRep(itemGeneralModelRep);
		invoiceItemsSaveValidator.setInvoiceGeneralModelRep(invoiceGeneralModel);
		invoiceItemsSaveValidator.setUomGeneralModelRep(uomGeneralModelRep);
		invoiceItemsSaveValidator
						.setVendorInvoiceItemsGeneralModelRep(vendorInvoiceItemsGeneralModelRep);
		return invoiceItemsSaveValidator;
	}
}
