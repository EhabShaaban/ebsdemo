package com.ebs.dda.config.inventory.goodsreceipt;

import com.ebs.dac.foundation.realization.concurrency.ConcurrentDataAccessManagersPool;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.commands.accounting.costing.DObCostingClosingJournalCreateCommand;
import com.ebs.dda.commands.accounting.costing.DObCostingCreateCommand;
import com.ebs.dda.commands.inventory.inventorydocument.DObInventoryDocumentActivateCommand;
import com.ebs.dda.commands.inventory.stockavailability.CObStockAvailabilityUpdateCommand;
import com.ebs.dda.commands.inventory.storetransaction.TObStoreTransactionCreateGoodsReceiptCommand;
import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderPostGoodsReceiptCommand;
import com.ebs.dda.commands.order.salesreturnorder.DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.inventory.goodsreceipt.CreateCostingClosingJournalEntry;
import com.ebs.dda.functions.executecommand.inventory.goodsreceipt.CreateCostingDocument;
import com.ebs.dda.functions.executecommand.inventory.goodsreceipt.UpdateGoodsReceiptReferenceDocument;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.executequery.inventory.goodsreceipt.FindCompletedLandedCostByPurchaseOrderCode;
import com.ebs.dda.functions.executequery.inventory.goodsreceipt.FindStockAvailabilitiesForGoodsReceipt;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.inventory.goodsreceipt.GoodsReceiptActivateLockManager;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModelRep;
import com.ebs.dda.rest.inventory.goodsreceipt.activation.ActivateGoodsReceipt;
import com.ebs.dda.validation.goodsreceipt.DObGoodReceiptActivateValidator;
import org.apache.commons.scxml2.model.ModelException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GoodsReceiptFunctionsBeans {

  @Bean
  public ActivateGoodsReceipt activateGoodsReceiptDocument(
      AuthorizationManager authorizationManager,
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep,
      DObInventoryDocumentActivateCommand inventoryDocumentActivateCommand,
      TransactionManagerDelegate transactionManagerDelegate,
      TObStoreTransactionCreateGoodsReceiptCommand storeTransactionCreateGoodsReceiptCommand,
      CObStockAvailabilityUpdateCommand stockAvailabilityUpdateCommand,
      DObPurchaseOrderPostGoodsReceiptCommand purchaseOrderPostGoodsReceiptCommand,
      DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand
          salesReturnOrderUpdateGoodsReceiptActivatedStateCommand,
      DObCostingCreateCommand costingCreateCommand,
      DObLandedCostGeneralModelRep landedCostGeneralModelRep,
      SerializionUtils serializionUtils,
      IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantityGeneralModelRep,
      StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator,
      ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool,
      UserAccountSecurityService userAccountSecurityService,
      DObGoodReceiptActivateValidator activateValidator) {
    return context -> {
      createActivateDocumentFunction(
              authorizationManager,
              goodsReceiptGeneralModelRep,
              inventoryDocumentActivateCommand,
              transactionManagerDelegate,
              storeTransactionCreateGoodsReceiptCommand,
              stockAvailabilityUpdateCommand,
              purchaseOrderPostGoodsReceiptCommand,
              salesReturnOrderUpdateGoodsReceiptActivatedStateCommand,
              costingCreateCommand,
              landedCostGeneralModelRep,
              serializionUtils,
              goodsReceiptItemQuantityGeneralModelRep,
              stockAvailabilityCodeGenerator,
              concurrentDataAccessManagersPool,
              userAccountSecurityService,
              activateValidator)
          .activate(context);
      return context.serializeResponse();
    };
  }

  private ActivateDocument createActivateDocumentFunction(
      AuthorizationManager authorizationManager,
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep,
      DObInventoryDocumentActivateCommand inventoryDocumentActivateCommand,
      TransactionManagerDelegate transactionManagerDelegate,
      TObStoreTransactionCreateGoodsReceiptCommand storeTransactionCreateGoodsReceiptCommand,
      CObStockAvailabilityUpdateCommand stockAvailabilityUpdateCommand,
      DObPurchaseOrderPostGoodsReceiptCommand purchaseOrderPostGoodsReceiptCommand,
      DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand
          salesReturnOrderUpdateGoodsReceiptActivatedStateCommand,
      DObCostingCreateCommand costingCreateCommand,
      DObLandedCostGeneralModelRep landedCostGeneralModelRep,
      SerializionUtils serializionUtils,
      IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantityGeneralModelRep,
      StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator,
      ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool,
      UserAccountSecurityService userAccountSecurityService,
      DObGoodReceiptActivateValidator activateValidator)
      throws ModelException {
    GoodsReceiptActivateLockManager goodsReceiptActivateLockManager =
        new GoodsReceiptActivateLockManager(
            concurrentDataAccessManagersPool, userAccountSecurityService);
    return new ActivateDocument()
        .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
        .findEntitiesOfInterest(
            new FindEntityByUserCode<>(goodsReceiptGeneralModelRep),
            new FindStockAvailabilitiesForGoodsReceipt(
                goodsReceiptItemQuantityGeneralModelRep, stockAvailabilityCodeGenerator))
        .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
        .lockEntitiesOfInterest(new LockEntitiesOfInterest(goodsReceiptActivateLockManager))
        .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObGoodsReceiptStateMachine()))
        .validateMissingFieldsForState(new ValidateMissingFieldsForState(activateValidator))
        .validateBusinessRules(new ValidateBusinessRules(activateValidator))
        .transactionManager(transactionManagerDelegate)
        .executeCommand(new ExecuteCommand<>(inventoryDocumentActivateCommand))
        .executeActivateDependencies(
            new FindCompletedLandedCostByPurchaseOrderCode(landedCostGeneralModelRep),
            new UpdateGoodsReceiptReferenceDocument(
                purchaseOrderPostGoodsReceiptCommand,
                salesReturnOrderUpdateGoodsReceiptActivatedStateCommand),
            new CreateCostingDocument(costingCreateCommand),
            new ExecuteCommand<>(storeTransactionCreateGoodsReceiptCommand),
            new ExecuteCommand<>(stockAvailabilityUpdateCommand))
        .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(goodsReceiptActivateLockManager))
        .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils));
  }
}
