package com.ebs.dda.config.accounting.paymentrequest;

import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.landedcost.factories.DObLandedCostUpdateActualCostValueObjectFactory;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.codegenerator.JournalBalanceCodeGenerator;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceDecreaseBalanceCommand;
import com.ebs.dda.commands.accounting.journalentry.create.paymentrequest.*;
import com.ebs.dda.commands.accounting.landedcost.DObLandedCostUpdateActualCostCommand;
import com.ebs.dda.commands.accounting.paymentrequest.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.activate.GenerateJournalEntryCode;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.accounting.journalbalance.DecreaseJournalBalance;
import com.ebs.dda.functions.executecommand.accounting.landedcost.UpdateLandedCostActualCost;
import com.ebs.dda.functions.executecommand.accounting.payment.CalculateRealizedExchangeRateDifference;
import com.ebs.dda.functions.executecommand.accounting.payment.CreatePaymentJournalEntry;
import com.ebs.dda.functions.executecommand.accounting.payment.DeterminePaymentAccount;
import com.ebs.dda.functions.executecommand.accounting.payment.UpdateVendorInvoiceRemaining;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.executequery.accounting.journalbalance.FindJournalBalance;
import com.ebs.dda.functions.executequery.accounting.landedCost.FindEstimatedLandedCostByPurchaseOrder;
import com.ebs.dda.functions.executequery.accounting.payment.ConstructPaymentJournalBalanceCodeGenerationValueObject;
import com.ebs.dda.functions.executequery.accounting.payment.FindPaymentPurhcaseOrder;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.paymentrequest.PaymentAgainstCreditNoteLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.paymentrequest.PaymentAgainstVendorInvoiceLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.paymentrequest.PaymentForVendorAgainstPurchaseOrderLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.paymentrequest.PaymentGeneralAgainstPurchaseOrderLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.functions.validate.ValidateSchema;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;
import com.ebs.dda.jpa.accounting.journalentry.DObPaymentRequestJournalEntry;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.order.DObOrderDocument;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.rest.accounting.paymentrequest.UpdatePoRemainingOfPaymentRequestForVendor;
import com.ebs.dda.rest.accounting.paymentrequest.activation.ActivatePayment;
import com.ebs.dda.validation.paymentrequest.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaymentFunctionsBeans {

    @Bean("BankGeneralPaymentForPOActivate")
    public ActivatePayment activateBankGeneralPaymentForPOActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            DObLandedCostGeneralModelRep landedCostRep,
            DObPurchaseOrderGeneralModelRep purchaseOrderRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObOrderDocument> purchaseOrderLockCommand,
            EntityLockCommand<DObLandedCost> landedCostLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            DObPaymentGeneralForPOActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator,
            DObBankPaymentForPurchaseOrderAccountDeterminationCommand accountDeterminationCommand,
            DObJournalEntryCreatePaymentRequestBankGeneralCommand journalEntryCreateCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand,
            DObLandedCostUpdateActualCostCommand updateLandedCostCommand,
            DObLandedCostUpdateActualCostValueObjectFactory landedCostUpdateActualCostValueObjectFactory) {

        return context -> {
            PaymentGeneralAgainstPurchaseOrderLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentGeneralAgainstPurchaseOrderLockActivateEntitiesOfInterest(
                            paymentLockCommand, purchaseOrderLockCommand, landedCostLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(
                            new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new FindPaymentPurhcaseOrder(purchaseOrderRep, paymentDetailsRep),
                            new FindEstimatedLandedCostByPurchaseOrder(landedCostRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(
                            new DeterminePaymentAccount(accountDeterminationCommand),
                            new CreatePaymentJournalEntry(journalEntryCreateCommand),
                            new UpdateLandedCostActualCost(updateLandedCostCommand, landedCostUpdateActualCostValueObjectFactory),
                            new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }

    @Bean("CashGeneralPaymentForPOActivate")
    public ActivatePayment activateCashGeneralPaymentForPOActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            DObLandedCostGeneralModelRep landedCostRep,
            DObPurchaseOrderGeneralModelRep purchaseOrderRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObOrderDocument> purchaseOrderLockCommand,
            EntityLockCommand<DObLandedCost> landedCostLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            DObPaymentGeneralForPOActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator,
            DObCashPaymentForPurchaseOrderAccountDeterminationCommand accountDeterminationCommand,
            DObJournalEntryCreatePaymentRequestCashGeneralCommand journalEntryCreateCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand,
            DObLandedCostUpdateActualCostCommand updateLandedCostCommand,
            DObLandedCostUpdateActualCostValueObjectFactory landedCostUpdateActualCostvalueObjectFactory
    ) {
        return context -> {
            PaymentGeneralAgainstPurchaseOrderLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentGeneralAgainstPurchaseOrderLockActivateEntitiesOfInterest(
                            paymentLockCommand, purchaseOrderLockCommand, landedCostLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(
                            new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new FindPaymentPurhcaseOrder(purchaseOrderRep, paymentDetailsRep),
                            new FindEstimatedLandedCostByPurchaseOrder(landedCostRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(
                            new DeterminePaymentAccount(accountDeterminationCommand),
                            new CreatePaymentJournalEntry(journalEntryCreateCommand),
                            new UpdateLandedCostActualCost(updateLandedCostCommand, landedCostUpdateActualCostvalueObjectFactory),
                            new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }

    @Bean("BankGeneralPaymentBasedOnCreditNoteActivate")
    public ActivatePayment activateBankGeneralPaymentBasedOnCreditNoteActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObAccountingNote> creditNoteLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            @Qualifier("DObPaymentRequestActivationValidator") DObPaymentRequestActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator) {
        return context -> {
            PaymentAgainstCreditNoteLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentAgainstCreditNoteLockActivateEntitiesOfInterest(
                            paymentLockCommand, creditNoteLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }

    @Bean("CashGeneralPaymentBasedOnCreditNoteActivate")
    public ActivatePayment activateCashGeneralPaymentBasedOnCreditNoteActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObAccountingNote> creditNoteLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            @Qualifier("DObPaymentRequestActivationValidator") DObPaymentRequestActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator) {
        return context -> {
            PaymentAgainstCreditNoteLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentAgainstCreditNoteLockActivateEntitiesOfInterest(
                            paymentLockCommand, creditNoteLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }

    @Bean("BankPaymentForVendorAgainstVendorInvoiceActivate")
    public ActivatePayment activateBankPaymentForVendorAgainstVendorInvoiceActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            @Qualifier("DObPaymentAgainstVendorInvoiceActivationValidator") DObPaymentAgainstVendorInvoiceActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator,
            DObBankPaymentForVendorAccountDeterminationCommand accountDeterminationCommand,
            DObPaymentUpdateVendorInvoiceRemainingCommand updateVendorInvoiceRemainingCommand,
            DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand journalEntryCreateCommand,
            DObJournalEntrySettlePaymentRealizedExchangeRateCommand realizedExchangeRateCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand) {
        return context -> {
            PaymentAgainstVendorInvoiceLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentAgainstVendorInvoiceLockActivateEntitiesOfInterest(
                            paymentLockCommand, vendorInvoiceLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(
                            new UpdateVendorInvoiceRemaining(updateVendorInvoiceRemainingCommand),
                            new DeterminePaymentAccount(accountDeterminationCommand),
                            new CreatePaymentJournalEntry(journalEntryCreateCommand),
                            new CalculateRealizedExchangeRateDifference(realizedExchangeRateCommand),
                            new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }

    @Bean("CashPaymentForVendorAgainstVendorInvoiceActivate")
    public ActivatePayment activateCashPaymentForVendorAgainstVendorInvoiceActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            @Qualifier("DObPaymentAgainstVendorInvoiceActivationValidator") DObPaymentAgainstVendorInvoiceActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator,
            DObCashPaymentForVendorAccountDeterminationCommand accountDeterminationCommand,
            DObPaymentUpdateVendorInvoiceRemainingCommand updateVendorInvoiceRemainingCommand,
            DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand journalEntryCreateCommand,
            DObJournalEntrySettlePaymentRealizedExchangeRateCommand realizedExchangeRateCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand) {
        return context -> {
            PaymentAgainstVendorInvoiceLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentAgainstVendorInvoiceLockActivateEntitiesOfInterest(
                            paymentLockCommand, vendorInvoiceLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(
                            new UpdateVendorInvoiceRemaining(updateVendorInvoiceRemainingCommand),
                            new DeterminePaymentAccount(accountDeterminationCommand),
                            new CreatePaymentJournalEntry(journalEntryCreateCommand),
                            new CalculateRealizedExchangeRateDifference(realizedExchangeRateCommand),
                            new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }

    @Bean("BankPaymentForVendorAgainstPurchaseOrderActivate")
    public ActivatePayment activateBankPaymentForVendorAgainstPurchaseOrderActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObOrderDocument> purchaseOrderLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            @Qualifier("DObPaymentAgainstPurchaseOrderActivationValidator") DObPaymentAgainstPurchaseOrderActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator,
            DObBankPaymentForVendorAccountDeterminationCommand accountDeterminationCommand,
            DObJournalEntryCreatePaymentRequestBankVendorDownPaymentCommand journalEntryCreateCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand,
            DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand paymentRequestForVendorAgainstPoUpdateRemainingCommand) {
        return context -> {
            PaymentForVendorAgainstPurchaseOrderLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentForVendorAgainstPurchaseOrderLockActivateEntitiesOfInterest(
                            paymentLockCommand, purchaseOrderLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(
                            new UpdatePoRemainingOfPaymentRequestForVendor(paymentRequestForVendorAgainstPoUpdateRemainingCommand),
                            new DeterminePaymentAccount(accountDeterminationCommand),
                            new CreatePaymentJournalEntry(journalEntryCreateCommand),
                            new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }

    @Bean("CashPaymentForVendorAgainstPurchaseOrderActivate")
    public ActivatePayment activateCashPaymentForVendorAgainstPurchaseOrderActivate(
            AuthorizationManager authorizationManager,
            SchemaValidator schemaValidator,
            DObPaymentRequestGeneralModelRep paymentGeneralModelRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep,
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            EntityLockCommand<DObPaymentRequest> paymentLockCommand,
            EntityLockCommand<DObOrderDocument> purchaseOrderLockCommand,
            CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
            SerializionUtils serializionUtils,
            @Qualifier("DObPaymentAgainstPurchaseOrderActivationValidator") DObPaymentAgainstPurchaseOrderActivationValidator validator,
            DObPaymentRequestActivateMandatoriesValidator mandatoriesValidator,
            TransactionManagerDelegate transactionManagerDelegate,
            DObPaymentRequestActivationCommand paymentActivationCommand,
            DocumentObjectCodeGenerator codeGenerator,
            CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
            JournalBalanceCodeGenerator journalBalanceCodeGenerator,
            DObCashPaymentForVendorAccountDeterminationCommand accountDeterminationCommand,
            DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand journalEntryCreateCommand,
            CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand,
            DObPaymentRequestForVendorAgainstPoUpdateRemainingCommand paymentRequestForVendorAgainstPoUpdateRemainingCommand) {
        return context -> {
            PaymentForVendorAgainstPurchaseOrderLockActivateEntitiesOfInterest paymentLockManager =
                    new PaymentForVendorAgainstPurchaseOrderLockActivateEntitiesOfInterest(
                            paymentLockCommand, purchaseOrderLockCommand, journalBalanceLockCommandBeanFactory);

            new ActivateDocument()
                    .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
                    .validateSchema(new ValidateSchema(schemaValidator))
                    .findEntitiesOfInterest(new FindEntityByUserCode<>(paymentGeneralModelRep),
                            new GenerateJournalEntryCode(codeGenerator, DObPaymentRequestJournalEntry.class),
                            new ConstructPaymentJournalBalanceCodeGenerationValueObject(companyDataGeneralModelRep, paymentDetailsRep),
                            new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator)
                    )
                    .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
                    .lockEntitiesOfInterest(new LockEntitiesOfInterest(paymentLockManager))
                    .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
                    .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObPaymentRequestStateMachine()))
                    .validateMissingFieldsForState(new ValidateMissingFieldsForState(mandatoriesValidator))
                    .validateBusinessRules(new ValidateBusinessRules<>(validator))
                    .transactionManager(transactionManagerDelegate)
                    .executeCommand(new ExecuteCommand<>(paymentActivationCommand))
                    .executeActivateDependencies(
                            new UpdatePoRemainingOfPaymentRequestForVendor(paymentRequestForVendorAgainstPoUpdateRemainingCommand),
                            new DeterminePaymentAccount(accountDeterminationCommand),
                            new CreatePaymentJournalEntry(journalEntryCreateCommand),
                            new DecreaseJournalBalance(decreaseBalanceCommand))
                    .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(paymentLockManager))
                    .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
                    .activate(context);
            return context.serializeResponse();
        };
    }
}
