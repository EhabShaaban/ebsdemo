package com.ebs.dda.config.masterdata;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.ChartOfAccountCodeGenerator;
import com.ebs.dda.commands.masterdata.chartofaccount.HObChartOfAccountCreateCommand;
import com.ebs.dda.commands.masterdata.chartofaccount.HObChartOfAccountDeleteCommand;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.*;

@Configuration
public class HObChartOfAccountCommandBeans {
	@Bean
	public HObChartOfAccountCreateCommand chartOfAccountCreateCommand(
					IUserAccountSecurityService userAccountSecurityService,
					HObIntermediateInChartOfAccountRep intermediateInChartOfAccountRep,
					HObLeafInChartOfAccountsRep leafInChartOfAccountsRep,
					HObRootInChartOfAccountRep rootInChartOfAccountRep,
					HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep,
					LObGlobalAccountConfigRep lObGlobalAccountConfigRep,
					ChartOfAccountCodeGenerator chartOfAccountCodeGenerator) throws Exception {
		HObChartOfAccountCreateCommand chartOfAccountCreateCommand = new HObChartOfAccountCreateCommand();
		chartOfAccountCreateCommand.setUserAccountSecurityService(userAccountSecurityService);

		chartOfAccountCreateCommand
						.setIntermediateInChartOfAccountRep(intermediateInChartOfAccountRep);
		chartOfAccountCreateCommand.setLeafInChartOfAccountsRep(leafInChartOfAccountsRep);
		chartOfAccountCreateCommand.setRootInChartOfAccountRep(rootInChartOfAccountRep);
		chartOfAccountCreateCommand.setGlobalAccountConfigRep(lObGlobalAccountConfigRep);
		chartOfAccountCreateCommand
						.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
		chartOfAccountCreateCommand.setChartOfAccountCodeGenerator(chartOfAccountCodeGenerator);

		return chartOfAccountCreateCommand;
	}

	@Bean
	public HObChartOfAccountDeleteCommand chartOfAccountDeleteCommand(
			HObChartOfAccountRep glAccountRep,
			HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep,
			LObGlobalAccountConfigRep lObGlobalAccountConfigRep) {
		HObChartOfAccountDeleteCommand command = new HObChartOfAccountDeleteCommand();
		command.setGlAccountRep(glAccountRep);
		command.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
		command.setGlobalAccountConfigRep(lObGlobalAccountConfigRep);
		return command;
	}
}
