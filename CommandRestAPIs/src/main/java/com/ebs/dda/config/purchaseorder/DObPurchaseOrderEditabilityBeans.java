package com.ebs.dda.config.purchaseorder;

import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPaymentAndDeliveryGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.editability.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObPurchaseOrderEditabilityBeans {
  @Bean
  public DObPurchasePaymentAndDeliverySectionEditabilityManager
      createDObPurchaseOrderPaymentAndDeliverySectionEditabilityManager(
          DObPaymentAndDeliveryGeneralModelRep dObPaymentAndDeliveryGeneralModelRep) {

    DObPurchasePaymentAndDeliverySectionEditabilityManager
        dObPurchasePaymentAndDeliverySectionEditabilityManager =
            new DObPurchasePaymentAndDeliverySectionEditabilityManager(
                dObPaymentAndDeliveryGeneralModelRep);

    return dObPurchasePaymentAndDeliverySectionEditabilityManager;
  }

  @Bean
  public DObPurchaseOrderItemsSectionEditabilityManager
      createDObPurchaseOrderItemsSectionEditabilityManager() {

    DObPurchaseOrderItemsSectionEditabilityManager purchaseOrderItemsSectionEditabilityManager =
        new DObPurchaseOrderItemsSectionEditabilityManager();

    return purchaseOrderItemsSectionEditabilityManager;
  }

  @Bean
  public DObPurchaseOrderItemsQtyInDnPlEditabilityManager
      createDObPurchaseOrderItemsQtyInDnPlEditabilityManager() {

    DObPurchaseOrderItemsQtyInDnPlEditabilityManager itemsQtyInDnPlEditabilityManager =
        new DObPurchaseOrderItemsQtyInDnPlEditabilityManager();

    return itemsQtyInDnPlEditabilityManager;
  }

  @Bean
  public DObPurchaseOrderItemQuantitySectionEditabilityManager
      createDObPurchaseOrderItemQuantitySectionEditabilityManager() {

    DObPurchaseOrderItemQuantitySectionEditabilityManager quantitySectionEditabilityManager =
        new DObPurchaseOrderItemQuantitySectionEditabilityManager();

    return quantitySectionEditabilityManager;
  }

  @Bean
  public DObPurchaseOrderCompanySectionEditabilityManager
      createPurchaseOrderCompanySectionEditabilityManager() {
    DObPurchaseOrderCompanySectionEditabilityManager purchaseOrderCompanySectionEditabilityManager =
        new DObPurchaseOrderCompanySectionEditabilityManager();
    return purchaseOrderCompanySectionEditabilityManager;
  }

  @Bean
  public IObServicePurchaseOrderItemsSectionEditabilityManager
      createPurchaseOrderItemsSectionEditabilityManager() {
    IObServicePurchaseOrderItemsSectionEditabilityManager
        servicePurchaseOrderItemsSectionEditabilityManager =
            new IObServicePurchaseOrderItemsSectionEditabilityManager();
    return servicePurchaseOrderItemsSectionEditabilityManager;
  }

  @Bean
  public IObPurchaseOrderPaymentDetailsEditabilityManager
      createPurchaseOrderPaymentDetailsEditabilityManager() {
    IObPurchaseOrderPaymentDetailsEditabilityManager purchaseOrderPaymentDetailsEditabilityManager =
        new IObPurchaseOrderPaymentDetailsEditabilityManager();
    return purchaseOrderPaymentDetailsEditabilityManager;
  }
}
