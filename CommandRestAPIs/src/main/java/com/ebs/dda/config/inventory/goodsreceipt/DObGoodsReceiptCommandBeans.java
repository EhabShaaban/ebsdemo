package com.ebs.dda.config.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.goodsreceipt.*;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderPlantEnterpriseDataRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderLineDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModelRep;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class DObGoodsReceiptCommandBeans {

  @Bean
  public DObGoodsReceiptCreateCommand createGoodsReceiptCreateCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      CObPlantGeneralModelRep plantGeneralModelRep,
      DObPurchaseOrderGeneralModelRep purchasingOrderGeneralModelRep,
      StoreKeeperRep storeKeeperRep,
      DObGoodsReceiptPurchaseOrderRep goodsReceiptRep,
      IObGoodsReceiptBatchedRecievedItemsRep goodsReceiptBatchedRecievedItemsRep,
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep goodsReceiptOrdinaryRecievedItemsRep,
      CObStorehouseRep storehouseRep,
      IObOrderLineDetailsGeneralModelRep purchaseOrderItemsGeneralModelRep,
      MObVendorRep vendorRep,
      CObPurchasingResponsibleRep purchasingResponsibleRep,
      CObPurchasingUnitRep purchasingUnitRep,
      IObOrderPlantEnterpriseDataRep orderPlantEnterpriseDataRep,
      PlatformTransactionManager transactionManager,
      MObItemRep itemRep,
      CObCompanyRep cObCompanyRep,
      CObGoodsReceiptRep cObGoodsReceiptRep,
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep,
      DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep,
      CObPlantGeneralModelRep plantRep,
      IObSalesReturnOrderCompanyDataGeneralModelRep iObSalesReturnOrderCompanyDataGeneralModelRep,
      MObCustomerRep customerRep,
      DocumentOwnerGeneralModelRep salesReturnOrderDocumentOwnerRep,
      IObSalesReturnOrderItemRep salesReturnOrderItemRep,
      MObItemGeneralModelRep itemGeneralModelRep)
      throws Exception {
    DObGoodsReceiptCreateCommand command =
        new DObGoodsReceiptCreateCommand(documentObjectCodeGenerator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setPurchasingOrderGeneralModelRep(purchasingOrderGeneralModelRep);
    command.setStoreKeeperRep(storeKeeperRep);
    command.setGoodsReceiptPurchaseOrderRep(goodsReceiptRep);
    command.setStorehouseRep(storehouseRep);
    command.setGoodsReceiptBatchedReceivedItemsRep(goodsReceiptBatchedRecievedItemsRep);
    command.setGoodsReceiptOrdinaryReceivedItemsRep(goodsReceiptOrdinaryRecievedItemsRep);
    command.setPurchaseOrderItemsGeneralModelRep(purchaseOrderItemsGeneralModelRep);
    command.setVendorRep(vendorRep);
    command.setPurchasingResponsibleRep(purchasingResponsibleRep);
    command.setPurchasingUnitRep(purchasingUnitRep);
    command.setOrderPlantEnterpriseDataRep(orderPlantEnterpriseDataRep);
    command.setTransactionManager(transactionManager);
    command.setItemRep(itemRep);
    command.setCObCompanyRep(cObCompanyRep);
    command.setCObGoodsReceiptRep(cObGoodsReceiptRep);
    command.setSalesReturnOrderGeneralModelRep(salesReturnOrderGeneralModelRep);
    command.setGoodsReceiptSalesReturnRep(goodsReceiptSalesReturnRep);
    command.setPlantRep(plantRep);
    command.setIObSalesReturnOrderCompanyDataGeneralModelRep(
        iObSalesReturnOrderCompanyDataGeneralModelRep);
    command.setCustomerRep(customerRep);
    command.setSalesReturnOrderDocumentOwnerRep(salesReturnOrderDocumentOwnerRep);
    command.setSalesReturnOrderItemRep(salesReturnOrderItemRep);
    command.setItemGeneralModelRep(itemGeneralModelRep);
    return command;
  }

  @Bean("DObGoodsReceiptDeleteCommand")
  public DObGoodsReceiptDeleteCommand createGoodsReceiptDeleteCommand(
      DObGoodsReceiptRep goodsReceiptRep,
      @Qualifier("goodsReceiptLockCommand") EntityLockCommand entityLockCommand) {
    DObGoodsReceiptDeleteCommand goodsReceiptDeleteCommand = new DObGoodsReceiptDeleteCommand();
    goodsReceiptDeleteCommand.setdObGoodsReceiptRep(goodsReceiptRep);
    goodsReceiptDeleteCommand.setEntityLockCommand(entityLockCommand);

    return goodsReceiptDeleteCommand;
  }

  @Bean("IObGoodsReceiptItemDifferenceReasonSaveCommand")
  public IObGoodsReceiptItemDifferenceReasonSaveCommand
      createGoodsReceiptItemDifferenceReasonSaveCommand(
          DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep,
          PlatformTransactionManager transactionManager,
          IUserAccountSecurityService userAccountSecurityService,
          IObGoodsReceiptPurchaseOrderReceivedItemsRep receivedItemsRep,
          IObGoodsReceiptReceivedItemsGeneralModelRep recievedItemsGeneralModelRep) {
    IObGoodsReceiptItemDifferenceReasonSaveCommand command =
        new IObGoodsReceiptItemDifferenceReasonSaveCommand();
    command.setDObGoodsReceiptPurchaseOrderRep(dObGoodsReceiptPurchaseOrderRep);
    command.setGoodsReceiptRecievedItemsGeneralModelRep(recievedItemsGeneralModelRep);
    command.setGoodsReceiptRecievedItemsRep(receivedItemsRep);
    command.setTransactionManager(transactionManager);
    command.setUserAccountSecurityService(userAccountSecurityService);
    return command;
  }

  @Bean("GoodsReceiptEntityLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("goodsReceiptLockCommand") EntityLockCommand entityLockCommand) throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=GoodsReceiptEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public IObGoodsReceiptOrdinaryItemAddCommand createDObGoodsReceiptAddItemCommand(
      DObGoodsReceiptPurchaseOrderRep goodsReceiptRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemsDataRep ordinaryRecievedItemsDataRep,
      @Qualifier("goodsReceiptLockCommand") EntityLockCommand entityLockCommand,
      CObMeasureRep measureRep,
      IObGoodsReceiptReceivedItemsGeneralModelRep receiptRecievedItemsGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObGoodsReceiptOrdinaryItemAddCommand addItemCommand =
        new IObGoodsReceiptOrdinaryItemAddCommand();
    addItemCommand.setdObGoodsReceiptPurchaseOrderRep(goodsReceiptRep);
    addItemCommand.setItemGeneralModelRep(itemGeneralModelRep);
    addItemCommand.setMeasureRep(measureRep);
    addItemCommand.setUserAccountSecurityService(userAccountSecurityService);
    addItemCommand.setOrdinaryReceivedItemsDataRep(ordinaryRecievedItemsDataRep);
    addItemCommand.setReceiptReceivedItemsGeneralModelRep(receiptRecievedItemsGeneralModelRep);
    addItemCommand.setEntityLockCommand(entityLockCommand);
    return addItemCommand;
  }

  @Bean
  public IObGoodsReceiptBatchedItemAddCommand createDObGoodsReceiptAddBatchItemCommand(
      DObGoodsReceiptPurchaseOrderRep goodsReceiptRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      IObGoodsReceiptBatchedRecievedItemsRep batchedRecievedItemsDataRep,
      IObGoodsReceiptReceivedItemsGeneralModelRep receiptRecievedItemsGeneralModelRep,
      IUserAccountSecurityService userAccountSecurityService,
      @Qualifier("goodsReceiptLockCommand") EntityLockCommand entityLockCommand) {

    IObGoodsReceiptBatchedItemAddCommand addItemCommand =
        new IObGoodsReceiptBatchedItemAddCommand();
    addItemCommand.setGoodsReceiptRep(goodsReceiptRep);
    addItemCommand.setItemGeneralModelRep(itemGeneralModelRep);
    addItemCommand.setUserAccountSecurityService(userAccountSecurityService);
    addItemCommand.setBatchedReceivedItemsDataRep(batchedRecievedItemsDataRep);
    addItemCommand.setReceiptRecievedItemsGeneralModelRep(receiptRecievedItemsGeneralModelRep);
    addItemCommand.setEntityLockCommand(entityLockCommand);
    return addItemCommand;
  }

  @Bean("goodsReceiptLockCommand")
  public EntityLockCommand<DObGoodsReceipt> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObGoodsReceiptRep goodsReceiptRep) {

    EntityLockCommand<DObGoodsReceipt> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObGoodsReceipt.class.getName()));
    entityLockCommand.setRepository(goodsReceiptRep);
    return entityLockCommand;
  }

  @Bean("goodsReceiptDeleteItemCommand")
  public IObGoodsReceiptDeleteItemCommand createIObGoodsReceiptDeleteItemCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep,
      IObGoodsReceiptPurchaseOrderReceivedItemsRep iObGoodsReceiptRecievedItemsDataRep,
      @Qualifier("goodsReceiptLockCommand") EntityLockCommand entityLockCommand,
      IObGoodsReceiptReceivedItemsGeneralModelRep iObGoodsReceiptReceivedItemsGeneralModelRep) {

    IObGoodsReceiptDeleteItemCommand iObGoodsReceiptDeleteItemCommand =
        new IObGoodsReceiptDeleteItemCommand();
    iObGoodsReceiptDeleteItemCommand.setGoodsReceiptRep(dObGoodsReceiptPurchaseOrderRep);
    iObGoodsReceiptDeleteItemCommand.setGoodsReceiptRecievedItemsDataRep(
        iObGoodsReceiptRecievedItemsDataRep);
    iObGoodsReceiptDeleteItemCommand.setEntityLockCommand(entityLockCommand);
    iObGoodsReceiptDeleteItemCommand.setGoodsReceiptRecievedItemsGeneralModelRep(
        iObGoodsReceiptReceivedItemsGeneralModelRep);
    iObGoodsReceiptDeleteItemCommand.setUserAccountInfoService(userAccountSecurityService);
    return iObGoodsReceiptDeleteItemCommand;
  }

  @Bean("ConcurrentDataAccessManagerJMXBean")
  public ObjectInstance createConcurrentDataAccessManagerJMXBean(
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool) throws Exception {
    IConcurrentDataAccessManager concurrentDataAccessManager =
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObGoodsReceipt.class.getName());
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName(
            "com.ebs.dac.foundation.realization.concurrency:type=ConcurrentDataAccessManager");
    return mbs.registerMBean(concurrentDataAccessManager, name);
  }

  @Bean
  public DObGoodsReceiptDeleteDetailCommand createDObGoodsReceiptDeleteDetailCommand(
      IObGoodsReceiptItemBatchesRep batchesRep,
      IObGoodsReceiptPurchaseOrderItemQuantitiesRep quantitiesRep,
      DObGoodsReceiptPurchaseOrderRep dObGoodsReceiptPurchaseOrderRep,
      IObGoodsReceiptPurchaseOrderReceivedItemsRep goodsReceiptPurchaseOrderReceivedItemsRep,
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep
          goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep,
      IObGoodsReceiptReceivedItemsGeneralModelRep recievedItemsGeneralModelRep,
      DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep,
      IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep,
      PlatformTransactionManager transactionManager,
      IUserAccountSecurityService userAccountSecurityService) {
    DObGoodsReceiptDeleteDetailCommand deleteDetailCommand =
        new DObGoodsReceiptDeleteDetailCommand();
    deleteDetailCommand.setBatchesRep(batchesRep);
    deleteDetailCommand.setGoodsReceiptPurchaseOrderQuantitiesRep(quantitiesRep);
    deleteDetailCommand.setGoodsReceiptPurchaseOrderRep(dObGoodsReceiptPurchaseOrderRep);
    deleteDetailCommand.setGoodsReceiptPurchaseOrderReceivedItemsRep(
        goodsReceiptPurchaseOrderReceivedItemsRep);
    deleteDetailCommand.setGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep(
        goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep);
    deleteDetailCommand.setReceivedItemsGeneralModelRep(recievedItemsGeneralModelRep);
    deleteDetailCommand.setGoodsReceiptSalesReturnRep(goodsReceiptSalesReturnRep);
    deleteDetailCommand.setGoodsReceiptSalesReturnItemQuantitiesRep(
        goodsReceiptSalesReturnItemQuantitiesRep);
    deleteDetailCommand.setTransactionManager(transactionManager);
    deleteDetailCommand.setUserAccountSecurityService(userAccountSecurityService);
    return deleteDetailCommand;
  }

  @Bean
  public DObGoodsReceiptSaveDetailCommand createDObGoodsReceiptSaveDetailCommand(
      IObGoodsReceiptItemBatchesRep batchesRep,
      IObGoodsReceiptPurchaseOrderItemQuantitiesRep goodsReceiptPurchaseOrderItemQuantitiesRep,
      IObGoodsReceiptSalesReturnItemQuantitiesRep goodsReceiptSalesReturnItemQuantitiesRep,
      DObGoodsReceiptPurchaseOrderRep goodsReceiptRep,
      IObGoodsReceiptPurchaseOrderReceivedItemsRep receivedItemsRep,
      IObGoodsReceiptReceivedItemsGeneralModelRep receivedItemsGeneralModelRep,
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          iObGoodsReceiptPurchaseOrderDataGeneralModelRep,
      ItemVendorRecordUnitOfMeasuresGeneralModelRep itemVendorRecordUnitOfMeasuresGeneralModelRep,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep,
      CObMeasureRep measureRep,
      IUserAccountSecurityService userAccountSecurityService,
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep
          goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep,
      DObGoodsReceiptSalesReturnRep goodsReceiptSalesReturnRep) {
    DObGoodsReceiptSaveDetailCommand saveDetailCommand = new DObGoodsReceiptSaveDetailCommand();
    saveDetailCommand.setBatchesRep(batchesRep);
    saveDetailCommand.setGoodsReceiptPurchaseOrderQuantitiesRep(
        goodsReceiptPurchaseOrderItemQuantitiesRep);
    saveDetailCommand.setGoodsReceiptSalesReturnItemQuantitiesRep(
        goodsReceiptSalesReturnItemQuantitiesRep);
    saveDetailCommand.setGoodsReceiptPurchaseOrderRep(goodsReceiptRep);
    saveDetailCommand.setGoodsReceiptPurchaseOrderReceivedItemsRep(receivedItemsRep);
    saveDetailCommand.setReceivedItemsGeneralModelRep(receivedItemsGeneralModelRep);
    saveDetailCommand.setMeasureRep(measureRep);
    saveDetailCommand.setIObGoodsReceiptPurchaseOrderDataGeneralModelRep(
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep);
    saveDetailCommand.setItemVendorRecordUnitOfMeasuresGeneralModelRep(
        itemVendorRecordUnitOfMeasuresGeneralModelRep);
    saveDetailCommand.setItemVendorRecordGeneralModelRep(itemVendorRecordGeneralModelRep);
    saveDetailCommand.setUserAccountSecurityService(userAccountSecurityService);
    saveDetailCommand.setGoodsReceiptSalesReturnRep(goodsReceiptSalesReturnRep);
    saveDetailCommand.setGoodsReceiptSalesReturnOrdinaryReceivedItemsDataRep(
        goodsReceiptSalesReturnOrdinaryReceivedItemsDataRep);
    return saveDetailCommand;
  }
}
