package com.ebs.dda.config;

import com.ebs.dac.dbo.jpa.repositories.usercodes.EntityUserCodeRep;
import com.ebs.dda.codegenerator.*;
import com.ebs.dda.repositories.inventory.stockavailability.CObStockAvailabilityRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import org.springframework.context.annotation.Bean;

public class CodeGeneratorBeans {

    @Bean(name = "MasterDataCodeGenrator")
    public MasterDataCodeGenrator createCodeGeneratorBean(EntityUserCodeRep entityUserCodeRep) {
        MasterDataCodeGenrator masterDataCodeGenrator = new MasterDataCodeGenrator();
        masterDataCodeGenrator.setEntityUserCodeRep(entityUserCodeRep);
        return masterDataCodeGenrator;
    }

    @Bean(name = "DocumentObjectCodeGenerator")
    public DocumentObjectCodeGenerator createDocumentObjectCodeGenerator(EntityUserCodeRep entityUserCodeRep) {
        DocumentObjectCodeGenerator documentObjectCodeGenerator = new DocumentObjectCodeGenerator();
        documentObjectCodeGenerator.setEntityUserCodeRep(entityUserCodeRep);
        return documentObjectCodeGenerator;
    }

    @Bean(name = "ChartOfAccountCodeGenerator")
    public ChartOfAccountCodeGenerator chartOfAccountCodeGenerator(HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
        ChartOfAccountCodeGenerator chartOfAccountCodeGenerator = new ChartOfAccountCodeGenerator();
        chartOfAccountCodeGenerator.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
        return chartOfAccountCodeGenerator;
    }

    @Bean(name = "ConfigurationObjectCodeGenrator")
    public ConfigurationObjectCodeGenrator createConfigurationObjectCodeGenratorBean(EntityUserCodeRep entityUserCodeRep) {
        ConfigurationObjectCodeGenrator configurationObjectCodeGenrator = new ConfigurationObjectCodeGenrator();
        configurationObjectCodeGenrator.setEntityUserCodeRep(entityUserCodeRep);
        return configurationObjectCodeGenrator;
    }

    @Bean(name = "JournalBalanceCodeGenerator")
    public JournalBalanceCodeGenerator createJournalBalanceCodeGeneratorBean() {
        return new JournalBalanceCodeGenerator();
    }

    @Bean(name = "StockAvailabilityCodeGenerator")
    public StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator() {
        StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator = new StockAvailabilityCodeGenerator();
        return stockAvailabilityCodeGenerator;
    }
}
