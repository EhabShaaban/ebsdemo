package com.ebs.dda.config.accounting.collection;

import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.collection.statemachines.DObCollectionStateMachine;
import com.ebs.dda.codegenerator.JournalBalanceCodeGenerator;
import com.ebs.dda.commands.accounting.accountingnote.DObDebitNoteUpdateRemainingCommand;
import com.ebs.dda.commands.accounting.collection.DObCollectionActivateCommand;
import com.ebs.dda.commands.accounting.collection.DObCollectionDebitNoteAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.collection.DObCollectionMonetaryNoteAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.collection.DObCollectionSalesInvoiceAndOrderAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceIncreaseBalanceCommand;
import com.ebs.dda.commands.accounting.journalentry.create.collection.DObJournalEntryItemsCollectionCreateCommand;
import com.ebs.dda.commands.accounting.notesreceivables.DObNotesReceivableUpdateRemainingCommand;
import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceUpdateStateToClosedCommand;
import com.ebs.dda.commands.accounting.salesinvoice.IObSalesInvoiceUpdateRemainingCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderUpdateRemainingCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.accounting.accountingnote.UpdateAccountingNoteRemaining;
import com.ebs.dda.functions.executecommand.accounting.collection.CreateCollectionJournalEntry;
import com.ebs.dda.functions.executecommand.accounting.collection.DeterminCollectionAccount;
import com.ebs.dda.functions.executecommand.accounting.journalbalance.IncreaseJournalBalance;
import com.ebs.dda.functions.executecommand.accounting.monetarynote.UpdateNotesReceivableRemaining;
import com.ebs.dda.functions.executecommand.accounting.salesinvoice.CloseSalesInvoice;
import com.ebs.dda.functions.executecommand.accounting.salesinvoice.UpdateSalesInvoiceRemaining;
import com.ebs.dda.functions.executecommand.order.salesorder.UpdateSalesOrderRemaining;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.executequery.accounting.collection.ConstructCollectionJournalBalanceCodeGenrationValueObject;
import com.ebs.dda.functions.executequery.accounting.journalbalance.FindJournalBalance;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.collection.CollectionLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.functions.validate.ValidateSchema;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceGeneralModelRep;
import com.ebs.dda.rest.accounting.collection.activation.ActivateCollection;
import com.ebs.dda.validation.collection.DObCollectionAccountingNoteActivateValidator;
import com.ebs.dda.validation.collection.DObCollectionMonetaryNoteActivateValidator;
import com.ebs.dda.validation.collection.DObCollectionSalesInvoiceActivateValidator;
import com.ebs.dda.validation.collection.DObCollectionSalesOrderActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CollectionFunctionsBeans {

  @Bean("CollectionSalesInvoiceActivate")
  public ActivateCollection activateCollectionSalesInvoice(
      AuthorizationManager authorizationManager, SchemaValidator schemaValidator,
      DObCollectionGeneralModelRep collectionGeneralModelRep,
      EntityLockCommand<DObCollection> collectionLockCmd,
      EntityLockCommand<DObSalesInvoice> salesInvoiceLockCmd,
      CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
      SerializionUtils serializionUtils, DObCollectionSalesInvoiceActivateValidator validator,
      TransactionManagerDelegate transactionManagerDelegate,
      DObCollectionActivateCommand collectionActivateCommand,
      DObCollectionSalesInvoiceAndOrderAccountDeterminationCommand collectionAccountDetermination,
      IObSalesInvoiceUpdateRemainingCommand salesInvoiceUpdateRemainingCommand,
      DObSalesInvoiceUpdateStateToClosedCommand closeSalesInvoiceCommand,
      DObJournalEntryItemsCollectionCreateCommand createCollectionJournalEntryCmd,
      IObCollectionDetailsGeneralModelRep collectionDetailsGMRep,
      CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
      JournalBalanceCodeGenerator journalBalanceCodeGenerator,
      CObJournalBalanceIncreaseBalanceCommand journalBalanceIncreaseBalanceCommand) {

    return context -> {
      CollectionLockActivateEntitiesOfInterest collectionLockManager =
          new CollectionLockActivateEntitiesOfInterest(collectionLockCmd, salesInvoiceLockCmd,
              journalBalanceLockCommandBeanFactory);

      new ActivateDocument().authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
          .validateSchema(new ValidateSchema(schemaValidator))

          .findEntitiesOfInterest(new FindEntityByUserCode<>(collectionGeneralModelRep),
              new ConstructCollectionJournalBalanceCodeGenrationValueObject(collectionDetailsGMRep),
              new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator))

          .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
          .lockEntitiesOfInterest(new LockEntitiesOfInterest(collectionLockManager))
          .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
          .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObCollectionStateMachine()))
          .validateMissingFieldsForState(new ValidateMissingFieldsForState(validator))
          .validateBusinessRules(new ValidateBusinessRules<>(validator))
          .transactionManager(transactionManagerDelegate)
          .executeCommand(new ExecuteCommand<>(collectionActivateCommand))
          .executeActivateDependencies(
              new DeterminCollectionAccount(collectionAccountDetermination),
              new UpdateSalesInvoiceRemaining(salesInvoiceUpdateRemainingCommand),
              new CloseSalesInvoice(closeSalesInvoiceCommand),
              new IncreaseJournalBalance(journalBalanceIncreaseBalanceCommand),
              new CreateCollectionJournalEntry(createCollectionJournalEntryCmd))
          .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(collectionLockManager))
          .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
          .activate(context);
      return context.serializeResponse();
    };
  }

  @Bean("CollectionSalesOrderActivate")
  public ActivateCollection activateCollectionSalesOrder(AuthorizationManager authorizationManager,
      SchemaValidator schemaValidator, DObCollectionGeneralModelRep collectionGeneralModelRep,
      EntityLockCommand<DObCollection> collectionLockCmd,
      EntityLockCommand<DObSalesOrder> salesOrderLockCmd,
      CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
      SerializionUtils serializionUtils, DObCollectionSalesOrderActivateValidator validator,
      TransactionManagerDelegate transactionManagerDelegate,
      DObCollectionActivateCommand collectionActivateCommand,
      DObCollectionSalesInvoiceAndOrderAccountDeterminationCommand collectionAccountDetermination,
      DObSalesOrderUpdateRemainingCommand salesOrderUpdateRemainingCommand,
      DObJournalEntryItemsCollectionCreateCommand createCollectionJournalEntryCmd,
      IObCollectionDetailsGeneralModelRep collectionDetailsGMRep,
      CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
      JournalBalanceCodeGenerator journalBalanceCodeGenerator,
      CObJournalBalanceIncreaseBalanceCommand journalBalanceIncreaseBalanceCommand) {

    return context -> {
      CollectionLockActivateEntitiesOfInterest collectionLockManager =
          new CollectionLockActivateEntitiesOfInterest(collectionLockCmd, salesOrderLockCmd,
              journalBalanceLockCommandBeanFactory);

      new ActivateDocument().authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
          .validateSchema(new ValidateSchema(schemaValidator))

          .findEntitiesOfInterest(new FindEntityByUserCode<>(collectionGeneralModelRep),
              new ConstructCollectionJournalBalanceCodeGenrationValueObject(collectionDetailsGMRep),
              new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator))

          .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
          .lockEntitiesOfInterest(new LockEntitiesOfInterest(collectionLockManager))
          .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
          .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObCollectionStateMachine()))
          .validateMissingFieldsForState(new ValidateMissingFieldsForState(validator))
          .validateBusinessRules(new ValidateBusinessRules<>(validator))
          .transactionManager(transactionManagerDelegate)
          .executeCommand(new ExecuteCommand<>(collectionActivateCommand))
          .executeActivateDependencies(
              new DeterminCollectionAccount(collectionAccountDetermination),
              new UpdateSalesOrderRemaining(salesOrderUpdateRemainingCommand),
              new IncreaseJournalBalance(journalBalanceIncreaseBalanceCommand),
              new CreateCollectionJournalEntry(createCollectionJournalEntryCmd))
          .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(collectionLockManager))
          .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
          .activate(context);
      return context.serializeResponse();
    };
  }

  @Bean("CollectionAccountingNoteActivate")
  public ActivateCollection activateCollectionAccountingNote(
      AuthorizationManager authorizationManager, SchemaValidator schemaValidator,
      DObCollectionGeneralModelRep collectionGeneralModelRep,
      EntityLockCommand<DObCollection> collectionLockCmd,
      EntityLockCommand<DObAccountingNote> accountingNoteLockCmd,
      CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory,
      SerializionUtils serializionUtils, DObCollectionAccountingNoteActivateValidator validator,
      TransactionManagerDelegate transactionManagerDelegate,
      DObCollectionActivateCommand collectionActivateCommand,
      DObCollectionDebitNoteAccountDeterminationCommand collectionAccountDetermination,
      DObDebitNoteUpdateRemainingCommand accountingNoteUpdateRemainingCommand,
      DObJournalEntryItemsCollectionCreateCommand createCollectionJournalEntryCmd,
      IObCollectionDetailsGeneralModelRep collectionDetailsGMRep,
      CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
      JournalBalanceCodeGenerator journalBalanceCodeGenerator,
      CObJournalBalanceIncreaseBalanceCommand journalBalanceIncreaseBalanceCommand) {

    return context -> {
      CollectionLockActivateEntitiesOfInterest collectionLockManager =
          new CollectionLockActivateEntitiesOfInterest(collectionLockCmd, accountingNoteLockCmd,
              journalBalanceLockCommandBeanFactory);

      new ActivateDocument().authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
          .validateSchema(new ValidateSchema(schemaValidator))

          .findEntitiesOfInterest(new FindEntityByUserCode<>(collectionGeneralModelRep),
              new ConstructCollectionJournalBalanceCodeGenrationValueObject(collectionDetailsGMRep),
              new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator))

          .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
          .lockEntitiesOfInterest(new LockEntitiesOfInterest(collectionLockManager))
          .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
          .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObCollectionStateMachine()))
          .validateMissingFieldsForState(new ValidateMissingFieldsForState(validator))
          .validateBusinessRules(new ValidateBusinessRules<>(validator))
          .transactionManager(transactionManagerDelegate)
          .executeCommand(new ExecuteCommand<>(collectionActivateCommand))
          .executeActivateDependencies(
              new DeterminCollectionAccount(collectionAccountDetermination),
              new UpdateAccountingNoteRemaining(accountingNoteUpdateRemainingCommand),
              new IncreaseJournalBalance(journalBalanceIncreaseBalanceCommand),
              new CreateCollectionJournalEntry(createCollectionJournalEntryCmd))
          .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(collectionLockManager))
          .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
          .activate(context);
      return context.serializeResponse();
    };
  }

  @Bean("CollectionMonetaryNotesActivate")
  public ActivateCollection activateCollectionMonetaryNotes(AuthorizationManager authorizationManager, SchemaValidator schemaValidator,
    DObCollectionGeneralModelRep collectionGeneralModelRep, EntityLockCommand<DObCollection> collectionLockCmd,
    EntityLockCommand<DObMonetaryNotes> monetaryNotesLockCmd,
    CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory, SerializionUtils serializionUtils, DObCollectionMonetaryNoteActivateValidator validator,
    TransactionManagerDelegate transactionManagerDelegate, DObCollectionActivateCommand collectionActivateCommand,
    DObCollectionMonetaryNoteAccountDeterminationCommand collectionAccountDetermination,
    DObJournalEntryItemsCollectionCreateCommand createCollectionJournalEntryCmd,
    IObCollectionDetailsGeneralModelRep collectionDetailsGMRep, CObJournalBalanceGeneralModelRep journalBalanceGeneralModelRep,
    JournalBalanceCodeGenerator journalBalanceCodeGenerator,
    CObJournalBalanceIncreaseBalanceCommand journalBalanceIncreaseBalanceCommand,
    DObNotesReceivableUpdateRemainingCommand updateRemainingCommand) {

    return context -> {
      CollectionLockActivateEntitiesOfInterest collectionLockManager =
          new CollectionLockActivateEntitiesOfInterest(collectionLockCmd, monetaryNotesLockCmd,
              journalBalanceLockCommandBeanFactory);

      new ActivateDocument().authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
          .validateSchema(new ValidateSchema(schemaValidator))

          .findEntitiesOfInterest(new FindEntityByUserCode<>(collectionGeneralModelRep),
              new ConstructCollectionJournalBalanceCodeGenrationValueObject(collectionDetailsGMRep),
              new FindJournalBalance(journalBalanceGeneralModelRep, journalBalanceCodeGenerator))

          .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
          .lockEntitiesOfInterest(new LockEntitiesOfInterest(collectionLockManager))
          .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
          .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(new DObCollectionStateMachine()))
          .validateMissingFieldsForState(new ValidateMissingFieldsForState(validator))
          .validateBusinessRules(new ValidateBusinessRules<>(validator))
          .transactionManager(transactionManagerDelegate)
          .executeCommand(new ExecuteCommand<>(collectionActivateCommand))
          .executeActivateDependencies(
              new DeterminCollectionAccount(collectionAccountDetermination),
              new UpdateNotesReceivableRemaining(updateRemainingCommand),
              new IncreaseJournalBalance(journalBalanceIncreaseBalanceCommand),
              new CreateCollectionJournalEntry(createCollectionJournalEntryCmd))
          .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(collectionLockManager))
          .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
          .activate(context);
      return context.serializeResponse();
    };
  }
}
