package com.ebs.dda.config.accounting.costing;

import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.costing.DObCostingGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.validation.costing.DObCostingActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObCostingValidationBeans {

  @Bean
  public DObCostingActivateValidator createCostingValidator(
      DObCostingGeneralModelRep costingGeneralModelRep,
      CObFiscalYearRep fiscalYearRep,
      DObLandedCostGeneralModelRep landedCostGeneralModelRep,
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep) {
    DObCostingActivateValidator costingActivationValidator = new DObCostingActivateValidator();
    costingActivationValidator.setCostingGeneralModelRep(costingGeneralModelRep);
    costingActivationValidator.setFiscalYearRep(fiscalYearRep);
    costingActivationValidator.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
    costingActivationValidator.setCostingDetailsGeneralModelRep(costingDetailsGeneralModelRep);
    return costingActivationValidator;
  }
}
