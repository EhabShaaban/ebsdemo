package com.ebs.dda.config.order.salesreturnorder;

import com.ebs.dda.order.salesreturnorder.IObSalesReturnOrderItemsEditabilityManger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObSalesReturnOrderEditabilityManagerBeans {

  @Bean
  public IObSalesReturnOrderItemsEditabilityManger createSalesReturnOrderItemsEditabilityManager() {
    IObSalesReturnOrderItemsEditabilityManger salesReturnOrderItemsEditabilityManger =
        new IObSalesReturnOrderItemsEditabilityManger();
    return salesReturnOrderItemsEditabilityManger;
  }
}
