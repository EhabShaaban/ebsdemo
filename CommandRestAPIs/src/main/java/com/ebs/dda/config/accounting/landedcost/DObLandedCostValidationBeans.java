package com.ebs.dda.config.accounting.landedcost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ebs.dda.accounting.landedcost.validation.DObLandedCostConvertToActualValidator;
import com.ebs.dda.accounting.landedcost.validation.DObLandedCostSaveItemValidator;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostFactorItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;

@Configuration
public class DObLandedCostValidationBeans {

	@Bean
	public DObLandedCostConvertToActualValidator landedCostConvertToActualValidator(
					DObLandedCostGeneralModelRep landedCostGeneralModelRep,
					IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep) {
		DObLandedCostConvertToActualValidator landedCostConvertToActualValidator = new DObLandedCostConvertToActualValidator();
		landedCostConvertToActualValidator.setLandedCostGeneralModelMRep(landedCostGeneralModelRep);
		landedCostConvertToActualValidator
						.setLandedCostDetailsGeneralModelRep(landedCostDetailsGeneralModelRep);
		return landedCostConvertToActualValidator;
	}

	@Bean
	public DObLandedCostSaveItemValidator landedCostSaveItemValidator(
					MObItemGeneralModelRep itemGeneralModelRep,
					IObLandedCostFactorItemsGeneralModelRep costFactorItemsGeneralModelRep) {
		DObLandedCostSaveItemValidator validator = new DObLandedCostSaveItemValidator();
		validator.setItemGeneralModelRep(itemGeneralModelRep);
		validator.setCostFactorItemsGeneralModelRep(costFactorItemsGeneralModelRep);
		return validator;
	}

}
