package com.ebs.dda.config.purchaseorder;
// Created By xerix - 5/2/18 - 10:21 AM

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.CObControlPointRep;
import com.ebs.dda.approval.dbo.jpa.repositories.businessobjects.informationObjects.IObControlPointUsersRep;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.attachement.LObAttachmentTypeRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObOrderDocumentRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApprovalCycleRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderApproverRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.*;
import com.ebs.dda.purchases.editability.DObPurchasePaymentAndDeliverySectionEditabilityManager;
import com.ebs.dda.purchases.validation.mandatoryvalidator.*;
import com.ebs.dda.purchases.validation.validator.*;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptPurchaseOrderRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.lookups.LObContainerRequirementRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIncotermsRep;
import com.ebs.dda.repositories.masterdata.lookups.LObPortRep;
import com.ebs.dda.repositories.masterdata.measure.CObUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.shipping.dbo.jpa.repositories.businessobjects.lookups.LObModeOfTransportRep;
import com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects.CObShippingRep;
import com.ebs.dda.validation.purchaseorder.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObPurchaseOrderValidationBeans {

  @Bean
  public DObPurchaseOrderShippedValidator createDObPurchaseOrderShippedValidator(
      DObPurchaseOrderRep purchaseOrderRep,
      DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep) {
    DObPurchaseOrderShippedValidator validationManager = new DObPurchaseOrderShippedValidator();

    validationManager.setPurchaseOrderCycleDatesGeneralModelRep(
        purchaseOrderCycleDatesGeneralModelRep);
    validationManager.setPurchaseOrderRep(purchaseOrderRep);

    return validationManager;
  }

  @Bean
  public DObPurchaseOrderCreateValidator createDObPurchaseOrderCreateValidator(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep,
      MObVendorGeneralModelRep vendorRep,
      DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep,
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    DObPurchaseOrderCreateValidator validator = new DObPurchaseOrderCreateValidator();
    validator.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    validator.setVendorRep(vendorRep);
    validator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    validator.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    return validator;
  }

  @Bean
  public DObPurchaseOrderConsigneeValidator createDObPurchaseOrderConsigneeValidator(
      CObPlantRep plantRep,
      CObStorehouseRep storehouseRep,
      CObCompanyGeneralModelRep companyGeneralModelRep,
      CObPlantGeneralModelRep plantGeneralModelRep,
      CObStorehouseGeneralModelRep storehouseGeneralModelRep) {
    DObPurchaseOrderConsigneeValidator purchaseOrderConsigneeValidator =
        new DObPurchaseOrderConsigneeValidator();
    purchaseOrderConsigneeValidator.setcObPlantRep(plantRep);
    purchaseOrderConsigneeValidator.setcObStorehouseRep(storehouseRep);
    purchaseOrderConsigneeValidator.setcObCompanyGeneralModelRep(companyGeneralModelRep);
    purchaseOrderConsigneeValidator.setcObPlantGeneralModelRep(plantGeneralModelRep);
    purchaseOrderConsigneeValidator.setcObStorehouseGeneralModelRep(storehouseGeneralModelRep);
    return purchaseOrderConsigneeValidator;
  }

  @Bean
  public DObPurchaseOrderCompanyValidator createDObPurchaseOrderCompanyValidator(
      CObCompanyGeneralModelRep companyGeneralModelRep,
      IObCompanyBankDetailsRep companyBankDetailsRep,
      IObOrderCompanyGeneralModelRep orderCompanyGeneralModelRep) {
    DObPurchaseOrderCompanyValidator dObPurchaseOrderCompanyValidator =
        new DObPurchaseOrderCompanyValidator();

    dObPurchaseOrderCompanyValidator.setCompanyGeneralModelRep(companyGeneralModelRep);
    dObPurchaseOrderCompanyValidator.setCompanyBankDetailsRep(companyBankDetailsRep);
    dObPurchaseOrderCompanyValidator.setOrderCompanyGeneralModelRep(orderCompanyGeneralModelRep);

    return dObPurchaseOrderCompanyValidator;
  }

  @Bean
  public DObPurchaseOrderPaymentAndDeliveryValidator
      createDObPurchaseOrderPaymentAndDeliveryValidator(
          LObIncotermsRep lObIncotermsRep,
          CObCurrencyRep cObCurrencyRep,
          CObShippingRep cObShippingRep,
          LObModeOfTransportRep lObModeOfTransportRep,
          LObPortRep lObPortRep,
          CObPaymentTermsRep cobPaymentTermsRep,
          LObContainerRequirementRep lObContainerRequirementRep,
          DObPurchasePaymentAndDeliverySectionEditabilityManager editabilityManager) {

    DObPurchaseOrderPaymentAndDeliveryValidator dObPurchaseOrderPaymentAndDeliveryValidator =
        new DObPurchaseOrderPaymentAndDeliveryValidator();

    dObPurchaseOrderPaymentAndDeliveryValidator.setLObIncotermsRep(lObIncotermsRep);
    dObPurchaseOrderPaymentAndDeliveryValidator.setCObCurrencyRep(cObCurrencyRep);
    dObPurchaseOrderPaymentAndDeliveryValidator.setCObShippingRep(cObShippingRep);
    dObPurchaseOrderPaymentAndDeliveryValidator.setLObModeOfTransportRep(lObModeOfTransportRep);
    dObPurchaseOrderPaymentAndDeliveryValidator.setLObPortRep(lObPortRep);
    dObPurchaseOrderPaymentAndDeliveryValidator.setCobPaymentTermsRep(cobPaymentTermsRep);
    dObPurchaseOrderPaymentAndDeliveryValidator.setLObContainerRequirementRep(
        lObContainerRequirementRep);
    dObPurchaseOrderPaymentAndDeliveryValidator.setPaymentAndDeliverySectionEditabilityManager(
        editabilityManager);

    return dObPurchaseOrderPaymentAndDeliveryValidator;
  }

  @Bean
  public DObPurchaseOrderPaymentAndDeliveryMandatoryValidator
      createDObPurchaseOrderPaymentAndDeliveryMandatoryValidator(
          DObPurchaseOrderRep DObPurchaseOrderRep,
          DObPaymentAndDeliveryGeneralModelRep paymentAndDeliveryGeneralModelRep) {

    DObPurchaseOrderPaymentAndDeliveryMandatoryValidator paymentAndDeliveryMandatoryValidator =
        new DObPurchaseOrderPaymentAndDeliveryMandatoryValidator();
    paymentAndDeliveryMandatoryValidator.setdObOrderRep(DObPurchaseOrderRep);
    paymentAndDeliveryMandatoryValidator.setPaymentAndDeliveryGeneralModelRep(
        paymentAndDeliveryGeneralModelRep);
    return paymentAndDeliveryMandatoryValidator;
  }

  @Bean
  public DObPurchaseOrderApproveAndRejectValidator createDObPurchaseOrderApproveValidator(
      IUserAccountSecurityService userAccountSecurityService,
      DObPurchaseOrderRep orderRep,
      IObOrderApprovalCycleRep approvalCycleRep,
      IObOrderApproverRep approverRep) {
    DObPurchaseOrderApproveAndRejectValidator purchaseOrderApproveValidator =
        new DObPurchaseOrderApproveAndRejectValidator();
    purchaseOrderApproveValidator.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderApproveValidator.setPurchaseOrderRep(orderRep);
    purchaseOrderApproveValidator.setApprovalCycleRep(approvalCycleRep);
    purchaseOrderApproveValidator.setApproverRep(approverRep);
    return purchaseOrderApproveValidator;
  }

  @Bean
  public DObPurchaseOrderReplaceApproverValidator createDObPurchaseOrderReplaceApproveValidator(
      IUserAccountSecurityService userAccountSecurityService,
      DObPurchaseOrderRep orderRep,
      IObOrderApprovalCycleRep approvalCycleRep,
      CObControlPointRep controlPointRep,
      IObControlPointUsersRep controlPointUsersRep,
      IObOrderApproverRep approverRep) {
    DObPurchaseOrderReplaceApproverValidator purchaseOrderApproveValidator =
        new DObPurchaseOrderReplaceApproverValidator();
    purchaseOrderApproveValidator.setUserAccountSecurityService(userAccountSecurityService);
    purchaseOrderApproveValidator.setPurchaseOrderRep(orderRep);
    purchaseOrderApproveValidator.setApprovalCycleRep(approvalCycleRep);
    purchaseOrderApproveValidator.setApproverRep(approverRep);
    purchaseOrderApproveValidator.setControlPointRep(controlPointRep);
    purchaseOrderApproveValidator.setControlPointUsersRep(controlPointUsersRep);
    return purchaseOrderApproveValidator;
  }

  @Bean
  public IObOrderDocumentRequiredDocumentsMandatoryValidator
      createIObPurchaseOrderRequiredDocumentsMandatoryValidator(
          IObOrderDocumentRequiredDocumentsGeneralModelRep
              orderDocumentRequiredDocumentsGeneralModelRep,
          DObOrderDocumentRep orderDocumentRep) {
    IObOrderDocumentRequiredDocumentsMandatoryValidator
        purchaseOrderRequiredDocumentsMandatoryValidator =
            new IObOrderDocumentRequiredDocumentsMandatoryValidator();
    purchaseOrderRequiredDocumentsMandatoryValidator.setOrderDocumentRep(orderDocumentRep);
    purchaseOrderRequiredDocumentsMandatoryValidator
        .setOrderDocumentRequiredDocumentsGeneralModelRep(
            orderDocumentRequiredDocumentsGeneralModelRep);
    return purchaseOrderRequiredDocumentsMandatoryValidator;
  }

  @Bean
  public IObOrderDocumentRequiredDocumentsValidator
      createIObPurchaseOrderRequiredDocumentsValidator(LObAttachmentTypeRep lObAttachmentTypeRep) {
    IObOrderDocumentRequiredDocumentsValidator purchaseOrderRequiredDocumentsValidator =
        new IObOrderDocumentRequiredDocumentsValidator();
    purchaseOrderRequiredDocumentsValidator.setlObAttachmentTypeRep(lObAttachmentTypeRep);
    return purchaseOrderRequiredDocumentsValidator;
  }

  @Bean
  public DObPurchaseOrderConsigneeMandatoryValidator
      createDObPurchaseOrderConsigneeValueObjectMandatoryValidator(
          DObPurchaseOrderRep orderRep,
          DObPurchaseOrderConsigneeGeneralModelRep consigneeGeneralModelRep) {
    DObPurchaseOrderConsigneeMandatoryValidator purchaseOrderGeneralDataMandatoryValidator =
        new DObPurchaseOrderConsigneeMandatoryValidator();
    purchaseOrderGeneralDataMandatoryValidator.setdObOrderRep(orderRep);
    purchaseOrderGeneralDataMandatoryValidator.setConsigneeGeneralModelRep(
        consigneeGeneralModelRep);
    return purchaseOrderGeneralDataMandatoryValidator;
  }

  @Bean
  public DObPurchaseOrderConfirmationValidator createDObPurchaseOrderConfirmationValidator(
      DObPurchaseOrderRep orderRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep,
      IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep,
      DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep,
      IObOrderItemGeneralModelRep orderItemGeneralModelRep) {
    DObPurchaseOrderConfirmationValidator purchaseOrderConfirmationValidator =
        new DObPurchaseOrderConfirmationValidator();
    purchaseOrderConfirmationValidator.setPurchaseOrderRep(orderRep);
    purchaseOrderConfirmationValidator.setPurchaseOrderCycleDatesGeneralModelRep(
        purchaseOrderCycleDatesGeneralModelRep);
    purchaseOrderConfirmationValidator.setPurchaseOrderGeneralModelRep(
        purchaseOrderGeneralModelRep);
    purchaseOrderConfirmationValidator.setOrderPaymentDetailsGeneralModelRep(
        orderPaymentDetailsGeneralModelRep);
    purchaseOrderConfirmationValidator.setOrderItemGeneralModelRep(orderItemGeneralModelRep);
    return purchaseOrderConfirmationValidator;
  }

  @Bean
  public DObPurchaseOrderItemsMandatoryValidator createIObPurchaseOrderItemsMandatoryValidator(
      IObOrderLineDetailsGeneralModelRep itemsGeneralModel,
      IObOrderLineDetailsQuantitiesGeneralModelRep itemQuantitiesGeneralModel,
      DObPurchaseOrderRep orderRep) {
    DObPurchaseOrderItemsMandatoryValidator itemsMandatoryValidator =
        new DObPurchaseOrderItemsMandatoryValidator();
    itemsMandatoryValidator.setLineDetailsGeneralModelRep(itemsGeneralModel);
    itemsMandatoryValidator.setQuantitiesGeneralModelRep(itemQuantitiesGeneralModel);
    itemsMandatoryValidator.setOrderRep(orderRep);
    return itemsMandatoryValidator;
  }

  @Bean
  public IObOrderLineDetailesValidator createIObOrderLineDetailesValidator(
      DObPurchaseOrderGeneralModelRep purchaseOrderHeaderGeneralModelRep,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep,
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep) {
    IObOrderLineDetailesValidator orderLineDetailesValidator = new IObOrderLineDetailesValidator();
    orderLineDetailesValidator.setPurchaseOrderHeaderGeneralModelRep(
        purchaseOrderHeaderGeneralModelRep);
    orderLineDetailesValidator.setItemVendorRecordGeneralModelRep(itemVendorRecordGeneralModelRep);
    orderLineDetailesValidator.setOrderLineDetailsGeneralModelRep(orderLineDetailsGeneralModelRep);
    return orderLineDetailesValidator;
  }

  @Bean
  public DObPurchaseOrderGeneralModelsMandatoryValidator
      createDObPurchaseOrderGeneralModelsMandatoryValidator(
          DObPurchaseOrderConsigneeMandatoryValidator consigneeMandatoryValidator,
          IObOrderDocumentRequiredDocumentsMandatoryValidator requiredDocumentsMandatoryValidator,
          DObPurchaseOrderPaymentAndDeliveryMandatoryValidator paymentAndDeliveryMandatoryValidator,
          DObPurchaseOrderItemsMandatoryValidator itemsMandatoryValidator) {
    DObPurchaseOrderGeneralModelsMandatoryValidator generalModelsMandatoryValidator =
        new DObPurchaseOrderGeneralModelsMandatoryValidator();
    generalModelsMandatoryValidator.setConsigneeMandatoryValidator(consigneeMandatoryValidator);
    generalModelsMandatoryValidator.setRequiredDocumentsMandatoryValidator(
        requiredDocumentsMandatoryValidator);
    generalModelsMandatoryValidator.setPaymentAndDeliveryMandatoryValidator(
        paymentAndDeliveryMandatoryValidator);
    generalModelsMandatoryValidator.setItemsMandatoryValidator(itemsMandatoryValidator);
    return generalModelsMandatoryValidator;
  }

  @Bean
  public DObPurchaseOrderMarkAsProductionFinishedValidator
      createDObPurchaseOrderMarkAsProductionFinishedValidator(
          DObPurchaseOrderRep purchaseOrderRep,
          DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep) {
    DObPurchaseOrderMarkAsProductionFinishedValidator
        purchaseOrderMarkAsProductionFinishedValidator =
            new DObPurchaseOrderMarkAsProductionFinishedValidator();
    purchaseOrderMarkAsProductionFinishedValidator.setPurchaseOrderCycleDatesGeneralModelRep(
        purchaseOrderCycleDatesGeneralModelRep);
    purchaseOrderMarkAsProductionFinishedValidator.setPurchaseOrderRep(purchaseOrderRep);

    return purchaseOrderMarkAsProductionFinishedValidator;
  }

  @Bean
  public DObPurchaseOrderArrivedValidator createDObPurchaseOrderArrivedValidator(
      DObPurchaseOrderRep purchaseOrderRep,
      DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep) {
    DObPurchaseOrderArrivedValidator purchaseOrderArrivedValidator =
        new DObPurchaseOrderArrivedValidator();

    purchaseOrderArrivedValidator.setPurchaseOrderCycleDatesGeneralModelRep(
        purchaseOrderCycleDatesGeneralModelRep);
    purchaseOrderArrivedValidator.setPurchaseOrderRep(purchaseOrderRep);

    return purchaseOrderArrivedValidator;
  }

  @Bean
  public DObPurchaseOrderMarkAsClearedValidator createDObPurchaseOrderMarkAsClearedValidator(
      DObPurchaseOrderRep purchaseOrderRep,
      DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep) {
    DObPurchaseOrderMarkAsClearedValidator purchaseOrderClearanceValidator =
        new DObPurchaseOrderMarkAsClearedValidator();
    purchaseOrderClearanceValidator.setPurchaseOrderCycleDatesGeneralModelRep(
        purchaseOrderCycleDatesGeneralModelRep);
    purchaseOrderClearanceValidator.setPurchaseOrderRep(purchaseOrderRep);

    return purchaseOrderClearanceValidator;
  }

  @Bean
  public DObPurchaseOrderMarkAsDeliveryCompleteValidator
      createDObPurchaseOrderMarkAsDeliveryCompleteValidator(
          DObPurchaseOrderRep purchaseOrderRep,
          DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep,
          IObGoodsReceiptPurchaseOrderDataGeneralModelRep
              goodsReceiptPurchaseOrderDataGeneralModelRep,
          DObGoodsReceiptPurchaseOrderRep goodsReceiptRep) {
    DObPurchaseOrderMarkAsDeliveryCompleteValidator purchaseOrderDeliveryCompleteValidator =
        new DObPurchaseOrderMarkAsDeliveryCompleteValidator();
    purchaseOrderDeliveryCompleteValidator.setGoodsReceiptPurchaseOrderDataGeneralModelRep(
        goodsReceiptPurchaseOrderDataGeneralModelRep);
    purchaseOrderDeliveryCompleteValidator.setGoodsReceiptRep(goodsReceiptRep);

    purchaseOrderDeliveryCompleteValidator.setPurchaseOrderCycleDatesGeneralModelRep(
        purchaseOrderCycleDatesGeneralModelRep);
    purchaseOrderDeliveryCompleteValidator.setPurchaseOrderRep(purchaseOrderRep);

    return purchaseOrderDeliveryCompleteValidator;
  }

  @Bean(name = "purchaseOrderStateTransitionCommonValidator")
  public DObPurchaseOrderStateTransitionCommonValidator
      createDObPurchaseOrderStateTransitionCommonValidator(
          DObPurchaseOrderRep purchaseOrderRep,
          DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep) {
    DObPurchaseOrderStateTransitionCommonValidator purchaseOrderCommonValidator =
        new DObPurchaseOrderStateTransitionCommonValidator();
    purchaseOrderCommonValidator.setPurchaseOrderRep(purchaseOrderRep);
    purchaseOrderCommonValidator.setPurchaseOrderCycleDatesGeneralModelRep(
        purchaseOrderCycleDatesGeneralModelRep);
    return purchaseOrderCommonValidator;
  }

  @Bean
  public DObPurchaseOrderSaveItemQuantityValidator createPurchaseOrderSaveItemQuantityValidator(
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep,
      IObOrderLineDetailsGeneralModelRep orderLineDetailsGeneralModelRep,
      IObOrderLineDetailsQuantitiesGeneralModelRep orderLineDetailsQuantitiesGeneralModelRep) {
    DObPurchaseOrderSaveItemQuantityValidator validator =
        new DObPurchaseOrderSaveItemQuantityValidator();
    validator.setItemVendorRecordGeneralModelRep(itemVendorRecordGeneralModelRep);
    validator.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    validator.setOrderLineDetailsGeneralModelRep(orderLineDetailsGeneralModelRep);
    validator.setOrderLineDetailsQuantitiesGeneralModelRep(
        orderLineDetailsQuantitiesGeneralModelRep);
    return validator;
  }

  @Bean
  public IObServicePurchaseOrderSavePaymentDetailsValidator
      createIObServicePurchaseOrderSavePaymentDetailsValidator(
          CObPaymentTermsRep paymentTermsRep, CObCurrencyRep currencyRep) {
    IObServicePurchaseOrderSavePaymentDetailsValidator validator =
        new IObServicePurchaseOrderSavePaymentDetailsValidator();
    validator.setPaymentTermsRep(paymentTermsRep);
    validator.setCurrencyRep(currencyRep);
    return validator;
  }

  @Bean
  public IObPurchaseOrderItemSaveAddMandatoryValidator
      createIObPurchaseOrderItemSaveAddMandatoryValidator(
          DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep) {
    IObPurchaseOrderItemSaveAddMandatoryValidator validator =
        new IObPurchaseOrderItemSaveAddMandatoryValidator();
    validator.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    return validator;
  }

  @Bean
  public IObPurchaseOrderItemSaveAddValidator createIObPurchaseOrderItemSaveAddValidator(
      IObOrderItemGeneralModelRep orderItemGeneralModelRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      CObUoMGeneralModelRep uomGeneralModelRep) {
    IObPurchaseOrderItemSaveAddValidator validator = new IObPurchaseOrderItemSaveAddValidator();
    validator.setPurchaseOrderItemsGeneralModelRep(orderItemGeneralModelRep);
    validator.setItemGeneralModelRep(itemGeneralModelRep);
    validator.setUomGeneralModelRep(uomGeneralModelRep);
    return validator;
  }
}
