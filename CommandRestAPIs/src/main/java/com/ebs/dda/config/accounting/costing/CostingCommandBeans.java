package com.ebs.dda.config.accounting.costing;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.accounting.costing.DObCostingStateMachine;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.costing.*;
import com.ebs.dda.commands.accounting.costing.services.CostingExchangeRateService;
import com.ebs.dda.commands.accounting.costing.services.CreateCostingItemsSectionService;
import com.ebs.dda.commands.accounting.costing.services.DObCostingAccountingDetailsCreateService;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.costing.*;
import com.ebs.dda.repositories.accounting.journalentry.DObCostingJournalEntryRep;
import com.ebs.dda.repositories.accounting.journalentry.DObJournalEntryGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceSummaryRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CostingCommandBeans {

  private static final Logger logger = LoggerFactory.getLogger(CostingCommandBeans.class);

  @Bean
  public DObCostingCreateCommand createCostingCreateCommand(
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep,
      DObCostingRep costingRep,
      DocumentObjectCodeGenerator codeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      CostingExchangeRateStrategyFactory costingExchangeRateStrategyFactory,
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
      IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep,
      DObLandedCostGeneralModelRep landedCostGeneralModelRep,
      CreateCostingItemsSectionService createCostingItemsSectionService) {
    DObCostingCreateCommand command = null;
    try {
      DObCostingStateMachine dObCostingStateMachine = new DObCostingStateMachine();
      command = new DObCostingCreateCommand(codeGenerator, dObCostingStateMachine);
      command.setCostingRep(costingRep);
      command.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
      command.setUserAccountSecurityService(userAccountSecurityService);
      command.setCostingExchangeRateStrategyFactory(costingExchangeRateStrategyFactory);
      command.setVendorInvoiceSummaryRep(vendorInvoiceSummaryRep);
      command.setVendorInvoiceGeneralModelRep(vendorInvoiceGeneralModelRep);
      command.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
      command.setCreateCostingItemsSectionService(createCostingItemsSectionService);
    } catch (Exception e) {
      logger.error("Error creating state machine", e);
    }
    return command;
  }

  @Bean
  public DObCostingClosingJournalCreateCommand createCostingClosingJournalCreateCommand(
      DocumentObjectCodeGenerator codeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      IObCostingCompanyDataGeneralModelRep costingCompanyDataGeneralModelRep,
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep,
      DObCostingJournalEntryRep costingJournalEntryRep,
      IObCostingAccountingDetailsGeneralModelRep costingAccountingDetailsGeneralModelRep,
      CObFiscalYearRep fiscalYearRep,
      CObExchangeRateGeneralModelRep exchangeRateGeneralModelRep) {

    DObCostingClosingJournalCreateCommand command = null;
    try {
      command = new DObCostingClosingJournalCreateCommand(codeGenerator);
      command.setUserAccountSecurityService(userAccountSecurityService);
      command.setCostingCompanyDataGeneralModelRep(costingCompanyDataGeneralModelRep);
      command.setCostingDetailsGeneralModelRep(costingDetailsGeneralModelRep);
      command.setCostingJournalEntryRep(costingJournalEntryRep);
      command.setCostingAccountingDetailsGeneralModelRep(costingAccountingDetailsGeneralModelRep);
      command.setExchangeRateGeneralModelRep(exchangeRateGeneralModelRep);
      command.setFiscalYearRep(fiscalYearRep);
    } catch (Exception e) {
      logger.error("Error creating bean for DObCostingClosingJournalCreateCommand", e);
    }
    return command;
  }

  @Bean
  public DObCostingActivateCommand createCostingActivateCommand(
      DObCostingRep costingRep,
      IUserAccountSecurityService userAccountSecurityService,
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep,
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
      DObCostingAccountingDetailsCreateService costingAccountingDetailsCreateService) {
    DObCostingActivateCommand costingActivateCommand = null;
    try {
      DObCostingStateMachine costingStateMachine = new DObCostingStateMachine();
      costingActivateCommand = new DObCostingActivateCommand(costingStateMachine);
      costingActivateCommand.setCostingRep(costingRep);
      costingActivateCommand.setCostingDetailsGeneralModelRep(costingDetailsGeneralModelRep);
      costingActivateCommand.setVendorInvoiceGeneralModelRep(vendorInvoiceGeneralModelRep);
      costingActivateCommand.setCostingAccountingDetailsCreateService(
          costingAccountingDetailsCreateService);
      costingActivateCommand.setUserAccountSecurityService(userAccountSecurityService);
    } catch (Exception ex) {
      logger.error("Error creating bean for DObCostingActivateCommand", ex);
    }
    return costingActivateCommand;
  }

  @Bean
  public DObCostingUpdateAfterRecostingCommand createCostingUpdateExchangeRateOnActualCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IObCostingDetailsGeneralModelRep costingDetailsGeneralModelRep,
      CostingExchangeRateService costingExchangeRateService,
      IObCostingDetailsRep costingDetailsRep,
      DObCostingRep costingRep,
      DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
      IObCostingItemRep costingItemRep,
      CreateCostingItemsSectionService createCostingItemsSectionService) {
    DObCostingUpdateAfterRecostingCommand command = new DObCostingUpdateAfterRecostingCommand();
    command.setExchangeRateService(costingExchangeRateService);
    command.setCostingRep(costingRep);
    command.setCostingDetailsGeneralModelRep(costingDetailsGeneralModelRep);
    command.setCostingDetailsRep(costingDetailsRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setCreateCostingItemsSectionService(createCostingItemsSectionService);
    command.setVendorInvoiceGeneralModelRep(vendorInvoiceGeneralModelRep);
    command.setCostingItemRep(costingItemRep);
    return command;
  }

  @Bean
  public DObCostingSetActivationDetailsCommand createCostingSetActivationDetailsCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObCostingRep costingRep,
      DObJournalEntryGeneralModelRep journalEntryGeneralModelRep,
      IObJournalEntryItemsGeneralModelRep journalEntryItemsGeneralModelRep) {
    DObCostingSetActivationDetailsCommand command = new DObCostingSetActivationDetailsCommand();
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setCostingRep(costingRep);
    command.setJournalEntryGeneralModelRep(journalEntryGeneralModelRep);
    command.setJournalEntryItemsGeneralModelRep(journalEntryItemsGeneralModelRep);
    return command;
  }
}
