package com.ebs.dda.config.masterdata;

import com.ebs.dda.masterdata.Itemgroup.ItemGroupValidator;
import com.ebs.dda.masterdata.item.MObItemAccountingDetailsValidator;
import com.ebs.dda.masterdata.item.MObItemCreationValidator;
import com.ebs.dda.masterdata.item.MObItemGeneralDataMandatoryValidator;
import com.ebs.dda.masterdata.item.MObItemUOMValidator;
import com.ebs.dda.masterdata.ivr.validation.ItemVendorRecordCreationValidator;
import com.ebs.dda.masterdata.ivr.validation.ItemVendorRecordGeneralDataValidator;
import com.ebs.dda.masterdata.vendor.MObVendorAccountingDetailsValidator;
import com.ebs.dda.masterdata.vendor.MObVendorCreationValidator;
import com.ebs.dda.masterdata.vendor.MObVendorGeneralDataValidator;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.CObMaterialGroupGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorCommonPurchaseUnitsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MasterDataValidationBeans {

  @Bean
  public MObItemCreationValidator itemCreationValidator(
      CObMaterialGroupGeneralModelRep materialGroupGeneralModelRep,
      CObMeasureRep measureRep,
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    MObItemCreationValidator itemCreationValidator = new MObItemCreationValidator();
    itemCreationValidator.setMaterialGroupGeneralModelRep(materialGroupGeneralModelRep);
    itemCreationValidator.setMeasureRep(measureRep);
    itemCreationValidator.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    return itemCreationValidator;
  }

  @Bean
  public MObItemGeneralDataMandatoryValidator createMObItemGeneralDataMandatoryValidator(
      MObItemRep itemRep,
      CObProductManagerRep productManagerRep,
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep) {

    MObItemGeneralDataMandatoryValidator validationManager =
        new MObItemGeneralDataMandatoryValidator();
    validationManager.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    validationManager.setItemRep(itemRep);
    validationManager.setProductManagerRep(productManagerRep);
    validationManager.setItemVendorRecordGeneralModelRep(itemVendorRecordGeneralModelRep);

    return validationManager;
  }

  @Bean
  public MObItemUOMValidator itemUOMValidator(
      CObMeasureRep measureRep, MObItemGeneralModelRep itemGeneralModelRep) {
    MObItemUOMValidator itemUOMValidator = new MObItemUOMValidator();
    itemUOMValidator.setCObMeasureRep(measureRep);
    itemUOMValidator.setItemGeneralModelRep(itemGeneralModelRep);
    return itemUOMValidator;
  }

  @Bean
  public MObItemAccountingDetailsValidator createMObItemAccountingDetailsValidator(
      HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
    MObItemAccountingDetailsValidator validationManager = new MObItemAccountingDetailsValidator();
    validationManager.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
    return validationManager;
  }

  @Bean
  public MObVendorCreationValidator createMObVendorCreationValidator(
          CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep,
          CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
          MObVendorRep vendorRep) {

    MObVendorCreationValidator validationManager = new MObVendorCreationValidator();
    validationManager.setPurchasingResponsibleGeneralModelRep(purchasingResponsibleGeneralModelRep);
    validationManager.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    validationManager.setVendorRep(vendorRep);

    return validationManager;
  }

  @Bean
  public MObVendorGeneralDataValidator createMObVendorGeneralDataValidator(
      CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep,
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep,
      MObVendorGeneralModelRep vendorRecordGeneralModelRep) {

    MObVendorGeneralDataValidator validationManager = new MObVendorGeneralDataValidator();
    validationManager.setPurchasingResponsibleGeneralModelRep(purchasingResponsibleGeneralModelRep);
    validationManager.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    validationManager.setItemVendorRecordGeneralModelRep(itemVendorRecordGeneralModelRep);
    validationManager.setVendorGeneralModelRep(vendorRecordGeneralModelRep);
    return validationManager;
  }

  @Bean
  public MObVendorAccountingDetailsValidator createMObVendorAccountingDetailsValidator(
      HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep) {
    MObVendorAccountingDetailsValidator validationManager =
        new MObVendorAccountingDetailsValidator();
    validationManager.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
    return validationManager;
  }

  @Bean
  public ItemVendorRecordCreationValidator itemVendorRecordCreationValidator(
      MObVendorGeneralModelRep vendorGeneralModelRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      CObCurrencyRep currencyRep,
      IObAlternativeUoMGeneralModelRep alternativeUoMGeneralModelRep,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep,
      ItemVendorCommonPurchaseUnitsGeneralModelRep itemVendorCommonPurchaseUnitsGeneralModelRep) {
    ItemVendorRecordCreationValidator itemVendorRecordCreationValidator =
        new ItemVendorRecordCreationValidator();
    itemVendorRecordCreationValidator.setVendorGeneralModelRep(vendorGeneralModelRep);
    itemVendorRecordCreationValidator.setItemGeneralModelRep(itemGeneralModelRep);
    itemVendorRecordCreationValidator.setCurrencyRep(currencyRep);
    itemVendorRecordCreationValidator.setAlternativeUoMGeneralModelRep(
        alternativeUoMGeneralModelRep);
    itemVendorRecordCreationValidator.setItemVendorRecordGeneralModelRep(
        itemVendorRecordGeneralModelRep);
    itemVendorRecordCreationValidator.setItemVendorCommonPurchaseUnitsGeneralModelRep(
        itemVendorCommonPurchaseUnitsGeneralModelRep);
    return itemVendorRecordCreationValidator;
  }

  @Bean
  public ItemVendorRecordGeneralDataValidator itemVendorRecordGeneralDataValidator(
      CObCurrencyRep currencyRep) {
    ItemVendorRecordGeneralDataValidator itemVendorRecordGeneralDataValidator =
        new ItemVendorRecordGeneralDataValidator();

    itemVendorRecordGeneralDataValidator.setCurrencyRep(currencyRep);

    return itemVendorRecordGeneralDataValidator;
  }

  @Bean
  public ItemGroupValidator itemGroupCreationValidator(
      CObMaterialGroupGeneralModelRep itemGroupGeneralModelRep) {
    ItemGroupValidator itemGroupValidator = new ItemGroupValidator();
    itemGroupValidator.setItemGroupGeneralModelRep(itemGroupGeneralModelRep);
    return itemGroupValidator;
  }
}
