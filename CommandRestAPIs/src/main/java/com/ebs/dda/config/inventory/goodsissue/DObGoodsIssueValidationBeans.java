package com.ebs.dda.config.inventory.goodsissue;

import com.ebs.dda.inventory.goodsissue.validation.DObGoodsIssueCreateValidator;
import com.ebs.dda.inventory.goodsissue.validation.DObGoodsIssueValidator;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.CObGoodsIssueRep;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueRefDocumentDataGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.repositories.inventory.stockavailability.DObStockAvailabilityGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.validation.goodsissue.DObGoodsIssueActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObGoodsIssueValidationBeans {

    @Bean
    public DObGoodsIssueCreateValidator createDObGoodsIssueCreateValidator(
            CObGoodsIssueRep goodsIssueRep,
            CObPurchasingUnitRep purchasingUnitRep,
            DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
            CObPlantGeneralModelRep plantGeneralModelRep,
            CObStorehouseGeneralModelRep storehouseGeneralModelRep,
            StoreKeeperRep storeKeeperRep,
            DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
        DObGoodsIssueCreateValidator validator = new DObGoodsIssueCreateValidator();
        validator.setGoodsIssueRep(goodsIssueRep);
        validator.setPurchasingUnitRep(purchasingUnitRep);
        validator.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
        validator.setPlantGeneralModelRep(plantGeneralModelRep);
        validator.setStorehouseGeneralModelRep(storehouseGeneralModelRep);
        validator.setStoreKeeperRep(storeKeeperRep);
        validator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
        return validator;
    }

    @Bean
    public DObGoodsIssueValidator createGoodsIssueValidator(
            DObSalesOrderGeneralModelRep salesOrderGeneralModelRep,
            IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep,
            IObGoodsIssueItemGeneralModelRep goodsIssueItemGeneralModelRep,
            DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep) {
        DObGoodsIssueValidator sectionsMandatoryValidator = new DObGoodsIssueValidator();
        sectionsMandatoryValidator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
        sectionsMandatoryValidator.setGoodsIssueRefDocumentDataGeneralModelRep(
                goodsIssueRefDocumentDataGeneralModelRep);
        sectionsMandatoryValidator.setGoodsIssueItemsGeneralModelRep(goodsIssueItemGeneralModelRep);
        sectionsMandatoryValidator.setStockAvailabilityGeneralModelRep(
                stockAvailabilityGeneralModelRep);
        return sectionsMandatoryValidator;
    }

    @Bean
    public DObGoodsIssueActivateValidator createGoodsIssueActivateValidator(
            DObSalesOrderGeneralModelRep salesOrderGeneralModelRep,
            IObGoodsIssueRefDocumentDataGeneralModelRep goodsIssueRefDocumentDataGeneralModelRep,
            IObGoodsIssueItemGeneralModelRep goodsIssueItemGeneralModelRep,
            DObStockAvailabilityGeneralModelRep stockAvailabilityGeneralModelRep,
            DObGoodsIssueGeneralModelRep goodsIssueGeneralModelRep) {
        DObGoodsIssueActivateValidator goodsIssueActivateValidator = new DObGoodsIssueActivateValidator();
        goodsIssueActivateValidator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
        goodsIssueActivateValidator.setGoodsIssueRefDocumentDataGeneralModelRep(
                goodsIssueRefDocumentDataGeneralModelRep);
        goodsIssueActivateValidator.setGoodsIssueItemsGeneralModelRep(goodsIssueItemGeneralModelRep);
        goodsIssueActivateValidator.setStockAvailabilityGeneralModelRep(
                stockAvailabilityGeneralModelRep);
        goodsIssueActivateValidator.setGoodsIssueGeneralModelRep(goodsIssueGeneralModelRep);
        return goodsIssueActivateValidator;
    }
}
