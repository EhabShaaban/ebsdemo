package com.ebs.dda.config.order.salesorder;

import com.ebs.dda.order.salesorder.*;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerAddressGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerContactPersonGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.salesresponsible.CObSalesResponsibleRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.*;
import com.ebs.dda.validation.salesorder.IObSalesOrderCompanyAndStoreDataMandatoryValidator;
import com.ebs.dda.validation.salesorder.IObSalesOrderDataSaveMandatoryValidator;
import com.ebs.dda.validation.salesorder.IObSalesOrderItemSaveAddMandatoryValidator;
import com.ebs.dda.validation.salesorder.IObSalesOrderItemSaveAddValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObSalesOrderValidationBeans {

  @Bean
  public DObSalesOrderCreateValidator createDObSalesOrderCreateValidator(
      CObSalesOrderTypeRep salesOrderTypeRep,
      MObCustomerRep customerRep,
      CObPurchasingUnitRep purchasingUnitRep,
      IObCustomerBusinessUnitGeneralModelRep customerBusinessUnitGeneralModelRep,
      CObSalesResponsibleRep salesResponsibleRep,
      CObCompanyGeneralModelRep companyGeneralModelRep) {
    DObSalesOrderCreateValidator validator = new DObSalesOrderCreateValidator();
    validator.setSalesOrderTypeRep(salesOrderTypeRep);
    validator.setCompanyGeneralModelRep(companyGeneralModelRep);
    validator.setCustomerRep(customerRep);
    validator.setPurchasingUnitRep(purchasingUnitRep);
    validator.setCustomerBusinessUnitGeneralModelRep(customerBusinessUnitGeneralModelRep);
    validator.setSalesResponsibleRep(salesResponsibleRep);

    return validator;
  }

  @Bean
  public IObSalesOrderItemSaveAddValidator createIObSalesOrderItemSaveAddValidator(
      IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep) {
    IObSalesOrderItemSaveAddValidator validator = new IObSalesOrderItemSaveAddValidator();
    validator.setSalesOrderItemsGeneralModelRep(salesOrderItemsGeneralModelRep);
    return validator;
  }

  @Bean
  public IObSalesOrderItemSaveAddMandatoryValidator
      createIObSalesOrderItemSaveAddMandatoryValidator(
          DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    IObSalesOrderItemSaveAddMandatoryValidator validator =
        new IObSalesOrderItemSaveAddMandatoryValidator();
    validator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    return validator;
  }

  @Bean
  public IObSalesOrderDataSaveMandatoryValidator createIObSalesOrderDataSaveMandatoryValidator(
      IObCustomerAddressGeneralModelRep customerAddressGeneralModelRep,
      IObCustomerContactPersonGeneralModelRep customerContactPersonGeneralModelRep,
      IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep,
      CObCurrencyRep currencyRep,
      CObPaymentTermsRep paymentTermsRep) {
    IObSalesOrderDataSaveMandatoryValidator validator =
        new IObSalesOrderDataSaveMandatoryValidator();
    validator.setCustomerAddressGeneralModelRep(customerAddressGeneralModelRep);
    validator.setCustomerContactPersonGeneralModelRep(customerContactPersonGeneralModelRep);
    validator.setSalesOrderDataGeneralModelRep(salesOrderDataGeneralModelRep);
    validator.setCurrencyRep(currencyRep);
    validator.setPaymentTermsRep(paymentTermsRep);
    return validator;
  }

  @Bean
  public IObSalesOrderCompanyAndStoreDataMandatoryValidator
      createIObSalesOrderCompanyAndStoreDataMandatoryValidator(
          DObSalesOrderGeneralModelRep salesOrderGeneralModelRep,
          CObStorehouseGeneralModelRep storehouseGeneralModelRep) {
    IObSalesOrderCompanyAndStoreDataMandatoryValidator mandatoryValidator =
        new IObSalesOrderCompanyAndStoreDataMandatoryValidator();
    mandatoryValidator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    mandatoryValidator.setStorehouseGeneralModelRep(storehouseGeneralModelRep);
    return mandatoryValidator;
  }

  @Bean
  public IObSalesOrderCompanyStoreDataMandatoryValidator
      createSalesOrderCompanyStoreDataMandatoryValidator(
          IObSalesOrderCompanyAndStoreDataGeneralModelRep companyAndStoreDataGeneralModelRep) {
    IObSalesOrderCompanyStoreDataMandatoryValidator companyStoreDataMandatoryValidator =
        new IObSalesOrderCompanyStoreDataMandatoryValidator();
    companyStoreDataMandatoryValidator.setCompanyStoreDataRep(companyAndStoreDataGeneralModelRep);
    return companyStoreDataMandatoryValidator;
  }

  @Bean
  public IObSalesOrderDataMandatoryValidator createSalesOrderDataMandatoryValidator(
      IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep,
      DObSalesOrderGeneralModelRep salesOrderRep) {
    IObSalesOrderDataMandatoryValidator salesOrderDataMandatoryValidator =
        new IObSalesOrderDataMandatoryValidator();
    salesOrderDataMandatoryValidator.setSalesOrderDataRep(salesOrderDataGeneralModelRep);
    salesOrderDataMandatoryValidator.setSalesOrderRep(salesOrderRep);
    return salesOrderDataMandatoryValidator;
  }

  @Bean
  public IObSalesOrderItemsMandatoryValidator createSalesOrderItemsMandatoryValidator(
      IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep) {
    IObSalesOrderItemsMandatoryValidator salesOrderItemsMandatoryValidator =
        new IObSalesOrderItemsMandatoryValidator();
    salesOrderItemsMandatoryValidator.setItemsGeneralModelRep(salesOrderItemsGeneralModelRep);
    return salesOrderItemsMandatoryValidator;
  }

  @Bean
  public DObSalesOrderMandatoryValidator createSalesOrderGeneralModelsMandatoryValidator(
      IObSalesOrderCompanyStoreDataMandatoryValidator companyAndStoreDataValidator,
      IObSalesOrderDataMandatoryValidator salesOrderDataMandatoryValidator,
      IObSalesOrderItemsMandatoryValidator salesOrderItemsMandatoryValidator) {
    DObSalesOrderMandatoryValidator salesOrderValidator = new DObSalesOrderMandatoryValidator();
    salesOrderValidator.setCompanyStoreDataMandatoryValidator(companyAndStoreDataValidator);
    salesOrderValidator.setSalesOrderDataMandatoryValidator(salesOrderDataMandatoryValidator);
    salesOrderValidator.setSalesOrderItemsMandatoryValidator(salesOrderItemsMandatoryValidator);
    return salesOrderValidator;
  }
}
