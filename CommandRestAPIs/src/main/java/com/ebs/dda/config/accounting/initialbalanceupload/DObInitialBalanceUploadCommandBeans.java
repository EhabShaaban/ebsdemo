package com.ebs.dda.config.accounting.initialbalanceupload;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.initialbalanceupload.DObInitialBalanceUploadActivateCommand;
import com.ebs.dda.commands.accounting.initialbalanceupload.DObInitialBalanceUploadCreateCommand;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.initialbalanceupload.DObInitialBalanceUploadRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObInitialBalanceUploadCommandBeans {

  @Bean
  public DObInitialBalanceUploadCreateCommand createCommandInitialBalanceUpload(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      CObPurchasingUnitRep purchasingUnitRep,
      CObCompanyRep companyRep,
      DObInitialBalanceUploadRep initialBalanceUploadRep,
      CObFiscalYearRep fiscalYearRep,
      MObVendorGeneralModelRep mObVendorGeneralModelRep,
      HObChartOfAccountsGeneralModelRep hObChartOfAccountsGeneralModelRep,
      CObCurrencyRep currencyRep)
      throws Exception {

    DObInitialBalanceUploadCreateCommand initialBalanceUploadCreateCommand =
        new DObInitialBalanceUploadCreateCommand(documentObjectCodeGenerator);
    initialBalanceUploadCreateCommand.setPurchasingUnitRep(purchasingUnitRep);
    initialBalanceUploadCreateCommand.setCompanyRep(companyRep);
    initialBalanceUploadCreateCommand.setFiscalYearRep(fiscalYearRep);
    initialBalanceUploadCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    initialBalanceUploadCreateCommand.setInitialBalanceUploadRep(initialBalanceUploadRep);
    initialBalanceUploadCreateCommand.setMObVendorGeneralModelRep(mObVendorGeneralModelRep);
    initialBalanceUploadCreateCommand.sethObChartOfAccountsGeneralModelRep(
        hObChartOfAccountsGeneralModelRep);
    initialBalanceUploadCreateCommand.setCurrencyRep(currencyRep);

    return initialBalanceUploadCreateCommand;
  }

  @Bean
  public DObInitialBalanceUploadActivateCommand createDObInitialBalanceUploadActivateCommand(
          DObInitialBalanceUploadRep initialBalanceUploadRep,
          IUserAccountSecurityService userAccountSecurityService
          ){
    DObInitialBalanceUploadActivateCommand initialBalanceUploadActivateCommand = new DObInitialBalanceUploadActivateCommand();
    initialBalanceUploadActivateCommand.setInitialBalanceUploadRep(initialBalanceUploadRep);
    initialBalanceUploadActivateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return initialBalanceUploadActivateCommand;
  }
}
