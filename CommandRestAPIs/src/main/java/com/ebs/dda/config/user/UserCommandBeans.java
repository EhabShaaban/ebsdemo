package com.ebs.dda.config.user;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.security.commands.UserChangePasswordCommand;
import com.ebs.dac.security.jpa.repositories.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserCommandBeans {

  @Bean
  public UserChangePasswordCommand createUserChangePasswordCommand(
      UserRepository userRepository, IUserAccountSecurityService userAccountSecurityService) {
    UserChangePasswordCommand command = new UserChangePasswordCommand();
    command.setUserRepository(userRepository);
    command.setUserAccountSecurityService(userAccountSecurityService);

    return command;
  }
}
