package com.ebs.dda.config.sales.invoice;

import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceSalesOrderBasedStateMachine;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceWithoutReferenceStateMachine;
import com.ebs.dda.commands.accounting.invoice.IObInvoiceTaxCopyCommand;
import com.ebs.dda.commands.accounting.journalentry.create.salesinvoice.DObJournalEntryCreateSalesInvoiceCommand;
import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceAccountDeterminationCommand;
import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoicePostCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderInvoicedCommand;
import com.ebs.dda.functions.activate.ActivateDocument;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.executecommand.accounting.salesinvoice.CopySalesInvoiceTaxes;
import com.ebs.dda.functions.executecommand.accounting.salesinvoice.CreateSalesInvoiceJournalEntry;
import com.ebs.dda.functions.executecommand.accounting.salesinvoice.CreateSalesOrderInvoiced;
import com.ebs.dda.functions.executecommand.accounting.salesinvoice.DetermineSalesInvoiceAccount;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.salesinvoice.SalesInvoiceSalesOrderLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.lock.accounting.salesinvoice.SalesInvoiceWithoutReferenceLockActivateEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.functions.validate.ValidateSchema;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.rest.accounting.salesinvoice.activation.ActivateSalesInvoice;
import com.ebs.dda.validation.salesinvoice.DObSalesInvoiceActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class SalesInvoiceFunctionsBeans {

  @Bean("SalesInvoiceSalesOrderActivate")
  @DependsOn("DObSalesInvoiceActivateValidator")
  public ActivateSalesInvoice activateSalesInvoiceSalesOrder(
          AuthorizationManager authorizationManager,
          SchemaValidator schemaValidator,
          DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
          EntityLockCommand<DObSalesInvoice> salesInvoiceLockCmd,
          EntityLockCommand<DObSalesOrder> salesOrderLockCmd,
          SerializionUtils serializionUtils,
          DObSalesInvoicePostCommand salesInvoiceActivateCommand,
          IObInvoiceTaxCopyCommand invoiceTaxCopyCmd,
          DObJournalEntryCreateSalesInvoiceCommand createSalesInvoiceJournalEntryCmd,
          DObSalesInvoiceAccountDeterminationCommand accountDeterminationCommand,
          DObSalesOrderInvoicedCommand salesOrderInvoicedCmd,
          TransactionManagerDelegate transactionManagerDelegate,
          DObSalesInvoiceActivateValidator validator
  ) {

    return context -> {
      SalesInvoiceSalesOrderLockActivateEntitiesOfInterest salesInvoiceLockManager =
              new SalesInvoiceSalesOrderLockActivateEntitiesOfInterest(salesInvoiceLockCmd, salesOrderLockCmd);

      DObSalesInvoiceSalesOrderBasedStateMachine stateMachine = new DObSalesInvoiceSalesOrderBasedStateMachine();

      new ActivateDocument()
              .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
              .validateSchema(new ValidateSchema(schemaValidator))
              .findEntitiesOfInterest(new FindEntityByUserCode<>(salesInvoiceGeneralModelRep))
              .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
              .lockEntitiesOfInterest(new LockEntitiesOfInterest(salesInvoiceLockManager))
              .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
              .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(stateMachine))
              .validateMissingFieldsForState(new ValidateMissingFieldsForState(validator))
              .validateBusinessRules(new ValidateBusinessRules<>(validator))
              .transactionManager(transactionManagerDelegate)
              .executeCommand(new ExecuteCommand<>(salesInvoiceActivateCommand))
              .executeActivateDependencies(
                      //TODO: add new dependencies here after separation
                      new CopySalesInvoiceTaxes(invoiceTaxCopyCmd),
                      new DetermineSalesInvoiceAccount(accountDeterminationCommand),
                      new CreateSalesInvoiceJournalEntry(createSalesInvoiceJournalEntryCmd),
                      new CreateSalesOrderInvoiced(salesOrderInvoicedCmd)
              )
              .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(salesInvoiceLockManager))
              .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
              .activate(context);
      return context.serializeResponse();
    };
  }

  @Bean("SalesInvoiceWithoutReferenceActivate")
  @DependsOn("DObSalesInvoiceActivateValidator")
  public ActivateSalesInvoice activateSalesInvoiceWithoutReference(
          AuthorizationManager authorizationManager,
          SchemaValidator schemaValidator,
          DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
          EntityLockCommand<DObSalesInvoice> salesInvoiceLockCmd,
          SerializionUtils serializionUtils,
          DObSalesInvoicePostCommand salesInvoiceActivateCommand,
          IObInvoiceTaxCopyCommand invoiceTaxCopyCmd,
          DObJournalEntryCreateSalesInvoiceCommand createSalesInvoiceJournalEntryCmd,
          DObSalesInvoiceAccountDeterminationCommand accountDeterminationCommand,
          TransactionManagerDelegate transactionManagerDelegate,
          DObSalesInvoiceActivateValidator validator
  ) {

    return context -> {
      SalesInvoiceWithoutReferenceLockActivateEntitiesOfInterest salesInvoiceLockManager =
              new SalesInvoiceWithoutReferenceLockActivateEntitiesOfInterest(salesInvoiceLockCmd);

      DObSalesInvoiceWithoutReferenceStateMachine stateMachine = new DObSalesInvoiceWithoutReferenceStateMachine();

      new ActivateDocument()
              .authorizeOnPermission(new AuthorizeOnPermission(authorizationManager))
              .validateSchema(new ValidateSchema(schemaValidator))
              .findEntitiesOfInterest(new FindEntityByUserCode<>(salesInvoiceGeneralModelRep))
              .authorizeOnCondition(new AuthorizeOnCondition(authorizationManager))
              .lockEntitiesOfInterest(new LockEntitiesOfInterest(salesInvoiceLockManager))
              .deserializeValueObject(new DeserializeValueObject<>(serializionUtils))
              .validateIfActionIsAllowed(new ValidateIfActionIsAllowed(stateMachine))
              .validateMissingFieldsForState(new ValidateMissingFieldsForState(validator))
              .validateBusinessRules(new ValidateBusinessRules<>(validator))
              .transactionManager(transactionManagerDelegate)
              .executeCommand(new ExecuteCommand<>(salesInvoiceActivateCommand))
              .executeActivateDependencies(
                      //TODO: add new dependencies here after separation
                      new CopySalesInvoiceTaxes(invoiceTaxCopyCmd),
                      new DetermineSalesInvoiceAccount(accountDeterminationCommand),
                      new CreateSalesInvoiceJournalEntry(createSalesInvoiceJournalEntryCmd)
              )
              .unlockEntitiesOfInterest(new UnlockEntitiesOfInterest(salesInvoiceLockManager))
              .serializeSuccessResponse(new SerializeActivateSuccessResponse(serializionUtils))
              .activate(context);
      return context.serializeResponse();
    };
  }
}
