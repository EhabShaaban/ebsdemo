package com.ebs.dda.config.masterdata;

import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIssueFromRep;
import com.ebs.dda.repositories.masterdata.lookups.LObTaxAdministrativeRep;
import com.ebs.dda.validation.masterdata.customer.MObCustomerCreateMandatoryValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MObCustomerValidationBeans {

  @Bean
  public MObCustomerCreateMandatoryValidator createMObCustomerCreateMandatoryValidator(
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
      LObIssueFromRep issueFromRep,
      LObTaxAdministrativeRep taxAdministrativeRep,
      CObCurrencyRep currencyRep,
      MObCustomerRep customerRep) {
    MObCustomerCreateMandatoryValidator customerCreateMandatoryValidator =
        new MObCustomerCreateMandatoryValidator();
    customerCreateMandatoryValidator.setPurchasingUnitGeneralModelRep(
        purchasingUnitGeneralModelRep);
    customerCreateMandatoryValidator.setIssueFromRep(issueFromRep);
    customerCreateMandatoryValidator.setTaxAdministrativeRep(taxAdministrativeRep);
    customerCreateMandatoryValidator.setCurrencyRep(currencyRep);
    customerCreateMandatoryValidator.setCustomerRep(customerRep);
    return customerCreateMandatoryValidator;
  }
}
