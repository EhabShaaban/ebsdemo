package com.ebs.dda.config.accounting.vendorinvoice;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.vendorinvoice.*;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.masterdata.dbo.jpa.repositories.businessobjects.informationobjects.IObOrderLineDetailsQuantitiesRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderItemRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderLineDetailsRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObPurchaseOrderFulFillerVendorRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPaymentAndDeliveryGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestInvoicePaymentDetailsRep;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesRep;
import com.ebs.dda.repositories.accounting.taxes.LObVendorTaxesRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.*;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class DObVendorInvoiceCommandBeans {

    @Bean("DObGoodsInvoiceCreateCommand")
    public DObGoodsInvoiceCreateCommand createGoodsInvoiceCreateCommand(
            DocumentObjectCodeGenerator documentObjectCodeGenerator,
            IUserAccountSecurityService userAccountSecurityService,
            DObPurchaseOrderRep dObPurchaseOrderRep,
            MObVendorRep mObVendorRep,
            DObPurchaseOrderGeneralModelRep dObPurchaseOrderHeaderGeneralModelRep,
            IObOrderCompanyGeneralModelRep dObPurchaseOrderCompanySectionGeneralModelRep,
            CObPaymentTermsRep cObPaymentTermsRep,
            DObVendorInvoiceRep dObInvoiceRep,
            CObCompanyRep cObCompanyRep,
            CObCurrencyRep cObCurrencyRep,
            CObPurchasingUnitRep purchasingUnitRep,
            DObPaymentAndDeliveryGeneralModelRep paymentAndDeliveryGeneralModelRep,
            IObOrderLineDetailsRep orderLineDetailsRep,
            IObOrderLineDetailsQuantitiesRep quantitiesRep
    )
            throws Exception {
        DObGoodsInvoiceCreateCommand command =
                new DObGoodsInvoiceCreateCommand(documentObjectCodeGenerator);
        command.setVendorRep(mObVendorRep);
        command.setOrderRep(dObPurchaseOrderRep);
        command.setPurchaseOrderHeaderGeneralModelRep(dObPurchaseOrderHeaderGeneralModelRep);
        command.setPurchaseOrderCompanySectionGeneralModelRep(
                dObPurchaseOrderCompanySectionGeneralModelRep);
        command.setPaymentTermsRep(cObPaymentTermsRep);
        command.setVendorInvoiceRep(dObInvoiceRep);
        command.setCompanyRep(cObCompanyRep);
        command.setCurrencyRep(cObCurrencyRep);
        command.setPurchasingUnitRep(purchasingUnitRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setPaymentAndDeliveryGeneralModelRep(paymentAndDeliveryGeneralModelRep);
        command.setOrderLineDetailsRep(orderLineDetailsRep);
        command.setQuantitiesRep(quantitiesRep);
        return command;
    }

    @Bean("DObServiceInvoiceCreateCommand")
    public DObPurchaseServiceLocalVendorInvoiceCreateCommand createServiceInvoiceCreateCommand(
            DocumentObjectCodeGenerator documentObjectCodeGenerator,
            IUserAccountSecurityService userAccountSecurityService,
            DObPurchaseOrderRep dObPurchaseOrderRep,
            MObVendorRep mObVendorRep,
            DObPurchaseOrderGeneralModelRep dObPurchaseOrderHeaderGeneralModelRep,
            IObOrderCompanyGeneralModelRep dObPurchaseOrderCompanySectionGeneralModelRep,
            CObPaymentTermsRep cObPaymentTermsRep,
            DObVendorInvoiceRep dObInvoiceRep,
            CObCompanyRep cObCompanyRep,
            CObCurrencyRep cObCurrencyRep,
            CObPurchasingUnitRep purchasingUnitRep,
            IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep,
            IObOrderItemRep orderItemRep,
            MObItemRep itemRep
    )
            throws Exception {
        DObPurchaseServiceLocalVendorInvoiceCreateCommand command =
                new DObPurchaseServiceLocalVendorInvoiceCreateCommand(documentObjectCodeGenerator);
        command.setVendorRep(mObVendorRep);
        command.setOrderRep(dObPurchaseOrderRep);
        command.setPurchaseOrderHeaderGeneralModelRep(dObPurchaseOrderHeaderGeneralModelRep);
        command.setPurchaseOrderCompanySectionGeneralModelRep(
                dObPurchaseOrderCompanySectionGeneralModelRep);
        command.setPaymentTermsRep(cObPaymentTermsRep);
        command.setVendorInvoiceRep(dObInvoiceRep);
        command.setCompanyRep(cObCompanyRep);
        command.setCurrencyRep(cObCurrencyRep);
        command.setPurchasingUnitRep(purchasingUnitRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setOrderPaymentDetailsGeneralModelRep(orderPaymentDetailsGeneralModelRep);
        command.setOrderItemRep(orderItemRep);
        command.setItemRep(itemRep);
        return command;
    }

    @Bean
    @DependsOn({"DObGoodsInvoiceCreateCommand", "DObServiceInvoiceCreateCommand"})
    public DObVendorInvoiceCreateCommandFactory createVendorInvoiceCreateCommandFactory(
            DObGoodsInvoiceCreateCommand goodsInvoiceCreateCommand,
            DObPurchaseServiceLocalVendorInvoiceCreateCommand serviceInvoiceCreateCommand
    ) {
        DObVendorInvoiceCreateCommandFactory factory = new DObVendorInvoiceCreateCommandFactory();
        factory.setGoodsInvoiceCreateCommand(goodsInvoiceCreateCommand);
        factory.setPurchaseServiceLocalVendorInvoiceCreateCommand(serviceInvoiceCreateCommand);
        return factory;
    }

    @Bean
    public IObVendorInvoiceItemsDeleteCommand createInvoiceItemsDeleteCommand(
            IObVendorInvoiceItemRep invoicesItemsRep,
            IUserAccountSecurityService userAccountSecurityService,
            DObVendorInvoiceRep invoiceRep,
            IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand) {
        IObVendorInvoiceItemsDeleteCommand command = new IObVendorInvoiceItemsDeleteCommand();
        command.setInvoiceRep(invoiceRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setInvoicesItemsRep(invoicesItemsRep);
        command.setSummaryUpdateCommand(summaryUpdateCommand);
        return command;
    }

    @Bean
    public IObVendorInvoiceTaxesDeleteCommand createInvoiceTaxesDeleteCommand(
            IObVendorInvoiceTaxesRep vendorInvoiceTaxesRep,
            DObVendorInvoiceRep vendorInvoiceRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep,
            IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand) {
        IObVendorInvoiceTaxesDeleteCommand command = new IObVendorInvoiceTaxesDeleteCommand();
        command.setVendorInvoiceTaxesRep(vendorInvoiceTaxesRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setVendorInvoiceRep(vendorInvoiceRep);
        command.setVendorInvoiceTaxesGeneralModelRep(vendorInvoiceTaxesGeneralModelRep);
        command.setSummaryUpdateCommand(summaryUpdateCommand);
        return command;
    }

    @Bean("dObInvoiceLockCommand")
    public EntityLockCommand<DObVendorInvoice> createEntityLockCommand(
            IUserAccountSecurityService userAccountSecurityService,
            IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
            DObVendorInvoiceRep dObInvoiceRep) {
        EntityLockCommand<DObVendorInvoice> entityLockCommand = new EntityLockCommand<>();
        entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
        entityLockCommand.setConcurrentDataAccessManager(
                concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
                        DObVendorInvoice.class.getName()));
        entityLockCommand.setRepository(dObInvoiceRep);
        return entityLockCommand;
    }

    @Bean("InvoiceLockCommandJMXBean")
    public ObjectInstance createEntityLockCommandJMXBean(
            @Qualifier("dObInvoiceLockCommand") EntityLockCommand entityLockCommand) throws Exception {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        ObjectName name = new ObjectName("com.ebs.dda.commands.general:name=InvoiceEntityLockCommand");
        return mbs.registerMBean(entityLockCommand, name);
    }

    @Bean
    public DObVendorInvoiceDetailsEditCommand createInvoiceDetailsEditCommand(
            IUserAccountSecurityService userAccountSecurityService,
            DObVendorInvoiceRep invoiceRep,
            IObVendorInvoiceDetailsRep invoiceDetailsRep,
            IObVendorInvoicePurchaseOrdeRep invoicePurchaseOrderRep,
            CObCurrencyRep currencyRep,
            CObPaymentTermsRep paymentTermsRep) {
        DObVendorInvoiceDetailsEditCommand command = new DObVendorInvoiceDetailsEditCommand();
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setInvoiceRep(invoiceRep);
        command.setInvoiceDetailsRep(invoiceDetailsRep);
        command.setInvoicePurchaseOrderRep(invoicePurchaseOrderRep);
        command.setCurrencyRep(currencyRep);
        command.setPaymentTermsRep(paymentTermsRep);
        return command;
    }

    @Bean
    public DObVendorInvoiceActivateCommand createInvoiceActivateCommand(
            IUserAccountSecurityService userAccountSecurityService,
            DObVendorInvoiceRep dObInvoiceRep,
            DObVendorInvoicePostPreparationGeneralModelRep vendorInvoicePostPreparationGeneralModelRep,
            CObFiscalYearRep cObFiscalYearRep) {

        DObVendorInvoiceActivateCommand command = new DObVendorInvoiceActivateCommand();
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setInvoiceRep(dObInvoiceRep);
        command.setVendorInvoicePostPreparationGeneralModelRep(
                vendorInvoicePostPreparationGeneralModelRep);
        command.setcObFiscalYearRep(cObFiscalYearRep);

        return command;
    }

    @Bean
    public DObVendorInvoiceUpdateDownPaymentCommand createUpdateDownPaymentCommand(
            IUserAccountSecurityService userAccountSecurityService,
            IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsRep,
            DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep,
            IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep) {

        DObVendorInvoiceUpdateDownPaymentCommand command = new DObVendorInvoiceUpdateDownPaymentCommand();
        command.setUserAccountSecurityService(userAccountSecurityService);
        command.setInvoiceDetailsRep(invoiceDetailsRep);
        command.setPaymentRequestGeneralModelRep(paymentRequestGeneralModelRep);
        command.setVendorInvoiceDetailsRep(vendorInvoiceDetailsRep);

        return command;
    }

    @Bean
    public DObVendorInvoiceItemSaveCommand invoiceItemSaveCommand(
            DObVendorInvoiceRep invoiceRep,
            MObItemGeneralModelRep mObItemGeneralModelRep,
            CObMeasureRep cObMeasureRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObVendorInvoiceItemRep itemsRep,
            IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand,
            IObVendorInvoiceTaxesUpdateCommand taxesUpdateCommand) {
        DObVendorInvoiceItemSaveCommand dObInvoiceItemSaveCommand =
                new DObVendorInvoiceItemSaveCommand();
        dObInvoiceItemSaveCommand.setInvoiceRep(invoiceRep);
        dObInvoiceItemSaveCommand.setmObItemGeneralModelRep(mObItemGeneralModelRep);
        dObInvoiceItemSaveCommand.setcObMeasureRep(cObMeasureRep);
        dObInvoiceItemSaveCommand.setUserAccountSecurityService(userAccountSecurityService);
        dObInvoiceItemSaveCommand.setItemsRep(itemsRep);
        dObInvoiceItemSaveCommand.setSummaryUpdateCommand(summaryUpdateCommand);
        dObInvoiceItemSaveCommand.setTaxesUpdateCommand(taxesUpdateCommand);

        return dObInvoiceItemSaveCommand;
    }

    @Bean("IObVendorInvoiceTaxesAddCommand")
    public IObVendorInvoiceTaxesAddCommand createVendorInvoiceTaxesAddCommand(
            IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep,
            IObVendorInvoiceTaxesRep taxesRep,
            LObVendorTaxesRep vendorTaxesRep,
            IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep,
            IUserAccountSecurityService userAccountSecurityService) {
        IObVendorInvoiceTaxesAddCommand taxesAddCommand = new IObVendorInvoiceTaxesAddCommand();
        taxesAddCommand.setVendorInvoiceItemsGeneralModelRep(vendorInvoiceItemsGeneralModelRep);
        taxesAddCommand.setTaxesRep(taxesRep);
        taxesAddCommand.setlObVendorTaxesRep(vendorTaxesRep);
        taxesAddCommand.setVendorInvoiceDetailsRep(vendorInvoiceDetailsRep);
        taxesAddCommand.setUserAccountSecurityService(userAccountSecurityService);
        return taxesAddCommand;
    }

    @Bean("IObLocalGoodsInvoiceTaxesAddCommand")
    public IObLocalGoodsInvoiceTaxesAddCommand createLocalGoodsInvoiceTaxesAddCommand(
            IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep,
            IObVendorInvoiceTaxesRep taxesRep,
            LObCompanyTaxesRep companyTaxesRep,
            IObVendorInvoiceCompanyDataRep vendorInvoiceCompanyDataRep,
            IUserAccountSecurityService userAccountSecurityService) {
        IObLocalGoodsInvoiceTaxesAddCommand command = new IObLocalGoodsInvoiceTaxesAddCommand();
        command.setVendorInvoiceItemsGeneralModelRep(vendorInvoiceItemsGeneralModelRep);
        command.setTaxesRep(taxesRep);
        command.setlObCompanyTaxesRep(companyTaxesRep);
        command.setVendorInvoiceCompanyDataRep(vendorInvoiceCompanyDataRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        return command;
    }

    @Bean("IObVendorInvoiceAddTaxesCommandExecuter")
    @DependsOn({"IObVendorInvoiceTaxesAddCommand", "IObLocalGoodsInvoiceTaxesAddCommand"})
    public IObVendorInvoiceAddTaxesCommandExecuter createVendorInvoiceAddTaxesCommandExecuter(
            IObVendorInvoiceTaxesAddCommand vendorInvoiceTaxesAddCommand,
            IObLocalGoodsInvoiceTaxesAddCommand localGoodsInvoiceTaxesAddCommand) {
        IObVendorInvoiceAddTaxesCommandExecuter executer = new IObVendorInvoiceAddTaxesCommandExecuter();
        executer.setVendorInvoiceTaxesAddCommand(vendorInvoiceTaxesAddCommand);
        executer.setLocalGoodsInvoiceTaxesAddCommand(localGoodsInvoiceTaxesAddCommand);
        return executer;
    }

    @Bean("IObVendorInvoiceSummaryAddCommand")
    public IObVendorInvoiceSummaryAddCommand createVendorInvoiceSummaryAddCommand(
            IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep,
            IObVendorInvoiceTaxesGeneralModelRep vendorInvoiceTaxesGeneralModelRep,
            IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep,
            IUserAccountSecurityService userAccountSecurityService) {
        IObVendorInvoiceSummaryAddCommand summaryAddCommand = new IObVendorInvoiceSummaryAddCommand();
        summaryAddCommand.setVendorInvoiceSummaryRep(vendorInvoiceSummaryRep);
        summaryAddCommand.setVendorInvoiceItemsGeneralModelRep(vendorInvoiceItemsGeneralModelRep);
        summaryAddCommand.setVendorInvoiceTaxesGeneralModelRep(vendorInvoiceTaxesGeneralModelRep);
        summaryAddCommand.setUserAccountSecurityService(userAccountSecurityService);
        return summaryAddCommand;
    }

    @Bean("IObVendorInvoiceSummaryUpdateCommand")
    public IObVendorInvoiceSummaryUpdateCommand createVendorInvoiceSummaryUpdateCommand(
            IObVendorInvoiceDetailsDownPaymentRep VendorInvoiceDetailsDownPaymentRep,
            IObPaymentRequestInvoicePaymentDetailsRep paymentRequestInvoicePaymentDetailsRep,
            IObVendorInvoiceSummaryRep vendorInvoiceSummaryRep,
            DObVendorInvoiceRep vendorInvoiceRep,
            IObVendorInvoiceDetailsRep vendorInvoiceDetailsRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep) {
        IObVendorInvoiceSummaryUpdateCommand summaryUpdateCommand = new IObVendorInvoiceSummaryUpdateCommand();
        summaryUpdateCommand.setVendorInvoiceSummaryRep(vendorInvoiceSummaryRep);
        summaryUpdateCommand.setUserAccountSecurityService(userAccountSecurityService);
        summaryUpdateCommand.setVendorInvoiceDetailsDownPaymentRep(VendorInvoiceDetailsDownPaymentRep);
        summaryUpdateCommand.setPaymentRequestInvoicePaymentDetailsRep(paymentRequestInvoicePaymentDetailsRep);
        summaryUpdateCommand.setUserAccountSecurityService(userAccountSecurityService);
        summaryUpdateCommand.setVendorInvoiceRep(vendorInvoiceRep);
        summaryUpdateCommand.setVendorInvoiceDetailsRep(vendorInvoiceDetailsRep);
        summaryUpdateCommand.setInvoiceSummaryGeneralModelRep(invoiceSummaryGeneralModelRep);
        return summaryUpdateCommand;
    }

    @Bean
    public IObVendorInvoiceTaxesUpdateCommand createVendorInvoiceTaxesUpdateCommand(
            IObVendorInvoiceItemsGeneralModelRep vendorInvoiceItemsGeneralModelRep,
            IObVendorInvoiceTaxesGeneralModelRep taxesGeneralModelRep,
            IObVendorInvoiceTaxesRep taxesRep,
            IUserAccountSecurityService userAccountSecurityService) {
        IObVendorInvoiceTaxesUpdateCommand taxesAddCommand = new IObVendorInvoiceTaxesUpdateCommand();
        taxesAddCommand.setVendorInvoiceItemsGeneralModelRep(vendorInvoiceItemsGeneralModelRep);
        taxesAddCommand.setTaxesGeneralModelRep(taxesGeneralModelRep);
        taxesAddCommand.setTaxesRep(taxesRep);
        taxesAddCommand.setUserAccountSecurityService(userAccountSecurityService);
        return taxesAddCommand;
    }

    @Bean
    public DObVendorInvoiceDeleteCommand createDObVendorInvoiceDeleteCommand(
            DObVendorInvoiceRep vendorInvoiceRep) {
        DObVendorInvoiceDeleteCommand vendorInvoiceDeleteCommand = new DObVendorInvoiceDeleteCommand();
        vendorInvoiceDeleteCommand.setVendorInvoiceRep(vendorInvoiceRep);
        return vendorInvoiceDeleteCommand;
    }
    @Bean
    public DObImportGoodsInvoiceAccountDeterminationCommand createImportGoodsInvoiceAccountDeterminationCommand(
            DObVendorInvoiceRep vendorInvoiceRep,
            DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep journalDetailPreparationGeneralModelٌRep,
            DObVendorInvoiceItemAccountsPreparationGeneralModelRep itemAccountsPreparationGeneralModelRep,
            IUserAccountSecurityService userAccountSecurityService) {
        DObImportGoodsInvoiceAccountDeterminationCommand command = new DObImportGoodsInvoiceAccountDeterminationCommand();
        command.setVendorInvoiceRep(vendorInvoiceRep);
        command.setJournalDetailPreparationGeneralModelٌRep(journalDetailPreparationGeneralModelٌRep);
        command.setItemAccountsPreparationGeneralModelRep(itemAccountsPreparationGeneralModelRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        return command;
    }
    @Bean
    public DObLocalGoodsInvoiceAccountDeterminationCommand createLocalGoodsInvoiceAccountDeterminationCommand(
            DObVendorInvoiceRep vendorInvoiceRep,
            DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep journalDetailPreparationGeneralModelٌRep,
            DObVendorInvoiceItemAccountsPreparationGeneralModelRep itemAccountsPreparationGeneralModelRep,
            IObVendorInvoiceTaxesGeneralModelRep taxesGeneralModelRep,
            IUserAccountSecurityService userAccountSecurityService) {
        DObLocalGoodsInvoiceAccountDeterminationCommand command = new DObLocalGoodsInvoiceAccountDeterminationCommand();
        command.setVendorInvoiceRep(vendorInvoiceRep);
        command.setJournalDetailPreparationGeneralModelٌRep(journalDetailPreparationGeneralModelٌRep);
        command.setItemAccountsPreparationGeneralModelRep(itemAccountsPreparationGeneralModelRep);
        command.setTaxesRep(taxesGeneralModelRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        return command;
    }
    @Bean
    public DObPurchaseServiceLocalInvoiceAccountDeterminationCommand createPurchaseServiceLocalInvoiceAccountDeterminationCommand(
            DObVendorInvoiceRep vendorInvoiceRep,
            DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep journalDetailPreparationGeneralModelٌRep,
            DObVendorInvoiceItemAccountsPreparationGeneralModelRep itemAccountsPreparationGeneralModelRep,
            IObVendorInvoiceTaxesGeneralModelRep taxesGeneralModelRep,
            IObPurchaseOrderFulFillerVendorRep purchaseOrderFulFillerVendorRep,
            IUserAccountSecurityService userAccountSecurityService) {
        DObPurchaseServiceLocalInvoiceAccountDeterminationCommand command = new DObPurchaseServiceLocalInvoiceAccountDeterminationCommand();
        command.setVendorInvoiceRep(vendorInvoiceRep);
        command.setJournalDetailPreparationGeneralModelٌRep(journalDetailPreparationGeneralModelٌRep);
        command.setItemAccountsPreparationGeneralModelRep(itemAccountsPreparationGeneralModelRep);
        command.setTaxesRep(taxesGeneralModelRep);
        command.setPurchaseOrderFulFillerVendorRep(purchaseOrderFulFillerVendorRep);
        command.setUserAccountSecurityService(userAccountSecurityService);
        return command;
    }
}
