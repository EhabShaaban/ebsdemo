package com.ebs.dda.config.order.salesreturnorder;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesreturnorder.*;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxesGeneralModel;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.repositories.accounting.IObInvoiceTaxesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.lookups.LObReasonRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.CObSalesReturnOrderTypeRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderCycleDatesRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.sql.DataSource;
import java.lang.management.ManagementFactory;

@Configuration
public class DObSalesReturnOrderCommandBeans {
  @Bean
  public DObSalesReturnOrderCreateCommand createCommandSalesReturnOrder(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      CObSalesReturnOrderTypeRep salesReturnOrderTypeRep,
      DObSalesReturnOrderRep salesReturnOrderRep,
      UserAccountSecurityService userAccountSecurityService,
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
      CObCompanyGeneralModelRep companyGeneralModelRep,
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
      IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerGeneralModelRep,
      MObCustomerGeneralModelRep customerGeneralModelRep,
      CObProductManagerGeneralModelRep productManagerGeneralModelRep,
      CObCurrencyRep currencyRep,
      IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep,
      CObMeasureRep measureRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      LObCompanyTaxesRep companyTaxesRep,
      IObInvoiceTaxesGeneralModelRep<IObSalesInvoiceTaxesGeneralModel>
          salesInvoiceTaxesGeneralModelRep)
      throws Exception {

    DObSalesReturnOrderCreateCommand salesOrderCreateCommand =
        new DObSalesReturnOrderCreateCommand(documentObjectCodeGenerator);
    salesOrderCreateCommand.setSalesReturnOrderTypeRep(salesReturnOrderTypeRep);
    salesOrderCreateCommand.setSalesReturnOrderRep(salesReturnOrderRep);
    salesOrderCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    salesOrderCreateCommand.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    salesOrderCreateCommand.setCompanyGeneralModelRep(companyGeneralModelRep);
    salesOrderCreateCommand.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    salesOrderCreateCommand.setSalesInvoiceBusinessPartnerGeneralModelRep(
        salesInvoiceBusinessPartnerGeneralModelRep);
    salesOrderCreateCommand.setCustomerGeneralModelRep(customerGeneralModelRep);
    salesOrderCreateCommand.setCurrencyRep(currencyRep);
    salesOrderCreateCommand.setSalesInvoiceItemsGeneralModelRep(salesInvoiceItemsGeneralModelRep);
    salesOrderCreateCommand.setMeasureRep(measureRep);
    salesOrderCreateCommand.setItemGeneralModelRep(itemGeneralModelRep);
    salesOrderCreateCommand.setSalesInvoiceTaxesGeneralModelRep(salesInvoiceTaxesGeneralModelRep);
    return salesOrderCreateCommand;
  }

  @Bean
  public DObSalesReturnOrderDeleteCommand createSalesReturnOrderDeleteCommand(
      DObSalesReturnOrderRep salesReturnOrderRep) {
    DObSalesReturnOrderDeleteCommand salesReturnOrderDeleteCommand =
        new DObSalesReturnOrderDeleteCommand();
    salesReturnOrderDeleteCommand.setSalesReturnOrderRep(salesReturnOrderRep);
    return salesReturnOrderDeleteCommand;
  }

  @Bean
  public IObSalesReturnOrderDeleteItemCommand createSalesReturnOrderDeleteItemCommand(
      IObSalesReturnOrderItemRep salesReturnOrderItemRep,
      DObSalesReturnOrderRep salesReturnOrderRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSalesReturnOrderDeleteItemCommand salesReturnOrderDeleteItemCommand =
        new IObSalesReturnOrderDeleteItemCommand();
    salesReturnOrderDeleteItemCommand.setSalesReturnOrderItemRep(salesReturnOrderItemRep);
    salesReturnOrderDeleteItemCommand.setSalesReturnOrderRep(salesReturnOrderRep);
    salesReturnOrderDeleteItemCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesReturnOrderDeleteItemCommand;
  }

  @Bean("salesReturnOrderLockCommand")
  public EntityLockCommand<DObSalesReturnOrder> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObSalesReturnOrderRep salesReturnOrderRep) {

    EntityLockCommand<DObSalesReturnOrder> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObSalesReturnOrder.class.getName()));
    entityLockCommand.setRepository(salesReturnOrderRep);
    return entityLockCommand;
  }

  @Bean("SalesReturnOrderEntityLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("salesReturnOrderLockCommand") EntityLockCommand entityLockCommand)
      throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=SalesReturnOrderEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public DObSalesReturnOrderMarkAsShippedCommand createSalesReturnOrderMarkAsShippedCommand(
      DObSalesReturnOrderRep salesReturnOrderRep,
      IObSalesReturnOrderCycleDatesRep cycleDatesRep,
      IUserAccountSecurityService userAccountSecurityService) {
    DObSalesReturnOrderMarkAsShippedCommand salesReturnOrderMarkAsShippedCommand =
        new DObSalesReturnOrderMarkAsShippedCommand();
    salesReturnOrderMarkAsShippedCommand.setSalesReturnOrderRep(salesReturnOrderRep);
    salesReturnOrderMarkAsShippedCommand.setCycleDatesRep(cycleDatesRep);
    salesReturnOrderMarkAsShippedCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesReturnOrderMarkAsShippedCommand;
  }

  @Bean
  public IObSalesReturnOrderSaveItemCommand createSalesReturnOrderSaveItemCommand(
      DObSalesReturnOrderRep salesReturnOrderRep,
      IObSalesReturnOrderItemRep salesReturnOrderItemRep,
      LObReasonRep reasonRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSalesReturnOrderSaveItemCommand salesOrderItemsSaveCommand =
        new IObSalesReturnOrderSaveItemCommand();

    salesOrderItemsSaveCommand.setSalesReturnOrderRep(salesReturnOrderRep);
    salesOrderItemsSaveCommand.setSalesReturnOrderItemRep(salesReturnOrderItemRep);
    salesOrderItemsSaveCommand.setReasonRep(reasonRep);
    salesOrderItemsSaveCommand.setUserAccountSecurityService(userAccountSecurityService);

    return salesOrderItemsSaveCommand;
  }

  @Bean
  public DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand
      createDObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand(
          DObSalesReturnOrderRep salesReturnOrderRep,
          IUserAccountSecurityService userAccountSecurityService) {
    DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand
        salesReturnOrderUpdateGoodsReceiptActivatedStateCommand =
            new DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand();
    salesReturnOrderUpdateGoodsReceiptActivatedStateCommand.setSalesReturnOrderRep(
        salesReturnOrderRep);
    salesReturnOrderUpdateGoodsReceiptActivatedStateCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return salesReturnOrderUpdateGoodsReceiptActivatedStateCommand;
  }

  @Bean
  public DObSalesReturnPdfGeneratorCommand createSalesReturnGeneratePdfCommand(
      DataSource dataSource) {
    DObSalesReturnPdfGeneratorCommand salesReturnPdfGeneratorCommand =
        new DObSalesReturnPdfGeneratorCommand();
    salesReturnPdfGeneratorCommand.setDataSource(dataSource);
    return salesReturnPdfGeneratorCommand;
  }
}
