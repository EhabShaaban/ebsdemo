package com.ebs.dda.config.notesreceivables;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.NotesReceivableReferenceDocumentsTypeEnum;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;

public class DObNotesReceivableRefDocumentLockCommandFactory {

  private EntityLockCommand<DObMonetaryNotes> notesReceivableLockCommand;
  private EntityLockCommand<DObSalesInvoice> salesInvoiceRefDocumentLockCommand;
  private EntityLockCommand<DObSalesOrder> salesOrderRefDocumentLockCommand;

  public DObNotesReceivableRefDocumentLockCommandFactory(
      EntityLockCommand<DObMonetaryNotes> notesReceivableLockCommand,
      EntityLockCommand<DObSalesInvoice> salesInvoiceRefDocumentLockCommand,
      EntityLockCommand<DObSalesOrder> salesOrderRefDocumentLockCommand) {
    this.notesReceivableLockCommand = notesReceivableLockCommand;
    this.salesInvoiceRefDocumentLockCommand = salesInvoiceRefDocumentLockCommand;
    this.salesOrderRefDocumentLockCommand = salesOrderRefDocumentLockCommand;
  }

  public EntityLockCommand<?> getRefDocumentLockCommand(String type) {

    if (NotesReceivableReferenceDocumentsTypeEnum.SALES_ORDER.name().equals(type)) {
      return salesOrderRefDocumentLockCommand;
    } else if (NotesReceivableReferenceDocumentsTypeEnum.SALES_INVOICE.name().equals(type)) {
      return salesInvoiceRefDocumentLockCommand;
    } else if (NotesReceivableReferenceDocumentsTypeEnum.NOTES_RECEIVABLE.name().equals(type)) {
      return notesReceivableLockCommand;
    }
    throw new UnsupportedOperationException(type + " is not supported");
  }
}
