package com.ebs.dda.config.accounting.journalbalance;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.journalbalance.CObBankJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.CObTreasuryJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceTypeEnum.JournalBalanceType;

public class CObJournalBalanceLockCommandBeanFactory {

  private final static String CASH = "CASH";

  private EntityLockCommand<CObBankJournalBalance> bankLockCommand;
  private EntityLockCommand<CObTreasuryJournalBalance> treasuryLockCommand;

  public CObJournalBalanceLockCommandBeanFactory(
      EntityLockCommand<CObBankJournalBalance> bankLockCommand,
      EntityLockCommand<CObTreasuryJournalBalance> treasuryLockCommand) {
    this.bankLockCommand = bankLockCommand;
    this.treasuryLockCommand = treasuryLockCommand;
  }

  public <T extends CObJournalBalance> EntityLockCommand<T> getJournalBalanceLockCommand(
      String type) {

    if (JournalBalanceType.BANK.name().equals(type)) {
      return (EntityLockCommand<T>) bankLockCommand;
    }

    if (JournalBalanceType.TREASURY.name().equals(type) || CASH.equals(type)) {
      return (EntityLockCommand<T>) treasuryLockCommand;
    }
    throw new UnsupportedOperationException(type + " is not supported");
  }
}
