package com.ebs.dda.config.order.salesorder;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderCreateCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderDeleteCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderInvoicedCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderIssuedCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderUpdateRemainingCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderCompanyStoreUpdateCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderDataUpdateCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderItemsDeleteCommand;
import com.ebs.dda.commands.order.salesorder.IObSalesOrderItemsSaveCommand;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.accounting.taxes.LObCompanyTaxesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.masterdata.salesresponsible.CObSalesResponsibleRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseRep;
import com.ebs.dda.repositories.order.salesorder.CObSalesOrderTypeRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderCompanyStoreDataRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderItemsRep;
import java.lang.management.ManagementFactory;
import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObSalesOrderCommandBeans {
  @Bean
  public DObSalesOrderCreateCommand createCommandSalesOrder(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      CObPurchasingUnitRep businessUnitRep,
      CObSalesOrderTypeRep salesOrderTypeRep,
      CObSalesResponsibleRep salesResponsibleRep,
      MObCustomerRep customerRep,
      DObSalesOrderRep salesOrderRep,
      UserAccountSecurityService userAccountSecurityService,
      CObCompanyRep companyRep,
      LObCompanyTaxesGeneralModelRep companyTaxesGeneralModelRep)
      throws Exception {

    DObSalesOrderCreateCommand salesOrderCreateCommand =
        new DObSalesOrderCreateCommand(documentObjectCodeGenerator);

    salesOrderCreateCommand.setBusinessUnitRep(businessUnitRep);
    salesOrderCreateCommand.setCustomerRep(customerRep);
    salesOrderCreateCommand.setCompanyRep(companyRep);
    salesOrderCreateCommand.setSalesOrderRep(salesOrderRep);
    salesOrderCreateCommand.setCompanyTaxesGeneralModelRep(companyTaxesGeneralModelRep);
    salesOrderCreateCommand.setSalesOrderTypeRep(salesOrderTypeRep);
    salesOrderCreateCommand.setSalesResponsibleRep(salesResponsibleRep);
    salesOrderCreateCommand.setUserAccountSecurityService(userAccountSecurityService);

    return salesOrderCreateCommand;
  }

  @Bean("salesOrderLockCommand")
  public EntityLockCommand<DObSalesOrder> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObSalesOrderRep salesOrderRep) {

    EntityLockCommand<DObSalesOrder> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObSalesOrder.class.getName()));
    entityLockCommand.setRepository(salesOrderRep);
    return entityLockCommand;
  }

  @Bean("SalesOrderEntityLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("salesOrderLockCommand") EntityLockCommand entityLockCommand) throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=SalesOrderEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public IObSalesOrderItemsDeleteCommand createSalesOrderItemsDeleteCommand(
      IObSalesOrderItemsRep salesOrderItemRep,
      DObSalesOrderRep salesOrderRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSalesOrderItemsDeleteCommand salesOrderItemsDeleteCommand =
        new IObSalesOrderItemsDeleteCommand();
    salesOrderItemsDeleteCommand.setSalesOrderItemRep(salesOrderItemRep);
    salesOrderItemsDeleteCommand.setdObSalesOrderRep(salesOrderRep);
    salesOrderItemsDeleteCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesOrderItemsDeleteCommand;
  }

  @Bean
  public IObSalesOrderDataUpdateCommand createSalesOrderDataUpdateCommand(
      DObSalesOrderRep salesOrderRep,
      CObCurrencyRep currencyRep,
      CObPaymentTermsRep paymentTermsRep,
      IObSalesOrderDataRep salesOrderDataRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSalesOrderDataUpdateCommand salesOrderDataUpdateCommand =
        new IObSalesOrderDataUpdateCommand();
    salesOrderDataUpdateCommand.setSalesOrderRep(salesOrderRep);
    salesOrderDataUpdateCommand.setCurrencyRep(currencyRep);
    salesOrderDataUpdateCommand.setPaymentTermsRep(paymentTermsRep);
    salesOrderDataUpdateCommand.setSalesOrderDataRep(salesOrderDataRep);
    salesOrderDataUpdateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesOrderDataUpdateCommand;
  }

  @Bean
  public IObSalesOrderItemsSaveCommand createSalesOrderItemsSaveCommand(
      DObSalesOrderRep salesOrderRep,
      MObItemRep itemRep,
      CObMeasureRep measureRep,
      UserAccountSecurityService securityService) {
    IObSalesOrderItemsSaveCommand salesOrderItemsSaveCommand = new IObSalesOrderItemsSaveCommand();

    salesOrderItemsSaveCommand.setSalesOrderRep(salesOrderRep);
    salesOrderItemsSaveCommand.setItemRep(itemRep);
    salesOrderItemsSaveCommand.setMeasureRep(measureRep);
    salesOrderItemsSaveCommand.setUserAccountSecurityService(securityService);

    return salesOrderItemsSaveCommand;
  }

  @Bean
  public DObSalesOrderDeleteCommand createSalesOrderDeleteCommand(DObSalesOrderRep salesOrderRep) {
    DObSalesOrderDeleteCommand salesOrderDeleteCommand = new DObSalesOrderDeleteCommand();
    salesOrderDeleteCommand.setSalesOrderRep(salesOrderRep);
    return salesOrderDeleteCommand;
  }

  @Bean
  public IObSalesOrderCompanyStoreUpdateCommand createCompanyStoreUpdateCommand(
      DObSalesOrderRep salesOrderRep,
      CObCompanyRep companyRep,
      CObStorehouseRep storehouseRep,
      IObSalesOrderCompanyStoreDataRep companyStoreDataRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSalesOrderCompanyStoreUpdateCommand companyStoreUpdateCommand =
        new IObSalesOrderCompanyStoreUpdateCommand();
    companyStoreUpdateCommand.setSalesOrderRep(salesOrderRep);
    companyStoreUpdateCommand.setCompanyRep(companyRep);
    companyStoreUpdateCommand.setStorehouseRep(storehouseRep);
    companyStoreUpdateCommand.setCompanyStoreDataRep(companyStoreDataRep);
    companyStoreUpdateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return companyStoreUpdateCommand;
  }

  @Bean
  public DObSalesOrderInvoicedCommand createDObSalesOrderUpdateStateCommand(
      DObSalesOrderRep salesOrderRep, IUserAccountSecurityService userAccountSecurityService) {
    DObSalesOrderInvoicedCommand salesOrderUpdateStateCommand = new DObSalesOrderInvoicedCommand();
    salesOrderUpdateStateCommand.setSalesOrderRep(salesOrderRep);
    salesOrderUpdateStateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesOrderUpdateStateCommand;
  }

  @Bean
  public DObSalesOrderIssuedCommand createDObSalesOrderIssuedCommand(
      DObSalesOrderRep salesOrderRep, IUserAccountSecurityService userAccountSecurityService) {
    DObSalesOrderIssuedCommand salesOrderIssuedCommand = new DObSalesOrderIssuedCommand();
    salesOrderIssuedCommand.setSalesOrderRep(salesOrderRep);
    salesOrderIssuedCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesOrderIssuedCommand;
  }

  @Bean
  public DObSalesOrderUpdateRemainingCommand createDObSalesOrderUpdateRemainingCommand(
      DObSalesOrderRep salesOrderRep, IUserAccountSecurityService userAccountSecurityService) {
    DObSalesOrderUpdateRemainingCommand salesOrderUpdateRemainingCommand = new DObSalesOrderUpdateRemainingCommand();
    salesOrderUpdateRemainingCommand.setSalesOrderRep(salesOrderRep);
    salesOrderUpdateRemainingCommand.setUserAccountSecurityService(userAccountSecurityService);
    return salesOrderUpdateRemainingCommand;
  }
}
