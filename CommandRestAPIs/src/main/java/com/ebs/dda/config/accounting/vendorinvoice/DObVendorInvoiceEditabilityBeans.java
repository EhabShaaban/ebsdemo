package com.ebs.dda.config.accounting.vendorinvoice;

import com.ebs.dda.accounting.vendorinvoice.editiablity.IObVendorInvoiceDetailsEditabilityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObVendorInvoiceEditabilityBeans {

    @Bean
    IObVendorInvoiceDetailsEditabilityManager createIObInvoiceDetailsEditabilityManager() {
        IObVendorInvoiceDetailsEditabilityManager editabilityManager =
                new IObVendorInvoiceDetailsEditabilityManager();
        return editabilityManager;
    }
}
