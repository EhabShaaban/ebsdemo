package com.ebs.dda.config.accounting.collection;

import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyTreasuriesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.validation.collection.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObCollectionValidationBeans {

  @Bean
  public DObCollectionCreationValidator createCollectionValidator(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
      DocumentOwnerGeneralModelRep documentOwnerRep,
      DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    DObCollectionCreationValidator collectionCreationValidator =
        new DObCollectionCreationValidator();
    collectionCreationValidator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    collectionCreationValidator.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    collectionCreationValidator.setDocumentOwnerRep(documentOwnerRep);
    collectionCreationValidator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    return collectionCreationValidator;
  }

  @Bean
  public DObCollectionSalesInvoiceActivateValidator createCollectionSalesInvoiceActivateValidator(
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
      DObCollectionGeneralModelRep collectionGeneralModelRep,
      IObCollectionDetailsDataMandatoryValidator collectioDetailsDataMandatoryValidator,
      CObFiscalYearRep fiscalYearRep) {

    DObCollectionSalesInvoiceActivateValidator validator =
        new DObCollectionSalesInvoiceActivateValidator();

    validator.setCollectionGeneralModelRep(collectionGeneralModelRep);
    validator.setCollectionDetailsDataMandatoryValidator(collectioDetailsDataMandatoryValidator);
    validator.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean
  public DObCollectionSalesOrderActivateValidator createCollectionSalesOrderActivateValidator(
      DObSalesOrderGeneralModelRep salesOrderGeneralModelRep,
      DObCollectionGeneralModelRep collectionGeneralModelRep,
      IObCollectionDetailsDataMandatoryValidator collectioDetailsDataMandatoryValidator,
      CObFiscalYearRep fiscalYearRep) {

    DObCollectionSalesOrderActivateValidator validator =
        new DObCollectionSalesOrderActivateValidator();

    validator.setCollectionGeneralModelRep(collectionGeneralModelRep);
    validator.setCollectionDetailsDataMandatoryValidator(collectioDetailsDataMandatoryValidator);
    validator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean
  public DObCollectionMonetaryNoteActivateValidator createCollectionMonetaryNoteActivateValidator(
      DObCollectionGeneralModelRep collectionGeneralModelRep,
      IObCollectionDetailsDataMandatoryValidator collectioDetailsDataMandatoryValidator,
      CObFiscalYearRep fiscalYearRep) {

    DObCollectionMonetaryNoteActivateValidator validator =
        new DObCollectionMonetaryNoteActivateValidator();

    validator.setCollectionGeneralModelRep(collectionGeneralModelRep);
    validator.setCollectionDetailsDataMandatoryValidator(collectioDetailsDataMandatoryValidator);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean
  public DObCollectionAccountingNoteActivateValidator createCollectionAccountingNoteActivateValidator(
      DObCollectionGeneralModelRep collectionGeneralModelRep,
      IObCollectionDetailsDataMandatoryValidator collectioDetailsDataMandatoryValidator,
      CObFiscalYearRep fiscalYearRep) {

    DObCollectionAccountingNoteActivateValidator validator =
        new DObCollectionAccountingNoteActivateValidator();

    validator.setCollectionGeneralModelRep(collectionGeneralModelRep);
    validator.setCollectionDetailsDataMandatoryValidator(collectioDetailsDataMandatoryValidator);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean
  public IObCollectionDetailsDataMandatoryValidator createCollectionDetailsDataMandatoryValidator(
      IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep) {
    IObCollectionDetailsDataMandatoryValidator collectioDetailsDataMandatoryValidator =
        new IObCollectionDetailsDataMandatoryValidator();
    collectioDetailsDataMandatoryValidator
        .setCollectionDetailsGeneralModelRep(collectionDetailsGeneralModelRep);
    return collectioDetailsDataMandatoryValidator;
  }

  @Bean
  public IObCollectionDetailsValidator createCollectionDetailsValidator(
      IObCompanyTreasuriesGeneralModelRep companyTreasuriesGeneralModelRep,
      IObCompanyBankDetailsGeneralModelRep companyBankDetailsGeneralModelRep,
      CObTreasuryRep treasuryRep) {
    IObCollectionDetailsValidator collectionDetailsValidator = new IObCollectionDetailsValidator();
    collectionDetailsValidator
        .setCompanyBankDetailsGeneralModelRep(companyBankDetailsGeneralModelRep);
    collectionDetailsValidator
        .setCompanyTreasuriesGeneralModelRep(companyTreasuriesGeneralModelRep);
    collectionDetailsValidator.setTreasuryRep(treasuryRep);
    return collectionDetailsValidator;
  }
}
