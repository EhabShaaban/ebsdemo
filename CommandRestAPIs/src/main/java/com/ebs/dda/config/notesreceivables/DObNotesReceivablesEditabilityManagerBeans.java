package com.ebs.dda.config.notesreceivables;

import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableDetailsEditabilityManager;
import com.ebs.dda.accounting.notesreceivables.IObNotesReceivableReferenceDocumentsEditabilityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObNotesReceivablesEditabilityManagerBeans {
  @Bean
  public IObNotesReceivableDetailsEditabilityManager
      createNotesReceivableDetailsEditabilityManager() {
    IObNotesReceivableDetailsEditabilityManager notesReceivableDetailsEditabilityManager =
        new IObNotesReceivableDetailsEditabilityManager();
    return notesReceivableDetailsEditabilityManager;
  }

  @Bean
  public IObNotesReceivableReferenceDocumentsEditabilityManager
      createNotesReceivableReferenceDocumentsEditabilityManager() {
    IObNotesReceivableReferenceDocumentsEditabilityManager
        notesReceivableReferenceDocumentsEditabilityManager =
            new IObNotesReceivableReferenceDocumentsEditabilityManager();
    return notesReceivableReferenceDocumentsEditabilityManager;
  }
}
