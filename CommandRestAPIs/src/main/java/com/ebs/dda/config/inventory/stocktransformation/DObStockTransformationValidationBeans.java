package com.ebs.dda.config.inventory.stocktransformation;

import com.ebs.dda.inventory.stocktransformation.validation.DObStockTransformationCreationValidator;
import com.ebs.dda.repositories.inventory.stocktransformation.CObStockTransformationReasonRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObStockTransformationValidationBeans {

    @Bean
    public DObStockTransformationCreationValidator stockTransformationCreationValidator(
            CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
            CObStockTransformationReasonRep stockTransformationReasonRep,
            TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep
    ){
        DObStockTransformationCreationValidator stockTransformationCreationValidator= new DObStockTransformationCreationValidator();
        stockTransformationCreationValidator.setPurchasingUnitRep(purchasingUnitGeneralModelRep);
        stockTransformationCreationValidator.setStockTransformationReasonRep(stockTransformationReasonRep);
        stockTransformationCreationValidator.setStoreTransactionGeneralModelRep(storeTransactionGeneralModelRep);
        return stockTransformationCreationValidator;
    }
}
