package com.ebs.dda.config.accounting.collection;

import com.ebs.dda.accounting.collection.editability.IObCollectionDetailsEditabilityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObCollectionEditabilityBeans {
    @Bean
    public IObCollectionDetailsEditabilityManager createCollectionDetailsEditabilityManager (){
        IObCollectionDetailsEditabilityManager collectionDetailsEditabilityManager =
                new IObCollectionDetailsEditabilityManager();
        return collectionDetailsEditabilityManager;
    }

}
