package com.ebs.dda.config.order.salesreturnorder;

import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderCreateValidator;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderMandatoryValidator;
import com.ebs.dda.order.salesreturnorder.DObSalesReturnOrderValidator;
import com.ebs.dda.order.salesreturnorder.IObSalesReturnOrderItemMandatoryValidator;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.lookups.LObReasonRep;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerRep;
import com.ebs.dda.repositories.order.salesreturnorder.CObSalesReturnOrderTypeRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;
import com.ebs.dda.validation.salesreturnorder.IObSalesReturnOrderItemSaveEditValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObSalesReturnOrderValidationBeans {

  @Bean
  public IObSalesReturnOrderItemSaveEditValidator createIObSalesReturnOrderItemSaveEditValidator(
      LObReasonRep setLObReasonRep) {
    IObSalesReturnOrderItemSaveEditValidator validator =
        new IObSalesReturnOrderItemSaveEditValidator();
    validator.setLObReasonRep(setLObReasonRep);
    return validator;
  }

  @Bean
  public DObSalesReturnOrderCreateValidator createDObSalesReturnOrderCreateValidator(
      CObSalesReturnOrderTypeRep salesReturnOrderTypeRep,
      CObProductManagerRep productManagerRep,
      DocumentOwnerGeneralModelRep documentOwnerRep,
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {
    DObSalesReturnOrderCreateValidator validator = new DObSalesReturnOrderCreateValidator();
    validator.setSalesReturnOrderTypeRep(salesReturnOrderTypeRep);
    validator.setProductManagerRep(productManagerRep);
    validator.setDocumentOwnerRep(documentOwnerRep);
    validator.setDObSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);

    return validator;
  }

  @Bean
  public IObSalesReturnOrderItemMandatoryValidator createSalesReturnOrderItemMandatoryValidator(
      IObSalesReturnOrderItemGeneralModelRep itemGeneralModelRep) {
    IObSalesReturnOrderItemMandatoryValidator validator =
        new IObSalesReturnOrderItemMandatoryValidator();
    validator.setItemRep(itemGeneralModelRep);
    return validator;
  }

  @Bean
  public DObSalesReturnOrderMandatoryValidator createSalesReturnOrderMandatoryValidator(
      IObSalesReturnOrderItemMandatoryValidator itemMandatoryValidator) {
    DObSalesReturnOrderMandatoryValidator validator = new DObSalesReturnOrderMandatoryValidator();
    validator.setItemMandatoryValidator(itemMandatoryValidator);
    return validator;
  }

  @Bean
  public DObSalesReturnOrderValidator createSalesReturnOrderValidator(
      IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemGeneralModelRep) {
    DObSalesReturnOrderValidator validator = new DObSalesReturnOrderValidator();
    validator.setSalesReturnOrderItemGeneralModelRep(salesReturnOrderItemGeneralModelRep);
    return validator;
  }
}
