package com.ebs.dda.config.inventory.goodsreceipt;

import com.ebs.dda.inventory.goodsreceipt.validation.*;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.IObOrderPlantEnterpriseDataRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderConsigneeGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.ItemVendorRecordUnitOfMeasuresGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.*;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.ItemVendorRecordGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesreturnorder.IObSalesReturnOrderItemGeneralModelRep;
import com.ebs.dda.validation.goodsreceipt.DObGoodReceiptActivateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObGoodsReceiptValidationBeans {

  @Bean
  public DObGoodsReceiptCreationValidator goodsReceiptCreationValidator(
      DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep,
      DObPurchaseOrderConsigneeGeneralModelRep consigneeGeneralModelRep,
      StoreKeeperRep storeKeeperRep,
      CObStorehouseGeneralModelRep storehouseRep,
      IObOrderPlantEnterpriseDataRep plantEnterpriseDataRep,
      CObCompanyGeneralModelRep cObCompanyRep,
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep,
      CObPlantRep plantRep) {
    DObGoodsReceiptCreationValidator goodsReceiptCreationValidator =
        new DObGoodsReceiptCreationValidator();
    goodsReceiptCreationValidator.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    goodsReceiptCreationValidator.setPurchaseOrderConsigneeGeneralModelRep(
        consigneeGeneralModelRep);
    goodsReceiptCreationValidator.setSkeeperRep(storeKeeperRep);
    goodsReceiptCreationValidator.setStorehouseRep(storehouseRep);
    goodsReceiptCreationValidator.setPlantEnterpriseDataRep(plantEnterpriseDataRep);
    goodsReceiptCreationValidator.setcObCompanyRep(cObCompanyRep);
    goodsReceiptCreationValidator.setSalesReturnOrderGeneralModelRep(
        salesReturnOrderGeneralModelRep);
    goodsReceiptCreationValidator.setPlantRep(plantRep);
    return goodsReceiptCreationValidator;
  }

  @Bean
  public DObGoodsReceiptDifferenceReasonValidator
      createDObGoodsReceiptDifferenceReasonValidatorValidator(
          DObGoodsReceiptPurchaseOrderRep goodsReceiptRep) {
    return new DObGoodsReceiptDifferenceReasonValidator(goodsReceiptRep);
  }

  @Bean
  public DObGoodsReceiptGeneralDataSectionValidator
      createDObGoodsReceiptGeneralDataSectionValidator(
          DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    DObGoodsReceiptGeneralDataSectionValidator validator =
        new DObGoodsReceiptGeneralDataSectionValidator();
    validator.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    return validator;
  }

  @Bean
  public DObGoodsReceiptItemsSectionValidator createDObGoodsReceiptItemsSectionValidator(
      IObGoodsReceiptReceivedItemsGeneralModelRep recievedItemsGeneralModelRep,
      IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep itemBatchesGeneralModelRep,
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep itemQuantitiesGeneralModelRep,
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep goodsReceiptPurchaseOrderDataGeneralModelRep,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep,
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
          grSalesReturnQuantitiesGeneralModelRep,
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    DObGoodsReceiptItemsSectionValidator itemsSectionValidator =
        new DObGoodsReceiptItemsSectionValidator();
    itemsSectionValidator.setReceivedItemsGeneralModelRep(recievedItemsGeneralModelRep);
    itemsSectionValidator.setItemBatchesGeneralModelRep(itemBatchesGeneralModelRep);
    itemsSectionValidator.setGrPurchaseOrderItemQuantitiesGeneralModelRep(
        itemQuantitiesGeneralModelRep);
    itemsSectionValidator.setGoodsReceiptPurchaseOrderDataGeneralModelRep(
        goodsReceiptPurchaseOrderDataGeneralModelRep);
    itemsSectionValidator.setItemVendorRecordGeneralModelRep(itemVendorRecordGeneralModelRep);
    itemsSectionValidator.setGrSalesReturnQuantitiesGeneralModelRep(
        grSalesReturnQuantitiesGeneralModelRep);
    itemsSectionValidator.setGoodReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    return itemsSectionValidator;
  }

  @Bean
  public DObGoodReceiptActivateValidator createDObGoodReceiptActivateValidator(
      DObGoodsReceiptGeneralDataSectionValidator generalDataSectionValidator,
      DObGoodsReceiptItemsSectionValidator itemsSectionValidator,
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep,
      DObLandedCostGeneralModelRep landedCostGeneralModelRep,
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    DObGoodReceiptActivateValidator goodReceiptActivateValidator =
        new DObGoodReceiptActivateValidator();
    goodReceiptActivateValidator.setGeneralDataSectionValidator(generalDataSectionValidator);
    goodReceiptActivateValidator.setItemsSectionValidator(itemsSectionValidator);
    goodReceiptActivateValidator.setSalesReturnOrderGeneralModelRep(
        salesReturnOrderGeneralModelRep);
    goodReceiptActivateValidator.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
    goodReceiptActivateValidator.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    return goodReceiptActivateValidator;
  }

  @Bean
  public DObGoodsReceiptDetailValidator createDObGoodsReceiptDetailValidator(
      ItemVendorRecordUnitOfMeasuresGeneralModelRep itemVendorRecordUnitOfMeasuresGeneralModelRep,
      IObGoodsReceiptPurchaseOrderDataGeneralModelRep
          iObGoodsReceiptPurchaseOrderDataGeneralModelRep,
      MObItemGeneralModelRep itemGeneralModelRep,
      ItemVendorRecordGeneralModelRep itemVendorRecordGeneralModelRep,
      IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModelRep itemQuantitiesGeneralModelRep,
      IObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep
          iObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep,
      IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModelRep
          goodsReceiptSalesReturnItemQuantitiesGeneralModelRep,
      IObSalesReturnOrderItemGeneralModelRep salesReturnOrderItemGeneralModelRep,
      DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep) {
    DObGoodsReceiptDetailValidator dObGoodsReceiptDetailValidator =
        new DObGoodsReceiptDetailValidator();
    dObGoodsReceiptDetailValidator.setiObGoodsReceiptPurchaseOrderDataGeneralModelRep(
        iObGoodsReceiptPurchaseOrderDataGeneralModelRep);
    dObGoodsReceiptDetailValidator.setItemVendorRecordUnitOfMeasuresGeneralModelRep(
        itemVendorRecordUnitOfMeasuresGeneralModelRep);
    dObGoodsReceiptDetailValidator.setItemGeneralModelRep(itemGeneralModelRep);
    dObGoodsReceiptDetailValidator.setItemVendorRecordGeneralModelRep(
        itemVendorRecordGeneralModelRep);
    dObGoodsReceiptDetailValidator.setGoodsReceiptPurchaseOrderItemQuantitiesGeneralModelRep(
        itemQuantitiesGeneralModelRep);
    dObGoodsReceiptDetailValidator.setiObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep(
        iObGoodsReceiptBatchedReceivedItemBatchesGeneralModelRep);
    dObGoodsReceiptDetailValidator.setGoodsReceiptSalesReturnItemQuantitiesGeneralModelRep(
        goodsReceiptSalesReturnItemQuantitiesGeneralModelRep);
    dObGoodsReceiptDetailValidator.setSalesReturnOrderItemGeneralModelRep(
        salesReturnOrderItemGeneralModelRep);
    dObGoodsReceiptDetailValidator.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    return dObGoodsReceiptDetailValidator;
  }
}
