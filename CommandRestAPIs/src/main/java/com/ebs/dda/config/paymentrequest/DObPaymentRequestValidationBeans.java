package com.ebs.dda.config.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.validation.DObPaymentRequestPaymentDetailsMandatoryValidator;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderCompanyGeneralModelRep;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObPaymentRequestActivatePreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.*;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyTreasuriesGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.treasury.CObTreasuryRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRep;
import com.ebs.dda.validation.paymentrequest.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObPaymentRequestValidationBeans {

  @Bean
  public DObDownPaymentCreationValidator createDownPaymentCreationValidator(
      CObPurchasingUnitGeneralModelRep purchasingUnitRep,
      MObVendorGeneralModelRep vendorGeneralModel,
      MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep,
      DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderRep,
      IObOrderCompanyGeneralModelRep poComapnyGMRep) {
    DObDownPaymentCreationValidator paymentRequestCreationValidator =
        new DObDownPaymentCreationValidator();
    paymentRequestCreationValidator.setPurchasingUnitRep(purchasingUnitRep);
    paymentRequestCreationValidator.setVendorGeneralModelRep(vendorGeneralModel);
    paymentRequestCreationValidator.setVendorPurchaseUnitGeneralModelRep(
        vendorPurchaseUnitGeneralModelRep);
    paymentRequestCreationValidator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    paymentRequestCreationValidator.setPurchaseOrderRep(purchaseOrderRep);
    paymentRequestCreationValidator.setPoComapnyGMRep(poComapnyGMRep);
    return paymentRequestCreationValidator;
  }

  @Bean
  public DObGeneralPaymentCreationValidator createGeneralPaymentCreationValidator(
      CObPurchasingUnitGeneralModelRep purchasingUnitRep,
      DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep,
      DObPurchaseOrderGeneralModelRep purchaseOrderRep,
      IObOrderCompanyGeneralModelRep poCompanyGMRep) {
    DObGeneralPaymentCreationValidator paymentRequestCreationValidator =
        new DObGeneralPaymentCreationValidator();
    paymentRequestCreationValidator.setPurchasingUnitRep(purchasingUnitRep);
    paymentRequestCreationValidator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    paymentRequestCreationValidator.setPurchaseOrderRep(purchaseOrderRep);
    paymentRequestCreationValidator.setPoComapnyGMRep(poCompanyGMRep);
    return paymentRequestCreationValidator;
  }

  @Bean
  public DObPaymentAgainstVendorInvoiceCreationValidator createPaymentAgainstVendorInvoiceCreationValidator(
          CObPurchasingUnitGeneralModelRep purchasingUnitRep,
          MObVendorGeneralModelRep vendorGeneralModel,
          MObVendorPurchaseUnitGeneralModelRep vendorPurchaseUnitGeneralModelRep,
          DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep,
          DObVendorInvoiceGeneralModelRep vendorInvoiceRep) {
    DObPaymentAgainstVendorInvoiceCreationValidator paymentRequestCreationValidator =
        new DObPaymentAgainstVendorInvoiceCreationValidator();
    paymentRequestCreationValidator.setPurchasingUnitRep(purchasingUnitRep);
    paymentRequestCreationValidator.setVendorGeneralModelRep(vendorGeneralModel);
    paymentRequestCreationValidator.setVendorPurchaseUnitGeneralModelRep(
        vendorPurchaseUnitGeneralModelRep);
    paymentRequestCreationValidator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    paymentRequestCreationValidator.setVendorInvoiceRep(vendorInvoiceRep);
    return paymentRequestCreationValidator;
  }

  @Bean
  public DObPaymentAgainstCreditNoteCreationValidator createPaymentAgainstCreditNoteCreationValidator(
      CObPurchasingUnitGeneralModelRep purchasingUnitRep,
      DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
    DObPaymentAgainstCreditNoteCreationValidator paymentRequestCreationValidator =
        new DObPaymentAgainstCreditNoteCreationValidator();
    paymentRequestCreationValidator.setPurchasingUnitRep(purchasingUnitRep);
    paymentRequestCreationValidator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    return paymentRequestCreationValidator;
  }
  @Bean
  public DObUnderSettlementPaymentCreationValidator createUnderSettlementPaymentCreationValidator(
      CObPurchasingUnitGeneralModelRep purchasingUnitRep,
      DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
    DObUnderSettlementPaymentCreationValidator paymentRequestCreationValidator =
        new DObUnderSettlementPaymentCreationValidator();
    paymentRequestCreationValidator.setPurchasingUnitRep(purchasingUnitRep);
    paymentRequestCreationValidator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    return paymentRequestCreationValidator;
  }

  @Bean("DObPaymentRequestActivationValidator")
  @DependsOn ({"JournalBalanceCodeGenerator"})
  public DObPaymentRequestActivationValidator createPaymentRequestActivationValidator(
    DObPaymentRequestActivatePreparationGeneralModelRep paymentRequestActivatePreparationGeneralModel,
    CObFiscalYearRep fiscalYearRep) {
    DObPaymentRequestActivationValidator validator =
        new DObPaymentRequestActivationValidator();
    validator.setPaymentRequestActivatePreparationGeneralModelRep(paymentRequestActivatePreparationGeneralModel);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean("DObPaymentAgainstPurchaseOrderActivationValidator")
  @DependsOn ({"JournalBalanceCodeGenerator"})
  public DObPaymentAgainstPurchaseOrderActivationValidator createPaymentAgainstPurchaseOrderActivationValidator(
    DObPurchaseOrderRep purchaseOrderRep,
    DObPaymentRequestActivatePreparationGeneralModelRep activatePreparationGeneralModelRep,
    CObFiscalYearRep fiscalYearRep
  ) {
    DObPaymentAgainstPurchaseOrderActivationValidator validator =
        new DObPaymentAgainstPurchaseOrderActivationValidator();
    validator.setDObPurchaseOrderRep(purchaseOrderRep);
    validator.setPaymentRequestActivatePreparationGeneralModelRep(activatePreparationGeneralModelRep);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean("DObPaymentGeneralForPOActivationValidator")
  @DependsOn ({"JournalBalanceCodeGenerator"})
  public DObPaymentGeneralForPOActivationValidator createPaymentGeneralForPOActivationValidator(
    DObPaymentRequestActivatePreparationGeneralModelRep activatePreparationGeneralModelRep,
    OrderPaymentsGeneralModelRep orderPaymentsGeneralModelRep,
    DObLandedCostGeneralModelRep landedCostGeneralModelRep,
    CObFiscalYearRep fiscalYearRep
  ) {
    DObPaymentGeneralForPOActivationValidator validator =
        new DObPaymentGeneralForPOActivationValidator();

    validator.setPaymentRequestActivatePreparationGeneralModelRep(activatePreparationGeneralModelRep);
    validator.setOrderPaymentsGeneralModelRep(orderPaymentsGeneralModelRep);
    validator.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean("DObPaymentAgainstVendorInvoiceActivationValidator")
  @DependsOn({"JournalBalanceCodeGenerator"})
  public DObPaymentAgainstVendorInvoiceActivationValidator createPaymentAgainstVendorInvoiceActivationValidator(
    IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep,
    DObPaymentRequestActivatePreparationGeneralModelRep activatePreparationGeneralModelRep,
    CObFiscalYearRep fiscalYearRep
  ) {
    DObPaymentAgainstVendorInvoiceActivationValidator validator =
            new DObPaymentAgainstVendorInvoiceActivationValidator();
    validator.setiObInvoiceSummaryGeneralModelRep(
            invoiceSummaryGeneralModelRep);
    validator.setPaymentRequestActivatePreparationGeneralModelRep(activatePreparationGeneralModelRep);
    validator.setFiscalYearRep(fiscalYearRep);
    return validator;
  }

  @Bean
  @DependsOn ({"JournalBalanceCodeGenerator"})
  public DObPaymentRequestActivateMandatoriesValidator createPaymentRequestActivateMandatoriesValidator(
          DObPaymentRequestPaymentDetailsMandatoryValidator
          paymentRequestPaymentDetailsMandatoryValidator,
          DObPaymentRequestActivatePreparationGeneralModelRep activatePreparationGeneralModelRep) {
    DObPaymentRequestActivateMandatoriesValidator validator =
        new DObPaymentRequestActivateMandatoriesValidator();
    validator.setPaymentDetailsMandatoryValidator(
        paymentRequestPaymentDetailsMandatoryValidator);
    validator.setPaymentRequestActivatePreparationGeneralModelRep(activatePreparationGeneralModelRep);
    return validator;
  }

  @Bean
  public IObDownPaymentDetailsSaveValidator
      createDownPaymentDetailsSaveValidator(
          DObPaymentRequestSaveDetailsPreparationGeneralModelRep paymentRequestGeneralModelRep,
          IObCompanyBankDetailsGeneralModelRep companyBankDetailsRep,
          CObTreasuryRep treasuryRep,
          IObCompanyTreasuriesGeneralModelRep treasuriesGeneralModelRep) {
    IObDownPaymentDetailsSaveValidator paymentPaymentDetailsSaveValidator =
        new IObDownPaymentDetailsSaveValidator();

    paymentPaymentDetailsSaveValidator.setPaymentDetailsGeneralModelRep(
            paymentRequestGeneralModelRep);
    paymentPaymentDetailsSaveValidator.setCompanyBankDetailsGeneralModelRep(companyBankDetailsRep);
    paymentPaymentDetailsSaveValidator.setTreasuryRep(treasuryRep);
    paymentPaymentDetailsSaveValidator.setCompanyTreasuriesGeneralModelRep(
        treasuriesGeneralModelRep);
    return paymentPaymentDetailsSaveValidator;
  }

  @Bean
  public IObGeneralPaymentDetailsSaveValidator
      createGeneralPaymentDetailsSaveValidator(
          CObCurrencyRep currencyRep,
          DObPaymentRequestSaveDetailsPreparationGeneralModelRep paymentRequestGeneralModelRep,
          IObCompanyBankDetailsGeneralModelRep companyBankDetailsRep,
          MObItemGeneralModelRep itemGMRep,
          CObTreasuryRep treasuryRep,
          IObCompanyTreasuriesGeneralModelRep treasuriesGeneralModelRep) {
    IObGeneralPaymentDetailsSaveValidator paymentPaymentDetailsSaveValidator =
        new IObGeneralPaymentDetailsSaveValidator();

    paymentPaymentDetailsSaveValidator.setPaymentDetailsGeneralModelRep(
            paymentRequestGeneralModelRep);
    paymentPaymentDetailsSaveValidator.setCurrencyRep(currencyRep);
    paymentPaymentDetailsSaveValidator.setCompanyBankDetailsGeneralModelRep(companyBankDetailsRep);
    paymentPaymentDetailsSaveValidator.setItemGMRep(itemGMRep);
    paymentPaymentDetailsSaveValidator.setTreasuryRep(treasuryRep);
    paymentPaymentDetailsSaveValidator.setCompanyTreasuriesGeneralModelRep(
        treasuriesGeneralModelRep);
    return paymentPaymentDetailsSaveValidator;
  }

  @Bean
  public IObPaymentAgainstCreditNoteDetailsSaveValidator
      createPaymentAgainstCreditNoteDetailsSaveValidator(
          DObPaymentRequestSaveDetailsPreparationGeneralModelRep paymentRequestGeneralModelRep,
          IObCompanyBankDetailsGeneralModelRep companyBankDetailsRep,
          CObTreasuryRep treasuryRep,
          IObCompanyTreasuriesGeneralModelRep treasuriesGeneralModelRep) {
    IObPaymentAgainstCreditNoteDetailsSaveValidator paymentPaymentDetailsSaveValidator =
        new IObPaymentAgainstCreditNoteDetailsSaveValidator();

    paymentPaymentDetailsSaveValidator.setPaymentDetailsGeneralModelRep(
            paymentRequestGeneralModelRep);
    paymentPaymentDetailsSaveValidator.setCompanyBankDetailsGeneralModelRep(companyBankDetailsRep);
    paymentPaymentDetailsSaveValidator.setTreasuryRep(treasuryRep);
    paymentPaymentDetailsSaveValidator.setCompanyTreasuriesGeneralModelRep(
        treasuriesGeneralModelRep);
    return paymentPaymentDetailsSaveValidator;
  }

  @Bean
  public IObPaymentAgainstVendorInvoiceDetailsSaveValidator
      createPaymentAgainstVendorInvoiceDetailsSaveValidator(
          DObPaymentRequestSaveDetailsPreparationGeneralModelRep paymentRequestGeneralModelRep,
          IObCompanyBankDetailsGeneralModelRep companyBankDetailsRep,
          CObTreasuryRep treasuryRep,
          IObCompanyTreasuriesGeneralModelRep treasuriesGeneralModelRep) {
    IObPaymentAgainstVendorInvoiceDetailsSaveValidator paymentPaymentDetailsSaveValidator =
        new IObPaymentAgainstVendorInvoiceDetailsSaveValidator();

    paymentPaymentDetailsSaveValidator.setPaymentDetailsGeneralModelRep(
            paymentRequestGeneralModelRep);
    paymentPaymentDetailsSaveValidator.setCompanyBankDetailsGeneralModelRep(companyBankDetailsRep);
    paymentPaymentDetailsSaveValidator.setTreasuryRep(treasuryRep);
    paymentPaymentDetailsSaveValidator.setCompanyTreasuriesGeneralModelRep(
        treasuriesGeneralModelRep);
    return paymentPaymentDetailsSaveValidator;
  }
  @Bean
  public IObUnderSettlementPaymentDetailsSaveValidator
  createUnderSettlementPaymentDetailsSaveValidator(
          CObCurrencyRep currencyRep,
          DObPaymentRequestSaveDetailsPreparationGeneralModelRep paymentRequestGeneralModelRep,
          IObCompanyBankDetailsGeneralModelRep companyBankDetailsRep,
          CObTreasuryRep treasuryRep,
          IObCompanyTreasuriesGeneralModelRep treasuriesGeneralModelRep) {
    IObUnderSettlementPaymentDetailsSaveValidator paymentPaymentDetailsSaveValidator =
            new IObUnderSettlementPaymentDetailsSaveValidator();

    paymentPaymentDetailsSaveValidator.setPaymentDetailsGeneralModelRep(
            paymentRequestGeneralModelRep);
    paymentPaymentDetailsSaveValidator.setCurrencyRep(currencyRep);
    paymentPaymentDetailsSaveValidator.setCompanyBankDetailsGeneralModelRep(companyBankDetailsRep);
    paymentPaymentDetailsSaveValidator.setTreasuryRep(treasuryRep);
    paymentPaymentDetailsSaveValidator.setCompanyTreasuriesGeneralModelRep(
            treasuriesGeneralModelRep);
    return paymentPaymentDetailsSaveValidator;
  }


  @Bean
  public DObPaymentRequestPaymentDetailsMandatoryValidator
      createDObPaymentRequestPaymentDetailsMandatoryValidator(
          DObPaymentRequestGeneralModelRep paymentRequestGeneralModelRep,
          IObPaymentRequestPaymentDetailsGeneralModelRep
              paymentRequestPaymentDetailsGeneralModelRep) {
    DObPaymentRequestPaymentDetailsMandatoryValidator mandatoryValidator =
        new DObPaymentRequestPaymentDetailsMandatoryValidator();
    mandatoryValidator.setPaymentRequestGeneralModelRep(paymentRequestGeneralModelRep);
    mandatoryValidator.setPaymentDetailsGeneralModelRep(
        paymentRequestPaymentDetailsGeneralModelRep);
    return mandatoryValidator;
  }
}
