package com.ebs.dda.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.env.Environment;
import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.foundation.realization.AllowedActionService;
import com.ebs.dac.foundation.realization.concurrency.ConcurrentDataAccessManagersPool;
import com.ebs.dac.foundation.realization.concurrency.policies.TimeoutEditTokenValidityPolicy;
import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dac.restexceptionhandlers.ExceptionResponseHandlerFactory;
import com.ebs.dac.security.jpa.repositories.AuthorizationConditionRepository;
import com.ebs.dac.security.jpa.repositories.UserAssignmentRepository;
import com.ebs.dac.security.jpa.repositories.UserPermissionGeneralModelRepository;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dac.security.services.AuthorizationService;
import com.ebs.dac.security.services.AuthorizedPurchaseUnitService;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.functions.serialize.SerializionUtils;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.rx.ObservablesEventBus;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 20, 2018 10:55:42 AM
 */
@Configuration
public class GeneralBeans {

  @Bean(name = "userAccountSecurityService")
  public UserAccountSecurityService createUserAccountSecurityService() {
    UserAccountSecurityService userAccountSecurityService = new UserAccountSecurityService();
    return userAccountSecurityService;
  }

  @Bean(name = "authorizationService")
  @DependsOn({"userAccountSecurityService", "UserAssignmentRepository"})
  public AuthorizationService createAuthorizationService(
      UserAccountSecurityService userAccountSecurityService,
      UserAssignmentRepository userAssignmentRepository,
      UserPermissionGeneralModelRepository userPermissionGeneralModelRepository,
      AuthorizationConditionRepository authorizationConditionRepository,
      AuthorizationManager authorizationManager) {
    AuthorizationService authorizationService = new AuthorizationService(userAccountSecurityService,
        userAssignmentRepository, userPermissionGeneralModelRepository,
        authorizationConditionRepository, authorizationManager);
    return authorizationService;
  }

  @Bean("goodsReceiptAllowedActionService")
  @DependsOn({"authorizationService"})
  public AllowedActionService createAllowedActionService(AuthorizationService authorizationService)
      throws Exception {
    AllowedActionService allowedActionService =
        new AllowedActionService(authorizationService, new DObGoodsReceiptStateMachine());
    return allowedActionService;
  }

  @Bean("concurrentDataAccessManagersPool")
  public IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersObjectPool(
      Environment env, IUserAccountSecurityService userAccountSecurityService) {
    return new ConcurrentDataAccessManagersPool(new TimeoutEditTokenValidityPolicy(
        env.getRequiredProperty("concurrentDataAccessManager.initialTimeout", Integer.class),
        env.getRequiredProperty("concurrentDataAccessManager.extention", Integer.class)), null)
            .userAccountSecurityService(userAccountSecurityService);
  }

  @Bean("authorizedPurchaseUnitsService")
  @DependsOn({"AuthorizationManager"})
  public AuthorizedPurchaseUnitService createAuthorizedPurchaseUnitsService(
      AuthorizationManager authorizationManager,
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep) {
    AuthorizedPurchaseUnitService authorizedPurchaseUnitsService =
        new AuthorizedPurchaseUnitService();
    authorizedPurchaseUnitsService.setAuthorizationManager(authorizationManager);
    authorizedPurchaseUnitsService.setPurchasingUnitRep(purchasingUnitGeneralModelRep);
    return authorizedPurchaseUnitsService;
  }

  @Bean
  public ObservablesEventBus createObservablesEventBus() {
    return ObservablesEventBus.getInstance();
  }

  @Bean
  public SchemaValidator createSchemaValidator() {
    return new SchemaValidator();
  }

  @Bean
  @DependsOn({"concurrentDataAccessManagersPool"})
  public ExceptionResponseHandlerFactory createExceptionResponseHandlerFactory(
      IConcurrentDataAccessManagersObjectPool accessManagersPool) {
    return new ExceptionResponseHandlerFactory(accessManagersPool);
  }

  @Bean
  public SerializionUtils createSerializionUtils() {
    return new SerializionUtils();
  }
}
