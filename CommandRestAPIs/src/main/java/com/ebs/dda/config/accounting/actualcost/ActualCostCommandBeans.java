package com.ebs.dda.config.accounting.actualcost;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.accounting.actualcost.ActualCostCalculateCommand;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.DObCostingRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemGeneralModelRep;
import com.ebs.dda.repositories.accounting.costing.IObCostingItemRep;
import com.ebs.dda.repositories.accounting.journalentry.IObJournalEntryItemsGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPostingDetailsRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceRemainingGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActualCostCommandBeans {

	@Bean
	public ActualCostCalculateCommand createActualCostCalculateCommand(
			IObGoodsReceiptPurchaseOrderDataGeneralModelRep goodsReceiptPORep,
			IObVendorInvoiceRemainingGeneralModelRep invoiceRemainingRep,
			IObPaymentRequestPaymentDetailsGeneralModelRep paymentRequestRep,
			IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
			IObJournalEntryItemsGeneralModelRep journalEntryItemsRep,
			LObGlobalAccountConfigGeneralModelRep accountConfigGeneralModelRep,
			IObInvoiceSummaryGeneralModelRep invoiceSummaryRep,
			IObCostingItemGeneralModelRep grAccountingDocumentItemRep,
			IObCostingDetailsGeneralModelRep dObGoodsReceiptAccountingDocumentRep,
			IObVendorInvoiceItemsGeneralModelRep invoiceItemsRep,
			IObCostingItemRep grItemRep,
			IUserAccountSecurityService userAccountSecurityService,
			DObCostingRep accountingDocumentRep
	) {
		ActualCostCalculateCommand command = new ActualCostCalculateCommand();
		command.setGoodsReceiptPORep(goodsReceiptPORep);
		command.setInvoiceRemainingRep(invoiceRemainingRep);
		command.setPaymentRequestRep(paymentRequestRep);
		command.setPaymentRequestPostingDetailsRep(paymentRequestPostingDetailsRep);
		command.setJournalEntryItemsRep(journalEntryItemsRep);
		command.setAccountConfigGeneralModelRep(accountConfigGeneralModelRep);
		command.setInvoiceSummaryRep(invoiceSummaryRep);
		command.setCostingItemGeneralModelRep(grAccountingDocumentItemRep);
		command.setCostingDetailsGeneralModelRep(dObGoodsReceiptAccountingDocumentRep);
		command.setInvoiceItemsRep(invoiceItemsRep);
		command.setCostingItemRep(grItemRep);
		command.setUserAccountSecurityService(userAccountSecurityService);
		command.setCostingRep(accountingDocumentRep);
		return command;
	}
}
