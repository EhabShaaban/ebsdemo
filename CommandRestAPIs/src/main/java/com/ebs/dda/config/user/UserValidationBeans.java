package com.ebs.dda.config.user;

import com.ebs.dac.security.jpa.repositories.UserRepository;
import com.ebs.dda.validation.user.UserChangeDefaultPasswordValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserValidationBeans {

  @Bean
  public UserChangeDefaultPasswordValidator createUserChangeDefaultPasswordValidator(
      UserRepository userRepository) {
    UserChangeDefaultPasswordValidator validator = new UserChangeDefaultPasswordValidator();
    validator.setUserRepository(userRepository);
    return validator;
  }
}
