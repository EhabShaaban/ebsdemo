package com.ebs.dda.config.masterdata;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorAccountingDetailsEditCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorCreateCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorDeleteCommand;
import com.ebs.dda.commands.masterdata.vendor.MObVendorGeneralDataEditCommand;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.masterdata.dbo.jpa.repositories.events.MObVendorEventRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleRep;
import com.ebs.dda.repositories.masterdata.vendor.CObVendorRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorPurchaseUnitRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class MObVendorCommandBeans {

  @Bean
  public MObVendorCreateCommand createVendorCreateCommand(
          MasterDataCodeGenrator masterDataCodeGenrator,
          IUserAccountSecurityService userAccountSecurityService,
          MObVendorRep mObVendorRep,
          CObVendorRep cObVendorRep,
          MObVendorEventRep vendorEventRep,
          CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
          CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep,
          TransactionManagerDelegate transactionManagerDelegate) {
    MObVendorCreateCommand command = new MObVendorCreateCommand(masterDataCodeGenrator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setMObVendorRep(mObVendorRep);
    command.setPurchasingResponsibleGeneralModelRep(purchasingResponsibleGeneralModelRep);
    command.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    command.setTransactionManagerDelegate(transactionManagerDelegate);
    command.setVendorEventRep(vendorEventRep);
    command.setcObVendorRep(cObVendorRep);
    return command;
  }

  @Bean
  MObVendorDeleteCommand createVendorDeleteCommand(
          MObVendorRep vendorRep, EntityLockCommand<MObVendor> entityLockCommand) {
    MObVendorDeleteCommand vendorDeleteCommand = new MObVendorDeleteCommand();
    vendorDeleteCommand.setMObVendorRep(vendorRep);
    vendorDeleteCommand.setEntityLockCommand(entityLockCommand);
    return vendorDeleteCommand;
  }

  @Bean("vendorLockCommand")
  public EntityLockCommand<MObVendor> createEntityLockCommand(
          IUserAccountSecurityService userAccountSecurityService,
          IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
          MObVendorRep vendortRep) {

    EntityLockCommand<MObVendor> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
            concurrentDataAccessManagersPool.getConcurrentDataAccessManager(MObVendor.class.getName()));
    entityLockCommand.setRepository(vendortRep);
    return entityLockCommand;
  }

  @Bean("vendorLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
          @Qualifier("vendorLockCommand") EntityLockCommand entityLockCommand) throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name = new ObjectName("com.ebs.dda.commands.general:name=vendorLockCommandJMXBean");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public MObVendorGeneralDataEditCommand createVendorGeneralDataEditCommand(
          @Qualifier("vendorLockCommand") EntityLockCommand entityLockCommand,
          CObPurchasingResponsibleRep purchasingResponsibleRep,
          CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
          MObVendorRep vendorRep,
          IObVendorPurchaseUnitRep vendorPurchaseUnitRep,
          IUserAccountSecurityService userAccountSecurityService) {
    MObVendorGeneralDataEditCommand generalDataEditCommand = new MObVendorGeneralDataEditCommand();
    generalDataEditCommand.setEntityLockCommand(entityLockCommand);
    generalDataEditCommand.setPurchasingResponsibleRep(purchasingResponsibleRep);
    generalDataEditCommand.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    generalDataEditCommand.setVendorRep(vendorRep);
    generalDataEditCommand.setVendorPurchaseUnitRep(vendorPurchaseUnitRep);
    generalDataEditCommand.setUserAccountSecurityService(userAccountSecurityService);
    return generalDataEditCommand;
  }

  @Bean
  public MObVendorAccountingDetailsEditCommand createVendorAccountingDetailsEditCommand(
          @Qualifier("vendorLockCommand") EntityLockCommand entityLockCommand,
          HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep,
          MObVendorRep vendorRep,
          IObVendorAccountingInfoRep vendorAccountingInfoRep,
          IUserAccountSecurityService userAccountSecurityService) {
    MObVendorAccountingDetailsEditCommand accountingDetailsEditCommand =
            new MObVendorAccountingDetailsEditCommand();
    accountingDetailsEditCommand.setEntityLockCommand(entityLockCommand);
    accountingDetailsEditCommand.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
    accountingDetailsEditCommand.setVendorRep(vendorRep);
    accountingDetailsEditCommand.setUserAccountSecurityService(userAccountSecurityService);
    accountingDetailsEditCommand.setVendorAccountingInfoRep(vendorAccountingInfoRep);
    return accountingDetailsEditCommand;
  }
}
