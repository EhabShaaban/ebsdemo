package com.ebs.dda.config;

import com.ebs.dac.aop.logging.LoggerAspect;
import com.ebs.dac.dbo.jpa.repositories.BusinessObjectRepository;
import com.ebs.dac.spring.configs.RepositoryFactoryBean;
import com.ebs.dda.config.accounting.accountingnote.DObAccountingNoteCommandBeans;
import com.ebs.dda.config.accounting.actualcost.ActualCostCommandBeans;
import com.ebs.dda.config.accounting.actualcost.ActualCostValidationBeans;
import com.ebs.dda.config.accounting.collection.CollectionFunctionsBeans;
import com.ebs.dda.config.accounting.collection.DObCollectionCommandBeans;
import com.ebs.dda.config.accounting.collection.DObCollectionEditabilityBeans;
import com.ebs.dda.config.accounting.collection.DObCollectionValidationBeans;
import com.ebs.dda.config.accounting.costing.CostingCommandBeans;
import com.ebs.dda.config.accounting.costing.CostingFunctionBeans;
import com.ebs.dda.config.accounting.costing.DObCostingValidationBeans;
import com.ebs.dda.config.accounting.initialbalanceupload.DObInitialBalanceUploadCommandBeans;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceCommandBeans;
import com.ebs.dda.config.accounting.journalentry.DObJournalEntryCommandBeans;
import com.ebs.dda.config.accounting.journalentry.DObJournalEntryVendorInvoiceCommandBeans;
import com.ebs.dda.config.accounting.journalentry.DObPaymentJournalEntryCommandBeans;
import com.ebs.dda.config.accounting.landedcost.DObLandedCostCommandBeans;
import com.ebs.dda.config.accounting.landedcost.DObLandedCostEditabilityBeans;
import com.ebs.dda.config.accounting.landedcost.DObLandedCostValidationBeans;
import com.ebs.dda.config.accounting.paymentrequest.DObPaymentRequestEditabilityBeans;
import com.ebs.dda.config.accounting.paymentrequest.PaymentFunctionsBeans;
import com.ebs.dda.config.accounting.settlement.DObSettlementCommandBeans;
import com.ebs.dda.config.accounting.settlement.DObSettlementValidationBeans;
import com.ebs.dda.config.accounting.settlement.SettlementFunctionBeans;
import com.ebs.dda.config.accounting.vendorinvoice.DObVendorInvoiceCommandBeans;
import com.ebs.dda.config.accounting.vendorinvoice.DObVendorInvoiceEditabilityBeans;
import com.ebs.dda.config.accounting.vendorinvoice.DObVendorInvoiceValidationBeans;
import com.ebs.dda.config.accounting.vendorinvoice.VendorInvoiceFunctionsBeans;
import com.ebs.dda.config.inventory.goodsissue.DObGoodsIssueCommandBeans;
import com.ebs.dda.config.inventory.goodsissue.DObGoodsIssueValidationBeans;
import com.ebs.dda.config.inventory.goodsissue.GoodsIssueFunctionBeans;
import com.ebs.dda.config.inventory.goodsreceipt.DObGoodsReceiptCommandBeans;
import com.ebs.dda.config.inventory.goodsreceipt.DObGoodsReceiptEditabilityBeans;
import com.ebs.dda.config.inventory.goodsreceipt.DObGoodsReceiptValidationBeans;
import com.ebs.dda.config.inventory.goodsreceipt.GoodsReceiptFunctionsBeans;
import com.ebs.dda.config.inventory.initialstockupload.DObInitialStockUploadCommandBeans;
import com.ebs.dda.config.inventory.inventorydocument.DObInventoryDocumentCommandBeans;
import com.ebs.dda.config.inventory.stockavailability.CObStockAvailabilityCommandBeans;
import com.ebs.dda.config.inventory.stocktransformation.DObStockTransformationCommandBeans;
import com.ebs.dda.config.inventory.stocktransformation.DObStockTransformationValidationBeans;
import com.ebs.dda.config.inventory.storetransaction.TObStoreTransactionCommandBeans;
import com.ebs.dda.config.masterdata.*;
import com.ebs.dda.config.masterdata.accounting.CObExchangeRateValidationdBeans;
import com.ebs.dda.config.notesreceivables.DObNotesReceivableValidationBeans;
import com.ebs.dda.config.notesreceivables.DObNotesReceivablesCommandBeans;
import com.ebs.dda.config.notesreceivables.DObNotesReceivablesEditabilityManagerBeans;
import com.ebs.dda.config.notesreceivables.NotesReceivableFunctionsBeans;
import com.ebs.dda.config.order.salesorder.DObSalesOrderCommandBeans;
import com.ebs.dda.config.order.salesorder.DObSalesOrderEditabilityManagerBeans;
import com.ebs.dda.config.order.salesorder.DObSalesOrderValidationBeans;
import com.ebs.dda.config.order.salesreturnorder.DObSalesReturnOrderCommandBeans;
import com.ebs.dda.config.order.salesreturnorder.DObSalesReturnOrderEditabilityManagerBeans;
import com.ebs.dda.config.order.salesreturnorder.DObSalesReturnOrderValidationBeans;
import com.ebs.dda.config.paymentrequest.DObPaymentCreateCommandBeans;
import com.ebs.dda.config.paymentrequest.DObPaymentRequestCommandBeans;
import com.ebs.dda.config.paymentrequest.DObPaymentRequestValidationBeans;
import com.ebs.dda.config.purchaseorder.DObPurchaseOrderCommandBeans;
import com.ebs.dda.config.purchaseorder.DObPurchaseOrderEditabilityBeans;
import com.ebs.dda.config.purchaseorder.DObPurchaseOrderValidationBeans;
import com.ebs.dda.config.sales.invoice.DObSalesInvoiceCommandBeans;
import com.ebs.dda.config.sales.invoice.DObSalesInvoiceValidationBeans;
import com.ebs.dda.config.sales.invoice.SalesInvoiceFunctionsBeans;
import com.ebs.dda.config.user.UserCommandBeans;
import com.ebs.dda.config.user.UserValidationBeans;
import com.ebs.dda.validation.paymentrequest.DObPaymentCreationValidatorFactory;
import com.ebs.dda.validation.paymentrequest.IObPaymentSaveDetailsValidatorFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@Import({
  CObJournalBalanceCommandBeans.class,
  DObPurchaseOrderValidationBeans.class,
  DObPurchaseOrderEditabilityBeans.class,
  DObPurchaseOrderCommandBeans.class,
  DObAccountingNoteCommandBeans.class,
  PaymentFunctionsBeans.class,
  DObPaymentRequestCommandBeans.class,
  DObPaymentCreateCommandBeans.class,
  DObPaymentRequestValidationBeans.class,
  CObExchangeRateValidationdBeans.class,
  DObGoodsReceiptCommandBeans.class,
  GoodsReceiptFunctionsBeans.class,
  GoodsIssueFunctionBeans.class,
  DObInventoryDocumentCommandBeans.class,
  DObGoodsReceiptValidationBeans.class,
  DObGoodsReceiptEditabilityBeans.class,
  DObGoodsIssueCommandBeans.class,
  DObGoodsIssueValidationBeans.class,
  DObInitialStockUploadCommandBeans.class,
  DObInitialBalanceUploadCommandBeans.class,
  MasterDataCommandBeans.class,
  CObMeasureCommandBeans.class,
  MObVendorCommandBeans.class,
  MasterDataValidationBeans.class,
  CObCompanyCommandBeans.class,
  CodeGeneratorBeans.class,
  DObVendorInvoiceCommandBeans.class,
  DObVendorInvoiceValidationBeans.class,
  DObVendorInvoiceEditabilityBeans.class,
  VendorInvoiceFunctionsBeans.class,
  CostingFunctionBeans.class,
  DObCostingValidationBeans.class,
  GeneralBeans.class,
  CObExchangeRateCommandBeans.class,
  HObChartOfAccountCommandBeans.class,
  DObPaymentRequestEditabilityBeans.class,
  HObChartOfAccountValidationBeans.class,
  DObJournalEntryCommandBeans.class,
  DObJournalEntryVendorInvoiceCommandBeans.class,
  DObPaymentJournalEntryCommandBeans.class,
  UserCommandBeans.class,
  UserValidationBeans.class,
  DObSalesInvoiceCommandBeans.class,
  DObSalesInvoiceValidationBeans.class,
  SalesInvoiceFunctionsBeans.class,
  TObStoreTransactionCommandBeans.class,
  DObNotesReceivablesCommandBeans.class,
  DObNotesReceivableValidationBeans.class,
  DObCollectionCommandBeans.class,
  DObCollectionValidationBeans.class,
  DObCollectionEditabilityBeans.class,
  CollectionFunctionsBeans.class,
  DObSalesOrderCommandBeans.class,
  DObSalesOrderValidationBeans.class,
  DObSalesReturnOrderCommandBeans.class,
  DObSalesReturnOrderValidationBeans.class,
  DObStockTransformationCommandBeans.class,
  DObStockTransformationValidationBeans.class,
  DObSalesOrderEditabilityManagerBeans.class,
  DObSalesReturnOrderEditabilityManagerBeans.class,
  ActualCostCommandBeans.class,
  ActualCostValidationBeans.class,
  DObLandedCostCommandBeans.class,
  DObLandedCostEditabilityBeans.class,
  DObLandedCostValidationBeans.class,
  DObSettlementCommandBeans.class,
  DObSettlementValidationBeans.class,
  CObMeasureValidationBeans.class,
  MObCustomerValidationBeans.class,
  CObStockAvailabilityCommandBeans.class,
  DObNotesReceivablesEditabilityManagerBeans.class,
  CostingCommandBeans.class,
  NotesReceivableFunctionsBeans.class,
  SettlementFunctionBeans.class,
  DObPaymentCreationValidatorFactory.class,
  IObPaymentSaveDetailsValidatorFactory.class, 
  LoggerAspect.class
})
@EnableJpaRepositories(
    repositoryFactoryBeanClass = RepositoryFactoryBean.class,
    repositoryBaseClass = BusinessObjectRepository.class,
    basePackages = {
      "com.ebs.dac.dbo.jpa.repositories.usercodes",
      "com.ebs.dac.security.jpa.repositories",
      "com.ebs.dda.masterdata.dbo.jpa.repositories.*",
      "com.ebs.dda.purchases.dbo.jpa.repositories.*",
      "com.ebs.dda.approval.dbo.jpa.repositories.*",
      "com.ebs.dda.shipping.dbo.jpa.repositories.businessobjects.lookups",
      "com.ebs.dda.shipping.dbo.jpa.repositories.minorobjects",
      "com.ebs.dda.accounting.dbo.jpa.repositories.*",
      "com.ebs.dda.dbo.jpa.entities.lookups.repositories",
      "com.ebs.dda.sales.dbo.jpa",
      "com.ebs.dda.repositories.inventory",
      "com.ebs.dda.repositories.accounting",
      "com.ebs.dda.repositories.order",
      "com.ebs.dda.repositories",
      "com.ebs.dda.accounting.collection",
    })
@ComponentScan({
  "com.ebs.dda.rest",
  "com.ebs.dda.purchases.statemachines",
  "com.ebs.dac.restexceptionhandlers",
  "com.ebs.dac.requestbodyadvice",
  "com.ebs.dda.rx",
  "com.ebs.dda.commands.order.*",
  "com.ebs.dda.commands.accounting.*",
  "com.ebs.dda.commands.inventory.*"
})
public class CommandApplicationLauncher {

  public static void main(String[] args) {
    SpringApplication.run(CommandApplicationLauncher.class, args);
  }
}
