package com.ebs.dda.config.accounting.actualcost;

import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptPurchaseOrderDataGeneralModelRep;
import com.ebs.dda.validation.actualcost.ActualCostCalculateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ActualCostValidationBeans {

  @Bean
  public ActualCostCalculateValidator actualCostCalculateValidator( DObGoodsReceiptGeneralModelRep goodsReceiptGeneralModelRep,
           IObGoodsReceiptPurchaseOrderDataGeneralModelRep goodsReceiptPurchaseOrderDataGeneralModelRep,
           DObVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep,
           DObPurchaseOrderRep orderRep) {
    ActualCostCalculateValidator actualCostCalculateValidator =
            new ActualCostCalculateValidator();
    actualCostCalculateValidator.setGoodsReceiptPurchaseOrderDataGeneralModelRep(goodsReceiptPurchaseOrderDataGeneralModelRep);
    actualCostCalculateValidator.setOrderRep(orderRep);
    actualCostCalculateValidator.setGoodsReceiptGeneralModelRep(goodsReceiptGeneralModelRep);
    actualCostCalculateValidator.setVendorInvoiceGeneralModelRep(vendorInvoiceGeneralModelRep);

    return actualCostCalculateValidator;
  }

}
