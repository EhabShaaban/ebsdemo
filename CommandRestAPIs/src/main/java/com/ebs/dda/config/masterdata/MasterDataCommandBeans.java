package com.ebs.dda.config.masterdata;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.MasterDataCodeGenrator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.masterdata.customer.MObCustomerCreateCommand;
import com.ebs.dda.commands.masterdata.customer.MObCustomerDeleteCommand;
import com.ebs.dda.commands.masterdata.item.*;
import com.ebs.dda.commands.masterdata.itemGroup.ItemGroupCreateCommand;
import com.ebs.dda.commands.masterdata.itemGroup.ItemGroupDeleteCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecordCreateCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecordDeleteCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecordSaveCommand;
import com.ebs.dda.commands.masterdata.itemvendorrecord.ItemVendorRecoredGeneralDataEditCommand;
import com.ebs.dda.jpa.masterdata.item.MObItem;
import com.ebs.dda.jpa.masterdata.vendor.ItemVendorRecord;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.*;
import com.ebs.dda.repositories.masterdata.ivr.ItemVendorRecordRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIssueFromRep;
import com.ebs.dda.repositories.masterdata.lookups.LObTaxAdministrativeRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.measure.IObAlternativeUoMGeneralModelRep;
import com.ebs.dda.repositories.masterdata.productmanager.CObProductManagerRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import org.apache.commons.scxml2.model.ModelException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class MasterDataCommandBeans {

  @Bean
  public MObCustomerCreateCommand createMObCustomerCreateCommand(
      MasterDataCodeGenrator masterDataCodeGenrator,
      CObCurrencyRep currencyRep,
      MObCustomerRep customerRep,
      CObPurchasingUnitRep purchasingUnitRep,
      IUserAccountSecurityService userAccountSecurityService,
      LObIssueFromRep issueFromRep,
      LObTaxAdministrativeRep taxAdministrativeRep)
      throws ModelException {
    MObCustomerCreateCommand command = new MObCustomerCreateCommand();
    command.setPurchasingUnitRep(purchasingUnitRep);
    command.setCurrencyRep(currencyRep);
    command.setCustomerRep(customerRep);
    command.setMasterDataCodeGenerator(masterDataCodeGenrator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setIssueFromRep(issueFromRep);
    command.setTaxAdministrativeRep(taxAdministrativeRep);
    return command;
  }

  @Bean
  public MObCustomerDeleteCommand createCustomerDeleteCommand(MObCustomerRep customerRep) {
    MObCustomerDeleteCommand customerDeleteCommand = new MObCustomerDeleteCommand();
    customerDeleteCommand.setCustomerRep(customerRep);
    return customerDeleteCommand;
  }

  @Bean
  public MObItemCreateCommand createItemCreateCommand(
      MasterDataCodeGenrator masterDataCodeGenrator,
      IUserAccountSecurityService userAccountSecurityService,
      CObMeasureRep measureRep,
      CObMaterialGroupRep materialGroupRep,
      MObItemRep itemRep,
      CObItemRep cObItemRep,
      CObPurchasingUnitRep purchasingUnitRep) {
    MObItemCreateCommand command = new MObItemCreateCommand(masterDataCodeGenrator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setMeasureRep(measureRep);
    command.setMaterialGroupRep(materialGroupRep);
    command.setItemRep(itemRep);
    command.setPurchasingUnitRep(purchasingUnitRep);
    command.setcObItemRep(cObItemRep);
    return command;
  }

  @Bean
  public MObItemDeleteCommand createItemDeleteCommand(
      MObItemRep mObItemRep, @Qualifier("mObItemLockCommand") EntityLockCommand entityLockCommand) {
    MObItemDeleteCommand mObItemDeleteCommand = new MObItemDeleteCommand();
    mObItemDeleteCommand.setmObItemRep(mObItemRep);
    mObItemDeleteCommand.setEntityLockCommand(entityLockCommand);
    return mObItemDeleteCommand;
  }

  @Bean
  public MObItemUOMEditCommand createMObItemUOMEditCommand(
      @Qualifier("mObItemLockCommand") EntityLockCommand entityLockCommand,
      CObMeasureRep measureRep,
      MObItemRep itemRep,
      IObItemAlternativeUOMRep itemUOMRep,
      IUserAccountSecurityService userAccountSecurityService) {
    MObItemUOMEditCommand uomEditCommand = new MObItemUOMEditCommand();
    uomEditCommand.setEntityLockCommand(entityLockCommand);
    uomEditCommand.setCObMeasureRep(measureRep);
    uomEditCommand.setMObItemRep(itemRep);
    uomEditCommand.setItemAlternativeUOMRep(itemUOMRep);
    uomEditCommand.setUserAccountSecurityService(userAccountSecurityService);
    return uomEditCommand;
  }

  @Bean("mObItemLockCommand")
  public EntityLockCommand<MObItem> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      MObItemRep mObItemRep) {
    EntityLockCommand<MObItem> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(MObItem.class.getName()));
    entityLockCommand.setRepository(mObItemRep);
    return entityLockCommand;
  }

  @Bean("ItemLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("mObItemLockCommand") EntityLockCommand entityLockCommand) throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name = new ObjectName("com.ebs.dda.commands.general:name=ItemEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public ItemVendorRecordCreateCommand createItemVendorRecordCreateCommand(
      MasterDataCodeGenrator masterDataCodeGenrator,
      IUserAccountSecurityService userAccountSecurityService,
      MObItemRep itemRep,
      MObVendorRep vendorRep,
      CObCurrencyRep currencyRep,
      ItemVendorRecordRep itemVendorRecordRep,
      CObPurchasingUnitRep purchaseUnitRep,
      CObMeasureRep measureRep,
      IObAlternativeUoMGeneralModelRep alternativeUoMRep) {
    ItemVendorRecordCreateCommand itemVendorRecordCreateCommand =
        new ItemVendorRecordCreateCommand(masterDataCodeGenrator);
    itemVendorRecordCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    itemVendorRecordCreateCommand.setItemRep(itemRep);
    itemVendorRecordCreateCommand.setVendorRep(vendorRep);
    itemVendorRecordCreateCommand.setCurrencyRep(currencyRep);
    itemVendorRecordCreateCommand.setItemVendorRecordRep(itemVendorRecordRep);
    itemVendorRecordCreateCommand.setPurchaseUnitRep(purchaseUnitRep);
    itemVendorRecordCreateCommand.setMeasureRep(measureRep);
    itemVendorRecordCreateCommand.setAlternativeUoMRep(alternativeUoMRep);
    return itemVendorRecordCreateCommand;
  }

  @Bean
  public ItemVendorRecordSaveCommand createItemVendorRecordSaveCommand(
      CObCurrencyRep currencyRep,
      ItemVendorRecordRep itemVendorRecordRep,
      IUserAccountSecurityService userAccountSecurityService) {
    ItemVendorRecordSaveCommand itemVendorRecordSaveCommand = new ItemVendorRecordSaveCommand();
    itemVendorRecordSaveCommand.setCurrencyRep(currencyRep);
    itemVendorRecordSaveCommand.setItemVendorRecordRep(itemVendorRecordRep);
    itemVendorRecordSaveCommand.setUserAccountSecurityService(userAccountSecurityService);
    return itemVendorRecordSaveCommand;
  }

  @Bean
  public MObItemGeneralDataEditCommand createItemGeneralDataEditCommand(
      @Qualifier("mObItemLockCommand") EntityLockCommand entityLockCommand,
      CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep,
      MObItemRep itemRep,
      IObItemPurchaseUnitRep itemPurchaseUnitRep,
      CObProductManagerRep productManagerRep,
      IUserAccountSecurityService userAccountSecurityService) {

    MObItemGeneralDataEditCommand generalDataEditCommand = new MObItemGeneralDataEditCommand();
    generalDataEditCommand.setEntityLockCommand(entityLockCommand);
    generalDataEditCommand.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    generalDataEditCommand.setItemRep(itemRep);
    generalDataEditCommand.setProductManagerRep(productManagerRep);
    generalDataEditCommand.setItemPurchaseUnitRep(itemPurchaseUnitRep);
    generalDataEditCommand.setUserAccountSecurityService(userAccountSecurityService);
    return generalDataEditCommand;
  }

  @Bean
  public ItemVendorRecordDeleteCommand deleteItemVendorRecordCommand(
      ItemVendorRecordRep itemVendorRecordRep,
      @Qualifier("ItemVendorRecoredLockCommand") EntityLockCommand entityLockCommand) {
    ItemVendorRecordDeleteCommand itemVendorRecordDeleteCommand =
        new ItemVendorRecordDeleteCommand();
    itemVendorRecordDeleteCommand.setItemVendorRecordRep(itemVendorRecordRep);
    itemVendorRecordDeleteCommand.setEntityLockCommand(entityLockCommand);
    return itemVendorRecordDeleteCommand;
  }

  @Bean("ItemVendorRecoredLockCommand")
  public EntityLockCommand<ItemVendorRecord> createItemVendorRecoredLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      ItemVendorRecordRep itemVendorRecordRep) {
    EntityLockCommand<ItemVendorRecord> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            ItemVendorRecord.class.getName()));
    entityLockCommand.setRepository(itemVendorRecordRep);
    return entityLockCommand;
  }

  @Bean("ItemVendorRecoredLockCommandJMXBean")
  public ObjectInstance createItemVendorRecoredLockCommandJMXBean(
      @Qualifier("ItemVendorRecoredLockCommand") EntityLockCommand entityLockCommand)
      throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=ItemVendorRecoredLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public ItemVendorRecoredGeneralDataEditCommand createItemVendorRecoredGeneralDataEditCommand(
      @Qualifier("ItemVendorRecoredLockCommand") EntityLockCommand entityLockCommand) {
    ItemVendorRecoredGeneralDataEditCommand generalDataEditCommand =
        new ItemVendorRecoredGeneralDataEditCommand();
    generalDataEditCommand.setEntityLockCommand(entityLockCommand);
    return generalDataEditCommand;
  }

  @Bean
  public ItemGroupCreateCommand createItemGroupCreateCommand(
      IUserAccountSecurityService userAccountSecurityService,
      CObMaterialGroupRep itemGroupRep,
      CObMaterialGroupGeneralModelRep itemGroupGeneralModelRep) {
    ItemGroupCreateCommand command = new ItemGroupCreateCommand();
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setItemRep(itemGroupRep);
    command.setItemGroupGeneralModelRep(itemGroupGeneralModelRep);
    return command;
  }

  @Bean
  public ItemGroupDeleteCommand itemGroupDeleteCommand(
      CObMaterialGroupRep cObMaterialGroupRep,
      CObMaterialGroupGeneralModelRep itemGroupGeneralModelRep) {
    ItemGroupDeleteCommand command = new ItemGroupDeleteCommand();
    command.setcObMaterialGroupGeneralModelRep(itemGroupGeneralModelRep);
    command.setcObMaterialGroupRep(cObMaterialGroupRep);
    return command;
  }

  @Bean
  public MObItemAccountingDetailsEditCommand createItemAccountingDetailsEditCommand(
      @Qualifier("mObItemLockCommand") EntityLockCommand entityLockCommand,
      MObItemRep itemRep,
      IObItemAccountingInfoRep itemAccountingInfoRep,
      HObChartOfAccountsGeneralModelRep accountsGeneralModelRep,
      IUserAccountSecurityService accountSecurityService) {
    MObItemAccountingDetailsEditCommand accountingDetailsEditCommand =
        new MObItemAccountingDetailsEditCommand();
    accountingDetailsEditCommand.setEntityLockCommand(entityLockCommand);
    accountingDetailsEditCommand.setItemRep(itemRep);
    accountingDetailsEditCommand.setAccountingInfoRep(itemAccountingInfoRep);
    accountingDetailsEditCommand.setAccountsGeneralModelRep(accountsGeneralModelRep);
    accountingDetailsEditCommand.setUserAccountSecurityService(accountSecurityService);
    return accountingDetailsEditCommand;
  }
}
