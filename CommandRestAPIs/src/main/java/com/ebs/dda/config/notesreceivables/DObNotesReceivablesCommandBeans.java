package com.ebs.dda.config.notesreceivables;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.notesreceivables.*;
import com.ebs.dda.commands.accounting.notesreceivables.factory.IObNotesReceivablesReferenceDocumentFactory;
import com.ebs.dda.commands.accounting.notesreceivables.factory.NotesReceivablesCreditAccountDeterminationFactory;
import com.ebs.dda.commands.accounting.notesreceivables.factory.NotesReceivablesReferenceDocumentUpdateRemainingFactory;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.DObNotesReceivableJournalEntryRep;
import com.ebs.dda.repositories.accounting.monetarynotes.*;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceBusinessPartnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceSummaryRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.exchangerate.CObExchangeRateGeneralModelRep;
import com.ebs.dda.repositories.masterdata.lookups.LObDepotRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class DObNotesReceivablesCommandBeans {

  @Bean
  public DObNotesReceivablesCreateCommand createCommandNotesReceivables(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      CObPurchasingUnitRep purchasingUnitRep,
      DObMonetaryNotesRep monetaryNotesRep,
      MObCustomerRep customerRep,
      CObCurrencyRep currencyRep,
      CObCompanyRep companyRep)
      throws Exception {
    DObNotesReceivablesCreateCommand notesReceivablesCreateCommand =
        new DObNotesReceivablesCreateCommand(documentObjectCodeGenerator);
    notesReceivablesCreateCommand.setPurchasingUnitRep(purchasingUnitRep);
    notesReceivablesCreateCommand.setMonetaryNotesRep(monetaryNotesRep);
    notesReceivablesCreateCommand.setMObCustomerRep(customerRep);
    notesReceivablesCreateCommand.setCurrencyRep(currencyRep);
    notesReceivablesCreateCommand.setCompanyRep(companyRep);
    notesReceivablesCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return notesReceivablesCreateCommand;
  }

  @Bean("NotesReceivableLockCommand")
  public EntityLockCommand<DObMonetaryNotes> createItemVendorRecordLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObMonetaryNotesRep notesReceivablesRep) {
    EntityLockCommand<DObMonetaryNotes> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObMonetaryNotes.class.getName()));
    entityLockCommand.setRepository(notesReceivablesRep);
    return entityLockCommand;
  }

  @Bean("NotesReceivableEntityLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("NotesReceivableLockCommand") EntityLockCommand entityLockCommand)
      throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=NotesReceivableEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public DObNotesReceivableActivateCommand notesReceivablesActivateCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObMonetaryNotesRep notesReceivablesRep,
      CObExchangeRateGeneralModelRep exchangeRateGMRep,
      IObMonetaryNotesDetailsGeneralModelRep nrDetailsGMRep,
      CObFiscalYearRep fiscalYearRep) {
    DObNotesReceivableActivateCommand command = new DObNotesReceivableActivateCommand();

    command.setDObNotesReceivablesRep(notesReceivablesRep);
    command.setExchangeRateGMRep(exchangeRateGMRep);
    command.setNrDetailsGMRep(nrDetailsGMRep);
    command.setFiscalYearRep(fiscalYearRep);
    command.setUserAccountSecurityService(userAccountSecurityService);

    return command;
  }

  @Bean
  public IObNoteReceivablesDetailsUpdateCommand createNoteReceivablesDetailsUpdateCommand(
      DObMonetaryNotesRep monetaryNotesRep,
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
      IObMonetaryNotesDetailsRep monetaryNotesDetailsRep,
      LObDepotRep depotRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObNoteReceivablesDetailsUpdateCommand noteReceivablesDetailsUpdateCommand =
        new IObNoteReceivablesDetailsUpdateCommand();
    noteReceivablesDetailsUpdateCommand.setMonetaryNotesRep(monetaryNotesRep);
    noteReceivablesDetailsUpdateCommand.setMonetaryNotesDetailsRep(monetaryNotesDetailsRep);
    noteReceivablesDetailsUpdateCommand.setNotesReceivablesGeneralModelRep(
        notesReceivablesGeneralModelRep);
    noteReceivablesDetailsUpdateCommand.setDepotRep(depotRep);
    noteReceivablesDetailsUpdateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return noteReceivablesDetailsUpdateCommand;
  }

  @Bean
  public DObNotesReceivableRefDocumentLockCommandFactory
      createNotesReceivableRefDocumentLockCommandFactory(
          EntityLockCommand<DObMonetaryNotes> notesReceivableLockCommand,
          EntityLockCommand<DObSalesInvoice> salesInvoiceRefDocumentLockCommand,
          EntityLockCommand<DObSalesOrder> salesOrderRefDocumentLockCommand) {

    DObNotesReceivableRefDocumentLockCommandFactory factory =
        new DObNotesReceivableRefDocumentLockCommandFactory(
            notesReceivableLockCommand,
            salesInvoiceRefDocumentLockCommand,
            salesOrderRefDocumentLockCommand);

    return factory;
  }

  @Bean
  public IObNotesReceivablesReferenceDocumentFactory createNotesReceivablesReferenceDocumentFactory(
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
      DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {
    IObNotesReceivablesReferenceDocumentFactory referenceDocumentFactory =
        new IObNotesReceivablesReferenceDocumentFactory();
    referenceDocumentFactory.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    referenceDocumentFactory.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    referenceDocumentFactory.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    return referenceDocumentFactory;
  }

  @Bean
  public IObNoteReceivablesReferenceDocumentSaveCommand
      createNoteReceivablesReferenceDocumentSaveCommand(
          DObMonetaryNotesRep monetaryNotesRep,
          IUserAccountSecurityService userAccountSecurityService,
          IObNotesReceivablesReferenceDocumentFactory factory) {
    IObNoteReceivablesReferenceDocumentSaveCommand noteReceivablesReferenceDocumentSaveCommand =
        new IObNoteReceivablesReferenceDocumentSaveCommand();
    noteReceivablesReferenceDocumentSaveCommand.setMonetaryNotesRep(monetaryNotesRep);
    noteReceivablesReferenceDocumentSaveCommand.setReferenceDocumentFactory(factory);
    noteReceivablesReferenceDocumentSaveCommand.setUserAccountSecurityService(
        userAccountSecurityService);
    return noteReceivablesReferenceDocumentSaveCommand;
  }

  @Bean
  public DObNotesReceivableAccountDeterminationCommand
      createNotesReceivableAccountDeterminationCommand(
          IUserAccountSecurityService userAccountSecurityService,
          LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
          DObMonetaryNotesRep notesReceivableRep,
          IObMonetaryNotesDetailsGeneralModelRep notesDetailsGeneralModelRep,
          IObNotesReceivablesReferenceDocumentGeneralModelRep referenceDocumentGeneralModelRep,
          NotesReceivablesCreditAccountDeterminationFactory
              notesReceivablesCreditAccountDeterminationFactory) {

    DObNotesReceivableAccountDeterminationCommand command =
        new DObNotesReceivableAccountDeterminationCommand();

    command.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
    command.setMonetaryNotesRep(notesReceivableRep);
    command.setNotesDetailsGeneralModelRep(notesDetailsGeneralModelRep);
    command.setReferenceDocumentGeneralModelRep(referenceDocumentGeneralModelRep);
    command.setNotesReceivablesCreditAccountDeterminationFactory(
        notesReceivablesCreditAccountDeterminationFactory);
    command.setUserAccountSecurityService(userAccountSecurityService);
    return command;
  }

  @Bean
  public NotesReceivableRefDocumentUpdateNRRemainingHandler
      createNotesReceivableRefDocumentUpdateNRRemaining(
          DObMonetaryNotesRep notesReceivablesRep,
          IObMonetaryNotesDetailsRep notesDetailsRep,
          IUserAccountSecurityService userAccountSecurityService,
          CObExchangeRateGeneralModelRep exchangeRateRep,
          DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {

    NotesReceivableRefDocumentUpdateNRRemainingHandler notesReceivableRefDocumentUpdateNRRemaining =
        new NotesReceivableRefDocumentUpdateNRRemainingHandler();

    notesReceivableRefDocumentUpdateNRRemaining.setDObMonetaryNotesRep(notesReceivablesRep);
    notesReceivableRefDocumentUpdateNRRemaining.setIObMonetaryNotesDetailsRep(notesDetailsRep);
    notesReceivableRefDocumentUpdateNRRemaining.setUserAccountSecurityService(
        userAccountSecurityService);
    notesReceivableRefDocumentUpdateNRRemaining.setExchangeRateRep(exchangeRateRep);
    notesReceivableRefDocumentUpdateNRRemaining.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    return notesReceivableRefDocumentUpdateNRRemaining;
  }

  @Bean
  public NotesReceivablesReferenceDocumentUpdateRemainingFactory
      createNotesReceivablesReferenceDocumentUpdateRemainingFactory(
          NotesReceivableRefDocumentUpdateNRRemainingHandler
              notesReceivableRefDocumentUpdateNRRemaining,
          NotesReceivableRefDocumentUpdateSORemainingHandler
              notesReceivableRefDocumentUpdateSORemainingHandler,
          NotesReceivableRefDocumentUpdateSIRemainingHandler
              notesReceivableRefDocumentUpdateSIRemainingHandler) {

    NotesReceivablesReferenceDocumentUpdateRemainingFactory
        notesReceivablesReferenceDocumentUpdateRemainingFactory =
            new NotesReceivablesReferenceDocumentUpdateRemainingFactory();

    notesReceivablesReferenceDocumentUpdateRemainingFactory
        .setNotesReceivableRefDocumentUpdateNRRemainingHandler(
            notesReceivableRefDocumentUpdateNRRemaining);

    notesReceivablesReferenceDocumentUpdateRemainingFactory
        .setNotesReceivableRefDocumentUpdateSORemainingHandler(
            notesReceivableRefDocumentUpdateSORemainingHandler);
    notesReceivablesReferenceDocumentUpdateRemainingFactory
        .setNotesReceivableRefDocumentUpdateSIRemainingHandler(
            notesReceivableRefDocumentUpdateSIRemainingHandler);

    return notesReceivablesReferenceDocumentUpdateRemainingFactory;
  }

  @Bean
  public IObNotesReceivableRefDocumentUpdateRemainingCommand
      createIObNotesReceivableRefDocumentUpdateRemainingCommand(
          NotesReceivablesReferenceDocumentUpdateRemainingFactory
              notesReceivablesReferenceDocumentUpdateRemainingFactory,
          IObNotesReceivablesReferenceDocumentGeneralModelRep
              notesReceivablesReferenceDocumentGeneralModelRep) {

    IObNotesReceivableRefDocumentUpdateRemainingCommand
        notesReceivableRefDocumentUpdateRemainingCommand =
            new IObNotesReceivableRefDocumentUpdateRemainingCommand();

    notesReceivableRefDocumentUpdateRemainingCommand
        .setNotesReceivablesReferenceDocumentUpdateRemainingFactory(
            notesReceivablesReferenceDocumentUpdateRemainingFactory);
    notesReceivableRefDocumentUpdateRemainingCommand
        .setNotesReceivablesReferenceDocumentGeneralModelRep(
            notesReceivablesReferenceDocumentGeneralModelRep);

    return notesReceivableRefDocumentUpdateRemainingCommand;
  }

  @Bean
  public NotesReceivableCreditNRAccountDeterminationHandler
      createNotesReceivableCreditNRAccountDeterminationHandler(
          DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
          LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
          IUserAccountSecurityService userAccountSecurityService) {

    NotesReceivableCreditNRAccountDeterminationHandler
        notesReceivableCreditNRAccountDeterminationHandler =
            new NotesReceivableCreditNRAccountDeterminationHandler();

    notesReceivableCreditNRAccountDeterminationHandler.setNotesReceivablesGeneralModelRep(
        notesReceivablesGeneralModelRep);
    notesReceivableCreditNRAccountDeterminationHandler.setGlobalAccountConfigGeneralModelRep(
        globalAccountConfigGeneralModelRep);
    notesReceivableCreditNRAccountDeterminationHandler.setUserAccountSecurityService(
        userAccountSecurityService);
    return notesReceivableCreditNRAccountDeterminationHandler;
  }

  @Bean
  public NotesReceivableCreditSIAndSOAccountDeterminationHandler
      createNotesReceivableCreditSIAndSOAccountDeterminationHandler(
          MObCustomerGeneralModelRep customerGeneralModelRep,
          LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
          IUserAccountSecurityService userAccountSecurityService) {

    NotesReceivableCreditSIAndSOAccountDeterminationHandler
        notesReceivableCreditNRAccountDeterminationHandler =
            new NotesReceivableCreditSIAndSOAccountDeterminationHandler();

    notesReceivableCreditNRAccountDeterminationHandler.setCustomerGeneralModelRep(
        customerGeneralModelRep);
    notesReceivableCreditNRAccountDeterminationHandler.setGlobalAccountConfigGeneralModelRep(
        globalAccountConfigGeneralModelRep);
    notesReceivableCreditNRAccountDeterminationHandler.setUserAccountSecurityService(
        userAccountSecurityService);
    return notesReceivableCreditNRAccountDeterminationHandler;
  }

  @Bean
  public NotesReceivablesCreditAccountDeterminationFactory
      createNotesReceivablesCreditAccountDeterminationFactory(
          NotesReceivableCreditNRAccountDeterminationHandler
              notesReceivableCreditNRAccountDeterminationHandler,
          NotesReceivableCreditSIAndSOAccountDeterminationHandler
              notesReceivableCreditSIAndSOAccountDeterminationHandler) {

    NotesReceivablesCreditAccountDeterminationFactory
        notesReceivablesCreditAccountDeterminationFactory =
            new NotesReceivablesCreditAccountDeterminationFactory();

    notesReceivablesCreditAccountDeterminationFactory
        .setNotesReceivableCreditNRAccountDeterminationHandler(
            notesReceivableCreditNRAccountDeterminationHandler);

    notesReceivablesCreditAccountDeterminationFactory
        .setNotesReceivableCreditSIAndSOAccountDeterminationHandler(
            notesReceivableCreditSIAndSOAccountDeterminationHandler);
    return notesReceivablesCreditAccountDeterminationFactory;
  }

  @Bean
  public NotesReceivableRefDocumentUpdateSORemainingHandler
      createNotesReceivableRefDocumentUpdateSORemainingHandler(
          DObSalesOrderRep salesOrderRep, IUserAccountSecurityService userAccountSecurityService,
          CObExchangeRateGeneralModelRep exchangeRateRep,
          IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep) {

    NotesReceivableRefDocumentUpdateSORemainingHandler
        notesReceivableRefDocumentUpdateSORemainingHandler =
            new NotesReceivableRefDocumentUpdateSORemainingHandler();

    notesReceivableRefDocumentUpdateSORemainingHandler.setDObSalesOrderRep(salesOrderRep);
    notesReceivableRefDocumentUpdateSORemainingHandler.setUserAccountSecurityService(
        userAccountSecurityService);
    notesReceivableRefDocumentUpdateSORemainingHandler.setExchangeRateRep(exchangeRateRep);
    notesReceivableRefDocumentUpdateSORemainingHandler.setSalesOrderDataGeneralModelRep(salesOrderDataGeneralModelRep);
    return notesReceivableRefDocumentUpdateSORemainingHandler;
  }

  @Bean
  public NotesReceivableRefDocumentUpdateSIRemainingHandler
      createNotesReceivableRefDocumentUpdateSIRemainingHandler(
          DObSalesInvoiceRep salesInvoiceRep,
          IObSalesInvoiceSummaryRep salesInvoiceSummaryRep,
          IUserAccountSecurityService userAccountSecurityService,
          CObExchangeRateGeneralModelRep exchangeRateRep,
          IObSalesInvoiceBusinessPartnerGeneralModelRep salesInvoiceBusinessPartnerRep) {

    NotesReceivableRefDocumentUpdateSIRemainingHandler
        notesReceivableRefDocumentUpdateSIRemainingHandler =
            new NotesReceivableRefDocumentUpdateSIRemainingHandler();

    notesReceivableRefDocumentUpdateSIRemainingHandler.setIObSalesInvoiceSummaryRep(
        salesInvoiceSummaryRep);
    notesReceivableRefDocumentUpdateSIRemainingHandler.setDObSalesInvoiceRep(salesInvoiceRep);

    notesReceivableRefDocumentUpdateSIRemainingHandler.setUserAccountSecurityService(
        userAccountSecurityService);
    notesReceivableRefDocumentUpdateSIRemainingHandler.setExchangeRateRep(exchangeRateRep);
    notesReceivableRefDocumentUpdateSIRemainingHandler.setSalesInvoiceBusinessPartnerRep(salesInvoiceBusinessPartnerRep);
    return notesReceivableRefDocumentUpdateSIRemainingHandler;
  }

  @Bean
  public DObNotesReceivableUpdateRemainingCommand createDObNotesReceivableUpdateRemainingCommand(
      DObCollectionGeneralModelRep collectionRep,
      DObMonetaryNotesRep monetaryNotesRep,
      IObMonetaryNotesDetailsRep monetaryNotesDetailsRep,
      IUserAccountSecurityService accountService) {
    DObNotesReceivableUpdateRemainingCommand command =
        new DObNotesReceivableUpdateRemainingCommand();
    command.setCollectionGMRep(collectionRep);
    command.setMonetaryNotesRep(monetaryNotesRep);
    command.setMonetaryNotesDetailsRep(monetaryNotesDetailsRep);
    command.setUserAccountSecurityService(accountService);
    return command;
  }

  @Bean
  public DObJournalEntryCreateNotesReceivableCommand
      createDOnNotesReceivableCreateJournalEntryCommand(
          DocumentObjectCodeGenerator codeGenerator,
          IUserAccountSecurityService userAccountSecurityService,
          DObNotesReceivableJournalEntryRep notesReceivableJournalEntryRep,
          DObNotesReceivableJournalEntryPreparationGeneralModelRep journalEntryPreparationGMRep,
          IObNotesReceivableAccountingDetailsGeneralModelRep accountingDetailsGMRep) {

    DObJournalEntryCreateNotesReceivableCommand command =
        new DObJournalEntryCreateNotesReceivableCommand();

    command.setCodeGenerator(codeGenerator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setNotesReceivableJournalEntryRep(notesReceivableJournalEntryRep);
    command.setJournalEntryPreparationGMRep(journalEntryPreparationGMRep);
    command.setAccountingDetailsGMRep(accountingDetailsGMRep);

    return command;
  }

  @Bean
  public IObNotesReceivableReferenceDocumentDeleteCommand
      createNotesReceivableReferenceDocumentDeleteCommand(
          IObNotesReceivableReferenceDocumentSalesInvoiceRep
              notesReceivableReferenceDocumentSalesInvoiceRep,
          IObNotesReceivableReferenceDocumentNotesReceivableRep
              notesReceivableReferenceDocumentNotesReceivableRep,
          IObNotesReceivableReferenceDocumentSalesOrderRep
              notesReceivableReferenceDocumentSalesOrderRep,
          DObMonetaryNotesRep monetaryNotesRep,
          IUserAccountSecurityService userAccountSecurityService) {
    IObNotesReceivableReferenceDocumentDeleteCommand notesReceivableReferenceDocumentDeleteCommand =
        new IObNotesReceivableReferenceDocumentDeleteCommand();
    notesReceivableReferenceDocumentDeleteCommand
        .setNotesReceivableReferenceDocumentNotesReceivableRep(
            notesReceivableReferenceDocumentNotesReceivableRep);
    notesReceivableReferenceDocumentDeleteCommand.setNotesReceivableReferenceDocumentSalesOrderRep(
        notesReceivableReferenceDocumentSalesOrderRep);
    notesReceivableReferenceDocumentDeleteCommand
        .setNotesReceivableReferenceDocumentSalesInvoiceRep(
            notesReceivableReferenceDocumentSalesInvoiceRep);
    notesReceivableReferenceDocumentDeleteCommand.setMonetaryNotesRep(monetaryNotesRep);
    notesReceivableReferenceDocumentDeleteCommand.setUserAccountSecurityService(userAccountSecurityService);
    return notesReceivableReferenceDocumentDeleteCommand;
  }
}
