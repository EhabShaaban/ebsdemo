package com.ebs.dda.config.accounting.journalentry;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.accounting.journalentry.create.paymentrequest.*;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetails;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentActivationDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.journalentry.DObPaymentRequestJournalEntryRep;
import com.ebs.dda.repositories.accounting.paymentrequest.*;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObLeafInChartOfAccountsRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObPaymentJournalEntryCommandBeans {

    @Bean
    public DObJournalEntryCreatePaymentRequestBankGeneralCommand
    createGeneralPaymentBankJournalEntryCommand(
            DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep,
            IObPaymentRequestAccountDetailsGeneralModelRep
                    paymentRequestAccountingDetailsGeneralModelRep,
            IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
                    paymentRequestPaymentDetailsRep,
            HObLeafInChartOfAccountsRep leafInChartOfAccountsRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep
                    paymentRequestPaymentDetailsGeneralModelRep,
            DObPaymentRequestRep paymentRequestRep,
            IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep,
            IObAccountingDocumentActivationDetailsGeneralModelRep
                    accountingDocumentActivationDetailsGeneralModelRep) {
        DObJournalEntryCreatePaymentRequestBankGeneralCommand command =
                new DObJournalEntryCreatePaymentRequestBankGeneralCommand();

        command.setPaymentRequestJournalEntryRep(
                paymentRequestJournalEntryRep);
        command.setPaymentRequestPostingDetailsRep(
                paymentRequestPostingDetailsRep);
        command.setUserAccountSecurityService(
                userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsRep(
                paymentRequestPaymentDetailsRep);
        command
                .setPaymentRequestAccountingDetailsGeneralModelRep(
                        paymentRequestAccountingDetailsGeneralModelRep);
        command.setLeafInChartOfAccountsRep(
                leafInChartOfAccountsRep);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(
                paymentRequestPaymentDetailsGeneralModelRep);
        command.setPaymentRequestRep(paymentRequestRep);
        command.setPaymentRequestCompanyDataRep(
                paymentRequestCompanyDataRep);
        command.setAccountingDocumentActivationDetailsGeneralModelRep(accountingDocumentActivationDetailsGeneralModelRep);
        return command;
    }

    @Bean
    public DObJournalEntryCreatePaymentRequestCashGeneralCommand
    createGeneralPaymentCashJournalEntryCommand(
            DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep,
            IObPaymentRequestAccountDetailsGeneralModelRep
                    paymentRequestAccountingDetailsGeneralModelRep,
            IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
                    paymentRequestPaymentDetailsRep,
            HObLeafInChartOfAccountsRep leafInChartOfAccountsRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep
                    paymentRequestPaymentDetailsGeneralModelRep,
            DObPaymentRequestRep paymentRequestRep,
            IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep,
            IObAccountingDocumentActivationDetailsGeneralModelRep
                    accountingDocumentActivationDetailsGeneralModelRep) {
        DObJournalEntryCreatePaymentRequestCashGeneralCommand command =
                new DObJournalEntryCreatePaymentRequestCashGeneralCommand();

        command.setPaymentRequestJournalEntryRep(
                paymentRequestJournalEntryRep);
        command.setPaymentRequestPostingDetailsRep(
                paymentRequestPostingDetailsRep);
        command.setUserAccountSecurityService(
                userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsRep(
                paymentRequestPaymentDetailsRep);
        command
                .setPaymentRequestAccountingDetailsGeneralModelRep(
                        paymentRequestAccountingDetailsGeneralModelRep);
        command.setLeafInChartOfAccountsRep(
                leafInChartOfAccountsRep);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(
                paymentRequestPaymentDetailsGeneralModelRep);
        command.setPaymentRequestRep(paymentRequestRep);
        command.setPaymentRequestCompanyDataRep(
                paymentRequestCompanyDataRep);
        command.setAccountingDocumentActivationDetailsGeneralModelRep(accountingDocumentActivationDetailsGeneralModelRep);
        return command;
    }

    @Bean
    public DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand
    createPaymentForVendorBankJournalEntryCommand(
            DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep,
            IObPaymentRequestAccountDetailsGeneralModelRep
                    paymentRequestAccountingDetailsGeneralModelRep,
            IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
                    paymentRequestPaymentDetailsRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep
                    paymentRequestPaymentDetailsGeneralModelRep,
            DObPaymentRequestRep paymentRequestRep,
            IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep,
            IObAccountingDocumentActivationDetailsGeneralModelRep
                    accountingDocumentActivationDetailsGeneralModelRep
    ) {
        DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand command =
                new DObJournalEntryCreatePaymentRequestBankVendorInvoiceCommand();

        command.setPaymentRequestJournalEntryRep(
                paymentRequestJournalEntryRep);
        command.setPaymentRequestPostingDetailsRep(
                paymentRequestPostingDetailsRep);
        command.setUserAccountSecurityService(
                userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsRep(
                paymentRequestPaymentDetailsRep);
        command
                .setPaymentRequestAccountingDetailsGeneralModelRep(
                        paymentRequestAccountingDetailsGeneralModelRep);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(
                paymentRequestPaymentDetailsGeneralModelRep);
        command.setPaymentRequestRep(paymentRequestRep);
        command.setPaymentRequestCompanyDataRep(
                paymentRequestCompanyDataRep);
        command.setAccountingDocumentActivationDetailsGeneralModelRep(accountingDocumentActivationDetailsGeneralModelRep);
        return command;
    }

    @Bean
    public DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand
    createPaymentForVendorCashJournalEntryCommand(
            DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep,
            IObPaymentRequestAccountDetailsGeneralModelRep
                    paymentRequestAccountingDetailsGeneralModelRep,
            IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
                    paymentRequestPaymentDetailsRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep
                    paymentRequestPaymentDetailsGeneralModelRep,
            DObPaymentRequestRep paymentRequestRep,
            IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep,
            IObAccountingDocumentActivationDetailsGeneralModelRep
                    accountingDocumentActivationDetailsGeneralModelRep
    ) {
        DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand command =
                new DObJournalEntryCreatePaymentRequestCashVendorInvoiceCommand();

        command.setPaymentRequestJournalEntryRep(
                paymentRequestJournalEntryRep);
        command.setPaymentRequestPostingDetailsRep(
                paymentRequestPostingDetailsRep);
        command.setUserAccountSecurityService(
                userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsRep(
                paymentRequestPaymentDetailsRep);
        command
                .setPaymentRequestAccountingDetailsGeneralModelRep(
                        paymentRequestAccountingDetailsGeneralModelRep);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(
                paymentRequestPaymentDetailsGeneralModelRep);
        command.setPaymentRequestRep(paymentRequestRep);
        command.setPaymentRequestCompanyDataRep(
                paymentRequestCompanyDataRep);
        command.setAccountingDocumentActivationDetailsGeneralModelRep(accountingDocumentActivationDetailsGeneralModelRep);
        return command;
    }

    @Bean
    public DObJournalEntryCreatePaymentRequestBankVendorDownPaymentCommand
    createPaymentForVendorDownPaymentBankJournalEntryCommand(
            DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep,
            IObPaymentRequestAccountDetailsGeneralModelRep
                    paymentRequestAccountingDetailsGeneralModelRep,
            IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
                    paymentRequestPaymentDetailsRep,
            HObLeafInChartOfAccountsRep leafInChartOfAccountsRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep
                    paymentRequestPaymentDetailsGeneralModelRep,
            DObPaymentRequestRep paymentRequestRep,
            IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep,
            IObAccountingDocumentActivationDetailsGeneralModelRep
                    accountingDocumentActivationDetailsGeneralModelRep) {
        DObJournalEntryCreatePaymentRequestBankVendorDownPaymentCommand command =
                new DObJournalEntryCreatePaymentRequestBankVendorDownPaymentCommand();

        command.setPaymentRequestJournalEntryRep(
                paymentRequestJournalEntryRep);
        command.setPaymentRequestPostingDetailsRep(
                paymentRequestPostingDetailsRep);
        command.setUserAccountSecurityService(
                userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsRep(
                paymentRequestPaymentDetailsRep);
        command
                .setPaymentRequestAccountingDetailsGeneralModelRep(
                        paymentRequestAccountingDetailsGeneralModelRep);
        command.setLeafInChartOfAccountsRep(
                leafInChartOfAccountsRep);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(
                paymentRequestPaymentDetailsGeneralModelRep);
        command.setPaymentRequestRep(paymentRequestRep);
        command.setPaymentRequestCompanyDataRep(
                paymentRequestCompanyDataRep);
        command.setAccountingDocumentActivationDetailsGeneralModelRep(accountingDocumentActivationDetailsGeneralModelRep);
        return command;
    }

    @Bean
    public DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand
    createPaymentForVendorDownPaymentCashJournalEntryCommand(
            DObPaymentRequestJournalEntryRep paymentRequestJournalEntryRep,
            IObPaymentRequestAccountDetailsGeneralModelRep
                    paymentRequestAccountingDetailsGeneralModelRep,
            IObPaymentRequestPostingDetailsRep paymentRequestPostingDetailsRep,
            IUserAccountSecurityService userAccountSecurityService,
            IObPaymentRequestPaymentDetailsRep<IObPaymentRequestPaymentDetails>
                    paymentRequestPaymentDetailsRep,
            HObLeafInChartOfAccountsRep leafInChartOfAccountsRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep
                    paymentRequestPaymentDetailsGeneralModelRep,
            DObPaymentRequestRep paymentRequestRep,
            IObPaymentRequestCompanyDataRep paymentRequestCompanyDataRep,
            IObAccountingDocumentActivationDetailsGeneralModelRep
                    accountingDocumentActivationDetailsGeneralModelRep) {
        DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand command =
                new DObJournalEntryCreatePaymentRequestCashVendorDownPaymentCommand();

        command.setPaymentRequestJournalEntryRep(
                paymentRequestJournalEntryRep);
        command.setPaymentRequestPostingDetailsRep(
                paymentRequestPostingDetailsRep);
        command.setUserAccountSecurityService(
                userAccountSecurityService);
        command.setPaymentRequestPaymentDetailsRep(
                paymentRequestPaymentDetailsRep);
        command
                .setPaymentRequestAccountingDetailsGeneralModelRep(
                        paymentRequestAccountingDetailsGeneralModelRep);
        command.setLeafInChartOfAccountsRep(
                leafInChartOfAccountsRep);
        command.setPaymentRequestPaymentDetailsGeneralModelRep(
                paymentRequestPaymentDetailsGeneralModelRep);
        command.setPaymentRequestRep(paymentRequestRep);
        command.setPaymentRequestCompanyDataRep(
                paymentRequestCompanyDataRep);
        command.setAccountingDocumentActivationDetailsGeneralModelRep(accountingDocumentActivationDetailsGeneralModelRep);
        return command;
    }
}
