package com.ebs.dda.config.notesreceivables;

import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivableCreationValidator;
import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivableDetailsMandatoryValidator;
import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivableGeneralDataMandatoryValidator;
import com.ebs.dda.accounting.notesreceivables.validation.DObNotesReceivablesValidator;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerBusinessUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.lookups.LObDepotRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.validation.notesreceivable.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObNotesReceivableValidationBeans {

  @Bean
  public DObNotesReceivablesValidator createNotesReceivablePostValidator(
      DObNotesReceivableDetailsMandatoryValidator notesReceivableDetailsMandatoryValidator,
      DObNotesReceivableGeneralDataMandatoryValidator notesReceivableGeneralDataMandatoryValidator,
      DObSalesInvoiceGeneralModelRep dObSalesInvoiceGeneralModelRep) {
    DObNotesReceivablesValidator sectionsMandatoryValidator = new DObNotesReceivablesValidator();
    sectionsMandatoryValidator.setNotesReceivableDetailsMandatoryValidator(
        notesReceivableDetailsMandatoryValidator);
    sectionsMandatoryValidator.setNotesReceivableGeneralDataMandatoryValidator(
        notesReceivableGeneralDataMandatoryValidator);
    sectionsMandatoryValidator.setdObSalesInvoiceGeneralModelRep(dObSalesInvoiceGeneralModelRep);
    return sectionsMandatoryValidator;
  }

  @Bean
  public DObNotesReceivableDetailsMandatoryValidator
      createDObNotesReceivableDetailsMandatoryValidator(
          DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    DObNotesReceivableDetailsMandatoryValidator mandatoryValidator =
        new DObNotesReceivableDetailsMandatoryValidator();
    mandatoryValidator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    return mandatoryValidator;
  }

  @Bean
  public DObNotesReceivableGeneralDataMandatoryValidator
      createDObNotesReceivableGeneralDataMandatoryValidator(
          DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {
    DObNotesReceivableGeneralDataMandatoryValidator mandatoryValidator =
        new DObNotesReceivableGeneralDataMandatoryValidator();
    mandatoryValidator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    return mandatoryValidator;
  }

  @Bean
  public DObNotesReceivableCreationValidator createDObNotesReceivableCreationValidator(
      CObPurchasingUnitGeneralModelRep businessUnitRep,
      CObCompanyGeneralModelRep companyRep,
      IObCustomerBusinessUnitGeneralModelRep businessPartnerRep,
      DocumentOwnerGeneralModelRep documentOwnerRep,
      CObCurrencyRep currencyRep) {

    DObNotesReceivableCreationValidator validator = new DObNotesReceivableCreationValidator();

    validator.setBusinessUnitRep(businessUnitRep);
    validator.setCompanyRep(companyRep);
    validator.setBusinessPartnerRep(businessPartnerRep);
    validator.setDocumentOwnerRep(documentOwnerRep);
    validator.setCurrencyRep(currencyRep);

    return validator;
  }

  @Bean
  public IObNotesReceivableRefDocumentNRValidator createIObNotesReceivableRefDocumentNRValidator(
      IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsGeneralModelRep,
      DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep) {

    IObNotesReceivableRefDocumentNRValidator validator =
        new IObNotesReceivableRefDocumentNRValidator();

    validator.setNotesDetailsGeneralModelRep(monetaryNotesDetailsGeneralModelRep);
    validator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    return validator;
  }

  @Bean
  public IObNotesReceivableRefDocumentSOValidator createIObNotesReceivableRefDocumentSOValidator(
      DObSalesOrderGeneralModelRep salesOrderGeneralModelRep) {

    IObNotesReceivableRefDocumentSOValidator validator =
        new IObNotesReceivableRefDocumentSOValidator();

    validator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    return validator;
  }

  @Bean
  public IObNotesReceivableRefDocumentSIValidator createIObNotesReceivableRefDocumentSIValidator(
      IObInvoiceSummaryGeneralModelRep salesInvoiceSummaryRep,
      DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep) {

    IObNotesReceivableRefDocumentSIValidator validator =
        new IObNotesReceivableRefDocumentSIValidator();

    validator.setSalesInvoiceSummaryRep(salesInvoiceSummaryRep);
    validator.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    return validator;
  }

  @Bean
  public IObNotesReceivableRefDocumentValidator createIObNotesReceivableRefDocumentValidator(
      IObNotesReceivableRefDocumentNRValidator refDocumentNRValidator,
      IObNotesReceivableRefDocumentSOValidator refDocumentSOValidator,
      IObNotesReceivableRefDocumentSIValidator refDocumentSIValidator) {

    IObNotesReceivableRefDocumentValidator validator = new IObNotesReceivableRefDocumentValidator();

    validator.setRefDocumentNRValidator(refDocumentNRValidator);
    validator.setRefDocumentSOValidator(refDocumentSOValidator);
    validator.setRefDocumentSIValidator(refDocumentSIValidator);
    return validator;
  }

  @Bean
  public DObNotesReceivableActivateValidator createDObNotesReceivableActivateValidator(
      CObFiscalYearRep fiscalYearRep,
      IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep,
      IObNotesReceivableRefDocumentBusinessValidator notesReceivableRefDocumentBusinessValidator) {

    DObNotesReceivableActivateValidator validator = new DObNotesReceivableActivateValidator();
    validator.setFiscalYearRep(fiscalYearRep);
    validator.setMonetaryNotesReferenceDocumentsRep(monetaryNotesReferenceDocumentsRep);
    validator.setNotesReceivableRefDocumentBusinessValidator(
        notesReceivableRefDocumentBusinessValidator);
    return validator;
  }

  @Bean
  public IObNotesReceivableRefDocumentBusinessValidator
      createIObNotesReceivableRefDocumentBusinessValidator(
          IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsGeneralModelRep,
          IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep,
          DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep,
          IObNotesReceivableRefDocumentValidator notesReceivableRefDocumentValidator) {

    IObNotesReceivableRefDocumentBusinessValidator validator =
        new IObNotesReceivableRefDocumentBusinessValidator();

    validator.setMonetaryNotesDetailsGeneralModelRep(monetaryNotesDetailsGeneralModelRep);
    validator.setMonetaryNotesReferenceDocumentsRep(monetaryNotesReferenceDocumentsRep);
    validator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    validator.setNotesReceivableRefDocumentValidator(notesReceivableRefDocumentValidator);
    return validator;
  }

  @Bean
  public DObNotesReceivableActivateMandatoriesValidator
      createDObNotesReceivableActivateMandatoriesValidator(
          IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsRep,
          IObNotesReceivablesReferenceDocumentGeneralModelRep
              notesReceivablesReferenceDocumentRep) {
    DObNotesReceivableActivateMandatoriesValidator validator =
        new DObNotesReceivableActivateMandatoriesValidator();
    validator.setMonetaryNotesDetailsRep(monetaryNotesDetailsRep);
    validator.setNotesReceivablesReferenceDocumentRep(notesReceivablesReferenceDocumentRep);
    return validator;
  }

  @Bean
  public IObNotesReceivableDetailsValidator createNotesReceivableDetailsValidator(
      LObDepotRep depotRep) {
    IObNotesReceivableDetailsValidator notesReceivableDetailsValidator =
        new IObNotesReceivableDetailsValidator();
    notesReceivableDetailsValidator.setDepotRep(depotRep);
    return notesReceivableDetailsValidator;
  }
}
