package com.ebs.dda.config.accounting.journalbalance;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceDecreaseBalanceCommand;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceIncreaseBalanceCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.journalbalance.CObBankJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.CObTreasuryJournalBalance;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CObJournalBalanceCommandBeans {

  @Bean("CObBankJournalBalanceLockCommand")
  public EntityLockCommand<CObBankJournalBalance> createBankJournalBalanceLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      CObJournalBalanceRep journalBalanceRep) {

    EntityLockCommand<CObBankJournalBalance> command = new EntityLockCommand<>();

    command.setRepository(journalBalanceRep);
    command.setUserAccountSecurityService(userAccountSecurityService);

    command.setConcurrentDataAccessManager(concurrentDataAccessManagersPool
        .getConcurrentDataAccessManager(CObBankJournalBalance.class.getName()));

    return command;
  }

  @Bean("CObTreasuryJournalBalanceLockCommand")
  public EntityLockCommand<CObTreasuryJournalBalance> createTreasuryJournalBalanceLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      CObJournalBalanceRep journalBalanceRep) {

    EntityLockCommand<CObTreasuryJournalBalance> command = new EntityLockCommand<>();

    command.setRepository(journalBalanceRep);
    command.setUserAccountSecurityService(userAccountSecurityService);

    command.setConcurrentDataAccessManager(concurrentDataAccessManagersPool
        .getConcurrentDataAccessManager(CObTreasuryJournalBalance.class.getName()));

    return command;
  }

  @Bean
  public CObJournalBalanceLockCommandBeanFactory createJournalBalanceLockCommandBeanFactory(
      EntityLockCommand<CObBankJournalBalance> bankJBLockCmd,
      EntityLockCommand<CObTreasuryJournalBalance> treasuryJBLockCmd) {

    CObJournalBalanceLockCommandBeanFactory factory =
        new CObJournalBalanceLockCommandBeanFactory(bankJBLockCmd, treasuryJBLockCmd);

    return factory;
  }

  @Bean
  public CObJournalBalanceIncreaseBalanceCommand createCObJournalBalanceIncreaseBalanceCommand(
          IUserAccountSecurityService userAccountSecurityService,
          CObJournalBalanceRep journalBalanceRep) {

    CObJournalBalanceIncreaseBalanceCommand command = new CObJournalBalanceIncreaseBalanceCommand();
    command.setJournalBalanceRep(journalBalanceRep);
    command.setUserAccountSecurityService(userAccountSecurityService);

    return command;
  }

  @Bean
  public CObJournalBalanceDecreaseBalanceCommand createCObJournalBalanceDecreaseBalanceCommand(
          IUserAccountSecurityService userAccountSecurityService,
          CObJournalBalanceRep journalBalanceRep) {

    CObJournalBalanceDecreaseBalanceCommand command = new CObJournalBalanceDecreaseBalanceCommand();
    command.setJournalBalanceRep(journalBalanceRep);
    command.setUserAccountSecurityService(userAccountSecurityService);

    return command;
  }
}
