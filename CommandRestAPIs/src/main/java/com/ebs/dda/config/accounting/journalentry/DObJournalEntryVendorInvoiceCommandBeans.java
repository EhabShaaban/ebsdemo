package com.ebs.dda.config.accounting.journalentry;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand;
import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.DObJournalEntryCreateVendorInvoiceCommand;
import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.DObVendorInvoiceJournalEntryInitializer;
import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.IObVendorInvoiceJournalEntryDetailsInitializer;
import com.ebs.dda.dbo.jpa.entities.lookups.repositories.LObGlobalAccountConfigGeneralModelRep;
import com.ebs.dda.repositories.accounting.DObAccountingJournalEntryPreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsRep;
import com.ebs.dda.repositories.accounting.journalentry.DObVendorInvoiceJournalEntryRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.DObVendorInvoiceRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
public class DObJournalEntryVendorInvoiceCommandBeans {

  @Bean
  @DependsOn({
    "DObVendorInvoiceJournalEntryInitializer",
    "IObVendorInvoiceJournalEntryDetailsInitializer"
  })
  public DObJournalEntryCreateVendorInvoiceCommand createDObJournalEntryCreateVendorInvoiceCommand(
      DObVendorInvoiceJournalEntryRep vendorInvoiceJournalEntryRep,
      IUserAccountSecurityService userAccountSecurityService,
      DObVendorInvoiceRep vendorInvoiceRep,
      DObVendorInvoiceJournalDetailPreparationGeneralModelٌRep
          journalDetailPreparationGeneralModelٌRep,
      DObVendorInvoiceJournalEntryInitializer journalEntryInitializer,
      IObVendorInvoiceJournalEntryDetailsInitializer vendorInvoiceJournalEntryDetailsInitializer) {

    DObJournalEntryCreateVendorInvoiceCommand vendorInvoiceCommand =
        new DObJournalEntryCreateVendorInvoiceCommand();

    vendorInvoiceCommand.setUserAccountSecurityService(userAccountSecurityService);
    vendorInvoiceCommand.setVendorInvoiceJournalEntryRep(vendorInvoiceJournalEntryRep);
    vendorInvoiceCommand.setJournalEntryInitializer(journalEntryInitializer);
    vendorInvoiceCommand.setVendorInvoiceRep(vendorInvoiceRep);
    vendorInvoiceCommand.setJournalDetailPreparationGeneralModelٌRep(
        journalDetailPreparationGeneralModelٌRep);
    vendorInvoiceCommand.setVendorInvoiceJournalEntryDetailsInitializer(
        vendorInvoiceJournalEntryDetailsInitializer);
    return vendorInvoiceCommand;
  }

  @Bean("DObVendorInvoiceJournalEntryInitializer")
  public DObVendorInvoiceJournalEntryInitializer createVendorInvoiceJournalEntryInitializer(
      IUserAccountSecurityService userAccountSecurityService,
      DObAccountingJournalEntryPreparationGeneralModelRep
          accountingJournalEntryPreparationGeneralModelRep) {
    DObVendorInvoiceJournalEntryInitializer initializer =
        new DObVendorInvoiceJournalEntryInitializer();
    initializer.setUserAccountSecurityService(userAccountSecurityService);
    initializer.setAccountingJournalEntryPreparationGeneralModelRep(
        accountingJournalEntryPreparationGeneralModelRep);
    return initializer;
  }

  @Bean("IObVendorInvoiceJournalEntryDetailsInitializer")
  public IObVendorInvoiceJournalEntryDetailsInitializer
      createVendorInvoiceJournalEntryDetailsInitializer(
          IUserAccountSecurityService userAccountSecurityService,
          IObAccountingDocumentAccountingDetailsRep accountingDocumentAccountingDetailsRep) {

    IObVendorInvoiceJournalEntryDetailsInitializer initializer =
        new IObVendorInvoiceJournalEntryDetailsInitializer();
    initializer.setAccountingDocumentAccountingDetailsRep(accountingDocumentAccountingDetailsRep);
    initializer.setUserAccountSecurityService(userAccountSecurityService);
    return initializer;
  }

  @Bean("DObJournalEntryForRealizedExchangeRateCreateVendorInvoiceCommand")
  public DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand
      createDObJournalEntryForRealizedExchangeRateCreateVendorInvoiceCommand(
          IUserAccountSecurityService userAccountSecurityService,
          LObGlobalAccountConfigGeneralModelRep globalAccountConfigGeneralModelRep,
          DObVendorInvoiceJournalEntryRep vendorInvoiceJournalEntryRep,
          DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep
              vendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep) {

    DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand vendorInvoiceCommand =
        new DObJournalEntryCreateImportVendorInvoiceRealizedExchangeRateCommand();

    vendorInvoiceCommand.setUserAccountSecurityService(userAccountSecurityService);
    vendorInvoiceCommand.setGlobalAccountConfigGeneralModelRep(globalAccountConfigGeneralModelRep);
    vendorInvoiceCommand.setVendorInvoiceJournalEntryRep(vendorInvoiceJournalEntryRep);
    vendorInvoiceCommand.setVendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep(
        vendorInvoiceRealizedExchangeRateDifferenceGeneralModelRep);
    return vendorInvoiceCommand;
  }
}
