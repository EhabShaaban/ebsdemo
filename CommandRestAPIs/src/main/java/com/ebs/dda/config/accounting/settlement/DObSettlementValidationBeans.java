package com.ebs.dda.config.accounting.settlement;

import com.ebs.dda.accounting.dbo.jpa.repositories.generalmodels.SubAccountGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.validation.settlement.DObSettlementActivationValidator;
import com.ebs.dda.accounting.settlement.validation.IObSettlementAddAccountingDetailsValidator;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ebs.dda.accounting.settlement.validation.DObSettlementCreationValidator;
import com.ebs.dda.repositories.DocumentOwnerGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;

@Configuration
public class DObSettlementValidationBeans {

  @Bean
  public DObSettlementCreationValidator createSettlementCreationValidator(
      CObPurchasingUnitRep businessUnitRep,
      CObCompanyRep companyRep,
      CObCurrencyRep currencyRep,
      DocumentOwnerGeneralModelRep documentOwnerGeneralModelRep) {
    DObSettlementCreationValidator validator = new DObSettlementCreationValidator();
    validator.setBusinessUnitRep(businessUnitRep);
    validator.setCompanyRep(companyRep);
    validator.setCurrencyRep(currencyRep);
    validator.setDocumentOwnerGeneralModelRep(documentOwnerGeneralModelRep);
    return validator;
  }

  @Bean
  public DObSettlementActivationValidator createSettlementActivationValidation(
         DObSettlementGeneralModelRep settlementRep,
         IObSettlementAccountDetailsGeneralModelRep settlementAccountDetailsGeneralModelRep,
         IObSettlementDetailsGeneralModelRep settlementDetailsGeneralModelRep,
         CObFiscalYearRep fiscalYearRep
  ) {
    DObSettlementActivationValidator dObSettlementActivationValidator = new DObSettlementActivationValidator();
    dObSettlementActivationValidator.setSettlementRep(settlementRep);
    dObSettlementActivationValidator.setSettlementAccountDetailsGeneralModelRep(settlementAccountDetailsGeneralModelRep);
    dObSettlementActivationValidator.setSettlementDetailsGeneralModelRep(settlementDetailsGeneralModelRep);
    dObSettlementActivationValidator.setFiscalYearRep(fiscalYearRep);
    return dObSettlementActivationValidator;
  }

  @Bean
  public IObSettlementAddAccountingDetailsValidator createSettlementAddAccountingDetailsValidation(
          DObSettlementGeneralModelRep settlementGeneralModelRep,
          HObChartOfAccountsGeneralModelRep glAccountsGeneralModelRep,
          SubAccountGeneralModelRep subAccountGeneralModelRep) {
    IObSettlementAddAccountingDetailsValidator settlementAccountingDetailsValidator = new IObSettlementAddAccountingDetailsValidator();
    settlementAccountingDetailsValidator.setSettlementGeneralModelRep(settlementGeneralModelRep);
    settlementAccountingDetailsValidator.setGlAccountsGeneralModelRep(glAccountsGeneralModelRep);
    settlementAccountingDetailsValidator.setSubAccountGeneralModelRep(subAccountGeneralModelRep);
    return settlementAccountingDetailsValidator;
  }
}
