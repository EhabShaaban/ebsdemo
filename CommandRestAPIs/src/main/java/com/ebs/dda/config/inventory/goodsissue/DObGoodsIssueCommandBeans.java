package com.ebs.dda.config.inventory.goodsissue;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.goodsissue.DObGoodsIssueCreateCommand;
import com.ebs.dda.commands.inventory.goodsissue.DObGoodsIssueDeleteCommand;
import com.ebs.dda.commands.inventory.goodsissue.DObGoodsIssuePdfGeneratorCommand;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssue;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.IObSalesInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.*;
import com.ebs.dda.repositories.inventory.goodsreceipt.StoreKeeperRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerAddressRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerContactPersonRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.item.MObItemGeneralModelRep;
import com.ebs.dda.repositories.masterdata.measure.CObMeasureRep;
import com.ebs.dda.repositories.masterdata.plant.CObPlantGeneralModelRep;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderItemsGeneralModelRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DObGoodsIssueCommandBeans {

    @Bean
    public DObGoodsIssueCreateCommand createCommandGoodsIssue(
            DocumentObjectCodeGenerator documentObjectCodeGenerator,
            IUserAccountSecurityService userAccountSecurityService,
            DObGoodsIssueSalesInvoiceRep dObGoodsIssueRep,
            CObPurchasingUnitRep purchasingUnitRep,
            CObGoodsIssueRep cobGoodsIssueRep,
            MObCustomerRep customerRep,
            StoreKeeperRep storeKeeperRep,
            CObStorehouseGeneralModelRep storehouseRep,
            CObPlantGeneralModelRep plantRep,
            CObCompanyRep companyRep,
            DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep,
            IObSalesInvoiceItemsGeneralModelRep salesInvoiceItemsGeneralModelRep,
            MObItemGeneralModelRep itemGeneralModelRep,
            CObMeasureRep measureRep,
            IObCustomerContactPersonRep customerContactPersonRep,
            IObCustomerAddressRep customerAddressRep,
            DObSalesOrderGeneralModelRep salesOrderGeneralModelRep,
            IObSalesOrderItemsGeneralModelRep salesOrderItemsGeneralModelRep,
            DObGoodsIssueSalesOrderRep goodsIssueSalesOrderRep,
            IObSalesOrderDataRep salesOrderDataRep,
            DObSalesOrderRep salesOrderRep)
            throws Exception {

        DObGoodsIssueCreateCommand goodsIssueCreateCommand =
                new DObGoodsIssueCreateCommand(documentObjectCodeGenerator);
        goodsIssueCreateCommand.setGoodsIssueSalesInvoiceRep(dObGoodsIssueRep);
        goodsIssueCreateCommand.setPurchasingUnitRep(purchasingUnitRep);
        goodsIssueCreateCommand.setCobGoodsIssueRep(cobGoodsIssueRep);
        goodsIssueCreateCommand.setMObCustomerRep(customerRep);
        goodsIssueCreateCommand.setStoreKeeperRep(storeKeeperRep);
        goodsIssueCreateCommand.setStorehouseRep(storehouseRep);
        goodsIssueCreateCommand.setPlantRep(plantRep);
        goodsIssueCreateCommand.setCompanyRep(companyRep);
        goodsIssueCreateCommand.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
        goodsIssueCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
        goodsIssueCreateCommand.setSalesInvoiceItemsGeneralModelRep(salesInvoiceItemsGeneralModelRep);
        goodsIssueCreateCommand.setItemGeneralModelRep(itemGeneralModelRep);
        goodsIssueCreateCommand.setMeasureRep(measureRep);
        goodsIssueCreateCommand.setCustomerContactPersonRep(customerContactPersonRep);
        goodsIssueCreateCommand.setCustomerAddressRep(customerAddressRep);
        goodsIssueCreateCommand.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
        goodsIssueCreateCommand.setSalesOrderItemsGeneralModelRep(salesOrderItemsGeneralModelRep);
        goodsIssueCreateCommand.setGoodsIssueSalesOrderRep(goodsIssueSalesOrderRep);
        goodsIssueCreateCommand.setSalesOrderDataRep(salesOrderDataRep);
        goodsIssueCreateCommand.setSalesOrderRep(salesOrderRep);
        return goodsIssueCreateCommand;
    }

    @Bean
    DObGoodsIssueDeleteCommand deleteGoodsIssueCommand(
            DObGoodsIssueRep goodsIssueRep,
            @Qualifier("GoodsIssueLockCommand") EntityLockCommand entityLockCommand) {
        DObGoodsIssueDeleteCommand goodsIssueDeleteCommand = new DObGoodsIssueDeleteCommand();
        goodsIssueDeleteCommand.setGoodsIssueRep(goodsIssueRep);
        goodsIssueDeleteCommand.setEntityLockCommand(entityLockCommand);
        return goodsIssueDeleteCommand;
    }

    @Bean("GoodsIssueLockCommand")
    public EntityLockCommand<DObGoodsIssue> createGoodsIssueLockCommand(
            IUserAccountSecurityService userAccountSecurityService,
            IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
            DObGoodsIssueRep dObGoodsIssueRep) {
        EntityLockCommand<DObGoodsIssue> entityLockCommand = new EntityLockCommand<>();
        entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
        entityLockCommand.setConcurrentDataAccessManager(
                concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
                        DObGoodsIssue.class.getName()));
        entityLockCommand.setRepository(dObGoodsIssueRep);
        return entityLockCommand;
    }

    @Bean("DObGoodsIssuePdfGeneratorCommand")
    public DObGoodsIssuePdfGeneratorCommand createDObGoodsIssuePdfGeneratorCommand(
            DataSource dataSource, IObGoodsIssueItemGeneralModelRep goodsIssueItemGeneralModelRep) {
        DObGoodsIssuePdfGeneratorCommand goodsIssuePdfGeneratorCommand =
                new DObGoodsIssuePdfGeneratorCommand();
        goodsIssuePdfGeneratorCommand.setGoodsIssueItemGeneralModelRep(goodsIssueItemGeneralModelRep);
        goodsIssuePdfGeneratorCommand.setDataSource(dataSource);
        return goodsIssuePdfGeneratorCommand;
    }
}
