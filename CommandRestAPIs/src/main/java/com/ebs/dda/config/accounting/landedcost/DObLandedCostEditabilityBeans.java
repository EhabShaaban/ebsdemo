package com.ebs.dda.config.accounting.landedcost;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ebs.dda.accounting.landedcost.editability.LandedCostDetailsEditabilityManger;
import com.ebs.dda.accounting.landedcost.editability.LandedCostFactorItemEditabilityManger;

@Configuration
public class DObLandedCostEditabilityBeans {

	@Bean
	public LandedCostDetailsEditabilityManger createLandedCostDetailsEditabilityManger() {
		LandedCostDetailsEditabilityManger landedCostDetailsEditabilityManger = new LandedCostDetailsEditabilityManger();
		return landedCostDetailsEditabilityManger;
	}

	@Bean
	public LandedCostFactorItemEditabilityManger createLandedCostFactorItemEditabilityManger() {
		LandedCostFactorItemEditabilityManger landedCostFactorItemEditabilityManger = new LandedCostFactorItemEditabilityManger();
		return landedCostFactorItemEditabilityManger;
	}

}
