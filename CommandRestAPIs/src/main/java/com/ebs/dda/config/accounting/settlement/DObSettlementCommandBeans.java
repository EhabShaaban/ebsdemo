package com.ebs.dda.config.accounting.settlement;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dda.accounting.dbo.jpa.repositories.generalmodels.SubAccountGeneralModelRep;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.settlement.DObSettlementCreationCommand;
import com.ebs.dda.commands.accounting.settlement.DObSettlementPostCommand;
import com.ebs.dda.commands.accounting.settlement.IObSettlementAccountingDetailDeleteCommand;
import com.ebs.dda.commands.accounting.settlement.IObSettlementSaveAccountingDetailsCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.IObAccountingDocumentAccountingDetailsRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementActivationPreparationRep;
import com.ebs.dda.repositories.accounting.settlement.DObSettlementRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.chartofaccount.HObChartOfAccountsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

@Configuration
public class DObSettlementCommandBeans {

  @Bean
  public DObSettlementCreationCommand createSettlementCreationCommand(
      DObSettlementRep settlementRep,
      CObPurchasingUnitRep purchasingUnitRep,
      CObCompanyRep companyRep,
      CObCurrencyRep currencyRep,
      DocumentObjectCodeGenerator codeGenerator,
      IUserAccountSecurityService userAccountSecurityService)
      throws Exception {
    DObSettlementCreationCommand command = new DObSettlementCreationCommand();
    command.setSettlementRep(settlementRep);
    command.setPurchasingUnitRep(purchasingUnitRep);
    command.setCompanyRep(companyRep);
    command.setCurrencyRep(currencyRep);
    command.setCodeGenerator(codeGenerator);
    command.setUserAccountSecurityService(userAccountSecurityService);
    return command;
  }

  @Bean("DObSettlementLockCommand")
  public EntityLockCommand<DObSettlement> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObSettlementRep settlementRep) {

    EntityLockCommand<DObSettlement> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObSettlement.class.getName()));
    entityLockCommand.setRepository(settlementRep);
    return entityLockCommand;
  }

  @Bean("SettlementLockCommandJMXBean")
  public ObjectInstance createEntityLockCommandJMXBean(
      @Qualifier("DObSettlementLockCommand") EntityLockCommand entityLockCommand) throws Exception {
    MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
    ObjectName name =
        new ObjectName("com.ebs.dda.commands.general:name=SettlementEntityLockCommand");
    return mbs.registerMBean(entityLockCommand, name);
  }

  @Bean
  public DObSettlementPostCommand createSettlementPostCommand(
      DObSettlementRep settlementRep,
      TransactionManagerDelegate transactionManagerDelegate,
      IUserAccountSecurityService userAccountSecurityService,
      DObSettlementActivationPreparationRep settlementActivationPreparationRep,
      CObFiscalYearRep fiscalYearRep)
      throws Exception {
    DObSettlementPostCommand command = new DObSettlementPostCommand();
    command.setSettlementRep(settlementRep);
    command.setTransactionManagerDelegate(transactionManagerDelegate);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setSettlementActivationPreparationRep(settlementActivationPreparationRep);
    command.setFiscalYearRep(fiscalYearRep);
    return command;
  }

  @Bean
  public IObSettlementAccountingDetailDeleteCommand createSettlementAccountingDetailDeleteCommand(
      IObAccountingDocumentAccountingDetailsRep accountingDetailsRep,
      DObSettlementRep settlementRep,
      IUserAccountSecurityService userAccountSecurityService) {
    IObSettlementAccountingDetailDeleteCommand command =
        new IObSettlementAccountingDetailDeleteCommand();
    command.setAccountingDetailsRep(accountingDetailsRep);
    command.setSettlementRep(settlementRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    return command;
  }

  @Bean
  public IObSettlementSaveAccountingDetailsCommand createIObSettlementSaveAccountingDetailsCommand(
      DObSettlementRep settlementRep,
      IUserAccountSecurityService userAccountSecurityService,
      HObChartOfAccountsGeneralModelRep chartOfAccountsGeneralModelRep,
      SubAccountGeneralModelRep subAccountGeneralModelRep) {
    IObSettlementSaveAccountingDetailsCommand command =
        new IObSettlementSaveAccountingDetailsCommand();

    command.setSettlementRep(settlementRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setChartOfAccountsGeneralModelRep(chartOfAccountsGeneralModelRep);
    command.setSubAccountGeneralModelRep(subAccountGeneralModelRep);

    return command;
  }
}
