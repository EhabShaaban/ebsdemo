package com.ebs.dda.config.paymentrequest;

import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteRep;
import com.ebs.dda.repositories.accounting.accountingnote.IObAccountingNoteCompanyGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceOrderGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentReferenceVendorInvoiceGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.paymentrequest.*;
import com.ebs.dda.repositories.accounting.paymentrequest.DObPaymentRequestRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.customer.MObEmployeeRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;

@Configuration
public class DObPaymentCreateCommandBeans {
	@Bean
	public DObDownPaymentCreateCommand createDownPaymentCreateCommand(
			IUserAccountSecurityService userAccountSecurityService,
			CObPurchasingUnitRep purchasingUnitRep, DObPaymentRequestRep paymentRequestRep,
			DocumentObjectCodeGenerator documentObjectCodeGenerator, MObVendorRep vendorRep,
			DObPaymentReferenceOrderGeneralModelRep referenceOrderGeneralModelRep
			) throws Exception {
		DObPaymentRequestStateMachine stateMachine = new DObPaymentRequestStateMachine();
		DObDownPaymentCreateCommand command = new DObDownPaymentCreateCommand(stateMachine);
		command.setUserAccountSecurityService(userAccountSecurityService);
		command.setPaymentRequestRep(paymentRequestRep);
		command.setPurchasingUnitRep(purchasingUnitRep);
		command.setVendorRep(vendorRep);
		command.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
		command.setRefDocumentRep(referenceOrderGeneralModelRep);
		return command;
	}

	@Bean
	public DObPaymentAgainstVendorInvoiceCreateCommand createPaymentAgainstVendorInvoiceCreateCommand(
			IUserAccountSecurityService userAccountSecurityService,
			CObPurchasingUnitRep purchasingUnitRep, DObPaymentRequestRep paymentRequestRep,
			DocumentObjectCodeGenerator documentObjectCodeGenerator, MObVendorRep vendorRep,
			DObPaymentReferenceVendorInvoiceGeneralModelRep vendorInvoiceGeneralModelRep
			) throws Exception {
		DObPaymentRequestStateMachine stateMachine = new DObPaymentRequestStateMachine();
		DObPaymentAgainstVendorInvoiceCreateCommand command = new DObPaymentAgainstVendorInvoiceCreateCommand(stateMachine);
		command.setUserAccountSecurityService(userAccountSecurityService);
		command.setPaymentRequestRep(paymentRequestRep);
		command.setPurchasingUnitRep(purchasingUnitRep);
		command.setVendorRep(vendorRep);
		command.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
		command.setRefDocumentRep(vendorInvoiceGeneralModelRep);
		return command;
	}

	@Bean
	public DObGeneralPaymentCreateCommand createGeneralPaymentCreateCommand(
					IUserAccountSecurityService userAccountSecurityService,
					CObPurchasingUnitRep purchasingUnitRep, DObPaymentRequestRep paymentRequestRep,
					DocumentObjectCodeGenerator documentObjectCodeGenerator,
					DObPaymentReferenceOrderGeneralModelRep referenceOrderGeneralModelRep
	) throws Exception {
		DObPaymentRequestStateMachine stateMachine = new DObPaymentRequestStateMachine();
		DObGeneralPaymentCreateCommand command = new DObGeneralPaymentCreateCommand(stateMachine);
		command.setUserAccountSecurityService(userAccountSecurityService);
		command.setPaymentRequestRep(paymentRequestRep);
		command.setPurchasingUnitRep(purchasingUnitRep);
		command.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
		command.setRefDocumentRep(referenceOrderGeneralModelRep);
		return command;
	}

	@Bean
	public DObPaymentAgainstCreditNoteCreateCommand createPaymentAgainstCreditNoteCreateCommand(
			IUserAccountSecurityService userAccountSecurityService,
			CObPurchasingUnitRep purchasingUnitRep, DObPaymentRequestRep paymentRequestRep,
			DocumentObjectCodeGenerator documentObjectCodeGenerator, MObVendorRep vendorRep,
			DObAccountingNoteRep accountingNoteRep,
			IObAccountingNoteCompanyGeneralModelRep accountingNoteCompanyGeneralModelRep) throws Exception {
		DObPaymentRequestStateMachine stateMachine = new DObPaymentRequestStateMachine();
		DObPaymentAgainstCreditNoteCreateCommand command = new DObPaymentAgainstCreditNoteCreateCommand(stateMachine);
		command.setUserAccountSecurityService(userAccountSecurityService);
		command.setPaymentRequestRep(paymentRequestRep);
		command.setPurchasingUnitRep(purchasingUnitRep);
		command.setVendorRep(vendorRep);
		command.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
		command.setAccountingNoteRep(accountingNoteRep);
		command.setAccountingNoteCompanyGeneralModelRep(accountingNoteCompanyGeneralModelRep);
		return command;
	}

	@Bean
	public DObUnderSettlementPaymentCreateCommand createUnderSettlementPaymentCreateCommand(
					IUserAccountSecurityService userAccountSecurityService,
					CObPurchasingUnitRep purchasingUnitRep, DObPaymentRequestRep paymentRequestRep,
					DocumentObjectCodeGenerator documentObjectCodeGenerator,
					CObCompanyRep companyRep, MObEmployeeRep employeeRep) throws Exception {
		DObPaymentRequestStateMachine stateMachine = new DObPaymentRequestStateMachine();
		DObUnderSettlementPaymentCreateCommand command = new DObUnderSettlementPaymentCreateCommand(stateMachine);
		command.setUserAccountSecurityService(userAccountSecurityService);
		command.setPaymentRequestRep(paymentRequestRep);
		command.setPurchasingUnitRep(purchasingUnitRep);
		command.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
		command.setCompanyRep(companyRep);
		command.setEmployeeRep(employeeRep);
		return command;
	}

}
