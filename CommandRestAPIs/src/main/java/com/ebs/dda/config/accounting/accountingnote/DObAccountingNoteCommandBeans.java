package com.ebs.dda.config.accounting.accountingnote;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.accounting.accountingnote.AccountingNoteCreateCommand;
import com.ebs.dda.commands.accounting.accountingnote.DObDebitNoteUpdateRemainingCommand;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;
import com.ebs.dda.repositories.accounting.accountingnote.DObAccountingNoteRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import com.ebs.dda.repositories.masterdata.company.CObCompanyRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.MObCustomerRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.repositories.order.salesreturnorder.DObSalesReturnOrderGeneralModelRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObAccountingNoteCommandBeans {

  @Bean
  public AccountingNoteCreateCommand accountingNoteCreateCommand(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      DObAccountingNoteRep accountingNoteRep,
      CObCompanyRep companyRep,
      CObPurchasingUnitRep businessUnitRep,
      MObVendorRep vendorRep,
      CObCurrencyRep currencyRep,
      DObSalesReturnOrderGeneralModelRep salesReturnOrderGeneralModelRep,
      MObCustomerRep customerRep,
      IUserAccountSecurityService userAccountSecurityService)
      throws Exception {
    AccountingNoteCreateCommand accountingNoteCreateCommand =
        new AccountingNoteCreateCommand(documentObjectCodeGenerator);
    accountingNoteCreateCommand.setAccountingNoteRep(accountingNoteRep);
    accountingNoteCreateCommand.setCompanyRep(companyRep);
    accountingNoteCreateCommand.setBusinessUnitRep(businessUnitRep);
    accountingNoteCreateCommand.setVendorRep(vendorRep);
    accountingNoteCreateCommand.setCurrencyRep(currencyRep);
    accountingNoteCreateCommand.setSalesReturnOrderGeneralModelRep(salesReturnOrderGeneralModelRep);
    accountingNoteCreateCommand.setCustomerRep(customerRep);
    accountingNoteCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    return accountingNoteCreateCommand;
  }

  @Bean("dObAccountingNoteLockCommand")
  public EntityLockCommand<DObAccountingNote> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObAccountingNoteRep accountingNoteRep) {
    EntityLockCommand<DObAccountingNote> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObAccountingNote.class.getName()));
    entityLockCommand.setRepository(accountingNoteRep);
    return entityLockCommand;
  }

  @Bean
  public DObDebitNoteUpdateRemainingCommand createDObDebitNoteUpdateRemainingCommand(
      IUserAccountSecurityService userAccountSecurityService,
      DObAccountingNoteRep accountingNoteRep) {
    DObDebitNoteUpdateRemainingCommand command = new DObDebitNoteUpdateRemainingCommand();
    command.setAccountingNoteRep(accountingNoteRep);
    command.setUserAccountSecurityService(userAccountSecurityService);
    return command;
  }
}
