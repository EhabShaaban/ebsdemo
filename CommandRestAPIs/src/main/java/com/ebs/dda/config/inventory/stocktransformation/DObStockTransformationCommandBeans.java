package com.ebs.dda.config.inventory.stocktransformation;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManagersObjectPool;
import com.ebs.dac.foundation.apis.security.IUserAccountSecurityService;
import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.commands.inventory.stocktransformation.DObStockTransformationCreateCommand;
import com.ebs.dda.commands.inventory.stocktransformation.DObStockTransformationUpdateCommand;
import com.ebs.dda.inventory.stocktransformation.validation.DObStockTransformationPostValidator;
import com.ebs.dda.jpa.inventory.stocktransformation.DObStockTransformation;
import com.ebs.dda.repositories.inventory.stocktransformation.*;
import com.ebs.dda.repositories.inventory.storetransaction.TObStockTransformationStoreTransactionRep;
import com.ebs.dda.repositories.inventory.storetransaction.TObStoreTransactionGeneralModelRep;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitRep;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DObStockTransformationCommandBeans {

  @Bean
  public DObStockTransformationCreateCommand createCommandStockTransformation(
      DocumentObjectCodeGenerator documentObjectCodeGenerator,
      IUserAccountSecurityService userAccountSecurityService,
      DObStockTransformationRep dObStockTransformationRep,
      CObPurchasingUnitRep purchasingUnitRep,
      TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep,
      CObStockTransformationTypeRep cObStockTransformationTypeRep,
      CObStockTransformationReasonRep cObStockTransformationReasonRep)
      throws Exception {

    DObStockTransformationCreateCommand dObStockTransformationCreateCommand =
        new DObStockTransformationCreateCommand(documentObjectCodeGenerator);
    dObStockTransformationCreateCommand.setDocumentObjectCodeGenerator(documentObjectCodeGenerator);
    dObStockTransformationCreateCommand.setUserAccountSecurityService(userAccountSecurityService);
    dObStockTransformationCreateCommand.setdObStockTransformationRep(dObStockTransformationRep);
    dObStockTransformationCreateCommand.setPurchasingUnitRep(purchasingUnitRep);
    dObStockTransformationCreateCommand.settObStoreTransactionGeneralModelRep(
        tObStoreTransactionGeneralModelRep);
    dObStockTransformationCreateCommand.setcObStockTransformationTypeRep(
        cObStockTransformationTypeRep);
    dObStockTransformationCreateCommand.setcObStockTransformationReasonRep(
        cObStockTransformationReasonRep);
    return dObStockTransformationCreateCommand;
  }

  @Bean("StockTransformationLockCommand")
  public EntityLockCommand<DObStockTransformation> createEntityLockCommand(
      IUserAccountSecurityService userAccountSecurityService,
      IConcurrentDataAccessManagersObjectPool concurrentDataAccessManagersPool,
      DObStockTransformationRep stockTransformationRep) {

    EntityLockCommand<DObStockTransformation> entityLockCommand = new EntityLockCommand<>();
    entityLockCommand.setUserAccountSecurityService(userAccountSecurityService);
    entityLockCommand.setConcurrentDataAccessManager(
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(
            DObStockTransformation.class.getName()));
    entityLockCommand.setRepository(stockTransformationRep);
    return entityLockCommand;
  }

  @Bean
  public DObStockTransformationPostValidator createStockTransformationValidator(
      TObStoreTransactionGeneralModelRep tObStoreTransactionGeneralModelRep) {
    DObStockTransformationPostValidator dObStockTransformationValidator =
        new DObStockTransformationPostValidator();
    dObStockTransformationValidator.settObStoreTransactionGeneralModelRep(
        tObStoreTransactionGeneralModelRep);
    return dObStockTransformationValidator;
  }

  @Bean
  public DObStockTransformationUpdateCommand createStockTransformationUpdateCommand(
          IUserAccountSecurityService userAccountSecurityService,
          DObStockTransformationGeneralModelRep stockTransformationGeneralModelRep,
          TObStoreTransactionGeneralModelRep storeTransactionGeneralModelRep,
          IObStockTransformationDetailsRep stockTransformationDetailsRep,
          TObStockTransformationStoreTransactionRep stockTransformationStoreTransactionRep) {
    DObStockTransformationUpdateCommand command = new DObStockTransformationUpdateCommand();
    command.setUserAccountSecurityService(userAccountSecurityService);
    command.setStockTransformationGeneralModelRep(stockTransformationGeneralModelRep);
    command.setStockTransformationDetailsRep(stockTransformationDetailsRep);
    command.setStockTransformationStoreTransactionRep(stockTransformationStoreTransactionRep);
    return command;
  }
}
