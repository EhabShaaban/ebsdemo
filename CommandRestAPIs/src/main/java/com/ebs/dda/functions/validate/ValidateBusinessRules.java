package com.ebs.dda.functions.validate;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.IValueObject;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ValidateBusinessRules<V extends IValueObject>
    implements Observer<ValidateBusinessRulesContext<V>> {

  private IActivateBusinessRulesValidator<V> validator;

  public ValidateBusinessRules(IActivateBusinessRulesValidator<V> validator) {
    this.validator = validator;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ValidateBusinessRulesContext<V> context) {

    ValidationResult validationResult = validator.validateBusinessRules(context.valueObject());
    if (!validationResult.isValid()) {
      throw new BusinessRuleViolationException(validationResult);
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
