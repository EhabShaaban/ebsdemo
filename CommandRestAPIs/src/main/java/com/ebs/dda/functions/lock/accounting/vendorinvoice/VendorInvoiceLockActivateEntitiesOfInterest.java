package com.ebs.dda.functions.lock.accounting.vendorinvoice;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.AbstractActivateLockEntitesOfInterest;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;

public class VendorInvoiceLockActivateEntitiesOfInterest
    extends AbstractActivateLockEntitesOfInterest {

  private EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand;
  private EntityLockCommand<?> refDocumentLockCommand;

  public VendorInvoiceLockActivateEntitiesOfInterest(
      EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
      EntityLockCommand<?> refDocumentLockCommand) {

    this.vendorInvoiceLockCommand = vendorInvoiceLockCommand;
    this.refDocumentLockCommand = refDocumentLockCommand;

  }

  @Override
  public void lock(EntitiesOfInterestContainer entitiesOfInterestContainer) throws Exception {

    DObVendorInvoiceGeneralModel vendorInvoice =
        entitiesOfInterestContainer.getEntity(DObVendorInvoiceGeneralModel.class);

    lockEntity(vendorInvoiceLockCommand, vendorInvoice.getUserCode());

    if (vendorInvoice.getPurchaseOrderCode() != null) {
      lockEntity(refDocumentLockCommand, vendorInvoice.getPurchaseOrderCode());
    }
  }

}
