package com.ebs.dda.functions.lock.accounting.paymentrequest;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.order.DObOrderDocument;

public class PaymentForVendorAgainstPurchaseOrderLockActivateEntitiesOfInterest
    extends PaymentLockActivateEntitiesOfInterest {

  public PaymentForVendorAgainstPurchaseOrderLockActivateEntitiesOfInterest(
          EntityLockCommand<DObPaymentRequest> paymentLockCommand,
          EntityLockCommand<DObOrderDocument> purchaseOrderLockCommand,
          CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory) {
    super(paymentLockCommand, purchaseOrderLockCommand, journalBalanceLockCommandBeanFactory);
  }

}
