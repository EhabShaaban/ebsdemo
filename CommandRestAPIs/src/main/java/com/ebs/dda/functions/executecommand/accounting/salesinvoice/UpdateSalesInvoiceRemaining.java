package com.ebs.dda.functions.executecommand.accounting.salesinvoice;

import com.ebs.dda.commands.accounting.salesinvoice.IObSalesInvoiceUpdateRemainingCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionUpdateRefDocumentRemainingValueObject;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UpdateSalesInvoiceRemaining implements Observer<ExecuteActivateDependanciesContext> {

  private IObSalesInvoiceUpdateRemainingCommand updateRemainingCommand;

  public UpdateSalesInvoiceRemaining(IObSalesInvoiceUpdateRemainingCommand updateRemainingCommand) {
    this.updateRemainingCommand = updateRemainingCommand;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      updateRemainingCommand.executeCommand(
          initValueObject(context.entitiesOfInterestContainer().getMainEntity()));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObCollectionUpdateRefDocumentRemainingValueObject initValueObject(
      DObCollectionGeneralModel collection) {

    DObCollectionUpdateRefDocumentRemainingValueObject valueObject =
        new DObCollectionUpdateRefDocumentRemainingValueObject();

    valueObject.setCollectionCode(collection.getUserCode());
    valueObject.setRefDocumentCode(collection.getRefDocumentCode());
    valueObject.setRefDocumentType(collection.getRefDocumentTypeCode());

    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
