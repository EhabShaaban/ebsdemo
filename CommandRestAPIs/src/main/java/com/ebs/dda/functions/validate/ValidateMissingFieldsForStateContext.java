package com.ebs.dda.functions.validate;

public interface ValidateMissingFieldsForStateContext {

  String state();

  String objectCode();

}
