package com.ebs.dda.functions.executecommand.accounting.salesinvoice;

import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceAccountDeterminationCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DetermineSalesInvoiceAccount implements Observer<ExecuteActivateDependanciesContext> {

    private final DObSalesInvoiceAccountDeterminationCommand accountDeterminationCommand;

    public DetermineSalesInvoiceAccount(DObSalesInvoiceAccountDeterminationCommand accountDeterminationCommand) {
        this.accountDeterminationCommand = accountDeterminationCommand;
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(ExecuteActivateDependanciesContext context) {
        try {
            DObJournalEntryCreateValueObject valueObject = initValueObject(context.entitiesOfInterestContainer().getMainEntity());
            accountDeterminationCommand.executeCommand(valueObject);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private DObJournalEntryCreateValueObject initValueObject(DObSalesInvoiceGeneralModel salesInvoice) {
        DObJournalEntryCreateValueObject valueObject = new DObJournalEntryCreateValueObject();
        valueObject.setUserCode(salesInvoice.getUserCode());
        return valueObject;
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }
}
