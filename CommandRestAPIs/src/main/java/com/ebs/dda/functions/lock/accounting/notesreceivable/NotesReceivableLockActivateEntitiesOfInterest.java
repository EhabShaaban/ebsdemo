package com.ebs.dda.functions.lock.accounting.notesreceivable;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.notesreceivables.DObNotesReceivableRefDocumentLockCommandFactory;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.IActivateLockEntitiesOfInterest;
import com.ebs.dda.jpa.accounting.monetarynotes.DObMonetaryNotes;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;

import java.util.List;

public class NotesReceivableLockActivateEntitiesOfInterest
    implements IActivateLockEntitiesOfInterest {

  private EntityLockCommand<DObMonetaryNotes> notesReceivableLockCommand;

  private DObNotesReceivableRefDocumentLockCommandFactory refDocumentLockCommandFactory;
  private EntityLockCommand<?> refDocumentLockCommand;

  public NotesReceivableLockActivateEntitiesOfInterest(
      EntityLockCommand<DObMonetaryNotes> notesReceivableLockCommand,
      DObNotesReceivableRefDocumentLockCommandFactory refDocumentLockCommandFactory) {

    this.notesReceivableLockCommand = notesReceivableLockCommand;
    this.refDocumentLockCommandFactory = refDocumentLockCommandFactory;
  }

  @Override
  public void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {

    String notesReceivableCode = getNotesReceivableCode(entitiesOfInterestContainer);

    List<IObNotesReceivablesReferenceDocumentGeneralModel> notesReceivableReferenceDocumentList =
        entitiesOfInterestContainer.getEntityList(
            IObNotesReceivablesReferenceDocumentGeneralModel.class);

    try {
      notesReceivableLockCommand.lockAllSections(notesReceivableCode);
      lockNotesReceivableRefDocuments(notesReceivableReferenceDocumentList);
    } catch (Exception e) {
      unlock(notesReceivableCode, notesReceivableReferenceDocumentList);
      throw e;
    }
  }

  private String getNotesReceivableCode(EntitiesOfInterestContainer entitiesOfInterestContainer) {
    DObNotesReceivablesGeneralModel notesReceivable = entitiesOfInterestContainer.getMainEntity();
    return notesReceivable.getUserCode();
  }

  private void lockNotesReceivableRefDocuments(
      List<IObNotesReceivablesReferenceDocumentGeneralModel> notesReceivableReferenceDocumentList)
      throws Exception {

    for (IObNotesReceivablesReferenceDocumentGeneralModel notesReceivableReferenceDocument :
        notesReceivableReferenceDocumentList) {
      String refDocumentCode = notesReceivableReferenceDocument.getRefDocumentCode();
      String refDocumentType = notesReceivableReferenceDocument.getDocumentType();
      lockNotesReceivableRefDocument(refDocumentType, refDocumentCode);
    }
  }

  private void lockNotesReceivableRefDocument(String refDocumentType, String refDocumentCode)
      throws Exception {
    refDocumentLockCommand =
        refDocumentLockCommandFactory.getRefDocumentLockCommand(refDocumentType);
    refDocumentLockCommand.lockAllSections(refDocumentCode);
  }

  private void unlock(
      String notesReceivableCode,
      List<IObNotesReceivablesReferenceDocumentGeneralModel> notesReceivableReferenceDocumentList)
      throws Exception {

    unLockNotesReceivable(notesReceivableCode);
    unLockNotesReceivableRefDocuments(notesReceivableReferenceDocumentList);
  }

  private void unLockNotesReceivable(String notesReceivableCode) throws Exception {
    if (notesReceivableLockCommand.isAllSectionsLocked(notesReceivableCode)) {
      notesReceivableLockCommand.unlockAllSections(notesReceivableCode);
    }
  }

  private void unLockNotesReceivableRefDocuments(
      List<IObNotesReceivablesReferenceDocumentGeneralModel> notesReceivableReferenceDocumentList)
      throws Exception {
    for (IObNotesReceivablesReferenceDocumentGeneralModel notesReceivableReferenceDocument :
        notesReceivableReferenceDocumentList) {
      String refDocumentCode = notesReceivableReferenceDocument.getRefDocumentCode();
      String refDocumentType = notesReceivableReferenceDocument.getDocumentType();
      unlockNotesReceivableRefDocument(refDocumentType, refDocumentCode);
    }
  }

  private void unlockNotesReceivableRefDocument(String refDocumentType, String refDocumentCode)
      throws Exception {
    refDocumentLockCommand =
        refDocumentLockCommandFactory.getRefDocumentLockCommand(refDocumentType);
    if (refDocumentLockCommand.isAllSectionsLocked(refDocumentCode)) {
      refDocumentLockCommand.unlockAllSections(refDocumentCode);
    }
  }

  @Override
  public void unlockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    String notesReceivableCode = getNotesReceivableCode(entitiesOfInterestContainer);

    List<IObNotesReceivablesReferenceDocumentGeneralModel> notesReceivableReferenceDocumentList =
        entitiesOfInterestContainer.getEntityList(
            IObNotesReceivablesReferenceDocumentGeneralModel.class);

    unlock(notesReceivableCode, notesReceivableReferenceDocumentList);
  }
}
