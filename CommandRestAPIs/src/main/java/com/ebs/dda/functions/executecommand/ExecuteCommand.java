package com.ebs.dda.functions.executecommand;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.jpa.IValueObject;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ExecuteCommand<V extends IValueObject> implements Observer<ExecuteCommandContext<V>> {

  private ICommand<V> command;

  public ExecuteCommand(ICommand<V> command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteCommandContext<V> context) {
    try {
      command.executeCommand(context.valueObject());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
