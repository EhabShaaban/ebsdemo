package com.ebs.dda.functions.executequery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntitiesOfInterestContainer {
  private Map<Class, Object> entities;
  private Class mainObjectClass;

  public EntitiesOfInterestContainer(Class mainObjectClass) {
    this.entities = new HashMap<>();
    this.mainObjectClass = mainObjectClass;
  }

  public void setEntity(Class entityClass, Object entity) {
    if (entity != null) {
      this.entities.put(entityClass, entity);
    }
  }

  public <T> T getEntity(Class<T> entityClass) {
    return (T) this.entities.get(entityClass);
  }

  public List<String> getEntityListOfString(Class entityClass) {
    return (List<String>) this.entities.get(entityClass);
  }

  public <T> List<T> getEntityList(Class<T> entityClass) {
    return (List<T>) this.entities.get(entityClass);
  }

  public String getStringValue(Class entityClass) {
    return (String) this.entities.get(entityClass);
  }

  public <T> T getMainEntity() {
    return (T) this.getEntity(mainObjectClass);
  }
}
