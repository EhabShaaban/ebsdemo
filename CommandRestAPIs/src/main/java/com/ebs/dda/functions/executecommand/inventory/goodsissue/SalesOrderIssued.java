package com.ebs.dda.functions.executecommand.inventory.goodsissue;

import com.ebs.dda.commands.order.salesorder.DObSalesOrderIssuedCommand;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderUpdateStateValueObject;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class SalesOrderIssued
    implements Observer<ExecuteCommandContext<DObInventoryDocumentActivateValueObject>> {
  private DObSalesOrderIssuedCommand salesOrderIssuedCommand;


  public SalesOrderIssued(DObSalesOrderIssuedCommand salesOrderIssuedCommand) {
    this.salesOrderIssuedCommand = salesOrderIssuedCommand;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(
      @NonNull ExecuteCommandContext<DObInventoryDocumentActivateValueObject> context) {
    String refDocumentCode = context.valueObject().getRefDocumentCode();
    if (refDocumentCode != null) {
      DObSalesOrderUpdateStateValueObject valueObject = new DObSalesOrderUpdateStateValueObject();
      valueObject.setSalesOrderCode(refDocumentCode);
      try {
        salesOrderIssuedCommand.executeCommand(valueObject);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
