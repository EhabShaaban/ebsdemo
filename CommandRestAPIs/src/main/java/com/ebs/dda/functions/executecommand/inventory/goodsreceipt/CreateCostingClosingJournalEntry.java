package com.ebs.dda.functions.executecommand.inventory.goodsreceipt;

import com.ebs.dda.commands.accounting.costing.DObCostingClosingJournalCreateCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class CreateCostingClosingJournalEntry
    implements Observer<ExecuteActivateDependanciesContext> {
  public static final String GR_PO = "GR_PO";
  private DObCostingClosingJournalCreateCommand costingClosingJournalCreateCommand;

  public CreateCostingClosingJournalEntry(
      DObCostingClosingJournalCreateCommand costingClosingJournalCreateCommand) {
    this.costingClosingJournalCreateCommand = costingClosingJournalCreateCommand;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(@NonNull ExecuteActivateDependanciesContext context) {
    DObCostingActivateValueObject valueObject = context.valueObject();
    try {
      Observable<String> journalEntryCode =
          costingClosingJournalCreateCommand.executeCommand(valueObject);
      journalEntryCode.subscribe(userCode -> context.journalEntryUserCode(userCode));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
