package com.ebs.dda.functions.executequery.inventory.goodsreceipt;

import java.util.List;

public interface FindStockAvailabilitiesContext {
  void stockAvailabilityCodes(List<String> stockAvailabilityCodes);
}
