package com.ebs.dda.functions.executecommand.accounting.journalbalance;

import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceIncreaseBalanceCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceUpdateValueObject;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class IncreaseJournalBalance implements Observer<ExecuteActivateDependanciesContext> {

  private CObJournalBalanceIncreaseBalanceCommand command;

  public IncreaseJournalBalance(CObJournalBalanceIncreaseBalanceCommand command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      command.executeCommand(initValueObject(context.entitiesOfInterestContainer()));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private JournalBalanceUpdateValueObject initValueObject(
      EntitiesOfInterestContainer entitiesOfInterestContainer) {

    JournalBalanceUpdateValueObject valueObject = new JournalBalanceUpdateValueObject();

    CObJournalBalanceGeneralModel journalBalance =
        entitiesOfInterestContainer.getEntity(CObJournalBalanceGeneralModel.class);
    valueObject.setJournalBalanceCode(journalBalance.getUserCode());

    DObCollectionGeneralModel collection = entitiesOfInterestContainer.getMainEntity();
    valueObject.setAmount(collection.getAmount());

    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}


}
