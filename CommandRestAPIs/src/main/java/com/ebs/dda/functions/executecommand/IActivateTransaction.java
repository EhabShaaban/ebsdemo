package com.ebs.dda.functions.executecommand;

public interface IActivateTransaction {

  void doTransaction();
}
