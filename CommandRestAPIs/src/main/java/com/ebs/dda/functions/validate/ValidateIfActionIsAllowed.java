package com.ebs.dda.functions.validate;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ValidateIfActionIsAllowed implements Observer<ValidateIfActionIsAllowedContext> {

  private AbstractStateMachine stateMachine;

  public ValidateIfActionIsAllowed(AbstractStateMachine stateMachine) {
    this.stateMachine = stateMachine;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ValidateIfActionIsAllowedContext context) {
    try {
      StatefullBusinessObject businessObject =
          context.entitiesOfInterestContainer().getMainEntity();
      stateMachine.initObjectState(businessObject);
      stateMachine.isAllowedAction(context.actionName());
    } catch (Exception e) {
      throw new ActionNotAllowedPerStateException();
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
