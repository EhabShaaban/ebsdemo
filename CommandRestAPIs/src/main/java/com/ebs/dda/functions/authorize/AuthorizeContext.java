package com.ebs.dda.functions.authorize;

import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;

public interface AuthorizeContext {
    String objectSysName();
    String actionName();
    EntitiesOfInterestContainer entitiesOfInterestContainer();
}
