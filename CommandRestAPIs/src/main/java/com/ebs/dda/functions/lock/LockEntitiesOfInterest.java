package com.ebs.dda.functions.lock;

import com.ebs.dac.foundation.exceptions.operational.locking.AlreadyLockedException;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class LockEntitiesOfInterest implements Observer<LockContext> {

  private IActivateLockEntitiesOfInterest lock;

  public LockEntitiesOfInterest(IActivateLockEntitiesOfInterest lock) {
    this.lock = lock;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(LockContext context) {
    try {
      lock.lockEntitiesOfInterest(context.entitiesOfInterestContainer());
    } catch (Exception e) {
      throw new AlreadyLockedException(e);
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
