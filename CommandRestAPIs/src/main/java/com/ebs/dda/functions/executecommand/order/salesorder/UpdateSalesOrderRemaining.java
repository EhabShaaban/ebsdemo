package com.ebs.dda.functions.executecommand.order.salesorder;

import com.ebs.dda.commands.order.salesorder.DObSalesOrderUpdateRemainingCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.DocumentObjectUpdateRemainingValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UpdateSalesOrderRemaining implements Observer<ExecuteActivateDependanciesContext> {

  private DObSalesOrderUpdateRemainingCommand updateRemainingCommand;

  public UpdateSalesOrderRemaining(DObSalesOrderUpdateRemainingCommand updateRemainingCommand) {
    this.updateRemainingCommand = updateRemainingCommand;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      updateRemainingCommand.executeCommand(
          initValueObject(context.entitiesOfInterestContainer().getMainEntity()));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DocumentObjectUpdateRemainingValueObject initValueObject(
      DObCollectionGeneralModel collection) {

    DocumentObjectUpdateRemainingValueObject valueObject =
        new DocumentObjectUpdateRemainingValueObject();

    valueObject.setUserCode(collection.getRefDocumentCode());
    valueObject.setAmount(collection.getAmount());

    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
