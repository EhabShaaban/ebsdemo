package com.ebs.dda.functions.validate;

import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;

public interface ValidateIfActionIsAllowedContext {

  EntitiesOfInterestContainer entitiesOfInterestContainer();

  String actionName();

}
