package com.ebs.dda.functions.serialize;

import com.ebs.dda.jpa.IValueObject;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DeserializeValueObject<V extends IValueObject> implements Observer<DeserializeValueObjectContext<V>> {

  private SerializionUtils serializer;

  public DeserializeValueObject(SerializionUtils serializer) {
    this.serializer = serializer;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(DeserializeValueObjectContext<V> context) {
    V valueObject = serializer.deserializeActivateValueObject(context.activateData(), context.valueObjectClass(), context.objectCode());
    context.valueObject(valueObject);
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
