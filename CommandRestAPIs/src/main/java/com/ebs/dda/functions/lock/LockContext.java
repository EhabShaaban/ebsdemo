package com.ebs.dda.functions.lock;

import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;

public interface LockContext {

  EntitiesOfInterestContainer entitiesOfInterestContainer();
}
