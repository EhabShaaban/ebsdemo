package com.ebs.dda.functions.lock.accounting.salesinvoice;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrder;

public class SalesInvoiceSalesOrderLockActivateEntitiesOfInterest extends SalesInvoiceLockActivateEntitiesOfInterest {

  public SalesInvoiceSalesOrderLockActivateEntitiesOfInterest(EntityLockCommand<DObSalesInvoice> salesInvoiceLockCommand,
                                                              EntityLockCommand<DObSalesOrder> salesOrderLockCommand) {
    super(salesInvoiceLockCommand, salesOrderLockCommand);
  }

}
