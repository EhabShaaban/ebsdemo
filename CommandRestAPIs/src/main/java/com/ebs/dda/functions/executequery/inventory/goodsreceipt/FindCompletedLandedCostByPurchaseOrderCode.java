package com.ebs.dda.functions.executequery.inventory.goodsreceipt;

import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class FindCompletedLandedCostByPurchaseOrderCode implements Observer<FindEntityContext> {

  private DObLandedCostGeneralModelRep landedCostGeneralModelRep;

  public FindCompletedLandedCostByPurchaseOrderCode(
      DObLandedCostGeneralModelRep landedCostGeneralModelRep) {
    this.landedCostGeneralModelRep = landedCostGeneralModelRep;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(@NonNull FindEntityContext context) {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        context.entitiesOfInterestContainer().getMainEntity();
    DObLandedCostGeneralModel landedCostGeneralModel =
        landedCostGeneralModelRep.findByPurchaseOrderCodeAndCurrentStatesLike(
            goodsReceiptGeneralModel.getRefDocumentCode(),
            "%" + DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED + "%");
    if (landedCostGeneralModel != null) {
      context
          .entitiesOfInterestContainer()
          .setEntity(DObLandedCostGeneralModel.class, landedCostGeneralModel);
    }
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
