package com.ebs.dda.functions.executecommand.accounting.collection;

import com.ebs.dda.commands.accounting.journalentry.create.collection.DObJournalEntryItemsCollectionCreateCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObJournalEntryCreateCollectionValueObject;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CreateCollectionJournalEntry
    implements Observer<ExecuteActivateDependanciesContext> {

  private DObJournalEntryItemsCollectionCreateCommand command;

  public CreateCollectionJournalEntry(DObJournalEntryItemsCollectionCreateCommand command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      DObCollectionGeneralModel collectionGeneralModel =
          context.entitiesOfInterestContainer().getMainEntity();
      Observable<String> journalEntryCode =
          command.executeCommand(initValueObject(collectionGeneralModel));
      journalEntryCode.subscribe(userCode -> context.journalEntryUserCode(userCode));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObJournalEntryCreateCollectionValueObject initValueObject(
      DObCollectionGeneralModel collection) {
    DObJournalEntryCreateCollectionValueObject valueObject =
        new DObJournalEntryCreateCollectionValueObject();
    valueObject.setCollectionCode(collection.getUserCode());
    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
