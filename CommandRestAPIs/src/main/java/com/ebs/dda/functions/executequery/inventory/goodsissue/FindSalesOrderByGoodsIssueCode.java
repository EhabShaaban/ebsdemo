package com.ebs.dda.functions.executequery.inventory.goodsissue;

import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueDataGeneralModel;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueDataGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class FindSalesOrderByGoodsIssueCode
    implements Observer<ExecuteCommandContext<DObInventoryDocumentActivateValueObject>> {
  private final DObGoodsIssueDataGeneralModelRep goodsIssueDataGeneralModelRep;
  private final String BASED_ON_SALES_ORDER = "BASED_ON_SALES_ORDER";

  public FindSalesOrderByGoodsIssueCode(
      DObGoodsIssueDataGeneralModelRep goodsIssueDataGeneralModelRep) {
    this.goodsIssueDataGeneralModelRep = goodsIssueDataGeneralModelRep;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(
      @NonNull ExecuteCommandContext<DObInventoryDocumentActivateValueObject> context) {
    String userCode = context.valueObject().getUserCode();
    DObGoodsIssueDataGeneralModel goodsIssueDataGeneralModel =
        this.goodsIssueDataGeneralModelRep.findAllByUserCode(userCode).stream()
            .filter(
                item ->
                    BASED_ON_SALES_ORDER.equals(item.getGoodsIssueTypeCode())
                        && DObGoodsIssueStateMachine.ACTIVE_STATE.equals(item.getCurrentState()))
            .findFirst()
            .orElse(null);
    if (goodsIssueDataGeneralModel != null) {
      String referenceDocumentCode = goodsIssueDataGeneralModel.getReferenceDocumentCode();
      context.valueObject().setRefDocumentCode(referenceDocumentCode);
    }
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
