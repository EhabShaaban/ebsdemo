package com.ebs.dda.functions.lock.inventory.goodsreceipt;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.realization.concurrency.ConcurrentDataAccessManagersPool;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.IActivateLockEntitiesOfInterest;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceipt;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;
import com.ebs.dda.jpa.order.salesreturnorder.DObSalesReturnOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;

import java.util.List;

public class GoodsReceiptActivateLockManager implements IActivateLockEntitiesOfInterest {

  public static final String GR_PO = "GR_PO";
  private ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool;
  private UserAccountSecurityService userAccountSecurityService;
  private IBDKUser loggedInUser;

  public GoodsReceiptActivateLockManager(
      ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool,
      UserAccountSecurityService userAccountSecurityService) {
    this.concurrentDataAccessManagersPool = concurrentDataAccessManagersPool;
    this.userAccountSecurityService = userAccountSecurityService;
    this.loggedInUser = this.userAccountSecurityService.getLoggedInUser();
  }

  @Override
  public void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObGoodsReceiptGeneralModel goodsReceipt =
        entitiesOfInterestContainer.getEntity(DObGoodsReceiptGeneralModel.class);
    List<String> stockAvailabilities =
        entitiesOfInterestContainer.getEntityListOfString(CObStockAvailabilitiesGeneralModel.class);
    lockEntity(goodsReceipt.getUserCode(), DObGoodsReceipt.class.getName());
    lockRefDocument(goodsReceipt);
    for (String stockAvailabilityCode : stockAvailabilities) {
      lockEntity(stockAvailabilityCode, CObStockAvailability.class.getName());
    }
  }

  private void lockRefDocument(DObGoodsReceiptGeneralModel goodsReceipt) throws Exception {
    if (GR_PO.equals(goodsReceipt.getInventoryDocumentType())) {
      lockEntity(goodsReceipt.getRefDocumentCode(), DObPurchaseOrder.class.getName());
    } else {
      lockEntity(goodsReceipt.getRefDocumentCode(), DObSalesReturnOrder.class.getName());
    }
  }

  private void lockEntity(String userCode, String className) throws Exception {
    IConcurrentDataAccessManager concurrentDataAccessManager =
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(className);
    concurrentDataAccessManager.lock("*", userCode, this.loggedInUser);
  }

  @Override
  public void unlockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObGoodsReceiptGeneralModel goodsReceipt =
        entitiesOfInterestContainer.getEntity(DObGoodsReceiptGeneralModel.class);
    List<String> stockAvailabilities =
        entitiesOfInterestContainer.getEntityListOfString(CObStockAvailabilitiesGeneralModel.class);
    unlockEntity(goodsReceipt.getUserCode(), DObGoodsReceipt.class.getName());
    unLockRefDocument(goodsReceipt);
    for (String stockAvailabilityCode : stockAvailabilities) {
      unlockEntity(stockAvailabilityCode, CObStockAvailability.class.getName());
    }
  }

  private void unLockRefDocument(DObGoodsReceiptGeneralModel goodsReceipt) throws Exception {
    if (GR_PO.equals(goodsReceipt.getInventoryDocumentType())) {
      unlockEntity(goodsReceipt.getRefDocumentCode(), DObPurchaseOrder.class.getName());
    } else {
      unlockEntity(goodsReceipt.getRefDocumentCode(), DObSalesReturnOrder.class.getName());
    }
  }

  private void unlockEntity(String userCode, String className) throws Exception {
    IConcurrentDataAccessManager concurrentDataAccessManager =
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(className);
    concurrentDataAccessManager.checkIfResourceIsLockedByUser("*", userCode, loggedInUser);
    concurrentDataAccessManager.unlock("*", userCode, this.loggedInUser);
  }
}
