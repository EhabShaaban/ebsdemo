package com.ebs.dda.functions.executecommand.accounting.salesinvoice;

import com.ebs.dda.commands.accounting.invoice.IObInvoiceTaxCopyCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.IObInvoiceTaxesValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CopySalesInvoiceTaxes implements Observer<ExecuteActivateDependanciesContext> {

  private IObInvoiceTaxCopyCommand command;

  public CopySalesInvoiceTaxes(IObInvoiceTaxCopyCommand command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          context.entitiesOfInterestContainer().getMainEntity();
      command.executeCommand(initValueObject(salesInvoiceGeneralModel));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private IObInvoiceTaxesValueObject initValueObject(DObSalesInvoiceGeneralModel salesInvoice) {
    DObSalesInvoice invoice = new DObSalesInvoice();
    invoice.setId(salesInvoice.getId());
    invoice.setUserCode(salesInvoice.getUserCode());
    IObInvoiceTaxesValueObject valueObject = new IObInvoiceTaxesValueObject(invoice);
    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
