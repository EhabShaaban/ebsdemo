package com.ebs.dda.functions.serialize;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SerializeActivateSuccessResponse
    implements Observer<SerializeActivateResponseContext> {

  private SerializionUtils serializer;

  public SerializeActivateSuccessResponse(SerializionUtils serializer) {
    this.serializer = serializer;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(SerializeActivateResponseContext context) {
    String successResponse =
        serializer.serializeActivateSuccessResponse(context.journalEntryUserCode(), context.successMessage());
    context.response(successResponse);
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
