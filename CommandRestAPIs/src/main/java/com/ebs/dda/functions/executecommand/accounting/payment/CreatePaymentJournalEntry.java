package com.ebs.dda.functions.executecommand.accounting.payment;

import com.ebs.dda.commands.accounting.journalentry.create.paymentrequest.DObJournalEntryCreatePaymentRequestCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObPaymentRequestJournalEntry;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CreatePaymentJournalEntry implements Observer<ExecuteActivateDependanciesContext> {

  private DObJournalEntryCreatePaymentRequestCommand command;

  public CreatePaymentJournalEntry(DObJournalEntryCreatePaymentRequestCommand command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      DObPaymentRequestGeneralModel paymentRequestGeneralModel =
          context.entitiesOfInterestContainer().getMainEntity();
      Observable<String> journalEntryCode =
          command.executeCommand(initValueObject(paymentRequestGeneralModel, context));
      journalEntryCode.subscribe(userCode -> context.journalEntryUserCode(userCode));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObJournalEntryCreateValueObject initValueObject(DObPaymentRequestGeneralModel payment, ExecuteActivateDependanciesContext context) {
    DObJournalEntryCreateValueObject valueObject = new DObJournalEntryCreateValueObject();
    valueObject.setUserCode(payment.getUserCode());
    String journalEntryCode = context.entitiesOfInterestContainer().getStringValue(DObPaymentRequestJournalEntry.class);
    valueObject.setJournalEntryCode(journalEntryCode);
    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
