package com.ebs.dda.functions.executequery.accounting.journalbalance;

import com.ebs.dda.codegenerator.JournalBalanceCodeGenerator;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceCodeGenerationValueObject;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FindJournalBalance implements Observer<FindEntityContext> {

  private CObJournalBalanceGeneralModelRep repository;
  private JournalBalanceCodeGenerator journalBalanceCodeGenerator;

  public FindJournalBalance(CObJournalBalanceGeneralModelRep repository,
      JournalBalanceCodeGenerator journalBalanceCodeGenerator) {
    this.repository = repository;
    this.journalBalanceCodeGenerator = journalBalanceCodeGenerator;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(FindEntityContext context) {
    JournalBalanceCodeGenerationValueObject valueObject = context.entitiesOfInterestContainer()
        .getEntity(JournalBalanceCodeGenerationValueObject.class);

    String journalBalanceCode = journalBalanceCodeGenerator.generateUserCode(valueObject);

    CObJournalBalanceGeneralModel journalBalance = repository.findOneByUserCode(journalBalanceCode);

    context.entitiesOfInterestContainer().setEntity(CObJournalBalanceGeneralModel.class,
        journalBalance);
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
