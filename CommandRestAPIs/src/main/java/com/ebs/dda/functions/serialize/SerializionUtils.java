package com.ebs.dda.functions.serialize;

import com.ebs.dda.jpa.IValueObject;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class SerializionUtils {

  public static final String CODE_RESPONSE_KEY = "code";
  public static final String RESPONSE_STATUS = "status";
  public static final String USER_CODE_KEY = "userCode";
  public static final String SUCCESS_RESPONSE_KEY = "SUCCESS";
  public static final String FAIL_RESPONSE_KEY = "FAIL";

  public String serializeActivateSuccessResponse(String userCode, String successMsg) {
    JsonObject successResponse = new JsonObject();
    successResponse.addProperty(RESPONSE_STATUS, SUCCESS_RESPONSE_KEY);
    successResponse.addProperty(USER_CODE_KEY, userCode);
    successResponse.addProperty(CODE_RESPONSE_KEY, successMsg);
    return successResponse.toString();
  }

  public <V extends IValueObject> V deserializeActivateValueObject(String activateData,
      Class<V> valueObjectClass, String objectCode) {
    Gson gson = new Gson();
    V valueObject = (V) gson.fromJson(activateData, valueObjectClass);
    valueObject.userCode(objectCode);
    return valueObject;
  }
}
