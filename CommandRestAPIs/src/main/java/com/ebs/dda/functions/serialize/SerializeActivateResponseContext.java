package com.ebs.dda.functions.serialize;

public interface SerializeActivateResponseContext {

  String objectCode();

  void response(String response);

  String successMessage();

  String journalEntryUserCode();
}
