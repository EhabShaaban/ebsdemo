package com.ebs.dda.functions.serialize;

import com.ebs.dda.jpa.IValueObject;

public interface DeserializeValueObjectContext<V extends IValueObject> {

  Class<V> valueObjectClass();

  String activateData();

  String objectCode();

  void valueObject(V valueObject);

}
