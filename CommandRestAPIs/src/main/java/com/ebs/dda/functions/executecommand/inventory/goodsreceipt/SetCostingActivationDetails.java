package com.ebs.dda.functions.executecommand.inventory.goodsreceipt;

import com.ebs.dda.commands.accounting.costing.DObCostingSetActivationDetailsCommand;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class SetCostingActivationDetails implements Observer<SerializeActivateResponseContext> {
  private DObCostingSetActivationDetailsCommand costingSetActivationDetailsCommand;

  public SetCostingActivationDetails(
      DObCostingSetActivationDetailsCommand costingSetActivationDetailsCommand) {
    this.costingSetActivationDetailsCommand = costingSetActivationDetailsCommand;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(@NonNull SerializeActivateResponseContext context) {
    String journalEntryCode = context.journalEntryUserCode();
    try {
      costingSetActivationDetailsCommand.executeCommand(journalEntryCode);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
