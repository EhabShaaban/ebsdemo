package com.ebs.dda.functions.lock.accounting.paymentrequest;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.jpa.accounting.accountingnotes.DObAccountingNote;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;

public class PaymentAgainstCreditNoteLockActivateEntitiesOfInterest extends PaymentLockActivateEntitiesOfInterest {

    public PaymentAgainstCreditNoteLockActivateEntitiesOfInterest(EntityLockCommand<DObPaymentRequest> paymentLockCommand,
                                                                  EntityLockCommand<DObAccountingNote> creditNoteLockCommand,
                                                                  CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory) {
        super(paymentLockCommand, creditNoteLockCommand, journalBalanceLockCommandBeanFactory);
    }

}
