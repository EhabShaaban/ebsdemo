package com.ebs.dda.functions.executequery.accounting.payment;

import com.ebs.dac.foundation.exceptions.data.MissingFieldsException;
import com.ebs.dda.accounting.paymentrequest.statemachines.DObPaymentRequestStateMachine;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceCodeGenerationValueObject;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceTypeEnum.JournalBalanceType;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConstructPaymentJournalBalanceCodeGenerationValueObject
        implements Observer<FindEntityContext> {

    private static final String ACCOUNT_NUMBER_CURRANCY_SPERATOR = " - ";
    private final IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep;
    private final IObPaymentRequestPaymentDetailsGeneralModelRep detailsGeneralModelRep;

    public ConstructPaymentJournalBalanceCodeGenerationValueObject(
            IObPaymentRequestCompanyDataGeneralModelRep companyDataGeneralModelRep,
            IObPaymentRequestPaymentDetailsGeneralModelRep detailsGeneralModelRep) {
        this.companyDataGeneralModelRep = companyDataGeneralModelRep;
        this.detailsGeneralModelRep = detailsGeneralModelRep;
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(FindEntityContext context) {

        DObPaymentRequestGeneralModel paymentRequestGeneralModel = context.entitiesOfInterestContainer().getMainEntity();
        IObPaymentRequestCompanyDataGeneralModel companyDataGeneralModel =
                companyDataGeneralModelRep.findOneByRefInstanceId(paymentRequestGeneralModel.getId());

        IObPaymentRequestPaymentDetailsGeneralModel detailsGeneralModel =
                detailsGeneralModelRep.findOneByRefInstanceId(paymentRequestGeneralModel.getId());

        JournalBalanceCodeGenerationValueObject valueObject =
                initValueObject(companyDataGeneralModel, detailsGeneralModel);

        context.entitiesOfInterestContainer().setEntity(JournalBalanceCodeGenerationValueObject.class,
                valueObject);
    }

    private JournalBalanceCodeGenerationValueObject initValueObject(
            IObPaymentRequestCompanyDataGeneralModel companyDataGeneralModel, IObPaymentRequestPaymentDetailsGeneralModel detailsGeneralModel) {

        JournalBalanceCodeGenerationValueObject valueObject =
                new JournalBalanceCodeGenerationValueObject();

        valueObject.setBusinessUnitCode(companyDataGeneralModel.getPurchaseUnitCode());
        valueObject.setCompanyCode(companyDataGeneralModel.getCompanyCode());
        valueObject.setCurrencyCode(detailsGeneralModel.getCurrencyCode());
        valueObject.setTreasuryCode(detailsGeneralModel.getTreasuryCode());
        valueObject.setBankCode(detailsGeneralModel.getBankAccountCode());
        valueObject
                .setBankAccountNumber(getBankAccountNumber(detailsGeneralModel.getBankAccountNumber()));
        valueObject.setObjectTypeCode(getType(detailsGeneralModel.getPaymentForm()));
        return valueObject;
    }

    private String getBankAccountNumber(String accountNumber) {
        if (accountNumber == null) {
            return null;
        }
        return accountNumber.split(ACCOUNT_NUMBER_CURRANCY_SPERATOR)[0];
    }

    private String getType(String paymentForm) {
        if (isBank(paymentForm)) {
            return JournalBalanceType.BANK.name();
        }

        if (isCash(paymentForm)) {
            return JournalBalanceType.TREASURY.name();
        }

        throw new UnsupportedOperationException(paymentForm + " is not supported");
    }

    private boolean isBank(String paymentForm) {
        return PaymentFormEnum.PaymentForm.BANK.name().equals(paymentForm);
    }

    private boolean isCash(String paymentForm) {
        return PaymentFormEnum.PaymentForm.CASH.name().equals(paymentForm);
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }

}
