package com.ebs.dda.functions.executecommand.accounting.monetarynote;

import com.ebs.dda.commands.accounting.notesreceivables.DObNotesReceivableAccountDeterminationCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DetermineNotesReceivableAccount
    implements Observer<ExecuteActivateDependanciesContext> {

  private DObNotesReceivableAccountDeterminationCommand accountDeterminationCommand;

  public DetermineNotesReceivableAccount(
      DObNotesReceivableAccountDeterminationCommand accountDeterminationCommand) {
    this.accountDeterminationCommand = accountDeterminationCommand;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
          context.entitiesOfInterestContainer().getMainEntity();
      accountDeterminationCommand.executeCommand(notesReceivablesGeneralModel.getUserCode());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
