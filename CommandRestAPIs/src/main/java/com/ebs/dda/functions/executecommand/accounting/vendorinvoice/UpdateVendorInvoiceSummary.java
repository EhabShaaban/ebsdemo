package com.ebs.dda.functions.executecommand.accounting.vendorinvoice;

import com.ebs.dda.commands.accounting.vendorinvoice.IObVendorInvoiceSummaryUpdateCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UpdateVendorInvoiceSummary implements Observer<ExecuteActivateDependanciesContext> {

    private IObVendorInvoiceSummaryUpdateCommand command;

    public UpdateVendorInvoiceSummary(IObVendorInvoiceSummaryUpdateCommand command) {
        this.command = command;
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(ExecuteActivateDependanciesContext context) {

        try {
            DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
                    context.entitiesOfInterestContainer().getMainEntity();
            command.executeCommand(vendorInvoiceGeneralModel.getUserCode());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }
}
