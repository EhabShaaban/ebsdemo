package com.ebs.dda.functions.lock;

import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;

public interface IActivateLockEntitiesOfInterest {

  void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer) throws Exception;

  void unlockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer) throws Exception;
}
