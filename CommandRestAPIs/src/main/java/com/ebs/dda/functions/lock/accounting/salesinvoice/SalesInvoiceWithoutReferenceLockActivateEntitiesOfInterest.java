package com.ebs.dda.functions.lock.accounting.salesinvoice;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.IActivateLockEntitiesOfInterest;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;

public class SalesInvoiceWithoutReferenceLockActivateEntitiesOfInterest
    implements IActivateLockEntitiesOfInterest {

  private EntityLockCommand<DObSalesInvoice> salesInvoiceLockCommand;

  public SalesInvoiceWithoutReferenceLockActivateEntitiesOfInterest(
      EntityLockCommand<DObSalesInvoice> salesInvoiceLockCommand) {
    this.salesInvoiceLockCommand = salesInvoiceLockCommand;
  }

  @Override
  public void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoice =
        entitiesOfInterestContainer.getEntity(DObSalesInvoiceGeneralModel.class);
    salesInvoiceLockCommand.lockAllSections(salesInvoice.getUserCode());
  }

  @Override
  public void unlockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoice =
        entitiesOfInterestContainer.getEntity(DObSalesInvoiceGeneralModel.class);
    salesInvoiceLockCommand.unlockAllSections(salesInvoice.getUserCode());
  }
}
