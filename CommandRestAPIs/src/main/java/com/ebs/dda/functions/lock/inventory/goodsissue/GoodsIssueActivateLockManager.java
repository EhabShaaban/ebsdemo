package com.ebs.dda.functions.lock.inventory.goodsissue;

import com.ebs.dac.foundation.apis.concurrency.IConcurrentDataAccessManager;
import com.ebs.dac.foundation.apis.security.IBDKUser;
import com.ebs.dac.foundation.realization.concurrency.ConcurrentDataAccessManagersPool;
import com.ebs.dac.security.services.UserAccountSecurityService;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.IActivateLockEntitiesOfInterest;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssue;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailability;

import java.util.List;

public class GoodsIssueActivateLockManager implements IActivateLockEntitiesOfInterest {
  private ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool;
  private UserAccountSecurityService userAccountSecurityService;
  private IBDKUser loggedInUser;

  public GoodsIssueActivateLockManager(
      ConcurrentDataAccessManagersPool concurrentDataAccessManagersPool,
      UserAccountSecurityService userAccountSecurityService) {
    this.concurrentDataAccessManagersPool = concurrentDataAccessManagersPool;
    this.userAccountSecurityService = userAccountSecurityService;
    this.loggedInUser = this.userAccountSecurityService.getLoggedInUser();
  }

  @Override
  public void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObGoodsIssueGeneralModel goodsIssue = entitiesOfInterestContainer.getMainEntity();
    List<String> stockAvailabilityCodes =
        entitiesOfInterestContainer.getEntityListOfString(CObStockAvailabilitiesGeneralModel.class);
    lockEntity(goodsIssue.getUserCode(), DObGoodsIssue.class.getName());
    for (String code : stockAvailabilityCodes) {
      lockEntity(code, CObStockAvailability.class.getName());
    }
  }

  private void lockEntity(String userCode, String className) throws Exception {
    IConcurrentDataAccessManager concurrentDataAccessManager =
        concurrentDataAccessManagersPool.getConcurrentDataAccessManager(className);
    concurrentDataAccessManager.lock("*", userCode, this.loggedInUser);
  }

  private void unlockEntity(String userCode, String className) throws Exception {
    IConcurrentDataAccessManager concurrentDataAccessManager =
            concurrentDataAccessManagersPool.getConcurrentDataAccessManager(className);
    concurrentDataAccessManager.checkIfResourceIsLockedByUser("*", userCode, loggedInUser);
    concurrentDataAccessManager.unlock("*", userCode, this.loggedInUser);
  }

  @Override
  public void unlockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObGoodsIssueGeneralModel goodsIssue = entitiesOfInterestContainer.getMainEntity();
    List<String> stockAvailabilityCodes =
            entitiesOfInterestContainer.getEntityListOfString(CObStockAvailabilitiesGeneralModel.class);
    unlockEntity(goodsIssue.getUserCode(), DObGoodsIssue.class.getName());
    for (String code : stockAvailabilityCodes) {
      unlockEntity(code, CObStockAvailability.class.getName());
    }
  }
}
