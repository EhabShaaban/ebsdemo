package com.ebs.dda.functions.activate;

public interface ActivateContext {

  String objectCode();

  String activateData();

  String serializeResponse();
}
