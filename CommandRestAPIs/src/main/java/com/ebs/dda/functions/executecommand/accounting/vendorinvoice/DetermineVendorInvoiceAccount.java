package com.ebs.dda.functions.executecommand.accounting.vendorinvoice;

import com.ebs.dda.commands.accounting.vendorinvoice.DObVendorInvoiceAccountDeterminationCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceActivateValueObject;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DetermineVendorInvoiceAccount implements Observer<ExecuteActivateDependanciesContext> {

    private final DObVendorInvoiceAccountDeterminationCommand accountDeterminationCommand;

    public DetermineVendorInvoiceAccount(DObVendorInvoiceAccountDeterminationCommand accountDeterminationCommand) {
        this.accountDeterminationCommand = accountDeterminationCommand;
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(ExecuteActivateDependanciesContext context) {

        try {
            DObVendorInvoiceActivateValueObject valueObject = initValueObject(context.entitiesOfInterestContainer().getMainEntity());
            accountDeterminationCommand.executeCommand(valueObject);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private DObVendorInvoiceActivateValueObject initValueObject(DObVendorInvoiceGeneralModel payment) {
        DObVendorInvoiceActivateValueObject valueObject = new DObVendorInvoiceActivateValueObject();
        valueObject.setVendorInvoiceCode(payment.getUserCode());
        return valueObject;
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }

}
