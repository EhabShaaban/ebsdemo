package com.ebs.dda.functions.executecommand.inventory.goodsreceipt;

import com.ebs.dda.commands.order.purchaseorder.DObPurchaseOrderPostGoodsReceiptCommand;
import com.ebs.dda.commands.order.salesreturnorder.DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class UpdateGoodsReceiptReferenceDocument
    implements Observer<ExecuteCommandContext<DObInventoryDocumentActivateValueObject>> {
  public static final String GR_PO = "GR_PO";
  private DObPurchaseOrderPostGoodsReceiptCommand purchaseOrderPostGoodsReceiptCommand;
  private DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand
      salesReturnOrderUpdateGoodsReceiptActivatedStateCommand;

  public UpdateGoodsReceiptReferenceDocument(
      DObPurchaseOrderPostGoodsReceiptCommand purchaseOrderPostGoodsReceiptCommand,
      DObSalesReturnOrderUpdateGoodsReceiptActivatedStateCommand
          salesReturnOrderUpdateGoodsReceiptActivatedStateCommand) {
    this.purchaseOrderPostGoodsReceiptCommand = purchaseOrderPostGoodsReceiptCommand;
    this.salesReturnOrderUpdateGoodsReceiptActivatedStateCommand = salesReturnOrderUpdateGoodsReceiptActivatedStateCommand;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(
      @NonNull ExecuteCommandContext<DObInventoryDocumentActivateValueObject> context) {
    DObInventoryDocumentActivateValueObject valueObject = context.valueObject();
    try {
      if (isGoodsReceiptOfTypePurchaseOrder(valueObject))
        purchaseOrderPostGoodsReceiptCommand.executeCommand(valueObject);
      else
        salesReturnOrderUpdateGoodsReceiptActivatedStateCommand.executeCommand(valueObject);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private boolean isGoodsReceiptOfTypePurchaseOrder(DObInventoryDocumentActivateValueObject valueObject) {
    return GR_PO.equals(valueObject.getInventoryType());
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
