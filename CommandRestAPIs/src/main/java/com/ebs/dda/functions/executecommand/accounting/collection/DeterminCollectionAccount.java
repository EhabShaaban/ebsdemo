package com.ebs.dda.functions.executecommand.accounting.collection;

import com.ebs.dda.commands.accounting.collection.DObCollectionAccountDeterminationCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.collection.DObCollectionAccountDetermineValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DeterminCollectionAccount implements Observer<ExecuteActivateDependanciesContext> {

  private DObCollectionAccountDeterminationCommand accountDeterminationCommand;

  public DeterminCollectionAccount(DObCollectionAccountDeterminationCommand accountDeterminationCommand) {
    this.accountDeterminationCommand = accountDeterminationCommand;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      DObCollectionAccountDetermineValueObject valueObject = initValueObject(context.entitiesOfInterestContainer().getMainEntity());
      accountDeterminationCommand.executeCommand(valueObject);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObCollectionAccountDetermineValueObject initValueObject(DObCollectionGeneralModel collection) {
    DObCollectionAccountDetermineValueObject valueObject = new DObCollectionAccountDetermineValueObject();
    valueObject.setCollectionCode(collection.getUserCode());
    valueObject.setCollectionType(collection.getRefDocumentTypeCode());
    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
