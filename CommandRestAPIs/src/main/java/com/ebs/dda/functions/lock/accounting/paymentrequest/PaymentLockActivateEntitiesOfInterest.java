package com.ebs.dda.functions.lock.accounting.paymentrequest;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.AbstractActivateLockEntitesOfInterest;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;


public abstract class PaymentLockActivateEntitiesOfInterest
    extends AbstractActivateLockEntitesOfInterest {

  private EntityLockCommand<DObPaymentRequest> paymentLockCommand;
  private EntityLockCommand<?> refDocumentLockCommand;
  private CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory;
  private EntityLockCommand<CObJournalBalance> journalBalanceLockCommand;

  protected PaymentLockActivateEntitiesOfInterest(
      EntityLockCommand<DObPaymentRequest> paymentLockCommand,
      EntityLockCommand<?> refDocumentLockCommand,
      CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory
      ) {
    this.paymentLockCommand = paymentLockCommand;
    this.refDocumentLockCommand = refDocumentLockCommand;
    this.journalBalanceLockCommandBeanFactory = journalBalanceLockCommandBeanFactory;
  }

  @Override
  public void lock(EntitiesOfInterestContainer entitiesOfInterestContainer) throws Exception {
    DObPaymentRequestGeneralModel payment =
        entitiesOfInterestContainer.getEntity(DObPaymentRequestGeneralModel.class);
    lockEntity(paymentLockCommand, payment.getUserCode());
    lockRefDocument(payment);
    lockJournalBalance(entitiesOfInterestContainer, payment);
  }

  private void lockRefDocument(DObPaymentRequestGeneralModel payment) throws Exception {
    if (payment.getReferenceDocumentCode() != null) {
      lockEntity(refDocumentLockCommand, payment.getReferenceDocumentCode());
    }
  }

  private void lockJournalBalance(EntitiesOfInterestContainer entitiesOfInterestContainer, DObPaymentRequestGeneralModel payment) throws Exception {
    CObJournalBalanceGeneralModel journalBalance =
            entitiesOfInterestContainer.getEntity(CObJournalBalanceGeneralModel.class);
    //TODO: remove when move bank/treasury to creation step
    if(journalBalance != null) {
      journalBalanceLockCommand = journalBalanceLockCommandBeanFactory
              .getJournalBalanceLockCommand(payment.getPaymentForm());
      lockEntity(journalBalanceLockCommand, journalBalance.getUserCode());
    }
  }
}
