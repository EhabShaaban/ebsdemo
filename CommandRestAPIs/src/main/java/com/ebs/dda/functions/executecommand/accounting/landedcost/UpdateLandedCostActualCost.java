package com.ebs.dda.functions.executecommand.accounting.landedcost;

import com.ebs.dac.dbo.jpa.entities.businessobjects.CodedBusinessObject;
import com.ebs.dda.accounting.landedcost.factories.DObLandedCostUpdateActualCostValueObjectFactory;
import com.ebs.dda.commands.accounting.landedcost.DObLandedCostUpdateActualCostCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostUpdateActualCostValueObject;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UpdateLandedCostActualCost implements Observer<ExecuteActivateDependanciesContext> {

  private DObLandedCostUpdateActualCostCommand command;
  private DObLandedCostUpdateActualCostValueObjectFactory valueObjectFactory;

  public UpdateLandedCostActualCost(DObLandedCostUpdateActualCostCommand command,
      DObLandedCostUpdateActualCostValueObjectFactory valueObjectFactory) {
    this.command = command;
    this.valueObjectFactory = valueObjectFactory;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      CodedBusinessObject<?> businessObject = context.entitiesOfInterestContainer().getMainEntity();

      DObLandedCostUpdateActualCostValueObject valueObject =
          valueObjectFactory.create(businessObject.getSysName(), businessObject.getUserCode());

      command.executeCommand(valueObject);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
