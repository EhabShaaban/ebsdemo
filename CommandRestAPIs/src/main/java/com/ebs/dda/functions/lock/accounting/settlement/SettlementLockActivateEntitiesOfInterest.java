package com.ebs.dda.functions.lock.accounting.settlement;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.AbstractActivateLockEntitesOfInterest;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.DObSettlement;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;

import java.util.List;

public class SettlementLockActivateEntitiesOfInterest
    extends AbstractActivateLockEntitesOfInterest {

    private EntityLockCommand<DObSettlement> settlementLockCmd;
    private CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory;

    public SettlementLockActivateEntitiesOfInterest(
        EntityLockCommand<DObSettlement> settlementLockCmd,
        CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory) {
        this.settlementLockCmd = settlementLockCmd;
        this.journalBalanceLockCommandBeanFactory = journalBalanceLockCommandBeanFactory;
    }

    @Override
    protected void lock(EntitiesOfInterestContainer entitiesOfInterestContainer)
        throws Exception {

        DObSettlementGeneralModel settlement = entitiesOfInterestContainer.getMainEntity();
        lockEntity(settlementLockCmd, settlement.getUserCode());

        List<CObJournalBalanceGeneralModel> balances =
            entitiesOfInterestContainer.getEntityList(CObJournalBalanceGeneralModel.class);
        for (CObJournalBalanceGeneralModel balance : balances) {
            lockEntity(getBalanceLockCmd(balance), balance.getUserCode());
        }
    }

    private EntityLockCommand<?> getBalanceLockCmd(CObJournalBalanceGeneralModel balance) {
        return journalBalanceLockCommandBeanFactory
            .getJournalBalanceLockCommand(balance.getObjectTypeCode());
    }

}
