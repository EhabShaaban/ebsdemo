package com.ebs.dda.functions.executecommand.accounting.payment;

import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentUpdateVendorInvoiceRemainingCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UpdateVendorInvoiceRemaining implements Observer<ExecuteActivateDependanciesContext> {

    private DObPaymentUpdateVendorInvoiceRemainingCommand command;

    public UpdateVendorInvoiceRemaining(DObPaymentUpdateVendorInvoiceRemainingCommand command) {
        this.command = command;
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(ExecuteActivateDependanciesContext context) {

        try {
            DObPaymentRequestActivateValueObject valueObject = initValueObject(context.entitiesOfInterestContainer().getMainEntity());
            command.executeCommand(valueObject);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private DObPaymentRequestActivateValueObject initValueObject(DObPaymentRequestGeneralModel payment) {
        DObPaymentRequestActivateValueObject valueObject = new DObPaymentRequestActivateValueObject();
        valueObject.setPaymentRequestCode(payment.getUserCode());
        return valueObject;
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }

}
