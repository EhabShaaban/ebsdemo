package com.ebs.dda.functions.lock.accounting.paymentrequest;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.order.DObOrderDocument;

public class PaymentGeneralAgainstPurchaseOrderLockActivateEntitiesOfInterest
    extends PaymentLockActivateEntitiesOfInterest {

  private EntityLockCommand<DObLandedCost> landedCostLockCommand;

  public PaymentGeneralAgainstPurchaseOrderLockActivateEntitiesOfInterest(
      EntityLockCommand<DObPaymentRequest> paymentLockCommand,
      EntityLockCommand<DObOrderDocument> purchaseOrderLockCommand,
      EntityLockCommand<DObLandedCost> landedCostLockCommand,
      CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory) {
    super(paymentLockCommand, purchaseOrderLockCommand, journalBalanceLockCommandBeanFactory);

    this.landedCostLockCommand = landedCostLockCommand;
  }

  @Override
  public void lock(EntitiesOfInterestContainer entitiesOfInterestContainer) throws Exception {
    super.lock(entitiesOfInterestContainer);

    DObLandedCostGeneralModel landedCost =
        entitiesOfInterestContainer.getEntity(DObLandedCostGeneralModel.class);
    if (landedCost != null) {
      lockEntity(landedCostLockCommand, landedCost.getUserCode());
    }
  }

}
