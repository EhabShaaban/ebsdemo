package com.ebs.dda.functions.executecommand;

import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;

public interface ExecuteActivateDependanciesContext {
  EntitiesOfInterestContainer entitiesOfInterestContainer();

  void journalEntryUserCode(String userCode);

  <V> V valueObject();

}
