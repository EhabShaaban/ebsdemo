package com.ebs.dda.functions.executecommand.accounting.salesinvoice;

import com.ebs.dda.commands.order.salesorder.DObSalesOrderInvoicedCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderUpdateStateValueObject;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CreateSalesOrderInvoiced implements Observer<ExecuteActivateDependanciesContext> {

  private DObSalesOrderInvoicedCommand command;

  public CreateSalesOrderInvoiced(DObSalesOrderInvoicedCommand command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          context.entitiesOfInterestContainer().getMainEntity();
      command.executeCommand(initValueObject(salesInvoiceGeneralModel));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObSalesOrderUpdateStateValueObject initValueObject(
      DObSalesInvoiceGeneralModel salesInvoice) {
    DObSalesOrderUpdateStateValueObject valueObject = new DObSalesOrderUpdateStateValueObject();
    valueObject.setSalesOrderCode(salesInvoice.getSalesOrder());
    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
