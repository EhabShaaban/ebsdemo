package com.ebs.dda.functions.executequery.inventory.goodsreceipt;

import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.StockAvailabilityCodeGenerationValueObject;
import com.ebs.dda.repositories.inventory.goodsreceipt.IObGoodsReceiptItemQuantityGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;

public class FindStockAvailabilitiesForGoodsReceipt implements Observer<FindEntityContext> {

  private final IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantityGeneralModelRep;
  private final StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator;
  private List<String> stockAvailabilityCodes = new ArrayList<>();

  public FindStockAvailabilitiesForGoodsReceipt(
      IObGoodsReceiptItemQuantityGeneralModelRep goodsReceiptItemQuantityGeneralModelRep,
      StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator) {
    this.goodsReceiptItemQuantityGeneralModelRep = goodsReceiptItemQuantityGeneralModelRep;
    this.stockAvailabilityCodeGenerator = stockAvailabilityCodeGenerator;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(@NonNull FindEntityContext context) {
    List<IObGoodsReceiptItemQuantityGeneralModel> goodsReceiptItems = getGoodsReceiptItems(context);
    for (IObGoodsReceiptItemQuantityGeneralModel item : goodsReceiptItems) {
      String userCode = getStockAvailabilityCodeFromItem(item);
      stockAvailabilityCodes.add(userCode);
    }
    context
        .entitiesOfInterestContainer()
        .setEntity(CObStockAvailabilitiesGeneralModel.class, stockAvailabilityCodes);
  }

  private String getStockAvailabilityCodeFromItem(IObGoodsReceiptItemQuantityGeneralModel item) {
    StockAvailabilityCodeGenerationValueObject valueObject =
        new StockAvailabilityCodeGenerationValueObject().fromItemQuantityGeneralModel(item);
    return stockAvailabilityCodeGenerator.generateUserCode(valueObject);
  }

  private List<IObGoodsReceiptItemQuantityGeneralModel> getGoodsReceiptItems(
      FindEntityContext context) {
    DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
        context.entitiesOfInterestContainer().getMainEntity();
    return goodsReceiptItemQuantityGeneralModelRep.findAllByUserCode(
        goodsReceiptGeneralModel.getUserCode());
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
