package com.ebs.dda.functions.executecommand.accounting.settlement;

import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceDecreaseBalanceCommand;
import com.ebs.dda.commands.accounting.journalbalance.CObJournalBalanceIncreaseBalanceCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.utils.Pair;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceUpdateValueObject;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.chartofaccount.enums.AccountingEntry;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UpdateSettlementJournalBalance implements Observer<ExecuteActivateDependanciesContext> {

    private final CObJournalBalanceIncreaseBalanceCommand increaseBalanceCommand;
    private final CObJournalBalanceDecreaseBalanceCommand decreaseBalanceCommand;

    public UpdateSettlementJournalBalance(
        CObJournalBalanceIncreaseBalanceCommand journalBalanceIncreaseBalanceCommand,
        CObJournalBalanceDecreaseBalanceCommand journalBalanceDecreaseBalanceCommand) {

        this.increaseBalanceCommand = journalBalanceIncreaseBalanceCommand;
        this.decreaseBalanceCommand = journalBalanceDecreaseBalanceCommand;
    }

    @Override
    public void onSubscribe(Disposable disposable) {}

    @Override
    public void onNext(ExecuteActivateDependanciesContext context) {
        context
            .entitiesOfInterestContainer()
            .getEntityList(Pair.class)
            .forEach(accDetailsAndBalancePair -> {
                try {
                    executeCmd(accDetailsAndBalancePair);
                } catch (Exception e) {
                    throw new RuntimeException("Error while updating JB by Settlement");
                }
            });
    }

    private void executeCmd(Pair<IObSettlementAccountDetailsGeneralModel, JournalBalanceUpdateValueObject> accDetailsAndBalancePair)
        throws Exception {

        IObSettlementAccountDetailsGeneralModel accDetails = accDetailsAndBalancePair.getFirst();
        JournalBalanceUpdateValueObject valueObject = accDetailsAndBalancePair.getSecond();

        if (accDetails.getAccountingEntry().equals(AccountingEntry.DEBIT.name())) {
            increaseBalanceCommand.executeCommand(valueObject);
        }

        if (accDetails.getAccountingEntry().equals(AccountingEntry.CREDIT.name())) {
            decreaseBalanceCommand.executeCommand(valueObject);
        }
    }

    @Override
    public void onError(Throwable throwable) {}

    @Override
    public void onComplete() {}
}
