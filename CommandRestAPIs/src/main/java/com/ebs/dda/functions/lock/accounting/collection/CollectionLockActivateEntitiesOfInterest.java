package com.ebs.dda.functions.lock.accounting.collection;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.IActivateLockEntitiesOfInterest;
import com.ebs.dda.jpa.accounting.collection.DObCollection;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalance;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;

public class CollectionLockActivateEntitiesOfInterest implements IActivateLockEntitiesOfInterest {

  private EntityLockCommand<DObCollection> collectionLockCommand;
  private EntityLockCommand<?> refDocumentLockCommand;
  private CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory;
  private EntityLockCommand<CObJournalBalance> journalBalanceLockCommand;

  public CollectionLockActivateEntitiesOfInterest(
      EntityLockCommand<DObCollection> collectionLockCommand,
      EntityLockCommand<?> refDocumentLockCommand,
      CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory) {

    this.collectionLockCommand = collectionLockCommand;
    this.refDocumentLockCommand = refDocumentLockCommand;
    this.journalBalanceLockCommandBeanFactory = journalBalanceLockCommandBeanFactory;
  }

  @Override
  public void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {

    DObCollectionGeneralModel collection = entitiesOfInterestContainer.getMainEntity();
    CObJournalBalanceGeneralModel journalBalance =
        entitiesOfInterestContainer.getEntity(CObJournalBalanceGeneralModel.class);

    try {
      collectionLockCommand.lockAllSections(collection.getUserCode());
      refDocumentLockCommand.lockAllSections(collection.getRefDocumentCode());

      journalBalanceLockCommand = journalBalanceLockCommandBeanFactory
          .getJournalBalanceLockCommand(collection.getCollectionMethod());

      if(journalBalance !=null) {
        journalBalanceLockCommand.lockAllSections(journalBalance.getUserCode());
      }

    } catch (Exception e) {
      unlock(collection, journalBalance);
      throw e;
    }
  }

  private void unlock(DObCollectionGeneralModel collection,
      CObJournalBalanceGeneralModel journalBalance) throws Exception {

    if (collectionLockCommand.isAllSectionsLocked(collection.getUserCode())) {
      collectionLockCommand.unlockAllSections(collection.getUserCode());
    }

    if (refDocumentLockCommand.isAllSectionsLocked(collection.getRefDocumentCode())) {
      refDocumentLockCommand.unlockAllSections(collection.getRefDocumentCode());
    }

    if (journalBalanceLockCommand.isAllSectionsLocked(journalBalance.getUserCode())) {
      journalBalanceLockCommand.unlockAllSections(journalBalance.getUserCode());
    }
  }

  @Override
  public void unlockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObCollectionGeneralModel collection = entitiesOfInterestContainer.getMainEntity();

    collectionLockCommand.unlockAllSections(collection.getUserCode());
    refDocumentLockCommand.unlockAllSections(collection.getRefDocumentCode());

    journalBalanceLockCommand = journalBalanceLockCommandBeanFactory
        .getJournalBalanceLockCommand(collection.getCollectionMethod());

    CObJournalBalanceGeneralModel journalBalance =
        entitiesOfInterestContainer.getEntity(CObJournalBalanceGeneralModel.class);

    journalBalanceLockCommand.unlockAllSections(journalBalance.getUserCode());
  }

}
