package com.ebs.dda.functions.executequery.accounting.settlement;

import com.ebs.dac.foundation.exceptions.data.MissingSystemDataException;
import com.ebs.dda.functions.utils.Pair;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceCodeGenerationValueObject;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceTypeEnum;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;
import com.ebs.dda.jpa.masterdata.company.IObCompanyBankDetailsGeneralModel;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;

class SettlementJournalBalanceCodeGenerationValueObjectFactory {

    private static final String ACCOUNT_NUMBER_CURRENCY_SEPARATOR = " - ";
    private final IObCompanyBankDetailsGeneralModelRep companyBankDetailsGMRep;

    SettlementJournalBalanceCodeGenerationValueObjectFactory(
        IObCompanyBankDetailsGeneralModelRep companyBankDetailsGMRep) {
        this.companyBankDetailsGMRep = companyBankDetailsGMRep;
    }

    Pair<IObSettlementAccountDetailsGeneralModel, JournalBalanceCodeGenerationValueObject> initTreasuryJBGenerationValueObject(
        DObSettlementGeneralModel settlementGM,
        IObSettlementDetailsGeneralModel details,
        IObSettlementAccountDetailsGeneralModel accDetails) {

        JournalBalanceCodeGenerationValueObject valueObject =
            initJBGenerationValueObject(settlementGM, details);

        valueObject.setTreasuryCode(accDetails.getGlSubAccountCode());
        valueObject.setObjectTypeCode(JournalBalanceTypeEnum.JournalBalanceType.TREASURY.name());
        return Pair.create(accDetails, valueObject);
    }

    Pair<IObSettlementAccountDetailsGeneralModel, JournalBalanceCodeGenerationValueObject> initBankJBGenerationValueObject(
        DObSettlementGeneralModel settlementGM,
        IObSettlementDetailsGeneralModel details,
        IObSettlementAccountDetailsGeneralModel accDetails) {

        JournalBalanceCodeGenerationValueObject valueObject =
            initJBGenerationValueObject(settlementGM, details);

        valueObject.setBankCode(
            getBankCode(settlementGM.getCompanyCode(), accDetails.getGlSubAccountCode()));
        valueObject
            .setBankAccountNumber(getBankAccountNumber(accDetails.getGlSubAccountCode()));
        valueObject.setObjectTypeCode(JournalBalanceTypeEnum.JournalBalanceType.BANK.name());

        return Pair.create(accDetails, valueObject);
    }

    private JournalBalanceCodeGenerationValueObject initJBGenerationValueObject(
        DObSettlementGeneralModel settlementGM,
        IObSettlementDetailsGeneralModel details) {

        JournalBalanceCodeGenerationValueObject valueObject =
            new JournalBalanceCodeGenerationValueObject();

        valueObject.setBusinessUnitCode(settlementGM.getPurchaseUnitCode());
        valueObject.setCompanyCode(settlementGM.getCompanyCode());
        valueObject.setCurrencyCode(details.getCurrencyCode());
        return valueObject;
    }

    private String getBankCode(String companyCode, String bankAccNumber) {
        IObCompanyBankDetailsGeneralModel companyBankDetails = companyBankDetailsGMRep
            .findOneByCompanyCodeAndAccountNumber(companyCode, bankAccNumber);
        if(companyBankDetails == null) {
            throw new MissingSystemDataException(
                "Company[code=" + companyCode + "] does not have this bank[accountNo="
                    + bankAccNumber + "]");
        }
        return companyBankDetails.getCode();
    }

    private String getBankAccountNumber(String accountNumber) {
        if (accountNumber == null) {
            return null;
        }
        return accountNumber.split(ACCOUNT_NUMBER_CURRENCY_SEPARATOR)[0];
    }

}
