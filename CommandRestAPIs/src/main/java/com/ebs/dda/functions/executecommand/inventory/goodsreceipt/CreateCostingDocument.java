package com.ebs.dda.functions.executecommand.inventory.goodsreceipt;

import com.ebs.dda.commands.accounting.costing.DObCostingCreateCommand;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class CreateCostingDocument
    implements Observer<ExecuteCommandContext<DObInventoryDocumentActivateValueObject>> {
  public static final String GR_PO = "GR_PO";
  private DObCostingCreateCommand costingCreateCommand;

  public CreateCostingDocument(DObCostingCreateCommand costingCreateCommand) {
    this.costingCreateCommand = costingCreateCommand;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(
      @NonNull ExecuteCommandContext<DObInventoryDocumentActivateValueObject> context) {
    DObInventoryDocumentActivateValueObject valueObject = context.valueObject();
    if (valueObject.getInventoryType().equals(GR_PO)) {
      try {
        costingCreateCommand.executeCommand(valueObject);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
