package com.ebs.dda.functions.executecommand.accounting.payment;

import com.ebs.dda.commands.accounting.paymentrequest.DObPaymentRequestAccountDeterminationCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestAccountDeterminationValueObject;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class DeterminePaymentAccount implements Observer<ExecuteActivateDependanciesContext> {

    private DObPaymentRequestAccountDeterminationCommand accountDeterminationCommand;

    public DeterminePaymentAccount(DObPaymentRequestAccountDeterminationCommand accountDeterminationCommand) {
        this.accountDeterminationCommand = accountDeterminationCommand;
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(ExecuteActivateDependanciesContext context) {

        try {
            DObPaymentRequestAccountDeterminationValueObject valueObject = initValueObject(context.entitiesOfInterestContainer().getMainEntity());
            accountDeterminationCommand.executeCommand(valueObject);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private DObPaymentRequestAccountDeterminationValueObject initValueObject(DObPaymentRequestGeneralModel payment) {
        DObPaymentRequestAccountDeterminationValueObject valueObject = new DObPaymentRequestAccountDeterminationValueObject();
        valueObject.setCode(payment.getUserCode());
        return valueObject;
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }

}
