package com.ebs.dda.functions.activate;

import com.ebs.dda.codegenerator.DocumentObjectCodeGenerator;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntry;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class GenerateJournalEntryCode implements Observer<FindEntityContext> {

    private DocumentObjectCodeGenerator codeGenerator;
    private Class journalEntryClass;
    public GenerateJournalEntryCode(DocumentObjectCodeGenerator codeGenerator, Class journalEntryClass) {
        this.codeGenerator = codeGenerator;
        this.journalEntryClass = journalEntryClass;
    }

    @Override
    public void onSubscribe(@NonNull Disposable disposable) {

    }

    @Override
    public void onNext(@NonNull FindEntityContext context) {
        String userCode =
                codeGenerator.generateUserCode(DObJournalEntry.class.getSimpleName());
        context.entitiesOfInterestContainer().setEntity(journalEntryClass, userCode);
    }

    @Override
    public void onError(@NonNull Throwable throwable) {

    }

    @Override
    public void onComplete() {

    }
}
