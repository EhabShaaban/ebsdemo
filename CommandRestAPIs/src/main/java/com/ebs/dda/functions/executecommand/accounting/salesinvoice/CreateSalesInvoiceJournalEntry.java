package com.ebs.dda.functions.executecommand.accounting.salesinvoice;

import com.ebs.dda.commands.accounting.journalentry.create.salesinvoice.DObJournalEntryCreateSalesInvoiceCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CreateSalesInvoiceJournalEntry
    implements Observer<ExecuteActivateDependanciesContext> {

  private DObJournalEntryCreateSalesInvoiceCommand command;

  public CreateSalesInvoiceJournalEntry(DObJournalEntryCreateSalesInvoiceCommand command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {

    try {
      DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
          context.entitiesOfInterestContainer().getMainEntity();
      Observable<String> journalEntryCode =
          command.executeCommand(initValueObject(salesInvoiceGeneralModel));
      journalEntryCode.subscribe(userCode -> context.journalEntryUserCode(userCode));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObJournalEntryCreateValueObject initValueObject(
      DObSalesInvoiceGeneralModel salesInvoice) {
    DObJournalEntryCreateValueObject valueObject = new DObJournalEntryCreateValueObject();
    valueObject.setUserCode(salesInvoice.getUserCode());
    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
