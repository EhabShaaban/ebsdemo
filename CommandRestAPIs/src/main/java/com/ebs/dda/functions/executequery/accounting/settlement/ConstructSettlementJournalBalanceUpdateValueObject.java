package com.ebs.dda.functions.executequery.accounting.settlement;

import com.ebs.dac.foundation.exceptions.data.MissingSystemDataException;
import com.ebs.dda.codegenerator.JournalBalanceCodeGenerator;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.utils.Pair;
import com.ebs.dda.jpa.accounting.journalbalance.CObJournalBalanceGeneralModel;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceCodeGenerationValueObject;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceUpdateValueObject;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.journalbalance.CObJournalBalanceGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementDetailsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.company.IObCompanyBankDetailsGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers.Banks;
import static com.ebs.dda.jpa.masterdata.chartofaccount.enums.SubLedgers.Treasuries;

public class ConstructSettlementJournalBalanceUpdateValueObject
    implements Observer<FindEntityContext> {

    private final IObSettlementAccountDetailsGeneralModelRep accDetailsGMRep;
    private final IObSettlementDetailsGeneralModelRep detailsGMRep;
    private final CObJournalBalanceGeneralModelRep journalBalanceGMRep;
    private final JournalBalanceCodeGenerator journalBalanceCodeGenerator;
    private final SettlementJournalBalanceCodeGenerationValueObjectFactory jbCodeGenerationVOFactory;

    public ConstructSettlementJournalBalanceUpdateValueObject(
        IObSettlementAccountDetailsGeneralModelRep accDetailsGMRep,
        IObSettlementDetailsGeneralModelRep detailsGMRep,
        IObCompanyBankDetailsGeneralModelRep companyBankDetailsGMRep,
        CObJournalBalanceGeneralModelRep journalBalanceGMRep,
        JournalBalanceCodeGenerator journalBalanceCodeGenerator) {

        this.accDetailsGMRep = accDetailsGMRep;
        this.detailsGMRep = detailsGMRep;
        this.journalBalanceGMRep = journalBalanceGMRep;
        this.journalBalanceCodeGenerator = journalBalanceCodeGenerator;

        jbCodeGenerationVOFactory = new SettlementJournalBalanceCodeGenerationValueObjectFactory(companyBankDetailsGMRep);
    }

    @Override
    public void onSubscribe(Disposable d) {}

    @Override
    public void onNext(FindEntityContext context) {

        DObSettlementGeneralModel settlementGM =
            context.entitiesOfInterestContainer().getMainEntity();

        IObSettlementDetailsGeneralModel detailsGM =
            detailsGMRep.findOneByRefInstanceId(settlementGM.getId());

        Map<String, List<IObSettlementAccountDetailsGeneralModel>> accDetailsPerLedger = accDetailsGMRep
            .findByRefInstanceId(settlementGM.getId())
            .stream()
            .filter(ConstructSettlementJournalBalanceUpdateValueObject::isAccBankOrTreasury)
            .collect(Collectors.groupingBy(IObSettlementAccountDetailsGeneralModel::getSubLedger));

        List<CObJournalBalanceGeneralModel> journalBalances = new ArrayList<>();
        List<Pair<IObSettlementAccountDetailsGeneralModel, JournalBalanceUpdateValueObject>> journalBalanceUpdateVOs = new ArrayList<>();

        if(accDetailsPerLedger.containsKey(Banks.name())) {

            List<Pair<IObSettlementAccountDetailsGeneralModel, CObJournalBalanceGeneralModel>> bankJournalBalances = accDetailsPerLedger
                .get(Banks.name())
                .stream()
                .map(accDetails -> jbCodeGenerationVOFactory.initBankJBGenerationValueObject(settlementGM, detailsGM, accDetails))
                .map(this::getJournalBalance)
                .collect(Collectors.toList());

            bankJournalBalances.forEach(jb -> journalBalances.add(jb.getSecond()));

            journalBalanceUpdateVOs.addAll(
                bankJournalBalances
                    .stream()
                    .map(this::initUpdateJBValueObject)
                    .collect(Collectors.toList())
            );
        }

        if(accDetailsPerLedger.containsKey(Treasuries.name())) {

            List<Pair<IObSettlementAccountDetailsGeneralModel, CObJournalBalanceGeneralModel>> treasuryJournalBalances = accDetailsPerLedger
                .get(Treasuries.name())
                .stream()
                .map(accDetails -> jbCodeGenerationVOFactory.initTreasuryJBGenerationValueObject(settlementGM, detailsGM, accDetails))
                .map(this::getJournalBalance)
                .collect(Collectors.toList());

            treasuryJournalBalances.forEach(jb -> journalBalances.add(jb.getSecond()));

            journalBalanceUpdateVOs.addAll(
                treasuryJournalBalances
                    .stream()
                    .map(this::initUpdateJBValueObject)
                    .collect(Collectors.toList())
            );
        }

        context.entitiesOfInterestContainer().setEntity(CObJournalBalanceGeneralModel.class, journalBalances);
        context.entitiesOfInterestContainer().setEntity(Pair.class, journalBalanceUpdateVOs);
    }

    private Pair<IObSettlementAccountDetailsGeneralModel, JournalBalanceUpdateValueObject>
        initUpdateJBValueObject(Pair<IObSettlementAccountDetailsGeneralModel, CObJournalBalanceGeneralModel> balancePair) {

        JournalBalanceUpdateValueObject valueObject = new JournalBalanceUpdateValueObject();

        valueObject.setJournalBalanceCode(balancePair.getSecond().getUserCode());
        valueObject.setAmount(balancePair.getFirst().getAmount());
        return Pair.create(balancePair.getFirst(), valueObject);
    }

    private static boolean isAccBankOrTreasury(IObSettlementAccountDetailsGeneralModel acc) {
        return acc.getSubLedger().equals(Banks.name())
            || acc.getSubLedger().equals(Treasuries.name());
    }

    private Pair<IObSettlementAccountDetailsGeneralModel, CObJournalBalanceGeneralModel> getJournalBalance(
        Pair<IObSettlementAccountDetailsGeneralModel, JournalBalanceCodeGenerationValueObject> jbCodeGenerationVOPair) {
        String journalBalanceCode = journalBalanceCodeGenerator.generateUserCode(jbCodeGenerationVOPair.getSecond());
        CObJournalBalanceGeneralModel journalBalance =
            journalBalanceGMRep.findOneByUserCode(journalBalanceCode);
        if (journalBalance == null) {
            throw new MissingSystemDataException("Missing JB with code: " + journalBalanceCode);
        }
        return Pair.create(jbCodeGenerationVOPair.getFirst(), journalBalance);
    }

    @Override
    public void onError(Throwable e) {}

    @Override
    public void onComplete() {}

}
