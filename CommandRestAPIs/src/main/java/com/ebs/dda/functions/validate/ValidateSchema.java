package com.ebs.dda.functions.validate;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ValidateSchema implements Observer<ValidateSchemaContext> {

  private SchemaValidator schemaValidator;

  public ValidateSchema(SchemaValidator schemaValidator) {
    this.schemaValidator = schemaValidator;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ValidateSchemaContext context) {
    try {
      schemaValidator.validateRequestBodySchema(context.activateData(), context.schema());
    } catch (Exception e) {
      throw new ArgumentViolationSecurityException();
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
