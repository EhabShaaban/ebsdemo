package com.ebs.dda.functions.executequery.accounting.landedCost;

import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.enums.OrderTypeEnum;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FindEstimatedLandedCostByPurchaseOrder implements Observer<FindEntityContext> {

  private DObLandedCostGeneralModelRep landedCostRep;


  public FindEstimatedLandedCostByPurchaseOrder(DObLandedCostGeneralModelRep landedCostRep) {
    this.landedCostRep = landedCostRep;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(FindEntityContext context) {

    DObPurchaseOrderGeneralModel order =
        context.entitiesOfInterestContainer().getEntity(DObPurchaseOrderGeneralModel.class);

    if (order == null) {
      return;
    }

    DObLandedCostGeneralModel landedCost =
        landedCostRep.findByPurchaseOrderCodeAndCurrentStatesLike(getPurchaseOrderCode(order),
            "%" + DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED + "%");

    context.entitiesOfInterestContainer().setEntity(DObLandedCostGeneralModel.class, landedCost);

  }

  private String getPurchaseOrderCode(DObPurchaseOrderGeneralModel order) {
    if (order.getObjectTypeCode().equals(OrderTypeEnum.SERVICE_PO.name())) {
      return order.getRefDocumentCode();
    }
    return order.getUserCode();
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
