package com.ebs.dda.functions.executequery.accounting.vendorinvoice;

import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FindVendorInvoicePurchaseOrder implements Observer<FindEntityContext> {

  private DObPurchaseOrderGeneralModelRep purchaseOrderRep;

  public FindVendorInvoicePurchaseOrder(DObPurchaseOrderGeneralModelRep purchaseOrderRep) {
    this.purchaseOrderRep = purchaseOrderRep;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(FindEntityContext context) {

    DObVendorInvoiceGeneralModel vendorInvoice =
        context.entitiesOfInterestContainer().getMainEntity();

    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderRep.findOneByUserCode(vendorInvoice.getPurchaseOrderCode());

    context.entitiesOfInterestContainer().setEntity(DObPurchaseOrderGeneralModel.class,
        purchaseOrder);

  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
