package com.ebs.dda.functions.executequery.accounting.payment;

import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FindPaymentPurhcaseOrder implements Observer<FindEntityContext> {

  private DObPurchaseOrderGeneralModelRep purchaseOrderRep;
  private IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep;


  public FindPaymentPurhcaseOrder(DObPurchaseOrderGeneralModelRep purchaseOrderRep,
      IObPaymentRequestPaymentDetailsGeneralModelRep paymentDetailsRep) {
    this.purchaseOrderRep = purchaseOrderRep;
    this.paymentDetailsRep = paymentDetailsRep;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(FindEntityContext context) {

    DObPaymentRequestGeneralModel paymentRequest =
        context.entitiesOfInterestContainer().getMainEntity();

    IObPaymentRequestPaymentDetailsGeneralModel paymentDetails =
        paymentDetailsRep.findOneByUserCode(paymentRequest.getUserCode());

    DObPurchaseOrderGeneralModel purchaseOrder =
        purchaseOrderRep.findOneByUserCode(paymentDetails.getDueDocumentCode());

    context.entitiesOfInterestContainer().setEntity(DObPurchaseOrderGeneralModel.class,
        purchaseOrder);

  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
