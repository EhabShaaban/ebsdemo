package com.ebs.dda.functions.activate;

import java.util.stream.Stream;
import com.ebs.dac.infrastructure.exception.IExceptionResponse;
import com.ebs.dac.infrastructure.tm.TransactionCallback;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.functions.validate.ValidateSchema;
import io.reactivex.Observable;
import io.reactivex.Observer;

public class ActivateDocument {

  private Observable activateFunction;
  private AuthorizeOnPermission authorizeOnPermission;
  private ValidateSchema validateScheme;
  private Stream<Observer> findEntitiesOfInterest;
  private AuthorizeOnCondition authorizeOnCondition;
  private LockEntitiesOfInterest lockEntitiesOfInterest;
  private UnlockEntitiesOfInterest unlockEntitiesOfInterest;
  private DeserializeValueObject deserializeValueObject;
  private ValidateIfActionIsAllowed validateIfActionIsAllowed;
  private ValidateMissingFieldsForState validateMissingFieldsForState;
  private ValidateBusinessRules validateBusinessRules;
  private ExecuteCommand executeCommand;
  private Stream<Observer> dependenciesCommands;
  private SerializeActivateSuccessResponse serializeSuccessResponse;

  private TransactionManagerDelegate transactionManager;

  public ActivateDocument() {
    activateFunction = Observable.empty();
    dependenciesCommands = Stream.empty();
    findEntitiesOfInterest = Stream.empty();
  }

  public void activate(ActivateContext activateContext) throws Exception {

    activateFunction = Observable.just(activateContext);

    safeSubscribe(authorizeOnPermission);
    safeSubscribe(validateScheme);
    safeFindEntitiesOfInterest();
    safeSubscribe(authorizeOnCondition);

    safeSubscribe(lockEntitiesOfInterest);
    try {
      safeSubscribe(deserializeValueObject);

      safeSubscribe(validateIfActionIsAllowed);
      safeSubscribe(validateMissingFieldsForState);
      safeSubscribe(validateBusinessRules);

      safeExecuteCommandsInTransaction();
      safeSubscribe(serializeSuccessResponse);

    } finally {
      safeSubscribe(unlockEntitiesOfInterest);
    }
  }

  private void safeSubscribe(Observer observer) {
    if (observer != null) {
      try {
        activateFunction.subscribe(observer);
      } catch (RuntimeException e) {
        RuntimeException runtimeException = getException(e);
        throw runtimeException;
      }
    }
  }

  private RuntimeException getException(RuntimeException e) {
    if (e instanceof IExceptionResponse) {
      return e;
    }
    return (RuntimeException) e.getCause();
  }

  private void safeFindEntitiesOfInterest() {
    findEntitiesOfInterest.forEach(this::safeSubscribe);
  }

  private void safeExecuteCommandsInTransaction() {
    if (transactionManager != null) {
      transactionManager.executeTransaction(new TransactionCallback() {
        @Override
        protected void doTransaction() {
          safeSubscribe(executeCommand);
          dependenciesCommands.forEach(observer -> safeSubscribe(observer));
        }
      });
    }
  }

  public ActivateDocument authorizeOnPermission(AuthorizeOnPermission authorizeOnPermission) {
    this.authorizeOnPermission = authorizeOnPermission;
    return this;
  }

  public ActivateDocument validateSchema(ValidateSchema validateSchema) {
    this.validateScheme = validateSchema;
    return this;
  }

  public ActivateDocument findEntitiesOfInterest(Observer... entitiesOfInterestObservers) {
    this.findEntitiesOfInterest = Stream.of(entitiesOfInterestObservers);
    return this;
  }

  public ActivateDocument authorizeOnCondition(AuthorizeOnCondition authorizeOnCondition) {
    this.authorizeOnCondition = authorizeOnCondition;
    return this;
  }

  public ActivateDocument lockEntitiesOfInterest(LockEntitiesOfInterest lockEntitiesOfInterest) {
    this.lockEntitiesOfInterest = lockEntitiesOfInterest;
    return this;
  }

  public ActivateDocument deserializeValueObject(DeserializeValueObject deserializeValueObject) {
    this.deserializeValueObject = deserializeValueObject;
    return this;
  }

  public ActivateDocument validateIfActionIsAllowed(
      ValidateIfActionIsAllowed validateIfActionIsAllowed) {
    this.validateIfActionIsAllowed = validateIfActionIsAllowed;
    return this;
  }

  public ActivateDocument validateMissingFieldsForState(
      ValidateMissingFieldsForState validateMissingFieldsForState) {
    this.validateMissingFieldsForState = validateMissingFieldsForState;
    return this;
  }

  public ActivateDocument validateBusinessRules(ValidateBusinessRules validateBusinessRules) {
    this.validateBusinessRules = validateBusinessRules;
    return this;
  }

  public ActivateDocument transactionManager(TransactionManagerDelegate transactionManager) {
    this.transactionManager = transactionManager;
    return this;
  }

  public ActivateDocument executeCommand(ExecuteCommand executeCommand) {
    this.executeCommand = executeCommand;
    return this;
  }

  public ActivateDocument executeActivateDependencies(Observer... dependenciesCommands) {
    this.dependenciesCommands = Stream.of(dependenciesCommands);
    return this;
  }

  public ActivateDocument unlockEntitiesOfInterest(
      UnlockEntitiesOfInterest unlockEntitiesOfInterest) {
    this.unlockEntitiesOfInterest = unlockEntitiesOfInterest;
    return this;
  }

  public ActivateDocument serializeSuccessResponse(
      SerializeActivateSuccessResponse serializeResponse) {
    this.serializeSuccessResponse = serializeResponse;
    return this;
  }
}
