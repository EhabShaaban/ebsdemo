package com.ebs.dda.functions.lock.accounting.vendorinvoice;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCost;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;

public class ServiceInvoiceLockActivateEntitiesOfInterest
    extends VendorInvoiceLockActivateEntitiesOfInterest {

  private EntityLockCommand<DObLandedCost> landedCostLockCommand;

  public ServiceInvoiceLockActivateEntitiesOfInterest(
      EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
      EntityLockCommand<?> refDocumentLockCommand,
      EntityLockCommand<DObLandedCost> landedCostLockCommand) {

    super(vendorInvoiceLockCommand, refDocumentLockCommand);
    this.landedCostLockCommand = landedCostLockCommand;
  }

  @Override
  public void lock(EntitiesOfInterestContainer entitiesOfInterestContainer) throws Exception {

    super.lock(entitiesOfInterestContainer);

    DObLandedCostGeneralModel landedCost =
        entitiesOfInterestContainer.getEntity(DObLandedCostGeneralModel.class);

    if (landedCost != null) {
      lockEntity(landedCostLockCommand, landedCost.getUserCode());
    }
  }

}
