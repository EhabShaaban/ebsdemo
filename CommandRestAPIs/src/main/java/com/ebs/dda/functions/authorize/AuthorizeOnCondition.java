package com.ebs.dda.functions.authorize;

import com.ebs.dac.dbo.jpa.entities.BusinessObject;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class AuthorizeOnCondition implements Observer<AuthorizeContext> {

  private final AuthorizationManager authorizationManager;

  public AuthorizeOnCondition(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  @Override
  public void onSubscribe(Disposable disposable) {}

  @Override
  public void onNext(AuthorizeContext authorizeContext) {
    try {
      BusinessObject businessObject =
          authorizeContext.entitiesOfInterestContainer().getMainEntity();
      this.authorizationManager.authorizeActionOnObject(
          businessObject, authorizeContext.actionName());
    } catch (Exception e) {
      throw new AuthorizationException(e);
    }
  }

  @Override
  public void onError(Throwable throwable) {}

  @Override
  public void onComplete() {}
}
