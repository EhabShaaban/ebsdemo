package com.ebs.dda.functions.executecommand;

public interface ExecuteCommandContext<V> {

  V valueObject();
}
