package com.ebs.dda.functions.lock.accounting.salesinvoice;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.lock.IActivateLockEntitiesOfInterest;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;

public abstract class SalesInvoiceLockActivateEntitiesOfInterest
    implements IActivateLockEntitiesOfInterest {

  private EntityLockCommand<DObSalesInvoice> salesInvoiceLockCommand;
  private EntityLockCommand<?> refDocumentLockCommand;

  protected SalesInvoiceLockActivateEntitiesOfInterest(
      EntityLockCommand<DObSalesInvoice> salesInvoiceLockCommand,
      EntityLockCommand<?> refDocumentLockCommand) {
    this.salesInvoiceLockCommand = salesInvoiceLockCommand;
    this.refDocumentLockCommand = refDocumentLockCommand;
  }

  @Override
  public void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoice =
        entitiesOfInterestContainer.getEntity(DObSalesInvoiceGeneralModel.class);
    salesInvoiceLockCommand.lockAllSections(salesInvoice.getUserCode());
    if (this.refDocumentLockCommand != null) {
      refDocumentLockCommand.lockAllSections(salesInvoice.getSalesOrder());
    }
  }

  @Override
  public void unlockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    DObSalesInvoiceGeneralModel salesInvoice =
        entitiesOfInterestContainer.getEntity(DObSalesInvoiceGeneralModel.class);
    salesInvoiceLockCommand.unlockAllSections(salesInvoice.getUserCode());
    if (this.refDocumentLockCommand != null) {
      refDocumentLockCommand.unlockAllSections(salesInvoice.getSalesOrder());
    }
  }
}
