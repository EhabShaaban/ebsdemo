package com.ebs.dda.functions.validate;

import java.util.List;
import java.util.Map;
import com.ebs.dac.foundation.exceptions.data.MissingFieldsException;
import com.ebs.dda.validation.IMissingFieldsValidator;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ValidateMissingFieldsForState
    implements Observer<ValidateMissingFieldsForStateContext> {

  private IMissingFieldsValidator validator;

  public ValidateMissingFieldsForState(IMissingFieldsValidator validator) {
    this.validator = validator;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ValidateMissingFieldsForStateContext context) {
    Map<String, Object> missingFields =
        validator.validateMissingFields(context.objectCode(), context.state());
    if (!missingFields.isEmpty()) {
      throw new MissingFieldsException(missingFields);
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
