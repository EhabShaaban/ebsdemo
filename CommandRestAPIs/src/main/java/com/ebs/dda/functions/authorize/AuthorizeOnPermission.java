package com.ebs.dda.functions.authorize;

import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;


public class AuthorizeOnPermission implements Observer<AuthorizeContext> {

  private final AuthorizationManager authorizationManager;

  public AuthorizeOnPermission(AuthorizationManager authorizationManager) {
    this.authorizationManager = authorizationManager;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(AuthorizeContext authorizeContext) {
    try {
      authorizationManager.authorizeAction(authorizeContext.objectSysName(), authorizeContext.actionName());
    } catch (Exception e) {
      throw new AuthorizationException(e);
    }
  }

  @Override
  public void onError(Throwable throwable) {}

  @Override
  public void onComplete() {}
}
