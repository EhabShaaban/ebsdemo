package com.ebs.dda.functions.lock;

import java.util.Stack;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;

/**
 * @author Ahmed Ali Elfeky
 * @since Jun 23, 2021
 */

public abstract class AbstractActivateLockEntitesOfInterest
    implements IActivateLockEntitiesOfInterest {

  private Stack<Pair> lockedEntties;

  protected AbstractActivateLockEntitesOfInterest() {
    lockedEntties = new Stack<>();
  }

  protected abstract void lock(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception;

  @Override
  public final void lockEntitiesOfInterest(EntitiesOfInterestContainer entitiesOfInterestContainer)
      throws Exception {
    try {
      lock(entitiesOfInterestContainer);
    } catch (Exception e) {
      unlock();
      throw e;
    }
  }

  @Override
  public final void unlockEntitiesOfInterest(
      EntitiesOfInterestContainer entitiesOfInterestContainer) throws Exception {
    unlock();
  }

  protected void lockEntity(EntityLockCommand<?> lockCommand, String userCode) throws Exception {
    lockCommand.lockAllSections(userCode);
    lockedEntties.push(Pair.create(lockCommand, userCode));
  }

  private void unlock() throws Exception {

    while (!lockedEntties.isEmpty()) {
      Pair lockPair = lockedEntties.pop();
      lockPair.lockCommand.unlockAllSections(lockPair.userCode);
    }
  }

  private static class Pair {
    private EntityLockCommand<?> lockCommand;
    private String userCode;

    private Pair(EntityLockCommand<?> lockCommand, String userCode) {
      this.lockCommand = lockCommand;
      this.userCode = userCode;
    }

    static Pair create(EntityLockCommand<?> lockCommand, String userCode) {
      return new Pair(lockCommand, userCode);
    }
  }

}
