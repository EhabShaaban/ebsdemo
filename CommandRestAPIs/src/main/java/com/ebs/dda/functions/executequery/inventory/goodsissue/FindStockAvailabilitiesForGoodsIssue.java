package com.ebs.dda.functions.executequery.inventory.goodsissue;

import com.ebs.dda.codegenerator.StockAvailabilityCodeGenerator;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.inventory.goodsissue.DObGoodsIssueGeneralModel;
import com.ebs.dda.jpa.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.CObStockAvailabilitiesGeneralModel;
import com.ebs.dda.jpa.inventory.stockavailabilities.StockAvailabilityCodeGenerationValueObject;
import com.ebs.dda.repositories.inventory.goodsissue.IObGoodsIssueItemQuantityGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FindStockAvailabilitiesForGoodsIssue implements Observer<FindEntityContext> {
  private final IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep;
  private final StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator;
  private List<String> stockAvailabilityCodes = new ArrayList<>();

  public FindStockAvailabilitiesForGoodsIssue(
      IObGoodsIssueItemQuantityGeneralModelRep goodsIssueItemQuantityGeneralModelRep,
      StockAvailabilityCodeGenerator stockAvailabilityCodeGenerator) {
    this.goodsIssueItemQuantityGeneralModelRep = goodsIssueItemQuantityGeneralModelRep;
    this.stockAvailabilityCodeGenerator = stockAvailabilityCodeGenerator;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(@NonNull FindEntityContext context) {
    List<IObGoodsIssueItemQuantityGeneralModel> goodsIssueItems = getGoodsIssueItems(context);
    stockAvailabilityCodes =
        goodsIssueItems.stream()
            .map(this::getStockAvailabilityCodeForItem)
            .collect(Collectors.toList());
    context
        .entitiesOfInterestContainer()
        .setEntity(CObStockAvailabilitiesGeneralModel.class, stockAvailabilityCodes);
  }

  private List<IObGoodsIssueItemQuantityGeneralModel> getGoodsIssueItems(
      FindEntityContext context) {
    DObGoodsIssueGeneralModel goodsIssueGeneralModel =
        context.entitiesOfInterestContainer().getMainEntity();
    return goodsIssueItemQuantityGeneralModelRep.findAllByUserCode(
        goodsIssueGeneralModel.getUserCode());
  }

  private String getStockAvailabilityCodeForItem(IObGoodsIssueItemQuantityGeneralModel item) {
    StockAvailabilityCodeGenerationValueObject valueObject =
        new StockAvailabilityCodeGenerationValueObject().fromItemQuantityGeneralModel(item);
    return stockAvailabilityCodeGenerator.generateUserCode(valueObject);
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
