package com.ebs.dda.functions.executecommand.accounting.settlement;

import com.ebs.dda.commands.accounting.journalentry.create.DObJournalEntryCreateSettlementCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObSettlementJournalEntry;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementGeneralModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CreateSettlementJournalEntry implements Observer<ExecuteActivateDependanciesContext> {

  private final DObJournalEntryCreateSettlementCommand createJournalEntryCommand;

  public CreateSettlementJournalEntry(
    DObJournalEntryCreateSettlementCommand createJournalEntryCommand) {
    this.createJournalEntryCommand = createJournalEntryCommand;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      Observable<String> executeCommandObs =
        createJournalEntryCommand.executeCommand(initValueObject(context));
      executeCommandObs.subscribe(code -> context.journalEntryUserCode(code));
    } catch (Exception e) {
      throw new RuntimeException("Error while settlement create JE: ", e);
    }
  }

  private DObJournalEntryCreateValueObject initValueObject(ExecuteActivateDependanciesContext context) {
    DObSettlementGeneralModel settlement = context.entitiesOfInterestContainer().getMainEntity();

    DObJournalEntryCreateValueObject valueObject = new DObJournalEntryCreateValueObject();
    valueObject.setUserCode(settlement.getUserCode());

    String journalEntryCode = context.entitiesOfInterestContainer().getStringValue(
      DObSettlementJournalEntry.class);
    valueObject.setJournalEntryCode(journalEntryCode);

    return valueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
