package com.ebs.dda.functions.lock;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UnlockEntitiesOfInterest implements Observer<LockContext> {

  private static final Logger LOGGER = LoggerFactory.getLogger(UnlockEntitiesOfInterest.class);
  private IActivateLockEntitiesOfInterest lock;

  public UnlockEntitiesOfInterest(IActivateLockEntitiesOfInterest lock) {
    this.lock = lock;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(LockContext context) {
    try {
      lock.unlockEntitiesOfInterest(context.entitiesOfInterestContainer());
    } catch (Exception e) {
      LOGGER.error("Unexcepected error happend while unlocking");
    }
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
