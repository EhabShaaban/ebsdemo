package com.ebs.dda.functions.executecommand.accounting.monetarynote;

import com.ebs.dda.commands.accounting.notesreceivables.DObJournalEntryCreateNotesReceivableCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivableActivateValueObject;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;


public class CreateNotesReceivableJournalEntry
  implements Observer<ExecuteActivateDependanciesContext> {

  private final DObJournalEntryCreateNotesReceivableCommand command;

  public CreateNotesReceivableJournalEntry(DObJournalEntryCreateNotesReceivableCommand command) {
    this.command = command;
  }

  @Override public void onSubscribe(@NonNull Disposable d) {
  }

  @Override public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      command.executeCommand(inintValueObject(context))
        .subscribe(code -> context.journalEntryUserCode(code));
    } catch (Exception e) {
      throw new RuntimeException("Error while NR journal entry creation: ", e);
    }
  }

  private DObNotesReceivableActivateValueObject inintValueObject(
    ExecuteActivateDependanciesContext context) {

    DObNotesReceivablesGeneralModel notesReceivables =
      context.entitiesOfInterestContainer().getMainEntity();

    DObNotesReceivableActivateValueObject valueObject = new DObNotesReceivableActivateValueObject();
    valueObject.setNotesReceivableCode(notesReceivables.getUserCode());

    DObNotesReceivableActivateValueObject activateValueObject = context.valueObject();
    valueObject.setJournalEntryDate(activateValueObject.getJournalEntryDate());

    return valueObject;
  }

  @Override public void onError(@NonNull Throwable e) {
  }

  @Override public void onComplete() {
  }
}
