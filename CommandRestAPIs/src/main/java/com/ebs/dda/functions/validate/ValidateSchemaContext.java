package com.ebs.dda.functions.validate;

public interface ValidateSchemaContext {

  String activateData();

  String schema();

}
