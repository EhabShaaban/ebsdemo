package com.ebs.dda.functions.executecommand.accounting.vendorinvoice;

import com.ebs.dda.commands.accounting.journalentry.create.vendorinvoice.DObJournalEntryCreateVendorInvoiceCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObVendorInvoiceJournalEntry;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CreateVendorInvoiceJournalEntry implements Observer<ExecuteActivateDependanciesContext> {

    private DObJournalEntryCreateVendorInvoiceCommand command;

    public CreateVendorInvoiceJournalEntry(DObJournalEntryCreateVendorInvoiceCommand command) {
        this.command = command;
    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    @Override
    public void onNext(ExecuteActivateDependanciesContext context) {

        try {
            DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
                    context.entitiesOfInterestContainer().getMainEntity();
            Observable<String> journalEntryCode =
                    command.executeCommand(initValueObject(vendorInvoiceGeneralModel, context));
            journalEntryCode.subscribe(userCode -> context.journalEntryUserCode(userCode));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private DObJournalEntryCreateValueObject initValueObject(DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel, ExecuteActivateDependanciesContext context) {
        DObJournalEntryCreateValueObject valueObject = new DObJournalEntryCreateValueObject();
        valueObject.setUserCode(vendorInvoiceGeneralModel.getUserCode());
        String journalEntryCode = context.entitiesOfInterestContainer().getStringValue(DObVendorInvoiceJournalEntry.class);
        valueObject.setJournalEntryCode(journalEntryCode);
        return valueObject;
    }

    @Override
    public void onError(Throwable e) {
    }

    @Override
    public void onComplete() {
    }
}
