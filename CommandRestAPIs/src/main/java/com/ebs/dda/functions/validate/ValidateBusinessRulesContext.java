package com.ebs.dda.functions.validate;

public interface ValidateBusinessRulesContext<V> {

  V valueObject();
}
