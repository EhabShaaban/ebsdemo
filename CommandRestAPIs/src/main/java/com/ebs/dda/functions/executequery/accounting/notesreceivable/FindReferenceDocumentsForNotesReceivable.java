package com.ebs.dda.functions.executequery.accounting.notesreceivable;

import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.monetarynotes.DObNotesReceivablesGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

import java.util.List;

public class FindReferenceDocumentsForNotesReceivable implements Observer<FindEntityContext> {

  private final IObNotesReceivablesReferenceDocumentGeneralModelRep
      notesReceivablesReferenceDocumentGeneralModelRep;

  public FindReferenceDocumentsForNotesReceivable(
      IObNotesReceivablesReferenceDocumentGeneralModelRep
          notesReceivablesReferenceDocumentGeneralModelRep) {
    this.notesReceivablesReferenceDocumentGeneralModelRep =
        notesReceivablesReferenceDocumentGeneralModelRep;
  }

  @Override
  public void onSubscribe(@NonNull Disposable disposable) {}

  @Override
  public void onNext(@NonNull FindEntityContext context) {
    List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocuments =
        getNotesReceivableReferenceDocuments(context);
    context
        .entitiesOfInterestContainer()
        .setEntity(IObNotesReceivablesReferenceDocumentGeneralModel.class, referenceDocuments);
  }

  private List<IObNotesReceivablesReferenceDocumentGeneralModel>
      getNotesReceivableReferenceDocuments(FindEntityContext context) {
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        context.entitiesOfInterestContainer().getMainEntity();
    return notesReceivablesReferenceDocumentGeneralModelRep.findByNotesReceivableCodeOrderByIdAsc(
        notesReceivablesGeneralModel.getUserCode());
  }

  @Override
  public void onError(@NonNull Throwable throwable) {}

  @Override
  public void onComplete() {}
}
