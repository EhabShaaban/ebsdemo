package com.ebs.dda.functions.executecommand.accounting.salesinvoice;

import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceUpdateStateToClosedCommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionUpdateRefDocumentRemainingValueObject;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CloseSalesInvoice implements Observer<ExecuteActivateDependanciesContext> {

  private DObSalesInvoiceUpdateStateToClosedCommand command;

  public CloseSalesInvoice(DObSalesInvoiceUpdateStateToClosedCommand command) {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(ExecuteActivateDependanciesContext context) {
    try {
      DObCollectionGeneralModel collectionGeneralModel =
          context.entitiesOfInterestContainer().getMainEntity();
      command.executeCommand(initValueObject(collectionGeneralModel));
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private DObCollectionUpdateRefDocumentRemainingValueObject initValueObject(
      DObCollectionGeneralModel collection) {
    DObCollectionUpdateRefDocumentRemainingValueObject documentRemainingValueObject =
        new DObCollectionUpdateRefDocumentRemainingValueObject();
    documentRemainingValueObject.setCollectionCode(collection.getUserCode());
    documentRemainingValueObject.setRefDocumentCode(collection.getRefDocumentCode());
    documentRemainingValueObject.setRefDocumentType(collection.getRefDocumentTypeCode());
    return documentRemainingValueObject;
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}
}
