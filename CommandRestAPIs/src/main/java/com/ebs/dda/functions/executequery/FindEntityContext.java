package com.ebs.dda.functions.executequery;

public interface FindEntityContext {

  String objectCode();

  EntitiesOfInterestContainer entitiesOfInterestContainer();

}
