package com.ebs.dda.functions.lock.accounting.paymentrequest;

import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.config.accounting.journalbalance.CObJournalBalanceLockCommandBeanFactory;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequest;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;

public class PaymentAgainstVendorInvoiceLockActivateEntitiesOfInterest extends PaymentLockActivateEntitiesOfInterest {

    public PaymentAgainstVendorInvoiceLockActivateEntitiesOfInterest(EntityLockCommand<DObPaymentRequest> paymentLockCommand,
                                                                     EntityLockCommand<DObVendorInvoice> vendorInvoiceLockCommand,
                                                                     CObJournalBalanceLockCommandBeanFactory journalBalanceLockCommandBeanFactory) {
        super(paymentLockCommand, vendorInvoiceLockCommand, journalBalanceLockCommandBeanFactory);
    }

}
