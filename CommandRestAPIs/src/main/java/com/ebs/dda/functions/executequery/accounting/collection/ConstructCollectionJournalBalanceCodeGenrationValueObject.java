package com.ebs.dda.functions.executequery.accounting.collection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ebs.dac.foundation.exceptions.data.MissingFieldsException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.jpa.accounting.collection.CollectionMethodEnum.CollectionMethod;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IIObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceCodeGenerationValueObject;
import com.ebs.dda.jpa.accounting.journalbalance.JournalBalanceTypeEnum.JournalBalanceType;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ConstructCollectionJournalBalanceCodeGenrationValueObject
    implements Observer<FindEntityContext> {

  private static final String ACCOUNT_NUMBER_CURRANCY_SPERATOR = " - ";
  private IObCollectionDetailsGeneralModelRep detailsRepo;

  public ConstructCollectionJournalBalanceCodeGenrationValueObject(
      IObCollectionDetailsGeneralModelRep detailsRepo) {
    this.detailsRepo = detailsRepo;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(FindEntityContext context) {

    DObCollectionGeneralModel collectionGM = context.entitiesOfInterestContainer().getMainEntity();
    IObCollectionDetailsGeneralModel collectionDetails =
        detailsRepo.findOneByRefInstanceId(collectionGM.getId());

    JournalBalanceCodeGenerationValueObject valueObject =
        initValueObject(collectionGM, collectionDetails);

    context.entitiesOfInterestContainer().setEntity(JournalBalanceCodeGenerationValueObject.class,
        valueObject);
  }

  private JournalBalanceCodeGenerationValueObject initValueObject(
      DObCollectionGeneralModel collectionGM, IObCollectionDetailsGeneralModel collectionDetails) {

    // TODO must be removed as it is workaround while setting bank/treasury at creation
    assertThatBankOrTreasuryExist(collectionDetails.getCollectionMethod(), collectionDetails);

    JournalBalanceCodeGenerationValueObject valueObject =
        new JournalBalanceCodeGenerationValueObject();

    valueObject.setBusinessUnitCode(collectionGM.getPurchaseUnitCode());
    valueObject.setCompanyCode(collectionGM.getCompanyCode());
    valueObject.setCurrencyCode(collectionDetails.getCurrencyCode());
    valueObject.setTreasuryCode(collectionDetails.getTreasuryCode());
    valueObject.setBankCode(collectionDetails.getBankAccountCode());
    valueObject
        .setBankAccountNumber(getBankAccountNumber(collectionDetails.getBankAccountNumber()));
    valueObject.setObjectTypeCode(getType(collectionGM.getCollectionMethod()));
    return valueObject;
  }

  private void assertThatBankOrTreasuryExist(String collectionMethod,
      IObCollectionDetailsGeneralModel collectionDetails) {

    List<String> missingFields = new ArrayList<String>();

    if (collectionDetails.getAmount() == null) {
      missingFields.add(IIObCollectionDetailsGeneralModel.AMOUNT);
    }

    if (isCash(collectionMethod) && collectionDetails.getTreasuryCode() == null) {
      missingFields.add(IIObCollectionDetailsGeneralModel.TREASURY_CODE);
    }

    if (isBank(collectionMethod) && (collectionDetails.getBankAccountCode() == null
        || collectionDetails.getBankAccountNumber() == null)) {
      missingFields.add(IIObCollectionDetailsGeneralModel.BANK_ACCOUNT_CODE);
    }

    if (!missingFields.isEmpty()) {
      Map<String, Object> validationResults = new HashMap<String, Object>();
      validationResults.put(BasicStateMachine.ACTIVE_STATE, missingFields);
      throw new MissingFieldsException(validationResults);
    }
  }

  private String getBankAccountNumber(String accountNumber) {
    if (accountNumber == null) {
      return null;
    }
    return accountNumber.split(ACCOUNT_NUMBER_CURRANCY_SPERATOR)[0];
  }

  private String getType(String collectionMethod) {
    if (isBank(collectionMethod)) {
      return JournalBalanceType.BANK.name();
    }

    if (isCash(collectionMethod)) {
      return JournalBalanceType.TREASURY.name();
    }

    throw new UnsupportedOperationException(collectionMethod + " is not supported");
  }

  private boolean isBank(String collectionMethod) {
    return CollectionMethod.BANK.name().equals(collectionMethod);
  }

  private boolean isCash(String collectionMethod) {
    return CollectionMethod.CASH.name().equals(collectionMethod);
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
