package com.ebs.dda.functions.executequery;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class FindEntityByUserCode<T extends StatefullBusinessObject<?>> implements Observer<FindEntityContext> {

  private StatefullBusinessObjectRep<T, ?> repository;

  public FindEntityByUserCode(StatefullBusinessObjectRep<T, ?> repository) {
    this.repository = repository;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onNext(FindEntityContext context) {
    T entity = repository.findOneByUserCode(context.objectCode());
    if (entity == null) {
      throw new InstanceNotExistException();
    }
    context.entitiesOfInterestContainer().setEntity(entity.getClass(), entity);
  }

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

}
