package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.collection.CollectionCreateJournalEvent;
import com.ebs.dac.dbo.events.collection.CollectionCreateJournalSuccessEvent;
import com.ebs.dac.dbo.events.collection.CollectionUpdateRefDocumentSuccessEvent;
import com.ebs.dac.dbo.events.collection.UpdateSalesInvoiceEvent;
import com.ebs.dda.commands.accounting.journalentry.create.collection.DObJournalEntryItemsCollectionCreateCommand;
import com.ebs.dda.commands.accounting.salesinvoice.DObSalesInvoiceUpdateStateToClosedCommand;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.DObCollectionUpdateRefDocumentRemainingValueObject;
import com.ebs.dda.jpa.accounting.collection.DObJournalEntryCreateCollectionValueObject;
import com.ebs.dda.jpa.accounting.collection.RefDocumentTypeEnum;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import io.reactivex.Observable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CollectionActivationMonitorService implements IEventBusMonitorService {

  @Autowired DObSalesInvoiceUpdateStateToClosedCommand salesInvoiceUpdateStateToClosedCommand;
  @Autowired DObCollectionGeneralModelRep collectionGeneralModelRep;

  @Autowired private ObservablesEventBus eventBus;

  @Autowired
  private DObJournalEntryItemsCollectionCreateCommand journalEntryItemsCollectionCreateCommand;

  public CollectionActivationMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    try {
      if (event instanceof UpdateSalesInvoiceEvent) {
        executeUpdateRefDocumentRemainingAmountCommand(event);
      } else if (event instanceof CollectionCreateJournalEvent) {
        createJournalForCollection((CollectionCreateJournalEvent) event);
      }
    } catch (Exception ex) {
      // TODO handle error in creating journal entry to avoid problems in rx java subject
    }
  }

  private void createJournalForCollection(CollectionCreateJournalEvent event) {
    DObJournalEntryCreateCollectionValueObject valueObject = constructValueObject(event);
    executeJournalEntryItemsCollectionCreateCommand(valueObject);
  }

  private DObJournalEntryCreateCollectionValueObject constructValueObject(
      CollectionCreateJournalEvent event) {
    String collectionCode = event.getUserCode();
    DObJournalEntryCreateCollectionValueObject valueObject =
        new DObJournalEntryCreateCollectionValueObject();
    valueObject.setCollectionCode(collectionCode);
    return valueObject;
  }

  private void executeJournalEntryItemsCollectionCreateCommand(
      DObJournalEntryCreateCollectionValueObject valueObject) {
    final StringBuilder userCode = new StringBuilder();
    try {
      journalEntryItemsCollectionCreateCommand
          .executeCommand(valueObject)
          .subscribe(code -> userCode.append(code));
      consumeCreateJournalSuccessEvent(String.valueOf(userCode));
    } catch (Exception ignored) {
      // TODO: Handling Exceptions will be later
    }
  }

  private void consumeCreateJournalSuccessEvent(String userCode) {
    CollectionCreateJournalSuccessEvent event = new CollectionCreateJournalSuccessEvent(userCode);
    eventBus.registerObservable(CollectionCreateJournalSuccessEvent.class, event);
  }

  private void executeUpdateRefDocumentRemainingAmountCommand(EBSEvent event) throws Exception {
    DObCollectionUpdateRefDocumentRemainingValueObject documentRemainingValueObject =
        getValueObjectForRefDocumentRemaining(event);

    if (documentRemainingValueObject
        .getRefDocumentType()
        .equals(RefDocumentTypeEnum.SALES_INVOICE)) {
      Observable<String> salesInvoiceObservable =
          salesInvoiceUpdateStateToClosedCommand.executeCommand(documentRemainingValueObject);
      salesInvoiceObservable.subscribe(this::consumeCollectionUpdateRefDocumentSuccessEvent);
    }
  }

  private DObCollectionUpdateRefDocumentRemainingValueObject getValueObjectForRefDocumentRemaining(
      EBSEvent event) {
    String CollectionCode = ((UpdateSalesInvoiceEvent) event).getUserCode();
    DObCollectionGeneralModel collectionGeneralModel =
        collectionGeneralModelRep.findOneByUserCode(CollectionCode);
    DObCollectionUpdateRefDocumentRemainingValueObject documentRemainingValueObject =
        new DObCollectionUpdateRefDocumentRemainingValueObject();
    documentRemainingValueObject.setCollectionCode(CollectionCode);
    documentRemainingValueObject.setRefDocumentCode(collectionGeneralModel.getRefDocumentCode());
    documentRemainingValueObject.setRefDocumentType(
        collectionGeneralModel.getRefDocumentTypeCode());
    return documentRemainingValueObject;
  }

  private void consumeCollectionUpdateRefDocumentSuccessEvent(String userCode) {
    CollectionUpdateRefDocumentSuccessEvent event =
        new CollectionUpdateRefDocumentSuccessEvent(userCode);
    eventBus.registerObservable(CollectionUpdateRefDocumentSuccessEvent.class, event);
  }
}
