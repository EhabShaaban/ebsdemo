package com.ebs.dda.rx.inventory;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.stocktrasformation.*;
import com.ebs.dda.commands.inventory.stocktransformation.DObStockTransformationUpdateCommand;
import com.ebs.dda.commands.inventory.storetransaction.TObGoodsReceiptStoreTransactionUpdateCommand;
import com.ebs.dda.commands.inventory.storetransaction.TObStoreTransactionCreateStockTransformationCommand;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionCreateStockTransformationValueObject;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockTransformationActivateMonitorService implements IEventBusMonitorService {

    private ObservablesEventBus eventBus;

    @Autowired
    private TObStoreTransactionCreateStockTransformationCommand
            storeTransactionCreateStockTransformationCommand;

    @Autowired
    private TObGoodsReceiptStoreTransactionUpdateCommand goodsReceiptStoreTransactionUpdateCommand;

  @Autowired private DObStockTransformationUpdateCommand stockTransformationUpdateCommand;

    public StockTransformationActivateMonitorService(ObservablesEventBus eventBus) {
        this.eventBus = eventBus;
        this.eventBus.subscribe(this);
    }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    TObStoreTransactionCreateStockTransformationValueObject storeValueObject =
        new TObStoreTransactionCreateStockTransformationValueObject();

    if (event instanceof StockTransformationActivateSuccessEvent) {
      String storeTransactionCode =
              handleUpdateStoreTransaction(
                      (StockTransformationActivateSuccessEvent) event, storeValueObject);
      consumeUpdateStoreTransactionSuccessEvent(storeTransactionCode);
    } else if (event instanceof StockTransformationCreateStoreTransactionEvent) {
      String newStoreTransactionCode =
              handleCreateStockTransFormationStoreTransaction(
                      (StockTransformationCreateStoreTransactionEvent) event, storeValueObject);
      consumeStockTransformationStoreTransactionCreateSuccessEvent(newStoreTransactionCode);

    } else if (event instanceof StockTransformationUpdateEvent) {
      String stockTransformationCode =
          handleUpdateStockTransformation((StockTransformationUpdateEvent) event, storeValueObject);
      consumeUpdateStockTransformationSuccessEvent(stockTransformationCode);
    }
  }

  private String handleCreateStockTransFormationStoreTransaction(
          StockTransformationCreateStoreTransactionEvent storeTransactionCreateSuccessEvent,
          TObStoreTransactionCreateStockTransformationValueObject storeValueObject)
      throws Exception {

    String stockTransformationCode = storeTransactionCreateSuccessEvent.getUserCode();
    storeValueObject.setStockTransformationCode(stockTransformationCode);

    final StringBuilder newStoreTransactionCode = new StringBuilder();
    storeTransactionCreateStockTransformationCommand
        .executeCommand(storeValueObject)
        .subscribe(code -> newStoreTransactionCode.append(code));

    return newStoreTransactionCode.toString();
  }

  private String handleUpdateStoreTransaction(
          StockTransformationActivateSuccessEvent stockTransformationActivateSuccessEvent,
          TObStoreTransactionCreateStockTransformationValueObject storeValueObject)
      throws Exception {
    final StringBuilder storeTransactionCode = new StringBuilder();

    String stockTransformationCode = stockTransformationActivateSuccessEvent.getUserCode();
    storeValueObject.setStockTransformationCode(stockTransformationCode);

    goodsReceiptStoreTransactionUpdateCommand
        .executeCommand(storeValueObject)
        .subscribe(code -> storeTransactionCode.append(code));

    return storeTransactionCode.toString();
  }

  private String handleUpdateStockTransformation(
          StockTransformationUpdateEvent stockTransformationUpdateEvent,
          TObStoreTransactionCreateStockTransformationValueObject storeValueObject)
      throws Exception {

    String stockTransformationCode = stockTransformationUpdateEvent.getUserCode();
    storeValueObject.setStockTransformationCode(stockTransformationCode);

    stockTransformationUpdateCommand.executeCommand(storeValueObject);
    return stockTransformationCode;
  }

  private void consumeStockTransformationStoreTransactionCreateSuccessEvent(String userCode) {
    StockTransformationCreateStoreTransactionSuccessEvent event =
        new StockTransformationCreateStoreTransactionSuccessEvent(userCode);
    eventBus.registerObservable(StockTransformationCreateStoreTransactionSuccessEvent.class, event);
  }

  private void consumeUpdateStoreTransactionSuccessEvent(String userCode) {
    StoreTransactionUpdateSuccessEvent event = new StoreTransactionUpdateSuccessEvent(userCode);
    eventBus.registerObservable(StoreTransactionUpdateSuccessEvent.class, event);
  }

  private void consumeUpdateStockTransformationSuccessEvent(String userCode) {
    StockTransformationUpdateSuccessEvent event =
        new StockTransformationUpdateSuccessEvent(userCode);
    eventBus.registerObservable(StockTransformationUpdateSuccessEvent.class, event);
  }
}
