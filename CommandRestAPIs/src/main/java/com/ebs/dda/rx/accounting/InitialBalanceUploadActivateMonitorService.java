package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.initialbalanceupload.InitialBalanceUploadActivateEvent;
import com.ebs.dda.commands.accounting.journalentry.create.DObJournalEntryCreateInintialBalanceUploadCommand;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitialBalanceUploadActivateMonitorService implements IEventBusMonitorService {

  private final ObservablesEventBus eventBus;

  @Autowired
  private DObJournalEntryCreateInintialBalanceUploadCommand journalEntryCreateInintialBalanceUploadCommand;

  public InitialBalanceUploadActivateMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof InitialBalanceUploadActivateEvent) {

      InitialBalanceUploadActivateEvent initialBalanceUploadActivateEvent =
          (InitialBalanceUploadActivateEvent) event;
      String initialBalanceUploadUserCode = initialBalanceUploadActivateEvent.getUserCode();
      journalEntryCreateInintialBalanceUploadCommand.executeCommand(initialBalanceUploadUserCode);
    }
  }
}
