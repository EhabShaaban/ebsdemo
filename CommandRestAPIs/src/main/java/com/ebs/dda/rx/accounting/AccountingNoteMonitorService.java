package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.collection.CollectionUpdateRefDocumentSuccessEvent;
import com.ebs.dac.dbo.events.collection.UpdateDebitNoteEvent;
import com.ebs.dda.commands.accounting.accountingnote.DObDebitNoteUpdateRemainingCommand;
import com.ebs.dda.jpa.DocumentObjectUpdateRemainingValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AccountingNoteMonitorService implements IEventBusMonitorService {

  @Autowired private ObservablesEventBus eventBus;
  @Autowired private DObCollectionGeneralModelRep collectionGeneralModelRep;
  @Autowired private DObDebitNoteUpdateRemainingCommand debitNoteUpdateRemainingCommand;

  public AccountingNoteMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  @Transactional
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof UpdateDebitNoteEvent) {
      updateDebitNoteRemaining((UpdateDebitNoteEvent) event);
    }
  }

  private void updateDebitNoteRemaining(UpdateDebitNoteEvent updateDebitNoteEvent)
      throws Exception {
    DObCollectionGeneralModel collectionGeneralModel =
        collectionGeneralModelRep.findOneByUserCode(updateDebitNoteEvent.getUserCode());
    DocumentObjectUpdateRemainingValueObject valueObject =
        new DocumentObjectUpdateRemainingValueObject();
    valueObject.setUserCode(collectionGeneralModel.getRefDocumentCode());
    valueObject.setAmount(collectionGeneralModel.getAmount());
    StringBuilder userCode = new StringBuilder();
    debitNoteUpdateRemainingCommand
        .executeCommand(valueObject)
        .subscribe(code -> userCode.append(code));
    consumeCollectionRefDocumentUpdateRemainingSuccessEvent(userCode.toString());
  }

  private void consumeCollectionRefDocumentUpdateRemainingSuccessEvent(String salesOrderCode) {
    CollectionUpdateRefDocumentSuccessEvent event =
        new CollectionUpdateRefDocumentSuccessEvent(salesOrderCode);
    eventBus.registerObservable(CollectionUpdateRefDocumentSuccessEvent.class, event);
  }
}
