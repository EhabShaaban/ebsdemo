package com.ebs.dda.rx;

import com.ebs.dac.dbo.events.EBSEvent;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class ObservablesEventBus {

  private static final Logger LOGGER = LoggerFactory.getLogger(ObservablesEventBus.class);
  private static Subject<EBSEvent> subject = PublishSubject.create();
  private static ObservablesEventBus instance;
  private static Map<Object, Observable> observableMap;

  protected ObservablesEventBus() {}

  public static ObservablesEventBus getInstance() {
    if (instance == null) {
      observableMap = new HashMap();
      instance = new ObservablesEventBus();
    }
    return instance;
  }

  public Observable<Class> getObservable(Class className) {
    return observableMap.get(className);
  }

  public Observable<Object> getObservableObject(Class className) {
    return observableMap.get(className);
  }

  public void registerObservable(Class className, Object object) {
    observableMap.put(className, Observable.just(object));
  }

  public void registerObservable(Class className) {
    observableMap.put(className, Observable.just(className));
  }

  public void consumeEvent(EBSEvent ebsEvent) {
    subject.onNext(ebsEvent);
  }

  public void subscribe(IEventBusMonitorService monitorService){
    subject
        .doOnNext(monitorService::handleEvent)
        .doOnError(
            error ->{
                LOGGER.error(
                    String.format("Exception %s happened during handling an event", error.getStackTrace()));
            }
        )
        .retry()
        .subscribe();
  }

  public void subscribe(
      IEventBusMonitorService monitorService, IEventBusErrorHandler errorHandler) {
    subject.subscribe(monitorService::handleEvent, errorHandler::handleError);
  }
}
