package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.landedCost.LandedCostConvertToActualEvent;
import com.ebs.dda.commands.accounting.costing.DObCostingUpdateAfterRecostingCommand;
import com.ebs.dda.commands.inventory.storetransaction.TObGoodsReceiptStoreTransactionRecostingCommand;
import com.ebs.dda.inventory.goodsreceipt.statemachines.DObGoodsReceiptStateMachine;
import com.ebs.dda.jpa.accounting.costing.DObCostingDocumentRecostingValueObject;
import com.ebs.dda.jpa.accounting.landedcost.IObLandedCostDetailsGeneralModel;
import com.ebs.dda.jpa.inventory.goodsreceipt.DObGoodsReceiptGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.IObLandedCostDetailsGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsreceipt.DObGoodsReceiptGeneralModelRep;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LandedCostConvertToActualMonitorService implements IEventBusMonitorService {

  private final String PO_GR = "GR_PO";
  @Autowired private ObservablesEventBus eventBus;
  @Autowired private DObCostingUpdateAfterRecostingCommand costingUpdateAfterRecostingCommand;
  @Autowired private TObGoodsReceiptStoreTransactionRecostingCommand storeTransactionRecostingCommand;
  @Autowired private IObLandedCostDetailsGeneralModelRep landedCostDetailsGeneralModelRep;
  @Autowired private DObGoodsReceiptGeneralModelRep goodsReceiptPurchaseOrderRep;

  public LandedCostConvertToActualMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof LandedCostConvertToActualEvent) {
      handleLandedCostConvertToActual((LandedCostConvertToActualEvent) event);
    }
  }

  private void handleLandedCostConvertToActual(
      LandedCostConvertToActualEvent landedCostConvertToActualEvent) throws Exception {
    try {
      String landedCostCode = landedCostConvertToActualEvent.getUserCode();
      IObLandedCostDetailsGeneralModel landedCostDetailsGeneralModel =
          landedCostDetailsGeneralModelRep.findOneByLandedCostCode(landedCostCode);
      DObGoodsReceiptGeneralModel goodsReceiptGeneralModel =
          goodsReceiptPurchaseOrderRep
              .findOneByRefDocumentCodeAndInventoryDocumentTypeAndCurrentStatesLike(
                  landedCostDetailsGeneralModel.getPurchaseOrderCode(),
                  PO_GR,
                  "%" + DObGoodsReceiptStateMachine.ACTIVE_STATE + "%");
      if (goodsReceiptGeneralModel != null) {
        DObCostingDocumentRecostingValueObject costingDocumentRecostingValueObject =
            new DObCostingDocumentRecostingValueObject(
                goodsReceiptGeneralModel.getRefDocumentCode(),
                goodsReceiptGeneralModel.getUserCode(),
                landedCostCode);
        costingUpdateAfterRecostingCommand.executeCommand(costingDocumentRecostingValueObject);
        storeTransactionRecostingCommand.executeCommand(landedCostCode);

      }

    } catch (Exception ignored) {
      // TODO: Handling Exceptions will be later
    }
  }
}
