package com.ebs.dda.rx;

import com.ebs.dac.dbo.events.EBSEvent;

@FunctionalInterface
public interface IEventBusMonitorService {

  void handleEvent(EBSEvent event) throws Exception;
}
