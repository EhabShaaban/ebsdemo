package com.ebs.dda.rx.inventory;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.initialstockupload.InitialStockUploadActivateEvent;
import com.ebs.dda.commands.inventory.stockavailability.CObStockAvailabilityUpdateCommand;
import com.ebs.dda.jpa.inventory.DObInventoryDocumentActivateValueObject;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StockAvailabilityUpdateMonitorService implements IEventBusMonitorService {

  private ObservablesEventBus eventBus;

  @Autowired
  CObStockAvailabilityUpdateCommand stockAvailabilityUpdateCommand;

  public StockAvailabilityUpdateMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof InitialStockUploadActivateEvent) {
      String initialStockCode = ((InitialStockUploadActivateEvent) event).getUserCode();
      handleUpdateStockAvailability(initialStockCode, "ISU");
    }
  }

  private void handleUpdateStockAvailability(String userCode, String type) {
    DObInventoryDocumentActivateValueObject valueObject = new DObInventoryDocumentActivateValueObject();
    valueObject.setInventoryType(type);
    valueObject.setUserCode(userCode);
    stockAvailabilityUpdateCommand.executeCommand(valueObject);
  }
}
