package com.ebs.dda.rx.inventory;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.initialstockupload.InitialStockUploadActivateEvent;
import com.ebs.dda.commands.inventory.storetransaction.TObStoreTransactionCreateInitialStockUploadCommand;
import com.ebs.dda.jpa.inventory.storetransaction.TObStoreTransactionCreateInitialStockUploadValueObject;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InitialStockUploadActivateMonitorService implements IEventBusMonitorService {

  private final ObservablesEventBus eventBus;

  @Autowired
  private TObStoreTransactionCreateInitialStockUploadCommand
      storeTransactionCreateInitialStockUploadCommand;

  public InitialStockUploadActivateMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof InitialStockUploadActivateEvent) {

      InitialStockUploadActivateEvent initialStockUploadActivateEvent =
          (InitialStockUploadActivateEvent) event;
      String initialStockUploadUserCode = initialStockUploadActivateEvent.getUserCode();
      Long initialStockUploadId = initialStockUploadActivateEvent.getGoodsIssueId();

      TObStoreTransactionCreateInitialStockUploadValueObject storeValueObject =
          new TObStoreTransactionCreateInitialStockUploadValueObject();
      storeValueObject.setInitialStockUploadCode(initialStockUploadUserCode);
      storeValueObject.setInitialStockUploadId(initialStockUploadId);
      storeTransactionCreateInitialStockUploadCommand.executeCommand(storeValueObject);
    }
  }
}
