package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoiceUpdateItemsEvent;
import com.ebs.dda.commands.accounting.salesinvoice.IObSalesInvoiceTaxesUpdateCommand;
import com.ebs.dda.jpa.accounting.IObInvoiceTaxesValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesInvoiceUpdateItemsMonitorService implements IEventBusMonitorService {

	@Autowired
	private ObservablesEventBus eventBus;
	@Autowired
	private IObSalesInvoiceTaxesUpdateCommand taxesUpdateCommand;

	public SalesInvoiceUpdateItemsMonitorService(ObservablesEventBus eventBus) {
		this.eventBus = eventBus;
		this.eventBus.subscribe(this);
	}

	@Override
	public void handleEvent(EBSEvent event) {
		if (event instanceof SalesInvoiceUpdateItemsEvent) {
			handleSalesInvoiceUpdateItemsEvent((SalesInvoiceUpdateItemsEvent) event);
		}
	}

	private void handleSalesInvoiceUpdateItemsEvent(SalesInvoiceUpdateItemsEvent itemsEvent) {

		DObSalesInvoice salesInvoice = (DObSalesInvoice) itemsEvent.getDocumentObject();
		IObInvoiceTaxesValueObject valueObject = new IObInvoiceTaxesValueObject(salesInvoice);
		final StringBuilder userCode = new StringBuilder();
		try {
			taxesUpdateCommand.executeCommand(valueObject).subscribe(code -> userCode.append(code));
		} catch (Exception ignored) {
			// TODO: Handling Exceptions will be later
		}
	}

}
