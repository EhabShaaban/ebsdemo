package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.vendorinvoice.VendorInvoiceCreateSuccessEvent;
import com.ebs.dda.commands.accounting.vendorinvoice.IObVendorInvoiceAddTaxesCommandExecuter;
import com.ebs.dda.commands.accounting.vendorinvoice.IObVendorInvoiceSummaryAddCommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VendorInvoiceCreateMonitorService implements IEventBusMonitorService {

  @Autowired private ObservablesEventBus eventBus;

  @Autowired private IObVendorInvoiceAddTaxesCommandExecuter addTaxesCommandExecuter;
  @Autowired private IObVendorInvoiceSummaryAddCommand SummaryAddCommand;

  public VendorInvoiceCreateMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof VendorInvoiceCreateSuccessEvent) {
      handleVendorInvoiceCreateSuccessEvent((VendorInvoiceCreateSuccessEvent) event);
    }
  }

  private void handleVendorInvoiceCreateSuccessEvent(VendorInvoiceCreateSuccessEvent event) {
    DObVendorInvoice vendorInvoice = event.getVendorInvoice();
    final StringBuilder userCode = new StringBuilder();
    try {
      addTaxesCommandExecuter.executeCommand(vendorInvoice).subscribe(code -> userCode.append(code));
      SummaryAddCommand.executeCommand(vendorInvoice).subscribe(code -> userCode.append(code));
    } catch (Exception ignored) {
      // TODO: Handling Exceptions will be later
    }
  }
}
