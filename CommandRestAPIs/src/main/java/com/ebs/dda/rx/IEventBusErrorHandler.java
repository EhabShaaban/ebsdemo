package com.ebs.dda.rx;

public interface IEventBusErrorHandler {
    void handleError(Throwable throwable);
}
