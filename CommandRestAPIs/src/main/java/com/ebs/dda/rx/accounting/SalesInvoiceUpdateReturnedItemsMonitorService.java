package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.order.salesreturnorder.SalesReturnOrderMarkAsShippedSuccessEvent;
import com.ebs.dda.commands.accounting.salesinvoice.IObSalesInvoiceUpdateReturnedItemsCommand;
import com.ebs.dda.jpa.order.salesreturnorder.IObSalesReturnOrderItemValueObject;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesInvoiceUpdateReturnedItemsMonitorService implements IEventBusMonitorService {

  @Autowired
  private ObservablesEventBus eventBus;
  @Autowired
  private IObSalesInvoiceUpdateReturnedItemsCommand updateReturnedItemsCommand;

  public SalesInvoiceUpdateReturnedItemsMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof SalesReturnOrderMarkAsShippedSuccessEvent) {
      handleSalesInvoiceUpdateReturnedItemsEvent((SalesReturnOrderMarkAsShippedSuccessEvent) event);
    }
  }

  private void handleSalesInvoiceUpdateReturnedItemsEvent(SalesReturnOrderMarkAsShippedSuccessEvent returnedItemsEvent) {
    String salesReturnOrderCode = returnedItemsEvent.getUserCode();
    IObSalesReturnOrderItemValueObject valueObject = new IObSalesReturnOrderItemValueObject(salesReturnOrderCode);
    final StringBuilder userCode = new StringBuilder();
    try {
      updateReturnedItemsCommand.executeCommand(valueObject).subscribe(code -> userCode.append(code));
    } catch (Exception ignored) {
      // TODO: Handling Exceptions will be later
    }
  }

}
