package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoiceJournalEntryCreatedSuccessEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoicePostSuccessEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoiceUpdateStateSuccesEvent;
import com.ebs.dda.commands.accounting.invoice.IObInvoiceTaxCopyCommand;
import com.ebs.dda.commands.accounting.journalentry.create.salesinvoice.DObJournalEntryCreateSalesInvoiceCommand;
import com.ebs.dda.jpa.accounting.IObInvoiceTaxesValueObject;
import com.ebs.dda.jpa.accounting.journalentry.DObJournalEntryCreateValueObject;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.jpa.accounting.salesinvoice.IObSalesInvoiceTaxes;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesInvoicePostMonitorService implements IEventBusMonitorService {

  @Autowired private ObservablesEventBus eventBus;

  @Autowired private DObJournalEntryCreateSalesInvoiceCommand journalEntryCreateSalesInvoiceCommand;

  @Autowired
  private IObInvoiceTaxCopyCommand<DObSalesInvoice, IObSalesInvoiceTaxes>
      salesInvoiceTaxCopyCommand;

  public SalesInvoicePostMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    if (event instanceof SalesInvoicePostSuccessEvent) {
      copyTaxPercentage((SalesInvoicePostSuccessEvent) event);
      createJournalentry((SalesInvoicePostSuccessEvent) event);
    }
  }

  private void consumeUpdateRefDocumentStateSuccessEvent(String userCode) {
    SalesInvoiceUpdateStateSuccesEvent event = new SalesInvoiceUpdateStateSuccesEvent(userCode);
    eventBus.registerObservable(SalesInvoiceUpdateStateSuccesEvent.class, event);
  }

  private void createJournalentry(SalesInvoicePostSuccessEvent salesInvoicePostEvent)
      throws Exception {

    String salesInvoiceCode = salesInvoicePostEvent.getUserCode();

    DObJournalEntryCreateValueObject valueObject = new DObJournalEntryCreateValueObject();
    valueObject.setUserCode(salesInvoiceCode);

    final StringBuilder userCode = new StringBuilder();
    try {
      journalEntryCreateSalesInvoiceCommand
          .executeCommand(valueObject)
          .subscribe(code -> userCode.append(code));
      registerCreateJournalSuccessEvent(String.valueOf(userCode));
    } catch (Exception ignored) {
      // TODO: Handling Exceptions will be later
    }
  }

  private void copyTaxPercentage(SalesInvoicePostSuccessEvent event) throws Exception {
    IObInvoiceTaxesValueObject valueObject =
        new IObInvoiceTaxesValueObject(event.getDocumentObject());
    salesInvoiceTaxCopyCommand.executeCommand(valueObject);
  }

  private void registerCreateJournalSuccessEvent(String userCode) {
    SalesInvoiceJournalEntryCreatedSuccessEvent event =
        new SalesInvoiceJournalEntryCreatedSuccessEvent(userCode);
    eventBus.registerObservable(SalesInvoiceJournalEntryCreatedSuccessEvent.class, event);
  }
}
