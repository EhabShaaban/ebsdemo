package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.vendorinvoice.VendorInvoiceSaveItemSuccessEvent;
import com.ebs.dda.commands.accounting.vendorinvoice.IObVendorInvoiceTaxesUpdateCommand;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoice;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VendorInvoiceSaveItemMonitorService implements IEventBusMonitorService {

	@Autowired
	private ObservablesEventBus eventBus;

	@Autowired
	private IObVendorInvoiceTaxesUpdateCommand taxesUpdateCommand;

	public VendorInvoiceSaveItemMonitorService(ObservablesEventBus eventBus) {
		this.eventBus = eventBus;
		this.eventBus.subscribe(this);
	}

	@Override
	public void handleEvent(EBSEvent event) throws Exception {
		if (event instanceof VendorInvoiceSaveItemSuccessEvent) {
			handleVendorInvoiceVendorInvoiceSaveItemSuccessEvent(
							(VendorInvoiceSaveItemSuccessEvent) event);
		}
	}

	private void handleVendorInvoiceVendorInvoiceSaveItemSuccessEvent(
					VendorInvoiceSaveItemSuccessEvent event) {
		DObVendorInvoice valueObject = event.getVendorInvoice();
		final StringBuilder userCode = new StringBuilder();
		try {
			taxesUpdateCommand.executeCommand(valueObject)
							.subscribe(code -> userCode.append(code));
		} catch (Exception ignored) {
			// TODO: Handling Exceptions will be later
		}
	}
}
