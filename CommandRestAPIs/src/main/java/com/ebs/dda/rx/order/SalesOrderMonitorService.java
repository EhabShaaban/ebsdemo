package com.ebs.dda.rx.order;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.collection.CollectionUpdateRefDocumentSuccessEvent;
import com.ebs.dac.dbo.events.collection.UpdateSalesOrderEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoicePostSuccessEvent;
import com.ebs.dda.accounting.salesinvoice.statemachines.DObSalesInvoiceStateMachine;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderInvoicedCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderIssuedCommand;
import com.ebs.dda.commands.order.salesorder.DObSalesOrderUpdateRemainingCommand;
import com.ebs.dda.inventory.goodsissue.statemachines.DObGoodsIssueStateMachine;
import com.ebs.dda.jpa.DocumentObjectUpdateRemainingValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.SalesInvoiceTypeEnum.SalesInvoiceType;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderUpdateStateValueObject;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.inventory.goodsissue.DObGoodsIssueDataGeneralModelRep;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class SalesOrderMonitorService implements IEventBusMonitorService {

  @Autowired private ObservablesEventBus eventBus;
  @Autowired private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  @Autowired private DObCollectionGeneralModelRep collectionGeneralModelRep;
  @Autowired private DObSalesOrderInvoicedCommand salesOrderInvoicedCommand;
  @Autowired private DObSalesOrderUpdateRemainingCommand salesOrderUpdateRemainingCommand;

  public SalesOrderMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  @Transactional
  public void handleEvent(EBSEvent event) throws Exception {
      if (event instanceof SalesInvoicePostSuccessEvent) {
          changeSalesOrderStateToInvoiced((SalesInvoicePostSuccessEvent) event);
      }
      if (event instanceof UpdateSalesOrderEvent) {
          updateSalesOrderRemaining((UpdateSalesOrderEvent) event);
      }
  }

  private DObSalesInvoiceGeneralModel checkIfSalesInvoiceOfTypeSalesOrder(
      SalesInvoicePostSuccessEvent salesInvoicePostSuccessEvent) throws Exception {
    String salesInvoiceCode = salesInvoicePostSuccessEvent.getUserCode();

      String deliveredToCustomerState =
              "%" + DObSalesInvoiceStateMachine.DELIVERED_TO_CUSTOMER_STATE + "%";

      return salesInvoiceGeneralModelRep.findOneByUserCodeAndInvoiceTypeCodeAndCurrentStatesLike(
              salesInvoiceCode,
              SalesInvoiceType.SALES_INVOICE_FOR_SALES_ORDER.name(),
              deliveredToCustomerState);
  }

  private void changeSalesOrderStateToInvoiced(
      SalesInvoicePostSuccessEvent salesInvoicePostSuccessEvent) throws Exception {
    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        checkIfSalesInvoiceOfTypeSalesOrder(salesInvoicePostSuccessEvent);

    if (salesInvoiceGeneralModel == null) {
      return;
    }

    DObSalesOrderUpdateStateValueObject valueObject = new DObSalesOrderUpdateStateValueObject();
    valueObject.setSalesOrderCode(salesInvoiceGeneralModel.getSalesOrder());

    final StringBuilder userCode = new StringBuilder();
    try {
      salesOrderInvoicedCommand
          .executeCommand(valueObject)
          .subscribe(code -> userCode.append(code));

    } catch (Exception ignored) {
      // TODO: Handling Exceptions will be later
    }
  }

  private void updateSalesOrderRemaining(UpdateSalesOrderEvent updateSalesOrderEvent) throws Exception{
    DObCollectionGeneralModel collectionGeneralModel = collectionGeneralModelRep.findOneByUserCode(updateSalesOrderEvent.getUserCode());
    DocumentObjectUpdateRemainingValueObject salesOrderUpdateRemainingValueObject = new DocumentObjectUpdateRemainingValueObject();
    salesOrderUpdateRemainingValueObject.setUserCode(collectionGeneralModel.getRefDocumentCode());
    salesOrderUpdateRemainingValueObject.setAmount(collectionGeneralModel.getAmount());
    StringBuilder userCode = new StringBuilder();
    salesOrderUpdateRemainingCommand.executeCommand(salesOrderUpdateRemainingValueObject).subscribe(code -> userCode.append(code));
    consumeSalesOrderUpdateRemainingSuccessEvent(userCode.toString());
  }

  private void consumeSalesOrderUpdateRemainingSuccessEvent(String salesOrderCode) {
    CollectionUpdateRefDocumentSuccessEvent event = new CollectionUpdateRefDocumentSuccessEvent(salesOrderCode);
    eventBus.registerObservable(CollectionUpdateRefDocumentSuccessEvent.class, event);
  }
}
