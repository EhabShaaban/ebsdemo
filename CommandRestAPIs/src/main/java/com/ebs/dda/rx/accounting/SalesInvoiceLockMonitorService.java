package com.ebs.dda.rx.accounting;

import com.ebs.dac.dbo.events.EBSEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoiceLockRequestEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoiceLockSuccessEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoiceUnlockRequestEvent;
import com.ebs.dac.dbo.events.sales.SalesInvoiceUnlockSuccessEvent;
import com.ebs.dda.commands.general.EntityLockCommand;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoice;
import com.ebs.dda.rx.IEventBusMonitorService;
import com.ebs.dda.rx.ObservablesEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesInvoiceLockMonitorService implements IEventBusMonitorService {

  @Autowired
  private ObservablesEventBus eventBus;
  @Autowired
  private EntityLockCommand<DObSalesInvoice> entityLockCommand;

  public SalesInvoiceLockMonitorService(ObservablesEventBus eventBus) {
    this.eventBus = eventBus;
    this.eventBus.subscribe(this);
  }

  @Override
  public void handleEvent(EBSEvent event) throws Exception {
    try {
      if (event instanceof SalesInvoiceLockRequestEvent) {
        lockSalesInvoice((SalesInvoiceLockRequestEvent) event);
      } else if (event instanceof SalesInvoiceUnlockRequestEvent) {
        unlockSalesInvoice((SalesInvoiceUnlockRequestEvent) event);
      }
    } catch (Exception ex) {
      // TODO handle error in creating journal entry to avoid problems in rx java subject
    }
  }

  private void unlockSalesInvoice(SalesInvoiceUnlockRequestEvent event) throws Exception {
    String salesInvoiceCode = event.getUserCode();
    entityLockCommand.unlockAllSections(salesInvoiceCode);
    consumeSalesInvoiceUnlockSuccessEvent(salesInvoiceCode);
  }

  private void consumeSalesInvoiceUnlockSuccessEvent(String salesInvoiceCode) {
    SalesInvoiceUnlockSuccessEvent event = new SalesInvoiceUnlockSuccessEvent(salesInvoiceCode);
    eventBus.registerObservable(SalesInvoiceUnlockSuccessEvent.class, event);
  }

  private void lockSalesInvoice(SalesInvoiceLockRequestEvent event) throws Exception {
    String salesInvoiceCode = event.getUserCode();
    entityLockCommand.lockAllSections(salesInvoiceCode);
    consumeLockSuccessEvent(salesInvoiceCode);
  }

  private void consumeLockSuccessEvent(String userCode) {
    SalesInvoiceLockSuccessEvent event = new SalesInvoiceLockSuccessEvent(userCode);
    eventBus.registerObservable(SalesInvoiceLockSuccessEvent.class, event);
  }

}
