package com.ebs.dda.functions.fakeobjects;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public abstract class FakeAbstractDependantCommandObserver
    implements Observer<ExecuteActivateDependanciesContext> {

  private ICommand command;

  public FakeAbstractDependantCommandObserver(ICommand command) throws Exception {
    this.command = command;
  }

  @Override
  public void onSubscribe(Disposable d) {}

  @Override
  public void onError(Throwable e) {}

  @Override
  public void onComplete() {}

  public ICommand getCommand() {
    return command;
  }
}
