package com.ebs.dda.functions.fakeobjects;

import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.usercodes.DefaultUserCode;

public class FakeStatefulBusinessObject extends StatefullBusinessObject<DefaultUserCode> {

  private static final long serialVersionUID = 1L;
  public static final String FAKE_STATEFUL_OBJECT_NAME = "FakeStatefulBusinessObject";

  public FakeStatefulBusinessObject(String userCode) {
    this.setUserCode(userCode);
  }

  @Override
  public String getSysName() {
    return FAKE_STATEFUL_OBJECT_NAME;
  }

}
