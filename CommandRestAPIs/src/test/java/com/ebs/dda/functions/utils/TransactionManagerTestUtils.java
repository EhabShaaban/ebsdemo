package com.ebs.dda.functions.utils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;

public class TransactionManagerTestUtils {

  private PlatformTransactionManager platformTransactionManager;

  public TransactionManagerTestUtils() {
    platformTransactionManager = mockPlatformTransactionManager();
  }

  public TransactionManagerDelegate mockCommittedTransaction() {
    TransactionManagerDelegate transactionManager = new TransactionManagerDelegate();
    doNothing().when(platformTransactionManager).commit(any());
    transactionManager.setTransactionManager(platformTransactionManager);
    return transactionManager;
  }

  private PlatformTransactionManager mockPlatformTransactionManager() {
    PlatformTransactionManager platformTransactionManager = mock(PlatformTransactionManager.class);
    TransactionStatus transactionStatus = mock(TransactionStatus.class);
    doReturn(transactionStatus).when(platformTransactionManager).getTransaction(any());
    return platformTransactionManager;
  }

  public PlatformTransactionManager getPlatformTransactionManager() {
    return platformTransactionManager;
  }

}
