package com.ebs.dda.functions.fakeobjects;

import com.ebs.dac.foundation.exceptions.localization.IExceptionsCodes;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.functions.activate.ActivateContext;
import com.ebs.dda.functions.authorize.AuthorizeContext;
import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.executecommand.ExecuteCommandContext;
import com.ebs.dda.functions.executequery.EntitiesOfInterestContainer;
import com.ebs.dda.functions.executequery.FindEntityContext;
import com.ebs.dda.functions.lock.LockContext;
import com.ebs.dda.functions.serialize.DeserializeValueObjectContext;
import com.ebs.dda.functions.serialize.SerializeActivateResponseContext;
import com.ebs.dda.functions.validate.ValidateBusinessRulesContext;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowedContext;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForStateContext;
import com.ebs.dda.functions.validate.ValidateSchemaContext;

public class FakeActivateContextBuilder {

  private FakeActivateContextBuilder() {}

  public static FakeActivateContext newContext() {
    return new FakeActivateContext();
  }

  public static class FakeActivateContext
      implements ActivateContext,
          AuthorizeContext,
          FindEntityContext,
          ValidateSchemaContext,
          LockContext,
          DeserializeValueObjectContext<FakeActivateValueObject>,
          ValidateIfActionIsAllowedContext,
          ValidateMissingFieldsForStateContext,
          ValidateBusinessRulesContext<FakeActivateValueObject>,
          ExecuteCommandContext<FakeActivateValueObject>,
          ExecuteActivateDependanciesContext,
          SerializeActivateResponseContext {

    public static final String ANY_ACTIVATION_VALIDATION_SCHEMA = "{}";

    private String objectCode;
    private String activateData;
    private FakeActivateValueObject valueObject;
    private String response;
    private String journalEntryUserCode;
    private EntitiesOfInterestContainer entitiesOfInstrestContainer;

    public FakeActivateContext() {
      this.entitiesOfInstrestContainer =
          new EntitiesOfInterestContainer(FakeStatefulBusinessObject.class);
    }

    @Override
    public String objectCode() {
      return objectCode;
    }

    @Override
    public String activateData() {
      return activateData;
    }

    @Override
    public String serializeResponse() {
      return response;
    }

    @Override
    public String objectSysName() {
      return FakeStatefulBusinessObject.FAKE_STATEFUL_OBJECT_NAME;
    }

    @Override
    public String actionName() {
      return IActionsNames.ACTIVATE;
    }

    @Override
    public String state() {
      return BasicStateMachine.ACTIVE_STATE;
    }

    public FakeActivateContext objectCode(String objectCode) {
      this.objectCode = objectCode;
      return this;
    }

    public FakeActivateContext valueObject(String valueObject) {
      this.activateData = valueObject;
      return this;
    }

    @Override
    public String schema() {
      return ANY_ACTIVATION_VALIDATION_SCHEMA;
    }

    @Override
    public Class<FakeActivateValueObject> valueObjectClass() {
      return FakeActivateValueObject.class;
    }

    @Override
    public void valueObject(FakeActivateValueObject valueObject) {
      this.valueObject = valueObject;
    }

    @Override
    public FakeActivateValueObject valueObject() {
      return this.valueObject;
    }

    @Override
    public void response(String response) {
      this.response = response;
    }

    @Override
    public String successMessage() {
      return IExceptionsCodes.SUCCESSFUL_ACTIVATION_CODE;
    }

    @Override
    public String journalEntryUserCode() {
      return this.journalEntryUserCode;
    }

    @Override
    public void journalEntryUserCode(String userCode) {
      this.journalEntryUserCode = userCode;
    }

    @Override
    public EntitiesOfInterestContainer entitiesOfInterestContainer() {
      return this.entitiesOfInstrestContainer;
    }
  }
}
