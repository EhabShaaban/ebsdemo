package com.ebs.dda.functions.fakeobjects;

import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.functions.executecommand.ExecuteCommand;

public class FakeActivateCommandObserver extends ExecuteCommand<FakeActivateValueObject> {

  private ICommand<FakeActivateValueObject> command;

  public FakeActivateCommandObserver(ICommand<FakeActivateValueObject> command) {
    super(command);
    this.command = command;
  }

  public ICommand<FakeActivateValueObject> getCommand() {
    return command;
  }

}
