package com.ebs.dda.functions.utils;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.SchemaValidator;
import com.ebs.dda.functions.validate.ValidateSchema;

public class SchemaValidatorTestUtils {

  private SchemaValidator schemaValidator;

  public SchemaValidatorTestUtils() {
    schemaValidator = mock(SchemaValidator.class);
  }

  public ValidateSchema mockValidJsonSchema(String activationSchema, String requestBody)
      throws Exception {
    doNothing().when(schemaValidator).validateRequestBodySchema(eq(requestBody),
        eq(activationSchema));
    return new ValidateSchema(schemaValidator);
  }

  public ValidateSchema mockInvalidJsonSchema(String activationSchema, String requestBody)
      throws Exception {
    doThrow(ArgumentViolationSecurityException.class).when(schemaValidator)
        .validateRequestBodySchema(eq(requestBody), eq(activationSchema));
    return new ValidateSchema(schemaValidator);
  }

  public SchemaValidator getSchemaValidator() {
    return schemaValidator;
  }

}
