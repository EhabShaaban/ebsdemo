package com.ebs.dda.functions.fakeobjects;

import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.utils.ExecutionCommandsTestUtils;

public class FakeSuccessDependantCommandObserver extends FakeAbstractDependantCommandObserver {

  public FakeSuccessDependantCommandObserver() throws Exception {
    super(ExecutionCommandsTestUtils.mockSuccessCommand());
  }

  @Override
  public void onNext(ExecuteActivateDependanciesContext t) {
    try {
      getCommand().executeCommand(t.entitiesOfInterestContainer().getMainEntity());
    } catch (Exception e) {
    }
  }
}
