package com.ebs.dda.functions.utils;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.entities.interfaces.IStatefullBusinessObject;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.realization.statemachine.AbstractStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;

public class StateMachineTestUtils {

  private AbstractStateMachine stateMachine;

  public StateMachineTestUtils() {
    stateMachine = mock(AbstractStateMachine.class);
  }

  public ValidateIfActionIsAllowed mockActivateActionToBeAllowed(
      StatefullBusinessObject<?> businessObject) {

    try {
      doNothing().when(stateMachine).initObjectState((IStatefullBusinessObject) eq(businessObject));
      doNothing().when(stateMachine).isAllowedAction(eq(IActionsNames.ACTIVATE));
    } catch (Exception e) {
    }

    return new ValidateIfActionIsAllowed(stateMachine);
  }

  public ValidateIfActionIsAllowed mockActivateActionToBeNotAllowed(
      StatefullBusinessObject<?> businessObject) throws Exception {

    doNothing().when(stateMachine).initObjectState((IStatefullBusinessObject) eq(businessObject));
    doThrow(ActionNotAllowedPerStateException.class).when(stateMachine)
        .isAllowedAction(eq(IActionsNames.ACTIVATE));
    return new ValidateIfActionIsAllowed(stateMachine);
  }

  public AbstractStateMachine getStateMachine() {
    return stateMachine;
  }
}
