package com.ebs.dda.functions.utils;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.data.MissingFieldsException;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.functions.fakeobjects.FakeActivateValueObject;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.validation.IActivateBusinessRulesValidator;
import com.ebs.dda.validation.IMissingFieldsValidator;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class ValidationTestUtils {

  private IMissingFieldsValidator missingFieldsValidator;
  private IActivateBusinessRulesValidator<FakeActivateValueObject> businessRulesValidator;


  public ValidationTestUtils() {
    missingFieldsValidator = mock(IMissingFieldsValidator.class);
    businessRulesValidator = mock(IActivateBusinessRulesValidator.class);
  }

  public ValidateMissingFieldsForState mockActivateNoMissingFields(String objectCode) {
    doReturn(Collections.emptyMap()).when(missingFieldsValidator)
        .validateMissingFields(eq(objectCode), eq(BasicStateMachine.ACTIVE_STATE));

    return new ValidateMissingFieldsForState(missingFieldsValidator);
  }

  public ValidateMissingFieldsForState mockActivateMissingFields(String objectCode) {
    doThrow(MissingFieldsException.class).when(missingFieldsValidator)
        .validateMissingFields(eq(objectCode), eq(BasicStateMachine.ACTIVE_STATE));

    return new ValidateMissingFieldsForState(missingFieldsValidator);
  }

  public ValidateBusinessRules<FakeActivateValueObject> mockSuccessActivateBusinessRules() {
    ValidationResult validationResult = mock(ValidationResult.class);
    doReturn(true).when(validationResult).isValid();
    doReturn(validationResult).when(businessRulesValidator).validateBusinessRules(any());

    return new ValidateBusinessRules<>(businessRulesValidator);
  }

  public ValidateBusinessRules<FakeActivateValueObject> mockFailedActivateBusinessRules() {
    doThrow(BusinessRuleViolationException.class).when(businessRulesValidator)
        .validateBusinessRules(any());

    return new ValidateBusinessRules<>(businessRulesValidator);
  }

  public IMissingFieldsValidator getMissingFieldsValidator() {
    return missingFieldsValidator;
  }

  public IActivateBusinessRulesValidator<FakeActivateValueObject> getBusinessRulesValidator() {
    return businessRulesValidator;
  }

}
