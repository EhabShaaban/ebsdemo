package com.ebs.dda.functions.utils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import com.ebs.dda.functions.fakeobjects.FakeActivateValueObject;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.serialize.SerializionUtils;

public class SerializationTestUtils {

  private SerializionUtils serializionUtils;

  public SerializationTestUtils() {
    serializionUtils = mock(SerializionUtils.class);
  }

  public DeserializeValueObject<FakeActivateValueObject> mockSuccessDeserializedActivateValueObject(
      String valueObjectAsString, Class<FakeActivateValueObject> valueObjectCls,
      String objectCode) {

    FakeActivateValueObject valueObject = new FakeActivateValueObject(objectCode);
    doReturn(valueObject).when(serializionUtils).deserializeActivateValueObject(
        eq(valueObjectAsString), eq(valueObjectCls), eq(objectCode));

    return new DeserializeValueObject<>(serializionUtils);
  }

  public SerializeActivateSuccessResponse mockSuccessActivateResponse() {
    doReturn("").when(serializionUtils).serializeActivateSuccessResponse(any(), any());
    return new SerializeActivateSuccessResponse(serializionUtils);
  }

  public SerializionUtils getSerializionUtils() {
    return serializionUtils;
  }
}
