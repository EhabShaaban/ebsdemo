package com.ebs.dda.functions.activate;

import com.ebs.dda.functions.fakeobjects.FakeActivateValueObject;
import com.ebs.dda.functions.fakeobjects.FakeStatefulBusinessObject;
import com.ebs.dda.functions.utils.AuthorizationTestUtils;
import com.ebs.dda.functions.utils.EntityRepositoryTestUtils;
import com.ebs.dda.functions.utils.ExecutionCommandsTestUtils;
import com.ebs.dda.functions.utils.LockTestUtils;
import com.ebs.dda.functions.utils.SchemaValidatorTestUtils;
import com.ebs.dda.functions.utils.SerializationTestUtils;
import com.ebs.dda.functions.utils.StateMachineTestUtils;
import com.ebs.dda.functions.utils.TransactionManagerTestUtils;
import com.ebs.dda.functions.utils.ValidationTestUtils;
import io.reactivex.Observer;

public class ActivateTestUtilsContext {

  private AuthorizationTestUtils authorizationTestUtils;
  private SchemaValidatorTestUtils schemaValidatorTestUtils;
  private EntityRepositoryTestUtils entityRepositoryTestUtils;
  private LockTestUtils lockTestUtils;
  private SerializationTestUtils serializationTestUtils;
  private StateMachineTestUtils stateMachineTestUtils;
  private ValidationTestUtils validationTestUtils;
  private ExecutionCommandsTestUtils executionCommandsTestUtils;
  private TransactionManagerTestUtils transactionManagerTestUtils;
  private FakeStatefulBusinessObject businessObject;
  private Observer<?>[] activateDependentCommands;
  private FakeActivateValueObject valueObjectAsObject;
  private Observer<?>[] entitiesOfInterest;

  public ActivateTestUtilsContext() {
    authorizationTestUtils = new AuthorizationTestUtils();
    schemaValidatorTestUtils = new SchemaValidatorTestUtils();
    entityRepositoryTestUtils = new EntityRepositoryTestUtils();
    lockTestUtils = new LockTestUtils();
    serializationTestUtils = new SerializationTestUtils();
    stateMachineTestUtils = new StateMachineTestUtils();
    validationTestUtils = new ValidationTestUtils();
    executionCommandsTestUtils = new ExecutionCommandsTestUtils();
    transactionManagerTestUtils = new TransactionManagerTestUtils();
  }

  public static ActivateTestUtilsContext init() {
    return new ActivateTestUtilsContext();
  }

  public AuthorizationTestUtils getAuthorizationTestUtils() {
    return authorizationTestUtils;
  }

  public SchemaValidatorTestUtils getSchemaValidatorTestUtils() {
    return schemaValidatorTestUtils;
  }

  public EntityRepositoryTestUtils getEntityRepositoryTestUtils() {
    return entityRepositoryTestUtils;
  }

  public LockTestUtils getLockTestUtils() {
    return lockTestUtils;
  }

  public SerializationTestUtils getSerializationTestUtils() {
    return serializationTestUtils;
  }

  public StateMachineTestUtils getStateMachineTestUtils() {
    return stateMachineTestUtils;
  }

  public ValidationTestUtils getValidationTestUtils() {
    return validationTestUtils;
  }

  public ExecutionCommandsTestUtils getExecutionCommandsTestUtils() {
    return executionCommandsTestUtils;
  }

  public TransactionManagerTestUtils getTransactionManagerTestUtils() {
    return transactionManagerTestUtils;
  }

  public void setBusinessObject(FakeStatefulBusinessObject businessObject) {
    this.businessObject = businessObject;
  }

  public FakeStatefulBusinessObject getBusinessObject() {
    return businessObject;
  }

  public void setDependantCommands(Observer<?>[] activateDependentCommands) {
    this.activateDependentCommands = activateDependentCommands;
  }

  public Observer<?>[] getActivateDependentCommands() {
    return activateDependentCommands;
  }

  public void setValueObjectAsObject(FakeActivateValueObject valueObjectAsObject) {
    this.valueObjectAsObject = valueObjectAsObject;
  }

  public FakeActivateValueObject getValueObjectAsObject() {
    return valueObjectAsObject;
  }

  public void setEntitiesOfInterest(Observer<?>[] entitiesOfInterest) {
    this.entitiesOfInterest = entitiesOfInterest;
  }

  public Observer<?>[] getEntitiesOfInterest() {
    return entitiesOfInterest;
  }
}
