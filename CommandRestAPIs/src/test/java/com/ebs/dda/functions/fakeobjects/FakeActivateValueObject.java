package com.ebs.dda.functions.fakeobjects;

import com.ebs.dda.jpa.IValueObject;

public class FakeActivateValueObject implements IValueObject {

  private String userCode;

  public FakeActivateValueObject(String objectCode) {
    this.userCode(objectCode);
  }

  @Override
  public void userCode(String userCode) {
    this.userCode = userCode;
  }
  
  public String getUserCode() {
    return userCode;
  }

}
