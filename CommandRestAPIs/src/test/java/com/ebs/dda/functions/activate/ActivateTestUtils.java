package com.ebs.dda.functions.activate;

import com.ebs.dac.foundation.exceptions.data.BusinessRuleViolationException;
import com.ebs.dac.foundation.exceptions.data.MissingFieldsException;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dac.foundation.exceptions.operational.locking.AlreadyLockedException;
import com.ebs.dac.foundation.exceptions.operational.other.ActionNotAllowedPerStateException;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.infrastructure.exception.TransactionFailedException;
import com.ebs.dac.infrastructure.tm.TransactionManagerDelegate;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;
import com.ebs.dda.functions.executecommand.ExecuteCommand;
import com.ebs.dda.functions.fakeobjects.ActivateFakeStatefulBusinessObject;
import com.ebs.dda.functions.fakeobjects.FakeActivateContextBuilder;
import com.ebs.dda.functions.fakeobjects.FakeActivateContextBuilder.FakeActivateContext;
import com.ebs.dda.functions.fakeobjects.FakeActivateValueObject;
import com.ebs.dda.functions.fakeobjects.FakeStatefulBusinessObject;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import com.ebs.dda.functions.serialize.DeserializeValueObject;
import com.ebs.dda.functions.serialize.SerializeActivateSuccessResponse;
import com.ebs.dda.functions.utils.EntityRepositoryTestUtils;
import com.ebs.dda.functions.validate.ValidateBusinessRules;
import com.ebs.dda.functions.validate.ValidateIfActionIsAllowed;
import com.ebs.dda.functions.validate.ValidateMissingFieldsForState;
import com.ebs.dda.functions.validate.ValidateSchema;
import io.reactivex.Observer;
import org.junit.Assert;

import static com.ebs.dda.functions.fakeobjects.FakeActivateContextBuilder.FakeActivateContext.ANY_ACTIVATION_VALIDATION_SCHEMA;
import static com.ebs.dda.functions.fakeobjects.FakeStatefulBusinessObject.FAKE_STATEFUL_OBJECT_NAME;

public class ActivateTestUtils {
  private static final String ANY_USER_CODE = "2021000001";
  public static final String ANY_ACTIVATE_REQUEST_BODY = "{\"dueDate\":\"06-Mar-2021 09:30 AM\"}";

  private ActivateTestUtilsContext utilsContext;
  private FakeActivateContext fakeActivateContext;
  private TransactionManagerDelegate transactionManager;
  private ActivateFakeStatefulBusinessObject activateSpecificDocument;

  private AuthorizeOnPermission authorizeOnPermissionFun;
  private ValidateSchema validateSchemaFun;
  private Observer<?>[] entitiesOfInterest;
  private AuthorizeOnCondition authorizeOnConditionFun;
  private LockEntitiesOfInterest lockEntitiesOfInterestFun;
  private UnlockEntitiesOfInterest unlockEntitiesOfInterestFun;
  private DeserializeValueObject<FakeActivateValueObject> deserializeValueObjectFun;
  private ValidateIfActionIsAllowed validateIfActionIsAllowedFun;
  private ValidateMissingFieldsForState validateActivateMissingFieldsFun;
  private ValidateBusinessRules<FakeActivateValueObject> validateBusinessRulesFun;
  private ExecuteCommand<FakeActivateValueObject> executeActivateCmdFun;
  private Observer<?>[] activateDependentCommands;
  private SerializeActivateSuccessResponse serializeSuccessResponseFun;

  private ActivateTestUtils() {
    utilsContext = new ActivateTestUtilsContext();
    utilsContext.setBusinessObject(new FakeStatefulBusinessObject(ANY_USER_CODE));
  }

  public static ActivateTestUtils init() {
    return new ActivateTestUtils();
  }

  public ActivateTestUtils withAuthorizedOnPermessionUser() {
    authorizeOnPermissionFun = utilsContext.getAuthorizationTestUtils()
        .mockActivateSuccessAuthorizedOnPermession(FAKE_STATEFUL_OBJECT_NAME);
    return this;
  }

  public ActivateTestUtils withUserHasNoPermession() {
    authorizeOnPermissionFun = utilsContext.getAuthorizationTestUtils()
        .mockActivateFailedAuthorizedOnPermession(FAKE_STATEFUL_OBJECT_NAME);
    return this;
  }

  public ActivateTestUtils withValidJsonSchema() throws Exception {
    validateSchemaFun = utilsContext.getSchemaValidatorTestUtils()
        .mockValidJsonSchema(ANY_ACTIVATION_VALIDATION_SCHEMA, ANY_ACTIVATE_REQUEST_BODY);
    return this;
  }

  public ActivateTestUtils withInvalidJsonSchema() throws Exception {
    validateSchemaFun = utilsContext.getSchemaValidatorTestUtils()
        .mockInvalidJsonSchema(ANY_ACTIVATION_VALIDATION_SCHEMA, ANY_ACTIVATE_REQUEST_BODY);
    return this;
  }

  public ActivateTestUtils withExsitEntitesOfInterest() {
    EntityRepositoryTestUtils repoUtils = utilsContext.getEntityRepositoryTestUtils();
    entitiesOfInterest = new Observer[] {
        repoUtils.mockExistStatefulEntityObserver(utilsContext.getBusinessObject())};
    utilsContext.setEntitiesOfInterest(entitiesOfInterest);
    return this;
  }

  public ActivateTestUtils withOneOfEntitiesOfInterestNotExist() {
    EntityRepositoryTestUtils repoUtils = utilsContext.getEntityRepositoryTestUtils();
    entitiesOfInterest = new Observer[] {repoUtils.mockNonExistStatefulEntityObserver()};
    utilsContext.setEntitiesOfInterest(entitiesOfInterest);
    return this;
  }

  public ActivateTestUtils withAuthorizedOnConditionUser() {
    authorizeOnConditionFun = utilsContext.getAuthorizationTestUtils()
        .mockActivateSuccessAuthorizedOnCondition(utilsContext.getBusinessObject());
    return this;
  }

  public ActivateTestUtils withUserHasNoAuthorizedCondition() throws Exception {
    authorizeOnConditionFun = utilsContext.getAuthorizationTestUtils()
        .mockActivateFailedAuthorizedOnCondition(utilsContext.getBusinessObject());
    return this;
  }

  public ActivateTestUtils withSuccessLockEntitiesOfInterest() {
    lockEntitiesOfInterestFun = utilsContext.getLockTestUtils()
        .mockSuccessActivateLockEntitiesOfInterest(utilsContext.getBusinessObject());
    return this;
  }

  public ActivateTestUtils withEntityIsAlreadyLocked() throws Exception {
    lockEntitiesOfInterestFun = utilsContext.getLockTestUtils()
        .mockFailedActivateLockEntitiesOfInterest(utilsContext.getBusinessObject());
    return this;
  }

  public ActivateTestUtils withSuccessDeserializedValueObject() {
    deserializeValueObjectFun =
        utilsContext.getSerializationTestUtils().mockSuccessDeserializedActivateValueObject(
            ANY_ACTIVATE_REQUEST_BODY, FakeActivateValueObject.class, ANY_USER_CODE);
    return this;
  }

  public ActivateTestUtils withActivateIsAllowed() {
    validateIfActionIsAllowedFun = utilsContext.getStateMachineTestUtils()
        .mockActivateActionToBeAllowed(utilsContext.getBusinessObject());
    return this;
  }

  public ActivateTestUtils withActivateIsNotAllowed() throws Exception {
    validateIfActionIsAllowedFun = utilsContext.getStateMachineTestUtils()
        .mockActivateActionToBeNotAllowed(utilsContext.getBusinessObject());
    return this;
  }

  public ActivateTestUtils withAllNoMissingFieldsForActivate() {
    validateActivateMissingFieldsFun =
        utilsContext.getValidationTestUtils().mockActivateNoMissingFields(ANY_USER_CODE);
    return this;
  }

  public ActivateTestUtils withMissingFieldsForActivate() {
    validateActivateMissingFieldsFun =
        utilsContext.getValidationTestUtils().mockActivateMissingFields(ANY_USER_CODE);
    return this;
  }

  public ActivateTestUtils withValidActivateBusinessRules() {
    validateBusinessRulesFun =
        utilsContext.getValidationTestUtils().mockSuccessActivateBusinessRules();
    return this;
  }

  public ActivateTestUtils withInvalidActivateBusinessRules() {
    validateBusinessRulesFun =
        utilsContext.getValidationTestUtils().mockFailedActivateBusinessRules();
    return this;
  }

  public ActivateTestUtils withSuccessActivateCommand() {
    executeActivateCmdFun =
        utilsContext.getExecutionCommandsTestUtils().mockSuccessActivateCommandObserver();
    return this;
  }

  public ActivateTestUtils withSuccessActivateDependantCommands() throws Exception {
    activateDependentCommands = new Observer[] {
        utilsContext.getExecutionCommandsTestUtils().mockSuccessDependantCommandObserver(),
        utilsContext.getExecutionCommandsTestUtils().mockSuccessDependantCommandObserver()};

    utilsContext.setDependantCommands(activateDependentCommands);

    return this;
  }

  public ActivateTestUtils withFailedOneOfActivateDependantCommands() throws Exception {
    activateDependentCommands = new Observer[] {
        utilsContext.getExecutionCommandsTestUtils().mockSuccessDependantCommandObserver(),
        utilsContext.getExecutionCommandsTestUtils().mockFailedDependantCommandObserver()};
    utilsContext.setDependantCommands(activateDependentCommands);
    return this;
  }

  public ActivateTestUtils withSuccessUnlockEntitiesOfInterest() {
    unlockEntitiesOfInterestFun = utilsContext.getLockTestUtils()
        .mockSuccessActivateUnlockEntitiesOfInterest(utilsContext.getBusinessObject());
    return this;
  }

  public ActivateTestUtils withSuccessResponseSerialization() {
    serializeSuccessResponseFun =
        utilsContext.getSerializationTestUtils().mockSuccessActivateResponse();
    return this;
  }

  private void setupActivateDocument() {
    transactionManager = utilsContext.getTransactionManagerTestUtils().mockCommittedTransaction();
    activateSpecificDocument = context -> {
      new ActivateDocument().authorizeOnPermission(authorizeOnPermissionFun)
          .validateSchema(validateSchemaFun).findEntitiesOfInterest(entitiesOfInterest)
          .authorizeOnCondition(authorizeOnConditionFun)
          .lockEntitiesOfInterest(lockEntitiesOfInterestFun)
          .deserializeValueObject(deserializeValueObjectFun)
          .validateIfActionIsAllowed(validateIfActionIsAllowedFun)
          .validateMissingFieldsForState(validateActivateMissingFieldsFun)
          .validateBusinessRules(validateBusinessRulesFun).transactionManager(transactionManager)
          .executeCommand(executeActivateCmdFun)
          .executeActivateDependencies(activateDependentCommands)
          .unlockEntitiesOfInterest(unlockEntitiesOfInterestFun)
          .serializeSuccessResponse(serializeSuccessResponseFun).activate(context);

      return context.serializeResponse();
    };
  }

  private void callActivateFunction() throws Exception {
    setupActivateDocument();
    fakeActivateContext = FakeActivateContextBuilder.newContext();
    activateSpecificDocument.activate(
        fakeActivateContext.objectCode(ANY_USER_CODE).valueObject(ANY_ACTIVATE_REQUEST_BODY));
  }

  public void verifySuccessActivateFunctionsCallOrder() {
    try {
      callActivateFunction();
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateSuccessfulFunctionsCallOrder(utilsContext);
    } catch (Exception e) {
      Assert.fail();
    }
  }

  public void verifyTransactionIsRollback() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (TransactionFailedException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.assertTransactionIsRollback(utilsContext);
    }
  }

  public void verifyActivateFailureDueToEntityNotExist() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (InstanceNotExistException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToEntityNotExist(utilsContext);
    }
  }

  public void verifyActivateFailureDueToUserHasNoPermession() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (AuthorizationException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToUserHasNoPermession(utilsContext);
    }
  }

  public void verifyActivateFailureDueToInvlaidJsonSchema() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (ArgumentViolationSecurityException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToInvlaidJsonSchema(utilsContext);
    }
  }

  public void verifyActivateFailureDueToUserHasNoCondition() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (AuthorizationException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToUserHasNoCondition(utilsContext);
    }
  }

  public void verifyActivateFailureDueToEntityIsAlreadyLocked() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (AlreadyLockedException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToEntityIsAlreadyLocked(utilsContext);
    }
  }

  public void verifyActivateFailureDueToActionIsNotAllowed() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (ActionNotAllowedPerStateException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToActionIsNotAllowed(utilsContext);
    }
  }

  public void verifyActivateFailureDueToMissingFields() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (MissingFieldsException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToMissingFields(utilsContext);
    }
  }

  public void verifyActivateFailureDueToInvalidBusinessRule() throws Exception {
    try {
      callActivateFunction();
      Assert.fail();
    } catch (BusinessRuleViolationException e) {
      utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());
      ActivateTestAssertion.verifyActivateFailureDueToInvalidBusinessRule(utilsContext);
    }
  }

  public void verifySafeSubscribe() throws Exception {
    setupSafeSubscribeActivateDocumentFunction();

    fakeActivateContext = FakeActivateContextBuilder.newContext();

    activateSpecificDocument.activate(
        fakeActivateContext.objectCode(ANY_USER_CODE).valueObject(ANY_ACTIVATE_REQUEST_BODY));

    utilsContext.setValueObjectAsObject(fakeActivateContext.valueObject());

    ActivateTestAssertion.verifySafeSubscribeActivateFunctionsCallOrder(utilsContext);
  }

  private void setupSafeSubscribeActivateDocumentFunction() {
    activateSpecificDocument = context -> {
      new ActivateDocument().authorizeOnPermission(authorizeOnPermissionFun)
          .findEntitiesOfInterest(entitiesOfInterest).authorizeOnCondition(authorizeOnConditionFun)
          .activate(context);
      return context.serializeResponse();
    };
  }

}
