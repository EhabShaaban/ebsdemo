package com.ebs.dda.functions.activate;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.functions.fakeobjects.FakeAbstractDependantCommandObserver;
import com.ebs.dda.functions.fakeobjects.FakeFindEntityByUserCode;
import org.mockito.InOrder;
import org.mockito.Mockito;

import static com.ebs.dda.functions.activate.ActivateTestUtils.ANY_ACTIVATE_REQUEST_BODY;
import static com.ebs.dda.functions.fakeobjects.FakeActivateContextBuilder.FakeActivateContext.ANY_ACTIVATION_VALIDATION_SCHEMA;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;

public class ActivateTestAssertion {

  public static void verifyActivateSuccessfulFunctionsCallOrder(ActivateTestUtilsContext context)
      throws Exception {

    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils())
        .deserializeActivateValueObject(
            eq(ANY_ACTIVATE_REQUEST_BODY), any(), eq(context.getBusinessObject().getUserCode()));

    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .initObjectState(eq(context.getBusinessObject()));
    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .isAllowedAction(eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getValidationTestUtils().getMissingFieldsValidator())
        .validateMissingFields(
            eq(context.getBusinessObject().getUserCode()), eq(BasicStateMachine.ACTIVE_STATE));

    orderedFunctions
        .verify(context.getValidationTestUtils().getBusinessRulesValidator())
        .validateBusinessRules(eq(context.getValueObjectAsObject()));

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd())
        .executeCommand(any());

    orderedFunctions.verify(getDependantCmdByIndex(context, 0)).executeCommand(any());
    orderedFunctions.verify(getDependantCmdByIndex(context, 1)).executeCommand(any());

    orderedFunctions
        .verify(context.getTransactionManagerTestUtils().getPlatformTransactionManager())
        .commit(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils())
        .serializeActivateSuccessResponse(any(), any());

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .unlockEntitiesOfInterest(Mockito.any());
  }

  private static StatefullBusinessObjectRep getFindEntityRepositoryByIndex(
      ActivateTestUtilsContext context, int index) {
    return ((FakeFindEntityByUserCode) context.getEntitiesOfInterest()[index]).getRepository();
  }

  private static InOrder initOrderFunctionVerification(ActivateTestUtilsContext context) {
    InOrder orderedFunctions =
        inOrder(
            context.getAuthorizationTestUtils().getAuthorizationManager(),
            context.getSchemaValidatorTestUtils().getSchemaValidator(),
            getFindEntityRepositoryByIndex(context, 0),
            context.getLockTestUtils().getLockManager(),
            context.getSerializationTestUtils().getSerializionUtils(),
            context.getStateMachineTestUtils().getStateMachine(),
            context.getValidationTestUtils().getMissingFieldsValidator(),
            context.getValidationTestUtils().getBusinessRulesValidator(),
            context.getTransactionManagerTestUtils().getPlatformTransactionManager(),
            context.getExecutionCommandsTestUtils().getSuccessActivateCmd(),
            getDependantCmdByIndex(context, 0),
            getDependantCmdByIndex(context, 1));
    return orderedFunctions;
  }

  public static void assertTransactionIsRollback(ActivateTestUtilsContext context)
      throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils())
        .deserializeActivateValueObject(
            eq(ANY_ACTIVATE_REQUEST_BODY), any(), eq(context.getBusinessObject().getUserCode()));

    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .initObjectState(eq(context.getBusinessObject()));
    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .isAllowedAction(eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getValidationTestUtils().getMissingFieldsValidator())
        .validateMissingFields(
            eq(context.getBusinessObject().getUserCode()), eq(BasicStateMachine.ACTIVE_STATE));

    orderedFunctions
        .verify(context.getValidationTestUtils().getBusinessRulesValidator())
        .validateBusinessRules(eq(context.getValueObjectAsObject()));

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd())
        .executeCommand(any());

    orderedFunctions.verify(getDependantCmdByIndex(context, 0)).executeCommand(any());
    orderedFunctions.verify(getDependantCmdByIndex(context, 1)).executeCommand(any());

    orderedFunctions
        .verify(context.getTransactionManagerTestUtils().getPlatformTransactionManager())
        .rollback(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .unlockEntitiesOfInterest(Mockito.any());
  }

  private static ICommand getDependantCmdByIndex(ActivateTestUtilsContext context, int index) {
    return ((FakeAbstractDependantCommandObserver) context.getActivateDependentCommands()[index])
        .getCommand();
  }

  public static void verifyActivateFailureDueToEntityNotExist(ActivateTestUtilsContext context)
      throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager(), never())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());
  }

  public static void verifyActivateFailureDueToUserHasNoPermession(ActivateTestUtilsContext context)
      throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager(), never())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());
  }

  public static void verifyActivateFailureDueToInvlaidJsonSchema(ActivateTestUtilsContext context)
      throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager(), never())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());
  }

  public static void verifyActivateFailureDueToUserHasNoCondition(ActivateTestUtilsContext context)
      throws Exception {

    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager(), never())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());
  }

  public static void verifyActivateFailureDueToEntityIsAlreadyLocked(
      ActivateTestUtilsContext context) throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());
  }

  public static void verifyActivateFailureDueToActionIsNotAllowed(ActivateTestUtilsContext context)
      throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils())
        .deserializeActivateValueObject(
            eq(ANY_ACTIVATE_REQUEST_BODY), any(), eq(context.getBusinessObject().getUserCode()));

    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .initObjectState(eq(context.getBusinessObject()));
    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .isAllowedAction(eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .unlockEntitiesOfInterest(Mockito.any());
  }

  public static void verifyActivateFailureDueToMissingFields(ActivateTestUtilsContext context)
      throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils())
        .deserializeActivateValueObject(
            eq(ANY_ACTIVATE_REQUEST_BODY), any(), eq(context.getBusinessObject().getUserCode()));

    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .initObjectState(eq(context.getBusinessObject()));
    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .isAllowedAction(eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getValidationTestUtils().getMissingFieldsValidator())
        .validateMissingFields(
            eq(context.getBusinessObject().getUserCode()), eq(BasicStateMachine.ACTIVE_STATE));

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .unlockEntitiesOfInterest(Mockito.any());
  }

  public static void verifyActivateFailureDueToInvalidBusinessRule(ActivateTestUtilsContext context)
      throws Exception {
    InOrder orderedFunctions = initOrderFunctionVerification(context);

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getSchemaValidatorTestUtils().getSchemaValidator())
        .validateRequestBodySchema(
            eq(ANY_ACTIVATE_REQUEST_BODY), eq(ANY_ACTIVATION_VALIDATION_SCHEMA));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .lockEntitiesOfInterest(Mockito.any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils())
        .deserializeActivateValueObject(
            eq(ANY_ACTIVATE_REQUEST_BODY), any(), eq(context.getBusinessObject().getUserCode()));

    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .initObjectState(eq(context.getBusinessObject()));
    orderedFunctions
        .verify(context.getStateMachineTestUtils().getStateMachine())
        .isAllowedAction(eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(context.getValidationTestUtils().getMissingFieldsValidator())
        .validateMissingFields(
            eq(context.getBusinessObject().getUserCode()), eq(BasicStateMachine.ACTIVE_STATE));

    orderedFunctions
        .verify(context.getValidationTestUtils().getBusinessRulesValidator())
        .validateBusinessRules(eq(context.getValueObjectAsObject()));

    orderedFunctions
        .verify(context.getExecutionCommandsTestUtils().getSuccessActivateCmd(), never())
        .executeCommand(any());

    orderedFunctions
        .verify(context.getSerializationTestUtils().getSerializionUtils(), never())
        .serializeActivateSuccessResponse(
            eq(context.getBusinessObject().getUserCode()), anyString());

    orderedFunctions
        .verify(context.getLockTestUtils().getLockManager())
        .unlockEntitiesOfInterest(Mockito.any());
  }

  public static void verifySafeSubscribeActivateFunctionsCallOrder(ActivateTestUtilsContext context)
      throws Exception {

    InOrder orderedFunctions =
        inOrder(
            context.getAuthorizationTestUtils().getAuthorizationManager(),
            getFindEntityRepositoryByIndex(context, 0));

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeAction(eq(context.getBusinessObject().getSysName()), eq(IActionsNames.ACTIVATE));

    orderedFunctions
        .verify(getFindEntityRepositoryByIndex(context, 0))
        .findOneByUserCode(anyString());

    orderedFunctions
        .verify(context.getAuthorizationTestUtils().getAuthorizationManager())
        .authorizeActionOnObject(eq(context.getBusinessObject()), eq(IActionsNames.ACTIVATE));
  }
}
