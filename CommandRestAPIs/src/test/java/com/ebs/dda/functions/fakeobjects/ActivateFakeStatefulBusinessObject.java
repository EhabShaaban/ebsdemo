package com.ebs.dda.functions.fakeobjects;

import com.ebs.dda.functions.fakeobjects.FakeActivateContextBuilder.FakeActivateContext;

public interface ActivateFakeStatefulBusinessObject {
  String activate(FakeActivateContext context) throws Exception;
}
