package com.ebs.dda.functions.fakeobjects;

import com.ebs.dda.functions.executecommand.ExecuteActivateDependanciesContext;
import com.ebs.dda.functions.utils.ExecutionCommandsTestUtils;

public class FakeFailedDependantCommandObserver extends FakeAbstractDependantCommandObserver {

  public FakeFailedDependantCommandObserver() throws Exception {
    super(ExecutionCommandsTestUtils.mockFailedCommand());
  }

  @Override
  public void onNext(ExecuteActivateDependanciesContext t) {
    try {
      getCommand().executeCommand(t.entitiesOfInterestContainer().getMainEntity());
    } catch (Exception e) {
      throw new RuntimeException();
    }
  }
}
