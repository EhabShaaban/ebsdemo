package com.ebs.dda.functions.utils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import com.ebs.dda.commands.apis.ICommand;
import com.ebs.dda.functions.fakeobjects.FakeActivateCommandObserver;
import com.ebs.dda.functions.fakeobjects.FakeFailedDependantCommandObserver;
import com.ebs.dda.functions.fakeobjects.FakeSuccessDependantCommandObserver;
import io.reactivex.Observable;
import io.reactivex.Observer;

public class ExecutionCommandsTestUtils {

  private ICommand successActivateCommand;

  public ExecutionCommandsTestUtils() {}

  public FakeActivateCommandObserver mockSuccessActivateCommandObserver() {
    successActivateCommand = mockSuccessCommand();
    return new FakeActivateCommandObserver(successActivateCommand);
  }

  public Observer mockSuccessDependantCommandObserver() throws Exception {
    return new FakeSuccessDependantCommandObserver();
  }

  public Observer mockFailedDependantCommandObserver() throws Exception {
    return new FakeFailedDependantCommandObserver();
  }

  public static ICommand mockSuccessCommand() {
    ICommand command = mock(ICommand.class);
    try {
      doReturn(mock(Observable.class)).when(command).executeCommand(any());
    } catch (Exception e) {
    }
    return command;
  }

  public static ICommand mockFailedCommand() throws Exception {
    ICommand command = mock(ICommand.class);
    doThrow(RuntimeException.class).when(command).executeCommand(any());
    return command;
  }

  public ICommand getSuccessActivateCmd() {
    return successActivateCommand;
  }

}
