package com.ebs.dda.functions.utils;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.foundation.realization.statemachine.IActionsNames;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dac.security.services.AuthorizationManager;
import com.ebs.dda.functions.authorize.AuthorizeOnCondition;
import com.ebs.dda.functions.authorize.AuthorizeOnPermission;

public class AuthorizationTestUtils {

  private AuthorizationManager authorizationManager;

  public AuthorizationTestUtils() {
    authorizationManager = mock(AuthorizationManager.class);
  }

  public AuthorizeOnPermission mockActivateSuccessAuthorizedOnPermession(String objectName) {
    doNothing().when(authorizationManager).authorizeAction(eq(objectName),
        eq(IActionsNames.ACTIVATE));
    return new AuthorizeOnPermission(authorizationManager);
  }

  public AuthorizeOnPermission mockActivateFailedAuthorizedOnPermession(String objectName) {
    doThrow(AuthorizationException.class).when(authorizationManager).authorizeAction(eq(objectName),
        eq(IActionsNames.ACTIVATE));
    return new AuthorizeOnPermission(authorizationManager);
  }

  public AuthorizeOnCondition mockActivateSuccessAuthorizedOnCondition(
      StatefullBusinessObject<?> businessObject) {
    try {
      doNothing().when(authorizationManager).authorizeActionOnObject(eq(businessObject),
          eq(IActionsNames.ACTIVATE));
    } catch (Exception e) {
    }
    return new AuthorizeOnCondition(authorizationManager);
  }

  public AuthorizeOnCondition mockActivateFailedAuthorizedOnCondition(
      StatefullBusinessObject<?> businessObject) throws Exception {
    doThrow(AuthorizationException.class).when(authorizationManager)
        .authorizeActionOnObject(eq(businessObject), eq(IActionsNames.ACTIVATE));
    return new AuthorizeOnCondition(authorizationManager);
  }

  public AuthorizationManager getAuthorizationManager() {
    return authorizationManager;
  }

}
