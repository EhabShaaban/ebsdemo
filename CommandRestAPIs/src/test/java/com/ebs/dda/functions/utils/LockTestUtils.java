package com.ebs.dda.functions.utils;

import com.ebs.dac.foundation.exceptions.operational.locking.AlreadyLockedException;
import com.ebs.dda.functions.fakeobjects.FakeStatefulBusinessObject;
import com.ebs.dda.functions.lock.IActivateLockEntitiesOfInterest;
import com.ebs.dda.functions.lock.LockEntitiesOfInterest;
import com.ebs.dda.functions.lock.UnlockEntitiesOfInterest;
import org.mockito.Mockito;

import static org.mockito.Mockito.*;

public class LockTestUtils {

  private IActivateLockEntitiesOfInterest lockManager;

  public LockTestUtils() {
    lockManager = mock(IActivateLockEntitiesOfInterest.class);
  }

  public LockEntitiesOfInterest mockSuccessActivateLockEntitiesOfInterest(
      FakeStatefulBusinessObject businessObject) {
    try {
      doNothing().when(lockManager).lockEntitiesOfInterest(Mockito.any());
    } catch (Exception e) {
    }
    return new LockEntitiesOfInterest(lockManager);
  }

  public LockEntitiesOfInterest mockFailedActivateLockEntitiesOfInterest(
      FakeStatefulBusinessObject businessObject) throws Exception {
    doThrow(AlreadyLockedException.class).when(lockManager).lockEntitiesOfInterest(Mockito.any());
    return new LockEntitiesOfInterest(lockManager);
  }

  public UnlockEntitiesOfInterest mockSuccessActivateUnlockEntitiesOfInterest(
      FakeStatefulBusinessObject businessObject) {
    try {
      doNothing().when(lockManager).unlockEntitiesOfInterest(Mockito.any());
    } catch (Exception e) {
    }
    return new UnlockEntitiesOfInterest(lockManager);
  }

  public IActivateLockEntitiesOfInterest getLockManager() {
    return lockManager;
  }
}
