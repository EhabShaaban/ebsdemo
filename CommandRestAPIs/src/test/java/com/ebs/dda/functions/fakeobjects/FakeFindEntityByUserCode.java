package com.ebs.dda.functions.fakeobjects;

import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dda.functions.executequery.FindEntityByUserCode;

public class FakeFindEntityByUserCode extends FindEntityByUserCode<FakeStatefulBusinessObject> {

  private StatefullBusinessObjectRep repository;

  public FakeFindEntityByUserCode(StatefullBusinessObjectRep repository) {
    super(repository);
    this.repository = repository;
  }

  public StatefullBusinessObjectRep getRepository() {
    return repository;
  }

}
