package com.ebs.dda.functions.utils;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import com.ebs.dac.dbo.jpa.entities.businessobjects.StatefullBusinessObject;
import com.ebs.dac.dbo.jpa.repositories.businessobjects.StatefullBusinessObjectRep;
import com.ebs.dac.foundation.exceptions.operational.accessability.InstanceNotExistException;
import com.ebs.dda.functions.fakeobjects.FakeFindEntityByUserCode;
import com.ebs.dda.functions.fakeobjects.FakeStatefulBusinessObject;

public class EntityRepositoryTestUtils {

  public FakeFindEntityByUserCode mockExistStatefulEntityObserver(
      StatefullBusinessObject<?> businessObject) {
    StatefullBusinessObjectRep repository = mock(StatefullBusinessObjectRep.class);
    doReturn(businessObject).when(repository).findOneByUserCode(anyString());
    return new FakeFindEntityByUserCode(repository);
  }

  public FakeFindEntityByUserCode mockNonExistStatefulEntityObserver() {
    StatefullBusinessObjectRep repository = mock(StatefullBusinessObjectRep.class);
    doThrow(InstanceNotExistException.class).when(repository).findOneByUserCode(anyString());
    return new FakeFindEntityByUserCode(repository);
  }

  public FakeStatefulBusinessObject mockExistFakeStatefulEntity() {
    return mock(FakeStatefulBusinessObject.class);
  }

}
