package com.ebs.dda.functions.activate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ActivateDocumentTest {

  @Test
  public void testThatActivateDocumentCalledSuccessfully() throws Exception {

    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withAllNoMissingFieldsForActivate()
        .withValidActivateBusinessRules().withSuccessActivateCommand()
        .withSuccessActivateDependantCommands().withSuccessUnlockEntitiesOfInterest()
        .withSuccessResponseSerialization().verifySuccessActivateFunctionsCallOrder();
  }

  @Test
  public void testThatActivateDocumentFailedDueToTransactionFailure() throws Exception {

    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withAllNoMissingFieldsForActivate()
        .withValidActivateBusinessRules().withSuccessActivateCommand()
        .withFailedOneOfActivateDependantCommands().withSuccessUnlockEntitiesOfInterest()
        .verifyTransactionIsRollback();
  }

  @Test
  public void testThatActivateDocumentFailedDueToEntityNotExist() throws Exception {

    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withOneOfEntitiesOfInterestNotExist().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withAllNoMissingFieldsForActivate()
        .withValidActivateBusinessRules().withSuccessActivateCommand()
        .withSuccessActivateDependantCommands().verifyActivateFailureDueToEntityNotExist();
  }

  @Test
  public void testThatActivateDocumentFailedDueToUserHasNoPermession() throws Exception {

    ActivateTestUtils.init().withUserHasNoPermession().withValidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withAllNoMissingFieldsForActivate()
        .withValidActivateBusinessRules().withSuccessActivateCommand()
        .withSuccessActivateDependantCommands().withSuccessUnlockEntitiesOfInterest()
        .withSuccessResponseSerialization().verifyActivateFailureDueToUserHasNoPermession();
  }

  @Test
  public void testThatActivateDocumentFailedDueToInvalidJsonSchema() throws Exception {

    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withInvalidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withAllNoMissingFieldsForActivate()
        .withValidActivateBusinessRules().withSuccessActivateCommand()
        .withSuccessActivateDependantCommands().withSuccessUnlockEntitiesOfInterest()
        .withSuccessResponseSerialization().verifyActivateFailureDueToInvlaidJsonSchema();
  }

  @Test
  public void testThatActivateDocumentFailedDueToUserHasNoCondition() throws Exception {

    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withExsitEntitesOfInterest().withUserHasNoAuthorizedCondition()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withAllNoMissingFieldsForActivate()
        .withValidActivateBusinessRules().withSuccessActivateCommand()
        .withSuccessActivateDependantCommands().withSuccessUnlockEntitiesOfInterest()
        .withSuccessResponseSerialization().verifyActivateFailureDueToUserHasNoCondition();
  }

  @Test
  public void testThatActivateDocumentFailedDueToEntityIsAlreadyLocked() throws Exception {

    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser().withEntityIsAlreadyLocked()
        .withSuccessDeserializedValueObject().withActivateIsAllowed()
        .withAllNoMissingFieldsForActivate().withValidActivateBusinessRules()
        .withSuccessActivateCommand().withSuccessActivateDependantCommands()
        .withSuccessUnlockEntitiesOfInterest().withSuccessResponseSerialization()
        .verifyActivateFailureDueToEntityIsAlreadyLocked();
  }

  @Test
  public void testThatActivateDocumentFailedDueToActionIsNotAllowed() throws Exception {
    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsNotAllowed().withAllNoMissingFieldsForActivate()
        .withValidActivateBusinessRules().withSuccessActivateCommand()
        .withSuccessActivateDependantCommands().withSuccessUnlockEntitiesOfInterest()
        .withSuccessResponseSerialization().verifyActivateFailureDueToActionIsNotAllowed();
  }

  @Test
  public void testThatActivateDocumentFailedDueMissingFields() throws Exception {
    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withMissingFieldsForActivate().withValidActivateBusinessRules()
        .withSuccessActivateCommand().withSuccessActivateDependantCommands()
        .withSuccessUnlockEntitiesOfInterest().withSuccessResponseSerialization()
        .verifyActivateFailureDueToMissingFields();
  }

  @Test
  public void testThatActivateDocumentFailedDueInvalidBusinessRule() throws Exception {

    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withValidJsonSchema()
        .withExsitEntitesOfInterest().withAuthorizedOnConditionUser()
        .withSuccessLockEntitiesOfInterest().withSuccessDeserializedValueObject()
        .withActivateIsAllowed().withAllNoMissingFieldsForActivate()
        .withInvalidActivateBusinessRules().withSuccessActivateCommand()
        .withSuccessActivateDependantCommands().withSuccessUnlockEntitiesOfInterest()
        .withSuccessResponseSerialization().verifyActivateFailureDueToInvalidBusinessRule();
  }

  @Test
  public void testSafeSubscribeAndSafeExecuteCommandsInTransaction() throws Exception {
    ActivateTestUtils.init().withAuthorizedOnPermessionUser().withExsitEntitesOfInterest()
        .withAuthorizedOnConditionUser().verifySafeSubscribe();
  }

}
