package com.ebs.dda.rx;

import com.ebs.dac.dbo.events.DeleteEvent;
import com.ebs.dda.commands.order.purchaseorder.PurchaseOrderDeleteCommand;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObImportPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObOrderDocumentRep;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

@RunWith(JUnit4.class)
public class DObPurchaseOrderDeleteEventTest {

  @Test
  public void testDeletePurchaseOrder() throws Exception {

    PurchaseOrderDeleteCommand deleteCommand = new PurchaseOrderDeleteCommand();
    DObOrderDocumentRep orderRep = Mockito.mock(DObOrderDocumentRep.class);
    deleteCommand.setOrderDocumentRep(orderRep);

    DObPurchaseOrder order = new DObImportPurchaseOrder();
    String userCode = "2018000021";
    order.setUserCode(userCode);

    Mockito.when(orderRep.findOneByUserCode(userCode)).thenReturn(order);
    Mockito.doNothing().when(orderRep).delete(order);

    ObservablesEventBus observablesEventBus = ObservablesEventBus.getInstance();

    final StringBuilder actualUserCode = new StringBuilder();
    observablesEventBus.subscribe(
        (event) -> {
          DeleteEvent deleteOrderEvent = (DeleteEvent) event;
          actualUserCode.append(deleteOrderEvent.getPurchaseOrderCode());
        });

    deleteCommand
        .executeCommand(userCode)
        .subscribe(
            (msg) -> {
              DeleteEvent deleteOrderEvent = new DeleteEvent(userCode);
              observablesEventBus.consumeEvent(deleteOrderEvent);
            });

    Assertions.assertEquals(userCode, actualUserCode.toString());
  }

  @Test
  public void testDeletePurchaseOrderUsingObservableMap() throws Exception {

    PurchaseOrderDeleteCommand deleteCommand = new PurchaseOrderDeleteCommand();
    DObOrderDocumentRep orderRep = Mockito.mock(DObOrderDocumentRep.class);
    deleteCommand.setOrderDocumentRep(orderRep);

    DObPurchaseOrder order = new DObImportPurchaseOrder();
    String userCode = "2018000021";
    order.setUserCode(userCode);

    Mockito.when(orderRep.findOneByUserCode(userCode)).thenReturn(order);
    Mockito.doNothing().when(orderRep).delete(order);

    ObservablesEventBus observablesEventBus = ObservablesEventBus.getInstance();

    deleteCommand
        .executeCommand(userCode)
        .subscribe(
            (msg) -> {
              DeleteEvent deleteOrderEvent = new DeleteEvent(userCode);
              observablesEventBus.registerObservable(DeleteEvent.class, deleteOrderEvent);
            });

    final StringBuilder actualUserCode = new StringBuilder();
    observablesEventBus.getObservableObject(DeleteEvent.class)
        .subscribe(
            (event) -> {
              DeleteEvent deleteOrderEvent = (DeleteEvent) event;
              actualUserCode.append(deleteOrderEvent.getPurchaseOrderCode());
            });

    Assertions.assertEquals(userCode, actualUserCode.toString());
  }

}
