package com.ebs.dda.rx;

import com.ebs.dac.dbo.events.EBSEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(JUnit4.class)
public class ObservableEventBusTest {

  @Test
  public void testEventBusSubjectStreamStillUpAfterException() throws Exception {
    FakeEventMonitorService monitorService = mockFakeEventMonitor();
    ObservablesEventBus eventBus = ObservablesEventBus.getInstance();
    eventBus.subscribe(monitorService);

    eventBus.consumeEvent(new EBSEvent());
    eventBus.consumeEvent(new EBSEvent());
    eventBus.consumeEvent(new EBSEvent());

    verify(monitorService, times(3)).handleEvent(any());
  }

  private FakeEventMonitorService mockFakeEventMonitor() throws Exception {
    FakeEventMonitorService fakeEventMonitorService = Mockito.mock(FakeEventMonitorService.class);
    Mockito.doThrow(new Exception()).when(fakeEventMonitorService).handleEvent(Mockito.any());
    return fakeEventMonitorService;
  }

  class FakeEventMonitorService implements IEventBusMonitorService {

    @Override
    public void handleEvent(EBSEvent event) throws Exception {

    }
  }
}
