package com.ebs.dda.rx;

import io.reactivex.Observable;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class EventBehaviourTest {

    @Test
    public void testFinalVarWithObservable(){
        final StringBuilder codeBuilder = new StringBuilder();
        Observable<String> codeObservable = Observable.just("CODE");
        codeObservable.subscribe((code)-> codeBuilder.append(code));
        Assertions.assertEquals("CODE",codeBuilder.toString());
    }

}
