//package com.ebs.dda.rx;
//
//import org.joda.time.DateTime;
//import org.joda.time.DateTimeZone;
//import org.joda.time.format.DateTimeFormat;
//import org.joda.time.format.DateTimeFormatter;
//import org.junit.Test;
//import org.junit.jupiter.api.Assertions;
//import org.junit.runner.RunWith;
//import org.junit.runners.JUnit4;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.TimeZone;
//
//@RunWith(JUnit4.class)
//public class TimeZoneTest {
//  private static final Logger LOGGER = LoggerFactory.getLogger(TimeZoneTest.class);
//
//  @Test
//  public void testTimeZone() {
//    DateTimeFormatter formatWithTimeZone = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm:ss a Z");
//    DateTime dateTimeEGWithTimeZone =
//        formatWithTimeZone.parseDateTime("16-Oct-2019 10:00:00 PM +0200");
//    DateTimeFormatter format = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm:ss a");
//    DateTime dateTimeEG = format.parseDateTime("16-Oct-2019 10:00:00 PM");
//
//    Assertions.assertEquals(dateTimeEGWithTimeZone, dateTimeEG);
//    LOGGER.info(dateTimeEGWithTimeZone.toString());
//    LOGGER.info(dateTimeEG.toString());
//  }
//
//  @Test
//  public void testTimeZone2() {
//    DateTimeFormatter formatWithTimeZone = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm:ss a Z");
//    DateTime dateTimeEGWithTimeZone =
//        formatWithTimeZone.parseDateTime("16-Oct-2019 08:00:00 PM +0000");
//    DateTimeFormatter format = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm:ss a");
//    DateTime dateTimeEG = format.parseDateTime("16-Oct-2019 10:00:00 PM");
//
//    Assertions.assertEquals(dateTimeEGWithTimeZone, dateTimeEG);
//    LOGGER.info(dateTimeEGWithTimeZone.toString());
//    LOGGER.info(dateTimeEG.toString());
//  }
//
//  @Test
//  public void testTimeZone3() {
//    LOGGER.info(TimeZone.getDefault().toString());
//  }
//}
