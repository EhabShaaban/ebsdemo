package com.ebs.dda.rest.accounting.collection;

import com.ebs.dda.rest.accounting.collection.utils.DObCollectionSchemaTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@RunWith(JUnitPlatform.class)
public class DObCollectionEditDetailsSchemaTests {

	private DObCollectionSchemaTestUtils utils;

	@BeforeEach
	public void init() {
		utils = new DObCollectionSchemaTestUtils();
	}

	private Map<String, String> validBankAccountNoMaps = new HashMap<String, String>() {
		{
			put("BankAccountNo with digits only", "9300552 - EGP");
			put("BankAccountNo with slash", "1018238/000/5030 - USD");
			put("BankAccountNo with dashes", "0102-080391-002 - USD");
		}
	};

	@TestFactory
	public Stream<DynamicTest> Should_Not_ThrowArgumentSecurityException_When_ValidBankAccountNo() {
		return validBankAccountNoMaps.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String validBankAccountNo = validBankAccountNoMaps.get(key);
			utils.withAmount(1000D).withBankAccountNo(validBankAccountNo)
							.assertDoesNotThrowException();
		}));

	}


}
