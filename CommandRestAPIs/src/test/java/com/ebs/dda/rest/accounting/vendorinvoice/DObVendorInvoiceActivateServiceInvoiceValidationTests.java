package com.ebs.dda.rest.accounting.vendorinvoice;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.accounting.vendorinvoice.exceptions.DObVendorInvoiceHasNoEstimatedLandedCostException;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;
import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObPurchaseOrderVendorGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObPurchaseOrderVendorGeneralModelRep;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModelRep;
import com.ebs.dda.repositories.masterdata.item.IObItemAccountingInfoGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.IObVendorAccountingInfoGeneralModelRep;
import com.ebs.dda.rest.accounting.paymentrequest.mocks.CObFiscalYearMock;
import com.ebs.dda.rest.accounting.vendorinvoice.utils.DObVendorInvoiceActivateServiceInvoiceValidatorUtils;
import com.ebs.dda.validation.vendorinvoice.DObVendorInvoiceActivateServiceInvoiceValidator;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.time.Year;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
public class DObVendorInvoiceActivateServiceInvoiceValidationTests {

    private DObVendorInvoiceActivateServiceInvoiceValidatorUtils utils;
    private DObVendorInvoiceActivateServiceInvoiceValidator activateServiceInvoiceValidator;

    @MockBean private IObVendorInvoiceDetailsGeneralModelRep invoiceDetailsGeneralModelRep;
    @MockBean private IObVendorInvoiceItemsGeneralModelRep invoiceItemsGeneralModelRep;
    @MockBean private IObVendorAccountingInfoGeneralModelRep vendorAccountingInfoGeneralModelRep;
    @MockBean private IObItemAccountingInfoGeneralModelRep itemAccountingInfoGeneralModelRep;
    @MockBean private IObPurchaseOrderVendorGeneralModelRep orderVendorGeneralModelRep;
    @MockBean private DObLandedCostGeneralModelRep landedCostGeneralModelRep;
    @MockBean private CObFiscalYearRep fiscalYearRep;
    @Before
    public void init(){
        utils = new DObVendorInvoiceActivateServiceInvoiceValidatorUtils();
        activateServiceInvoiceValidator = new DObVendorInvoiceActivateServiceInvoiceValidator();
    }

    @Test
    public void shouldThrowExceptionWhenInvoiceServiceOrderHasNoLandedCostForRelatedPurchaseOrder() {
        utils
                .withVendorInvoiceCode("2021000001", "2021000003", "000002")
                .withDueDate(new DateTime())
                .withItemAccountingInfo("000002")
                .withOrder("2021000003", "SERVICE_PO", "2021000002")
                .withAccountingInfo("000002")
                .withInvoiceItems("000002")
                .withLandedCost(null).withActiveFiscalYears(Arrays.asList(Integer.toString(Year.now().getValue())));

        injectServiceVendorInvoiceActivationValidatorDependencies("2021000001", "000002",
                "000002", "2021000003", "2021000002");

        assertThrows(DObVendorInvoiceHasNoEstimatedLandedCostException.class,
                () -> activateServiceInvoiceValidator.validateBusinessRules(utils.getValueObject()));
    }

    @Test
    public void shouldThrowSecurityExceptionWhenActivateInvoiceWIthInvalidDueDate() {
        utils
                .withVendorInvoiceCode("2021000001", "2021000003", "000002")
                .withDueDate(new DateTime())
                .withItemAccountingInfo("000002")
                .withOrder("2021000003", "SERVICE_PO", "2021000002")
                .withAccountingInfo("000002")
                .withInvoiceItems("000002")
                .withLandedCost("2021000004")
                .withActiveFiscalYears(Arrays.asList("2001", "2002"));


        injectServiceVendorInvoiceActivationValidatorDependencies("2021000001", "000002",
                "000002", "2021000003", "2021000002");

        assertThrows(ArgumentViolationSecurityException.class,
                () -> activateServiceInvoiceValidator.validateBusinessRules(utils.getValueObject()));
    }

    private void injectServiceVendorInvoiceActivationValidatorDependencies(String invoiceCode, String vendorCode,
                                                                           String itemCode,
                                                                           String servicePurchaseOrderCode,
                                                                           String purchaseOrderCode) {
        IObVendorInvoiceDetailsGeneralModel invoiceDetails = utils.getVendorInvoiceDetailsGeneralModelMock();
        IObVendorAccountingInfoGeneralModel accountingInfoGeneralModel = utils.getAccountingInfoGeneralModelMock();
        List<IObVendorInvoiceItemsGeneralModel> invoiceItems = Arrays.asList(utils.getInvoiceItemGeneralModelMock());
        IObItemAccountingInfoGeneralModel itemAccountingInfoGeneralModel = utils.getItemAccountingInfoGeneralModelMock();
        IObPurchaseOrderVendorGeneralModel orderVendorGeneralModel = utils.getOrderVendorGeneralModelMock();
        DObLandedCostGeneralModel landedCostGeneralModel = utils.getLandedCostGeneralModelMock();
        List<CObFiscalYearMock> activeFiscalYears = utils.getFiscalYearMockList();

        doReturn(invoiceDetails)
                .when(invoiceDetailsGeneralModelRep)
                .findByInvoiceCode(invoiceCode);

        doReturn(accountingInfoGeneralModel)
                .when(vendorAccountingInfoGeneralModelRep)
                .findByVendorCode(vendorCode);

        doReturn(invoiceItems)
                .when(invoiceItemsGeneralModelRep)
                .findByInvoiceCodeOrderByIdAsc(invoiceCode);

        doReturn(itemAccountingInfoGeneralModel)
                .when(itemAccountingInfoGeneralModelRep)
                .findByItemCode(itemCode);

        doReturn(orderVendorGeneralModel)
                .when(orderVendorGeneralModelRep)
                .findOneByUserCode(servicePurchaseOrderCode);

        doReturn(landedCostGeneralModel)
                .when(landedCostGeneralModelRep)
                .findByPurchaseOrderCodeAndCurrentStatesLike(purchaseOrderCode, DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED);

        doReturn(activeFiscalYears)
                .when(fiscalYearRep)
                .findByCurrentStatesLike("%Active%");

        activateServiceInvoiceValidator.setInvoiceDetailsGeneralModelRep(invoiceDetailsGeneralModelRep);
        activateServiceInvoiceValidator.setInvoiceItemsGeneralModelRep(invoiceItemsGeneralModelRep);
        activateServiceInvoiceValidator.setItemAccountingInfoGeneralModelRep(itemAccountingInfoGeneralModelRep);
        activateServiceInvoiceValidator.setVendorAccountingInfoGeneralModelRep(vendorAccountingInfoGeneralModelRep);
        activateServiceInvoiceValidator.setOrderVendorGeneralModelRep(orderVendorGeneralModelRep);
        activateServiceInvoiceValidator.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
        activateServiceInvoiceValidator.setFiscalYearRep(fiscalYearRep);
    }
}
