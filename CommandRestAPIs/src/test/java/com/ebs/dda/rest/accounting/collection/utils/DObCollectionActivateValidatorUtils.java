package com.ebs.dda.rest.accounting.collection.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.collection.DObCollectionActivateValueObject;
import com.ebs.dda.jpa.accounting.collection.DObCollectionGeneralModel;
import com.ebs.dda.jpa.accounting.collection.IObCollectionDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModel;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.collection.DObCollectionGeneralModelRep;
import com.ebs.dda.repositories.accounting.collection.IObCollectionDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.validation.collection.DObCollectionSalesInvoiceActivateValidator;
import com.ebs.dda.validation.collection.IObCollectionDetailsDataMandatoryValidator;
import com.ebs.entities.accounting.FiscalYearMockUtils;
import com.ebs.entities.accounting.collection.DObCollectionMockUtils;
import com.ebs.entities.accounting.collection.IObCollectionDetailsMockUtils;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceMockUtils;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObCollectionActivateValidatorUtils {

  List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocumentGeneralModels;
  List<IObNotesReceivablesReferenceDocumentGeneralModel> uniqueReferenceDocumentGeneralModels;
  String notesReceivableCode;
  String activeState = "Active";

  private DObCollectionActivateValueObject valueObject;
  private DObCollectionSalesInvoiceActivateValidator validator;
  private IObCollectionDetailsDataMandatoryValidator collectionDetailsDataMandatoryValidator;

  private DObCollectionGeneralModelRep collectionGeneralModelRep;
  private IObCollectionDetailsGeneralModelRep collectionDetailsGeneralModelRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private CObFiscalYearRep fiscalYearRep;

  public DObCollectionActivateValidatorUtils() throws ParseException {
    valueObject = new DObCollectionActivateValueObject();
    validator = new DObCollectionSalesInvoiceActivateValidator();
    referenceDocumentGeneralModels = new ArrayList<>();
    uniqueReferenceDocumentGeneralModels = new ArrayList<>();
    collectionDetailsDataMandatoryValidator = new IObCollectionDetailsDataMandatoryValidator();
    mockDependencies();
    mockFiscalYear();
    injectValidatorDependencies();
  }

  public DObCollectionActivateValidatorUtils withCollection(
      String userCode, String refDocumentCode, BigDecimal amount) {
    valueObject.setCollectionCode(userCode);
    DObCollectionGeneralModel collectionGeneralModel =
        DObCollectionMockUtils.mockGeneralModel()
            .collectionCode(userCode)
            .refDocumentCode(refDocumentCode)
            .amount(amount)
            .build();

    Mockito.when(collectionGeneralModelRep.findOneByUserCode(userCode))
        .thenReturn(collectionGeneralModel);
    return this;
  }

  public DObCollectionActivateValidatorUtils withJournalEntryDate(String date) {
    this.valueObject.setJournalEntryDate(date);
    return this;
  }

  public DObCollectionActivateValidatorUtils withCollectionDetails(
      String collectionCode,
      BigDecimal amount,
      String collectionMethod,
      String treasuryCode,
      String bankAccountCode) {

    IObCollectionDetailsGeneralModel collectionDetailsGeneralModel =
        IObCollectionDetailsMockUtils.mockGeneralModel()
            .collectionCode(collectionCode)
            .amount(amount)
            .collectionMethod(collectionMethod)
            .treasuryCode(treasuryCode)
            .backAccountCode(bankAccountCode)
            .build();

    Mockito.when(collectionDetailsGeneralModelRep.findByCollectionCode(collectionCode))
        .thenReturn(collectionDetailsGeneralModel);

    return this;
  }

  public DObCollectionActivateValidatorUtils withSalesInvoiceDataReferenceDocument(
      String salesInvoiceCode, String remaining, String state) {

    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        DObSalesInvoiceMockUtils.mockGeneralModel()
            .salesInvoiceCode(salesInvoiceCode)
            .currentStates(currentStates)
            .remaining(new BigDecimal(remaining))
            .build();

    Mockito.when(salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode))
        .thenReturn(salesInvoiceGeneralModel);

    Mockito.when(salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode))
        .thenReturn(salesInvoiceGeneralModel);

    return this;
  }

  public DObCollectionActivateValueObject getValueObject() {
    return valueObject;
  }

  public void assertDoesNotThrowException() {
    assertDoesNotThrow(() -> validator.validateBusinessRules(getValueObject()));
  }

  public void assertThrowsArgumentViolationSecurityException() {
    assertThrows(
        ArgumentViolationSecurityException.class,
        () -> validator.validateBusinessRules(getValueObject()));
  }

  private void injectValidatorDependencies() {
    validator.setCollectionDetailsDataMandatoryValidator(collectionDetailsDataMandatoryValidator);
    validator.setFiscalYearRep(fiscalYearRep);
    validator.setCollectionGeneralModelRep(collectionGeneralModelRep);
    validator.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
    validator.setFiscalYearRep(fiscalYearRep);
    collectionDetailsDataMandatoryValidator.setCollectionDetailsGeneralModelRep(
        collectionDetailsGeneralModelRep);
  }

  private void mockDependencies() {
    collectionGeneralModelRep = Mockito.mock(DObCollectionGeneralModelRep.class);
    collectionDetailsGeneralModelRep = Mockito.mock(IObCollectionDetailsGeneralModelRep.class);
    salesInvoiceGeneralModelRep = Mockito.mock(DObSalesInvoiceGeneralModelRep.class);
    fiscalYearRep = Mockito.mock(CObFiscalYearRep.class);
  }

  private void mockFiscalYear() throws ParseException {
    CObFiscalYear firstFiscalYear =
        FiscalYearMockUtils.mockEntity()
            .id(1L)
            .currentState("%" + activeState + "%")
            .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
            .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
            .build();
    CObFiscalYear secondFiscalYear =
        FiscalYearMockUtils.mockEntity()
            .id(2L)
            .currentState("%" + activeState + "%")
            .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
            .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2023 00:00 AM")))
            .build();
    List<CObFiscalYear> fiscalYearList = new ArrayList<>();
    fiscalYearList.add(firstFiscalYear);
    fiscalYearList.add(secondFiscalYear);
    Mockito.when(fiscalYearRep.findByCurrentStatesLike("%" + activeState + "%"))
        .thenReturn(fiscalYearList);
  }

  public Long getDateTimeInMilliSecondsFormat(String dateTime) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    Date date = dateFormat.parse(dateTime);
    return date.getTime();
  }
}
