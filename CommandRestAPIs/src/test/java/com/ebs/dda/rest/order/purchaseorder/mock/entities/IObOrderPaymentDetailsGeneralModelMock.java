package com.ebs.dda.rest.order.purchaseorder.mock.entities;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderPaymentDetailsGeneralModel;

public class IObOrderPaymentDetailsGeneralModelMock extends IObOrderPaymentDetailsGeneralModel {

  private String orderCode;

  private String objectTypeCode;

  private String paymentTermCode;

  private String currencyIso;

  @Override
  public String getOrderCode() {
    return orderCode;
  }

  public void setOrderCode(String orderCode) {
    this.orderCode = orderCode;
  }

  @Override
  public String getObjectTypeCode() {
    return objectTypeCode;
  }

  public void setObjectTypeCode(String objectTypeCode) {
    this.objectTypeCode = objectTypeCode;
  }

  @Override
  public String getPaymentTermCode() {
    return paymentTermCode;
  }

  public void setPaymentTermCode(String paymentTermCode) {
    this.paymentTermCode = paymentTermCode;
  }

  @Override
  public String getCurrencyIso() {
    return currencyIso;
  }

  public void setCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
  }
}
