package com.ebs.dda.rest.accounting.vendorinvoice.mocks;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceItemsGeneralModel;

public class IObVendorInvoiceItemsGeneralModelMock extends IObVendorInvoiceItemsGeneralModel {

    private String itemCode;

    public IObVendorInvoiceItemsGeneralModelMock withItemCode (String itemCode) {
        this.itemCode = itemCode;
        return this;
    }

    @Override
    public String getItemCode() { return itemCode; }
}
