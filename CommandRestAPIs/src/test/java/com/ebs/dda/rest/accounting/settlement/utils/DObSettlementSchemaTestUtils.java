package com.ebs.dda.rest.accounting.settlement.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.settlement.apis.IDObSettlementJsonSchema;
import com.ebs.dda.jpa.accounting.settlements.IDObSettlementCreationValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObSettlementSchemaTestUtils {

	private JsonObject requestBody;

	public DObSettlementSchemaTestUtils() {
		requestBody = new JsonObject();
	}

	public DObSettlementSchemaTestUtils withType(String type) {
		requestBody.addProperty(IDObSettlementCreationValueObject.TYPE, type);
		return this;
	}

	public DObSettlementSchemaTestUtils withBusinessUnitCode(String businessUnitCode) {
		requestBody.addProperty(IDObSettlementCreationValueObject.BUSINESS_UNIT_CODE, businessUnitCode);
		return this;
	}

	public DObSettlementSchemaTestUtils withCompanyCode(String companyCode) {
		requestBody.addProperty(IDObSettlementCreationValueObject.COMPANY_CODE, companyCode);
		return this;
	}

	public DObSettlementSchemaTestUtils withCurrencyCode(String currencyCode) {
		requestBody.addProperty(IDObSettlementCreationValueObject.CURRENCY_CODE, currencyCode);
		return this;
	}

	public DObSettlementSchemaTestUtils withDocumentOwnerId(Long documentOwnerId) {
		requestBody.addProperty(IDObSettlementCreationValueObject.DOCUMENT_OWNER_ID, documentOwnerId);
		return this;
	}

	public DObSettlementSchemaTestUtils withStringDocumentOwnerId(String documentOwnerId) {
		requestBody.addProperty(IDObSettlementCreationValueObject.DOCUMENT_OWNER_ID, documentOwnerId);
		return this;
	}

	public void assertThrowsArgumentSecurityException() {
		assertThrows(ArgumentViolationSecurityException.class, () -> {
			CommonControllerUtils.applySchemaValidationForString(IDObSettlementJsonSchema.CREATION_SCHEMA , requestBody.toString());
		});
	}
}
