package com.ebs.dda.rest.order.purchaseorder.utils;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.purchases.apis.IDObPurchaseOrderJsonSchema;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObServicePurchaseOrder;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderCycleDatesGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IDObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderItemGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.repositories.businessobjects.documentobjects.DObPurchaseOrderRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderCycleDatesGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.DObPurchaseOrderGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderItemGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.repositories.generalmodels.IObOrderPaymentDetailsGeneralModelRep;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.DObPurchaseOrderConfirmValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rest.order.purchaseorder.mock.entities.DObPurchaseOrderGeneralModelMock;
import com.ebs.dda.rest.order.purchaseorder.mock.entities.IObOrderPaymentDetailsGeneralModelMock;
import com.ebs.dda.validation.purchaseorder.DObPurchaseOrderConfirmationValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

public class DObServicePurchaseOrderConfirmValidationTestsUtils {

  private final DObPurchaseOrderConfirmValueObject valueObject;
  private final DObPurchaseOrderConfirmationValidator validator;
  private DObPurchaseOrderRep purchaseOrderRep;
  private DObPurchaseOrderGeneralModelRep purchaseOrderGeneralModelRep;
  private IObOrderPaymentDetailsGeneralModelRep orderPaymentDetailsGeneralModelRep;
  private IObOrderItemGeneralModelRep orderItemGeneralModelRep;
  private DObPurchaseOrderCycleDatesGeneralModelRep purchaseOrderCycleDatesGeneralModelRep;
  private String valueObjectAsJson;
  private DObPurchaseOrderGeneralModelMock servicePurchaseOrderGeneralModelMock;
  private DObPurchaseOrderGeneralModelMock relatedPurchaseOrderGeneralModelMock;
  private IObOrderPaymentDetailsGeneralModelMock orderPaymentDetailsGeneralModelMock;

  public DObServicePurchaseOrderConfirmValidationTestsUtils() {
    valueObject = new DObPurchaseOrderConfirmValueObject();
    validator = new DObPurchaseOrderConfirmationValidator();
    purchaseOrderRep = Mockito.mock(DObPurchaseOrderRep.class);
    purchaseOrderGeneralModelRep = Mockito.mock(DObPurchaseOrderGeneralModelRep.class);
    purchaseOrderCycleDatesGeneralModelRep =
        Mockito.mock(DObPurchaseOrderCycleDatesGeneralModelRep.class);
    orderPaymentDetailsGeneralModelRep = Mockito.mock(IObOrderPaymentDetailsGeneralModelRep.class);
    orderItemGeneralModelRep = Mockito.mock(IObOrderItemGeneralModelRep.class);
    servicePurchaseOrderGeneralModelMock = new DObPurchaseOrderGeneralModelMock();
    relatedPurchaseOrderGeneralModelMock = new DObPurchaseOrderGeneralModelMock();
    orderPaymentDetailsGeneralModelMock = new IObOrderPaymentDetailsGeneralModelMock();
  }

  public void applySchemaValidationOnValueObject() throws Exception {
    String schema = IDObPurchaseOrderJsonSchema.Confirm;
    CommonControllerUtils.applySchemaValidationForString(schema, valueObjectAsJson);
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils convertValueObjectToJson()
      throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    valueObjectAsJson = objectMapper.writeValueAsString(valueObject);
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withConfirmationDateTime(
      String confirmationDateTime) throws ParseException {
    valueObject.setConfirmationDateTime(
        new DateTime(getDateTimeInMilliSecondsFormat(confirmationDateTime)));
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withConfirmationDateTimeAsString(
      String confirmationDateTime) {
    JsonObject confirmJsonObject = new JsonObject();
    confirmJsonObject.addProperty("confirmationDateTime", confirmationDateTime);
    valueObjectAsJson = confirmJsonObject.toString();
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withPurchaseOrderCode(String userCode) {
    servicePurchaseOrderGeneralModelMock.setUserCode(userCode);
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withObjectTypeCode(
      String objectTypeCode) {
    servicePurchaseOrderGeneralModelMock.setObjectTypeCode(objectTypeCode);
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withRefDocumentCode(
      String refDocumentCode) {
    servicePurchaseOrderGeneralModelMock.setRefDocumentCode(refDocumentCode);
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withPurchaseOrderInState(String state) {
    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);
    servicePurchaseOrderGeneralModelMock.setCurrentStates(currentStates);
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withRelatedPO(
      String userCode, String objectTypeCode, String state) {
    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);
    relatedPurchaseOrderGeneralModelMock.setUserCode(userCode);
    relatedPurchaseOrderGeneralModelMock.setObjectTypeCode(objectTypeCode);
    relatedPurchaseOrderGeneralModelMock.setCurrentStates(currentStates);
    return this;
  }

  public DObServicePurchaseOrderConfirmValidationTestsUtils withPaymentDetails(
      String userCode, String objectTypeCode, String currencyIso, String paymentTermCode) {
    orderPaymentDetailsGeneralModelMock.setOrderCode(userCode);
    orderPaymentDetailsGeneralModelMock.setObjectTypeCode(objectTypeCode);
    orderPaymentDetailsGeneralModelMock.setCurrencyIso(currencyIso);
    orderPaymentDetailsGeneralModelMock.setPaymentTermCode(paymentTermCode);
    return this;
  }

  public Map<String, Object> whenValidateServicePurchaseOrderConfirmValueObject()
      throws Exception {

    mockPurchaseOrderRep();
    mockPurchaseOrderGeneralModelRep();
    mockPurchaseOrderCycleDatesGeneralModelRep();
    mockOrderPaymentDetailsGeneralModelRep();
    mockOrderItemsGeneralModelRep();
    validator.setPurchaseOrderRep(purchaseOrderRep);
    validator.setPurchaseOrderCycleDatesGeneralModelRep(purchaseOrderCycleDatesGeneralModelRep);
    validator.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    validator.setOrderPaymentDetailsGeneralModelRep(orderPaymentDetailsGeneralModelRep);
    validator.setOrderItemGeneralModelRep(orderItemGeneralModelRep);

    return validator.validateMandatories(servicePurchaseOrderGeneralModelMock, valueObject);
  }

  public ValidationResult whenValidateServicePurchaseOrderConfirmBusinessRules() throws Exception {

    mockPurchaseOrderRep();
    mockPurchaseOrderGeneralModelRep();
    mockPurchaseOrderCycleDatesGeneralModelRep();
    mockOrderPaymentDetailsGeneralModelRep();
    mockOrderItemsGeneralModelRep();
    validator.setPurchaseOrderRep(purchaseOrderRep);
    validator.setPurchaseOrderCycleDatesGeneralModelRep(purchaseOrderCycleDatesGeneralModelRep);
    validator.setPurchaseOrderGeneralModelRep(purchaseOrderGeneralModelRep);
    validator.setOrderPaymentDetailsGeneralModelRep(orderPaymentDetailsGeneralModelRep);
    validator.setOrderItemGeneralModelRep(orderItemGeneralModelRep);

    return validator.validate(servicePurchaseOrderGeneralModelMock, valueObject);
  }

  public void
      assertThatErrorCode_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH_IsReturned()
          throws Exception {
    ValidationResult validationResult = whenValidateServicePurchaseOrderConfirmBusinessRules();
    String purchaseOrderErrorCode =
        validationResult.getErrorCode(IDObPurchaseOrderGeneralModel.REFERENCE_PO_CODE);
    Assertions.assertEquals(purchaseOrderErrorCode, "Gen-msg-53");
  }

  public void
      assertThatErrorCode_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH_IsNotReturned()
          throws Exception {
    ValidationResult validationResult = whenValidateServicePurchaseOrderConfirmBusinessRules();
    String purchaseOrderErrorCode =
        validationResult.getErrorCode(IDObPurchaseOrderGeneralModel.REFERENCE_PO_CODE);
    Assertions.assertNull(purchaseOrderErrorCode);
  }

  public void assertThatSectionFieldIsMissing(String sectionName, String missingField)
      throws Exception {
    Map<String, Object> sectionMissingFields =
        whenValidateServicePurchaseOrderConfirmValueObject();
    Object missingFields = sectionMissingFields.get(sectionName);
    Assert.assertEquals(Arrays.asList(missingField), missingFields);
  }

  public void assertThatSectionFieldIsNotMissing(String sectionName) throws Exception {
    Map<String, Object> sectionMissingFields =
        whenValidateServicePurchaseOrderConfirmValueObject();
    Object missingFields = sectionMissingFields.get(sectionName);
    Assert.assertNull(missingFields);
  }

  private void mockPurchaseOrderRep() {
    Mockito.when(purchaseOrderRep.findOneByUserCode("2020000006"))
        .thenReturn(new DObServicePurchaseOrder());
    Mockito.when(purchaseOrderRep.findOneByUserCode("2020000005"))
        .thenReturn(new DObServicePurchaseOrder());
  }

  private void mockPurchaseOrderGeneralModelRep() {
    Mockito.when(purchaseOrderGeneralModelRep.findOneByUserCode("2020000006"))
        .thenReturn(servicePurchaseOrderGeneralModelMock);
    Mockito.when(purchaseOrderGeneralModelRep.findOneByUserCode("2020000005"))
        .thenReturn(servicePurchaseOrderGeneralModelMock);
    Mockito.when(purchaseOrderGeneralModelRep.findOneByUserCode("2020000004"))
        .thenReturn(relatedPurchaseOrderGeneralModelMock);
  }

  private void mockPurchaseOrderCycleDatesGeneralModelRep() {
    Mockito.when(
            purchaseOrderCycleDatesGeneralModelRep.findOneByUserCodeOrderByIdDesc("2020000006"))
        .thenReturn(new DObPurchaseOrderCycleDatesGeneralModel());
    Mockito.when(
            purchaseOrderCycleDatesGeneralModelRep.findOneByUserCodeOrderByIdDesc("2020000005"))
        .thenReturn(new DObPurchaseOrderCycleDatesGeneralModel());
  }

  private void mockOrderPaymentDetailsGeneralModelRep() {
    Mockito.when(orderPaymentDetailsGeneralModelRep.findOneByOrderCode("2020000006"))
        .thenReturn(orderPaymentDetailsGeneralModelMock);
    Mockito.when(orderPaymentDetailsGeneralModelRep.findOneByOrderCode("2020000005"))
        .thenReturn(orderPaymentDetailsGeneralModelMock);
  }

  private void mockOrderItemsGeneralModelRep() {
    Mockito.when(orderItemGeneralModelRep.findByOrderCodeOrderByIdDesc("2020000006"))
        .thenReturn(new ArrayList<>());
    List<IObOrderItemGeneralModel> result = new ArrayList<>();
    result.add(new IObOrderItemGeneralModel());
    Mockito.when(orderItemGeneralModelRep.findByOrderCodeOrderByIdDesc("2020000005"))
        .thenReturn(result);
  }

  public Long getDateTimeInMilliSecondsFormat(String dateTime) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    Date date = dateFormat.parse(dateTime);
    return date.getTime();
  }
}
