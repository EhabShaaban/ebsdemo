package com.ebs.dda.rest.order.purchaseorder.items.utils;

import com.ebs.dda.jpa.order.IObPurchaseOrderItemValueObject;
import com.ebs.dda.purchases.apis.IDObPurchaseOrderJsonSchema;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;
import java.math.BigDecimal;

public class ServiceItemSaveSchemaValidationTestsUtils {

  private JsonObject valueObjectAsJson;

  public ServiceItemSaveSchemaValidationTestsUtils() {
    valueObjectAsJson = new JsonObject();
  }

  public void applySchemaValidationOnServiceItemValueObject() throws Exception {
    String saveServiceItemSchema = IDObPurchaseOrderJsonSchema.SAVE_ADD_SERVICE_ITEM;
    CommonControllerUtils.applySchemaValidationForString(saveServiceItemSchema, valueObjectAsJson.toString());
  }

  public ServiceItemSaveSchemaValidationTestsUtils withPrice(String price) {
    valueObjectAsJson.addProperty(
        IPurchaseOrderItemValueObject.PRICE, new BigDecimal(price));
    return this;
  }

  public ServiceItemSaveSchemaValidationTestsUtils withPriceAsString(String price) {
    valueObjectAsJson.addProperty(IPurchaseOrderItemValueObject.PRICE, price);
    return this;
  }

  public ServiceItemSaveSchemaValidationTestsUtils withQuantity(String quantity) {
    valueObjectAsJson.addProperty(
        IPurchaseOrderItemValueObject.QUANTITY, new BigDecimal(quantity));
    return this;
  }

  public ServiceItemSaveSchemaValidationTestsUtils withQuantityAsString(String quantity) {
    valueObjectAsJson.addProperty(IPurchaseOrderItemValueObject.QUANTITY, quantity);
    return this;
  }
}
