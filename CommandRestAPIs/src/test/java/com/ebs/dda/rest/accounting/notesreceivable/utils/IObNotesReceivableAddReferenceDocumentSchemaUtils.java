package com.ebs.dda.rest.accounting.notesreceivable.utils;

import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.jpa.accounting.monetarynotes.IIObMonetaryNotesReferenceDocumentsValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;

import java.math.BigDecimal;

public class IObNotesReceivableAddReferenceDocumentSchemaUtils {

  private JsonObject valueObjectAsJson;

  public IObNotesReceivableAddReferenceDocumentSchemaUtils() {
    valueObjectAsJson = new JsonObject();
  }

  public void applySchemaValidationOnReferenceDocumentValueObject() throws Exception {
    String schema = IDObNotesReceivableJsonSchema.SAVE_ADD_REF_DOCUMENT_SCHEMA;
    CommonControllerUtils.applySchemaValidationForString(schema, valueObjectAsJson.toString());
  }

  public IObNotesReceivableAddReferenceDocumentSchemaUtils withDocumentType(String documentType) {
    valueObjectAsJson.addProperty(
        IIObMonetaryNotesReferenceDocumentsValueObject.DOCUMENT_TYPE, documentType);
    return this;
  }

  public IObNotesReceivableAddReferenceDocumentSchemaUtils withRefDocumentCode(
      String refDocumentCode) {
    valueObjectAsJson.addProperty(
        IIObMonetaryNotesReferenceDocumentsValueObject.REF_DOCUMENT_CODE, refDocumentCode);
    return this;
  }

  public IObNotesReceivableAddReferenceDocumentSchemaUtils withAmountToCollect(
      String amountToCollect) {
    valueObjectAsJson.addProperty(
        IIObMonetaryNotesReferenceDocumentsValueObject.AMOUNT_TO_COLLECT,
        new BigDecimal(amountToCollect));
    return this;
  }

  public IObNotesReceivableAddReferenceDocumentSchemaUtils withAmountToCollectAsString(
      String amountToCollect) {
    valueObjectAsJson.addProperty(
        IIObMonetaryNotesReferenceDocumentsValueObject.AMOUNT_TO_COLLECT, amountToCollect);
    return this;
  }
}
