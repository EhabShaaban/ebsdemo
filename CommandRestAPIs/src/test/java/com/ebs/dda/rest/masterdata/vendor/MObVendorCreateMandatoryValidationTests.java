package com.ebs.dda.rest.masterdata.vendor;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.masterdata.vendor.utils.MObVendorCreateValidationTestsUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class MObVendorCreateMandatoryValidationTests {

  private MObVendorCreateValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new MObVendorCreateValidationTestsUtils();
  }

  // OldVendorReference Tests

  @Test
  public void Should_Pass_When_OldVendorReferenceExistsInValueObject() {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("1");
    assertDoesNotThrow(() -> utils.whenValidateVendorCreateValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldVendorReferenceNotExistsInValueObject() {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateVendorCreateValueObject());
  }
}
