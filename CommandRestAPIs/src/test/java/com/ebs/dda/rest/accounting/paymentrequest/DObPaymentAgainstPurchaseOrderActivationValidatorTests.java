package com.ebs.dda.rest.accounting.paymentrequest;

import com.ebs.dda.rest.accounting.paymentrequest.utils.DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

public class DObPaymentAgainstPurchaseOrderActivationValidatorTests {
  private DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils utils;

  @Before
  public void setUp() {
    utils = new DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils();
  }

  @Test
  public void Should_Pass_WhenUpdatePoRemainingWithPaymentAmountLessThanOrEqualRemaining()
      throws Exception {
    utils
        .withPaymentCode("2019000030")
        .withPaymentAmount(BigDecimal.valueOf(1000))
        .withPurchaseOrderCode("2019000108")
        .withPoRemaining(BigDecimal.valueOf(488000))
        .withCurrencyPrice(BigDecimal.valueOf(17.126738))
        .withExchangeRate("2018000026")
        .withBalance(BigDecimal.valueOf(2000))
        .withDueDate("06-Jan-2019 12:00 AM")
        .validateSuccessfully();
  }

  @Test
  public void
      Should_throwsPaymentAmountGreaterThanPurchaseOrderException_WhenUpdatePoRemainingWithPaymentAmountgreaterThanRemaining()
          throws Exception {
    utils
        .withPaymentCode("2019000030")
        .withPaymentAmount(BigDecimal.valueOf(489000))
        .withPurchaseOrderCode("2019000108")
        .withPoRemaining(BigDecimal.valueOf(488000))
        .withCurrencyPrice(BigDecimal.valueOf(17.126738))
        .withExchangeRate("2018000026")
        .withBalance(BigDecimal.valueOf(499000))
        .withDueDate("06-Jan-2019 12:00 AM")
        .throwsPaymentAmountGreaterThanPurchaseOrderException();
  }
}
