package com.ebs.dda.rest.accounting.notesreceivable.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.notesreceivable.*;
import com.ebs.entities.accounting.FiscalYearMockUtils;
import com.ebs.entities.accounting.monetarynotes.DObNotesReceivableMockUtils;
import com.ebs.entities.accounting.monetarynotes.IObNotesReceivableDetailsMockUtils;
import com.ebs.entities.accounting.monetarynotes.IObNotesReceivableReferenceDocumentMockUtils;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceMockUtils;
import com.ebs.entities.accounting.salesinvoice.IObSalesInvoiceSummaryMockUtils;
import com.ebs.entities.order.salesorder.DObSalesOrderMockUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObNotesReceivableActivateValidatorUtils {

  List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocumentGeneralModels;
  List<IObNotesReceivablesReferenceDocumentGeneralModel> uniqueReferenceDocumentGeneralModels;
  String notesReceivableCode;
  String activeState = "Active";

  private DObNotesReceivableActivateValueObject valueObject;
  private String valueObjectAsJson;
  private IObNotesReceivableRefDocumentBusinessValidator notesReceivableRefDocumentBusinessValidator;
  private DObNotesReceivableActivateValidator validator;
  private DObNotesReceivableActivateMandatoriesValidator mandatoriesValidator;
  private IObNotesReceivableRefDocumentValidator notesReceivableRefDocumentValidator;
  private IObNotesReceivableRefDocumentNRValidator refDocumentNRValidator;
  private IObNotesReceivableRefDocumentSIValidator refDocumentSIValidator;
  private IObNotesReceivableRefDocumentSOValidator refDocumentSOValidator;

  private IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsGeneralModelRep;
  private IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;
  private CObFiscalYearRep fiscalYearRep;

  public DObNotesReceivableActivateValidatorUtils() {
    valueObject = new DObNotesReceivableActivateValueObject();
    notesReceivableRefDocumentBusinessValidator = new IObNotesReceivableRefDocumentBusinessValidator();
    validator = new DObNotesReceivableActivateValidator();
    mandatoriesValidator = new DObNotesReceivableActivateMandatoriesValidator();
    referenceDocumentGeneralModels = new ArrayList<>();
    uniqueReferenceDocumentGeneralModels = new ArrayList<>();
    notesReceivableRefDocumentValidator = new IObNotesReceivableRefDocumentValidator();
    refDocumentNRValidator = new IObNotesReceivableRefDocumentNRValidator();
    refDocumentSIValidator = new IObNotesReceivableRefDocumentSIValidator();
    refDocumentSOValidator = new IObNotesReceivableRefDocumentSOValidator();
    mockDependencies();
    injectValidatorDependencies();
  }

  public DObNotesReceivableActivateValidatorUtils withNotesReceivable(
      Long id,
      String userCode,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {
    valueObject.setNotesReceivableCode(userCode);
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        DObNotesReceivableMockUtils.mockGeneralModel()
            .id(id)
            .notesReceivableCode(userCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .businessPartnerCode(businessPartnerCode)
            .build();

    Mockito.when(notesReceivablesGeneralModelRep.findOneByUserCode(userCode))
        .thenReturn(notesReceivablesGeneralModel);
    return this;
  }

  public DObNotesReceivableActivateValidatorUtils withJournalEntryDate(String date)
      throws ParseException, JsonProcessingException {
    this.valueObject.setJournalEntryDate(date);
    convertValueObjectToJson(date);
    mockFiscalYear();
    return this;
  }

  public void convertValueObjectToJson(String date) throws JsonProcessingException {
    JsonObject jsonObject = new JsonObject();
    jsonObject.addProperty("journalEntryDate", date);
    valueObjectAsJson = jsonObject.toString();
  }

  public void withoutJournalEntryDate(){
    JsonObject jsonObject = new JsonObject();
    valueObjectAsJson = jsonObject.toString();
  }


  public DObNotesReceivableActivateValidatorUtils withReferenceDocumentData(
      String refDocumentCode,
      String notesReceivableCode,
      BigDecimal amountToCollect,
      String documentType) {

    IObNotesReceivablesReferenceDocumentGeneralModel notesReceivablesReferenceDocumentGeneralModel =
        IObNotesReceivableReferenceDocumentMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .refDocumentCode(refDocumentCode)
            .amountToCollect(amountToCollect)
            .documentType(documentType)
            .build();

    referenceDocumentGeneralModels.add(notesReceivablesReferenceDocumentGeneralModel);
    this.notesReceivableCode = notesReceivableCode;

    return this;
  }

  public DObNotesReceivableActivateValidatorUtils withReturnedReferenceDocumentData() {
    Mockito.when(
            monetaryNotesReferenceDocumentsRep.findByNotesReceivableCodeOrderByIdAsc(
                notesReceivableCode))
        .thenReturn(referenceDocumentGeneralModels);
    mockRefDocument();
    return this;
  }

  public DObNotesReceivableActivateValidatorUtils withNotesDetails(
      Long refInstanceId, String notesReceivableCode, BigDecimal amount, BigDecimal remaining) {

    IObMonetaryNotesDetailsGeneralModel monetaryNotesDetailsGeneralModel =
        IObNotesReceivableDetailsMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .refInstanceId(refInstanceId)
            .amount(amount)
            .remaining(remaining)
            .build();

    Mockito.when(monetaryNotesDetailsGeneralModelRep.findOneByRefInstanceId(refInstanceId))
        .thenReturn(monetaryNotesDetailsGeneralModel);

    return this;
  }

  public DObNotesReceivableActivateValidatorUtils withNotesFullDetails(
      Long refInstanceId,
      String notesReceivableCode,
      BigDecimal amount,
      BigDecimal remaining,
      String noteNumber,
      String noteBank,
      DateTime dueDate,
      String depotCode,
      String noteForm,
      String currencyIso,
      String businessPartnerCode) {

    IObMonetaryNotesDetailsGeneralModel monetaryNotesDetailsGeneralModel =
        IObNotesReceivableDetailsMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .refInstanceId(refInstanceId)
            .amount(amount)
            .remaining(remaining)
            .noteNumber(noteNumber)
            .noteBank(noteBank)
            .dueDate(dueDate)
            .depotCode(depotCode)
            .noteForm(noteForm)
            .currencyIso(currencyIso)
            .businessPartnerCode(businessPartnerCode)
            .build();

    Mockito.when(
            monetaryNotesDetailsGeneralModelRep.findByUserCodeAndObjectTypeCode(
                notesReceivableCode, NotesReceivableTypeEnum.NOTES_RECEIVABLE_AS_COLLECTION.name()))
        .thenReturn(monetaryNotesDetailsGeneralModel);

    this.notesReceivableCode = notesReceivableCode;

    return this;
  }

  public DObNotesReceivableActivateValidatorUtils withNotesReceivableReferenceDocument(
      Long id,
      String notesReceivableCode,
      String state,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {

    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        DObNotesReceivableMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .id(id)
            .currentStates(currentStates)
            .businessPartnerCode(businessPartnerCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .build();

    Mockito.when(notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode))
        .thenReturn(notesReceivablesGeneralModel);

    return this;
  }

  public DObNotesReceivableActivateValidatorUtils withSalesOrderDataReferenceDocument(
      String salesOrderCode,
      BigDecimal remaining,
      String state,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {

    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        DObSalesOrderMockUtils.mockGeneralModel()
            .salesOrderCode(salesOrderCode)
            .remaining(remaining)
            .currentStates(currentStates)
            .businessPartnerCode(businessPartnerCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .build();

    Mockito.when(salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode))
        .thenReturn(salesOrderGeneralModel);

    return this;
  }

  public DObNotesReceivableActivateValidatorUtils withSalesInvoiceDataReferenceDocument(
      String salesInvoiceCode,
      String remaining,
      String state,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {

    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        DObSalesInvoiceMockUtils.mockGeneralModel()
            .salesInvoiceCode(salesInvoiceCode)
            .currentStates(currentStates)
            .businessPartnerCode(businessPartnerCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .build();

    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        IObSalesInvoiceSummaryMockUtils.mockGeneralModel()
            .salesInvoiceCode(salesInvoiceCode)
            .remaining(remaining)
            .build();

    Mockito.when(
            invoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
                salesInvoiceCode, "SalesInvoice"))
        .thenReturn(invoiceSummaryGeneralModel);

    Mockito.when(salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode))
        .thenReturn(salesInvoiceGeneralModel);

    return this;
  }

  public DObNotesReceivableActivateValueObject getValueObject() {
    return valueObject;
  }

  public void applySchemaValidationOnValueObject() throws Exception {
    String schema = IDObNotesReceivableJsonSchema.ACTIVATION_SCHEMA;
    CommonControllerUtils.applySchemaValidationForString(schema, valueObjectAsJson);
  }

  public void assertErrorDObNotesReceivableAmountToCollectNotEqualsNRAmountIsReturned() {
    ValidationResult validationResult = validator.validateBusinessRules(getValueObject());

    String amountErrorCode =
        validationResult.getErrorCode(IIObMonetaryNotesDetailsGeneralModel.AMOUNT);
    Assertions.assertEquals(amountErrorCode, "NR-msg-05");
  }

  public void assertErrorDObNotesReceivableAmountToCollectNotEqualsNRAmountIsNotReturned() {
    ValidationResult validationResult = validator.validateBusinessRules(getValueObject());

    String amountErrorCode =
        validationResult.getErrorCode(IIObMonetaryNotesDetailsGeneralModel.AMOUNT);
    Assertions.assertNull(amountErrorCode);
  }

  public void
      assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsReturned() {
    ValidationResult validationResult = validator.validateBusinessRules(getValueObject());

    String refDocumentAmountErrorCode =
        validationResult.getErrorCode(
            IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT);

    List<String> refDocumentAmountErrorValues =
        validationResult.getErrorValues(
            IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT);
    Assertions.assertEquals(refDocumentAmountErrorCode, "NR-msg-06");
    List<String> referenceDocumentWithError = new ArrayList<>();
    referenceDocumentWithError.add("SALES_ORDER " + "2021000003");
    referenceDocumentWithError.add("SALES_INVOICE " + "2021000005");
    Assertions.assertEquals(refDocumentAmountErrorValues, referenceDocumentWithError);
  }

  public void
      assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsNotReturned() {
    ValidationResult validationResult = validator.validateBusinessRules(getValueObject());

    String refDocumentAmountErrorCode =
        validationResult.getErrorCode(
            IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE);

    Assertions.assertNull(refDocumentAmountErrorCode);
  }

  public void assertMissingField(String field) {
    Map<String, Object> missingFields =
        mandatoriesValidator.validateMissingFields(notesReceivableCode, activeState);
    List<Object> actualMissingFields =
        (List<Object>) Arrays.asList(missingFields.get(activeState)).get(0);
    List<String> expectedMissingFields = new LinkedList<>();
    expectedMissingFields.add(field);
    Assertions.assertEquals(expectedMissingFields, actualMissingFields);
  }

  public void assertDoesNotThrowException() {
    assertDoesNotThrow(() -> validator.validateBusinessRules(getValueObject()));
  }

  public void assertThrowsArgumentViolationSecurityException() {
    assertThrows(
        ArgumentViolationSecurityException.class,
        () -> validator.validateBusinessRules(getValueObject()));
  }

  public void assertObjectStateNotValidException() {
    ValidationResult validationResult = validator.validateBusinessRules(getValueObject());

    String refDocumentAmountErrorCode =
        validationResult.getErrorCode(
            IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE);

    List<String> refDocumentAmountErrorValues =
        validationResult.getErrorValues(
            IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE);
    Assertions.assertEquals(refDocumentAmountErrorCode, "NR-msg-07");
    List<String> referenceDocumentWithError = new ArrayList<>();
    referenceDocumentWithError.add("2021000003");
    referenceDocumentWithError.add("2021000005");
    Assertions.assertEquals(refDocumentAmountErrorValues, referenceDocumentWithError);
  }

  private void injectValidatorDependencies() {
    notesReceivableRefDocumentBusinessValidator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    notesReceivableRefDocumentBusinessValidator.setMonetaryNotesDetailsGeneralModelRep(monetaryNotesDetailsGeneralModelRep);
    notesReceivableRefDocumentBusinessValidator.setMonetaryNotesReferenceDocumentsRep(monetaryNotesReferenceDocumentsRep);
    notesReceivableRefDocumentBusinessValidator.setNotesReceivableRefDocumentValidator(notesReceivableRefDocumentValidator);

    validator.setFiscalYearRep(fiscalYearRep);
    validator.setMonetaryNotesReferenceDocumentsRep(monetaryNotesReferenceDocumentsRep);
    validator.setNotesReceivableRefDocumentBusinessValidator(notesReceivableRefDocumentBusinessValidator);

    mandatoriesValidator.setMonetaryNotesDetailsRep(monetaryNotesDetailsGeneralModelRep);
    mandatoriesValidator.setNotesReceivablesReferenceDocumentRep(
        monetaryNotesReferenceDocumentsRep);
    notesReceivableRefDocumentValidator.setRefDocumentSIValidator(refDocumentSIValidator);
    notesReceivableRefDocumentValidator.setRefDocumentSOValidator(refDocumentSOValidator);
    notesReceivableRefDocumentValidator.setRefDocumentNRValidator(refDocumentNRValidator);
    refDocumentSOValidator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    refDocumentNRValidator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    refDocumentNRValidator.setNotesDetailsGeneralModelRep(monetaryNotesDetailsGeneralModelRep);
    refDocumentSIValidator.setSalesInvoiceSummaryRep(invoiceSummaryGeneralModelRep);
    refDocumentSIValidator.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);
  }

  private void mockDependencies() {
    notesReceivablesGeneralModelRep = Mockito.mock(DObNotesReceivablesGeneralModelRep.class);
    monetaryNotesDetailsGeneralModelRep =
        Mockito.mock(IObMonetaryNotesDetailsGeneralModelRep.class);
    monetaryNotesReferenceDocumentsRep =
        Mockito.mock(IObNotesReceivablesReferenceDocumentGeneralModelRep.class);
    salesOrderGeneralModelRep = Mockito.mock(DObSalesOrderGeneralModelRep.class);
    invoiceSummaryGeneralModelRep = Mockito.mock(IObInvoiceSummaryGeneralModelRep.class);
    salesInvoiceGeneralModelRep = Mockito.mock(DObSalesInvoiceGeneralModelRep.class);
    fiscalYearRep = Mockito.mock(CObFiscalYearRep.class);
  }

  private void mockRefDocument() {
    uniqueReferenceDocumentGeneralModels.add(
        new IObNotesReceivablesReferenceDocumentGeneralModel());
    Mockito.when(
            monetaryNotesReferenceDocumentsRep
                .findByNotesReceivableCodeAndRefDocumentCodeAndDocumentTypeOrderByIdAsc(
                    "2021000001", "2021000003", "NOTES_RECEIVABLE"))
        .thenReturn(uniqueReferenceDocumentGeneralModels);
    Mockito.when(
            monetaryNotesReferenceDocumentsRep
                .findByNotesReceivableCodeAndRefDocumentCodeAndDocumentTypeOrderByIdAsc(
                    "2021000001", "2021000003", "SALES_ORDER"))
        .thenReturn(uniqueReferenceDocumentGeneralModels);
    Mockito.when(
            monetaryNotesReferenceDocumentsRep
                .findByNotesReceivableCodeAndRefDocumentCodeAndDocumentTypeOrderByIdAsc(
                    "2021000001", "2021000003", "SALES_INVOICE"))
        .thenReturn(uniqueReferenceDocumentGeneralModels);
    uniqueReferenceDocumentGeneralModels = new ArrayList<>();
    uniqueReferenceDocumentGeneralModels.add(
        new IObNotesReceivablesReferenceDocumentGeneralModel());
    uniqueReferenceDocumentGeneralModels.add(
        new IObNotesReceivablesReferenceDocumentGeneralModel());
    Mockito.when(
            monetaryNotesReferenceDocumentsRep
                .findByNotesReceivableCodeAndRefDocumentCodeAndDocumentTypeOrderByIdAsc(
                    "2021000001", "2021000004", "NOTES_RECEIVABLE"))
        .thenReturn(uniqueReferenceDocumentGeneralModels);
  }

  public Long getDateTimeInMilliSecondsFormat(String dateTime) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    Date date = dateFormat.parse(dateTime);
    return date.getTime();
  }

  private void mockFiscalYear() throws ParseException {
    CObFiscalYear firstFiscalYear =
        FiscalYearMockUtils.mockEntity()
            .id(1L)
            .currentState("%" + activeState + "%")
            .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
            .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
            .build();
    CObFiscalYear secondFiscalYear =
        FiscalYearMockUtils.mockEntity()
            .id(2L)
            .currentState("%" + activeState + "%")
            .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
            .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2023 00:00 AM")))
            .build();
    List<CObFiscalYear> fiscalYearList = new ArrayList<>();
    fiscalYearList.add(firstFiscalYear);
    fiscalYearList.add(secondFiscalYear);
    Mockito.when(fiscalYearRep.findByCurrentStatesLike("%" + activeState + "%"))
        .thenReturn(fiscalYearList);
  }
}
