package com.ebs.dda.rest.accounting.notesreceivable.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.notesreceivables.apis.IDObNotesReceivableJsonSchema;
import com.ebs.dda.jpa.accounting.IObInvoiceSummaryGeneralModel;
import com.ebs.dda.jpa.accounting.monetarynotes.*;
import com.ebs.dda.jpa.accounting.salesinvoice.DObSalesInvoiceGeneralModel;
import com.ebs.dda.jpa.order.salesorder.DObSalesOrderGeneralModel;
import com.ebs.dda.repositories.accounting.IObInvoiceSummaryGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObNotesReceivablesGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObMonetaryNotesDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.IObNotesReceivablesReferenceDocumentGeneralModelRep;
import com.ebs.dda.repositories.accounting.salesinvoice.DObSalesInvoiceGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.notesreceivable.*;
import com.ebs.entities.accounting.monetarynotes.DObNotesReceivableMockUtils;
import com.ebs.entities.accounting.monetarynotes.IObNotesReceivableDetailsMockUtils;
import com.ebs.entities.accounting.monetarynotes.IObNotesReceivableReferenceDocumentMockUtils;
import com.ebs.entities.accounting.salesinvoice.DObSalesInvoiceMockUtils;
import com.ebs.entities.accounting.salesinvoice.IObSalesInvoiceSummaryMockUtils;
import com.ebs.entities.order.salesorder.DObSalesOrderMockUtils;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class IObNotesReceivableRefDocumentValidatorUtils {

  List<IObNotesReceivablesReferenceDocumentGeneralModel> referenceDocumentGeneralModels;
  String notesReceivableCode;

  private IObMonetaryNotesReferenceDocumentsValueObject valueObject;
  final boolean isSavedRefDocBefore = false;
  private String valueObjectAsJson;
  private IObNotesReceivableRefDocumentBusinessValidator
      notesReceivableRefDocumentBusinessValidator;

  private IObNotesReceivableRefDocumentValidator notesReceivableRefDocumentValidator;
  private IObNotesReceivableRefDocumentNRValidator refDocumentNRValidator;
  private IObNotesReceivableRefDocumentSIValidator refDocumentSIValidator;
  private IObNotesReceivableRefDocumentSOValidator refDocumentSOValidator;

  private IObMonetaryNotesDetailsGeneralModelRep monetaryNotesDetailsGeneralModelRep;
  private IObNotesReceivablesReferenceDocumentGeneralModelRep monetaryNotesReferenceDocumentsRep;
  private DObNotesReceivablesGeneralModelRep notesReceivablesGeneralModelRep;
  private DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private IObInvoiceSummaryGeneralModelRep invoiceSummaryGeneralModelRep;
  private DObSalesInvoiceGeneralModelRep salesInvoiceGeneralModelRep;

  public IObNotesReceivableRefDocumentValidatorUtils() {
    valueObject = new IObMonetaryNotesReferenceDocumentsValueObject();
    notesReceivableRefDocumentBusinessValidator =
        new IObNotesReceivableRefDocumentBusinessValidator();
    referenceDocumentGeneralModels = new ArrayList<>();
    notesReceivableRefDocumentValidator = new IObNotesReceivableRefDocumentValidator();
    refDocumentNRValidator = new IObNotesReceivableRefDocumentNRValidator();
    refDocumentSIValidator = new IObNotesReceivableRefDocumentSIValidator();
    refDocumentSOValidator = new IObNotesReceivableRefDocumentSOValidator();
    mockDependencies();
    injectValidatorDependencies();
  }

  public IObNotesReceivableRefDocumentValidatorUtils withNotesReceivable(
      Long id,
      String userCode,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {
    valueObject.setNotesReceivableCode(userCode);
    this.notesReceivableCode = userCode;
    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        DObNotesReceivableMockUtils.mockGeneralModel()
            .id(id)
            .notesReceivableCode(userCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .businessPartnerCode(businessPartnerCode)
            .build();

    Mockito.when(notesReceivablesGeneralModelRep.findOneByUserCode(userCode))
        .thenReturn(notesReceivablesGeneralModel);
    return this;
  }

  public IObNotesReceivableRefDocumentValidatorUtils withReferenceDocumentData(
      String refDocumentCode,
      String notesReceivableCode,
      BigDecimal amountToCollect,
      String documentType) {

    IObNotesReceivablesReferenceDocumentGeneralModel notesReceivablesReferenceDocumentGeneralModel =
        IObNotesReceivableReferenceDocumentMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .refDocumentCode(refDocumentCode)
            .amountToCollect(amountToCollect)
            .documentType(documentType)
            .build();

    referenceDocumentGeneralModels.add(notesReceivablesReferenceDocumentGeneralModel);

    return this;
  }

  public IObNotesReceivableRefDocumentValidatorUtils requestToAddReferenceDocumentWithData(
      String refDocumentCode,
      String notesReceivableCode,
      BigDecimal amountToCollect,
      String documentType) {

    valueObject.setAmountToCollect(amountToCollect);
    valueObject.setDocumentType(documentType);
    valueObject.setRefDocumentCode(refDocumentCode);

    return this;
  }

  public IObNotesReceivableRefDocumentValidatorUtils withReturnedReferenceDocumentData() {
    Mockito.when(
            monetaryNotesReferenceDocumentsRep.findByNotesReceivableCodeOrderByIdAsc(
                notesReceivableCode))
        .thenReturn(referenceDocumentGeneralModels);
    return this;
  }

  public IObNotesReceivableRefDocumentValidatorUtils withNotesDetails(
      Long refInstanceId, String notesReceivableCode, BigDecimal amount, BigDecimal remaining) {

    IObMonetaryNotesDetailsGeneralModel monetaryNotesDetailsGeneralModel =
        IObNotesReceivableDetailsMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .refInstanceId(refInstanceId)
            .amount(amount)
            .remaining(remaining)
            .build();

    Mockito.when(monetaryNotesDetailsGeneralModelRep.findOneByRefInstanceId(refInstanceId))
        .thenReturn(monetaryNotesDetailsGeneralModel);

    return this;
  }

  public IObNotesReceivableRefDocumentValidatorUtils withNotesReceivableReferenceDocument(
      Long id,
      String notesReceivableCode,
      String state,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {

    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);

    DObNotesReceivablesGeneralModel notesReceivablesGeneralModel =
        DObNotesReceivableMockUtils.mockGeneralModel()
            .notesReceivableCode(notesReceivableCode)
            .id(id)
            .currentStates(currentStates)
            .businessPartnerCode(businessPartnerCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .build();

    Mockito.when(notesReceivablesGeneralModelRep.findOneByUserCode(notesReceivableCode))
        .thenReturn(notesReceivablesGeneralModel);

    return this;
  }

  public IObNotesReceivableRefDocumentValidatorUtils withSalesOrderDataReferenceDocument(
      String salesOrderCode,
      BigDecimal remaining,
      String state,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {

    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);

    DObSalesOrderGeneralModel salesOrderGeneralModel =
        DObSalesOrderMockUtils.mockGeneralModel()
            .salesOrderCode(salesOrderCode)
            .remaining(remaining)
            .currentStates(currentStates)
            .businessPartnerCode(businessPartnerCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .build();

    Mockito.when(salesOrderGeneralModelRep.findOneByUserCode(salesOrderCode))
        .thenReturn(salesOrderGeneralModel);

    return this;
  }

  public IObNotesReceivableRefDocumentValidatorUtils withSalesInvoiceDataReferenceDocument(
      String salesInvoiceCode,
      String remaining,
      String state,
      String businessPartnerCode,
      String businessUnitCode,
      String companyCode) {

    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);

    DObSalesInvoiceGeneralModel salesInvoiceGeneralModel =
        DObSalesInvoiceMockUtils.mockGeneralModel()
            .salesInvoiceCode(salesInvoiceCode)
            .currentStates(currentStates)
            .businessPartnerCode(businessPartnerCode)
            .businessUnitCode(businessUnitCode)
            .companyCode(companyCode)
            .build();

    IObInvoiceSummaryGeneralModel invoiceSummaryGeneralModel =
        IObSalesInvoiceSummaryMockUtils.mockGeneralModel()
            .salesInvoiceCode(salesInvoiceCode)
            .remaining(remaining)
            .build();

    Mockito.when(
            invoiceSummaryGeneralModelRep.findOneByInvoiceCodeAndInvoiceType(
                salesInvoiceCode, "SalesInvoice"))
        .thenReturn(invoiceSummaryGeneralModel);

    Mockito.when(salesInvoiceGeneralModelRep.findOneByUserCode(salesInvoiceCode))
        .thenReturn(salesInvoiceGeneralModel);

    return this;
  }

  public IObMonetaryNotesReferenceDocumentsValueObject getValueObject() {
    return valueObject;
  }

  public void applySchemaValidationOnValueObject() throws Exception {
    String schema = IDObNotesReceivableJsonSchema.ACTIVATION_SCHEMA;
    CommonControllerUtils.applySchemaValidationForString(schema, valueObjectAsJson);
  }

  public void assertErrorDObNotesReceivableAmountToCollectGreaterThanNRAmountIsReturned() {
    ValidationResult validationResult = new ValidationResult();
    notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
        valueObject, isSavedRefDocBefore, validationResult);

    String amountErrorCode =
        validationResult.getErrorCode(IIObMonetaryNotesDetailsGeneralModel.AMOUNT);
    Assertions.assertEquals(amountErrorCode, "NR-msg-05");
  }

  public void assertErrorDObNotesReceivableAmountToCollectNotEqualsNRAmountIsNotReturned() {
    ValidationResult validationResult = new ValidationResult();
    notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
        valueObject, isSavedRefDocBefore, validationResult);

    String amountErrorCode =
        validationResult.getErrorCode(IIObMonetaryNotesDetailsGeneralModel.AMOUNT);
    Assertions.assertNull(amountErrorCode);
  }

  public void
      assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsReturned() {
    ValidationResult validationResult = new ValidationResult();
    notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
        valueObject, isSavedRefDocBefore, validationResult);

    String refDocumentAmountErrorCode =
        validationResult.getErrorCode(
            IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT);

    List<String> refDocumentAmountErrorValues =
        validationResult.getErrorValues(
            IIObNotesReceivablesReferenceDocumentGeneralModel.AMOUNT_TO_COLLECT);
    Assertions.assertEquals(refDocumentAmountErrorCode, "NR-msg-06");
    List<String> referenceDocumentWithError = new ArrayList<>();
    referenceDocumentWithError.add("SALES_INVOICE " + "2021000005");
    Assertions.assertEquals(refDocumentAmountErrorValues, referenceDocumentWithError);
  }

  public void
      assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsNotReturned() {
    ValidationResult validationResult = new ValidationResult();
    notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
        valueObject, isSavedRefDocBefore, validationResult);

    String refDocumentAmountErrorCode =
        validationResult.getErrorCode(
            IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE);

    Assertions.assertNull(refDocumentAmountErrorCode);
  }

  public void assertDoesNotThrowException() {
    assertDoesNotThrow(
        () -> {
          ValidationResult validationResult = new ValidationResult();
          notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
              valueObject, isSavedRefDocBefore, validationResult);
        });
  }

  public void assertThrowsArgumentViolationSecurityException() {
    assertThrows(
        ArgumentViolationSecurityException.class,
        () -> {
          ValidationResult validationResult = new ValidationResult();
          notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
              valueObject, isSavedRefDocBefore, validationResult);
        });
  }

  public void assertObjectStateNotValidException() {
    ValidationResult validationResult = new ValidationResult();
    notesReceivableRefDocumentBusinessValidator.validateBusinessRules(
        valueObject, isSavedRefDocBefore, validationResult);

    String refDocumentAmountErrorCode =
        validationResult.getErrorCode(
            IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE);

    List<String> refDocumentAmountErrorValues =
        validationResult.getErrorValues(
            IIObNotesReceivablesReferenceDocumentGeneralModel.REF_DOCUMENT_CODE);
    Assertions.assertEquals(refDocumentAmountErrorCode, "NR-msg-07");
    List<String> referenceDocumentWithError = new ArrayList<>();
    referenceDocumentWithError.add("2021000003");
    Assertions.assertEquals(refDocumentAmountErrorValues, referenceDocumentWithError);
  }

  private void injectValidatorDependencies() {
    notesReceivableRefDocumentValidator.setRefDocumentSIValidator(refDocumentSIValidator);
    notesReceivableRefDocumentValidator.setRefDocumentSOValidator(refDocumentSOValidator);
    notesReceivableRefDocumentValidator.setRefDocumentNRValidator(refDocumentNRValidator);
    refDocumentSOValidator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    refDocumentNRValidator.setNotesReceivablesGeneralModelRep(notesReceivablesGeneralModelRep);
    refDocumentNRValidator.setNotesDetailsGeneralModelRep(monetaryNotesDetailsGeneralModelRep);
    refDocumentSIValidator.setSalesInvoiceSummaryRep(invoiceSummaryGeneralModelRep);
    refDocumentSIValidator.setSalesInvoiceGeneralModelRep(salesInvoiceGeneralModelRep);

    notesReceivableRefDocumentBusinessValidator.setNotesReceivablesGeneralModelRep(
        notesReceivablesGeneralModelRep);
    notesReceivableRefDocumentBusinessValidator.setMonetaryNotesDetailsGeneralModelRep(
        monetaryNotesDetailsGeneralModelRep);
    notesReceivableRefDocumentBusinessValidator.setMonetaryNotesReferenceDocumentsRep(
        monetaryNotesReferenceDocumentsRep);
    notesReceivableRefDocumentBusinessValidator.setNotesReceivableRefDocumentValidator(
        notesReceivableRefDocumentValidator);
  }

  private void mockDependencies() {
    notesReceivablesGeneralModelRep = Mockito.mock(DObNotesReceivablesGeneralModelRep.class);
    monetaryNotesDetailsGeneralModelRep =
        Mockito.mock(IObMonetaryNotesDetailsGeneralModelRep.class);
    monetaryNotesReferenceDocumentsRep =
        Mockito.mock(IObNotesReceivablesReferenceDocumentGeneralModelRep.class);
    salesOrderGeneralModelRep = Mockito.mock(DObSalesOrderGeneralModelRep.class);
    invoiceSummaryGeneralModelRep = Mockito.mock(IObInvoiceSummaryGeneralModelRep.class);
    salesInvoiceGeneralModelRep = Mockito.mock(DObSalesInvoiceGeneralModelRep.class);
  }
}
