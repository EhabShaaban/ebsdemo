package com.ebs.dda.rest.accounting.paymentrequest.mocks;

import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestCompanyDataGeneralModel;

public class IObPaymentRequestCompanyDataGeneralModelMock
    extends IObPaymentRequestCompanyDataGeneralModel {

  private String documentCode;
  private String purchaseUnitCode;
  private String companyCode;

  public IObPaymentRequestCompanyDataGeneralModelMock withUserCode(String userCode){
    documentCode = userCode;
    return this;
  }

  public IObPaymentRequestCompanyDataGeneralModelMock withCompanyCode(String companyCode){
    this.companyCode = companyCode;
    return this;
  }

  public IObPaymentRequestCompanyDataGeneralModelMock withBusinessUnitCode(String businessUnitCode){
    purchaseUnitCode = businessUnitCode;
    return this;
  }

  @Override
  public String getDocumentCode() {
    return this.documentCode;
  }

  @Override
  public String getPurchaseUnitCode() {
    return purchaseUnitCode;
  }

  @Override
  public String getCompanyCode() {
    return companyCode;
  }
}
