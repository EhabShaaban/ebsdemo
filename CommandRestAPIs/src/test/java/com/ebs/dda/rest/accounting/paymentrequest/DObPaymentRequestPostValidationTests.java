package com.ebs.dda.rest.accounting.paymentrequest;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentHasNoEstimatedLandedCostException;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentRequestAmountGreaterThanBalanceException;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivatePreparationGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.OrderPaymentsGeneralModel;
import com.ebs.dda.repositories.accounting.landedcost.DObLandedCostGeneralModelRep;
import com.ebs.dda.repositories.accounting.monetarynotes.DObPaymentRequestActivatePreparationGeneralModelRep;
import com.ebs.dda.repositories.accounting.paymentrequest.OrderPaymentsGeneralModelRep;
import com.ebs.dda.rest.accounting.paymentrequest.utils.DObPaymentRequestPostValidatorUtils;
import com.ebs.dda.validation.paymentrequest.DObPaymentGeneralForPOActivationValidator;
import com.ebs.dda.validation.paymentrequest.DObPaymentRequestActivationValidator;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doReturn;

@RunWith(SpringRunner.class)
public class DObPaymentRequestPostValidationTests {

  private DObPaymentRequestPostValidatorUtils utils;
  private DObPaymentRequestActivationValidator validator;
  private DObPaymentGeneralForPOActivationValidator generalForPOActivationValidator;

  @MockBean private DObPaymentRequestActivatePreparationGeneralModelRep paymentRequestActivatePreparationGeneralModelRep;
  @MockBean private OrderPaymentsGeneralModelRep orderPaymentsGeneralModelRep;
  @MockBean private DObLandedCostGeneralModelRep landedCostGeneralModelRep;

  @Before
  public void init(){
    utils = new DObPaymentRequestPostValidatorUtils();
    validator = new DObPaymentRequestActivationValidator();
    generalForPOActivationValidator = new DObPaymentGeneralForPOActivationValidator();
  }

  @Test
  public void shouldThrowExceptionWhenPaymentAmountIsGreaterThanBankBalance() {
    utils
            .withPaymentRequestCode("202000000")
            .withDueDate(new DateTime().withYear(2021))
            .withExchangeRateCode("000026")
            .withCurrencyPrice(BigDecimal.valueOf(17.44))
            .withPaymentRequestType("OTHER_PARTY_FOR_PURCHASE")
            .withPaymentRequestCompany("0001", "0002")
        .withPaymentRequestPaymentDetails(
                "0001", BigDecimal.valueOf(200), "0001", "202011654654", null)
        .withJournalBalance(new BigDecimal("100.00"));

    injectValidatorDependencies("202000000");

    assertThrows(
        DObPaymentRequestAmountGreaterThanBalanceException.class,
        () -> validator.validateBusinessRules(utils.getValueObject()));
  }

  @Test
  public void shouldNotThrowExceptionWhenPaymentAmountIsEqualToBankBalance() {
    utils
        .withPaymentRequestCode("202000000")
            .withDueDate(new DateTime().withYear(2021))
            .withExchangeRateCode("000026")
            .withCurrencyPrice(BigDecimal.valueOf(17.44))
            .withPaymentRequestType("OTHER_PARTY_FOR_PURCHASE")
            .withPaymentRequestCompany("0001", "0002")
        .withPaymentRequestPaymentDetails(
                "0001", BigDecimal.valueOf(100), "0001", "202011654654", null)
        .withJournalBalance(new BigDecimal("100.00"));

    injectValidatorDependencies("202000000");

    assertDoesNotThrow(() -> validator.validateBusinessRules(utils.getValueObject()));
  }

  @Test
  public void shouldThrowExceptionWhenPaymentAmountIsGreaterThanTreasuryBalance() {
    utils
            .withPaymentRequestCode("202000000")
            .withDueDate(new DateTime().withYear(2021))
            .withExchangeRateCode("000026")
            .withCurrencyPrice(BigDecimal.valueOf(17.44))
            .withPaymentRequestType("BASED_ON_CREDIT_NOTE")
            .withPaymentRequestCompany("0001", "0002")
        .withPaymentRequestPaymentDetails(
                "0001", BigDecimal.valueOf(200), null, null, "0002")
        .withJournalBalance(new BigDecimal("100.00"));

    injectValidatorDependencies("202000000");

    assertThrows(
        DObPaymentRequestAmountGreaterThanBalanceException.class,
        () -> validator.validateBusinessRules(utils.getValueObject()));
  }

  @Test
  public void shouldNotThrowExceptionWhenPaymentAmountIsEqualToTreasuryBalance() {
    utils
            .withPaymentRequestCode("202000000")
            .withDueDate(new DateTime().withYear(2021))
            .withExchangeRateCode("000026")
            .withCurrencyPrice(BigDecimal.valueOf(17.44))
            .withPaymentRequestType("UNDER_SETTLEMENT")
            .withPaymentRequestCompany("0001", "0002")
        .withPaymentRequestPaymentDetails(
            "0001", BigDecimal.valueOf(100), null, null, "0002")
        .withJournalBalance(new BigDecimal("100.00"));

    injectValidatorDependencies("202000000");

    assertDoesNotThrow(() -> validator.validateBusinessRules(utils.getValueObject()));
  }


  @Test
  public void shouldThrowExceptionWhenPaymentPurchaseOrderHasNoLandedCost() {
    utils
            .withPaymentRequestCode("202100000")
            .withDueDate(new DateTime().withYear(2021))
            .withExchangeRateCode("000026")
            .withCurrencyPrice(BigDecimal.valueOf(17.44))
            .withPaymentRequestType("OTHER_PARTY_FOR_PURCHASE")
            .withPaymentRequestCompany("0002", "0002")
            .withPaymentRequestPaymentDetails(
                    "0002", BigDecimal.valueOf(100), null, null, "0002")
            .withOrder("LOCAL_PO","202100001", null)
            .withJournalBalance(new BigDecimal("10000.00"))
            .withLandedCost(null);

    injectGeneralForPOActivationValidatorDependencies("202100000", "202100001");

    assertThrows(DObPaymentHasNoEstimatedLandedCostException.class,
            () -> generalForPOActivationValidator.validateBusinessRules(utils.getValueObject()));
  }

  @Test
  public void shouldThrowExceptionWhenPaymentServiceOrderHasNoLandedCostForRelatedPurchaseOrder() {
    utils
            .withPaymentRequestCode("202100000")
            .withDueDate(new DateTime().withYear(2021))
            .withExchangeRateCode("000026")
            .withCurrencyPrice(BigDecimal.valueOf(17.44))
            .withPaymentRequestType("OTHER_PARTY_FOR_PURCHASE")
            .withPaymentRequestCompany("0002", "0002")
            .withPaymentRequestPaymentDetails(
                    "0002", BigDecimal.valueOf(100), null, null, "0002")
            .withOrder("SERVICE_PO","202100002", "202100001")
            .withJournalBalance(new BigDecimal("10000.00"))
            .withLandedCost(null);

    injectGeneralForPOActivationValidatorDependencies("202100000", "202100001");

    assertThrows(DObPaymentHasNoEstimatedLandedCostException.class,
            () -> generalForPOActivationValidator.validateBusinessRules(utils.getValueObject()));
  }

  @Test
  public void should_ThrowSecurityException_WhenJournalDateYearIsNotActiveFiscalYear(){
    utils
      .withPaymentRequestCode("202100000")
      .withDueDate(new DateTime().withYear(2022))
      .withExchangeRateCode("000026")
      .withCurrencyPrice(BigDecimal.valueOf(17.44))
      .withPaymentRequestType("OTHER_PARTY_FOR_PURCHASE")
      .withPaymentRequestCompany("0002", "0002")
      .withPaymentRequestPaymentDetails(
        "0002", BigDecimal.valueOf(100), null, null, "0002")
      .withOrder("SERVICE_PO","202100002", "202100001")
      .withJournalBalance(new BigDecimal("10000.00"))
      .withLandedCost(null);

    injectGeneralForPOActivationValidatorDependencies("202100000", "202100001");

    assertThrows(ArgumentViolationSecurityException.class,
      () -> generalForPOActivationValidator.validateBusinessRules(utils.getValueObject()));

  }

  private void injectValidatorDependencies(String paymentRequestCode) {
    DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel =
        utils.getPaymentRequestActivatePreparationGeneralModelMock();

    doReturn(paymentRequestActivatePreparationGeneralModel)
        .when(paymentRequestActivatePreparationGeneralModelRep)
        .findOneByPaymentRequestCode(paymentRequestCode);

    validator.setPaymentRequestActivatePreparationGeneralModelRep(paymentRequestActivatePreparationGeneralModelRep);
    validator.setFiscalYearRep(utils.prepareFiscalYearRep());
  }

  private void injectGeneralForPOActivationValidatorDependencies(String paymentRequestCode, String purchaseOrderCode) {
    DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel =
        utils.getPaymentRequestActivatePreparationGeneralModelMock();

    OrderPaymentsGeneralModel orderPaymentsGeneralModelMock =
        utils.getOrderPaymentsGeneralModelMock();

    DObLandedCostGeneralModel landedCostGeneralModelMock =
        utils.getLandedCostGeneralModelMock();

    doReturn(paymentRequestActivatePreparationGeneralModel)
        .when(paymentRequestActivatePreparationGeneralModelRep)
        .findOneByPaymentRequestCode(paymentRequestCode);

    doReturn(orderPaymentsGeneralModelMock)
        .when(orderPaymentsGeneralModelRep)
        .findOneByPaymentCode(paymentRequestCode);

    doReturn(landedCostGeneralModelMock)
        .when(landedCostGeneralModelRep)
        .findByPurchaseOrderCodeAndCurrentStatesLike(purchaseOrderCode, DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED);


    generalForPOActivationValidator.setPaymentRequestActivatePreparationGeneralModelRep(paymentRequestActivatePreparationGeneralModelRep);
    generalForPOActivationValidator.setOrderPaymentsGeneralModelRep(orderPaymentsGeneralModelRep);
    generalForPOActivationValidator.setLandedCostGeneralModelRep(landedCostGeneralModelRep);
    generalForPOActivationValidator.setFiscalYearRep(utils.prepareFiscalYearRep());
  }

}
