package com.ebs.dda.rest.order.salesorder;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.salesorder.utils.IObSalesOrderDataSaveValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class IObSalesOrderDataSchemaValidationTests {

  private IObSalesOrderDataSaveValidationTestsUtils utils;

  @Before
  public void init() throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();
  }

  @Test
  public void Should_PassSchemaValidation_When_AllFieldsCorrectInValueObject() {
    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  // notes
  @Test
  public void Should_PassSchemaValidation_WhenNotesFieldIsValidAsEmpty()
      throws JsonProcessingException {
    utils.withValueObjectNotes("");
    utils.convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_NotesFieldNotValidAsMoreThan250()
      throws JsonProcessingException {
    utils.withValueObjectNotes(
        "NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250NotesForSaveHpMoreThan250_");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_NotesFieldNotValidAsPattern()
      throws JsonProcessingException {
    utils.withValueObjectNotes("$");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  // paymentTermCode
  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_PaymentTermCodeFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withValueObjectPaymentTermCode("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_PaymentTermCodeFieldNotValidAsLessThan4()
          throws JsonProcessingException {
    utils.withValueObjectPaymentTermCode("001");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_PaymentTermCodeFieldNotValidAsMoreThan4()
          throws JsonProcessingException {
    utils.withValueObjectPaymentTermCode("00001");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_PaymentTermCodeFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withValueObjectPaymentTermCode("abcD");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  // currencyCode
  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyCodeFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withValueObjectCurrencyCode(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyCodeFieldNotValidAsEmpty()
      throws JsonProcessingException {
    utils.withValueObjectCurrencyCode("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyCodeFieldNotValidAsLessThan4()
          throws JsonProcessingException {
    utils.withValueObjectCurrencyCode("001");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyCodeFieldNotValidAsMoreThan4()
          throws JsonProcessingException {
    utils.withValueObjectCurrencyCode("00001");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyCodeFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withValueObjectCurrencyCode("abcD");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  // expectedDeliveryDate
  @Test
  public void
  Should_ThrowArgumentViolationSecurityException_When_ExpectedDeliveryDateFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withValueObjectExpectedDeliveryDate("Ay7aga");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
            .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
  @Test
  public void
  Should_ThrowArgumentViolationSecurityException_When_ExpectedDeliveryDateFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withValueObjectExpectedDeliveryDate("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
            .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
}
