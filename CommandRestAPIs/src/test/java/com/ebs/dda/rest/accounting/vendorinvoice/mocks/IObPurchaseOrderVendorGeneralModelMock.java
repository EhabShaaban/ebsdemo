package com.ebs.dda.rest.accounting.vendorinvoice.mocks;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObPurchaseOrderVendorGeneralModel;

public class IObPurchaseOrderVendorGeneralModelMock extends IObPurchaseOrderVendorGeneralModel {

    private String userCode;
    private String objectTypeCode;
    private String referencePOCode;

    public IObPurchaseOrderVendorGeneralModelMock withUserCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    public IObPurchaseOrderVendorGeneralModelMock withObjectTypeCode(String objectTypeCode) {
        this.objectTypeCode = objectTypeCode;
        return this;
    }

    public IObPurchaseOrderVendorGeneralModelMock withReferencePOCode(String referencePOCode) {
        this.referencePOCode = referencePOCode;
        return this;
    }

    @Override
    public String getUserCode() { return userCode; }

    @Override
    public String getObjectTypeCode() { return objectTypeCode; }

    @Override
    public String getReferencePOCode() { return referencePOCode; }
}
