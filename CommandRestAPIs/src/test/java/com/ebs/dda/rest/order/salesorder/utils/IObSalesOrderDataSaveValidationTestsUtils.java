package com.ebs.dda.rest.order.salesorder.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.masterdata.paymentterms.CObPaymentTerms;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderDataValueObject;
import com.ebs.dda.order.salesorder.IObSalesOrderDataJsonSchema;
import com.ebs.dda.order.salesorder.IObSalesOrderDataMandatoryValidator;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerAddressGeneralModelRep;
import com.ebs.dda.repositories.masterdata.customer.IObCustomerContactPersonGeneralModelRep;
import com.ebs.dda.repositories.masterdata.paymentterms.CObPaymentTermsRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.IObSalesOrderDataGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rest.order.salesorder.mock.entities.CObCurrencyMock;
import com.ebs.dda.rest.order.salesorder.mock.entities.DObSalesOrderGeneralModelMock;
import com.ebs.dda.rest.order.salesorder.mock.entities.IObCustomerAddressGeneralModelMock;
import com.ebs.dda.rest.order.salesorder.mock.entities.IObCustomerContactPersonGeneralModelMock;
import com.ebs.dda.rest.order.salesorder.mock.entities.IObSalesOrderDataGeneralModelMock;
import com.ebs.dda.validation.salesorder.IObSalesOrderDataSaveMandatoryValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashSet;
import java.util.Optional;
import org.mockito.Mockito;

public class IObSalesOrderDataSaveValidationTestsUtils {

  private final IObSalesOrderDataSaveMandatoryValidator validator;
  private final IObSalesOrderDataMandatoryValidator mandatoryInStateValidator;
  private final IObSalesOrderDataValueObject valueObject;
  private final DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private String valueObjectAsJson;
  private IObCustomerAddressGeneralModelRep customerAddressGeneralModelRep;
  private IObCustomerContactPersonGeneralModelRep customerContactPersonGeneralModelRep;
  private IObSalesOrderDataGeneralModelRep salesOrderDataGeneralModelRep;
  private CObCurrencyRep currencyRep;
  private CObPaymentTermsRep paymentTermsRep;

  private IObSalesOrderDataGeneralModelMock salesOrderDataGeneralModelMock;
  private DObSalesOrderGeneralModelMock salesOrderGeneralModelMock;
  private IObCustomerAddressGeneralModelMock customerAddressGeneralModelMock;
  private IObCustomerContactPersonGeneralModelMock customerContactPersonGeneralModelMock;
  private CObCurrencyMock currencyMock;

  public IObSalesOrderDataSaveValidationTestsUtils() {
    valueObject = new IObSalesOrderDataValueObject();
    validator = new IObSalesOrderDataSaveMandatoryValidator();
    mandatoryInStateValidator = new IObSalesOrderDataMandatoryValidator();

    customerAddressGeneralModelRep = Mockito.mock(IObCustomerAddressGeneralModelRep.class);
    customerContactPersonGeneralModelRep =
        Mockito.mock(IObCustomerContactPersonGeneralModelRep.class);
    salesOrderDataGeneralModelRep = Mockito.mock(IObSalesOrderDataGeneralModelRep.class);
    salesOrderGeneralModelRep = Mockito.mock(DObSalesOrderGeneralModelRep.class);
    currencyRep = Mockito.mock(CObCurrencyRep.class);
    paymentTermsRep = Mockito.mock(CObPaymentTermsRep.class);

    salesOrderGeneralModelMock = new DObSalesOrderGeneralModelMock();
    salesOrderDataGeneralModelMock = new IObSalesOrderDataGeneralModelMock();
    customerAddressGeneralModelMock = new IObCustomerAddressGeneralModelMock();
    customerContactPersonGeneralModelMock = new IObCustomerContactPersonGeneralModelMock();
    currencyMock = new CObCurrencyMock();
  }

  public IObSalesOrderDataSaveValidationTestsUtils withValueObjectSalesOrderCode(String salesOrderCode) {
    valueObject.setSalesOrderCode(salesOrderCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils withValueObjectPaymentTermCode(String paymentTermCode) {
    valueObject.setPaymentTermCode(paymentTermCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils withValueObjectCustomerAddressId(Long customerAddressId) {
    valueObject.setCustomerAddressId(customerAddressId);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils withValueObjectCurrencyCode(String currencyCode) {
    valueObject.setCurrencyCode(currencyCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils withValueObjectContactPersonId(Long contactPersonId) {
    valueObject.setContactPersonId(contactPersonId);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils withValueObjectExpectedDeliveryDate(
      String expectedDeliveryDate) {
    valueObject.setExpectedDeliveryDate(expectedDeliveryDate);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils withValueObjectNotes(String notes) {
    valueObject.setNotes(notes);
    return this;
  }

  private void mockPaymentTermsRep() {
    Mockito.when(paymentTermsRep.findOneByUserCode("9999")).thenReturn(null);
    Mockito.when(paymentTermsRep.findOneByUserCode("0001")).thenReturn(new CObPaymentTerms());
  }

  private void mockCurrencyRep() {
    Mockito.when(currencyRep.findOneByUserCode("9999")).thenReturn(null);
    Mockito.when(currencyRep.findOneByUserCode("0001")).thenReturn(currencyMock);
    Mockito.when(currencyRep.findOneByUserCode("0002")).thenReturn(currencyMock);
  }

  private void mockSalesOrderDataGeneralModelRep() {
    Mockito.when(salesOrderDataGeneralModelRep.findOneBySalesOrderCode("2019000010"))
        .thenReturn(salesOrderDataGeneralModelMock);
  }

  private void mockCustomerContactPersonGeneralModelRep() {
    Mockito.when(customerContactPersonGeneralModelRep.findById(new Long(5)))
        .thenReturn(java.util.Optional.of(customerContactPersonGeneralModelMock));
    Mockito.when(customerContactPersonGeneralModelRep.findById(new Long(3)))
        .thenReturn(java.util.Optional.of(customerContactPersonGeneralModelMock));
    Mockito.when(customerContactPersonGeneralModelRep.findById(new Long(99))).thenReturn(Optional.ofNullable(null));
  }

  private void mockCustomerAddressGeneralModelRep() {
    Mockito.when(customerAddressGeneralModelRep.findById(new Long(6)))
        .thenReturn(java.util.Optional.of(customerAddressGeneralModelMock));
    Mockito.when(customerAddressGeneralModelRep.findById(new Long(4)))
        .thenReturn(java.util.Optional.of(customerAddressGeneralModelMock));
    Mockito.when(customerAddressGeneralModelRep.findById(new Long(99))).thenReturn(Optional.ofNullable(null));
  }

  public IObSalesOrderDataSaveValidationTestsUtils salesOrderWithState(String state) {
    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);
    salesOrderGeneralModelMock.setCurrentStates(currentStates);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils salesOrderWithSalesOrderCode(
      String salesOrderCode) {
    salesOrderGeneralModelMock.setUserCode(salesOrderCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils salesOrderDataWithSalesOrderCode(
      String salesOrderCode) {
    salesOrderDataGeneralModelMock.setSalesOrderCode(salesOrderCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils salesOrderDataWithCustomerCode(
      String customerCode) {
    salesOrderDataGeneralModelMock.setCustomerCode(customerCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils currencyRowWithCurrencyCode(
      String currencyCode) {
    currencyMock.setUserCode(currencyCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils currencyRowWithCurrencyIso(String currencyIso) {
    currencyMock.setIso(currencyIso);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils customerAddressWithId(Long customerAddressId) {
    customerAddressGeneralModelMock.setId(customerAddressId);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils customerAddressWithCustomerCode(
      String customerCode) {
    customerAddressGeneralModelMock.setUserCode(customerCode);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils customerContactPersonWithId(
      Long customerContactPersonId) {
    customerContactPersonGeneralModelMock.setId(customerContactPersonId);
    return this;
  }

  public IObSalesOrderDataSaveValidationTestsUtils customerContactPersonWithCustomerCode(
      String customerCode) {
    customerContactPersonGeneralModelMock.setUserCode(customerCode);
    return this;
  }

  public void whenValidateSaveIObSalesOrderDataValueObject()
      throws ArgumentViolationSecurityException {

    mockPaymentTermsRep();
    mockCurrencyRep();
    mockSalesOrderDataGeneralModelRep();
    mockCustomerContactPersonGeneralModelRep();
    mockCustomerAddressGeneralModelRep();

    validator.setCustomerAddressGeneralModelRep(customerAddressGeneralModelRep);
    validator.setCustomerContactPersonGeneralModelRep(customerContactPersonGeneralModelRep);
    validator.setSalesOrderDataGeneralModelRep(salesOrderDataGeneralModelRep);
    validator.setCurrencyRep(currencyRep);
    validator.setPaymentTermsRep(paymentTermsRep);
    validator.validate(valueObject);
  }

  private void mockSalesOrderGeneralModelRep() {
    Mockito.when(salesOrderGeneralModelRep.findOneByUserCode("2019000010"))
        .thenReturn(salesOrderGeneralModelMock);
    Mockito.when(salesOrderGeneralModelRep.findOneByUserCode("2019000011"))
        .thenReturn(salesOrderGeneralModelMock);
  }

  public void whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject()
      throws Exception {
    mockSalesOrderDataGeneralModelRep();
    mockSalesOrderGeneralModelRep();
    mandatoryInStateValidator.setSalesOrderDataRep(salesOrderDataGeneralModelRep);
    mandatoryInStateValidator.setSalesOrderRep(salesOrderGeneralModelRep);
    mandatoryInStateValidator.validateValueObjectMandatories(valueObject);
  }

  public IObSalesOrderDataSaveValidationTestsUtils convertValueObjectToJson()
      throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    valueObjectAsJson = objectMapper.writeValueAsString(valueObject);
    return this;
  }

  public void applySchemaValidationOnValueObject() throws Exception {
    String createSchema = IObSalesOrderDataJsonSchema.SAVE;
    CommonControllerUtils.applySchemaValidationForString(createSchema, valueObjectAsJson);
  }
}
