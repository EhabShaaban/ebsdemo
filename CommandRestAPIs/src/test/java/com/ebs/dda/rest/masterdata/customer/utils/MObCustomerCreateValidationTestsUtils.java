package com.ebs.dda.rest.masterdata.customer.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.currency.CObCurrency;
import com.ebs.dda.jpa.masterdata.customer.MObCustomerCreateValueObject;
import com.ebs.dda.jpa.masterdata.lookups.LObIssueFrom;
import com.ebs.dda.jpa.masterdata.lookups.LObTaxAdministrative;
import com.ebs.dda.masterdata.customer.MObCustomerJsonSchema;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.country.CObCurrencyRep;
import com.ebs.dda.repositories.masterdata.lookups.LObIssueFromRep;
import com.ebs.dda.repositories.masterdata.lookups.LObTaxAdministrativeRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.masterdata.customer.MObCustomerCreateMandatoryValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.Mockito;

import java.util.List;

public class MObCustomerCreateValidationTestsUtils {

  private final MObCustomerCreateValueObject valueObject;
  private final MObCustomerCreateMandatoryValidator validator;
  private String valueObjectAsJson;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private LObIssueFromRep issueFromRep;
  private LObTaxAdministrativeRep taxAdministrativeRep;
  private CObCurrencyRep currencyRep;

  public MObCustomerCreateValidationTestsUtils() {
    valueObject = new MObCustomerCreateValueObject();
    validator = new MObCustomerCreateMandatoryValidator();
    purchasingUnitGeneralModelRep = Mockito.mock(CObPurchasingUnitGeneralModelRep.class);
    issueFromRep = Mockito.mock(LObIssueFromRep.class);
    taxAdministrativeRep = Mockito.mock(LObTaxAdministrativeRep.class);
    currencyRep = Mockito.mock(CObCurrencyRep.class);
  }

  public MObCustomerCreateValidationTestsUtils withLegalName(String legalName) {
    valueObject.setLegalName(legalName);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withMarketName(String marketName) {
    valueObject.setMarketName(marketName);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withBusinessUnitCodes(
      List<String> businessUnitCodes) {
    valueObject.setBusinessUnitCodes(businessUnitCodes);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withCreditLimit(Double creditLimit) {
    valueObject.setCreditLimit(creditLimit);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withCurrencyIso(String currencyIso) {
    valueObject.setCurrencyIso(currencyIso);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withRegistrationNumber(String registrationNumber) {
    valueObject.setRegistrationNumber(registrationNumber);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withIssuedFromCode(String issuedFromCode) {
    valueObject.setIssuedFromCode(issuedFromCode);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withTaxAdministrationCode(
      String taxAdministrationCode) {
    valueObject.setTaxAdministrationCode(taxAdministrationCode);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withTaxCardNumber(String taxCardNumber) {
    valueObject.setTaxCardNumber(taxCardNumber);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils withTaxFileNumber(String taxFileNumber) {
    valueObject.setTaxFileNumber(taxFileNumber);
    return this;
  }
  public MObCustomerCreateValidationTestsUtils withOldCustomerNumber(String oldCustomerNumber) {
    valueObject.setOldCustomerNumber(oldCustomerNumber);
    return this;
  }

  public MObCustomerCreateValidationTestsUtils convertValueObjectToJson()
      throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    valueObjectAsJson = objectMapper.writeValueAsString(valueObject);
    return this;
  }

  private void mockPurchasingUnitGeneralModelRep() {
    Mockito.when(purchasingUnitGeneralModelRep.findOneByUserCode("9999")).thenReturn(null);
    Mockito.when(purchasingUnitGeneralModelRep.findOneByUserCode("0001"))
        .thenReturn(new CObPurchasingUnitGeneralModel());
    Mockito.when(purchasingUnitGeneralModelRep.findOneByUserCode("0002"))
        .thenReturn(new CObPurchasingUnitGeneralModel());
  }

  private void mockIssuedFromRep() {
    Mockito.when(issueFromRep.findOneByUserCode("9999")).thenReturn(null);
    Mockito.when(issueFromRep.findOneByUserCode("0001")).thenReturn(new LObIssueFrom());
  }

  private void mockTaxAdministrationRep() {
    Mockito.when(taxAdministrativeRep.findOneByUserCode("9999")).thenReturn(null);
    Mockito.when(taxAdministrativeRep.findOneByUserCode("0001")).thenReturn(new LObTaxAdministrative());
  }

  private void mockCurrencyRep() {
    Mockito.when(currencyRep.findOneByIso("XXX")).thenReturn(null);
    Mockito.when(currencyRep.findOneByIso("EGP")).thenReturn(new CObCurrency());
  }

  public void applySchemaValidationOnValueObject() throws Exception {
    String createSchema = MObCustomerJsonSchema.CREATE;
    CommonControllerUtils.applySchemaValidationForString(createSchema, valueObjectAsJson);
  }

  public void whenValidateCreateCustomerValueObject() throws ArgumentViolationSecurityException {

    mockPurchasingUnitGeneralModelRep();
    mockIssuedFromRep();
    mockTaxAdministrationRep();
    mockCurrencyRep();

    validator.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    validator.setIssueFromRep(issueFromRep);
    validator.setTaxAdministrativeRep(taxAdministrativeRep);
    validator.setCurrencyRep(currencyRep);

    validator.validate(valueObject);
  }
}
