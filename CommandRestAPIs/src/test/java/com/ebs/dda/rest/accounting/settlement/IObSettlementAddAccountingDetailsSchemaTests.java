package com.ebs.dda.rest.accounting.settlement;

import com.ebs.dda.rest.accounting.settlement.utils.IObSettlementAddAccountingDetailsSchemaTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@RunWith(JUnitPlatform.class)
public class IObSettlementAddAccountingDetailsSchemaTests {

  private IObSettlementAddAccountingDetailsSchemaTestUtils utils;

  @BeforeEach
  public void init() {
    utils = new IObSettlementAddAccountingDetailsSchemaTestUtils();
  }


  private Map<String, String> invalidCodesForGLAccountCodeMaps =
      new HashMap<String, String>() {
        {
          put("Code exceeds max number of digits", "000002");
          put("Code has alphabets", "test");
          put("Code has non ASCII letters", "0$002");
        }
      };

  @TestFactory
  public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidGLCode() {
    return invalidCodesForGLAccountCodeMaps.keySet().stream()
        .map(
            key ->
                DynamicTest.dynamicTest(
                    key,
                    () -> {
                      String invalidCode = invalidCodesForGLAccountCodeMaps.get(key);
                      utils
                          .withCode("2021000001")
                          .withGLAccountCode(invalidCode)
                          .withGLSubAccount("000002")
                          .withAccountType("DEBIT")
                          .withAmount(2000D)
                          .assertThrowsArgumentSecurityException();
                    }));
  }

  @Test
  public void Should_ThrowsArgumentSecurityException_When_InvalidAccountType() {
    utils
        .withCode("2021000001")
        .withGLAccountCode("10221")
        .withGLSubAccount("000002")
        .withAccountType("ay7aga")
        .withAmount(2000D)
        .assertThrowsArgumentSecurityException();
  }

  @Test
  public void Should_ThrowsArgumentSecurityException_When_InvalidAmount() {
    utils
        .withCode("2021000001")
        .withGLAccountCode("10221")
        .withGLSubAccount("000002")
        .withAccountType("DEBIT")
        .withAmount(null)
        .assertThrowsArgumentSecurityException();
  }
}
