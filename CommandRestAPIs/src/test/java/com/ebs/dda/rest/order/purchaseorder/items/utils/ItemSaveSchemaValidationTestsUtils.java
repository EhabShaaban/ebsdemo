package com.ebs.dda.rest.order.purchaseorder.items.utils;

import com.ebs.dda.purchases.apis.IDObPurchaseOrderJsonSchema;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IPurchaseOrderItemValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;
import java.math.BigDecimal;

public class ItemSaveSchemaValidationTestsUtils {

  private JsonObject valueObjectAsJson;

  public ItemSaveSchemaValidationTestsUtils() {
    valueObjectAsJson = new JsonObject();
  }

  public void applySchemaValidationOnItemSaveValueObject() throws Exception {
    String saveItemQuantityInDnPlSchema = IDObPurchaseOrderJsonSchema.SAVE_ITEM_QUANTITY_IN_DN_PL;
    CommonControllerUtils.applySchemaValidationForString(
        saveItemQuantityInDnPlSchema, valueObjectAsJson.toString());
  }

  public ItemSaveSchemaValidationTestsUtils withQuantityInDnPl(String quantity) {
    valueObjectAsJson.addProperty(
        IPurchaseOrderItemValueObject.QUANTITY_IN_DN, new BigDecimal(quantity));
    return this;
  }

  public ItemSaveSchemaValidationTestsUtils withQuantityInDnPlAsString(String quantity) {
    valueObjectAsJson.addProperty(IPurchaseOrderItemValueObject.QUANTITY_IN_DN, quantity);
    return this;
  }
}
