package com.ebs.dda.rest.accounting.vendorinvoice.utils;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.i18n.locales.ISysDefaultLocales;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceActivateValueObject;
import com.ebs.dda.rest.accounting.paymentrequest.mocks.CObFiscalYearMock;
import com.ebs.dda.rest.accounting.vendorinvoice.mocks.*;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;


public class DObVendorInvoiceActivateServiceInvoiceValidatorUtils {
    private DObVendorInvoiceActivateValueObject valueObject;
    private IObVendorInvoiceDetailsGeneralModelMock vendorInvoiceDetailsGeneralModelMock;
    private IObItemAccountingInfoGeneralModelMock itemAccountingInfoGeneralModelMock;
    private IObPurchaseOrderVendorGeneralModelMock orderVendorGeneralModelMock;
    private IObVendorAccountingInfoGeneralModelMock accountingInfoGeneralModelMock;
    private IObVendorInvoiceItemsGeneralModelMock invoiceItemGeneralModelMock;
    private DObLandedCostGeneralModelMock landedCostGeneralModelMock;
    private List<CObFiscalYearMock> fiscalYearMockList;

    public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withVendorInvoiceCode(String userCode, String poCode,
                                                                                      String vendorCode) {
        this.vendorInvoiceDetailsGeneralModelMock = new IObVendorInvoiceDetailsGeneralModelMock()
                .withUserCode(userCode)
                .withPurchaseOrderCode(poCode)
                .withVendorCode(vendorCode);
        this.valueObject = new DObVendorInvoiceActivateValueObject();
        this.valueObject.setVendorInvoiceCode(userCode);
        return this;
    }

    public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withDueDate(DateTime date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
        this.valueObject.setJournalDate(date.toString(formatter));
        return this;
    }

    public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withItemAccountingInfo(String itemCode) {
        itemAccountingInfoGeneralModelMock = new IObItemAccountingInfoGeneralModelMock().withItemCode(itemCode);
        return this;
    }

    public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withOrder(String userCode, String objectTypeCode, String referencePOCode) {
        orderVendorGeneralModelMock = new IObPurchaseOrderVendorGeneralModelMock()
                .withUserCode(userCode)
                .withObjectTypeCode(objectTypeCode)
                .withReferencePOCode(referencePOCode);
        return this;
    }

    public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withAccountingInfo(String vendorCode) {
        accountingInfoGeneralModelMock = new IObVendorAccountingInfoGeneralModelMock().withVendorCode(vendorCode);
        return this;
    }

    public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withInvoiceItems(String itemCode) {
        invoiceItemGeneralModelMock = new IObVendorInvoiceItemsGeneralModelMock().withItemCode(itemCode);
        return this;
    }
public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withActiveFiscalYears(List<String> activeFiscalYears){
        fiscalYearMockList = new ArrayList<>();
    for (String activeFiscalYear : activeFiscalYears){
        CObFiscalYearMock fiscalYear = new CObFiscalYearMock();
        LocalizedString localizedString = new LocalizedString();
        localizedString.setValue(ISysDefaultLocales.ENGLISH_LOCALE, activeFiscalYear);
        localizedString.setValue(ISysDefaultLocales.ARABIC_LOCALE, activeFiscalYear);
        fiscalYear.setName(localizedString);
        fiscalYearMockList.add(fiscalYear);
    }
    return this;
}
    public DObVendorInvoiceActivateServiceInvoiceValidatorUtils withLandedCost(String landedCostCode) {
        landedCostGeneralModelMock = new DObLandedCostGeneralModelMock().withUserCode(landedCostCode);
        return this;
    }

    public IObVendorInvoiceDetailsGeneralModelMock getVendorInvoiceDetailsGeneralModelMock() {
        return vendorInvoiceDetailsGeneralModelMock;
    }

    public DObLandedCostGeneralModelMock getLandedCostGeneralModelMock() {
        return landedCostGeneralModelMock;
    }

    public IObItemAccountingInfoGeneralModelMock getItemAccountingInfoGeneralModelMock() {
        return itemAccountingInfoGeneralModelMock;
    }

    public IObPurchaseOrderVendorGeneralModelMock getOrderVendorGeneralModelMock() {
        return orderVendorGeneralModelMock;
    }

    public IObVendorAccountingInfoGeneralModelMock getAccountingInfoGeneralModelMock() {
        return accountingInfoGeneralModelMock;
    }

    public IObVendorInvoiceItemsGeneralModelMock getInvoiceItemGeneralModelMock() {
        return invoiceItemGeneralModelMock;
    }

    public DObVendorInvoiceActivateValueObject getValueObject() { return valueObject; }

    public List<CObFiscalYearMock> getFiscalYearMockList() {
        return fiscalYearMockList;
    }
}
