package com.ebs.dda.rest.accounting.settlement;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import com.ebs.dda.rest.accounting.settlement.utils.DObSettlementSchemaTestUtils;

@RunWith(JUnitPlatform.class)
public class DObSettlementCreationSchemaTests {

	private DObSettlementSchemaTestUtils utils;

	@BeforeEach
	public void init() {
		utils = new DObSettlementSchemaTestUtils();
	}

	private Map<String, String> invalidCodesMaps = new HashMap<String, String>() {
		{
			put("Code exceeds max number of digits", "00002");
			put("Code has alphabets", "test");
			put("Code has non ASCII letters", "0$02");
		}
	};

	@TestFactory
	public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidBusinessUnitCode() {
		return invalidCodesMaps.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String invalidCode = invalidCodesMaps.get(key);
			utils.withType("SETTLEMENTS").withBusinessUnitCode(invalidCode).withCompanyCode("0002")
							.withCurrencyCode("0001").withDocumentOwnerId(34L)
							.assertThrowsArgumentSecurityException();
		}));

	}

	@TestFactory
	public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidCompanyCode() {
		return invalidCodesMaps.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String invalidCode = invalidCodesMaps.get(key);
			utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode(invalidCode)
							.withCurrencyCode("0001").withDocumentOwnerId(34L)
							.assertThrowsArgumentSecurityException();
		}));

	}

	@TestFactory
	public Stream<DynamicTest> Should_ThrowsArgumentSecurityException_When_InvalidCurrencyCode() {
		return invalidCodesMaps.keySet().stream().map(key -> DynamicTest.dynamicTest(key, () -> {
			String invalidCode = invalidCodesMaps.get(key);
			utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode("0002")
							.withCurrencyCode(invalidCode).withDocumentOwnerId(34L)
							.assertThrowsArgumentSecurityException();
		}));

	}

	@Test
	public void Should_ThrowsArgumentSecurityException_When_InvalidType() {
		utils.withType("anyType").withBusinessUnitCode("0002").withCompanyCode("0002")
						.withCurrencyCode("0002").withDocumentOwnerId(34L)
						.assertThrowsArgumentSecurityException();
	}

	@Test
	public void Should_ThrowsArgumentSecurityException_When_InvalidDocumentOwner() {
		utils.withType("SETTLEMENTS").withBusinessUnitCode("0002").withCompanyCode("0002")
						.withCurrencyCode("0002").withStringDocumentOwnerId("anyString")
						.assertThrowsArgumentSecurityException();
	}

}
