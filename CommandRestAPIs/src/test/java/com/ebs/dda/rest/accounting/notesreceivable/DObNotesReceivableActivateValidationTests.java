package com.ebs.dda.rest.accounting.notesreceivable;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.accounting.notesreceivable.utils.DObNotesReceivableActivateValidatorUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@RunWith(SpringRunner.class)
public class DObNotesReceivableActivateValidationTests {

  private DObNotesReceivableActivateValidatorUtils utils;

  @Before
  public void init() {
    utils = new DObNotesReceivableActivateValidatorUtils();
  }

  @Test
  public void shouldThrowExceptionWhenAmountToCollectNotEqualsNRAmount() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(12000), new BigDecimal(12000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertErrorDObNotesReceivableAmountToCollectNotEqualsNRAmountIsReturned();
  }

  @Test
  public void shouldNotThrowExceptionWhenAmountToCollectEqualsNRAmount() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertErrorDObNotesReceivableAmountToCollectNotEqualsNRAmountIsNotReturned();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentAmountToCollectIsGreaterThanItsRemaining()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withReferenceDocumentData(
            "2021000005", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(26000), new BigDecimal(26000))
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("12000.0"), "Approved", "000001", "0002", "0001")
        .withSalesInvoiceDataReferenceDocument(
            "2021000005", "12000", "Active", "000001", "0002", "0001")
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsReturned();
  }

  @Test
  public void
      shouldThrowExceptionWhenAmountToCollectNotEqualsNRAmountAndAmountToCollectGreaterThanItsRemaining()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withReferenceDocumentData(
            "2021000005", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(12000), new BigDecimal(12000))
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("11000.0"), "Approved", "000001", "0002", "0001")
        .withSalesInvoiceDataReferenceDocument(
            "2021000005", "12000", "Active", "000001", "0002", "0001");
    utils
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsReturned();
    utils.assertErrorDObNotesReceivableAmountToCollectNotEqualsNRAmountIsReturned();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentAmountToCollectIsLessThanItsRemaining()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesOrderDataReferenceDocument(
            "2021000003",
            new BigDecimal("15000.0"),
            "GoodsIssueActivated",
            "000001",
            "0002",
            "0001")
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsNotReturned();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentAmountToCollectIsEqualsItsRemaining()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsNotReturned();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentNRNotInActiveState() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Draft", "000001", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentSONotInGIActivatedOrApprovedState()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withReferenceDocumentData("2021000005", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesOrderDataReferenceDocument(
            "2021000003",
            new BigDecimal("15000.0"),
            "SalesInvoiceActivated",
            "000001",
            "0002",
            "0001")
        .withSalesOrderDataReferenceDocument(
            "2021000005",
            new BigDecimal("15000.0"),
            "SalesInvoiceActivated",
            "000001",
            "0002",
            "0001")
        .assertObjectStateNotValidException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentSOINDraftState() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("15000.0"), "Draft", "000001", "0002", "0001")
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentSINotInActiveState() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Draft", "000001", "0002", "0001")
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentInValidState() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentHasNotTheSameBusinessPartnerOfNR()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000002", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentHasTheSameBusinessPartnerOfNR()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentHasNotTheSameBusinessUnitOfNR()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0001", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentHasTheSameBusinessUnitOfNR()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentHasNotTheSameCompanyOfNR()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0002", "0002")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentHasTheSameCompanyOfNR()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentIsNotUnique() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000004", "2021000001", new BigDecimal(10000), "NOTES_RECEIVABLE")
        .withReferenceDocumentData(
            "2021000004", "2021000001", new BigDecimal(3000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(4L, "2021000004", "Active", "000001", "0002", "0001")
        .withNotesDetails(4L, "2021000004", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(4L, "2021000004", "Active", "000001", "0002", "0001")
        .withNotesDetails(4L, "2021000004", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentIsUnique() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(10000), "SALES_INVOICE")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(3000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("15000.0"), "Approved", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenJournalEntryDateNotInActiveFiscalYear()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2018 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(10000), "SALES_INVOICE")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(3000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("15000.0"), "Approved", "000001", "0002", "0001")
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenWhenJournalEntryDateInActiveFiscalYear()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2022 02:00 AM")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(10000), "SALES_INVOICE")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(3000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("15000.0"), "Approved", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenNoteNumberIsNull() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000004", "2021000001", new BigDecimal(10000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesFullDetails(
            1L,
            "2021000001",
            new BigDecimal(13000),
            new BigDecimal(13000),
            null,
            "QNB Bank",
            new DateTime(),
            "0001",
            "CHEQUE",
            "EGP",
            "000001")
        .assertMissingField("noteNumber");
  }

  @Test
  public void shouldThrowExceptionWhenNoteBankIsNull() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000004", "2021000001", new BigDecimal(10000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesFullDetails(
            1L,
            "2021000001",
            new BigDecimal(13000),
            new BigDecimal(13000),
            "263562356",
            null,
            new DateTime(),
            "0001",
            "CHEQUE",
            "EGP",
            "000001")
        .assertMissingField("noteBank");
  }

  @Test
  public void shouldThrowExceptionWhenDueDateIsNull() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000004", "2021000001", new BigDecimal(10000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesFullDetails(
            1L,
            "2021000001",
            new BigDecimal(13000),
            new BigDecimal(13000),
            "263562356",
            "QNB Bank",
            null,
            "0001",
            "CHEQUE",
            "EGP",
            "000001")
        .assertMissingField("dueDate");
  }

  @Test
  public void shouldThrowExceptionWhenDepotIsNull() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withReferenceDocumentData(
            "2021000004", "2021000001", new BigDecimal(10000), "NOTES_RECEIVABLE")
        .withReturnedReferenceDocumentData()
        .withNotesFullDetails(
            1L,
            "2021000001",
            new BigDecimal(13000),
            new BigDecimal(13000),
            "263562356",
            "QNB Bank",
            new DateTime(),
            null,
            "CHEQUE",
            "EGP",
            "000001")
        .assertMissingField("depotCode");
  }

  @Test
  public void shouldThrowExceptionWhenThereIsNoReferenceDocument() throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withJournalEntryDate("01-Feb-2021 02:00 AM")
        .withNotesFullDetails(
            1L,
            "2021000001",
            new BigDecimal(13000),
            new BigDecimal(13000),
            "263562356",
            "QNB Bank",
            new DateTime(),
            "0001",
            "CHEQUE",
            "EGP",
            "000001")
        .assertMissingField("ReferenceDocuments");
  }
// schema Test
  @Test
  public void Should_PassSchemaValidation_When_AllFieldsCorrectInValueObject() throws ParseException, JsonProcessingException {
    utils.withJournalEntryDate("01-Feb-2021 02:00 AM");

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_JournalEntryDateFieldNotValidAsEmpty()
          throws JsonProcessingException, ParseException {
    utils.withJournalEntryDate("");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_JournalEntryDateFieldNotValidAsPattern()
          throws JsonProcessingException, ParseException {
    utils.withJournalEntryDate("Ay 7aga");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_JournalEntryDateFieldNotValidAsSpaces()
          throws JsonProcessingException, ParseException {
    utils.withJournalEntryDate("    ");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_JournalEntryDateFieldNotValidAsNull()
          throws JsonProcessingException, ParseException {
    utils.withJournalEntryDate(null);

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_JournalEntryDateFieldNotSend(){
    utils.withoutJournalEntryDate();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
}
