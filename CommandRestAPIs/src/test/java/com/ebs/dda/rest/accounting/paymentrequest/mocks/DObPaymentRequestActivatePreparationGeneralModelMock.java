package com.ebs.dda.rest.accounting.paymentrequest.mocks;

import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivatePreparationGeneralModel;

import java.math.BigDecimal;

public class DObPaymentRequestActivatePreparationGeneralModelMock extends DObPaymentRequestActivatePreparationGeneralModel {

    private String paymentRequestCode;
    private String businessUnitCode;
    private String companyCode;
    private String currencyCode;
    private String bankCode;
    private String bankAccountNo;
    private String treasuryCode;
    private BigDecimal paymentNetAmount;
    private BigDecimal balance;
    private String paymentType;
    private BigDecimal currencyPrice;
    private String exchangeRate;

    public DObPaymentRequestActivatePreparationGeneralModelMock withUserCode(String userCode) {
        this.paymentRequestCode = userCode;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withCompanyCode(String companyCode) {
        this.companyCode = companyCode;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withBusinessUnitCode(String businessUnitCode) {
        this.businessUnitCode = businessUnitCode;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withNetAmount(BigDecimal netAmount) {
        this.paymentNetAmount = netAmount;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withBankAccountNumber(
            String bankAccountNumber) {
        this.bankAccountNo = bankAccountNumber;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withTreasuryCode(String treasuryCode) {
        this.treasuryCode = treasuryCode;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withBankCode(String bankCode) {
        this.bankCode = bankCode;
        return this;
    }
    public DObPaymentRequestActivatePreparationGeneralModelMock withBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withCurrencyPrice(BigDecimal currencyPrice) {
        this.currencyPrice = currencyPrice;
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    @Override
    public String getPaymentRequestCode() {
        return paymentRequestCode;
    }

    @Override
    public String getBusinessUnitCode() {
        return businessUnitCode;
    }

    @Override
    public String getCompanyCode() {
        return companyCode;
    }

    @Override
    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public String getBankCode() {
        return bankCode;
    }

    @Override
    public String getBankAccountNo() {
        return bankAccountNo;
    }

    @Override
    public String getTreasuryCode() {
        return treasuryCode;
    }

    @Override
    public BigDecimal getPaymentNetAmount() {
        return paymentNetAmount;
    }

    @Override
    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public String getPaymentType() {
        return paymentType;
    }

    @Override
    public BigDecimal getCurrencyPrice() {
        return currencyPrice;
    }

    @Override
    public String getExchangeRate() {
        return exchangeRate;
    }
}
