package com.ebs.dda.rest.order.purchaseorder;

import com.ebs.dda.rest.order.purchaseorder.utils.DObServicePurchaseOrderConfirmValidationTestsUtils;
import org.junit.Before;
import org.junit.Test;

public class DObServicePurchaseOrderConfirmBusinessRulesValidationTests {

  private DObServicePurchaseOrderConfirmValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new DObServicePurchaseOrderConfirmValidationTestsUtils();
  }

  @Test
  public void Should_Pass_When_RelatedPOIsNotCancelled() throws Exception {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withRelatedPO("2020000004", "IMPORT_PO", "Cleared")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM")
        .assertThatErrorCode_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH_IsNotReturned();
  }

  @Test
  public void Should_ReturnError_When_RelatedPOIsCancelled()
      throws Exception {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withRelatedPO("2020000004", "IMPORT_PO", "Cancelled")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM")
        .assertThatErrorCode_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_NO_REFRESH_IsReturned();
  }
}
