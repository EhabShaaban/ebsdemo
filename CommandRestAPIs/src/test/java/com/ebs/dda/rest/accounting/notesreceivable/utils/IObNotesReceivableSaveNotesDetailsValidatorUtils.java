package com.ebs.dda.rest.accounting.notesreceivable.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.accounting.monetarynotes.IObMonetaryNotesDetailsValueObject;
import com.ebs.dda.jpa.masterdata.lookups.LObDepot;
import com.ebs.dda.repositories.masterdata.lookups.LObDepotRep;
import com.ebs.dda.validation.notesreceivable.IObNotesReceivableDetailsValidator;
import org.joda.time.DateTime;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class IObNotesReceivableSaveNotesDetailsValidatorUtils {

  private IObMonetaryNotesDetailsValueObject valueObject;
  private IObNotesReceivableDetailsValidator notesReceivableDetailsValidator;
  private LObDepotRep depotRep;

  public IObNotesReceivableSaveNotesDetailsValidatorUtils() {
    valueObject = new IObMonetaryNotesDetailsValueObject();
    notesReceivableDetailsValidator = new IObNotesReceivableDetailsValidator();

    depotRep = Mockito.mock(LObDepotRep.class);
  }

  public IObNotesReceivableSaveNotesDetailsValidatorUtils withNotesDetails(
      String notesReceivableCode,
      String noteNumber,
      String noteBank,
      DateTime dueDate,
      String depotCode) {

    valueObject.setNotesReceivableCode(notesReceivableCode);
    valueObject.setDepotCode(depotCode);
    valueObject.setDueDate(dueDate.toString());
    valueObject.setNoteBank(noteBank);
    valueObject.setNoteNumber(noteNumber);

    return this;
  }

  public IObNotesReceivableSaveNotesDetailsValidatorUtils whenDepotDoesNotExist() {
    Mockito.when(depotRep.findOneByUserCode(valueObject.getDepotCode())).thenReturn(null);
    notesReceivableDetailsValidator.setDepotRep(depotRep);
    return this;
  }

  public IObNotesReceivableSaveNotesDetailsValidatorUtils whenDepotExist() {
    mockDepot();
    return this;
  }

  public IObMonetaryNotesDetailsValueObject getValueObject() {
    return valueObject;
  }

  public void assertDoesNotThrowException() {
    assertDoesNotThrow(() -> notesReceivableDetailsValidator.validate(getValueObject()));
  }

  public void assertThrowsArgumentViolationSecurityException() {
    assertThrows(
        ArgumentViolationSecurityException.class,
        () -> notesReceivableDetailsValidator.validate(getValueObject()));
  }

  private void mockDepot() {
    Mockito.when(depotRep.findOneByUserCode(valueObject.getDepotCode())).thenReturn(new LObDepot());
    notesReceivableDetailsValidator.setDepotRep(depotRep);
  }
}
