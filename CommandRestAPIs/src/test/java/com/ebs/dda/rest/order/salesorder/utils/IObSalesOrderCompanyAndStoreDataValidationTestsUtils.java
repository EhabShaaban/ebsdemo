package com.ebs.dda.rest.order.salesorder.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.order.salesorder.IObSalesOrderCompanyAndStoreValueObject;
import com.ebs.dda.order.salesorder.IObSalesOrderCompanyAndStoreDataJsonSchema;
import com.ebs.dda.repositories.masterdata.storehouse.CObStorehouseGeneralModelRep;
import com.ebs.dda.repositories.order.salesorder.DObSalesOrderGeneralModelRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.rest.order.salesorder.mock.entities.DObSalesOrderGeneralModelMock;
import com.ebs.dda.validation.salesorder.IObSalesOrderCompanyAndStoreDataMandatoryValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashSet;
import org.mockito.Mockito;

public class IObSalesOrderCompanyAndStoreDataValidationTestsUtils {

  private final IObSalesOrderCompanyAndStoreValueObject valueObject;
  private final IObSalesOrderCompanyAndStoreDataMandatoryValidator validator;
  private final DObSalesOrderGeneralModelRep salesOrderGeneralModelRep;
  private final CObStorehouseGeneralModelRep storehouseGeneralModelRep;
  private String valueObjectAsJson;
  private DObSalesOrderGeneralModelMock salesOrderGeneralModelMock;

  public IObSalesOrderCompanyAndStoreDataValidationTestsUtils() {
    valueObject = new IObSalesOrderCompanyAndStoreValueObject();
    validator = new IObSalesOrderCompanyAndStoreDataMandatoryValidator();

    salesOrderGeneralModelMock = new DObSalesOrderGeneralModelMock();
    salesOrderGeneralModelRep = Mockito.mock(DObSalesOrderGeneralModelRep.class);
    storehouseGeneralModelRep = Mockito.mock(CObStorehouseGeneralModelRep.class);
  }

  public void applySchemaValidationOnValueObject() throws Exception {
    String schema = IObSalesOrderCompanyAndStoreDataJsonSchema.SAVE;
    CommonControllerUtils.applySchemaValidationForString(schema, valueObjectAsJson);
  }

  public IObSalesOrderCompanyAndStoreDataValidationTestsUtils convertValueObjectToJson()
      throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    valueObjectAsJson = objectMapper.writeValueAsString(valueObject);
    return this;
  }

  public IObSalesOrderCompanyAndStoreDataValidationTestsUtils withStoreCode(String storeCode) {
    valueObject.setStoreCode(storeCode);
    return this;
  }

  public IObSalesOrderCompanyAndStoreDataValidationTestsUtils withSalesOrderCode(String userCode) {
    salesOrderGeneralModelMock.setUserCode(userCode);
    valueObject.setSalesOrderCode(userCode);
    return this;
  }

  public IObSalesOrderCompanyAndStoreDataValidationTestsUtils withSalesOrderInState(String state) {
    HashSet<String> currentStates = new HashSet<>();
    currentStates.add(state);
    salesOrderGeneralModelMock.setCurrentStates(currentStates);
    return this;
  }

  public void whenValidateSaveCompanyAndStoreValueObject()
      throws ArgumentViolationSecurityException {

    mockSalesOrderGeneralModelRep();
    mockCObStorehouseGeneralModelRep();
    validator.setSalesOrderGeneralModelRep(salesOrderGeneralModelRep);
    validator.setStorehouseGeneralModelRep(storehouseGeneralModelRep);

    validator.validate(valueObject);
  }

  private void mockSalesOrderGeneralModelRep() {
    Mockito.when(salesOrderGeneralModelRep.findOneByUserCode("2020000005"))
        .thenReturn(salesOrderGeneralModelMock);
  }

  private void mockCObStorehouseGeneralModelRep() {
    Mockito.when(storehouseGeneralModelRep.findOneByUserCode("9999")).thenReturn(null);
  }
}
