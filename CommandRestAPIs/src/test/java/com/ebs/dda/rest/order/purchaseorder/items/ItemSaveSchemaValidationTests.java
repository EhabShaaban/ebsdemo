package com.ebs.dda.rest.order.purchaseorder.items;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.purchaseorder.items.utils.ItemSaveSchemaValidationTestsUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class ItemSaveSchemaValidationTests {

  private ItemSaveSchemaValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new ItemSaveSchemaValidationTestsUtils();
  }

  //  QtyInDn : number , null
  //  QtyInDn : minimum 0
  //  QtyInDn : maximum 1000000000
  //  QtyInDn : multiple of 0.000001

  @Test
  public void Should_PassSchemaValidation_When_ValueMatchesWithSchema() {

    utils.withQuantityInDnPl("152.014262");

    assertDoesNotThrow(() -> utils.applySchemaValidationOnItemSaveValueObject());
  }

  @Test
  public void Should_PassSchemaValidation_When_QtyInDnValueIsNull() {

    utils.withQuantityInDnPlAsString(null);

    assertDoesNotThrow(() -> utils.applySchemaValidationOnItemSaveValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_QtyInDnValueNotValidAsNumber() {
    utils.withQuantityInDnPlAsString(" ");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnItemSaveValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_QtyInDnValueNotValidAsMinimum() {
    utils.withQuantityInDnPl("-152.014262");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnItemSaveValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_QtyInDnValueNotValidAsMaximum() {
    utils.withQuantityInDnPl("1000000001.0");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnItemSaveValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_QtyInDnValueNotValidAsMultipleOf() {
    utils.withQuantityInDnPl("152.0142621");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnItemSaveValueObject());
  }
}
