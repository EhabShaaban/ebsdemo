package com.ebs.dda.rest.accounting.vendorinvoice.mocks;

import com.ebs.dda.jpa.masterdata.item.IObItemAccountingInfoGeneralModel;

public class IObItemAccountingInfoGeneralModelMock extends IObItemAccountingInfoGeneralModel {

    private String itemCode;

    public IObItemAccountingInfoGeneralModelMock withItemCode(String itemCode) {
        this.itemCode = itemCode;
        return this;
    }

    @Override
    public String getItemCode() { return itemCode; }
}
