package com.ebs.dda.rest.order.salesorder;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.salesorder.utils.IObSalesOrderDataSaveValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class IObSalesOrderDataMandatoryInStateValidatorTests {

  private IObSalesOrderDataSaveValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
  }

  // draft
  @Test
  public void Should_Pass_When_NotesFieldNotExistsInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");

    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_NotesFieldIsNullInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");

    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_CurrencyFieldNotExistsInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_CurrencyFieldIsNullInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectCurrencyCode(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_ExpectedDeliveryDateFieldNotExistsInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_ExpectedDeliveryDateFieldIsNullInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectExpectedDeliveryDate(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_ContactPersonIdFieldNotExistsInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_ContactPersonIdFieldIsNullInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectContactPersonId(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_CustomerAddressIdFieldNotExistsInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_CustomerAddressIdFieldIsNullInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectCustomerAddressId(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_PaymentTermFieldNotExistsInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_PaymentTermFieldIsNullInValueObjectInDraftState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectPaymentTermCode(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000010").salesOrderWithState("Draft");
    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }
  // Rejected
  @Test
  public void Should_Pass_When_NotesFieldNotExistsInValueObjectInRejectedState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");

    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_Pass_When_NotesFieldIsNullInValueObjectInRejectedState()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");

    assertDoesNotThrow(
        () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldNotExistsInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldIsNullInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectCurrencyCode(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ExpectedDeliveryDateFieldNotExistsInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ExpectedDeliveryDateFieldIsNullInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectExpectedDeliveryDate(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ContactPersonIdFieldNotExistsInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ContactPersonIdFieldIsNullInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectContactPersonId(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CustomerAddressIdFieldNotExistsInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CustomerAddressIdFieldIsNullInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectCustomerAddressId(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_PaymentTermFieldNotExistsInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_PaymentTermFieldIsNullInValueObjectInRejectedState()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000011");
    utils.withValueObjectCustomerAddressId(new Long(6));
    utils.withValueObjectContactPersonId(new Long(5));
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectPaymentTermCode(null);
    utils.convertValueObjectToJson();

    utils.salesOrderWithSalesOrderCode("2019000011").salesOrderWithState("Rejected");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(
            () -> utils.whenValidateMandatoryFieldsInStateInSaveIObSalesOrderDataValueObject());
  }
}
