package com.ebs.dda.rest.accounting.costing;

import com.ebs.dda.rest.accounting.costing.utils.DObCostingSchemaTestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@RunWith(JUnitPlatform.class)
public class DObCostingActivateSchemaTests {

  private DObCostingSchemaTestUtils utils;

  @Before
  public void init() {
    utils = new DObCostingSchemaTestUtils();
  }

  @Test
  public void Should_PassSchemaValidation_When_ValueMatchesWithSchema() {
    utils.withDueDate("06-Sep-2020 09:30 AM");
    assertDoesNotThrow(() -> utils.applySchemaValidationOnCostingActivationValueObject());
  }

  @Test
  public void Should_PassSchemaValidation_When_DueDateValueIsNull() {
    utils.withDueDate(null).assertThrowsArgumentSecurityException();
  }

  @Test
  public void Should_PassSchemaValidation_When_DueDateValueIsInValid() {
    utils.withDueDate("ay 7aga").assertThrowsArgumentSecurityException();
  }
}
