package com.ebs.dda.rest.order.salesorder;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.salesorder.utils.IObSalesOrderCompanyAndStoreDataValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class IObSalesOrderCompanyAndStoreDataSchemaValidationTests {

  private IObSalesOrderCompanyAndStoreDataValidationTestsUtils utils;

  @Before
  public void init() throws JsonProcessingException {
    utils = new IObSalesOrderCompanyAndStoreDataValidationTestsUtils();
  }

  @Test
  public void Should_PassSchemaValidation_When_AllFieldsCorrectInValueObject()
      throws JsonProcessingException {
    utils.withStoreCode("0001").convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  //    Storehouse Tests

  @Test
  public void Should_PassSchemaValidation_When_StoreCodeFieldIsNullAsOptionalField()
      throws JsonProcessingException {
    utils.withStoreCode(null);
    utils.convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_StoreCodeFieldNotValidAsEmpty()
      throws JsonProcessingException {
    utils.withStoreCode("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_StoreCodeFieldNotValidAsLessThan4()
          throws JsonProcessingException {
    utils.withStoreCode("12");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_StoreCodeFieldNotValidAsMoreThan4()
          throws JsonProcessingException {
    utils.withStoreCode("12345");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_StoreCodeFieldNotValidAsPattern()
      throws JsonProcessingException {
    utils.withStoreCode("abcD");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_StoreCodeFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withStoreCode("    ");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
}
