package com.ebs.dda.rest.accounting.collection.utils;

import com.ebs.dda.accounting.collection.apis.IIObCollectionDetailsJsonSchema;
import com.ebs.dda.jpa.accounting.collection.IIObCollectionDetailsGeneralModel;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class DObCollectionSchemaTestUtils {

    private final JsonObject requestBody;

    public DObCollectionSchemaTestUtils() {
        this.requestBody = new JsonObject();
    }

    public DObCollectionSchemaTestUtils withAmount(Double amount) {
        requestBody.addProperty(IIObCollectionDetailsGeneralModel.AMOUNT, amount);

        return this;
    }

    public DObCollectionSchemaTestUtils withBankAccountNo(String bankAccountNo) {
        requestBody.addProperty(IIObCollectionDetailsGeneralModel.BANK_ACCOUNT_NUMBER, bankAccountNo);
        return this;
    }

    public void assertDoesNotThrowException() {
        assertDoesNotThrow( () -> CommonControllerUtils.applySchemaValidationForString(IIObCollectionDetailsJsonSchema.COLLECTION_DETAILS_SCHEMA , requestBody.toString()) );

    }
}
