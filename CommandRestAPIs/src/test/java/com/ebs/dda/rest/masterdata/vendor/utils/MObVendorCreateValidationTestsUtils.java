package com.ebs.dda.rest.masterdata.vendor.utils;

import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.IMObVendorCreationValueObject;
import com.ebs.dda.jpa.masterdata.vendor.IObVendorPurchaseUnitValueObject;
import com.ebs.dda.jpa.masterdata.vendor.MObVendor;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorCreationValueObject;
import com.ebs.dda.masterdata.vendor.MObVendorCreationValidator;
import com.ebs.dda.masterdata.vendor.MObVendorJsonSchema;
import com.ebs.dda.repositories.masterdata.businessunit.CObPurchasingUnitGeneralModelRep;
import com.ebs.dda.repositories.masterdata.purchaseresponsible.CObPurchasingResponsibleGeneralModelRep;
import com.ebs.dda.repositories.masterdata.vendor.MObVendorRep;
import com.ebs.dda.rest.CommonControllerUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class MObVendorCreateValidationTestsUtils {

  private final MObVendorCreationValueObject valueObject;
  private final MObVendorCreationValidator validator;
  private CObPurchasingResponsibleGeneralModelRep purchasingResponsibleGeneralModelRep;
  private CObPurchasingUnitGeneralModelRep purchasingUnitGeneralModelRep;
  private MObVendorRep vendorRep;
  private String valueObjectAsJson;

  public MObVendorCreateValidationTestsUtils() {
    valueObject = new MObVendorCreationValueObject();
    validator = new MObVendorCreationValidator();
    purchasingResponsibleGeneralModelRep =
        Mockito.mock(CObPurchasingResponsibleGeneralModelRep.class);
    purchasingUnitGeneralModelRep = Mockito.mock(CObPurchasingUnitGeneralModelRep.class);
    vendorRep = Mockito.mock(MObVendorRep.class);
  }

  public void applySchemaValidationOnValueObject() throws Exception {
    String schema = MObVendorJsonSchema.CREATE;
    CommonControllerUtils.applySchemaValidationForString(schema, valueObjectAsJson);
  }

  public MObVendorCreateValidationTestsUtils convertValueObjectToJson()
      throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    valueObjectAsJson = objectMapper.writeValueAsString(valueObject);
    return this;
  }

  public MObVendorCreateValidationTestsUtils withOldVendorReference(String oldVendorReference) {
    valueObject.setOldVendorReference(oldVendorReference);
    return this;
  }

  public MObVendorCreateValidationTestsUtils withVendorName(String vendorName) {
    valueObject.setVendorName(vendorName);
    return this;
  }

  public MObVendorCreateValidationTestsUtils withPurchaseUnits(String purchaseUnitCode) {
    List<IObVendorPurchaseUnitValueObject> result = new ArrayList<>();
    IObVendorPurchaseUnitValueObject purchaseUnitValueObject =
        new IObVendorPurchaseUnitValueObject();
    purchaseUnitValueObject.setPurchaseUnitCode(purchaseUnitCode);
    result.add(purchaseUnitValueObject);
    valueObject.setPurchaseUnits(result);
    return this;
  }

  public MObVendorCreateValidationTestsUtils withVendorType(String vendorType) {
    valueObject.setVendorType(vendorType);
    return this;
  }

  public MObVendorCreateValidationTestsUtils withPurchaseResponsibleCode(
      String purchaseResponsibleCode) {
    valueObject.setPurchaseResponsibleCode(purchaseResponsibleCode);
    return this;
  }

  public MObVendorCreateValidationTestsUtils withAccountWithVendor(String accountWithVendor) {
    valueObject.setAccountWithVendor(accountWithVendor);
    return this;
  }

  public void whenValidateVendorCreateValueObject() throws Exception {

    mockPurchasingResponsibleGeneralModelRep();
    mockPurchasingUnitGeneralModelRep();
    mockVendorRep();
    validator.setPurchasingResponsibleGeneralModelRep(purchasingResponsibleGeneralModelRep);
    validator.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    validator.setVendorRep(vendorRep);

    validator.validate(valueObject);
  }

  public ValidationResult whenValidateVendorCreateBusinessRules() {

    mockPurchasingResponsibleGeneralModelRep();
    mockPurchasingUnitGeneralModelRep();
    mockVendorRep();
    validator.setPurchasingResponsibleGeneralModelRep(purchasingResponsibleGeneralModelRep);
    validator.setPurchasingUnitGeneralModelRep(purchasingUnitGeneralModelRep);
    validator.setVendorRep(vendorRep);

    return validator.validateOnBusiness(valueObject);
  }

  public void assertThatErrorCode_VENDOR_OLD_NUMBER_EXIST_IsReturned() {
    ValidationResult validationResult = whenValidateVendorCreateBusinessRules();
    String oldVendorReferenceErrorCode =
        validationResult.getErrorCode(IMObVendorCreationValueObject.OLD_VENDOR_REFERENCE);
    Assertions.assertEquals(oldVendorReferenceErrorCode, "Vendor-msg-04");
  }

  public void assertThatErrorCode_VENDOR_OLD_NUMBER_EXIST_IsNotReturned() {
    ValidationResult validationResult = whenValidateVendorCreateBusinessRules();
    String oldVendorReferenceErrorCode =
        validationResult.getErrorCode(IMObVendorCreationValueObject.OLD_VENDOR_REFERENCE);
    Assertions.assertNull(oldVendorReferenceErrorCode);
  }

  private void mockPurchasingResponsibleGeneralModelRep() {
    Mockito.when(purchasingResponsibleGeneralModelRep.findOneByUserCode("0002"))
        .thenReturn(new CObPurchasingResponsibleGeneralModel());
  }

  private void mockPurchasingUnitGeneralModelRep() {
    Mockito.when(purchasingUnitGeneralModelRep.findOneByUserCode("0002"))
        .thenReturn(new CObPurchasingUnitGeneralModel());
  }

  private void mockVendorRep() {
    Mockito.when(vendorRep.findOneByOldVendorNumber("1")).thenReturn(null);
    Mockito.when(vendorRep.findOneByOldVendorNumber("111111")).thenReturn(new MObVendor());
  }
}
