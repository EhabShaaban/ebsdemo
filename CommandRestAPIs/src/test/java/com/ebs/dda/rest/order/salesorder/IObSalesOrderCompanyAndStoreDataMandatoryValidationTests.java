package com.ebs.dda.rest.order.salesorder;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.salesorder.utils.IObSalesOrderCompanyAndStoreDataValidationTestsUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class IObSalesOrderCompanyAndStoreDataMandatoryValidationTests {

  private IObSalesOrderCompanyAndStoreDataValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new IObSalesOrderCompanyAndStoreDataValidationTestsUtils();
  }

  // Storehouse Tests

  @Test
  public void
  Should_Pass_When_StorehouseFieldNotExistsInValueObjectInDraftState() {
    utils.withSalesOrderCode("2020000005").withSalesOrderInState("Draft");

    assertDoesNotThrow(() -> utils.whenValidateSaveCompanyAndStoreValueObject());
  }

  @Test
  public void
  Should_ThrowArgumentViolationSecurityException_When_StorehouseFieldNotExistsInDatabaseInDraftState() {
    utils.withSalesOrderCode("2020000005").withSalesOrderInState("Draft");
    utils.withStoreCode("9999");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveCompanyAndStoreValueObject());
  }

  @Test
  public void
  Should_ThrowArgumentViolationSecurityException_When_StorehouseFieldNotExistsInValueObjectInRejectedState() {
    utils.withSalesOrderCode("2020000005").withSalesOrderInState("Rejected");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveCompanyAndStoreValueObject());
  }

  @Test
  public void
  Should_ThrowArgumentViolationSecurityException_When_StorehouseFieldNotExistsInDatabaseInRejectedState() {
    utils.withSalesOrderCode("2020000005").withSalesOrderInState("Rejected");
    utils.withStoreCode("9999");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveCompanyAndStoreValueObject());
  }
}
