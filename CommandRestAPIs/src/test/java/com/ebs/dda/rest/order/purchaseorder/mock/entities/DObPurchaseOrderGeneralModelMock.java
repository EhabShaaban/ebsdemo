package com.ebs.dda.rest.order.purchaseorder.mock.entities;

import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;

public class DObPurchaseOrderGeneralModelMock extends DObPurchaseOrderGeneralModel {
    private String objectTypeCode;
    private String refDocumentCode;


    @Override
    public String getObjectTypeCode() {
        return objectTypeCode;
    }

    public void setObjectTypeCode(String objectTypeCode) {
        this.objectTypeCode = objectTypeCode;
    }

    @Override
    public String getRefDocumentCode() {
        return refDocumentCode;
    }

    public void setRefDocumentCode(String refDocumentCode) {
        this.refDocumentCode = refDocumentCode;
    }
}
