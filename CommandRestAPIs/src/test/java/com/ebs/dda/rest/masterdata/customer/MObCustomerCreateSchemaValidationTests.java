package com.ebs.dda.rest.masterdata.customer;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.masterdata.customer.utils.MObCustomerCreateValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class MObCustomerCreateSchemaValidationTests {

  private MObCustomerCreateValidationTestsUtils utils;

  @Before
  public void init() throws JsonProcessingException {
    utils = new MObCustomerCreateValidationTestsUtils();
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.015);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("12345");
    utils.convertValueObjectToJson();
  }

  @Test
  public void Should_PassSchemaValidation_When_AllFieldsCorrectInValueObject() {
    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  //  Legal Name Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_LegalNameFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withLegalName(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_LegalNameFieldNotValidAsEmpty()
      throws JsonProcessingException {
    utils.withLegalName("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_LegalNameFieldNotValidAsMoreThan100()
          throws JsonProcessingException {
    utils.withLegalName(
        "MoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100Char");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_LegalNameFieldNotValidAsPattern()
      throws JsonProcessingException {
    utils.withLegalName("#[]");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  Market Name Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_MarketNameFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withMarketName(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_MarketNameFieldNotValidAsEmpty()
      throws JsonProcessingException {
    utils.withMarketName("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_MarketNameFieldNotValidAsMoreThan100()
          throws JsonProcessingException {
    utils.withMarketName(
        "MoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100Char");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_MarketNameFieldNotValidAsPattern()
      throws JsonProcessingException {
    utils.withMarketName("#[]");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  BusinessUnitCodes Tests

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotValidAsNull()
          throws JsonProcessingException {
    utils.withBusinessUnitCodes(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotValidAsNullForEachBusinessUnitCode()
          throws JsonProcessingException {
    utils.withBusinessUnitCodes(Arrays.asList(null, null));
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotValidAsSpacesForEachBusinessUnitCode()
          throws JsonProcessingException {
    utils.withBusinessUnitCodes(Arrays.asList("   ", "    "));
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotValidAsEmptyForEachBusinessUnitCode()
          throws JsonProcessingException {
    utils.withBusinessUnitCodes(Arrays.asList("", ""));
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotValidMoreThan4ForEachBusinessUnitCode()
          throws JsonProcessingException {
    utils.withBusinessUnitCodes(Arrays.asList("12345", "1234"));
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotValidAsPatternForEachBusinessUnitCode()
          throws JsonProcessingException {
    utils.withBusinessUnitCodes(Arrays.asList("abcd", "1234"));
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  CreditLimit Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CreditLimitFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withCreditLimit(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CreditLimitFieldNotValidAsPositiveNumber()
          throws JsonProcessingException {
    utils.withCreditLimit(-1.0);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  CurrencyIso Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyIsoFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withCurrencyIso(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyIsoFieldNotValidAsEmpty()
      throws JsonProcessingException {
    utils.withCurrencyIso("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyIsoFieldNotValidAsLessThan3()
          throws JsonProcessingException {
    utils.withCurrencyIso("US");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyIsoFieldNotValidAsMoreThan3()
          throws JsonProcessingException {
    utils.withCurrencyIso("USDe");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyIsoFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withCurrencyIso("Us3");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  RegistrationNumber Tests

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_RegistrationNumberFieldNotValidAsNull()
          throws JsonProcessingException {
    utils.withRegistrationNumber(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_RegistrationNumberFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withRegistrationNumber("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_Pass_When_RegistrationNumberFieldHasCorrectPattern()
      throws JsonProcessingException {
    utils.withRegistrationNumber("abC");
    utils.convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_RegistrationNumberFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withRegistrationNumber("#[]");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_RegistrationNumberFieldNotValidAsMoreThan100()
          throws JsonProcessingException {
    utils.withRegistrationNumber(
        "MoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100Char");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  IssuedFromCode Tests

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_IssuedFromCodeFieldNotValidAsNull()
          throws JsonProcessingException {
    utils.withIssuedFromCode(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_IssuedFromCodeFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withIssuedFromCode("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_IssuedFromCodeFieldNotValidAsLessThan4()
          throws JsonProcessingException {
    utils.withIssuedFromCode("12");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_IssuedFromCodeFieldNotValidAsMoreThan4()
          throws JsonProcessingException {
    utils.withIssuedFromCode("12345");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_IssuedFromCodeFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withIssuedFromCode("abcD");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  TaxAdministrationCode Tests

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationCodeFieldNotValidAsNull()
          throws JsonProcessingException {
    utils.withTaxAdministrationCode(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationCodeFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withTaxAdministrationCode("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationCodeFieldNotValidAsLessThan4()
          throws JsonProcessingException {
    utils.withTaxAdministrationCode("12");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationCodeFieldNotValidAsMoreThan4()
          throws JsonProcessingException {
    utils.withTaxAdministrationCode("12345");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationCodeFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withTaxAdministrationCode("abcD");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  TaxCardNumber Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxCardNumberFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withTaxCardNumber(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxCardNumberFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withTaxCardNumber("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_Pass_When_TaxCardNumberFieldHasCorrectPattern()
      throws JsonProcessingException {
    utils.withTaxCardNumber("abcD");
    utils.convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxCardNumberFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withTaxCardNumber("#[]");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxCardNumberFieldNotValidAsMoreThan100()
          throws JsonProcessingException {
    utils.withTaxCardNumber(
        "MoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100Char");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  TaxFileNumber Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxFileNumberFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withTaxFileNumber(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxFileNumberFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withTaxFileNumber("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_Pass_When_TaxFileNumberFieldHasCorrectPattern()
      throws JsonProcessingException {
    utils.withTaxFileNumber("abcD");
    utils.convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxFileNumberFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withTaxFileNumber("#[]");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_TaxFileNumberFieldNotValidAsMoreThan100()
          throws JsonProcessingException {
    utils.withTaxFileNumber(
        "MoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100CharMoreThan100Char");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  // oldCustomerNumber
  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_OldCustomerNumberNotValidAsNull()
      throws JsonProcessingException {
    utils.withOldCustomerNumber(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldCustomerNumberFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withOldCustomerNumber("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_Pass_When_OldCustomerNumberFieldHasCorrectPattern()
      throws JsonProcessingException {
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldCustomerNumberFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withOldCustomerNumber("abc");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldCustomerNumberFieldNotValidAsMoreThan20()
          throws JsonProcessingException {
    utils.withOldCustomerNumber("123456789101112131415");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
}
