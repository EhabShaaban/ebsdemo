package com.ebs.dda.rest.order.salesorder;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.salesorder.utils.IObSalesOrderDataSaveValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class IObSalesOrderDataMandatoryValidatorTests {

  private IObSalesOrderDataSaveValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
  }

  @Test
  public void Should_PassMandatoryValidation_When_AllFieldsAreExistInValueObject()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    assertDoesNotThrow(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  // notes
  @Test
  public void Should_Pass_When_NotesFieldIsSpaces() throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("  ");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    assertDoesNotThrow(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  // currencyCode
  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldNotExistsInValueObject() // mandatory
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldIsSpaces()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectCurrencyCode("  ");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldNotExistsInDataBase()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectCurrencyCode("9999");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldNotEGPForLocalSalesOrder()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectCurrencyCode("0002");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0002")
        .currencyRowWithCurrencyIso("USD");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  // expectedDeliveryDate
  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ExpectedDeliveryDateFieldIsSpaces()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectExpectedDeliveryDate("  ");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  // contactPersonId
  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ContactPersonIdFieldNotExistInDataBase()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(99L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ContactPersonIdFieldNotRelatedToSalesOrderDataCustomer()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(3L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(3L)
        .customerContactPersonWithCustomerCode("000006")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }
  // customerAddressId
  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CustomerAddressIdFieldNotExistsInDataBase()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(99L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_CustomerAddressIdFieldNotRelatedToSalesOrderDataCustomer()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("0001");
    utils.withValueObjectCustomerAddressId(4L);
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(4L)
        .customerAddressWithCustomerCode("000006")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }
  // paymentTermCode
  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_PaymentTermFieldNotExistsInDataBase()
          throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("9999");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_PaymentTermFieldIsSpaces()
      throws JsonProcessingException {
    utils = new IObSalesOrderDataSaveValidationTestsUtils();
    utils.withValueObjectSalesOrderCode("2019000010");
    utils.withValueObjectPaymentTermCode("   ");
    utils.withValueObjectCustomerAddressId(6L);
    utils.withValueObjectContactPersonId(5L);
    utils.withValueObjectCurrencyCode("0001");
    utils.withValueObjectNotes("notes for save hp");
    utils.withValueObjectExpectedDeliveryDate("01-Jul-2021 12:44 PM");
    utils.convertValueObjectToJson();

    utils
        .salesOrderDataWithSalesOrderCode("2019000010")
        .salesOrderDataWithCustomerCode("000007")
        .customerAddressWithId(6L)
        .customerAddressWithCustomerCode("000007")
        .customerContactPersonWithId(5L)
        .customerContactPersonWithCustomerCode("000007")
        .currencyRowWithCurrencyCode("0001")
        .currencyRowWithCurrencyIso("EGP");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateSaveIObSalesOrderDataValueObject());
  }
}
