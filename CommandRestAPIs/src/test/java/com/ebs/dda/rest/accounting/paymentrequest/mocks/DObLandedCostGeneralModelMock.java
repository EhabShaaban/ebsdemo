package com.ebs.dda.rest.accounting.paymentrequest.mocks;

import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;

public class DObLandedCostGeneralModelMock extends DObLandedCostGeneralModel {


    private String userCode;
    private String purchaseOrderCode;


    public DObLandedCostGeneralModelMock withPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
        return this;
    }

    public DObLandedCostGeneralModelMock withUserCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    @Override
    public String getUserCode() {
        return userCode;
    }

    @Override
    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }
}
