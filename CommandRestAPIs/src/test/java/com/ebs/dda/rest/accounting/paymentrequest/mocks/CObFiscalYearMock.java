package com.ebs.dda.rest.accounting.paymentrequest.mocks;

import com.ebs.dda.jpa.accounting.CObFiscalYear;

public class CObFiscalYearMock extends CObFiscalYear {
  private String userCode;

  public CObFiscalYearMock withUserCode(String userCode) {
    this.userCode = userCode;
    return this;
  }

  @Override
  public String getUserCode() {
    return userCode;
  }
}
