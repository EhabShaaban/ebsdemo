package com.ebs.dda.rest.accounting.settlement.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.settlement.apis.IIObSettlementAccountingDetailsJsonSchema;
import com.ebs.dda.jpa.accounting.settlements.IIObSettlementAddAccountingDetailsValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class IObSettlementAddAccountingDetailsSchemaTestUtils {

  private JsonObject requestBody;

  public IObSettlementAddAccountingDetailsSchemaTestUtils() {
    requestBody = new JsonObject();
  }

  public IObSettlementAddAccountingDetailsSchemaTestUtils withCode(String settlementCode) {
    return this;
  }

  public IObSettlementAddAccountingDetailsSchemaTestUtils withGLAccountCode(String glAccountCode) {
    requestBody.addProperty(
        IIObSettlementAddAccountingDetailsValueObject.GL_ACCOUNT, glAccountCode);
    return this;
  }

  public IObSettlementAddAccountingDetailsSchemaTestUtils withAccountType(String accountType) {
    requestBody.addProperty(
        IIObSettlementAddAccountingDetailsValueObject.ACCOUNT_TYPE, accountType);
    return this;
  }

  public IObSettlementAddAccountingDetailsSchemaTestUtils withGLSubAccount(String subAccountCode) {
    requestBody.addProperty(
        IIObSettlementAddAccountingDetailsValueObject.GL_SUB_ACCOUNT, subAccountCode);
    return this;
  }

  public IObSettlementAddAccountingDetailsSchemaTestUtils withAmount(Double amount) {
    requestBody.addProperty(IIObSettlementAddAccountingDetailsValueObject.AMOUNT, amount);
    return this;
  }

  public void assertThrowsArgumentSecurityException() {
    assertThrows(
        ArgumentViolationSecurityException.class,
        () -> {
          CommonControllerUtils.applySchemaValidationForString(
              IIObSettlementAccountingDetailsJsonSchema.SAVE_ACCOUNTING_DETAILS_SCHEMA, requestBody.toString());
        });
  }
}
