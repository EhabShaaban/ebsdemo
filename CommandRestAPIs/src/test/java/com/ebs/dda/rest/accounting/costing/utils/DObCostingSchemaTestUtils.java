package com.ebs.dda.rest.accounting.costing.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.accounting.costing.apis.IDObCostingJsonSchema;
import com.ebs.dda.accounting.settlement.apis.IIObSettlementAccountingDetailsJsonSchema;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObCostingSchemaTestUtils {

  private final JsonObject valueObjectAsJson;

  public DObCostingSchemaTestUtils() {
    this.valueObjectAsJson = new JsonObject();
  }

  public void applySchemaValidationOnCostingActivationValueObject() throws Exception {
    String costingActivationSchema = IDObCostingJsonSchema.ACTIVATION_SCHEMA;
    CommonControllerUtils.applySchemaValidationForString(
        costingActivationSchema, valueObjectAsJson.toString());
  }

  public DObCostingSchemaTestUtils withDueDate(String dueDateTime) {
    valueObjectAsJson.addProperty("dueDate", dueDateTime);
    return this;
  }

  public void assertThrowsArgumentSecurityException() {
    assertThrows(
        ArgumentViolationSecurityException.class,
        () -> {
          CommonControllerUtils.applySchemaValidationForString(
              IDObCostingJsonSchema.ACTIVATION_SCHEMA, valueObjectAsJson.toString());
        });
  }
}
