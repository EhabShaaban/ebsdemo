package com.ebs.dda.rest.masterdata.vendor;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.masterdata.vendor.utils.MObVendorCreateValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class MObVendorCreateSchemaValidationTests {

  private MObVendorCreateValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new MObVendorCreateValidationTestsUtils();
  }

  @Test
  public void Should_PassSchemaValidation_When_AllFieldsCorrectInValueObject()
      throws JsonProcessingException {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("1")
        .convertValueObjectToJson();

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  //    OldVendorReference Tests

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldVendorReferenceFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("")
        .convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldVendorReferenceFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("Ay 7aga")
        .convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldVendorReferenceFieldNotValidAsSpaces()
          throws JsonProcessingException {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("    ")
        .convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldVendorReferenceFieldNotValidAsNull()
          throws JsonProcessingException {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference(null)
        .convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_OldVendorReferenceFieldNotValidMoreThan20()
          throws JsonProcessingException {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("111111111111111111111")
        .convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
}
