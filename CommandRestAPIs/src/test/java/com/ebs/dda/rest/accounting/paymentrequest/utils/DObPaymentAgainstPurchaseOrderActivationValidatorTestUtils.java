package com.ebs.dda.rest.accounting.paymentrequest.utils;

import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.paymentrequest.exceptions.DObPaymentAmountGreaterThanPurchaseOrderException;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivatePreparationGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.purchases.dbo.jpa.entities.businessobjects.documentobjects.DObPurchaseOrder;
import com.ebs.dda.validation.paymentrequest.DObPaymentAgainstPurchaseOrderActivationValidator;
import com.ebs.dda.validation.paymentrequest.DObPaymentRequestActivationValidator;
import com.ebs.entities.accounting.FiscalYearMockUtils;
import com.ebs.entities.accounting.payment.PaymentUtils;
import com.ebs.entities.order.purchaseorder.DObPurchaseOrderMockUtils;
import com.ebs.repositories.FiscalYearRepMockUtils;
import com.ebs.repositories.payment.PaymentRequestRepUtils;
import com.ebs.repositories.purchases.PurchaseOrderRepTestUtils;
import org.joda.time.DateTime;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils {

  private DObPaymentAgainstPurchaseOrderActivationValidator validator;
  private DObPaymentRequestActivationValidator paymentRequestActivationValidatorValidator;
  private DObPaymentRequestActivateValueObject valueObject;

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils() {
    valueObject = new DObPaymentRequestActivateValueObject();
    validator =
        new DObPaymentAgainstPurchaseOrderActivationValidator();
    paymentRequestActivationValidatorValidator = new DObPaymentRequestActivationValidator();
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withPaymentCode(
      String paymentCode) {
    PaymentUtils.withPaymentCode(paymentCode);
    valueObject.setPaymentRequestCode(paymentCode);
    return this;
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withPaymentAmount(
      BigDecimal paymentAmount) {
    PaymentUtils.withAmount(paymentAmount);
    return this;
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withBalance(
      BigDecimal balance) {
    PaymentUtils.withBalance(balance);
    return this;
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withDueDate(String dueDate) {
    valueObject.setDueDate(dueDate);
    return this;
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withCurrencyPrice(
      BigDecimal currencyPrice) {
    PaymentUtils.withCurrencyPrice(currencyPrice);
    return this;
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withExchangeRate(
      String exchangeRate) {
    PaymentUtils.withExchangeRate(exchangeRate);
    return this;
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withPurchaseOrderCode(
      String purchaseOrderCode) {
    PaymentUtils.withDueDocumentCode(purchaseOrderCode);
    DObPurchaseOrderMockUtils.withpoCode(purchaseOrderCode);
    return this;
  }

  public DObPaymentAgainstPurchaseOrderActivationValidatorTestUtils withPoRemaining(
      BigDecimal remaining) {
    DObPurchaseOrderMockUtils.withRemaining(remaining);
    return this;
  }

  private ValidationResult executeValidate() throws Exception {
    prepareValidatorDependencies();
    return validator.validateBusinessRules(valueObject);
  }

  private void prepareValidatorDependencies() {
    prepareFiscalYear();
    preparePaymentRequestActivatePreparationGeneralModel();
    preparePurchaseOrder();
  }

  private void prepareFiscalYear() {
        FiscalYearRepMockUtils.mockFindActiveFiscalYears(
          FiscalYearMockUtils.mockEntity()
            .id(1L)
            .currentState("%"+ BasicStateMachine.ACTIVE_STATE + "%")
            .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2019 00:00 AM")))
            .toDate(new DateTime(getDateTimeInMilliSecondsFormat("31-DEC-2019 11:59 PM")))
            .build()
        );

    validator.setFiscalYearRep(FiscalYearRepMockUtils.getRepository());
  }

  private void preparePaymentRequestActivatePreparationGeneralModel() {
    DObPaymentRequestActivatePreparationGeneralModel paymentRequestActivatePreparationGeneralModel =
        PaymentUtils.buildPaymentRequestActivatePreparationGeneralModel();
    PaymentRequestRepUtils.findPaymentRequestActivatePreparationGeneralModelByPaymentCode(
        paymentRequestActivatePreparationGeneralModel);
    validator
        .setPaymentRequestActivatePreparationGeneralModelRep(
            PaymentRequestRepUtils.getPaymentRequestActivatePreparationGeneralModelRep());
  }

  private void preparePurchaseOrder() {
    DObPurchaseOrder purchaseOrder = DObPurchaseOrderMockUtils.buildPurchaseOrder();
    PurchaseOrderRepTestUtils.findOneByUserCode(purchaseOrder, "2019000108");
    validator.setDObPurchaseOrderRep(
        PurchaseOrderRepTestUtils.getRepository());
  }

  public void throwsPaymentAmountGreaterThanPurchaseOrderException() throws Exception {
    assertThrows(DObPaymentAmountGreaterThanPurchaseOrderException.class, () -> executeValidate());
  }

  public void validateSuccessfully() throws Exception {
    ValidationResult validationResult = executeValidate();
    org.junit.Assert.assertTrue(validationResult.isValid());
  }

  public Long getDateTimeInMilliSecondsFormat(String dateTime) {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    Date date = null;
    try {
      date = dateFormat.parse(dateTime);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date.getTime();
  }

}
