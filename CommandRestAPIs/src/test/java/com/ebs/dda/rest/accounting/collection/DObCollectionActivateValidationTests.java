package com.ebs.dda.rest.accounting.collection;

import com.ebs.dda.jpa.accounting.collection.CollectionMethodEnum;
import com.ebs.dda.rest.accounting.collection.utils.DObCollectionActivateValidatorUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;

@RunWith(SpringRunner.class)
public class DObCollectionActivateValidationTests {

  private DObCollectionActivateValidatorUtils utils;

  @Before
  public void init() throws ParseException {
    utils = new DObCollectionActivateValidatorUtils();
  }

  @Test
  public void shouldThrowExceptionWhenJournalEntryDateNotInActiveFiscalYear() {
    utils
        .withCollection("2021000001", "2021000003", new BigDecimal(3000))
        .withJournalEntryDate("01-Feb-2018 02:00 AM")
        .withCollectionDetails(
            "2021000001",
            new BigDecimal(3000),
            CollectionMethodEnum.CollectionMethod.CASH.name(),
            "0002",
            "")
        .withSalesInvoiceDataReferenceDocument("2021000003", "4000", "Active")
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenWhenJournalEntryDateInActiveFiscalYear() {
    utils
        .withCollection("2021000001", "2021000003", new BigDecimal(3000))
        .withJournalEntryDate("01-Feb-2022 02:00 AM")
        .withCollectionDetails(
            "2021000001",
            new BigDecimal(3000),
            CollectionMethodEnum.CollectionMethod.CASH.name(),
            "0002",
            "")
        .withSalesInvoiceDataReferenceDocument("2021000003", "4000", "Active")
        .assertDoesNotThrowException();
  }
}
