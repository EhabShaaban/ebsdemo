package com.ebs.dda.rest.masterdata.unitofmeasure;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.masterdata.unitofmeasure.utils.CObMeasureCreateValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class CObMeasureCreateSchemaValidationTests {

  private CObMeasureCreateValidationTestsUtils utils;

  @Before
  public void init() throws JsonProcessingException {
    utils = new CObMeasureCreateValidationTestsUtils();
    utils.withUnitOfMeasureName("M2");
    utils.withIsStandard(Boolean.FALSE);
    utils.convertValueObjectToJson();
  }

  @Test
  public void Should_PassSchemaValidation_When_AllFieldsCorrectInValueObject() {
    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  //  UOM Name Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_UomNameFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withUnitOfMeasureName(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_UomNameFieldNotValidAsEmpty()
      throws JsonProcessingException {
    utils.withUnitOfMeasureName("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_UomNameFieldNotValidAsMoreThan50()
      throws JsonProcessingException {
    utils.withUnitOfMeasureName(
        "MoreThan50CharMoreThan50CharMoreThan50CharMoreThan50CharMoreThan50CharMoreThan50CharMoreThan50CharMoreThan50Char");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  //  isStandard Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_IsStandardFieldNotValidAsNull()
      throws JsonProcessingException {
    utils.withIsStandard(null);
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
}
