package com.ebs.dda.rest.order.purchaseorder.items;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.purchaseorder.items.utils.ServiceItemSaveSchemaValidationTestsUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class ServiceItemSaveSchemaValidationTests {

  private ServiceItemSaveSchemaValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new ServiceItemSaveSchemaValidationTestsUtils();
  }

  //  Price, Quantity : number
  //  Price, Quantity : minimum 0
  //  Price, Quantity : maximum 1000000000
  //  Price, Quantity : multiple of 0.000001

  @Test
  public void Should_PassSchemaValidation_When_ValueMatchesWithSchema() {

    utils.withPrice("152.014262").withQuantity("152.014262");

    assertDoesNotThrow(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_PriceValueNotValidAsNumber() {
    utils.withPriceAsString(" ").withQuantity("152.014262");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_PriceValueNotValidAsMinimum() {
    utils.withPrice("-152.014262").withQuantity("152.014262");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_PriceValueNotValidAsMaximum() {
    utils.withPrice("1000000001.0").withQuantity("152.014262");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_PriceValueNotValidAsMultipleOf() {
    utils.withPrice("152.0142621").withQuantity("152.014262");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  //  Quantity Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_QuantityValueNotValidAsNumber() {
    utils.withPrice("152.014262").withQuantityAsString(" ");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_QuantityValueNotValidAsMinimum() {
    utils.withPrice("152.014262").withQuantity("-152.014262");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_QuantityValueNotValidAsMaximum() {
    utils.withPrice("152.014262").withQuantity("1000000001.0");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_QuantityValueNotValidAsMultipleOf() {
    utils.withPrice("152.014262").withQuantity("152.0142621");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnServiceItemValueObject());
  }
}
