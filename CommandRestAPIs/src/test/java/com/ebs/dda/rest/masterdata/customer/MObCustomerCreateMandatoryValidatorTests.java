package com.ebs.dda.rest.masterdata.customer;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.masterdata.customer.utils.MObCustomerCreateValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class MObCustomerCreateMandatoryValidatorTests {

  private MObCustomerCreateValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new MObCustomerCreateValidationTestsUtils();
  }

  @Test
  public void Should_PassMandatoryValidation_When_AllFieldsAreExistInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();
    assertDoesNotThrow(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // LegalName Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_LegalNameFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_LegalNameFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withLegalName("  ");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // MarketName Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_MarketNameFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_MarketNameFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("  ");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // BusinessUnitCodes Tests

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotExistInValueObject()
          throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList());
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_BusinessUnitCodesFieldNotExistInDataBase()
          throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("9999","0001"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // CreditLimit Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CreditLimitFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // Currency Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("   ");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_CurrencyFieldNotExistsInDataBase()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("XXX");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // RegistrationNumber Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_RegistrationNumberFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_RegistrationNumberFieldNotValidAsSpaces()
          throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("    ");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // IssuedFrom Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_IssuedFromFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_IssuedFromFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("    ");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }


  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_IssuedFromFieldNotExistsInDataBase()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("9999");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // TaxAdministration Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("    ");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxAdministrationFieldNotExistsInDataBase()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("9999");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // TaxCardNumber Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxCardNumberFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxCardNumberFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("   ");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  // TaxFileNumber Tests

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxFileNumberFieldNotExistsInValueObject()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TaxFileNumberFieldNotValidAsSpaces()
      throws JsonProcessingException {
    utils.withLegalName("مطبعة أكتوبر الهندسية");
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("  ");
    utils.withOldCustomerNumber("123");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }
  // oldCustomerNumber

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_OldCustomerNumberFieldNotExistsInValueObject()
          throws JsonProcessingException {
    utils.withMarketName("مطبعة أكتوبر الهندسية");
    utils.withBusinessUnitCodes(Arrays.asList("0001", "0002"));
    utils.withCreditLimit(100000.0);
    utils.withCurrencyIso("EGP");
    utils.withRegistrationNumber("56734");
    utils.withIssuedFromCode("0001");
    utils.withTaxAdministrationCode("0001");
    utils.withTaxCardNumber("345-567-874");
    utils.withTaxFileNumber("567-4-34523-345-23-45");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
            .isThrownBy(() -> utils.whenValidateCreateCustomerValueObject());
  }
}
