package com.ebs.dda.rest.order.purchaseorder;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.purchaseorder.utils.DObServicePurchaseOrderConfirmValidationTestsUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class DObServicePurchaseOrderConfirmMandatoryValidationTests {

  private DObServicePurchaseOrderConfirmValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new DObServicePurchaseOrderConfirmValidationTestsUtils();
  }

  // ConfirmationDateTime Tests

  @Test
  public void Should_Pass_When_ConfirmationDateTimeFieldExistsInValueObjectInDraftState() throws ParseException {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM");
    assertDoesNotThrow(() -> utils.whenValidateServicePurchaseOrderConfirmValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ConfirmationDateTimeFieldNotExistsInValueObjectInDraftState() {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateServicePurchaseOrderConfirmValueObject());
  }

  @Test
  public void Should_Pass_When_PaymentDetailsSectionHasAllMandatories() throws Exception {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM")
        .withPaymentDetails("2020000005", "SERVICE_PO", "EGP", "0002")
        .assertThatSectionFieldIsNotMissing("PaymentDetails");
  }

  @Test
  public void Should_Pass_When_ItemSectionHasAllMandatories() throws Exception {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM")
        .withPaymentDetails("2020000005", "SERVICE_PO", "EGP", "0002")
        .assertThatSectionFieldIsNotMissing("ItemsData");
  }

  @Test
  public void Should_ReturnErrorBindToPaymentDetailsSection_When_CurrencyIsMissing()
      throws Exception {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM")
        .withPaymentDetails("2020000005", "SERVICE_PO", null, "0002")
        .assertThatSectionFieldIsMissing("PaymentDetails", "currencyIso");
  }

  @Test
  public void Should_ReturnErrorBindToPaymentDetailsSection_When_PaymentTermCodeIsMissing()
      throws Exception {
    utils
        .withPurchaseOrderCode("2020000005")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM")
        .withPaymentDetails("2020000005", "SERVICE_PO", "EGP", null)
        .assertThatSectionFieldIsMissing("PaymentDetails", "paymentTermCode");
  }

  @Test
  public void Should_ReturnErrorBindToItemSection_When_ItemSectionIsMissing() throws Exception {
    utils
        .withPurchaseOrderCode("2020000006")
        .withRefDocumentCode("2020000004")
        .withPurchaseOrderInState("Draft")
        .withObjectTypeCode("SERVICE_PO")
        .withConfirmationDateTime("06-Jan-2019 11:30 AM")
        .withPaymentDetails("2020000006", "SERVICE_PO", "EGP", "0002")
        .assertThatSectionFieldIsMissing("ItemsData", "OrderItemsList");
  }
}
