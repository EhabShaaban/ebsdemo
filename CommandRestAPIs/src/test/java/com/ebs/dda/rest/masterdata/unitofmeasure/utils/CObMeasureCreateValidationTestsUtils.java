package com.ebs.dda.rest.masterdata.unitofmeasure.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.jpa.masterdata.measure.CObMeasureCreationValueObject;
import com.ebs.dda.masterdata.unitofmeasure.CObMeasureJsonSchema;
import com.ebs.dda.rest.CommonControllerUtils;
import com.ebs.dda.validation.masterdata.unitofmeasure.CObMeasureCreateMandatoryValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CObMeasureCreateValidationTestsUtils {

  private final String createSchema;
  private final CObMeasureCreateMandatoryValidator validator;
  private final CObMeasureCreationValueObject valueObject;
  private String valueObjectAsJson;

  public CObMeasureCreateValidationTestsUtils() {
    createSchema = CObMeasureJsonSchema.CREATE;
    valueObject = new CObMeasureCreationValueObject();
    validator = new CObMeasureCreateMandatoryValidator();
  }

  public CObMeasureCreateValidationTestsUtils withUnitOfMeasureName(String uomNameValue) {
    valueObject.setUnitOfMeasureName(uomNameValue);
    return this;
  }

  public CObMeasureCreateValidationTestsUtils withIsStandard(Boolean isStandardValue) {
    valueObject.setIsStandard(isStandardValue);
    return this;
  }

  public void whenValidateCreateUnitOfMeasureValueObject()
      throws ArgumentViolationSecurityException {
    validator.validate(valueObject);
  }

  public CObMeasureCreateValidationTestsUtils convertValueObjectToJson()
      throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    valueObjectAsJson = objectMapper.writeValueAsString(valueObject);
    return this;
  }
  public void applySchemaValidationOnValueObject() throws Exception {
    CommonControllerUtils.applySchemaValidationForString(createSchema, valueObjectAsJson);
  }
}
