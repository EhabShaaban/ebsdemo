package com.ebs.dda.rest.accounting.notesreceivable;

import com.ebs.dda.rest.accounting.notesreceivable.utils.IObNotesReceivableSaveNotesDetailsValidatorUtils;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class IObNotesReceivableSaveNotesDetailsValidationTests {

  private IObNotesReceivableSaveNotesDetailsValidatorUtils utils;

  @Before
  public void init() {
    utils = new IObNotesReceivableSaveNotesDetailsValidatorUtils();
  }

  @Test
  public void shouldThrowExceptionWhenDepotExist() {
    utils
        .withNotesDetails("2021000001", null, "QNB Bank", new DateTime(), "0001")
        .whenDepotExist()
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenDepotDoesNotExists() {
    utils
        .withNotesDetails("2021000001", null, "QNB Bank", new DateTime(), "9999")
        .whenDepotDoesNotExist()
        .assertThrowsArgumentViolationSecurityException();
  }
}
