package com.ebs.dda.rest.order.purchaseorder.items.utils;

import com.ebs.dda.purchases.apis.IDObPurchaseOrderJsonSchema;
import com.ebs.dda.purchases.dbo.jpa.valueobjects.IIObOrderLineDetailsQuantitiesValueObject;
import com.ebs.dda.rest.CommonControllerUtils;
import com.google.gson.JsonObject;
import java.math.BigDecimal;

public class ItemQuantitySaveSchemaValidationTestsUtils {

  private JsonObject valueObjectAsJson;

  public ItemQuantitySaveSchemaValidationTestsUtils() {
    valueObjectAsJson = new JsonObject();
  }

  public void applySchemaValidationOnItemQuantitySaveValueObject() throws Exception {
    String saveItemQuantitySchema = IDObPurchaseOrderJsonSchema.SAVE_ITEM_QUANTITY;
    CommonControllerUtils.applySchemaValidationForString(saveItemQuantitySchema, valueObjectAsJson.toString());
  }

  public ItemQuantitySaveSchemaValidationTestsUtils withPrice(String price) {
    valueObjectAsJson.addProperty(
        IIObOrderLineDetailsQuantitiesValueObject.PRICE, new BigDecimal(price));
    return this;
  }

  public ItemQuantitySaveSchemaValidationTestsUtils withPriceAsString(String price) {
    valueObjectAsJson.addProperty(IIObOrderLineDetailsQuantitiesValueObject.PRICE, price);
    return this;
  }

  public ItemQuantitySaveSchemaValidationTestsUtils withQuantity(String quantity) {
    valueObjectAsJson.addProperty(
        IIObOrderLineDetailsQuantitiesValueObject.QUANTITY, new BigDecimal(quantity));
    return this;
  }

  public ItemQuantitySaveSchemaValidationTestsUtils withQuantityAsString(String quantity) {
    valueObjectAsJson.addProperty(IIObOrderLineDetailsQuantitiesValueObject.QUANTITY, quantity);
    return this;
  }
}
