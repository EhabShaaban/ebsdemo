package com.ebs.dda.rest.masterdata.vendor;

import com.ebs.dda.rest.masterdata.vendor.utils.MObVendorCreateValidationTestsUtils;
import org.junit.Before;
import org.junit.Test;

public class MObVendorCreateBusinessRulesValidationTests {

  private MObVendorCreateValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new MObVendorCreateValidationTestsUtils();
  }

  @Test
  public void Should_Pass_When_OldVendorNumberUnique() {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("1")
        .assertThatErrorCode_VENDOR_OLD_NUMBER_EXIST_IsNotReturned();
  }

  @Test
  public void Should_ReturnError_When_OldVendorNumberNotUnique() {
    utils
        .withVendorName("Alex Professional Limited Company International")
        .withPurchaseUnits("0002")
        .withPurchaseResponsibleCode("0002")
        .withAccountWithVendor("Madina 12345-fgh")
        .withVendorType("COMMERCIAL")
        .withOldVendorReference("111111")
        .assertThatErrorCode_VENDOR_OLD_NUMBER_EXIST_IsReturned();
  }
}
