package com.ebs.dda.rest.accounting.paymentrequest.mocks;

import com.ebs.dda.jpa.accounting.paymentrequest.OrderPaymentsGeneralModel;

public class OrderPaymentsGeneralModelMock extends OrderPaymentsGeneralModel {
    private String paymentCode;
    private String orderCode;
    private String orderType;
    private String referenceOrderCode;

    public OrderPaymentsGeneralModelMock withPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
        return this;
    }

    public OrderPaymentsGeneralModelMock withOrderCode(String orderCode) {
        this.orderCode = orderCode;
        return this;
    }

    public OrderPaymentsGeneralModelMock withOrderType(String orderType) {
        this.orderType = orderType;
        return this;
    }

    public OrderPaymentsGeneralModelMock withReferenceOrderCode(String referenceOrderCode) {
        this.referenceOrderCode = referenceOrderCode;
        return this;
    }

    @Override
    public String getPaymentCode() {
        return paymentCode;
    }

    @Override
    public String getOrderCode() {
        return orderCode;
    }

    @Override
    public String getOrderType() {
        return orderType;
    }

    @Override
    public String getReferenceOrderCode() {
        return referenceOrderCode;
    }
}
