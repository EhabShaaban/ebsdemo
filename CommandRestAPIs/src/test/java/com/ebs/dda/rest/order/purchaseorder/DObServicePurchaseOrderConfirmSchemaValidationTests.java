package com.ebs.dda.rest.order.purchaseorder;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.order.purchaseorder.utils.DObServicePurchaseOrderConfirmValidationTestsUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class DObServicePurchaseOrderConfirmSchemaValidationTests {

  private DObServicePurchaseOrderConfirmValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new DObServicePurchaseOrderConfirmValidationTestsUtils();
  }

  @Test
  public void Should_PassSchemaValidation_When_AllFieldsCorrectInValueObject() {
    utils.withConfirmationDateTimeAsString("06-Jan-2019 11:30 AM");

    assertDoesNotThrow(() -> utils.applySchemaValidationOnValueObject());
  }

  //    ConfirmationDateTime Tests

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ConfirmationDateTimeFieldNotValidAsEmpty()
          throws JsonProcessingException {
    utils.withConfirmationDateTimeAsString("");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ConfirmationDateTimeFieldNotValidAsPattern()
          throws JsonProcessingException {
    utils.withConfirmationDateTimeAsString("Ay 7aga");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_ConfirmationDateTimeFieldNotValidAsSpaces()
          throws JsonProcessingException {
    utils.withConfirmationDateTimeAsString("    ");
    utils.convertValueObjectToJson();

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnValueObject());
  }
}
