package com.ebs.dda.rest.accounting.notesreceivable;

import com.ebs.dda.rest.accounting.notesreceivable.utils.IObNotesReceivableRefDocumentValidatorUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.ParseException;

@RunWith(SpringRunner.class)
public class IObNotesReceivableSaveRefDocValidationTests {

  private IObNotesReceivableRefDocumentValidatorUtils utils;

  @Before
  public void init() {
    utils = new IObNotesReceivableRefDocumentValidatorUtils();
  }

  @Test
  public void shouldThrowExceptionWhenAmountToCollectGreaterThanNRAmount()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(12000), new BigDecimal(12000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertErrorDObNotesReceivableAmountToCollectGreaterThanNRAmountIsReturned();
  }

  @Test
  public void shouldNotThrowExceptionWhenAmountToCollectEqualsNRAmount()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withReferenceDocumentData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withReturnedReferenceDocumentData()
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(26000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertErrorDObNotesReceivableAmountToCollectNotEqualsNRAmountIsNotReturned();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentAmountToCollectIsGreaterThanItsRemaining()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000005", "2021000001", new BigDecimal(26000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(26000), new BigDecimal(26000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000005", "12000", "Active", "000001", "0002", "0001")
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsReturned();
  }

  @Test
  public void
      shouldThrowExceptionWhenAmountToCollectGreaterThanNRAmountAndAmountToCollectGreaterThanItsRemaining()
          throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000005", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(12000), new BigDecimal(12000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000005", "11000", "Active", "000001", "0002", "0001");
    utils
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsReturned();
    utils.assertErrorDObNotesReceivableAmountToCollectGreaterThanNRAmountIsReturned();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentAmountToCollectIsLessThanItsRemaining()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000005", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000005", "14000", "Active", "000001", "0002", "0001")
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsNotReturned();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentAmountToCollectIsEqualsItsRemaining()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000005", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000005", "13000", "Active", "000001", "0002", "0001")
        .assertErrorDObNotesReceivableReferenceDocumentAmountToCollectGreaterThanItsRemainingIsNotReturned();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentNRNotInActiveState()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Draft", "000001", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentSONotInGIActivatedOrApprovedState()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesOrderDataReferenceDocument(
            "2021000003",
            new BigDecimal("15000.0"),
            "SalesInvoiceActivated",
            "000001",
            "0002",
            "0001")
        .assertObjectStateNotValidException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentSOINDraftState()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_ORDER")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("15000.0"), "Draft", "000001", "0002", "0001")
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentSINotInActiveState()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Draft", "000001", "0002", "0001")
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentSIInValidState()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentHasNotTheSameBusinessPartnerOfNR()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000002", "0002", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentHasTheSameBusinessPartnerOfNR()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentHasNotTheSameBusinessUnitOfNR()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0001", "0001")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentHasTheSameBusinessUnitOfNR()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentHasNotTheSameCompanyOfNR()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "NOTES_RECEIVABLE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withNotesReceivableReferenceDocument(3L, "2021000003", "Active", "000001", "0002", "0002")
        .withNotesDetails(3L, "2021000003", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }

  @Test
  public void shouldNotThrowExceptionWhenReferenceDocumentHasTheSameCompanyOfNR()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(13000), "SALES_INVOICE")
        .withNotesDetails(1L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .withSalesInvoiceDataReferenceDocument(
            "2021000003", "13000", "Active", "000001", "0002", "0001")
        .assertDoesNotThrowException();
  }

  @Test
  public void shouldThrowExceptionWhenReferenceDocumentIsNotUnique()
      throws ParseException, JsonProcessingException {
    utils
        .withNotesReceivable(1L, "2021000001", "000001", "0002", "0001")
        .withReferenceDocumentData("2021000003", "2021000001", new BigDecimal(10000), "SALES_ORDER")
        .withReturnedReferenceDocumentData()
        .requestToAddReferenceDocumentWithData(
            "2021000003", "2021000001", new BigDecimal(3000), "SALES_ORDER")
        .withSalesOrderDataReferenceDocument(
            "2021000003", new BigDecimal("15000.0"), "Draft", "000001", "0002", "0001")
        .withNotesReceivableReferenceDocument(4L, "2021000001", "Active", "000001", "0002", "0001")
        .withNotesDetails(4L, "2021000001", new BigDecimal(13000), new BigDecimal(13000))
        .assertThrowsArgumentViolationSecurityException();
  }
}
