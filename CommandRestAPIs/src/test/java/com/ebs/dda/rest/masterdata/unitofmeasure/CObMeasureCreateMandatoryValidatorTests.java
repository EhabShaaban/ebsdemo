package com.ebs.dda.rest.masterdata.unitofmeasure;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.masterdata.unitofmeasure.utils.CObMeasureCreateValidationTestsUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CObMeasureCreateMandatoryValidatorTests {

  private CObMeasureCreateValidationTestsUtils utils;

  @Before
  public void init() {
    utils = new CObMeasureCreateValidationTestsUtils();
  }

  @Test
  public void Should_PassValidation_When_AllFieldsExistInValueObject() {
    utils.withUnitOfMeasureName("UOMNameValue").withIsStandard(Boolean.FALSE);

    assertDoesNotThrow(() -> utils.whenValidateCreateUnitOfMeasureValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationException_When_UnitOfMeasureNameFieldIsMissingFromValueObject() {
    utils.withIsStandard(Boolean.FALSE);

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateUnitOfMeasureValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationException_When_IsStandardFieldIsMissingFromValueObject() {
    utils.withUnitOfMeasureName("UOMNameValue");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateUnitOfMeasureValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationException_When_UOMNameFieldIsSpaceInValueObject() {
    utils.withUnitOfMeasureName("  ").withIsStandard(Boolean.FALSE);

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.whenValidateCreateUnitOfMeasureValueObject());
  }
}
