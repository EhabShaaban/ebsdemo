package com.ebs.dda.rest.accounting.vendorinvoice.mocks;

import com.ebs.dda.jpa.masterdata.vendor.IObVendorAccountingInfoGeneralModel;

public class IObVendorAccountingInfoGeneralModelMock extends IObVendorAccountingInfoGeneralModel {

    private String vendorCode;

    public IObVendorAccountingInfoGeneralModelMock withVendorCode (String vendorCode) {
        this.vendorCode = vendorCode;
        return this;
    }

    @Override
    public String getVendorCode() { return vendorCode; }
}
