package com.ebs.dda.rest.accounting.paymentrequest.mocks;

import com.ebs.dda.jpa.accounting.paymentrequest.IObPaymentRequestPaymentDetailsGeneralModel;

import java.math.BigDecimal;

public class IObPaymentRequestPaymentDetailsGeneralModelMock
    extends IObPaymentRequestPaymentDetailsGeneralModel {

  private String userCode;
  private String paymentForm;
  private BigDecimal netAmount;
  private String bankAccountCode;
  private String bankAccountNumber;
  private String treasuryCode;
  private String currencyCode;

  public IObPaymentRequestPaymentDetailsGeneralModelMock withUserCode(String userCode) {
    this.userCode = userCode;
    return this;
  }

  public IObPaymentRequestPaymentDetailsGeneralModelMock withPaymentForm(String paymentForm) {
    this.paymentForm = paymentForm;
    return this;
  }

  public IObPaymentRequestPaymentDetailsGeneralModelMock withNetAmount(BigDecimal netAmount) {
    this.netAmount = netAmount;
    return this;
  }

  public IObPaymentRequestPaymentDetailsGeneralModelMock withCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
    return this;
  }

  public IObPaymentRequestPaymentDetailsGeneralModelMock withBankAccountNumber(
      String bankAccountNumber) {
    this.bankAccountNumber = bankAccountNumber;
    return this;
  }

  public IObPaymentRequestPaymentDetailsGeneralModelMock withTreasuryCode(String treasuryCode) {
    this.treasuryCode = treasuryCode;
    return this;
  }

  public IObPaymentRequestPaymentDetailsGeneralModelMock withBankCode(String bankCode) {
    this.bankAccountCode = bankCode;
    return this;
  }

  @Override
  public String getUserCode() {
    return userCode;
  }

  @Override
  public String getPaymentForm() {
    return paymentForm;
  }

  @Override
  public String getBankAccountNumber() {
    return bankAccountNumber;
  }

  @Override
  public String getTreasuryCode() {
    return treasuryCode;
  }

  @Override
  public String getCurrencyCode() {
    return currencyCode;
  }

  @Override
  public BigDecimal getNetAmount() {
    return netAmount;
  }

  @Override
  public String getBankAccountCode() {
    return bankAccountCode;
  }
}
