package com.ebs.dda.rest.accounting.notesreceivable;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dda.rest.accounting.notesreceivable.utils.IObNotesReceivableAddReferenceDocumentSchemaUtils;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

public class IObNotesReceivableAddReferenceDocumentSchemaTests {

  private IObNotesReceivableAddReferenceDocumentSchemaUtils utils;

  @Before
  public void init() {
    utils = new IObNotesReceivableAddReferenceDocumentSchemaUtils();
  }

  @Test
  public void Should_PassSchemaValidation_When_ValueMatchesWithSchema() {

    utils
        .withDocumentType("SALES_INVOICE")
        .withRefDocumentCode("2021000001")
        .withAmountToCollect("152.014262");

    assertDoesNotThrow(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_TypeValueNotValidAsEnumValues() {
    utils
        .withDocumentType("any Type")
        .withRefDocumentCode("2021000001")
        .withAmountToCollect("152.014262");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_RefDocumentCodeValueNotValidAsGreaterThanMaximumNumberOfDigits() {
    utils
        .withDocumentType("SALES_INVOICE")
        .withRefDocumentCode("20210000010")
        .withAmountToCollect("152.014262");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_RefDocumentCodeValueNotValidAsLessThanMinimumNumberOfDigits() {
    utils
        .withDocumentType("SALES_INVOICE")
        .withRefDocumentCode("202100001")
        .withAmountToCollect("152.014262");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_RefDocumentCodeValueNotValidAsNull() {
    utils
        .withDocumentType("SALES_INVOICE")
        .withRefDocumentCode(null)
        .withAmountToCollect("152.014262");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_RefDocumentCodeValueNotValidAsEmpty() {
    utils
        .withDocumentType("SALES_INVOICE")
        .withRefDocumentCode("")
        .withAmountToCollect("152.014262");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_RefDocumentCodeValueNotValidAsPattern() {
    utils
        .withDocumentType("SALES_INVOICE")
        .withRefDocumentCode("abcdabcdab")
        .withAmountToCollect("152.014262");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void Should_ThrowArgumentViolationSecurityException_When_RefDocumentCodeValueNotValidAsPatternHasNonASCIILetters() {
    utils
        .withDocumentType("SALES_INVOICE")
        .withRefDocumentCode("20210000$1")
        .withAmountToCollect("152.014262");
    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_AmountToCollectValueNotValidAsNumber() {
    utils
        .withDocumentType("SALES_ORDER")
        .withRefDocumentCode("2021000001")
        .withAmountToCollectAsString(" ");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_AmountToCollectValueNotValidAsMinimum() {
    utils
        .withDocumentType("NOTES_RECEIVABLE")
        .withRefDocumentCode("2021000001")
        .withAmountToCollect("-152.014262");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_AmountToCollectValueNotValidAsMaximum() {
    utils
        .withDocumentType("NOTES_RECEIVABLE")
        .withRefDocumentCode("2021000001")
        .withAmountToCollect("1000000001.0");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_AmountToCollectValueNotValidAsMultipleOf() {
    utils
        .withDocumentType("NOTES_RECEIVABLE")
        .withRefDocumentCode("2021000001")
        .withAmountToCollect("152.0142621");

    Assertions.assertThatExceptionOfType(ArgumentViolationSecurityException.class)
        .isThrownBy(() -> utils.applySchemaValidationOnReferenceDocumentValueObject());
  }
}
