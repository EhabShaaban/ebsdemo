package com.ebs.dda.rest.accounting.paymentrequest.utils;

import com.ebs.dac.foundation.realization.statemachine.BasicStateMachine;
import com.ebs.dda.jpa.accounting.paymentrequest.DObPaymentRequestActivateValueObject;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.rest.accounting.paymentrequest.mocks.DObLandedCostGeneralModelMock;
import com.ebs.dda.rest.accounting.paymentrequest.mocks.DObPaymentRequestActivatePreparationGeneralModelMock;
import com.ebs.dda.rest.accounting.paymentrequest.mocks.OrderPaymentsGeneralModelMock;
import com.ebs.entities.accounting.FiscalYearMockUtils;
import com.ebs.repositories.FiscalYearRepMockUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DObPaymentRequestPostValidatorUtils {
    private DObPaymentRequestActivatePreparationGeneralModelMock paymentRequestActivatePreparationGeneralModelMock;
    private DObPaymentRequestActivateValueObject valueObject;
    private OrderPaymentsGeneralModelMock orderPaymentsGeneralModelMock;
    private DObLandedCostGeneralModelMock landedCostGeneralModelMock;

    public DObPaymentRequestPostValidatorUtils withPaymentRequestCode(
            String userCode) {
        this.paymentRequestActivatePreparationGeneralModelMock =
                new DObPaymentRequestActivatePreparationGeneralModelMock()
                        .withUserCode(userCode);
        this.orderPaymentsGeneralModelMock = new OrderPaymentsGeneralModelMock().withPaymentCode(userCode);
        this.valueObject = new DObPaymentRequestActivateValueObject();
        this.valueObject.setPaymentRequestCode(userCode);
        return this;
    }

    public DObPaymentRequestPostValidatorUtils withDueDate(DateTime date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd-MMM-yyyy hh:mm a");
        this.valueObject.setDueDate(date.toString(formatter));
        return this;
    }

    public DObPaymentRequestPostValidatorUtils withCurrencyPrice(BigDecimal currencyPrice) {
        this.paymentRequestActivatePreparationGeneralModelMock = this.paymentRequestActivatePreparationGeneralModelMock.withCurrencyPrice(currencyPrice);
        return this;
    }

    public DObPaymentRequestPostValidatorUtils withExchangeRateCode(String exchangeRateCode) {
        this.paymentRequestActivatePreparationGeneralModelMock = this.paymentRequestActivatePreparationGeneralModelMock.withExchangeRate(exchangeRateCode);
        return this;
    }

    public DObPaymentRequestPostValidatorUtils withPaymentRequestType(
            String paymentType) {
        this.paymentRequestActivatePreparationGeneralModelMock =
                this.paymentRequestActivatePreparationGeneralModelMock
                        .withPaymentType(paymentType);
        return this;
    }

    public DObPaymentRequestPostValidatorUtils withPaymentRequestCompany(
            String companyCode, String businessUnitCode) {
        this.paymentRequestActivatePreparationGeneralModelMock =
                this.paymentRequestActivatePreparationGeneralModelMock
                        .withCompanyCode(companyCode)
                        .withBusinessUnitCode(businessUnitCode);
        return this;
    }

    public DObPaymentRequestPostValidatorUtils withPaymentRequestPaymentDetails(
            String currencyCode,
            BigDecimal amount,
            String bankCode,
            String bankAccountNumber,
            String treasuryCode) {

        this.paymentRequestActivatePreparationGeneralModelMock =
                this.paymentRequestActivatePreparationGeneralModelMock
                        .withCurrencyCode(currencyCode)
                        .withNetAmount(amount)
                        .withBankCode(bankCode)
                        .withBankAccountNumber(bankAccountNumber)
                        .withTreasuryCode(treasuryCode);
        return this;
    }

    public DObPaymentRequestPostValidatorUtils withJournalBalance(BigDecimal balance) {
        this.paymentRequestActivatePreparationGeneralModelMock =
                this.paymentRequestActivatePreparationGeneralModelMock
                        .withBalance(balance);
        return this;
    }

    public void withLandedCost(String landedCostCode) {
        landedCostGeneralModelMock = new DObLandedCostGeneralModelMock().withUserCode(landedCostCode);
    }
    public DObPaymentRequestPostValidatorUtils withOrder(String orderType, String orderCode, String referenceOrderCode) {
        this.orderPaymentsGeneralModelMock.withOrderType(orderType).withOrderCode(orderCode).withReferenceOrderCode(referenceOrderCode);
        return this;
    }

    public DObPaymentRequestActivatePreparationGeneralModelMock getPaymentRequestActivatePreparationGeneralModelMock() {
        return paymentRequestActivatePreparationGeneralModelMock;
    }


    public OrderPaymentsGeneralModelMock getOrderPaymentsGeneralModelMock() {
        return orderPaymentsGeneralModelMock;
    }

    public DObLandedCostGeneralModelMock getLandedCostGeneralModelMock() {
        return landedCostGeneralModelMock;
    }

    public DObPaymentRequestActivateValueObject getValueObject() {
        return valueObject;
    }

    public CObFiscalYearRep prepareFiscalYearRep() {
      FiscalYearRepMockUtils.mockFindActiveFiscalYears(
        FiscalYearMockUtils.mockEntity()
          .id(1L)
          .currentState("%"+ BasicStateMachine.ACTIVE_STATE + "%")
          .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
          .toDate(new DateTime(getDateTimeInMilliSecondsFormat("31-DEC-2021 11:59 PM")))
          .build()
      );

      return FiscalYearRepMockUtils.getRepository();
    }

  public Long getDateTimeInMilliSecondsFormat(String dateTime) {
      SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
      Date date = null;
    try {
      date = dateFormat.parse(dateTime);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date.getTime();
  }
}
