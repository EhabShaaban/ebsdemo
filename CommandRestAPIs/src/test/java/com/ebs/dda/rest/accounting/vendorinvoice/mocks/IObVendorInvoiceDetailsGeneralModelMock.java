package com.ebs.dda.rest.accounting.vendorinvoice.mocks;

import com.ebs.dda.jpa.accounting.vendorinvoice.IObVendorInvoiceDetailsGeneralModel;

public class IObVendorInvoiceDetailsGeneralModelMock extends IObVendorInvoiceDetailsGeneralModel {

    private String invoiceCode;
    private String purchaseOrderCode;
    private String vendorCode;

    public IObVendorInvoiceDetailsGeneralModelMock withUserCode(String userCode) {
        this.invoiceCode = userCode;
        return this;
    }

    public IObVendorInvoiceDetailsGeneralModelMock withPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
        return this;
    }

    public IObVendorInvoiceDetailsGeneralModelMock withVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
        return this;
    }

    @Override
    public String getInvoiceCode() { return invoiceCode; }

    @Override
    public String getPurchaseOrderCode() { return purchaseOrderCode; }

    @Override
    public String getVendorCode() { return vendorCode; }
}
