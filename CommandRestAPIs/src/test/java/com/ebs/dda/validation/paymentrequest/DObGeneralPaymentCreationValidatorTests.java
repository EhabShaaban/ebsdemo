package com.ebs.dda.validation.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.validation.paymentrequest.utils.DObGeneralPaymentCreationValidatorTestsUtils;
import org.junit.Before;
import org.junit.Test;

public class DObGeneralPaymentCreationValidatorTests {

  private DObGeneralPaymentCreationValidatorTestsUtils utils;

  @Before
  public void setUp() {
    utils = new DObGeneralPaymentCreationValidatorTestsUtils();
  }

  @Test
  public void Should_Pass_WhenAllDataIsCorrect() throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, 1L, "0002")
        .validateSuccessfully();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDocumentOwnerIsMissing() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(null)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, 1L, "0002")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDocumentOwnerDoesNotExists() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1000L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, 1L, "0002")
        .whenDocumentOwnerDoesNotExist()
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenBusinessUnitDoesNotExists() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("9999")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, 1L, "0002")
        .whenBusinessUnitDoesNotExist()
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenBusinessPartnerNotNull() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner("000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, 1L, "0002")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void
      Should_throwsArgumentViolationSecurityException_WhenDueDocumentTypeNotPurchaseOrder() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, 1L, "0002")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDueDocumentCodeIsMissing() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument(null, "0001", "000001", "Shipped", 1L, 1L, "0002")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDueDocumentDoesNotExists()
      throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021999999", "0001", "000001", "Shipped", 1L, 1L, "0002")
        .whenDueDocumentDoesNotExist()
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void
      Should_throwsArgumentViolationSecurityException_WhenPurchaseOrderHasNotTheSelectedBusinessUnit()
          throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0002", "000001", "Shipped", 1L, 1L, "0002")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenPurchaseOrderInDraftState()
      throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Draft", 1L, 1L, "0002")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void
      Should_Error_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_IsReturned_WhenPurchaseOrderHasInvalidStateState()
          throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "WaitingApproval", 1L, 1L, "0002")
        .assertError_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_IsReturned();
  }

  @Test
  public void
      Should_Error_INVOICE_PO_HAS_NO_COMPANY_IsReturned_WhenPurchaseOrderCompanyDoesNotExists()
          throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, 9999L, "9999")
        .whenDueDocumentCompanyDoesNotExist()
        .assertError_INVOICE_PO_HAS_NO_COMPANY_IsReturned();
  }

  @Test
  public void Should_Error_INVOICE_PO_HAS_NO_COMPANY_IsReturned_WhenPurchaseOrderCompanyIsMissing()
      throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.OTHER_PARTY_FOR_PURCHASE)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.PURCHASEORDER)
        .withDueDocument("2021000001", "0001", "000001", "Shipped", 1L, null, null)
        .assertError_INVOICE_PO_HAS_NO_COMPANY_IsReturned();
  }
}
