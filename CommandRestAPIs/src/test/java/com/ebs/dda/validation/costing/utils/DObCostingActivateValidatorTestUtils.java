package com.ebs.dda.validation.costing.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.costing.exceptions.DObCostingHasNoActualLandedCostException;
import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.costing.DObCostingActivateValueObject;
import com.ebs.dda.jpa.accounting.costing.DObCostingGeneralModel;
import com.ebs.dda.jpa.accounting.costing.IObCostingDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.landedcost.DObLandedCostGeneralModel;
import com.ebs.dda.validation.costing.DObCostingActivateValidator;
import com.ebs.entities.LandedCostGeneralModelMockUtils;
import com.ebs.entities.accounting.FiscalYearMockUtils;
import com.ebs.entities.accounting.costing.CostingDetailsMockUtils;
import com.ebs.entities.accounting.costing.CostingMockUtils;
import com.ebs.repositories.FiscalYearRepMockUtils;
import com.ebs.repositories.LandedCostGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingDetailsGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingGeneralModelRepMockUtils;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObCostingActivateValidatorTestUtils {
    private ValidationResult validationResult;
    private DObCostingGeneralModel costingGeneralModel;

    private DObCostingActivateValidator validator;
    private DObCostingActivateValueObject costingActivateValueObject;
    String activeState = "Active";

    public DObCostingActivateValidatorTestUtils() {
        validator = new DObCostingActivateValidator();
        costingActivateValueObject = new DObCostingActivateValueObject();

        validator.setCostingGeneralModelRep(CostingGeneralModelRepMockUtils.getRepository());
        validator.setLandedCostGeneralModelRep(LandedCostGeneralModelRepMockUtils.getRepository());
        validator.setFiscalYearRep(FiscalYearRepMockUtils.getRepository());
        validator.setCostingDetailsGeneralModelRep(CostingDetailsGeneralModelRepMockUtils.getRepository());
    }

    public DObCostingActivateValidatorTestUtils whenFiscalYearIsActive(String costingCode, String dueDate) throws ParseException {
        costingActivateValueObject.setUserCode(costingCode);
        costingActivateValueObject.setDueDate(dueDate);

        costingGeneralModel =
                CostingMockUtils.mockGeneralModel()
                        .userCode(costingCode)
                        .build();

        CostingGeneralModelRepMockUtils.findOneByUserCode(costingGeneralModel);

        CObFiscalYear fiscalYear =
                FiscalYearMockUtils.mockEntity()
                        .id(1L)
                        .currentState(activeState)
                        .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
                        .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
                        .build();

        FiscalYearRepMockUtils.mockFindActiveFiscalYears(fiscalYear);
        return this;
    }


    public DObCostingActivateValidatorTestUtils whenCostingDocumentNotExists(String costingCode, String dueDate) {
        costingActivateValueObject.setUserCode(costingCode);
        costingActivateValueObject.setDueDate(dueDate);
        CostingDetailsGeneralModelRepMockUtils.findOneByUserCode(null);
        return this;
    }

    public DObCostingActivateValidatorTestUtils whenLandedCostStateIsEstimate(String costingCode, String dueDate) throws ParseException {
        costingActivateValueObject.setUserCode(costingCode);
        costingActivateValueObject.setDueDate(dueDate);

        costingGeneralModel =
                CostingMockUtils.mockGeneralModel()
                        .userCode(costingCode)
                        .build();

        CostingGeneralModelRepMockUtils.findOneByUserCode(costingGeneralModel);

        CObFiscalYear fiscalYear =
                FiscalYearMockUtils.mockEntity()
                        .id(1L)
                        .currentState(activeState)
                        .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
                        .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
                        .build();

        FiscalYearRepMockUtils.mockFindActiveFiscalYears(fiscalYear);

        IObCostingDetailsGeneralModel costingDetailsGeneralModel =
                CostingDetailsMockUtils.mockGeneralModel()
                        .userCode(costingCode)
                        .purchaseOrderCode("2021000004")
                        .build();

        CostingDetailsGeneralModelRepMockUtils.findOneByUserCode(costingDetailsGeneralModel);

        DObLandedCostGeneralModel landedCostGeneralModel =
                LandedCostGeneralModelMockUtils.mockGeneralModel()
                        .purchaseOrderCode("2021000004")
                        .state(DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED)
                        .build();

        LandedCostGeneralModelRepMockUtils.findByPurchaseOrderCodeAndCurrentStatesLike(landedCostGeneralModel, DObLandedCostStateMachine.ESTIMATED_VALUE_COMPLETED);
        return this;
    }

    public void assertThrowsArgumentSecurityException() {
        assertThrows(
                ArgumentViolationSecurityException.class,
                () -> {
                    validator.validateBusinessRules(costingActivateValueObject);
                });
    }

    public void assertThrowsCostingHasNoActualLandedCostException() {
        assertThrows(
                DObCostingHasNoActualLandedCostException.class,
                () -> {
                    validator.validateBusinessRules(costingActivateValueObject);
                });
    }

    public Long getDateTimeInMilliSecondsFormat(String dateTime) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date = dateFormat.parse(dateTime);
        return date.getTime();
    }
}
