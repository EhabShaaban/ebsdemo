package com.ebs.dda.validation.settlement;

import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.validation.settlement.utils.DObSettlementActivationValidatorTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@RunWith(JUnitPlatform.class)
public class DObSettlementActivationValidatorTests {

  private DObSettlementActivationValidatorTestUtils utils;
  private Map<String, String> missingValuesMap =
      new HashMap<String, String>() {
        {
          put("Null Value", null);
          put("Empty String", " ");
        }
      };
  private HashMap<String, List<IObSettlementAccountDetailsGeneralModel>> missingValuesListMap =
      new HashMap<String, List<IObSettlementAccountDetailsGeneralModel>>() {
        {
          put("Null Value", null);
          put("Empty String", new ArrayList<>());
        }
      };

  @BeforeEach
  public void init() {
    utils = new DObSettlementActivationValidatorTestUtils();
  }

  @TestFactory
  public Stream<DynamicTest> Should_ReturnErrorBindToDetailsSection_When_CurrencyIsMissing() {
    return missingValuesMap.keySet().stream()
        .map(
            key ->
                DynamicTest.dynamicTest(
                    key,
                    () -> {
                      String missingValue = missingValuesMap.get(key);
                      utils
                          .withDocumentCode("202100009")
                          .withCurrencyCode(missingValue)
                          .withAccountingDetails(
                              utils.getAccountingDetailsThatAreEquivalent(
                                  "202100009", BigDecimal.valueOf(20D), BigDecimal.valueOf(20D)))
                          .withJournalEntryDate("17-Jun-2021 11:00 AM")
                          .whenSectionFieldIsMissing()
                          .assertThatSectionFieldAreMissing("SettlementDetails", "currencyISO");
                    }));
  }

  @TestFactory
  public Stream<DynamicTest>
      Should_ReturnErrorBindToDetailsSection_When_AccountingDetailsAreMissing() {
    return missingValuesListMap.keySet().stream()
        .map(
            key ->
                DynamicTest.dynamicTest(
                    key,
                    () -> {
                      List<IObSettlementAccountDetailsGeneralModel> missingValuesMap =
                          missingValuesListMap.get(key);
                      utils
                          .withDocumentCode("202100009")
                          .withCurrencyCode("0002")
                          .withCurrencyIso("USD")
                          .withAccountingDetails(missingValuesMap)
                          .withJournalEntryDate("17-Jun-2021 11:00 AM")
                          .whenSectionFieldIsMissing()
                          .assertThatSectionFieldAreMissing(
                              "AccountingDetails", "AccountingDetails");
                    }));
  }

  @Test
  public void Should_ReturnErrorCode_When_CreditAndDebitAreNotEquivalent() throws ParseException {
    utils
        .withDocumentCode("202100009")
        .withCurrencyCode("0002")
        .withCurrencyIso("USD")
        .withAccountingDetails(
            utils.getAccountingDetailsThatAreNotEquivalent(
                "202100009",
                BigDecimal.valueOf(30D),
                BigDecimal.valueOf(20D),
                BigDecimal.valueOf(20D)))
        .withJournalEntryDate("17-Jun-2021 11:00 AM")
        .whenSectionFieldIsMissing()
        .assertThatErrorCode_SETTLEMENTS_ACCOUNTING_DETAILS_NOT_EQUALS_IsReturned();
  }

  @Test
  public void ShouldReturnErrorCodeWhenSettlementsAmountGreaterThanBalance() throws ParseException {
    utils
        .withDocumentCode("202100009")
        .withCurrencyCode("0002")
        .withCurrencyIso("USD")
        .withAccountingDetails(
            utils.getAccountingDetailsThatAreEquivalent(
                "202100009", BigDecimal.valueOf(20D), BigDecimal.valueOf(10D)))
        .withJournalEntryDate("17-Jun-2021 11:00 AM")
        .whenSectionFieldIsMissing()
        .assertThatErrorCode_SETTLEMENTS_AMOUNT_GREATER_THAN_BALANCE_IsReturned();
  }

  @Test
  public void ShouldNotReturnErrorCodeWhenSettlementsAmountLessThanBalance() throws ParseException {
    utils
        .withDocumentCode("202100009")
        .withCurrencyCode("0002")
        .withCurrencyIso("USD")
        .withAccountingDetails(
            utils.getAccountingDetailsThatAreEquivalent(
                "202100009", BigDecimal.valueOf(20D), BigDecimal.valueOf(30D)))
        .withJournalEntryDate("17-Jun-2021 11:00 AM")
        .whenSectionFieldIsMissing()
        .assertThatErrorCode_SETTLEMENTS_AMOUNT_GREATER_THAN_BALANCE_IsNotReturned();
  }

  @Test
  public void ShouldNotReturnErrorCodeWhenSettlementsAmountEqualsBalance() throws ParseException {
    utils
        .withDocumentCode("202100009")
        .withCurrencyCode("0002")
        .withCurrencyIso("USD")
        .withAccountingDetails(
            utils.getAccountingDetailsThatAreEquivalent(
                "202100009", BigDecimal.valueOf(20D), BigDecimal.valueOf(20D)))
        .withJournalEntryDate("17-Jun-2021 11:00 AM")
        .whenSectionFieldIsMissing()
        .assertThatErrorCode_SETTLEMENTS_AMOUNT_GREATER_THAN_BALANCE_IsNotReturned();
  }

  @Test
  public void
      Should_ThrowArgumentViolationSecurityException_When_JournalEntryDateIsNotInActiveFiscalYear()
          throws ParseException {
    utils
        .withDocumentCode("202100009")
        .withCurrencyCode("0002")
        .withAccountingDetails(
            utils.getAccountingDetailsThatAreEquivalent(
                "202100009", BigDecimal.valueOf(20D), BigDecimal.valueOf(20D)))
        .withJournalEntryDate("17-Jun-2020 11:00 AM");
    utils.assertThrowsArgumentViolationSecurityException();
  }
}
