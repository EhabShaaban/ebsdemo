package com.ebs.dda.validation.paymentrequest.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.DObPurchaseOrderGeneralModel;
import com.ebs.dda.purchases.dbo.jpa.entities.generalmodels.IObOrderCompanyGeneralModel;
import com.ebs.dda.validation.paymentrequest.DObDownPaymentCreationValidator;
import com.ebs.entities.DocumentOwnerTestUtils;
import com.ebs.entities.masterdata.businessunit.BusinessUnitTestUtils;
import com.ebs.entities.masterdata.vendor.MObVendorPurchaseUnitGeneralModelMockUtils;
import com.ebs.entities.masterdata.vendor.VendorMockUtils;
import com.ebs.entities.purchases.OrderCompanyMockUtils;
import com.ebs.entities.purchases.PurchaseOrderMockUtils;
import com.ebs.repositories.DocumentOwnerRepTestUtils;
import com.ebs.repositories.masterdata.businessunit.BusinessUnitRepTestUtils;
import com.ebs.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRepMockUtils;
import com.ebs.repositories.masterdata.vendor.VendorGeneralModelRepTestUtils;
import com.ebs.repositories.purchases.DObPurchaseOrderGeneralModelRepMockUtils;
import com.ebs.repositories.purchases.OrderCompanyGeneralModelRepTestUtils;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObDownPaymentCreationValidatorTestsUtils {

  private DObDownPaymentCreationValidator validator;
  private DObPaymentRequestCreateValueObject valueObject;

  public DObDownPaymentCreationValidatorTestsUtils() {
    valueObject = new DObPaymentRequestCreateValueObject();
    validator = new DObDownPaymentCreationValidator();
  }

  public DObDownPaymentCreationValidatorTestsUtils withPaymentType(
      PaymentTypeEnum.PaymentType paymentType) {
    valueObject.setPaymentType(paymentType);
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils withPaymentForm(
      PaymentFormEnum.PaymentForm paymentForm) {
    valueObject.setPaymentForm(paymentForm);
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils withDocumentOwnerId(Long documentOwnerId) {
    valueObject.setDocumentOwnerId(documentOwnerId);
    prepareDocumentOwner();
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils withBusinessUnitCode(String businessUnitCode) {
    valueObject.setBusinessUnitCode(businessUnitCode);
    prepareBusinessUnit();
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils withBusinessPartner(
      Long businessPartnerId, String businessPartnerCode) {
    valueObject.setBusinessPartnerCode(businessPartnerCode);
    prepareBusinessPartner(businessPartnerId);
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils withDueDocumentType(
      DueDocumentTypeEnum.DueDocumentType dueDocumentType) {
    valueObject.setDueDocumentType(dueDocumentType);
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils withDueDocument(
      String dueDocumentCode,
      String dueDocumentPurchaseUnitCode,
      String dueDocumentVendorCode,
      String state,
      Long dueDocumentId,
      Long dueDocumentCompanyId,
      String dueDocumentCompanyCode) {
    valueObject.setDueDocumentCode(dueDocumentCode);
    prepareDueDocument(
        dueDocumentPurchaseUnitCode,
        dueDocumentVendorCode,
        state,
        dueDocumentId,
        dueDocumentCompanyId,
        dueDocumentCompanyCode);
    return this;
  }

  private ValidationResult executeValidate() throws Exception {
    return validator.validate(valueObject);
  }

  public void throwsArgumentViolationSecurityException() {
    assertThrows(ArgumentViolationSecurityException.class, this::executeValidate);
  }

  public void throwsAuthorizationException() {
    assertThrows(AuthorizationException.class, this::executeValidate);
  }

  public void validateSuccessfully() throws Exception {
    ValidationResult validationResult = executeValidate();
    org.junit.Assert.assertTrue(validationResult.isValid());
  }

  public void assertErrorIncorrectFieldInputIsReturned() throws Exception {
    ValidationResult validationResult = executeValidate();

    String businessPartnerErrorCode =
        validationResult.getErrorCode(IDObPaymentRequestCreateValueObject.BUSINESSPARTNER_CODE);
    Assertions.assertEquals(businessPartnerErrorCode, "Gen-msg-46");
  }

  public void assertError_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_IsReturned()
      throws Exception {
    ValidationResult validationResult = executeValidate();

    String dueDocumentErrorCode =
        validationResult.getErrorCode(IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_CODE);
    Assertions.assertEquals(dueDocumentErrorCode, "Gen-msg-32");
  }

  public void assertError_INVOICE_PO_HAS_NO_COMPANY_IsReturned() throws Exception {
    ValidationResult validationResult = executeValidate();

    String dueDocumentErrorCode =
        validationResult.getErrorCode(IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_CODE);
    Assertions.assertEquals(dueDocumentErrorCode, "Inv-msg-02");
  }

  private void prepareDocumentOwner() {
    DocumentOwnerGeneralModel documentOwner =
        DocumentOwnerTestUtils.mockGeneralModel().userId(valueObject.getDocumentOwnerId()).build();

    DocumentOwnerRepTestUtils.mockFindByUserIdAndObjectName(
        documentOwner, valueObject.getDocumentOwnerId(), IDObPaymentRequest.SYS_NAME);

    validator.setDocumentOwnerGeneralModelRep(DocumentOwnerRepTestUtils.getRepository());
  }

  public DObDownPaymentCreationValidatorTestsUtils whenDocumentOwnerDoesNotExist() {
    DocumentOwnerRepTestUtils.mockFindByUserIdAndObjectName(
        null, valueObject.getDocumentOwnerId(), IDObPaymentRequest.SYS_NAME);
    return this;
  }

  private void prepareBusinessUnit() {
    CObPurchasingUnitGeneralModel businessUnit =
        BusinessUnitTestUtils.newGeneralModel().userCode(valueObject.getBusinessUnitCode()).build();

    BusinessUnitRepTestUtils.mockFindBusinessUnitByCode(
        businessUnit, valueObject.getBusinessUnitCode());
    validator.setPurchasingUnitRep(BusinessUnitRepTestUtils.getRepository());
  }

  public DObDownPaymentCreationValidatorTestsUtils whenBusinessUnitDoesNotExist() {
    BusinessUnitRepTestUtils.mockFindBusinessUnitByCode(null, valueObject.getBusinessUnitCode());
    return this;
  }

  private void prepareBusinessPartner(Long businessPartnerId) {
    MObVendorGeneralModel vendorGeneralModel =
        VendorMockUtils.mockGeneralModel().id(businessPartnerId).build();
    MObVendorPurchaseUnitGeneralModel vendorPurchaseUnitGeneralModel =
        MObVendorPurchaseUnitGeneralModelMockUtils.mockGeneralModel().id(businessPartnerId).build();

    VendorGeneralModelRepTestUtils.findOneByUserCode(
        vendorGeneralModel, valueObject.getBusinessPartnerCode());
    MObVendorPurchaseUnitGeneralModelRepMockUtils.findOneByUserCodeAndPurchaseUnitCode(
        vendorPurchaseUnitGeneralModel,
        valueObject.getBusinessPartnerCode(),
        valueObject.getBusinessUnitCode());
    validator.setVendorGeneralModelRep(VendorGeneralModelRepTestUtils.getRepository());
    validator.setVendorPurchaseUnitGeneralModelRep(
        MObVendorPurchaseUnitGeneralModelRepMockUtils.getRepository());
  }

  public DObDownPaymentCreationValidatorTestsUtils whenBusinessPartnerDoesNotExist() {
    VendorGeneralModelRepTestUtils.findOneByUserCode(null, valueObject.getBusinessPartnerCode());
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils whenBusinessPartnerHasNotBusinessUnitAllowed() {
    MObVendorPurchaseUnitGeneralModelRepMockUtils.findOneByUserCodeAndPurchaseUnitCode(
        null, valueObject.getBusinessPartnerCode(), valueObject.getBusinessUnitCode());
    return this;
  }

  private void prepareDueDocument(
      String dueDocumentPurchaseUnitCode,
      String dueDocumentVendorCode,
      String state,
      Long dueDocumentId,
      Long dueDocumentCompanyId,
      String dueDocumentCompanyCode) {
    DObPurchaseOrderGeneralModel purchaseOrderGeneralModel =
        PurchaseOrderMockUtils.newGeneralModel()
            .userCode(valueObject.getDueDocumentCode())
            .purchaseUnitCode(dueDocumentPurchaseUnitCode)
            .vendorCode(dueDocumentVendorCode)
            .state(state)
            .build();

    DObPurchaseOrderGeneralModelRepMockUtils.findOneByUserCode(
        purchaseOrderGeneralModel, valueObject.getDueDocumentCode());
    validator.setPurchaseOrderRep(DObPurchaseOrderGeneralModelRepMockUtils.getRepository());

    IObOrderCompanyGeneralModel orderCompanyGeneralModel =
        OrderCompanyMockUtils.mockGeneralModel()
            .purchaseUnitId(dueDocumentId)
            .companyId(dueDocumentCompanyId)
            .companyCode(dueDocumentCompanyCode)
            .build();

    OrderCompanyGeneralModelRepTestUtils.findOneByUserCode(
        orderCompanyGeneralModel, valueObject.getDueDocumentCode());
    validator.setPoComapnyGMRep(OrderCompanyGeneralModelRepTestUtils.getRepository());
  }

  public DObDownPaymentCreationValidatorTestsUtils whenDueDocumentDoesNotExist() {
    DObPurchaseOrderGeneralModelRepMockUtils.findOneByUserCode(
        null, valueObject.getDueDocumentCode());
    return this;
  }

  public DObDownPaymentCreationValidatorTestsUtils whenDueDocumentCompanyDoesNotExist() {
    OrderCompanyGeneralModelRepTestUtils.findOneByUserCode(null, valueObject.getDueDocumentCode());
    return this;
  }
}
