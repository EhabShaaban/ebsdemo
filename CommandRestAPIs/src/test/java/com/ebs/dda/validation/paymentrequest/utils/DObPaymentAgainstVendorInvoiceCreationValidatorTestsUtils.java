package com.ebs.dda.validation.paymentrequest.utils;

import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dac.security.exceptions.AuthorizationException;
import com.ebs.dda.jpa.DocumentOwnerGeneralModel;
import com.ebs.dda.jpa.accounting.paymentrequest.*;
import com.ebs.dda.jpa.accounting.vendorinvoice.DObVendorInvoiceGeneralModel;
import com.ebs.dda.jpa.masterdata.businessunit.CObPurchasingUnitGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorGeneralModel;
import com.ebs.dda.jpa.masterdata.vendor.MObVendorPurchaseUnitGeneralModel;
import com.ebs.dda.validation.paymentrequest.DObPaymentAgainstVendorInvoiceCreationValidator;
import com.ebs.entities.DocumentOwnerTestUtils;
import com.ebs.entities.VendorInvoiceUtils;
import com.ebs.entities.masterdata.businessunit.BusinessUnitTestUtils;
import com.ebs.entities.masterdata.vendor.MObVendorPurchaseUnitGeneralModelMockUtils;
import com.ebs.entities.masterdata.vendor.VendorMockUtils;
import com.ebs.repositories.DocumentOwnerRepTestUtils;
import com.ebs.repositories.VendorInvoiceGeneralModelRepMockUtils;
import com.ebs.repositories.masterdata.businessunit.BusinessUnitRepTestUtils;
import com.ebs.repositories.masterdata.vendor.MObVendorPurchaseUnitGeneralModelRepMockUtils;
import com.ebs.repositories.masterdata.vendor.VendorGeneralModelRepTestUtils;
import com.ebs.repositories.purchases.OrderCompanyGeneralModelRepTestUtils;
import org.junit.jupiter.api.Assertions;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils {

  private DObPaymentAgainstVendorInvoiceCreationValidator validator;
  private DObPaymentRequestCreateValueObject valueObject;

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils() {
    valueObject = new DObPaymentRequestCreateValueObject();
    validator = new DObPaymentAgainstVendorInvoiceCreationValidator();
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils withPaymentType(
      PaymentTypeEnum.PaymentType paymentType) {
    valueObject.setPaymentType(paymentType);
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils withPaymentForm(
      PaymentFormEnum.PaymentForm paymentForm) {
    valueObject.setPaymentForm(paymentForm);
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils withDocumentOwnerId(
      Long documentOwnerId) {
    valueObject.setDocumentOwnerId(documentOwnerId);
    prepareDocumentOwner();
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils withBusinessUnitCode(
      String businessUnitCode) {
    valueObject.setBusinessUnitCode(businessUnitCode);
    prepareBusinessUnit();
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils withBusinessPartner(
      Long businessPartnerId, String businessPartnerCode) {
    valueObject.setBusinessPartnerCode(businessPartnerCode);
    prepareBusinessPartner(businessPartnerId);
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils withDueDocumentType(
      DueDocumentTypeEnum.DueDocumentType dueDocumentType) {
    valueObject.setDueDocumentType(dueDocumentType);
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils withDueDocument(
      String dueDocumentCode,
      String dueDocumentPurchaseUnitCode,
      String dueDocumentVendorCode,
      String state) {
    valueObject.setDueDocumentCode(dueDocumentCode);
    prepareDueDocument(dueDocumentPurchaseUnitCode, dueDocumentVendorCode, state);
    return this;
  }

  private ValidationResult executeValidate() throws Exception {
    return validator.validate(valueObject);
  }

  public void throwsArgumentViolationSecurityException() {
    assertThrows(ArgumentViolationSecurityException.class, this::executeValidate);
  }

  public void throwsAuthorizationException() {
    assertThrows(AuthorizationException.class, this::executeValidate);
  }

  public void validateSuccessfully() throws Exception {
    ValidationResult validationResult = executeValidate();
    org.junit.Assert.assertTrue(validationResult.isValid());
  }

  public void assertErrorIncorrectFieldInputIsReturned() throws Exception {
    ValidationResult validationResult = executeValidate();

    String businessPartnerErrorCode =
        validationResult.getErrorCode(IDObPaymentRequestCreateValueObject.BUSINESSPARTNER_CODE);
    Assertions.assertEquals(businessPartnerErrorCode, "Gen-msg-46");
  }

  public void assertError_General_MESSAGE_ACTION_NOT_ALLOWED_PER_STATE_IsReturned()
      throws Exception {
    ValidationResult validationResult = executeValidate();

    String dueDocumentErrorCode =
        validationResult.getErrorCode(IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_CODE);
    Assertions.assertEquals(dueDocumentErrorCode, "Gen-msg-32");
  }

  public void assertError_INVOICE_PO_HAS_NO_COMPANY_IsReturned() throws Exception {
    ValidationResult validationResult = executeValidate();

    String dueDocumentErrorCode =
        validationResult.getErrorCode(IDObPaymentRequestCreateValueObject.DUE_DOCUMENT_CODE);
    Assertions.assertEquals(dueDocumentErrorCode, "Inv-msg-02");
  }

  private void prepareDocumentOwner() {
    DocumentOwnerGeneralModel documentOwner =
        DocumentOwnerTestUtils.mockGeneralModel().userId(valueObject.getDocumentOwnerId()).build();

    DocumentOwnerRepTestUtils.mockFindByUserIdAndObjectName(
        documentOwner, valueObject.getDocumentOwnerId(), IDObPaymentRequest.SYS_NAME);

    validator.setDocumentOwnerGeneralModelRep(DocumentOwnerRepTestUtils.getRepository());
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils whenDocumentOwnerDoesNotExist() {
    DocumentOwnerRepTestUtils.mockFindByUserIdAndObjectName(
        null, valueObject.getDocumentOwnerId(), IDObPaymentRequest.SYS_NAME);
    return this;
  }

  private void prepareBusinessUnit() {
    CObPurchasingUnitGeneralModel businessUnit =
        BusinessUnitTestUtils.newGeneralModel().userCode(valueObject.getBusinessUnitCode()).build();

    BusinessUnitRepTestUtils.mockFindBusinessUnitByCode(
        businessUnit, valueObject.getBusinessUnitCode());
    validator.setPurchasingUnitRep(BusinessUnitRepTestUtils.getRepository());
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils whenBusinessUnitDoesNotExist() {
    BusinessUnitRepTestUtils.mockFindBusinessUnitByCode(null, valueObject.getBusinessUnitCode());
    return this;
  }

  private void prepareBusinessPartner(Long businessPartnerId) {
    MObVendorGeneralModel vendorGeneralModel =
        VendorMockUtils.mockGeneralModel().id(businessPartnerId).build();
    MObVendorPurchaseUnitGeneralModel vendorPurchaseUnitGeneralModel =
        MObVendorPurchaseUnitGeneralModelMockUtils.mockGeneralModel().id(businessPartnerId).build();

    VendorGeneralModelRepTestUtils.findOneByUserCode(
        vendorGeneralModel, valueObject.getBusinessPartnerCode());
    MObVendorPurchaseUnitGeneralModelRepMockUtils.findOneByUserCodeAndPurchaseUnitCode(
        vendorPurchaseUnitGeneralModel,
        valueObject.getBusinessPartnerCode(),
        valueObject.getBusinessUnitCode());
    validator.setVendorGeneralModelRep(VendorGeneralModelRepTestUtils.getRepository());
    validator.setVendorPurchaseUnitGeneralModelRep(
        MObVendorPurchaseUnitGeneralModelRepMockUtils.getRepository());
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils
      whenBusinessPartnerDoesNotExist() {
    VendorGeneralModelRepTestUtils.findOneByUserCode(null, valueObject.getBusinessPartnerCode());
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils
      whenBusinessPartnerHasNotBusinessUnitAllowed() {
    MObVendorPurchaseUnitGeneralModelRepMockUtils.findOneByUserCodeAndPurchaseUnitCode(
        null, valueObject.getBusinessPartnerCode(), valueObject.getBusinessUnitCode());
    return this;
  }

  private void prepareDueDocument(
      String dueDocumentPurchaseUnitCode, String dueDocumentVendorCode, String state) {
    DObVendorInvoiceGeneralModel vendorInvoiceGeneralModel =
        VendorInvoiceUtils.newGeneralModel()
            .userCode(valueObject.getDueDocumentCode())
            .vendorCode(dueDocumentVendorCode)
            .purchaseUnitCode(dueDocumentPurchaseUnitCode)
            .state(state)
            .build();

    VendorInvoiceGeneralModelRepMockUtils.findOneByUserCode(
        vendorInvoiceGeneralModel, valueObject.getDueDocumentCode());
    validator.setVendorInvoiceRep(VendorInvoiceGeneralModelRepMockUtils.getRepository());
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils whenDueDocumentDoesNotExist() {
    VendorInvoiceGeneralModelRepMockUtils.findOneByUserCode(null, valueObject.getDueDocumentCode());
    return this;
  }

  public DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils
      whenDueDocumentCompanyDoesNotExist() {
    OrderCompanyGeneralModelRepTestUtils.findOneByUserCode(null, valueObject.getDueDocumentCode());
    return this;
  }
}
