package com.ebs.dda.validation.settlement.utils;

import com.ebs.dac.dbo.types.LocalizedString;
import com.ebs.dac.foundation.exceptions.security.ArgumentViolationSecurityException;
import com.ebs.dac.foundation.realization.validation.validators.ValidationResult;
import com.ebs.dda.accounting.settlement.statemachine.DObSettlementStateMachine;
import com.ebs.dda.jpa.accounting.CObFiscalYear;
import com.ebs.dda.jpa.accounting.settlements.DObSettlementActivateValueObject;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementAccountDetailsGeneralModel;
import com.ebs.dda.jpa.accounting.settlements.IObSettlementDetailsGeneralModel;
import com.ebs.dda.repositories.accounting.CObFiscalYearRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementAccountDetailsGeneralModelRep;
import com.ebs.dda.repositories.accounting.settlement.IObSettlementDetailsGeneralModelRep;
import com.ebs.dda.validation.settlement.DObSettlementActivationValidator;
import com.ebs.entities.accounting.FiscalYearMockUtils;
import com.ebs.entities.accounting.settlement.IObSettlementAccountDetailsGeneralModelMockUtils;
import com.ebs.entities.accounting.settlement.IObSettlementDetailsGeneralModelMockUtils;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class DObSettlementActivationValidatorTestUtils {
  private IObSettlementDetailsGeneralModel settlementDetailsGM;
  private List<IObSettlementAccountDetailsGeneralModel> accountingDetailsGMMocks;
  private DObSettlementActivationValidator validator;
  private IObSettlementDetailsGeneralModelRep settlementDetailsGMRep;
  private IObSettlementAccountDetailsGeneralModelRep settlementAccountDetailsGMRep;
  private String documentCode;
  private String currencyCode;
  private String currencyIso;
  private String journalEntryDate;
  private String settlementCode;
  private DObSettlementActivateValueObject valueObject;
  private CObFiscalYearRep fiscalYearRep;
  private final String ACTIVATE_STATE = "Active";


  public DObSettlementActivationValidatorTestUtils() {
    settlementDetailsGMRep = Mockito.mock(IObSettlementDetailsGeneralModelRep.class);
    settlementAccountDetailsGMRep = Mockito.mock(IObSettlementAccountDetailsGeneralModelRep.class);
    fiscalYearRep = Mockito.mock(CObFiscalYearRep.class);
    accountingDetailsGMMocks = new ArrayList<>();
    validator = new DObSettlementActivationValidator();
    valueObject = new DObSettlementActivateValueObject();
    prepareValidator();
  }

  private void prepareValidator() {
    validator.setSettlementAccountDetailsGeneralModelRep(settlementAccountDetailsGMRep);
    validator.setSettlementDetailsGeneralModelRep(settlementDetailsGMRep);
    validator.setFiscalYearRep(fiscalYearRep);
  }

  public DObSettlementActivationValidatorTestUtils whenSectionFieldIsMissing() {

    settlementDetailsGM =
        IObSettlementDetailsGeneralModelMockUtils.buildGeneralModel()
            .currencyCode(currencyCode)
            .currencyIso(currencyIso)
            .settlementCode(documentCode)
            .build();

    Mockito.when(settlementDetailsGMRep.findOneBySettlementCode(documentCode))
        .thenReturn(settlementDetailsGM);
    Mockito.when(settlementAccountDetailsGMRep.findAllByDocumentCode(documentCode))
        .thenReturn(accountingDetailsGMMocks);

    return this;
  }

  public DObSettlementActivationValidatorTestUtils withCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
    return this;
  }

  public DObSettlementActivationValidatorTestUtils withCurrencyIso(String currencyIso) {
    this.currencyIso = currencyIso;
    return this;
  }

  public DObSettlementActivationValidatorTestUtils withDocumentCode(String documentCode) {
    this.settlementCode = documentCode;
    this.documentCode = documentCode;
    this.valueObject.setSettlementCode(documentCode);
    return this;
  }
  public DObSettlementActivationValidatorTestUtils withJournalEntryDate(String journalEntryDate) throws ParseException {
    this.journalEntryDate = journalEntryDate;
    this.valueObject.setJournalEntryDate(journalEntryDate);
    mockFiscalYear();
    return this;
  }

  public DObSettlementActivationValidatorTestUtils withAccountingDetails(
      List<IObSettlementAccountDetailsGeneralModel> accountingDetails) {
    accountingDetailsGMMocks = accountingDetails;
    return this;
  }

  public void assertThatSectionFieldAreMissing(String sectionName, String missingField) {
    Map<String, Object> sectionMissingFields =
        validator.validateMissingFields(documentCode, DObSettlementStateMachine.POSTED_STATE);
    List<String> missingFields = (List<String>) sectionMissingFields.get(sectionName);
    missingFields.contains(missingField);
  }

  public void assertThatErrorCode_SETTLEMENTS_ACCOUNTING_DETAILS_NOT_EQUALS_IsReturned() {
    ValidationResult validationResult = validator.validateBusinessRules(valueObject);
    String returnedErrorCode = validationResult.getErrorCode("AccountingDetails");
    Assertions.assertEquals(returnedErrorCode, "S-msg-10");
  }

  public void assertThatErrorCode_SETTLEMENTS_AMOUNT_GREATER_THAN_BALANCE_IsReturned() {
    ValidationResult validationResult = validator.validateBusinessRules(valueObject);
    String returnedErrorCode = validationResult.getErrorCode("amount");
    Assertions.assertEquals(returnedErrorCode, "S-msg-11");
  }

  public void assertThrowsArgumentViolationSecurityException() {
    assertThrows(
            ArgumentViolationSecurityException.class,
            () -> validator.validateBusinessRules(valueObject));
  }

  public List<IObSettlementAccountDetailsGeneralModel> getAccountingDetailsThatAreEquivalent(
      String documentCode, BigDecimal creditDebitAmount, BigDecimal balance) {
    List<IObSettlementAccountDetailsGeneralModel> settlementAccountingDetails = new ArrayList<>();
    IObSettlementAccountDetailsGeneralModel settlementWithDebitAmount =
        IObSettlementAccountDetailsGeneralModelMockUtils.buildGeneralModel()
            .amount(creditDebitAmount)
            .accountingEntry("DEBIT")
            .docCode(documentCode)
            .subLedger("Local_Vendors")
            .build();

    IObSettlementAccountDetailsGeneralModel settlementWithCreditAmount =
        IObSettlementAccountDetailsGeneralModelMockUtils.buildGeneralModel()
            .amount(creditDebitAmount)
            .accountingEntry("CREDIT")
            .docCode(documentCode)
            .subLedger("Banks")
            .balance(balance)
            .glSubAccountCode("1011121314678 - USD")
            .glSubAccountName(getGlSubAccountName())
            .build();

    settlementAccountingDetails.add(settlementWithDebitAmount);
    settlementAccountingDetails.add(settlementWithCreditAmount);
    return settlementAccountingDetails;
  }

  private LocalizedString getGlSubAccountName() {
    LocalizedString glSubAccountName = new LocalizedString();
    glSubAccountName.setValue(new Locale("en"), "National Bank of Egypt");
    glSubAccountName.setValue(new Locale("ar"), "البنك الأهلي");
    return glSubAccountName;
  }

  public List<IObSettlementAccountDetailsGeneralModel> getAccountingDetailsThatAreNotEquivalent(
      String documentCode, BigDecimal debitAmount, BigDecimal creditAmount, BigDecimal balance) {
    List<IObSettlementAccountDetailsGeneralModel> settlementAccountingDetails = new ArrayList<>();

    IObSettlementAccountDetailsGeneralModel settlementWithDebitAmount =
        IObSettlementAccountDetailsGeneralModelMockUtils.buildGeneralModel()
            .amount(debitAmount)
            .accountingEntry("DEBIT")
            .docCode(documentCode)
            .subLedger("Local_Vendors")
            .build();

    IObSettlementAccountDetailsGeneralModel settlementWithCreditAmount =
        IObSettlementAccountDetailsGeneralModelMockUtils.buildGeneralModel()
            .amount(creditAmount)
            .accountingEntry("CREDIT")
            .docCode(documentCode)
            .subLedger("Banks")
            .balance(balance)
            .glSubAccountCode("1011121314678 - USD")
            .glSubAccountName(getGlSubAccountName())
            .build();

    settlementAccountingDetails.add(settlementWithDebitAmount);
    settlementAccountingDetails.add(settlementWithCreditAmount);
    return settlementAccountingDetails;
  }

  public void assertThatErrorCode_SETTLEMENTS_AMOUNT_GREATER_THAN_BALANCE_IsNotReturned() {
    ValidationResult validationResult = validator.validateBusinessRules(valueObject);
    String returnedErrorCode = validationResult.getErrorCode("amount");
    Assertions.assertNull(returnedErrorCode);
  }
  private void mockFiscalYear() throws ParseException {
    CObFiscalYear firstFiscalYear =
            FiscalYearMockUtils.mockEntity()
                    .id(1L)
                    .currentState("%" + ACTIVATE_STATE + "%")
                    .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2021 00:00 AM")))
                    .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
                    .build();
    CObFiscalYear secondFiscalYear =
            FiscalYearMockUtils.mockEntity()
                    .id(2L)
                    .currentState("%" + ACTIVATE_STATE + "%")
                    .fromDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2022 00:00 AM")))
                    .toDate(new DateTime(getDateTimeInMilliSecondsFormat("01-JAN-2023 00:00 AM")))
                    .build();
    List<CObFiscalYear> fiscalYearList = new ArrayList<>();
    fiscalYearList.add(firstFiscalYear);
    fiscalYearList.add(secondFiscalYear);
    Mockito.when(fiscalYearRep.findByCurrentStatesLike("%" + ACTIVATE_STATE + "%"))
            .thenReturn(fiscalYearList);
  }
  public Long getDateTimeInMilliSecondsFormat(String dateTime) throws ParseException {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    Date date = dateFormat.parse(dateTime);
    return date.getTime();
  }
}
