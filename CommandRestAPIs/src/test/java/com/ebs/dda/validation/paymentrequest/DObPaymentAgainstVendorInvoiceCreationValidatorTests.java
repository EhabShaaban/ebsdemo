package com.ebs.dda.validation.paymentrequest;

import com.ebs.dda.jpa.accounting.paymentrequest.DueDocumentTypeEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentFormEnum;
import com.ebs.dda.jpa.accounting.paymentrequest.PaymentTypeEnum;
import com.ebs.dda.validation.paymentrequest.utils.DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils;
import org.junit.Before;
import org.junit.Test;

public class DObPaymentAgainstVendorInvoiceCreationValidatorTests {

  private DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils utils;

  @Before
  public void setUp() {
    utils = new DObPaymentAgainstVendorInvoiceCreationValidatorTestsUtils();
  }

  @Test
  public void Should_Pass_WhenAllDataIsCorrect() throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000001", "Posted")
        .validateSuccessfully();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDocumentOwnerIsMissing() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(null)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000001", "Posted")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDocumentOwnerDoesNotExists() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1000L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000001", "Posted")
        .whenDocumentOwnerDoesNotExist()
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenBusinessUnitDoesNotExists() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("9999")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000001", "Posted")
        .whenBusinessUnitDoesNotExist()
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenBusinessPartnerIsMissing() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(null, null)
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000001", "Posted")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_ErrorIncorrectFieldInputIsReturned_WhenBusinessPartnerDoesNotExists()
      throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(999999L, "999999")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "999999", "Posted")
        .whenBusinessPartnerDoesNotExist()
        .assertErrorIncorrectFieldInputIsReturned();
  }

  @Test
  public void
      Should_throwsArgumentViolationSecurityException_WhenBusinessPartnerHasNotBusinessUnitAllowed() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0004")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0004", "000001", "Posted")
        .whenBusinessPartnerHasNotBusinessUnitAllowed()
        .throwsAuthorizationException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDueDocumentTypeIsMissing() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(null)
        .withDueDocument("2021000001", "0001", "000001", "Posted")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDueDocumentCodeIsMissing() {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument(null, "0001", "000001", "Posted")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenDueDocumentDoesNotExists()
      throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021999999", "0001", "000001", "Posted")
        .whenDueDocumentDoesNotExist()
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void
      Should_throwsArgumentViolationSecurityException_WhenVendorInvoiceHasNotTheSelectedBusinessUnit()
          throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0002", "000001", "Posted")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void
      Should_throwsArgumentViolationSecurityException_WhenVendorInvoiceHasNotTheSelectedBusinessPartner()
          throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000002", "Posted")
        .throwsArgumentViolationSecurityException();
  }

  @Test
  public void Should_throwsArgumentViolationSecurityException_WhenVendorInvoiceInDraftState()
      throws Exception {
    utils
        .withPaymentType(PaymentTypeEnum.PaymentType.VENDOR)
        .withPaymentForm(PaymentFormEnum.PaymentForm.CASH)
        .withDocumentOwnerId(1L)
        .withBusinessUnitCode("0001")
        .withBusinessPartner(1L, "000001")
        .withDueDocumentType(DueDocumentTypeEnum.DueDocumentType.INVOICE)
        .withDueDocument("2021000001", "0001", "000001", "Draft")
        .throwsArgumentViolationSecurityException();
  }
}
