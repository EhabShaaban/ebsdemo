package com.ebs.dda.validation.costing;

import com.ebs.dda.accounting.landedcost.statemachines.DObLandedCostStateMachine;
import com.ebs.dda.validation.costing.utils.DObCostingActivateValidatorTestUtils;
import com.ebs.repositories.FiscalYearRepMockUtils;
import com.ebs.repositories.LandedCostGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingDetailsGeneralModelRepMockUtils;
import com.ebs.repositories.accounting.costing.CostingGeneralModelRepMockUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.text.ParseException;

@RunWith(JUnitPlatform.class)
public class DObCostingActivateValidatorTests {

    private DObCostingActivateValidatorTestUtils utils;

    @Before
    public void init() {
        utils = new DObCostingActivateValidatorTestUtils();
    }

    @Test
    public void Should_ThrowsArgumentSecurityException_When_FiscalYearIsInActive() throws ParseException {
        utils.whenFiscalYearIsActive("2021000001", "06-Sep-2019 09:30 AM").assertThrowsArgumentSecurityException();
    }

    @Test
    public void Should_ThrowsArgumentSecurityException_When_CostingDocumentNotExists() {
        utils.whenCostingDocumentNotExists("2019000001", "06-Sep-2021 09:30 AM").assertThrowsArgumentSecurityException();
    }

    @Test
    public void Should_ThrowsCostingHasNoActualLandedCostException_When_CostingDocumentNotExists() throws ParseException {
        String costingCode = "2021000001";
        String activeState = "%Active%";
        String purchaseOrderCode = "2021000004";

        utils.whenLandedCostStateIsEstimate(costingCode, "06-Sep-2021 09:30 AM").assertThrowsCostingHasNoActualLandedCostException();

        Mockito.verify(CostingGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
                .findOneByUserCode(costingCode);

        Mockito.verify(FiscalYearRepMockUtils.getRepository(), Mockito.times(1))
                .findByCurrentStatesLike(activeState);

        Mockito.verify(CostingDetailsGeneralModelRepMockUtils.getRepository(), Mockito.times(1))
                .findOneByUserCode(costingCode);

        Mockito.verify(LandedCostGeneralModelRepMockUtils.getRepository(), Mockito.times(1)).findByPurchaseOrderCodeAndCurrentStatesLike(
                purchaseOrderCode,
                '%' + DObLandedCostStateMachine.ACTUAL_VALUE_COMPLETED + '%');
    }

    @After
    public void tearDown() {
        CostingGeneralModelRepMockUtils.resetRepository();
        LandedCostGeneralModelRepMockUtils.resetRepository();
        FiscalYearRepMockUtils.resetRepository();
        CostingDetailsGeneralModelRepMockUtils.resetRepository();
    }
}
