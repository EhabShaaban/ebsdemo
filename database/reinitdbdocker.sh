#!/bin/bash

export DATABASE="postgres"
export FOUND="FALSE"
echo "$PWD" | grep -e database && export FOUND="TRUE"

if [[ $FOUND == "TRUE" ]]; then
  export DB_SCRIPT=$PWD
else
  export DB_SCRIPT=$PWD/database
fi
cd $DB_SCRIPT

if [[ $1 == "local" ]]; then
  ./reinitdb.sh
  ./seeddata.sh
else
  ./reinitdb.sh "aws"
  ./seeddata.sh "aws"
fi
