#!/bin/bash

export DATABASE="postgres"

export FOUND="FALSE"
echo "$PWD" | grep -e database && export FOUND="TRUE"
if [[ $FOUND == "TRUE" ]]; then
  export DB_SCRIPT=$PWD
else
  export DB_SCRIPT=$PWD/database
fi

if [[ $1 == "aws" ]]; then
  echo 'seed data as AWS environment'
  cd $DB_SCRIPT/$DATABASE/test
  . ./seeddata_aws_test.sh $1
else
  echo 'seed data as LOCAL environment'
  cd $DB_SCRIPT/$DATABASE/test
  . ./seeddata_aws_test.sh
fi
