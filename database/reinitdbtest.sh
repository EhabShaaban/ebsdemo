#!/bin/bash

export FOUND="FALSE"
echo "$PWD" | grep -e database && export FOUND="TRUE"
if [[ $FOUND = "TRUE" ]] ; then
	export DB_SCRIPT=$PWD
else
	export DB_SCRIPT=$PWD/database
fi
cd $DB_SCRIPT

./reinitdb.sh
./seeddata.sh "test"
