#!/bin/bash

if [[ $1 == "aws" ]]; then
  cat reinitDB.sql | psql -U postgres --port 5432
  cat ebdk_core_collected_scripts_production_schema.sql | psql -U bdk --port 5432 --dbname ebdk_core_v2
else
  cat reinitDB.sql | psql -U postgres --host localhost --port 5432
  cat ebdk_core_collected_scripts_production_schema.sql | psql -U bdk --host localhost --port 5432 --dbname ebdk_core_v2
fi


