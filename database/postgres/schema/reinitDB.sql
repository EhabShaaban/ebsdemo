SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = 'ebdk_core_v2';
DROP DATABASE IF EXISTS ebdk_core_v2;
DROP ROLE IF EXISTS bdk;
CREATE ROLE bdk WITH CREATEDB
  LOGIN
  PASSWORD 'bdk';
CREATE DATABASE ebdk_core_v2 OWNER bdk;
