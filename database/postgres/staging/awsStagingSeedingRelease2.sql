
\ir '../dbscripts/PurchaseOrder/CObShipping.sql'
\ir '../dbscripts/PurchaseOrder/DObPurchaseOrder.sql'
\ir '../dbscripts/PurchaseOrder/DObPurchaseOrderPDF.sql'

\ir '../dbscripts/StagingData/document-types.sql'

\ir '../dbscripts/StagingData/incoterms.sql'

\ir '../dbscripts/StagingData/transport-modes.sql'

\ir '../dbscripts/StagingData/LocalizedLobMaterial.sql'

\ir '../dbscripts/StagingData/countries.sql'

\ir '../dbscripts/StagingData/ports.sql'

\ir '../dbscripts/StagingData/payment-terms.sql'

\ir '../dbscripts/StagingData/shipping-instructions.sql'
