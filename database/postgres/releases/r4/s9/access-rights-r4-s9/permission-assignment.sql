INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'LandedCostDocumentOwner'),
        (select id from permission where permissionexpression = 'LandedCost:CanBeDocumentOwner'),
        now());

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'PaymentRequestDocumentOwner'),
        (select id from permission where permissionexpression = 'PaymentRequest:CanBeDocumentOwner'),
        now());

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'DepotViewer'),
        (select id from permission where permissionexpression = 'Depot:ReadAll'),
        now());

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingViewer_offset'),
        (select id from permission where permissionexpression = 'Costing:ReadDetails'),
        now(),
        (select id
         from authorizationcondition
         where concat(objectname, ' - ', condition) =
               'Costing  - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NotesPayableViewer_Offset'),
        (select id from permission where permissionexpression = 'NotesPayable:ReadAll'),
        now(),
        (select id
         from authorizationcondition
         where concat(objectname, ' - ', condition) =
               'NotesPayable - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'NotesPayableViewer'),
        (select id from permission where permissionexpression = 'NotesPayable:ReadAll'),
        now());