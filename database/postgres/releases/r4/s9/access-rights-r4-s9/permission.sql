INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('LandedCost:CanBeDocumentOwner', null, null, null, 'LandedCost');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('PaymentRequest:CanBeDocumentOwner', null, null, null, 'PaymentRequest');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Depot:ReadAll', null, null, null, 'Depot');

INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Costing:ReadDetails', null, null, null, 'Costing');

INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesPayable:ReadAll', null, null, null, 'NotesPayable');