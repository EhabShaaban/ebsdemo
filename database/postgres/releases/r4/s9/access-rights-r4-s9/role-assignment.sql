INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Accountant_offset'),
        (select id from role where roleexpression = 'LandedCostDocumentOwner'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'AccountantHead_offset'),
        (select id from role where roleexpression = 'LandedCostDocumentOwner'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Accountant_offset'),
        (select id from role where roleexpression = 'PaymentRequestDocumentOwner'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'AccountantHead_offset'),
        (select id from role where roleexpression = 'PaymentRequestDocumentOwner'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'AdminTressuary_offset'),
        (select id from role where roleexpression = 'PaymentRequestDocumentOwner'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'PurchasingResponsible_offset'),
        (select id from role where roleexpression = 'VendorInvoiceDocumentOwner'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Accountant_offset'),
        (select id from role where roleexpression = 'DepotViewer'));

INSERT INTO public.roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Accountant_offset'),
        (select id from role where roleexpression = 'CostingViewer_offset'));