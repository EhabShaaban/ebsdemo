DROP VIEW IF EXISTS IObMonetaryNotesDetailsGeneralModel;
CREATE VIEW IObMonetaryNotesDetailsGeneralModel AS
SELECT IObMonetaryNotesDetails.id,
       IObMonetaryNotesDetails.refinstanceid,
       IObMonetaryNotesDetails.creationInfo,
       IObMonetaryNotesDetails.modificationInfo,
       IObMonetaryNotesDetails.creationDate,
       IObMonetaryNotesDetails.modifiedDate,
       dobaccountingdocument.code AS userCode,
       dobaccountingdocument.objecttypecode,
       iobmonetarynotesdetails.noteForm,
       MasterData.code            AS businessPartnerCode,
       MasterData.name            AS businessPartnerName,
       iobmonetarynotesdetails.notenumber,
       iobmonetarynotesdetails.notebank,
       iobmonetarynotesdetails.notedate,
       CObCurrency.code           AS currencyCode,
       CObCurrency.iso            AS currencyIso,
       iobmonetarynotesdetails.amount,
       iobmonetarynotesdetails.remaining,
       lobdepot.code              AS depotCode,
       lobdepot.name              AS depotName
FROM IObMonetaryNotesDetails
         LEFT JOIN dobmonetarynotes ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = dobmonetarynotes.id
         LEFT JOIN lobdepot ON lobdepot.id = iobmonetarynotesdetails.depotid
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '3';