ALTER TABLE IObGoodsReceiptAccountingDocumentDetails ADD COLUMN exchangeRateId INT8;

ALTER TABLE IObGoodsReceiptAccountingDocumentDetails
    ADD CONSTRAINT FK_IObGoodsReceiptAccountingDocumentDetails_echangeRateId FOREIGN KEY (exchangeRateId) REFERENCES cobexchangerate (id) ON DELETE restrict ;
