DROP VIEW IF EXISTS IObGoodsReceiptAccountingDocumentDetailsGeneralModel;

create view IObGoodsReceiptAccountingDocumentDetailsGeneralModel AS
select IObGoodsReceiptAccountingDocumentDetails.id,
       dobaccountingdocument.code                           as userCode,
       IObGoodsReceiptAccountingDocumentDetails.refinstanceid,
       IObGoodsReceiptAccountingDocumentDetails.creationDate,
       IObGoodsReceiptAccountingDocumentDetails.creationInfo,
       IObGoodsReceiptAccountingDocumentDetails.modifiedDate,
       IObGoodsReceiptAccountingDocumentDetails.modificationInfo,
       dobinventorydocument.code                            as goodsReceiptCode,
       doborder.code                                        as purchaseOrderCode,
       exchangeRate.code                                    AS exchangeRateCode,
       exchangeRate.firstvalue,
       (exchangerate.firstvalue * exchangeRate.secondvalue) AS currencyPrice,
       exchangeRate.firstcurrencyiso,
       exchangeRate.secondcurrencyiso,
       purchaseUnit.name :: json ->> 'en'                   as purchaseUnitNameEn,
       purchaseUnit.name                                    as purchaseUnitName
from IObGoodsReceiptAccountingDocumentDetails
         left join DObGoodsReceiptPurchaseOrder
                   on IObGoodsReceiptAccountingDocumentDetails.goodsreceiptid =
                      DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         left join dobgoodsreceiptaccountingdocument
                   on IObGoodsReceiptAccountingDocumentDetails.refinstanceid =
                      dobGoodsReceiptAccountingDocument.id

         left join dobaccountingdocument on dobaccountingdocument.id = dobgoodsreceiptaccountingdocument.id

         left join iobgoodsreceiptpurchaseorderdata
                   on iobgoodsreceiptpurchaseorderdata.refinstanceid =
                      DObGoodsReceiptPurchaseOrder.id
         left join doborderdocument doborder
                   on iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id
         left join cobexchangerategeneralmodel exchangeRate
                   on IObGoodsReceiptAccountingDocumentDetails.exchangerateid = exchangerate.id
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join cobenterprise purchaseUnit on iobaccountingdocumentcompanydata.purchaseunitid = purchaseUnit.id;