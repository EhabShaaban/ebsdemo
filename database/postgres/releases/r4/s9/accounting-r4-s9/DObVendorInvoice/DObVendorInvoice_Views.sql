DROP VIEW If EXISTS dobVendorInvoicePostPreparationGeneralModel;


create or replace view dobVendorInvoicePostPreparationGeneralModel as
select dobaccountingdocument.id,
       code,
       IObVendorInvoicePurchaseOrder.currencyid,
       latestexchangerate.exchangerateid as exchangerateid,
       latestexchangerate.currencyprice  as currencyprice
from dobaccountingdocument
         inner join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         inner join iobaccountingdocumentcompanydata
                    on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         inner join iobenterprisebasicdata companydata
                    on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         inner join IObVendorInvoicePurchaseOrder
                    on dobaccountingdocument.id = iobvendorinvoicepurchaseorder.refinstanceid
         inner join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(IObVendorInvoicePurchaseOrder.currencyid,
                                                                companydata.currencyid) latestexchangerate
                    on latestexchangerate.firstcurrencyid = IObVendorInvoicePurchaseOrder.currencyid
                        and latestexchangerate.secondcurrencyid = companydata.currencyid;

CREATE OR REPLACE VIEW DObVendorInvoiceGeneralModel AS
SELECT dobvendorInvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                                                     AS vendorCode,
       masterdata.name                                                     AS vendorName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en') AS vendorCodeName,
       dobaccountingdocument.objecttypecode                                AS invoiceType,
       purchaseUnit.code                                                   AS purchaseUnitCode,
       purchaseUnit.name                                                   AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                  AS purchaseUnitNameEn,
       company.code                                                        AS companyCode,
       company.name                                                        AS companyName,
       cobcurrency.code                                                    AS currencyCode,
       cobcurrency.name                                                    AS currencyName,
       cobcurrency.iso                                                     AS currencyISO,
       cobpaymentterms.code                                                AS paymentTermCode,
       doborderdocument.code                                               AS purchaseOrderCode,
       iobvendorinvoicedetails.invoiceNumber                               AS invoiceNumber,
       userinfo.name                                                       AS documentOwner
FROM dobvendorInvoice
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN iobvendorinvoicedetails
                   ON iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id
         LEFT JOIN masterdata ON iobvendorinvoicedetails.vendorId = masterdata.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit
                   ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
                       AND purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobenterprise company ON company.id = iobvendorinvoicecompanydata.companyId
    AND company.objecttypecode = '1'
         LEFT JOIN IObVendorInvoicePurchaseOrder
                   ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms
                   ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument
                   ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId AND
                      doborderdocument.objecttypecode LIKE '%_PO'
            LEFT JOIN userinfo on userinfo.id = dobaccountingdocument.documentownerid;
CREATE OR REPLACE VIEW DObVendorInvoiceDebitPreparationGeneralModel AS
SELECT ROW_NUMBER() OVER ()                                       as id,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companyData.currencyid                                     as companyCurrencyId,
       iobvendorinvoicepurchaseorder.currencyid                   as documentCurrencyId,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
       iobitemaccountinginfo.chartofaccountid                     as glaccountId,
       CASE
           when isGoodsInvoice(dobaccountingdocument.objecttypecode)
               then iobvendorinvoicepurchaseorder.purchaseorderid
           else iobpurchaseorderfulfillervendor.referencepoid end as glsubaccountId,
       leafglaccount.leadger                                      as accountLedger,
       Case
           when (Select count(*)
                 from iobvendorinvoicetaxes
                 where iobvendorinvoicetaxes.refinstanceid = iobvendorinvoiceitem.refinstanceid) = 0
               then (Select (item.quantityinorderunit * item.price *
                             getconversionfactortobase(item.baseunitofmeasureid, item.orderunitofmeasureid,
                                                       item.itemid))
                     from iobvendorinvoiceitem item
                     where item.id = iobvendorinvoiceitem.id
                     GROUP BY item.id
           )
           else
               (Select (iobvendorinvoiceitem.quantityinorderunit * iobvendorinvoiceitem.price *
                        getconversionfactortobase(iobvendorinvoiceitem.baseunitofmeasureid,
                                                  iobvendorinvoiceitem.orderunitofmeasureid,
                                                  iobvendorinvoiceitem.itemid)) + sum(
                                   (iobvendorinvoiceitem.quantityinorderunit * iobvendorinvoiceitem.price *
                                    getconversionfactortobase(iobvendorinvoiceitem.baseunitofmeasureid,
                                                              iobvendorinvoiceitem.orderunitofmeasureid,
                                                              iobvendorinvoiceitem.itemid)) *
                                   (iobvendorinvoicetaxes.percentage/100))
                from iobvendorinvoicetaxes
                where iobvendorinvoicetaxes.refinstanceid = iobvendorinvoiceitem.refinstanceid
                GROUP BY iobvendorinvoiceitem.id
               ) end                                              as amount

from dobaccountingdocument
         inner join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         left join iobaccountingdocumentactivationdetails activationdetails
                   on dobaccountingdocument.id = activationdetails.refinstanceid
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobenterprisebasicdata companyData
                   on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
         left join iobvendorinvoicepurchaseorder on iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         left join iobpurchaseorderfulfillervendor
                   on iobpurchaseorderfulfillervendor.refinstanceid = iobvendorinvoicepurchaseorder.purchaseorderid
         left join iobvendorinvoiceitem on iobvendorinvoiceitem.refinstanceid = dobvendorinvoice.id
         left join iobitemaccountinginfo on iobitemaccountinginfo.refinstanceid = iobvendorinvoiceitem.itemid
         left join leafglaccount on leafglaccount.id = iobitemaccountinginfo.chartofaccountid;