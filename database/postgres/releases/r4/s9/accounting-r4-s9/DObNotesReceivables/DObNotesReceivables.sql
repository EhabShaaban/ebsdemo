ALTER TABLE iobmonetarynotesdetails
    DROP COLUMN treasuryid,
    DROP COLUMN bankaccountid,
    DROP COLUMN depot;

ALTER TABLE iobmonetarynotesdetails
    RENAME COLUMN duedate TO noteDate;

ALTER TABLE iobmonetarynotesdetails
    ADD COLUMN depotId INT8 REFERENCES lobdepot (id) ON DELETE RESTRICT;