CREATE OR REPLACE VIEW DObCollectionGeneralModel AS
SELECT c.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode,
       c.collectiontype,
       company.code                                        AS companycode,
       company.name                                        AS companyname,
       cobcurrency.code                                    AS companylocalcurrencycode,
       cobcurrency.name                                    AS companylocalcurrencyname,
       cobcurrency.iso                                     AS companylocalcurrencyiso,
       masterdata.name                                     AS businessPartnername,
       masterdata.code                                     AS businessPartnercode,
       CASE
           WHEN masterdata.objecttypecode = '4' THEN cast('{"ar": "موظف", "en": "Employee"}' AS JSON)
           WHEN masterdata.objecttypecode = '3' THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
           WHEN masterdata.objecttypecode = '2' THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
           ELSE NULL END
                                                           AS businessPartnerType,
       iobcollectiondetails.amount,
       refDocumentType.code                                AS refdocumenttypecode,
       refDocumentType.name                                AS refdocumenttypename,
       getRefDocumentCodeBasedOnType(iobcollectiondetails.id,
                                     refDocumentType.code) AS refDocumentCode,
       dobaccountingdocument.objecttypecode                AS collectionDocumentType,

       dobaccountingdocument.documentOwnerId                                   AS documentOwnerId,
       userinfo.name             AS documentOwner,
       ebsuser.username              AS documentOwnerUserName,
       iobcollectiondetails.collectionmethod

FROM dobcollection c
         LEFT JOIN iobcollectiondetails ON c.id = iobcollectiondetails.refinstanceid
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = c.id
         LEFT JOIN cobcollectiontype refDocumentType
                   ON iobcollectiondetails.refDocumentTypeId = refDocumentType.id
         LEFT JOIN mobbusinessPartner ON iobcollectiondetails.businessPartnerid = mobbusinessPartner.id
         LEFT JOIN masterdata ON mobbusinessPartner.id = masterdata.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON iobaccountingdocumentcompanydata.refInstanceId = c.id
                       AND iobaccountingdocumentcompanydata.objecttypecode = 'Collection'
         LEFT JOIN cobenterprise businessUnit
                   ON iobaccountingdocumentcompanydata.purchaseUnitId = businessUnit.id
         LEFT JOIN cobenterprise company
                   ON iobaccountingdocumentcompanydata.companyid = company.id
         LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id
         LEFT JOIN userinfo on userinfo.id = dobaccountingdocument.documentownerid
         LEFT JOIN ebsuser on ebsuser.id = dobaccountingdocument.documentownerid;