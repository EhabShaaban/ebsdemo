CREATE OR REPLACE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       paymentdetails.paymentForm,
       paymentdetails.netAmount           AS amountValue,
       cobcurrency.iso                    AS amountCurrency,
       companyData.purchaseUnitId,
       purchaseunit.name                  AS purchaseUnitName,
       purchaseunit.name:: json ->> 'en'  AS purchaseUnitNameEn,
       purchaseunit.code                  AS purchaseUnitCode,
       businesspartner.code               AS businessPartnerCode,
       businesspartner.name               AS businessPartnerName,
       duedocumenttype                    AS dueDocumentType,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           when duedocumenttype = 'CREDITNOTE' then creditNote.code
           else doborderdocument.code end as referenceDocumentCode,
       case
           when duedocumenttype = 'INVOICE' then '{
             "en": "Vendor Invoice",
             "ar": "فاتورة مورد"
           }'::json
           when duedocumenttype = 'PURCHASEORDER' then '{
             "en": "Purchase Order",
             "ar": "أمر شراء"
           }'::json
           when duedocumenttype = 'CREDITNOTE' then '{
             "en": "Credit Note",
             "ar": "الاستحقاقات الالحقاية"
           }'::json end                   as referenceDocumentType,
       DObPaymentRequest.remaining,
       paymentdetails.expectedDueDate                                                                   AS expectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS fromExpectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS toExpectedDueDate,
       paymentdetails.bankTransRef                                                                      AS bankTransRef,
       userInfo.name                                                                                    AS documentOwner
FROM DObPaymentRequest
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN cobenterprise purchaseunit ON purchaseunit.id = companyData.purchaseunitid
         LEFT JOIN iobpaymentrequestpaymentdetails paymentdetails on dobpaymentrequest.id = paymentdetails.refinstanceid
         LEFT JOIN mobbusinesspartner on mobbusinesspartner.id = paymentdetails.businesspartnerid
         LEFT JOIN masterdata businesspartner on mobbusinesspartner.id = businesspartner.id
         LEFT JOIN IObPaymentRequestInvoicePaymentDetails invoicepaymentdetails
                   on invoicepaymentdetails.id = paymentdetails.id
         LEFT JOIN iobpaymentrequestcreditnotepaymentdetails creditNotepaymentdetails
                   on creditNotepaymentdetails.id = paymentdetails.id
         LEFT JOIN IObPaymentRequestPurchaseOrderPaymentDetails orderpaymentdetails
                   on orderpaymentdetails.id = paymentdetails.id
         LEFT JOIN dobaccountingdocument vendorInvoice on vendorInvoice.id = invoicepaymentdetails.duedocumentid
         LEFT JOIN dobaccountingdocument creditNote on creditNote.id = creditNotepaymentdetails.duedocumentid
         LEFT JOIN doborderdocument on doborderdocument.id = orderpaymentdetails.duedocumentid
         LEFT JOIN cobcurrency on paymentdetails.currencyid = cobcurrency.id
         LEFT JOIN userinfo on userinfo.id = dobaccountingdocument.documentownerid;

         DROP VIEW IF EXISTS DObPaymentRequestActivatePreparationGeneralModel;

DROP FUNCTION IF EXISTS getJournalBalanceByCode(paymentForm varchar, businessUnitCode varchar,
                                                companyCode varchar, currencyCode varchar, bankCode varchar,
                                                bankAccountNo varchar,
                                                treasuryCode varchar);

CREATE or REPLACE FUNCTION getJournalBalanceByCode(paymentForm varchar, businessUnitCode varchar,
                                                   companyCode varchar, currencyCode varchar, bankCode varchar,
                                                   bankAccountNo varchar,
                                                   treasuryCode varchar)
    RETURNS float4 AS
$$
BEGIN
    RETURN
        (
            case
                when paymentForm = 'BANK' then (Select balance
                                                from cobjournalbalance
                                                where cobjournalbalance.code =
                                                      concat('BANK', businessUnitCode, companyCode,
                                                             currencyCode, bankCode,
                                                             bankAccountNo))
                when paymentForm = 'CASH' then (Select balance
                                                from cobjournalbalance
                                                where cobjournalbalance.code =
                                                      concat('TREASURY', businessUnitCode, companyCode,
                                                             currencyCode, treasuryCode)) end
            );

END ;
$$
    LANGUAGE PLPGSQL;



CREATE Or Replace View DObPaymentRequestActivatePreparationGeneralModel AS
Select paymentRequest.id,
       paymentRequest.code                                                        as paymentRequestCode,
       businessUnit.code                                                          as businessUnitCode,
       company.code                                                               as companyCode,
       cobcurrency.code                                                           as currencyCode,
       cobbank.code                                                               as bankCode,
       iobcompanybankdetails.accountno                                            as bankAccountNo,
       cobtreasury.code                                                           as treasuryCode,
       details.netamount                                                          as paymentNetAmount,
       dobpaymentrequest.paymenttype                                              as paymenttype,
       details.dueDocumentType                                                    as dueDocumentType,
       getDueDocumentCodeAccordingToPaymentType(details.id, details.dueDocumentType   ) as dueDocumentCode,
       getJournalBalanceByCode(details.paymentform, businessUnit.code, company.code, cobcurrency.code,
                               cobbank.code,
                               iobcompanybankdetails.accountno, cobtreasury.code) as balance,
       latestexchangerate.currencyprice                                           as currencyprice,
       cobexchangerate.code                                                       as exchangerate
from dobaccountingdocument paymentRequest
         left join dobpaymentrequest on dobpaymentrequest.id =  paymentRequest.id
         left join iobaccountingdocumentcompanydata paymentCompanyData
                   on paymentRequest.id = paymentCompanyData.refinstanceid
         left join cobenterprise company on company.id = paymentCompanyData.companyid
         left join cobenterprise businessUnit on businessUnit.id = paymentCompanyData.purchaseunitid
         left join iobpaymentrequestpaymentdetails details on details.refinstanceid = paymentRequest.id
         left join cobcurrency on cobcurrency.id = details.currencyid
         left join iobcompanybankdetails on iobcompanybankdetails.id = details.bankaccountid
         left join cobbank on cobbank.id = iobcompanybankdetails.bankid
         left join cobtreasury on cobtreasury.id = details.treasuryid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = paymentCompanyData.companyid
         left join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(details.currencyid,
                                                                            companyData.currencyid) latestexchangerate
                   on latestexchangerate.firstcurrencyid = details.currencyid
                       and latestexchangerate.secondcurrencyid = companydata.currencyid
         left join cobexchangerate on cobexchangerate.id = latestexchangerate.exchangerateid
WHERE paymentRequest.objecttypecode = 'PaymentRequest'
  AND paymentCompanyData.objecttypecode = 'PaymentRequest'
  AND company.objecttypecode = '1'
  AND businessUnit.objecttypecode = '5';

