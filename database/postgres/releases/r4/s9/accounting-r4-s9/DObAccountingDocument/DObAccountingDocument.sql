ALTER TABLE DObAccountingDocument ADD COLUMN documentOwnerId INT8 REFERENCES ebsuser (id) ON DELETE RESTRICT;


UPDATE dobaccountingdocument
SET documentownerid = (Select dobaccountingnote.documentownerid
                       from dobaccountingnote
                       where dobaccountingnote.id = dobaccountingdocument.id);

UPDATE dobaccountingdocument
SET documentownerid = (Select dobcollection.documentownerid
                       from dobcollection
                       where dobcollection.id = dobaccountingdocument.id);

UPDATE dobaccountingdocument
SET documentownerid = (Select dobinitialbalanceupload.documentownerid
                       from dobinitialbalanceupload
                       where dobinitialbalanceupload.id = dobaccountingdocument.id);
UPDATE dobaccountingdocument
SET documentownerid = (Select dobmonetarynotes.documentownerid
                       from dobmonetarynotes
                       where dobmonetarynotes.id = dobaccountingdocument.id);

UPDATE dobaccountingdocument
SET documentownerid = (Select dobsalesinvoice.documentownerid
                       from dobsalesinvoice
                       where dobsalesinvoice.id = dobaccountingdocument.id);
UPDATE dobaccountingdocument
SET documentownerid = (Select dobsettlement.documentownerid
                       from dobsettlement
                       where dobsettlement.id = dobaccountingdocument.id);




