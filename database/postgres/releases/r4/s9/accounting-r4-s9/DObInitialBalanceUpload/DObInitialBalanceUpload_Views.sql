CREATE OR REPLACE VIEW DObInitialBalanceUploadGeneralModel AS
SELECT ibu.id,
       ibuAccountingDocument.code,
       ibuAccountingDocument.creationDate,
       ibuAccountingDocument.modifiedDate,
       ibuAccountingDocument.creationInfo,
       ibuAccountingDocument.modificationInfo,
       ibuAccountingDocument.currentStates,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en' AS purchaseUnitNameEn,
       businessUnit.code                  AS purchaseUnitcode,
       company.code                       AS companycode,
       company.name                       AS companyname,

       ibuAccountingDocument.documentOwnerId                AS documentOwnerId,
       UserInfo.name                      AS documentOwner,
       fiscalPeriod.name::json ->> 'en'   AS FiscalYear

FROM DObInitialBalanceUpload ibu
         INNER JOIN dobaccountingdocument ibuAccountingDocument ON ibu.id = ibuAccountingDocument.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON iobaccountingdocumentcompanydata.refInstanceId = ibu.id
                       AND iobaccountingdocumentcompanydata.objecttypecode = 'InitialBalanceUpload'
         LEFT JOIN cobenterprise businessUnit
                   ON iobaccountingdocumentcompanydata.purchaseUnitId = businessUnit.id
         LEFT JOIN cobenterprise company
                   ON iobaccountingdocumentcompanydata.companyid = company.id
         LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN CObFiscalPeriod fiscalPeriod
on ibu.fiscalPeriodId = fiscalPeriod.id
    LEFT JOIN UserInfo on UserInfo.id = ibuAccountingDocument.documentownerid;