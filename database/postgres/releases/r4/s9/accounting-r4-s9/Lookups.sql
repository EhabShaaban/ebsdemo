drop table if exists LObDepot;

CREATE TABLE LObDepot
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    name             JSON                     NOT NULL,
    description      VARCHAR(1024)                NULL,
    PRIMARY KEY (id)
);
