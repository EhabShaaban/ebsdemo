CREATE OR REPLACE VIEW IObGoodsReceiptPurchaseOrderDataGeneralModel AS
SELECT iobgoodsreceiptpurchaseorderdata.id,
       iobgoodsreceiptpurchaseorderdata.refinstanceid,
       iobgoodsreceiptpurchaseorderdata.deliverynote,
       iobgoodsreceiptpurchaseorderdata.comments,
       dobinventorydocument.code AS goodsreceiptcode,
       podata.purchaseordercode,
       vendordata.vendorcode,
       iobgoodsreceiptpurchaseorderdata.vendorname,
       cobenterprise.code        AS purchaseunitcode,
       cobenterprise.name        AS purchaseunitname,
       iobgoodsreceiptpurchaseorderdata.purchaseresponsiblename,
       accountingDocument.code   as costingDocumentCode
FROM iobgoodsreceiptpurchaseorderdata
         LEFT JOIN dobgoodsreceiptpurchaseorder
                   ON iobgoodsreceiptpurchaseorderdata.refinstanceid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN dobinventorydocument ON dobgoodsreceiptpurchaseorder.id = dobinventorydocument.id
         LEFT JOIN cobenterprise ON cobenterprise.objecttypecode::text = '5'::text AND
                                    dobinventorydocument.purchaseunitid = cobenterprise.id
         LEFT JOIN (SELECT masterdata.id,
                           masterdata.code AS vendorcode,
                           masterdata.name AS vendorname
                    FROM masterdata
                    WHERE masterdata.objecttypecode::text = '2'::text) vendordata
                   ON iobgoodsreceiptpurchaseorderdata.vendorid = vendordata.id
         LEFT JOIN (SELECT doborderdocument.id,
                           doborderdocument.code AS purchaseordercode
                    FROM doborderdocument
                    WHERE doborderdocument.objecttypecode::text ~~ '%_PO'::text) podata
                   ON iobgoodsreceiptpurchaseorderdata.purchaseorderid = podata.id
         LEFT JOIN iobgoodsreceiptaccountingdocumentdetails accountingDocumentDetails
                   on accountingDocumentDetails.goodsreceiptid = dobinventorydocument.id
         LEFT JOIN dobgoodsreceiptaccountingdocument goodsReceiptAccountingDocument
                   ON accountingDocumentDetails.refinstanceid = goodsReceiptAccountingDocument.id
         LEFT JOIN dobaccountingdocument accountingDocument
                   ON accountingDocument.id = goodsReceiptAccountingDocument.id;
