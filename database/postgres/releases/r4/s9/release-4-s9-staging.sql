\ir 'access-rights-r4-s9/access-rights-r4-s9.sql'
\ir 'accounting-r4-s9/accounting-r4-s9.sql'
\ir 'coded-business-objects-r4-s9/coded-business-objects-r4-s9.sql'
\ir 'inventory-r4-s9/inventory-r4-s9.sql'
\ir 'systemdata/Depot.sql'
\ir 'migration-data-s4-s9/migration-data-s4-s9.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
