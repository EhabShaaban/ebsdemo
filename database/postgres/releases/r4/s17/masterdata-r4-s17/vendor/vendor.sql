ALTER TABLE mobvendor
    add constraint fk_mobvendor_cobenterprise
        FOREIGN KEY (purchaseresponsibleid) REFERENCES cobenterprise (id);