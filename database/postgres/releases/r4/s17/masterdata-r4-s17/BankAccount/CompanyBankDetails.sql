ALTER TABLE iobcompanybankdetails ADD CONSTRAINT fk_iobcompanybankdetails_cobcurrency
    FOREIGN KEY (currencyid) REFERENCES cobcurrency (id) ON DELETE RESTRICT;