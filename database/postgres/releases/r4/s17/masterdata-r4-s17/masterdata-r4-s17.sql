\ir 'Item/Item_Views.sql'
\ir 'Customer/Customer.sql'
\ir 'vendor/vendor.sql'
\ir 'itemvendorrecord/itemvendorrecord.sql'
\ir 'paymentterms/paymentterms.sql'
\ir 'BankAccount/CompanyBankDetails.sql'
\ir 'Company/company.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
