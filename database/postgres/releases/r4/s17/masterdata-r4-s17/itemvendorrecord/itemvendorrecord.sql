ALTER TABLE itemvendorrecord
    add constraint fk_itemvendorrecord_cobcurrency
        FOREIGN KEY (currencyid) REFERENCES cobcurrency (id) ON DELETE RESTRICT;

ALTER TABLE itemvendorrecord
    add constraint fk_itemvendorrecord_cobenterprise
        FOREIGN KEY (purchaseunitid) REFERENCES cobenterprise (id) ON DELETE RESTRICT;