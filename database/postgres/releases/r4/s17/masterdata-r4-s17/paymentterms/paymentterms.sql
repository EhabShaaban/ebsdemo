ALTER TABLE iobpaymenttermsinstallments
    add constraint fk_iobpaymenttermsinstallments_cobpaymentterms
        foreign key (refinstanceid) references cobpaymentterms (id) ON DELETE CASCADE;
