
ALTER TABLE MObCustomer
    ADD CONSTRAINT FK_MObCustomer_CurrencyId FOREIGN KEY (currencyid) REFERENCES CObCurrency (id) ON DELETE RESTRICT;

ALTER TABLE IObCustomerAccountingInfo
DROP CONSTRAINT fk_iobcustomeraccountinginfo_mobbusinesspartner;

ALTER TABLE IObCustomerAccountingInfo
    ADD CONSTRAINT FK_IObCustomerAccountingInfo_MObCustomer FOREIGN KEY (refinstanceid) REFERENCES MObCustomer (id) ON DELETE CASCADE;


ALTER TABLE IObCustomerAddress
DROP CONSTRAINT fk_iobcustomeraddress_address;

ALTER TABLE IObCustomerAddress
    ADD CONSTRAINT FK_IObCustomerAddress_AddressId FOREIGN KEY (addressid) REFERENCES LObAddress (id) ON DELETE RESTRICT;

DROP TABLE iobcustomercollectionrequestaccountingdetails;

ALTER TABLE IObCustomerContactPerson
DROP CONSTRAINT fk_iobcustomercontactperson_contactpersonid;

ALTER TABLE IObCustomerContactPerson
    ADD CONSTRAINT FK_IObCustomerContactPerson_ContactPersonId FOREIGN KEY (contactpersonid) REFERENCES LObContactPerson (id) ON DELETE RESTRICT;



