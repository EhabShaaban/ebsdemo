CREATE OR REPLACE VIEW MObVendorItemsGeneralModel AS
SELECT row_number() OVER ()   as id,
       item.code         itemCode,
       item.name         itemName,
       vendor.code       vendorCode,
       vendor.name       vendorName,
       cobmeasure.code   baseUnitCode,
       cobmeasure.symbol baseUnitSymbol,
       ivrMeasure.code   ivrMeasureCode,
       ivrMeasure.symbol ivrMeasureSymbol,
       cobenterprise.code businessUnitCode,
       cobenterprise.name businessUnitName,
       (SELECT CASE when iobalternativeuom.conversionfactor is null then 1 else iobalternativeuom.conversionfactor end)
FROM masterdata item
         inner join mobitem on item.id = mobitem.id
         inner join itemvendorrecord on itemvendorrecord.itemid = mobitem.id
         inner join masterdata vendor on vendor.id = itemvendorrecord.vendorid
         inner join cobmeasure on cobmeasure.id = mobitem.basicunitofmeasure
         inner join cobmeasure ivrMeasure on ivrMeasure.id = itemvendorrecord.uomid
         inner join iobitempurchaseunit on mobitem.id = iobitempurchaseunit.refinstanceid
         inner join cobenterprise on cobenterprise.id = itemvendorrecord.purchaseunitid
         left join iobalternativeuom on iobalternativeuom.refinstanceid = itemvendorrecord.itemid
         AND itemvendorrecord.uomid = iobalternativeuom.alternativeunitofmeasureid

WHERE itemvendorrecord.purchaseunitid = iobitempurchaseunit.purchaseunitid
  AND item.objecttypecode = '1'
  AND vendor.objecttypecode = '2'
