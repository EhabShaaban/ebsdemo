ALTER TABLE DObLandedCost
    ADD CONSTRAINT FK_DObLandedCost_DObAccountingDocument FOREIGN KEY (id) REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;


ALTER TABLE IObLandedCostFactorItemsDetails
DROP CONSTRAINT ioblandedcostfactoritemsdetails_documentinfoid_fkey;

ALTER TABLE IObLandedCostFactorItemsDetails
    ADD CONSTRAINT ioblandedcostfactoritemsdetails_documentinfoid_fkey FOREIGN KEY (documentinfoid) REFERENCES DObAccountingDocument (id) ON DELETE RESTRICT;
