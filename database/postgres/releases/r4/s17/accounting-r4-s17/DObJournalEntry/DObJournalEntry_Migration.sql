Delete from iobjournalentryitemcustomersubaccount
where id not IN (
    select id from iobjournalentryitem where iobjournalentryitem.type = 'Customers'
);
Delete from iobjournalentryitemtaxsubaccount
where id not IN (
    select id from iobjournalentryitem where iobjournalentryitem.type = 'Taxes'
);