
ALTER TABLE DObNotesReceivableJournalEntry
    ADD CONSTRAINT FK_DObNotesReceivableJournalEntry_DObJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItemCustomerSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemCustomerSubAccount_IObJournalEntryItem FOREIGN KEY (id)
        REFERENCES IObJournalEntryItem (id) ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItemCustomerSubAccount
DROP CONSTRAINT FK_IObJournalEntryItemCustomerSubAccount_IObSalesInvoiceBusines;

ALTER TABLE IObJournalEntryItemCustomerSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemCustomerSubAccount_MObCustomer FOREIGN KEY (subaccountid)
        REFERENCES MObCustomer (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItemNotesReceivableSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemNotesReceivableSubAccount_NotesReceivable FOREIGN KEY (subaccountid)
        REFERENCES DObMonetaryNotes (id) ON DELETE RESTRICT;

ALTER TABLE IObJournalEntryItemSalesSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemSalesSubAccount_IObJournalEntryItem FOREIGN KEY (id)
        REFERENCES IObJournalEntryItem (id) ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItemTaxSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemTaxSubAccount_IObJournalEntryItem FOREIGN KEY (id)
        REFERENCES IObJournalEntryItem (id) ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItemUnrealizedCurrencyGainLossSubaccount
    ADD CONSTRAINT FK_IObJournalEntryItemUnrealizedCurrencyGainLossSubaccount_IObJournalEntryItem FOREIGN KEY (id)
        REFERENCES IObJournalEntryItem (id) ON DELETE CASCADE;

ALTER TABLE IObJournalEntryItemUnrealizedCurrencyGainLossSubaccount
    DROP COLUMN subaccountid;

