\ir 'DObSalesInvoice/DObSalesInvoice_Views.sql'
\ir 'DObVendorInvoice/DObVendorInvoice.sql'
\ir 'DObPayment/DObPayment.sql'
\ir 'DObAccountingDocument/DObAccountingDocument.sql'
\ir 'ChartOfAccounts/ChartOfAccounts.sql'
\ir 'DObJournalEntry/DObJournalEntry.sql'
\ir 'DObSettlement/DObSettlement.sql'
\ir 'DObLandedCost/DObLandedCost.sql'
\ir 'DObCosting/DObCosting.sql'
\ir 'DObSalesInvoice/DObSalesInvoice.sql'
\ir 'DObAccountingNote/DObAccountingNote.sql'
\ir 'DObCollection/DObCollection.sql'
\ir 'DObInitialBalanceUpload/DObInitialBalanceUpload.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
