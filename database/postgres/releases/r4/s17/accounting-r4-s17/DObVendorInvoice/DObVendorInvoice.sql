ALTER TABLE IObVendorInvoiceDetailsDownpayment
DROP CONSTRAINT FK_IObInvoiceDetailsDownPayment_Downpayment;

ALTER TABLE IObVendorInvoiceDetailsDownpayment
    ADD CONSTRAINT FK_IObVendorInvoiceDetailsDownPayment_Payment FOREIGN KEY (downpaymentid) REFERENCES dobpaymentrequest (id) ON DELETE RESTRICT;


ALTER TABLE IObVendorInvoiceItem
DROP CONSTRAINT FK_IObInvoiceitem_MObItem;

ALTER TABLE IObVendorInvoiceItem
    ADD CONSTRAINT FK_IObVendorInvoiceItem_MObItem FOREIGN KEY (itemid) REFERENCES MObItem (id) ON DELETE RESTRICT;


ALTER TABLE IObVendorInvoiceItem
DROP CONSTRAINT FK_IObInvoiceItem_CObMeasure_BaseUnitOfMeasure;

ALTER TABLE IObVendorInvoiceItem
    ADD CONSTRAINT FK_IObVendorInvoiceItem_CObMeasure_BaseUnitOfMeasure FOREIGN KEY (baseunitofmeasureid) REFERENCES cobmeasure (id) ON DELETE RESTRICT;

ALTER TABLE IObVendorInvoiceItem
DROP CONSTRAINT FK_IObInvoiceItem_CObMeasure_OrderUnitOfMeasure;

ALTER TABLE IObVendorInvoiceItem
    ADD CONSTRAINT FK_IObVendorInvoiceItem_CObMeasure_OrderUnitOfMeasure FOREIGN KEY (orderunitofmeasureid) REFERENCES cobmeasure (id) ON DELETE RESTRICT;
