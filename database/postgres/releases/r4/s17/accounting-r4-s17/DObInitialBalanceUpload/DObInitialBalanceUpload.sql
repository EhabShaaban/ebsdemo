ALTER TABLE dobinitialbalanceupload ADD CONSTRAINT pk_dobinitialbalanceupload PRIMARY KEY (id);
ALTER TABLE dobinitialbalanceuploadjournalentry
    add constraint fk_dobinitialbalanceuploadjournalentry_dobinitialbalanceupload
        FOREIGN KEY (documentreferenceid) REFERENCES dobinitialbalanceupload (id) ON DELETE CASCADE;
ALTER TABLE iobinitialbalanceuploaditem add constraint fk_iobinitialbalanceuploaditem_cobcurrency
FOREIGN KEY (currencyid) REFERENCES cobcurrency (id) ON DELETE RESTRICT;
