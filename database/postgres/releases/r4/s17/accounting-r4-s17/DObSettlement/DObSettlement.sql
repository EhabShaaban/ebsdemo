ALTER TABLE DObSettlement
    ADD CONSTRAINT FK_DObSettlement_DObAccountingDocument FOREIGN KEY (id) REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;
