ALTER TABLE iobaccountingnotedetails
DROP CONSTRAINT iobaccountingnotedetails_businesspartnerid_fkey;

ALTER TABLE iobaccountingnotedetails
    ADD CONSTRAINT iobaccountingnotedetails_businesspartnerid_fkey FOREIGN KEY (businesspartnerid) REFERENCES mobbusinesspartner (id) ON DELETE RESTRICT;


ALTER TABLE iobaccountingnotedetails
DROP CONSTRAINT iobaccountingnotedetails_currencyid_fkey;

ALTER TABLE iobaccountingnotedetails
    ADD CONSTRAINT iobaccountingnotedetails_currencyid_fkey FOREIGN KEY (currencyid) REFERENCES cobcurrency (id) ON DELETE RESTRICT;
