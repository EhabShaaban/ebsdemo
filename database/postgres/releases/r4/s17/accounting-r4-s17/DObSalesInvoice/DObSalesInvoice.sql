ALTER TABLE iobsalesinvoicebusinesspartner
DROP CONSTRAINT fk_iobsalesinvoicebusinesspartner_paymenttermid;

ALTER TABLE iobsalesinvoicebusinesspartner
    ADD CONSTRAINT fk_iobsalesinvoicebusinesspartner_paymenttermid FOREIGN KEY (paymentTermId) REFERENCES cobpaymentterms (id) ON DELETE RESTRICT;

ALTER TABLE iobsalesinvoicebusinesspartner
DROP CONSTRAINT fk_iobsalesinvoicebusinesspartner_currencyid;

ALTER TABLE iobsalesinvoicebusinesspartner
    ADD CONSTRAINT fk_iobsalesinvoicebusinesspartner_currencyid FOREIGN KEY (currencyid) REFERENCES cobcurrency (id) ON DELETE RESTRICT;

ALTER TABLE iobsalesinvoicebusinesspartner
DROP CONSTRAINT fk_iobsalesinvoicebusinesspartner_dosalesorder;

ALTER TABLE iobsalesinvoicebusinesspartner
    ADD CONSTRAINT fk_iobsalesinvoicebusinesspartner_dosalesorder FOREIGN KEY (salesorderid) REFERENCES dobsalesorder (id) ON DELETE RESTRICT;

ALTER TABLE iobsalesinvoicecustomerbusinesspartner
DROP CONSTRAINT fk_iobsalesinvoicebusinesspartner_customerid;

ALTER TABLE iobsalesinvoicecustomerbusinesspartner
    ADD CONSTRAINT fk_iobsalesinvoicebusinesspartner_customerid FOREIGN KEY (customerid) REFERENCES mobcustomer (id) ON DELETE RESTRICT;

ALTER TABLE iobsalesinvoicepostingdetails
    ADD CONSTRAINT FK_iobsalesinvoicepostingdetails_refInstanceId FOREIGN KEY (refinstanceid) REFERENCES DObSalesInvoice (id) ON DELETE CASCADE;


ALTER TABLE iobsalesinvoiceitem
    ADD CONSTRAINT fk_iobsalesinvoiceitem_baseunit FOREIGN KEY (baseunitofmeasureid) REFERENCES cobmeasure (id) ON DELETE RESTRICT;

