CREATE VIEW IObSalesInvoiceAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               customer.code
               )
           when accountDetails.subledger = 'Taxes' AND accountDetails.accountingEntry = 'CREDIT' then (
               lobtaxinfo.code
               )
           end                    as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               customeraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'Taxes' AND accountDetails.accountingEntry = 'CREDIT' then (
               taxesaccountingdetails.glSubAccountId
               )
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               customer.name
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Taxes'
               then (lobtaxinfo.name)
           end                    as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join dobaccountingdocument on dobaccountingdocument.id = accountDetails.refinstanceid
         left join DObSalesInvoice on dobaccountingdocument.id = DObSalesInvoice.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join iobaccountingdocumentcustomeraccountingdetails customeraccountingdetails
                   on customeraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumenttaxaccountingdetails taxesaccountingdetails
                   on taxesaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         left join masterdata customer
                   on customer.id = customeraccountingdetails.glSubAccountId and customer.objecttypecode = '3'
         left join lobtaxinfo on lobtaxinfo.id = taxesaccountingdetails.glSubAccountId
WHERE accountDetails.objecttypecode = 'SalesInvoice';
