DROP VIEW IF EXISTS IObPaymentRequestApproverGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestApprovalCycleGeneralModel;
DROP TABLE iobpaymentrequestapprover;
DROP TABLE iobpaymentrequestapprovalcycle;
DROP TABLE iobpaymentrequestaccountdetails;


ALTER TABLE iobpaymentrequestpaymentdetails
    ADD CONSTRAINT fk_iobpaymentrequestpaymentdetails_currencyid FOREIGN KEY (currencyid) REFERENCES cobcurrency (id) ON DELETE RESTRICT;

