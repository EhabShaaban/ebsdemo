ALTER TABLE TObGoodsReceiptStoreTransaction
    ADD CONSTRAINT FK_TObGoodsReceiptStoreTransaction_DObGoodsReceipt FOREIGN KEY (documentReferenceId)
        REFERENCES DObInventoryDocument (id) ON DELETE RESTRICT;

ALTER TABLE tobgoodsissuestoretransaction
    ADD CONSTRAINT FK_TObGoodsReceiptStoreTransaction_DObGoodsIssue FOREIGN KEY (documentReferenceId)
        REFERENCES DObInventoryDocument (id) ON DELETE RESTRICT;

ALTER TABLE tobinitialstockuploadstoretransaction
    ADD CONSTRAINT FK_TObGoodsReceiptStoreTransaction_DObInitialstockupload FOREIGN KEY (documentReferenceId)
        REFERENCES DObInventoryDocument (id) ON DELETE RESTRICT;






