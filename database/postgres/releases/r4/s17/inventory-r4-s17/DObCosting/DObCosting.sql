ALTER TABLE iobcostingdetails
    add constraint fk_dobgoodsreceipt_iobcostingdetails
        FOREIGN KEY (goodsreceiptid) REFERENCES dobgoodsreceiptpurchaseorder (id) ON DELETE RESTRICT;
