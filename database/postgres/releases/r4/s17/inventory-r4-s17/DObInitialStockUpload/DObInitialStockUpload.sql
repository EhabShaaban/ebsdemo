ALTER TABLE tobinitialstockuploadstoretransaction
    add constraint fk_tobinitialstockuploadstoretransaction_dobISU
        FOREIGN KEY (documentreferenceid) REFERENCES dobinventorydocument (id) ON DELETE CASCADE;
