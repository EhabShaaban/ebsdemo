\ir 'DObGoodsReceipt/DObGoodsReceipt.sql'
\ir 'DObGoodsIssue/DObGoodsIssue.sql'
\ir 'DObCosting/DObCosting.sql'
\ir 'StoreTransaction/TObStoreTransaction.sql'
\ir 'DObInitialStockUpload/DObInitialStockUpload.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
