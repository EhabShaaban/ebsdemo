ALTER TABLE IObGoodsIssueSalesInvoiceData
    add constraint FK_IObGoodsIssueSalesInvoiceData_refinstanceid
        FOREIGN KEY (refinstanceid) REFERENCES dobinventorydocument (id) ON DELETE CASCADE;


ALTER TABLE IObGoodsIssueSalesInvoiceItem
    ADD CONSTRAINT FK_iObGoodsIssueSalesInvoiceItem_refinstanceid
        FOREIGN KEY (refInstanceId) REFERENCES dobgoodsissuesalesinvoice (id) ON DELETE CASCADE;
