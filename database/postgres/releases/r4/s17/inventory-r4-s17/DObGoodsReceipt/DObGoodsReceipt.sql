ALTER TABLE iobgoodsreceiptpurchaseorderreceiveditemsdata
    ADD CONSTRAINT FK_iobGoodsReceiptPurchaseOrderReceivedItemsData_item FOREIGN KEY (itemId) REFERENCES mobitem (id) ON DELETE RESTRICT;

ALTER TABLE iobgoodsreceiptsalesreturnreceiveditemsdata
    ADD CONSTRAINT FK_iobGoodsReceiptSalesReturnReceivedItemsData_item FOREIGN KEY (itemId) REFERENCES mobitem (id) ON DELETE RESTRICT;

ALTER TABLE iobgoodsreceiptpurchaseorderdata
    ADD CONSTRAINT FK_DObGoodsReceiptPurchaseOrder_PurchaseOrderId FOREIGN KEY (purchaseorderid) REFERENCES dobpurchaseorder (id) ON DELETE RESTRICT;

ALTER TABLE iobgoodsreceiptsalesreturndata
    ADD CONSTRAINT FK_IObGoodsReceiptSalesReturn_refinstanceid
        FOREIGN KEY (refinstanceid) REFERENCES dobgoodsreceiptsalesreturn (id) ON DELETE CASCADE;

ALTER TABLE iobgoodsreceiptpurchaseorderdata
    ADD CONSTRAINT FK_IObGoodsReceiptPurchaseOrder_purchaseresponsibleid
        FOREIGN KEY (purchaseresponsibleid) REFERENCES ebsUser (id) ON DELETE RESTRICT;
