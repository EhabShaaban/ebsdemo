\ir 'accounting-r4-s17/DObJournalEntry/DObJournalEntry_Migration.sql'
\ir 'accounting-r4-s17/accounting-r4-s17.sql'
\ir 'masterdata-r4-s17/Customer/Customer_Migration.sql'
\ir 'masterdata-r4-s17/masterdata-r4-s17.sql'
\ir 'inventory-r4-s17/inventory-r4-s17.sql'
\ir 'order-r4-s17/order-r4-s17.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
