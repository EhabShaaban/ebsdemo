ALTER TABLE ioborderapprover
    ADD CONSTRAINT FK_IObOrderApprover_CObControlPoint FOREIGN KEY (controlpointid)
        REFERENCES cobcontrolpoint (id) ON DELETE RESTRICT;

ALTER TABLE iobordercycledates
    ADD CONSTRAINT FK_iobordercycledates_doborderdocument FOREIGN KEY (refinstanceid)
        REFERENCES doborderdocument (id) ON DELETE CASCADE;

ALTER TABLE ioborderdeliverydetails
    RENAME COLUMN deliverydateid TO deliverydate;

ALTER TABLE ioborderlinedetailsquantities
    ADD CONSTRAINT FK_ioborderlinedetailsquantities_cobmeasure FOREIGN KEY (orderunitid)
        REFERENCES cobmeasure (id) ON DELETE CASCADE;

