\ir 'accounting-r4-s7/accounting-r4-s7.sql'
\ir 'inventory-r4-s7/inventory-r4-s7.sql'
\ir 'access-rights-r4-s7/access-rights-r4-s7.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;