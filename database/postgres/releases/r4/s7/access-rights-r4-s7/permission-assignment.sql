INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GRViewer_Offset'),
        (select id from permission where permissionexpression = 'GoodsReceipt:ReadAccountingDetails'),
        '2021-04-04 11:53:03+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'GoodsReceipt - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'GRViewer'),
        (select id from permission where permissionexpression = 'GoodsReceipt:ReadAccountingDetails'),
        '2021-04-04 11:54:04+00');