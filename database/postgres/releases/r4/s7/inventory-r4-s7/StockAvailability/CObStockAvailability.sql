Create TABLE CObStockAvailability
(
    id               BIGSERIAL                                             NOT NULL,
    code             VARCHAR(1024)                                         NOT NULL,
    name             json,
    creationDate     TIMESTAMP                                             NOT NULL,
    modifiedDate     TIMESTAMP                                             NOT NULL,
    creationInfo     VARCHAR(1024)                                         NOT NULL,
    modificationInfo VARCHAR(1024)                                         NOT NULL,
    companyId        INT8 REFERENCES CObCompany (id) ON DELETE RESTRICT    NOT NULL,
    plantId          INT8 REFERENCES CObPlant (id) ON DELETE RESTRICT      NOT NULL,
    storehouseId     INT8 REFERENCES cobstorehouse (id) ON DELETE RESTRICT NOT NULL,
    businessUnitId   INT8 REFERENCES CObEnterprise (id) ON DELETE RESTRICT NOT NULL,
    itemId           INT8 REFERENCES MObItem (id) ON DELETE RESTRICT       NOT NULL,
    unitOfMeasureId  INT8 REFERENCES CObMeasure (id) ON DELETE RESTRICT    NOT NULL,
    stockType        varchar                                               NOT NULL,
    availableQuantity          DOUBLE PRECISION DEFAULT 0
)


