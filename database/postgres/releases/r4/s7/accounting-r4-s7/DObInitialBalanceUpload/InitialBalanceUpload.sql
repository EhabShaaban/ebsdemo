DROP TABLE IF EXISTS DObInitialBalanceUploadJournalEntry;
CREATE TABLE DObInitialBalanceUploadJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);