DROP VIEW If EXISTS IObPaymentRequestPaymentDetailsGeneralModel;

DROP FUNCTION IF EXISTS getJournalBalanceByCode(paymentForm varchar, businessUnitCode varchar,
                                                   companyCode varchar, currencyCode varchar, bankCode varchar,
                                                   bankAccountNo varchar,
                                                   treasuryCode varchar);

CREATE or REPLACE FUNCTION getJournalBalanceByCode(paymentForm varchar, businessUnitCode varchar,
                                                   companyCode varchar, currencyCode varchar, bankCode varchar,
                                                   bankAccountNo varchar,
                                                   treasuryCode varchar)
    RETURNS float4 AS
$$
BEGIN
    RETURN
        (
            case
                when paymentForm = 'BANK' then (Select balance
                                                from cobjournalbalance
                                                         left join cobfiscalperiod
                                                                   on cobfiscalperiod.id = cobjournalbalance.fiscalyearid
                                                where cobjournalbalance.code =
                                                      concat('BANK', businessUnitCode, companyCode,
                                                             currencyCode, cobfiscalperiod.code, bankCode,
                                                             bankAccountNo)
                                                  and cobfiscalperiod.currentstates like '%Active%')
                when paymentForm = 'CASH' then (Select balance
                                                from cobjournalbalance
                                                         left join cobfiscalperiod
                                                                   on cobfiscalperiod.id = cobjournalbalance.fiscalyearid
                                                where cobjournalbalance.code =
                                                      concat('TREASURY', businessUnitCode, companyCode,
                                                             currencyCode, cobfiscalperiod.code, treasuryCode)
                                                  and cobfiscalperiod.currentstates like '%Active%') end
            );

END ;
$$
    LANGUAGE PLPGSQL;



CREATE OR REPLACE FUNCTION getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(firstCurrency bigint, secondCurrency bigint)
    RETURNS table
            (
                currencyPrice    float,
                exchangerateId   bigint,
                firstcurrencyid  bigint,
                secondcurrencyid bigint
            )
AS
$$
BEGIN
    RETURN QUERY (
        (select firstvalue * secondvalue,
                cobexchangerate.id,
                cobexchangerate.firstcurrencyid,
                cobexchangerate.secondcurrencyid
         from cobexchangerate left join cobexchangeratetype type on cobexchangerate.typeid = type.id
         where cobexchangerate.firstcurrencyid = firstCurrency
           and cobexchangerate.secondcurrencyid = secondCurrency
        and type.isdefault = true
         ORDER BY cobexchangerate.id DESC
         LIMIT 1));

END;
$$
    LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS getDueDocumentCodeAccordingToPaymentType(paymentDetailsId BIGINT, dueDocumentType varchar);

CREATE or REPLACE FUNCTION getDueDocumentCodeAccordingToPaymentType(paymentDetailsId BIGINT, dueDocumentType varchar)
    RETURNS varchar AS
$$
BEGIN
    RETURN
        (
            case
                when dueDocumentType = 'INVOICE' then (Select vendorInvoice.code from dobaccountingdocument vendorInvoice left join iobpaymentrequestinvoicepaymentdetails
                    on vendorInvoice.id = iobpaymentrequestinvoicepaymentdetails.duedocumentid
                    WHERE iobpaymentrequestinvoicepaymentdetails.id = paymentDetailsId
                   AND vendorInvoice.objecttypecode = 'VendorInvoice'
                    )
                when dueDocumentType = 'CREDITNOTE'  then (Select creditNote.code from dobaccountingdocument creditNote left join iobpaymentrequestcreditnotepaymentdetails
                on creditNote.id = iobpaymentrequestcreditnotepaymentdetails.duedocumentid
                WHERE iobpaymentrequestcreditnotepaymentdetails.id = paymentDetailsId
                )
                when dueDocumentType = 'PURCHASEORDER'  then (Select doborderdocument.code from doborderdocument left join iobpaymentrequestpurchaseorderpaymentdetails

                                                                                                                   on doborderdocument.id = iobpaymentrequestpurchaseorderpaymentdetails.duedocumentid
                                                                                                                   WHERE iobpaymentrequestpurchaseorderpaymentdetails.id = paymentDetailsId)
                end
            );

END ;
$$
    LANGUAGE PLPGSQL;

CREATE Or Replace View DObPaymentRequestActivatePreparationGeneralModel AS
Select paymentRequest.id,
       paymentRequest.code                                                        as paymentRequestCode,
       businessUnit.code                                                          as businessUnitCode,
       company.code                                                               as companyCode,
       cobcurrency.code                                                           as currencyCode,
       cobbank.code                                                               as bankCode,
       iobcompanybankdetails.accountno                                            as bankAccountNo,
       cobtreasury.code                                                           as treasuryCode,
       details.netamount                                                          as paymentNetAmount,
       dobpaymentrequest.paymenttype                                              as paymenttype,
       details.dueDocumentType                                                    as dueDocumentType,
       getDueDocumentCodeAccordingToPaymentType(details.id, details.dueDocumentType   ) as dueDocumentCode,
    getJournalBalanceByCode(details.paymentform, businessUnit.code, company.code, cobcurrency.code,
                               cobbank.code,
                               iobcompanybankdetails.accountno, cobtreasury.code) as balance,
       latestexchangerate.currencyprice                                           as currencyprice,
       cobexchangerate.code                                                       as exchangerate
from dobaccountingdocument paymentRequest
         left join dobpaymentrequest on dobpaymentrequest.id =  paymentRequest.id
         left join iobaccountingdocumentcompanydata paymentCompanyData
                   on paymentRequest.id = paymentCompanyData.refinstanceid
         left join cobenterprise company on company.id = paymentCompanyData.companyid
         left join cobenterprise businessUnit on businessUnit.id = paymentCompanyData.purchaseunitid
         left join iobpaymentrequestpaymentdetails details on details.refinstanceid = paymentRequest.id
         left join cobcurrency on cobcurrency.id = details.currencyid
         left join iobcompanybankdetails on iobcompanybankdetails.id = details.bankaccountid
         left join cobbank on cobbank.id = iobcompanybankdetails.bankid
         left join cobtreasury on cobtreasury.id = details.treasuryid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = paymentCompanyData.companyid
         left join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(details.currencyid,
                                                                            companyData.currencyid) latestexchangerate
                   on latestexchangerate.firstcurrencyid = details.currencyid
                       and latestexchangerate.secondcurrencyid = companydata.currencyid
         left join cobexchangerate on cobexchangerate.id = latestexchangerate.exchangerateid
WHERE paymentRequest.objecttypecode = 'PaymentRequest'
  AND paymentCompanyData.objecttypecode = 'PaymentRequest'
  AND company.objecttypecode = '1'
  AND businessUnit.objecttypecode = '5';


 CREATE OR REPLACE VIEW IObPaymentRequestPaymentDetailsGeneralModel AS
SELECT pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                                                                       as businessPartnerCode,
       md.name                                                                       as businessPartnerName,
       pd.duedocumenttype,
       getduedocumentcodeaccordingtopaymenttype(pd.id, pd.duedocumenttype)                                           as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso                                                               as currencyISO,
       cobcurrency.name                                                              as currencyName,
       cobcurrency.code                                                              as currencyCode,
       companyCurrency.iso                                                           AS companyCurrencyIso,
       companyCurrency.code                                                          AS companyCurrencyCode,
       companyCurrency.id                                                            AS companyCurrencyId,
       pd.description,
       pd.paymentForm,
       IObCompanyBankDetails.id                                                      AS bankAccountId,
       companyBank.code                                                              AS bankAccountCode,
       companyBank.name                                                              AS bankAccountName,
       cobtreasury.code                                                              AS treasuryCode,
       cobtreasury.name                                                              AS treasuryName,
       CASE
           WHEN
               IObCompanyBankDetails.accountno IS NULL THEN NULL
           ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END AS bankAccountNumber,
       item.code                                                                     AS costFactorItemCode,
       item.name                                                                     AS costFactorItemName,

       pd.expectedDueDate                                                            AS expectedDueDate,
       pd.bankTransRef                                                               AS bankTransRef
FROM iobpaymentrequestpaymentdetails pd
         LEFT JOIN masterdata md on md.id = pd.businessPartnerId
         LEFT JOIN dobpaymentrequest pr on pr.id = pd.refinstanceid
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = pr.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
         LEFT JOIN cobcurrency on pd.currencyid = cobcurrency.id
         LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
         LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = pd.bankAccountId
         LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN MObItemGeneralModel item on item.id = pd.costFactorItemId
         LEFT JOIN cobtreasury on cobtreasury.id = pd.treasuryId;
