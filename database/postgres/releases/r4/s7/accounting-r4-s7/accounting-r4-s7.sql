\ir 'DObAccounting_Views_Drop.sql'
\ir 'GoodsReciept/GoodsReceiptAccounting.sql'
\ir 'GoodsReciept/GoodsReceiptAccounting_views.sql'
\ir 'DObNotesReceivables/Drop_Dependancy.sql'
\ir 'DObNotesReceivables/DObNotesReceivables.sql'
\ir 'DObNotesReceivables/DObNotesReceivables_Views.sql'
\ir '../masterdata-r4-s7/masterdata-r4-s7.sql'
\ir 'DObSettlement/DObSettlement_Views.sql'
\ir 'DObSalesInvoice/DObSalesInvoice_Views.sql'
\ir 'DObInitialBalanceUpload/InitialBalanceUpload.sql'
\ir 'JournalEntry/DObJournalEntry_Views.sql'
\ir 'DObPaymentRequest/DObPaymentRequest_Views.sql'
\ir 'Collection/DObCollection_Views.sql'
\ir 'permissions-migration/permissions.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;