CREATE OR REPLACE VIEW IObCollectionAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code       AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger         AS subLedger,
       account.code                     AS glAccountCode,
       account.name                     AS glAccountName,
       account.id                       AS glAccountId,
       accountDetails.amount,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'CREDIT' THEN (
               customer.code
               )
           WHEN accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'CREDIT' THEN (
               vendor.code
               )
           WHEN accountDetails.subledger = 'Notes_Receivables'
               AND accountDetails.accountingEntry = 'CREDIT' THEN (
               monetary.code
               )
           WHEN accountDetails.subledger = 'Banks' AND accountDetails.accountingEntry = 'DEBIT'
               THEN (concat(bankSubAccount.accountno, ' - ', bankCurrency.iso))
           WHEN accountDetails.subledger = 'Treasuries' AND accountDetails.accountingEntry = 'DEBIT'
               THEN (treasury.code)
           END                          AS glSubAccountCode,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'CREDIT' THEN
               (customeraccountingdetails.glSubAccountId)
           WHEN accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'CREDIT' THEN
               (vendoraccountingdetails.glSubAccountId)
           WHEN accountDetails.subledger = 'Notes_Receivables'
               AND accountDetails.accountingEntry = 'CREDIT' THEN
               (notesreceivableaccountingdetails.glSubAccountId)
           WHEN accountDetails.subledger = 'Banks' AND accountDetails.accountingEntry = 'DEBIT'
               THEN (bankaccountingdetails.glSubAccountId)
           WHEN accountDetails.subledger = 'Treasuries' AND accountDetails.accountingEntry = 'DEBIT'
               THEN (treasuryaccountingdetails.glSubAccountId)
           END                          AS glSubAccountId,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'CREDIT'
               THEN (customer.name)
           WHEN accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'CREDIT'
               THEN (vendor.name)
           WHEN accountDetails.subledger = 'Banks' AND accountDetails.accountingEntry = 'DEBIT'
               THEN (cobbank.name)
           WHEN accountDetails.subledger = 'Treasuries' AND accountDetails.accountingEntry = 'DEBIT'
               THEN (treasury.name) END AS glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         LEFT JOIN dobcollection c ON accountDetails.refinstanceid = c.id
         INNER JOIN dobaccountingdocument ON dobaccountingdocument.id = c.id
         LEFT JOIN hobglaccount account ON account.id = accountDetails.glaccountid
         LEFT JOIN iobaccountingdocumentcustomeraccountingDetails customeraccountingdetails
                   ON customeraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         LEFT JOIN iobaccountingdocumentvendoraccountingDetails vendoraccountingdetails
                   ON vendoraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         LEFT JOIN iobaccountingdocumentnotesreceivableaccountingdetails notesreceivableaccountingdetails
                   ON notesreceivableaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         LEFT JOIN iobaccountingdocumentbankaccountingdetails bankaccountingdetails
                   ON bankaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         LEFT JOIN iobaccountingdocumenttreasuryaccountingdetails treasuryaccountingdetails
                   ON treasuryaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         LEFT JOIN cobtreasury treasury ON treasuryaccountingdetails.glsubaccountid = treasury.id
         LEFT JOIN masterdata customer
                   ON customer.id = customeraccountingdetails.glSubAccountId AND
                      customer.objecttypecode = '3'
         LEFT JOIN masterdata vendor
                   ON vendor.id = vendoraccountingdetails.glSubAccountId AND
                      vendor.objecttypecode = '2'
         LEFT JOIN dobmonetarynotes
                   ON dobmonetarynotes.id = notesreceivableaccountingdetails.glSubAccountId
         LEFT JOIN dobaccountingdocument monetary on monetary.id = dobmonetarynotes.id
         LEFT JOIN IObCompanyBankDetails bankSubAccount
                   ON bankSubAccount.id = bankaccountingdetails.glsubaccountid
         LEFT JOIN cobcurrency bankCurrency on bankSubAccount.currencyId = bankCurrency.id
         LEFT JOIN cobbank ON cobbank.id = bankSubAccount.bankid;

CREATE OR REPLACE FUNCTION getRefDocumentCodeBasedOnType(collectionDetailsId BIGINT, refDocumentType VARCHAR)
    RETURNS VARCHAR AS
$$
BEGIN
    RETURN CASE
               WHEN refDocumentType = 'SALES_INVOICE' THEN
                   (SELECT code
                    FROM dobaccountingdocument
                    WHERE dobaccountingdocument.id =
                          (SELECT salesinvoiceid
                           FROM iobcollectionsalesinvoicedetails
                           WHERE iobcollectionsalesinvoicedetails.id = collectionDetailsId)
                      AND objecttypecode = 'SalesInvoice')
               WHEN refDocumentType = 'NOTES_RECEIVABLE' THEN
                   (SELECT code
                    FROM dobaccountingdocument JOIN dobmonetarynotes ON dobaccountingdocument.id = dobmonetarynotes.id
                    AND dobmonetarynotes.id =
                          (SELECT notesReceivableId
                           FROM IObCollectionNotesReceivableDetails
                           WHERE IObCollectionNotesReceivableDetails.id = collectionDetailsId))
               WHEN refDocumentType = 'SALES_ORDER' THEN
                   (SELECT code
                    FROM dobsalesorder
                    WHERE dobsalesorder.id =
                          (SELECT salesOrderId
                           FROM IObCollectionSalesOrderDetails
                           WHERE IObCollectionSalesOrderDetails.id = collectionDetailsId))
               WHEN refDocumentType = 'DEBIT_NOTE' THEN
                   (SELECT code
                    FROM dobaccountingdocument
                    WHERE dobaccountingdocument.id =
                          (SELECT accountingNoteId
                           FROM IObCollectionDebitNoteDetails
                           WHERE IObCollectionDebitNoteDetails.id = collectionDetailsId)
                      AND objecttypecode like '%DEBIT_NOTE%')
               WHEN refDocumentType = 'PAYMENT_REQUEST' THEN
                   (SELECT code
                    FROM dobaccountingdocument
                    WHERE dobaccountingdocument.id =
                          (SELECT paymentRequestId
                           FROM IObCollectionPaymentRequestDetails
                           WHERE IObCollectionPaymentRequestDetails.id = collectionDetailsId)
                      AND objecttypecode like '%PaymentRequest%')
        END;
END;
$$
    LANGUAGE PLPGSQL;
