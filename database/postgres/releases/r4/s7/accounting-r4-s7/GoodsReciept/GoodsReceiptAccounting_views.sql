CREATE VIEW IObGoodsReceiptAccountingDocumentAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       dobaccountingdocument.code,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger AS subLedger,
       account.code             AS glAccountCode,
       account.name             AS glAccountName,
       account.id               AS glAccountId,
       accountDetails.amount,
       dobOrder.code            as glSubAccountCode,
       dobOrder.id              as glSubAccountId

FROM IObAccountingDocumentAccountingDetails accountDetails
         left join dobgoodsreceiptaccountingdocument grAccountinDocument
                   on accountDetails.refinstanceid = grAccountinDocument.id
         left join dobaccountingdocument on dobaccountingdocument.id = grAccountinDocument.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join IObGoodsReceiptAccountingDocumentAccountingDetails graccountingdetails
                   on graccountingdetails.id = accountDetails.id
         left join doborderdocument dobOrder on dobOrder.id = graccountingdetails.glSubAccountId;


create view DobGoodsReceiptAccountingDocumentGeneralModel AS
select dobGoodsReceiptAccountingDocument.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.creationdate,
       dobaccountingdocument.modificationinfo,
       dobaccountingdocument.modifieddate,
       dobaccountingdocument.currentstates,
       concat(company.code, ' - ', company.name::json ->> 'en')           as companyCodeName,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name::json ->> 'en') as purchaseUnitCodeName

from dobGoodsReceiptAccountingDocument
         JOIN dobaccountingdocument on dobaccountingdocument.id = dobGoodsReceiptAccountingDocument.id
         LEFT JOIN IObAccountingDocumentCompanyData
                   ON IObAccountingDocumentCompanyData.refInstanceId = dobGoodsReceiptAccountingDocument.id
         LEFT JOIN cobenterprise purchaseUnit
                   ON IObAccountingDocumentCompanyData.purchaseUnitId = purchaseUnit.id AND
                      purchaseUnit.objecttypecode = '5'
         left join cobenterprise company
                   on IObAccountingDocumentCompanyData.companyid = company.id and company.objecttypecode = '1';


create view IObGoodsReceiptAccountingDocumentItemsGeneralModel AS
select IObGoodsReceiptAccountingDocumentItems.id,
       dobaccountingdocument.code                                     as userCode,
       iobgoodsreceiptaccountingdocumentitems.refinstanceid,
       iobgoodsreceiptaccountingdocumentitems.quantity,
       iobgoodsreceiptaccountingdocumentitems.stocktype,
       iobgoodsreceiptaccountingdocumentitems.tobeconsideredincostingper,
       iobgoodsreceiptaccountingdocumentitems.itemactualcost,
       concat(masterdata.code, ' - ', masterdata.name::json ->> 'en') as itemCodeName,
       concat(cobmeasure.code, ' - ', cobmeasure.name::json ->> 'en') as uomCodeName,
       iobgoodsreceiptaccountingdocumentitems.creationDate,
       iobgoodsreceiptaccountingdocumentitems.creationInfo,
       iobgoodsreceiptaccountingdocumentitems.modifiedDate,
       iobgoodsreceiptaccountingdocumentitems.modificationInfo

from IObGoodsReceiptAccountingDocumentItems
         left join masterdata
                   on IObGoodsReceiptAccountingDocumentItems.itemid = masterdata.id and masterdata.objecttypecode = '1'
         left join cobmeasure on IObGoodsReceiptAccountingDocumentItems.uomid = cobmeasure.id
         left join dobgoodsreceiptaccountingdocument
                   on IObGoodsReceiptAccountingDocumentItems.refinstanceid = dobGoodsReceiptAccountingDocument.id
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobGoodsReceiptAccountingDocument.id;

create view IObGoodsReceiptAccountingDocumentDetailsGeneralModel AS
select IObGoodsReceiptAccountingDocumentDetails.id,
       dobaccountingdocument.code as userCode,
       IObGoodsReceiptAccountingDocumentDetails.refinstanceid,
       IObGoodsReceiptAccountingDocumentDetails.creationDate,
       IObGoodsReceiptAccountingDocumentDetails.creationInfo,
       IObGoodsReceiptAccountingDocumentDetails.modifiedDate,
       IObGoodsReceiptAccountingDocumentDetails.modificationInfo,
       dobinventorydocument.code  as goodsReceiptCode,
       doborder.code              as purchaseOrderCode
from IObGoodsReceiptAccountingDocumentDetails
         left join DObGoodsReceiptPurchaseOrder
                   on IObGoodsReceiptAccountingDocumentDetails.goodsreceiptid =
                      DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         left join dobgoodsreceiptaccountingdocument
                   on IObGoodsReceiptAccountingDocumentDetails.refinstanceid =
                      dobGoodsReceiptAccountingDocument.id

         left join dobaccountingdocument on dobaccountingdocument.id = dobgoodsreceiptaccountingdocument.id

         left join iobgoodsreceiptpurchaseorderdata
                   on iobgoodsreceiptpurchaseorderdata.refinstanceid =
                      DObGoodsReceiptPurchaseOrder.id
         left join doborderdocument doborder
                   on iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id;