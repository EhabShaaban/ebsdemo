DROP TABLE IF EXISTS IObGoodsReceiptAccountingDetails;
DROP TABLE IF EXISTS IObGoodsReceiptAccountingDocumentAccountingDetails;

alter table dobGoodsReceiptAccountingDocument drop column companyid;
alter table dobGoodsReceiptAccountingDocument drop column purchaseunitid;
alter table dobGoodsReceiptAccountingDocument drop column refDocumentId;
alter table dobGoodsReceiptAccountingDocument drop column code;
alter table dobGoodsReceiptAccountingDocument drop column creationDate;
alter table dobGoodsReceiptAccountingDocument drop column modifiedDate;
alter table dobGoodsReceiptAccountingDocument drop column creationInfo;
alter table dobGoodsReceiptAccountingDocument drop column modificationInfo;
alter table dobGoodsReceiptAccountingDocument drop column currentStates;


CREATE TABLE IObGoodsReceiptAccountingDocumentAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES doborderdocument  (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);
