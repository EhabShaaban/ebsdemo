drop view if exists IObJournalEntryItemGeneralModel;
drop view if exists IObJournalEntryItemSubAccounts;

CREATE or REPLACE FUNCTION getSubAccountCodeByJournalItemIdAndType(journalItemId bigint, journalItemType varchar)
    RETURNS varchar AS
$$
BEGIN
    RETURN
        (SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitempurchaseordersubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitempurchaseordersubaccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                AND journalItemType = subaccountgeneralmodel.ledger
                                AND journalItemType = 'PO'
         where iobjournalentryitempurchaseordersubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemvendorsubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemvendorsubaccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                AND iobjournalentryitemvendorsubaccount.type =
                                    subaccountgeneralmodel.ledger
                                AND journalItemType = 'Vendors'
         where iobjournalentryitemvendorsubaccount.id = journalItemId
         UNION
         SELECT concat(iobcompanybankdetails.accountno, ' - ', currency.iso)
         FROM iobcompanybankdetails
                  LEFT JOIN cobcurrency currency on iobcompanybankdetails.currencyId = currency.id
                  RIGHT JOIN iobjournalentryitembanksubaccount
                             ON iobjournalentryitembanksubaccount.subaccountid =
                                iobcompanybankdetails.id
                                 AND journalItemType = 'Banks'
         WHERE iobjournalentryitembanksubaccount.id = journalItemId
         UNION
         SELECT cobtreasury.code
         FROM cobtreasury
                  RIGHT JOIN iobjournalentryitemtreasurysubaccount
                             ON iobjournalentryitemtreasurysubaccount.subaccountid = cobtreasury.id
                                 AND journalItemType = 'Treasuries'
         WHERE iobjournalentryitemtreasurysubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemtaxsubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemtaxsubaccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                AND journalItemType = subaccountgeneralmodel.ledger
                                AND journalItemType = 'Taxes'
         where iobjournalentryitemtaxsubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemcustomersubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemcustomersubaccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                AND iobjournalentryitemcustomersubaccount.type =
                                    subaccountgeneralmodel.ledger
                                AND journalItemType = 'Customers'
         where iobjournalentryitemcustomersubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemnotesreceivablesubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemnotesreceivablesubaccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                AND journalItemType = subaccountgeneralmodel.ledger
                                AND journalItemType = 'NotesReceivable'
         where iobjournalentryitemnotesreceivablesubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitempurchaseordersubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitempurchaseordersubaccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                and subaccountgeneralmodel.ledger = 'PO'
         where iobjournalentryitempurchaseordersubaccount.id = journalItemId);
END;
$$
    LANGUAGE PLPGSQL;

CREATE VIEW IObJournalEntryItemSubAccounts AS
SELECT JEI.id,
       JEI.type,
       CASE
           WHEN COALESCE(SubACC.code, treasur.code) IS NULL THEN CASE
                                                                     WHEN BankDetails.accountno is not null
                                                                         then concat(BankDetails.accountno, ' - ', curr.iso)
                                                                     else null end
           ELSE COALESCE(SubACC.code, treasur.code) END     as subaccountcode,
       COALESCE(SUBACC.name, treasur.name, bank.name)       as subaccountname,
       COALESCE(VENDSubACC.type, CustSubACC.type, JEI.type) as subledger
from iobjournalentryitem JEI
         left join iobjournalentryitempurchaseordersubaccount POSubACC on JEI.id = POSubACC.id
         left join iobjournalentryitemvendorsubaccount VENDSubACC on JEI.id = VENDSubACC.id
         left join iobjournalentryitemtaxsubaccount TaxSubACC on JEI.id = TaxSubACC.id
         left join iobjournalentryitemcustomersubaccount CustSubACC on JEI.id = CustSubACC.id
         left join iobjournalentryitemnotesreceivablesubaccount NotesSubACC
                   on JEI.id = NotesSubACC.id
         left join subaccountgeneralmodel SubACC
                   on (
                           (SubACC.subaccountid = POSubACC.subaccountid AND JEI.type = 'PO' AND
                            SubACC.ledger = JEI.type) OR
                           (SubACC.subaccountid = VENDSubACC.subaccountid AND
                            JEI.TYPE = 'Vendors' AND
                            SubACC.ledger = VENDSubACC.type) OR
                           (SubACC.subaccountid = TaxSubACC.subaccountid AND JEI.type = 'Taxes' AND
                            JEI.type = SubACC.ledger) OR
                           (SubACC.subaccountid = CustSubACC.subaccountid AND
                            JEI.type = 'Customers' AND
                            CustSubACC.type = SubACC.ledger) OR
                           SubACC.subaccountid = NotesSubACC.subaccountid AND
                           JEI.type = 'NotesReceivable'
                               AND SubACC.ledger = JEI.type)
         left join iobjournalentryitembanksubaccount BankSubACC
                   on JEI.id = BankSubACC.id and JEI.type = 'Banks'
         left join iobcompanybankdetails BankDetails on BankSubACC.subaccountid = BankDetails.id
         left join cobBank bank on BankDetails.bankid = bank.id
         left join cobcurrency curr on curr.id = BankDetails.currencyid
         left join iobjournalentryitemtreasurysubaccount TreasSubACC
                   on JEI.id = TreasSubACC.id and JEI.type = 'Treasuries'
         left join cobtreasury treasur on TreasSubACC.subaccountid = treasur.id;


CREATE VIEW IObJournalEntryItemGeneralModel AS
select JEI.id,
       JEI.creationdate,
       JEI.creationinfo,
       JEI.modifieddate,
       JEI.modificationinfo,
       JEI.refinstanceid,
       dobjournalentry.code,
       dobjournalentry.duedate,
       dobjournalentry.objecttypecode AS referencetype,
       SUB_ACC.subledger              as subledger,
       hobglaccountgeneralmodel.code  AS accountcode,
       hobglaccountgeneralmodel.name  AS accountname,
       SUB_ACC.subaccountcode         as subaccountcode,
       SUB_ACC.subaccountname         as subaccountname,
       JEI.debit,
       JEI.credit,
       curren.code                    AS currencycode,
       curren.iso                     AS currencyname,
       curren2.code                   AS companycurrencycode,
       curren2.iso                    AS companycurrencyname,
       JEI.currencyprice,
       purchaseunit.code              AS purchaseunitcode,
       purchaseunit.name              AS purchaseunitname,
       purchaseunit.name ->> 'en'     AS purchaseunitnameen,
       JEI.exchangerateid             AS exhangerateid,
       cobexchangerate.code           AS exchangeratecode
from iobjournalentryitem JEI
         LEFT JOIN dobjournalentry ON JEI.refinstanceid = dobjournalentry.id AND
                                      (dobjournalentry.objecttypecode = 'VendorInvoice' OR
                                       dobjournalentry.objecttypecode = 'PaymentRequest' OR
                                       dobjournalentry.objecttypecode = 'SalesInvoice' OR
                                       dobjournalentry.objecttypecode = 'Collection' OR
                                       dobjournalentry.objecttypecode = 'NotesReceivable' OR
                                       dobjournalentry.objecttypecode = 'LandedCost' OR
                                       dobjournalentry.objecttypecode = 'Settlement' OR
                                       dobjournalentry.objecttypecode = 'InitialBalanceUpload')
         left join hobglaccountgeneralmodel on (hobglaccountgeneralmodel.id = JEI.accountid)
         LEFT JOIN cobenterprise purchaseunit ON dobjournalentry.purchaseunitid = purchaseunit.id
         LEFT JOIN cobexchangerate ON cobexchangerate.id = JEI.exchangerateid
         left join cobcurrency curren on (JEI.currency = curren.id)
         left join cobcurrency curren2 on (JEI.companycurrency = curren2.id)
         left join IObJournalEntryItemSubAccounts SUB_ACC
                   on (SUB_ACC.id = JEI.id AND SUB_ACC.type = JEI.type);


CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode                              AS documentReferenceType,
       DObJournalEntry.dueDate,
       company.code                                                AS companyCode,
       company.name                                                AS companyName,
       purchaseUnit.code                                           AS purchaseUnitCode,
       purchaseUnit.name                                           AS purchaseUnitName,
       purchaseUnit.name:: json ->> 'en'                           AS purchaseUnitNameEn,
       (SELECT dobaccountingdocument.code
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObVendorInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'VendorInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObPaymentRequestJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSalesInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcollectionJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON dobcollectionJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcollectionJournalEntry.id
          AND dobaccountingdocument.objecttypecode in ('C_SI', 'C_NR', 'C_SO','C_DN')
        UNION
        SELECT dobaccountingdocument.code
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobmonetarynotes ON dobNotesReceivableJournalEntry.documentreferenceid =
                                               dobmonetarynotes.id
                 LEFT JOIN dobaccountingdocument ON dobmonetarynotes.id = dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM doblandedcostjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON doblandedcostjournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = doblandedcostjournalentry.id
          and dobaccountingdocument.objecttypecode = 'LandedCost'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSettlementJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSettlementJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSettlementJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'Settlement'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObInitialBalanceUploadJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObInitialBalanceUploadJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObInitialBalanceUploadJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'IBU'
       ) AS documentReferenceCode,

       fiscalPeriod.name::json ->> 'en'                            AS FiscalPeriod
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company
                   ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join CObFiscalPeriod fiscalPeriod
                   on DObJournalEntry.fiscalPeriodId = fiscalPeriod.id;

