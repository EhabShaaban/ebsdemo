DROP TABLE IObNotesReceivableCollectionRequestAccountingDetails;

INSERT INTO dobaccountingdocument (code, creationdate, modifieddate, creationinfo, modificationinfo,
                                   currentstates, objecttypecode)
SELECT code,
       creationdate,
       modifieddate,
       creationinfo,
       modificationinfo,
       currentstates,
       'MONETARY_NOTE_CHEQUE_RECEIVABLE'
FROM dobnotesreceivables;

INSERT INTO iobaccountingdocumentaccountingdetails (refinstanceid, creationdate, modifieddate,
                                                    creationinfo, modificationinfo, subledger,
                                                    accountingentry, glaccountid, amount,
                                                    objecttypecode)
SELECT (SELECT id
        FROM dobaccountingdocument
        WHERE dobaccountingdocument.code = dobnotesreceivables.code
          AND objecttypecode = 'MONETARY_NOTE_CHEQUE_RECEIVABLE'),
       details.creationdate,
       details.modifieddate,
       details.creationinfo,
       details.modificationinfo,
       'Notes_Receivables',
       'DEBIT',
       details.debitaccountid,
       iobnotesreceivablesdetails.amount,
       'MONETARY_NOTE_CHEQUE_RECEIVABLE'
FROM iobnotesreceivablesaccountingdetails details
         JOIN iobnotesreceivablesdetails
              ON details.refinstanceid = iobnotesreceivablesdetails.refinstanceid
         JOIN dobnotesreceivables ON dobnotesreceivables.id = details.refinstanceid;

INSERT INTO iobaccountingdocumentaccountingdetails (refinstanceid, creationdate, modifieddate,
                                                    creationinfo, modificationinfo, subledger,
                                                    accountingentry, glaccountid, amount,
                                                    objecttypecode)
SELECT (SELECT id
        FROM dobaccountingdocument
        WHERE dobaccountingdocument.code = dobnotesreceivables.code
          AND objecttypecode = 'MONETARY_NOTE_CHEQUE_RECEIVABLE'),
       details.creationdate,
       details.modifieddate,
       details.creationinfo,
       details.modificationinfo,
       'Local_Customer',
       'CREDIT',
       details.creditaccountid,
       iobnotesreceivablesdetails.amount,
       'MONETARY_NOTE_CHEQUE_RECEIVABLE'
FROM iobnotesreceivablesaccountingdetails details
         JOIN iobnotesreceivablesdetails
              ON details.refinstanceid = iobnotesreceivablesdetails.refinstanceid
         JOIN dobnotesreceivables ON dobnotesreceivables.id = details.refinstanceid;


DROP TABLE IObNotesReceivablesAccountingDetails;

DROP VIEW DObNotesReceivablesSimpleGeneralModel;

DROP VIEW DObNotesReceivablesGeneralModel;

DROP TABLE IObNotesReceivablesSalesInvoiceData;

INSERT INTO iobaccountingdocumentactivationdetails (refinstanceid, creationdate, modifieddate,
                                                    creationinfo, modificationinfo, accountant,
                                                    activationdate, duedate, objecttypecode)
SELECT (SELECT id
        FROM dobaccountingdocument
        WHERE dobaccountingdocument.code = dobnotesreceivables.code
          AND objecttypecode = 'MONETARY_NOTE_CHEQUE_RECEIVABLE'),
       details.creationdate,
       details.modifieddate,
       details.creationinfo,
       details.modificationinfo,
       'Admin',
       journalduedate,
       journalduedate,
       'MONETARY_NOTE_CHEQUE_RECEIVABLE'
FROM iobnotesreceivablespostingdetails details
         JOIN dobnotesreceivables ON dobnotesreceivables.id = details.refinstanceid;

DROP TABLE iobnotesreceivablespostingdetails;

INSERT INTO iobaccountingdocumentcompanydata (refinstanceid, creationdate, modifieddate,
                                              creationinfo, modificationinfo, purchaseunitid,
                                              companyid, objecttypecode)
SELECT (SELECT id
        FROM dobaccountingdocument
        WHERE dobaccountingdocument.code = dobnotesreceivables.code
          AND objecttypecode = 'MONETARY_NOTE_CHEQUE_RECEIVABLE'),
       creationdate,
       modifieddate,
       creationinfo,
       modificationinfo,
       purchaseunitid,
       companyid,
       'MONETARY_NOTE_CHEQUE_RECEIVABLE'
FROM dobnotesreceivables;

ALTER TABLE dobnotesreceivables
    DROP COLUMN companyid,
    DROP COLUMN purchaseunitid;


ALTER TABLE iobnotesreceivablesdetails
    ADD COLUMN customerId INT8 NOT NULL REFERENCES MObCustomer (id) ON DELETE RESTRICT DEFAULT 66,
    ADD COLUMN currencyId INT8 NOT NULL REFERENCES CObCurrency (id) ON DELETE RESTRICT DEFAULT 1;


UPDATE iobnotesreceivablesdetails
SET customerId = dobnotesreceivables.customerid,
    currencyId = dobnotesreceivables.currencyid
FROM dobnotesreceivables
WHERE iobnotesreceivablesdetails.refinstanceid = dobnotesreceivables.id;

-- Change Constraints for dobnotesreceivable

ALTER TABLE iobnotesreceivablesdetails
    DROP CONSTRAINT iobnotesreceivablesdetails_refinstanceid_fkey;

ALTER TABLE dobnotesreceivablejournalentry
    DROP CONSTRAINT fk_dobnotesreceivablejournalentry_dobnotesreceivables;

ALTER TABLE iobaccountingdocumentnotesreceivableaccountingdetails
    DROP CONSTRAINT iobaccountingdocumentnotesreceivableaccount_glsubaccountid_fkey;

ALTER TABLE iobcollectionnotesreceivabledetails
    DROP CONSTRAINT fk_iobcollectionrequestnotesreceivabledetails_notesreceivableid;


ALTER TABLE iobnotesreceivablesdetails
    ADD CONSTRAINT iobnotesreceivablesdetails_refinstanceid_fkey
        FOREIGN KEY (refinstanceid) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE dobnotesreceivablejournalentry
    ADD CONSTRAINT fk_dobnotesreceivablejournalentry_dobnotesreceivables
        FOREIGN KEY (documentReferenceId) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE iobaccountingdocumentnotesreceivableaccountingdetails
    ADD CONSTRAINT iobaccountingdocumentnotesreceivableaccount_glsubaccountid_fkey
        FOREIGN KEY (glsubaccountid) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE iobcollectionnotesreceivabledetails
    ADD CONSTRAINT fk_iobcollectionrequestnotesreceivabledetails_notesreceivableid
        FOREIGN KEY (notesreceivableid) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE ON UPDATE CASCADE;


UPDATE dobnotesreceivables
SET id = (SELECT id
          FROM dobaccountingdocument
          WHERE dobaccountingdocument.code = dobnotesreceivables.code
            AND objecttypecode = 'MONETARY_NOTE_CHEQUE_RECEIVABLE');


ALTER TABLE iobnotesreceivablesdetails
    DROP CONSTRAINT iobnotesreceivablesdetails_refinstanceid_fkey;

ALTER TABLE dobnotesreceivablejournalentry
    DROP CONSTRAINT fk_dobnotesreceivablejournalentry_dobnotesreceivables;

ALTER TABLE iobaccountingdocumentnotesreceivableaccountingdetails
    DROP CONSTRAINT iobaccountingdocumentnotesreceivableaccount_glsubaccountid_fkey;

ALTER TABLE iobcollectionnotesreceivabledetails
    DROP CONSTRAINT fk_iobcollectionrequestnotesreceivabledetails_notesreceivableid;


ALTER TABLE iobnotesreceivablesdetails
    ADD CONSTRAINT iobnotesreceivablesdetails_refinstanceid_fkey
        FOREIGN KEY (refinstanceid) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE;


ALTER TABLE dobnotesreceivablejournalentry
    ADD CONSTRAINT fk_dobnotesreceivablejournalentry_dobnotesreceivables
        FOREIGN KEY (documentReferenceId) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE;


ALTER TABLE iobaccountingdocumentnotesreceivableaccountingdetails
    ADD CONSTRAINT iobaccountingdocumentnotesreceivableaccount_glsubaccountid_fkey
        FOREIGN KEY (glsubaccountid) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE;


ALTER TABLE iobcollectionnotesreceivabledetails
    ADD CONSTRAINT fk_iobcollectionrequestnotesreceivabledetails_notesreceivableid
        FOREIGN KEY (notesreceivableid) REFERENCES dobnotesreceivables (id) ON DELETE CASCADE;


ALTER TABLE DObNotesReceivables
    ADD CONSTRAINT DObNotesReceivables_DObAccountingDocument
        FOREIGN KEY (id) REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;
--

ALTER TABLE dobnotesreceivables
    ADD COLUMN documentOwnerId INT8 NOT NULL REFERENCES ebsuser (id) DEFAULT 1,
    ADD COLUMN remaining       DOUBLE PRECISION,
    ADD COLUMN method          VARCHAR(1024) DEFAULT 'AS_PAYMENT';

ALTER TABLE dobnotesreceivables
    DROP COLUMN customerId,
    DROP COLUMN currencyId,
    DROP COLUMN creationDate,
    DROP COLUMN modifiedDate,
    DROP COLUMN creationInfo,
    DROP COLUMN modificationInfo,
    DROP COLUMN currentStates,
    DROP COLUMN source,
    DROP COLUMN code;

ALTER TABLE dobnotesreceivables
    RENAME TO dobmonetarynotes;
ALTER SEQUENCE dobnotesreceivables_id_seq RENAME TO dobmonetarynotes_id_seq;
ALTER TABLE iobnotesreceivablesdetails
    RENAME TO iobmonetarynotesdetails;
ALTER SEQUENCE iobnotesreceivablesdetails_id_seq RENAME TO iobmonetarynotesdetails_id_seq;


ALTER TABLE iobmonetarynotesdetails
    RENAME COLUMN notesinformation TO noteNumber;
ALTER TABLE iobmonetarynotesdetails
    RENAME COLUMN issuingbank TO noteBank;

ALTER TABLE iobmonetarynotesdetails
    DROP CONSTRAINT iobnotesreceivablesdetails_customerid_fkey;

ALTER TABLE iobmonetarynotesdetails
    RENAME COLUMN customerid TO businessPartnerId;

ALTER TABLE iobmonetarynotesdetails
    ADD CONSTRAINT iobnotesreceivablesdetails_mobbusinesspartner_fkey FOREIGN KEY (businessPartnerId) REFERENCES mobbusinesspartner (id) ON DELETE RESTRICT;

ALTER TABLE iobmonetarynotesdetails
    RENAME COLUMN refdocumenttype TO depot;

ALTER TABLE iobmonetarynotesdetails
    ADD COLUMN treasuryId INT8 REFERENCES cobtreasury (id) ON DELETE RESTRICT;

ALTER TABLE iobmonetarynotesdetails
    ADD COLUMN bankAccountId INT8 REFERENCES iobcompanybankdetails (id) ON DELETE RESTRICT;

CREATE TABLE IObMonetaryNotesReferenceDocuments
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES dobmonetarynotes (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    documentType     VARCHAR(1024)            NOT NULL,
    dueData          TIMESTAMP WITH TIME ZONE NOT NULL,
    amountToPay      DOUBLE PRECISION,
    PRIMARY KEY (id)
);


CREATE TABLE IObMonetaryNotesSalesInvoiceReferenceDocument
(
    id         BIGSERIAL NOT NULL REFERENCES IObMonetaryNotesReferenceDocuments (id) ON DELETE CASCADE,
    documentId INT8      NOT NULL REFERENCES dobsalesinvoice (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);


CREATE TABLE IObMonetaryNotesSalesOrderReferenceDocument
(
    id         BIGSERIAL NOT NULL REFERENCES IObMonetaryNotesReferenceDocuments (id) ON DELETE CASCADE,
    documentId INT8      NOT NULL REFERENCES dobsalesorder (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);


CREATE TABLE IObMonetaryNotesDebitNoteReferenceDocument
(
    id         BIGSERIAL NOT NULL REFERENCES IObMonetaryNotesReferenceDocuments (id) ON DELETE CASCADE,
    documentId INT8      NOT NULL REFERENCES dobaccountingnote (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);