DROP VIEW IF EXISTS DObNotesReceivablesGeneralModel;
CREATE VIEW DObNotesReceivablesGeneralModel AS
SELECT dobmonetarynotes.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.objecttypecode,
       MasterData.code                                          AS customerCode,
       MasterData.name                                          AS customerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
       dobmonetarynotes.remaining,
       dobmonetarynotes.documentownerid,
       userinfo.name                                            AS documentOwnerName,
       concat(Company.code, ' - ', Company.name::json ->> 'en') AS companyCodeName,
       Company.code                                             AS companyCode,
       iobmonetarynotesdetails.notenumber,
       iobmonetarynotesdetails.dueDate,
       iobmonetarynotesdetails.amount,
       CObCurrency.iso                                          AS currencyIso,
       iobmonetarynotesdetails.notebank,
       dobmonetarynotes.method
FROM dobmonetarynotes
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = dobmonetarynotes.id AND method = 'AS_PAYMENT' AND
                      objecttypecode LIKE '%_RECEIVABLE'
         LEFT JOIN userinfo ON dobmonetarynotes.documentownerid = userinfo.id
         LEFT JOIN ebsuser ON dobmonetarynotes.documentownerid = ebsuser.id
         LEFT JOIN iobmonetarynotesdetails
                   ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN CObEnterprise BusinessUnit
                   ON iobaccountingdocumentcompanydata.purchaseunitid = BusinessUnit.id AND
                      BusinessUnit.objecttypecode = '5'
         LEFT JOIN CObEnterprise Company
                   ON iobaccountingdocumentcompanydata.companyid = Company.id AND
                      Company.objecttypecode = '1'
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '3';