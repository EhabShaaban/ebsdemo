CREATE OR REPLACE VIEW DObVendorInvoicePostPreparationGeneralModel AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       latestexchangerate.exchangerateid as exchangerateid,
       latestexchangerate.currencyprice  as currencyprice
from dobaccountingdocument
         inner join iobaccountingdocumentcompanydata
                    on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
                        AND iobaccountingdocumentcompanydata.objecttypecode = 'VendorInvoice'
         inner join iobenterprisebasicdata companydata
                    on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         inner join IObVendorInvoicePurchaseOrder
                    on dobaccountingdocument.id = iobvendorinvoicepurchaseorder.refinstanceid
         inner join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(IObVendorInvoicePurchaseOrder.currencyid,
                                                                             companydata.currencyid) latestexchangerate
                    on latestexchangerate.firstcurrencyid = IObVendorInvoicePurchaseOrder.currencyid
                        and latestexchangerate.secondcurrencyid = companydata.currencyid
                        and dobaccountingdocument.objecttypecode = 'VendorInvoice';