

CREATE OR REPLACE VIEW DObSalesInvoiceGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.modificationInfo,
       creationuserinfo.name                                                AS creationInfoName,
       dobsalesorder.code                                                   AS salesOrder,
       cobcompanygeneralmodel.currencyiso                                   AS companyCurrencyIso,
       cobsalesinvoice.code                									AS invoiceTypeCode,
       cobsalesinvoice.name  												AS invoiceType,
       purchaseUnit.code 													AS purchaseUnitCode,
       purchaseUnit.name 													AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' 									AS purchaseUnitNameEn,
       company.code 														AS companyCode,
       company.name 														AS companyName,
       masterdata.code                                                      AS customerCode,
       masterdata.name                                                      AS customer,
       dobsalesinvoice.documentOwnerId                                      AS documentOwnerId,
       userinfo.name 														AS documentOwner,
       ebsuser.username 													AS documentOwnerUserName,
       siSummary.remaining					 		 						AS remaining
FROM DObSalesInvoice
         LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN iobsalesinvoicebusinesspartner
                   ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
         LEFT JOIN iobsalesinvoicecustomerbusinesspartner
                   ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
         LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
         left join ebsuser creationebsuser on creationebsuser.username = dobaccountingdocument.creationinfo
         left join userinfo creationuserinfo on creationuserinfo.id = creationebsuser.id
         left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesOrderId
         left join cobcompanygeneralmodel
                   on cobcompanygeneralmodel.id = iobsalesinvoicecompanydata.companyId
         left join cobsalesinvoice on cobsalesinvoice.id = dobsalesinvoice.typeid
         left join cobenterprise purchaseUnit on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid
         left join cobenterprise company on company.id = iobsalesinvoicecompanydata.companyid
         left join userinfo on  userinfo.id = dobsalesinvoice.documentOwnerId
         left join ebsuser on  ebsuser.id = dobsalesinvoice.documentOwnerId
         left join iobsalesinvoicesummary siSummary
         			on siSummary.refinstanceId = DObSalesInvoice.id;


