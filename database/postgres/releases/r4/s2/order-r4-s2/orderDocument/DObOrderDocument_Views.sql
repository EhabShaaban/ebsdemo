CREATE OR REPLACE VIEW DObPurchaseOrderGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       doborderdocument.documentownerid,
       userinfo.name                      AS documentOwnerName,
       iobordercompany.businessunitid     AS purchaseUnitId,
       businessUnit.code                  AS purchaseUnitCode,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
        iobpurchaseorderfulfillervendor.vendorid,
        vendor.code                        AS vendorCode,
        vendor.name                        AS vendorName,
        CASE WHEN doborderdocument.objecttypecode = 'SERVICE_PO' THEN servicePoCurrency.code ELSE cobcurrency.code END AS currencyCode,
        CASE WHEN doborderdocument.objecttypecode = 'SERVICE_PO' THEN servicePoCurrency.iso ELSE cobcurrency.iso END AS currencyISO,
        CASE WHEN doborderdocument.objecttypecode = 'SERVICE_PO' THEN servicePoCurrency.name ELSE cobcurrency.name END AS currencyName,
        refDocument.code                   As refDocumentCode,
        refDocument.objecttypecode         As refDocumentType
        FROM doborderdocument
        JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
        LEFT JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
        LEFT JOIN iobpurchaseorderfulfillervendor
        ON dobpurchaseorder.id = iobpurchaseorderfulfillervendor.refinstanceid
        LEFT JOIN masterdata vendor
        ON iobpurchaseorderfulfillervendor.vendorid = vendor.id
        LEFT JOIN cobenterprise businessUnit on iobordercompany.businessunitid = businessUnit.id
        JOIN userinfo ON doborderdocument.documentownerid = userinfo.id
        JOIN ebsuser ON doborderdocument.documentownerid = ebsuser.id
        left join ioborderpaymenttermsdetails
        on dobpurchaseorder.id = ioborderpaymenttermsdetails.refinstanceid
        LEFT JOIN ioborderpaymentdetails
            ON dobpurchaseorder.id = ioborderpaymentdetails.refinstanceid
        left join cobcurrency on ioborderpaymenttermsdetails.currencyid = cobcurrency.id
        left join cobcurrency servicePoCurrency on ioborderpaymentdetails.currencyid = servicePoCurrency.id
        left join doborderdocument refDocument
        on iobpurchaseorderfulfillervendor.referencepoid = refDocument.id;