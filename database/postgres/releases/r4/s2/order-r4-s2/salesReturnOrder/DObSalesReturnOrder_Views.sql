DROP VIEW IF EXISTS IObSalesReturnOrderTaxGeneralModel;
DROP VIEW IF EXISTS IObSalesReturnOrderSummaryGeneralModel;
DROP VIEW IF EXISTS DObSalesReturnOrderGeneralModel;

CREATE VIEW IObSalesReturnOrderTaxGeneralModel AS
SELECT iobsalesreturnordertax.id,
       iobsalesreturnordertax.creationDate,
       iobsalesreturnordertax.modifiedDate,
       iobsalesreturnordertax.creationInfo,
       iobsalesreturnordertax.modificationInfo,
       iobsalesreturnordertax.refinstanceid,
       dobsalesreturnorder.code AS salesReturnOrderCode,
       iobsalesreturnordertax.code,
       iobsalesreturnordertax.name,
       iobsalesreturnordertax.percentage,
       (SELECT sum(iobsalesreturnorderitemgeneralmodel.totalamount) *
               (iobsalesreturnordertax.percentage / 100::double precision)
        FROM iobsalesreturnorderitemgeneralmodel
        WHERE iobsalesreturnorderitemgeneralmodel.refinstanceid = dobsalesreturnorder.id) AS amount
FROM iobsalesreturnordertax
         LEFT JOIN dobsalesreturnorder
                   ON dobsalesreturnorder.id = iobsalesreturnordertax.refInstanceId;


CREATE OR REPLACE FUNCTION getSalesReturnOrderTotalAmountBeforeTaxes(orderId bigint)
    RETURNS DOUBLE PRECISION AS
$$
BEGIN
    RETURN (
        (SELECT CASE WHEN SUM(totalamount) IS NULL THEN 0 ELSE SUM(totalamount) END
         FROM iobsalesreturnorderitemgeneralmodel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION getSalesReturnOrderTotalTaxes(orderId bigint)
    RETURNS DOUBLE PRECISION AS
$$
BEGIN
    RETURN (
        (SELECT CASE WHEN SUM(amount) IS NULL THEN 0 ELSE SUM(amount) END
         FROM iobsalesreturnordertaxgeneralmodel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;


CREATE VIEW IObSalesReturnOrderSummaryGeneralModel AS
SELECT dobsalesreturnorder.id,
       dobsalesreturnorder.code                                    AS orderCode,
       getSalesReturnOrderTotalAmountBeforeTaxes(dobsalesreturnorder.id) AS totalAmountBeforeTaxes,
       getSalesReturnOrderTotalTaxes(dobsalesreturnorder.id)             AS totalTaxes,
       getSalesReturnOrderTotalAmountBeforeTaxes(dobsalesreturnorder.id)
           + getSalesReturnOrderTotalTaxes(dobsalesreturnorder.id)       AS totalAmountAfterTaxes
FROM dobsalesreturnorder;


CREATE VIEW DObSalesReturnOrderGeneralModel AS
SELECT dobsalesreturnorder.id,
       dobsalesreturnorder.code,
       dobsalesreturnorder.creationDate,
       dobsalesreturnorder.modifiedDate,
       dobsalesreturnorder.creationInfo,
       dobsalesreturnorder.modificationInfo,
       dobsalesreturnorder.currentStates,
       cobsalesreturnordertype.code       AS salesReturnOrderTypeCode,
       cobsalesreturnordertype.name       AS salesReturnOrderTypeName,
       businessunit.code                  AS purchaseUnitCode,
       businessunit.name                  AS purchaseUnitName,
       businessunit.name::json ->> 'en'   AS purchaseUnitNameEn,
        dobaccountingdocument.code         AS salesInvoiceCode,
        masterdata.code                    AS customerCode,
        masterdata.name                    AS customerName,
        documentownergeneralmodel.userid   AS documentOwnerId,
        documentownergeneralmodel.username AS documentOwnerName,
        company.code                       AS companyCode,
        company.name                       AS companyName,
        getSalesReturnOrderTotalAmountBeforeTaxes(dobsalesreturnorder.id)
        + getSalesReturnOrderTotalTaxes(dobsalesreturnorder.id)       AS amount,
        cobcurrency.iso        AS currencyIso,
        cobcurrency.code                                                           as currencyCode,
        cobcurrency.name                                                           as currencyName
        FROM dobsalesreturnorder
        LEFT JOIN cobsalesreturnordertype
        ON dobsalesreturnorder.salesreturnordertypeid = cobsalesreturnordertype.id
        LEFT JOIN iobsalesreturnordercompanydata companydata
        on dobsalesreturnorder.id = companydata.refinstanceid
        LEFT JOIN cobenterprise company ON companyData.companyid = company.id
        LEFT JOIN iobsalesreturnorderdetails
        on dobsalesreturnorder.id = iobsalesreturnorderdetails.refinstanceid
        LEFT JOIN cobenterprise businessunit ON companydata.businessunitid = businessunit.id
        AND businessunit.objecttypecode = '5'
        LEFT JOIN mobcustomer ON iobsalesreturnorderdetails.customerid = mobcustomer.id
        LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
        LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
        LEFT JOIN dobaccountingdocument
        ON iobsalesreturnorderdetails.salesinvoiceid = dobaccountingdocument.id
        AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        LEFT JOIN documentownergeneralmodel
        ON dobsalesreturnorder.documentownerid = documentownergeneralmodel.userid AND
        documentownergeneralmodel.objectname = 'SalesReturnOrder'
        LEFT JOIN cobcurrency ON cobcurrency.id = iobsalesreturnorderdetails.currencyId;

