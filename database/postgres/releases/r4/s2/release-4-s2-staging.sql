\ir './masterdata-r4-s2/masterdata-r4-s2.sql'
\ir './accounting-r4-s2/accounting-r4-s2.sql'
\ir './order-r4-s2/order-r4-s2.sql'
\ir './system-data/system-data-4-s2.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
