UPDATE public.cobexchangerate exRate
SET typeid = (
				SELECT id 
				FROM CObExchangeRateType exRateType
				WHERE exRateType.code = exRate.type
			);
