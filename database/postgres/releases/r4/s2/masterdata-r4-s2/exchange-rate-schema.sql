
DROP TABLE IF EXISTS CObExchangeRateType;
CREATE TABLE CObExchangeRateType
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentstates    VARCHAR(1024) NOT NULL,
    name             json          not null,
	isDefault		 Boolean	   DEFAULT False,	
    PRIMARY KEY (id)
);

ALTER TABLE cobexchangerate	ADD COLUMN typeId INT8;
ALTER TABLE cobexchangerate
	ADD CONSTRAINT FK_typeId_cobexchangerate_cobexchangeratetype FOREIGN KEY (typeId) REFERENCES cobexchangeratetype (id) ON DELETE RESTRICT;                   

CREATE or REPLACE VIEW CObExchangeRateGeneralModel AS
SELECT cobexchangerate.id,
       cobexchangerate.code,
       cobexchangerate.creationDate,
       cobexchangerate.modifiedDate,
       cobexchangerate.creationInfo,
       cobexchangerate.modificationInfo,
       cobexchangerate.currentstates,
       cobexchangerate.validFrom,
       cobexchangerate.firstValue,
       cobexchangerate.secondValue,
       cobexchangerate.firstCurrencyId,
       cobexchangerate.secondCurrencyId,
       CObExchangeRateType.code 			AS exchangetype,
       cobexchangerate.name,
       firstCurrency.iso    				AS firstCurrencyIso,
       firstCurrency.code   				AS firstCurrencyCode,
       secondCurrency.iso   				AS secondCurrencyIso,
       secondCurrency.code  				AS secondCurrencyCode,
       CObExchangeRateType.name 			AS exchangetypename
FROM cobexchangerate
         LEFT JOIN CObCurrency firstCurrency
                   ON cobexchangerate.firstCurrencyId = firstCurrency.id
         LEFT JOIN CObCurrency secondCurrency
                   ON cobexchangerate.secondCurrencyId = secondCurrency.id
         LEFT JOIN CObExchangeRateType
         		   ON cobexchangerate.typeId = CObExchangeRateType.id;

-----------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION getLatestCurrencyPrice(firstCurrency bigint, secondCurrency bigint)
    RETURNS table
            (
                currencyPrice    float,
                exchangerateId   bigint,
                firstcurrencyid  bigint,
                secondcurrencyid bigint
            )
AS
$$
BEGIN
    RETURN QUERY (
        (select firstvalue * secondvalue,
                cobexchangerate.id,
                cobexchangerate.firstcurrencyid,
                cobexchangerate.secondcurrencyid
         from 
         	cobexchangerate join cobexchangeratetype
         	on cobexchangerate.typeid = cobexchangeratetype.id 
         	and cobexchangeratetype.isdefault is true
         where 
         	cobexchangerate.firstcurrencyid = firstCurrency
           	and cobexchangerate.secondcurrencyid = secondCurrency
         
         ORDER BY validfrom DESC
         LIMIT 1));

END;
$$
    LANGUAGE PLPGSQL;
