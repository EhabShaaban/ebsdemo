DROP VIEW IF EXISTS DObAccountingNoteGeneralModel;
CREATE VIEW DObAccountingNoteGeneralModel AS
SELECT DObAccountingNote.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       DObAccountingNote.type,
       IObAccountingNoteDetails.amount,
       DObAccountingNote.remaining,
       dobaccountingdocument.objecttypecode,
       CASE
           WHEN businessPartner.objecttypecode = '2'
               THEN cast('{"ar": "مورد", "en": "Vendor"}' AS JSON)
           ELSE CASE
                    WHEN businessPartner.objecttypecode = '3'
                        THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON) END
           END                                                                            AS businessPartnerType,
       businessPartner.name                                                               AS businessPartnerName,
       businessPartner.code                                                               AS businessPartnerCode,
       CASE
           WHEN dobaccountingdocument.objecttypecode = 'CREDIT_NOTE_BASED_ON_SALES_RETURN'
               THEN cast(
                   '{"ar": "طلب ارتجاع مبيعات", "en": "Sales Return Order"}' AS JSON) END AS referenceDocumentType,
       CASE
           WHEN dobaccountingdocument.objecttypecode = 'CREDIT_NOTE_BASED_ON_SALES_RETURN'
               THEN (SELECT code
                     FROM dobsalesreturnorder
                     WHERE id = refdocumentid) END                                        AS referenceDocumentCode,
       purchaseUnit.name                                                                  AS purchaseUnitName,
       purchaseUnit.name::json ->> 'en'                                                   AS purchaseUnitNameEn,
       purchaseUnit.code                                                                  AS purchaseUnitCode,
       cobcurrency.iso                                                                    AS currencyISO,
       cobcurrency.code                                                                   AS currencyCode,
       documentownergeneralmodel.username                                                 AS documentOwnerName,
       company.code                                                                       AS companycode,
       company.name                                                                       AS companyname,
       dobaccountingdocument.currentStates,
       IObAccountingDocumentActivationDetails.activationDate
FROM DObAccountingNote
         LEFT JOIN dobaccountingdocument ON DObAccountingNote.id = dobaccountingdocument.id
         LEFT JOIN IObAccountingNoteDetails
                   ON DObAccountingNote.id = IObAccountingNoteDetails.refInstanceId
         LEFT JOIN IObAccountingDocumentActivationDetails
                   ON IObAccountingDocumentActivationDetails.refInstanceId = DObAccountingNote.id
         LEFT JOIN masterdata businessPartner
                   ON businessPartner.id = IObAccountingNoteDetails.businessPartnerId
         LEFT JOIN IObAccountingDocumentCompanyData
                   ON IObAccountingDocumentCompanyData.refInstanceId = DObAccountingDocument.id
         LEFT JOIN cobenterprise purchaseUnit
                   ON IObAccountingDocumentCompanyData.purchaseUnitId = purchaseUnit.id AND
                      purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobcurrency ON IObAccountingNoteDetails.currencyid = cobcurrency.id
         LEFT JOIN documentownergeneralmodel
                   ON DObAccountingNote.documentownerid = documentownergeneralmodel.userid AND
                      documentownergeneralmodel.objectname = 'AccountingNote'
         LEFT JOIN cobenterprise company
                   ON iobaccountingdocumentcompanydata.companyid = company.id
         LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id;