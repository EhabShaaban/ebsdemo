
ALTER TABLE IObPaymentRequestCreditNotePaymentDetails
    drop CONSTRAINT FK_iobpaymentrequestcreditnotepaymentdetails_iobpaymentrequestpaymentdetails;

ALTER TABLE IObPaymentRequestCreditNotePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestcreditnotepaymentdetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE CASCADE;

ALTER TABLE IObPaymentRequestCreditNotePaymentDetails
    drop CONSTRAINT FK_iobpaymentrequestcreditnotepaymentdetails_duedocument ;

ALTER TABLE IObPaymentRequestCreditNotePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestcreditnotepaymentdetails_duedocument FOREIGN KEY (dueDocumentId) REFERENCES DObAccountingNote (id) ON DELETE RESTRICT;

ALTER TABLE dobpaymentrequest
    ADD COLUMN remaining DOUBLE PRECISION

