CREATE OR REPLACE VIEW DObLandedCostGeneralModel AS
SELECT LC.id,
       AccDoc.code,
       AccDoc.creationdate,
       AccDoc.modifieddate,
       AccDoc.creationinfo,
       AccDoc.modificationinfo,
       AccDoc.currentstates,
       cobenterprise.code                AS purchaseUnitCode,
       cobenterprise.name                AS purchaseUnitName,
       cobenterprise.name::json ->> 'en' AS purchaseUnitNameEn,
       CObLandedCostType.code            AS typeCode,
       CObLandedCostType.name            AS typeName,
       doborderdocument.code             AS purchaseOrderCode
FROM DObLandedCost LC
         LEFT JOIN DObAccountingDocument AccDoc
                   ON LC.id = AccDoc.id AND AccDoc.objecttypecode = 'LandedCost'
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON LC.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         LEFT JOIN cobenterprise ON IObLandedCostCompanyData.purchaseUnitId = cobenterprise.id
         LEFT JOIN IObLandedCostDetails LCD ON LC.id = LCD.refinstanceid
         LEFT JOIN doborderdocument ON LCD.purchaseorderid = doborderdocument.id AND
                                       doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN CObLandedCostType ON CObLandedCostType.id = LC.typeId;

create or replace view IObLandedCostDetailsGeneralModel as
select ioblandedcostdetails.id,
       ioblandedcostdetails.refinstanceid,
       ioblandedcostdetails.creationdate,
       ioblandedcostdetails.creationinfo,
       ioblandedcostdetails.modificationinfo,
       ioblandedcostdetails.modifieddate,
       landedCost.code                                           as landedCostCode,
       dobaccountingdocument.code                                as vendorInvoiceCode,
       dobaccountingdocument.id                                  as vendorInvoiceId,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           dobaccountingdocument.objecttypecode) as invoiceAmount,
       doborderdocument.code                                     as purchaseOrderCode,
       doborderdocument.id                                       as purchaseOrderId,
       companyCurrency.iso                                       as companyCurrencyIso,
       invoiceCurrency.iso                                       as invoiceCurrencyIso,
       ioblandedcostdetails.damageStock,
       cobenterprise.code                                        as businessUnitCode,
       cobenterprise.name                                        as businessUnitName,
       concat(cobenterprise.code, ' ', '-', ' ',
              cobenterprise.name::json ->> 'en')                 as businessUnitCodeName,
       (select case
       when getAmountPaidViaAccountingDocument(dobaccountingdocument.code,
                                               dobaccountingdocument.objecttypecode) =
            0.0
           then (select avg(iobAccountingdocumentactivationdetails.currencyprice)
                 from iobAccountingdocumentactivationdetails
                          left JOIN iobpaymentrequestpaymentdetails
                                    on iobpaymentrequestpaymentdetails.refinstanceid =
                                       iobAccountingdocumentactivationdetails.refinstanceid
                          left join iobpaymentrequestinvoicepaymentdetails
                                    on iobpaymentrequestinvoicepaymentdetails.id =
                                       iobpaymentrequestpaymentdetails.id
                 where iobpaymentrequestinvoicepaymentdetails.duedocumentid =
                       ioblandedcostdetails.vendorinvoiceid
       )
       else (
       		 select latestexchangerate.currencyPrice 
       		 from getLatestCurrencyPrice(iobvendorinvoicepurchaseorder.currencyid, 
       		 							iobenterprisebasicdata.currencyid) latestexchangerate
       		)
       end )  													  as currencyPrice
from ioblandedcostdetails
         left join dobaccountingdocument landedCost
                   on ioblandedcostdetails.refinstanceid = landedCost.id
         left join doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join dobaccountingdocument
                   on ioblandedcostdetails.vendorinvoiceid = dobaccountingdocument.id

         left join iobordercompany on iobordercompany.refinstanceid = doborderdocument.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         left join iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = ioblandedcostdetails.vendorinvoiceid

         left join cobcurrency invoiceCurrency
                   on invoiceCurrency.id = iobvendorinvoicepurchaseorder.currencyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid
         left join doblandedcost on doblandedcost.id = ioblandedcostdetails.refinstanceid
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON landedCost.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         left join cobenterprise on cobenterprise.id = IObLandedCostCompanyData.purchaseUnitId;

