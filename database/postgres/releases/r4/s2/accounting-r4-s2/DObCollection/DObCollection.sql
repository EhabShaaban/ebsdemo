drop table if exists IObCollectionPaymentRequestDetails;
---------------------------------------------------------------------------------------------
create table IObCollectionPaymentRequestDetails
(
    id                BIGSERIAL NOT NULL,
    paymentRequestId  INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObCollectionPaymentRequestDetails
    ADD CONSTRAINT FK_IObCollectionPaymentRequestDetails_paymentRequestId FOREIGN KEY (paymentRequestId) REFERENCES dobpaymentrequest (id) ON DELETE restrict;

ALTER TABLE IObCollectionPaymentRequestDetails
    ADD CONSTRAINT FK_IObCollectionPaymentRequestDetails_id FOREIGN KEY (id) REFERENCES IObCollectionDetails (id) ON DELETE cascade;