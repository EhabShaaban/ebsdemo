\ir 'AccountingDocument/AccountingDocument_Drop_Dependencies.sql'
\ir 'AccountingDocument/AccountingDocument_Functions.sql'
\ir 'DObJournalEntry/DObJournalEntry_View.sql'
\ir 'DObPaymentRequest/DObPaymentRequest_Views.sql'
\ir 'DObSalesInvoice/DObSalesInvoice_Drop_Dependencies.sql'
\ir 'AccountingDocument/AccountingDocument_Views.sql'
\ir 'DObSalesInvoice/DObSalesInvoice.sql'
\ir 'DObSalesReturn/DObSalesReturn_Views.sql'
\ir 'DObSalesInvoice/DObSalesInvoice_Views.sql'
\ir 'DObLandedCost/DObLandedCost_Views.sql'
\ir 'DObVendorInvoice/DObVendorInvoice_Views.sql'
\ir '../inventory-r4-s14/DObGoodsReceipt/DObGoodsReceipt_Views.sql'
\ir 'DObCosting/Costing.sql'
\ir 'DObCosting/Costing_Views.sql'
\ir 'DObMonetaryNote/DObNotesReceivable.sql'
\ir 'DObMonetaryNote/DObMonetaryNote_Views.sql'
\ir 'JournalEntry/DObJournalEntry_Functions.sql'
\ir 'JournalEntry/DObJournalEntry_Views.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
