CREATE or REPLACE VIEW IObVendorInvoiceItemsGeneralModel AS
SELECT IObVendorInvoiceItem.id,
       vendorInvoice.code,
       item.code                                                                               as itemCode,
       item.id                                                                                 as itemId,
       item.name                                                                               as itemName,
       baseUnitOfMeasure.code                                                                  as baseUnitCode,
       baseUnitOfMeasure.symbol                                                                as baseUnitSymbol,
       orderUnitOfMeasure.code                                                                 as orderUnitCode,
       orderUnitOfMeasure.symbol                                                               as orderunitsymbol,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  AS conversionFactorToBase,
       ((IObVendorInvoiceItem.quantityinorderunit *
         getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                   IObVendorInvoiceItem.itemid)) * IObVendorInvoiceItem.price) as totalAmount,
       IObVendorInvoiceItem.creationDate,
       IObVendorInvoiceItem.modifiedDate,
       IObVendorInvoiceItem.creationInfo,
       IObVendorInvoiceItem.modificationInfo,
       IObVendorInvoiceItem.quantityinorderunit,
       IObVendorInvoiceItem.refinstanceid,
       IObVendorInvoiceItem.price,
       IObVendorInvoiceItem.price *
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  as orderUnitPrice

FROM IObVendorInvoiceItem
         left join masterdata item on IObVendorInvoiceItem.itemId = item.id
         left join cobmeasure orderUnitOfMeasure on IObVendorInvoiceItem.orderunitofmeasureid = orderUnitOfMeasure.id
         left join mobitem on IObVendorInvoiceItem.itemId = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument vendorInvoice on IObVendorInvoiceItem.refInstanceId = vendorInvoice.id
         left join dobvendorinvoice on vendorInvoice.id = dobvendorinvoice.id;

CREATE or REPLACE view VendorInvoiceItemWeight as
select invoiceItem.refinstanceid                                          as invoiceId,
       invoiceItem.code                                                   as invoiceCode,
       LCDetails.purchaseorderid                                          as purchaseorderid,
       purchaseOrder.code                                               as purchaseorderCode,
       dobinventorydocument.id                                            as goodsreceiptId,
       dobinventorydocument.code                                          as goodsreceiptCode,
       LC.code                                                            as landedCostCode,
       LC.id                                                              as landedCostId,
       LC.currentstates                                                   as landedCostState,
       invoiceItem.id                                                     as itemId,
       invoiceItem.itemcode                                               as itemCode,
       invoiceItem.totalamount /
       getInvoiceTotalAmountBeforeTaxes(invoiceItem.code, 'VendorInvoice') as itemWeight,
       case
           when
                   LC.currentstates like '%ActualValuesCompleted%'
               then LCSummary.actualtotalamount
           else
               LCSummary.estimatetotalamount
           end                                                            as landedCostTotal,
       invoiceItem.orderunitprice                                         as itemUnitPrice,
       (select case
                   when getAmountPaidViaAccountingDocument(vendorInvoice.code,
                                                           vendorInvoiceCompany.objecttypecode) =
                        0.0
                       then (select avg(iobAccountingdocumentactivationdetails.currencyprice)
                             from iobAccountingdocumentactivationdetails
                                      left JOIN iobpaymentrequestpaymentdetails
                                                on iobpaymentrequestpaymentdetails.refinstanceid =
                                                   iobAccountingdocumentactivationdetails.refinstanceid
                                      left join iobpaymentrequestinvoicepaymentdetails
                                                on iobpaymentrequestinvoicepaymentdetails.id =
                                                   iobpaymentrequestpaymentdetails.id
                             where iobpaymentrequestinvoicepaymentdetails.duedocumentid =
                                   LCDetails.vendorinvoiceid
                   )
                   else (
                       select latestexchangerate.currencyPrice
                       from getLatestCurrencyPrice(iobvendorinvoicepurchaseorder.currencyid,
                                                   iobenterprisebasicdata.currencyid) latestexchangerate
                   )
                   end ) as currencyPrice,
       invoiceItem.orderunitcode,
       invoiceItem.orderunitsymbol
from IObVendorInvoiceItemsGeneralModel invoiceItem
         left join ioblandedcostdetails LCDetails
                   on invoiceItem.refinstanceid = LCDetails.vendorinvoiceId
         left join dobaccountingdocument vendorInvoice
                   on vendorInvoice.id = LCDetails.vendorinvoiceId
         inner join dobvendorinvoice on dobvendorinvoice.id = vendorInvoice.id
         left join doborderdocument purchaseOrder on purchaseOrder.id = LCDetails.purchaseorderid
         left join iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = LCDetails.vendorinvoiceid
         left join iobaccountingdocumentcompanydata vendorInvoiceCompany on vendorInvoiceCompany.refinstanceid = LCDetails.vendorinvoiceId and vendorInvoiceCompany.objecttypecode = 'VendorInvoice'
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = vendorInvoiceCompany.companyid
         inner join dobaccountingdocument LC
                    on LCDetails.refinstanceid = LC.id and
                       LC.objecttypecode = 'LandedCost' and LC.currentstates not like '%Draft%'
         left join IObLandedCostFactorItemsSummary LCSummary on LCSummary.id = LC.id
         inner join iobgoodsreceiptpurchaseorderdata
                    on LCDetails.purchaseorderid = iobgoodsreceiptpurchaseorderdata.purchaseorderid
         inner join dobinventorydocument on dobinventorydocument.id = iobgoodsreceiptpurchaseorderdata.refinstanceid
    and dobinventorydocument.currentstates like '%Active%';

CREATE OR REPLACE VIEW DObVendorInvoiceItemAccountsPreparationGeneralModel AS
SELECT iobvendorinvoiceitem.id                     AS id,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       iobitemaccountinginfo.chartofaccountid   AS glaccountid,
       iobvendorinvoicepurchaseorder.purchaseorderid AS orderid,
       leafglaccount.leadger                    AS accountledger,
       iobvendorinvoiceitem.totalamount         AS amount
FROM dobaccountingdocument
         JOIN dobvendorinvoice ON dobaccountingdocument.id = dobvendorinvoice.id
         LEFT JOIN iobvendorinvoicepurchaseorder ON iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobvendorinvoiceitemsgeneralmodel iobvendorinvoiceitem ON iobvendorinvoiceitem.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobitemaccountinginfo ON iobitemaccountinginfo.refinstanceid = iobvendorinvoiceitem.itemid
         LEFT JOIN leafglaccount ON leafglaccount.id = iobitemaccountinginfo.chartofaccountid;

Drop View IF EXISTS DObVendorInvoiceCreditPreparationGeneralModel;
CREATE OR REPLACE VIEW DObVendorInvoiceJournalDetailPreparationGeneralModel AS
SELECT dobaccountingdocument.id ,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companyData.currencyid as companyCurrencyId,
       iobvendorinvoicepurchaseorder.currencyid as documentCurrencyId,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
       iobvendoraccountinginfo.chartofaccountid as glaccountId,
       iobvendorinvoicedetails.vendorid as glsubaccountId,
       leafglaccount.leadger as accountLedger,
       getInvoiceNetAmount(code, dobaccountingdocument.objecttypecode)             as amount
from dobaccountingdocument
         right join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         left join iobaccountingdocumentactivationdetails activationdetails
                   on dobaccountingdocument.id = activationdetails.refinstanceid
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
         left join iobvendorinvoicepurchaseorder on iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         left join iobvendorinvoicedetails on iobvendorinvoicedetails.refinstanceid = dobvendorinvoice.id
         left join iobvendoraccountinginfo on iobvendoraccountinginfo.refinstanceid = iobvendorinvoicedetails.vendorid
         left join leafglaccount on leafglaccount.id = iobvendoraccountinginfo.chartofaccountid;

CREATE or REPLACE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE WHEN invoiceSummary.totalremaining IS NULL THEN 0 ELSE invoiceSummary.totalremaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
                   ON dobInvoice.id = invoiceSummary.id;

CREATE OR REPLACE VIEW DObVendorInvoiceDebitPreparationGeneralModel AS
SELECT row_number() OVER ()                     AS id,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companydata.currencyid                   AS companycurrencyid,
       iobvendorinvoicepurchaseorder.currencyid AS documentcurrencyid,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
       iobitemaccountinginfo.chartofaccountid   AS glaccountid,
       CASE
           WHEN isgoodsinvoice(dobaccountingdocument.objecttypecode) THEN iobvendorinvoicepurchaseorder.purchaseorderid
           ELSE iobpurchaseorderfulfillervendor.referencepoid
           END                                  AS glsubaccountid,
       leafglaccount.leadger                    AS accountledger,
       getItemAmountAfterTaxes(dobvendorinvoice.id, iobvendorinvoiceitem.totalamount)                                 AS amount
FROM dobaccountingdocument
         JOIN dobvendorinvoice ON dobaccountingdocument.id = dobvendorinvoice.id
         LEFT JOIN iobaccountingdocumentactivationdetails activationdetails
                   ON dobaccountingdocument.id = activationdetails.refinstanceid
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata companydata
                   ON companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         LEFT JOIN iobvendorinvoicepurchaseorder ON iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON iobpurchaseorderfulfillervendor.refinstanceid = iobvendorinvoicepurchaseorder.purchaseorderid
         LEFT JOIN iobvendorinvoiceitemsgeneralmodel iobvendorinvoiceitem ON iobvendorinvoiceitem.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobitemaccountinginfo ON iobitemaccountinginfo.refinstanceid = iobvendorinvoiceitem.itemid
         LEFT JOIN leafglaccount ON leafglaccount.id = iobitemaccountinginfo.chartofaccountid;