ALTER TABLE iobcostingdetails
    RENAME COLUMN currencyprice TO currencyPriceEstimateTime;

ALTER TABLE iobcostingdetails
    ADD COLUMN currencyPriceActualTime numeric;
