DROP VIEW IF EXISTS IObCostingDetailsGeneralModel;

create or replace view IObCostingDetailsGeneralModel AS
select iobcostingdetails.id,
       dobaccountingdocument.code       AS usercode,
       iobcostingdetails.refinstanceid,
       iobcostingdetails.creationdate,
       iobcostingdetails.creationinfo,
       iobcostingdetails.modifieddate,
       iobcostingdetails.modificationinfo,
       iobcostingdetails.currencypriceestimatetime,
       iobcostingdetails.currencypriceactualtime,
       cobcurrency.iso AS localCurrencyIso,
       dobinventorydocument.code        AS goodsreceiptcode,
       doborder.code                    AS purchaseordercode,
       purchaseunit.name ->> 'en'::text AS purchaseunitnameen,
       purchaseunit.name                AS purchaseunitname
FROM iobcostingdetails
         LEFT JOIN dobgoodsreceiptpurchaseorder ON iobcostingdetails.goodsreceiptid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN dobinventorydocument ON dobgoodsreceiptpurchaseorder.id = dobinventorydocument.id
         LEFT JOIN dobcosting ON iobcostingdetails.refinstanceid = dobcosting.id
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = dobcosting.id
         LEFT JOIN iobgoodsreceiptpurchaseorderdata
                   ON iobgoodsreceiptpurchaseorderdata.refinstanceid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN doborderdocument doborder ON iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata
                   ON iobaccountingdocumentcompanydata.companyid = iobenterprisebasicdata.refinstanceid
         left join cobcurrency on cobcurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN cobenterprise purchaseunit ON iobaccountingdocumentcompanydata.purchaseunitid = purchaseunit.id;



CREATE VIEW UnrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId          as itemId,
       vendorinvoiceitemweight.landedCostState as landedCostState,
       vendorinvoiceitemweight.landedCostCode  as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal            as itemTotalLandedCost,
       (itemweight * landedcosttotal) /
       GRUnrestrectedItem.receivedqtyuoe       as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
        (itemunitprice * currencyprice))       as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode =
                       vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and
                       vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode;


CREATE VIEW PurchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode =
                      unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode =
                           GRDamagedItem.unitofentrycode;

