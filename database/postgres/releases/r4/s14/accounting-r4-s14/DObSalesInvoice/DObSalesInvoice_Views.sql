CREATE or REPLACE FUNCTION getInvoiceNetAmount(iCode VARCHAR, invoiceType VARCHAR)
    RETURNS NUMERIC AS $$
BEGIN
RETURN
        getInvoiceTotalAmountBeforeTaxes(iCode, invoiceType) + getInvoiceTotalTaxAmount(iCode, invoiceType);
END; $$
LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getInvoiceTotalTaxAmount(iCode VARCHAR, invoiceType VARCHAR)
    RETURNS NUMERIC AS $$
BEGIN
RETURN
    CASE WHEN invoiceType = 'SalesInvoice' THEN
             (select case when sum(taxes.taxAmount) is null then 0 else sum(taxes.taxAmount) end
              from iobsalesinvoicetaxesgeneralmodel taxes
              where taxes.invoicecode = iCode)
         ELSE
             (select case when sum(taxes.taxAmount) is null then 0 else sum(taxes.taxAmount) end
              from iobvendorinvoicetaxesgeneralmodel taxes
              where taxes.invoicecode = iCode)
        END;
END; $$
LANGUAGE PLPGSQL;

Create OR REPLACE view DObSalesInvoiceJournalEntryPreparationGeneralModel as
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       customerbusinesspartner.customerid,
       postingdetails.duedate,
       iobaccountingdocumentcompanydata.companyid,
       iobaccountingdocumentcompanydata.purchaseunitid,
       dobaccountingdocument.id                        as documentreferenceid,
       businesspartner.currencyid                      as customercurrencyid,
       iobenterprisebasicdata.currencyid               as companycurrencyid,
       currecyPrice.currencyPrice,
       currecyPrice.exchangerateId,
       getInvoiceTotalAmountBeforeTaxes(dobaccountingdocument.code,
                                       'SalesInvoice') as amountBeforeTaxes,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           'SalesInvoice')             as amountAfterTaxes,
       iobcustomeraccountinginfo.chartofaccountid      as customeraccountId,
       customerAccount.leadger                         as customerLeadger,
       lobglobalglaccountconfig.accountid              as salesaccountid
from dobaccountingdocument
         left join iobaccountingdocumentactivationdetails postingdetails
                   on postingdetails.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicebusinesspartner businesspartner
                   on businesspartner.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicecustomerbusinesspartner customerbusinesspartner
                   on customerbusinesspartner.id = businesspartner.id
         left join iobaccountingdocumentcompanydata
                   on iobaccountingdocumentcompanydata.refinstanceid = dobaccountingdocument.id
         left join iobenterprisebasicdata on iobaccountingdocumentcompanydata.companyid =
                                             iobenterprisebasicdata.refinstanceid
         left join getLatestCurrencyPrice(businesspartner.currencyid,
                                          iobenterprisebasicdata.currencyid) currecyPrice
                   on currecyPrice.firstcurrencyid = businesspartner.currencyid
                       and currecyPrice.secondcurrencyid = iobenterprisebasicdata.currencyid
         left join iobcustomeraccountinginfo
                   on iobcustomeraccountinginfo.refinstanceid = customerbusinesspartner.customerid
         left join leafglaccount customerAccount
                   on customerAccount.id = iobcustomeraccountinginfo.chartofaccountid
         left join lobglobalglaccountconfig on lobglobalglaccountconfig.code = 'SalesGLAccount'
where dobaccountingdocument.objecttypecode = 'SalesInvoice'
  and postingdetails.objecttypecode = 'SalesInvoice'
  and iobaccountingdocumentcompanydata.objecttypecode = 'SalesInvoice';

CREATE OR REPLACE VIEW DObSalesInvoiceDataGeneralModel AS
SELECT DObSalesInvoice.id,
       dobaccountingdocument.code,
       IObSalesInvoicePostingDetails.dueDate,
       cobenterprise.name::json ->> 'ar'                                                            as companyArName,
       masterdata.name::json ->> 'en'                                                               as customerEnName,
       getInvoiceTotalAmountBeforeTaxes(dobaccountingdocument.code,
                                       dobaccountingdocument.objecttypecode)                        as totalAmountBeforeTaxes,
       getSalesInvoiceTotalTaxPercentage(dobaccountingdocument.code)                                as totalTaxPercentages,
       getInvoiceTotalTaxAmount(dobaccountingdocument.code,
                                dobaccountingdocument.objecttypecode)                               as totalTaxAmount,
       (getInvoiceTotalAmountBeforeTaxes(dobaccountingdocument.code, dobaccountingdocument.objecttypecode) +
        getInvoiceTotalTaxAmount(dobaccountingdocument.code, dobaccountingdocument.objecttypecode)) as netAmount,
       cobcurrency.iso                                                                              as customerCurrencyIso,
       cobpaymentterms.name::json ->> 'ar'                                                          as paymentTerm,
       enterpriseTelephone.contactValue                                                             as companyTelephone,
       enterpriseFax.contactValue                                                                   as companyFax,
       iobcompanylogodetails.logo                                                                   as companyLogo,
       IObEnterpriseAddress.addressLine::json ->> 'ar'                                              as companyArAddress,
       cobcompany.regnumber                                                                         as registrationNumber,
       cobcompany.taxcardnumber,
       cobcompany.taxfilenumber,
       lobtaxadministrative.name::json ->> 'ar'                                                     as taxadministrativeArName

from DObSalesInvoice
    left join IObSalesInvoicePostingDetails on DObSalesInvoice.id = IObSalesInvoicePostingDetails.refInstanceId
    left join dobaccountingdocument on DObSalesInvoice.id = dobaccountingdocument.id
    LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
    ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
    iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
    left join cobenterprise
    on iobsalesinvoicecompanydata.companyId = cobenterprise.id and cobenterprise.objecttypecode = '1'
    left join cobcompany on iobsalesinvoicecompanydata.companyId = cobcompany.id
    left join IObSalesInvoiceBusinessPartner on DObSalesInvoice.id = IObSalesInvoiceBusinessPartner.refInstanceId
    left join cobpaymentterms on IObSalesInvoiceBusinessPartner.paymentTermId = cobpaymentterms.id
    left join iobsalesinvoicecustomerbusinesspartner
    on iobsalesinvoicecustomerbusinesspartner.id = IObSalesInvoiceBusinessPartner.id
    left join masterdata on iobsalesinvoicecustomerbusinesspartner.customerId = masterdata.id and
    masterdata.objecttypecode = '3'
    left join IObEnterpriseContact enterpriseTelephone
    on cobcompany.id = enterpriseTelephone.refinstanceid and enterpriseTelephone.objectTypeCode = '1' and
    enterpriseTelephone.contactTypeId = 1
    left join IObEnterpriseContact enterpriseFax
    on cobcompany.id = enterpriseFax.refinstanceid and enterpriseFax.objectTypeCode = '1' and
    enterpriseFax.contactTypeId = 2
    left join iobcompanylogodetails on cobcompany.id = iobcompanylogodetails.refinstanceid
    left join IObEnterpriseAddress on cobcompany.id = IObEnterpriseAddress.refinstanceid
    left join lobtaxadministrative on cobcompany.taxadministrativeid = lobtaxadministrative.id
    left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId;

CREATE OR REPLACE VIEW IObSalesInvoiceBusinessPartnerGeneralModel as
select iobsalesinvoicebusinesspartner.id,
       iobsalesinvoicebusinesspartner.refinstanceid,
       iobsalesinvoicebusinesspartner.creationDate,
       iobsalesinvoicebusinesspartner.modifiedDate,
       iobsalesinvoicebusinesspartner.creationInfo,
       iobsalesinvoicebusinesspartner.modificationInfo,
       iobsalesinvoicebusinesspartner.type,
       dobsalesorder.code                                                         as salesOrder,
       iobsalesinvoicebusinesspartner.downPayment,
       dobaccountingdocument.code as code,
       dobaccountingdocument.currentStates,
       masterdata.code                                                            as businessPartnerCode,
       masterdata.name                                                            as businessPartnerName,
       concat(masterdata.code, ' - ', masterdata.name :: json ->> 'en')           as businesspartnercodename,

       cobcurrency.iso                                                            as currencyIso,
       cobcurrency.code                                                           as currencyCode,
       cobcurrency.name                                                           as currencyName,

       cobpaymentterms.code                                                       AS paymentTermCode,
       cobpaymentterms.name                                                       AS paymentTermName,
       concat(cobpaymentterms.code, ' - ', cobpaymentterms.name :: json ->> 'en') as paymentTermCodeName
from iobsalesinvoicebusinesspartner
         left join iobsalesinvoicecustomerbusinesspartner
                   on iobsalesinvoicebusinesspartner.id = iobsalesinvoicecustomerbusinesspartner.id
         left join mobcustomer on iobsalesinvoicecustomerbusinesspartner.customerid = mobcustomer.id
         left join masterdata on masterdata.id = mobcustomer.id
         left join dobaccountingdocument on dobaccountingdocument.id = iobsalesinvoicebusinesspartner.refInstanceId and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join cobcurrency on cobcurrency.id = iobsalesinvoicebusinesspartner.currencyId
         left join cobpaymentterms on cobpaymentterms.id = iobsalesinvoicebusinesspartner.paymentTermId
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesorderid;



CREATE VIEW ItemsLastSalesPriceGeneralModel AS
SELECT iobsalesinvoiceitem.id,
       dobaccountingdocument.code                                           as salesInvoiceCode,
       iobAccountingdocumentactivationdetails.creationdate                     as salesInvoicePostingDate,

       uom.code                                                             as uomCode,
       concat(uom.code, ' - ', uom.name :: json ->> 'en')                   as uomCodeName,

       item.code                                                            as itemCode,
       concat(item.code, ' - ', item.name :: json ->> 'en')                 as itemCodeName,

       customer.code                                                        as customerCode,
       concat(customer.code, ' - ', customer.name :: json ->> 'en')         as customerCodeName,

       purchaseUnit.code                                                    as purchaseUnitCode,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name :: json ->> 'en') as purchaseUnitCodeName,
       iobsalesinvoiceitem.price                                            as salesPrice,
       iobsalesinvoiceitem.price *
       getconversionfactortobase(iobsalesinvoiceitem.baseunitofmeasureid,
                                 iobsalesinvoiceitem.orderunitofmeasureid,
                                 iobsalesinvoiceitem.itemid)                AS orderUnitPrice
from iobsalesinvoiceitem
         LEFT JOIN dobaccountingdocument
                   on iobsalesinvoiceitem.refinstanceid = dobaccountingdocument.id
                       and dobaccountingdocument.objecttypecode = 'SalesInvoice' and
                      dobaccountingdocument.currentstates like '%Active%'
         left join dobsalesinvoice on dobaccountingdocument.id = dobsalesinvoice.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN masterdata item
                   on iobsalesinvoiceitem.itemid = item.id and item.objecttypecode = '1'
         left join cobmeasure uom on uom.id = iobsalesinvoiceitem.orderunitofmeasureid
         left join cobenterprise company
                   on company.id = iobsalesinvoicecompanydata.companyid and
                      company.objecttypecode = '1'
         left join cobenterprise purchaseUnit
                   on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid and
                      purchaseUnit.objecttypecode = '5'
         left join iobsalesinvoicebusinesspartner
                   on iobsalesinvoicebusinesspartner.refinstanceid = dobsalesinvoice.id
         join iobsalesinvoiceCustomerbusinesspartner
              on iobsalesinvoiceCustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         join masterdata customer
              on customer.id = iobsalesinvoiceCustomerbusinesspartner.customerid and
                 customer.objecttypecode = '3'
         left join iobAccountingdocumentactivationdetails
                   on dobsalesinvoice.id = iobAccountingdocumentactivationdetails.refinstanceid and
                      iobAccountingdocumentactivationdetails.objecttypecode = 'SalesInvoice';


CREATE OR REPLACE VIEW IObInvoiceSummaryGeneralModel AS
SELECT dobaccountingdocument.id                              as id,
       code                                                  as invoiceCode,
       (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                'SalesInvoice'
            WHEN isVendorInvoice(objectTypeCode) THEN
                'VendorInvoice' END)     as invoiceType,
       getInvoiceTotalAmountBeforeTaxes(code, objecttypecode) as totalAmountBeforeTaxes,
       getInvoiceTotalTaxAmount(code, objecttypecode)        as taxAmount,
       getInvoiceNetAmount(code, objecttypecode)             as netAmount,
       (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                getDownPaymentForInvoice(code, objecttypecode)
            WHEN isVendorInvoice(objectTypeCode) THEN
                iobvendorinvoicesummary.downpayment END)     as downpayment,
       (CASE  WHEN objecttypecode = 'SalesInvoice' THEN
                  SiSummary.remaining
              WHEN isVendorInvoice(objectTypeCode) THEN
                  iobvendorinvoicesummary.remaining END)     as totalRemaining
FROM dobaccountingdocument
         LEFT JOIN IObSalesInvoiceSummary SiSummary
                   ON SiSummary.refInstanceId = dobaccountingdocument.id AND
                      dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join iobvendorinvoicesummary on IObVendorInvoiceSummary.refinstanceid = dobaccountingdocument.id
WHERE isInvoice(objecttypecode) ;


CREATE or REPLACE VIEW IObSalesInvoiceTaxesGeneralModel AS
SELECT IObSalesInvoiceTaxes.id,
       IObSalesInvoiceTaxes.creationDate,
       IObSalesInvoiceTaxes.modifiedDate,
       IObSalesInvoiceTaxes.creationInfo,
       IObSalesInvoiceTaxes.modificationInfo,
       IObSalesInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code AS invoicecode,
       IObSalesInvoiceTaxes.code  AS taxcode,
       IObSalesInvoiceTaxes.name  AS taxname,
       (getInvoiceTotalAmountBeforeTaxes(dobaccountingdocument.code, 'SalesInvoice') *
        (SELECT CASE
                    WHEN dobaccountingdocument.currentStates LIKE '%Active%' then IObSalesInvoiceTaxes.percentage
                    WHEN dobaccountingdocument.currentStates LIKE '%Closed%' then IObSalesInvoiceTaxes.percentage
                    ELSE LObTaxInfo.percentage
                    END
        )) / 100                  AS taxAmount,
       (SELECT CASE
                   WHEN dobaccountingdocument.currentStates LIKE '%Active%' then IObSalesInvoiceTaxes.percentage
                   WHEN dobaccountingdocument.currentStates LIKE '%Closed%' then IObSalesInvoiceTaxes.percentage
                   ELSE LObTaxInfo.percentage
                   END
       )                          AS taxpercentage
FROM IObSalesInvoiceTaxes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = IObSalesInvoiceTaxes.refInstanceId
                       and dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join dobsalesinvoice on dobsalesinvoice.id = IObSalesInvoiceTaxes.refinstanceid
         left join LObTaxInfo on LObTaxInfo.code = IObSalesInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis

CREATE or REPLACE VIEW IObSalesInvoiceItemGeneralModel as
select SII.id,
       SI.id                                             as refinstanceid,
       SI.code                                           as code,
       m.code                                            as itemCode,
       m.name                                            as itemName,
       baseUnitOfMeasure.code                            as baseUnitCode,
       baseUnitOfMeasure.symbol                          as baseUnitSymbol,
       c.code                                            as orderUnitCode,
       c.symbol                                          as orderunitsymbol,
       SII.quantityinorderunit                           as quantityInOrderUnit,
       SII.returnedQuantity                              as returnedQuantity,
       SII.price,
       getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid,
                                 SII.itemid)::numeric             AS conversionFactorToBase,
       SII.price * getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid,
                                             SII.itemid)::numeric as orderUnitPrice,
       ((SII.quantityinorderunit *
         getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid, SII.itemid))::numeric *
        SII.price)                                       as totalAmount,
       concat(m.code, ' - ', m.name :: json ->> 'en')    as itemCodeName,
       c.name                                            as orderUnitName,
       concat(c.code, ' - ', c.name :: json ->> 'en')    as orderUnitCodeName,
       concat(c.code, ' - ', c.symbol :: json ->> 'en')  as orderUnitCodeSymbol,
       baseUnitOfMeasure.name                            as baseUnitOfMeasureName,
       concat(baseUnitOfMeasure.code, ' - ',
              baseUnitOfMeasure.name :: json ->> 'en')   as baseUnitOfMeasureCodeName,
       concat(baseUnitOfMeasure.code, ' - ',
              baseUnitOfMeasure.symbol :: json ->> 'en') as baseUnitOfMeasureCodeSymbol,
       SII.creationDate,
       SII.creationInfo,
       SII.modifiedDate,
       SII.modificationInfo
from IObSalesInvoiceItem SII
         left join masterdata m on SII.itemId = m.id
         left join cobmeasure c on SII.orderunitofmeasureid = c.id
         left join mobitem on SII.itemid = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument SI on SII.refInstanceId = SI.id and SI.objecttypecode = 'SalesInvoice';

CREATE OR REPLACE VIEW DObSalesInvoiceGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.modificationInfo,
       creationuserinfo.name                                                AS creationInfoName,
       dobsalesorder.code                                                   AS salesOrder,
       cobcompanygeneralmodel.currencyiso                                   AS companyCurrencyIso,
       cobsalesinvoice.code                									AS invoiceTypeCode,
       cobsalesinvoice.name  												AS invoiceType,
       purchaseUnit.code 													AS purchaseUnitCode,
       purchaseUnit.name 													AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' 									AS purchaseUnitNameEn,
       company.code 														AS companyCode,
       company.name 														AS companyName,
       masterdata.code                                                      AS customerCode,
       masterdata.name                                                      AS customer,
       dobaccountingdocument.documentOwnerId                                AS documentOwnerId,
       userinfo.name 														AS documentOwner,
       ebsuser.username 													AS documentOwnerUserName,
       siSummary.remaining					 		 						AS remaining
FROM DObSalesInvoice
    LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
    dobaccountingdocument.objecttypecode = 'SalesInvoice'
    LEFT JOIN iobsalesinvoicebusinesspartner
    ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
    LEFT JOIN iobsalesinvoicecustomerbusinesspartner
    ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
    LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
    ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
    iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
    LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
    LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
    left join ebsuser creationebsuser on creationebsuser.username = dobaccountingdocument.creationinfo
    left join userinfo creationuserinfo on creationuserinfo.id = creationebsuser.id
    left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId
    left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesOrderId
    left join cobcompanygeneralmodel
    on cobcompanygeneralmodel.id = iobsalesinvoicecompanydata.companyId
    left join cobsalesinvoice on cobsalesinvoice.id = dobsalesinvoice.typeid
    left join cobenterprise purchaseUnit on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid
    left join cobenterprise company on company.id = iobsalesinvoicecompanydata.companyid
    left join userinfo on  userinfo.id = dobaccountingdocument.documentOwnerId
    left join ebsuser on  ebsuser.id = dobaccountingdocument.documentOwnerId
    left join iobsalesinvoicesummary siSummary
    on siSummary.refinstanceId = DObSalesInvoice.id;

CREATE OR REPLACE VIEW IObSalesInvoiceJournalEntryItemsPreparationGeneralModel as
select iobsalesinvoicetaxes.id,
       dobaccountingdocument.code,
       leafglaccount.id as taxesaccountid,
       lobtaxinfo.id    as taxId,
       (iobsalesinvoicetaxes.percentage *
        getInvoiceTotalAmountBeforeTaxes(dobaccountingdocument.code, 'SalesInvoice')) /
       100              as taxamount
from dobaccountingdocument
         left join iobsalesinvoicetaxes
                   on iobsalesinvoicetaxes.refinstanceid = dobaccountingdocument.id
         left join lobglobalglaccountconfig accountconfig on accountconfig.code = 'TaxesGLAccount'
         left join leafglaccount on leafglaccount.id = accountconfig.accountid
         left join lobtaxinfo on iobsalesinvoicetaxes.code = lobtaxinfo.code
where dobaccountingdocument.objecttypecode = 'SalesInvoice';