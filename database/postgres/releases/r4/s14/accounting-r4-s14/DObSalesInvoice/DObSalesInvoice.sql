ALTER TABLE iobsalesinvoicebusinesspartner alter column downpayment type numeric;
ALTER TABLE iobsalesinvoiceitem alter column quantityinorderunit type numeric;
ALTER TABLE iobsalesinvoiceitem alter column price type numeric;
ALTER TABLE iobsalesinvoiceitem alter column returnedquantity type numeric;
ALTER TABLE iobsalesinvoicesummary alter column remaining type numeric;
ALTER TABLE iobsalesinvoicetaxes alter column percentage type numeric;
ALTER TABLE iobsalesinvoicetaxes alter column amount type numeric;