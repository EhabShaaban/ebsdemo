CREATE or REPLACE FUNCTION getAmountPaidViaAccountingDocument(iCode VARCHAR, invoiceType VARCHAR)
    RETURNS NUMERIC AS $$
BEGIN
RETURN
    CASE WHEN invoiceType = 'SalesInvoice' THEN
             round((getInvoiceTotalAmountBeforeTaxes(iCode, invoiceType)
                        + getInvoiceTotalTaxAmount(iCode, invoiceType)
                 - getDownPaymentForInvoice(iCode, invoiceType)
                 - getAmountFromPostedNotesReceivalbles(iCode)
                 - getAmountFromPostedCollections(iCode)) ::numeric, 3)
         ELSE
             round((getInvoiceTotalAmountBeforeTaxes(iCode, invoiceType)
                        + getInvoiceTotalTaxAmount(iCode, invoiceType)
                 - getDownPaymentForInvoice(iCode, invoiceType)
                 - COALESCE((SELECT SUM(paymentDetails.netamount) AS amountPaid
                             FROM iobpaymentrequestinvoicepaymentdetails invoicepaymentDetails
                                      JOIN iobpaymentrequestpaymentdetails paymentDetails on paymentDetails.id = invoicepaymentDetails.id
                                      JOIN dobpaymentrequest paymentRequest ON paymentDetails.refinstanceid = paymentRequest.id
                                      JOIN dobaccountingdocument on dobaccountingdocument.id =paymentRequest.id AND dobaccountingdocument.currentstates like '%PaymentDone%'
                                      JOIN dobaccountingdocument invoice ON invoicepaymentDetails.duedocumentid = invoice.id AND invoice.objecttypecode = 'VendorInvoice'
                             WHERE invoice.code = iCode),0)) ::numeric, 3)
        END;
END; $$
LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW IObLandedCostDetailsGeneralModel AS
SELECT ioblandedcostdetails.id,
       ioblandedcostdetails.refinstanceid,
       ioblandedcostdetails.creationdate,
       ioblandedcostdetails.creationinfo,
       ioblandedcostdetails.modificationinfo,
       ioblandedcostdetails.modifieddate,
       landedCost.code                                           as landedCostCode,
       dobaccountingdocument.code                                as vendorInvoiceCode,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           dobaccountingdocument.objecttypecode) as invoiceAmount,
       doborderdocument.code                                     as purchaseOrderCode,
       companyCurrency.iso                                       as companyCurrencyIso,
       invoiceCurrency.iso                                       as invoiceCurrencyIso,
       ioblandedcostdetails.damageStock,
       cobenterprise.code                                        as businessUnitCode,
       cobenterprise.name                                        as businessUnitName,
       concat(cobenterprise.code, ' ', '-', ' ',
              cobenterprise.name::json ->> 'en')                 as businessUnitCodeName
FROM ioblandedcostdetails
         LEFT JOIN dobaccountingdocument landedCost
                   on ioblandedcostdetails.refinstanceid = landedCost.id
         LEFT JOIN doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN dobaccountingdocument
                   on ioblandedcostdetails.vendorinvoiceid = dobaccountingdocument.id

         LEFT JOIN iobordercompany on iobordercompany.refinstanceid = doborderdocument.id
         LEFT JOIN iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         LEFT JOIN iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = ioblandedcostdetails.vendorinvoiceid

         LEFT JOIN cobcurrency invoiceCurrency
                   on invoiceCurrency.id = iobvendorinvoicepurchaseorder.currencyid
         LEFT JOIN doblandedcost on doblandedcost.id = ioblandedcostdetails.refinstanceid
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON landedCost.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         LEFT JOIN cobenterprise on cobenterprise.id = IObLandedCostCompanyData.purchaseUnitId
         LEFT JOIN cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid;

CREATE OR REPLACE VIEW DObLandedCostItemsInvoicesGeneralModel
AS
select row_number() OVER () as id, dobaccountingdocument.code as vendorInvoiceCode, doborderdocument.code as purchaseOrderCode ,
       masterdata.id as itemId,
       ((iobvendorinvoiceitem.quantityinorderunit* getConversionFactorToBase(iobvendorinvoiceitem.baseunitofmeasureid, iobvendorinvoiceitem.orderunitofmeasureid, iobvendorinvoiceitem.itemid)) * iobvendorinvoiceitem.price  )           as totalAmount
from iobvendorinvoiceitem left join dobaccountingdocument
                                    on iobvendorinvoiceitem.refinstanceid = dobaccountingdocument.id
                          left join dobvendorinvoice on dobvendorinvoice.id = dobaccountingdocument.id
                          left join iobvendorinvoicedetails on iobvendorinvoicedetails.refinstanceid = dobaccountingdocument.id
                          left join iobvendorinvoicepurchaseorder on iobvendorinvoicedetails.id = iobvendorinvoicepurchaseorder.id
                          left join doborderdocument on iobvendorinvoicepurchaseorder.purchaseorderid = doborderdocument.id
                          left join masterdata on masterdata.id = iobvendorinvoiceitem.itemid
where isPurchaseServiceLocalInvoice(dobaccountingdocument.objecttypecode) and dobaccountingdocument.currentstates like '%Posted%';
