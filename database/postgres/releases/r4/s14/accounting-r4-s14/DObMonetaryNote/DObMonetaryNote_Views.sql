DROP VIEW IF EXISTS IObMonetaryNotesDetailsGeneralModel;
CREATE VIEW IObMonetaryNotesDetailsGeneralModel AS
SELECT IObMonetaryNotesDetails.id,
       IObMonetaryNotesDetails.refinstanceid,
       IObMonetaryNotesDetails.creationInfo,
       IObMonetaryNotesDetails.modificationInfo,
       IObMonetaryNotesDetails.creationDate,
       IObMonetaryNotesDetails.modifiedDate,
       dobaccountingdocument.code AS userCode,
       dobaccountingdocument.objecttypecode,
       iobmonetarynotesdetails.noteForm,
       MasterData.code            AS businessPartnerCode,
       MasterData.name            AS businessPartnerName,
       iobmonetarynotesdetails.notenumber,
       iobmonetarynotesdetails.notebank,
       iobmonetarynotesdetails.duedate,
       CObCurrency.code           AS currencyCode,
       CObCurrency.iso            AS currencyIso,
       iobmonetarynotesdetails.amount,
       iobmonetarynotesdetails.remaining,
       lobdepot.code              AS depotCode,
       lobdepot.name              AS depotName,
       companyCurrency.code       AS companyCurrencyCode,
       companyCurrency.iso        AS companyCurrencyIso
FROM IObMonetaryNotesDetails
         LEFT JOIN dobmonetarynotes ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = dobmonetarynotes.id
         LEFT JOIN lobdepot ON lobdepot.id = iobmonetarynotesdetails.depotid
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '3'
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   on dobmonetarynotes.id = companyData.refinstanceid
         LEFT JOIN iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = companyData.companyid
         LEFT JOIN cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid;

CREATE OR REPLACE VIEW DObNotesReceivableJournalEntryPreparationGeneralModel AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       iobaccountingdocumentcompanydata.purchaseunitid as businessunitid,
       iobaccountingdocumentcompanydata.companyid,
       iobaccountingdocumentactivationdetails.fiscalperiodid,
       iobaccountingdocumentactivationdetails.exchangerateid,
       iobaccountingdocumentactivationdetails.currencyprice,
       companyData.currencyid                             companycurrencyid,
       iobmonetarynotesdetails.currencyid                 documentcurrencyid
from dobmonetarynotes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = dobmonetarynotes.id
                       and dobaccountingdocument.objecttypecode LIKE 'NOTES_RECEIVABLE_%'
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
                       and iobaccountingdocumentcompanydata.objecttypecode = 'NotesReceivable'
         left join iobaccountingdocumentactivationdetails
                   on dobaccountingdocument.id =
                      iobaccountingdocumentactivationdetails.refinstanceid
                       and iobaccountingdocumentactivationdetails.objecttypecode LIKE
                           'NOTES_RECEIVABLE_%'
         left join iobmonetarynotesdetails
                   on iobmonetarynotesdetails.refinstanceid = dobaccountingdocument.id
         left join iobenterprisebasicdata companyData
                   on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid;
