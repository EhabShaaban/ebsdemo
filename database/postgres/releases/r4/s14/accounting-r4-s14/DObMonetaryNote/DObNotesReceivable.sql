DROP VIEW IF EXISTS DObJournalEntryGeneralModel;
DROP TABLE IF EXISTS DObNotesReceivableJournalEntry;
CREATE TABLE DObNotesReceivableJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE DObNotesReceivableJournalEntry
    ADD CONSTRAINT FK_DObNotesReceivableJournalEntry_documentReferenceId FOREIGN KEY (documentReferenceId) REFERENCES DObMonetaryNotes (id) On DELETE RESTRICT;
