DROP VIEW If EXISTS IObPaymentRequestPaymentDetailsGeneralModel;

CREATE OR REPLACE VIEW IObPaymentRequestPaymentDetailsGeneralModel AS
SELECT pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                                                                       as businessPartnerCode,
       md.name                                                                       as businessPartnerName,
       pd.duedocumenttype,
       getduedocumentcodeaccordingtopaymenttype(pd.id, pd.duedocumenttype)                                           as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso                                                               as currencyISO,
       cobcurrency.name                                                              as currencyName,
       cobcurrency.code                                                              as currencyCode,
       companyCurrency.iso                                                           AS companyCurrencyIso,
       companyCurrency.code                                                          AS companyCurrencyCode,
       companyCurrency.id                                                            AS companyCurrencyId,
       pd.description,
       pd.paymentForm,
       IObCompanyBankDetails.id                                                      AS bankAccountId,
       companyBank.code                                                              AS bankAccountCode,
       companyBank.name                                                              AS bankAccountName,
       cobtreasury.id                                                                AS treasuryAccountId,
       cobtreasury.code                                                              AS treasuryCode,
       cobtreasury.name                                                              AS treasuryName,
       CASE
           WHEN
               IObCompanyBankDetails.accountno IS NULL THEN NULL
           ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END AS bankAccountNumber,
       item.code                                                                     AS costFactorItemCode,
       item.name                                                                     AS costFactorItemName,

       pd.expectedDueDate                                                            AS expectedDueDate,
       pd.bankTransRef                                                               AS bankTransRef
FROM iobpaymentrequestpaymentdetails pd
         LEFT JOIN masterdata md on md.id = pd.businessPartnerId
         LEFT JOIN dobpaymentrequest pr on pr.id = pd.refinstanceid
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = pr.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
         LEFT JOIN cobcurrency on pd.currencyid = cobcurrency.id
         LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
         LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = pd.bankAccountId
         LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN MObItemGeneralModel item on item.id = pd.costFactorItemId
         LEFT JOIN cobtreasury on cobtreasury.id = pd.treasuryId;
