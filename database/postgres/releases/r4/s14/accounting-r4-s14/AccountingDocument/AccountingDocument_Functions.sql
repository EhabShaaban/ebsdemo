DROP FUNCTION IF EXISTS getConversionFactorToBase(baseunitofmeasureid bigint, orderunitofmeasureid bigint, itemid bigint);

CREATE or REPLACE FUNCTION getConversionFactorToBase(baseunitofmeasureid bigint, orderunitofmeasureid bigint, itemid bigint)
RETURNS NUMERIC AS $$
BEGIN
RETURN (SELECT CASE
                   WHEN (baseunitofmeasureid = orderunitofmeasureid)
                       THEN 1
                   WHEN (SELECT iobalternativeuom.conversionfactor
                         FROM iobalternativeuom
                         WHERE itemid = iobalternativeuom.refinstanceid
                           AND orderunitofmeasureid = iobalternativeuom.alternativeunitofmeasureid
                   ) is null THEN 1
                   ELSE (SELECT iobalternativeuom.conversionfactor
                         FROM iobalternativeuom
                         WHERE itemid = iobalternativeuom.refinstanceid
                           AND orderunitofmeasureid = iobalternativeuom.alternativeunitofmeasureid
                   ) END AS conversionFactorToBase
);
END; $$
LANGUAGE PLPGSQL;