CREATE or REPLACE FUNCTION getInvoiceTotalAmountBeforeTaxes(invoiceCode VARCHAR, invoiceType VARCHAR)
    RETURNS NUMERIC AS $$
BEGIN
RETURN
    CASE WHEN invoiceType = 'SalesInvoice' THEN
             COALESCE((SELECT
                           (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * (case when invoiceItem.conversionfactortobase is null then 1 else invoiceItem.conversionfactortobase::numeric end )))::numeric AS totalAmount
                            FROM iobsalesinvoiceitemgeneralmodel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                       FROM dobsalesinvoice dobInvoice
                                LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobInvoice.id
                       WHERE dobaccountingdocument.objecttypecode = 'SalesInvoice' AND dobaccountingdocument.code = invoiceCode
                      ),0)
         ELSE
             COALESCE((SELECT
                           (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * invoiceItem.conversionfactortobase)) AS totalAmount
                            FROM IObVendorInvoiceItemsGeneralModel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                       FROM dobvendorInvoice dobvendorInvoice
                                LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
                       WHERE dobaccountingdocument.code = invoiceCode
                      ),0)
        END;
END; $$
LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW IObAccountingDocumentAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Vendors'
               then (
               vendor.code
               )
           when accountDetails.subledger = 'PO' then (
               doborderdocument.code
               )
           when
                   accountDetails.subledger = 'Banks'
               then (concat(bankSubAccount.accountno, ' - ', currency.iso))
           when
                   accountDetails.subledger = 'Treasuries'
               then (cobtreasury.code)
           end                    as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Vendors' then (
               vendoraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'PO' then (
               orderaccountingdetails.glSubAccountId
               )
           when
                   accountDetails.subledger = 'Banks'
               then (bankaccountingdetails.glSubAccountId)
           when
                   accountDetails.subledger = 'Treasuries'
               then (treasuryaccountingdetails.glSubAccountId)
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Vendors'
               then (
               vendor.name
               )
           when
                   accountDetails.subledger = 'Banks' THEN
               (cobbank.name)
           when
                   accountDetails.subledger = 'Treasuries'
               then (cobtreasury.name)
           end                    as glSubAccountName,
       accountDetails.objecttypecode
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join dobaccountingdocument on dobaccountingdocument.id = accountDetails.refinstanceid
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails
                   on vendoraccountingdetails.id = accountDetails.id
         left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails
                   on orderaccountingdetails.id = accountDetails.id
         left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails
                   on bankaccountingdetails.id = accountDetails.id
         left join iobaccountingdocumenttreasuryaccountingdetails treasuryaccountingdetails
                   on treasuryaccountingdetails.id = accountDetails.id
         left join masterdata vendor
                   on vendor.id = vendoraccountingdetails.glSubAccountId and vendor.objecttypecode = '2'
         left join doborderdocument on doborderdocument.id = orderaccountingdetails.glSubAccountId
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         LEFT JOIN cobcurrency currency on bankSubAccount.currencyId = currency.id
         left join cobbank on cobbank.id = bankSubAccount.bankid
         left join cobtreasury on cobtreasury.id = treasuryaccountingdetails.glsubaccountid;