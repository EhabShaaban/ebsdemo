CREATE VIEW IObSalesReturnOrderItemGeneralModel AS
SELECT iobsalesreturnorderitems.id,
       iobsalesreturnorderitems.refinstanceid,
       iobsalesreturnorderitems.creationDate,
       iobsalesreturnorderitems.modifiedDate,
       iobsalesreturnorderitems.creationInfo,
       iobsalesreturnorderitems.modificationInfo,
       dobsalesreturnorder.code                                                         AS salesReturnOrderCode,
       masterdata.id                                                                    AS itemId,
       masterdata.code                                                                  AS itemCode,
       masterdata.name                                                                  AS itemName,
       cobmeasure.code                                                                  AS orderUnitCode,
       cobmeasure.name                                                                  AS orderUnitName,
       cobmeasure.symbol                                                                AS orderUnitSymbol,
       lobreason.code                                                                   AS returnReasonCode,
       lobreason.name                                                                   AS returnReasonName,
       iobsalesreturnorderitems.returnquantity                                          AS returnQuantity,
       (iobsalesinvoiceitem.quantityinorderunit - iobsalesinvoiceitem.returnedquantity) AS maxReturnQuantity,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid is NULL
                     THEN iobsalesreturnorderitems.returnquantity
                 ELSE iobsalesreturnorderitems.returnquantity *
                      iobalternativeuom.conversionfactor
                 END ::numeric,
             3)                                                                         AS returnQuantityInBase,
       iobsalesreturnorderitems.salesprice                                              AS salesPrice,
       iobsalesreturnorderitems.returnquantity * iobsalesreturnorderitems.salesprice    AS totalAmount
FROM dobsalesreturnorder
         JOIN iobsalesreturnorderitems ON iobsalesreturnorderitems.refInstanceId = dobsalesreturnorder.id
         JOIN mobitem ON iobsalesreturnorderitems.itemId = mobitem.id
         JOIN masterdata ON mobitem.id = masterdata.id
         JOIN iobsalesreturnorderdetails ON iobsalesreturnorderdetails.refinstanceid = dobsalesreturnorder.id
         JOIN iobsalesinvoiceitem
              ON iobsalesinvoiceitem.refinstanceid = iobsalesreturnorderdetails.salesinvoiceid
                  AND iobsalesinvoiceitem.itemid = iobsalesreturnorderitems.itemId
                  AND iobsalesinvoiceitem.orderunitofmeasureid = iobsalesreturnorderitems.orderunitid
         JOIN cobmeasure ON iobsalesreturnorderitems.orderUnitId = cobmeasure.id
         LEFT JOIN lobreason ON iobsalesreturnorderitems.returnreasonid = lobreason.id AND
                                lobreason.type = 'SRO_REASON'
         LEFT JOIN iobalternativeuom
                   on iobalternativeuom.alternativeunitofmeasureid = iobsalesreturnorderitems.orderunitid
                       and iobalternativeuom.refinstanceid = masterdata.id;


CREATE VIEW IObSalesReturnOrderTaxGeneralModel AS
SELECT iobsalesreturnordertax.id,
       iobsalesreturnordertax.creationDate,
       iobsalesreturnordertax.modifiedDate,
       iobsalesreturnordertax.creationInfo,
       iobsalesreturnordertax.modificationInfo,
       iobsalesreturnordertax.refinstanceid,
       dobsalesreturnorder.code AS salesReturnOrderCode,
       iobsalesreturnordertax.code,
       iobsalesreturnordertax.name,
       iobsalesreturnordertax.percentage,
       (SELECT sum(iobsalesreturnorderitemgeneralmodel.totalamount) *
               (iobsalesreturnordertax.percentage / 100::double precision)
        FROM iobsalesreturnorderitemgeneralmodel
        WHERE iobsalesreturnorderitemgeneralmodel.refinstanceid = dobsalesreturnorder.id) AS amount
FROM iobsalesreturnordertax
         LEFT JOIN dobsalesreturnorder
                   ON dobsalesreturnorder.id = iobsalesreturnordertax.refInstanceId;

