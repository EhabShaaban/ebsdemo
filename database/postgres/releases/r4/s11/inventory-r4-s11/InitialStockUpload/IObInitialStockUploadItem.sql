alter table iobinitialstockuploaditem alter column quantity type numeric;
alter table iobinitialstockuploaditem alter column actualcost type numeric;
alter table iobinitialstockuploaditem alter column remainingquantity type numeric;
alter table iobinitialstockuploaditem alter column estimatecost type numeric;
