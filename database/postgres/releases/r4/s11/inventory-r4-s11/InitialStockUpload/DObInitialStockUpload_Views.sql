CREATE VIEW IObInitialStockUploadItemQuantityGeneralModel AS
SELECT row_number()
       over (order by dobinventorydocument.code, iobinitialstockuploaditem.itemid, iobinitialstockuploaditem.stocktype) as id,
       dobinventorydocument.code                                                 AS userCode,
       iobinitialstockuploaditem.itemid                                          as itemId,
       item.code                                                                 AS itemcode,
       iobinitialstockuploaditem.quantity                                        as quantity,
       iobinitialstockuploaditem.stocktype                                       as stockType,
       uom.id                                                                    AS unitOfMeasureId,
       uom.code                                                                  AS unitofmeasurecode,
       businessunit.id                                                           as purchaseunitid,
       businessunit.code                                                         AS purchaseunitcode,
       goodsissuecompany.id                                                      as companyId,
       goodsissuecompany.code                                                    as companyCode,
       goodsissuestorehouse.id                                                   as storehouseId,
       goodsissuestorehouse.code                                                 as storehouseCode,
       goodsissueplant.id                                                        as plantId,
       goodsissueplant.code                                                      as plantCode,
       dobinventorydocument.creationdate,
       dobinventorydocument.creationinfo,
       dobinventorydocument.modifieddate,
       dobinventorydocument.modificationinfo
FROM iobinitialstockuploaditem
         JOIN dobinventorydocument ON dobinventorydocument.id = iobinitialstockuploaditem.refinstanceid
         JOIN iobinventorycompany ON dobinventorydocument.id = iobinventorycompany.refinstanceid
         JOIN cobenterprise businessunit ON businessunit.id = dobinventorydocument.purchaseunitid
         JOIN masterdata item ON iobinitialstockuploaditem.itemid = item.id
         JOIN cobmeasure uom ON iobinitialstockuploaditem.unitofmeasureid = uom.id
         JOIN cobenterprise goodsissuecompany ON iobinventorycompany.companyid = goodsissuecompany.id
         JOIN cobenterprise goodsissuestorehouse ON iobinventorycompany.storehouseid = goodsissuestorehouse.id
         JOIN cobenterprise goodsissueplant ON iobinventorycompany.plantid = goodsissueplant.id
WHERE dobinventorydocument.inventorydocumenttype::text = 'ISU'::text;

CREATE OR REPLACE VIEW TObInitialStockUploadStoreTransactionDataGeneralModel AS
SELECT ROW_NUMBER()
       OVER ()                                                   AS id,
       DObInventoryDocument.creationdate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.code,
       DObInventoryDocument.id AS initialStockUploadId,
       iobinventorycompany.storehouseid,
       iobinventorycompany.plantid,
       iobinventorycompany.companyid,
       dobinventoryDocument.purchaseunitid,
       iobinitialstockuploaditem.originaladdingdate,
       iobinitialstockuploaditem.itemid,
       iobinitialstockuploaditem.actualcost,
       iobinitialstockuploaditem.quantity,
       iobinitialstockuploaditem.remainingquantity,
       iobinitialstockuploaditem.unitofmeasureid,
       iobinitialstockuploaditem.stocktype,
       iobinitialstockuploaditem.estimatecost

FROM dobinventorydocument
         LEFT JOIN iobinventorycompany ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN iobinitialstockuploaditem
                   ON dobinventorydocument.id = iobinitialstockuploaditem.refinstanceid
where dobinventorydocument.inventorydocumenttype = 'ISU';


CREATE OR REPLACE VIEW IObInitialStockUploadItemGeneralModel AS
SELECT IObInitialStockUploadItem.id,
       IObInitialStockUploadItem.creationDate,
       IObInitialStockUploadItem.modifiedDate,
       IObInitialStockUploadItem.creationInfo,
       IObInitialStockUploadItem.modificationInfo,
       DObInventoryDocument.code                 AS initialStockUploadCode,
       DObInventoryDocument.currentStates,
       businessUnit.name                         AS purchaseUnitName,
       businessUnit.name:: JSON ->> 'en'         AS purchaseUnitNameEn,
       businessUnit.code                         AS purchaseUnitCode,
       IObInitialStockUploadItem.itemid          AS itemId,
       item.code                                 AS itemCode,
       item.name                                 AS itemName,
       IObInitialStockUploadItem.unitofmeasureid AS unitOfMeasureId,
       uom.code                                  AS unitOfMeasureCode,
       uom.symbol                                AS unitOfMeasureSymbol,
       IObInitialStockUploadItem.originaladdingdate,
       IObInitialStockUploadItem.stocktype,
       IObInitialStockUploadItem.quantity,
       IObInitialStockUploadItem.actualcost,
       IObInitialStockUploadItem.remainingquantity,
       IObInitialStockUploadItem.estimateCost
FROM IObInitialStockUploadItem
         JOIN DObInventoryDocument ON DObInventoryDocument.id = IObInitialStockUploadItem.refInstanceId
         JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         JOIN CObEnterprise businessUnit ON businessUnit.id = DObInventoryDocument.purchaseunitid
         JOIN MasterData item ON IObInitialStockUploadItem.itemid = item.id
         JOIN CObMeasure uom ON IObInitialStockUploadItem.unitofmeasureid = uom.id
WHERE DObInventoryDocument.inventoryDocumentType = 'ISU'

