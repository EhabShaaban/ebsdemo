CREATE OR REPLACE VIEW BankAccountBalanceGeneralModel As
WITH Balance AS (
    SELECT dobjournalentry.companyid          AS companyId,
           dobjournalentry.fiscalperiodid     AS fiscalYearId,
           dobjournalentry.purchaseunitid     AS businessUnitId,
           bank.subaccountid                  AS bankAccountNumberId,
           item.currency                      AS currencyId,
           SUM(item.debit) - SUM(item.credit) AS balance
    FROM dobjournalentry
             JOIN iobjournalentryitem item on dobjournalentry.id = item.refinstanceid
             JOIN iobjournalentryitembanksubaccount bank on bank.id = item.id
    GROUP BY bank.subaccountid,
             dobjournalentry.fiscalperiodid,
             dobjournalentry.purchaseunitid,
             dobjournalentry.companyid,
             item.currency
)
SELECT row_number() OVER ()                                                AS id,
       businessUnit.id                                                     AS businessUnitId,
       businessUnit.code                                                   AS businessUnitCode,
       businessUnit.name                                                   AS businessUnitName,
       company.id                                                          AS companyId,
       company.code                                                        AS companyCode,
       company.name                                                        AS companyName,
       fiscalYear.code                                                     AS fiscalYearCode,
       fiscalYear.name                                                     AS fiscalYearName,
       companyBank.id                                                      AS bankAccountNumberId,
       concat(companyBank.accountno, ' - ', bankAccountNumberCurrency.iso) AS bankAccountNumber,
       balance,
       currency.id                                                         AS currencyId,
       currency.code                                                       AS currencyCode,
       currency.iso                                                        AS currencyIso
FROM Balance
         JOIN cobenterprise businessUnit ON businessUnit.id = Balance.businessUnitId
         JOIN cobenterprise company ON company.id = Balance.companyId
         JOIN cobfiscalperiod fiscalYear ON fiscalYear.id = Balance.fiscalYearId
         JOIN cobcurrency currency ON currency.id = Balance.currencyId
         JOIN iobcompanybankdetails companyBank ON companyBank.id = Balance.bankAccountNumberId
         JOIN cobcurrency bankAccountNumberCurrency ON bankAccountNumberCurrency.id = companyBank.currencyid;

CREATE OR REPLACE VIEW TreasuryBalanceGeneralModel As
WITH Balance AS (
    SELECT dobjournalentry.companyid          AS companyId,
           dobjournalentry.fiscalperiodid     AS fiscalYearId,
           dobjournalentry.purchaseunitid     AS businessUnitId,
           treasury.subaccountid              AS treasuryId,
           item.currency                      AS currencyId,
           SUM(item.debit) - SUM(item.credit) AS balance
    FROM dobjournalentry
             JOIN iobjournalentryitem item on dobjournalentry.id = item.refinstanceid
             JOIN iobjournalentryitemtreasurysubaccount treasury on treasury.id = item.id
    GROUP BY treasury.subaccountid,
             dobjournalentry.fiscalperiodid,
             dobjournalentry.purchaseunitid,
             dobjournalentry.companyid,
             item.currency
)
SELECT row_number() OVER () AS id,
       businessUnit.id      AS businessUnitId,
       businessUnit.code    AS businessUnitCode,
       businessUnit.name    AS businessUnitName,
       company.id           AS companyId,
       company.code         AS companyCode,
       company.name         AS companyName,
       fiscalYear.code      AS fiscalYearCode,
       fiscalYear.name      AS fiscalYearName,
       treasury.id          AS treasuryId,
       treasury.code        AS treasuryCode,
       treasury.name        AS treasuryName,
       balance,
       currency.id          AS currencyId,
       currency.code        AS currencyCode,
       currency.iso         AS currencyIso
FROM Balance
         JOIN cobenterprise businessUnit ON businessUnit.id = Balance.businessUnitId
         JOIN cobenterprise company ON company.id = Balance.companyId
         JOIN cobfiscalperiod fiscalYear ON fiscalYear.id = Balance.fiscalYearId
         JOIN cobcurrency currency ON currency.id = Balance.currencyId
         JOIN cobtreasury treasury ON treasury.id = Balance.treasuryId;