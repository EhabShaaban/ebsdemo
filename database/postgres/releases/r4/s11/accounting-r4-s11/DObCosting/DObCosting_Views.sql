drop view IObCostingItemsGeneralModel;

create or replace view IObCostingItemsGeneralModel AS
select IObCostingItems.id,
       dobaccountingdocument.code as userCode,
       IObCostingItems.refinstanceid,
       IObCostingItems.quantity,
       IObCostingItems.stocktype,
       IObCostingItems.tobeconsideredincostingper,
       masterdata.code            as itemCode,
       masterdata.name            as itemName,
       cobmeasure.code            as uomCode,
       cobmeasure.symbol          as uomSymbol,
       IObCostingItems.creationDate,
       IObCostingItems.creationInfo,
       IObCostingItems.modifiedDate,
       IObCostingItems.modificationInfo,
       IObCostingItems.unitPrice,
       IObCostingItems.unitLandedCost
from IObCostingItems
         left join masterdata
                   on IObCostingItems.itemid = masterdata.id and masterdata.objecttypecode = '1'
         left join cobmeasure on IObCostingItems.uomid = cobmeasure.id
         left join dobCosting
                   on IObCostingItems.refinstanceid = dobCosting.id
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobCosting.id;

