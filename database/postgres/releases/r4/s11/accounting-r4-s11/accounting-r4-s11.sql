\ir 'JournalEntry/DropViews.sql'
\ir 'JournalEntry/DObJournalEntry.sql'
\ir 'JournalEntry/DObJournalEntry_Views.sql'
\ir 'Balances/Balances_Views.sql'
\ir 'DObCosting/DObCosting_Views.sql'
\ir 'DObMonetaryNote/DObMonetaryNote.sql'
\ir 'DObCollection/DObCollection.sql'
\ir 'DObCollection/DObCollection_Views.sql'
\ir 'DObVendorInvoice/DropViews.sql'
\ir 'Costing/Drop_Costing_Views.sql'
\ir 'DObVendorInvoice/DObVendorInvoice.sql'
\ir 'DObVendorInvoice/DObVendorInvoice_Views.sql'
\ir 'Costing/Costing_Views.sql'
\ir 'Settlement/DObSettlement_Views.sql'
\ir 'DObLandedCost/DObLandedCost_Views.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;