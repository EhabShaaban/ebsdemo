CREATE VIEW IObJournalEntryItemGeneralModel AS
select JEI.id,
       JEI.creationdate,
       JEI.creationinfo,
       JEI.modifieddate,
       JEI.modificationinfo,
       JEI.refinstanceid,
       dobjournalentry.code,
       dobjournalentry.duedate,
       dobjournalentry.objecttypecode AS referencetype,
       SUB_ACC.subledger              as subledger,
       hobglaccountgeneralmodel.code  AS accountcode,
       hobglaccountgeneralmodel.name  AS accountname,
       SUB_ACC.subaccountcode         as subaccountcode,
       SUB_ACC.subaccountname         as subaccountname,
       JEI.debit::varchar(255),
       JEI.credit::varchar(255),
       curren.code                    AS currencycode,
       curren.iso                     AS currencyname,
       curren2.code                   AS companycurrencycode,
       curren2.iso                    AS companycurrencyname,
       JEI.currencyprice::varchar(255),
       purchaseunit.code              AS purchaseunitcode,
       purchaseunit.name              AS purchaseunitname,
       purchaseunit.name ->> 'en'     AS purchaseunitnameen,
        JEI.exchangerateid             AS exhangerateid,
        cobexchangerate.code           AS exchangeratecode
        from iobjournalentryitem JEI
        LEFT JOIN dobjournalentry ON JEI.refinstanceid = dobjournalentry.id AND
        (dobjournalentry.objecttypecode = 'VendorInvoice' OR
        dobjournalentry.objecttypecode = 'PaymentRequest' OR
        dobjournalentry.objecttypecode = 'SalesInvoice' OR
        dobjournalentry.objecttypecode = 'Collection' OR
        dobjournalentry.objecttypecode = 'NotesReceivable' OR
        dobjournalentry.objecttypecode = 'LandedCost' OR
        dobjournalentry.objecttypecode = 'Settlement' OR
        dobjournalentry.objecttypecode = 'InitialBalanceUpload')
        left join hobglaccountgeneralmodel on (hobglaccountgeneralmodel.id = JEI.accountid)
        LEFT JOIN cobenterprise purchaseunit ON dobjournalentry.purchaseunitid = purchaseunit.id
        LEFT JOIN cobexchangerate ON cobexchangerate.id = JEI.exchangerateid
        left join cobcurrency curren on (JEI.currency = curren.id)
        left join cobcurrency curren2 on (JEI.companycurrency = curren2.id)
        left join IObJournalEntryItemSubAccounts SUB_ACC
        on (SUB_ACC.id = JEI.id AND SUB_ACC.type = JEI.type);