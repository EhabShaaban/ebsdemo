CREATE OR REPLACE VIEW DObCollectionGeneralModel AS
SELECT c.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode,
       c.collectiontype,
       company.code                                        AS companycode,
       company.name                                        AS companyname,
       cobcurrency.code                                    AS companylocalcurrencycode,
       cobcurrency.name                                    AS companylocalcurrencyname,
       cobcurrency.iso                                     AS companylocalcurrencyiso,
       masterdata.name                                     AS businessPartnername,
       masterdata.code                                     AS businessPartnercode,
       CASE
           WHEN masterdata.objecttypecode = '4' THEN cast('{"ar": "موظف", "en": "Employee"}' AS JSON)
           WHEN masterdata.objecttypecode = '3' THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
           WHEN masterdata.objecttypecode = '2' THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
           ELSE NULL END
                                                           AS businessPartnerType,
       iobcollectiondetails.amount::varchar ,
       refDocumentType.code                                AS refdocumenttypecode,
       refDocumentType.name                                AS refdocumenttypename,
       getRefDocumentCodeBasedOnType(iobcollectiondetails.id,
                                     refDocumentType.code) AS refDocumentCode,
       dobaccountingdocument.objecttypecode                AS collectionDocumentType,

       dobaccountingdocument.documentOwnerId                                   AS documentOwnerId,
       userinfo.name             AS documentOwner,
       ebsuser.username              AS documentOwnerUserName,
       iobcollectiondetails.collectionmethod

FROM dobcollection c
         LEFT JOIN iobcollectiondetails ON c.id = iobcollectiondetails.refinstanceid
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = c.id
         LEFT JOIN cobcollectiontype refDocumentType
                   ON iobcollectiondetails.refDocumentTypeId = refDocumentType.id
         LEFT JOIN mobbusinessPartner ON iobcollectiondetails.businessPartnerid = mobbusinessPartner.id
         LEFT JOIN masterdata ON mobbusinessPartner.id = masterdata.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON iobaccountingdocumentcompanydata.refInstanceId = c.id
                       AND iobaccountingdocumentcompanydata.objecttypecode = 'Collection'
         LEFT JOIN cobenterprise businessUnit
                   ON iobaccountingdocumentcompanydata.purchaseUnitId = businessUnit.id
         LEFT JOIN cobenterprise company
                   ON iobaccountingdocumentcompanydata.companyid = company.id
         LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id
         LEFT JOIN userinfo on userinfo.id = dobaccountingdocument.documentownerid
         LEFT JOIN ebsuser on ebsuser.id = dobaccountingdocument.documentownerid;

CREATE OR REPLACE VIEW IObCollectionDetailsGeneralModel AS
SELECT cd.id,
       cd.refInstanceId,
       crAccountingDocument.code                           AS collectionCode,
       crAccountingDocument.creationDate,
       crAccountingDocument.modifiedDate,
       crAccountingDocument.creationInfo,
       crAccountingDocument.modificationInfo,
       crAccountingDocument.currentStates,
       refDocumentType.code                                AS refdocumenttype,
       getRefDocumentCodeBasedOnType(cd.id,
                                     refDocumentType.code) AS refDocumentCode,
       cd.amount ::varchar,
       cobcurrency.iso                                     AS currencyISO,
       cobcurrency.name                                    AS currencyName,
       cobcurrency.code                                    AS currencyCode,
       companyCurrency.iso                                 AS companyCurrencyIso,
       companyCurrency.code                                AS companyCurrencyCode,
       businessPartner.code                                AS businessPartnerCode,
       businessPartner.name                                AS businessPartnername,
       CASE
           WHEN businessPartner.objecttypecode = '4' THEN cast('{"ar": "موظف", "en": "Employee"}' AS JSON)
           WHEN businessPartner.objecttypecode = '3' THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
           WHEN businessPartner.objecttypecode = '2' THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
           ELSE NULL END
                                                           AS businessPartnerType,
       cd.collectionMethod,
       cobbank.code                                        AS bankAccountCode,
       cobbank.name                                        AS bankAccountName,
       cobtreasury.code                                    AS treasuryCode,
       cobtreasury.name                                    AS treasuryName,
       CASE WHEN
                IObCompanyBankDetails.accountno IS NULL THEN NULL
            ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END                    AS bankAccountNumber,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode

FROM iobcollectiondetails cd
    LEFT JOIN cobcollectiontype refDocumentType
ON cd.refDocumentTypeId = refDocumentType.id
    LEFT JOIN mobbusinessPartner ON cd.businessPartnerid = mobbusinessPartner.id
    LEFT JOIN MasterData businessPartner ON businessPartner.id = cd.businessPartnerid
    LEFT JOIN dobcollection c ON c.id = cd.refinstanceid
    INNER JOIN dobaccountingdocument crAccountingDocument ON c.id = crAccountingDocument.id
    LEFT JOIN cobcurrency ON cd.currencyid = cobcurrency.id
    LEFT JOIN iobaccountingdocumentcompanydata companyDetails
    ON crAccountingDocument.id = companyDetails.refinstanceid
    LEFT JOIN cobenterprise businessUnit ON companyDetails.purchaseUnitId = businessUnit.id
    LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = cd.bankAccountId
    LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
    LEFT JOIN cobbank ON cobbank.id = IObCompanyBankDetails.bankid
    LEFT JOIN cobtreasury ON cobtreasury.id = cd.treasuryId
    LEFT JOIN IObAccountingDocumentCompanyData companyData on c.id = companyData.refinstanceid
    LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
    LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid;