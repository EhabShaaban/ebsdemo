ALTER TABLE IObVendorInvoiceDetailsDownPayment
ALTER COLUMN downPaymentAmount TYPE NUMERIC;

ALTER TABLE IObAccountingDocumentActivationDetails
ALTER COLUMN currencyPrice TYPE NUMERIC;

ALTER TABLE IObVendorInvoiceSummary
ALTER COLUMN remaining TYPE NUMERIC,
ALTER COLUMN downPayment TYPE NUMERIC;

ALTER TABLE IObVendorInvoiceItem
ALTER COLUMN quantityinorderunit TYPE NUMERIC,
ALTER COLUMN price TYPE NUMERIC;

ALTER TABLE IObVendorInvoiceTaxes
ALTER COLUMN percentage TYPE NUMERIC,
ALTER COLUMN amount TYPE NUMERIC;