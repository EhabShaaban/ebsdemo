DROP VIEW IF EXISTS iobvendorinvoiceremaininggeneralmodel;
DROP VIEW IF EXISTS dobvendorinvoicedebitpreparationgeneralmodel;
DROP VIEW IF EXISTS iobinvoicesummarygeneralmodel;
DROP VIEW IF EXISTS iobvendorinvoicetaxesgeneralmodel;
DROP VIEW IF EXISTS DObVendorInvoiceCreditPreparationGeneralModel;
DROP VIEW IF EXISTS dobsettlementjournalentrypreparationgeneralmodel;
DROP VIEW IF EXISTS IObAccountingDocumentActivationDetailsGeneralModel;
DROP VIEW IF EXISTS DObLandedCostItemsInvoicesGeneralModel;

--DON'T USE THIS VIEW OR COPY IT.
CREATE or REPLACE VIEW IObVendorInvoiceItemsGeneralModel AS
SELECT IObVendorInvoiceItem.id,
       vendorInvoice.code,
       item.code                                                                               as itemCode,
       item.id                                                                                 as itemId,
       item.name                                                                               as itemName,
       baseUnitOfMeasure.code                                                                  as baseUnitCode,
       baseUnitOfMeasure.symbol                                                                as baseUnitSymbol,
       orderUnitOfMeasure.code                                                                 as orderUnitCode,
       orderUnitOfMeasure.symbol                                                               as orderunitsymbol,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  AS conversionFactorToBase,
       ((
         getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                   IObVendorInvoiceItem.itemid))) as totalAmount,
       IObVendorInvoiceItem.creationDate,
       IObVendorInvoiceItem.modifiedDate,
       IObVendorInvoiceItem.creationInfo,
       IObVendorInvoiceItem.modificationInfo,
       IObVendorInvoiceItem.itemId::numeric as quantityinorderunit,
       IObVendorInvoiceItem.refinstanceid,
       IObVendorInvoiceItem.itemId::numeric as price,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  as orderUnitPrice

FROM IObVendorInvoiceItem
         left join masterdata item on IObVendorInvoiceItem.itemId = item.id
         left join cobmeasure orderUnitOfMeasure on IObVendorInvoiceItem.orderunitofmeasureid = orderUnitOfMeasure.id
         left join mobitem on IObVendorInvoiceItem.itemId = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument vendorInvoice on IObVendorInvoiceItem.refInstanceId = vendorInvoice.id
         left join dobvendorinvoice on vendorInvoice.id = dobvendorinvoice.id;