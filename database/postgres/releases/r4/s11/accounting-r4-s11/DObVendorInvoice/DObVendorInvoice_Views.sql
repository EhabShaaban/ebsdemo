CREATE OR REPLACE VIEW DObVendorInvoiceDebitPreparationGeneralModel AS
SELECT row_number() OVER ()                     AS id,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companydata.currencyid                   AS companycurrencyid,
       iobvendorinvoicepurchaseorder.currencyid AS documentcurrencyid,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
       iobitemaccountinginfo.chartofaccountid   AS glaccountid,
       CASE
           WHEN isgoodsinvoice(dobaccountingdocument.objecttypecode) THEN iobvendorinvoicepurchaseorder.purchaseorderid
           ELSE iobpurchaseorderfulfillervendor.referencepoid
           END                                  AS glsubaccountid,
       leafglaccount.leadger                    AS accountledger,
       getItemAmountAfterTaxes(dobvendorinvoice.id, iobvendorinvoiceitem.totalamount)                                 AS amount
FROM dobaccountingdocument
         JOIN dobvendorinvoice ON dobaccountingdocument.id = dobvendorinvoice.id
         LEFT JOIN iobaccountingdocumentactivationdetails activationdetails
                   ON dobaccountingdocument.id = activationdetails.refinstanceid
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata companydata
                   ON companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         LEFT JOIN iobvendorinvoicepurchaseorder ON iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON iobpurchaseorderfulfillervendor.refinstanceid = iobvendorinvoicepurchaseorder.purchaseorderid
         LEFT JOIN iobvendorinvoiceitemsgeneralmodel iobvendorinvoiceitem ON iobvendorinvoiceitem.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobitemaccountinginfo ON iobitemaccountinginfo.refinstanceid = iobvendorinvoiceitem.itemid
         LEFT JOIN leafglaccount ON leafglaccount.id = iobitemaccountinginfo.chartofaccountid;

CREATE OR REPLACE VIEW IObInvoiceSummaryGeneralModel AS
SELECT dobaccountingdocument.id                              as id,
       code                                                  as invoiceCode,
       (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                'SalesInvoice'
            WHEN isVendorInvoice(objectTypeCode) THEN
                'VendorInvoice' END)     as invoiceType,
       getInvoiceTotalAmountBeforTaxes(code, objecttypecode) as totalAmountBeforeTaxes,
       getInvoiceTotalTaxAmount(code, objecttypecode)        as taxAmount,
       getInvoiceNetAmount(code, objecttypecode)             as netAmount,
       (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                getDownPaymentForInvoice(code, objecttypecode)
            WHEN isVendorInvoice(objectTypeCode) THEN
                iobvendorinvoicesummary.downpayment END)     as downpayment,
       (CASE  WHEN objecttypecode = 'SalesInvoice' THEN
                  SiSummary.remaining
              WHEN isVendorInvoice(objectTypeCode) THEN
                  iobvendorinvoicesummary.remaining END)     as totalRemaining
FROM dobaccountingdocument
         LEFT JOIN IObSalesInvoiceSummary SiSummary
                   ON SiSummary.refInstanceId = dobaccountingdocument.id AND
                      dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join iobvendorinvoicesummary on IObVendorInvoiceSummary.refinstanceid = dobaccountingdocument.id
WHERE isInvoice(objecttypecode) ;

CREATE or REPLACE VIEW IObVendorInvoiceTaxesGeneralModel AS
SELECT IObVendorInvoiceTaxes.id,
       IObVendorInvoiceTaxes.creationDate,
       IObVendorInvoiceTaxes.modifiedDate,
       IObVendorInvoiceTaxes.creationInfo,
       IObVendorInvoiceTaxes.modificationInfo,
       IObVendorInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code   AS invoicecode,
       IObVendorInvoiceTaxes.code   AS taxcode,
       IObVendorInvoiceTaxes.name   AS taxname,
       LObTaxInfo.percentage        as taxpercentage,
       IObVendorInvoiceTaxes.amount as taxAmount
FROM IObVendorInvoiceTaxes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = IObVendorInvoiceTaxes.refInstanceId
         inner join dobvendorInvoice on dobvendorInvoice.id = IObVendorInvoiceTaxes.refinstanceid
         left join LObTaxInfo on LObTaxInfo.code = IObVendorInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis

--TODO: recreate view without joining with DObVendorInvoiceGeneralModel
CREATE or REPLACE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE WHEN invoiceSummary.totalremaining IS NULL THEN 0 ELSE invoiceSummary.totalremaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
                   ON dobInvoice.id = invoiceSummary.id;

CREATE OR REPLACE VIEW DObVendorInvoiceCreditPreparationGeneralModel AS
SELECT dobaccountingdocument.id ,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companyData.currencyid as companyCurrencyId,
       iobvendorinvoicepurchaseorder.currencyid as documentCurrencyId,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
       iobvendoraccountinginfo.chartofaccountid as glaccountId,
       iobvendorinvoicedetails.vendorid as glsubaccountId,
       leafglaccount.leadger as accountLedger,
       getInvoiceNetAmount(code, dobaccountingdocument.objecttypecode)             as amount
from dobaccountingdocument
         left join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         left join iobaccountingdocumentactivationdetails activationdetails
                   on dobaccountingdocument.id = activationdetails.refinstanceid
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
         left join iobvendorinvoicepurchaseorder on iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         left join iobvendorinvoicedetails on iobvendorinvoicedetails.refinstanceid = dobvendorinvoice.id
         left join iobvendoraccountinginfo on iobvendoraccountinginfo.refinstanceid = iobvendorinvoicedetails.vendorid
         left join leafglaccount on leafglaccount.id = iobvendoraccountinginfo.chartofaccountid;

CREATE Or Replace VIEW IObAccountingDocumentActivationDetailsGeneralModel AS
SELECT activationDetails.id,
       activationDetails.refinstanceid,
       activationDetails.creationDate,
       activationDetails.creationInfo,
       activationDetails.modificationInfo,
       activationDetails.modifieddate,
       accountingDocument.code,
       accountingDocument.objecttypecode                                 as documentType,
       activationDetails.accountant,
       activationDetails.activationdate,
       activationDetails.duedate,
       exchangeRate.code                                                 AS exchangeRateCode,
       exchangeRate.id                                                   AS exchangeRateId,
       exchangeRate.firstvalue,
       exchangeRate.secondvalue,
       exchangeRate.firstcurrencyiso,
       exchangeRate.secondcurrencyiso,
       activationDetails.currencyprice,
       getJournalEntryCodeDependsOnObjectTypeCode(activationDetails.objecttypecode,
                                                  accountingDocument.id) as journalEntryCode,
       activationDetails.objecttypecode,
       fiscalPeriod.id                                                  AS FiscalPeriodId,
       fiscalPeriod.name::json ->> 'en'                                 AS FiscalPeriod
FROM IObAccountingDocumentActivationDetails activationDetails
    left JOIN dobaccountingdocument accountingDocument
on accountingDocument.id = activationDetails.refinstanceid
    left join cobexchangerategeneralmodel exchangeRate
    on activationDetails.exchangerateid = exchangeRate.id
    left join CObFiscalPeriod fiscalPeriod
    on activationDetails.fiscalPeriodId=fiscalPeriod.id;

CREATE or REPLACE VIEW IObVendorInvoiceItemsGeneralModel AS
SELECT IObVendorInvoiceItem.id,
       vendorInvoice.code,
       item.code                                                                               as itemCode,
       item.id                                                                                 as itemId,
       item.name                                                                               as itemName,
       baseUnitOfMeasure.code                                                                  as baseUnitCode,
       baseUnitOfMeasure.symbol                                                                as baseUnitSymbol,
       orderUnitOfMeasure.code                                                                 as orderUnitCode,
       orderUnitOfMeasure.symbol                                                               as orderunitsymbol,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  AS conversionFactorToBase,
       ((IObVendorInvoiceItem.quantityinorderunit *
         getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                   IObVendorInvoiceItem.itemid)) * IObVendorInvoiceItem.price) as totalAmount,
       IObVendorInvoiceItem.creationDate,
       IObVendorInvoiceItem.modifiedDate,
       IObVendorInvoiceItem.creationInfo,
       IObVendorInvoiceItem.modificationInfo,
       IObVendorInvoiceItem.quantityinorderunit,
       IObVendorInvoiceItem.refinstanceid,
       IObVendorInvoiceItem.price,
       IObVendorInvoiceItem.price *
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  as orderUnitPrice

FROM IObVendorInvoiceItem
         left join masterdata item on IObVendorInvoiceItem.itemId = item.id
         left join cobmeasure orderUnitOfMeasure on IObVendorInvoiceItem.orderunitofmeasureid = orderUnitOfMeasure.id
         left join mobitem on IObVendorInvoiceItem.itemId = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument vendorInvoice on IObVendorInvoiceItem.refInstanceId = vendorInvoice.id
         left join dobvendorinvoice on vendorInvoice.id = dobvendorinvoice.id;