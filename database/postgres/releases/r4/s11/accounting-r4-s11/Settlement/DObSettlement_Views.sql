CREATE OR REPLACE VIEW DObSettlementJournalEntryPreparationGeneralModel AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       iobaccountingdocumentcompanydata.purchaseunitid as businessunitid,
       iobaccountingdocumentcompanydata.companyid,
       iobaccountingdocumentactivationdetails.fiscalperiodid,
       iobaccountingdocumentactivationdetails.exchangerateid,
       iobaccountingdocumentactivationdetails.currencyprice,
       companyData.currencyid companycurrencyid,
       iobsettlementdetails.currencyid documentcurrencyid
from dobaccountingdocument
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobaccountingdocumentactivationdetails
                   on dobaccountingdocument.id = iobaccountingdocumentactivationdetails.refinstanceid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
         left join iobsettlementdetails on iobsettlementdetails.refinstanceid = dobaccountingdocument.id
WHERE dobaccountingdocument.objecttypecode = 'Settlement'
  And iobaccountingdocumentcompanydata.objecttypecode = 'Settlement'
  And iobaccountingdocumentactivationdetails.objecttypecode = 'Settlement';