ALTER TABLE iobmonetarynotesdetails RENAME COLUMN noteDate TO dueDate;
ALTER TABLE iobmonetarynotesreferencedocuments RENAME COLUMN amounttopay TO amounttocollect;

DROP VIEW IF EXISTS DObNotesReceivablesGeneralModel;
CREATE VIEW DObNotesReceivablesGeneralModel AS
SELECT dobmonetarynotes.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.objecttypecode,
       MasterData.code                                          AS businessPartnerCode,
       MasterData.name                                          AS businessPartnerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
        iobmonetarynotesdetails.remaining,
        dobaccountingdocument.documentownerid,
        userinfo.name                                            AS documentOwner,
        concat(Company.code, ' - ', Company.name::json ->> 'en') AS companyCodeName,
        Company.code                                             AS companyCode,
        Company.name                                             AS companyName,
        iobmonetarynotesdetails.notenumber,
        iobmonetarynotesdetails.duedate,
        iobmonetarynotesdetails.amount,
        CObCurrency.iso                                          AS currencyIso,
        iobmonetarynotesdetails.notebank,
        iobmonetarynotesdetails.noteForm,
        lobdepot.code                                            AS depotCode,
        lobdepot.name                                            AS depotName
        FROM dobmonetarynotes
        JOIN dobaccountingdocument
        ON dobaccountingdocument.id = dobmonetarynotes.id AND
        objecttypecode LIKE 'NOTES_RECEIVABLE_%'
        LEFT JOIN userinfo ON dobaccountingdocument.documentownerid = userinfo.id
        LEFT JOIN ebsuser ON dobaccountingdocument.documentownerid = ebsuser.id
        LEFT JOIN iobmonetarynotesdetails
        ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
        LEFT JOIN iobaccountingdocumentcompanydata
        ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
        LEFT JOIN CObEnterprise BusinessUnit
        ON iobaccountingdocumentcompanydata.purchaseunitid = BusinessUnit.id AND
        BusinessUnit.objecttypecode = '5'
        LEFT JOIN CObEnterprise Company
        ON iobaccountingdocumentcompanydata.companyid = Company.id AND
        Company.objecttypecode = '1'
        LEFT JOIN lobdepot ON lobdepot.id = iobmonetarynotesdetails.depotid
        LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
        LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
        MasterData.objecttypecode = '3';

DROP VIEW IF EXISTS IObMonetaryNotesDetailsGeneralModel;
CREATE VIEW IObMonetaryNotesDetailsGeneralModel AS
SELECT IObMonetaryNotesDetails.id,
       IObMonetaryNotesDetails.refinstanceid,
       IObMonetaryNotesDetails.creationInfo,
       IObMonetaryNotesDetails.modificationInfo,
       IObMonetaryNotesDetails.creationDate,
       IObMonetaryNotesDetails.modifiedDate,
       dobaccountingdocument.code AS userCode,
       dobaccountingdocument.objecttypecode,
       iobmonetarynotesdetails.noteForm,
       MasterData.code            AS businessPartnerCode,
       MasterData.name            AS businessPartnerName,
       iobmonetarynotesdetails.notenumber,
       iobmonetarynotesdetails.notebank,
       iobmonetarynotesdetails.duedate,
       CObCurrency.code           AS currencyCode,
       CObCurrency.iso            AS currencyIso,
       iobmonetarynotesdetails.amount,
       iobmonetarynotesdetails.remaining,
       lobdepot.code              AS depotCode,
       lobdepot.name              AS depotName
FROM IObMonetaryNotesDetails
         LEFT JOIN dobmonetarynotes ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = dobmonetarynotes.id
         LEFT JOIN lobdepot ON lobdepot.id = iobmonetarynotesdetails.depotid
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '3';

DROP VIEW IF EXISTS DObNotesPayableGeneralModel;
CREATE VIEW DObNotesPayableGeneralModel AS
SELECT dobmonetarynotes.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.objecttypecode,
       MasterData.code                                          AS businessPartnerCode,
       MasterData.name                                          AS businessPartnerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
        iobmonetarynotesdetails.remaining,
        dobaccountingdocument.documentownerid,
        userinfo.name                                            AS documentOwner,
        Company.code                                             AS companyCode,
        Company.name                                             AS companyName,
        iobmonetarynotesdetails.duedate,
        iobmonetarynotesdetails.amount,
        CObCurrency.iso                                          AS currencyIso,
        iobmonetarynotesdetails.noteForm
        FROM dobmonetarynotes
        JOIN dobaccountingdocument
        ON dobaccountingdocument.id = dobmonetarynotes.id AND
        objecttypecode LIKE 'NOTES_PAYABLE_%'
        LEFT JOIN userinfo ON dobaccountingdocument.documentownerid = userinfo.id
        LEFT JOIN ebsuser ON dobaccountingdocument.documentownerid = ebsuser.id
        LEFT JOIN iobmonetarynotesdetails
        ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
        LEFT JOIN iobaccountingdocumentcompanydata
        ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
        LEFT JOIN CObEnterprise BusinessUnit
        ON iobaccountingdocumentcompanydata.purchaseunitid = BusinessUnit.id AND
        BusinessUnit.objecttypecode = '5'
        LEFT JOIN CObEnterprise Company
        ON iobaccountingdocumentcompanydata.companyid = Company.id AND
        Company.objecttypecode = '1'
        LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
        LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
        MasterData.objecttypecode = '2';

ALTER TABLE IObMonetaryNotesReferenceDocuments DROP COLUMN dueData;

ALTER TABLE IObMonetaryNotesDebitNoteReferenceDocument RENAME TO IObNotesReceivableReferenceDocumentDebitNote;
ALTER SEQUENCE IF EXISTS IObMonetaryNotesDebitNoteReferenceDocument_id_seq RENAME TO IObNotesReceivableReferenceDocumentDebitNote_id_seq;

ALTER TABLE IObMonetaryNotesSalesOrderReferenceDocument RENAME TO IObNotesReceivableReferenceDocumentSalesOrder;
ALTER SEQUENCE IF EXISTS IObMonetaryNotesSalesOrderReferenceDocument_id_seq RENAME TO IObNotesReceivableReferenceDocumentSalesOrder_id_seq;

ALTER TABLE IObMonetaryNotesSalesInvoiceReferenceDocument RENAME TO IObNotesReceivableReferenceDocumentSalesInvoice;
ALTER SEQUENCE IF EXISTS IObMonetaryNotesSalesInvoiceReferenceDocument_id_seq RENAME TO IObNotesReceivableReferenceDocumentSalesInvoice_id_seq;


CREATE TABLE IObNotesReceivableReferenceDocumentNotesReceivable
(
    id         BIGSERIAL NOT NULL REFERENCES IObMonetaryNotesReferenceDocuments (id) ON DELETE CASCADE,
    documentId INT8      NOT NULL REFERENCES dobmonetarynotes (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

DROP VIEW IF EXISTS IObNotesReceivablesReferenceDocumentGeneralModel;
CREATE VIEW IObNotesReceivablesReferenceDocumentGeneralModel AS
SELECT IObMonetaryNotesReferenceDocuments.id,
       IObMonetaryNotesReferenceDocuments.refInstanceId,
       notesReceivable.code as notesReceivableCode,
       notesReceivable.creationInfo,
       notesReceivable.modificationInfo,
       notesReceivable.creationDate,
       notesReceivable.modifiedDate,
       notesReceivable.currentStates,
       notesReceivable.objecttypecode,
       IObMonetaryNotesReferenceDocuments.documentType,
       IObMonetaryNotesReferenceDocuments.amounttocollect,
       (CASE
            WHEN debitNote.code IS NULL AND salesInvoice.code IS NULL AND notesReceivableRefDoc.code IS NULL
                THEN dobsalesorder.code
            WHEN
                    salesInvoice.code IS NULL AND notesReceivableRefDoc.code IS NULL AND dobsalesorder.code IS NULL
                THEN debitNote.code
            WHEN
                    notesReceivableRefDoc.code IS NULL AND dobsalesorder.code IS NULL AND debitNote.code IS NULL
                THEN salesInvoice.code
            WHEN
                    salesInvoice.code IS NULL AND dobsalesorder.code IS NULL AND debitNote.code IS NULL
                THEN notesReceivableRefDoc.code
            ELSE NULL END
           ) AS refDocumentCode,
       (CASE
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'SALES_INVOICE'
                THEN ('{"en":"Sales Invoice","ar":"فاتورة مبيعات"}') :: json
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'SALES_ORDER'
                THEN ('{"en":"Sales Order","ar":"طلب بيع"}') :: json
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'DEBIT_NOTE'
                THEN ('{"en":"Debit Note","ar":"مذكرة خصم"}') :: json
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'NOTES_RECEIVABLE'
                THEN ('{"en":"Notes Receivable","ar":"ورقة قبض"}') :: json
            ELSE NULL END
           ) AS refDocumentName,
       CObCurrency.iso            AS currencyIso
FROM dobaccountingdocument notesReceivable
         LEFT JOIN dobmonetarynotes
                   ON notesReceivable.id = dobmonetarynotes.id AND
                      objecttypecode LIKE 'NOTES_RECEIVABLE_%'
         JOIN IObMonetaryNotesReferenceDocuments
              on IObMonetaryNotesReferenceDocuments.refInstanceId = dobmonetarynotes.id
         LEFT JOIN IObNotesReceivableReferenceDocumentDebitNote
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentDebitNote.id
         LEFT JOIN dobaccountingdocument debitNote
                   on IObNotesReceivableReferenceDocumentDebitNote.documentId = debitNote.id
         LEFT JOIN IObNotesReceivableReferenceDocumentSalesOrder
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentSalesOrder.id
         LEFT JOIN dobsalesorder on IObNotesReceivableReferenceDocumentSalesOrder.documentId = dobsalesorder.id
         LEFT JOIN IObNotesReceivableReferenceDocumentSalesInvoice
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentSalesInvoice.id
         LEFT JOIN dobaccountingdocument salesInvoice
                   on IObNotesReceivableReferenceDocumentSalesInvoice.documentId = salesInvoice.id
         LEFT JOIN IObNotesReceivableReferenceDocumentNotesReceivable
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentNotesReceivable.id
         LEFT JOIN dobaccountingdocument notesReceivableRefDoc
                   on IObNotesReceivableReferenceDocumentNotesReceivable.documentId = notesReceivableRefDoc.id
         LEFT JOIN iobmonetarynotesdetails ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid;
