\ir 'accounting-r4-s11/accounting-r4-s11.sql'
\ir 'systemdata/Depot.sql'
\ir 'inventory-r4-s11/inventory-r4-s11.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
