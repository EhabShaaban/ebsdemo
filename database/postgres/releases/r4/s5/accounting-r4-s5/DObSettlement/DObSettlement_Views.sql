CREATE OR REPLACE VIEW IObSettlementAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code                                                    AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger                                                      AS subLedger,
	   glAccount.code										                         as glAccountCode,
	   glAccount.name														      	 as glAccountName,
	   accountDetails.glaccountid													 as glAccountId,
       accountDetails.amount,
       glSubAccount.code															 as glSubAccountCode,
       glSubAccount.subaccountid  													 as glSubAccountId,
       glSubAccount.name    														 as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObSettlement settlement on accountDetails.refinstanceid = settlement.id
         left join dobaccountingdocument on dobaccountingdocument.id = settlement.id
         left join hobglaccountgeneralmodel glAccount on glAccount.id = accountDetails.glaccountid
         left join SubAccountGeneralModel glSubAccount
         	on accountDetails.subledger = glSubAccount.ledger
			and glSubAccount.subaccountid = (
    		case 
		      when accountDetails.subledger = 'Banks'
		      then (
		      		SELECT subDetails.glSubAccountId
			        FROM IObAccountingDocumentBankAccountingDetails subDetails
			        WHERE subDetails.id = accountDetails.id
		           )
	 			      
		      when accountDetails.subledger = 'Local_Customer'
		      then (
		      		SELECT subDetails.glSubAccountId
			        FROM IObAccountingDocumentCustomerAccountingDetails subDetails
			        WHERE subDetails.id = accountDetails.id
		           )
		      
		      when accountDetails.subledger = 'Notes_Receivables'
		      then (
		      		SELECT subDetails.glSubAccountId
			        FROM IObAccountingDocumentNotesReceivableAccountingDetails subDetails
			        WHERE subDetails.id = accountDetails.id
		           )
		      
		      when accountDetails.subledger = 'PO'
		      then (
		      		SELECT subDetails.glSubAccountId
			        FROM IObAccountingDocumentOrderAccountingDetails subDetails
			        WHERE subDetails.id = accountDetails.id
		           )
		      
		      when accountDetails.subledger = 'Treasuries'
		      then (
		      		SELECT subDetails.glSubAccountId
			        FROM IObAccountingDocumentTreasuryAccountingDetails subDetails
			        WHERE subDetails.id = accountDetails.id
		           )
		      
		      when accountDetails.subledger = 'Local_Vendors'
		      then (
		      		SELECT subDetails.glSubAccountId
			        FROM IObAccountingDocumentVendorAccountingDetails subDetails
			        WHERE subDetails.id = accountDetails.id
		           )
			   end	           
		) ;
		
	
Drop VIEW If EXISTS DObSettlementActivationPreparation;
Drop VIEW If EXISTS DObSettlementJournalEntryPreparationGeneralModel;

CREATE OR REPLACE VIEW DObSettlementActivationPreparation AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       cobfiscalperiod.id             as fiscalPeriodId,
       latestexchangerate.exchangerateid as exchangeRateId,
       latestexchangerate.currencyprice
from dobsettlement
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobsettlement.id
         left join cobfiscalperiod ON cobfiscalperiod.currentstates like '%Active%'
         left join iobsettlementdetails on iobsettlementdetails.refinstanceid = dobaccountingdocument.id
         left join iobaccountingdocumentcompanydata
                   on iobaccountingdocumentcompanydata.refinstanceid = dobaccountingdocument.id
    left join iobenterprisebasicdata on iobaccountingdocumentcompanydata.companyid = iobenterprisebasicdata.refinstanceid
         left join getLatestCurrencyPrice(iobsettlementdetails.currencyid, iobenterprisebasicdata.currencyid ) latestexchangerate
                    on latestexchangerate.firstcurrencyid = iobsettlementdetails.currencyid
                        and latestexchangerate.secondcurrencyid = iobenterprisebasicdata.currencyid;



 CREATE OR REPLACE VIEW DObSettlementJournalEntryPreparationGeneralModel AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       iobaccountingdocumentcompanydata.purchaseunitid as businessunitid,
       iobaccountingdocumentcompanydata.companyid,
       iobaccountingdocumentactivationdetails.fiscalperiodid,
       iobaccountingdocumentactivationdetails.exchangerateid,
       iobaccountingdocumentactivationdetails.currencyprice,
       companyData.currencyid companycurrencyid,
    iobsettlementdetails.currencyid documentcurrencyid
from dobaccountingdocument
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobaccountingdocumentactivationdetails
                   on dobaccountingdocument.id = iobaccountingdocumentactivationdetails.refinstanceid
left join iobenterprisebasicdata companyData on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
left join iobsettlementdetails on iobsettlementdetails.refinstanceid = dobaccountingdocument.id
WHERE dobaccountingdocument.objecttypecode = 'Settlement'
  And iobaccountingdocumentcompanydata.objecttypecode = 'Settlement'
  And iobaccountingdocumentactivationdetails.objecttypecode = 'Settlement';

CREATE OR REPLACE VIEW IObSettlementSummaryGeneralModel AS
SELECT dobaccountingdocument.code                                                                   AS settlementCode,
       dobSettlement.id,
       accountingdetails.REFINSTANCEID,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       cobcurrency.code                                                                             AS currencyCode,
       cobcurrency.iso                                                                              AS currencyISO,
       sum(accountingdetails.amount) FILTER (WHERE accountingdetails.accountingentry = 'CREDIT')    AS totalCredit,
       sum(accountingdetails.amount) FILTER (WHERE accountingdetails.accountingentry = 'DEBIT')     AS totalDebit
FROM dobSettlement
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobSettlement.id
         LEFT JOIN IObSettlementDetails on dobaccountingdocument.id = IObSettlementDetails.refinstanceid
         LEFT JOIN cobcurrency ON cobcurrency.id = IObSettlementDetails.currencyId
         LEFT JOIN iobaccountingdocumentaccountingdetails accountingdetails
                   ON accountingdetails.refinstanceid = dobSettlement.id
GROUP BY dobaccountingdocument.code,
         dobSettlement.id,
         accountingdetails.REFINSTANCEID,
         dobaccountingdocument.creationDate,
         dobaccountingdocument.modifiedDate,
         dobaccountingdocument.creationInfo,
         dobaccountingdocument.modificationInfo,
         cobcurrency.code,
         cobcurrency.iso;