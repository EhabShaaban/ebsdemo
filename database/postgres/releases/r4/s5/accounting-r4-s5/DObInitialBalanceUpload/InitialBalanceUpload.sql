CREATE TABLE DObInitialBalanceUpload
(
    id              BIGSERIAL REFERENCES DObAccountingDocument (id) ON DELETE CASCADE NOT NULL,
    documentOwnerId INT8 REFERENCES ebsuser (id) ON DELETE RESTRICT,
    fiscalPeriodId INT8 REFERENCES cobfiscalperiod (id) ON DELETE RESTRICT
);

