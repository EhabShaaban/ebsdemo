\ir 'DObInitialBalanceUpload/InitialBalanceUpload.sql'
\ir 'DObInitialBalanceUpload/InitialBalanceUpload_Views.sql'
\ir 'DObJournalEntry/DObJournalEntry.sql'
\ir 'DObJournalEntry/DObJournalEntry_Views.sql'
\ir 'PaymentRequest/DObPaymentRequest.sql'
\ir 'PaymentRequest/DObPaymentRequest_Views.sql'
\ir 'DObSettlement/DObSettlement_Views.sql'
\ir 'Balances/Balances_Views.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;