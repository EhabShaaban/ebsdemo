ALTER TABLE IObPaymentRequestPaymentDetails
    ADD COLUMN bankTransRef VARCHAR(1024);
ALTER TABLE IObPaymentRequestPaymentDetails
    ADD COLUMN expectedDueDate TIMESTAMP With time zone;
