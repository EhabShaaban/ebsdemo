CREATE OR REPLACE VIEW IObPaymentRequestPaymentDetailsGeneralModel AS
SELECT pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                                                                       as businessPartnerCode,
       md.name                                                                       as businessPartnerName,
       pd.duedocumenttype,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           when duedocumenttype = 'CREDITNOTE' then creditNote.code
           else doborderdocument.code end                                            as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso                                                               as currencyISO,
       cobcurrency.name                                                              as currencyName,
       cobcurrency.code                                                              as currencyCode,
       companyCurrency.iso                                                           AS companyCurrencyIso,
       companyCurrency.code                                                          AS companyCurrencyCode,
       companyCurrency.id                                                            AS companyCurrencyId,
       pd.description,
       pd.paymentForm,
       IObCompanyBankDetails.id                                                      AS bankAccountId,
       companyBank.code                                                              AS bankAccountCode,
       companyBank.name                                                              AS bankAccountName,
       cobtreasury.code                                                              AS treasuryCode,
       cobtreasury.name                                                              AS treasuryName,
       CASE
           WHEN
               IObCompanyBankDetails.accountno IS NULL THEN NULL
           ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END AS bankAccountNumber,
       item.code                                                                     AS costFactorItemCode,
       item.name                                                                     AS costFactorItemName,

       pd.expectedDueDate                                                            AS expectedDueDate,
       pd.bankTransRef                                                               AS bankTransRef
FROM iobpaymentrequestpaymentdetails pd
         LEFT JOIN IObPaymentRequestInvoicePaymentDetails ipd on ipd.id = pd.id
         LEFT JOIN iobpaymentrequestcreditnotepaymentdetails Cpd on Cpd.id = pd.id
         LEFT JOIN IObPaymentRequestPurchaseOrderPaymentDetails dpd on dpd.id = pd.id
         LEFT JOIN dobvendorinvoice on dobvendorinvoice.id = ipd.duedocumentid
         LEFT JOIN dobaccountingdocument vendorInvoice on vendorInvoice.id = dobvendorinvoice.id
         LEFT JOIN dobaccountingnote on dobaccountingnote.id = Cpd.duedocumentid
         LEFT JOIN dobaccountingdocument creditNote on creditNote.id = dobaccountingnote.id
         LEFT JOIN doborderdocument on doborderdocument.id = dpd.duedocumentid AND
                                       doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN masterdata md on md.id = pd.businessPartnerId
         LEFT JOIN dobpaymentrequest pr on pr.id = pd.refinstanceid
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = pr.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
         LEFT JOIN cobcurrency on pd.currencyid = cobcurrency.id
         LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
         LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = pd.bankAccountId
         LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN MObItemGeneralModel item on item.id = pd.costFactorItemId
         LEFT JOIN cobtreasury on cobtreasury.id = pd.treasuryId;


CREATE OR REPLACE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       paymentdetails.paymentForm,
       paymentdetails.netAmount           AS amountValue,
       cobcurrency.iso                    AS amountCurrency,
       companyData.purchaseUnitId,
       purchaseunit.name                  AS purchaseUnitName,
       purchaseunit.name:: json ->> 'en'  AS purchaseUnitNameEn,
       purchaseunit.code                  AS purchaseUnitCode,
       businesspartner.code               AS businessPartnerCode,
       businesspartner.name               AS businessPartnerName,
       duedocumenttype                    AS dueDocumentType,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           when duedocumenttype = 'CREDITNOTE' then creditNote.code
           else doborderdocument.code end as referenceDocumentCode,
       case
           when duedocumenttype = 'INVOICE' then '{
             "en": "Vendor Invoice",
             "ar": "فاتورة مورد"
           }'::json
           when duedocumenttype = 'PURCHASEORDER' then '{
             "en": "Purchase Order",
             "ar": "أمر شراء"
           }'::json
           when duedocumenttype = 'CREDITNOTE' then '{
             "en": "Credit Note",
             "ar": "الاستحقاقات الالحقاية"
           }'::json end                   as referenceDocumentType,
       DObPaymentRequest.remaining,
       paymentdetails.expectedDueDate                                                                   AS expectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS fromExpectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS toExpectedDueDate,
       paymentdetails.bankTransRef                                                                      AS bankTransRef
FROM DObPaymentRequest
         left join dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN cobenterprise purchaseunit ON purchaseunit.id = companyData.purchaseunitid
         LEFT JOIN iobpaymentrequestpaymentdetails paymentdetails on dobpaymentrequest.id = paymentdetails.refinstanceid
         LEFT JOIN mobbusinesspartner on mobbusinesspartner.id = paymentdetails.businesspartnerid
         LEFT JOIN masterdata businesspartner on mobbusinesspartner.id = businesspartner.id
         left join IObPaymentRequestInvoicePaymentDetails invoicepaymentdetails
                   on invoicepaymentdetails.id = paymentdetails.id
         left join iobpaymentrequestcreditnotepaymentdetails creditNotepaymentdetails
                   on creditNotepaymentdetails.id = paymentdetails.id
         left join IObPaymentRequestPurchaseOrderPaymentDetails orderpaymentdetails
                   on orderpaymentdetails.id = paymentdetails.id
         left join dobaccountingdocument vendorInvoice on vendorInvoice.id = invoicepaymentdetails.duedocumentid
         left join dobaccountingdocument creditNote on creditNote.id = creditNotepaymentdetails.duedocumentid
         left join doborderdocument on doborderdocument.id = orderpaymentdetails.duedocumentid
         left join cobcurrency on paymentdetails.currencyid = cobcurrency.id;
