
alter table DObSettlementJournalEntry
    add CONSTRAINT FK_DObSettlementJournalEntry_id FOREIGN KEY (id) REFERENCES DObJournalEntry (id) On DELETE CASCADE ;



alter table DObSettlementJournalEntry
    add CONSTRAINT FK_DObSettlementJournalEntry_documentReferenceId FOREIGN KEY (documentReferenceId) REFERENCES DObSettlement (id) On DELETE RESTRICT ;



alter table DObLandedCostJournalEntry
    add CONSTRAINT FK_DObLandedCostJournalEntry_id FOREIGN KEY (id) REFERENCES DObJournalEntry (id) On DELETE CASCADE ;



alter table DObLandedCostJournalEntry
    add CONSTRAINT FK_DObLandedCostJournalEntry_documentReferenceId FOREIGN KEY (documentReferenceId) REFERENCES DObLandedCost (id) On DELETE RESTRICT ;


ALTER TABLE DObCollectionJournalEntry DROP CONSTRAINT FK_DObCollectionRequestJournalEntry_DObJournalEntry;


ALTER TABLE DObCollectionJournalEntry
    ADD CONSTRAINT FK_DObCollectionJournalEntry_DObJournalEntry FOREIGN KEY (id)
        REFERENCES DObJournalEntry (id) ON DELETE CASCADE ;


