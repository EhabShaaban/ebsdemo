CREATE TABLE IObInitialStockUploadItem
(
    id                 BIGSERIAL                NOT NULL,
    refInstanceId      INT8 REFERENCES DObInventoryDocument (id) ON DELETE CASCADE,
    creationDate       TIMESTAMP                NOT NULL,
    modifiedDate       TIMESTAMP                NOT NULL,
    creationInfo       VARCHAR(1024)            NOT NULL,
    modificationInfo   VARCHAR(1024)            NOT NULL,
    originalAddingDate TIMESTAMP With time zone NOT NULL,
    itemId             INT8 REFERENCES MObItem (id) ON DELETE CASCADE,
    unitOfMeasureId    INT8 REFERENCES CObMeasure (id) ON DELETE CASCADE,
    stockType          VARCHAR(1024)            NOT NULL,
    quantity           float                    NOT NULL,
    actualCost         double precision,
    remainingQuantity  float DEFAULT 0
);
