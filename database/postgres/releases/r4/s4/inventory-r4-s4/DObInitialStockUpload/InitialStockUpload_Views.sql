CREATE VIEW IObInitialStockUploadItemGeneralModel AS
SELECT IObInitialStockUploadItem.id,
       IObInitialStockUploadItem.creationDate,
       IObInitialStockUploadItem.modifiedDate,
       IObInitialStockUploadItem.creationInfo,
       IObInitialStockUploadItem.modificationInfo,
       DObInventoryDocument.code                 AS initialStockUploadCode,
       DObInventoryDocument.currentStates,
       businessUnit.name                         AS purchaseUnitName,
       businessUnit.name:: JSON ->> 'en'         AS purchaseUnitNameEn,
       businessUnit.code                         AS purchaseUnitCode,
       IObInitialStockUploadItem.itemid          AS itemId,
       item.code                                 AS itemCode,
       item.name                                 AS itemName,
       IObInitialStockUploadItem.unitofmeasureid AS unitOfMeasureId,
       uom.code                                  AS unitOfMeasureCode,
       uom.symbol                                AS unitOfMeasureSymbol,
       IObInitialStockUploadItem.originaladdingdate,
       IObInitialStockUploadItem.stocktype,
       IObInitialStockUploadItem.quantity,
       IObInitialStockUploadItem.actualcost,
       IObInitialStockUploadItem.remainingquantity
FROM IObInitialStockUploadItem
         JOIN DObInventoryDocument ON DObInventoryDocument.id = IObInitialStockUploadItem.refInstanceId
         JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         JOIN CObEnterprise businessUnit ON businessUnit.id = DObInventoryDocument.purchaseunitid
         JOIN MasterData item ON IObInitialStockUploadItem.itemid = item.id
         JOIN CObMeasure uom ON IObInitialStockUploadItem.unitofmeasureid = uom.id
WHERE DObInventoryDocument.inventoryDocumentType = 'ISU'
