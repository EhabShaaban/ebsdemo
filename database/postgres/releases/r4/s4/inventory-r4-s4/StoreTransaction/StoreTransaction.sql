CREATE TABLE TObInitialStockUploadStoreTransaction
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE TObInitialStockUploadStoreTransaction
    ADD CONSTRAINT FK_TObInitialStockUploadStoreTransaction_DObStoreTransaction FOREIGN KEY (id) REFERENCES TObStoreTransaction (id) ON DELETE RESTRICT;