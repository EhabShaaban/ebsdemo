CREATE or REPLACE FUNCTION getStoreTransactionRefDocumentType(objectTypeCode varchar, transactionId bigint)
    RETURNS json AS
$$
BEGIN
RETURN case
           when objecttypecode = 'GR' then '{"en":"Goods Receipt","ar": "إذن إضافة"}'
           when objecttypecode = 'GI' then '{"en":"Goods Issue","ar": "اذون صرف البضاعة"}'
           when objecttypecode = 'STK_TR' then '{"en":"Stock Transformation","ar": "تحويل المخزون"}'
           when objecttypecode = 'ISU' then '{"en":"Initial Stock Upload","ar": "رصيد أول المدة"}'
    end;
END;
$$
LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getStoreTransactionRefDocumentCode(objectTypeCode varchar, transactionId bigint)
  RETURNS varchar AS
$$
BEGIN
RETURN case
           when objecttypecode = 'GR' and (select documentreferenceid
                                           from tobgoodsreceiptstoretransaction gr
                                           where gr.id = transactionId) is null then 'Legacy System نظام المتكامل'
           when objecttypecode = 'GR' then
               (select code
                from dobinventorydocument
                where id = (select documentreferenceid
                            from tobgoodsreceiptstoretransaction gr
                            where gr.id = transactionId))

           when objecttypecode = 'GI' then
               (select code
                from dobinventorydocument
                where id = (select documentreferenceid
                            from TObGoodsIssueStoreTransaction gi
                            where gi.id = transactionId))

           when objecttypecode = 'STK_TR' then
               (select code
                from dobinventorydocument
                where id = (select documentreferenceid
                            from TObstocktransformationStoreTransaction stktr
                            where stktr.id = transactionId))
           when objecttypecode = 'ISU' then
               (select code
                from dobinventorydocument
                where id = (select documentreferenceid
                            from TObInitialStockUploadStoreTransaction isu
                            where isu.id = transactionId))

    end;
END;
$$
LANGUAGE PLPGSQL;


CREATE VIEW TObInitialStockUploadStoreTransactionDataGeneralModel AS
SELECT ROW_NUMBER()
                                  OVER ()                                                   AS id,
       DObInventoryDocument.creationdate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.code,
       DObInventoryDocument.id AS initialStockUploadId,
       iobinventorycompany.storehouseid,
       iobinventorycompany.plantid,
       iobinventorycompany.companyid,
       dobinventoryDocument.purchaseunitid,
       iobinitialstockuploaditem.originaladdingdate,
       iobinitialstockuploaditem.itemid,
       iobinitialstockuploaditem.actualcost,
       iobinitialstockuploaditem.quantity,
       iobinitialstockuploaditem.remainingquantity,
       iobinitialstockuploaditem.unitofmeasureid,
       iobinitialstockuploaditem.stocktype

FROM dobinventorydocument
         LEFT JOIN iobinventorycompany ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN iobinitialstockuploaditem
                   ON dobinventorydocument.id = iobinitialstockuploaditem.refinstanceid
where dobinventorydocument.inventorydocumenttype = 'ISU';