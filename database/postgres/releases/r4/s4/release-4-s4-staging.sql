\ir 'masterdata-r4-s4/masterdata-r4-s4.sql'
\ir 'order-r4-s4/order-r4-s4.sql'
\ir 'inventory-r4-s4/inventory-r4-s4.sql'
\ir 'accounting-r4-s4/accounting-r4-s4.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;