CREATE OR REPLACE VIEW DObSettlementGeneralModel AS
SELECT settlement.id,
       AccDoc.code,
       AccDoc.creationdate,
       AccDoc.modifieddate,
       AccDoc.creationinfo,
       AccDoc.modificationinfo,
       AccDoc.currentstates,
       cobenterprise.code                                AS purchaseUnitCode,
       cobenterprise.name                                AS purchaseUnitName,
       cobenterprise.name::json ->> 'en'                 AS purchaseUnitNameEn,
       company.code                                      AS companyCode,
       company.name                                      AS companyName,
       company.name::json ->> 'en'                       AS companyNameEn,
       settlement.settlementtype                         AS settlementType,
       cobcurrency.iso                                   AS currencyIso,
       iobaccountingdocumentactivationdetails.accountant AS activatedBy,
       documentownergeneralmodel.username                AS documentOwner

FROM dobsettlement settlement
         LEFT JOIN DObAccountingDocument AccDoc ON settlement.id = AccDoc.id AND AccDoc.objecttypecode = 'Settlement'
         LEFT JOIN IObAccountingDocumentCompanyData IObSettlementCompanyData
                   ON settlement.id = IObSettlementCompanyData.refinstanceid AND
                      IObSettlementCompanyData.objecttypecode = 'Settlement'
         LEFT JOIN cobenterprise ON IObSettlementCompanyData.purchaseUnitId = cobenterprise.id
         LEFT JOIN cobenterprise company ON IObSettlementCompanyData.companyid = company.id
         LEFT JOIN IObSettlementDetails settlemetDetails ON settlement.id = settlemetDetails.refinstanceid
         left join cobcurrency on cobcurrency.id = settlemetDetails.currencyid
         left join iobaccountingdocumentactivationdetails
                   on iobaccountingdocumentactivationdetails.refinstanceid = AccDoc.id
         left join documentownergeneralmodel
                   on settlement.documentOwnerId = documentownergeneralmodel.userId
                       and documentownergeneralmodel.objectName = 'Settlement';


CREATE OR REPLACE VIEW IObSettlementAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code                                                    AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger                                                      AS subLedger,
       (select glaccountcode
        from getGlAccount(accountDetails.glaccountid, iobvendoraccountinginfo.chartofaccountid,
                          accountDetails.subledger,
                          accountDetails.accountingEntry))                           as glAccountCode,
       (select glaccountname
        from getGlAccount(accountDetails.glaccountid, iobvendoraccountinginfo.chartofaccountid,
                          accountDetails.subledger,
                          accountDetails.accountingEntry))                           as glAccountName,
       (select glaccountid
        from getGlAccount(accountDetails.glaccountid, iobvendoraccountinginfo.chartofaccountid,
                          accountDetails.subledger, accountDetails.accountingEntry)) as glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.code
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               doborderdocument.code
               )
           when accountDetails.accountingEntry = 'CREDIT'
               then (CASE
                         WHEN bankSubAccount.accountno IS NULL THEN NULL
                         ELSE concat(bankSubAccount.accountno, ' - ', bankCurrency.iso) END)
           end                                                                       as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendoraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               orderaccountingdetails.glSubAccountId
               )
           when accountDetails.accountingEntry = 'CREDIT'
               then (bankaccountingdetails.glSubAccountId)
           end                                                                       as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.name
               )
           when accountDetails.accountingEntry = 'CREDIT' THEN
               (cobbank.name) end                                                    as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObSettlement settlement on accountDetails.refinstanceid = settlement.id
         left join dobaccountingdocument on dobaccountingdocument.id = settlement.id
         left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails
                   on vendoraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join hobglaccount account on account.id = accountDetails.glaccountid

--     ---------------------------------
         left join masterdata vendor on vendor.id = vendoraccountingdetails.glSubAccountId and
                                        vendor.objecttypecode = '2'
         left join iobvendoraccountinginfo on vendor.id = iobvendoraccountinginfo.refinstanceid
         left join hobglaccount vendorAccount
                   on vendorAccount.id = iobvendoraccountinginfo.chartofaccountid
    ----------------------------------
         left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails
                   on orderaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails
                   on bankaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'

         left join doborderdocument
                   on doborderdocument.id = orderaccountingdetails.glSubAccountId AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         LEFT JOIN cobcurrency bankCurrency on bankSubAccount.currencyId = bankCurrency.id
         left join cobbank on cobbank.id = bankSubAccount.bankid;

CREATE OR REPLACE VIEW DObSettlementActivationPreparation AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       cobfiscalperiod.id             as fiscalPeriodId,
       cobexchangerategeneralmodel.id as exchangeRateId,
       cobexchangerategeneralmodel.secondvalue
from dobsettlement
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobsettlement.id
         left join cobfiscalperiod ON cobfiscalperiod.currentstates like '%Active%'
         left join iobsettlementdetails on iobsettlementdetails.refinstanceid = dobaccountingdocument.id
         left join iobaccountingdocumentcompanydata
                   on iobaccountingdocumentcompanydata.refinstanceid = dobaccountingdocument.id
         left join cobcompanygeneralmodel on iobaccountingdocumentcompanydata.companyid = cobcompanygeneralmodel.id
         left join cobexchangerategeneralmodel
                   on cobexchangerategeneralmodel.secondcurrencyid  = iobsettlementdetails.currencyid and
                      cobexchangerategeneralmodel.firstcurrencyid = cobcompanygeneralmodel.currencyid;