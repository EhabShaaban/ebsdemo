CREATE or REPLACE FUNCTION getSubAccountCodeByJournalItemIdAndType(journalItemId bigint, journalItemType varchar)
    RETURNS varchar AS
$$
BEGIN
RETURN
    (SELECT subaccountgeneralmodel.code
     FROM iobjournalentryitempurchaseordersubaccount
              LEFT JOIN subaccountgeneralmodel ON iobjournalentryitempurchaseordersubaccount.subaccountid =
                                                  subaccountgeneralmodel.subaccountid
         AND journalItemType = subaccountgeneralmodel.ledger
         AND journalItemType = 'PO'
     where iobjournalentryitempurchaseordersubaccount.id = journalItemId
     UNION
     SELECT subaccountgeneralmodel.code
     FROM iobjournalentryitemvendorsubaccount
              LEFT JOIN subaccountgeneralmodel
                        ON iobjournalentryitemvendorsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                            AND iobjournalentryitemvendorsubaccount.type = subaccountgeneralmodel.ledger
                            AND journalItemType = 'Vendors'
     where iobjournalentryitemvendorsubaccount.id = journalItemId
     UNION
     SELECT concat(iobcompanybankdetails.accountno, ' - ', currency.iso)
     FROM iobcompanybankdetails
              LEFT JOIN cobcurrency currency on iobcompanybankdetails.currencyId = currency.id
              RIGHT JOIN iobjournalentryitembanksubaccount
                         ON iobjournalentryitembanksubaccount.subaccountid = iobcompanybankdetails.id
                             AND journalItemType = 'Banks'
     WHERE iobjournalentryitembanksubaccount.id = journalItemId
     UNION
     SELECT cobtreasury.code
     FROM cobtreasury
              RIGHT JOIN iobjournalentryitemtreasurysubaccount
                         ON iobjournalentryitemtreasurysubaccount.subaccountid = cobtreasury.id
                             AND journalItemType = 'Treasuries'
     WHERE iobjournalentryitemtreasurysubaccount.id = journalItemId
     UNION
     SELECT subaccountgeneralmodel.code
     FROM iobjournalentryitemtaxsubaccount
              LEFT JOIN subaccountgeneralmodel
                        ON iobjournalentryitemtaxsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                            AND journalItemType = subaccountgeneralmodel.ledger
                            AND journalItemType = 'Taxes'
     where iobjournalentryitemtaxsubaccount.id = journalItemId
     UNION
     SELECT subaccountgeneralmodel.code
     FROM iobjournalentryitemcustomersubaccount
              LEFT JOIN subaccountgeneralmodel
                        ON iobjournalentryitemcustomersubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                            AND iobjournalentryitemcustomersubaccount.type = subaccountgeneralmodel.ledger
                            AND journalItemType = 'Customers'
     where iobjournalentryitemcustomersubaccount.id = journalItemId
     UNION
     SELECT subaccountgeneralmodel.code
     FROM IObJournalEntryItemNotesReceivableSubAccount
              LEFT JOIN subaccountgeneralmodel
                        ON IObJournalEntryItemNotesReceivableSubAccount.subaccountid =
                           subaccountgeneralmodel.subaccountid
                            AND journalItemType = subaccountgeneralmodel.ledger
                            AND journalItemType = 'NotesReceivable'
     where IObJournalEntryItemNotesReceivableSubAccount.id = journalItemId
     UNION
     SELECT subaccountgeneralmodel.code
     FROM iobjournalentryitempurchaseordersubaccount
              LEFT JOIN subaccountgeneralmodel
                        ON iobjournalentryitempurchaseordersubaccount.subaccountid =
                           subaccountgeneralmodel.subaccountid
                            and subaccountgeneralmodel.ledger = 'PO'
     where iobjournalentryitempurchaseordersubaccount.id = journalItemId);
END;
$$
LANGUAGE PLPGSQL;