DROP VIEW IF EXISTS IObPaymentRequestAccountDetailsGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestPaymentDetailsGeneralModel;

CREATE VIEW IObPaymentRequestAccountDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.code
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               doborderdocument.code
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks'
               then (concat(bankSubAccount.accountno, ' - ', currency.iso))
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (cobtreasury.code)
           end                    as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendoraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               orderaccountingdetails.glSubAccountId
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks'
               then (bankaccountingdetails.glSubAccountId)
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (treasuryaccountingdetails.glSubAccountId)
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.name
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks' THEN
               (cobbank.name)
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (cobtreasury.name)
           end as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObPaymentRequest pr on accountDetails.refinstanceid = pr.id
         left join dobaccountingdocument on dobaccountingdocument.id = pr.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails on vendoraccountingdetails.id = accountDetails.id AND
                                                                                           accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails on orderaccountingdetails.id = accountDetails.id AND
                                                                                         accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails on bankaccountingdetails.id = accountDetails.id AND
                                                                                       accountDetails.accountingEntry = 'CREDIT'
         left join iobaccountingdocumenttreasuryaccountingdetails treasuryaccountingdetails on treasuryaccountingdetails.id = accountDetails.id AND
                                                                                               accountDetails.accountingEntry = 'CREDIT'
         left join masterdata vendor on vendor.id = vendoraccountingdetails.glSubAccountId and vendor.objecttypecode = '2'
         left join doborderdocument on doborderdocument.id = orderaccountingdetails.glSubAccountId
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         LEFT JOIN cobcurrency currency on bankSubAccount.currencyId = currency.id
         left join cobbank on cobbank.id = bankSubAccount.bankid
         left join cobtreasury on cobtreasury.id = treasuryaccountingdetails.glsubaccountid;


CREATE VIEW IObPaymentRequestPaymentDetailsGeneralModel AS
SELECT pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                    as businessPartnerCode,
       md.name                    as businessPartnerName,
       pd.duedocumenttype,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           when duedocumenttype = 'CREDITNOTE' then creditNote.code
           else doborderdocument.code end as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso            as currencyISO,
       cobcurrency.name           as currencyName,
       cobcurrency.code           as currencyCode,
       companyCurrency.iso        AS companyCurrencyIso,
       companyCurrency.code       AS companyCurrencyCode,
       companyCurrency.id         AS companyCurrencyId,
       pd.description,
       pd.paymentForm,
       IObCompanyBankDetails.id                              AS bankAccountId,
       companyBank.code                                      AS bankAccountCode,
       companyBank.name                                      AS bankAccountName,
       cobtreasury.code                                      AS treasuryCode,
       cobtreasury.name                                      AS treasuryName,
       CASE WHEN
           IObCompanyBankDetails.accountno IS NULL THEN NULL
           ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END                       AS bankAccountNumber,
       item.code											 AS costFactorItemCode,
       item.name											 AS costFactorItemName
FROM iobpaymentrequestpaymentdetails pd
    LEFT JOIN IObPaymentRequestInvoicePaymentDetails ipd on ipd.id = pd.id
    LEFT JOIN iobpaymentrequestcreditnotepaymentdetails Cpd on Cpd.id = pd.id
    LEFT JOIN IObPaymentRequestPurchaseOrderPaymentDetails dpd on dpd.id = pd.id
    LEFT JOIN dobvendorinvoice on dobvendorinvoice.id = ipd.duedocumentid
    LEFT JOIN dobaccountingdocument vendorInvoice on vendorInvoice.id = dobvendorinvoice.id
    LEFT JOIN  dobaccountingnote on dobaccountingnote.id = Cpd.duedocumentid
    LEFT JOIN dobaccountingdocument creditNote on creditNote.id = dobaccountingnote.id
    LEFT JOIN doborderdocument on doborderdocument.id = dpd.duedocumentid AND
                                  doborderdocument.objecttypecode LIKE '%_PO'
    LEFT JOIN masterdata md on md.id = pd.businessPartnerId
    LEFT JOIN dobpaymentrequest pr on pr.id = pd.refinstanceid
    LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = pr.id
    LEFT JOIN IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
    LEFT JOIN cobcurrency on pd.currencyid = cobcurrency.id
    LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
    LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
    LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = pd.bankAccountId
    LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
    LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
    LEFT JOIN MObItemGeneralModel item on item.id = pd.costFactorItemId
    LEFT JOIN cobtreasury on cobtreasury.id = pd.treasuryId;