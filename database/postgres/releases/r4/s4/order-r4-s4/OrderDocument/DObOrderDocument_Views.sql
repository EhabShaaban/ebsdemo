DROP VIEW IF EXISTS IObOrderCompanyGeneralModel;

CREATE VIEW IObOrderCompanyGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       iobordercompany.partyrole,
       businessUnit.id                 AS purchaseUnitId,
       businessUnit.name               AS purchaseUnitName,
       businessUnit.code               AS purchaseUnitCode,
       company.id                      AS companyId,
       company.code                    AS companyCode,
       company.name                    AS companyName,
       (SELECT currencyCode From getDObOrderCompanyCurrencyInformation(dobpurchaseorder.id)),
       (SELECT currencyIso From getDObOrderCompanyCurrencyInformation(dobpurchaseorder.id)),
       (SELECT currencyName From getDObOrderCompanyCurrencyInformation(dobpurchaseorder.id)),
       IObCompanyBankDetails.id        AS bankAccountId,
       companyBank.code                AS bankCode,
       companyBank.name                AS bankName,
       CASE WHEN
           IObCompanyBankDetails.accountno IS NULL THEN NULL
           ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END AS bankAccountNumber
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         JOIN iobordercompany ON dobpurchaseorder.id = iobordercompany.refinstanceid
         JOIN cobenterprise businessUnit ON businessUnit.id = iobordercompany.businessunitid
         LEFT JOIN IObCompanyBankDetails
                   ON IObCompanyBankDetails.id = iobordercompany.bankAccountId
         LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN cobenterprise company ON company.id = iobordercompany.companyid;