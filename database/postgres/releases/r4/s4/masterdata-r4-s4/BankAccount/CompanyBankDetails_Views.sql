DROP VIEW IF EXISTS iobcompanybankdetailsgeneralmodel;

CREATE VIEW iobcompanybankdetailsgeneralmodel as
SELECT iobcompanybankdetails.id,
       cobenterprise.code                                                          AS companycode,
       cobbank.code,
       cobbank.name,
       concat(iobcompanybankdetails.accountno, ' - ', currency.iso) AS accountno,
       currency.code AS currencyCode,
       currency.iso AS currencyISO,
       concat(cobbank.name ->> 'en'::text, ' - ', iobcompanybankdetails.accountno, ' - ', currency.iso) AS bankaccountnumber
FROM iobcompanybankdetails
    JOIN cobcompany ON iobcompanybankdetails.refinstanceid = cobcompany.id
    JOIN cobbank ON iobcompanybankdetails.bankid = cobbank.id
    JOIN cobenterprise ON cobcompany.id = cobenterprise.id
    LEFT JOIN cobcurrency currency on iobcompanybankdetails.currencyId = currency.id;