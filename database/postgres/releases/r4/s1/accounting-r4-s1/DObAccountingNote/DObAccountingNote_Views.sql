CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode                              AS documentReferenceType,
       DObJournalEntry.dueDate,
       company.code                                                AS companyCode,
       company.name                                                AS companyName,
       purchaseUnit.code                                           AS purchaseUnitCode,
       purchaseUnit.name                                           AS purchaseUnitName,
       purchaseUnit.name:: json ->> 'en'                           AS purchaseUnitNameEn,
       (SELECT dobaccountingdocument.code
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObVendorInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'VendorInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObPaymentRequestJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSalesInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcollectionJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON dobcollectionJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcollectionJournalEntry.id
          AND dobaccountingdocument.objecttypecode in ('C_SI', 'C_NR', 'C_SO')
        UNION
        SELECT dobnotesreceivables.code
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobnotesreceivables
                           ON dobNotesReceivableJournalEntry.documentreferenceid =
                              dobnotesreceivables.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM doblandedcostjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON doblandedcostjournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = doblandedcostjournalentry.id
          and dobaccountingdocument.objecttypecode = 'LandedCost') AS documentReferenceCode
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company
                   ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id;

DROP VIEW IF EXISTS DObAccountingNoteGeneralModel;
CREATE VIEW DObAccountingNoteGeneralModel AS
SELECT DObAccountingNote.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       DObAccountingNote.type,
       IObAccountingNoteDetails.amount,
       DObAccountingNote.remaining,
       dobaccountingdocument.objecttypecode,
       CASE
           WHEN businessPartner.objecttypecode = '2'
               THEN cast('{"ar": "مورد", "en": "Vendor"}' AS JSON)
           ELSE CASE
                    WHEN businessPartner.objecttypecode = '3'
                        THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON) END
           END                                                                            AS businessPartnerType,
       businessPartner.name                                                               AS businessPartnerName,
       businessPartner.code                                                               AS businessPartnerCode,
       CASE
           WHEN dobaccountingdocument.objecttypecode = 'CREDIT_NOTE_BASED_ON_SALES_RETURN'
               THEN cast(
                   '{"ar": "طلب ارتجاع مبيعات", "en": "Sales Return Order"}' AS JSON) END AS referenceDocumentType,
       CASE
           WHEN dobaccountingdocument.objecttypecode = 'CREDIT_NOTE_BASED_ON_SALES_RETURN'
               THEN (SELECT code
                     FROM dobsalesreturnorder
                     WHERE id = refdocumentid) END                                        AS referenceDocumentCode,
       purchaseUnit.name                                                                  AS purchaseUnitName,
       purchaseUnit.name::json ->> 'en'                                                   AS purchaseUnitNameEn,
       purchaseUnit.code                                                                  AS purchaseUnitCode,
       cobcurrency.iso                                                                    AS currencyISO,
       cobcurrency.code                                                                   AS currencyCode,
       documentownergeneralmodel.username                                                 AS documentOwnerName,
       company.code                                                                       AS companycode,
       company.name                                                                       AS companyname,
       dobaccountingdocument.currentStates,
       IObAccountingDocumentActivationDetails.activationDate
FROM DObAccountingNote
         LEFT JOIN dobaccountingdocument ON DObAccountingNote.id = dobaccountingdocument.id
         LEFT JOIN IObAccountingNoteDetails
                   ON DObAccountingNote.id = IObAccountingNoteDetails.refInstanceId
         LEFT JOIN IObAccountingDocumentActivationDetails
                   ON IObAccountingDocumentActivationDetails.refInstanceId = DObAccountingNote.id
         LEFT JOIN masterdata businessPartner
                   ON businessPartner.id = IObAccountingNoteDetails.businessPartnerId
         LEFT JOIN IObAccountingDocumentCompanyData
                   ON IObAccountingDocumentCompanyData.refInstanceId = DObAccountingDocument.id
         LEFT JOIN cobenterprise purchaseUnit
                   ON IObAccountingDocumentCompanyData.purchaseUnitId = purchaseUnit.id AND
                      purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobcurrency ON IObAccountingNoteDetails.currencyid = cobcurrency.id
         LEFT JOIN documentownergeneralmodel
                   ON DObAccountingNote.documentownerid = documentownergeneralmodel.userid AND
                      documentownergeneralmodel.objectname = 'AccountingNote'
         LEFT JOIN cobenterprise company
                   ON iobaccountingdocumentcompanydata.companyid = company.id
         LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id;

DROP VIEW IF EXISTS IObAccountingNoteDetailsGeneralModel;
CREATE VIEW IObAccountingNoteDetailsGeneralModel AS
SELECT IObAccountingNoteDetails.id,
       dobaccountingdocument.code,
       masterdata.name  AS businessPartnername,
       masterdata.code  AS businessPartnercode,
       CASE
           WHEN masterdata.objecttypecode = '3'
               THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
           WHEN masterdata.objecttypecode = '2'
               THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
           END          AS businessPartnerType,
       IObAccountingNoteDetails.amount,
       cobcurrency.iso  AS currencyISO,
       cobcurrency.code AS currencyCode
FROM IObAccountingNoteDetails
         LEFT JOIN DObAccountingNote
                   ON IObAccountingNoteDetails.refInstanceId = DObAccountingNote.id
         LEFT JOIN dobaccountingdocument ON DObAccountingNote.id = dobaccountingdocument.id
         LEFT JOIN mobbusinesspartner
                   ON IObAccountingNoteDetails.businessPartnerId = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinessPartner.id = masterdata.id
         LEFT JOIN cobcurrency ON IObAccountingNoteDetails.currencyid = cobcurrency.id;


