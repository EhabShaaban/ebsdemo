DROP VIEW DObJournalEntryGeneralModel;
DROP VIEW DObCreditDebitNoteGeneralModel;
ALTER TABLE DObCreditNoteJournalEntry
    DROP
        CONSTRAINT fk_dobcreditnotejournalentry_dobcreditnote;
ALTER TABLE dobdebitnotejournalentry
    DROP
        CONSTRAINT fk_dobdebitnotejournalentry_dobdebitnote;

DROP TABLE DObCreditDebitNote;

CREATE TABLE DObAccountingNote
(
    id              BIGSERIAL     NOT NULL REFERENCES dobaccountingdocument (id) on delete cascade,
    documentOwnerId INT8          NOT NULL REFERENCES ebsuser (id),
    remaining       DOUBLE PRECISION,
    type            VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObAccountingNoteDetails
(
    id                BIGSERIAL                NOT NULL,
    refInstanceId     INT8                     NOT NULL REFERENCES DObAccountingNote (id) on delete cascade,
    creationDate      TIMESTAMP With time zone NOT NULL,
    modifiedDate      TIMESTAMP With time zone NOT NULL,
    creationInfo      VARCHAR(1024)            NOT NULL,
    modificationInfo  VARCHAR(1024)            NOT NULL,
    businessPartnerId INT8                     NOT NULL REFERENCES mobbusinesspartner (id),
    amount            DOUBLE PRECISION,
    currencyId        INT8                     NOT NULL REFERENCES cobcurrency (id),
    refDocumentId     INT8,
    PRIMARY KEY (id)
);

ALTER TABLE DObCreditNoteJournalEntry
    ADD CONSTRAINT FK_DObCreditNoteJournalEntry_DObAccountingNote FOREIGN KEY (documentReferenceId)
        REFERENCES DObAccountingNote (id) ON DELETE RESTRICT;

ALTER TABLE DObDebitNoteJournalEntry
    ADD CONSTRAINT FK_DObDebitNoteJournalEntry_DObAccountingNote FOREIGN KEY (documentReferenceId)
        REFERENCES DObAccountingNote (id) ON DELETE RESTRICT;


