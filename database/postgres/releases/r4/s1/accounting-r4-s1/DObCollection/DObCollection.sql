drop table if exists IObCollectionDebitNoteDetails;
ALTER TABLE IObCollectionDetails
    DROP CONSTRAINT fk_iobcollectionrequestdetails_customer;
---------------------------------------------------------------------------------------------
create table IObCollectionDebitNoteDetails
(
    id                BIGSERIAL NOT NULL,
    accountingNoteId  INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObCollectionDebitNoteDetails
    ADD CONSTRAINT FK_IObCollectionDebitNoteDetails_debitNoteId FOREIGN KEY (accountingNoteId) REFERENCES DObAccountingNote (id) ON DELETE restrict;

ALTER TABLE IObCollectionDebitNoteDetails
    ADD CONSTRAINT FK_IObCollectionDebitNoteDetails_id FOREIGN KEY (id) REFERENCES IObCollectionDetails (id) ON DELETE cascade;


ALTER TABLE IObCollectionDetails
    ADD CONSTRAINT FK_IObCollectionDetails_businessPartner_id FOREIGN KEY (businessPartnerId) REFERENCES mobbusinesspartner (id) ON DELETE restrict;
