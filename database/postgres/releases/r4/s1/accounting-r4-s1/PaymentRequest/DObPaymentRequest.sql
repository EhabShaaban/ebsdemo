create table IObPaymentRequestCreditNotePaymentDetails
(
    id            BIGSERIAL NOT NULL,
    dueDocumentId INT8,
    PRIMARY KEY (id)
);

ALTER TABLE IObPaymentRequestCreditNotePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestcreditnotepaymentdetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE restrict;

ALTER TABLE IObPaymentRequestCreditNotePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestcreditnotepaymentdetails_duedocument FOREIGN KEY (dueDocumentId) REFERENCES DObAccountingNote (id) ON DELETE restrict;
