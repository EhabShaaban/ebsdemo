DROP VIEW IF EXISTS DObPaymentRequestGeneralModel;

CREATE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       paymentdetails.paymentForm,
       paymentdetails.netAmount           AS amountValue,
       cobcurrency.iso                    AS amountCurrency,
       companyData.purchaseUnitId,
       purchaseunit.name                  AS purchaseUnitName,
       purchaseunit.name:: json ->> 'en'  AS purchaseUnitNameEn,
       purchaseunit.code                  AS purchaseUnitCode,
       businesspartner.code               AS businessPartnerCode,
       businesspartner.name               AS businessPartnerName,
       duedocumenttype                    AS dueDocumentType,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           else doborderdocument.code end as referenceDocumentCode,
       case
           when duedocumenttype = 'INVOICE' then '{
             "en": "Vendor Invoice",
             "ar": "فاتورة مورد"
           }'::json
           when duedocumenttype = 'PURCHASEORDER' then '{
             "en": "Purchase Order",
             "ar": "أمر شراء"
           }'::json end                   as referenceDocumentType

FROM DObPaymentRequest
         left join dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN cobenterprise purchaseunit ON purchaseunit.id = companyData.purchaseunitid
         LEFT JOIN iobpaymentrequestpaymentdetails paymentdetails on dobpaymentrequest.id = paymentdetails.refinstanceid
         LEFT JOIN mobbusinesspartner on mobbusinesspartner.id = paymentdetails.businesspartnerid
         LEFT JOIN masterdata businesspartner on mobbusinesspartner.id = businesspartner.id
         left join IObPaymentRequestInvoicePaymentDetails invoicepaymentdetails
                   on invoicepaymentdetails.id = paymentdetails.id
         left join iobpaymentrequestcreditnotepaymentdetails creditNotepaymentdetails
                   on creditNotepaymentdetails.id = paymentdetails.id
         left join IObPaymentRequestPurchaseOrderPaymentDetails orderpaymentdetails
                   on orderpaymentdetails.id = paymentdetails.id
         left join dobaccountingdocument vendorInvoice on vendorInvoice.id = invoicepaymentdetails.duedocumentid
         left join dobaccountingdocument creditNote on creditNote.id = creditNotepaymentdetails.duedocumentid
         left join doborderdocument on doborderdocument.id = orderpaymentdetails.duedocumentid
         left join cobcurrency on paymentdetails.currencyid = cobcurrency.id;

