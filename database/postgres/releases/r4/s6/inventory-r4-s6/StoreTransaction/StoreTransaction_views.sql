CREATE OR REPLACE VIEW TObInitialStockUploadStoreTransactionDataGeneralModel AS
SELECT ROW_NUMBER()
                                  OVER ()                                                   AS id,
       DObInventoryDocument.creationdate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.code,
       DObInventoryDocument.id AS initialStockUploadId,
       iobinventorycompany.storehouseid,
       iobinventorycompany.plantid,
       iobinventorycompany.companyid,
       dobinventoryDocument.purchaseunitid,
       iobinitialstockuploaditem.originaladdingdate,
       iobinitialstockuploaditem.itemid,
       iobinitialstockuploaditem.actualcost,
       iobinitialstockuploaditem.quantity,
       iobinitialstockuploaditem.remainingquantity,
       iobinitialstockuploaditem.unitofmeasureid,
       iobinitialstockuploaditem.stocktype,
       iobinitialstockuploaditem.estimatecost

FROM dobinventorydocument
         LEFT JOIN iobinventorycompany ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN iobinitialstockuploaditem
                   ON dobinventorydocument.id = iobinitialstockuploaditem.refinstanceid
where dobinventorydocument.inventorydocumenttype = 'ISU';