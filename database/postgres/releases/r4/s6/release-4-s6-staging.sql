\ir 'masterdata-r4-s6/masterdata-r4-s6.sql'
\ir 'order-r4-s6/orderdocument-r4-s6.sql'
\ir 'accounting-r4-s6/accounting-r4-s6.sql'
\ir 'inventory-r4-s6/inventory-r4-s6.sql'
\ir 'coded-business-objects-r4-s6/coded-business-objects-r4-s6.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;