CREATE TABLE CObJournalBalance
(
    id               BIGSERIAL                                               NOT NULL,
    name             JSON                                                    NOT NULL,
    code             VARCHAR(1024)                                           NOT NULL,
    creationDate     TIMESTAMP                                               NOT NULL,
    modifiedDate     TIMESTAMP                                               NOT NULL,
    creationInfo     VARCHAR(1024)                                           NOT NULL,
    modificationInfo VARCHAR(1024)                                           NOT NULL,
    objectTypeCode   VARCHAR(1024)                                           NOT NULL,
    companyId        INT8 REFERENCES CObCompany (id) ON DELETE RESTRICT      NOT NULL,
    currencyId       INT8 REFERENCES CObCurrency (id) ON DELETE RESTRICT     NOT NULL,
    businessUnitId   INT8 REFERENCES CObEnterprise (id) ON DELETE RESTRICT   NOT NULL,
    fiscalYearId     INT8 REFERENCES CObFiscalPeriod (id) ON DELETE RESTRICT NOT NULL,
    balance          DOUBLE PRECISION                                        NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE CObBankJournalBalance
(
    id        INT8 REFERENCES CObJournalBalance ON DELETE CASCADE                  NOT NULL,
    accountId INT8 REFERENCES IObCompanyBankDetails (id) ON DELETE RESTRICT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE CObTreasuryJournalBalance
(
    id         INT8 REFERENCES CObJournalBalance ON DELETE CASCADE        NOT NULL,
    treasuryId INT8 REFERENCES cobtreasury (id) ON DELETE RESTRICT NOT NULL,
    PRIMARY KEY (id)
);
