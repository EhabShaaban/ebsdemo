DELETE FROM cobassettype;

INSERT INTO public.cobassettype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (1, 'BUILDINGS', '2020-06-15 13:47:49.000000', '2020-06-15 13:47:51.000000', 'Admin', 'Admin', '["Active"]', '{"en": "Buildings", "ar": "المباني"}');

INSERT INTO public.cobassettype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (2, 'LANDS', '2020-06-15 13:47:49.000000', '2020-06-15 13:47:51.000000', 'Admin', 'Admin', '["Active"]', '{"en": "Lands", "ar": "الأراضي"}');

INSERT INTO public.cobassettype (id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name)
VALUES (3, 'FURNITURE', '2020-06-15 13:47:49.000000', '2020-06-15 13:47:51.000000', 'Admin', 'Admin', '["Active"]', '{"en": "Furniture", "ar": "الأثاث"}');
