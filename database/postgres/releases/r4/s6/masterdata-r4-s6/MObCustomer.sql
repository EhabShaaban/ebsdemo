ALTER TABLE MObCustomer ADD COLUMN oldCustomerNumber VARCHAR(1024) ;


UPDATE MObCustomer SET oldCustomerNumber = (select code from masterdata
                                            where MObCustomer.id = masterdata.id);

ALTER TABLE MObCustomer ALTER COLUMN oldCustomerNumber SET NOT NULL ;
ALTER TABLE MObCustomer ADD UNIQUE (oldCustomerNumber);

CREATE OR REPLACE VIEW MObCustomerGeneralModel AS
SELECT MObCustomer.id,
       MasterData.creationDate,
       MasterData.modifiedDate,
       MasterData.creationInfo,
       MasterData.modificationInfo,
       MasterData.currentStates,
       MasterData.code,
       MObCustomer.marketname,
       MasterData.name    AS legalName,
       MObCustomer.type,
       cobenterprise.code AS kapCode,
       cobenterprise.name AS kapName,
       (SELECT string_agg(cobenterprise.code, ', ')
        FROM cobenterprise
        WHERE cobenterprise.id IN (SELECT iobcustomerbusinessunit.businessunitid
                                   FROM iobcustomerbusinessunit
                                   WHERE iobcustomerbusinessunit.refinstanceid = mobcustomer.id)
          AND cobenterprise.objecttypecode = '5'
       )                  AS businessUnitCodes,
       (SELECT array_to_json(array_agg(cobenterprise.name))
        FROM cobenterprise
        WHERE cobenterprise.id IN (SELECT iobcustomerbusinessunit.businessunitid
                                   FROM iobcustomerbusinessunit
                                   WHERE iobcustomerbusinessunit.refinstanceid = mobcustomer.id)
          AND cobenterprise.objecttypecode = '5'
       )                  AS businessUnitNames,
       IObCustomerLegalRegistrationData.registrationNumber,
       LObIssueFrom.code as issueFromCode,
       LObIssueFrom.name as issueFromName,
       IObCustomerLegalRegistrationData.taxCardNumber,
       LObTaxAdministrative.code as taxAdministrativeCode,
       LObTaxAdministrative.name as taxAdministrativeName,
       IObCustomerLegalRegistrationData.taxFileNumber,
       MObCustomer.oldCustomerNumber
FROM MObCustomer
         LEFT JOIN MasterData ON MObCustomer.id = MasterData.id AND MasterData.objecttypecode = '3'
         LEFT JOIN cobenterprise ON MObCustomer.kapId = cobenterprise.id
         LEFT JOIN IObCustomerLegalRegistrationData ON MObCustomer.id = IObCustomerLegalRegistrationData.refInstanceId
         LEFT JOIN LObIssueFrom ON IObCustomerLegalRegistrationData.issueFromId = LObIssueFrom.id
         LEFT JOIN LObTaxAdministrative ON IObCustomerLegalRegistrationData.taxAdministrativeId = LObTaxAdministrative.id;