CREATE TABLE CObAssetType
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024),
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

create TABLE MObAsset
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    assetTypeId      INT8  NOT NULL REFERENCES CObAssetType (id) ON DELETE RESTRICT,
    currentStates    VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    OldNumber        VARCHAR(1024) NOT NULL UNIQUE,
    PRIMARY KEY (id)
);

create TABLE IObAssetBusinessUnit
(
    id               BIGSERIAL NOT NULL,
    refInstanceId        INT8  NOT NULL REFERENCES MObAsset (id) ON DELETE CASCADE,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    businessUnitId   INT8  NOT NULL  REFERENCES cobenterprise (id) ON delete RESTRICT,
    primary key (id)
);


CREATE VIEW MObAssetGeneralModel AS
SELECT MObAsset.id,
       MObAsset.creationDate,
       MObAsset.modifiedDate,
       MObAsset.creationInfo,
       MObAsset.modificationInfo,
       MObAsset.currentStates,
       MObAsset.code,
       MObAsset.name    AS assetName,
       MObAsset.OldNumber,
       CObAssetType.code As assetTypeCode,
       CObAssetType.name As assetTypeName,
       (SELECT string_agg(cobenterprise.code, ', ')
        FROM cobenterprise
        WHERE cobenterprise.id IN (SELECT IObAssetBusinessUnit.businessUnitId
                                   FROM IObAssetBusinessUnit
                                   WHERE IObAssetBusinessUnit.refinstanceid = MObAsset.id)
          AND cobenterprise.objecttypecode = '5'
       )                  AS businessUnitCodes,
       (SELECT array_to_json(array_agg(cobenterprise.name))
        FROM cobenterprise
        WHERE cobenterprise.id IN (SELECT IObAssetBusinessUnit.businessUnitId
                                   FROM IObAssetBusinessUnit
                                   WHERE IObAssetBusinessUnit.refinstanceid = MObAsset.id)
          AND cobenterprise.objecttypecode = '5'
       )                  AS businessUnitNames
FROM MObAsset
         LEFT JOIN CObAssetType ON MObAsset.assetTypeId = CObAssetType.id;
