UPDATE mobvendor SET oldvendornumber = (select code from masterdata
                                            where mobvendor.id = masterdata.id);

ALTER TABLE MObVendor ALTER COLUMN oldvendornumber SET NOT NULL ;
ALTER TABLE MObVendor ADD UNIQUE (oldvendornumber);

