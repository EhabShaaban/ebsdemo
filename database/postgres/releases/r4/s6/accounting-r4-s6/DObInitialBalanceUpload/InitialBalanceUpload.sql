CREATE TABLE IObInitialBalanceUploadItem
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL REFERENCES DObAccountingDocument (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    glAccountId      INT8 REFERENCES hobglaccount (id) ON DELETE RESTRICT,
    credit           double precision DEFAULT 0,
    debit            double precision DEFAULT 0,
    currencyId       INT8,
    currencyPrice    double precision,
    subLedger        VARCHAR(100)  NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObInitialBalanceUploadDocumentOrderItem
(
    id BIGSERIAL NOT NULL REFERENCES IObInitialBalanceUploadItem (id) ON DELETE cascade,
    PRIMARY KEY (id)
);


CREATE TABLE IObInitialBalanceUploadVendorItem
(
    id       BIGSERIAL NOT NULL REFERENCES IObInitialBalanceUploadItem (id) ON DELETE cascade,
    vendorId INT8 REFERENCES mobvendor (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);


DROP TABLE IF EXISTS DObInitialBalanceUploadJournalEntry;
CREATE TABLE DObInitialBalanceUploadJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);