DROP VIEW if exists IObInitialBalanceUploadItemGeneralModel;

CREATE OR REPLACE VIEW IObInitialBalanceUploadItemGeneralModel AS
SELECT IObInitialBalanceUploadItem.id,
       IObInitialBalanceUploadItem.creationDate,
       IObInitialBalanceUploadItem.modifiedDate,
       IObInitialBalanceUploadItem.creationInfo,
       IObInitialBalanceUploadItem.modificationInfo,
       DObAccountingDocument.code                AS initialBalanceUploadCode,
       DObAccountingDocument.currentStates,
       IObInitialBalanceUploadItem.subledger     AS subledger,
       IObInitialBalanceUploadItem.debit         AS debit,
       IObInitialBalanceUploadItem.credit        AS credit,
       cobcurrency.iso                           AS currencyISO,
       IObInitialBalanceUploadItem.currencyprice AS currencyprice,

       account.code                              AS glAccountCode,
       account.name                              AS glAccountName,

       case
           when IObInitialBalanceUploadItem.subledger = 'Local_Vendors' then (vendor.code)
           end                                   as glSubAccountCode,
       case
           when IObInitialBalanceUploadItem.subledger = 'Local_Vendors' then (vendor.name)
           end                                   as glSubAccountName


FROM IObInitialBalanceUploadItem
         JOIN DObAccountingDocument ON DObAccountingDocument.id = IObInitialBalanceUploadItem.refInstanceId
         JOIN IObAccountingDocumentCompanyData
              ON DObAccountingDocument.id = IObAccountingDocumentCompanyData.refinstanceid
         JOIN cobcurrency ON cobcurrency.id = IObInitialBalanceUploadItem.currencyId

         LEFT JOIN iobinitialbalanceuploadvendoritem
                   on iobinitialbalanceuploaditem.id = iobinitialbalanceuploadvendoritem.id
         LEFT JOIN masterdata vendor
                   ON vendor.id = iobinitialbalanceuploadvendoritem.vendorId AND vendor.objecttypecode = '2'

         left join hobglaccount account on account.id = IObInitialBalanceUploadItem.glaccountid

WHERE DObAccountingDocument.objecttypecode = 'IBU';