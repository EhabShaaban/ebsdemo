\ir 'HObChartOfAccounts/HObChartOfAccounts_Views.sql';
\ir 'DObVendorInvoice/DObVendorInvoice.sql';
\ir 'DObSalesInvoice/DObSalesInvoice.sql'
\ir 'DObSalesInvoice/DObSalesInvoice_Views.sql';
\ir 'Costing/Costing_Views.sql';
\ir 'LandedCost/DObLandedCost.sql';
\ir 'LandedCost/DObLandedCost_Views.sql';
\ir 'DObInitialBalanceUpload/InitialBalanceUpload.sql'
\ir 'DObInitialBalanceUpload/InitialBalanceUpload_Views.sql'
\ir 'DObVendorInvoice/DObVendorInvoice-view.sql';
\ir 'DObJournalEntry/DObJournalEntry_Views.sql';



GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;