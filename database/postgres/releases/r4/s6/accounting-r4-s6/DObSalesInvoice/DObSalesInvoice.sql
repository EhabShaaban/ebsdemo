create table IObSalesInvoiceSummary
(
    id                  BIGSERIAL     NOT NULL,
    refInstanceId       INT8          NOT NULL,
    creationDate        TIMESTAMP     NOT NULL,
    modifiedDate        TIMESTAMP     NOT NULL,
    creationInfo        VARCHAR(1024) NOT NULL,
    modificationInfo    VARCHAR(1024) NOT NULL,
    remaining           FLOAT,
    PRIMARY KEY (id)
);