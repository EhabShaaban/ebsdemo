CREATE OR REPLACE FUNCTION getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(firstCurrency bigint, secondCurrency bigint)
    RETURNS table
            (
                currencyPrice    float,
                exchangerateId   bigint,
                firstcurrencyid  bigint,
                secondcurrencyid bigint
            )
AS
$$
BEGIN
    RETURN QUERY (
        (select firstvalue * secondvalue,
                cobexchangerate.id,
                cobexchangerate.firstcurrencyid,
                cobexchangerate.secondcurrencyid
         from cobexchangerate left join cobexchangeratetype type on cobexchangerate.typeid = type.id
         where cobexchangerate.firstcurrencyid = firstCurrency
           and cobexchangerate.secondcurrencyid = secondCurrency
        and type.isdefault = true
         ORDER BY validfrom DESC
         LIMIT 1));

END;
$$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW DObSalesInvoicePostPreparationGeneralModel AS
select
    dobaccountingdocument.id,
    dobaccountingdocument.code,
    latestexchangerate.exchangerateid as exchangerateid,
    latestexchangerate.currencyprice as currencyprice
from dobaccountingdocument inner join iobsalesinvoicebusinesspartner
                                      on dobaccountingdocument.id = iobsalesinvoicebusinesspartner.refinstanceid
                           inner join iobaccountingdocumentcompanydata on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
    AND iobaccountingdocumentcompanydata.objecttypecode = 'SalesInvoice'
                           inner join iobenterprisebasicdata companydata on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
                           inner join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(iobsalesinvoicebusinesspartner.currencyid, companydata.currencyid) latestexchangerate
                                      on latestexchangerate.firstcurrencyid = iobsalesinvoicebusinesspartner.currencyid
                                          and latestexchangerate.secondcurrencyid = companydata.currencyid
                                          and
                                         dobaccountingdocument.objecttypecode = 'SalesInvoice';

CREATE OR REPLACE VIEW IObInvoiceSummaryGeneralModel AS
SELECT dobaccountingdocument.id                              as id,
       code                                                  as invoiceCode,
       objectTypeCode                                        as invoiceType,
       getInvoiceTotalAmountBeforTaxes(code, objecttypecode) as totalAmountBeforeTaxes,
       getInvoiceTotalTaxAmount(code, objecttypecode)        as taxAmount,
       getInvoiceNetAmount(code, objecttypecode)             as netAmount,
       (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                getDownPaymentForInvoice(code, objecttypecode)
            WHEN objecttypecode = 'VendorInvoice' THEN
                iobvendorinvoicesummary.downpayment END)     as downpayment,
       (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                SiSummary.remaining
            WHEN objecttypecode = 'VendorInvoice' THEN
                iobvendorinvoicesummary.remaining END)     as totalRemaining
FROM dobaccountingdocument
         LEFT JOIN IObSalesInvoiceSummary SiSummary
                   ON SiSummary.refInstanceId = dobaccountingdocument.id AND
                      dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join iobvendorinvoicesummary on IObVendorInvoiceSummary.refinstanceid = dobaccountingdocument.id
WHERE objecttypecode in ('SalesInvoice', 'VendorInvoice');