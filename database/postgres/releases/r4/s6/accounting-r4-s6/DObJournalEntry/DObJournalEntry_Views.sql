DROP VIEW IF EXISTS DObJournalEntryGeneralModel;
DROP VIEW IF EXISTS IObJournalEntryItemGeneralModel;

CREATE or REPLACE FUNCTION getJournalEntryCodeDependsOnObjectTypeCode(objectTypeCode varchar, accouningDocumentId bigint)
    RETURNS varchar AS
$$
BEGIN
RETURN case
           when objecttypecode = 'PaymentRequest' then
               (select journalentry.code
                from dobjournalentry journalentry
                where journalentry.id = (select id
                                         from dobpaymentrequestjournalentry
                                         where documentreferenceid = accouningDocumentId))
           when objecttypecode in ('Collection') then
               (select journalentry.code
                from dobjournalentry journalentry
                where journalentry.id = (select id
                                         from dobcollectionjournalentry
                                         where documentreferenceid = accouningDocumentId))
           when objecttypecode = 'SalesInvoice' then
               (select journalentry.code
                from dobjournalentry journalentry
                where journalentry.id =
                      (select id from dobsalesinvoicejournalentry where documentreferenceid = accouningDocumentId))
           when objecttypecode = 'VendorInvoice' then
               (select journalentry.code
                from dobjournalentry journalentry
                where journalentry.id =
                      (select id from dobvendorinvoicejournalentry where documentreferenceid = accouningDocumentId))
           when objecttypecode = 'LandedCost' then
               (select journalentry.code
                from dobjournalentry journalentry
                where journalentry.id =
                      (select id from doblandedcostjournalentry where documentreferenceid = accouningDocumentId))
           when objecttypecode = 'Settlement' then
               (select journalentry.code
                from dobjournalentry journalentry
                where journalentry.id =
                      (select id from dobsettlementjournalentry where documentreferenceid = accouningDocumentId))
           when objecttypecode = 'InitialBalanceUpload' then
               (select journalentry.code
                from dobjournalentry journalentry
                where journalentry.id =
                      (select id from dobsettlementjournalentry where documentreferenceid = accouningDocumentId))
    end;
END;
$$
LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode                              AS documentReferenceType,
       DObJournalEntry.dueDate,
       company.code                                                AS companyCode,
       company.name                                                AS companyName,
       purchaseUnit.code                                           AS purchaseUnitCode,
       purchaseUnit.name                                           AS purchaseUnitName,
       purchaseUnit.name:: json ->> 'en'                           AS purchaseUnitNameEn,
       (SELECT dobaccountingdocument.code
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObVendorInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'VendorInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObPaymentRequestJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSalesInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcollectionJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON dobcollectionJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcollectionJournalEntry.id
          AND dobaccountingdocument.objecttypecode in ('C_SI', 'C_NR', 'C_SO','C_DN')
        UNION
        SELECT dobnotesreceivables.code
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobnotesreceivables
                           ON dobNotesReceivableJournalEntry.documentreferenceid =
                              dobnotesreceivables.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM doblandedcostjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON doblandedcostjournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = doblandedcostjournalentry.id
          and dobaccountingdocument.objecttypecode = 'LandedCost'
           UNION
        SELECT dobaccountingdocument.code
        FROM DObSettlementJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSettlementJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSettlementJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'Settlement'
           UNION
        SELECT dobaccountingdocument.code
        FROM DObInitialBalanceUploadJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObInitialBalanceUploadJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObInitialBalanceUploadJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'IBU'
          ) AS documentReferenceCode,

       fiscalPeriod.name::json ->> 'en'                            AS FiscalPeriod
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company
                   ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join CObFiscalPeriod fiscalPeriod
                   on DObJournalEntry.fiscalPeriodId = fiscalPeriod.id;


CREATE OR REPLACE VIEW IObJournalEntryItemGeneralModel AS
SELECT iobjournalentryitem.id,
       iobjournalentryitem.creationdate,
       iobjournalentryitem.creationinfo,
       iobjournalentryitem.modifieddate,
       iobjournalentryitem.modificationinfo,
       iobjournalentryitem.refInstanceId,
       dobjournalentry.code,
       dobjournalentry.duedate,
       dobjournalentry.objecttypecode                                                            as referenceType,
       getSubLedgerByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type)      as subLedger,
       (SELECT hobglaccountgeneralmodel.code
        FROM hobglaccountgeneralmodel
        WHERE hobglaccountgeneralmodel.id = iobjournalentryitem.accountid
       )                                                                                         AS AccountCode,
       (SELECT hobglaccountgeneralmodel.name
        FROM hobglaccountgeneralmodel
        WHERE hobglaccountgeneralmodel.id = iobjournalentryitem.accountid)                       AS AccountName,
       getSubAccountCodeByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type) AS subAccountCode,
       getSubAccountNameByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type) AS subAccountName,
       iobjournalentryitem.debit,
       iobjournalentryitem.credit,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE iobjournalentryitem.currency = cobcurrency.id)                                     AS currencyCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE iobjournalentryitem.currency = cobcurrency.id)                                     AS currencyName,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE iobjournalentryitem.companyCurrency = cobcurrency.id)                              AS companycurrencyCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE iobjournalentryitem.companyCurrency = cobcurrency.id)                              AS companycurrencyName,
       iobjournalentryitem.currencyprice,
       purchaseUnit.code                                                                         AS purchaseUnitCode,
       purchaseUnit.name                                                                         AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                                        AS purchaseUnitNameEn,
       iobjournalentryitem.exchangeRateId                                                        as exhangeRateId,
       cobexchangerate.code                                                                      as exchangeratecode
FROM iobjournalentryitem
         LEFT JOIN dobjournalentry
                   ON iobjournalentryitem.refinstanceid = dobjournalentry.id
                       AND (dobjournalentry.objecttypecode = 'VendorInvoice'
                           OR dobjournalentry.objecttypecode = 'PaymentRequest'
                           OR dobjournalentry.objecttypecode = 'SalesInvoice'
                           OR dobjournalentry.objecttypecode = 'Collection'
                           OR dobjournalentry.objecttypecode = 'NotesReceivable'
                           OR dobjournalentry.objecttypecode = 'LandedCost'
                           OR dobjournalentry.objecttypecode = 'Settlement'
                           OR dobjournalentry.objecttypecode = 'InitialBalanceUpload'
                           )
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join cobexchangerate on cobexchangerate.id = iobjournalentryitem.exchangerateid;



