CREATE Or REPLACE VIEW HObGLAccountGeneralModel AS
SELECT glAccount.id,
       glAccount.code,
       glAccount.name,
       glAccount.creationdate,
       glAccount.modifieddate,
       glAccount.creationinfo,
       glAccount.modificationinfo,
       glAccount.currentstates,
       glAccount.level,
       glAccount.parentid,
       leaf.type                                                                 AS type,
       leaf.creditdebit                                                          AS creditDebit,
       leaf.leadger                                                              AS leadger,
       parentAccount.code                                                        AS parentCode,
       parentAccount.name                                                        AS parentName,
       concat_ws(' - ', parentAccount.code, parentAccount.name :: json ->> 'en') AS parentCodeName,
       concat_ws(' ', 'Level', glAccount.level)                                  AS enLevel,
       concat_ws(' ', 'مستوي', glAccount.level)                                  AS arLevel,
       leaf.mappedaccount,
       glAccount.oldAccountCode
FROM HObGLAccount glAccount
         left join LeafGLAccount leaf on glAccount.id = leaf.id
         left join HObGLAccount parentAccount on glAccount.parentid = parentAccount.id;