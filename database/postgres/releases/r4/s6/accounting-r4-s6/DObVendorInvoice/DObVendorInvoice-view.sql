
CREATE or REPLACE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE WHEN IObVendorInvoiceSummary.remaining IS NULL THEN 0 ELSE IObVendorInvoiceSummary.remaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObVendorInvoiceSummary
                   ON dobInvoice.id = IObVendorInvoiceSummary.refinstanceid;
