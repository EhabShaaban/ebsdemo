DROP TABLE IF EXISTS IObVendorInvoiceSummary;
CREATE TABLE IObVendorInvoiceSummary
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL REFERENCES dobvendorinvoice (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    remaining        float,
    downPayment      float,
    PRIMARY KEY (id)
);