DROP VIEW If EXISTS IObLandedCostDetailsGeneralModel;

CREATE OR REPLACE VIEW IObLandedCostFactorItemsDetailsGeneralModel AS

SELECT 
	IObLandedCostFactorItemsDetails.id,
	IObLandedCostFactorItemsDetails.refinstanceid,
	IObLandedCostFactorItemsDetails.actualValue,
	IObLandedCostFactorItemsDetails.creationDate,
    IObLandedCostFactorItemsDetails.modifiedDate,
    IObLandedCostFactorItemsDetails.creationInfo,
    IObLandedCostFactorItemsDetails.modificationInfo,
	
	DObAccountingDocument.code 									   AS documentInfoCode,
	DObAccountingDocument.objectTypeCode 						   AS documentInfoType
	
FROM IObLandedCostFactorItemsDetails

LEFT JOIN IObLandedCostFactorItems
	   ON IObLandedCostFactorItems.id = IObLandedCostFactorItemsDetails.refinstanceid 

LEFT JOIN DObAccountingDocument
	   ON DObAccountingDocument.id = IObLandedCostFactorItemsDetails.documentInfoId;


create or replace view IObLandedCostDetailsGeneralModel as
select ioblandedcostdetails.id,
       ioblandedcostdetails.refinstanceid,
       ioblandedcostdetails.creationdate,
       ioblandedcostdetails.creationinfo,
       ioblandedcostdetails.modificationinfo,
       ioblandedcostdetails.modifieddate,
       landedCost.code                                           as landedCostCode,
       dobaccountingdocument.code                                as vendorInvoiceCode,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           dobaccountingdocument.objecttypecode) as invoiceAmount,
       doborderdocument.code                                     as purchaseOrderCode,
       invoiceCurrency.iso                                       as invoiceCurrencyIso,
       ioblandedcostdetails.damageStock,
       cobenterprise.code                                        as businessUnitCode,
       cobenterprise.name                                        as businessUnitName,
       concat(cobenterprise.code, ' ', '-', ' ',
              cobenterprise.name::json ->> 'en')                 as businessUnitCodeName
from ioblandedcostdetails
         left join dobaccountingdocument landedCost
                   on ioblandedcostdetails.refinstanceid = landedCost.id
         left join doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join dobaccountingdocument
                   on ioblandedcostdetails.vendorinvoiceid = dobaccountingdocument.id

         left join iobordercompany on iobordercompany.refinstanceid = doborderdocument.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         left join iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = ioblandedcostdetails.vendorinvoiceid

         left join cobcurrency invoiceCurrency
                   on invoiceCurrency.id = iobvendorinvoicepurchaseorder.currencyid
         left join doblandedcost on doblandedcost.id = ioblandedcostdetails.refinstanceid
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON landedCost.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         left join cobenterprise on cobenterprise.id = IObLandedCostCompanyData.purchaseUnitId;
