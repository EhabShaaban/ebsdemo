DROP VIEW If Exists IObLandedCostCalculationStatusGeneralModel;
DROP TABLE If Exists IObLandedCostCalculationStatus;
ALTER TABLE IObLandedCostDetails DROP COLUMN currencyPrice;

CREATE TABLE IObLandedCostFactorItemsDetails (
    id BIGSERIAL NOT NULL,
    refInstanceId INT8 NOT NULL REFERENCES IObLandedCostFactorItems (id) ON DELETE CASCADE,
    documentInfoId INT8 NOT NULL REFERENCES DObAccountingDocument (id),
    actualValue DOUBLE PRECISION NOT NULL,
    creationDate TIMESTAMP NOT NULL,
    modifiedDate TIMESTAMP NOT NULL,
    creationInfo VARCHAR (1024) NOT NULL,
    modificationInfo VARCHAR (1024) NOT NULL,
    PRIMARY KEY (id)
);