DROP VIEW IF EXISTS IObOrderItemSummaryGeneralModel;

DROP FUNCTION if exists getOrderTotalTaxes(orderId bigint);
CREATE or REPLACE FUNCTION getOrderTotalTaxes(orderId bigint)
    RETURNS float AS
$$
BEGIN
return case when
                (SELECT SUM(amount)
                 FROM iobordertaxgeneralmodel
                 WHERE refinstanceid = orderId) is null then 0
            else (SELECT SUM(amount)
                  FROM iobordertaxgeneralmodel
                  WHERE refinstanceid = orderId) end ;
END;
$$
LANGUAGE PLPGSQL;


CREATE VIEW IObOrderItemSummaryGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.objecttypecode,
       doborderdocument.code                               AS orderCode,
       getOrderTotalAmountBeforeTaxes(doborderdocument.id) AS totalAmountBeforeTaxes,
       getOrderTotalTaxes(doborderdocument.id)             AS totalTaxes,
       getOrderTotalAmountBeforeTaxes(doborderdocument.id)
           + getOrderTotalTaxes(doborderdocument.id)       AS totalAmountAfterTaxes
FROM doborderdocument;
