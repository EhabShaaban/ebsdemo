CREATE OR REPLACE VIEW DObLandedCostGeneralModel AS
SELECT LC.id,
       AccDoc.code,
       AccDoc.creationdate,
       AccDoc.modifieddate,
       AccDoc.creationinfo,
       AccDoc.modificationinfo,
       AccDoc.currentstates,
       cobenterprise.code                AS purchaseUnitCode,
       cobenterprise.name                AS purchaseUnitName,
       cobenterprise.name::json ->> 'en' AS purchaseUnitNameEn,
       CObLandedCostType.code            AS typeCode,
       CObLandedCostType.name            AS typeName,
       doborderdocument.code             AS purchaseOrderCode,
       userinfo.name                     AS documentOwner,
       doborderdocument.id               AS purchaseOrderId
FROM DObLandedCost LC
         LEFT JOIN DObAccountingDocument AccDoc
                   ON LC.id = AccDoc.id AND AccDoc.objecttypecode = 'LandedCost'
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON LC.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         LEFT JOIN cobenterprise ON IObLandedCostCompanyData.purchaseUnitId = cobenterprise.id
         LEFT JOIN IObLandedCostDetails LCD ON LC.id = LCD.refinstanceid
         LEFT JOIN doborderdocument ON LCD.purchaseorderid = doborderdocument.id AND
                                       doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN CObLandedCostType ON CObLandedCostType.id = LC.typeId
         LEFT JOIN userinfo on userinfo.id = AccDoc.documentownerid;