DROP VIEW IF EXISTS IObNotesReceivableAccountingDetailsGeneralModel;

CREATE VIEW IObNotesReceivableAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer'
               THEN (
               customer.code
               )
           WHEN accountDetails.subledger = 'Notes_Receivables'
               THEN (
               notesRecivable.code
               )
           END                    AS glSubAccountCode,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer' THEN
               (customeraccountingdetails.glSubAccountId)
           WHEN accountDetails.subledger = 'Notes_Receivables'
               THEN
               (notesreceivableaccountingdetails.glSubAccountId)
           END                    AS glSubAccountId,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer'
               THEN (customer.name)
           END                    AS glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         LEFT JOIN dobmonetarynotes ON accountDetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = dobmonetarynotes.id
         LEFT JOIN hobglaccount account ON account.id = accountDetails.glaccountid
         LEFT JOIN iobaccountingdocumentcustomeraccountingDetails customeraccountingdetails
                   ON customeraccountingdetails.id = accountDetails.id
         LEFT JOIN iobaccountingdocumentnotesreceivableaccountingdetails notesreceivableaccountingdetails
                   ON notesreceivableaccountingdetails.id = accountDetails.id
         LEFT JOIN masterdata customer
                   ON customer.id = customeraccountingdetails.glSubAccountId AND
                      customer.objecttypecode = '3'
         LEFT JOIN dobaccountingdocument notesRecivable
                   ON notesRecivable.id = notesreceivableaccountingdetails.glSubAccountId AND
                      notesRecivable.objecttypecode = 'NOTES_RECEIVABLE_AS_COLLECTION'
where accountDetails.objecttypecode = 'NOTES_RECEIVABLE';