CREATE OR REPLACE VIEW DObSettlementActivationPreparation AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       cobfiscalperiod.id             as fiscalPeriodId,
       latestexchangerate.exchangerateid as exchangeRateId,
       latestexchangerate.currencyprice
from dobsettlement
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobsettlement.id
         left join cobfiscalperiod ON cobfiscalperiod.currentstates like '%Active%'
         left join iobsettlementdetails on iobsettlementdetails.refinstanceid = dobaccountingdocument.id
         left join iobaccountingdocumentcompanydata
                   on iobaccountingdocumentcompanydata.refinstanceid = dobaccountingdocument.id
         left join iobenterprisebasicdata on iobaccountingdocumentcompanydata.companyid = iobenterprisebasicdata.refinstanceid
         left join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(iobsettlementdetails.currencyid, iobenterprisebasicdata.currencyid ) latestexchangerate
                   on latestexchangerate.firstcurrencyid = iobsettlementdetails.currencyid
                       and latestexchangerate.secondcurrencyid = iobenterprisebasicdata.currencyid;