CREATE Or Replace View DObPaymentRequestActivatePreparationGeneralModel AS
Select paymentRequest.id,
       paymentRequest.code                                                        as paymentRequestCode,
       businessUnit.code                                                          as businessUnitCode,
       company.code                                                               as companyCode,
       cobcurrency.code                                                           as currencyCode,
       cobbank.code                                                               as bankCode,
       iobcompanybankdetails.accountno                                            as bankAccountNo,
       cobtreasury.code                                                           as treasuryCode,
       details.netamount                                                          as paymentNetAmount,
       dobpaymentrequest.paymenttype                                              as paymenttype,
       details.dueDocumentType                                                    as dueDocumentType,
       getDueDocumentCodeAccordingToPaymentType(details.id, details.dueDocumentType   ) as dueDocumentCode,
       getJournalBalanceByCode(details.paymentform, businessUnit.code, company.code, cobcurrency.code,
                               cobbank.code,
                               iobcompanybankdetails.accountno, cobtreasury.code) as balance,
       latestexchangerate.currencyprice                                           as currencyprice,
       cobexchangerate.code                                                       as exchangerate
from dobaccountingdocument paymentRequest
         left join dobpaymentrequest on dobpaymentrequest.id =  paymentRequest.id
         left join iobaccountingdocumentcompanydata paymentCompanyData
                   on paymentRequest.id = paymentCompanyData.refinstanceid
         left join cobenterprise company on company.id = paymentCompanyData.companyid
         left join cobenterprise businessUnit on businessUnit.id = paymentCompanyData.purchaseunitid
         left join iobpaymentrequestpaymentdetails details on details.refinstanceid = paymentRequest.id
         left join cobcurrency on cobcurrency.id = details.currencyid
         left join iobcompanybankdetails on iobcompanybankdetails.id = details.bankaccountid
         left join cobbank on cobbank.id = iobcompanybankdetails.bankid
         left join cobtreasury on cobtreasury.id = details.treasuryid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = paymentCompanyData.companyid
         left join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(details.currencyid,
                                                                            companyData.currencyid) latestexchangerate
                   on latestexchangerate.firstcurrencyid = details.currencyid
                       and latestexchangerate.secondcurrencyid = companydata.currencyid
         left join cobexchangerate on cobexchangerate.id = latestexchangerate.exchangerateid
WHERE paymentRequest.objecttypecode = 'PaymentRequest'
  AND paymentCompanyData.objecttypecode = 'PaymentRequest'
  AND company.objecttypecode = '1'
  AND businessUnit.objecttypecode = '5';

CREATE OR REPLACE VIEW DObPaymentRequestCurrencyPriceGeneralModel AS
SELECT accountingDocument.id,
       accountingDocument.code AS userCode,
       paymentDetails.netamount,
       paymentDetails.duedocumenttype,
       purchaseOrderPaymentDetails.duedocumentid,
       CASE
           WHEN paymentdetails.duedocumenttype::text = 'PURCHASEORDER'::text THEN purchaseOrder.code
           WHEN paymentdetails.duedocumenttype::text = 'INVOICE'::text THEN vendorInvoice.code
           WHEN paymentdetails.duedocumenttype::text = 'CREDITNOTE'::text THEN creditnote.code
           END                 AS referencedocumentcode,
       activationDetails.currencyprice,
       paymentRequest.paymenttype
FROM iobpaymentrequestpaymentdetails paymentDetails
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails creditNotePaymentDetails
                   ON creditNotePaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails purchaseOrderPaymentDetails
                   ON purchaseOrderPaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestinvoicepaymentdetails invoicePaymentDetails
                   ON invoicePaymentDetails.id = paymentDetails.id
         JOIN dobpaymentrequest paymentRequest ON paymentRequest.id = paymentDetails.refinstanceid
         JOIN dobaccountingdocument accountingDocument ON accountingDocument.id = paymentRequest.id
         JOIN iobaccountingdocumentactivationdetails activationDetails
              ON activationDetails.refinstanceid = accountingDocument.id
         LEFT JOIN doborderdocument purchaseOrder ON purchaseOrder.id = purchaseOrderPaymentDetails.duedocumentid
         LEFT JOIN dobaccountingdocument vendorInvoice ON vendorInvoice.id = invoicePaymentDetails.duedocumentid
         LEFT JOIN dobaccountingdocument creditNote ON creditNote.id = creditNotePaymentDetails.duedocumentid;