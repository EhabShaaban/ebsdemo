CREATE or REPLACE view DObVendorInvoicePostPreparationGeneralModel as
select dobaccountingdocument.id,
       code,
       IObVendorInvoicePurchaseOrder.currencyid,
       latestexchangerate.exchangerateid as exchangerateid,
       latestexchangerate.currencyprice  as currencyprice
from dobaccountingdocument
         inner join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         inner join iobaccountingdocumentcompanydata
                    on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         inner join iobenterprisebasicdata companydata
                    on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         inner join IObVendorInvoicePurchaseOrder
                    on dobaccountingdocument.id = iobvendorinvoicepurchaseorder.refinstanceid
         inner join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(IObVendorInvoicePurchaseOrder.currencyid,
                                                                             companydata.currencyid) latestexchangerate
                    on latestexchangerate.firstcurrencyid = IObVendorInvoicePurchaseOrder.currencyid
                        and latestexchangerate.secondcurrencyid = companydata.currencyid;
