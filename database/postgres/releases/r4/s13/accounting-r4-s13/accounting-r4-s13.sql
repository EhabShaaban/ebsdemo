\ir 'CObExchangeRate/CObExchangeRate_Dependencies_Drop.sql'
\ir 'CObExchangeRate/CObExchangeRate.sql'
\ir 'CObExchangeRate/CObExchangeRate_Views.sql'
\ir 'AccountingDocument/AccountingDocument_Views.sql'
\ir 'HObChartsOfAccounts/HObChartsOfAccounts_Views.sql'
\ir 'DObPaymentRequest/DObPaymentRequest_Views.sql'
\ir 'DObVendorInvoice/DObVendorInvoice_Views.sql'
\ir 'DObSalesInvoice/DObSalesInvoice_Views.sql'
\ir 'DObSettlement/DObSettlement_Views.sql'
\ir 'Costing/Costing_Views.sql'
\ir 'DObNotesReceivables/DObNotesReceivables_Views.sql'
\ir 'DObPaymentRequest/DObPaymentRequest_Views.sql'
\ir 'DObLandedCost/DObLandedCost_Views.sql'
\ir 'DObJournalEntry/DObJournalEntry_View.sql'


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
