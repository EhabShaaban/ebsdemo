CREATE or REPLACE view VendorInvoiceItemWeight as
select invoiceItem.refinstanceid                                          as invoiceId,
       invoiceItem.code                                                   as invoiceCode,
       LCDetails.purchaseorderid                                          as purchaseorderid,
       purchaseOrder.code                                               as purchaseorderCode,
       dobinventorydocument.id                                            as goodsreceiptId,
       dobinventorydocument.code                                          as goodsreceiptCode,
       LC.code                                                            as landedCostCode,
       LC.id                                                              as landedCostId,
       LC.currentstates                                                   as landedCostState,
       invoiceItem.id                                                     as itemId,
       invoiceItem.itemcode                                               as itemCode,
       invoiceItem.totalamount /
       getInvoiceTotalAmountBeforTaxes(invoiceItem.code, 'VendorInvoice') as itemWeight,
       case
           when
                   LC.currentstates like '%ActualValuesCompleted%'
               then LCSummary.actualtotalamount
           else
               LCSummary.estimatetotalamount
           end                                                            as landedCostTotal,
       invoiceItem.orderunitprice                                         as itemUnitPrice,
       (select case
                   when getAmountPaidViaAccountingDocument(vendorInvoice.code,
                                                           vendorInvoiceCompany.objecttypecode) =
                        0.0
                       then (select avg(iobAccountingdocumentactivationdetails.currencyprice)
                             from iobAccountingdocumentactivationdetails
                                      left JOIN iobpaymentrequestpaymentdetails
                                                on iobpaymentrequestpaymentdetails.refinstanceid =
                                                   iobAccountingdocumentactivationdetails.refinstanceid
                                      left join iobpaymentrequestinvoicepaymentdetails
                                                on iobpaymentrequestinvoicepaymentdetails.id =
                                                   iobpaymentrequestpaymentdetails.id
                             where iobpaymentrequestinvoicepaymentdetails.duedocumentid =
                                   LCDetails.vendorinvoiceid
                   )
                   else (
                       select latestexchangerate.currencyPrice
                       from getLatestCurrencyPrice(iobvendorinvoicepurchaseorder.currencyid,
                                                   iobenterprisebasicdata.currencyid) latestexchangerate
                   )
                   end ) as currencyPrice,
       invoiceItem.orderunitcode,
       invoiceItem.orderunitsymbol
from IObVendorInvoiceItemsGeneralModel invoiceItem
         left join ioblandedcostdetails LCDetails
                   on invoiceItem.refinstanceid = LCDetails.vendorinvoiceId
         left join dobaccountingdocument vendorInvoice
                   on vendorInvoice.id = LCDetails.vendorinvoiceId
         inner join dobvendorinvoice on dobvendorinvoice.id = vendorInvoice.id
         left join doborderdocument purchaseOrder on purchaseOrder.id = LCDetails.purchaseorderid
         left join iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = LCDetails.vendorinvoiceid
         left join iobaccountingdocumentcompanydata vendorInvoiceCompany on vendorInvoiceCompany.refinstanceid = LCDetails.vendorinvoiceId and vendorInvoiceCompany.objecttypecode = 'VendorInvoice'
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = vendorInvoiceCompany.companyid
         inner join dobaccountingdocument LC
                    on LCDetails.refinstanceid = LC.id and
                       LC.objecttypecode = 'LandedCost' and LC.currentstates not like '%Draft%'
         left join IObLandedCostFactorItemsSummary LCSummary on LCSummary.id = LC.id
         inner join iobgoodsreceiptpurchaseorderdata
                    on LCDetails.purchaseorderid = iobgoodsreceiptpurchaseorderdata.purchaseorderid
         inner join dobinventorydocument on dobinventorydocument.id = iobgoodsreceiptpurchaseorderdata.refinstanceid
    and dobinventorydocument.currentstates like '%Active%';

CREATE VIEW UnrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId          as itemId,
       vendorinvoiceitemweight.landedCostState as landedCostState,
       vendorinvoiceitemweight.landedCostCode  as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal            as itemTotalLandedCost,
       (itemweight * landedcosttotal) /
       GRUnrestrectedItem.receivedqtyuoe       as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
        (itemunitprice * currencyprice))       as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode =
                       vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and
                       vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode;

CREATE VIEW PurchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode =
                      unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode =
                           GRDamagedItem.unitofentrycode;

CREATE OR REPLACE VIEW IObCostingAccountingDetailsGeneralModel AS
SELECT accountingDetails.id,
       accountingDetails.refinstanceid,
       dobaccountingdocument.code AS documentCode,
       accountingDetails.creationDate,
       accountingDetails.modifiedDate,
       accountingDetails.creationInfo,
       accountingDetails.modificationInfo,
       accountingDetails.accountingEntry,
       accountingDetails.subledger,
       account.id                 AS glAccountid,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       dobinventorydocument.code  AS goodsReceiptCode,
       orderDocument.id           AS glSubAccountid,
       orderDocument.code         AS glSubAccountCode,
       cobcurrency.code           AS currencyCode,
       cobcurrency.iso            AS currencyISO,
       null::json                 As glSubAccountName,
       accountingDetails.amount
FROM IObAccountingDocumentAccountingDetails accountingDetails
         LEFT JOIN iobaccountingdocumentorderaccountingdetails orderAccountingDetails ON orderAccountingDetails.id = accountingDetails.id
         LEFT JOIN doborderdocument orderDocument ON orderDocument.id = orderAccountingDetails.glsubaccountid
         LEFT JOIN dobcosting costing ON accountingDetails.refinstanceid = costing.id
         INNER JOIN dobaccountingdocument ON dobaccountingdocument.id = costing.id
         INNER JOIN iobcostingdetails on costing.id = iobcostingdetails.refinstanceid
         INNER JOIN dobinventorydocument on dobinventorydocument.id = iobcostingdetails.goodsreceiptid
         INNER JOIN iobinventorycompany on iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN cobenterprise company ON iobinventorycompany.companyid = company.id
         LEFT JOIN cobcompany ON iobinventorycompany.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id
         LEFT JOIN hobglaccount account ON account.id = accountingDetails.glaccountid;