DROP FUNCTION IF EXISTS getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(firstCurrency bigint, secondCurrency bigint);
DROP FUNCTION IF EXISTS getLatestCurrencyPrice(firstCurrency bigint, secondCurrency bigint);
CREATE or REPLACE VIEW CObExchangeRateGeneralModel AS
SELECT cobexchangerate.id,
       cobexchangerate.code,
       cobexchangerate.creationDate,
       cobexchangerate.modifiedDate,
       cobexchangerate.creationInfo,
       cobexchangerate.modificationInfo,
       cobexchangerate.currentstates,
       cobexchangerate.validFrom,
       cobexchangerate.firstValue,
       cobexchangerate.secondValue,
       cobexchangerate.firstCurrencyId,
       cobexchangerate.secondCurrencyId,
       CObExchangeRateType.code 			AS exchangetype,
       cobexchangerate.name,
       firstCurrency.iso    				AS firstCurrencyIso,
       firstCurrency.code   				AS firstCurrencyCode,
       secondCurrency.iso   				AS secondCurrencyIso,
       secondCurrency.code  				AS secondCurrencyCode,
       CObExchangeRateType.name 			AS exchangetypename
FROM cobexchangerate
         LEFT JOIN CObCurrency firstCurrency
                   ON cobexchangerate.firstCurrencyId = firstCurrency.id
         LEFT JOIN CObCurrency secondCurrency
                   ON cobexchangerate.secondCurrencyId = secondCurrency.id
         LEFT JOIN CObExchangeRateType
                   ON cobexchangerate.typeId = CObExchangeRateType.id;

CREATE OR REPLACE FUNCTION getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(firstCurrency bigint, secondCurrency bigint)
    RETURNS table
            (
                currencyPrice    numeric,
                exchangerateId   bigint,
                firstcurrencyid  bigint,
                secondcurrencyid bigint
            )
AS
$$
BEGIN
RETURN QUERY (
        (select firstvalue * secondvalue,
                cobexchangerate.id,
                cobexchangerate.firstcurrencyid,
                cobexchangerate.secondcurrencyid
         from cobexchangerate left join cobexchangeratetype type on cobexchangerate.typeid = type.id
         where cobexchangerate.firstcurrencyid = firstCurrency
           and cobexchangerate.secondcurrencyid = secondCurrency
           and type.isdefault = true
         ORDER BY cobexchangerate.id DESC
            LIMIT 1));

END;
$$
LANGUAGE PLPGSQL;


CREATE OR REPLACE FUNCTION getLatestCurrencyPrice(firstCurrency bigint, secondCurrency bigint)
    RETURNS table
            (
                currencyPrice    numeric,
                exchangerateId   bigint,
                firstcurrencyid  bigint,
                secondcurrencyid bigint
            )
AS
$$
BEGIN
RETURN QUERY (
        (select firstvalue * secondvalue,
                cobexchangerate.id,
                cobexchangerate.firstcurrencyid,
                cobexchangerate.secondcurrencyid
         from
             cobexchangerate join cobexchangeratetype
                                  on cobexchangerate.typeid = cobexchangeratetype.id
                                      and cobexchangeratetype.isdefault is true
         where
                 cobexchangerate.firstcurrencyid = firstCurrency
           and cobexchangerate.secondcurrencyid = secondCurrency

         ORDER BY validfrom DESC
            LIMIT 1));

END;
$$
LANGUAGE PLPGSQL;
