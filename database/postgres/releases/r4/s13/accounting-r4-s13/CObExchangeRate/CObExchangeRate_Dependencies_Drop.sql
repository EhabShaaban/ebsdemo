DROP VIEW If EXISTS IObAccountingDocumentActivationDetailsGeneralModel;
DROP VIEW If EXISTS CObExchangeRateGeneralModel;
DROP VIEW If EXISTS DObPaymentRequestActivatePreparationGeneralModel;
DROP VIEW If EXISTS DObSalesInvoicePostPreparationGeneralModel;
DROP VIEW If EXISTS DObSettlementActivationPreparation;
DROP VIEW If EXISTS DObVendorInvoicePostPreparationGeneralModel;
DROP VIEW If EXISTS DObSalesInvoiceJournalEntryPreparationGeneralModel;
DROP VIEW If EXISTS PurchasedItemsTotalFinalCost;
DROP VIEW If EXISTS UnrestrictedItemsSingleUnitFinalCost;
DROP VIEW If EXISTS VendorInvoiceItemWeight;
