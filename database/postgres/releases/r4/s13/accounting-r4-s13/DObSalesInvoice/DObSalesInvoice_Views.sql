CREATE OR REPLACE VIEW DObSalesInvoicePostPreparationGeneralModel AS
select
    dobaccountingdocument.id,
    dobaccountingdocument.code,
    latestexchangerate.exchangerateid as exchangerateid,
    latestexchangerate.currencyprice as currencyprice
from dobaccountingdocument inner join iobsalesinvoicebusinesspartner
                                      on dobaccountingdocument.id = iobsalesinvoicebusinesspartner.refinstanceid
                           inner join iobaccountingdocumentcompanydata on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
    AND iobaccountingdocumentcompanydata.objecttypecode = 'SalesInvoice'
                           inner join iobenterprisebasicdata companydata on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
                           inner join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(iobsalesinvoicebusinesspartner.currencyid, companydata.currencyid) latestexchangerate
                                      on latestexchangerate.firstcurrencyid = iobsalesinvoicebusinesspartner.currencyid
                                          and latestexchangerate.secondcurrencyid = companydata.currencyid
                                          and
                                         dobaccountingdocument.objecttypecode = 'SalesInvoice';


Create OR REPLACE view DObSalesInvoiceJournalEntryPreparationGeneralModel as
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       customerbusinesspartner.customerid,
       postingdetails.duedate,
       iobaccountingdocumentcompanydata.companyid,
       iobaccountingdocumentcompanydata.purchaseunitid,
       dobaccountingdocument.id                        as documentreferenceid,
       businesspartner.currencyid                      as customercurrencyid,
       iobenterprisebasicdata.currencyid               as companycurrencyid,
       currecyPrice.currencyPrice,
       currecyPrice.exchangerateId,
       getInvoiceTotalAmountBeforTaxes(dobaccountingdocument.code,
                                       'SalesInvoice') as amountBeforeTaxes,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           'SalesInvoice')             as amountAfterTaxes,
       iobcustomeraccountinginfo.chartofaccountid      as customeraccountId,
       customerAccount.leadger                         as customerLeadger,
       lobglobalglaccountconfig.accountid              as salesaccountid
from dobaccountingdocument
         left join iobaccountingdocumentactivationdetails postingdetails
                   on postingdetails.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicebusinesspartner businesspartner
                   on businesspartner.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicecustomerbusinesspartner customerbusinesspartner
                   on customerbusinesspartner.id = businesspartner.id
         left join iobaccountingdocumentcompanydata
                   on iobaccountingdocumentcompanydata.refinstanceid = dobaccountingdocument.id
         left join iobenterprisebasicdata on iobaccountingdocumentcompanydata.companyid =
                                             iobenterprisebasicdata.refinstanceid
         left join getLatestCurrencyPrice(businesspartner.currencyid,
                                          iobenterprisebasicdata.currencyid) currecyPrice
                   on currecyPrice.firstcurrencyid = businesspartner.currencyid
                       and currecyPrice.secondcurrencyid = iobenterprisebasicdata.currencyid
         left join iobcustomeraccountinginfo
                   on iobcustomeraccountinginfo.refinstanceid = customerbusinesspartner.customerid
         left join leafglaccount customerAccount
                   on customerAccount.id = iobcustomeraccountinginfo.chartofaccountid
         left join lobglobalglaccountconfig on lobglobalglaccountconfig.code = 'SalesGLAccount'
where dobaccountingdocument.objecttypecode = 'SalesInvoice'
  and postingdetails.objecttypecode = 'SalesInvoice'
  and iobaccountingdocumentcompanydata.objecttypecode = 'SalesInvoice';

