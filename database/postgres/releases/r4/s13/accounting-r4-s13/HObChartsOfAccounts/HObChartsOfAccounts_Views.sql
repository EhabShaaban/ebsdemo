Create OR REPLACE View SubAccount As
select id as subaccountid, code, null as name, 'PO' as ledger
from doborderdocument
WHERE objecttypecode LIKE '%_PO'
union all
select id as subaccountid, accountNo as code, name, 'Banks' as ledger
from IObCompanyBankDetailsGeneralModel
union all
select id as subaccountid, code, name, 'Treasuries' as ledger
from cobtreasury
union all
select id as subaccountid, code, name, 'Local_Customer' as ledger
from masterdata
where objecttypecode = '3'
union all
select id as subaccountid, code, name, 'Export_Customers' as ledger
from masterdata
where objecttypecode = '3'
union all
select id as subaccountid, code, name, 'Taxes' as ledger
from lobtaxinfo
union all
select id as subaccountid, code, name, 'Local_Vendors' as ledger
from masterdata
where objecttypecode = '2'
union all
select id as subaccountid, code, name, 'Import_Vendors' as ledger
from masterdata
where objecttypecode = '2'
union all
select dobmonetarynotes.id as subaccountid, code, null, 'NotesReceivable' as ledger
from dobaccountingdocument
         JOIN dobmonetarynotes
              ON dobaccountingdocument.id = dobmonetarynotes.id AND
                 objecttypecode = 'NOTES_RECEIVABLE_AS_COLLECTION';


Create OR REPLACE View SubAccountGeneralModel As
select row_number() over () as id, SubAccount.*
from SubAccount;

