\ir 'order-r4-s13/order-r4-s13.sql'
\ir 'accounting-r4-s13/accounting-r4-s13.sql'
\ir 'masterdata-r4-s13/masterdata-r4-s13.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
