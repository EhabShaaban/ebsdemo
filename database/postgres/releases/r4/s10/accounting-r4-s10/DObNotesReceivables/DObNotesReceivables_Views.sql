DROP VIEW IF EXISTS DObNotesPayableGeneralModel;
CREATE VIEW DObNotesPayableGeneralModel AS
SELECT dobmonetarynotes.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.objecttypecode,
       MasterData.code                                          AS businessPartnerCode,
       MasterData.name                                          AS businessPartnerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
       iobmonetarynotesdetails.remaining,
       dobaccountingdocument.documentownerid,
       userinfo.name                                            AS documentOwner,
       Company.code                                             AS companyCode,
       Company.name                                             AS companyName,
       iobmonetarynotesdetails.notedate,
       iobmonetarynotesdetails.amount,
       CObCurrency.iso                                          AS currencyIso,
       iobmonetarynotesdetails.noteForm
FROM dobmonetarynotes
         JOIN dobaccountingdocument
              ON dobaccountingdocument.id = dobmonetarynotes.id AND
                 objecttypecode LIKE 'NOTES_PAYABLE_%'
         LEFT JOIN userinfo ON dobaccountingdocument.documentownerid = userinfo.id
         LEFT JOIN ebsuser ON dobaccountingdocument.documentownerid = ebsuser.id
         LEFT JOIN iobmonetarynotesdetails
                   ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN CObEnterprise BusinessUnit
                   ON iobaccountingdocumentcompanydata.purchaseunitid = BusinessUnit.id AND
                      BusinessUnit.objecttypecode = '5'
         LEFT JOIN CObEnterprise Company
                   ON iobaccountingdocumentcompanydata.companyid = Company.id AND
                      Company.objecttypecode = '1'
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '2';


DROP VIEW IF EXISTS IObMonetaryNotesDetailsGeneralModel;
CREATE VIEW IObMonetaryNotesDetailsGeneralModel AS
SELECT IObMonetaryNotesDetails.id,
       IObMonetaryNotesDetails.refinstanceid,
       IObMonetaryNotesDetails.creationInfo,
       IObMonetaryNotesDetails.modificationInfo,
       IObMonetaryNotesDetails.creationDate,
       IObMonetaryNotesDetails.modifiedDate,
       dobaccountingdocument.code AS userCode,
       dobaccountingdocument.objecttypecode,
       iobmonetarynotesdetails.noteForm,
       MasterData.code            AS businessPartnerCode,
       MasterData.name            AS businessPartnerName,
       iobmonetarynotesdetails.notenumber,
       iobmonetarynotesdetails.notebank,
       iobmonetarynotesdetails.notedate,
       CObCurrency.code           AS currencyCode,
       CObCurrency.iso            AS currencyIso,
       iobmonetarynotesdetails.amount,
       iobmonetarynotesdetails.remaining,
       lobdepot.code              AS depotCode,
       lobdepot.name              AS depotName
FROM IObMonetaryNotesDetails
         LEFT JOIN dobmonetarynotes ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = dobmonetarynotes.id
         LEFT JOIN lobdepot ON lobdepot.id = iobmonetarynotesdetails.depotid
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '3';

DROP VIEW IF EXISTS DObNotesReceivablesGeneralModel;
CREATE VIEW DObNotesReceivablesGeneralModel AS
SELECT dobmonetarynotes.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.objecttypecode,
       MasterData.code                                          AS businessPartnerCode,
       MasterData.name                                          AS businessPartnerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
       iobmonetarynotesdetails.remaining,
       dobaccountingdocument.documentownerid,
       userinfo.name                                            AS documentOwner,
       concat(Company.code, ' - ', Company.name::json ->> 'en') AS companyCodeName,
       Company.code                                             AS companyCode,
       Company.name                                             AS companyName,
       iobmonetarynotesdetails.notenumber,
       iobmonetarynotesdetails.notedate,
       iobmonetarynotesdetails.amount,
       CObCurrency.iso                                          AS currencyIso,
       iobmonetarynotesdetails.notebank,
       iobmonetarynotesdetails.noteForm,
       lobdepot.code                                            AS depotCode,
       lobdepot.name                                            AS depotName
FROM dobmonetarynotes
         JOIN dobaccountingdocument
              ON dobaccountingdocument.id = dobmonetarynotes.id AND
                 objecttypecode LIKE 'NOTES_RECEIVABLE_%'
         LEFT JOIN userinfo ON dobaccountingdocument.documentownerid = userinfo.id
         LEFT JOIN ebsuser ON dobaccountingdocument.documentownerid = ebsuser.id
         LEFT JOIN iobmonetarynotesdetails
                   ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN CObEnterprise BusinessUnit
                   ON iobaccountingdocumentcompanydata.purchaseunitid = BusinessUnit.id AND
                      BusinessUnit.objecttypecode = '5'
         LEFT JOIN CObEnterprise Company
                   ON iobaccountingdocumentcompanydata.companyid = Company.id AND
                      Company.objecttypecode = '1'
         LEFT JOIN lobdepot ON lobdepot.id = iobmonetarynotesdetails.depotid
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '3';
