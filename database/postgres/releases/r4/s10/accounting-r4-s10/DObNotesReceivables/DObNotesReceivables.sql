
\ir 'Drop_Views.sql'
ALTER TABLE iobmonetarynotesdetails
    ALTER COLUMN amount TYPE numeric,
    ALTER COLUMN remaining TYPE numeric;
