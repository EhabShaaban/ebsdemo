
CREATE or REPLACE FUNCTION getJournalEntryCodeDependsOnObjectTypeCode(objectTypeCode varchar, accouningDocumentId bigint)
    RETURNS varchar AS
$$
BEGIN
    RETURN case
               when objecttypecode = 'PaymentRequest' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobpaymentrequestjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode in ('Collection') then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobcollectionjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'SalesInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsalesinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'VendorInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobvendorinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'LandedCost' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from doblandedcostjournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'Settlement' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsettlementjournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'InitialBalanceUpload' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsettlementjournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'Costing' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from DObCostingJournalEntry where documentreferenceid = accouningDocumentId))
        end;
END;
$$
    LANGUAGE PLPGSQL;