\ir 'DObCosting/DObCosting_Views_Drop.sql'
\ir 'DObCosting/DObCosting.sql'
\ir 'DObCosting/DObCosting_Views.sql'

\ir 'JournalEntry/DObJournalEntry.sql'
\ir 'JournalEntry/DObJournalEntry_Views.sql'

\ir 'DObVendorInvoice/DObVendorInvoice_Views.sql'

\ir 'DObNotesReceivables/DObNotesReceivables.sql'
\ir 'DObNotesReceivables/DObNotesReceivables_Views.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;