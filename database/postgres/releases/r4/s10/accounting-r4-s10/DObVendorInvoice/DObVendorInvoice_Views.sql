DROP VIEW If EXISTS DObVendorInvoiceDebitPreparationGeneralModel;

DROP FUNCTION if exists hasVendorInvoiceTaxes(bigint);
CREATE FUNCTION hasVendorInvoiceTaxes(vendorInvoiceId BIGINT)
    RETURNS BOOLEAN AS
$$
BEGIN
    RETURN CASE
               WHEN
   ( SELECT count(*) AS taxesCount
    FROM iobvendorinvoicetaxes
    WHERE iobvendorinvoicetaxes.refinstanceid = vendorInvoiceId) > 0 then true
               else
                   false
        end;
END;
$$
    LANGUAGE PLPGSQL;


DROP FUNCTION if exists getItemAmountAfterTaxes(bigint, double precision);
CREATE FUNCTION getItemAmountAfterTaxes(vendorInvoiceId BIGINT , itemAmountBeforeTaxes DOUBLE PRECISION
                                              )
    RETURNS DOUBLE PRECISION AS
$$
BEGIN
    RETURN CASE
               WHEN hasVendorInvoiceTaxes(vendorInvoiceId) then

                   itemAmountBeforeTaxes + ((Select sum(lobtaxinfo.percentage)
                       from iobvendorinvoicetaxes left join lobtaxinfo
                   on iobvendorinvoicetaxes.code = lobtaxinfo.code
                   where iobvendorinvoicetaxes.refinstanceid = vendorInvoiceId) * itemAmountBeforeTaxes )/100
        else itemAmountBeforeTaxes
        end;
END;
$$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW DObVendorInvoiceDebitPreparationGeneralModel AS
SELECT row_number() OVER ()                     AS id,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companydata.currencyid                   AS companycurrencyid,
       iobvendorinvoicepurchaseorder.currencyid AS documentcurrencyid,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
       iobitemaccountinginfo.chartofaccountid   AS glaccountid,
       CASE
           WHEN isgoodsinvoice(dobaccountingdocument.objecttypecode) THEN iobvendorinvoicepurchaseorder.purchaseorderid
           ELSE iobpurchaseorderfulfillervendor.referencepoid
           END                                  AS glsubaccountid,
       leafglaccount.leadger                    AS accountledger,
       getItemAmountAfterTaxes(dobvendorinvoice.id, iobvendorinvoiceitem.totalamount)                                 AS amount
FROM dobaccountingdocument
         JOIN dobvendorinvoice ON dobaccountingdocument.id = dobvendorinvoice.id
         LEFT JOIN iobaccountingdocumentactivationdetails activationdetails
                   ON dobaccountingdocument.id = activationdetails.refinstanceid
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata companydata
                   ON companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         LEFT JOIN iobvendorinvoicepurchaseorder ON iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON iobpurchaseorderfulfillervendor.refinstanceid = iobvendorinvoicepurchaseorder.purchaseorderid
         LEFT JOIN iobvendorinvoiceitemsgeneralmodel iobvendorinvoiceitem ON iobvendorinvoiceitem.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobitemaccountinginfo ON iobitemaccountinginfo.refinstanceid = iobvendorinvoiceitem.itemid
         LEFT JOIN leafglaccount ON leafglaccount.id = iobitemaccountinginfo.chartofaccountid;

