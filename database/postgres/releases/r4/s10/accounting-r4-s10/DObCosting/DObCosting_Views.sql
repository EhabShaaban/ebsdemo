create or replace view IObCostingDetailsGeneralModel AS
select IObCostingDetails.id,
       dobaccountingdocument.code                           as userCode,
       IObCostingDetails.refinstanceid,
       IObCostingDetails.creationDate,
       IObCostingDetails.creationInfo,
       IObCostingDetails.modifiedDate,
       IObCostingDetails.modificationInfo,
       dobinventorydocument.code                            as goodsReceiptCode,
       doborder.code                                        as purchaseOrderCode,
       exchangeRate.code                                    AS exchangeRateCode,
       exchangeRate.firstvalue,
       (exchangerate.firstvalue * exchangeRate.secondvalue) AS currencyPrice,
       exchangeRate.firstcurrencyiso,
       exchangeRate.secondcurrencyiso,
       purchaseUnit.name :: json ->> 'en'                   as purchaseUnitNameEn,
       purchaseUnit.name                                    as purchaseUnitName
from IObCostingDetails
         left join DObGoodsReceiptPurchaseOrder
                   on IObCostingDetails.goodsreceiptid =
                      DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         left join DObCosting
                   on IObCostingDetails.refinstanceid =
                      DObCosting.id

         left join dobaccountingdocument on dobaccountingdocument.id = DObCosting.id

         left join iobgoodsreceiptpurchaseorderdata
                   on iobgoodsreceiptpurchaseorderdata.refinstanceid =
                      DObGoodsReceiptPurchaseOrder.id
         left join doborderdocument doborder
                   on iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id
         left join cobexchangerategeneralmodel exchangeRate
                   on IObCostingDetails.exchangerateid = exchangerate.id
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join cobenterprise purchaseUnit on iobaccountingdocumentcompanydata.purchaseunitid = purchaseUnit.id;



create or replace view DobCostingGeneralModel AS
select dobcosting.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.creationdate,
       dobaccountingdocument.modificationinfo,
       dobaccountingdocument.modifieddate,
       dobaccountingdocument.currentstates,
       concat(company.code, ' - ', company.name::json ->> 'en')           as companyCodeName,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name::json ->> 'en') as purchaseUnitCodeName,
       purchaseUnit.name                                                  AS purchaseUnitName,
       purchaseUnit.name :: JSON ->> 'en'                                 AS purchaseUnitNameEn,
       purchaseUnit.code                                                  AS purchaseUnitcode

from DObCosting
         JOIN dobaccountingdocument on dobaccountingdocument.id = DObCosting.id
         LEFT JOIN IObAccountingDocumentCompanyData
                   ON IObAccountingDocumentCompanyData.refInstanceId = DObCosting.id
         LEFT JOIN cobenterprise purchaseUnit
                   ON IObAccountingDocumentCompanyData.purchaseUnitId = purchaseUnit.id AND
                      purchaseUnit.objecttypecode = '5'
         left join cobenterprise company
                   on IObAccountingDocumentCompanyData.companyid = company.id and company.objecttypecode = '1';


create or replace view IObCostingAccountingDetailsGeneralModel as

SELECT accountingDetails.id,
       accountingDetails.refinstanceid,
       dobaccountingdocument.code AS documentCode,
       accountingDetails.creationDate,
       accountingDetails.modifiedDate,
       accountingDetails.creationInfo,
       accountingDetails.modificationInfo,
       accountingDetails.accountingEntry,
       accountingDetails.subledger,
       account.id                 AS glAccountid,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       accountingDetails.amount,
       dobinventorydocument.code  AS goodsReceiptCode,
       orderDocument.id           AS glSubAccountid,
       orderDocument.code         AS glSubAccountCode,
       cobcurrency.code           AS currencyCode,
       cobcurrency.iso            AS currencyISO,
       null::json                 As glSubAccountName

FROM IObAccountingDocumentAccountingDetails accountingDetails
         join iobcostingaccountingdetails costingAccountingDetails
              ON costingAccountingDetails.id = accountingDetails.id
         INNER JOIN doborderdocument orderDocument ON orderDocument.id = costingAccountingDetails.glsubaccountid
         LEFT JOIN dobcosting costing ON accountingDetails.refinstanceid = costing.id
         INNER JOIN dobaccountingdocument ON dobaccountingdocument.id = costing.id
         INNER JOIN iobcostingdetails
                    on costing.id = iobcostingdetails.refinstanceid
         INNER JOIN dobinventorydocument
                    on dobinventorydocument.id = iobcostingdetails.goodsreceiptid
         INNER JOIN iobinventorycompany
                    on iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN cobenterprise company
                   ON iobinventorycompany.companyid = company.id
         LEFT JOIN cobcompany ON iobinventorycompany.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id
         LEFT JOIN hobglaccount account ON account.id = accountingDetails.glaccountid;



create or replace view IObCostingItemsGeneralModel AS
select IObCostingItems.id,
       dobaccountingdocument.code                                     as userCode,
       IObCostingItems.refinstanceid,
       IObCostingItems.quantity,
       IObCostingItems.stocktype,
       IObCostingItems.tobeconsideredincostingper,
       concat(masterdata.code, ' - ', masterdata.name::json ->> 'en') as itemCodeName,
       concat(cobmeasure.code, ' - ', cobmeasure.name::json ->> 'en') as uomCodeName,
       cobmeasure.symbol::json ->> 'en'                               as uomSymbol,
       IObCostingItems.creationDate,
       IObCostingItems.creationInfo,
       IObCostingItems.modifiedDate,
       IObCostingItems.modificationInfo,
       IObCostingItems.unitPrice,
       IObCostingItems.unitLandedCost
from IObCostingItems
         left join masterdata
                   on IObCostingItems.itemid = masterdata.id and masterdata.objecttypecode = '1'
         left join cobmeasure on IObCostingItems.uomid = cobmeasure.id
         left join dobCosting
                   on IObCostingItems.refinstanceid = dobCosting.id
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobCosting.id;

