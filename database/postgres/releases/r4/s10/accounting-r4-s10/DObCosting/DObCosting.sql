ALTER TABLE dobGoodsReceiptAccountingDocument rename to DObCosting;
ALTER TABLE iobGoodsReceiptAccountingDocumentDetails rename to IObCostingDetails;
ALTER TABLE IObGoodsReceiptAccountingDocumentAccountingDetails rename to IObCostingAccountingDetails;
ALTER TABLE IObGoodsReceiptAccountingDocumentItems rename to IObCostingItems;


alter sequence dobgoodsreceiptaccountingdocument_id_seq rename to dobcosting_id_seq;
alter sequence iobgoodsreceiptaccountingdocumentitems_id_seq rename to iobcostingitems_id_seq;
alter sequence iobgoodsreceiptaccountingdocumentdetails_id_seq rename to iobcostingdetails_id_seq;
alter sequence iobgoodsreceiptaccountingdocumentaccountingdetails_id_seq rename to iobcostingaccountingdetails_id_seq;

ALTER TABLE IObCostingItems drop column itemActualCost;

ALTER TABLE IObCostingItems
    ADD COLUMN unitPrice       DOUBLE PRECISION;
ALTER TABLE IObCostingItems
    ADD COLUMN unitLandedCost       DOUBLE PRECISION;