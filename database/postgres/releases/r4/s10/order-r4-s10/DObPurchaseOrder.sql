
\ir 'Drop_Views.sql'
ALTER TABLE IObOrderTax
    ALTER COLUMN percentage TYPE numeric;


ALTER TABLE ioborderitem
    ALTER COLUMN quantity TYPE numeric,
    ALTER COLUMN price TYPE numeric;

-- -----------------------------------

ALTER TABLE ioborderlinedetailsquantities
    ALTER COLUMN quantity TYPE numeric;

ALTER TABLE ioborderlinedetails
    ALTER COLUMN quantityindn TYPE numeric;
