DROP VIEW IF EXISTS iobordertaxgeneralmodel;
DROP VIEW IF EXISTS ioborderitemgeneralmodel;

DROP VIEW IF EXISTS ioborderitemsummarygeneralmodel;
DROP VIEW IF EXISTS iobpurchaseorderitemsgeneralmodel;


DROP VIEW IF EXISTS purchasedItemsTotalFinalCost;
DROP VIEW IF EXISTS unrestrictedItemsSingleUnitFinalCost;

DROP VIEW IF EXISTS IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptReceivedItemsGeneralModel;
DROP VIEW IF EXISTS DObPurchaseOrderTotalAmountGeneralModel;
DROP VIEW IF EXISTS IObOrderLineDetailsSummaryGeneralModel;

DROP VIEW IF EXISTS ioborderlinedetailsgeneralmodel;
DROP VIEW IF EXISTS ioborderlinedetailsquantitiesgeneralmodel;

DROP VIEW IF EXISTS IObPurchaseOrderItemsQuantitiesGeneralModel;
