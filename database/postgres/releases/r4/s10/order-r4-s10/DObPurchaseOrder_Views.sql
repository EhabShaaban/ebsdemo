DROP FUNCTION getordertotalamountbeforetaxes(bigint);
CREATE FUNCTION getOrderTotalAmountBeforeTaxes(orderId bigint)
    RETURNS numeric AS
$$
BEGIN
    RETURN (
        (SELECT SUM(itemTotalAmount)
         FROM IObOrderItemGeneralModel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;


DROP FUNCTION IF EXISTS getOrderTotalTaxes(orderId bigint);
CREATE FUNCTION getOrderTotalTaxes(orderId bigint)
    RETURNS numeric AS
$$
BEGIN
    return CASE
               WHEN
                   (SELECT SUM(amount)
                    FROM iobordertaxgeneralmodel
                    WHERE refinstanceid = orderId) IS NULL THEN 0
               ELSE (SELECT SUM(amount)
                     FROM iobordertaxgeneralmodel
                     WHERE refinstanceid = orderId) END;
END;
$$
    LANGUAGE PLPGSQL;


DROP VIEW IF EXISTS IObOrderItemGeneralModel;
CREATE VIEW IObOrderItemGeneralModel AS
SELECT ioborderitem.id,
       doborderdocument.code                      AS orderCode,
       ioborderitem.refinstanceid,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.objecttypecode,
       mobitemgeneralmodel.code                   AS itemCode,
       mobitemgeneralmodel.name                   AS itemName,
       mobitemgeneralmodel.typecode               AS itemTypeCode,
       mobitemgeneralmodel.typename               AS itemTypeName,
       cobmeasure.code                            AS orderUnitCode,
       cobmeasure.name                            AS orderUnitName,
       cobmeasure.symbol                          AS orderUnitSymbol,
       ioborderitem.quantity,
       ioborderitem.price,
       ioborderitem.quantity * ioborderitem.price AS itemTotalAmount
FROM doborderdocument
         JOIN ioborderitem ON doborderdocument.id = ioborderitem.refinstanceid
         JOIN mobitemgeneralmodel ON ioborderitem.itemid = mobitemgeneralmodel.id
         JOIN cobmeasure ON ioborderitem.orderunitid = cobmeasure.id;



DROP VIEW IF EXISTS IObOrderTaxGeneralModel;
CREATE VIEW IObOrderTaxGeneralModel AS
SELECT iobordertax.id,
       iobordertax.creationDate,
       iobordertax.modifiedDate,
       iobordertax.creationInfo,
       iobordertax.modificationInfo,
       iobordertax.refinstanceid,
       doborderdocument.objecttypecode,
       doborderdocument.code                       AS orderCode,
       iobordertax.code,
       iobordertax.name,
       iobordertax.percentage,
       (SELECT SUM(itemTotalAmount) * (iobordertax.percentage / 100)
        FROM IObOrderItemGeneralModel
        WHERE refinstanceid = doborderdocument.id) AS amount
FROM iobordertax
         LEFT JOIN doborderdocument ON iobordertax.refinstanceid = doborderdocument.id;


DROP VIEW IF EXISTS IObOrderItemSummaryGeneralModel;
CREATE VIEW IObOrderItemSummaryGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.objecttypecode,
       doborderdocument.code                               AS orderCode,
       getOrderTotalAmountBeforeTaxes(doborderdocument.id) AS totalAmountBeforeTaxes,
       getOrderTotalTaxes(doborderdocument.id)             AS totalTaxes,
       getOrderTotalAmountBeforeTaxes(doborderdocument.id)
           + getOrderTotalTaxes(doborderdocument.id)       AS totalAmountAfterTaxes
FROM doborderdocument;


CREATE VIEW IObOrderLineDetailsQuantitiesGeneralModel AS
SELECT Quantities.id,
       Quantities.refinstanceid,
       Quantities.quantity,
       Quantities.price,
       Quantities.discountpercentage,
       round(case
                 when ioborderlinedetails.unitOfMeasureId = Quantities.orderunitid
                     then Quantities.quantity
                 else
                     (Quantities.quantity * (IObAlternativeUoM.conversionFactor)) end :: numeric,
             3)                                                 AS quantityinbase,
       (SELECT CObMeasure.symbol
        FROM CObMeasure
        WHERE Quantities.orderunitid = CObMeasure.id)           AS orderunitsymbol,
       (SELECT cobmeasure_1.code
        FROM cobmeasure cobmeasure_1
        WHERE (quantities.orderunitid = cobmeasure_1.id))       AS orderunitcode,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM ItemVendorRecordGeneralModel
                 JOIN DObPurchaseOrderGeneralModel
                      ON ItemVendorRecordGeneralModel.vendorid =
                         DObPurchaseOrderGeneralModel.vendorId
                          AND ItemVendorRecordGeneralModel.purchaseunitid =
                              DObPurchaseOrderGeneralModel.purchaseunitid
                 JOIN IObOrderLineDetails
                      ON ItemVendorRecordGeneralModel.itemid = IObOrderLineDetails.itemid
                 JOIN IObOrderLineDetailsQuantities
                      ON ItemVendorRecordGeneralModel.measureid =
                         IObOrderLineDetailsQuantities.orderunitid
        WHERE DObPurchaseOrderGeneralModel.id = IObOrderLineDetails.refinstanceid
          AND IObOrderLineDetails.id = IObOrderLineDetailsQuantities.refinstanceid
          AND IObOrderLineDetailsQuantities.id = Quantities.id) AS itemCodeAtVendor
FROM IObOrderLineDetailsQuantities AS Quantities
         LEFT JOIN IObOrderLineDetails
                   ON IObOrderLineDetails.id = Quantities.refinstanceid
         LEFT JOIN IObAlternativeUoM
                   ON IObOrderLineDetails.itemid = IObAlternativeUoM.refinstanceid
                       AND IObAlternativeUoM.alternativeunitofmeasureid = Quantities.orderunitid
         LEFT JOIN CObMeasure ON IObOrderLineDetails.unitofmeasureid = CObMeasure.id;


CREATE VIEW IObOrderLineDetailsGeneralModel AS
SELECT ioborderlinedetails.id,
       ioborderlinedetails.creationInfo,
       ioborderlinedetails.creationDate,
       ioborderlinedetails.modificationInfo,
       ioborderlinedetails.modifiedDate,
       doborderdocument.code                                           AS ordercode,
       mobitemgeneralmodel.name,
       mobitemgeneralmodel.code                                        AS itemcode,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId)     as unitsymbol,
       (select SUM(IObOrderLineDetailsQuantitiesGeneralModel.quantityinbase) as quantity
        from IObOrderLineDetailsQuantitiesGeneralModel
        where ioborderlinedetails.id =
              IObOrderLineDetailsQuantitiesGeneralModel.refinstanceid) as itemQuantity,
       ioborderlinedetails.quantityindn,
       mobitemgeneralmodel.itemgroupname
FROM ioborderlinedetails
         LEFT JOIN doborderdocument ON doborderdocument.id = ioborderlinedetails.refinstanceid AND
                                       doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN mobitemgeneralmodel ON mobitemgeneralmodel.id = ioborderlinedetails.itemid;


CREATE VIEW IObOrderLineDetailsSummaryGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       businessUnit.code                                             AS businessUnitCode,
       businessUnit.name                                             AS businessUnitName,
       sum(ioborderlinedetailsquantitiesgeneralmodel.price *
           ioborderlinedetailsquantitiesgeneralmodel.quantityinbase) AS TotalAmount,
       sum((ioborderlinedetailsquantitiesgeneralmodel.discountpercentage *
            ioborderlinedetailsquantitiesgeneralmodel.quantityinbase *
            ioborderlinedetailsquantitiesgeneralmodel.price) / 100)  AS TotalDiscount
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
         LEFT JOIN cobenterprise businessUnit on iobordercompany.businessunitid = businessUnit.id
         LEFT JOIN ioborderlinedetailsgeneralmodel
                   ON doborderdocument.code = ioborderlinedetailsgeneralmodel.ordercode
         LEFT JOIN ioborderlinedetailsquantitiesgeneralmodel
                   ON ioborderlinedetailsgeneralmodel.id =
                      ioborderlinedetailsquantitiesgeneralmodel.refinstanceid
GROUP BY doborderdocument.id, businessUnit.id;


CREATE VIEW DObPurchaseOrderTotalAmountGeneralModel AS
SELECT dobpurchaseordergeneralmodel.*,
       (CASE
            WHEN ioborderlinedetailssummarygeneralmodel.totalAmount IS NULL THEN 0
            ELSE ioborderlinedetailssummarygeneralmodel.totalAmount END -
        CASE
            WHEN ioborderlinedetailssummarygeneralmodel.totaldiscount IS NULL THEN 0
            ELSE ioborderlinedetailssummarygeneralmodel.totaldiscount END) AS TotalAmount
FROM ioborderlinedetailssummarygeneralmodel
         JOIN dobpurchaseordergeneralmodel
              ON ioborderlinedetailssummarygeneralmodel.id = dobpurchaseordergeneralmodel.id;



CREATE VIEW IObGoodsReceiptReceivedItemsGeneralModel AS
SELECT row_number() over ()                                                        AS id,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.id                            AS grItemId,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid                 AS goodsreceiptinstanceid,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationdate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modifieddate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationinfo,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                                   as goodsReceiptCode,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.differencereason,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                                    as itemcode,
       mobitemgeneralmodel.name                                                    as itemname,
       mobitemgeneralmodel.batchmanagementrequired                                 as isBatchManaged,
       cobmeasure.symbol                                                           as baseunit,
       cobmeasure.code                                                             as baseunitcode,
       (SELECT quantityInDn
        FROM IObOrderLineDetailsGeneralModel
                 left join iobgoodsreceiptpurchaseorderdatageneralmodel
                           ON iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseOrderCode =
                              IObOrderLineDetailsGeneralModel.orderCode
        WHERE iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
          AND IObOrderLineDetailsGeneralModel.itemCode = mobitemgeneralmodel.code) AS quantityInDn,
       null                                                                        AS returnedQuantityBase
FROM IObGoodsReceiptPurchaseOrderReceivedItemsData
         JOIN DObGoodsReceiptPurchaseOrder
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceId =
                 DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument
                   on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_PO'
         left join mobitemgeneralmodel
                   on IObGoodsReceiptPurchaseOrderReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
UNION ALL
SELECT row_number() over () +
       (SELECT COUNT(*) FROM IObGoodsReceiptPurchaseOrderReceivedItemsData) AS id,
       IObGoodsReceiptSalesReturnReceivedItemsData.id                       AS grItemId,
       IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid            AS goodsreceiptinstanceid,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationdate,
       IObGoodsReceiptSalesReturnReceivedItemsData.modifieddate,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationinfo,
       IObGoodsReceiptSalesReturnReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                            as goodsReceiptCode,
       IObGoodsReceiptSalesReturnReceivedItemsData.differencereason,
       IObGoodsReceiptSalesReturnReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                             as itemcode,
       mobitemgeneralmodel.name                                             as itemname,
       mobitemgeneralmodel.batchmanagementrequired                          as isBatchManaged,
       cobmeasure.symbol                                                    as baseunit,
       cobmeasure.code                                                      as baseunitcode,
       null                                                                 AS quantityInDn,
       (SELECT SUM(IObSalesReturnOrderItemGeneralModel.returnQuantityInBase)
        FROM IObSalesReturnOrderItemGeneralModel
        WHERE IObSalesReturnOrderItemGeneralModel.refinstanceid =
              iobgoodsreceiptsalesreturndata.salesreturnid
          AND IObSalesReturnOrderItemGeneralModel.itemId =
              mobitemgeneralmodel.id)                                       AS returnedQuantityBase
FROM IObGoodsReceiptSalesReturnReceivedItemsData
         JOIN dobgoodsreceiptsalesreturn
              ON IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceId =
                 dobgoodsreceiptsalesreturn.id
         left JOIN dobinventorydocument
                   on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_SR'
         left JOIN mobitemgeneralmodel
                   on IObGoodsReceiptSalesReturnReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
         left JOIN iobgoodsreceiptsalesreturndata
                   ON iobgoodsreceiptsalesreturndata.refinstanceid = dobgoodsreceiptsalesreturn.id;

CREATE VIEW IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderItemQuantities.id,
       IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype,
       IObGoodsReceiptPurchaseOrderItemQuantities.notes,
       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedCost,
       IObGoodsReceiptPurchaseOrderItemQuantities.actualCost,
       cobmeasure.symbol                                        AS unitofentry,
       cobmeasure.code                                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe)
                     * iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                                 AS receivedqtyWithDefectsbase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                                 AS baseunitfactor,
       mobitemgeneralmodel.code                                 as itemCode,
       mobitemgeneralmodel.name                                 as itemName,
       dobinventorydocument.code                                AS goodsreceiptcode,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                                        AS itemCodeAtVendor
FROM IObGoodsReceiptPurchaseOrderItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;



CREATE VIEW IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptSalesReturnItemQuantities.id,
       IObGoodsReceiptSalesReturnItemQuantities.refinstanceid AS ordinaryitemid,
       dobinventorydocument.code                              AS goodsreceiptcode,
       IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe,
       IObGoodsReceiptSalesReturnItemQuantities.stocktype,
       IObGoodsReceiptSalesReturnItemQuantities.notes,
       cobmeasure.symbol                                      AS unitofentry,
       cobmeasure.code                                        AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
                     THEN IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                               AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                               AS baseunitfactor,
       masterdata.code                                        as itemCode,
       masterdata.name                                        as itemName,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE masterdata.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                      AS baseUnitSymbol
FROM IObGoodsReceiptSalesReturnItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptSalesReturnItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptSalesReturnReceivedItemsData
              ON IObGoodsReceiptSalesReturnReceivedItemsData.id =
                 IObGoodsReceiptSalesReturnItemQuantities.refinstanceid
         JOIN masterdata
              ON IObGoodsReceiptSalesReturnReceivedItemsData.itemid = masterdata.id
         JOIN DObGoodsReceiptSalesReturn ON DObGoodsReceiptSalesReturn.id =
                                            IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON masterdata.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptSalesReturnItemQuantities.unitofentryid;


CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS
SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.stocktype,
       iobgoodsreceiptitembatches.notes,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE iobgoodsreceiptitembatches.receivedqtyuoe
                 END ::numeric,
             3)                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobinventorydocument.code                as goodsReceiptCode,
       (SELECT baseUnit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code =
              iobgoodsreceiptreceiveditemsgeneralmodel.itemCode
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                        AS itemCodeAtVendor
FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;



CREATE VIEW unrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId          as itemId,
       vendorinvoiceitemweight.landedCostState as landedCostState,
       vendorinvoiceitemweight.landedCostCode  as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal            as itemTotalLandedCost,
       (itemweight * landedcosttotal) /
       GRUnrestrectedItem.receivedqtyuoe       as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
        (itemunitprice * currencyprice))       as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode =
                       vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and
                       vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode;


CREATE VIEW purchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode =
                      unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode =
                           GRDamagedItem.unitofentrycode;

CREATE VIEW IObPurchaseOrderItemsQuantitiesGeneralModel AS
SELECT ioborderlinedetailsQuantities.id,
       ioborderlinedetailsQuantities.refInstanceId,
       ioborderlinedetailsQuantities.quantity,
       ioborderlinedetailsQuantities.orderunitid,
       ioborderlinedetailsQuantities.orderunitsymbol

FROM ioborderlinedetailsQuantities;

CREATE VIEW IObPurchaseOrderItemsGeneralModel AS
SELECT ioborderlinedetails.id,
       doborderdocument.id                                         as purchaseOrderId, --purchaseID
       doborderdocument.code                                       as ordercode,
       (Select masterdata.name
        from masterdata
        where masterdata.id = ioborderlinedetails.itemId)          AS itemname,
       mobitemgeneralmodel.code                                    as itemcode,
       mobitemgeneralmodel.basicUnitOfMeasure                      as basicUnitOfMeasureId,
       mobitemgeneralmodel.basicUnitOfMeasurecode                  as basicUnitOfMeasureCode,
       mobitemgeneralmodel.basicUnitOfMeasuresymbol                as basicUnitOfMeasureSymbol,
       mobitemgeneralmodel.batchmanagementrequired,
       itemvendorrecord.itemvendorcode,
       itemvendorrecord.itemId,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId) as unitsymbol,
       (select SUM(ioborderlinedetailsquantities.quantity) as quantity
        from ioborderlinedetailsquantities
        where ioborderlinedetails.id =
              ioborderlinedetailsquantities.refinstanceid)         as itemQuantity,
       ioborderlinedetails.quantityindn
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON dobpurchaseorder.id = iobpurchaseorderfulfillervendor.refinstanceid
         join ioborderlinedetails on doborderdocument.id = ioborderlinedetails.refinstanceid
         join mobitemgeneralmodel on ioborderlinedetails.itemid = mobitemgeneralmodel.id
         left join itemvendorrecord
                   on ioborderlinedetails.itemid = itemvendorrecord.itemid
                       ANd iobpurchaseorderfulfillervendor.vendorid = itemvendorrecord.vendorid
                       And ioborderlinedetails.unitofmeasureid = itemvendorrecord.uomid
where MObItemGeneralModel.basicunitofmeasure is not null
  and mobitemgeneralmodel.batchmanagementrequired is not null;
