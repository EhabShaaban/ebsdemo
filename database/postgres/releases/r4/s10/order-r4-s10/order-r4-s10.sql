\ir 'DObPurchaseOrder.sql'
\ir 'DObPurchaseOrder_Views.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;