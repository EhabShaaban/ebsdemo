drop view if exists IObInitialStockUploadItemQuantityGeneralModel;

CREATE VIEW IObInitialStockUploadItemQuantityGeneralModel AS
SELECT row_number()
       over (order by dobinventorydocument.code, iobinitialstockuploaditem.itemid, iobinitialstockuploaditem.stocktype) as id,
       dobinventorydocument.code                                                 AS userCode,
       iobinitialstockuploaditem.itemid                                          as itemId,
       item.code                                                                 AS itemcode,
       iobinitialstockuploaditem.quantity                                        as quantity,
       iobinitialstockuploaditem.stocktype                                       as stockType,
       uom.id                                                                    AS unitOfMeasureId,
       uom.code                                                                  AS unitofmeasurecode,
       businessunit.id                                                           as purchaseunitid,
       businessunit.code                                                         AS purchaseunitcode,
       goodsissuecompany.id                                                      as companyId,
       goodsissuecompany.code                                                    as companyCode,
       goodsissuestorehouse.id                                                   as storehouseId,
       goodsissuestorehouse.code                                                 as storehouseCode,
       goodsissueplant.id                                                        as plantId,
       goodsissueplant.code                                                      as plantCode,
       dobinventorydocument.creationdate,
       dobinventorydocument.creationinfo,
       dobinventorydocument.modifieddate,
       dobinventorydocument.modificationinfo
FROM iobinitialstockuploaditem
         JOIN dobinventorydocument ON dobinventorydocument.id = iobinitialstockuploaditem.refinstanceid
         JOIN iobinventorycompany ON dobinventorydocument.id = iobinventorycompany.refinstanceid
         JOIN cobenterprise businessunit ON businessunit.id = dobinventorydocument.purchaseunitid
         JOIN masterdata item ON iobinitialstockuploaditem.itemid = item.id
         JOIN cobmeasure uom ON iobinitialstockuploaditem.unitofmeasureid = uom.id
         JOIN cobenterprise goodsissuecompany ON iobinventorycompany.companyid = goodsissuecompany.id
         JOIN cobenterprise goodsissuestorehouse ON iobinventorycompany.storehouseid = goodsissuestorehouse.id
         JOIN cobenterprise goodsissueplant ON iobinventorycompany.plantid = goodsissueplant.id
WHERE dobinventorydocument.inventorydocumenttype::text = 'ISU'::text;
