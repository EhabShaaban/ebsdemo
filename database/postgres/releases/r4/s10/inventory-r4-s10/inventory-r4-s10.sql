\ir 'GoodsReceipt/GoodsReceipt_Views.sql'
\ir 'GoodsIssue/GoodsIssue_Views.sql'
\ir 'InitialStockUpload/DObInitialStockUpload_Views.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
