CREATE OR REPLACE VIEW IObGoodsReceiptPurchaseOrderDataGeneralModel AS
SELECT iobgoodsreceiptpurchaseorderdata.id,
       iobgoodsreceiptpurchaseorderdata.refinstanceid,
       iobgoodsreceiptpurchaseorderdata.deliverynote,
       iobgoodsreceiptpurchaseorderdata.comments,
       dobinventorydocument.code AS goodsreceiptcode,
       podata.purchaseordercode,
       vendordata.vendorcode,
       iobgoodsreceiptpurchaseorderdata.vendorname,
       cobenterprise.code        AS purchaseunitcode,
       cobenterprise.name        AS purchaseunitname,
       iobgoodsreceiptpurchaseorderdata.purchaseresponsiblename,
       accountingDocument.code   as costingDocumentCode
FROM iobgoodsreceiptpurchaseorderdata
         LEFT JOIN dobgoodsreceiptpurchaseorder
                   ON iobgoodsreceiptpurchaseorderdata.refinstanceid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN dobinventorydocument ON dobgoodsreceiptpurchaseorder.id = dobinventorydocument.id
         LEFT JOIN cobenterprise ON cobenterprise.objecttypecode::text = '5'::text AND
                                    dobinventorydocument.purchaseunitid = cobenterprise.id
         LEFT JOIN (SELECT masterdata.id,
                           masterdata.code AS vendorcode,
                           masterdata.name AS vendorname
                    FROM masterdata
                    WHERE masterdata.objecttypecode::text = '2'::text) vendordata
                   ON iobgoodsreceiptpurchaseorderdata.vendorid = vendordata.id
         LEFT JOIN (SELECT doborderdocument.id,
                           doborderdocument.code AS purchaseordercode
                    FROM doborderdocument
                    WHERE doborderdocument.objecttypecode::text ~~ '%_PO'::text) podata
                   ON iobgoodsreceiptpurchaseorderdata.purchaseorderid = podata.id
         LEFT JOIN iobCostingdetails costingDetails
                   on costingDetails.goodsreceiptid = dobinventorydocument.id
         LEFT JOIN dobcosting costing
                   ON costingDetails.refinstanceid = costing.id
         LEFT JOIN dobaccountingdocument accountingDocument
                   ON accountingDocument.id = costing.id;


DROP VIEW IF EXISTS iobgoodsreceiptitemquantitiesgeneralmodel;
DROP VIEW IF EXISTS iobgoodsreceiptitemquantitygeneralmodel;
create view iobgoodsreceiptitemquantitygeneralmodel(id, userCode, itemid, itemcode, quantity, stocktype, unitofmeasureid, unitofmeasurecode, purchaseunitid, purchaseunitcode, companyid, companycode, storehouseid, storehousecode, plantid, plantcode, goodsreceipttype, creationdate, creationinfo, modifieddate, modificationinfo) as
SELECT concat(goodsreceipt.id, itemquantity.id, itemdata.id) AS id,
       inventorydocument.code                                AS userCode,
       itemdata.id                                           AS itemid,
       itemdata.code                                         AS itemcode,
       itemquantity.receivedqtyuoe                           AS quantity,
       itemquantity.stocktype,
       itemunitofmeasure.id                                  AS unitofmeasureid,
       itemunitofmeasure.code                                AS unitofmeasurecode,
       goodsreceiptpurchaseunit.id                           AS purchaseunitid,
       goodsreceiptpurchaseunit.code                         AS purchaseunitcode,
       goodsreceiptcompany.id                                AS companyid,
       goodsreceiptcompany.code                              AS companycode,
       goodsreceiptstorehouse.id                             AS storehouseid,
       goodsreceiptstorehouse.code                           AS storehousecode,
       goodsreceiptplant.id                                  AS plantid,
       goodsreceiptplant.code                                AS plantcode,
       'PO'::text                                            AS goodsreceipttype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsreceiptpurchaseorder goodsreceipt
         JOIN dobinventorydocument inventorydocument ON goodsreceipt.id = inventorydocument.id
         JOIN iobgoodsreceiptpurchaseorderreceiveditemsdata goodsreceiptitem
              ON goodsreceipt.id = goodsreceiptitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsreceiptitem.itemid
         JOIN iobgoodsreceiptpurchaseorderitemquantities itemquantity
              ON goodsreceiptitem.id = itemquantity.refinstanceid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = itemquantity.unitofentryid
         JOIN cobenterprise goodsreceiptpurchaseunit ON inventorydocument.purchaseunitid = goodsreceiptpurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsreceiptcompany ON inventorycompany.companyid = goodsreceiptcompany.id
         JOIN cobenterprise goodsreceiptstorehouse ON inventorycompany.storehouseid = goodsreceiptstorehouse.id
         JOIN cobenterprise goodsreceiptplant ON inventorycompany.plantid = goodsreceiptplant.id
UNION ALL
SELECT concat(goodsreceipt.id, itemquantity.id, itemdata.id) AS id,
       inventorydocument.code                                AS userCode,
       itemdata.id                                           AS itemid,
       itemdata.code                                         AS itemcode,
       itemquantity.receivedqtyuoe                           AS quantity,
       itemquantity.stocktype,
       itemunitofmeasure.id                                  AS unitofmeasureid,
       itemunitofmeasure.code                                AS unitofmeasurecode,
       goodsreceiptpurchaseunit.id                           AS purchaseunitid,
       goodsreceiptpurchaseunit.code                         AS purchaseunitcode,
       goodsreceiptcompany.id                                AS companyid,
       goodsreceiptcompany.code                              AS companycode,
       goodsreceiptstorehouse.id                             AS storehouseid,
       goodsreceiptstorehouse.code                           AS storehousecode,
       goodsreceiptplant.id                                  AS plantid,
       goodsreceiptplant.code                                AS plantcode,
       'SR'::text                                            AS goodsreceipttype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsreceiptsalesreturn goodsreceipt
         JOIN dobinventorydocument inventorydocument ON goodsreceipt.id = inventorydocument.id
         JOIN iobgoodsreceiptsalesreturnreceiveditemsdata goodsreceiptitem
              ON goodsreceipt.id = goodsreceiptitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsreceiptitem.itemid
         JOIN iobgoodsreceiptsalesreturnitemquantities itemquantity ON goodsreceiptitem.id = itemquantity.refinstanceid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = itemquantity.unitofentryid
         JOIN cobenterprise goodsreceiptpurchaseunit ON inventorydocument.purchaseunitid = goodsreceiptpurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsreceiptcompany ON inventorycompany.companyid = goodsreceiptcompany.id
         JOIN cobenterprise goodsreceiptstorehouse ON inventorycompany.storehouseid = goodsreceiptstorehouse.id
         JOIN cobenterprise goodsreceiptplant ON inventorycompany.plantid = goodsreceiptplant.id;
