drop view if exists iobgoodsissueitemquantitiesgeneralmodel;
drop view if exists iobgoodsissueitemquantitygeneralmodel;
create view iobgoodsissueitemquantitygeneralmodel(id, userCode, itemid, itemcode, quantity, unitofmeasureid, unitofmeasurecode, purchaseunitid, purchaseunitcode, companyid, companycode, storehouseid, storehousecode, plantid, plantcode, goodsissuetype, creationdate, creationinfo, modifieddate, modificationinfo) as
SELECT concat(goodsissue.id, goodsissueitem.unitofmeasureid, itemdata.id) AS id,
       inventorydocument.code                                             AS userCode,
       itemdata.id                                                        AS itemid,
       itemdata.code                                                      AS itemcode,
       goodsissueitem.quantity,
       itemunitofmeasure.id                                               AS unitofmeasureid,
       itemunitofmeasure.code                                             AS unitofmeasurecode,
       goodsissuepurchaseunit.id                                          AS purchaseunitid,
       goodsissuepurchaseunit.code                                        AS purchaseunitcode,
       goodsissuecompany.id                                               AS companyid,
       goodsissuecompany.code                                             AS companycode,
       goodsissuestorehouse.id                                            AS storehouseid,
       goodsissuestorehouse.code                                          AS storehousecode,
       goodsissueplant.id                                                 AS plantid,
       goodsissueplant.code                                               AS plantcode,
       'SI'::text                                                         AS goodsissuetype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsissuesalesinvoice goodsissue
         JOIN dobinventorydocument inventorydocument ON goodsissue.id = inventorydocument.id
         JOIN iobgoodsissuesalesinvoiceitem goodsissueitem ON goodsissue.id = goodsissueitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsissueitem.itemid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = goodsissueitem.unitofmeasureid
         JOIN cobenterprise goodsissuepurchaseunit ON inventorydocument.purchaseunitid = goodsissuepurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsissuecompany ON inventorycompany.companyid = goodsissuecompany.id
         JOIN cobenterprise goodsissuestorehouse ON inventorycompany.storehouseid = goodsissuestorehouse.id
         JOIN cobenterprise goodsissueplant ON inventorycompany.plantid = goodsissueplant.id
UNION ALL
SELECT concat(goodsissue.id, goodsissueitem.unitofmeasureid, itemdata.id) AS id,
       inventorydocument.code                                             AS goodsissuecode,
       itemdata.id                                                        AS itemid,
       itemdata.code                                                      AS itemcode,
       goodsissueitem.quantity,
       itemunitofmeasure.id                                               AS unitofmeasureid,
       itemunitofmeasure.code                                             AS unitofmeasurecode,
       goodsissuepurchaseunit.id                                          AS purchaseunitid,
       goodsissuepurchaseunit.code                                        AS purchaseunitcode,
       goodsissuecompany.id                                               AS companyid,
       goodsissuecompany.code                                             AS companycode,
       goodsissuestorehouse.id                                            AS storehouseid,
       goodsissuestorehouse.code                                          AS storehousecode,
       goodsissueplant.id                                                 AS plantid,
       goodsissueplant.code                                               AS plantcode,
       'SO'::text                                                         AS goodsissuetype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsissuesalesorder goodsissue
         JOIN dobinventorydocument inventorydocument ON goodsissue.id = inventorydocument.id
         JOIN iobgoodsissuesalesorderitem goodsissueitem ON goodsissue.id = goodsissueitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsissueitem.itemid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = goodsissueitem.unitofmeasureid
         JOIN cobenterprise goodsissuepurchaseunit ON inventorydocument.purchaseunitid = goodsissuepurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsissuecompany ON inventorycompany.companyid = goodsissuecompany.id
         JOIN cobenterprise goodsissuestorehouse ON inventorycompany.storehouseid = goodsissuestorehouse.id
         JOIN cobenterprise goodsissueplant ON inventorycompany.plantid = goodsissueplant.id;
