\ir 'access-rights-r4-s10/access-rights-r4-s10.sql'
\ir 'order-r4-s10/order-r4-s10.sql'
\ir 'accounting-r4-s10/accounting-r4-s10.sql'
\ir 'accounting-r4-s10/DObPaymentRequest/data-migration.sql'
\ir 'inventory-r4-s10/inventory-r4-s10.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
