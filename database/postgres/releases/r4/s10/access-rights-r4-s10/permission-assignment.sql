delete from permissionassignment where roleid=(select id from role where roleexpression = 'GRActivateOwner_Offset_MadinaMain');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GRActivateOwner_Offset_Mariotya'),
        (select id from permission where permissionexpression = 'GoodsReceipt:Activate'),
        '2021-05-09 11:54:04+00',
        (select id from authorizationcondition where condition = '([purchaseUnitName=''Offset''] && [storehouseCode=''0001''])'
            AND objectname = 'GoodsReceipt')
               );
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GIActivateOwner_Offset_Mariotya'),
        (select id from permission where permissionexpression = 'GoodsIssue:Activate'),
        '2021-05-12 11:22:04+00',
        (select id from authorizationcondition where condition = '([purchaseUnitName=''Offset''] && [storehouseCode=''0001''])'
                                                 AND objectname = 'GoodsIssue')
       );

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GRActivateOwner_Offset_Mansourya'),
        (select id from permission where permissionexpression = 'GoodsReceipt:Activate'),
        '2021-05-09 11:54:04+00',
        (select id from authorizationcondition where condition = '([purchaseUnitName=''Offset''] && [storehouseCode=''0003''])'
            AND objectname = 'GoodsReceipt')
               );
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GIActivateOwner_Offset_Mansourya'),
        (select id from permission where permissionexpression = 'GoodsIssue:Activate'),
        '2021-05-12 11:22:04+00',
        (select id from authorizationcondition where condition = '([purchaseUnitName=''Offset''] && [storehouseCode=''0003''])'
                                                 AND objectname = 'GoodsIssue')
       );

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GRActivateOwner_Offset_SparePartsSalahsalem'),
        (select id from permission where permissionexpression = 'GoodsReceipt:Activate'),
        '2021-05-09 11:54:04+00',
        (select id from authorizationcondition where condition = '([purchaseUnitName=''Offset''] && [storehouseCode=''0005''])'
            AND objectname = 'GoodsReceipt')
       );
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GIActivateOwner_Offset_SparePartsSalahsalem'),
        (select id from permission where permissionexpression = 'GoodsIssue:Activate'),
        '2021-05-12 11:22:04+00',
        (select id from authorizationcondition where condition = '([purchaseUnitName=''Offset''] && [storehouseCode=''0005''])'
            AND objectname = 'GoodsIssue')
       );
