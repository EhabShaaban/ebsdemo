INSERT INTO authorizationcondition (objectname, condition) VALUES ('GoodsReceipt', '([purchaseUnitName=''Offset''] && [storehouseCode=''0003''])');
INSERT INTO authorizationcondition (objectname, condition) VALUES ('GoodsReceipt', '([purchaseUnitName=''Offset''] && [storehouseCode=''0005''])');

INSERT INTO authorizationcondition (objectname, condition) VALUES ('GoodsIssue', '([purchaseUnitName=''Offset''] && [storehouseCode=''0001''])');
INSERT INTO authorizationcondition (objectname, condition) VALUES ('GoodsIssue', '([purchaseUnitName=''Offset''] && [storehouseCode=''0003''])');
INSERT INTO authorizationcondition (objectname, condition) VALUES ('GoodsIssue', '([purchaseUnitName=''Offset''] && [storehouseCode=''0005''])');
