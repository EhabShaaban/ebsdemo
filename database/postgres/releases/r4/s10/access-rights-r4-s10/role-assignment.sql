INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mariotya'),
        (select id from role where roleexpression = 'GRActivateOwner_Offset_Mariotya'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mariotya'),
        (select id from role where roleexpression = 'GIActivateOwner_Offset_Mariotya'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mariotya'),
        (select id from role where roleexpression = 'GRViewer_Offset'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mariotya'),
        (select id from role where roleexpression = 'GIViewer_Offset'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mariotya'),
        (select id from role where roleexpression = 'GRTypeViewer'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mariotya'),
        (select id from role where roleexpression = 'GITypeViewer'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mansourya'),
        (select id from role where roleexpression = 'GRActivateOwner_Offset_Mansourya'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mansourya'),
        (select id from role where roleexpression = 'GIActivateOwner_Offset_Mansourya'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mansourya'),
        (select id from role where roleexpression = 'GRViewer_Offset'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mansourya'),
        (select id from role where roleexpression = 'GIViewer_Offset'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mansourya'),
        (select id from role where roleexpression = 'GRTypeViewer'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_Mansourya'),
        (select id from role where roleexpression = 'GITypeViewer'));


INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_SparePartsSalahsalem'),
        (select id from role where roleexpression = 'GRActivateOwner_Offset_SparePartsSalahsalem'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_SparePartsSalahsalem'),
        (select id from role where roleexpression = 'GIActivateOwner_Offset_SparePartsSalahsalem'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_SparePartsSalahsalem'),
        (select id from role where roleexpression = 'GRViewer_Offset'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_SparePartsSalahsalem'),
        (select id from role where roleexpression = 'GIViewer_Offset'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_SparePartsSalahsalem'),
        (select id from role where roleexpression = 'GRTypeViewer'));
INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_SparePartsSalahsalem'),
        (select id from role where roleexpression = 'GITypeViewer'));

