\ir 'access-rights-r4-s16/access-rights-r4-s16.sql'
\ir 'accounting-r4-s16/accounting-r4-s16.sql'
\ir 'order-r4-s16/order-r4-s16.sql'
\ir 'inventory-r4-s16/inventory-r4-s16.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
