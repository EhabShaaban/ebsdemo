\ir 'DObSettlement/DObSettlement_Views.sql'
\ir 'DObSalesInvoice/DObSalesInvoice_Views.sql'
\ir 'JournalEntry/DObJournalEntry_Views.sql'
\ir 'DObPayment/DObPayment_Views.sql'
\ir 'AccountingDocument/AccountingDocument_Views.sql'
\ir 'Settlement/DObSettlement_Views.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
