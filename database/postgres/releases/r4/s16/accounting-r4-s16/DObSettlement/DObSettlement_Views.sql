DROP FUNCTION if exists getJournalBalance(CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING,
    CHARACTER VARYING, CHARACTER VARYING, CHARACTER VARYING);


CREATE FUNCTION getJournalBalance(subLedger CHARACTER VARYING, businessUnitCode CHARACTER VARYING,
                                  companyCode CHARACTER VARYING, currencyCode CHARACTER VARYING,
                                  bankCode CHARACTER VARYING, bankAccountNo CHARACTER VARYING,
                                  treasuryCode CHARACTER VARYING) RETURNS NUMERIC
    LANGUAGE plpgsql
AS
$$
BEGIN
RETURN
    (
        CASE
            WHEN subLedger = 'Banks' THEN (SELECT balance
                                           FROM cobjournalbalance
                                           WHERE cobjournalbalance.code =
                                                 concat('BANK', businessUnitCode, companyCode,
                                                        currencyCode, bankCode,
                                                        bankAccountNo))
            WHEN subLedger = 'Treasuries' THEN (SELECT balance
                                                FROM cobjournalbalance
                                                WHERE cobjournalbalance.code =
                                                      concat('TREASURY', businessUnitCode, companyCode,
                                                             currencyCode, treasuryCode)) END
        );

END ;
$$;


CREATE OR REPLACE VIEW IObSettlementAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code                                           AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger                                             AS subLedger,
       glAccount.code                                                       AS glAccountCode,
       glAccount.name                                                       AS glAccountName,
       accountDetails.glaccountid                                           AS glAccountId,
       accountDetails.amount,
       glSubAccount.code                                                    AS glSubAccountCode,
       glSubAccount.subaccountid                                            AS glSubAccountId,
       glSubAccount.name                                                    AS glSubAccountName,
       getjournalbalance(accountDetails.subledger, businessUnit.code, company.code, cobcurrency.code,
                         cobbank.code,
                         iobcompanybankdetails.accountno, cobtreasury.code) AS balance
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObSettlement settlement on accountDetails.refinstanceid = settlement.id
         left join iobaccountingdocumentcompanydata
                   on settlement.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobaccountingdocumentactivationdetails
                   on settlement.id = iobaccountingdocumentactivationdetails.refinstanceid
         left join iobsettlementdetails on iobsettlementdetails.refinstanceid = settlement.id

         left join dobaccountingdocument on dobaccountingdocument.id = settlement.id
         left join cobenterprise company on company.id = iobaccountingdocumentcompanydata.companyid
         left join cobenterprise businessUnit on businessUnit.id = iobaccountingdocumentcompanydata.purchaseunitid
         left join cobcurrency on cobcurrency.id = iobsettlementdetails.currencyid
         left join hobglaccountgeneralmodel glAccount on glAccount.id = accountDetails.glaccountid
         left join SubAccountGeneralModel glSubAccount
                   on accountDetails.subledger = glSubAccount.ledger
                       and glSubAccount.subaccountid = (
                           case
                               when accountDetails.subledger = 'Banks'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentBankAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Local_Customer'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentCustomerAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Notes_Receivables'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentNotesReceivableAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'PO'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentOrderAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Treasuries'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentTreasuryAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Local_Vendors'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentVendorAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )
                               when accountDetails.subledger = 'Taxes'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentTaxAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )
                               end
                           )

         left join iobcompanybankdetails on iobcompanybankdetails.id = glSubAccount.subaccountid
         left join cobbank on cobbank.id = iobcompanybankdetails.bankid
         left join cobtreasury on cobtreasury.id = glSubAccount.subaccountid
         left join iobenterprisebasicdata companyData
                   on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid;

