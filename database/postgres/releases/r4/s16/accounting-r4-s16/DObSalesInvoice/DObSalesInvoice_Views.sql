DROP VIEW IF EXISTS IObSalesInvoiceItemGeneralModel;

Create OR REPLACE view DObSalesInvoiceJournalEntryPreparationGeneralModel as
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       customerbusinesspartner.customerid,
       postingdetails.duedate,
       iobaccountingdocumentcompanydata.companyid,
       iobaccountingdocumentcompanydata.purchaseunitid,
       dobaccountingdocument.id                        as documentreferenceid,
       businesspartner.currencyid                      as customercurrencyid,
       iobenterprisebasicdata.currencyid               as companycurrencyid,
       currecyPrice.currencyPrice,
       currecyPrice.exchangerateId,
       getInvoiceTotalAmountBeforeTaxes(dobaccountingdocument.code,
                                       'SalesInvoice') as amountBeforeTaxes,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           'SalesInvoice')             as amountAfterTaxes,
       iobcustomeraccountinginfo.chartofaccountid      as customeraccountId,
       customerAccount.leadger                         as customerLeadger,
       lobglobalglaccountconfig.accountid              as salesaccountid,
       postingdetails.fiscalperiodid                   as fiscalYearId
from dobaccountingdocument
         left join iobaccountingdocumentactivationdetails postingdetails
                   on postingdetails.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicebusinesspartner businesspartner
                   on businesspartner.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicecustomerbusinesspartner customerbusinesspartner
                   on customerbusinesspartner.id = businesspartner.id
         left join iobaccountingdocumentcompanydata
                   on iobaccountingdocumentcompanydata.refinstanceid = dobaccountingdocument.id
         left join iobenterprisebasicdata on iobaccountingdocumentcompanydata.companyid =
                                             iobenterprisebasicdata.refinstanceid
         left join getLatestCurrencyPrice(businesspartner.currencyid,
                                          iobenterprisebasicdata.currencyid) currecyPrice
                   on currecyPrice.firstcurrencyid = businesspartner.currencyid
                       and currecyPrice.secondcurrencyid = iobenterprisebasicdata.currencyid
         left join iobcustomeraccountinginfo
                   on iobcustomeraccountinginfo.refinstanceid = customerbusinesspartner.customerid
         left join leafglaccount customerAccount
                   on customerAccount.id = iobcustomeraccountinginfo.chartofaccountid
         left join lobglobalglaccountconfig on lobglobalglaccountconfig.code = 'SalesGLAccount'
where dobaccountingdocument.objecttypecode = 'SalesInvoice'
  and postingdetails.objecttypecode = 'SalesInvoice'
  and iobaccountingdocumentcompanydata.objecttypecode = 'SalesInvoice';

CREATE or REPLACE VIEW IObSalesInvoiceItemGeneralModel as
select SII.id,
       SI.id                                                                         as refinstanceid,
       SI.code                                                                       as code,
       m.code                                                                        as itemCode,
       m.name                                                                        as itemName,
       baseUnitOfMeasure.code                                                        as baseUnitCode,
       baseUnitOfMeasure.symbol                                                      as baseUnitSymbol,
       c.code                                                                        as orderUnitCode,
       c.symbol                                                                      as orderunitsymbol,
       SII.quantityinorderunit                                                       as quantityInOrderUnit,
       SII.returnedQuantity                                                          as returnedQuantity,
       SII.price                                                                     as orderUnitPrice,
       getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid,
                                 SII.itemid)::numeric                                AS conversionFactorToBase,
       trunc(cast(SII.price / getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid,
                                                        SII.itemid) as numeric), 10) as price,
       (SII.quantityinorderunit * SII.price)                                         as totalAmount,
       concat(m.code, ' - ', m.name :: json ->> 'en')                                as itemCodeName,
       c.name                                                                        as orderUnitName,
       concat(c.code, ' - ', c.name :: json ->> 'en')                                as orderUnitCodeName,
       concat(c.code, ' - ', c.symbol :: json ->> 'en')                              as orderUnitCodeSymbol,
       baseUnitOfMeasure.name                                                        as baseUnitOfMeasureName,
       concat(baseUnitOfMeasure.code, ' - ',
              baseUnitOfMeasure.name :: json ->> 'en')                               as baseUnitOfMeasureCodeName,
       concat(baseUnitOfMeasure.code, ' - ',
              baseUnitOfMeasure.symbol :: json ->> 'en')                             as baseUnitOfMeasureCodeSymbol,
       SII.creationDate,
       SII.creationInfo,
       SII.modifiedDate,
       SII.modificationInfo
from IObSalesInvoiceItem SII
         left join masterdata m on SII.itemId = m.id
         left join cobmeasure c on SII.orderunitofmeasureid = c.id
         left join mobitem on SII.itemid = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument SI on SII.refInstanceId = SI.id and SI.objecttypecode = 'SalesInvoice';
