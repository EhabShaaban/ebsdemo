CREATE OR REPLACE VIEW DObPaymentReferenceOrderGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       orderCompany.companyid,
       CASE
           When ioborderpaymenttermsdetails.currencyid is null then orderPaymentDetails.currencyid
           ELSE ioborderpaymenttermsdetails.currencyid END
from doborderdocument
         left join iobordercompany orderCompany on doborderdocument.id = orderCompany.refinstanceid
         left join ioborderpaymentdetails orderPaymentDetails on doborderdocument.id = orderPaymentDetails.refinstanceid
         left join ioborderpaymenttermsdetails on doborderdocument.id = ioborderpaymenttermsdetails.refinstanceid;

CREATE OR REPLACE VIEW DObPaymentReferenceVendorInvoiceGeneralModel AS
SELECT dobaccountingdocument.id,
       dobaccountingdocument.code,
       vendorInvoiceCompany.companyid,
       iobvendorinvoicepurchaseorder.currencyid
from dobaccountingdocument
         right join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         left join iobaccountingdocumentcompanydata vendorInvoiceCompany on dobaccountingdocument.id = vendorInvoiceCompany.refinstanceid
         left join iobvendorinvoicepurchaseorder on dobaccountingdocument.id = iobvendorinvoicepurchaseorder.refinstanceid;


CREATE OR REPLACE VIEW DObPaymentRequestSaveDetailsPreparationGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       paymentdetails.paymentForm,
       paymentdetails.duedocumenttype AS dueDocumentType,
       company.code                   AS companyCode,
       purchaseUnit.code              AS businessUnitCode
FROM DObPaymentRequest
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN cobenterprise company ON company.id = companyData.companyid
         LEFT JOIN cobenterprise purchaseUnit ON purchaseUnit.id = companyData.purchaseunitid
         LEFT JOIN iobpaymentrequestpaymentdetails paymentdetails
                   on dobpaymentrequest.id = paymentdetails.refinstanceid;





