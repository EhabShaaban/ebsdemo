CREATE or REPLACE FUNCTION getInvoiceTotalAmountBeforeTaxes(invoiceCode VARCHAR, invoiceType VARCHAR)
    RETURNS NUMERIC AS $$
BEGIN
RETURN
    CASE WHEN invoiceType = 'SalesInvoice' THEN
             COALESCE((SELECT
                           (SELECT SUM((invoiceItem.orderunitprice) * (invoiceItem.quantityinorderunit))::numeric AS totalAmount
                            FROM iobsalesinvoiceitemgeneralmodel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                       FROM dobsalesinvoice dobInvoice
                                LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobInvoice.id
                       WHERE dobaccountingdocument.objecttypecode = 'SalesInvoice' AND dobaccountingdocument.code = invoiceCode
                      ),0)
         ELSE
             COALESCE((SELECT
                           (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * invoiceItem.conversionfactortobase)) AS totalAmount
                            FROM IObVendorInvoiceItemsGeneralModel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                       FROM dobvendorInvoice dobvendorInvoice
                                LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
                       WHERE dobaccountingdocument.code = invoiceCode
                      ),0)
        END;
END; $$
LANGUAGE PLPGSQL;