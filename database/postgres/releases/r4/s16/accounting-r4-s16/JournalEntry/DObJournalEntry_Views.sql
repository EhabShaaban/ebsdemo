DROP VIEW IObJournalEntryItemGeneralModel;
CREATE OR REPLACE VIEW IObJournalEntryItemGeneralModel AS
select JEI.id,
       JEI.creationdate,
       JEI.creationinfo,
       JEI.modifieddate,
       JEI.modificationinfo,
       JEI.refinstanceid,
       dobjournalentry.code,
       dobjournalentry.duedate,
       dobjournalentry.objecttypecode AS referencetype,
       SUB_ACC.subledger              as subledger,
       hobglaccountgeneralmodel.code  AS accountcode,
       hobglaccountgeneralmodel.name  AS accountname,
       SUB_ACC.subaccountcode         as subaccountcode,
       SUB_ACC.subaccountname         as subaccountname,
       JEI.debit::varchar(255),
       JEI.credit::varchar(255),
       curren.code                    AS currencycode,
       curren.iso                     AS currencyname,
       curren2.code                   AS companycurrencycode,
       curren2.iso                    AS companycurrencyname,
       JEI.currencyprice::varchar(255),
       purchaseunit.code              AS purchaseunitcode,
       purchaseunit.name              AS purchaseunitname,
       purchaseunit.name ->> 'en'     AS purchaseunitnameen,
       JEI.exchangerateid             AS exchangerateid,
       cobexchangerate.code           AS exchangeratecode
from iobjournalentryitem JEI
         LEFT JOIN dobjournalentry ON JEI.refinstanceid = dobjournalentry.id AND
                                      (dobjournalentry.objecttypecode = 'VendorInvoice' OR
                                       dobjournalentry.objecttypecode = 'PaymentRequest' OR
                                       dobjournalentry.objecttypecode = 'SalesInvoice' OR
                                       dobjournalentry.objecttypecode = 'Collection' OR
                                       dobjournalentry.objecttypecode = 'NotesReceivable' OR
                                       dobjournalentry.objecttypecode = 'LandedCost' OR
                                       dobjournalentry.objecttypecode = 'Costing' OR
                                       dobjournalentry.objecttypecode = 'Settlement' OR
                                       dobjournalentry.objecttypecode = 'InitialBalanceUpload')
         left join hobglaccountgeneralmodel on (hobglaccountgeneralmodel.id = JEI.accountid)
         LEFT JOIN cobenterprise purchaseunit ON dobjournalentry.purchaseunitid = purchaseunit.id
         LEFT JOIN cobexchangerate ON cobexchangerate.id = JEI.exchangerateid
         left join cobcurrency curren on (JEI.currency = curren.id)
         left join cobcurrency curren2 on (JEI.companycurrency = curren2.id)
         left join IObJournalEntryItemSubAccounts SUB_ACC
                   on (SUB_ACC.id = JEI.id AND SUB_ACC.type = JEI.type);

CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode    AS documentReferenceType,
       DObJournalEntry.dueDate,
       company.code                      AS companyCode,
       company.name                      AS companyName,
       purchaseUnit.code                 AS purchaseUnitCode,
       purchaseUnit.name                 AS purchaseUnitName,
       purchaseUnit.name:: json ->> 'en' AS purchaseUnitNameEn,
       (SELECT dobaccountingdocument.code
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObVendorInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
          AND isVendorInvoice(dobaccountingdocument.objecttypecode)
        UNION
        SELECT dobaccountingdocument.code
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObPaymentRequestJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSalesInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcollectionJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON dobcollectionJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcollectionJournalEntry.id
          AND dobaccountingdocument.objecttypecode in ('C_SI', 'C_NR', 'C_SO', 'C_DN')
        UNION
        SELECT dobaccountingdocument.code
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobmonetarynotes ON dobNotesReceivableJournalEntry.documentreferenceid =
                                               dobmonetarynotes.id
                 LEFT JOIN dobaccountingdocument ON dobmonetarynotes.id = dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM doblandedcostjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON doblandedcostjournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = doblandedcostjournalentry.id
          and dobaccountingdocument.objecttypecode = 'LandedCost'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSettlementJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSettlementJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSettlementJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'Settlement'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObInitialBalanceUploadJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObInitialBalanceUploadJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObInitialBalanceUploadJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'IBU'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcostingjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON dobcostingjournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcostingjournalentry.id
          AND dobaccountingdocument.objecttypecode = 'Costing'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobnotesreceivablejournalentry
                 LEFT JOIN dobaccountingdocument
                           ON dobnotesreceivablejournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobnotesreceivablejournalentry.id
          AND dobaccountingdocument.objecttypecode like 'NOTES_RECEIVABLE%'
       )                                 AS documentReferenceCode,
       fiscalPeriod.name::json ->> 'en'  AS fiscalPeriod,
       fiscalPeriod.id                   AS fiscalPeriodId
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company
                   ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join CObFiscalPeriod fiscalPeriod
                   on DObJournalEntry.fiscalPeriodId = fiscalPeriod.id;
