INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'EmployeeViewer_Offset'),
        (select id from permission where permissionexpression = 'Employee:ReadAll'),
        '2021-07-28 11:54:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'Employee - [purchaseUnitName=''Offset'']'));