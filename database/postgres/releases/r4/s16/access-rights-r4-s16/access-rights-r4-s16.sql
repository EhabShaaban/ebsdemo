\ir 'authorization_conditions.sql'
\ir 'role.sql'
\ir 'role-assignment.sql'
\ir 'permission.sql'
\ir 'permission-assignment.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;