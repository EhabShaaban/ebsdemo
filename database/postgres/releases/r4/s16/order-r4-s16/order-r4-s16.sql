\ir 'DObSalesOrder/DObSalesOrder.sql'
\ir 'DObSalesOrder/DObSalesOrder_views.sql'
\ir 'DObOrder/DObOrder_Views.sql'
\ir 'DObSalesReturn/DObSalesReturn.sql'
\ir 'DObSalesReturn/DObSalesReturn_Views.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
