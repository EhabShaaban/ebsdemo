DROP VIEW IF EXISTS iobsalesreturnordertaxgeneralmodel;
DROP VIEW IF EXISTS salesreturnorderpdfdatageneralmodel;
DROP VIEW IF EXISTS salesreturnordertaxespdfdatageneralmodel;

ALTER TABLE iobsalesreturnordertax
    ALTER COLUMN percentage TYPE numeric;

\ir 'Drop_GoodsReceipt_Dependency.sql'
DROP VIEW IF EXISTS iobsalesreturnorderitemgeneralmodel;

ALTER TABLE iobsalesreturnorderitems
    ALTER COLUMN returnquantity TYPE numeric;
ALTER TABLE iobsalesreturnorderitems
    ALTER COLUMN salesprice TYPE numeric;
