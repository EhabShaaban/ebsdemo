CREATE VIEW IObGoodsReceiptReceivedItemsGeneralModel AS
SELECT row_number() over ()                                                        AS id,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.id                            AS grItemId,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid                 AS goodsreceiptinstanceid,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationdate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modifieddate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationinfo,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                                   as goodsReceiptCode,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.differencereason,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                                    as itemcode,
       mobitemgeneralmodel.name                                                    as itemname,
       mobitemgeneralmodel.batchmanagementrequired                                 as isBatchManaged,
       cobmeasure.symbol                                                           as baseunit,
       cobmeasure.code                                                             as baseunitcode,
       (SELECT quantityInDn
        FROM IObOrderLineDetailsGeneralModel
                 left join iobgoodsreceiptpurchaseorderdatageneralmodel
                           ON iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseOrderCode =
                              IObOrderLineDetailsGeneralModel.orderCode
        WHERE iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
          AND IObOrderLineDetailsGeneralModel.itemCode = mobitemgeneralmodel.code) AS quantityInDn,
       null                                                                        AS returnedQuantityBase
FROM IObGoodsReceiptPurchaseOrderReceivedItemsData
         JOIN DObGoodsReceiptPurchaseOrder
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceId =
                 DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument
                   on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_PO'
         left join mobitemgeneralmodel
                   on IObGoodsReceiptPurchaseOrderReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
UNION ALL
SELECT row_number() over () +
       (SELECT COUNT(*) FROM IObGoodsReceiptPurchaseOrderReceivedItemsData) AS id,
       IObGoodsReceiptSalesReturnReceivedItemsData.id                       AS grItemId,
       IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid            AS goodsreceiptinstanceid,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationdate,
       IObGoodsReceiptSalesReturnReceivedItemsData.modifieddate,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationinfo,
       IObGoodsReceiptSalesReturnReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                            as goodsReceiptCode,
       IObGoodsReceiptSalesReturnReceivedItemsData.differencereason,
       IObGoodsReceiptSalesReturnReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                             as itemcode,
       mobitemgeneralmodel.name                                             as itemname,
       mobitemgeneralmodel.batchmanagementrequired                          as isBatchManaged,
       cobmeasure.symbol                                                    as baseunit,
       cobmeasure.code                                                      as baseunitcode,
       null                                                                 AS quantityInDn,
       (SELECT SUM(IObSalesReturnOrderItemGeneralModel.returnQuantityInBase)
        FROM IObSalesReturnOrderItemGeneralModel
        WHERE IObSalesReturnOrderItemGeneralModel.refinstanceid =
              iobgoodsreceiptsalesreturndata.salesreturnid
          AND IObSalesReturnOrderItemGeneralModel.itemId =
              mobitemgeneralmodel.id)                                       AS returnedQuantityBase
FROM IObGoodsReceiptSalesReturnReceivedItemsData
         JOIN dobgoodsreceiptsalesreturn
              ON IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceId =
                 dobgoodsreceiptsalesreturn.id
         left JOIN dobinventorydocument
                   on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_SR'
         left JOIN mobitemgeneralmodel
                   on IObGoodsReceiptSalesReturnReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
         left JOIN iobgoodsreceiptsalesreturndata
                   ON iobgoodsreceiptsalesreturndata.refinstanceid = dobgoodsreceiptsalesreturn.id;

CREATE VIEW IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderItemQuantities.id,
       IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype,
       IObGoodsReceiptPurchaseOrderItemQuantities.notes,
       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedCost,
       IObGoodsReceiptPurchaseOrderItemQuantities.actualCost,
       cobmeasure.symbol                                        AS unitofentry,
       cobmeasure.code                                          AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe)
               * iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtyWithDefectsbase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                                 AS baseunitfactor,
       mobitemgeneralmodel.code                                 as itemCode,
       mobitemgeneralmodel.name                                 as itemName,
       dobinventorydocument.code                                AS goodsreceiptcode,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                                        AS itemCodeAtVendor,
       cobmeasure.id                                            AS unitofentryid
FROM IObGoodsReceiptPurchaseOrderItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;



CREATE VIEW IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptSalesReturnItemQuantities.id,
       IObGoodsReceiptSalesReturnItemQuantities.refinstanceid AS ordinaryitemid,
       dobinventorydocument.code                              AS goodsreceiptcode,
       IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe,
       IObGoodsReceiptSalesReturnItemQuantities.stocktype,
       IObGoodsReceiptSalesReturnItemQuantities.notes,
       cobmeasure.symbol                                      AS unitofentry,
       cobmeasure.code                                        AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
               THEN IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe
           END ::numeric                                      AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                               AS baseunitfactor,
       masterdata.code                                        as itemCode,
       masterdata.name                                        as itemName,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE masterdata.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                      AS baseUnitSymbol
FROM IObGoodsReceiptSalesReturnItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptSalesReturnItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptSalesReturnReceivedItemsData
              ON IObGoodsReceiptSalesReturnReceivedItemsData.id =
                 IObGoodsReceiptSalesReturnItemQuantities.refinstanceid
         JOIN masterdata
              ON IObGoodsReceiptSalesReturnReceivedItemsData.itemid = masterdata.id
         JOIN DObGoodsReceiptSalesReturn ON DObGoodsReceiptSalesReturn.id =
                                            IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON masterdata.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptSalesReturnItemQuantities.unitofentryid;


CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS
SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.stocktype,
       iobgoodsreceiptitembatches.notes,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                iobgoodsreceiptitembatches.unitofentryid
               THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE iobgoodsreceiptitembatches.receivedqtyuoe
           END ::numeric                        AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                iobgoodsreceiptitembatches.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobinventorydocument.code                as goodsReceiptCode,
       (SELECT baseUnit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code =
              iobgoodsreceiptreceiveditemsgeneralmodel.itemCode
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                        AS itemCodeAtVendor
FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;



CREATE VIEW unrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId          as itemId,
       vendorinvoiceitemweight.landedCostState as landedCostState,
       vendorinvoiceitemweight.landedCostCode  as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal            as itemTotalLandedCost,
       (itemweight * landedcosttotal) /
       GRUnrestrectedItem.receivedqtyuoe       as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
        (itemunitprice * currencyprice))       as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode =
                       vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and
                       vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode;


CREATE VIEW purchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode =
                      unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode =
                           GRDamagedItem.unitofentrycode;