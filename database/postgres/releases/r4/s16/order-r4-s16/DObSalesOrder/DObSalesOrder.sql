Drop view if EXISTS dobsalesordergeneralmodel;
Drop view if EXISTS iobsalesordersummarygeneralmodel;
DROP FUNCTION getsalesordertotaltaxes(bigint);
DROP FUNCTION getsalesordertotalamountbeforetaxes(bigint);
Drop view if EXISTS iobsalesordertaxgeneralmodel;
Drop view if EXISTS iobsalesorderitemgeneralmodel;

ALTER TABLE dobsalesorder ALTER COLUMN remaining TYPE NUMERIC;
ALTER TABLE dobsalesorder ALTER COLUMN totalamount TYPE NUMERIC;
ALTER TABLE iobsalesorderitem ALTER COLUMN quantity TYPE NUMERIC;
ALTER TABLE iobsalesorderitem ALTER COLUMN salesprice TYPE NUMERIC;