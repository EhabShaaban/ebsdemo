CREATE OR REPLACE VIEW IObSalesOrderItemGeneralModel AS
SELECT IObSalesOrderItem.id,
       IObSalesOrderItem.refinstanceid,
       IObSalesOrderItem.creationDate,
       IObSalesOrderItem.modifiedDate,
       IObSalesOrderItem.creationInfo,
       IObSalesOrderItem.modificationInfo,
       dobsalesorder.code                                        as salesOrderCode,
       masterdata.code                                           AS itemCode,
       masterdata.name                                           AS itemName,
       cobitem.code                                              AS itemTypeCode,
       cobitem.name                                              AS itemTypeName,
       cobmeasure.code                                           as unitOfMeasureCode,
       cobmeasure.name                                           as unitOfMeasureName,
       cobmeasure.symbol                                         as unitOfMeasureSymbol,
       IObSalesOrderItem.quantity,
       IObSalesOrderItem.salesprice,
       IObSalesOrderItem.quantity * IObSalesOrderItem.salesprice as totalAmount
FROM dobsalesorder
         join IObSalesOrderItem on IObSalesOrderItem.refInstanceId = dobsalesorder.id
         join mobitem on IObSalesOrderItem.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         join cobitem on mobitem.typeid = cobitem.id
         join cobmeasure on IObSalesOrderItem.orderUnitId = cobmeasure.id;

CREATE OR REPLACE VIEW IObSalesOrderTaxGeneralModel AS
SELECT iobsalesordertax.id,
       iobsalesordertax.creationDate,
       iobsalesordertax.modifiedDate,
       iobsalesordertax.creationInfo,
       iobsalesordertax.modificationInfo,
       iobsalesordertax.refinstanceid,
       dobsalesorder.code                                                     AS salesOrderCode,
       iobsalesordertax.code,
       iobsalesordertax.name,
       iobsalesordertax.percentage,
       (SELECT sum(iobsalesorderitemgeneralmodel.totalamount) *
               (iobsalesordertax.percentage / 100::numeric)
        FROM iobsalesorderitemgeneralmodel
        WHERE iobsalesorderitemgeneralmodel.refinstanceid = dobsalesorder.id) AS amount
FROM iobsalesordertax
         JOIN dobsalesorder ON dobsalesorder.id = iobsalesordertax.refInstanceId;

CREATE OR REPLACE FUNCTION getSalesOrderTotalAmountBeforeTaxes(orderId bigint)
    RETURNS numeric AS
$$
BEGIN
    RETURN (
        (SELECT CASE WHEN SUM(totalamount) IS NULL THEN 0 ELSE SUM(totalamount) END
         FROM iobsalesorderitemgeneralmodel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION getSalesOrderTotalTaxes(orderId bigint)
    RETURNS numeric AS
$$
BEGIN
    RETURN (
        (SELECT CASE WHEN SUM(amount) IS NULL THEN 0 ELSE SUM(amount) END
         FROM iobsalesordertaxgeneralmodel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW IObSalesOrderSummaryGeneralModel AS
SELECT dobsalesorder.id,
       dobsalesorder.code                                    AS orderCode,
       getSalesOrderTotalAmountBeforeTaxes(dobsalesorder.id) AS totalAmountBeforeTaxes,
       getSalesOrderTotalTaxes(dobsalesorder.id)             AS totalTaxes,
       getSalesOrderTotalAmountBeforeTaxes(dobsalesorder.id)
           + getSalesOrderTotalTaxes(dobsalesorder.id)       AS totalAmountAfterTaxes
FROM dobsalesorder;


CREATE OR REPLACE VIEW DObSalesOrderGeneralModel AS
SELECT dobsalesorder.id,
       dobsalesorder.code,
       dobsalesorder.creationDate,
       dobsalesorder.modifiedDate,
       dobsalesorder.creationInfo,
       dobsalesorder.modificationInfo,
       dobsalesorder.currentStates,
       cobsalesordertype.code               AS salesOrderTypeCode,
       cobsalesordertype.name               AS salesOrderTypeName,
       businessunit.code                    AS purchaseUnitCode,
       businessunit.name                    AS purchaseUnitName,
       businessunit.name::json ->> 'en'     AS purchaseUnitNameEn,
       salesResponsible.code                AS salesResponsibleCode,
       salesResponsible.name                AS salesResponsibleName,
       salesResponsible.name::json ->> 'en' AS salesResponsibleNameEn,
       masterdata.code                      AS customerCode,
       masterdata.name                      AS customerName,
       company.code                         AS companyCode,
       company.name                         AS companyName,
       store.code                           AS storeCode,
       store.name                           AS storeName,
       iobsalesorderdata.expectedDeliveryDate,
       iobsalesorderdata.notes,
       dobsalesorder.totalAmount,
       dobsalesorder.remaining
FROM dobsalesorder
         LEFT JOIN
     cobsalesordertype ON dobsalesorder.salesordertypeid = cobsalesordertype.id
         LEFT JOIN cobenterprise businessunit ON dobsalesorder.businessunitid = businessunit.id AND
                                                 businessunit.objecttypecode = '5'
         LEFT JOIN cobenterprise salesResponsible
                   ON dobsalesorder.salesresponsibleid = salesResponsible.id AND
                      salesResponsible.objecttypecode = '7'
         LEFT JOIN iobsalesorderdata ON dobsalesorder.id = iobsalesorderdata.refinstanceid

         LEFT JOIN mobcustomer ON iobsalesorderdata.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN iobsalesordercompanystoredata ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
         LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
                                            company.objecttypecode = '1'
         LEFT JOIN cobenterprise store ON iobsalesordercompanystoredata.storeid = store.id AND
                                          store.objecttypecode = '4';
