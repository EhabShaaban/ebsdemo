CREATE OR REPLACE VIEW DObOrderPaymentGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       iobordercompany.businessunitid     AS purchaseUnitId,
       businessUnit.code                  AS purchaseUnitCode,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       iobpurchaseorderfulfillervendor.vendorid,
       vendor.code                        AS vendorCode,
       vendor.name                        AS vendorName,
       cobcurrency.code                   AS currencyCode,
       cobcurrency.iso                    AS currencyISO,
       cobcurrency.name                   AS currencyName,
       company.code                    AS companyCode,
       company.name                    AS companyName
FROM doborderdocument
    JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
    LEFT JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
    LEFT JOIN iobpurchaseorderfulfillervendor
    ON dobpurchaseorder.id = iobpurchaseorderfulfillervendor.refinstanceid
    LEFT JOIN masterdata vendor
    ON iobpurchaseorderfulfillervendor.vendorid = vendor.id
    LEFT JOIN cobenterprise businessUnit on iobordercompany.businessunitid = businessUnit.id
    LEFT JOIN ioborderpaymenttermsdetails
    on dobpurchaseorder.id = ioborderpaymenttermsdetails.refinstanceid
    LEFT JOIN cobcurrency on ioborderpaymenttermsdetails.currencyid = cobcurrency.id
    LEFT JOIN doborderdocument refDocument
    on iobpurchaseorderfulfillervendor.referencepoid = refDocument.id
    LEFT JOIN cobenterprise company ON company.id = iobordercompany.companyid;
