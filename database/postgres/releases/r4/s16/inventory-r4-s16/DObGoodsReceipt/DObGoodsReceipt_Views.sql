CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS
SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.stocktype,
       iobgoodsreceiptitembatches.notes,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                iobgoodsreceiptitembatches.unitofentryid
               THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE iobgoodsreceiptitembatches.receivedqtyuoe
           END ::numeric                        AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                iobgoodsreceiptitembatches.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobinventorydocument.code                as goodsReceiptCode,
       (SELECT baseUnit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code =
              iobgoodsreceiptreceiveditemsgeneralmodel.itemCode
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                        AS itemCodeAtVendor
FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;

CREATE OR REPLACE VIEW IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderItemQuantities.id,
       IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype,
       IObGoodsReceiptPurchaseOrderItemQuantities.notes,
       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedCost,
       IObGoodsReceiptPurchaseOrderItemQuantities.actualCost,
       cobmeasure.symbol                                        AS unitofentry,
       cobmeasure.code                                          AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe)
               * iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtyWithDefectsbase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                                 AS baseunitfactor,
       mobitemgeneralmodel.code                                 as itemCode,
       mobitemgeneralmodel.name                                 as itemName,
       dobinventorydocument.code                                AS goodsreceiptcode,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                                        AS itemCodeAtVendor,
       cobmeasure.id                                            AS unitofentryid
FROM IObGoodsReceiptPurchaseOrderItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;

CREATE VIEW IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptSalesReturnItemQuantities.id,
       IObGoodsReceiptSalesReturnItemQuantities.refinstanceid AS ordinaryitemid,
       dobinventorydocument.code                              AS goodsreceiptcode,
       IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe,
       IObGoodsReceiptSalesReturnItemQuantities.stocktype,
       IObGoodsReceiptSalesReturnItemQuantities.notes,
       cobmeasure.symbol                                      AS unitofentry,
       cobmeasure.code                                        AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
               THEN IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe
           END ::numeric                                      AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                               AS baseunitfactor,
       masterdata.code                                        as itemCode,
       masterdata.name                                        as itemName,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE masterdata.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                      AS baseUnitSymbol
FROM IObGoodsReceiptSalesReturnItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptSalesReturnItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptSalesReturnReceivedItemsData
              ON IObGoodsReceiptSalesReturnReceivedItemsData.id =
                 IObGoodsReceiptSalesReturnItemQuantities.refinstanceid
         JOIN masterdata
              ON IObGoodsReceiptSalesReturnReceivedItemsData.itemid = masterdata.id
         JOIN DObGoodsReceiptSalesReturn ON DObGoodsReceiptSalesReturn.id =
                                            IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON masterdata.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptSalesReturnItemQuantities.unitofentryid;

create view iobgoodsreceiptitemquantitygeneralmodel(id, userCode, itemid, itemcode, quantity, stocktype, unitofmeasureid, unitofmeasurecode, purchaseunitid, purchaseunitcode, companyid, companycode, storehouseid, storehousecode, plantid, plantcode, goodsreceipttype, creationdate, creationinfo, modifieddate, modificationinfo) as
SELECT concat(goodsreceipt.id, itemquantity.id, itemdata.id) AS id,
       inventorydocument.code                                AS userCode,
       itemdata.id                                           AS itemid,
       itemdata.code                                         AS itemcode,
       itemquantity.receivedqtyuoe                           AS quantity,
       itemquantity.stocktype,
       itemunitofmeasure.id                                  AS unitofmeasureid,
       itemunitofmeasure.code                                AS unitofmeasurecode,
       goodsreceiptpurchaseunit.id                           AS purchaseunitid,
       goodsreceiptpurchaseunit.code                         AS purchaseunitcode,
       goodsreceiptcompany.id                                AS companyid,
       goodsreceiptcompany.code                              AS companycode,
       goodsreceiptstorehouse.id                             AS storehouseid,
       goodsreceiptstorehouse.code                           AS storehousecode,
       goodsreceiptplant.id                                  AS plantid,
       goodsreceiptplant.code                                AS plantcode,
       'PO'::text                                            AS goodsreceipttype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsreceiptpurchaseorder goodsreceipt
         JOIN dobinventorydocument inventorydocument ON goodsreceipt.id = inventorydocument.id
         JOIN iobgoodsreceiptpurchaseorderreceiveditemsdata goodsreceiptitem
              ON goodsreceipt.id = goodsreceiptitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsreceiptitem.itemid
         JOIN iobgoodsreceiptpurchaseorderitemquantities itemquantity
              ON goodsreceiptitem.id = itemquantity.refinstanceid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = itemquantity.unitofentryid
         JOIN cobenterprise goodsreceiptpurchaseunit ON inventorydocument.purchaseunitid = goodsreceiptpurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsreceiptcompany ON inventorycompany.companyid = goodsreceiptcompany.id
         JOIN cobenterprise goodsreceiptstorehouse ON inventorycompany.storehouseid = goodsreceiptstorehouse.id
         JOIN cobenterprise goodsreceiptplant ON inventorycompany.plantid = goodsreceiptplant.id
UNION ALL
SELECT concat(goodsreceipt.id, itemquantity.id, itemdata.id) AS id,
       inventorydocument.code                                AS userCode,
       itemdata.id                                           AS itemid,
       itemdata.code                                         AS itemcode,
       itemquantity.receivedqtyuoe                           AS quantity,
       itemquantity.stocktype,
       itemunitofmeasure.id                                  AS unitofmeasureid,
       itemunitofmeasure.code                                AS unitofmeasurecode,
       goodsreceiptpurchaseunit.id                           AS purchaseunitid,
       goodsreceiptpurchaseunit.code                         AS purchaseunitcode,
       goodsreceiptcompany.id                                AS companyid,
       goodsreceiptcompany.code                              AS companycode,
       goodsreceiptstorehouse.id                             AS storehouseid,
       goodsreceiptstorehouse.code                           AS storehousecode,
       goodsreceiptplant.id                                  AS plantid,
       goodsreceiptplant.code                                AS plantcode,
       'SR'::text                                            AS goodsreceipttype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsreceiptsalesreturn goodsreceipt
         JOIN dobinventorydocument inventorydocument ON goodsreceipt.id = inventorydocument.id
         JOIN iobgoodsreceiptsalesreturnreceiveditemsdata goodsreceiptitem
              ON goodsreceipt.id = goodsreceiptitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsreceiptitem.itemid
         JOIN iobgoodsreceiptsalesreturnitemquantities itemquantity ON goodsreceiptitem.id = itemquantity.refinstanceid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = itemquantity.unitofentryid
         JOIN cobenterprise goodsreceiptpurchaseunit ON inventorydocument.purchaseunitid = goodsreceiptpurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsreceiptcompany ON inventorycompany.companyid = goodsreceiptcompany.id
         JOIN cobenterprise goodsreceiptstorehouse ON inventorycompany.storehouseid = goodsreceiptstorehouse.id
         JOIN cobenterprise goodsreceiptplant ON inventorycompany.plantid = goodsreceiptplant.id;

CREATE VIEW UnrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId          as itemId,
       vendorinvoiceitemweight.landedCostState as landedCostState,
       vendorinvoiceitemweight.landedCostCode  as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal            as itemTotalLandedCost,
       (itemweight * landedcosttotal) /
       GRUnrestrectedItem.receivedqtyuoe       as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
        (itemunitprice * currencyprice))       as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode =
                       vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and
                       vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode;


CREATE VIEW PurchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode =
                      unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode =
                           GRDamagedItem.unitofentrycode;

