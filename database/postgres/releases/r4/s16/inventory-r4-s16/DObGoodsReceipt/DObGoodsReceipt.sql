alter table iobgoodsreceiptitembatches alter column receivedQtyUoE type numeric;
alter table iobgoodsreceiptitembatches alter column estimatedCost type numeric;
alter table iobgoodsreceiptitembatches alter column actualcost type numeric;
alter table IObGoodsReceiptPurchaseOrderItemQuantities alter column receivedQtyUoE type numeric;
alter table IObGoodsReceiptSalesReturnItemQuantities alter column receivedQtyUoE type numeric;
