CREATE OR REPLACE VIEW DObGoodsIssueDataGeneralModel AS
SELECT ROW_NUMBER() OVER ()                            AS id,
       DObInventoryDocument.code,
       c1.id                                           AS purchaseUnitId,
       c1.code                                         AS purchaseUnitCode,
       c1.name                                         AS purchaseUnitName,
       DObInventoryDocument.currentStates,
       m1.id                                           AS itemId,
       m1.code                                         AS itemCode,
       m1.name                                         AS itemName,
       cobmeasure.id                                   AS unitOfMeasureId,
       cobmeasure.code                                 AS unitOfMeasureCode,
       cobmeasure.name                                 AS unitOfMeasureName,
       IObGoodsIssueSalesInvoiceItem.quantity,
       IObGoodsIssueSalesInvoiceItem.batchNo,
       IObGoodsIssueSalesInvoiceItem.price,
       CObGoodsIssue.code                              AS goodsIssueTypeCode,
       CObGoodsIssue.name                              AS goodsIssueTypeName,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.modifiedDate,
       dobaccountingdocument.code                      AS referenceDocumentCode,
       c2.id                                           AS companyId,
       c2.code                                         AS companyCode,
       c2.name                                         AS companyName,
       c3.id                                           AS plantId,
       c3.code                                         AS plantCode,
       c3.name                                         AS plantName,
       c4.id                                           AS storeHouseId,
       c4.code                                         AS storeHouseCode,
       c4.name                                         AS storeHouseName,
       storekeeper.code                                AS storekeeperCode,
       storekeeper.name                                AS storekeeperName,
       m2.code                                         AS customerCode,
       m2.name                                         AS customerName,
       lobaddress.address::JSON ->> 'en'               AS address,
       LObContactPerson.name                           AS contactPersonName,
       c5.code                                         AS salesRepresentativeCode,
       c5.name                                         AS salesRepresentativeName,
       IObGoodsIssueSalesInvoiceData.comments,
       c6.code                                         AS kapCode,
       c6.name                                         AS kapName,
       case
           when iobalternativeuom.conversionfactor is null then 1
           else iobalternativeuom.conversionfactor end as conversionfactor,
       c2.name::JSON ->> 'ar'                          AS companyArName,
       enterpriseTelephone.contactValue                AS companyTelephone,
       enterpriseFax.contactValue                      AS companyFax,
       iobcompanylogodetails.logo                      AS companyLogo,
       IObEnterpriseAddress.addressLine                AS companyAddress,
       IObEnterpriseAddress.addressLine::JSON ->> 'ar' AS companyArAddress,
       m2.name::JSON ->> 'ar'                          AS customerArName,
       dobinventorydocument.id                         AS goodsIssueId

FROM DObInventoryDocument
         RIGHT JOIN dobgoodsissuesalesinvoice ON dobgoodsissuesalesinvoice.id = DObInventoryDocument.id
         LEFT JOIN cobenterprise c1 ON DObInventoryDocument.purchaseunitid = c1.id
         LEFT JOIN IObGoodsIssueSalesInvoiceItem
                   ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceItem.refinstanceid
         LEFT JOIN mobitem ON IObGoodsIssueSalesInvoiceItem.itemid = mobitem.id
         LEFT JOIN masterdata m1 ON mobitem.id = m1.id
         LEFT JOIN cobmeasure ON IObGoodsIssueSalesInvoiceItem.unitOfMeasureId = cobmeasure.id
         LEFT JOIN CObGoodsIssue ON dobgoodsissuesalesinvoice.typeid = CObGoodsIssue.id
         LEFT JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         LEFT JOIN cobcompany ON IObInventoryCompany.companyid = cobcompany.id
         LEFT JOIN cobenterprise c2 ON c2.id = cobcompany.id
         LEFT JOIN cobplant ON IObInventoryCompany.plantid = cobplant.id
         LEFT JOIN cobenterprise c3 ON c3.id = cobplant.id
         LEFT JOIN cobstorehouse ON IObInventoryCompany.storehouseid = cobstorehouse.id
         LEFT JOIN cobenterprise c4 ON c4.id = cobstorehouse.id
         LEFT JOIN storekeeper ON IObInventoryCompany.storekeeperid = storekeeper.id
         LEFT JOIN IObGoodsIssueSalesInvoiceData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceData.refinstanceid
         LEFT JOIN dobaccountingdocument
                   ON IObGoodsIssueSalesInvoiceData.salesInvoiceId = dobaccountingdocument.id
         LEFT JOIN mobcustomer ON IObGoodsIssueSalesInvoiceData.customerid = mobcustomer.id
         INNER JOIN MObBusinessPartner ON mobcustomer.id = MObBusinessPartner.id
         LEFT JOIN masterdata m2 ON MObBusinessPartner.id = m2.id
         LEFT JOIN lobaddress ON IObGoodsIssueSalesInvoiceData.addressid = lobaddress.id
         LEFT JOIN LObContactPerson
                   ON IObGoodsIssueSalesInvoiceData.contactpersonid = LObContactPerson.id
         LEFT JOIN CObenterprise c5 ON IObGoodsIssueSalesInvoiceData.salesrepresentativeid = c5.id
         LEFT JOIN cobenterprise c6 ON MObCustomer.kapid = c6.id
         LEFT JOIN iobalternativeuom
                   ON IObGoodsIssueSalesInvoiceItem.itemId = iobalternativeuom.refinstanceid AND
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         LEFT JOIN IObEnterpriseContact enterpriseTelephone
                   ON cobcompany.id = enterpriseTelephone.refinstanceid AND
                      enterpriseTelephone.objectTypeCode = '1' AND
                      enterpriseTelephone.contactTypeId = 1
         LEFT JOIN IObEnterpriseContact enterpriseFax
                   ON cobcompany.id = enterpriseFax.refinstanceid AND
                      enterpriseFax.objectTypeCode = '1' AND
                      enterpriseFax.contactTypeId = 2
         LEFT JOIN iobcompanylogodetails ON cobcompany.id = iobcompanylogodetails.refinstanceid
         LEFT JOIN IObEnterpriseAddress ON cobcompany.id = IObEnterpriseAddress.refinstanceid
WHERE IObGoodsIssueSalesInvoiceItem.id IS NOT NULL

UNION ALL

SELECT ROW_NUMBER() OVER () + (SELECT COUNT(*) FROM IObGoodsIssueSalesInvoiceItem) AS id,
       DObInventoryDocument.code,
       c1.id                                                                       AS purchaseUnitId,
       c1.code                                                                     AS purchaseUnitCode,
       c1.name                                                                     AS purchaseUnitName,
       DObInventoryDocument.currentStates,
       m1.id                                                                       AS itemId,
       m1.code                                                                     AS itemCode,
       m1.name                                                                     AS itemName,
       cobmeasure.id                                                               AS unitOfMeasureId,
       cobmeasure.code                                                             AS unitOfMeasureCode,
       cobmeasure.name                                                             AS unitOfMeasureName,
       IObGoodsIssueSalesOrderItem.quantity,
       IObGoodsIssueSalesOrderItem.batchNo,
       IObGoodsIssueSalesOrderItem.price,
       CObGoodsIssue.code                                                          AS goodsIssueTypeCode,
       CObGoodsIssue.name                                                          AS goodsIssueTypeName,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.modifiedDate,
       dobsalesorder.code                                                          AS referenceDocumentCode,
       c2.id                                                                       AS companyId,
       c2.code                                                                     AS companyCode,
       c2.name                                                                     AS companyName,
       c3.id                                                                       AS plantId,
       c3.code                                                                     AS plantCode,
       c3.name                                                                     AS plantName,
       c4.id                                                                       AS storeHouseId,
       c4.code                                                                     AS storeHouseCode,
       c4.name                                                                     AS storeHouseName,
       storekeeper.code                                                            AS storekeeperCode,
       storekeeper.name                                                            AS storekeeperName,
       m2.code                                                                     AS customerCode,
       m2.name                                                                     AS customerName,
       lobaddress.address::JSON ->> 'en'                                           AS address,
       LObContactPerson.name                                                       AS contactPersonName,
       c5.code                                                                     AS salesRepresentativeCode,
       c5.name                                                                     AS salesRepresentativeName,
       IObGoodsIssueSalesOrderData.comments,
       c6.code                                                                     AS kapCode,
       c6.name                                                                     AS kapName,
       case
           when iobalternativeuom.conversionfactor is null then 1
           else iobalternativeuom.conversionfactor end                             as conversionfactor,
       c2.name::JSON ->> 'ar'                                                      AS companyArName,
       enterpriseTelephone.contactValue                                            AS companyTelephone,
       enterpriseFax.contactValue                                                  AS companyFax,
       iobcompanylogodetails.logo                                                  AS companyLogo,
       IObEnterpriseAddress.addressLine                                            AS companyAddress,
       IObEnterpriseAddress.addressLine::JSON ->> 'ar'                             AS companyArAddress,
       m2.name::JSON ->> 'ar'                                                      AS customerArName,
       dobinventorydocument.id                         AS goodsIssueId
FROM DObInventoryDocument
         RIGHT JOIN dobgoodsissuesalesorder ON dobgoodsissuesalesorder.id = DObInventoryDocument.id
         LEFT JOIN cobenterprise c1 ON DObInventoryDocument.purchaseunitid = c1.id
         LEFT JOIN IObGoodsIssueSalesOrderItem ON DObInventoryDocument.id = IObGoodsIssueSalesOrderItem.refinstanceid
         LEFT JOIN mobitem ON IObGoodsIssueSalesOrderItem.itemid = mobitem.id
         LEFT JOIN masterdata m1 ON mobitem.id = m1.id
         LEFT JOIN cobmeasure ON IObGoodsIssueSalesOrderItem.unitOfMeasureId = cobmeasure.id
         LEFT JOIN CObGoodsIssue ON dobgoodsissuesalesorder.typeid = CObGoodsIssue.id
         LEFT JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         LEFT JOIN cobcompany ON IObInventoryCompany.companyid = cobcompany.id
         LEFT JOIN cobenterprise c2 ON c2.id = cobcompany.id
         LEFT JOIN cobplant ON IObInventoryCompany.plantid = cobplant.id
         LEFT JOIN cobenterprise c3 ON c3.id = cobplant.id
         LEFT JOIN cobstorehouse ON IObInventoryCompany.storehouseid = cobstorehouse.id
         LEFT JOIN cobenterprise c4 ON c4.id = cobstorehouse.id
         LEFT JOIN storekeeper ON IObInventoryCompany.storekeeperid = storekeeper.id
         LEFT JOIN IObGoodsIssueSalesOrderData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesOrderData.refinstanceid
         LEFT JOIN dobsalesorder
                   ON IObGoodsIssueSalesOrderData.salesorderid = dobsalesorder.id
         LEFT JOIN mobcustomer ON IObGoodsIssueSalesOrderData.customerid = mobcustomer.id
         LEFT JOIN MObBusinessPartner ON mobcustomer.id = MObBusinessPartner.id
         LEFT JOIN masterdata m2 ON MObBusinessPartner.id = m2.id
         LEFT JOIN lobaddress ON IObGoodsIssueSalesOrderData.addressid = lobaddress.id
         LEFT JOIN LObContactPerson
                   ON IObGoodsIssueSalesOrderData.contactpersonid = LObContactPerson.id
         LEFT JOIN CObenterprise c5 ON IObGoodsIssueSalesOrderData.salesRepresentativeId = c5.id
         LEFT JOIN cobenterprise c6 ON MObCustomer.kapid = c6.id
         LEFT JOIN iobalternativeuom
                   ON IObGoodsIssueSalesOrderItem.itemId = iobalternativeuom.refinstanceid AND
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         LEFT JOIN IObEnterpriseContact enterpriseTelephone
                   ON cobcompany.id = enterpriseTelephone.refinstanceid AND
                      enterpriseTelephone.objectTypeCode = '1' AND
                      enterpriseTelephone.contactTypeId = 1
         LEFT JOIN IObEnterpriseContact enterpriseFax
                   ON cobcompany.id = enterpriseFax.refinstanceid AND
                      enterpriseFax.objectTypeCode = '1' AND
                      enterpriseFax.contactTypeId = 2
         LEFT JOIN iobcompanylogodetails ON cobcompany.id = iobcompanylogodetails.refinstanceid
         LEFT JOIN IObEnterpriseAddress ON cobcompany.id = IObEnterpriseAddress.refinstanceid
WHERE IObGoodsIssueSalesOrderItem.id IS NOT NULL;

CREATE OR REPLACE VIEW IObGoodsIssueItemGeneralModel AS
SELECT IObGoodsIssueSalesInvoiceItem.id,
       IObGoodsIssueSalesInvoiceItem.refInstanceId,
       DObInventoryDocument.code      AS goodsIssueCode,
       masterdata.name                AS itemName,
       masterdata.name::JSON ->> 'en' AS itemNameEn,
       masterdata.code                AS itemCode,
       c1.symbol                      AS unitOfMeasureSymbol,
       c1.symbol::JSON ->> 'en'       AS unitOfMeasureSymbolEn,
       c1.code                        AS unitOfMeasureCode,
       IObGoodsIssueSalesInvoiceItem.quantity,
       c2.code                        as baseUnitCode,
       c2.symbol                      as baseUnitSymbol,
       round(CASE
                 WHEN IObGoodsIssueSalesInvoiceItem.unitOfMeasureId =
                      iobalternativeuom.alternativeunitofmeasureid
                     THEN IObGoodsIssueSalesInvoiceItem.quantity *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsIssueSalesInvoiceItem.quantity
                 END ::numeric,
             3)                       AS receivedqtybase
        ,
       IObGoodsIssueSalesInvoiceItem.batchNo,
       IObGoodsIssueSalesInvoiceItem.price
FROM IObGoodsIssueSalesInvoiceItem
         left join DObInventoryDocument on IObGoodsIssueSalesInvoiceItem.refInstanceId = DObInventoryDocument.id
         left join mobitem on IObGoodsIssueSalesInvoiceItem.itemId = mobitem.id
         left join masterdata on mobitem.id = masterdata.id
         left join cobmeasure c1 on IObGoodsIssueSalesInvoiceItem.unitOfMeasureId = c1.id
         left join cobmeasure c2 on mobitem.basicunitofmeasure = c2.id
         left join iobalternativeuom on iobalternativeuom.refinstanceid = mobitem.id
    and iobalternativeuom.alternativeunitofmeasureid = IObGoodsIssueSalesInvoiceItem.unitofmeasureid

UNION ALL

SELECT IObGoodsIssueSalesOrderItem.id,
       IObGoodsIssueSalesOrderItem.refInstanceId,
       DObInventoryDocument.code      AS goodsIssueCode,
       masterdata.name                AS itemName,
       masterdata.name::JSON ->> 'en' AS itemNameEn,
       masterdata.code                AS itemCode,
       c1.symbol                      AS unitOfMeasureSymbol,
       c1.symbol::JSON ->> 'en'       AS unitOfMeasureSymbolEn,
       c1.code                        AS unitOfMeasureCode,
       IObGoodsIssueSalesOrderItem.quantity,
       c2.code                        as baseUnitCode,
       c2.symbol                      as baseUnitSymbol,
       CASE
           WHEN IObGoodsIssueSalesOrderItem.unitOfMeasureId =
                iobalternativeuom.alternativeunitofmeasureid
               THEN IObGoodsIssueSalesOrderItem.quantity *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsIssueSalesOrderItem.quantity
           END ::numeric
                                      AS receivedqtybase,
       IObGoodsIssueSalesOrderItem.batchNo,
       IObGoodsIssueSalesOrderItem.price
FROM IObGoodsIssueSalesOrderItem
         left join DObInventoryDocument on IObGoodsIssueSalesOrderItem.refInstanceId = DObInventoryDocument.id
         left join mobitem on IObGoodsIssueSalesOrderItem.itemId = mobitem.id
         left join masterdata on mobitem.id = masterdata.id
         left join cobmeasure c1 on IObGoodsIssueSalesOrderItem.unitOfMeasureId = c1.id
         left join cobmeasure c2 on mobitem.basicunitofmeasure = c2.id
         left join iobalternativeuom on iobalternativeuom.refinstanceid = mobitem.id
    and iobalternativeuom.alternativeunitofmeasureid = IObGoodsIssueSalesOrderItem.unitofmeasureid;

CREATE OR REPLACE VIEW iobgoodsissueitemquantitygeneralmodel
            (id, userCode, itemid, itemcode, quantity, unitofmeasureid, unitofmeasurecode, purchaseunitid,
             purchaseunitcode, companyid, companycode, storehouseid, storehousecode, plantid, plantcode, goodsissuetype,
             creationdate, creationinfo, modifieddate, modificationinfo)
as
SELECT concat(goodsissue.id, goodsissueitem.unitofmeasureid, itemdata.id) AS id,
       inventorydocument.code                                             AS userCode,
       itemdata.id                                                        AS itemid,
       itemdata.code                                                      AS itemcode,
       goodsissueitem.quantity,
       itemunitofmeasure.id                                               AS unitofmeasureid,
       itemunitofmeasure.code                                             AS unitofmeasurecode,
       goodsissuepurchaseunit.id                                          AS purchaseunitid,
       goodsissuepurchaseunit.code                                        AS purchaseunitcode,
       goodsissuecompany.id                                               AS companyid,
       goodsissuecompany.code                                             AS companycode,
       goodsissuestorehouse.id                                            AS storehouseid,
       goodsissuestorehouse.code                                          AS storehousecode,
       goodsissueplant.id                                                 AS plantid,
       goodsissueplant.code                                               AS plantcode,
       'SI'::text                                                         AS goodsissuetype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsissuesalesinvoice goodsissue
         JOIN dobinventorydocument inventorydocument ON goodsissue.id = inventorydocument.id
         JOIN iobgoodsissuesalesinvoiceitem goodsissueitem ON goodsissue.id = goodsissueitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsissueitem.itemid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = goodsissueitem.unitofmeasureid
         JOIN cobenterprise goodsissuepurchaseunit ON inventorydocument.purchaseunitid = goodsissuepurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsissuecompany ON inventorycompany.companyid = goodsissuecompany.id
         JOIN cobenterprise goodsissuestorehouse ON inventorycompany.storehouseid = goodsissuestorehouse.id
         JOIN cobenterprise goodsissueplant ON inventorycompany.plantid = goodsissueplant.id
UNION ALL
SELECT concat(goodsissue.id, goodsissueitem.unitofmeasureid, itemdata.id) AS id,
       inventorydocument.code                                             AS goodsissuecode,
       itemdata.id                                                        AS itemid,
       itemdata.code                                                      AS itemcode,
       goodsissueitem.quantity,
       itemunitofmeasure.id                                               AS unitofmeasureid,
       itemunitofmeasure.code                                             AS unitofmeasurecode,
       goodsissuepurchaseunit.id                                          AS purchaseunitid,
       goodsissuepurchaseunit.code                                        AS purchaseunitcode,
       goodsissuecompany.id                                               AS companyid,
       goodsissuecompany.code                                             AS companycode,
       goodsissuestorehouse.id                                            AS storehouseid,
       goodsissuestorehouse.code                                          AS storehousecode,
       goodsissueplant.id                                                 AS plantid,
       goodsissueplant.code                                               AS plantcode,
       'SO'::text                                                         AS goodsissuetype,
       inventorydocument.creationdate,
       inventorydocument.creationinfo,
       inventorydocument.modifieddate,
       inventorydocument.modificationinfo
FROM dobgoodsissuesalesorder goodsissue
         JOIN dobinventorydocument inventorydocument ON goodsissue.id = inventorydocument.id
         JOIN iobgoodsissuesalesorderitem goodsissueitem ON goodsissue.id = goodsissueitem.refinstanceid
         JOIN masterdata itemdata ON itemdata.id = goodsissueitem.itemid
         JOIN cobmeasure itemunitofmeasure ON itemunitofmeasure.id = goodsissueitem.unitofmeasureid
         JOIN cobenterprise goodsissuepurchaseunit ON inventorydocument.purchaseunitid = goodsissuepurchaseunit.id
         JOIN iobinventorycompany inventorycompany ON inventorydocument.id = inventorycompany.refinstanceid
         JOIN cobenterprise goodsissuecompany ON inventorycompany.companyid = goodsissuecompany.id
         JOIN cobenterprise goodsissuestorehouse ON inventorycompany.storehouseid = goodsissuestorehouse.id
         JOIN cobenterprise goodsissueplant ON inventorycompany.plantid = goodsissueplant.id;
