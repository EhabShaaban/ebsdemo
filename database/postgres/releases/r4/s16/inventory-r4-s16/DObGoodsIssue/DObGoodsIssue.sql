Drop view if EXISTS dobgoodsissuedatageneralmodel;
Drop view if EXISTS iobgoodsissueitemgeneralmodel;
Drop view if EXISTS iobgoodsissueitemquantitygeneralmodel;

alter table iobgoodsissuesalesinvoiceitem alter column quantity type numeric;
alter table iobgoodsissuesalesinvoiceitem alter column price type numeric;
alter table iobgoodsissuesalesorderitem alter column quantity type numeric;
alter table iobgoodsissuesalesorderitem alter column price type numeric;