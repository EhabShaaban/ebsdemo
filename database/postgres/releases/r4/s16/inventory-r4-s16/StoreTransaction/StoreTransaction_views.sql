
CREATE VIEW TObGoodsReceiptStoreTransactionDataGeneralModel AS
SELECT ROW_NUMBER()
       OVER ()                                                   AS id,
       DObInventoryDocument.creationdate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.code,
       DObGoodsReceiptPurchaseOrder.id                           AS goodsReceiptId,
       iobinventorycompany.storehouseid,
       iobinventorycompany.plantid,

       cobplant.companyid,

       dobinventoryDocument.purchaseunitid,

       IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid,

       c1.symbol                                                 AS baseUnitSymbol,
       masterdata.code                                           AS itemCode,
       masterdata.name                                           AS itemName,
       (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
        alternative1.conversionfactor)                           AS receivedQtyBase,

       (iobgoodsreceiptitembatches.receivedqtyuoe *
        alternative2.conversionfactor)                           AS receivedBatchQtyBase,

       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedcost  AS estimatedCostQty,
       iobgoodsreceiptitembatches.estimatedcost                  AS estimatedCostBatch,

       IObGoodsReceiptPurchaseOrderItemQuantities.actualcost     AS actualCostQty,
       iobgoodsreceiptitembatches.actualcost                     AS actualCostBatch,

       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe AS receivedQty,
       IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid  AS receivedUoeId,
       c2.code                                                   AS receivedUoeCode,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype      AS receivedStockType,


       IObGoodsReceiptItemBatches.batchcode                      AS batchNo,
       IObGoodsReceiptItemBatches.receivedqtyuoe                 AS batchQty,
       IObGoodsReceiptItemBatches.unitofentryid                  AS batchUoeId,
       IObGoodsReceiptItemBatches.stocktype                      AS batchStockType

FROM DObGoodsReceiptPurchaseOrder
         LEFT JOIN dobinventorydocument ON DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id AND
                                           dobinventorydocument.inventorydocumenttype = 'GR_PO'
         LEFT JOIN iobinventorycompany ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN cobplant ON iobinventorycompany.plantid = cobplant.id
         LEFT JOIN IObGoodsReceiptPurchaseOrderData
                   ON DObGoodsReceiptPurchaseOrder.id = IObGoodsReceiptPurchaseOrderData.refinstanceid
         LEFT JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
                   ON DObGoodsReceiptPurchaseOrder.id = IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         LEFT JOIN IObGoodsReceiptPurchaseOrderItemQuantities
                   ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                      IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid

         LEFT JOIN IObGoodsReceiptItemBatches
                   ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id = IObGoodsReceiptItemBatches.refinstanceid
         LEFT JOIN mobitem ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitem.id
         LEFT JOIN iobalternativeuom alternative2 ON mobitem.id = alternative2.refinstanceid AND
                                                     alternative2.alternativeunitofmeasureid =
                                                     iobgoodsreceiptitembatches.unitofentryid
         LEFT JOIN iobalternativeuom alternative1 ON mobitem.id = alternative1.refinstanceid AND
                                                     alternative1.alternativeunitofmeasureid =
                                                     IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
         LEFT JOIN masterdata ON mobitem.id = masterdata.id
         LEFT JOIN cobmeasure c1 ON c1.id = mobitem.basicunitofmeasure
         LEFT JOIN cobmeasure c2 ON c2.id = IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;
