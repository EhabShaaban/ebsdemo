\ir 'drop_views.sql'
\ir 'DObGoodsReceipt/DObGoodsReceipt.sql'
\ir 'DObGoodsReceipt/DObGoodsReceipt_Views.sql'
\ir 'StoreTransaction/StoreTransaction_views.sql'
\ir 'DObGoodsIssue/DObGoodsIssue.sql'
\ir 'DObGoodsIssue/DObGoodsIssue_views.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
