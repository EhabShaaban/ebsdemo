DROP VIEW IF EXISTS CObJournalBalanceGeneralModel;

ALTER TABLE CObJournalBalance
ALTER COLUMN balance TYPE NUMERIC;
