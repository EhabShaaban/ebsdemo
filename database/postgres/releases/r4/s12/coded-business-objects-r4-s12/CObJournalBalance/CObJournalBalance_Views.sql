CREATE VIEW CObJournalBalanceGeneralModel AS
SELECT balance.id,
       balance.code,
       balance.name,
       balance.creationinfo,
       balance.creationdate,
       balance.modificationinfo,
       balance.modifieddate,
       balance.objecttypecode,
       businessUnit.code                                             AS businessUnitCode,
       businessUnit.name                                             AS businessUnitName,
       company.code                                                  AS companyCode,
       company.name                                                  AS companyName,
       currency.code                                                 AS currencyCode,
       currency.iso                                                  AS currencyIso,
       bankAccountBank.code                                          AS bankCode,
       bankAccountBank.name                                          AS bankName,
       concat(bankAccount.accountno, ' - ', bankAccountCurrency.iso) AS bankAccountNumber,
       treasury.code                                                 AS treasuryCode,
       treasury.name                                                 AS treasuryName,
       balance.balance
FROM cobjournalbalance balance
         JOIN cobenterprise businessUnit
              ON balance.businessunitid = businessunit.id
         JOIN cobenterprise company ON balance.companyid = company.id
         JOIN cobcurrency currency ON balance.currencyid = currency.id
         LEFT JOIN cobbankjournalbalance bankBalance ON balance.id = bankBalance.id
         LEFT JOIN iobcompanybankdetails bankAccount
                   ON bankBalance.accountid = bankAccount.id
                       AND balance.companyid = bankAccount.refinstanceid
                       AND balance.currencyid = bankAccount.currencyid
         LEFT JOIN cobcurrency bankAccountCurrency ON bankAccount.currencyid = bankAccountCurrency.id
         LEFT JOIN cobbank bankAccountBank ON bankAccount.bankid = bankAccountBank.id
         LEFT JOIN cobtreasuryjournalbalance treasuryBalance ON balance.id = treasuryBalance.id
         LEFT JOIN cobtreasury treasury ON treasuryBalance.treasuryid = treasury.id;
