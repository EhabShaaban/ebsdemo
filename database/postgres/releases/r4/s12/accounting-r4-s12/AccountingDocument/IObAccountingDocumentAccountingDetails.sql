DROP VIEW IObCostingAccountingDetailsGeneralModel;
DROP VIEW iobsettlementaccountingdetailsgeneralmodel;
DROP VIEW IObSettlementSummaryGeneralModel;
DROP VIEW IObNotesReceivableAccountingDetailsGeneralModel;
DROP VIEW IObCollectionAccountingDetailsGeneralModel;
DROP VIEW IObPaymentRequestAccountDetailsGeneralModel;
DROP VIEW IObLandedCostAccountingDetailsGeneralModel;

ALTER TABLE IObAccountingDocumentAccountingDetails
ALTER COLUMN amount TYPE NUMERIC;


Create Table IObAccountingDocumentTaxAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES lobtaxinfo  (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);
