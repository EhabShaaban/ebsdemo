DROP VIEW IF EXISTS IObLandedCostDetailsGeneralModel;

CREATE OR REPLACE VIEW IObLandedCostAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       doborderdocument.code      AS glSubAccountCode,
       doborderdocument.id        AS glSubAccountId,
       null::json                 As glSubAccountName,
       accountDetails.amount
FROM IObAccountingDocumentAccountingDetails accountDetails
         join dobaccountingdocument on dobaccountingdocument.id = accountDetails.refinstanceid
         join hobglaccount account on account.id = accountDetails.glaccountid
        left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails
              on orderaccountingdetails.id = accountDetails.id
        left join doborderdocument on doborderdocument.id = orderaccountingdetails.glSubAccountId
WHERE dobaccountingdocument.objecttypecode = 'LandedCost'
  AND accountDetails.objecttypecode = 'LandedCost';

CREATE OR REPLACE VIEW IObLandedCostDetailsGeneralModel AS
SELECT ioblandedcostdetails.id,
       ioblandedcostdetails.refinstanceid,
       ioblandedcostdetails.creationdate,
       ioblandedcostdetails.creationinfo,
       ioblandedcostdetails.modificationinfo,
       ioblandedcostdetails.modifieddate,
       landedCost.code                                           as landedCostCode,
       dobaccountingdocument.code                                as vendorInvoiceCode,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           dobaccountingdocument.objecttypecode) as invoiceAmount,
       doborderdocument.code                                     as purchaseOrderCode,
       companyCurrency.iso                                       as companyCurrencyIso,
       invoiceCurrency.iso                                       as invoiceCurrencyIso,
       ioblandedcostdetails.damageStock,
       cobenterprise.code                                        as businessUnitCode,
       cobenterprise.name                                        as businessUnitName,
       concat(cobenterprise.code, ' ', '-', ' ',
              cobenterprise.name::json ->> 'en')                 as businessUnitCodeName
FROM ioblandedcostdetails
         LEFT JOIN dobaccountingdocument landedCost
                   on ioblandedcostdetails.refinstanceid = landedCost.id
         LEFT JOIN doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN dobaccountingdocument
                   on ioblandedcostdetails.vendorinvoiceid = dobaccountingdocument.id

         LEFT JOIN iobordercompany on iobordercompany.refinstanceid = doborderdocument.id
         LEFT JOIN iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         LEFT JOIN iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = ioblandedcostdetails.vendorinvoiceid

         LEFT JOIN cobcurrency invoiceCurrency
                   on invoiceCurrency.id = iobvendorinvoicepurchaseorder.currencyid
         LEFT JOIN doblandedcost on doblandedcost.id = ioblandedcostdetails.refinstanceid
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON landedCost.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         LEFT JOIN cobenterprise on cobenterprise.id = IObLandedCostCompanyData.purchaseUnitId
         LEFT JOIN cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid;