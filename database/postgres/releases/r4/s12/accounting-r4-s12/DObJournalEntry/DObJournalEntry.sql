ALTER TABLE IObJournalEntryItemTaxSubAccount
    DROP CONSTRAINT FK_IObJournalEntryItemTaxSubAccount_iobcompanyTaxdetails;


ALTER TABLE IObJournalEntryItemTaxSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemTaxSubAccount_LObTaxInfo FOREIGN KEY (subAccountId)
        REFERENCES LObTaxInfo (id) ON DELETE RESTRICT;