CREATE OR Replace VIEW OrderPaymentsGeneralModel AS
SELECT payment.id                      as paymentId,
       payment.code                    as paymentCode,
       doborderdocument.code           as orderCode,
       doborderdocument.objecttypecode as orderType,
       referenceOrder.code             as referenceOrderCode
from dobaccountingdocument payment
         left join iobpaymentrequestpaymentdetails paymentdetails on payment.id = paymentdetails.refinstanceid
         left join iobpaymentrequestpurchaseorderpaymentdetails orderpaymentdetails
                   on paymentdetails.id = orderpaymentdetails.id
         left join doborderdocument on doborderdocument.id = orderpaymentdetails.duedocumentid
         left join iobpurchaseorderfulfillervendor
                   on iobpurchaseorderfulfillervendor.refinstanceid = doborderdocument.id
         left join doborderdocument referenceOrder on referenceOrder.id = iobpurchaseorderfulfillervendor.referencepoid
where payment.objecttypecode = 'PaymentRequest';

CREATE VIEW IObPaymentRequestAccountDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.code
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               doborderdocument.code
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks'
               then (concat(bankSubAccount.accountno, ' - ', currency.iso))
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (cobtreasury.code)
           end                    as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendoraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               orderaccountingdetails.glSubAccountId
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks'
               then (bankaccountingdetails.glSubAccountId)
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (treasuryaccountingdetails.glSubAccountId)
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.name
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks' THEN
               (cobbank.name)
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (cobtreasury.name)
           end                    as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObPaymentRequest pr on accountDetails.refinstanceid = pr.id
         left join dobaccountingdocument on dobaccountingdocument.id = pr.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails
                   on vendoraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails
                   on orderaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails
                   on bankaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         left join iobaccountingdocumenttreasuryaccountingdetails treasuryaccountingdetails
                   on treasuryaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         left join masterdata vendor
                   on vendor.id = vendoraccountingdetails.glSubAccountId and vendor.objecttypecode = '2'
         left join doborderdocument on doborderdocument.id = orderaccountingdetails.glSubAccountId
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         LEFT JOIN cobcurrency currency on bankSubAccount.currencyId = currency.id
         left join cobbank on cobbank.id = bankSubAccount.bankid
         left join cobtreasury on cobtreasury.id = treasuryaccountingdetails.glsubaccountid;

CREATE VIEW DObPaymentRequestCurrencyPriceGeneralModel AS
SELECT accountingDocument.id,
       accountingDocument.code AS userCode,
       paymentDetails.netamount,
       paymentDetails.duedocumenttype,
       purchaseOrderPaymentDetails.duedocumentid,
       CASE
           WHEN paymentdetails.duedocumenttype::text = 'PURCHASEORDER'::text THEN purchaseOrder.code
           WHEN paymentdetails.duedocumenttype::text = 'INVOICE'::text THEN vendorInvoice.code
           WHEN paymentdetails.duedocumenttype::text = 'CREDITNOTE'::text THEN creditnote.code
           END                 AS referencedocumentcode,
       activationDetails.currencyprice
FROM iobpaymentrequestpaymentdetails paymentDetails
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails creditNotePaymentDetails
                   ON creditNotePaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails purchaseOrderPaymentDetails
                   ON purchaseOrderPaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestinvoicepaymentdetails invoicePaymentDetails
                   ON invoicePaymentDetails.id = paymentDetails.id
         JOIN dobpaymentrequest paymentRequest ON paymentRequest.id = paymentDetails.refinstanceid
         JOIN dobaccountingdocument accountingDocument ON accountingDocument.id = paymentRequest.id
         JOIN iobaccountingdocumentactivationdetails activationDetails
              ON activationDetails.refinstanceid = accountingDocument.id
         LEFT JOIN doborderdocument purchaseOrder ON purchaseOrder.id = purchaseOrderPaymentDetails.duedocumentid
         LEFT JOIN dobaccountingdocument vendorInvoice ON vendorInvoice.id = invoicePaymentDetails.duedocumentid
         LEFT JOIN dobaccountingdocument creditNote ON creditNote.id = creditNotePaymentDetails.duedocumentid;
