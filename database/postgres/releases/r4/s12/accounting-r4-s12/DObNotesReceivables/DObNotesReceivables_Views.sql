CREATE VIEW IObNotesReceivableAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer'
               THEN (
               customer.code
               )
           WHEN accountDetails.subledger = 'Notes_Receivables'
               THEN (
               dobaccountingdocument.code
               )
           END                    AS glSubAccountCode,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer' THEN
               (customeraccountingdetails.glSubAccountId)
           WHEN accountDetails.subledger = 'Notes_Receivables'
               THEN
               (notesreceivableaccountingdetails.glSubAccountId)
           END                    AS glSubAccountId,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer'
               THEN (customer.name)
           END                    AS glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         LEFT JOIN dobmonetarynotes ON accountDetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = dobmonetarynotes.id
         LEFT JOIN hobglaccount account ON account.id = accountDetails.glaccountid
         LEFT JOIN iobaccountingdocumentcustomeraccountingDetails customeraccountingdetails
                   ON customeraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         LEFT JOIN iobaccountingdocumentnotesreceivableaccountingdetails notesreceivableaccountingdetails
                   ON notesreceivableaccountingdetails.id = accountDetails.id
                       AND accountDetails.accountingEntry = 'DEBIT'
                       AND dobaccountingdocument.id = notesreceivableaccountingdetails.glSubAccountId
         LEFT JOIN masterdata customer
                   ON customer.id = customeraccountingdetails.glSubAccountId AND
                      customer.objecttypecode = '3'
where accountDetails.objecttypecode = 'NOTES_RECEIVABLE';


DROP VIEW IF EXISTS IObNotesReceivablesReferenceDocumentGeneralModel;
CREATE VIEW IObNotesReceivablesReferenceDocumentGeneralModel AS
SELECT IObMonetaryNotesReferenceDocuments.id,
       IObMonetaryNotesReferenceDocuments.refInstanceId,
       notesReceivable.code as notesReceivableCode,
       notesReceivable.creationInfo,
       notesReceivable.modificationInfo,
       notesReceivable.creationDate,
       notesReceivable.modifiedDate,
       notesReceivable.currentStates,
       notesReceivable.objecttypecode,
       IObMonetaryNotesReferenceDocuments.documentType,
       IObMonetaryNotesReferenceDocuments.amounttocollect,
       (CASE
            WHEN salesInvoice.code IS NULL AND notesReceivableRefDoc.code IS NULL
                THEN dobsalesorder.code
            WHEN
                    notesReceivableRefDoc.code IS NULL AND dobsalesorder.code IS NULL
                THEN salesInvoice.code
            WHEN
                    salesInvoice.code IS NULL AND dobsalesorder.code IS NULL
                THEN notesReceivableRefDoc.code
            ELSE NULL END
           ) AS refDocumentCode,
       (CASE
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'SALES_INVOICE'
                THEN ('{"en":"Sales Invoice","ar":"فاتورة مبيعات"}') :: json
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'SALES_ORDER'
                THEN ('{"en":"Sales Order","ar":"طلب بيع"}') :: json
            WHEN IObMonetaryNotesReferenceDocuments.documentType = 'NOTES_RECEIVABLE'
                THEN ('{"en":"Notes Receivable","ar":"ورقة قبض"}') :: json
            ELSE NULL END
           ) AS refDocumentName,
       CObCurrency.iso            AS currencyIso
FROM dobaccountingdocument notesReceivable
         LEFT JOIN dobmonetarynotes
                   ON notesReceivable.id = dobmonetarynotes.id AND
                      objecttypecode LIKE 'NOTES_RECEIVABLE_%'
         JOIN IObMonetaryNotesReferenceDocuments
              on IObMonetaryNotesReferenceDocuments.refInstanceId = dobmonetarynotes.id
         LEFT JOIN IObNotesReceivableReferenceDocumentSalesOrder
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentSalesOrder.id
         LEFT JOIN dobsalesorder on IObNotesReceivableReferenceDocumentSalesOrder.documentId = dobsalesorder.id
         LEFT JOIN IObNotesReceivableReferenceDocumentSalesInvoice
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentSalesInvoice.id
         LEFT JOIN dobaccountingdocument salesInvoice
                   on IObNotesReceivableReferenceDocumentSalesInvoice.documentId = salesInvoice.id
         LEFT JOIN IObNotesReceivableReferenceDocumentNotesReceivable
                   on IObMonetaryNotesReferenceDocuments.id = IObNotesReceivableReferenceDocumentNotesReceivable.id
         LEFT JOIN dobaccountingdocument notesReceivableRefDoc
                   on IObNotesReceivableReferenceDocumentNotesReceivable.documentId = notesReceivableRefDoc.id
         LEFT JOIN iobmonetarynotesdetails ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid;
