update cobexchangerate exchangerate
set code = concat((
               select extract(year from creationdate) from cobexchangerate where exchangerate.id = cobexchangerate.id)::varchar ,
           code)
WHERE char_length(code) = 6;


update entityusercode
set code = concat((
                      select extract(year from creationdate) from cobexchangerate where typeid = 1 ORDER BY cobexchangerate.id DESC
                      LIMIT 1)::varchar ,
                  code)
WHERE char_length(code) = 6 and type = 'CObExchangeRate'