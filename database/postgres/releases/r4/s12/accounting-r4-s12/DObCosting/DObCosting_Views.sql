create or replace view IObCostingAccountingDetailsGeneralModel as

SELECT accountingDetails.id,
       accountingDetails.refinstanceid,
       dobaccountingdocument.code AS documentCode,
       accountingDetails.creationDate,
       accountingDetails.modifiedDate,
       accountingDetails.creationInfo,
       accountingDetails.modificationInfo,
       accountingDetails.accountingEntry,
       accountingDetails.subledger,
       account.id                 AS glAccountid,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       dobinventorydocument.code  AS goodsReceiptCode,
       orderDocument.id           AS glSubAccountid,
       orderDocument.code         AS glSubAccountCode,
       cobcurrency.code           AS currencyCode,
       cobcurrency.iso            AS currencyISO,
       null::json                 As glSubAccountName,
       accountingDetails.amount

FROM IObAccountingDocumentAccountingDetails accountingDetails
         join iobcostingaccountingdetails costingAccountingDetails
              ON costingAccountingDetails.id = accountingDetails.id
         INNER JOIN doborderdocument orderDocument ON orderDocument.id = costingAccountingDetails.glsubaccountid
         LEFT JOIN dobcosting costing ON accountingDetails.refinstanceid = costing.id
         INNER JOIN dobaccountingdocument ON dobaccountingdocument.id = costing.id
         INNER JOIN iobcostingdetails
                    on costing.id = iobcostingdetails.refinstanceid
         INNER JOIN dobinventorydocument
                    on dobinventorydocument.id = iobcostingdetails.goodsreceiptid
         INNER JOIN iobinventorycompany
                    on iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN cobenterprise company
                   ON iobinventorycompany.companyid = company.id
         LEFT JOIN cobcompany ON iobinventorycompany.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id
         LEFT JOIN hobglaccount account ON account.id = accountingDetails.glaccountid;

DROP VIEW IF EXISTS IObCostingDetailsGeneralModel;
DROP VIEW IF EXISTS IObCostingItemsGeneralModel;

create or replace view IObCostingDetailsGeneralModel AS
select iobcostingdetails.id,
       dobaccountingdocument.code       AS usercode,
       iobcostingdetails.refinstanceid,
       iobcostingdetails.creationdate,
       iobcostingdetails.creationinfo,
       iobcostingdetails.modifieddate,
       iobcostingdetails.modificationinfo,
       iobcostingdetails.currencyprice  AS currencyprice,
       cobcurrency.iso AS localCurrencyIso,
       dobinventorydocument.code        AS goodsreceiptcode,
       doborder.code                    AS purchaseordercode,
       purchaseunit.name ->> 'en'::text AS purchaseunitnameen,
       purchaseunit.name                AS purchaseunitname
FROM iobcostingdetails
         LEFT JOIN dobgoodsreceiptpurchaseorder ON iobcostingdetails.goodsreceiptid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN dobinventorydocument ON dobgoodsreceiptpurchaseorder.id = dobinventorydocument.id
         LEFT JOIN dobcosting ON iobcostingdetails.refinstanceid = dobcosting.id
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = dobcosting.id
         LEFT JOIN iobgoodsreceiptpurchaseorderdata
                   ON iobgoodsreceiptpurchaseorderdata.refinstanceid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN doborderdocument doborder ON iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata
                   ON iobaccountingdocumentcompanydata.companyid = iobenterprisebasicdata.refinstanceid
         left join cobcurrency on cobcurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN cobenterprise purchaseunit ON iobaccountingdocumentcompanydata.purchaseunitid = purchaseunit.id;

create or replace view IObCostingItemsGeneralModel AS
select IObCostingItems.id,
       dobaccountingdocument.code as userCode,
       IObCostingItems.refinstanceid,
       IObCostingItems.quantity,
       IObCostingItems.stocktype,
       IObCostingItems.tobeconsideredincostingper,
       masterdata.code            as itemCode,
       masterdata.name            as itemName,
       cobmeasure.code            as uomCode,
       cobmeasure.symbol          as uomSymbol,
       IObCostingItems.creationDate,
       IObCostingItems.creationInfo,
       IObCostingItems.modifiedDate,
       IObCostingItems.modificationInfo,
       IObCostingItems.unitPrice,
       IObCostingItems.unitLandedCost
from IObCostingItems
         left join masterdata
                   on IObCostingItems.itemid = masterdata.id and masterdata.objecttypecode = '1'
         left join cobmeasure on IObCostingItems.uomid = cobmeasure.id
         left join dobCosting
                   on IObCostingItems.refinstanceid = dobCosting.id
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobCosting.id;



create or replace view DobCostingGeneralModel AS
select dobcosting.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.creationdate,
       dobaccountingdocument.modificationinfo,
       dobaccountingdocument.modifieddate,
       dobaccountingdocument.currentstates,
       concat(company.code, ' - ', company.name::json ->> 'en')           as companyCodeName,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name::json ->> 'en') as purchaseUnitCodeName,
       purchaseUnit.name                                                  AS purchaseUnitName,
       purchaseUnit.name :: JSON ->> 'en'                                 AS purchaseUnitNameEn,
       purchaseUnit.code                                                  AS purchaseUnitcode,
       dobaccountingdocument.documentownerid,
       userinfo.name                                                      AS documentOwner

from DObCosting
         JOIN dobaccountingdocument on dobaccountingdocument.id = DObCosting.id
         LEFT JOIN userinfo ON dobaccountingdocument.documentownerid = userinfo.id
         LEFT JOIN IObAccountingDocumentCompanyData
                   ON IObAccountingDocumentCompanyData.refInstanceId = DObCosting.id
         LEFT JOIN cobenterprise purchaseUnit
                   ON IObAccountingDocumentCompanyData.purchaseUnitId = purchaseUnit.id AND
                      purchaseUnit.objecttypecode = '5'
         left join cobenterprise company
                   on IObAccountingDocumentCompanyData.companyid = company.id and company.objecttypecode = '1';