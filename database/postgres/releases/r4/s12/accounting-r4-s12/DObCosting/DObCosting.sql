ALTER table IobCostingItems alter column quantity type numeric;
ALTER table IobCostingItems alter column unitprice type numeric;
ALTER table IobCostingItems alter column unitlandedcost type numeric;
ALTER table IobCostingItems alter column tobeconsideredincostingper type numeric;

ALTER TABLE IObCostingDetails ADD COLUMN currencyPrice NUMERIC;
ALTER TABLE IObCostingDetails drop COLUMN exchangerateid;