ALTER table iobvendorinvoicesummary
    add column totalAmount numeric;
UPDATE iobvendorinvoicesummary
set totalAmount = iobinvoicesummarygeneralmodel.netamount
from iobinvoicesummarygeneralmodel
where iobvendorinvoicesummary.refinstanceid = iobinvoicesummarygeneralmodel.id
  and iobinvoicesummarygeneralmodel.invoicetype = 'VendorInvoice'
