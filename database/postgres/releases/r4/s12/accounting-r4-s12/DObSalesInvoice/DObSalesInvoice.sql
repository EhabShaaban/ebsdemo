
ALTER TABLE IObSalesInvoiceSummary
    ADD constraint FK_IObSalesInvoiceSummary_DObSalesInvoice FOREIGN KEY (refInstanceId)
        REFERENCES dobsalesinvoice (id) ON DELETE CASCADE;