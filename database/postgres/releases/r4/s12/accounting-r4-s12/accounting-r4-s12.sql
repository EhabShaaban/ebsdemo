\ir 'LobTaxes/LObTaxInfo.sql'
\ir 'LobTaxes/LObTaxInfo_Views.sql'
\ir 'AccountingDocument/IObAccountingDocumentAccountingDetails.sql'
\ir 'DObSettlement/DObSettlement_Views.sql'
\ir 'DObNotesReceivables/DObNotesReceivables.sql'
\ir 'DObNotesReceivables/DObNotesReceivables_Views.sql'
\ir 'DObCollection/DObCollection_Views.sql'
\ir 'DObCosting/DropViews.sql'
\ir 'DObCosting/DObCosting.sql'
\ir 'DObCosting/DObCosting_Views.sql'
\ir 'DObPaymentRequest/DObPaymentRequest_Views.sql'
\ir 'LandedCost/DObLandedCost_Views.sql'
\ir 'DObSalesInvoice/DObSalesInvoice.sql'
\ir 'HObChartsOfAccounts/HObChartsOfAccounts_Views.sql'
\ir 'DObJournalEntry/DObJournalEntry.sql'
\ir 'DObVendorInvoice/DObVendorInvoice.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
