CREATE VIEW IObSalesOrderTaxGeneralModel AS
SELECT iobsalesordertax.id,
       iobsalesordertax.creationDate,
       iobsalesordertax.modifiedDate,
       iobsalesordertax.creationInfo,
       iobsalesordertax.modificationInfo,
       iobsalesordertax.refinstanceid,
       dobsalesorder.code                                                     AS salesOrderCode,
       iobsalesordertax.code,
       iobsalesordertax.name,
       iobsalesordertax.percentage,
       (SELECT sum(iobsalesorderitemgeneralmodel.totalamount) *
               (iobsalesordertax.percentage / 100::numeric)
        FROM iobsalesorderitemgeneralmodel
        WHERE iobsalesorderitemgeneralmodel.refinstanceid = dobsalesorder.id) AS amount
FROM iobsalesordertax
         JOIN dobsalesorder ON dobsalesorder.id = iobsalesordertax.refInstanceId;


CREATE VIEW LObCompanyTaxesGeneralModel AS
SELECT lobcompanytaxes.id,
       lobtaxinfo.code,
       lobtaxinfo.creationDate,
       lobtaxinfo.modifiedDate,
       lobtaxinfo.creationinfo,
       lobtaxinfo.modificationInfo,
       lobtaxinfo.name,
       lobtaxinfo.percentage,
       cobenterprise.code as companyCode,
       cobenterprise.name as companyName,
       lobtaxinfo.description
From lobcompanytaxes
         left join lobtaxinfo on lobtaxinfo.id = lobcompanytaxes.id
         left join cobcompany on lobcompanytaxes.companyid = cobcompany.id
         left join cobenterprise on cobenterprise.id = cobcompany.id;


CREATE VIEW LObVendorTaxesGeneralModel AS
SELECT lobvendortaxes.id,
       lobtaxinfo.code,
       lobtaxinfo.creationDate,
       lobtaxinfo.modifiedDate,
       lobtaxinfo.creationinfo,
       lobtaxinfo.modificationInfo,
       lobtaxinfo.name,
       lobtaxinfo.percentage,
       masterdata.code as vendorCode,
       masterdata.name as vendorName,
       lobtaxinfo.description
From lobvendortaxes
         left join lobtaxinfo on lobtaxinfo.id = lobvendortaxes.id
         left join masterdata
                   on lobvendortaxes.vendorid = masterdata.id and masterdata.objecttypecode = '2';


CREATE VIEW IObSalesInvoiceTaxesGeneralModel AS
SELECT IObSalesInvoiceTaxes.id,
       IObSalesInvoiceTaxes.creationDate,
       IObSalesInvoiceTaxes.modifiedDate,
       IObSalesInvoiceTaxes.creationInfo,
       IObSalesInvoiceTaxes.modificationInfo,
       IObSalesInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code AS invoicecode,
       IObSalesInvoiceTaxes.code  AS taxcode,
       IObSalesInvoiceTaxes.name  AS taxname,
       (getInvoiceTotalAmountBeforTaxes(dobaccountingdocument.code, 'SalesInvoice') *
        (SELECT CASE
                    WHEN dobaccountingdocument.currentStates LIKE '%Active%'
                        then IObSalesInvoiceTaxes.percentage
                    WHEN dobaccountingdocument.currentStates LIKE '%Closed%'
                        then IObSalesInvoiceTaxes.percentage
                    ELSE LObTaxInfo.percentage
                    END
        )) / 100                  AS taxAmount,
       (SELECT CASE
                   WHEN dobaccountingdocument.currentStates LIKE '%Active%'
                       then IObSalesInvoiceTaxes.percentage
                   WHEN dobaccountingdocument.currentStates LIKE '%Closed%'
                       then IObSalesInvoiceTaxes.percentage
                   ELSE LObTaxInfo.percentage
                   END
       )                          AS taxpercentage
FROM IObSalesInvoiceTaxes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = IObSalesInvoiceTaxes.refInstanceId
                       and dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join dobsalesinvoice on dobsalesinvoice.id = IObSalesInvoiceTaxes.refinstanceid
         left join LObTaxInfo on LObTaxInfo.code = IObSalesInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis


CREATE VIEW IObVendorInvoiceTaxesGeneralModel AS
SELECT IObVendorInvoiceTaxes.id,
       IObVendorInvoiceTaxes.creationDate,
       IObVendorInvoiceTaxes.modifiedDate,
       IObVendorInvoiceTaxes.creationInfo,
       IObVendorInvoiceTaxes.modificationInfo,
       IObVendorInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code   AS invoicecode,
       IObVendorInvoiceTaxes.code   AS taxcode,
       IObVendorInvoiceTaxes.name   AS taxname,
       LObTaxInfo.percentage        as taxpercentage,
       IObVendorInvoiceTaxes.amount as taxAmount
FROM IObVendorInvoiceTaxes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = IObVendorInvoiceTaxes.refInstanceId
         inner join dobvendorInvoice on dobvendorInvoice.id = IObVendorInvoiceTaxes.refinstanceid
         left join LObTaxInfo on LObTaxInfo.code = IObVendorInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis
