\ir 'LObTaxInfo_Drop.sql'

ALTER TABLE lobtaxinfo
    ALTER COLUMN percentage TYPE numeric,
    DROP COLUMN sysflag;

ALTER TABLE IObSalesOrderTax
    ALTER COLUMN percentage TYPE numeric;
