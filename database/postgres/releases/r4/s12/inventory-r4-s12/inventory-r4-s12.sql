\ir 'StockAvailability/CObStockAvailability.sql'
\ir 'StockAvailability/DObInitialStockUpload_Views.sql'
\ir 'GoodsReceipt/GoodsReceipt_Views.sql'
\ir 'StoreTransaction/TObStoreTransaction.sql'
\ir 'StoreTransaction/TObStoreTransaction_Views.sql'


GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
