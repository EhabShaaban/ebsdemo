drop view cobstockavailabilitiesgeneralmodel;

alter table cobstockavailability alter column availablequantity type numeric;

CREATE VIEW CObStockAvailabilitiesGeneralModel AS
select CObStockAvailability.id,
       CObStockAvailability.code,
       CObStockAvailability.name,
       CObStockAvailability.creationDate,
       CObStockAvailability.modifiedDate,
       CObStockAvailability.creationInfo,
       CObStockAvailability.modificationInfo,
       company.code                     as companyCode,
       company.name                     as companyName,
       plant.code                       as plantCode,
       plant.name                       as plantName,
       storehouse.code                  as storehouseCode,
       storehouse.name                  as storehouseName,
       businessUnit.code                as purchaseUnitCode,
       businessUnit.name                as purchaseUnitName,
       businessUnit.name::json ->> 'en' as purchaseUnitNameEn,
       CObStockAvailability.stocktype,
       item.code                        as itemCode,
       item.name                        as itemName,
       unitOfMeasure.code               as unitOfMeasureCode,
       unitOfMeasure.symbol             as unitOfMeasureName,
       CObStockAvailability.availableQuantity
from CObStockAvailability
         join cobenterprise company on CObStockAvailability.companyId = company.id and company.objecttypecode = '1'
         left join cobenterprise plant on CObStockAvailability.plantid = plant.id and plant.objecttypecode = '3'
         left join cobenterprise storehouse
                   on CObStockAvailability.storehouseid = storehouse.id and storehouse.objecttypecode = '4'
         left join cobenterprise businessUnit
                   on CObStockAvailability.businessunitid = businessUnit.id and businessUnit.objecttypecode = '5'
         left join masterdata item on CObStockAvailability.itemid = item.id and item.objecttypecode = '1'
         left join cobmeasure unitOfMeasure on CObStockAvailability.unitofmeasureid = unitOfMeasure.id;
