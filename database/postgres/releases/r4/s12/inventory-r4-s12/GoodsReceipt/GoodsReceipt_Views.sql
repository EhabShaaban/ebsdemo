drop view if exists DObGoodsReceiptGeneralModel;

CREATE VIEW DObGoodsReceiptGeneralModel AS
    SELECT DObGoodsReceiptPurchaseOrder.id,
           DObInventoryDocument.code,
           IObInventoryActivationDetails.activationDate,
           DObInventoryDocument.creationDate,
           DObInventoryDocument.modifieddate,
           DObInventoryDocument.creationinfo,
           DObInventoryDocument.modificationinfo,
           DObInventoryDocument.currentStates,
           DObInventoryDocument.inventoryDocumentType,
           doborderdocument.code                       AS refDocumentCode,
           CObGoodsReceipt.code                        AS type,
           CObGoodsReceipt.name                        AS typeName,
           PurchaseUnitData.name :: JSON ->> 'en'      AS purchaseUnitNameEn,
           PurchaseUnitData.name                       AS purchaseUnitName,
           PurchaseUnitData.code                       AS PurchaseUnitCode,
           PurchaseUnitData.id                         AS PurchaseUnitId,
           ce1.id                                      AS companyId,
           ce1.name                                    AS companyName,
           ce1.code                                    AS companyCode,
           ce2.name                                    AS plantName,
           ce2.code                                    AS plantCode,
           IObGoodsReceiptPurchaseOrderData.vendorName AS businessPartnerName,
           masterdata.code                             AS businessPartnercode,
           ce3.code                                    AS storehouseCode,
           ce3.name                                    AS storehouseName,
           storekeeper.code                            AS storekeeperCode,
           storekeeper.name                            AS storekeeperName
    FROM DObGoodsReceiptPurchaseOrder
             LEFT JOIN dobinventorydocument ON DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
             LEFT JOIN iobinventorycompany
                       ON iobinventorycompany.refinstanceid = dobinventorydocument.id
             LEFT JOIN IObInventoryActivationDetails
                       ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
             JOIN iobgoodsreceiptpurchaseorderdata
                  ON iobgoodsreceiptpurchaseorderdata.refinstanceid = DObGoodsReceiptPurchaseOrder.id
             LEFT JOIN cobenterprise AS PurchaseUnitData
                       ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                           AND PurchaseUnitData.objectTypeCode = '5'
             LEFT JOIN masterdata
                       ON IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id AND
                          masterdata.objecttypecode LIKE '2'
             LEFT JOIN doborderdocument
                       ON iobgoodsreceiptpurchaseorderdata.purchaseOrderId = doborderdocument.id AND
                          doborderdocument.objecttypecode LIKE '%_PO'
             LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = DObGoodsReceiptPurchaseOrder.typeid
             LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
             LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
             LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
             LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid

    UNION ALL
    SELECT DObGoodsReceiptSalesReturn.id,
           DObInventoryDocument.code,
           IObInventoryActivationDetails.activationDate,
           DObInventoryDocument.creationDate,
           DObInventoryDocument.modifieddate,
           DObInventoryDocument.creationinfo,
           DObInventoryDocument.modificationinfo,
           DObInventoryDocument.currentStates,
           DObInventoryDocument.inventoryDocumentType,
           dobsalesreturnorder.code               AS refDocumentCode,
           CObGoodsReceipt.code                   AS type,
           CObGoodsReceipt.name                   AS typeName,
           PurchaseUnitData.name :: JSON ->> 'en' AS purchaseUnitNameEn,
           PurchaseUnitData.name                  AS purchaseUnitName,
           PurchaseUnitData.code                  AS PurchaseUnitCode,
           PurchaseUnitData.id                    AS PurchaseUnitId,
           ce1.id                                 AS companyId,
           ce1.name                               AS companyName,
           ce1.code                               AS companyCode,
           ce2.name                               AS plantName,
           ce2.code                               AS plantCode,
           masterdata.name                        AS businessPartnerName,
           masterdata.code                        AS businessPartnercode,
           ce3.code                               AS storehouseCode,
           ce3.name                               AS storehouseName,
           storekeeper.code                       AS storekeeperCode,
           storekeeper.name                       AS storekeeperName

    FROM DObGoodsReceiptSalesReturn
             LEFT JOIN dobinventorydocument ON DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
             LEFT JOIN iobinventorycompany
                       ON iobinventorycompany.refinstanceid = dobinventorydocument.id
             LEFT JOIN IObInventoryActivationDetails
                       ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
             JOIN IObGoodsReceiptSalesReturnData
                  ON IObGoodsReceiptSalesReturnData.refinstanceid = DObGoodsReceiptSalesReturn.id
             LEFT JOIN dobsalesreturnorder
                       ON iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
             LEFT JOIN cobenterprise AS PurchaseUnitData
                       ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                           AND PurchaseUnitData.objectTypeCode = '5'
             LEFT JOIN mobcustomer
                       ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
             LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
             LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
             LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = dobgoodsreceiptsalesreturn.typeid
             LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
             LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
             LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
             LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid;
