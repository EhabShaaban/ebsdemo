drop view tobgoodsreceiptstoretransactiongeneralmodel;
drop view tobgoodsissuestoretransactiongeneralmodel;
drop view tobstoretransactiongeneralmodel;
drop view tobstoretransactionalldatageneralmodel;

alter table tobstoretransaction
    alter column estimatecost type numeric;
alter table tobstoretransaction
    alter column actualcost type numeric;
alter table tobstoretransaction
    alter column quantity type numeric;
alter table tobstoretransaction
    alter column remainingquantity type numeric;

