
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingViewer_offset'),
        (select id from permission where permissionexpression = 'Costing:ReadItems'),
        now(),
        (select id
         from authorizationcondition
         where concat(objectname, ' - ', condition) =
               'Costing  - [purchaseUnitName=''Offset'']'));
