\ir 'access-rights-r4-s12/access-rights-r4-s12.sql'
\ir 'coded-business-objects-r4-s12/coded-business-objects-r4-s12.sql'
\ir 'order-r4-s12/order-r4-s12.sql'
\ir 'accounting-r4-s12/accounting-r4-s12.sql'
\ir 'accounting-r4-s12/CObExchangeRate/data-migration-scripts.sql'
\ir 'inventory-r4-s12/inventory-r4-s12.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
