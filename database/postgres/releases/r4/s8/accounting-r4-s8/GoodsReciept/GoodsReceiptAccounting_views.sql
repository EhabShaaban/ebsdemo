CREATE OR REPLACE VIEW IObGoodsReceiptAccountingDetailsGeneralModel AS
SELECT accountingDetails.id,
       accountingDetails.refinstanceid,
       dobaccountingdocument.code AS documentCode,
       accountingDetails.creationDate,
       accountingDetails.modifiedDate,
       accountingDetails.creationInfo,
       accountingDetails.modificationInfo,
       accountingDetails.accountingEntry,
       accountingDetails.subledger,
       account.id                 AS glAccountid,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       accountingDetails.amount,
       dobinventorydocument.code  AS goodsReceiptCode,
       orderDocument.id           AS glSubAccountid,
       orderDocument.code         AS glSubAccountCode,
       cobcurrency.code           AS currencyCode,
       cobcurrency.iso            AS currencyISO,
       null::json                 As glSubAccountName

    FROM IObAccountingDocumentAccountingDetails accountingDetails
         join iobgoodsreceiptaccountingdocumentaccountingdetails grAccountingDetails
              ON grAccountingDetails.id = accountingDetails.id
         INNER JOIN doborderdocument orderDocument ON orderDocument.id = grAccountingDetails.glsubaccountid
         LEFT JOIN dobgoodsreceiptaccountingdocument gr ON accountingDetails.refinstanceid = gr.id
         INNER JOIN dobaccountingdocument ON dobaccountingdocument.id = gr.id
         INNER JOIN iobgoodsreceiptaccountingdocumentdetails
                    on gr.id = iobgoodsreceiptaccountingdocumentdetails.refinstanceid
         INNER JOIN dobinventorydocument
                    on dobinventorydocument.id = iobgoodsreceiptaccountingdocumentdetails.goodsreceiptid
         INNER JOIN iobinventorycompany
                    on iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN cobenterprise company
                   ON iobinventorycompany.companyid = company.id
         LEFT JOIN cobcompany ON iobinventorycompany.companyid = cobcompany.id
         LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id
         LEFT JOIN hobglaccount account ON account.id = accountingDetails.glaccountid;
