DROP TABLE IF EXISTS DObGRAccountingDocumentJournalEntry;

CREATE TABLE DObGRAccountingDocumentJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);


alter table DObGRAccountingDocumentJournalEntry
    add CONSTRAINT FK_DObGRAccountingDocumentJournalEntry_id FOREIGN KEY (id) REFERENCES DObJournalEntry (id) On DELETE CASCADE ;



alter table DObGRAccountingDocumentJournalEntry
    add CONSTRAINT FK_DObGRAccountingDocumentJournalEntry_documentReferenceId FOREIGN KEY (documentReferenceId) REFERENCES DObGoodsReceiptAccountingDocument (id) On DELETE RESTRICT ;


