drop view if exists IObJournalEntryItemGeneralModel;
drop view if exists IObJournalEntryItemSubAccounts;

CREATE VIEW IObJournalEntryItemSubAccounts AS
SELECT JEI.id,
       JEI.type,
       CASE
           WHEN COALESCE(SubACC.code, treasur.code) IS NULL THEN CASE
                                                                     WHEN BankDetails.accountno is not null
                                                                         then concat(BankDetails.accountno, ' - ', curr.iso)
                                                                     else null end
           ELSE COALESCE(SubACC.code, treasur.code) END     as subaccountcode,
       COALESCE(SUBACC.name, treasur.name, bank.name)       as subaccountname,
       COALESCE(VENDSubACC.type, CustSubACC.type, JEI.type) as subledger
from iobjournalentryitem JEI
         left join iobjournalentryitempurchaseordersubaccount POSubACC on JEI.id = POSubACC.id
         left join iobjournalentryitemvendorsubaccount VENDSubACC on JEI.id = VENDSubACC.id
         left join iobjournalentryitemtaxsubaccount TaxSubACC on JEI.id = TaxSubACC.id
         left join iobjournalentryitemcustomersubaccount CustSubACC on JEI.id = CustSubACC.id
         left join iobjournalentryitemnotesreceivablesubaccount NotesSubACC
                   on JEI.id = NotesSubACC.id
         left join subaccountgeneralmodel SubACC
                   on (
                           (SubACC.subaccountid = POSubACC.subaccountid AND JEI.type = 'PO' AND
                            SubACC.ledger = JEI.type) OR
                           (SubACC.subaccountid = VENDSubACC.subaccountid AND
                            JEI.TYPE = 'Vendors' AND
                            SubACC.ledger = VENDSubACC.type) OR
                           (SubACC.subaccountid = TaxSubACC.subaccountid AND JEI.type = 'Taxes' AND
                            JEI.type = SubACC.ledger) OR
                           (SubACC.subaccountid = CustSubACC.subaccountid AND
                            JEI.type = 'Customers' AND
                            CustSubACC.type = SubACC.ledger) OR
                           SubACC.subaccountid = NotesSubACC.subaccountid AND
                           JEI.type = 'NotesReceivable'
                               AND SubACC.ledger = JEI.type)
         left join iobjournalentryitembanksubaccount BankSubACC
                   on JEI.id = BankSubACC.id and JEI.type = 'Banks'
         left join iobcompanybankdetails BankDetails on BankSubACC.subaccountid = BankDetails.id
         left join cobBank bank on BankDetails.bankid = bank.id
         left join cobcurrency curr on curr.id = BankDetails.currencyid
         left join iobjournalentryitemtreasurysubaccount TreasSubACC
                   on JEI.id = TreasSubACC.id and JEI.type = 'Treasuries'
         left join cobtreasury treasur on TreasSubACC.subaccountid = treasur.id;

CREATE VIEW IObJournalEntryItemGeneralModel AS
select JEI.id,
       JEI.creationdate,
       JEI.creationinfo,
       JEI.modifieddate,
       JEI.modificationinfo,
       JEI.refinstanceid,
       dobjournalentry.code,
       dobjournalentry.duedate,
       dobjournalentry.objecttypecode AS referencetype,
       SUB_ACC.subledger              as subledger,
       hobglaccountgeneralmodel.code  AS accountcode,
       hobglaccountgeneralmodel.name  AS accountname,
       SUB_ACC.subaccountcode         as subaccountcode,
       SUB_ACC.subaccountname         as subaccountname,
       JEI.debit,
       JEI.credit,
       curren.code                    AS currencycode,
       curren.iso                     AS currencyname,
       curren2.code                   AS companycurrencycode,
       curren2.iso                    AS companycurrencyname,
       JEI.currencyprice,
       purchaseunit.code              AS purchaseunitcode,
       purchaseunit.name              AS purchaseunitname,
       purchaseunit.name ->> 'en'     AS purchaseunitnameen,
       JEI.exchangerateid             AS exhangerateid,
       cobexchangerate.code           AS exchangeratecode
from iobjournalentryitem JEI
         LEFT JOIN dobjournalentry ON JEI.refinstanceid = dobjournalentry.id AND
                                      (dobjournalentry.objecttypecode = 'VendorInvoice' OR
                                       dobjournalentry.objecttypecode = 'PaymentRequest' OR
                                       dobjournalentry.objecttypecode = 'SalesInvoice' OR
                                       dobjournalentry.objecttypecode = 'Collection' OR
                                       dobjournalentry.objecttypecode = 'NotesReceivable' OR
                                       dobjournalentry.objecttypecode = 'LandedCost' OR
                                       dobjournalentry.objecttypecode = 'Settlement' OR
                                       dobjournalentry.objecttypecode = 'InitialBalanceUpload')
         left join hobglaccountgeneralmodel on (hobglaccountgeneralmodel.id = JEI.accountid)
         LEFT JOIN cobenterprise purchaseunit ON dobjournalentry.purchaseunitid = purchaseunit.id
         LEFT JOIN cobexchangerate ON cobexchangerate.id = JEI.exchangerateid
         left join cobcurrency curren on (JEI.currency = curren.id)
         left join cobcurrency curren2 on (JEI.companycurrency = curren2.id)
         left join IObJournalEntryItemSubAccounts SUB_ACC
                   on (SUB_ACC.id = JEI.id AND SUB_ACC.type = JEI.type);


CREATE or REPLACE FUNCTION getJournalEntryCodeDependsOnObjectTypeCode(objectTypeCode varchar, accouningDocumentId bigint)
    RETURNS varchar AS
$$
BEGIN
    RETURN case
               when objecttypecode = 'PaymentRequest' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobpaymentrequestjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode in ('Collection') then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobcollectionjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'SalesInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsalesinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'VendorInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobvendorinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'LandedCost' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from doblandedcostjournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'Settlement' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsettlementjournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'InitialBalanceUpload' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsettlementjournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'GoodsReceiptAccountingDocument' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from DObGRAccountingDocumentJournalEntry where documentreferenceid = accouningDocumentId))
        end;
END;
$$
    LANGUAGE PLPGSQL;