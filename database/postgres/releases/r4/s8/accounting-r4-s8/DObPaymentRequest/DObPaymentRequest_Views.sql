CREATE OR REPLACE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       paymentdetails.paymentForm,
       paymentdetails.netAmount           AS amountValue,
       cobcurrency.iso                    AS amountCurrency,
       companyData.purchaseUnitId,
       purchaseunit.name                  AS purchaseUnitName,
       purchaseunit.name:: json ->> 'en'  AS purchaseUnitNameEn,
       purchaseunit.code                  AS purchaseUnitCode,
       businesspartner.code               AS businessPartnerCode,
       businesspartner.name               AS businessPartnerName,
       duedocumenttype                    AS dueDocumentType,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           when duedocumenttype = 'CREDITNOTE' then creditNote.code
           else doborderdocument.code end as referenceDocumentCode,
       case
           when duedocumenttype = 'INVOICE' then '{
             "en": "Vendor Invoice",
             "ar": "فاتورة مورد"
           }'::json
           when duedocumenttype = 'PURCHASEORDER' then '{
             "en": "Purchase Order",
             "ar": "أمر شراء"
           }'::json
           when duedocumenttype = 'CREDITNOTE' then '{
             "en": "Credit Note",
             "ar": "الاستحقاقات الالحقاية"
           }'::json end                   as referenceDocumentType,
       DObPaymentRequest.remaining,
       paymentdetails.expectedDueDate                                                                   AS expectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS fromExpectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS toExpectedDueDate,
       paymentdetails.bankTransRef                                                                      AS bankTransRef
FROM DObPaymentRequest
         left join dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN cobenterprise purchaseunit ON purchaseunit.id = companyData.purchaseunitid
         LEFT JOIN iobpaymentrequestpaymentdetails paymentdetails on dobpaymentrequest.id = paymentdetails.refinstanceid
         LEFT JOIN mobbusinesspartner on mobbusinesspartner.id = paymentdetails.businesspartnerid
         LEFT JOIN masterdata businesspartner on mobbusinesspartner.id = businesspartner.id
         left join IObPaymentRequestInvoicePaymentDetails invoicepaymentdetails
                   on invoicepaymentdetails.id = paymentdetails.id
         left join iobpaymentrequestcreditnotepaymentdetails creditNotepaymentdetails
                   on creditNotepaymentdetails.id = paymentdetails.id
         left join IObPaymentRequestPurchaseOrderPaymentDetails orderpaymentdetails
                   on orderpaymentdetails.id = paymentdetails.id
         left join dobaccountingdocument vendorInvoice on vendorInvoice.id = invoicepaymentdetails.duedocumentid
         left join dobaccountingdocument creditNote on creditNote.id = creditNotepaymentdetails.duedocumentid
         left join doborderdocument on doborderdocument.id = orderpaymentdetails.duedocumentid
         left join cobcurrency on paymentdetails.currencyid = cobcurrency.id;



CREATE or REPLACE FUNCTION getDueDocumentCodeAccordingToPaymentType(paymentDetailsId BIGINT, dueDocumentType varchar)
    RETURNS varchar AS
$$
BEGIN
    RETURN
        (
            case
                when dueDocumentType = 'INVOICE' then (Select vendorInvoice.code from dobaccountingdocument vendorInvoice inner join dobvendorinvoice on dobvendorinvoice.id =  vendorInvoice.id
                    left join iobpaymentrequestinvoicepaymentdetails
                                                                                                                                    on vendorInvoice.id = iobpaymentrequestinvoicepaymentdetails.duedocumentid
                                                       WHERE iobpaymentrequestinvoicepaymentdetails.id = paymentDetailsId
                )
                when dueDocumentType = 'CREDITNOTE'  then (Select creditNote.code from dobaccountingdocument creditNote left join iobpaymentrequestcreditnotepaymentdetails
                                                                                                                                  on creditNote.id = iobpaymentrequestcreditnotepaymentdetails.duedocumentid
                                                           WHERE iobpaymentrequestcreditnotepaymentdetails.id = paymentDetailsId
                )
                when dueDocumentType = 'PURCHASEORDER'  then (Select doborderdocument.code from doborderdocument left join iobpaymentrequestpurchaseorderpaymentdetails

                                                                                                                           on doborderdocument.id = iobpaymentrequestpurchaseorderpaymentdetails.duedocumentid
                                                              WHERE iobpaymentrequestpurchaseorderpaymentdetails.id = paymentDetailsId)
                end
            );

END ;
$$
    LANGUAGE PLPGSQL;