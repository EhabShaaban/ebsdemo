create or replace view vendorInvoiceItemWeight as
select invoiceItem.refinstanceid                                          as invoiceId,
       invoiceItem.code                                                   as invoiceCode,
       LCDetails.purchaseorderid                                          as purchaseorderid,
       purchaseOrder.code                                               as purchaseorderCode,
       dobinventorydocument.id                                            as goodsreceiptId,
       dobinventorydocument.code                                          as goodsreceiptCode,
       LC.code                                                            as landedCostCode,
       LC.id                                                              as landedCostId,
       LC.currentstates                                                   as landedCostState,
       invoiceItem.id                                                     as itemId,
       invoiceItem.itemcode                                               as itemCode,
       invoiceItem.totalamount /
       getInvoiceTotalAmountBeforTaxes(invoiceItem.code, 'VendorInvoice') as itemWeight,
       case
           when
                   LC.currentstates like '%ActualValuesCompleted%'
               then LCSummary.actualtotalamount
           else
               LCSummary.estimatetotalamount
           end                                                            as landedCostTotal,
       invoiceItem.orderunitprice                                         as itemUnitPrice,
       (select case
                   when getAmountPaidViaAccountingDocument(vendorInvoice.code,
                                                           vendorInvoiceCompany.objecttypecode) =
                        0.0
                       then (select avg(iobAccountingdocumentactivationdetails.currencyprice)
                             from iobAccountingdocumentactivationdetails
                                      left JOIN iobpaymentrequestpaymentdetails
                                                on iobpaymentrequestpaymentdetails.refinstanceid =
                                                   iobAccountingdocumentactivationdetails.refinstanceid
                                      left join iobpaymentrequestinvoicepaymentdetails
                                                on iobpaymentrequestinvoicepaymentdetails.id =
                                                   iobpaymentrequestpaymentdetails.id
                             where iobpaymentrequestinvoicepaymentdetails.duedocumentid =
                                   LCDetails.vendorinvoiceid
                   )
                   else (
                       select latestexchangerate.currencyPrice
                       from getLatestCurrencyPrice(iobvendorinvoicepurchaseorder.currencyid,
                                                   iobenterprisebasicdata.currencyid) latestexchangerate
                   )
                   end ) as currencyPrice,
       invoiceItem.orderunitcode,
       invoiceItem.orderunitsymbol
from IObVendorInvoiceItemsGeneralModel invoiceItem
         left join ioblandedcostdetails LCDetails
                   on invoiceItem.refinstanceid = LCDetails.vendorinvoiceId
         left join dobaccountingdocument vendorInvoice
                   on vendorInvoice.id = LCDetails.vendorinvoiceId
         inner join dobvendorinvoice on dobvendorinvoice.id = vendorInvoice.id
         left join doborderdocument purchaseOrder on purchaseOrder.id = LCDetails.purchaseorderid
         left join iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = LCDetails.vendorinvoiceid
         left join iobaccountingdocumentcompanydata vendorInvoiceCompany on vendorInvoiceCompany.refinstanceid = LCDetails.vendorinvoiceId and vendorInvoiceCompany.objecttypecode = 'VendorInvoice'
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = vendorInvoiceCompany.companyid
         inner join dobaccountingdocument LC
                    on LCDetails.refinstanceid = LC.id and
                       LC.objecttypecode = 'LandedCost' and LC.currentstates not like '%Draft%'
         left join IObLandedCostFactorItemsSummary LCSummary on LCSummary.id = LC.id
         inner join iobgoodsreceiptpurchaseorderdata
                    on LCDetails.purchaseorderid = iobgoodsreceiptpurchaseorderdata.purchaseorderid
         inner join dobinventorydocument on dobinventorydocument.id = iobgoodsreceiptpurchaseorderdata.refinstanceid
    and dobinventorydocument.currentstates like '%Active%';
