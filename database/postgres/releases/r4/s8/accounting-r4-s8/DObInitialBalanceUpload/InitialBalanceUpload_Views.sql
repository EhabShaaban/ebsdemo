
CREATE OR REPLACE VIEW DObInitialBalanceUploadJournalEntryPreparationGeneralModel AS
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       iobaccountingdocumentcompanydata.purchaseunitid as businessunitId,
       iobaccountingdocumentcompanydata.companyid,
       dobinitialbalanceupload.fiscalperiodid,
       companyData.currencyid companyCurrencyId
from dobaccountingdocument
         left join dobinitialbalanceupload on dobaccountingdocument.id = dobinitialbalanceupload.id
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
WHERE dobaccountingdocument.objecttypecode = 'IBU'
  And iobaccountingdocumentcompanydata.objecttypecode = 'InitialBalanceUpload';


CREATE OR REPLACE VIEW DObInitialBalanceUploadJournalEntryItemPreparationGeneralModel AS
select dobaccountingdocument.code,
       iobinitialbalanceuploaditem.id,
       iobinitialbalanceuploaditem.glaccountid,
       iobinitialbalanceuploaditem.credit,
       iobinitialbalanceuploaditem.debit,
       iobinitialbalanceuploaditem.currencyid,
       iobinitialbalanceuploaditem.currencyprice,
       iobinitialbalanceuploaditem.subledger,
       iobinitialbalanceuploadvendoritem.vendorid
from dobaccountingdocument
         left join iobinitialbalanceuploaditem on dobaccountingdocument.id = iobinitialbalanceuploaditem.refinstanceid
         left join iobinitialbalanceuploadvendoritem on iobinitialbalanceuploaditem.id = iobinitialbalanceuploadvendoritem.id
WHERE dobaccountingdocument.objecttypecode = 'IBU';
