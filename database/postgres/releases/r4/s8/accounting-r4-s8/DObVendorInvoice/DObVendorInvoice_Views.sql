DROP VIEW If EXISTS IObVendorInvoiceRemainingGeneralModel;
DROP VIEW If EXISTS IObInvoiceSummaryGeneralModel;
DROP VIEW If EXISTS DObVendorInvoiceGeneralModel;

CREATE OR REPLACE VIEW DObVendorInvoiceGeneralModel AS
SELECT dobvendorInvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                                                     AS vendorCode,
       masterdata.name                                                     AS vendorName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en') AS vendorCodeName,
       dobaccountingdocument.objecttypecode                                AS invoiceType,
       purchaseUnit.code                                                   AS purchaseUnitCode,
       purchaseUnit.name                                                   AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                  AS purchaseUnitNameEn,
       company.code                                                        AS companyCode,
       company.name                                                        AS companyName,
       cobcurrency.code                                                    AS currencyCode,
       cobcurrency.name                                                    AS currencyName,
       cobcurrency.iso                                                     AS currencyISO,
       cobpaymentterms.code                                                AS paymentTermCode,
       doborderdocument.code                                               AS purchaseOrderCode,
       iobvendorinvoicedetails.invoiceNumber                               AS invoiceNumber
FROM dobvendorInvoice
         left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN iobvendorinvoicedetails
                   ON iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id
         LEFT JOIN masterdata ON iobvendorinvoicedetails.vendorId = masterdata.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit
                   ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
                       AND purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobenterprise company ON company.id = iobvendorinvoicecompanydata.companyId
    AND company.objecttypecode = '1'
         LEFT JOIN IObVendorInvoicePurchaseOrder
                   ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms
                   ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument
                   ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId AND
                      doborderdocument.objecttypecode LIKE '%_PO';


CREATE or REPLACE VIEW IObVendorInvoiceItemsGeneralModel AS
SELECT IObVendorInvoiceItem.id,
       vendorInvoice.code,
       item.code                                                                               as itemCode,
       item.id                                                                                 as itemId,
       item.name                                                                               as itemName,
       baseUnitOfMeasure.code                                                                  as baseUnitCode,
       baseUnitOfMeasure.symbol                                                                as baseUnitSymbol,
       orderUnitOfMeasure.code                                                                 as orderUnitCode,
       orderUnitOfMeasure.symbol                                                               as orderunitsymbol,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  AS conversionFactorToBase,
       ((IObVendorInvoiceItem.quantityinorderunit *
         getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                   IObVendorInvoiceItem.itemid)) * IObVendorInvoiceItem.price) as totalAmount,
       IObVendorInvoiceItem.creationDate,
       IObVendorInvoiceItem.modifiedDate,
       IObVendorInvoiceItem.creationInfo,
       IObVendorInvoiceItem.modificationInfo,
       IObVendorInvoiceItem.quantityinorderunit,
       IObVendorInvoiceItem.refinstanceid,
       IObVendorInvoiceItem.price,
       IObVendorInvoiceItem.price *
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  as orderUnitPrice

FROM IObVendorInvoiceItem
         left join masterdata item on IObVendorInvoiceItem.itemId = item.id
         left join cobmeasure orderUnitOfMeasure on IObVendorInvoiceItem.orderunitofmeasureid = orderUnitOfMeasure.id
         left join mobitem on IObVendorInvoiceItem.itemId = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument vendorInvoice on IObVendorInvoiceItem.refInstanceId = vendorInvoice.id
         left join dobvendorinvoice on vendorInvoice.id = dobvendorinvoice.id;

CREATE or REPLACE VIEW IObVendorInvoiceTaxesGeneralModel AS
SELECT IObVendorInvoiceTaxes.id,
       IObVendorInvoiceTaxes.creationDate,
       IObVendorInvoiceTaxes.modifiedDate,
       IObVendorInvoiceTaxes.creationInfo,
       IObVendorInvoiceTaxes.modificationInfo,
       IObVendorInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code   AS invoicecode,
       IObVendorInvoiceTaxes.code   AS taxcode,
       IObVendorInvoiceTaxes.name   AS taxname,
       LObTaxInfo.percentage        as taxpercentage,
       IObVendorInvoiceTaxes.amount as taxAmount
FROM IObVendorInvoiceTaxes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = IObVendorInvoiceTaxes.refInstanceId
         inner join dobvendorInvoice on dobvendorInvoice.id = IObVendorInvoiceTaxes.refinstanceid
         left join LObTaxInfo on LObTaxInfo.code = IObVendorInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis


create or replace view dobVendorInvoicePostPreparationGeneralModel as
select dobaccountingdocument.id,
       code,
       IObVendorInvoicePurchaseOrder.currencyid,
       latestexchangerate.exchangerateid as exchangerateid,
       latestexchangerate.currencyprice  as currencyprice
from dobaccountingdocument
       inner join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         inner join iobaccountingdocumentcompanydata
                    on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         inner join iobenterprisebasicdata companydata
                    on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         inner join IObVendorInvoicePurchaseOrder
                    on dobaccountingdocument.id = iobvendorinvoicepurchaseorder.refinstanceid
         inner join getLatestCurrencyPrice(IObVendorInvoicePurchaseOrder.currencyid,
                                           companydata.currencyid) latestexchangerate
                    on latestexchangerate.firstcurrencyid = IObVendorInvoicePurchaseOrder.currencyid
                        and latestexchangerate.secondcurrencyid = companydata.currencyid;


DROP FUNCTION if exists isGoodsInvoice(character varying);
CREATE FUNCTION isGoodsInvoice(objectTypeCode VARCHAR)
    RETURNS BOOLEAN AS
$$
BEGIN
    RETURN CASE
               WHEN objectTypeCode = 'LOCAL_GOODS_INVOICE' or objecttypecode = 'IMPORT_GOODS_INVOICE'  THEN TRUE
               else FALSE
        end;
END;
$$
    LANGUAGE PLPGSQL;


DROP FUNCTION if exists isPurchaseServiceLocalInvoice(character varying);
CREATE FUNCTION isPurchaseServiceLocalInvoice(objectTypeCode VARCHAR)
    RETURNS BOOLEAN AS
$$
BEGIN
    RETURN CASE
               WHEN objecttypecode = 'CUSTOM_TRUSTEE_INVOICE' or
                    objecttypecode = 'SHIPMENT_INVOICE' or objecttypecode = 'INSURANCE_INVOICE'  THEN TRUE
               else FALSE
        end;
END;
$$
    LANGUAGE PLPGSQL;


DROP FUNCTION if exists isVendorInvoice(character varying);
CREATE FUNCTION isVendorInvoice(objectTypeCode VARCHAR)
    RETURNS BOOLEAN AS
$$
BEGIN
    RETURN CASE
               WHEN isGoodsInvoice(objecttypecode) or isPurchaseServiceLocalInvoice(objecttypecode)  THEN TRUE

               else FALSE
        end;
END;
$$
    LANGUAGE PLPGSQL;


DROP FUNCTION if exists isInvoice(character varying);
CREATE FUNCTION isInvoice(objectTypeCode VARCHAR)
    RETURNS BOOLEAN AS
$$
BEGIN
    RETURN CASE
               WHEN objectTypeCode = 'SalesInvoice' or isVendorInvoice(objecttypecode)  THEN TRUE
               else FALSE
        end;
END;
$$
    LANGUAGE PLPGSQL;


CREATE or REPLACE FUNCTION getInvoiceTotalAmountBeforTaxes(invoiceCode VARCHAR, invoiceType VARCHAR)
    RETURNS float AS $$
BEGIN
    RETURN
        CASE WHEN invoiceType = 'SalesInvoice' THEN
                 COALESCE((SELECT
                               (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * (case when invoiceItem.conversionfactortobase is null then 1 else invoiceItem.conversionfactortobase end ))) AS totalAmount
                                FROM iobsalesinvoiceitemgeneralmodel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                           FROM dobsalesinvoice dobInvoice
                                    LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobInvoice.id
                           WHERE dobaccountingdocument.objecttypecode = 'SalesInvoice' AND dobaccountingdocument.code = invoiceCode
                          ),0)
             ELSE
                 COALESCE((SELECT
                               (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * invoiceItem.conversionfactortobase)) AS totalAmount
                                FROM IObVendorInvoiceItemsGeneralModel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                           FROM dobvendorInvoice dobvendorInvoice
                                    LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
                           WHERE dobaccountingdocument.code = invoiceCode
                          ),0)
            END;
END; $$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW IObInvoiceSummaryGeneralModel AS
SELECT dobaccountingdocument.id                              as id,
       code                                                  as invoiceCode,
        (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                'SalesInvoice'
            WHEN isVendorInvoice(objectTypeCode) THEN
                'VendorInvoice' END)     as invoiceType,
       getInvoiceTotalAmountBeforTaxes(code, objecttypecode) as totalAmountBeforeTaxes,
       getInvoiceTotalTaxAmount(code, objecttypecode)        as taxAmount,
       getInvoiceNetAmount(code, objecttypecode)             as netAmount,
       (CASE
            WHEN objecttypecode = 'SalesInvoice' THEN
                getDownPaymentForInvoice(code, objecttypecode)
            WHEN isVendorInvoice(objectTypeCode) THEN
                iobvendorinvoicesummary.downpayment END)     as downpayment,
       (CASE  WHEN objecttypecode = 'SalesInvoice' THEN
                SiSummary.remaining
                WHEN isVendorInvoice(objectTypeCode) THEN
           iobvendorinvoicesummary.remaining END)     as totalRemaining
FROM dobaccountingdocument
         LEFT JOIN IObSalesInvoiceSummary SiSummary
                   ON SiSummary.refInstanceId = dobaccountingdocument.id AND
                      dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join iobvendorinvoicesummary on IObVendorInvoiceSummary.refinstanceid = dobaccountingdocument.id
WHERE isInvoice(objecttypecode) ;



--TODO: recreate view without joining with DObVendorInvoiceGeneralModel
CREATE or REPLACE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE WHEN invoiceSummary.totalremaining IS NULL THEN 0 ELSE invoiceSummary.totalremaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
                   ON dobInvoice.id = invoiceSummary.id;



CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode                              AS documentReferenceType,
       DObJournalEntry.dueDate,
       company.code                                                AS companyCode,
       company.name                                                AS companyName,
       purchaseUnit.code                                           AS purchaseUnitCode,
       purchaseUnit.name                                           AS purchaseUnitName,
       purchaseUnit.name:: json ->> 'en'                           AS purchaseUnitNameEn,
       (SELECT dobaccountingdocument.code
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObVendorInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
          AND  isVendorInvoice(dobaccountingdocument.objecttypecode)
        UNION
        SELECT dobaccountingdocument.code
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObPaymentRequestJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSalesInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcollectionJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON dobcollectionJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcollectionJournalEntry.id
          AND dobaccountingdocument.objecttypecode in ('C_SI', 'C_NR', 'C_SO','C_DN')
        UNION
        SELECT dobaccountingdocument.code
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobmonetarynotes ON dobNotesReceivableJournalEntry.documentreferenceid =
                                               dobmonetarynotes.id
                 LEFT JOIN dobaccountingdocument ON dobmonetarynotes.id = dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM doblandedcostjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON doblandedcostjournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = doblandedcostjournalentry.id
          and dobaccountingdocument.objecttypecode = 'LandedCost'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSettlementJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSettlementJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSettlementJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'Settlement'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObInitialBalanceUploadJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObInitialBalanceUploadJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObInitialBalanceUploadJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'IBU'
       ) AS documentReferenceCode,

       fiscalPeriod.name::json ->> 'en'                            AS FiscalPeriod
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company
                   ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join CObFiscalPeriod fiscalPeriod
                   on DObJournalEntry.fiscalPeriodId = fiscalPeriod.id;




 CREATE OR REPLACE VIEW DObVendorInvoiceCreditPreparationGeneralModel AS
SELECT dobaccountingdocument.id ,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companyData.currencyid as companyCurrencyId,
       iobvendorinvoicepurchaseorder.currencyid as documentCurrencyId,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
     iobvendoraccountinginfo.chartofaccountid as glaccountId,
    iobvendorinvoicedetails.vendorid as glsubaccountId,
    leafglaccount.leadger as accountLedger,
    getInvoiceNetAmount(code, dobaccountingdocument.objecttypecode)             as amount
from dobaccountingdocument
    left join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         left join iobaccountingdocumentactivationdetails activationdetails
                   on dobaccountingdocument.id = activationdetails.refinstanceid
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
left join iobvendorinvoicepurchaseorder on iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
left join iobvendorinvoicedetails on iobvendorinvoicedetails.refinstanceid = dobvendorinvoice.id
left join iobvendoraccountinginfo on iobvendoraccountinginfo.refinstanceid = iobvendorinvoicedetails.vendorid
left join leafglaccount on leafglaccount.id = iobvendoraccountinginfo.chartofaccountid;

  CREATE OR REPLACE VIEW DObVendorInvoiceDebitPreparationGeneralModel AS
 SELECT ROW_NUMBER() OVER ()                                       as id,
       dobaccountingdocument.code,
       dobaccountingdocument.objecttypecode,
       companyData.currencyid                                     as companyCurrencyId,
       iobvendorinvoicepurchaseorder.currencyid                   as documentCurrencyId,
       activationdetails.currencyprice,
       activationdetails.exchangerateid,
       iobitemaccountinginfo.chartofaccountid                     as glaccountId,
       CASE
           when  isGoodsInvoice(dobaccountingdocument.objecttypecode)
               then iobvendorinvoicepurchaseorder.purchaseorderid
           else iobpurchaseorderfulfillervendor.referencepoid end as glsubaccountId,
       leafglaccount.leadger                                      as accountLedger,
       Case
           when (Select count(*)
                 from iobvendorinvoicetaxes
                 where iobvendorinvoicetaxes.refinstanceid = iobvendorinvoiceitem.refinstanceid) = 0
               then (Select (item.quantityinorderunit * item.price)
                     from iobvendorinvoiceitem item
                     where item.id = iobvendorinvoiceitem.id
                     GROUP BY item.id
           )
           else
               (Select (iobvendorinvoiceitem.quantityinorderunit * iobvendorinvoiceitem.price) + sum(
                           (iobvendorinvoiceitem.quantityinorderunit * iobvendorinvoiceitem.price) *
                           (1 / iobvendorinvoicetaxes.percentage))
                from iobvendorinvoicetaxes
                where iobvendorinvoicetaxes.refinstanceid = iobvendorinvoiceitem.refinstanceid
                GROUP BY iobvendorinvoiceitem.id
               ) end                                              as amount

from dobaccountingdocument
         inner join dobvendorinvoice on dobaccountingdocument.id = dobvendorinvoice.id
         left join iobaccountingdocumentactivationdetails activationdetails
                   on dobaccountingdocument.id = activationdetails.refinstanceid
         left join iobaccountingdocumentcompanydata
                   on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         left join iobenterprisebasicdata companyData
                   on companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
         left join iobvendorinvoicepurchaseorder on iobvendorinvoicepurchaseorder.refinstanceid = dobvendorinvoice.id
         left join iobpurchaseorderfulfillervendor
                   on iobpurchaseorderfulfillervendor.refinstanceid = iobvendorinvoicepurchaseorder.purchaseorderid
         left join iobvendorinvoiceitem on iobvendorinvoiceitem.refinstanceid = dobvendorinvoice.id
         left join iobitemaccountinginfo on iobitemaccountinginfo.refinstanceid = iobvendorinvoiceitem.itemid
         left join leafglaccount on leafglaccount.id = iobitemaccountinginfo.chartofaccountid;