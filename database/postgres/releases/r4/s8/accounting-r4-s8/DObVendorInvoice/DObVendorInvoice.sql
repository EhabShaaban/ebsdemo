update  dobaccountingdocument
set objecttypecode =  (Select cobvendorinvoice.code from dobvendorinvoice left join cobvendorinvoice on dobvendorinvoice.typeid = cobvendorinvoice.id
                       where dobvendorinvoice.id = dobaccountingdocument.id )
where dobaccountingdocument.objecttypecode = 'VendorInvoice'
;


update dobaccountingdocument
set objecttypecode = (Select CASE
                                 when doborderdocument.objecttypecode = 'LOCAL_PO' then 'LOCAL_GOODS_INVOICE'
                                 when doborderdocument.objecttypecode = 'IMPORT_PO' then 'IMPORT_GOODS_INVOICE' end
                      from iobvendorinvoicepurchaseorder
                               left join doborderdocument
                                         on doborderdocument.id = iobvendorinvoicepurchaseorder.purchaseorderid
                      where dobaccountingdocument.id = iobvendorinvoicepurchaseorder.refinstanceid)
where dobaccountingdocument.objecttypecode = 'GOODS_INVOICE';

