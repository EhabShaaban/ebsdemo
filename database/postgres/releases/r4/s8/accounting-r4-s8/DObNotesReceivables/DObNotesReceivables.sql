ALTER TABLE iobmonetarynotesdetails
    ADD COLUMN remaining       DOUBLE PRECISION;

UPDATE iobmonetarynotesdetails
SET remaining = dobmonetarynotes.remaining
FROM dobmonetarynotes
WHERE iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id;

DROP VIEW DObNotesReceivablesGeneralModel;

ALTER TABLE dobmonetarynotes
    DROP COLUMN remaining;

ALTER TABLE dobmonetarynotes
    DROP COLUMN method;

UPDATE dobaccountingdocument
SET objecttypecode = 'NOTES_RECEIVABLE_AS_COLLECTION'
FROM dobmonetarynotes
WHERE dobaccountingdocument.id = dobmonetarynotes.id;

ALTER TABLE iobmonetarynotesdetails
    ADD COLUMN noteForm VARCHAR(1024) NOT NULL DEFAULT 'CHEQUE';

ALTER TABLE iobmonetarynotesdetails alter column duedate drop not null;
ALTER TABLE iobmonetarynotesdetails alter column noteNumber drop not null;
ALTER TABLE iobmonetarynotesdetails alter column noteBank drop not null;
ALTER TABLE iobmonetarynotesdetails alter column depot drop not null;


CREATE or REPLACE FUNCTION getAmountFromPostedCollectionRequests(SICode varchar)
    RETURNS float AS $$
BEGIN
RETURN case when (select SUM (DObCollectionRequestGeneralModel.amount)
                  from DObCollectionRequestGeneralModel
                  where DObCollectionRequestGeneralModel.refdocumentcode = SICode
                    and DObCollectionRequestGeneralModel.currentStates like '%Active%'
                    and DObCollectionRequestGeneralModel.refdocumenttypecode like '%SALES_INVOICE%') is null then 0
            else (select SUM (DObCollectionRequestGeneralModel.amount)
                  from DObCollectionRequestGeneralModel
                  where DObCollectionRequestGeneralModel.refdocumentcode = SICode
                    and DObCollectionRequestGeneralModel.currentStates like '%Active%'
                    and DObCollectionRequestGeneralModel.refdocumenttypecode like '%SALES_INVOICE%')  end;
END; $$
LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getAmountFromPostedNotesReceivalbles(SICode varchar)
    RETURNS float AS $$
BEGIN
RETURN case when (select SUM (DObNotesReceivablesSimpleGeneralModel.amount)
                  from DObNotesReceivablesSimpleGeneralModel
                  where DObNotesReceivablesSimpleGeneralModel.salesInvoiceCode = SICode
                    and DObNotesReceivablesSimpleGeneralModel.currentStates like '%Active%'
                    and DObNotesReceivablesSimpleGeneralModel.refdocumenttype like '%SALES_INVOICE%') is null then 0
            else (select SUM (DObNotesReceivablesSimpleGeneralModel.amount)
                  from DObNotesReceivablesSimpleGeneralModel
                  where DObNotesReceivablesSimpleGeneralModel.salesInvoiceCode = SICode
                    and DObNotesReceivablesSimpleGeneralModel.currentStates like '%Active%'
                    and DObNotesReceivablesSimpleGeneralModel.refdocumenttype like '%SALES_INVOICE%')  end;
END; $$
LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getNotesReceivablePaidAmount(NRCode varchar)
  RETURNS float AS $$
DECLARE remaining float;
BEGIN
  remaining = (
              select SUM(DObCollectionRequestGeneralModel.amount)
              from DObCollectionRequestGeneralModel
              where DObCollectionRequestGeneralModel.refdocumentcode = NRCode
                and DObCollectionRequestGeneralModel.currentStates like '%Active%'
                and DObCollectionRequestGeneralModel.refdocumenttypecode like '%NOTES_RECEIVABLE%');
return case when
                remaining is null then 0
            else remaining
    end;
END; $$
LANGUAGE PLPGSQL;