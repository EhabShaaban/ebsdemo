DROP VIEW IF EXISTS DObNotesReceivablesGeneralModel;
CREATE VIEW DObNotesReceivablesGeneralModel AS
SELECT dobmonetarynotes.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.objecttypecode,
       MasterData.code                                          AS customerCode,
       MasterData.name                                          AS customerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
       iobmonetarynotesdetails.remaining,
       dobmonetarynotes.documentownerid,
       userinfo.name                                            AS documentOwnerName,
       concat(Company.code, ' - ', Company.name::json ->> 'en') AS companyCodeName,
       Company.code                                             AS companyCode,
       Company.name                                             AS companyName,
       iobmonetarynotesdetails.notenumber,
       iobmonetarynotesdetails.dueDate,
       iobmonetarynotesdetails.amount,
       CObCurrency.iso                                          AS currencyIso,
       iobmonetarynotesdetails.notebank,
       iobmonetarynotesdetails.noteForm
FROM dobmonetarynotes
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = dobmonetarynotes.id AND
                      objecttypecode = 'NOTES_RECEIVABLE_AS_COLLECTION'
         LEFT JOIN userinfo ON dobmonetarynotes.documentownerid = userinfo.id
         LEFT JOIN ebsuser ON dobmonetarynotes.documentownerid = ebsuser.id
         LEFT JOIN iobmonetarynotesdetails
                   ON iobmonetarynotesdetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN CObEnterprise BusinessUnit
                   ON iobaccountingdocumentcompanydata.purchaseunitid = BusinessUnit.id AND
                      BusinessUnit.objecttypecode = '5'
         LEFT JOIN CObEnterprise Company
                   ON iobaccountingdocumentcompanydata.companyid = Company.id AND
                      Company.objecttypecode = '1'
         LEFT JOIN CObCurrency ON CObCurrency.id = iobmonetarynotesdetails.currencyid
         LEFT JOIN MasterData ON MasterData.id = iobmonetarynotesdetails.businesspartnerid AND
                                 MasterData.objecttypecode = '3';

CREATE VIEW IObNotesReceivableAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer'
               THEN (
               customer.code
               )
           WHEN accountDetails.subledger = 'Notes_Receivables'
               THEN (
               dobaccountingdocument.code
               )
           END                    AS glSubAccountCode,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer' THEN
               (customeraccountingdetails.glSubAccountId)
           WHEN accountDetails.subledger = 'Notes_Receivables'
               THEN
               (notesreceivableaccountingdetails.glSubAccountId)
           END                    AS glSubAccountId,
       CASE
           WHEN accountDetails.subledger = 'Local_Customer'
               THEN (customer.name)
           END                    AS glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         LEFT JOIN dobmonetarynotes ON accountDetails.refinstanceid = dobmonetarynotes.id
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = dobmonetarynotes.id
         LEFT JOIN hobglaccount account ON account.id = accountDetails.glaccountid
         LEFT JOIN iobaccountingdocumentcustomeraccountingDetails customeraccountingdetails
                   ON customeraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'
         LEFT JOIN iobaccountingdocumentnotesreceivableaccountingdetails notesreceivableaccountingdetails
                   ON notesreceivableaccountingdetails.id = accountDetails.id
                       AND accountDetails.accountingEntry = 'DEBIT'
                       AND dobaccountingdocument.id = notesreceivableaccountingdetails.glSubAccountId
         LEFT JOIN masterdata customer
                   ON customer.id = customeraccountingdetails.glSubAccountId AND
                      customer.objecttypecode = '3'
where accountDetails.objecttypecode = 'NOTES_RECEIVABLE';