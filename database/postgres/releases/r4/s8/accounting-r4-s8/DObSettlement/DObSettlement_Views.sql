CREATE OR REPLACE VIEW IObSettlementAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code                                                    AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger                                                      AS subLedger,
       glAccount.code										                         as glAccountCode,
       glAccount.name														      	 as glAccountName,
       accountDetails.glaccountid													 as glAccountId,
       accountDetails.amount,
       glSubAccount.code															 as glSubAccountCode,
       glSubAccount.subaccountid  													 as glSubAccountId,
       glSubAccount.name    														 as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObSettlement settlement on accountDetails.refinstanceid = settlement.id
         left join dobaccountingdocument on dobaccountingdocument.id = settlement.id
         left join hobglaccountgeneralmodel glAccount on glAccount.id = accountDetails.glaccountid
         left join SubAccountGeneralModel glSubAccount
                   on accountDetails.subledger = glSubAccount.ledger
                       and glSubAccount.subaccountid = (
                           case
                               when accountDetails.subledger = 'Banks'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentBankAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Local_Customer'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentCustomerAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Notes_Receivables'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentNotesReceivableAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'PO'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentOrderAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Treasuries'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentTreasuryAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )

                               when accountDetails.subledger = 'Local_Vendors'
                                   then (
                                   SELECT subDetails.glSubAccountId
                                   FROM IObAccountingDocumentVendorAccountingDetails subDetails
                                   WHERE subDetails.id = accountDetails.id
                               )
                               end
                           ) ;

