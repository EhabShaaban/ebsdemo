CREATE OR REPLACE VIEW DObLandedCostItemsInvoicesGeneralModel
AS
select row_number() OVER () as id, dobaccountingdocument.code as vendorInvoiceCode, doborderdocument.code as purchaseOrderCode ,
       masterdata.id as itemId,
       ((iobvendorinvoiceitem.quantityinorderunit* getConversionFactorToBase(iobvendorinvoiceitem.baseunitofmeasureid, iobvendorinvoiceitem.orderunitofmeasureid, iobvendorinvoiceitem.itemid)) * iobvendorinvoiceitem.price  )           as totalAmount
from iobvendorinvoiceitem left join dobaccountingdocument
                                    on iobvendorinvoiceitem.refinstanceid = dobaccountingdocument.id
                          left join dobvendorinvoice on dobvendorinvoice.id = dobaccountingdocument.id
                          left join iobvendorinvoicedetails on iobvendorinvoicedetails.refinstanceid = dobaccountingdocument.id
                          left join iobvendorinvoicepurchaseorder on iobvendorinvoicedetails.id = iobvendorinvoicepurchaseorder.id
                          left join doborderdocument on iobvendorinvoicepurchaseorder.purchaseorderid = doborderdocument.id
                          left join masterdata on masterdata.id = iobvendorinvoiceitem.itemid
where isPurchaseServiceLocalInvoice(dobaccountingdocument.objecttypecode) and dobaccountingdocument.currentstates like '%Posted%';
