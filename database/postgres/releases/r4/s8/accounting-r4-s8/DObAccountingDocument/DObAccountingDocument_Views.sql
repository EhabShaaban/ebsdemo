CREATE OR REPLACE VIEW DObAccountingJournalEntryPreparationGeneralModel AS
    SELECT dobaccountingdocument.id ,
           dobaccountingdocument.code,
           dobaccountingdocument.objecttypecode,
           iobaccountingdocumentcompanydata.purchaseunitid as businessunitid,
           iobaccountingdocumentcompanydata.companyid,
           activationdetails.fiscalperiodid,
           activationdetails.duedate
    from dobaccountingdocument
                               left join iobaccountingdocumentactivationdetails activationdetails
                                         on dobaccountingdocument.id = activationdetails.refinstanceid
                               left join iobaccountingdocumentcompanydata
                                         on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid;