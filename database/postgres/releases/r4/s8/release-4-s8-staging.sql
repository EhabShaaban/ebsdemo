\ir 'accounting-r4-s8/accounting-r4-s8.sql'
\ir 'masterdata-r4-s8/masterdata-r4-s8.sql'
\ir 'access-rights-r4-s8/access-rights-r4-s8.sql'
\ir 'inventory-r4-s8/inventory-r4-s8.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
