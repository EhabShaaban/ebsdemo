INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'NotesReceivableDocumentOwner'),
        (select id from permission where permissionexpression = 'NotesReceivable:CanBeDocumentOwner'),
        '2021-04-04 11:53:03+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GRViewer_Offset'),
        (select id from permission where permissionexpression = 'GoodsReceipt:ReadAccountingDocumentActivationDetails'),
        '2021-04-04 11:53:03+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'GoodsReceipt - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'GRViewer'),
        (select id from permission where permissionexpression = 'GoodsReceipt:ReadAccountingDocumentActivationDetails'),
        '2021-04-04 11:54:04+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'GRActivateOwner_Offset_MadinaMain'),
        (select id from permission where permissionexpression = 'GoodsReceipt:Activate'),
        '2021-04-04 11:54:04+00',
        (select id from authorizationcondition where condition = '([purchaseUnitName=''Offset''] && [storehouseCode=''0001''])')
               );

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NROwner_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:UpdateNotesReceivableDetails'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NROwner_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:AddReferenceDocument'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NROwner_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:DeleteReferenceDocument'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NRViewer_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadNotesReceivableDetails'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NRViewer_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadAccountingDetails'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NRViewer_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadActivationDetails'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NRViewer_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadCompanyData'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime,authorizationconditionid)
VALUES ((select id from role where roleexpression = 'NRViewer_Offset'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadReferenceDocuments'),
        '2021-04-19 11:54:04+00',(select id
                                  from authorizationcondition
                                  where concat(objectname, ' - ', condition) =
                                        'NotesReceivable - [purchaseUnitName=''Offset'']'));


INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'NRViewer'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadNotesReceivableDetails'),
        '2021-04-19 11:54:04+00');
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'NRViewer'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadAccountingDetails'),
        '2021-04-19 11:54:04+00');
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'NRViewer'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadActivationDetails'),
        '2021-04-19 11:54:04+00');
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'NRViewer'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadCompanyData'),
        '2021-04-19 11:54:04+00');
INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'NRViewer'),
        (select id from permission where permissionexpression = 'NotesReceivable:ReadReferenceDocuments'),
        '2021-04-19 11:54:04+00');


INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'VendorInvoiceDocumentOwner'),
        (select id from permission where permissionexpression = 'VendorInvoice:CanBeDocumentOwner'),
        '2021-04-04 11:53:03+00');