INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:CanBeDocumentOwner', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:ReadCompanyData', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:ReadReferenceDocuments', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('GoodsReceipt:ReadAccountingDocumentActivationDetails', null, null, null, 'GoodsReceipt');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:ReadNotesReceivableDetails', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:ReadAccountingDetails', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:ReadActivationDetails', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:UpdateNotesReceivableDetails', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:AddReferenceDocument', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('NotesReceivable:DeleteReferenceDocument', null, null, null, 'NotesReceivable');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('VendorInvoice:CanBeDocumentOwner', null, null, null, 'VendorInvoice');

UPDATE permission SET permissionexpression = 'NotesReceivable:Activate' WHERE permissionexpression = 'NotesReceivable:DeliverToTreasury';

UPDATE permission
SET
    permissionexpression = REPLACE(permissionexpression,'Invoice','VendorInvoice')
WHERE
        objectname = 'Invoice';


UPDATE permission
SET
    objectname = 'VendorInvoice'
WHERE
        objectname = 'Invoice';