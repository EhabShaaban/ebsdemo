INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_MadinaMain'),
        (select id from role where roleexpression = 'GRActivateOwner_Offset_MadinaMain'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_MadinaMain'),
        (select id from role where roleexpression = 'GRViewer_Offset'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Storekeeper_Offset_MadinaMain'),
        (select id from role where roleexpression = 'GRTypeViewer'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Accountant_offset'),
        (select id from role where roleexpression = 'VendorInvoiceDocumentOwner'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'AccountantHead_offset'),
        (select id from role where roleexpression = 'VendorInvoiceDocumentOwner'));
