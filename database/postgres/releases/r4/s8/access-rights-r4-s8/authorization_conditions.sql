INSERT INTO authorizationcondition (objectname, condition) VALUES ('GoodsReceipt', '([purchaseUnitName=''Offset''] && [storehouseCode=''0001''])');

UPDATE authorizationcondition
SET
    objectname = 'VendorInvoice'
WHERE
        objectname = 'Invoice';