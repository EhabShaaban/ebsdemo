drop view if exists IObGoodsReceiptItemQuantitiesGeneralModel;

CREATE VIEW IObGoodsReceiptItemQuantitiesGeneralModel AS
select concat(goodsReceipt.id, itemQuantity.id, itemData.id) as id,
       inventoryDocument.code                                as goodsReceiptCode,
       itemData.id                                           as itemId,
       itemData.code                                         as itemCode,
       itemQuantity.receivedqtyuoe                           as quantity,
       itemQuantity.stocktype                                as stockType,
       itemUnitOfMeasure.id                                  as unitOfMeasureId,
       itemUnitOfMeasure.code                                as unitOfMeasureCode,
       goodsReceiptPurchaseUnit.id                           as purchaseUnitId,
       goodsReceiptPurchaseUnit.code                         as purchaseUnitCode,
       goodsReceiptCompany.id                                as companyId,
       goodsReceiptCompany.code                              as companyCode,
       goodsReceiptStorehouse.id                             as storehouseId,
       goodsReceiptStorehouse.code                           as storehouseCode,
       goodsReceiptPlant.id                                  as plantId,
       goodsReceiptPlant.code                                as plantCode,
       'PO'                                                  as goodsReceiptType,
       inventoryDocument.creationdate                        as creationDate,
       inventoryDocument.creationinfo                        as creationInfo,
       inventoryDocument.modifieddate                        as modifiedDate,
       inventoryDocument.modificationinfo                    as modificationInfo
from dobgoodsreceiptpurchaseorder goodsReceipt
         inner join dobinventorydocument inventoryDocument on goodsReceipt.id = inventoryDocument.id
         inner join iobgoodsreceiptpurchaseorderreceiveditemsdata goodsReceiptItem
                    on goodsReceipt.id = goodsReceiptItem.refinstanceid
         inner join masterdata itemData on itemData.id = goodsReceiptItem.itemid
         inner join iobgoodsreceiptpurchaseorderitemquantities itemQuantity
                    on goodsReceiptItem.id = itemQuantity.refinstanceid
         inner join cobmeasure itemUnitOfMeasure on itemUnitOfMeasure.id = itemQuantity.unitofentryid
         inner join cobenterprise goodsReceiptPurchaseUnit
                    on inventoryDocument.purchaseunitid = goodsReceiptPurchaseUnit.id
         inner join iobinventorycompany inventoryCompany on inventoryDocument.id = inventoryCompany.refinstanceid
         inner join cobenterprise goodsReceiptCompany on inventoryCompany.companyid = goodsReceiptCompany.id
         inner join cobenterprise goodsReceiptStorehouse on inventoryCompany.storehouseid = goodsReceiptStorehouse.id
         inner join cobenterprise goodsReceiptPlant on inventoryCompany.plantid = goodsReceiptPlant.id
UNION ALL
select concat(goodsReceipt.id, itemQuantity.id, itemData.id) as id,
       inventoryDocument.code                                as goodsReceiptCode,
       itemData.id                                           as itemId,
       itemData.code                                         as itemCode,
       itemQuantity.receivedqtyuoe                           as quantity,
       itemQuantity.stocktype                                as stockType,
       itemUnitOfMeasure.id                                  as unitOfMeasureId,
       itemUnitOfMeasure.code                                as unitOfMeasureCode,
       goodsReceiptPurchaseUnit.id                           as purchaseUnitId,
       goodsReceiptPurchaseUnit.code                         as purchaseUnitCode,
       goodsReceiptCompany.id                                as companyId,
       goodsReceiptCompany.code                              as companyCode,
       goodsReceiptStorehouse.id                             as storehouseId,
       goodsReceiptStorehouse.code                           as storehouseCode,
       goodsReceiptPlant.id                                  as plantId,
       goodsReceiptPlant.code                                as plantCode,
       'SR'                                                  as goodsReceiptType,
       inventoryDocument.creationdate                        as creationDate,
       inventoryDocument.creationinfo                        as creationInfo,
       inventoryDocument.modifieddate                        as modifiedDate,
       inventoryDocument.modificationinfo                    as modificationInfo
from dobgoodsreceiptsalesreturn goodsReceipt
         inner join dobinventorydocument inventoryDocument on goodsReceipt.id = inventoryDocument.id
         inner join iobgoodsreceiptsalesreturnreceiveditemsdata goodsReceiptItem
                    on goodsReceipt.id = goodsReceiptItem.refinstanceid
         inner join masterdata itemData on itemData.id = goodsReceiptItem.itemid
         inner join iobgoodsreceiptsalesreturnitemquantities itemQuantity
                    on goodsReceiptItem.id = itemQuantity.refinstanceid
         inner join cobmeasure itemUnitOfMeasure on itemUnitOfMeasure.id = itemQuantity.unitofentryid
         inner join cobenterprise goodsReceiptPurchaseUnit
                    on inventoryDocument.purchaseunitid = goodsReceiptPurchaseUnit.id
         inner join iobinventorycompany inventoryCompany on inventoryDocument.id = inventoryCompany.refinstanceid
         inner join cobenterprise goodsReceiptCompany on inventoryCompany.companyid = goodsReceiptCompany.id
         inner join cobenterprise goodsReceiptStorehouse on inventoryCompany.storehouseid = goodsReceiptStorehouse.id
         inner join cobenterprise goodsReceiptPlant on inventoryCompany.plantid = goodsReceiptPlant.id;
