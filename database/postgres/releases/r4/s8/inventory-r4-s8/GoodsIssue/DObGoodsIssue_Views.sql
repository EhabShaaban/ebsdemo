drop view if exists IObGoodsIssueItemQuantitiesGeneralModel;

CREATE VIEW IObGoodsIssueItemQuantitiesGeneralModel AS
select concat(goodsIssue.id, unitofmeasureid, itemData.id) as id,
       inventoryDocument.code             as goodsIssueCode,
       itemData.id                        as itemId,
       itemData.code                      as itemCode,
       goodsIssueItem.quantity            as quantity,
       itemUnitOfMeasure.id               as unitOfMeasureId,
       itemUnitOfMeasure.code             as unitOfMeasureCode,
       goodsIssuePurchaseUnit.id          as purchaseUnitId,
       goodsIssuePurchaseUnit.code        as purchaseUnitCode,
       goodsIssueCompany.id               as companyId,
       goodsIssueCompany.code             as companyCode,
       goodsIssueStorehouse.id            as storehouseId,
       goodsIssueStorehouse.code          as storehouseCode,
       goodsIssuePlant.id                 as plantId,
       goodsIssuePlant.code               as plantCode,
       'SI'                               as goodsIssueType,
       inventoryDocument.creationdate     as creationDate,
       inventoryDocument.creationinfo     as creationInfo,
       inventoryDocument.modifieddate     as modifiedDate,
       inventoryDocument.modificationinfo as modificationInfo
from dobgoodsissuesalesinvoice goodsIssue
         inner join dobinventorydocument inventoryDocument on goodsIssue.id = inventoryDocument.id
         inner join iobgoodsissuesalesinvoiceitem goodsIssueItem
                    on goodsIssue.id = goodsIssueItem.refinstanceid
         inner join masterdata itemData on itemData.id = goodsIssueItem.itemid
         inner join cobmeasure itemUnitOfMeasure on itemUnitOfMeasure.id = goodsIssueItem.unitofmeasureid
         inner join cobenterprise goodsIssuePurchaseUnit
                    on inventoryDocument.purchaseunitid = goodsIssuePurchaseUnit.id
         inner join iobinventorycompany inventoryCompany on inventoryDocument.id = inventoryCompany.refinstanceid
         inner join cobenterprise goodsIssueCompany on inventoryCompany.companyid = goodsIssueCompany.id
         inner join cobenterprise goodsIssueStorehouse on inventoryCompany.storehouseid = goodsIssueStorehouse.id
         inner join cobenterprise goodsIssuePlant on inventoryCompany.plantid = goodsIssuePlant.id
UNION ALL
select concat(goodsIssue.id, unitofmeasureid, itemData.id) as id,
       inventoryDocument.code             as goodsIssueCode,
       itemData.id                        as itemId,
       itemData.code                      as itemCode,
       goodsIssueItem.quantity            as quantity,
       itemUnitOfMeasure.id               as unitOfMeasureId,
       itemUnitOfMeasure.code             as unitOfMeasureCode,
       goodsIssuePurchaseUnit.id          as purchaseUnitId,
       goodsIssuePurchaseUnit.code        as purchaseUnitCode,
       goodsIssueCompany.id               as companyId,
       goodsIssueCompany.code             as companyCode,
       goodsIssueStorehouse.id            as storehouseId,
       goodsIssueStorehouse.code          as storehouseCode,
       goodsIssuePlant.id                 as plantId,
       goodsIssuePlant.code               as plantCode,
       'SO'                               as goodsIssueType,
       inventoryDocument.creationdate     as creationDate,
       inventoryDocument.creationinfo     as creationInfo,
       inventoryDocument.modifieddate     as modifiedDate,
       inventoryDocument.modificationinfo as modificationInfo
from dobgoodsissuesalesorder goodsIssue
         inner join dobinventorydocument inventoryDocument on goodsIssue.id = inventoryDocument.id
         inner join iobgoodsissuesalesorderitem goodsIssueItem
                    on goodsIssue.id = goodsIssueItem.refinstanceid
         inner join masterdata itemData on itemData.id = goodsIssueItem.itemid
         inner join cobmeasure itemUnitOfMeasure on itemUnitOfMeasure.id = goodsIssueItem.unitofmeasureid
         inner join cobenterprise goodsIssuePurchaseUnit
                    on inventoryDocument.purchaseunitid = goodsIssuePurchaseUnit.id
         inner join iobinventorycompany inventoryCompany on inventoryDocument.id = inventoryCompany.refinstanceid
         inner join cobenterprise goodsIssueCompany on inventoryCompany.companyid = goodsIssueCompany.id
         inner join cobenterprise goodsIssueStorehouse on inventoryCompany.storehouseid = goodsIssueStorehouse.id
         inner join cobenterprise goodsIssuePlant on inventoryCompany.plantid = goodsIssuePlant.id;
