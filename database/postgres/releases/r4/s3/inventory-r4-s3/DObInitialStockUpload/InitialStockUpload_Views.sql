CREATE OR REPLACE VIEW DObInitialStockUploadGeneralModel AS
SELECT DObInventoryDocument.id,
       DObInventoryDocument.code,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.currentStates,
       ce4.name                           AS purchaseUnitName,
       ce4.name:: JSON ->> 'en'           AS purchaseUnitNameEn,
       ce4.code                           AS purchaseUnitCode,
       ce3.code                           AS storehouseCode,
       ce3.name                           AS storehouseName,
       storekeeper.code                   AS storekeeperCode,
       storekeeper.name                   AS storekeeperName,
       ce1.code                           AS companyCode,
       ce1.name                           AS companyName
FROM DObInventoryDocument
       LEFT JOIN IObInventoryCompany
                 ON DObInventoryDocument.id = IObInventoryCompany.refInstanceId
       JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
       JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
       JOIN cobenterprise ce4 ON ce4.id = DObInventoryDocument.purchaseunitid
       JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid
WHERE DObInventoryDocument.inventoryDocumentType = 'ISU'
