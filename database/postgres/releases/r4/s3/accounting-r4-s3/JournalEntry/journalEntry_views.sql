CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode                              AS documentReferenceType,
       DObJournalEntry.dueDate,
       company.code                                                AS companyCode,
       company.name                                                AS companyName,
       purchaseUnit.code                                           AS purchaseUnitCode,
       purchaseUnit.name                                           AS purchaseUnitName,
       purchaseUnit.name:: json ->> 'en'                           AS purchaseUnitNameEn,
       (SELECT dobaccountingdocument.code
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObVendorInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'VendorInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObPaymentRequestJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSalesInvoiceJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcollectionJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON dobcollectionJournalEntry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcollectionJournalEntry.id
          AND dobaccountingdocument.objecttypecode in ('C_SI', 'C_NR', 'C_SO','C_DN')
        UNION
        SELECT dobnotesreceivables.code
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobnotesreceivables
                           ON dobNotesReceivableJournalEntry.documentreferenceid =
                              dobnotesreceivables.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM doblandedcostjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON doblandedcostjournalentry.documentreferenceid =
                              dobaccountingdocument.id
        WHERE DObJournalEntry.id = doblandedcostjournalentry.id
          and dobaccountingdocument.objecttypecode = 'LandedCost') AS documentReferenceCode,

       fiscalPeriod.name::json ->> 'en'                            AS FiscalPeriod
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company
                   ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join CObFiscalPeriod fiscalPeriod
                   on DObJournalEntry.fiscalPeriodId = fiscalPeriod.id;

