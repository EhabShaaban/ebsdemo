DROP VIEW If EXISTS IObVendorInvoiceRemainingGeneralModel;
DROP VIEW If EXISTS IObVendorInvoiceDetailsGeneralModel;
DROP VIEW If EXISTS DObVendorInvoiceGeneralModel;



CREATE OR REPLACE VIEW DObVendorInvoiceGeneralModel AS
SELECT dobvendorInvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                                                     AS vendorCode,
       masterdata.name                                                     AS vendorName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en') AS vendorCodeName,
       cobvendorinvoice.code                                               AS invoiceTypeCode,
       cobvendorinvoice.name                                               AS invoiceTypeName,
       cobvendorinvoice.name :: json ->> 'en'                              AS invoiceTypeNameEn,
       purchaseUnit.code                                                   AS purchaseUnitCode,
       purchaseUnit.name                                                   AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                  AS purchaseUnitNameEn,
       company.code                                                        AS companyCode,
       company.name                                                        AS companyName,
       cobcurrency.code                                                    AS currencyCode,
       cobcurrency.name                                                    AS currencyName,
       cobcurrency.iso                                                     AS currencyISO,
       cobpaymentterms.code                                                AS paymentTermCode,
       doborderdocument.code                                               AS purchaseOrderCode,
       iobvendorinvoicedetails.invoiceNumber                               AS invoiceNumber
FROM dobvendorInvoice
         left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN iobvendorinvoicedetails
                   ON iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id
         LEFT JOIN masterdata ON iobvendorinvoicedetails.vendorId = masterdata.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit
                   ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
                       AND purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobenterprise company ON company.id = iobvendorinvoicecompanydata.companyId
    AND company.objecttypecode = '1'
         LEFT JOIN IObVendorInvoicePurchaseOrder
                   ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobvendorinvoice ON cobvendorinvoice.id = dobvendorInvoice.typeId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms
                   ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument
                   ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId AND
                      doborderdocument.objecttypecode LIKE '%_PO';
         ;

CREATE or REPLACE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE WHEN invoiceSummary.totalremaining IS NULL THEN 0 ELSE invoiceSummary.totalremaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
                   ON dobInvoice.id = invoiceSummary.id;


CREATE OR REPLACE VIEW IObVendorInvoiceDetailsGeneralModel AS
SELECT dobaccountingdocument.code         as invoiceCode,
       dobvendorInvoice.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                    AS vendorCode,
       masterdata.name :: json ->> 'en'   AS vendorName,
       iobvendorinvoicedetails.refinstanceid,
       iobvendorinvoicedetails.invoiceNumber,
       iobvendorinvoicedetails.invoiceDate,
       cobcurrency.code                   AS currencyCode,
       cobcurrency.iso                    AS currencyISO,
       cobcurrency.name                   AS currencyName,
       cobpaymentterms.code               AS paymentTermCode,
       cobpaymentterms.name               AS paymentTerm,
       doborderdocument.code              AS purchaseOrderCode,
       purchaseUnit.code                  AS purchaseUnitCode,
       purchaseUnit.name                  AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       (select string_agg(CONCAT(dobaccountingdocument.code, '-', IObPaymentRequestPaymentDetails.netamount), ', ')
        from dobaccountingdocument,
             iobpaymentrequestpaymentdetails
        where dobaccountingdocument.id in (select iobvendorinvoicedetailsdownpayment.DownPaymentid
                                           from iobvendorinvoicedetailsdownpayment
                                           where iobvendorinvoicedetailsdownpayment.refInstanceId =
                                                 iobvendorinvoicedetails.id)
          and dobaccountingdocument.objecttypecode = 'PaymentRequest'
          and iobpaymentrequestpaymentdetails.refinstanceid = dobaccountingdocument.id
       )                                  as DownPayment

FROM dobvendorInvoice
         left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN iobvendorinvoicedetails on iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id
         LEFT JOIN masterdata ON iobvendorinvoicedetails.vendorId = masterdata.id
         LEFT JOIN IObVendorInvoicePurchaseOrder ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
    AND purchaseUnit.objecttypecode = '5';

ALTER TABLE dobvendorinvoice
    DROP COLUMN vendorid;

