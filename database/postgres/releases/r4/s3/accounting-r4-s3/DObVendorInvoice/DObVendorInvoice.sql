ALTER TABLE iobvendorinvoicedetails
    ADD column vendorId INT8 REFERENCES mobvendor On DELETE RESTRICT;

UPDATE iobvendorinvoicedetails
SET vendorid = dobvendorinvoice.vendorid
FROM dobvendorinvoice
WHERE dobvendorinvoice.id = iobvendorinvoicedetails.refinstanceid;


ALTER TABLE iobvendorinvoicedetails
    ALTER COLUMN vendorId SET NOT NULL;