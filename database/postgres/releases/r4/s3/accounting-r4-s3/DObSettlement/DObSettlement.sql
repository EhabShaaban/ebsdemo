DROP TABLE IF EXISTS DObSettlement;
CREATE TABLE DObSettlement
(
    id             BIGSERIAL     NOT NULL,
    settlementType VARCHAR(1024) NOT NULL,
    documentOwnerId  INT8 NOT NULL REFERENCES EBSUser (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS IObSettlementDetails;
CREATE TABLE IObSettlementDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL REFERENCES DObSettlement (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currencyId       int8 REFERENCES cobcurrency (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);
DROP TABLE IF EXISTS DObSettlementJournalEntry;
CREATE TABLE DObSettlementJournalEntry
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);