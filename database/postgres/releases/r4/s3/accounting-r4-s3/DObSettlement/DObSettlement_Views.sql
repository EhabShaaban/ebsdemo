DROP VIEW IF EXISTS DObSettlementGeneralModel;
CREATE OR REPLACE VIEW DObSettlementGeneralModel AS
SELECT settlement.id,
       AccDoc.code,
       AccDoc.creationdate,
       AccDoc.modifieddate,
       AccDoc.creationinfo,
       AccDoc.modificationinfo,
       AccDoc.currentstates,
       cobenterprise.code                                AS purchaseUnitCode,
       cobenterprise.name                                AS purchaseUnitName,
       cobenterprise.name::json ->> 'en'                 AS purchaseUnitNameEn,
       company.code                                      AS companyCode,
       company.name                                      AS companyName,
       company.name::json ->> 'en'                       AS companyNameEn,
       settlement.settlementtype                         AS settlementType,
       cobcurrency.iso                                   AS currencyIso,
       iobaccountingdocumentactivationdetails.accountant AS activatedBy

FROM dobsettlement settlement
         LEFT JOIN DObAccountingDocument AccDoc ON settlement.id = AccDoc.id AND AccDoc.objecttypecode = 'Settlement'
         LEFT JOIN IObAccountingDocumentCompanyData IObSettlementCompanyData
                   ON settlement.id = IObSettlementCompanyData.refinstanceid AND
                      IObSettlementCompanyData.objecttypecode = 'Settlement'
         LEFT JOIN cobenterprise ON IObSettlementCompanyData.purchaseUnitId = cobenterprise.id
         LEFT JOIN cobenterprise company ON IObSettlementCompanyData.companyid = company.id
         LEFT JOIN IObSettlementDetails settlemetDetails ON settlement.id = settlemetDetails.refinstanceid
         left join cobcurrency on cobcurrency.id = settlemetDetails.currencyid
         left join iobaccountingdocumentactivationdetails
                   on iobaccountingdocumentactivationdetails.refinstanceid = AccDoc.id;

CREATE OR REPLACE VIEW IObSettlementDetailsGeneralModel AS
SELECT dobaccountingdocument.code         as settlementCode,
       dobSettlement.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       iobSettlementDetails.refinstanceid,
       cobcurrency.code                   AS currencyCode,
       cobcurrency.iso                    AS currencyISO,
       cobcurrency.name                   AS currencyName

FROM dobSettlement
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobSettlement.id
         LEFT JOIN IObSettlementDetails on dobaccountingdocument.id = IObSettlementDetails.refinstanceid
         LEFT JOIN cobcurrency ON cobcurrency.id = IObSettlementDetails.currencyId
