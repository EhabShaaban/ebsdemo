create TABLE CObFiscalPeriod
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024),
    name             JSON                     NOT NULL,
    fromDate         TIMESTAMP With time zone NOT NULL,
    toDate           TIMESTAMP With time zone NOT NULL,
    PRIMARY KEY (id)
);