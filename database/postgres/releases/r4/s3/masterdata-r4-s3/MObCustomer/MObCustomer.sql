ALTER TABLE mobcustomer
    RENAME COLUMN customertype TO type;
ALTER TABLE mobcustomer
    ADD COLUMN currencyId INT8,
    DROP CONSTRAINT fk_mobcustomer_customertype,
    DROP CONSTRAINT fk_mobcustomer_legaltype,
    DROP CONSTRAINT fk_mobcustomer_risklevel,
    DROP COLUMN customertypeid,
    DROP COLUMN legaltypeid,
    DROP COLUMN risklevelid;


ALTER TABLE IObCustomerTaxInfo RENAME COLUMN taxCard TO taxCardNumber;
ALTER TABLE IObCustomerTaxInfo
    RENAME TO IObCustomerLegalRegistrationData;
ALTER SEQUENCE IF EXISTS iobcustomertaxinfo_id_seq RENAME TO iobcustomerlegalregistrationdata_id_seq;


