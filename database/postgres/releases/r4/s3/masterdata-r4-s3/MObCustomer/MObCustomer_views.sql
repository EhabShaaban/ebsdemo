DROP VIEW IF EXISTS IObGoodsIssueRefDocumentDataGeneralModel;
DROP VIEW IF EXISTS MObCustomerGeneralModel;

CREATE VIEW MObCustomerGeneralModel AS
SELECT MObCustomer.id,
       MasterData.creationDate,
       MasterData.modifiedDate,
       MasterData.creationInfo,
       MasterData.modificationInfo,
       MasterData.currentStates,
       MasterData.code,
       MObCustomer.marketname,
       MasterData.name    AS legalName,
       MObCustomer.type,
       cobenterprise.code AS kapCode,
       cobenterprise.name AS kapName,
       (SELECT string_agg(cobenterprise.code, ', ')
        FROM cobenterprise
        WHERE cobenterprise.id IN (SELECT iobcustomerbusinessunit.businessunitid
                                   FROM iobcustomerbusinessunit
                                   WHERE iobcustomerbusinessunit.refinstanceid = mobcustomer.id)
          AND cobenterprise.objecttypecode = '5'
       )                  AS businessUnitCodes,
       (SELECT array_to_json(array_agg(cobenterprise.name))
        FROM cobenterprise
        WHERE cobenterprise.id IN (SELECT iobcustomerbusinessunit.businessunitid
                                   FROM iobcustomerbusinessunit
                                   WHERE iobcustomerbusinessunit.refinstanceid = mobcustomer.id)
          AND cobenterprise.objecttypecode = '5'
       )                  AS businessUnitNames,
       IObCustomerLegalRegistrationData.registrationNumber,
       LObIssueFrom.code as issueFromCode,
       LObIssueFrom.name as issueFromName,
       IObCustomerLegalRegistrationData.taxCardNumber,
       LObTaxAdministrative.code as taxAdministrativeCode,
       LObTaxAdministrative.name as taxAdministrativeName,
       IObCustomerLegalRegistrationData.taxFileNumber
FROM MObCustomer
         LEFT JOIN MasterData ON MObCustomer.id = MasterData.id AND MasterData.objecttypecode = '3'
         LEFT JOIN cobenterprise ON MObCustomer.kapId = cobenterprise.id
         LEFT JOIN IObCustomerLegalRegistrationData ON MObCustomer.id = IObCustomerLegalRegistrationData.refInstanceId
         LEFT JOIN LObIssueFrom ON IObCustomerLegalRegistrationData.issueFromId = LObIssueFrom.id
         LEFT JOIN LObTaxAdministrative ON IObCustomerLegalRegistrationData.taxAdministrativeId = LObTaxAdministrative.id;

CREATE VIEW IObGoodsIssueRefDocumentDataGeneralModel AS
SELECT IObGoodsIssueSalesInvoiceData.id,
       IObGoodsIssueSalesInvoiceData.refInstanceId,
       IObGoodsIssueSalesInvoiceData.comments,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesInvoiceData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueCode,
       (SELECT DObInventoryDocument.currentStates
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesInvoiceData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueState,
       (SELECT masterdata.name
        FROM masterdata
        WHERE IObGoodsIssueSalesInvoiceData.customerId = masterdata.id)               AS customerName,
       (SELECT masterdata.code
        FROM masterdata
        WHERE IObGoodsIssueSalesInvoiceData.customerId = masterdata.id)               AS customerCode,
       (SELECT lobaddress.address
        FROM lobaddress
        WHERE IObGoodsIssueSalesInvoiceData.addressId = lobaddress.id)                AS address,
       (SELECT lobcontactperson.name
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesInvoiceData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonName,
       (SELECT lobcontactperson.name:: JSON ->> 'en'
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesInvoiceData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonNameEn,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueSalesInvoiceData.salesRepresentativeId = cobenterprise.id)       AS salesRepresentativeName,
       (SELECT MObCustomerGeneralModel.kapCode
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesInvoiceData.customerId = MObCustomerGeneralModel.id)  AS kapCode,
       (SELECT MObCustomerGeneralModel.kapName
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesInvoiceData.customerId = MObCustomerGeneralModel.id)  AS kapName,
       (SELECT dobaccountingdocument.code
        FROM dobaccountingdocument
        WHERE IObGoodsIssueSalesInvoiceData.salesInvoiceId =
              dobaccountingdocument.id and dobaccountingdocument.objecttypecode = 'SalesInvoice') AS refDocument,
       dobinventorydocument.inventorydocumenttype
FROM IObGoodsIssueSalesInvoiceData
         LEFT JOIN dobinventorydocument on IObGoodsIssueSalesInvoiceData.refInstanceId = dobinventorydocument.id

UNION ALL

SELECT IObGoodsIssueSalesOrderData.id,
       IObGoodsIssueSalesOrderData.refInstanceId,
       IObGoodsIssueSalesOrderData.comments,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesOrderData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueCode,
       (SELECT DObInventoryDocument.currentStates
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesOrderData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueState,
       (SELECT masterdata.name
        FROM masterdata
        WHERE IObGoodsIssueSalesOrderData.customerId = masterdata.id)               AS customerName,
       (SELECT masterdata.code
        FROM masterdata
        WHERE IObGoodsIssueSalesOrderData.customerId = masterdata.id)               AS customerCode,
       (SELECT lobaddress.address
        FROM lobaddress
        WHERE IObGoodsIssueSalesOrderData.addressId = lobaddress.id)                AS address,
       (SELECT lobcontactperson.name
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesOrderData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonName,
       (SELECT lobcontactperson.name:: JSON ->> 'en'
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesOrderData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonNameEn,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueSalesOrderData.salesRepresentativeId = cobenterprise.id)       AS salesRepresentativeName,
       (SELECT MObCustomerGeneralModel.kapCode
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesOrderData.customerId = MObCustomerGeneralModel.id)  AS kapCode,
       (SELECT MObCustomerGeneralModel.kapName
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesOrderData.customerId = MObCustomerGeneralModel.id)  AS kapName,
       (SELECT dobsalesorder.code
        FROM dobsalesorder
        WHERE IObGoodsIssueSalesOrderData.salesOrderId =
              dobsalesorder.id )                                                     AS referenceDocument,
       dobinventorydocument.inventorydocumenttype
FROM IObGoodsIssueSalesOrderData
         LEFT JOIN dobinventorydocument on IObGoodsIssueSalesOrderData.refInstanceId = dobinventorydocument.id;
