\ir 'accounting-r4-s3/accounting-r4-s3.sql'
\ir 'masterdata-r4-s3/masterdata-r4-s3.sql'
\ir 'inventory-r4-s3/inventory-r4-s3.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
