
INSERT INTO CObExchangeRateType(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name, isDefault)
VALUES (1, 'USER_DAILY_RATE', '2020-12-17 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Active"]', '{"en":"User Daily Rate","ar":"سعر المستخدم اليومي"}', true);

INSERT INTO CObExchangeRateType(id, code, creationdate, modifieddate, creationinfo, modificationinfo, currentstates, name, isDefault)
VALUES (2, 'CENTRAL_BANK_OF_EGYPT', '2020-12-17 09:02:00', '2020-06-24 09:02:00', 'Gehan.Ahmed', 'Gehan.Ahmed', '["Active"]', '{"en":"Central Bank Of Egypt","ar":"البنك المركزي المصري"}', false);