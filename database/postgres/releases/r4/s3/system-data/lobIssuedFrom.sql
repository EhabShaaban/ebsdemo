INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (1, '0001', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"جنوب القاهرة","ar":"جنوب القاهرة"}', '{"en":"42 إسماعيل أباظة - لاظوغلى","ar":"42 إسماعيل أباظة - لاظوغلى"}', '27941052');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (2, '0002', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"6-Oct","ar":"6-Oct"}', '{"en":"6 أكتوبر ح11مج 2 ع47 بجوار الطب البيطرى","ar":"6 أكتوبر ح11مج 2 ع47 بجوار الطب البيطرى"}', '38334925');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (3, '0003', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"القاهرة","ar":"القاهرة"}', '{"en":"38 ش رمسيس","ar":"38 ش رمسيس"}', '25750264');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (4, '0004', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"شمال القاهرة","ar":"شمال القاهرة"}', '{"en":"1 ش 26 يوليو","ar":"1 ش 26 يوليو"}', '25929551');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (5, '0005', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"الجيزة","ar":"الجيزة"}', '{"en":"82 ش وادى النيل مت عقبة جيزة","ar":"82 ش وادى النيل مت عقبة جيزة"}', '3345323');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (6, '0006', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"15-May","ar":"15-May"}', '{"en":"مدينة مايو حى رجال الأعمال مج 3 ع 9 ش 9 فوق الشهر العقارى","ar":"مدينة مايو حى رجال الأعمال مج 3 ع 9 ش 9 فوق الشهر العقارى"}', '25505119');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (7, '0007', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"الأسكندرية","ar":"الأسكندرية"}', '{"en":"21 ميدان سعد زغلول محطة الرمل","ar":"21 ميدان سعد زغلول محطة الرمل"}', '34865397');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (8, '0008', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"المنصورة","ar":"المنصورة"}', '{"en":"105 ش الجمهورية المنصورة","ar":"105 ش الجمهورية المنصورة"}', '502244067');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (9, '0009', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار القاهرة","ar":"إستثمار القاهرة"}', '{"en":"3ش صلاح سالم","ar":"3ش صلاح سالم"}', '22633790');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (10, '0010', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار أسيوط","ar":"إستثمار أسيوط"}', '{"en":"ديوان عام المحافظة","ar":"ديوان عام المحافظة"}', '882293225');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (11, '0011', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار الإسماعيلية","ar":"إستثمار الإسماعيلية"}', '{"en":"برج قرطبة الدور 3 أمام موقف الرئيسى","ar":"برج قرطبة الدور 3 أمام موقف الرئيسى"}', '643353487');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (12, '0012', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار الأسكندرية","ar":"إستثمار الأسكندرية"}', '{"en":"المنطقة الحرة طريق مصر إسكندرية الصحراوى كيلو 26","ar":"المنطقة الحرة طريق مصر إسكندرية الصحراوى كيلو 26"}', '34493664');

INSERT INTO public.lobissuefrom (id, code, creationdate, modifieddate, creationinfo, modificationinfo, name, address, telephone)
VALUES (13, '0013', '2021-02-08 13:34:19.956038', '2021-02-08 13:34:19.956038', 'Admin', 'Admin', '{"en":"إستثمار العاشر من رمضان","ar":"إستثمار العاشر من رمضان"}', '{"en":"العاشر من رمضان","ar":"العاشر من رمضان"}', '111111111');
