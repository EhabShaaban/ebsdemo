INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (1, '000001', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 1, 1, '{
    "ar": "EGPEGP",
    "en": "EGPEGP"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (2, '000002', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 2, 2, '{
    "ar": "USDUSD",
    "en": "USDUSD"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (3, '000003', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 3, 3, '{
    "ar": "EUREUR",
    "en": "EUREUR"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (4, '000004', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 4, 4, '{
    "ar": "CNYCNY",
    "en": "CNYCNY"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (5, '000005', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 5, 5, '{
    "ar": "JPYJPY",
    "en": "JPYJPY"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (6, '000006', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 6, 6, '{
    "ar": "HKDHKD",
    "en": "HKDHKD"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (7, '000007', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 7, 7, '{
    "ar": "INRINR",
    "en": "INRINR"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (8, '000008', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 8, 8, '{
    "ar": "KPWKPW",
    "en": "KPWKPW"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (9, '000009', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 9, 9, '{
    "ar": "KPWKPW",
    "en": "KPWKPW"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (10, '000010', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 10, 10, '{
    "ar": "SARSAR",
    "en": "SARSAR"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (11, '000011', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 11, 11, '{
    "ar": "TRYTRY",
    "en": "TRYTRY"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (12, '000012', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 12, 12, '{
    "ar": "AEDAED",
    "en": "AEDAED"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (13, '000013', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 13, 13, '{
    "ar": "GBPGBP",
    "en": "GBPGBP"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (14, '000014', '2020-04-30 10:01:57.507000', '2020-04-30 10:01:57.491000', '2020-04-30 10:01:57.507000', 'admin', 'admin', '["Active"]', 1, 1, 14, 14, '{
    "ar": "SDGSDG",
    "en": "SDGSDG"
  }', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (20, '000015', '2021-01-27 09:02:29.733000', '2021-01-27 09:02:29.733000', '2021-01-27 09:02:29.733000', 'System', 'System', '["Active"]', 1, 21.601, 13, 1, '{"ar":"GBP-EGP","en":"GBP-EGP"}', 2);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (18, '000016', '2021-01-27 09:02:29.823000', '2021-01-27 09:02:29.823000', '2021-01-27 09:02:29.823000', 'System', 'System', '["Active"]', 1, 15.221, 5, 1, '{"ar":"JPY-EGP","en":"JPY-EGP"}', 2);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (15, '000017', '2021-01-14 09:59:41.543000', '2021-01-14 09:59:41.535000', '2021-01-14 09:59:41.543000', 'admin', 'admin', '["Active"]', 1, 22, 2, 1, '{"ar":"USDEGP","en":"USDEGP"}', 1);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (21, '000018', '2021-01-27 09:02:29.901000', '2021-01-27 09:02:29.901000', '2021-01-27 09:02:29.901000', 'System', 'System', '["Active"]', 1, 4.3005, 12, 1, '{"ar":"AED-EGP","en":"AED-EGP"}', 2);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (22, '000019', '2021-01-27 09:02:29.860000', '2021-01-27 09:02:29.860000', '2021-01-27 09:02:29.860000', 'System', 'System', '["Active"]', 1, 4.2108, 10, 1, '{"ar":"SAR-EGP","en":"SAR-EGP"}', 2);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (17, '000020', '2021-01-27 09:02:29.679000', '2021-01-27 09:02:29.679000', '2021-01-27 09:02:29.679000', 'System', 'System', '["Active"]', 1, 19.1749, 3, 1, '{"ar":"EUR-EGP","en":"EUR-EGP"}', 2);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (19, '000021', '2021-01-27 09:02:29.624000', '2021-01-27 09:02:29.624000', '2021-01-27 09:02:29.624000', 'System', 'System', '["Active"]', 1, 15.7948, 2, 1, '{"ar":"USD-EGP","en":"USD-EGP"}', 2);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (16, '000022', '2021-01-27 09:02:29.971000', '2021-01-27 09:02:29.971000', '2021-01-27 09:02:29.971000', 'System', 'System', '["Active"]', 1, 2.4416, 4, 1, '{"ar":"CNY-EGP","en":"CNY-EGP"}', 2);
INSERT INTO public.cobexchangerate (id, code, creationdate, validfrom, modifieddate, creationinfo, modificationinfo, currentstates, firstvalue, secondvalue, firstcurrencyid, secondcurrencyid, name, typeid) VALUES (23, '000023', '2021-01-27 11:32:15.515000', '2021-01-27 11:32:15.502000', '2021-01-27 11:32:15.515000', 'admin', 'admin', '["Active"]', 1, 20, 2, 1, '{"ar":"USDEGP","en":"USDEGP"}', 1);