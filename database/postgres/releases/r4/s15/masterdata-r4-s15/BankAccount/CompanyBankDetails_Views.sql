CREATE OR REPLACE VIEW iobcompanybankdetailsgeneralmodel as
SELECT iobcompanybankdetails.id,
       cobenterprise.code                                                          AS companycode,
       cobbank.code,
       cobbank.name,
       concat(iobcompanybankdetails.accountno, ' - ', currency.iso) AS accountno,
       currency.code AS currencyCode,
       currency.iso AS currencyISO,
       concat(cobbank.name ->> 'en'::text, ' - ', iobcompanybankdetails.accountno, ' - ', currency.iso) AS bankaccountnumber,
       iobcompanybankdetails.accountno AS iobBankAccountNo
FROM iobcompanybankdetails
    JOIN cobcompany ON iobcompanybankdetails.refinstanceid = cobcompany.id
    JOIN cobbank ON iobcompanybankdetails.bankid = cobbank.id
    JOIN cobenterprise ON cobcompany.id = cobenterprise.id
    LEFT JOIN cobcurrency currency on iobcompanybankdetails.currencyId = currency.id;


CREATE  OR REPLACE FUNCTION InitializeBankJournalBalance(_initialBalance double precision)  RETURNS void
LANGUAGE plpgsql
AS $$
BEGIN

WITH journalBalanceData AS (SELECT  concat('BANK', cobenterprise.code, cobcompanygeneralmodel.code, cobcurrency.code, iobcompanybankdetailsgeneralmodel.code, iobcompanybankdetailsgeneralmodel.iobBankAccountNo) as code
                                 , cast('{"ar": "", "en": ""}' AS json)  as name, NOW()::TIMESTAMP as creationdate, NOW()::TIMESTAMP as modifieddate, 'Admin' as creationinfo, 'Admin' as modificationinfo, 'BANK' as objecttypecode
                                 , cobenterprise.id as businessunitid, cobcompanygeneralmodel.id as companyid, cobcurrency.id as currencyid,  _initialBalance as initialBalance
                                 ,iobcompanybankdetailsgeneralmodel.iobBankAccountNo
                            from cobenterprise, cobcompanygeneralmodel, cobcurrency, iobcompanybankdetailsgeneralmodel
                            where cobenterprise.objecttypecode = '5'
                              and  cobcompanygeneralmodel.code = iobcompanybankdetailsgeneralmodel.companycode),
     cobJournalBalance AS (
INSERT INTO CObJournalBalance (code, name, creationdate, modifieddate, creationinfo,
                               modificationinfo,
                               objecttypecode,
                               businessunitid, companyid, currencyid, balance)
select code, name, creationdate, modifieddate, creationinfo,
       modificationinfo,
       objecttypecode,
       businessunitid, companyid, currencyid, initialBalance from  journalBalanceData
                                                                RETURNING CObJournalBalance.id, CObJournalBalance.code)
INSERT INTO cobbankjournalbalance (id, accountid)
select cobJournalBalance.id,  (SELECT id
                               FROM iobcompanybankdetailsgeneralmodel where iobcompanybankdetailsgeneralmodel.iobbankaccountno = journalBalanceData.iobbankaccountno)
from cobJournalBalance, journalBalanceData
where cobJournalBalance.code = journalBalanceData.code;

END;
$$;


CREATE  OR REPLACE FUNCTION InitializeTreasuryJournalBalance(_initialBalance double precision)  RETURNS void
LANGUAGE plpgsql
AS $$
BEGIN

WITH journalBalanceData AS (SELECT  concat('TREASURY', cobenterprise.code, cobcompanygeneralmodel.code, cobcurrency.code, iobcompanytreasuriesgeneralmodel.treasurycode) as code
                                 , cast('{"ar": "", "en": ""}' AS json)  as name, NOW()::TIMESTAMP as creationdate, NOW()::TIMESTAMP as modifieddate, 'Admin' as creationinfo, 'Admin' as modificationinfo, 'TREASURY' as objecttypecode
                                 , cobenterprise.id as businessunitid, cobcompanygeneralmodel.id as companyid, cobcurrency.id as currencyid,  _initialBalance as initialBalance
                                 , iobcompanytreasuriesgeneralmodel.treasurycode, iobcompanytreasuriesgeneralmodel.treasuryname
                            from cobenterprise, cobcompanygeneralmodel, cobcurrency, iobcompanytreasuriesgeneralmodel
                            where cobenterprise.objecttypecode = '5' and  cobcompanygeneralmodel.code = iobcompanytreasuriesgeneralmodel.companycode),
     cobJournalBalance AS (
INSERT INTO CObJournalBalance (code, name, creationdate, modifieddate, creationinfo,
                               modificationinfo,
                               objecttypecode,
                               businessunitid, companyid, currencyid, balance)
select code, name, creationdate, modifieddate, creationinfo,
       modificationinfo,
       objecttypecode,
       businessunitid, companyid, currencyid, initialBalance from  journalBalanceData
                                                                       RETURNING CObJournalBalance.id, CObJournalBalance.code)
INSERT INTO cobtreasuryjournalbalance (id, treasuryid)
select cobJournalBalance.id,  (SELECT id
                               FROM cobtreasury WHERE concat(code, ' - ', name::json ->> 'en') = concat(journalBalanceData.treasurycode, ' - ', journalBalanceData.treasuryname::json ->> 'en'))
from cobJournalBalance, journalBalanceData
where cobJournalBalance.code = journalBalanceData.code;

END;
$$;