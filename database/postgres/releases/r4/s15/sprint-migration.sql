\ir 'access-rights-r4-s15/access-rights-r4-s15.sql'
\ir 'masterdata-r4-s15/masterdata-r4-s15.sql'
\ir 'accounting-r4-s15/accounting-r4-s15.sql'
\ir 'inventory-r4-s15/inventory-r4-s15.sql'

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
