INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingActivateOwner_Offset'),
        (select id from permission where permissionexpression = 'Costing:Activate'),
        '2021-07-28 11:54:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'Costing  - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingViewer_Offset'),
        (select id from permission where permissionexpression = 'Costing:ReadAccountingDocumentActivationDetails'),
        '2021-07-28 11:54:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'Costing  - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingViewer_Offset'),
        (select id from permission where permissionexpression = 'Costing:ReadAccountingDetails'),
        '2021-07-28 11:54:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'Costing  - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingViewer_Offset'),
        (select id from permission where permissionexpression = 'Costing:ReadCompany'),
        '2021-07-28 11:54:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'Costing  - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingViewer_Offset'),
        (select id from permission where permissionexpression = 'Costing:ReadGeneralData'),
        '2021-07-28 11:54:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'Costing  - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime, authorizationconditionid)
VALUES ((select id from role where roleexpression = 'CostingViewer_Offset'),
        (select id from permission where permissionexpression = 'Costing:ReadAll'),
        '2021-07-28 11:54:04+00', (select id
                                   from authorizationcondition
                                   where concat(objectname, ' - ', condition) =
                                         'Costing  - [purchaseUnitName=''Offset'']'));

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'CostingViewer'),
        (select id from permission where permissionexpression = 'Costing:ReadItems'),
        '2021-07-28 11:53:03+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'CostingViewer'),
        (select id from permission where permissionexpression = 'Costing:ReadDetails'),
        '2021-07-28 11:53:03+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'CostingViewer'),
        (select id from permission where permissionexpression = 'Costing:ReadAccountingDocumentActivationDetails'),
        '2021-07-28 11:53:03+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'CostingViewer'),
        (select id from permission where permissionexpression = 'Costing:ReadAccountingDetails'),
        '2021-07-28 11:53:03+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'CostingViewer'),
        (select id from permission where permissionexpression = 'Costing:ReadCompany'),
        '2021-07-28 11:53:03+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'CostingViewer'),
        (select id from permission where permissionexpression = 'Costing:ReadGeneralData'),
        '2021-07-28 11:53:03+00');

INSERT INTO public.permissionassignment (roleid, permissionid, assignmenttime)
VALUES ((select id from role where roleexpression = 'CostingViewer'),
        (select id from permission where permissionexpression = 'Costing:ReadAll'),
        '2021-07-28 11:53:03+00');
