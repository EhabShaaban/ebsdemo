INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Costing:Activate', null, null, null, 'Costing');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Costing:ReadAccountingDocumentActivationDetails', null, null, null, 'Costing');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Costing:ReadAccountingDetails', null, null, null, 'Costing');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Costing:ReadCompany', null, null, null, 'Costing');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Costing:ReadGeneralData', null, null, null, 'Costing');
INSERT INTO public.permission (permissionexpression, description, objectcatalogid, operationid, objectname) VALUES ('Costing:ReadAll', null, null, null, 'Costing');
