INSERT INTO public.role (roleexpression, description) VALUES ('CostingActivateOwner_Offset', null);
INSERT INTO public.role (roleexpression, description) VALUES ('CostingViewer', null);

UPDATE role SET roleexpression = 'Accountant_Offset' WHERE roleexpression = 'Accountant_offset';
UPDATE role SET roleexpression = 'CostingViewer_Offset' WHERE roleexpression = 'CostingViewer_offset';
