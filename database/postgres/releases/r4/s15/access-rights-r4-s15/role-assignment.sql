INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Accountant_Offset'),
        (select id from role where roleexpression = 'CostingActivateOwner_Offset'));

INSERT INTO roleassignment (parentroleid, roleid)
VALUES ((select id from role where roleexpression = 'Accountant_Offset'),
        (select id from role where roleexpression = 'CostingViewer'));