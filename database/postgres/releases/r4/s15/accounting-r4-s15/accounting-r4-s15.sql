\ir 'DObPaymentRequest/DObPaymentRequest.sql'
\ir 'DObPaymentRequest/DObPaymentRequest_Views.sql'
\ir 'DObVendorInvoice/DObVendorInvoice_Views.sql'
\ir 'DObLandedCost/DObLandedCost_Drop_Dependencies.sql'
\ir 'DObLandedCost/DObLandedCost.sql'
\ir 'DObLandedCost/DObLandedCost_Views.sql'
\ir 'DObCosting/drop_views.sql'
\ir 'DObCosting/Costing.sql'
\ir 'DObCosting/Costing_Views.sql'
\ir 'AccountingDocument/AccountingDocument_Views.sql'
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO bdk;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA public TO bdk;
