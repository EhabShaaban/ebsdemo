ALTER TABLE IObCostingItems
    drop column unitLandedCost;
ALTER TABLE IObCostingItems
    drop column unitPrice;

ALTER TABLE IObCostingItems
    ADD COLUMN estimateUnitLandedCost numeric;
ALTER TABLE IObCostingItems
    ADD COLUMN actualUnitLandedCost numeric;


ALTER TABLE IObCostingItems
    ADD COLUMN estimateUnitPrice numeric;
ALTER TABLE IObCostingItems
    ADD COLUMN actualUnitPrice numeric;