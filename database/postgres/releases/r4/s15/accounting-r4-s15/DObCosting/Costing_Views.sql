DROP VIEW IF EXISTS IObCostingDetailsGeneralModel;

create view IObCostingDetailsGeneralModel as
SELECT iobcostingdetails.id,
       dobaccountingdocument.code       AS usercode,
       dobaccountingdocument.currentstates as currentstates,
       iobcostingdetails.refinstanceid,
       iobcostingdetails.creationdate,
       iobcostingdetails.creationinfo,
       iobcostingdetails.modifieddate,
       iobcostingdetails.modificationinfo,
       iobcostingdetails.currencypriceestimatetime,
       iobcostingdetails.currencypriceactualtime,
       cobcurrency.iso                  AS localcurrencyiso,
       dobinventorydocument.code        AS goodsreceiptcode,
       doborder.code                    AS purchaseordercode,
       purchaseunit.name ->> 'en'::text AS purchaseunitnameen,
       purchaseunit.name                AS purchaseunitname
FROM iobcostingdetails
         LEFT JOIN dobgoodsreceiptpurchaseorder ON iobcostingdetails.goodsreceiptid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN dobinventorydocument ON dobgoodsreceiptpurchaseorder.id = dobinventorydocument.id
         LEFT JOIN dobcosting ON iobcostingdetails.refinstanceid = dobcosting.id
         LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = dobcosting.id
         LEFT JOIN iobgoodsreceiptpurchaseorderdata
                   ON iobgoodsreceiptpurchaseorderdata.refinstanceid = dobgoodsreceiptpurchaseorder.id
         LEFT JOIN doborderdocument doborder ON iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata
                   ON iobaccountingdocumentcompanydata.companyid = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON cobcurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN cobenterprise purchaseunit ON iobaccountingdocumentcompanydata.purchaseunitid = purchaseunit.id;

create or replace view IObCostingItemsGeneralModel AS
select IObCostingItems.id,
       dobaccountingdocument.code as userCode,
       IObCostingItems.refinstanceid,
       IObCostingItems.quantity,
       IObCostingItems.stocktype,
       IObCostingItems.tobeconsideredincostingper,
       masterdata.code            as itemCode,
       masterdata.name            as itemName,
       cobmeasure.code            as uomCode,
       cobmeasure.symbol          as uomSymbol,
       IObCostingItems.creationDate,
       IObCostingItems.creationInfo,
       IObCostingItems.modifiedDate,
       IObCostingItems.modificationInfo,
       IObCostingItems.estimateUnitPrice,
       IObCostingItems.actualUnitPrice,
       IObCostingItems.estimateUnitLandedCost,
       IObCostingItems.actualUnitLandedCost

from IObCostingItems
         left join masterdata
                   on IObCostingItems.itemid = masterdata.id and masterdata.objecttypecode = '1'
         left join cobmeasure on IObCostingItems.uomid = cobmeasure.id
         left join dobCosting
                   on IObCostingItems.refinstanceid = dobCosting.id
         left JOIN dobaccountingdocument on dobaccountingdocument.id = dobCosting.id;


CREATE or REPLACE view VendorInvoiceItemWeight as
select invoiceItem.refinstanceid                                          as invoiceId,
       invoiceItem.code                                                   as invoiceCode,
       LCDetails.purchaseorderid                                          as purchaseorderid,
       purchaseOrder.code                                               as purchaseorderCode,
       dobinventorydocument.id                                            as goodsreceiptId,
       dobinventorydocument.code                                          as goodsreceiptCode,
       LC.code                                                            as landedCostCode,
       LC.id                                                              as landedCostId,
       LC.currentstates                                                   as landedCostState,
       invoiceItem.id                                                     as itemId,
       invoiceItem.itemcode                                               as itemCode,
       invoiceItem.totalamount /
       getInvoiceTotalAmountBeforeTaxes(invoiceItem.code, 'VendorInvoice') as itemWeight,
       case
           when
                   LC.currentstates like '%ActualValuesCompleted%'
               then LCSummary.actualtotalamount
           else
               LCSummary.estimatetotalamount
           end                                                            as landedCostTotal,
       invoiceItem.orderunitprice                                         as itemUnitPrice,
       (select case
                   when getAmountPaidViaAccountingDocument(vendorInvoice.code,
                                                           vendorInvoiceCompany.objecttypecode) =
                        0.0
                       then (select avg(iobAccountingdocumentactivationdetails.currencyprice)
                             from iobAccountingdocumentactivationdetails
                                      left JOIN iobpaymentrequestpaymentdetails
                                                on iobpaymentrequestpaymentdetails.refinstanceid =
                                                   iobAccountingdocumentactivationdetails.refinstanceid
                                      left join iobpaymentrequestinvoicepaymentdetails
                                                on iobpaymentrequestinvoicepaymentdetails.id =
                                                   iobpaymentrequestpaymentdetails.id
                             where iobpaymentrequestinvoicepaymentdetails.duedocumentid =
                                   LCDetails.vendorinvoiceid
                   )
                   else (
                       select latestexchangerate.currencyPrice
                       from getLatestCurrencyPrice(iobvendorinvoicepurchaseorder.currencyid,
                                                   iobenterprisebasicdata.currencyid) latestexchangerate
                   )
                   end ) as currencyPrice,
       invoiceItem.orderunitcode,
       invoiceItem.orderunitsymbol
from IObVendorInvoiceItemsGeneralModel invoiceItem
         left join ioblandedcostdetails LCDetails
                   on invoiceItem.refinstanceid = LCDetails.vendorinvoiceId
         left join dobaccountingdocument vendorInvoice
                   on vendorInvoice.id = LCDetails.vendorinvoiceId
         inner join dobvendorinvoice on dobvendorinvoice.id = vendorInvoice.id
         left join doborderdocument purchaseOrder on purchaseOrder.id = LCDetails.purchaseorderid
         left join iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = LCDetails.vendorinvoiceid
         left join iobaccountingdocumentcompanydata vendorInvoiceCompany on vendorInvoiceCompany.refinstanceid = LCDetails.vendorinvoiceId and vendorInvoiceCompany.objecttypecode = 'VendorInvoice'
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = vendorInvoiceCompany.companyid
         inner join dobaccountingdocument LC
                    on LCDetails.refinstanceid = LC.id and
                       LC.objecttypecode = 'LandedCost' and LC.currentstates not like '%Draft%'
         left join IObLandedCostFactorItemsSummary LCSummary on LCSummary.id = LC.id
         inner join iobgoodsreceiptpurchaseorderdata
                    on LCDetails.purchaseorderid = iobgoodsreceiptpurchaseorderdata.purchaseorderid
         inner join dobinventorydocument on dobinventorydocument.id = iobgoodsreceiptpurchaseorderdata.refinstanceid
    and dobinventorydocument.currentstates like '%Active%';

CREATE VIEW UnrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId          as itemId,
       vendorinvoiceitemweight.landedCostState as landedCostState,
       vendorinvoiceitemweight.landedCostCode  as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal            as itemTotalLandedCost,
       (itemweight * landedcosttotal) /
       GRUnrestrectedItem.receivedqtyuoe       as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
        (itemunitprice * currencyprice))       as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode =
                       vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and
                       vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode;

CREATE VIEW PurchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode =
                      unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode =
                           GRDamagedItem.unitofentrycode;


