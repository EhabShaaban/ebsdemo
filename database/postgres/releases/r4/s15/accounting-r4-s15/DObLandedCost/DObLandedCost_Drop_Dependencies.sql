DROP VIEW IF EXISTS PurchasedItemsTotalFinalCost;
DROP VIEW IF EXISTS UnrestrictedItemsSingleUnitFinalCost;
DROP VIEW IF EXISTS VendorInvoiceItemWeight;
DROP VIEW IF EXISTS IObLandedCostFactorItemsSummary;
DROP VIEW IF EXISTS IObLandedCostFactorItemsDetailsGeneralModel;
DROP VIEW IF EXISTS IObLandedCostFactorItemsGeneralModel;