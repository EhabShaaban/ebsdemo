CREATE OR REPLACE VIEW DObLandedCostItemsPaymentRequestsGeneralModel
AS
select row_number() OVER () as id, dobaccountingdocument.code as paymentRequestCode, doborderdocument.code as purchaseOrderCode ,
       masterdata.id as itemId,
       iobpaymentrequestpaymentdetails.netamount
from iobpaymentrequestpaymentdetails left join dobaccountingdocument
                                               on iobpaymentrequestpaymentdetails.refinstanceid = dobaccountingdocument.id and objecttypecode = 'PaymentRequest' and iobpaymentrequestpaymentdetails.duedocumenttype = 'PURCHASEORDER'
                                     left join iobpaymentrequestpurchaseorderpaymentdetails on iobpaymentrequestpaymentdetails.id = iobpaymentrequestpurchaseorderpaymentdetails.id
                                     left join doborderdocument on iobpaymentrequestpurchaseorderpaymentdetails.duedocumentid = doborderdocument.id
                                     left join masterdata on masterdata.id = iobpaymentrequestpaymentdetails.costfactoritemid
where  dobaccountingdocument.currentstates like '%PaymentDone%';

CREATE OR REPLACE VIEW IObLandedCostFactorItemsSummary as
select dobaccountingdocument.code,
       dobaccountingdocument.id,
       companyCurrency.iso                     as companyCurrency,
       sum(estimatevalue)                      as estimateTotalAmount,
       sum(actualvalue)                        as actualTotalAmount,
       (sum(estimatevalue) - sum(actualvalue)) as differenceTotalAmount
from ioblandedcostfactoritems
         left join dobaccountingdocument
                   on dobaccountingdocument.id = ioblandedcostfactoritems.refinstanceid and
                      objecttypecode = 'LandedCost'
         left JOIN ioblandedcostdetails
                   on ioblandedcostdetails.refinstanceid = ioblandedcostfactoritems.refInstanceId
         left join doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join iobordercompany
                   on iobordercompany.refinstanceid = doborderdocument.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid
Group By (ioblandedcostfactoritems.refinstanceid, dobaccountingdocument.id,
          dobaccountingdocument.code,
          companyCurrency.iso);

CREATE OR REPLACE VIEW IObLandedCostFactorItemsDetailsGeneralModel AS
SELECT
    IObLandedCostFactorItemsDetails.id,
    IObLandedCostFactorItemsDetails.refinstanceid,
    IObLandedCostFactorItemsDetails.actualValue,
    IObLandedCostFactorItemsDetails.creationDate,
    IObLandedCostFactorItemsDetails.modifiedDate,
    IObLandedCostFactorItemsDetails.creationInfo,
    IObLandedCostFactorItemsDetails.modificationInfo,

    DObAccountingDocument.code 									   AS documentInfoCode,
    DObAccountingDocument.objectTypeCode 						   AS documentInfoType

FROM IObLandedCostFactorItemsDetails

         LEFT JOIN IObLandedCostFactorItems
                   ON IObLandedCostFactorItems.id = IObLandedCostFactorItemsDetails.refinstanceid

         LEFT JOIN DObAccountingDocument
                   ON DObAccountingDocument.id = IObLandedCostFactorItemsDetails.documentInfoId;

CREATE OR REPLACE VIEW IObLandedCostFactorItemsGeneralModel as
select ioblandedcostfactoritems.id,
       dobaccountingdocument.code,
       ioblandedcostfactoritems.refinstanceid,
       masterdata.code               as itemCode,
       masterdata.name               as itemName,
       estimateValue                 as estimatedvalue,
       actualValue                   as actualvalue,
       (estimateValue - actualValue) as difference,
       companyCurrency.iso           as companyCurrency,
       doborderdocument.code         as orderCode,
       ioblandedcostfactoritems.creationDate,
       ioblandedcostfactoritems.modifiedDate,
       ioblandedcostfactoritems.creationInfo,
       ioblandedcostfactoritems.modificationInfo
from ioblandedcostfactoritems
         left join dobaccountingdocument
                   on dobaccountingdocument.id = ioblandedcostfactoritems.refInstanceId and
                      objecttypecode = 'LandedCost'
         left JOIN masterdata on masterdata.id = ioblandedcostfactoritems.costItemId
         left JOIN ioblandedcostdetails
                   on ioblandedcostdetails.refinstanceid = ioblandedcostfactoritems.refInstanceId
         left join doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join iobordercompany
                   on iobordercompany.refinstanceid = doborderdocument.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid;


