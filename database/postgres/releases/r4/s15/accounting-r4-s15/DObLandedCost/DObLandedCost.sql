Alter TABLE ioblandedcostfactoritems
ALTER COLUMN estimatevalue TYPE numeric,
ALTER COLUMN actualvalue TYPE numeric;

Alter TABLE ioblandedcostfactoritemsdetails
ALTER COLUMN actualvalue TYPE numeric;
