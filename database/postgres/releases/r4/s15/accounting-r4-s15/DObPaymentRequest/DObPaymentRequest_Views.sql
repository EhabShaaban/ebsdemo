CREATE OR REPLACE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       paymentdetails.paymentForm,
       paymentdetails.netAmount           AS amountValue,
       cobcurrency.iso                    AS amountCurrency,
       companyData.purchaseUnitId,
       purchaseunit.name                  AS purchaseUnitName,
       purchaseunit.name:: json ->> 'en'  AS purchaseUnitNameEn,
       purchaseunit.code                  AS purchaseUnitCode,
       businesspartner.code               AS businessPartnerCode,
       businesspartner.name               AS businessPartnerName,
       duedocumenttype                    AS dueDocumentType,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           when duedocumenttype = 'CREDITNOTE' then creditNote.code
           else doborderdocument.code end as referenceDocumentCode,
       case
           when duedocumenttype = 'INVOICE' then '{
             "en": "Vendor Invoice",
             "ar": "فاتورة مورد"
           }'::json
           when duedocumenttype = 'PURCHASEORDER' then '{
             "en": "Purchase Order",
             "ar": "أمر شراء"
           }'::json
           when duedocumenttype = 'CREDITNOTE' then '{
             "en": "Credit Note",
             "ar": "الاستحقاقات الالحقاية"
           }'::json end                   as referenceDocumentType,
       DObPaymentRequest.remaining,
       paymentdetails.expectedDueDate                                                                   AS expectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS fromExpectedDueDate,
       paymentdetails.expectedDueDate                                                                   AS toExpectedDueDate,
       paymentdetails.bankTransRef                                                                      AS bankTransRef,
       userInfo.name                                                                                    AS documentOwner
FROM DObPaymentRequest
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN cobenterprise purchaseunit ON purchaseunit.id = companyData.purchaseunitid
         LEFT JOIN iobpaymentrequestpaymentdetails paymentdetails on dobpaymentrequest.id = paymentdetails.refinstanceid
         LEFT JOIN mobbusinesspartner on mobbusinesspartner.id = paymentdetails.businesspartnerid
         LEFT JOIN masterdata businesspartner on mobbusinesspartner.id = businesspartner.id
         LEFT JOIN IObPaymentRequestInvoicePaymentDetails invoicepaymentdetails
                   on invoicepaymentdetails.id = paymentdetails.id
         LEFT JOIN iobpaymentrequestcreditnotepaymentdetails creditNotepaymentdetails
                   on creditNotepaymentdetails.id = paymentdetails.id
         LEFT JOIN IObPaymentRequestPurchaseOrderPaymentDetails orderpaymentdetails
                   on orderpaymentdetails.id = paymentdetails.id
         LEFT JOIN dobaccountingdocument vendorInvoice on vendorInvoice.id = invoicepaymentdetails.duedocumentid
         LEFT JOIN dobaccountingdocument creditNote on creditNote.id = creditNotepaymentdetails.duedocumentid
         LEFT JOIN doborderdocument on doborderdocument.id = orderpaymentdetails.duedocumentid
         LEFT JOIN cobcurrency on paymentdetails.currencyid = cobcurrency.id
         LEFT JOIN userinfo on userinfo.id = dobaccountingdocument.documentownerid;

CREATE OR REPLACE VIEW IObPaymentRequestPaymentDetailsGeneralModel AS
SELECT pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                                                                       as businessPartnerCode,
       md.name                                                                       as businessPartnerName,
       pd.duedocumenttype,
       getduedocumentcodeaccordingtopaymenttype(pd.id, pd.duedocumenttype)                                           as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso                                                               as currencyISO,
       cobcurrency.name                                                              as currencyName,
       cobcurrency.code                                                              as currencyCode,
       companyCurrency.iso                                                           AS companyCurrencyIso,
       companyCurrency.code                                                          AS companyCurrencyCode,
       companyCurrency.id                                                            AS companyCurrencyId,
       pd.description,
       pd.paymentForm,
       IObCompanyBankDetails.id                                                      AS bankAccountId,
       companyBank.code                                                              AS bankAccountCode,
       companyBank.name                                                              AS bankAccountName,
       cobtreasury.id                                                                AS treasuryAccountId,
       cobtreasury.code                                                              AS treasuryCode,
       cobtreasury.name                                                              AS treasuryName,
       CASE
           WHEN
               IObCompanyBankDetails.accountno IS NULL THEN NULL
           ELSE concat(IObCompanyBankDetails.accountno, ' - ', bankCurrency.iso) END AS bankAccountNumber,
       item.code                                                                     AS costFactorItemCode,
       item.name                                                                     AS costFactorItemName,

       pd.expectedDueDate                                                            AS expectedDueDate,
       pd.bankTransRef                                                               AS bankTransRef
FROM iobpaymentrequestpaymentdetails pd
         LEFT JOIN masterdata md on md.id = pd.businessPartnerId
         LEFT JOIN dobpaymentrequest pr on pr.id = pd.refinstanceid
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = pr.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
         LEFT JOIN cobcurrency on pd.currencyid = cobcurrency.id
         LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
         LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = pd.bankAccountId
         LEFT JOIN cobcurrency bankCurrency on IObCompanyBankDetails.currencyId = bankCurrency.id
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN MObItemGeneralModel item on item.id = pd.costFactorItemId
         LEFT JOIN cobtreasury on cobtreasury.id = pd.treasuryId;

CREATE OR REPLACE VIEW DObPaymentRequestCurrencyPriceGeneralModel AS
SELECT accountingDocument.id,
       accountingDocument.code AS userCode,
       paymentDetails.netamount,
       paymentDetails.duedocumenttype,
       purchaseOrderPaymentDetails.duedocumentid,
       CASE
           WHEN paymentdetails.duedocumenttype::text = 'PURCHASEORDER'::text THEN purchaseOrder.code
           WHEN paymentdetails.duedocumenttype::text = 'INVOICE'::text THEN vendorInvoice.code
           WHEN paymentdetails.duedocumenttype::text = 'CREDITNOTE'::text THEN creditnote.code
           END                 AS referencedocumentcode,
       activationDetails.currencyprice,
       paymentRequest.paymenttype
FROM iobpaymentrequestpaymentdetails paymentDetails
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails creditNotePaymentDetails
                   ON creditNotePaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails purchaseOrderPaymentDetails
                   ON purchaseOrderPaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestinvoicepaymentdetails invoicePaymentDetails
                   ON invoicePaymentDetails.id = paymentDetails.id
         JOIN dobpaymentrequest paymentRequest ON paymentRequest.id = paymentDetails.refinstanceid
         JOIN dobaccountingdocument accountingDocument ON accountingDocument.id = paymentRequest.id
         JOIN iobaccountingdocumentactivationdetails activationDetails
              ON activationDetails.refinstanceid = accountingDocument.id
         LEFT JOIN doborderdocument purchaseOrder ON purchaseOrder.id = purchaseOrderPaymentDetails.duedocumentid
         LEFT JOIN dobaccountingdocument vendorInvoice ON vendorInvoice.id = invoicePaymentDetails.duedocumentid
         LEFT JOIN dobaccountingdocument creditNote ON creditNote.id = creditNotePaymentDetails.duedocumentid;

CREATE Or Replace View DObPaymentRequestActivatePreparationGeneralModel AS
Select paymentRequest.id,
       paymentRequest.code                                                        as paymentRequestCode,
       businessUnit.code                                                          as businessUnitCode,
       company.code                                                               as companyCode,
       cobcurrency.code                                                           as currencyCode,
       cobbank.code                                                               as bankCode,
       iobcompanybankdetails.accountno                                            as bankAccountNo,
       cobtreasury.code                                                           as treasuryCode,
       details.netamount                                                          as paymentNetAmount,
       dobpaymentrequest.paymenttype                                              as paymenttype,
       details.dueDocumentType                                                    as dueDocumentType,
       getDueDocumentCodeAccordingToPaymentType(details.id, details.dueDocumentType   ) as dueDocumentCode,
       getJournalBalanceByCode(details.paymentform, businessUnit.code, company.code, cobcurrency.code,
                               cobbank.code,
                               iobcompanybankdetails.accountno, cobtreasury.code) as balance,
       latestexchangerate.currencyprice                                           as currencyprice,
       cobexchangerate.code                                                       as exchangerate
from dobaccountingdocument paymentRequest
         left join dobpaymentrequest on dobpaymentrequest.id =  paymentRequest.id
         left join iobaccountingdocumentcompanydata paymentCompanyData
                   on paymentRequest.id = paymentCompanyData.refinstanceid
         left join cobenterprise company on company.id = paymentCompanyData.companyid
         left join cobenterprise businessUnit on businessUnit.id = paymentCompanyData.purchaseunitid
         left join iobpaymentrequestpaymentdetails details on details.refinstanceid = paymentRequest.id
         left join cobcurrency on cobcurrency.id = details.currencyid
         left join iobcompanybankdetails on iobcompanybankdetails.id = details.bankaccountid
         left join cobbank on cobbank.id = iobcompanybankdetails.bankid
         left join cobtreasury on cobtreasury.id = details.treasuryid
         left join iobenterprisebasicdata companyData on companyData.refinstanceid = paymentCompanyData.companyid
         left join getLatestCurrencyPriceBasedOnDefaultExchangeRateStrategy(details.currencyid,
                                                                            companyData.currencyid) latestexchangerate
                   on latestexchangerate.firstcurrencyid = details.currencyid
                       and latestexchangerate.secondcurrencyid = companydata.currencyid
         left join cobexchangerate on cobexchangerate.id = latestexchangerate.exchangerateid
WHERE paymentRequest.objecttypecode = 'PaymentRequest'
  AND paymentCompanyData.objecttypecode = 'PaymentRequest'
  AND company.objecttypecode = '1'
  AND businessUnit.objecttypecode = '5';

DROP VIEW IF EXISTS DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel;

CREATE VIEW DObPaymentCalculateRealizedExchangeRateDifferenceGeneralModel AS
SELECT payment.id                                   as id,
       payment.code                                 as paymentCode,
       paymentdetails.netamount                     as paymentAmount,
       companycurrency.id                           as localCurrencyid,
       paymentdebitaccountingdetails.glaccountid    as glaccountid,
       paymentdebitaccountingdetails.subledger      as subledger,
       vendoraccountingdetails.glsubaccountid       as glsubaccountid,
       paymentactivationDetails.currencyprice       as paymentCurrencyPrice,
       vendorinvoiceactivationDetails.currencyprice as vendorInvoiceCurrencyPrice
from dobaccountingdocument payment
         right join dobpaymentrequest on dobpaymentrequest.id = payment.id
         left join iobaccountingdocumentactivationdetails paymentactivationDetails
                   on payment.id = paymentactivationDetails.refinstanceid
         left join iobaccountingdocumentaccountingdetails paymentdebitaccountingdetails
                   on payment.id = paymentdebitaccountingdetails.refinstanceid
         left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails
                   on vendoraccountingdetails.id = paymentdebitaccountingdetails.id
         left join iobpaymentrequestpaymentdetails paymentdetails on paymentdetails.refinstanceid = payment.id
         left join iobpaymentrequestinvoicepaymentdetails invoicepaymentdetails
                   on invoicepaymentdetails.id = paymentdetails.id
         left join iobaccountingdocumentactivationdetails vendorinvoiceactivationDetails
                   on vendorinvoiceactivationDetails.refinstanceid = invoicepaymentdetails.duedocumentid
         LEFT JOIN iobaccountingdocumentcompanydata paymentcompanydata ON payment.id = paymentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata ON iobenterprisebasicdata.refinstanceid = paymentcompanydata.companyid
         LEFT JOIN cobcurrency companycurrency ON companycurrency.id = iobenterprisebasicdata.currencyid
where paymentdebitaccountingdetails.accountingentry = 'DEBIT'

