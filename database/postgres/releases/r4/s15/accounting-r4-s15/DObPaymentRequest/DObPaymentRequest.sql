DROP VIEW dobpaymentrequestgeneralmodel;
DROP VIEW iobpaymentrequestpaymentdetailsgeneralmodel;
DROP VIEW dobpaymentrequestcurrencypricegeneralmodel;
DROP VIEW dobpaymentrequestactivatepreparationgeneralmodel;
DROP VIEW iobvendorinvoicedetailsgeneralmodel;
DROP VIEW doblandedcostitemspaymentrequestsgeneralmodel;

ALTER TABLE DObPaymentRequest
    ALTER COLUMN remaining TYPE NUMERIC;

ALTER TABLE IObPaymentRequestPaymentDetails
    ALTER COLUMN netAmount TYPE NUMERIC;

