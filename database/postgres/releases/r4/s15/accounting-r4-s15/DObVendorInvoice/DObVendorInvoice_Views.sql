CREATE OR REPLACE VIEW IObVendorInvoiceDetailsGeneralModel AS
SELECT dobaccountingdocument.code         as invoiceCode,
       dobvendorInvoice.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                    AS vendorCode,
       masterdata.name :: json ->> 'en'   AS vendorName,
       iobvendorinvoicedetails.refinstanceid,
       iobvendorinvoicedetails.invoiceNumber,
       iobvendorinvoicedetails.invoiceDate,
       cobcurrency.code                   AS currencyCode,
       cobcurrency.iso                    AS currencyISO,
       cobcurrency.name                   AS currencyName,
       cobpaymentterms.code               AS paymentTermCode,
       cobpaymentterms.name               AS paymentTerm,
       doborderdocument.code              AS purchaseOrderCode,
       purchaseUnit.code                  AS purchaseUnitCode,
       purchaseUnit.name                  AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       (select string_agg(CONCAT(dobaccountingdocument.code, '-', IObPaymentRequestPaymentDetails.netamount), ', ')
        from dobaccountingdocument,
             iobpaymentrequestpaymentdetails
        where dobaccountingdocument.id in (select iobvendorinvoicedetailsdownpayment.DownPaymentid
                                           from iobvendorinvoicedetailsdownpayment
                                           where iobvendorinvoicedetailsdownpayment.refInstanceId =
                                                 iobvendorinvoicedetails.id)
          and dobaccountingdocument.objecttypecode = 'PaymentRequest'
          and iobpaymentrequestpaymentdetails.refinstanceid = dobaccountingdocument.id
       )                                  as DownPayment

FROM dobvendorInvoice
         left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN iobvendorinvoicedetails on iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id
         LEFT JOIN masterdata ON iobvendorinvoicedetails.vendorId = masterdata.id
         LEFT JOIN IObVendorInvoicePurchaseOrder ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
    AND purchaseUnit.objecttypecode = '5';
DROP VIEW IF EXISTS DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel;

CREATE VIEW DObVendorInvoiceRealizedExchangeRateDifferenceGeneralModel AS
SELECT accountingDocument.id,
       accountingDocument.code                                                           AS paymentRequestUserCode,
       paymentDetails.duedocumenttype,
       paymentDetails.netamount                                                          AS paymentRequestNetAmount,
       RefPurchaseOrder.code                                                             AS referencedocumentcode,
       activationDetails.currencyprice                                                   AS paymentCurrencyPrice,
       paymentRequest.paymenttype,
       vendorInvoice.code                                                                As vendorInvoiceCode,
       vendorInvoice.objecttypecode                                                      AS vendorObjectTypeCode,
       companyData.currencyid                                                            AS companyCurrencyId,
       iobvendorinvoicepurchaseorder.currencyid                                          AS vendorInvoiceCurrencyId,
       vendorInvoiceActivationdetails.currencyprice                                      AS vendorInvoiceCurrencyPrice,
       vendorInvoiceActivationdetails.exchangerateid,
       iobvendoraccountinginfo.chartofaccountid                                          AS vendorInvoiceglaccountId,
       iobvendorinvoicedetails.vendorid                                                  AS vendorInvoiceglsubaccountId,
       leafglaccount.leadger                                                             AS vendorInvoiceAccountLedger

FROM iobpaymentrequestpaymentdetails paymentDetails
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails creditNotePaymentDetails
                   ON creditNotePaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestpurchaseorderpaymentdetails purchaseOrderPaymentDetails
                   ON purchaseOrderPaymentDetails.id = paymentDetails.id
         LEFT JOIN iobpaymentrequestinvoicepaymentdetails invoicePaymentDetails
                   ON invoicePaymentDetails.id = paymentDetails.id
         JOIN dobpaymentrequest paymentRequest ON paymentRequest.id = paymentDetails.refinstanceid
         JOIN dobaccountingdocument accountingDocument ON accountingDocument.id = paymentRequest.id
         JOIN iobaccountingdocumentactivationdetails activationDetails
              ON activationDetails.refinstanceid = accountingDocument.id
         LEFT JOIN doborderdocument RefPurchaseOrder ON RefPurchaseOrder.id = purchaseOrderPaymentDetails.duedocumentid

         LEFT JOIN iobvendorinvoicepurchaseorder ON iobvendorinvoicepurchaseorder.purchaseorderid = RefPurchaseOrder.id
         LEFT JOIN dobaccountingdocument vendorInvoice ON iobvendorinvoicepurchaseorder.refinstanceid = vendorInvoice.id
         LEFT JOIN dobvendorinvoice ON vendorInvoice.id = dobvendorinvoice.id
         LEFT JOIN iobaccountingdocumentactivationdetails vendorInvoiceActivationdetails
                   ON vendorInvoice.id = vendorInvoiceActivationdetails.refinstanceid
         LEFT JOIN iobaccountingdocumentcompanydata
                   ON vendorInvoice.id = iobaccountingdocumentcompanydata.refinstanceid
         LEFT JOIN iobenterprisebasicdata companyData ON companyData.refinstanceid = iobaccountingdocumentcompanydata.companyid
         LEFT JOIN iobvendorinvoicedetails ON iobvendorinvoicedetails.refinstanceid = dobvendorinvoice.id
         LEFT JOIN iobvendoraccountinginfo ON iobvendoraccountinginfo.refinstanceid = iobvendorinvoicedetails.vendorid
         LEFT JOIN leafglaccount ON leafglaccount.id = iobvendoraccountinginfo.chartofaccountid
WHERE paymentDetails.duedocumenttype::text = 'PURCHASEORDER'::text;


CREATE or REPLACE VIEW IObVendorInvoiceItemsGeneralModel AS
SELECT IObVendorInvoiceItem.id,
       vendorInvoice.code,
       item.code                                                                               as itemCode,
       item.id                                                                                 as itemId,
       item.name                                                                               as itemName,
       baseUnitOfMeasure.code                                                                  as baseUnitCode,
       baseUnitOfMeasure.symbol                                                                as baseUnitSymbol,
       orderUnitOfMeasure.code                                                                 as orderUnitCode,
       orderUnitOfMeasure.symbol                                                               as orderunitsymbol,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  AS conversionFactorToBase,
       ((IObVendorInvoiceItem.quantityinorderunit *
         getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                   IObVendorInvoiceItem.itemid)) * IObVendorInvoiceItem.price) as totalAmount,
       IObVendorInvoiceItem.creationDate,
       IObVendorInvoiceItem.modifiedDate,
       IObVendorInvoiceItem.creationInfo,
       IObVendorInvoiceItem.modificationInfo,
       IObVendorInvoiceItem.quantityinorderunit,
       IObVendorInvoiceItem.refinstanceid,
       IObVendorInvoiceItem.price,
       IObVendorInvoiceItem.price *
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  as orderUnitPrice,
       orderUnitOfMeasure.id                                                                   as orderUnitId
FROM IObVendorInvoiceItem
         left join masterdata item on IObVendorInvoiceItem.itemId = item.id
         left join cobmeasure orderUnitOfMeasure on IObVendorInvoiceItem.orderunitofmeasureid = orderUnitOfMeasure.id
         left join mobitem on IObVendorInvoiceItem.itemId = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument vendorInvoice on IObVendorInvoiceItem.refInstanceId = vendorInvoice.id
         left join dobvendorinvoice on vendorInvoice.id = dobvendorinvoice.id;

