CREATE OR REPLACE VIEW IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderItemQuantities.id,
       IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype,
       IObGoodsReceiptPurchaseOrderItemQuantities.notes,
       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedCost,
       IObGoodsReceiptPurchaseOrderItemQuantities.actualCost,
       cobmeasure.symbol                                        AS unitofentry,
       cobmeasure.code                                          AS unitofentrycode,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
                    iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtybase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe)
               * iobalternativeuom.conversionfactor
           ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
           END ::numeric                                        AS receivedqtyWithDefectsbase,
       CASE
           WHEN iobalternativeuom.alternativeunitofmeasureid =
                IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
               THEN iobalternativeuom.conversionfactor
           ELSE 1 END ::numeric                                 AS baseunitfactor,
       mobitemgeneralmodel.code                                 as itemCode,
       mobitemgeneralmodel.name                                 as itemName,
       dobinventorydocument.code                                AS goodsreceiptcode,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                                        AS itemCodeAtVendor,
       cobmeasure.id                                            AS unitofentryid
FROM IObGoodsReceiptPurchaseOrderItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;