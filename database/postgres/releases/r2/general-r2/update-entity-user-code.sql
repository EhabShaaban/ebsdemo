--TODO: Generalize this script for all DOb objects
DO
$$
    DECLARE userCode TEXT;
    DECLARE tableName TEXT;
    BEGIN
        FOR tableName IN (SELECT type FROM entityusercode) LOOP
            CASE
                WHEN tableName = 'MObItem' THEN
                    EXECUTE format('SELECT code FROM masterdata WHERE objecttypecode = %L ORDER BY code DESC LIMIT 1', '1') INTO userCode;
                    DELETE from EntityUserCode WHERE type = tableName;
                    EXECUTE format('INSERT INTO EntityUserCode (type, code) values (%L, %L)', tableName, userCode);

                WHEN tableName = 'MObVendor' THEN
                    EXECUTE format('SELECT code FROM masterdata WHERE objecttypecode = %L ORDER BY code DESC LIMIT 1', '2') INTO userCode;
                    DELETE from EntityUserCode WHERE type = tableName;
                    EXECUTE format('INSERT INTO EntityUserCode (type, code) values (%L, %L)', tableName, userCode);

                WHEN tableName = 'MObCustomer' THEN
                    EXECUTE format('SELECT code FROM masterdata WHERE objecttypecode = %L ORDER BY code DESC LIMIT 1', '3') INTO userCode;
                    DELETE from EntityUserCode WHERE type = tableName;
                    EXECUTE format('INSERT INTO EntityUserCode (type, code) values (%L, %L)', tableName, userCode);

                WHEN tableName = ANY (ARRAY['ItemVendorRecord']) THEN
                    EXECUTE format('SELECT code FROM %s ORDER BY code DESC LIMIT 1', tableName) INTO userCode;
                    DELETE from EntityUserCode WHERE type = tableName;
                    EXECUTE format('INSERT INTO EntityUserCode (type, code) values (%L, %L)', tableName, userCode);

                WHEN tableName = ANY (ARRAY['DObSalesOrder']) THEN
                    EXECUTE format('SELECT code FROM DObOrderDocument WHERE objectTypeCode LIKE %L ORDER BY code DESC LIMIT 1','SALES_ORDER') INTO userCode;
                    DELETE from EntityUserCode WHERE type = tableName;
                    EXECUTE format('INSERT INTO EntityUserCode (type, code) values (%L, %L)', tableName, userCode);

                WHEN tableName = ANY (ARRAY['DObSalesReturnOrder']) THEN
                    EXECUTE format('SELECT code FROM DObOrderDocument WHERE objectTypeCode LIKE %L ORDER BY code DESC LIMIT 1', 'SALES_RETURN') INTO userCode;
                    DELETE from EntityUserCode WHERE type = tableName;
                    EXECUTE format('INSERT INTO EntityUserCode (type, code) values (%L, %L)', tableName, userCode);

                WHEN tableName = ANY (ARRAY['DObPurchaseOrder']) THEN
                    EXECUTE format('SELECT code FROM DObOrderDocument WHERE objectTypeCode LIKE %L ORDER BY code DESC LIMIT 1', '%_PO') INTO userCode;
                    DELETE from EntityUserCode WHERE type = tableName;
                    EXECUTE format('INSERT INTO EntityUserCode (type, code) values (%L, %L)', tableName, userCode);

                ELSE
                    CONTINUE;
            END CASE;
        END LOOP;
    END
$$;