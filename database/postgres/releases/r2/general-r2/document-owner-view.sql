DROP VIEW IF EXISTS DocumentOwnerGeneralModel CASCADE;

CREATE VIEW DocumentOwnerGeneralModel AS
SELECT DISTINCT permission.permissionexpression,
                permission.objectname,
                userInfo.id   as userId,
                userInfo.name as userName,
                permissionassignment.authorizationconditionid,
                authorizationcondition.condition
FROM permission
         LEFT JOIN permissionassignment ON permission.id = permissionassignment.permissionid
         FULL OUTER JOIN authorizationcondition ON permissionassignment.authorizationconditionid =
                                                   authorizationcondition.id
         LEFT JOIN roleassignment ON roleassignment.roleid = permissionassignment.roleid
         LEFT JOIN userassignment ON userassignment.roleid = roleassignment.parentroleid
         LEFT JOIN role ON role.id = userassignment.roleid
         LEFT JOIN useraccount ON userassignment.userid = useraccount.id
         LEFT JOIN ebsuser ON useraccount.userid = ebsuser.id
         LEFT JOIN userInfo ON userInfo.id = ebsuser.id
WHERE permission.permissionexpression LIKE '%:CanBeDocumentOwner'
