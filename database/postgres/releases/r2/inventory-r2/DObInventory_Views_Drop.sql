DROP VIEW IF Exists TObGoodsIssueStoreTransactionGeneralModel CASCADE;
DROP VIEW IF Exists TObStoreTransactionAllDataGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssueStoreGeneralModel CASCADE;
DROP VIEW IF Exists DObGoodsIssueGeneralModel CASCADE;
DROP VIEW IF Exists IObInventoryCompanyGeneralModel CASCADE;
DROP VIEW IF Exists DObGoodsIssueDataGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssueItemGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssuePostingDetailsGeneralModel CASCADE;
DROP VIEW IF Exists IObInventoryActivationDetailsGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssueConsigneeGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssueSalesInvoiceDataGeneralModel CASCADE;

DROP VIEW IF Exists DObGoodsReceiptGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptRecievedItemsGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptReceivedItemsGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptOrdinaryRecievedItemGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptPurchaseOrderGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptPurchaseOrderDataGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptHeaderGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsReceiptAccountingDocumentDetailsGeneralModel CASCADE;
DROP VIEW IF Exists TObGoodsReceiptStoreTransactionGeneralModel CASCADE;
DROP VIEW IF Exists TObGoodsReceiptStoreTransactionDataGeneralModel CASCADE;

DROP VIEW IF Exists DObStockTransformationGeneralModel CASCADE;

