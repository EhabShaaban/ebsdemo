DROP TABLE IF EXISTS DObGoodsIssueSalesInvoice;
DROP TABLE IF EXISTS IObGoodsIssueSalesOrderData;
DROP TABLE IF EXISTS DObGoodsIssueSalesOrder;

CREATE TABLE DObGoodsIssueSalesInvoice
(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    typeId INT8              NOT NULL
);


CREATE TABLE DObGoodsIssueSalesOrder
(
    id BIGSERIAL NOT NULL PRIMARY KEY,
    typeId INT8              NOT NULL
);

CREATE TABLE IObGoodsIssueSalesOrderData
(
    id                    BIGSERIAL     NOT NULL,
    refInstanceId         INT8          NOT NULL,
    creationDate          TIMESTAMP     NOT NULL,
    modifiedDate          TIMESTAMP     NOT NULL,
    creationInfo          VARCHAR(1024) NOT NULL,
    modificationInfo      VARCHAR(1024) NOT NULL,
    salesOrderId          INT8,
    customerId            INT8,
    addressId             INT8,
    contactPersonId       INT8,
    salesRepresentativeId INT8,
    comments              VARCHAR(1024),
    PRIMARY KEY (id)
);


ALTER TABLE iobinventorycompany
    DROP CONSTRAINT FK_IObGoodsIssueStore_refinstanceid;
ALTER TABLE IObGoodsIssueItem
    DROP CONSTRAINT FK_iobGoodsIssueItem_refinstanceid;
ALTER TABLE iobgoodsissueconsignee
    DROP CONSTRAINT fk_iobgoodsissueconsignee_refinstanceid ;
ALTER TABLE IObInventoryActivationDetails
    DROP CONSTRAINT FK_IObGoodsIssuePostingDetails_refinstanceid;
ALTER TABLE TObGoodsIssueStoreTransaction
    DROP CONSTRAINT FK_TObGoodsIssueStoreTransaction_DObGoodsIssue;
ALTER TABLE TObGoodsIssueStoreTransaction
    ADD CONSTRAINT FK_TObGoodsIssueStoreTransaction_DObGoodsIssue FOREIGN KEY (documentReferenceId)
        REFERENCES DObGoodsIssueSalesInvoice (id) ON DELETE RESTRICT;
------------------------------------------------------------------------------------------------------
-- for adding new GI type sales order based --
ALTER TABLE IObGoodsIssueItem RENAME to IObGoodsIssueSalesInvoiceItem;

ALTER SEQUENCE iobgoodsissueitem_id_seq RENAME to iobgoodsissuesalesinvoiceitem_id_seq;

ALTER TABLE IObGoodsIssueSalesInvoiceItem
ALTER COLUMN quantity TYPE double precision;

CREATE TABLE IObGoodsIssueSalesOrderItem
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    itemId           INT8,
    unitOfMeasureId  INT8,
    quantity         double precision,
    batchNo          VARCHAR(1024),
    price            double precision,
    PRIMARY KEY (id)
);

ALTER TABLE IObGoodsIssueSalesOrderItem
    ADD CONSTRAINT FK_iObGoodsIssueSalesOrderItem_item FOREIGN KEY (itemId) REFERENCES mobitem (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueSalesOrderItem
    ADD CONSTRAINT FK_iObGoodsIssueSalesOrderItem_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObGoodsIssueSalesOrder (id) ON DELETE CASCADE;

ALTER TABLE IObGoodsIssueSalesOrderItem
    ADD CONSTRAINT FK_iObGoodsIssueSalesOrderItem_unitOfMeasure FOREIGN KEY (unitOfMeasureId) REFERENCES cobmeasure (id) ON DELETE RESTRICT;

ALTER TABLE tobgoodsissuestoretransaction
    DROP CONSTRAINT fk_tobgoodsissuestoretransaction_dobgoodsissue;
--------------------------------------------------------------------------------------------------------------------------
DROP TABLE IObGoodsIssueSalesInvoiceConsignee;
ALTER TABLE IObGoodsIssueConsignee RENAME to IObGoodsIssueSalesInvoiceData;
ALTER TABLE IObGoodsIssueSalesInvoiceData ADD COLUMN salesInvoiceId INT8;
ALTER TABLE IObGoodsIssueSalesInvoiceData DROP COLUMN type;

ALTER TABLE IObGoodsIssueSalesInvoiceData
    ADD CONSTRAINT FK_IObGoodsIssueSalesInvoiceData_DObSalesInvoice FOREIGN KEY (salesInvoiceId) REFERENCES DObSalesInvoice (id) ON DELETE RESTRICT;
ALTER SEQUENCE iobgoodsissueconsignee_id_seq RENAME TO iobgoodsissuesalesinvoicedata_id_seq;

DROP TABLE DObGoodsIssue;


ALTER TABLE DObGoodsIssueSalesInvoice
    ADD CONSTRAINT FK_DObGoodsIssueSalesInvoice_dobinventorydocument FOREIGN KEY (id) REFERENCES dobinventorydocument (id) ON DELETE CASCADE;
ALTER TABLE DObGoodsIssueSalesInvoice
    ADD CONSTRAINT FK_DObGoodsIssueSalesInvoice_cobgoodsissue FOREIGN KEY (typeId) REFERENCES cobgoodsissue (id) ON DELETE RESTRICT;

ALTER TABLE DObGoodsIssueSalesOrder
    ADD CONSTRAINT FK_DObGoodsIssueSalesOrder_dobinventorydocument FOREIGN KEY (id) REFERENCES dobinventorydocument (id) ON DELETE CASCADE;
ALTER TABLE DObGoodsIssueSalesOrder
    ADD CONSTRAINT FK_DObGoodsIssueSalesOrder_cobgoodsissue FOREIGN KEY (typeId) REFERENCES cobgoodsissue (id) ON DELETE RESTRICT;



ALTER TABLE IObGoodsIssueSalesOrderData
    ADD CONSTRAINT FK_IObGoodsIssueSalesOrderData_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES dobinventorydocument (id) ON DELETE CASCADE;

ALTER TABLE IObGoodsIssueSalesOrderData
    ADD CONSTRAINT FK_IObGoodsIssueSalesOrderData_customer FOREIGN KEY (customerId) REFERENCES MObCustomer (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueSalesOrderData
    ADD CONSTRAINT FK_IObGoodsIssueSalesOrderData_address FOREIGN KEY (addressId) REFERENCES lobaddress (Id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueSalesOrderData
    ADD CONSTRAINT FK_IObGoodsIssueSalesOrderData_contactPerson FOREIGN KEY (contactPersonId) REFERENCES lobcontactperson (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueSalesOrderData
    ADD CONSTRAINT FK_IObGoodsIssueSalesOrderData_cobenterprise FOREIGN KEY (salesrepresentativeId) REFERENCES cobenterprise (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueSalesOrderData
    ADD CONSTRAINT FK_IObGoodsIssueSalesOrderData_DObSalesOrder FOREIGN KEY (salesOrderId) REFERENCES DObSalesOrder (id) ON DELETE RESTRICT;