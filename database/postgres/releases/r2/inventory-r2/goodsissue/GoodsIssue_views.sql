
CREATE VIEW IObGoodsIssueRefDocumentDataGeneralModel AS
SELECT IObGoodsIssueSalesInvoiceData.id,
       IObGoodsIssueSalesInvoiceData.refInstanceId,
       IObGoodsIssueSalesInvoiceData.comments,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesInvoiceData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueCode,
       (SELECT DObInventoryDocument.currentStates
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesInvoiceData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueState,
       (SELECT masterdata.name
        FROM masterdata
        WHERE IObGoodsIssueSalesInvoiceData.customerId = masterdata.id)               AS customerName,
       (SELECT masterdata.code
        FROM masterdata
        WHERE IObGoodsIssueSalesInvoiceData.customerId = masterdata.id)               AS customerCode,
       (SELECT lobaddress.address
        FROM lobaddress
        WHERE IObGoodsIssueSalesInvoiceData.addressId = lobaddress.id)                AS address,
       (SELECT lobcontactperson.name
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesInvoiceData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonName,
       (SELECT lobcontactperson.name:: JSON ->> 'en'
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesInvoiceData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonNameEn,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueSalesInvoiceData.salesRepresentativeId = cobenterprise.id)       AS salesRepresentativeName,
       (SELECT MObCustomerGeneralModel.kapCode
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesInvoiceData.customerId = MObCustomerGeneralModel.id)  AS kapCode,
       (SELECT MObCustomerGeneralModel.kapName
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesInvoiceData.customerId = MObCustomerGeneralModel.id)  AS kapName,
       (SELECT dobaccountingdocument.code
        FROM dobaccountingdocument
        WHERE IObGoodsIssueSalesInvoiceData.salesInvoiceId =
              dobaccountingdocument.id and dobaccountingdocument.objecttypecode = 'SalesInvoice') AS refDocument,
        dobinventorydocument.inventorydocumenttype
FROM IObGoodsIssueSalesInvoiceData
LEFT JOIN dobinventorydocument on IObGoodsIssueSalesInvoiceData.refInstanceId = dobinventorydocument.id

UNION ALL

SELECT IObGoodsIssueSalesOrderData.id,
       IObGoodsIssueSalesOrderData.refInstanceId,
       IObGoodsIssueSalesOrderData.comments,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesOrderData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueCode,
       (SELECT DObInventoryDocument.currentStates
        FROM DObInventoryDocument
        WHERE IObGoodsIssueSalesOrderData.refInstanceId =
              DObInventoryDocument.id)                      AS goodsIssueState,
       (SELECT masterdata.name
        FROM masterdata
        WHERE IObGoodsIssueSalesOrderData.customerId = masterdata.id)               AS customerName,
       (SELECT masterdata.code
        FROM masterdata
        WHERE IObGoodsIssueSalesOrderData.customerId = masterdata.id)               AS customerCode,
       (SELECT lobaddress.address
        FROM lobaddress
        WHERE IObGoodsIssueSalesOrderData.addressId = lobaddress.id)                AS address,
       (SELECT lobcontactperson.name
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesOrderData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonName,
       (SELECT lobcontactperson.name:: JSON ->> 'en'
        FROM lobcontactperson
        WHERE IObGoodsIssueSalesOrderData.contactPersonId =
              lobcontactperson.id)                                                    AS contactPersonNameEn,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueSalesOrderData.salesRepresentativeId = cobenterprise.id)       AS salesRepresentativeName,
       (SELECT MObCustomerGeneralModel.kapCode
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesOrderData.customerId = MObCustomerGeneralModel.id)  AS kapCode,
       (SELECT MObCustomerGeneralModel.kapName
        FROM MObCustomerGeneralModel
        WHERE IObGoodsIssueSalesOrderData.customerId = MObCustomerGeneralModel.id)  AS kapName,
       (SELECT dobsalesorder.code
        FROM dobsalesorder
        WHERE IObGoodsIssueSalesOrderData.salesOrderId =
              dobsalesorder.id )                                                     AS referenceDocument,
        dobinventorydocument.inventorydocumenttype
FROM IObGoodsIssueSalesOrderData
LEFT JOIN dobinventorydocument on IObGoodsIssueSalesOrderData.refInstanceId = dobinventorydocument.id;

CREATE VIEW DObGoodsIssueGeneralModel AS
SELECT DObInventoryDocument.id,
       DObInventoryDocument.code,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.currentStates,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id) AS purchaseUnitName,
       (SELECT cobenterprise.name:: JSON ->> 'en'
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id) AS purchaseUnitNameEn,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id) AS purchaseUnitCode,
       (SELECT cobgoodsissue.name
        FROM cobgoodsissue
        WHERE dobgoodsissuesalesinvoice.typeid = cobgoodsissue.id)    AS typeName,
       (SELECT cobgoodsissue.name:: JSON ->> 'en'
        FROM cobgoodsissue
        WHERE dobgoodsissuesalesinvoice.typeid = cobgoodsissue.id)    AS typeNameEn,
       (SELECT cobgoodsissue.code
        FROM cobgoodsissue
        WHERE dobgoodsissuesalesinvoice.typeid = cobgoodsissue.id)    AS typeCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.plantId = cobenterprise.id)         AS plantName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.plantId = cobenterprise.id)         AS plantCode,
      (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.storehouseid = cobenterprise.id)         AS storehouseCode,
      (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.storehouseid = cobenterprise.id)         AS storehouseName,
      (SELECT storekeeper.code
        FROM storekeeper
        WHERE IObInventoryCompany.storekeeperid = storekeeper.id)         AS storekeeperCode,
       (SELECT storekeeper.name
        FROM storekeeper
        WHERE IObInventoryCompany.storekeeperid = storekeeper.id)         AS storekeeperName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.companyid = cobenterprise.id)         AS companyCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.companyid = cobenterprise.id)         AS companyName,
        MasterData.name                                                AS customerName,
        MasterData.code                                                 AS customerCode,
       DObInventoryDocument.inventoryDocumentType

FROM DObInventoryDocument
         LEFT JOIN IObGoodsIssueSalesInvoiceData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceData.refInstanceId
                   AND DObInventoryDocument.inventoryDocumentType='GI_SI'
        LEFT JOIN MasterData ON IObGoodsIssueSalesInvoiceData.customerId = MasterData.Id AND MasterData.objecttypecode = '3'
         LEFT JOIN IObInventoryCompany
                   ON DObInventoryDocument.id = IObInventoryCompany.refInstanceId
         LEFT JOIN dobgoodsissuesalesinvoice
                   ON DObInventoryDocument.id = dobgoodsissuesalesinvoice.id
where DObInventoryDocument.inventoryDocumentType='GI_SI'

union all

SELECT DObInventoryDocument.id,
       DObInventoryDocument.code,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.currentStates,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id) AS purchaseUnitName,
       (SELECT cobenterprise.name:: JSON ->> 'en'
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id) AS purchaseUnitNameEn,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id) AS purchaseUnitCode,
       (SELECT cobgoodsissue.name
        FROM cobgoodsissue
        WHERE dobgoodsissuesalesorder.typeid = cobgoodsissue.id)    AS typeName,
       (SELECT cobgoodsissue.name:: JSON ->> 'en'
        FROM cobgoodsissue
        WHERE dobgoodsissuesalesorder.typeid = cobgoodsissue.id)    AS typeNameEn,
       (SELECT cobgoodsissue.code
        FROM cobgoodsissue
        WHERE dobgoodsissuesalesorder.typeid = cobgoodsissue.id)    AS typeCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.plantId = cobenterprise.id)         AS plantName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.plantId = cobenterprise.id)         AS plantCode,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.storehouseid = cobenterprise.id)         AS storehouseCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.storehouseid = cobenterprise.id)         AS storehouseName,
       (SELECT storekeeper.code
        FROM storekeeper
        WHERE IObInventoryCompany.storekeeperid = storekeeper.id)         AS storekeeperCode,
       (SELECT storekeeper.name
        FROM storekeeper
        WHERE IObInventoryCompany.storekeeperid = storekeeper.id)         AS storekeeperName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.companyid = cobenterprise.id)         AS companyCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.companyid = cobenterprise.id)         AS companyName,
       MasterData.name                                                AS customerName,
       MasterData.code                                                 AS customerCode,
       DObInventoryDocument.inventoryDocumentType

FROM DObInventoryDocument
         LEFT JOIN IObGoodsIssueSalesOrderData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesOrderData.refInstanceId
                       AND DObInventoryDocument.inventoryDocumentType='GI_SO'
         LEFT JOIN MasterData ON IObGoodsIssueSalesOrderData.customerId = MasterData.Id AND MasterData.objecttypecode = '3'
         LEFT JOIN IObInventoryCompany
                   ON DObInventoryDocument.id = IObInventoryCompany.refInstanceId
         LEFT JOIN dobgoodsissuesalesorder
                   ON DObInventoryDocument.id = dobgoodsissuesalesorder.id
where DObInventoryDocument.inventoryDocumentType='GI_SO';


CREATE VIEW DObGoodsIssueDataGeneralModel AS
SELECT ROW_NUMBER() OVER () AS id,
       DObInventoryDocument.code,
       c1.id                                           AS purchaseUnitId,
       c1.code                                         AS purchaseUnitCode,
       c1.name                                         AS purchaseUnitName,
       DObInventoryDocument.currentStates,
       m1.id                                           AS itemId,
       m1.code                                         AS itemCode,
       m1.name                                         AS itemName,
       cobmeasure.id                                   AS unitOfMeasureId,
       cobmeasure.code                                 AS unitOfMeasureCode,
       cobmeasure.name                                 AS unitOfMeasureName,
       IObGoodsIssueSalesInvoiceItem.quantity,
       IObGoodsIssueSalesInvoiceItem.batchNo,
       IObGoodsIssueSalesInvoiceItem.price,
       CObGoodsIssue.code                              AS goodsIssueTypeCode,
       CObGoodsIssue.name                              AS goodsIssueTypeName,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.modifiedDate,
       dobaccountingdocument.code                      AS referenceDocumentCode,
       c2.id                                           AS companyId,
       c2.code                                         AS companyCode,
       c2.name                                         AS companyName,
       c3.id                                           AS plantId,
       c3.code                                         AS plantCode,
       c3.name                                         AS plantName,
       c4.id                                           AS storeHouseId,
       c4.code                                         AS storeHouseCode,
       c4.name                                         AS storeHouseName,
       storekeeper.code                                AS storekeeperCode,
       storekeeper.name                                AS storekeeperName,
       m2.code                                         AS customerCode,
       m2.name                                         AS customerName,
       lobaddress.address::JSON ->> 'en'               AS address,
       LObContactPerson.name                           AS contactPersonName,
       c5.code                                         AS salesRepresentativeCode,
       c5.name                                         AS salesRepresentativeName,
       IObGoodsIssueSalesInvoiceData.comments,
       c6.code                                         AS kapCode,
       c6.name                                         AS kapName,
       case when iobalternativeuom.conversionfactor is null then 1
            else iobalternativeuom.conversionfactor end as conversionfactor,
       c2.name::JSON ->> 'ar'                          AS companyArName,
       enterpriseTelephone.contactValue                AS companyTelephone,
       enterpriseFax.contactValue                      AS companyFax,
       iobcompanylogodetails.logo                      AS companyLogo,
       IObEnterpriseAddress.addressLine                AS companyAddress,
       IObEnterpriseAddress.addressLine::JSON ->> 'ar' AS companyArAddress,
       m2.name::JSON ->> 'ar'                          AS customerArName

FROM DObInventoryDocument
         RIGHT JOIN dobgoodsissuesalesinvoice ON dobgoodsissuesalesinvoice.id = DObInventoryDocument.id
         LEFT JOIN cobenterprise c1 ON DObInventoryDocument.purchaseunitid = c1.id
         LEFT JOIN IObGoodsIssueSalesInvoiceItem ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceItem.refinstanceid
         LEFT JOIN mobitem ON IObGoodsIssueSalesInvoiceItem.itemid = mobitem.id
         LEFT JOIN masterdata m1 ON mobitem.id = m1.id
         LEFT JOIN cobmeasure ON IObGoodsIssueSalesInvoiceItem.unitOfMeasureId = cobmeasure.id
         LEFT JOIN CObGoodsIssue ON dobgoodsissuesalesinvoice.typeid = CObGoodsIssue.id
         LEFT JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         LEFT JOIN cobcompany ON IObInventoryCompany.companyid = cobcompany.id
         LEFT JOIN cobenterprise c2 ON c2.id = cobcompany.id
         LEFT JOIN cobplant ON IObInventoryCompany.plantid = cobplant.id
         LEFT JOIN cobenterprise c3 ON c3.id = cobplant.id
         LEFT JOIN cobstorehouse ON IObInventoryCompany.storehouseid = cobstorehouse.id
         LEFT JOIN cobenterprise c4 ON c4.id = cobstorehouse.id
         LEFT JOIN storekeeper ON IObInventoryCompany.storekeeperid = storekeeper.id
         LEFT JOIN IObGoodsIssueSalesInvoiceData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceData.refinstanceid
         LEFT JOIN dobaccountingdocument
                   ON IObGoodsIssueSalesInvoiceData.salesInvoiceId = dobaccountingdocument.id
         LEFT JOIN mobcustomer ON IObGoodsIssueSalesInvoiceData.customerid = mobcustomer.id
         INNER JOIN MObBusinessPartner ON mobcustomer.id = MObBusinessPartner.id
         LEFT JOIN masterdata m2 ON MObBusinessPartner.id = m2.id
         LEFT JOIN lobaddress ON IObGoodsIssueSalesInvoiceData.addressid = lobaddress.id
         LEFT JOIN LObContactPerson
                   ON IObGoodsIssueSalesInvoiceData.contactpersonid = LObContactPerson.id
         LEFT JOIN CObenterprise c5 ON IObGoodsIssueSalesInvoiceData.salesrepresentativeid = c5.id
         LEFT JOIN cobenterprise c6 ON MObCustomer.kapid = c6.id
         LEFT JOIN iobalternativeuom
                   ON IObGoodsIssueSalesInvoiceItem.itemId = iobalternativeuom.refinstanceid AND
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         LEFT JOIN IObEnterpriseContact enterpriseTelephone
                   ON cobcompany.id = enterpriseTelephone.refinstanceid AND
                      enterpriseTelephone.objectTypeCode = '1' AND
                      enterpriseTelephone.contactTypeId = 1
         LEFT JOIN IObEnterpriseContact enterpriseFax
                   ON cobcompany.id = enterpriseFax.refinstanceid AND
                      enterpriseFax.objectTypeCode = '1' AND
                      enterpriseFax.contactTypeId = 2
         LEFT JOIN iobcompanylogodetails ON cobcompany.id = iobcompanylogodetails.refinstanceid
         LEFT JOIN IObEnterpriseAddress ON cobcompany.id = IObEnterpriseAddress.refinstanceid
         WHERE IObGoodsIssueSalesInvoiceItem.id IS NOT NULL

UNION ALL

SELECT ROW_NUMBER() OVER ()+ (SELECT COUNT(*) FROM IObGoodsIssueSalesInvoiceItem) AS id,
       DObInventoryDocument.code,
       c1.id                                           AS purchaseUnitId,
       c1.code                                         AS purchaseUnitCode,
       c1.name                                         AS purchaseUnitName,
       DObInventoryDocument.currentStates,
       m1.id                                           AS itemId,
       m1.code                                         AS itemCode,
       m1.name                                         AS itemName,
       cobmeasure.id                                   AS unitOfMeasureId,
       cobmeasure.code                                 AS unitOfMeasureCode,
       cobmeasure.name                                 AS unitOfMeasureName,
       IObGoodsIssueSalesOrderItem.quantity,
       IObGoodsIssueSalesOrderItem.batchNo,
       IObGoodsIssueSalesOrderItem.price,
       CObGoodsIssue.code                              AS goodsIssueTypeCode,
       CObGoodsIssue.name                              AS goodsIssueTypeName,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.modifiedDate,
       dobsalesorder.code                              AS referenceDocumentCode,
       c2.id                                           AS companyId,
       c2.code                                         AS companyCode,
       c2.name                                         AS companyName,
       c3.id                                           AS plantId,
       c3.code                                         AS plantCode,
       c3.name                                         AS plantName,
       c4.id                                           AS storeHouseId,
       c4.code                                         AS storeHouseCode,
       c4.name                                         AS storeHouseName,
       storekeeper.code                                AS storekeeperCode,
       storekeeper.name                                AS storekeeperName,
       m2.code                                         AS customerCode,
       m2.name                                         AS customerName,
       lobaddress.address::JSON ->> 'en'               AS address,
       LObContactPerson.name                           AS contactPersonName,
       c5.code                                         AS salesRepresentativeCode,
       c5.name                                         AS salesRepresentativeName,
       IObGoodsIssueSalesOrderData.comments,
       c6.code                                         AS kapCode,
       c6.name                                         AS kapName,
       case when iobalternativeuom.conversionfactor is null then 1
            else iobalternativeuom.conversionfactor end as conversionfactor,
       c2.name::JSON ->> 'ar'                          AS companyArName,
       enterpriseTelephone.contactValue                AS companyTelephone,
       enterpriseFax.contactValue                      AS companyFax,
       iobcompanylogodetails.logo                      AS companyLogo,
       IObEnterpriseAddress.addressLine                AS companyAddress,
       IObEnterpriseAddress.addressLine::JSON ->> 'ar' AS companyArAddress,
       m2.name::JSON ->> 'ar'                          AS customerArName

FROM DObInventoryDocument
         RIGHT JOIN dobgoodsissuesalesorder ON dobgoodsissuesalesorder.id = DObInventoryDocument.id
         LEFT JOIN cobenterprise c1 ON DObInventoryDocument.purchaseunitid = c1.id
         LEFT JOIN IObGoodsIssueSalesOrderItem ON DObInventoryDocument.id = IObGoodsIssueSalesOrderItem.refinstanceid
         LEFT JOIN mobitem ON IObGoodsIssueSalesOrderItem.itemid = mobitem.id
         LEFT JOIN masterdata m1 ON mobitem.id = m1.id
         LEFT JOIN cobmeasure ON IObGoodsIssueSalesOrderItem.unitOfMeasureId = cobmeasure.id
         LEFT JOIN CObGoodsIssue ON dobgoodsissuesalesorder.typeid = CObGoodsIssue.id
         LEFT JOIN IObInventoryCompany ON DObInventoryDocument.id = IObInventoryCompany.refinstanceid
         LEFT JOIN cobcompany ON IObInventoryCompany.companyid = cobcompany.id
         LEFT JOIN cobenterprise c2 ON c2.id = cobcompany.id
         LEFT JOIN cobplant ON IObInventoryCompany.plantid = cobplant.id
         LEFT JOIN cobenterprise c3 ON c3.id = cobplant.id
         LEFT JOIN cobstorehouse ON IObInventoryCompany.storehouseid = cobstorehouse.id
         LEFT JOIN cobenterprise c4 ON c4.id = cobstorehouse.id
         LEFT JOIN storekeeper ON IObInventoryCompany.storekeeperid = storekeeper.id
         LEFT JOIN IObGoodsIssueSalesOrderData
                   ON DObInventoryDocument.id = IObGoodsIssueSalesOrderData.refinstanceid
         LEFT JOIN dobsalesorder
                   ON IObGoodsIssueSalesOrderData.salesorderid = dobsalesorder.id
         LEFT JOIN mobcustomer ON IObGoodsIssueSalesOrderData.customerid = mobcustomer.id
         LEFT JOIN MObBusinessPartner ON mobcustomer.id = MObBusinessPartner.id
         LEFT JOIN masterdata m2 ON MObBusinessPartner.id = m2.id
         LEFT JOIN lobaddress ON IObGoodsIssueSalesOrderData.addressid = lobaddress.id
         LEFT JOIN LObContactPerson
                   ON IObGoodsIssueSalesOrderData.contactpersonid = LObContactPerson.id
         LEFT JOIN CObenterprise c5 ON IObGoodsIssueSalesOrderData.salesRepresentativeId = c5.id
         LEFT JOIN cobenterprise c6 ON MObCustomer.kapid = c6.id
         LEFT JOIN iobalternativeuom
                   ON IObGoodsIssueSalesOrderItem.itemId = iobalternativeuom.refinstanceid AND
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         LEFT JOIN IObEnterpriseContact enterpriseTelephone
                   ON cobcompany.id = enterpriseTelephone.refinstanceid AND
                      enterpriseTelephone.objectTypeCode = '1' AND
                      enterpriseTelephone.contactTypeId = 1
         LEFT JOIN IObEnterpriseContact enterpriseFax
                   ON cobcompany.id = enterpriseFax.refinstanceid AND
                      enterpriseFax.objectTypeCode = '1' AND
                      enterpriseFax.contactTypeId = 2
         LEFT JOIN iobcompanylogodetails ON cobcompany.id = iobcompanylogodetails.refinstanceid
         LEFT JOIN IObEnterpriseAddress ON cobcompany.id = IObEnterpriseAddress.refinstanceid
                  WHERE IObGoodsIssueSalesOrderItem.id IS NOT NULL;

CREATE VIEW IObGoodsIssueItemGeneralModel AS
SELECT IObGoodsIssueSalesInvoiceItem.id,
       IObGoodsIssueSalesInvoiceItem.refInstanceId,
       DObInventoryDocument.code AS goodsIssueCode,
       masterdata.name AS itemName,
       masterdata.name::JSON ->> 'en' AS itemNameEn,
       masterdata.code AS itemCode,
       c1.symbol AS unitOfMeasureSymbol,
       c1.symbol::JSON ->> 'en' AS unitOfMeasureSymbolEn,
       c1.code AS unitOfMeasureCode,
       IObGoodsIssueSalesInvoiceItem.quantity,
       c2.code as baseUnitCode,
       c2.symbol as baseUnitSymbol,
       round(CASE
                 WHEN IObGoodsIssueSalesInvoiceItem.unitOfMeasureId =
                      iobalternativeuom.alternativeunitofmeasureid
                     THEN IObGoodsIssueSalesInvoiceItem.quantity *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsIssueSalesInvoiceItem.quantity
                 END ::numeric,
             3) AS receivedqtybase
             ,
       IObGoodsIssueSalesInvoiceItem.batchNo,
       IObGoodsIssueSalesInvoiceItem.price
FROM IObGoodsIssueSalesInvoiceItem
left join DObInventoryDocument on IObGoodsIssueSalesInvoiceItem.refInstanceId = DObInventoryDocument.id
left join mobitem on IObGoodsIssueSalesInvoiceItem.itemId = mobitem.id
left join masterdata on mobitem.id = masterdata.id
left join cobmeasure c1 on IObGoodsIssueSalesInvoiceItem.unitOfMeasureId = c1.id
left join cobmeasure c2 on mobitem.basicunitofmeasure = c2.id
left join iobalternativeuom on iobalternativeuom.refinstanceid = mobitem.id
and iobalternativeuom.alternativeunitofmeasureid = IObGoodsIssueSalesInvoiceItem.unitofmeasureid

UNION ALL

SELECT IObGoodsIssueSalesOrderItem.id,
       IObGoodsIssueSalesOrderItem.refInstanceId,
       DObInventoryDocument.code AS goodsIssueCode,
       masterdata.name AS itemName,
       masterdata.name::JSON ->> 'en' AS itemNameEn,
       masterdata.code AS itemCode,
       c1.symbol AS unitOfMeasureSymbol,
       c1.symbol::JSON ->> 'en' AS unitOfMeasureSymbolEn,
       c1.code AS unitOfMeasureCode,
       IObGoodsIssueSalesOrderItem.quantity,
       c2.code as baseUnitCode,
       c2.symbol as baseUnitSymbol,
       round(CASE
                 WHEN IObGoodsIssueSalesOrderItem.unitOfMeasureId =
                      iobalternativeuom.alternativeunitofmeasureid
                     THEN IObGoodsIssueSalesOrderItem.quantity *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsIssueSalesOrderItem.quantity
                 END ::numeric,
             3) AS receivedqtybase
             ,
       IObGoodsIssueSalesOrderItem.batchNo,
       IObGoodsIssueSalesOrderItem.price
FROM IObGoodsIssueSalesOrderItem
left join DObInventoryDocument on IObGoodsIssueSalesOrderItem.refInstanceId = DObInventoryDocument.id
left join mobitem on IObGoodsIssueSalesOrderItem.itemId = mobitem.id
left join masterdata on mobitem.id = masterdata.id
left join cobmeasure c1 on IObGoodsIssueSalesOrderItem.unitOfMeasureId = c1.id
left join cobmeasure c2 on mobitem.basicunitofmeasure = c2.id
left join iobalternativeuom on iobalternativeuom.refinstanceid = mobitem.id
and iobalternativeuom.alternativeunitofmeasureid = IObGoodsIssueSalesOrderItem.unitofmeasureid

