DROP TABLE if EXISTS DObInventoryDocument;

CREATE TABLE DObInventoryDocument
(
    id                    BIGSERIAL                NOT NULL,
    code                  VARCHAR(1024)            NOT NULL,
    creationDate          TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate          TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo          VARCHAR(1024)            NOT NULL,
    modificationInfo      VARCHAR(1024)            NOT NULL,
    currentStates         VARCHAR(1024)            NOT NULL,
    purchaseUnitId        INT8                     NOT NULL,
    inventoryDocumentType VARCHAR(1024)            NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE DObInventoryDocument
    ADD CONSTRAINT FK_DObInventoryDocument_CObEnterprise FOREIGN KEY (purchaseUnitId) REFERENCES CObEnterprise (id) ON DELETE RESTRICT;


ALTER TABLE IObGoodsIssueStore
    RENAME TO IObInventoryCompany;

ALTER SEQUENCE iobgoodsissuestore_id_seq RENAME TO IObInventoryCompany_id_seq;

ALTER TABLE iobgoodsissuepostingdetails RENAME COLUMN postingDate TO activationDate;
ALTER TABLE iobgoodsissuepostingdetails RENAME COLUMN postedBy TO activatedBy;

ALTER TABLE iobgoodsissuepostingdetails RENAME TO IObInventoryActivationDetails;

ALTER SEQUENCE iobgoodsissuepostingdetails_id_seq RENAME TO IObInventoryActivationDetails_id_seq;

ALTER TABLE iobinventorycompany
    ADD CONSTRAINT FK_IObInventoryCompany_DObInventoryDocument FOREIGN KEY (refInstanceId) REFERENCES DObInventoryDocument (id) ON DELETE CASCADE;

ALTER TABLE IObInventoryActivationDetails
    ADD CONSTRAINT FK_IObInventoryActivationDetails_DObInventoryDocument FOREIGN KEY (refInstanceId) REFERENCES DObInventoryDocument (id) ON DELETE CASCADE;
