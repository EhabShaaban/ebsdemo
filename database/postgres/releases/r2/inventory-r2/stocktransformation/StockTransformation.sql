
ALTER TABLE DObStockTransformation DROP COLUMN code;
ALTER TABLE DObStockTransformation DROP COLUMN creationdate;
ALTER TABLE DObStockTransformation DROP COLUMN modifieddate;
ALTER TABLE DObStockTransformation DROP COLUMN creationinfo;
ALTER TABLE DObStockTransformation DROP COLUMN modificationinfo;
ALTER TABLE DObStockTransformation DROP COLUMN currentstates;
ALTER TABLE DObStockTransformation DROP COLUMN purchaseUnitId;

ALTER TABLE DObStockTransformation
    ADD CONSTRAINT FK_DObStockTransformation_DObInventoryDocument FOREIGN KEY (id) REFERENCES DObInventoryDocument (id) ON DELETE CASCADE;

DROP TABLE IObStockTransformationPostingDetails;
