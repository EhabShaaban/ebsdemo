
CREATE VIEW DObStockTransformationGeneralModel
AS SELECT
       DObStockTransformation.id                                                   ,
       DObInventoryDocument.code                                                   ,
       DObInventoryDocument.creationDate                                           ,
       DObInventoryDocument.modifiedDate                                           ,
       DObInventoryDocument.creationInfo                                           ,
       DObInventoryDocument.modificationInfo                                       ,
       DObInventoryDocument.currentStates                                          ,
       IObStockTransformationDetails.newStockType                                  ,
       IObStockTransformationDetails.transformedQty                                ,
       CObEnterprise.code                            as purchaseUnitCode           ,
       CObEnterprise.name                            as purchaseUnitname           ,
       CObEnterprise.name :: json ->> 'en'           as purchaseUnitNameEn         ,
       refStoreTransaction.code                      as refstoreTransactionCode    ,
       newStoreTransaction.Code                      as newstoreTransactionCode    ,
       type.code                                     as stockTransformationTypeCode,
       type.name                                     as stockTransformationTypeName,
       reason.code                                   as transformationReasonCode   ,
       reason.name                                   as transformationReasonName

   FROM DObStockTransformation
            LEFT JOIN DObInventoryDocument ON DObStockTransformation.id = DObInventoryDocument.id AND inventorydocumenttype = 'StkTr'
            LEFT JOIN IObStockTransformationDetails ON DObStockTransformation.id = IObStockTransformationDetails.refInstanceId
            LEFT JOIN CObEnterprise ON DObInventoryDocument.purchaseUnitId = CObEnterprise.id
            LEFT JOIN CObStockTransformationType type ON DObStockTransformation.transformationTypeId = type.id
            LEFT JOIN CObStockTransformationReason reason ON IObStockTransformationDetails.transformationReasonId = reason.id
            LEFT JOIN TObStoreTransaction refStoreTransaction ON IObStockTransformationDetails.storeTransactionId = refStoreTransaction.id
            LEFT JOIN TObStoreTransaction newStoreTransaction ON IObStockTransformationDetails.newstoreTransactionId = newStoreTransaction.id;
