CREATE VIEW IObInventoryActivationDetailsGeneralModel AS
(
SELECT IObInventoryActivationDetails.id,
       IObInventoryActivationDetails.refInstanceId,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObInventoryActivationDetails.refInstanceId =
              DObInventoryDocument.id
          ) AS inventoryDocumentCode,
       IObInventoryActivationDetails.activatedBy,
       IObInventoryActivationDetails.activationDate
FROM IObInventoryActivationDetails);

CREATE VIEW IObInventoryCompanyGeneralModel AS
(
SELECT IObInventoryCompany.id,
       IObInventoryCompany.refInstanceId,
       (SELECT DObInventoryDocument.code
        FROM DObInventoryDocument
        WHERE IObInventoryCompany.refInstanceId =
              DObInventoryDocument.id)                                     AS inventoryDocumentCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.companyId = cobenterprise.id)    AS companyName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.companyId = cobenterprise.id)    AS companyCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObInventoryCompany.storehouseId = cobenterprise.id) AS storehouseName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObInventoryCompany.storehouseId = cobenterprise.id) AS storehouseCode,
       (SELECT storekeeper.name
        FROM storekeeper
        WHERE IObInventoryCompany.storeKeeperId = storekeeper.id)  AS storeKeeperName,
       (SELECT storekeeper.code
        FROM storekeeper
        WHERE IObInventoryCompany.storeKeeperId = storekeeper.id)  AS storeKeeperCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id)       AS purchaseUnitName,
       (SELECT cobenterprise.name:: JSON ->> 'en'
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id)       AS purchaseUnitNameEn,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE DObInventoryDocument.purchaseUnitId = cobenterprise.id)       AS purchaseUnitCode,
        DObInventoryDocument.inventoryDocumentType

FROM IObInventoryCompany
         JOIN DObInventoryDocument
              ON DObInventoryDocument.id = IObInventoryCompany.refInstanceId
    );

