DROP TABLE IF EXISTS DObGoodsReceiptSalesReturn;
DROP TABLE IF EXISTS CObGoodsReceipt;
DROP TABLE IF EXISTS IObGoodsReceiptSalesReturnData;
DROP TABLE IF EXISTS IObGoodsReceiptPurchaseOrderReceivedItemsData;
DROP TABLE IF EXISTS IObGoodsReceiptPurchaseOrderItemQuantities;
DROP TABLE IF EXISTS IObGoodsReceiptSalesReturnReceivedItemsData;
DROP TABLE IF EXISTS IObGoodsReceiptSalesReturnItemQuantities;


ALTER TABLE IObGoodsReceiptPurchaseOrderData ADD COLUMN comments VARCHAR(1024);

ALTER TABLE DObGoodsReceipt RENAME TO DObGoodsReceiptPurchaseOrder;

ALTER TABLE DObGoodsReceiptPurchaseOrder
    ADD CONSTRAINT FK_DObGoodsReceiptPurchaseOrder_DObInventoryDocument FOREIGN KEY (id) REFERENCES DObInventoryDocument (id) ON DELETE CASCADE;

ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN code;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN creationdate;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN modifieddate;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN postingdate;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN creationinfo;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN modificationinfo;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN currentstates;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN storekeeperid;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN storekeepername;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN storehouseid;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN storehousename;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN plantid;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN plantname;
ALTER TABLE DObGoodsReceiptPurchaseOrder DROP COLUMN type;

ALTER TABLE DObGoodsReceiptPurchaseOrder ADD COLUMN typeId INT8  NOT NULL;

ALTER SEQUENCE dobgoodsreceipt_id_seq RENAME TO DObGoodsReceiptPurchaseOrder_id_seq;

ALTER TABLE iobgoodsreceiptpurchaseorderdata DROP COLUMN purchaseunitid;
ALTER TABLE iobgoodsreceiptpurchaseorderdata DROP COLUMN purchaseunitname;



create table CObGoodsReceipt
(
    id       	     BIGSERIAL      NOT NULL PRIMARY KEY,
    code             VARCHAR(1024)            NOT NULL,
    name             json                     NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentStates    VARCHAR(1024)            NOT NULL
);

ALTER TABLE DObGoodsReceiptPurchaseOrder
    ADD CONSTRAINT FK_CObGoodReceipt_DObGoodsReceiptPurchaseOrder FOREIGN KEY (typeId) REFERENCES CObGoodsReceipt (id) ON DELETE RESTRICT;


CREATE TABLE DObGoodsReceiptSalesReturn
(
    id        BIGSERIAL      NOT NULL PRIMARY KEY,
    typeId    INT8           NOT NULL REFERENCES CObGoodsReceipt (id) ON DELETE RESTRICT
);


CREATE TABLE IObGoodsReceiptSalesReturnData
(
    id       		    BIGSERIAL      NOT NULL PRIMARY KEY,
    refInstanceId           INT8          NOT NULL,
    creationDate            TIMESTAMP     NOT NULL,
    modifiedDate            TIMESTAMP     NOT NULL,
    creationInfo            VARCHAR(1024) NOT NULL,
    modificationInfo        VARCHAR(1024) NOT NULL,
    customerId              INT8                     NOT NULL REFERENCES mobcustomer (id) ON DELETE RESTRICT,
    salesreturnId           INT8                     NOT NULL REFERENCES dobsalesreturnorder (id) ON DELETE RESTRICT,
    SalesRepresentativeId   INT8                     NOT NULL REFERENCES EBSUser (id) ON DELETE RESTRICT,
    comments                VARCHAR(1024)
);

------------------------------------------------------GR_Items-------------------------------------------------------------------
ALTER TABLE iobgoodsreceiptrecieveditemsdata RENAME TO IObGoodsReceiptPurchaseOrderReceivedItemsData;
ALTER SEQUENCE iobgoodsreceiptrecieveditemsdata_id_seq RENAME TO iobgoodsreceiptpurchaseorderreceiveditemsdata_id_seq;

CREATE TABLE IObGoodsReceiptSalesReturnReceivedItemsData
(
    id               BIGSERIAL                    NOT NULL,
    refInstanceId    INT8                         NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE     NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE     NOT NULL,
    creationInfo     VARCHAR(1024)                NOT NULL,
    modificationInfo VARCHAR(1024)                NOT NULL,
    itemId           INT8                         NOT NULL,
    differenceReason VARCHAR(1024),
    objectTypeCode   VARCHAR(1024)                NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (refInstanceId) REFERENCES DObGoodsReceiptSalesReturn (id) ON DELETE CASCADE
);
------------------------------------------------GR_Quantities-----------------------------------------------------------------------
ALTER TABLE IObGoodsReceiptItemQuantities RENAME TO IObGoodsReceiptPurchaseOrderItemQuantities;
ALTER TABLE IObGoodsReceiptPurchaseOrderItemQuantities RENAME column defectDescription TO notes;
ALTER TABLE iobgoodsreceiptitembatches RENAME column defectDescription TO notes;
ALTER SEQUENCE iobgoodsreceiptitemquantities_id_seq RENAME TO iobgoodsreceiptpurchaseorderitemquantities_id_seq;

CREATE TABLE IObGoodsReceiptSalesReturnItemQuantities
(
    id                 BIGSERIAL                    NOT NULL,
    refInstanceId      INT8                         NOT NULL,
    creationDate       TIMESTAMP WITH TIME ZONE     NOT NULL,
    modifiedDate       TIMESTAMP WITH TIME ZONE     NOT NULL,
    creationInfo       VARCHAR(1024)                NOT NULL,
    modificationInfo   VARCHAR(1024)                NOT NULL,
    receivedQtyUoE     double precision,
    unitOfEntryId      INT8,
    stockType          VARCHAR(1024),
    notes              VARCHAR(1024),
    PRIMARY KEY (id),
    FOREIGN KEY (refInstanceId) REFERENCES IObGoodsReceiptSalesReturnReceivedItemsData (id) ON DELETE CASCADE
);

ALTER TABLE IObGoodsReceiptSalesReturnItemQuantities
    ADD CONSTRAINT IObGoodsReceiptSalesReturnItemQuantities_CObMeasure FOREIGN KEY (unitOfEntryId) REFERENCES cobmeasure (id) ON DELETE RESTRICT;
