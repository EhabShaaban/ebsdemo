CREATE VIEW DObGoodsReceiptGeneralModel AS
SELECT DObGoodsReceiptPurchaseOrder.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborder.code                                                    AS refDocumentCode,

       (SELECT CObGoodsReceipt.code
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptPurchaseOrder.typeid = CObGoodsReceipt.id) AS type,
       (SELECT CObGoodsReceipt.name
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptPurchaseOrder.typeid = CObGoodsReceipt.id) AS typeName,
       PurchaseUnitData.name :: json ->> 'en'                           As purchaseUnitNameEn,
       PurchaseUnitData.name                                            As purchaseUnitName,
       PurchaseUnitData.code                                            AS PurchaseUnitCode,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyName,
        (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyCode,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)           as plantName,
       (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)           as plantCode,
       IObGoodsReceiptPurchaseOrderData.vendorName                      as businessPartnerName,
       masterdata.code                                                  as businessPartnercode,
       StorehouseData.storehouseCode,
       StoreKeeperData.storekeeperCode,
       StoreKeeperData.storekeeperName,
       StorehouseData.storehouseName
FROM DObGoodsReceiptPurchaseOrder
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         left join iobinventorycompany
                   on iobinventorycompany.refinstanceid = dobinventorydocument.id
         left join IObInventoryActivationDetails
                   on IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN iobgoodsreceiptpurchaseorderdata
              on iobgoodsreceiptpurchaseorderdata.refinstanceid = DObGoodsReceiptPurchaseOrder.id
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS storehouseCode,
                           CObEnterprise.name AS storehouseName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '4') AS StorehouseData
                   ON iobinventorycompany.storehouseId = StorehouseData.id
         LEFT JOIN (SELECT id,
                           StoreKeeper.code AS storekeeperCode,
                           StoreKeeper.name AS storekeeperName
                    FROM StoreKeeper) AS StoreKeeperData
                   ON iobinventorycompany.storeKeeperId = StoreKeeperData.id
         LEFT JOIN cobenterprise as PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN masterdata On IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id and
                                 masterdata.objecttypecode like '2'
         LEFT JOIN doborder ON iobgoodsreceiptpurchaseorderdata.purchaseOrderId = doborder.id
UNION All

SELECT DObGoodsReceiptSalesReturn.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       dobsalesreturnorder.code                                       AS refDocumentCode,
       (SELECT CObGoodsReceipt.code
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptSalesReturn.typeid = CObGoodsReceipt.id) AS type,
       (SELECT CObGoodsReceipt.name
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptSalesReturn.typeid = CObGoodsReceipt.id) AS typeName,
       PurchaseUnitData.name :: json ->> 'en'                         As purchaseUnitNameEn,
       PurchaseUnitData.name                                          As purchaseUnitName,
       PurchaseUnitData.code                                          AS PurchaseUnitCode,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyName,
        (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyCode,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)         as plantName,
       (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)         as plantCode,
       masterdata.name                                                as businessPartnerName,
       masterdata.code                                                as businessPartnercode,

       StorehouseData.storehouseCode,
       StoreKeeperData.storekeeperCode,
       StoreKeeperData.storekeeperName,
       StorehouseData.storehouseName
FROM DObGoodsReceiptSalesReturn
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         left join iobinventorycompany
                   on iobinventorycompany.refinstanceid = dobinventorydocument.id
         left join IObInventoryActivationDetails
                   on IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN IObGoodsReceiptSalesReturnData
              on IObGoodsReceiptSalesReturnData.refinstanceid = DObGoodsReceiptSalesReturn.id
         LEFT JOIN dobsalesreturnorder
                   on iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS storehouseCode,
                           CObEnterprise.name AS storehouseName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '4') AS StorehouseData
                   ON iobinventorycompany.storehouseId = StorehouseData.id
         LEFT JOIN (SELECT id,
                           StoreKeeper.code AS storekeeperCode,
                           StoreKeeper.name AS storekeeperName
                    FROM StoreKeeper) AS StoreKeeperData
                   ON iobinventorycompany.storeKeeperId = StoreKeeperData.id
         LEFT JOIN cobenterprise as PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN mobcustomer ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id;

CREATE VIEW DObGoodsReceiptDataGeneralModel AS
SELECT DObGoodsReceiptPurchaseOrder.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborder.code                                                    AS refDocumentCode,

       (SELECT CObGoodsReceipt.code
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptPurchaseOrder.typeid = CObGoodsReceipt.id) AS type,
       (SELECT CObGoodsReceipt.name
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptPurchaseOrder.typeid = CObGoodsReceipt.id) AS typeName,
       PurchaseUnitData.id                                              As purchaseUnitId,
       PurchaseUnitData.name :: json ->> 'en'                           As purchaseUnitNameEn,
       PurchaseUnitData.name                                            As purchaseUnitName,
       PurchaseUnitData.code                                            AS PurchaseUnitCode,
       (select id
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyId,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyName,
        (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyCode,
       (select id
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)           as plantId,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)           as plantName,
       (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)           as plantCode,
       IObGoodsReceiptPurchaseOrderData.vendorName                      as businessPartnerName,
       masterdata.code                                                  as businessPartnercode,
       StoreKeeperData.storekeeperCode,
       StoreKeeperData.storekeeperName,
       StorehouseData.id                                                AS storehouseId,
       StorehouseData.storehouseCode,
       StorehouseData.storehouseName
FROM DObGoodsReceiptPurchaseOrder
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         left join iobinventorycompany
                   on iobinventorycompany.refinstanceid = dobinventorydocument.id
         left join IObInventoryActivationDetails
                   on IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN iobgoodsreceiptpurchaseorderdata
              on iobgoodsreceiptpurchaseorderdata.refinstanceid = DObGoodsReceiptPurchaseOrder.id
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS storehouseCode,
                           CObEnterprise.name AS storehouseName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '4') AS StorehouseData
                   ON iobinventorycompany.storehouseId = StorehouseData.id
         LEFT JOIN (SELECT id,
                           StoreKeeper.code AS storekeeperCode,
                           StoreKeeper.name AS storekeeperName
                    FROM StoreKeeper) AS StoreKeeperData
                   ON iobinventorycompany.storeKeeperId = StoreKeeperData.id
         LEFT JOIN cobenterprise as PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN masterdata On IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id and
                                 masterdata.objecttypecode like '2'
         LEFT JOIN doborder ON iobgoodsreceiptpurchaseorderdata.purchaseOrderId = doborder.id
UNION All

SELECT DObGoodsReceiptSalesReturn.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       dobsalesreturnorder.code                                       AS refDocumentCode,
       (SELECT CObGoodsReceipt.code
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptSalesReturn.typeid = CObGoodsReceipt.id) AS type,
       (SELECT CObGoodsReceipt.name
        FROM CObGoodsReceipt
        WHERE DObGoodsReceiptSalesReturn.typeid = CObGoodsReceipt.id) AS typeName,
       PurchaseUnitData.id                                            As purchaseUnitId,
       PurchaseUnitData.name :: json ->> 'en'                         As purchaseUnitNameEn,
       PurchaseUnitData.name                                          As purchaseUnitName,
       PurchaseUnitData.code                                          AS PurchaseUnitCode,
       (select id
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyId,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyName,
        (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.companyid)           as companyCode,
       (select id
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)           as plantId,
       (select name
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)         as plantName,
       (select code
        from cobenterprise
        where cobenterprise.id = iobinventorycompany.plantid)         as plantCode,
       masterdata.name                                                as businessPartnerName,
       masterdata.code                                                as businessPartnercode,
       StoreKeeperData.storekeeperCode,
       StoreKeeperData.storekeeperName,
       StorehouseData.id                                              AS storehouseId,
       StorehouseData.storehouseCode,
       StorehouseData.storehouseName
FROM DObGoodsReceiptSalesReturn
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         left join iobinventorycompany
                   on iobinventorycompany.refinstanceid = dobinventorydocument.id
         left join IObInventoryActivationDetails
                   on IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
         JOIN IObGoodsReceiptSalesReturnData
              on IObGoodsReceiptSalesReturnData.refinstanceid = DObGoodsReceiptSalesReturn.id
         LEFT JOIN dobsalesreturnorder
                   on iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS storehouseCode,
                           CObEnterprise.name AS storehouseName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '4') AS StorehouseData
                   ON iobinventorycompany.storehouseId = StorehouseData.id
         LEFT JOIN (SELECT id,
                           StoreKeeper.code AS storekeeperCode,
                           StoreKeeper.name AS storekeeperName
                    FROM StoreKeeper) AS StoreKeeperData
                   ON iobinventorycompany.storeKeeperId = StoreKeeperData.id
         LEFT JOIN cobenterprise as PurchaseUnitData
                   ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5'
         LEFT JOIN mobcustomer ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id;


CREATE VIEW IObGoodsReceiptPurchaseOrderDataGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderData.id,
       IObGoodsReceiptPurchaseOrderData.refInstanceId,
       IObGoodsReceiptPurchaseOrderData.deliveryNote,
       IObGoodsReceiptPurchaseOrderData.comments,
       dobinventorydocument.code AS goodsReceiptCode,
       POData.purchaseOrderCode,
       VendorData.vendorCode,
       IObGoodsReceiptPurchaseOrderData.vendorName,
       CObEnterprise.code        AS purchaseUnitCode,
       CObEnterprise.name        AS purchaseUnitName,
       PurchaseResponsibleData.purchaseResponsibleCode,
       IObGoodsReceiptPurchaseOrderData.purchaseResponsibleName
FROM IObGoodsReceiptPurchaseOrderData
         LEFT JOIN DObGoodsReceiptPurchaseOrder
                   ON IObGoodsReceiptPurchaseOrderData.refInstanceId =
                      DObGoodsReceiptPurchaseOrder.id
         LEFT join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN CObEnterprise
                   on CObEnterprise.objectTypeCode = '5'
                       AND dobinventorydocument.purchaseunitid = CObEnterprise.id
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS purchaseResponsibleCode,
                           CObEnterprise.name AS purchaseResponsibleName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '6') AS PurchaseResponsibleData
                   ON IObGoodsReceiptPurchaseOrderData.purchaseResponsibleId =
                      PurchaseResponsibleData.id
         LEFT JOIN (SELECT id, masterdata.code AS vendorCode, masterdata.name AS vendorName
                    FROM masterdata
                    WHERE masterdata.objectTypeCode = '2') AS VendorData
                   ON IObGoodsReceiptPurchaseOrderData.vendorId = VendorData.id
         LEFT JOIN (SELECT id, doborder.code AS purchaseOrderCode FROM doborder) AS POData
                   ON IObGoodsReceiptPurchaseOrderData.purchaseOrderId = POData.id;

CREATE VIEW IObGoodsReceiptReceivedItemsGeneralModel AS
SELECT row_number() over()                                                         AS id,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.id                            AS grItemId,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid                 AS goodsreceiptinstanceid,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationdate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modifieddate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationinfo,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                                   as goodsReceiptCode,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.differencereason,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                                    as itemcode,
       mobitemgeneralmodel.name                                                    as itemname,
       mobitemgeneralmodel.batchmanagementrequired                                 as isBatchManaged,
       cobmeasure.symbol                                                           as baseunit,
       cobmeasure.code                                                             as baseunitcode,
       (SELECT quantityInDn
        FROM IObOrderLineDetailsGeneralModel
                 left join iobgoodsreceiptpurchaseorderdatageneralmodel
                           ON iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseOrderCode =
                              IObOrderLineDetailsGeneralModel.orderCode
        WHERE iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
          AND IObOrderLineDetailsGeneralModel.itemCode = mobitemgeneralmodel.code) AS quantityInDn,
       null                                                                        AS returnedQuantityBase
FROM IObGoodsReceiptPurchaseOrderReceivedItemsData
         JOIN DObGoodsReceiptPurchaseOrder
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceId =
                 DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument
                   on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_PO'
         left join mobitemgeneralmodel
                   on IObGoodsReceiptPurchaseOrderReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
UNION ALL
SELECT row_number() over()+(SELECT COUNT(*) FROM IObGoodsReceiptPurchaseOrderReceivedItemsData) AS id,
       IObGoodsReceiptSalesReturnReceivedItemsData.id            AS grItemId,
       IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid AS goodsreceiptinstanceid,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationdate,
       IObGoodsReceiptSalesReturnReceivedItemsData.modifieddate,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationinfo,
       IObGoodsReceiptSalesReturnReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                 as goodsReceiptCode,
       IObGoodsReceiptSalesReturnReceivedItemsData.differencereason,
       IObGoodsReceiptSalesReturnReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                  as itemcode,
       mobitemgeneralmodel.name                                  as itemname,
       mobitemgeneralmodel.batchmanagementrequired               as isBatchManaged,
       cobmeasure.symbol                                         as baseunit,
       cobmeasure.code                                           as baseunitcode,
       null                                                      AS quantityInDn,
       (SELECT SUM(IObSalesReturnOrderItemGeneralModel.returnQuantityInBase)
       FROM IObSalesReturnOrderItemGeneralModel
       WHERE IObSalesReturnOrderItemGeneralModel.refinstanceid = iobgoodsreceiptsalesreturndata.salesreturnid
         AND IObSalesReturnOrderItemGeneralModel.itemId = mobitemgeneralmodel.id)  AS returnedQuantityBase

FROM IObGoodsReceiptSalesReturnReceivedItemsData
         JOIN dobgoodsreceiptsalesreturn
              ON IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceId =
                 dobgoodsreceiptsalesreturn.id
        left JOIN dobinventorydocument
                   on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_SR'
        left JOIN mobitemgeneralmodel
                   on IObGoodsReceiptSalesReturnReceivedItemsData.itemId = mobitemgeneralmodel.id
        left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
        left JOIN iobgoodsreceiptsalesreturndata
                   ON iobgoodsreceiptsalesreturndata.refinstanceid = dobgoodsreceiptsalesreturn.id;

CREATE VIEW IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderItemQuantities.id,
       IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype,
       IObGoodsReceiptPurchaseOrderItemQuantities.notes,
       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedCost,
       IObGoodsReceiptPurchaseOrderItemQuantities.actualCost,
       cobmeasure.symbol                                        AS unitofentry,
       cobmeasure.code                                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe)
                     * iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                                 AS receivedqtyWithDefectsbase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                                 AS baseunitfactor,
       mobitemgeneralmodel.code                                 as itemCode,
       mobitemgeneralmodel.name                                 as itemName,
       dobinventorydocument.code                                AS goodsreceiptcode,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                                        AS itemCodeAtVendor

FROM IObGoodsReceiptPurchaseOrderItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;

CREATE VIEW IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptSalesReturnItemQuantities.id,
       IObGoodsReceiptSalesReturnItemQuantities.refinstanceid AS ordinaryitemid,
       dobinventorydocument.code                              AS goodsreceiptcode,
       IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe,
       IObGoodsReceiptSalesReturnItemQuantities.stocktype,
       IObGoodsReceiptSalesReturnItemQuantities.notes,
       cobmeasure.symbol                                      AS unitofentry,
       cobmeasure.code                                        AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
                     THEN IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                               AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                               AS baseunitfactor,
       masterdata.code                                        as itemCode,
       masterdata.name                                        as itemName,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE masterdata.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                      AS baseUnitSymbol

FROM IObGoodsReceiptSalesReturnItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptSalesReturnItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptSalesReturnReceivedItemsData
              ON IObGoodsReceiptSalesReturnReceivedItemsData.id =
                 IObGoodsReceiptSalesReturnItemQuantities.refinstanceid
         JOIN masterdata
              ON IObGoodsReceiptSalesReturnReceivedItemsData.itemid = masterdata.id
         JOIN DObGoodsReceiptSalesReturn ON DObGoodsReceiptSalesReturn.id =
                                            IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON masterdata.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptSalesReturnItemQuantities.unitofentryid;

CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS

SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.stocktype,
       iobgoodsreceiptitembatches.notes,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE iobgoodsreceiptitembatches.receivedqtyuoe
                 END ::numeric,
             3)                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobinventorydocument.code                as goodsReceiptCode,
       (SELECT baseUnit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code =
              iobgoodsreceiptreceiveditemsgeneralmodel.itemCode
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                        AS itemCodeAtVendor

FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;

create view IObGoodsReceiptAccountingDocumentDetailsGeneralModel AS
select IObGoodsReceiptAccountingDocumentDetails.id,
       dobgoodsreceiptaccountingdocument.code as userCode,
       IObGoodsReceiptAccountingDocumentDetails.refinstanceid,
       IObGoodsReceiptAccountingDocumentDetails.creationDate,
       IObGoodsReceiptAccountingDocumentDetails.creationInfo,
       IObGoodsReceiptAccountingDocumentDetails.modifiedDate,
       IObGoodsReceiptAccountingDocumentDetails.modificationInfo,
       dobinventorydocument.code              as goodsReceiptCode,
       doborder.code                          as purchaseOrderCode
from IObGoodsReceiptAccountingDocumentDetails
         left join DObGoodsReceiptPurchaseOrder
                   on IObGoodsReceiptAccountingDocumentDetails.goodsreceiptid =
                      DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         left join dobgoodsreceiptaccountingdocument
                   on IObGoodsReceiptAccountingDocumentDetails.refinstanceid =
                      dobGoodsReceiptAccountingDocument.id
         left join iobgoodsreceiptpurchaseorderdata
                   on iobgoodsreceiptpurchaseorderdata.refinstanceid =
                      DObGoodsReceiptPurchaseOrder.id
         left join doborder
                   on iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id;


CREATE VIEW IObGoodsReceiptSalesReturnDataGeneralModel AS
SELECT IObGoodsReceiptSalesReturnData.id,
       IObGoodsReceiptSalesReturnData.refInstanceId,
       dobinventorydocument.code          AS goodsReceiptCode,
       dobsalesreturnorder.code           AS salesReturnOrderCode,
       masterdata.name                    as customerName,
       masterdata.code                    as customercode,
       CObEnterprise.code                 AS purchaseUnitCode,
       CObEnterprise.name                 AS purchaseUnitName,
       documentownergeneralmodel.userid   AS salesRepresentativeId,
       documentownergeneralmodel.username AS salesRepresentativeName,
       IObGoodsReceiptSalesReturnData.comments
FROM IObGoodsReceiptSalesReturnData
         LEFT JOIN dobgoodsreceiptsalesreturn
                   ON IObGoodsReceiptSalesReturnData.refInstanceId = dobgoodsreceiptsalesreturn.id
         LEFT join dobinventorydocument on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id
         left join dobsalesreturnorder
                   on iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
         LEFT JOIN mobcustomer ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN CObEnterprise
                   on CObEnterprise.objectTypeCode = '5'
                       AND dobinventorydocument.purchaseunitid = CObEnterprise.id
         LEFT JOIN documentownergeneralmodel
                   ON dobsalesreturnorder.documentownerid = documentownergeneralmodel.userid AND
                      documentownergeneralmodel.objectname = 'SalesReturnOrder';

