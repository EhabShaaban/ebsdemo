
CREATE VIEW TObGoodsIssueStoreTransactionGeneralModel AS
SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
              round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionoperation,
       c1.code            as companyCode,
       c1.name            as companyName,
       c2.code            as storehouseCode,
       c2.name            as storehouseName,
       c3.code            as plantCode,
       c3.name            as plantName,
       c4.code            as purchaseUnitCode,
       c4.name            as purchaseUnitName,
       cobmeasure.symbol  as unitOfMeasureSymbol,
       cobmeasure.name    as unitOfMeasureName,
       cobmeasure.code    as unitOfMeasureCode,
       t2.code            as refTransactionCode,
       masterdata.name    as itemName,
       masterdata.code    as itemCode,
       DObInventoryDocument.code as goodsIssueCode

FROM TObStoreTransaction
         left join TObGoodsIssueStoreTransaction
                   on TObStoreTransaction.id = TObGoodsIssueStoreTransaction.id
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         join mobitem on TObStoreTransaction.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         join DObInventoryDocument on TObGoodsIssueStoreTransaction.documentReferenceId = DObInventoryDocument.id;



CREATE VIEW TObStoreTransactionAllDataGeneralModel AS
    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           DObInventoryDocument.code                 as documentCode

    FROM TObStoreTransaction
             left join TObGoodsIssueStoreTransaction
                       on TObStoreTransaction.id = TObGoodsIssueStoreTransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             join DObInventoryDocument
                  on TObGoodsIssueStoreTransaction.documentReferenceId = DObInventoryDocument.id

    union

SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.originalAddingDate,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
       round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionOperation,
       c1.code                            as companyCode,
       c1.name :: json ->> 'en'           as companyName,
       c2.code                            as storehouseCode,
       c2.name :: json ->> 'en'           as storehouseName,
       c3.code                            as plantCode,
       c3.name :: json ->> 'en'           as plantName,
       c4.code                            as purchaseUnitCode,
       c4.name :: json ->> 'en'           as purchaseUnitName,
       cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
       cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
       cobmeasure.code                    as unitOfMeasureCode,
       t2.code                            as refTransactionCode,
       masterdata.name :: json ->> 'en'   as itemName,
       masterdata.code                    as itemCode,
       'Legacy System نظام المتكامل'      as documentCode

FROM TObStoreTransaction
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         join mobitem on TObStoreTransaction.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         JOIN tobgoodsreceiptstoretransaction on TObStoreTransaction.id = tobgoodsreceiptstoretransaction.id
and tobgoodsreceiptstoretransaction.documentreferenceid is null

    union

    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           dobinventorydocument.code               as documentCode

    FROM TObStoreTransaction
             left join TObGoodsReceiptStoreTransaction
                       on TObStoreTransaction.id = TObGoodsReceiptStoreTransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             JOIN dobinventorydocument
                  on TObGoodsReceiptStoreTransaction.documentReferenceId = dobinventorydocument.id

    union

    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           DObInventoryDocument.code           as documentCode

    FROM TObStoreTransaction
             left join tobstocktransformationstoretransaction
                       on TObStoreTransaction.id = tobstocktransformationstoretransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             JOIN DObInventoryDocument
                  on tobstocktransformationstoretransaction.documentReferenceId = DObInventoryDocument.id and inventoryDocumentType = 'StkTr';




CREATE or REPLACE FUNCTION getStoreTransactionRefDocumentCode(objectTypeCode varchar, transactionId bigint)
  RETURNS varchar AS
$$
BEGIN
  RETURN case
           when objecttypecode = 'GR' and (select documentreferenceid
                                           from tobgoodsreceiptstoretransaction gr
                                           where gr.id = transactionId) is null then 'Legacy System نظام المتكامل'
           when objecttypecode = 'GR' then
             (select code
              from dobinventorydocument
              where id = (select documentreferenceid
                          from tobgoodsreceiptstoretransaction gr
                          where gr.id = transactionId))

           when objecttypecode = 'GI' then
             (select code
              from dobinventorydocument
              where id = (select documentreferenceid
                          from TObGoodsIssueStoreTransaction gi
                          where gi.id = transactionId))

           when objecttypecode = 'STK_TR' then
             (select code
              from dobinventorydocument
              where id = (select documentreferenceid
                          from TObstocktransformationStoreTransaction gi
                          where gi.id = transactionId))

    end;
END;
$$
  LANGUAGE PLPGSQL;

CREATE VIEW TObGoodsReceiptStoreTransactionGeneralModel AS
SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.originalAddingDate,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
              round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionOperation,
       c1.code              as companyCode,
       c1.name              as companyName,
       c2.code              as storehouseCode,
       c2.name              as storehouseName,
       c3.code              as plantCode,
       c3.name              as plantName,
       c4.code              as purchaseUnitCode,
       c4.name              as purchaseUnitName,
       cobmeasure.symbol    AS unitOfMeasureSymbol,
       cobmeasure.name      AS unitOfMeasureName,
       cobmeasure.code      AS unitOfMeasureCode,
       t2.code              as refTransactionCode,
       masterdata.name      AS itemName,
       masterdata.code      AS itemCode,
       getStoreTransactionRefDocumentCode(TObStoreTransaction.objecttypecode,TObStoreTransaction.id)    AS goodsReceiptCode

FROM TObStoreTransaction
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left JOIN TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         JOIN mobitem on TObStoreTransaction.itemId = mobitem.id
         JOIN masterdata on mobitem.id = masterdata.id
  where TObStoreTransaction.objecttypecode = 'GR';

CREATE VIEW TObGoodsReceiptStoreTransactionDataGeneralModel AS
SELECT row_number()
       OVER ()                                                       as id,
       DObInventoryDocument.creationdate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.code,
       DObGoodsReceiptPurchaseOrder.id                                            as goodsReceiptId,
       iobinventorycompany.storehouseid,
       iobinventorycompany.plantid,

       cobplant.companyid,

       dobinventoryDocument.purchaseunitid,

       IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid,

       cobmeasure.symbol                                             as baseUnitSymbol,
       masterdata.code                                               as itemCode,
       masterdata.name                                               as itemName,
       (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
        alternative1.conversionfactor)                               AS receivedQtyBase,

       (iobgoodsreceiptitembatches.receivedqtyuoe *
        alternative2.conversionfactor)                               AS receivedBatchQtyBase,

       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedcost                   AS estimatedCostQty,
       iobgoodsreceiptitembatches.estimatedcost                      AS estimatedCostBatch,

       IObGoodsReceiptPurchaseOrderItemQuantities.actualcost                      AS actualCostQty,
       iobgoodsreceiptitembatches.actualcost                         AS actualCostBatch,

       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe                  as receivedQty,
       IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid                   as receivedUoeId,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype as receivedStockType,


       IObGoodsReceiptItemBatches.batchcode                          AS batchNo,
       IObGoodsReceiptItemBatches.receivedqtyuoe                     as batchQty,
       IObGoodsReceiptItemBatches.unitofentryid                      as batchUoeId,
       IObGoodsReceiptItemBatches.stocktype  as batchStockType

from DObGoodsReceiptPurchaseOrder
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id and dobinventorydocument.inventorydocumenttype = 'GR_PO'
         left join iobinventorycompany on iobinventorycompany.refinstanceid = dobinventorydocument.id
         left join cobplant on iobinventorycompany.plantid = cobplant.id
         left join IObGoodsReceiptPurchaseOrderData
                   on DObGoodsReceiptPurchaseOrder.id = IObGoodsReceiptPurchaseOrderData.refinstanceid
         left join IObGoodsReceiptPurchaseOrderReceivedItemsData
                   on DObGoodsReceiptPurchaseOrder.id = IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join IObGoodsReceiptPurchaseOrderItemQuantities
                   on IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                      IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid

         left join IObGoodsReceiptItemBatches
                   on IObGoodsReceiptPurchaseOrderReceivedItemsData.id = IObGoodsReceiptItemBatches.refinstanceid
         LEFT JOIN mobitem on IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitem.id
         LEFT JOIN iobalternativeuom alternative2 ON mobitem.id = alternative2.refinstanceid and
                                                     alternative2.alternativeunitofmeasureid =
                                                     iobgoodsreceiptitembatches.unitofentryid
         LEFT JOIN iobalternativeuom alternative1 ON mobitem.id = alternative1.refinstanceid and
                                                     alternative1.alternativeunitofmeasureid =
                                                     IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
         LEFT JOIN masterdata on mobitem.id = masterdata.id
         LEFT join cobmeasure on cobmeasure.id = mobitem.basicunitofmeasure;
