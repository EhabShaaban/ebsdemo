DROP VIEW IF EXISTS IObInvoiceRemainingGeneralModel  ;
DROP VIEW IF EXISTS iobinvoicesummarygeneralmodel  ;
DROP VIEW IF EXISTS dobnotesreceivablesgeneralmodel ;
DROP VIEW IF EXISTS dobsalesinvoicegeneralmodel ;
DROP VIEW IF EXISTS dobsalesinvoicedatageneralmodel ;
DROP VIEW IF EXISTS iobsalesinvoiceitemsummarygeneralmodel;
DROP FUNCTION IF EXISTS getSalesInvoiceTotalTaxAmount(salesInvoiceCode varchar);
DROP FUNCTION IF EXISTS getSalesInvoiceTotalAmountBeforTaxes(salesInvoiceCode varchar);
DROP FUNCTION IF EXISTS getDownPaymentFromSalesInvoice(salesinvoicecode varchar);

CREATE or REPLACE FUNCTION getDownPaymentForInvoice(invoiceCode VARCHAR, invoiceType VARCHAR)
    RETURNS FLOAT AS $$
BEGIN
    RETURN
        CASE WHEN invoiceType = 'SalesInvoice' THEN
            (CASE WHEN (select downpayment
                from iobsalesinvoicebusinesspartnergeneralmodel businesspartner
                where businesspartner.code = invoiceCode) is null THEN 0
            ELSE (select downpayment
                from iobsalesinvoicebusinesspartnergeneralmodel businesspartner
                where businesspartner.code = invoiceCode)
            END)
        ELSE
            COALESCE((SELECT
                    (SELECT SUM(downpaymentamount)
                    FROM iobvendorinvoicedetailsdownpayment invoiceDownPayment
                    WHERE invoiceDownPayment.refinstanceid = invoiceDetails.id
                      AND invoiceDetails.refinstanceid = dobvendorInvoice.id)
            FROM dobvendorInvoice dobvendorInvoice
            LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = dobvendorInvoice.id
            LEFT JOIN iobvendorinvoicedetails invoiceDetails ON dobvendorInvoice.id = invoiceDetails.refinstanceid
            WHERE dobaccountingdocument.objecttypecode = 'VendorInvoice' AND dobaccountingdocument.code = invoiceCode
            ),0)
        END;
END; $$
    LANGUAGE PLPGSQL;


CREATE or REPLACE FUNCTION getInvoiceTotalAmountBeforTaxes(invoiceCode VARCHAR, invoiceType VARCHAR)
    RETURNS float AS $$
BEGIN
    RETURN
        CASE WHEN invoiceType = 'SalesInvoice' THEN
                 COALESCE((SELECT
                               (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * (case when invoiceItem.conversionfactortobase is null then 1 else invoiceItem.conversionfactortobase end ))) AS totalAmount
                                FROM iobsalesinvoiceitemgeneralmodel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                           FROM dobsalesinvoice dobInvoice
                                    LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobInvoice.id
                           WHERE dobaccountingdocument.objecttypecode = 'SalesInvoice' AND dobaccountingdocument.code = invoiceCode
                          ),0)
             ELSE
                 COALESCE((SELECT
                               (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * invoiceItem.conversionfactortobase)) AS totalAmount
                                FROM IObVendorInvoiceItemsGeneralModel invoiceItem WHERE invoiceItem.code = dobaccountingdocument.code)
                           FROM dobvendorInvoice dobvendorInvoice
                                    LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
                           WHERE dobaccountingdocument.objecttypecode = 'VendorInvoice' AND dobaccountingdocument.code = invoiceCode
                          ),0)
            END;
END; $$
    LANGUAGE PLPGSQL;



CREATE or REPLACE FUNCTION getInvoiceTotalTaxAmount(iCode VARCHAR, invoiceType VARCHAR)
    RETURNS float AS $$
BEGIN
    RETURN
        CASE WHEN invoiceType = 'SalesInvoice' THEN
                 (select case when sum(taxes.taxAmount) is null then 0 else sum(taxes.taxAmount) end
                  from iobsalesinvoicetaxesgeneralmodel taxes
                  where taxes.invoicecode = iCode)
             ELSE
                 (select case when sum(taxes.taxAmount) is null then 0 else sum(taxes.taxAmount) end
                  from iobvendorinvoicetaxesgeneralmodel taxes
                  where taxes.invoicecode = iCode)
            END;
END; $$
    LANGUAGE PLPGSQL;



CREATE or REPLACE FUNCTION getAmountPaidViaAccountingDocument(iCode VARCHAR, invoiceType VARCHAR)
    RETURNS float AS $$
BEGIN
    RETURN
        CASE WHEN invoiceType = 'SalesInvoice' THEN
            round((getInvoiceTotalAmountBeforTaxes(iCode, invoiceType)
                       + getInvoiceTotalTaxAmount(iCode, invoiceType)
                       - getDownPaymentForInvoice(iCode, invoiceType)
                       - getAmountFromPostedNotesReceivalbles(iCode)
                       - getAmountFromPostedCollectionRequests(iCode)) ::numeric, 3)
        ELSE
            round((getInvoiceTotalAmountBeforTaxes(iCode, invoiceType)
                       + getInvoiceTotalTaxAmount(iCode, invoiceType)
                - getDownPaymentForInvoice(iCode, invoiceType)
                - COALESCE((SELECT SUM(paymentDetails.netamount) AS amountPaid
             FROM iobpaymentrequestinvoicepaymentdetails invoicepaymentDetails
             JOIN iobpaymentrequestpaymentdetails paymentDetails on paymentDetails.id = invoicepaymentDetails.id
             JOIN dobpaymentrequest paymentRequest ON paymentDetails.refinstanceid = paymentRequest.id
             LEFT JOIN dobaccountingdocument on dobaccountingdocument.id =paymentRequest.id AND dobaccountingdocument.currentstates like '%PaymentDone%'
             JOIN dobaccountingdocument invoice ON invoicepaymentDetails.duedocumentid = invoice.id AND invoice.code = dobaccountingdocument.code AND invoice.objecttypecode = 'VendorInvoice'
             WHERE dobaccountingdocument.code = iCode),0)) ::numeric, 3)
        END;
END; $$
    LANGUAGE PLPGSQL;




CREATE or REPLACE FUNCTION getInvoiceNetAmount(iCode VARCHAR, invoiceType VARCHAR)
    RETURNS float AS $$
BEGIN
    RETURN
        getInvoiceTotalAmountBeforTaxes(iCode, invoiceType) + getInvoiceTotalTaxAmount(iCode, invoiceType);
END; $$
    LANGUAGE PLPGSQL;


CREATE OR REPLACE VIEW IObInvoiceSummaryGeneralModel AS
SELECT id,
    code                                                                                                                                          as invoiceCode,
    objectTypeCode                                                                                                                                as invoiceType,
    getInvoiceTotalAmountBeforTaxes(code, objecttypecode)                                                                     as totalAmountBeforeTaxes,
    getInvoiceTotalTaxAmount(code, objecttypecode)                                                                                 as taxAmount,
    getInvoiceNetAmount(code, objecttypecode)  as netAmount,
    getDownPaymentForInvoice(code, objecttypecode)                                                                             as downpayment,
    getAmountPaidViaAccountingDocument(code, objecttypecode)                                                                        as totalRemaining
FROM dobaccountingdocument
WHERE objecttypecode in ('SalesInvoice', 'VendorInvoice');