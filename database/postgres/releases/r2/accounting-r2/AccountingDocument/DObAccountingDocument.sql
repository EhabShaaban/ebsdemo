drop view if exists iobpaymentrequestpostingdetailsgeneralmodel;
drop view if exists IObCollectionRequestPostingDetailsGeneralModel;
drop view if exists IObInvoicePostingDetailsGeneralModel;
drop table if exists iobpaymentrequestpostingdetails;
drop table if exists iobCollectionRequestpostingdetails;
drop table if exists iobInvoicepostingdetails;
-- drop table if exists iobSalesInvoicepostingdetails;
drop view if exists IObAccountingDocumentPostingDetailsGeneralModel;
drop table if exists IObAccountingDocumentPostingDetails;
CREATE TABLE DObAccountingDocument
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    objectTypeCode   VARCHAR(1024),

    PRIMARY KEY (id)
);


create table IObAccountingDocumentPostingDetails
(
    id               BIGSERIAL                   NOT NULL,
    refInstanceId    INT8                        NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE    NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE    NOT NULL,
    creationInfo     VARCHAR(1024)               NOT NULL,
    modificationInfo VARCHAR(1024)               NOT NULL,
    accountant       VARCHAR(1024)               NOT NULL,
    postingDate      TIMESTAMP WITH TIME ZONE    NOT NULL,
    dueDate          TIMESTAMP WITH TIME ZONE    NOT NULL,
    exchangeRateId   INT8,
    currencyPrice    float,
    objectTypeCode   VARCHAR(1024)               NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObAccountingDocumentPostingDetails
    ADD CONSTRAINT FK_accountingDocument_IObAccountingDocumentPostingDetails FOREIGN KEY
        (refInstanceId) REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;

ALTER TABLE IObAccountingDocumentPostingDetails
    ADD CONSTRAINT FK_cobexchangerate_IObAccountingDocumentPostingDetails FOREIGN KEY
        (exchangeRateId) REFERENCES CObExchangeRate (id) ON DELETE CASCADE;


