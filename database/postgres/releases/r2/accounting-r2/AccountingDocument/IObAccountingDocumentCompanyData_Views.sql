DROP VIEW IF EXISTS IObPaymentRequestCompanyDataGeneralModel;
DROP VIEW IF EXISTS IObAccountingDocumentCompanyDataGeneralModel;

CREATE VIEW IObAccountingDocumentCompanyDataGeneralModel AS
SELECT companyData.id,
       companyData.refInstanceId,
       accountingDocument.code                 AS documentCode,
       companyData.creationDate,
       companyData.modifiedDate,
       companyData.creationInfo,
       companyData.modificationInfo,
       companyData.purchaseUnitId,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE companyData.purchaseUnitId = cobenterprise.id) AS purchaseUnitName,
       (SELECT cobenterprise.name:: json ->> 'en'
        FROM cobenterprise
        WHERE companyData.purchaseUnitId = cobenterprise.id) AS purchaseUnitNameEn,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE companyData.purchaseUnitId = cobenterprise.id) AS purchaseUnitCode,
       company.code                                          AS companyCode,
       company.name                                          AS companyName,
       IObCompanyBankDetails.id                              AS bankAccountId,
       companyBank.code                                      AS bankAccountCode,
       companyBank.name                                      AS bankAccountName,
       IObCompanyBankDetails.accountno                       AS bankAccountNumber,
       companyData.objectTypeCode                            AS objectTypeCode
FROM IObAccountingDocumentCompanyData companyData
         JOIN dobaccountingdocument accountingDocument ON companyData.refinstanceid = accountingDocument.id
         LEFT JOIN IObCompanyBankDetails
                   ON IObCompanyBankDetails.id = companyData.bankAccountId
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN cobenterprise company ON company.id = companyData.companyid;