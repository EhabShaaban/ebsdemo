CREATE TABLE IObAccountingDocumentAccountingDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL REFERENCES DObAccountingDocument (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    subLedger        VARCHAR(100)  NOT NULL,
    accountingEntry  VARCHAR(1024) NOT NULL,
    glAccountId      INT8 REFERENCES hobglaccount (id) ON DELETE RESTRICT,
    amount           DOUBLE PRECISION,
    objectTypeCode   VARCHAR(1024),
    PRIMARY KEY (id)
);


CREATE TABLE IObAccountingDocumentOrderAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES Doborder (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);


CREATE TABLE IObAccountingDocumentVendorAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES mobvendor  (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);


CREATE TABLE IObAccountingDocumentBankAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES iobcompanybankdetails  (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);

CREATE TABLE IObAccountingDocumentCustomerAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES mobcustomer  (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);

CREATE TABLE IObAccountingDocumentNotesReceivableAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES dobnotesreceivables  (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);
