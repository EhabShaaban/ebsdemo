CREATE or REPLACE FUNCTION getJournalEntryCodeDependsOnObjectTypeCode(objectTypeCode varchar, accouningDocumentId bigint)
    RETURNS varchar AS
$$
BEGIN
    RETURN case
               when objecttypecode = 'PaymentRequest' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobpaymentrequestjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'CollectionRequest' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobcollectionrequestjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'SalesInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsalesinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'VendorInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsalesinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'LandedCost' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from doblandedcostjournalentry where documentreferenceid = accouningDocumentId))
        end;
END;
$$
    LANGUAGE PLPGSQL;

CREATE Or Replace VIEW IObAccountingDocumentPostingDetailsGeneralModel AS
SELECT postingDetails.id,
       postingDetails.refinstanceid,
       postingDetails.creationDate,
       postingDetails.creationInfo,
       postingDetails.modificationInfo,
       postingDetails.modifieddate,
       accountingDocument.code,
       accountingDocument.objecttypecode                                 as documentType,
       postingDetails.accountant,
       postingDetails.postingdate,
       postingDetails.duedate,
       exchangeRate.code                                                 AS exchangeRateCode,
       exchangeRate.id                                                   AS exchangeRateId,
       exchangeRate.firstvalue,
       exchangeRate.secondvalue,
       exchangeRate.firstcurrencyiso,
       exchangeRate.secondcurrencyiso,
       postingDetails.currencyprice,
       getJournalEntryCodeDependsOnObjectTypeCode(accountingDocument.objecttypecode,
                                                  accountingDocument.id) as journalEntryCode
FROM IObAccountingDocumentPostingDetails postingDetails
         left JOIN dobaccountingdocument accountingDocument
                   on accountingDocument.id = postingDetails.refinstanceid
         left join cobexchangerategeneralmodel exchangeRate
                   on postingDetails.exchangerateid = exchangeRate.id;

