DROP TABLE IF EXISTS IObAccountingDocumentCompanyData;

CREATE TABLE IObAccountingDocumentCompanyData
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObAccountingDocument (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,

    purchaseUnitId   INT8                     NOT NULL REFERENCES cobenterprise (id) ON DELETE RESTRICT,
    companyId        INT8 REFERENCES                   CObCompany (id) ON DELETE RESTRICT,
    bankAccountId    INT8 REFERENCES                   iobcompanybankdetails (id) ON DELETE RESTRICT,
    objectTypeCode   VARCHAR(1024),
    PRIMARY KEY (id)
);

-- Data Migration -------------------------------------------------------------------
INSERT INTO IObAccountingDocumentCompanyData(refInstanceId,
                                            creationDate,
                                            modifiedDate,
                                            creationInfo,
                                            modificationInfo,
                                            purchaseUnitId,
                                            companyId,
                                            bankAccountId,
                                            objectTypeCode)
    SELECT paymentRequestCompanyData.refinstanceid,
           paymentRequestCompanyData.creationDate,
           paymentRequestCompanyData.modifiedDate,
           paymentRequestCompanyData.creationInfo,
           paymentRequestCompanyData.modificationInfo,
           paymentRequestCompanyData.purchaseUnitId,
           paymentRequestCompanyData.companyId,
           paymentRequestCompanyData.bankAccountId,
           'PaymentRequest'
    FROM IObPaymentRequestCompanyData paymentRequestCompanyData;

-- Delete Old Company Data Table -------------------------------------------------------------------
DROP TABLE IF EXISTS IObPaymentRequestCompanyData CASCADE;