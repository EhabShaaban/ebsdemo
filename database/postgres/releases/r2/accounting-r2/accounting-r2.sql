\ir 'Taxes/LObTaxInfo.sql'
\ir 'Taxes/LObTaxInfo_views.sql'
\ir 'AccountingDocument/DObAccountingDocument.sql'
\ir 'AccountingDocument/DobAccountingDocument_views.sql'
\ir 'AccountingDocument/IObAccountingDocumentAccountingDetails.sql'
--
\ir 'Summary/IObInvoiceSummaryGeneralModel.sql'

\ir 'paymentRequest/DObPaymentRequest.sql'
\ir 'AccountingDocument/IObAccountingDocumentCompanyData.sql'
\ir 'AccountingDocument/IObAccountingDocumentCompanyData_Views.sql'
\ir 'paymentRequest/DObPaymentRequest_views.sql'

\ir 'collectionRequest/DObCollection.sql'
\ir 'collectionRequest/DObCollection_views.sql'

\ir 'VendorInvoice/DObVendorInvoice.sql'
\ir 'VendorInvoice/DObVendorInvoice_views.sql'

\ir 'SalesInvoice/DObSalesInvoice.sql'
\ir 'SalesInvoice/DObSalesInvoice_views.sql'

\ir 'JournalEntry/journalEntry_view.sql'
\ir 'NotesReceivable/notesReceivable_view.sql'

\ir 'LandedCost/DObLandedCost.sql'
\ir 'LandedCost/DObLandedCost_Views.sql'