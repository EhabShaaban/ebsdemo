DROP VIEW IF EXISTS IObPaymentRequestApprovalCycleGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestApproverGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestAccountDetailsGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestPaymentDetailsGeneralModel;
DROP VIEW IF EXISTS DObPaymentRequestGeneralModel;

CREATE VIEW IObPaymentRequestApprovalCycleGeneralModel AS
SELECT iobpaymentrequestapprovalcycle.id,
       dobaccountingdocument.code as paymentRequestCode,
       iobpaymentrequestapprovalcycle.refinstanceid,
       iobpaymentrequestapprovalcycle.creationDate,
       iobpaymentrequestapprovalcycle.creationInfo,
       iobpaymentrequestapprovalcycle.modificationInfo,
       iobpaymentrequestapprovalcycle.modifieddate,
       iobpaymentrequestapprovalcycle.approvalCycleNum,
       iobpaymentrequestapprovalcycle.startDate,
       iobpaymentrequestapprovalcycle.endDate,
       iobpaymentrequestapprovalcycle.finalDecision
FROM iobpaymentrequestapprovalcycle
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = iobpaymentrequestapprovalcycle.refInstanceId
                       and dobaccountingdocument.objecttypecode = 'PaymentRequest';


CREATE VIEW IObPaymentRequestApproverGeneralModel AS
SELECT iobpaymentrequestapprover.id,
       iobpaymentrequestapprover.refinstanceid,
       iobpaymentrequestapprover.creationDate,
       iobpaymentrequestapprover.creationInfo,
       iobpaymentrequestapprover.modificationInfo,
       iobpaymentrequestapprover.modifieddate,
       dobaccountingdocument.code        as paymentRequestCode,
       iobpaymentrequestapprovalcycle.id as approvalCycleId,
       iobpaymentrequestapprovalcycle.approvalCycleNum,
       cobcontrolpoint.name              as controlPointName,
       cobcontrolpoint.code              as controlPointCode,
       iobcontrolpointusers.ismain       as isMainApprover,
       UserInfo.name                     as approverName,
       iobpaymentrequestapprover.decisionDatetime,
       iobpaymentrequestapprover.decision,
       iobpaymentrequestapprover.notes
FROM iobpaymentrequestapprovalcycle
         LEFT JOIN dobaccountingdocument
                   ON dobaccountingdocument.id = iobpaymentrequestapprovalcycle.refInstanceId
                       and dobaccountingdocument.objecttypecode = 'PaymentRequest'
         LEFT JOIN iobpaymentrequestapprover
                   ON iobpaymentrequestapprovalcycle.id = iobpaymentrequestapprover.refInstanceId
         LEFT JOIN cobcontrolpoint ON iobpaymentrequestapprover.controlPointId = cobcontrolpoint.id
         LEFT JOIN iobcontrolpointusers
                   ON iobcontrolpointusers.refinstanceid = cobcontrolpoint.id AND
                      iobcontrolpointusers.userid = iobpaymentrequestapprover.approverId
         LEFT JOIN UserInfo ON UserInfo.id = iobpaymentrequestapprover.approverId
ORDER BY iobpaymentrequestapprover.id ASC;

CREATE VIEW IObPaymentRequestAccountDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
           vendor.code
           )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
             doborder.code
           )
           when accountDetails.accountingEntry = 'CREDIT'
               then (bankSubAccount.accountno)
           end                    as glSubAccountCode,
           case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
           vendoraccountingdetails.glSubAccountId
           )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
             orderaccountingdetails.glSubAccountId
           )
           when accountDetails.accountingEntry = 'CREDIT'
               then (bankaccountingdetails.glSubAccountId)
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
           vendor.name
           )
           when accountDetails.accountingEntry = 'CREDIT' THEN
               (cobbank.name) end as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObPaymentRequest pr on accountDetails.refinstanceid = pr.id
         left join dobaccountingdocument on dobaccountingdocument.id = pr.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
    left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails on vendoraccountingdetails.id = accountDetails.id AND
                                                                                      accountDetails.accountingEntry = 'DEBIT'
    left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails on orderaccountingdetails.id = accountDetails.id AND
                                                                                     accountDetails.accountingEntry = 'DEBIT'
    left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails on bankaccountingdetails.id = accountDetails.id AND
                                                                                  accountDetails.accountingEntry = 'CREDIT'
left join masterdata vendor on vendor.id = vendoraccountingdetails.glSubAccountId and vendor.objecttypecode = '2'
left join doborder on doborder.id = orderaccountingdetails.glSubAccountId
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         left join cobbank on cobbank.id = bankSubAccount.bankid;

--

CREATE VIEW IObPaymentRequestPaymentDetailsGeneralModel as
select pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                    as businessPartnerCode,
       md.name                    as businessPartnerName,
       pd.duedocumenttype,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           else doborder.code end as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso            as currencyISO,
       cobcurrency.name           as currencyName,
       cobcurrency.code           as currencyCode,
       companyCurrency.iso        AS companyCurrencyIso,
       companyCurrency.code       AS companyCurrencyCode,
       companyCurrency.id         AS companyCurrencyId,
       pd.description
from iobpaymentrequestpaymentdetails pd
         left join IObPaymentRequestInvoicePaymentDetails ipd on ipd.id = pd.id
         left join IObPaymentRequestPurchaseOrderPaymentDetails dpd on dpd.id = pd.id
         left join dobinvoice on dobinvoice.id = ipd.duedocumentid
         left join dobaccountingdocument vendorInvoice on vendorInvoice.id = dobinvoice.id
         left join doborder on doborder.id = dpd.duedocumentid
         left join masterdata md on md.id = pd.businessPartnerId
         left join dobpaymentrequest pr on pr.id = pd.refinstanceid
         left join dobaccountingdocument on dobaccountingdocument.id = pr.id
         left join IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
         left join cobcurrency on pd.currencyid = cobcurrency.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = companyData.companyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid;

--
--
CREATE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       DObPaymentRequest.paymentForm,
       pd.netAmount                                          AS amount,
       companyData.purchaseUnitId,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE companyData.purchaseUnitId = cobenterprise.id) AS purchaseUnitName,
       (SELECT cobenterprise.name:: json ->> 'en'
        FROM cobenterprise
        WHERE companyData.purchaseUnitId = cobenterprise.id) AS purchaseUnitNameEn,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE companyData.purchaseUnitId = cobenterprise.id) AS purchaseUnitCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE cobcurrency.id = pd.currencyId)                AS currencyISO,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE cobcurrency.id = pd.currencyId)                AS currencyCode,
       company.code                                          AS companyCode,
       company.name                                          AS companyName,
       IObCompanyBankDetails.id                              AS bankAccountId,
       companyBank.code                                      AS bankAccountCode,
       companyBank.name                                      AS bankAccountName,
       IObCompanyBankDetails.accountno                       AS bankAccountNumber
FROM DObPaymentRequest
         FULL JOIN IObPaymentRequestPaymentDetails pd ON DObPaymentRequest.id = pd.refInstanceId
         left join dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN IObCompanyBankDetails
                   ON IObCompanyBankDetails.id = companyData.bankAccountId
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN cobenterprise company ON company.id = companyData.companyid;
--