drop view if exists IObPaymentRequestPaymentDetailsGeneralModel;
drop view if exists IObPaymentRequestAccountDetailsGeneralModel;
DROP VIEW IF EXISTS DObPaymentRequestGeneralModel;
DROP TABLE IF EXISTS IObPaymentRequestCompanyData;
DROP TABLE IF EXISTS IObPaymentRequestDownPaymentPaymentDetails;
DROP TABLE IF EXISTS IObPaymentRequestPurchaseOrderPaymentDetails;
DROP TABLE IF EXISTS IObPaymentRequestOrderAccountingDetails;
DROP TABLE IF EXISTS IObPaymentRequestVendorOrderAccountingDetails;


ALTER TABLE IObPaymentRequestInvoicePaymentDetails
    DROP CONSTRAINT FK_iobpaymentrequestInvoicepaymentdetails_iobpaymentrequestpaymentdetails;

ALTER TABLE IObPaymentRequestInvoicePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestInvoicepaymentdetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE CASCADE;


ALTER TABLE IF EXISTS IObPaymentRequestDownPaymentPaymentDetails
    DROP CONSTRAINT FK_iobpaymentrequestDownPaymentpaymentdetails_iobpaymentrequestpaymentdetails;

ALTER TABLE IObInvoiceDetailsDownPayment
    DROP CONSTRAINT FK_IObInvoiceDetailsDownPayment_downpayment;

ALTER TABLE IObInvoiceDetailsDownPayment
    ADD CONSTRAINT FK_IObInvoiceDetailsDownPayment_downpayment FOREIGN KEY (DownPaymentid) REFERENCES DObPaymentRequest (id) ON DELETE CASCADE;


create table IObPaymentRequestPurchaseOrderPaymentDetails
(
    id            BIGSERIAL NOT NULL,
    dueDocumentId INT8,
    PRIMARY KEY (id)
);

ALTER TABLE IObPaymentRequestPurchaseOrderPaymentDetails
    ADD CONSTRAINT FK_IObPaymentRequestPurchaseOrderPaymentDetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE CASCADE;

ALTER TABLE IObPaymentRequestPurchaseOrderPaymentDetails
    ADD CONSTRAINT FK_IObPaymentRequestPurchaseOrderPaymentDetails_duedocument FOREIGN KEY (dueDocumentId) REFERENCES doborder (id) ON DELETE restrict;


CREATE TABLE IObPaymentRequestCompanyData
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObPaymentRequest (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,

    purchaseUnitId   INT8                     NOT NULL REFERENCES cobenterprise (id) ON DELETE RESTRICT,
    companyId        INT8 REFERENCES CObCompany (id) ON DELETE RESTRICT,
    bankAccountId    INT8 REFERENCES iobcompanybankdetails (id) ON DELETE RESTRICT,

    PRIMARY KEY (id)
);

-- Data Migration -------------------------------------------------------------------
INSERT INTO IObPaymentRequestCompanyData(refInstanceId, creationDate, modifiedDate,
                                         creationInfo, modificationInfo, purchaseUnitId)
SELECT paymentRequest.id,
       paymentRequest.creationDate,
       paymentRequest.modifiedDate,
       paymentRequest.creationInfo,
       paymentRequest.modificationInfo,
       paymentRequest.purchaseUnitId
FROM DObPaymentRequest paymentRequest;

UPDATE IObPaymentRequestCompanyData
SET companyId     = IObPaymentRequestAccountDetails.companyId,
    bankAccountId = IObPaymentRequestAccountDetails.bankAccountId
FROM IObPaymentRequestAccountDetails
WHERE IObPaymentRequestAccountDetails.refInstanceId = IObPaymentRequestCompanyData.refInstanceId;

-- -----------------------------------------------------------------------------------
ALTER TABLE DObPaymentRequest
    DROP CONSTRAINT FK_doppaymentrequest_purchaseunit,
    DROP COLUMN purchaseUnitId,
    DROP COLUMN code CASCADE,
    DROP COLUMN creationDate CASCADE,
    DROP COLUMN modifiedDate CASCADE,
    DROP COLUMN creationInfo CASCADE,
    DROP COLUMN modificationInfo CASCADE,
    DROP COLUMN currentStates CASCADE;


ALTER TABLE dobpaymentrequest
    ADD CONSTRAINT FK_DObAccountingDocument_DobPaymentRequest FOREIGN KEY (id)
        REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;


-- -----------------------payment Request refactor ---------------------------------------


