ALTER TABLE dobinvoice
DROP COLUMN code CASCADE,
DROP COLUMN creationDate CASCADE,
DROP COLUMN modifiedDate CASCADE,
DROP COLUMN creationInfo CASCADE,
DROP COLUMN modificationInfo CASCADE,
DROP COLUMN currentStates CASCADE;

ALTER TABLE IObInvoiceDetails DROP COLUMN tax;

ALTER TABLE dobinvoice
    ADD CONSTRAINT FK_DObAccountingDocument_DOVendorInvoice FOREIGN KEY (id)
        REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;

DROP VIEW IF EXISTS IObVendorInvoiceTaxesGeneralModel;
DROP VIEW IF EXISTS IObInvoiceItemsGeneralModel;
DROP TABLE IF EXISTS IObVendorInvoiceTaxes;
DROP VIEW IF EXISTS DObInvoiceGeneralModel;
DROP VIEW IF EXISTS iobinvoicedetailsgeneralmodel;
DROP VIEW IF EXISTS IObVendorInvoiceDetailsGeneralModel;
DROP VIEW IF EXISTS IObVendorInvoiceRemainingGeneralModel;
DROP VIEW IF EXISTS IObInvoiceRemainingGeneralModel;

CREATE TABLE IObVendorInvoiceTaxes (
  id               BIGSERIAL     NOT NULL,
  refInstanceId    BIGSERIAL     NOT NULL REFERENCES dobinvoice (id) ON DELETE CASCADE,
  creationDate     TIMESTAMP     NOT NULL,
  modifiedDate     TIMESTAMP     NOT NULL,
  creationInfo     VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  code     VARCHAR(30) NOT NULL,
  name     json NOT NULL,
  percentage       DOUBLE PRECISION,
  amount           DOUBLE PRECISION,

  PRIMARY KEY (id)
);

ALTER TABLE dobinvoice
  RENAME TO DObVendorInvoice;

ALTER TABLE iobinvoicedetails
  RENAME TO iobvendorinvoicedetails;

ALTER TABLE iobinvoicedetailsdownpayment
  RENAME TO iobvendorinvoicedetailsdownpayment;

ALTER TABLE iobinvoiceitem
  RENAME TO iobvendorinvoiceitem;

ALTER TABLE iobinvoicepurchaseorder
  DROP COLUMN companyId CASCADE,
  DROP COLUMN purchaseunitid CASCADE;

ALTER TABLE iobinvoicepurchaseorder
  RENAME TO iobvendorinvoicepurchaseorder;

ALTER TABLE cobinvoice
  RENAME TO cobvendorinvoice;

ALTER SEQUENCE dobinvoice_id_seq RENAME TO dobvendorinvoice_id_seq;

ALTER SEQUENCE iobinvoicedetails_id_seq RENAME TO iobvendorinvoicedetails_id_seq;

ALTER SEQUENCE iobinvoicedetailsdownpayment_id_seq RENAME TO iobvendorinvoicedetailsdownpayment_id_seq;

ALTER SEQUENCE iobinvoiceitem_id_seq RENAME TO iobvendorinvoiceitem_id_seq;

ALTER SEQUENCE iobinvoicepurchaseorder_id_seq RENAME TO iobvendorinvoicepurchaseorder_id_seq;

ALTER SEQUENCE cobinvoice_id_seq RENAME TO cobvendorinvoice_id_seq;

