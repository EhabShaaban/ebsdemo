
CREATE or REPLACE FUNCTION getConversionFactorToBase(baseunitofmeasureid bigint, orderunitofmeasureid bigint, itemid bigint)
RETURNS float AS $$
BEGIN
RETURN (SELECT CASE
    WHEN (baseunitofmeasureid = orderunitofmeasureid)
    THEN 1
     WHEN (SELECT iobalternativeuom.conversionfactor
            FROM iobalternativeuom
            WHERE itemid = iobalternativeuom.refinstanceid
              AND orderunitofmeasureid = iobalternativeuom.alternativeunitofmeasureid
                   ) is null THEN 1
    ELSE (SELECT iobalternativeuom.conversionfactor
            FROM iobalternativeuom
            WHERE itemid = iobalternativeuom.refinstanceid
              AND orderunitofmeasureid = iobalternativeuom.alternativeunitofmeasureid
        ) END AS conversionFactorToBase
    );
END; $$
LANGUAGE PLPGSQL;


CREATE or REPLACE VIEW DObVendorInvoiceGeneralModel AS
SELECT dobvendorInvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                                                     AS vendorCode,
       masterdata.name                                                     AS vendorName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en') AS vendorCodeName,
       cobvendorinvoice.code                                                     AS invoiceTypeCode,
       cobvendorinvoice.name                                                     AS invoiceTypeName,
       cobvendorinvoice.name :: json ->> 'en'                                    AS invoiceTypeNameEn,
       purchaseUnit.code                                                   AS purchaseUnitCode,
       purchaseUnit.name                                                   AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                  AS purchaseUnitNameEn,
       company.code                                                        AS companyCode,
       company.name                                                        AS companyName,
       cobcurrency.code                                                    AS currencyCode,
       cobcurrency.name                                                    AS currencyName,
       cobcurrency.iso                                                     AS currencyISO,
       cobpaymentterms.code                                                AS paymentTermCode,
       doborder.code                                                       AS purchaseOrderCode,
       iobvendorinvoicedetails.invoiceNumber                                     AS invoiceNumber
FROM dobvendorInvoice
    left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN masterdata ON dobvendorInvoice.vendorId = masterdata.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
           ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit
           ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
           AND purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobenterprise company ON company.id = iobvendorinvoicecompanydata.companyId
	       AND company.objecttypecode = '1'
         LEFT JOIN IObVendorInvoicePurchaseOrder ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobvendorinvoice ON cobvendorinvoice.id = dobvendorInvoice.typeId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborder ON doborder.id = IObVendorInvoicePurchaseOrder.purchaseOrderId
         LEFT JOIN iobvendorinvoicedetails ON iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id;

CREATE or REPLACE VIEW IObVendorInvoiceItemsGeneralModel AS
SELECT IObVendorInvoiceItem.id,
       vendorInvoice.code,
       item.code as itemCode,
       item.name as itemName,
       baseUnitOfMeasure.code                         					as baseUnitCode,
       baseUnitOfMeasure.symbol                       					as baseUnitSymbol,
       orderUnitOfMeasure.code                                          as orderUnitCode,
       orderUnitOfMeasure.symbol                                        as orderunitsymbol,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid, IObVendorInvoiceItem.itemid) AS conversionFactorToBase,
       ((IObVendorInvoiceItem.quantityinorderunit* getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid, IObVendorInvoiceItem.itemid)) * IObVendorInvoiceItem.price  )           as totalAmount,
       IObVendorInvoiceItem.creationDate,
       IObVendorInvoiceItem.modifiedDate,
       IObVendorInvoiceItem.creationInfo,
       IObVendorInvoiceItem.modificationInfo,
       IObVendorInvoiceItem.quantityinorderunit,
       IObVendorInvoiceItem.refinstanceid,
       IObVendorInvoiceItem.price,
       IObVendorInvoiceItem.price * getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid, IObVendorInvoiceItem.itemid)  as orderUnitPrice

FROM IObVendorInvoiceItem
          left join masterdata item on IObVendorInvoiceItem.itemId = item.id
          left join cobmeasure orderUnitOfMeasure on IObVendorInvoiceItem.orderunitofmeasureid = orderUnitOfMeasure.id
         left join mobitem on IObVendorInvoiceItem.itemId = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
          left join dobaccountingdocument vendorInvoice on IObVendorInvoiceItem.refInstanceId = vendorInvoice.id and vendorInvoice.objecttypecode = 'VendorInvoice';



CREATE OR REPLACE VIEW IObVendorInvoiceDetailsGeneralModel AS
SELECT dobaccountingdocument.code as invoiceCode,
       dobvendorInvoice.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code AS vendorCode,
       masterdata.name :: json ->> 'en' AS vendorName,
       iobvendorinvoicedetails.refinstanceid,
       iobvendorinvoicedetails.invoiceNumber,
       iobvendorinvoicedetails.invoiceDate,
       cobcurrency.code AS currencyCode,
       cobcurrency.iso AS currencyISO,
       cobcurrency.name AS currencyName,
       cobpaymentterms.code AS paymentTermCode,
       cobpaymentterms.name :: json ->> 'en' AS paymentTerm,
       doborder.code AS purchaseOrderCode,
       purchaseUnit.code                  AS purchaseUnitCode,
       purchaseUnit.name                  AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       (select string_agg(CONCAT(dobaccountingdocument.code,'-',IObPaymentRequestPaymentDetails.netamount), ', ')
        from dobaccountingdocument, iobpaymentrequestpaymentdetails
        where dobaccountingdocument.id in (select iobvendorinvoicedetailsdownpayment.DownPaymentid
                                           from iobvendorinvoicedetailsdownpayment
                                           where iobvendorinvoicedetailsdownpayment.refInstanceId = iobvendorinvoicedetails.id)
          and dobaccountingdocument.objecttypecode = 'PaymentRequest'
          and iobpaymentrequestpaymentdetails.refinstanceid=dobaccountingdocument.id
       ) as DownPayment

FROM dobvendorInvoice
    left join dobaccountingdocument on dobaccountingdocument.id =dobvendorInvoice.id
         LEFT JOIN masterdata ON dobvendorInvoice.vendorId = masterdata.id
         LEFT JOIN IObVendorInvoicePurchaseOrder ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborder ON doborder.id = IObVendorInvoicePurchaseOrder.purchaseOrderId
         LEFT JOIN iobvendorinvoicedetails on iobvendorinvoicedetails.refInstanceId=dobvendorInvoice.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
           ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit  ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
	       AND purchaseUnit.objecttypecode = '5';


CREATE or REPLACE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE WHEN invoiceSummary.totalremaining IS NULL THEN 0 ELSE invoiceSummary.totalremaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
                   ON dobInvoice.id = invoiceSummary.id;


CREATE or REPLACE VIEW IObVendorInvoiceTaxesGeneralModel AS
SELECT IObVendorInvoiceTaxes.id,
       IObVendorInvoiceTaxes.creationDate,
       IObVendorInvoiceTaxes.modifiedDate,
       IObVendorInvoiceTaxes.creationInfo,
       IObVendorInvoiceTaxes.modificationInfo,
       IObVendorInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code                           AS invoicecode,
       IObVendorInvoiceTaxes.code                                AS taxcode,
       IObVendorInvoiceTaxes.name                                AS taxname,
       LObTaxInfo.percentage as taxpercentage,
       IObVendorInvoiceTaxes.amount as taxAmount
FROM
    IObVendorInvoiceTaxes
        left join dobaccountingdocument
                  on dobaccountingdocument.id = IObVendorInvoiceTaxes.refInstanceId
                      and dobaccountingdocument.objecttypecode = 'VendorInvoice'

        left join dobvendorInvoice on dobvendorInvoice.id = IObVendorInvoiceTaxes.refinstanceid
        left join LObTaxInfo on LObTaxInfo.code = IObVendorInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis


