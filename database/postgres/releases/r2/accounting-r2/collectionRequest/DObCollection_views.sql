DROP VIEW IF Exists IObCollectionRequestDetailsGeneralModel;
DROP VIEW IF Exists IObCollectionRequestCompanyDetailsGeneralModel;
DROP VIEW IF Exists IObCollectionRequestAccountingDetailsGeneralModel;
DROP VIEW IF EXISTS DObCollectionRequestGeneralModel;

DROP FUNCTION if exists getRefDocumentCodeBasedOnType(bigint, character varying);
CREATE FUNCTION getRefDocumentCodeBasedOnType(collectionRequestDetailsId BIGINT,
                                              refDocumentType VARCHAR)
    RETURNS VARCHAR AS
$$
BEGIN
    RETURN CASE
               WHEN refDocumentType = 'SALES_INVOICE' THEN
                   (select code
                    from dobaccountingdocument
                    where dobaccountingdocument.id =
                          (select salesinvoiceid
                           from iobcollectionrequestsalesinvoicedetails
                           where iobcollectionrequestsalesinvoicedetails.id =
                                 collectionRequestDetailsId)
                      AND dobaccountingdocument.objecttypecode = 'SalesInvoice')
               else
                   (select code
                    from dobnotesreceivables
                    where dobnotesreceivables.id =
                          (select notesReceivableId
                           from IObCollectionRequestNotesReceivableDetails
                           where IObCollectionRequestNotesReceivableDetails.id =
                                 collectionRequestDetailsId))
        end;
END;
$$
    LANGUAGE PLPGSQL;
----------------------------------
DROP FUNCTION if exists getcollectionrequestsubaccountcode(bigint, character varying);
----------------------------------

CREATE VIEW IObCollectionRequestDetailsGeneralModel as
select cd.id,
       cd.refInstanceId,
       crAccountingDocument.creationDate,
       crAccountingDocument.modifiedDate,
       crAccountingDocument.creationInfo,
       crAccountingDocument.modificationInfo,
       crAccountingDocument.code,
       crAccountingDocument.currentStates,
       refDocumentType.code                    as refdocumenttype,
       case
           when refDocumentType.code = 'SALES_INVOICE' then dobaccountingdocument.code
           when refDocumentType.code = 'NOTES_RECEIVABLE' then DObNotesReceivables.code
           else dobaccountingdocument.code end as refDocumentCode,
       cd.amount,
       cobcurrency.iso                         as currencyISO,
       cobcurrency.name                        as currencyName,
       cobcurrency.code                        as currencyCode,
       customer.code                           as customerCode,
       customer.name                           as customername
from iobcollectionrequestdetails cd
         left join IObCollectionRequestSalesInvoiceDetails icd on icd.id = cd.id
         left join IObCollectionRequestNotesReceivableDetails inr on inr.id = cd.id
         left join CObCollectionRequestReferenceDocumentType refDocumentType
                   on cd.refDocumentTypeId = refDocumentType.id
         left join MasterData customer on customer.id = cd.customerid
         left join dobaccountingdocument on dobaccountingdocument.id = icd.salesInvoiceId and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join DObNotesReceivables on DObNotesReceivables.id = inr.notesReceivableId
         left join dobcollectionrequest cr on cr.id = cd.refinstanceid
         left join dobaccountingdocument crAccountingDocument on cr.id = crAccountingDocument.id
         left join cobcurrency on cd.currencyid = cobcurrency.id;


CREATE VIEW DObCollectionRequestGeneralModel AS
SELECT cr.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: json ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode,
       cr.collectiontype,
       cr.collectionform,
       company.code                                        AS companycode,
       company.name                                        AS companyname,
       cobcurrency.code                                    AS companylocalcurrencycode,
       cobcurrency.name                                    AS companylocalcurrencyname,
       cobcurrency.iso                                     AS companylocalcurrencyiso,
       masterdata.name                                     AS customername,
       masterdata.code                                     AS customercode,
       iobcollectionrequestdetails.amount,
       refDocumentType.code                                AS refdocumenttypecode,
       refDocumentType.name                                AS refdocumenttypename,
       getRefDocumentCodeBasedOnType(iobcollectionrequestdetails.id,
                                     refDocumentType.code) AS refDocumentCode
FROM dobcollectionrequest cr
         left join iobcollectionrequestdetails on cr.id = iobcollectionrequestdetails.refinstanceid
         left join dobaccountingdocument on dobaccountingdocument.id = cr.id
         left join CObCollectionRequestReferenceDocumentType refDocumentType
                   on iobcollectionrequestdetails.refDocumentTypeId = refDocumentType.id
         left join masterdata on iobcollectionrequestdetails.customerid = masterdata.id
         left join IObCollectionRequestCompanyDetails
                   on IObCollectionRequestCompanyDetails.refInstanceId = cr.id
         left join cobenterprise businessUnit
                   on IObCollectionRequestCompanyDetails.purchaseUnitId = businessUnit.id
         left join cobenterprise company
                   on IObCollectionRequestCompanyDetails.companyid = company.id
         left join cobcompany on IObCollectionRequestCompanyDetails.companyid = cobcompany.id
         left join iobenterprisebasicdata on cobcompany.id = iobenterprisebasicdata.refinstanceid
         left join cobcurrency on iobenterprisebasicdata.currencyid = cobcurrency.id;



CREATE VIEW IObCollectionRequestCompanyDetailsGeneralModel AS
SELECT companyDetails.id,
       companyDetails.refInstanceId,
       companyDetails.creationDate,
       companyDetails.modifiedDate,
       companyDetails.creationInfo,
       companyDetails.modificationInfo,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       businessUnit.code                  AS purchaseUnitcode,
       company.code                       AS companycode,
       company.name                       AS companyname,
       CObBank.code                       AS bankcode,
       CObBank.name                       AS bankname,
       dobaccountingdocument.code         as collecttionRequestCode,
       bankdetails.accountno              as accountNumber
FROM IObCollectionRequestCompanyDetails companyDetails
         JOIN cobenterprise company ON companyDetails.companyid = company.id
         left join cobenterprise businessUnit on companyDetails.purchaseUnitId = businessUnit.id
         left JOIN iobcompanybankdetails bankdetails
                   ON companyDetails.bankDetailsId = bankdetails.id
         left JOIN CObBank ON bankdetails.bankid = CObBank.id
         JOIN DObCollectionRequest ON DObCollectionRequest.id = companyDetails.refInstanceId
         left join dobaccountingdocument on dobaccountingdocument.id = DObCollectionRequest.id;

-- TODO: Refactor to be one General Model for Accounting Document
CREATE VIEW IObCollectionRequestAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'CREDIT' then (
               customer.code
               )
           when accountDetails.subledger = 'Notes_Receivables'
                    AND accountDetails.accountingEntry = 'CREDIT' then (
               dobnotesreceivables.code
               )
           when accountDetails.accountingEntry = 'DEBIT'
               then (bankSubAccount.accountno)
           end                    as glSubAccountCode,
                  case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'CREDIT' then (
               customeraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'Notes_Receivables'
                    AND accountDetails.accountingEntry = 'CREDIT' then (
               notesreceivableaccountingdetails.glSubAccountId
               )
           when accountDetails.accountingEntry = 'DEBIT'
               then (bankaccountingdetails.glSubAccountId)
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Customer' AND
                accountDetails.accountingEntry = 'CREDIT' then (
               customer.name
               )
           when accountDetails.accountingEntry = 'DEBIT' THEN
               (cobbank.name) end as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join dobcollectionrequest cr on accountDetails.refinstanceid = cr.id
         left join dobaccountingdocument on dobaccountingdocument.id = cr.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join iobaccountingdocumentcustomeraccountingDetails customeraccountingdetails on customeraccountingdetails.id = accountDetails.id AND
                                                                                           accountDetails.accountingEntry = 'CREDIT'
         left join iobaccountingdocumentnotesreceivableaccountingdetails notesreceivableaccountingdetails on notesreceivableaccountingdetails.id = accountDetails.id AND
                                                                                         accountDetails.accountingEntry = 'CREDIT'
         left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails on bankaccountingdetails.id = accountDetails.id AND
                                                                                       accountDetails.accountingEntry = 'DEBIT'
         left join masterdata customer on customer.id = customeraccountingdetails.glSubAccountId and customer.objecttypecode = '3'
         left join dobnotesreceivables on dobnotesreceivables.id = notesreceivableaccountingdetails.glSubAccountId
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         left join cobbank on cobbank.id = bankSubAccount.bankid;

