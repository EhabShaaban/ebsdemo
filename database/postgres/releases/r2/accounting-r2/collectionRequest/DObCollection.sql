DROP VIEW IF Exists IObCollectionRequestDetailsGeneralModel;
DROP VIEW IF Exists IObCollectionRequestPostingDetailsGeneralModel;
DROP VIEW IF Exists IObCollectionRequestCompanyDetailsGeneralModel;
DROP VIEW IF Exists IObCollectionRequestAccountingDetailsGeneralModel;
DROP VIEW IF EXISTS DObCollectionRequestGeneralModel;

---------------------------------------------------------------------
ALTER TABLE IObCollectionRequestCompanyDetails
    ADD COLUMN purchaseUnitId INT8 NOT NULL DEFAULT 0;
---------------------------------------------------------------------
UPDATE IObCollectionRequestCompanyDetails
SET purchaseUnitId = DObCollectionRequest.businessunitid
FROM DObCollectionRequest
WHERE IObCollectionRequestCompanyDetails.refinstanceid = DObCollectionRequest.id;
---------------------------------------------------------------------
ALTER TABLE IObCollectionRequestCompanyDetails
    ADD CONSTRAINT FK_IObCollectionRequestCompanyDetails_businessUnitId FOREIGN KEY (purchaseUnitId) REFERENCES cobenterprise (id) ON DELETE RESTRICT;
---------------------------------------------------------------------
ALTER TABLE DObCollectionRequest
    DROP CONSTRAINT FK_DObCollectionRequest_businessunit;
---------------------------------------------------------------------
ALTER TABLE dobcollectionrequest
    DROP COLUMN code CASCADE,
    DROP COLUMN creationDate CASCADE,
    DROP COLUMN modifiedDate CASCADE,
    DROP COLUMN creationInfo CASCADE,
    DROP COLUMN modificationInfo CASCADE,
    DROP COLUMN currentStates CASCADE,
    DROP COLUMN businessUnitId;

ALTER TABLE dobcollectionrequest
    ADD CONSTRAINT FK_DObAccountingDocument_DObCollectionRequest FOREIGN KEY (id)
        REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;

-- ---------------------------------------------------------------------

DROP TABLE IObCollectionRequestAccountingDetails;