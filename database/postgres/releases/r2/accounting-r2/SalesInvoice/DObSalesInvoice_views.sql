CREATE or REPLACE FUNCTION getSalesInvoiceTotalTaxAmount(salesInvoiceCode varchar)
    RETURNS float AS
$$
BEGIN
    RETURN (select case when sum(taxes.taxAmount) is null then 0 else sum(taxes.taxAmount) end
            from iobsalesinvoicetaxesgeneralmodel taxes
            where taxes.invoicecode = salesInvoiceCode);
END;
$$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getSalesInvoiceTotalAmountBeforTaxes(salesInvoiceCode varchar)
    RETURNS float AS
$$
BEGIN
    RETURN (select CASE
                       when sum(item.totalAmount) is NULL
                           then 0
                       else sum(item.totalAmount) end
            from iobsalesinvoiceitemgeneralmodel item
            where item.code = salesInvoiceCode);
END;
$$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getSalesInvoiceTotalTaxPercentage(salesInvoiceCode varchar)
    RETURNS float AS
$$
BEGIN
    RETURN (select case when sum(taxPercentages.taxPercentage) is null then 0 else sum(taxPercentages.taxPercentage) end
            from iobsalesinvoicetaxesgeneralmodel taxPercentages
            where taxPercentages.invoicecode = salesInvoiceCode);
END;
$$
    LANGUAGE PLPGSQL;

create or REPLACE VIEW IObSalesInvoiceItemGeneralModel as
select SII.id,
       SI.id                                             as refinstanceid,
       SI.code                                           as code,
       m.code                                            as itemCode,
       m.name                                            as itemName,
       baseUnitOfMeasure.code                            as baseUnitCode,
       baseUnitOfMeasure.symbol                          as baseUnitSymbol,
       c.code                                            as orderUnitCode,
       c.symbol                                          as orderunitsymbol,
       SII.quantityinorderunit                           as quantityInOrderUnit,
       SII.returnedQuantity                              as returnedQuantity,
       SII.price,
       getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid,
                                 SII.itemid)             AS conversionFactorToBase,
       SII.price * getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid,
                                             SII.itemid) as orderUnitPrice,
       ((SII.quantityinorderunit *
         getConversionFactorToBase(SII.baseunitofmeasureid, SII.orderunitofmeasureid, SII.itemid)) *
        SII.price)                                       as totalAmount,
       concat(m.code, ' - ', m.name :: json ->> 'en')    as itemCodeName,
       c.name                                            as orderUnitName,
       concat(c.code, ' - ', c.name :: json ->> 'en')    as orderUnitCodeName,
       concat(c.code, ' - ', c.symbol :: json ->> 'en')  as orderUnitCodeSymbol,
       baseUnitOfMeasure.name                            as baseUnitOfMeasureName,
       concat(baseUnitOfMeasure.code, ' - ',
              baseUnitOfMeasure.name :: json ->> 'en')   as baseUnitOfMeasureCodeName,
       concat(baseUnitOfMeasure.code, ' - ',
              baseUnitOfMeasure.symbol :: json ->> 'en') as baseUnitOfMeasureCodeSymbol,
       SII.creationDate,
       SII.creationInfo,
       SII.modifiedDate,
       SII.modificationInfo
from IObSalesInvoiceItem SII
         left join masterdata m on SII.itemId = m.id
         left join cobmeasure c on SII.orderunitofmeasureid = c.id
         left join mobitem on SII.itemid = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument SI on SII.refInstanceId = SI.id and SI.objecttypecode = 'SalesInvoice';


CREATE or REPLACE VIEW IObSalesInvoiceTaxesGeneralModel AS
SELECT IObSalesInvoiceTaxes.id,
       IObSalesInvoiceTaxes.creationDate,
       IObSalesInvoiceTaxes.modifiedDate,
       IObSalesInvoiceTaxes.creationInfo,
       IObSalesInvoiceTaxes.modificationInfo,
       IObSalesInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code AS invoicecode,
       IObSalesInvoiceTaxes.code  AS taxcode,
       IObSalesInvoiceTaxes.name  AS taxname,
       (getInvoiceTotalAmountBeforTaxes(dobaccountingdocument.code, 'SalesInvoice') *
        (SELECT CASE
                    WHEN dobaccountingdocument.currentStates LIKE '%Active%' then IObSalesInvoiceTaxes.percentage
                    WHEN dobaccountingdocument.currentStates LIKE '%Closed%' then IObSalesInvoiceTaxes.percentage
                    ELSE LObTaxInfo.percentage
                    END
        )) / 100                  AS taxAmount,
       (SELECT CASE
                   WHEN dobaccountingdocument.currentStates LIKE '%Active%' then IObSalesInvoiceTaxes.percentage
                   WHEN dobaccountingdocument.currentStates LIKE '%Closed%' then IObSalesInvoiceTaxes.percentage
                   ELSE LObTaxInfo.percentage
                   END
       )                          AS taxpercentage
FROM IObSalesInvoiceTaxes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = IObSalesInvoiceTaxes.refInstanceId
                       and dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join dobsalesinvoice on dobsalesinvoice.id = IObSalesInvoiceTaxes.refinstanceid
         left join LObTaxInfo on LObTaxInfo.code = IObSalesInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis


CREATE OR REPLACE VIEW DObSalesInvoiceGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.modificationInfo,
       userinfo.name                                                        as creationInfoName,
       dobsalesorder.code                                                   as salesOrder,
       cobcompanygeneralmodel.currencyiso                                   as companyCurrencyIso,
       cobcurrency.iso                                                      as currencyIso,
       cobcurrency.name                                                     as currencyName,
       (SELECT code
        from cobsalesinvoice
        where cobsalesinvoice.id = dobsalesinvoice.typeId)                  as invoiceTypeCode,
       (SELECT name
        from cobsalesinvoice
        where cobsalesinvoice.id = dobsalesinvoice.typeId)                  as invoiceType,
       (SELECT code
        from cobenterprise
        where cobenterprise.id = iobsalesinvoicecompanydata.purchaseUnitId) as purchaseUnitCode,
       (SELECT name
        from cobenterprise
        where cobenterprise.id = iobsalesinvoicecompanydata.purchaseUnitId) as purchaseUnitName,
       (SELECT name :: json ->> 'en'
        from cobenterprise
        where cobenterprise.id = iobsalesinvoicecompanydata.purchaseUnitId) as purchaseUnitNameEn,
       (SELECT code
        from cobenterprise
        where cobenterprise.id = iobsalesinvoicecompanydata.companyId)      as companyCode,
       (SELECT name
        from cobenterprise
        where cobenterprise.id = iobsalesinvoicecompanydata.companyId)      as companyName,
       (SELECT name
        from cobenterprise
        where cobenterprise.id = iobsalesinvoicecompanydata.companyId)      as company,
       (SELECT concat(cobenterprise.code, ' - ', cobenterprise.name :: json ->> 'en')
        from cobenterprise
        where cobenterprise.id = iobsalesinvoicecompanydata.companyId)      as companyCodeName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en')  AS customerCodeName,
       masterdata.code                                                      AS customerCode,
       masterdata.name                                                      as customer,
       dobsalesinvoice.documentOwnerId                                      as documentOwnerId,
       (SELECT name
        from UserInfo
        where UserInfo.id = dobsalesinvoice.documentOwnerId)                as documentOwner,
       (SELECT username
        from EBSUser
        where EBSUser.id = dobsalesinvoice.documentOwnerId)                 as documentOwnerUserName,
       invoiceSummary.totalremaining                                        AS remaining
FROM DObSalesInvoice
         LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN iobsalesinvoicebusinesspartner ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
         LEFT JOIN iobsalesinvoicecustomerbusinesspartner
                   ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
         LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
         left join ebsuser on ebsuser.username = dobaccountingdocument.creationinfo
         left join userinfo on userinfo.id = ebsuser.id
         left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesOrderId
         left join cobcompanygeneralmodel on cobcompanygeneralmodel.id = iobsalesinvoicecompanydata.companyId
         Left JOIN iobinvoicesummarygeneralmodel invoiceSummary ON dobsalesInvoice.id = invoiceSummary.id;


create OR REPLACE view IObSalesInvoiceBusinessPartnerGeneralModel as
select iobsalesinvoicebusinesspartner.id,
       iobsalesinvoicebusinesspartner.creationDate,
       iobsalesinvoicebusinesspartner.modifiedDate,
       iobsalesinvoicebusinesspartner.creationInfo,
       iobsalesinvoicebusinesspartner.modificationInfo,
       iobsalesinvoicebusinesspartner.type,
       dobsalesorder.code                                                         as salesOrder,
       iobsalesinvoicebusinesspartner.downPayment,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       masterdata.code                                                            as businessPartnerCode,
       masterdata.name                                                            as businessPartnerName,
       concat(masterdata.code, ' - ', masterdata.name :: json ->> 'en')           as businesspartnercodename,

       cobcurrency.iso                                                            as currencyIso,
       cobcurrency.code                                                           as currencyCode,
       cobcurrency.name                                                           as currencyName,

       cobpaymentterms.code                                                       AS paymentTermCode,
       cobpaymentterms.name                                                       AS paymentTermName,
       concat(cobpaymentterms.code, ' - ', cobpaymentterms.name :: json ->> 'en') as paymentTermCodeName
from iobsalesinvoicebusinesspartner
         left join iobsalesinvoicecustomerbusinesspartner
                   on iobsalesinvoicebusinesspartner.id = iobsalesinvoicecustomerbusinesspartner.id
         left join mobcustomer on iobsalesinvoicecustomerbusinesspartner.customerid = mobcustomer.id
         left join masterdata on masterdata.id = mobcustomer.id
         left join dobaccountingdocument on dobaccountingdocument.id = iobsalesinvoicebusinesspartner.refInstanceId and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join cobcurrency on cobcurrency.id = iobsalesinvoicebusinesspartner.currencyId
         left join cobpaymentterms on cobpaymentterms.id = iobsalesinvoicebusinesspartner.paymentTermId
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesorderid;



create OR REPLACE VIEW IObSalesInvoicePostingDetailsGeneralModel AS
SELECT postingDetails.id,
       postingDetails.refinstanceid,
       postingDetails.creationDate,
       postingDetails.creationInfo,
       postingDetails.modificationInfo,
       postingDetails.modifieddate,
       invoice.code AS invoiceCode,
       postingDetails.duedate
FROM IObSalesInvoicePostingDetails postingDetails
         JOIN dobaccountingdocument invoice
              on postingDetails.refinstanceid = invoice.id and invoice.objecttypecode = 'SalesInvoice';


CREATE OR REPLACE VIEW DObSalesInvoiceDataGeneralModel AS
SELECT DObSalesInvoice.id,
       dobaccountingdocument.code,
       IObSalesInvoicePostingDetails.dueDate,
       cobenterprise.name::json ->> 'ar'                                                            as companyArName,
       masterdata.name::json ->> 'en'                                                               as customerEnName,
       getInvoiceTotalAmountBeforTaxes(dobaccountingdocument.code,
                                       dobaccountingdocument.objecttypecode)                        as totalAmountBeforeTaxes,
       getSalesInvoiceTotalTaxPercentage(dobaccountingdocument.code)                                as totalTaxPercentages,
       getInvoiceTotalTaxAmount(dobaccountingdocument.code,
                                dobaccountingdocument.objecttypecode)                               as totalTaxAmount,
       (getInvoiceTotalAmountBeforTaxes(dobaccountingdocument.code, dobaccountingdocument.objecttypecode) +
        getInvoiceTotalTaxAmount(dobaccountingdocument.code, dobaccountingdocument.objecttypecode)) as netAmount,
       cobcurrency.iso                                                                              as customerCurrencyIso,
       cobpaymentterms.name::json ->> 'ar'                                                          as paymentTerm,
       enterpriseTelephone.contactValue                                                             as companyTelephone,
       enterpriseFax.contactValue                                                                   as companyFax,
       iobcompanylogodetails.logo                                                                   as companyLogo,
       IObEnterpriseAddress.addressLine::json ->> 'ar'                                              as companyArAddress,
       cobcompany.regnumber                                                                         as registrationNumber,
       cobcompany.taxcardnumber,
       cobcompany.taxfilenumber,
       lobtaxadministrative.name::json ->> 'ar'                                                     as taxadministrativeArName

from DObSalesInvoice
         left join IObSalesInvoicePostingDetails on DObSalesInvoice.id = IObSalesInvoicePostingDetails.refInstanceId
         left join dobaccountingdocument on DObSalesInvoice.id = dobaccountingdocument.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         left join cobenterprise
                   on iobsalesinvoicecompanydata.companyId = cobenterprise.id and cobenterprise.objecttypecode = '1'
         left join cobcompany on iobsalesinvoicecompanydata.companyId = cobcompany.id
         left join IObSalesInvoiceBusinessPartner on DObSalesInvoice.id = IObSalesInvoiceBusinessPartner.refInstanceId
         left join cobpaymentterms on IObSalesInvoiceBusinessPartner.paymentTermId = cobpaymentterms.id
         left join iobsalesinvoicecustomerbusinesspartner
                   on iobsalesinvoicecustomerbusinesspartner.id = IObSalesInvoiceBusinessPartner.id
         left join masterdata on iobsalesinvoicecustomerbusinesspartner.customerId = masterdata.id and
                                 masterdata.objecttypecode = '3'
         left join IObEnterpriseContact enterpriseTelephone
                   on cobcompany.id = enterpriseTelephone.refinstanceid and enterpriseTelephone.objectTypeCode = '1' and
                      enterpriseTelephone.contactTypeId = 1
         left join IObEnterpriseContact enterpriseFax
                   on cobcompany.id = enterpriseFax.refinstanceid and enterpriseFax.objectTypeCode = '1' and
                      enterpriseFax.contactTypeId = 2
         left join iobcompanylogodetails on cobcompany.id = iobcompanylogodetails.refinstanceid
         left join IObEnterpriseAddress on cobcompany.id = IObEnterpriseAddress.refinstanceid
         left join lobtaxadministrative on cobcompany.taxadministrativeid = lobtaxadministrative.id
         left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId;


create view ItemsLastSalesPriceGeneralModel as
select iobsalesinvoiceitem.id,
       dobaccountingdocument.code                                           as salesInvoiceCode,
       iobAccountingdocumentpostingdetails.creationdate                     as salesInvoicePostingDate,

       uom.code                                                             as uomCode,
       concat(uom.code, ' - ', uom.name :: json ->> 'en')                   as uomCodeName,

       item.code                                                            as itemCode,
       concat(item.code, ' - ', item.name :: json ->> 'en')                 as itemCodeName,

       customer.code                                                        as customerCode,
       concat(customer.code, ' - ', customer.name :: json ->> 'en')         as customerCodeName,

       purchaseUnit.code                                                    as purchaseUnitCode,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name :: json ->> 'en') as purchaseUnitCodeName,
       iobsalesinvoiceitem.price                                            as salesPrice
from iobsalesinvoiceitem
         LEFT JOIN dobaccountingdocument on iobsalesinvoiceitem.refinstanceid = dobaccountingdocument.id
    and dobaccountingdocument.objecttypecode = 'SalesInvoice' and dobaccountingdocument.currentstates like '%Active%'
         left join dobsalesinvoice on dobaccountingdocument.id = dobsalesinvoice.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN masterdata item on iobsalesinvoiceitem.itemid = item.id and item.objecttypecode = '1'
         left join cobmeasure uom on uom.id = iobsalesinvoiceitem.orderunitofmeasureid
         left join cobenterprise company
                   on company.id = iobsalesinvoicecompanydata.companyid and company.objecttypecode = '1'
         left join cobenterprise purchaseUnit
                   on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid and purchaseUnit.objecttypecode = '5'
         left join iobsalesinvoicebusinesspartner on iobsalesinvoicebusinesspartner.refinstanceid = dobsalesinvoice.id
         join iobsalesinvoiceCustomerbusinesspartner
              on iobsalesinvoiceCustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         join masterdata customer
              on customer.id = iobsalesinvoiceCustomerbusinesspartner.customerid and customer.objecttypecode = '3'
         left join iobAccountingdocumentpostingdetails
                   on dobsalesinvoice.id = iobAccountingdocumentpostingdetails.refinstanceid and
                      iobAccountingdocumentpostingdetails.objecttypecode = 'SalesInvoice';

create or replace view DobSalesInvoiceBasicGeneralModel
as
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.currentStates,
       dobsalesorder.code                                             as salesOrder,
       cobsalesinvoice.code                                           as invoiceTypeCode,
       cobsalesinvoice.name                                           as invoiceType,
       purchaseUnit.code                                              as purchaseUnitCode,
       purchaseUnit.name                                              as purchaseUnitName,
       purchaseUnit.name::json ->> 'en'                               as purchaseUnitNameEn,
       company.code                                                   as companyCode,
       company.name                                                   as company,
       masterdata.code                                                AS customerCode,
       masterdata.name                                                as customerName,
       concat(masterdata.code, ' - ', masterdata.name::json ->> 'en') as customerCodeName,
       EBSUser.username                                               as documentOwnerUserName,
       UserInfo.name                                                  as documentowner
FROM DObSalesInvoice
         LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN iobsalesinvoicebusinesspartner ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
         LEFT JOIN iobsalesinvoicecustomerbusinesspartner
                   ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
         LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
         Left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesOrderId
         Left JOIN cobsalesinvoice ON cobsalesinvoice.id = dobsalesinvoice.typeId
         Left JOIN cobenterprise purchaseUnit ON purchaseUnit.id = iobsalesinvoicecompanydata.purchaseUnitId
         Left JOIN EBSUser ON EBSUser.id = dobsalesinvoice.documentOwnerId
         Left JOIN UserInfo ON UserInfo.id = dobsalesinvoice.documentOwnerId
         Left JOIN cobenterprise company ON company.id = iobsalesinvoicecompanydata.companyId;

