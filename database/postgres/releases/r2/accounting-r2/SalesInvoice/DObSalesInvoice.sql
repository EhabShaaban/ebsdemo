
alter table iobsalesinvoiceitem rename orderUnitId to orderunitofMeasureId;
alter table iobsalesinvoiceitem rename quantity to quantityinorderunit;
alter table iobsalesinvoiceitem rename salesPrice to price;

alter table iobsalesinvoiceitem add column baseUnitOfMeasureId    INT8  ;
alter table iobsalesinvoiceitem add column returnedQuantity FLOAT DEFAULT 0;

UPDATE iobsalesinvoiceitem
set baseunitofmeasureid = (select baseunitofmeasureid from mobitem
                           where iobsalesinvoiceitem.itemid = mobitem.id);

ALTER TABLE dobsalesinvoice
    DROP COLUMN code CASCADE,
    DROP COLUMN creationDate CASCADE,
    DROP COLUMN modifiedDate CASCADE,
    DROP COLUMN creationInfo CASCADE,
    DROP COLUMN modificationInfo CASCADE,
    DROP COLUMN currentStates CASCADE;

ALTER TABLE dobsalesinvoice
    ADD CONSTRAINT FK_DObAccountingDocument_DOSalesInvoice FOREIGN KEY (id)
        REFERENCES DObAccountingDocument (id) ON DELETE CASCADE;

ALTER TABLE DObSalesInvoice
    DROP CONSTRAINT FK_collectionResponsible_DObSalesInvoice;

ALTER TABLE DObSalesInvoice
    RENAME COLUMN collectionResponsibleId TO documentOwnerId;
    
ALTER TABLE dobSalesinvoice
    ADD CONSTRAINT FK_EBSUser_DOSalesInvoice FOREIGN KEY (documentOwnerId)
        REFERENCES EBSUser (id) ON DELETE RESTRICT;

ALTER TABLE DObSalesInvoice
    DROP CONSTRAINT FK_cobcompany_DObSalesInvoice;

ALTER TABLE DObSalesInvoice
    DROP CONSTRAINT FK_purchesUnit_DObSalesInvoice;

ALTER TABLE dobsalesinvoice 
	DROP COLUMN companyId CASCADE,
	DROP COLUMN purchaseunitid CASCADE;

DROP TABLE IF EXISTS IObSalesInvoiceTaxes;

CREATE TABLE IObSalesInvoiceTaxes (
  id               BIGSERIAL     NOT NULL,
  refInstanceId    BIGSERIAL     NOT NULL REFERENCES dobsalesinvoice (id) ON DELETE CASCADE,
  creationDate     TIMESTAMP     NOT NULL,
  modifiedDate     TIMESTAMP     NOT NULL,
  creationInfo     VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  code     VARCHAR(30) NOT NULL,
  name     json NOT NULL,
  percentage       DOUBLE PRECISION,
  amount           DOUBLE PRECISION,

  PRIMARY KEY (id)
);

alter table IObSalesInvoiceBusinessPartner
    drop column salesorder,
    add column salesOrderId int8,
    add constraint FK_IObSalesInvoiceBusinessPartner_DOSalesOrder FOREIGN KEY (salesOrderId)
        REFERENCES dobsalesorder (id) ON DELETE CASCADE