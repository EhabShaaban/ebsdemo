DROP TABLE IF EXISTS CObLandedCostType;
CREATE TABLE CObLandedCostType
(
   id BIGSERIAL NOT NULL,
   code VARCHAR (1024) NOT NULL,
   creationDate TIMESTAMP With time zone NOT NULL,
   modifiedDate TIMESTAMP With time zone NOT NULL,
   creationInfo VARCHAR (1024) NOT NULL,
   modificationInfo VARCHAR (1024) NOT NULL,
   currentstates VARCHAR (1024),
   name JSON NOT NULL,
   PRIMARY KEY (id)
);
DROP TABLE IF EXISTS DObLandedCost;
CREATE TABLE DObLandedCost
(
   id BIGSERIAL NOT NULL,
   typeId INT8 NOT NULL REFERENCES CObLandedCostType (id) ON DELETE RESTRICT,
   PRIMARY KEY (id)
);
DROP TABLE IF EXISTS IObLandedCostDetails;
CREATE TABLE IObLandedCostDetails
(
   id BIGSERIAL NOT NULL,
   refInstanceId INT8 NOT NULL REFERENCES DObLandedCost (id) ON DELETE CASCADE,
   creationDate TIMESTAMP NOT NULL,
   modifiedDate TIMESTAMP NOT NULL,
   creationInfo VARCHAR (1024) NOT NULL,
   modificationInfo VARCHAR (1024) NOT NULL,
   purchaseOrderId INT8 REFERENCES DObOrder (id) ON DELETE RESTRICT,
   vendorInvoiceId INT8 REFERENCES DobVendorInvoice (id) ON DELETE RESTRICT,
   damageStock boolean not null,
   PRIMARY KEY (id)
);

DROP TABLE IF EXISTS IObLandedCostCalculationStatus;
CREATE TABLE IObLandedCostCalculationStatus
(
   id BIGSERIAL NOT NULL,
   refInstanceId INT8 NOT NULL REFERENCES DObLandedCost (id) ON DELETE CASCADE,
   creationDate TIMESTAMP NOT NULL,
   modifiedDate TIMESTAMP NOT NULL,
   creationInfo VARCHAR (1024) NOT NULL,
   modificationInfo VARCHAR (1024) NOT NULL,
   estimateCostCurrencyPrice double precision,
   actualCostCurrencyPrice double precision ,
   PRIMARY KEY (id)
);

CREATE TABLE IObLandedCostFactorItems
(
    id BIGSERIAL NOT NULL,
    refInstanceId INT8 NOT NULL REFERENCES DObLandedCost (id) ON DELETE CASCADE,
    costItemId INT8 NOT NULL REFERENCES MObItem (id) ON DELETE RESTRICT,
    estimateValue double precision,
    actualValue double precision ,
    creationDate TIMESTAMP NOT NULL,
    modifiedDate TIMESTAMP NOT NULL,
    creationInfo VARCHAR (1024) NOT NULL,
    modificationInfo VARCHAR (1024) NOT NULL,
    PRIMARY KEY (id)
);