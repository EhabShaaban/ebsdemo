DROP VIEW IF EXISTS DObLandedCostGeneralModel;
DROP VIEW IF EXISTS IObLandedCostCalculationStatusGeneralModel;
CREATE OR REPLACE VIEW DObLandedCostGeneralModel AS
SELECT LC.id,
       AccDoc.code,
       AccDoc.creationdate,
       AccDoc.modifieddate,
       AccDoc.creationinfo,
       AccDoc.modificationinfo,
       AccDoc.currentstates,
       cobenterprise.code                AS purchaseUnitCode,
       cobenterprise.name                AS purchaseUnitName,
       cobenterprise.name::json ->> 'en' AS purchaseUnitNameEn,
       CObLandedCostType.code            AS typeCode,
       CObLandedCostType.name            AS typeName,
       doborder.code                     AS purchaseOrderCode

FROM DObLandedCost LC
         LEFT JOIN DObAccountingDocument AccDoc ON LC.id = AccDoc.id AND AccDoc.objecttypecode = 'LandedCost'
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
	        ON LC.id = IObLandedCostCompanyData.refinstanceid AND IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         LEFT JOIN cobenterprise ON IObLandedCostCompanyData.purchaseUnitId = cobenterprise.id
         LEFT JOIN IObLandedCostDetails LCD ON LC.id = LCD.refinstanceid
         LEFT JOIN DObOrder ON LCD.purchaseorderid = doborder.id
         LEFT JOIN CObLandedCostType ON CObLandedCostType.id = LC.typeId;



CREATE OR REPLACE View IObLandedCostCalculationStatusGeneralModel AS
SELECT landedcostdocument.code,
       coststatus.id,
       coststatus.refinstanceid,
       coststatus.creationdate,
       coststatus.modifieddate,
       coststatus.creationinfo,
       coststatus.modificationinfo,
       coststatus.actualcostcurrencyprice,
       coststatus.estimatecostcurrencyprice,
       getInvoiceNetAmount(invoicedocument.code, 'VendorInvoice') *
       coststatus.estimatecostcurrencyprice as estimateCostGoodsInvoiceTotalValue,
       getInvoiceNetAmount(invoicedocument.code, 'VendorInvoice') *
       coststatus.actualcostcurrencyprice   as actualCostGoodsInvoiceTotalValue,
       invoicecurrency.iso                  as goodsInvoiceCurrency,
       companycurrency.iso                  as companyCurrency
from ioblandedcostcalculationstatus coststatus
         left join doblandedcost landedcost on coststatus.refinstanceid = landedcost.id
         left join dobaccountingdocument landedcostdocument on landedcost.id = landedcostdocument.id
         left join ioblandedcostdetails costdetails on costdetails.refinstanceid = landedcost.id
         left join dobaccountingdocument invoicedocument on costdetails.vendorinvoiceid = invoicedocument.id
         left join iobvendorinvoicepurchaseorder invoicepurchaseorder
                   on invoicepurchaseorder.refinstanceid = costdetails.vendorinvoiceid
         left join cobcurrency invoicecurrency on invoicepurchaseorder.currencyid = invoicecurrency.id
         left join iobenterprisedata
                   on iobenterprisedata.refinstanceid = costdetails.purchaseorderid and enterprisetype = 'Company'
         left join iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = iobenterprisedata.enterpriseid
         left join cobcurrency companycurrency on companycurrency.id = iobenterprisebasicdata.currencyid;

create or replace view IObLandedCostDetailsGeneralModel as
select ioblandedcostdetails.id,
       ioblandedcostdetails.refinstanceid,
       ioblandedcostdetails.creationdate,
       ioblandedcostdetails.creationinfo,
       ioblandedcostdetails.modificationinfo,
       ioblandedcostdetails.modifieddate,
       landedCost.code                                                              as landedCostCode,
       dobaccountingdocument.code                                                   as vendorInvoiceCode,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           dobaccountingdocument.objecttypecode)                    as invoiceAmount,
       doborder.code                                                                as purchaseOrderCode,
       companyCurrency.iso                                                          as companyCurrencyIso,
       invoiceCurrency.iso                                                          as invoiceCurrencyIso,
       ioblandedcostdetails.damageStock,
       cobenterprise.code                                                           as businessUnitCode,
       cobenterprise.name                                                           as businessUnitName,
       concat(cobenterprise.code, ' ', '-', ' ', cobenterprise.name::json ->> 'en') as businessUnitCodeName,
       (select case
                   when getAmountPaidViaAccountingDocument(dobaccountingdocument.code,
                                                           dobaccountingdocument.objecttypecode) = 0.0
                       then (select avg(iobaccountingdocumentpostingdetails.currencyprice)
                             from iobaccountingdocumentpostingdetails
                                      left JOIN iobpaymentrequestpaymentdetails
                                                on iobpaymentrequestpaymentdetails.refinstanceid =
                                                   iobaccountingdocumentpostingdetails.refinstanceid
                                      left join iobpaymentrequestinvoicepaymentdetails
                                                on iobpaymentrequestinvoicepaymentdetails.id =
                                                   iobpaymentrequestpaymentdetails.id
                             where iobpaymentrequestinvoicepaymentdetails.duedocumentid =
                                   ioblandedcostdetails.vendorinvoiceid
                   )
                   else (select cobexchangerate.firstvalue * cobexchangerate.secondvalue
                         from cobexchangerate
                         where (cobexchangerate.secondcurrencyid = iobenterprisebasicdata.currencyid
                             and cobexchangerate.firstcurrencyid = iobvendorinvoicepurchaseorder.currencyid)
                         ORDER BY id DESC
                         LIMIT 1) end
       )                                                                            as currencyPrice
from ioblandedcostdetails
         left join dobaccountingdocument landedCost on ioblandedcostdetails.refinstanceid = landedCost.id
         left join doborder
                   on doborder.id = ioblandedcostdetails.purchaseorderid
         left join dobaccountingdocument on ioblandedcostdetails.vendorinvoiceid = dobaccountingdocument.id

         left join iobenterprisedata on iobenterprisedata.refinstanceid = doborder.id and
                                        iobenterprisedata.enterprisetype = 'Company'
         left join iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = iobenterprisedata.enterpriseid
         inner join iobvendorinvoicepurchaseorder
                    on iobvendorinvoicepurchaseorder.id = ioblandedcostdetails.vendorinvoiceid

         left join cobcurrency invoiceCurrency on invoiceCurrency.id = iobvendorinvoicepurchaseorder.currencyid
         left join cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
         left join doblandedcost on doblandedcost.id = ioblandedcostdetails.refinstanceid
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
	        ON landedCost.id = IObLandedCostCompanyData.refinstanceid AND IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         left join cobenterprise on cobenterprise.id = IObLandedCostCompanyData.purchaseUnitId;



create or REPLACE view IObLandedCostFactorItemsGeneralModel as
select ioblandedcostfactoritems.id,
       dobaccountingdocument.code,
       ioblandedcostfactoritems.refinstanceid,
       masterdata.code               as itemCode,
       masterdata.name               as itemName,
       estimateValue                 as estimatedvalue,
       actualValue                   as actualvalue,
       (estimateValue - actualValue) as difference,
       companyCurrency.iso           as companyCurrency,
       doborder.code                 as orderCode,
       ioblandedcostfactoritems.creationDate,
       ioblandedcostfactoritems.modifiedDate,
       ioblandedcostfactoritems.creationInfo,
       ioblandedcostfactoritems.modificationInfo
from ioblandedcostfactoritems
         left join dobaccountingdocument on dobaccountingdocument.id = ioblandedcostfactoritems.refInstanceId and
                                            objecttypecode = 'LandedCost'
         left JOIN masterdata on masterdata.id = ioblandedcostfactoritems.costItemId
         left JOIN ioblandedcostdetails on ioblandedcostdetails.refinstanceid = ioblandedcostfactoritems.refInstanceId
         left join doborder
                   on doborder.id = ioblandedcostdetails.purchaseorderid
         left join iobenterprisedata on iobenterprisedata.refinstanceid = doborder.id and
                                        iobenterprisedata.enterprisetype = 'Company'
         left join iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = iobenterprisedata.enterpriseid
         left join cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid;

create or replace view IObLandedCostFactorItemsSummary as
select dobaccountingdocument.code,
       dobaccountingdocument.id,
       companyCurrency.iso                     as companyCurrency,
       sum(estimatevalue)                      as estimateTotalAmount,
       sum(actualvalue)                        as actualTotalAmount,
       (sum(estimatevalue) - sum(actualvalue)) as differenceTotalAmount
from ioblandedcostfactoritems
         left join dobaccountingdocument on dobaccountingdocument.id = ioblandedcostfactoritems.refinstanceid and
                                            objecttypecode = 'LandedCost'
         left JOIN ioblandedcostdetails on ioblandedcostdetails.refinstanceid = ioblandedcostfactoritems.refInstanceId
         left join doborder
                   on doborder.id = ioblandedcostdetails.purchaseorderid
         left join iobenterprisedata on iobenterprisedata.refinstanceid = doborder.id and
                                        iobenterprisedata.enterprisetype = 'Company'
         left join iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = iobenterprisedata.enterpriseid
         left join cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
Group By (ioblandedcostfactoritems.refinstanceid, dobaccountingdocument.id, dobaccountingdocument.code,
          companyCurrency.iso);





