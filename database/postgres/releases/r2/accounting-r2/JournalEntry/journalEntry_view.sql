
CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
  SELECT DObJournalEntry.code,
         DObJournalEntry.currentStates,
         DObJournalEntry.id,
         DObJournalEntry.creationDate,
         DObJournalEntry.modifiedDate,
         DObJournalEntry.creationInfo,
         DObJournalEntry.modificationInfo,
         DObJournalEntry.objectTypeCode                                 as documentReferenceType,
         DObJournalEntry.dueDate,
         company.code                                                   AS companyCode,
         company.name                                                   AS companyName,
         purchaseUnit.code                                              AS purchaseUnitCode,
         purchaseUnit.name                                              AS purchaseUnitName,
         purchaseUnit.name :: json ->> 'en'                             AS purchaseUnitNameEn,
         (SELECT dobaccountingdocument.code
          FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument ON DObVendorInvoiceJournalEntry.documentreferenceid = dobaccountingdocument.id
          WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id AND dobaccountingdocument.objecttypecode = 'VendorInvoice'
          UNION
          SELECT dobaccountingdocument.code
          FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument ON DObPaymentRequestJournalEntry.documentreferenceid = dobaccountingdocument.id
          WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
          UNION
          SELECT DObCreditDebitNote.code
          FROM DObCreditNoteJournalEntry
                 LEFT JOIN DObCreditDebitNote ON DObCreditNoteJournalEntry.documentreferenceid = DObCreditDebitNote.id
          WHERE DObCreditDebitNote.type = 'CREDIT_NOTE'
            AND DObJournalEntry.id = DObCreditNoteJournalEntry.id
          UNION
          SELECT DObCreditDebitNote.code
          FROM DObDebitNoteJournalEntry
                 LEFT JOIN DObCreditDebitNote ON DObDebitNoteJournalEntry.documentreferenceid = DObCreditDebitNote.id
          WHERE DObCreditDebitNote.type = 'DEBIT_NOTE'
            AND DObJournalEntry.id = DObDebitNoteJournalEntry.id
          UNION
          SELECT dobaccountingdocument.code
          FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument ON DObSalesInvoiceJournalEntry.documentreferenceid = dobaccountingdocument.id
          WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
          UNION
          SELECT dobaccountingdocument.code
          FROM dobcollectionrequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                   ON dobcollectionrequestJournalEntry.documentreferenceid = dobaccountingdocument.id
          WHERE DObJournalEntry.id = dobcollectionrequestJournalEntry.id AND dobaccountingdocument.objecttypecode = 'CollectionRequest'
          UNION
          SELECT dobnotesreceivables.code
          FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobnotesreceivables
                   ON dobNotesReceivableJournalEntry.documentreferenceid = dobnotesreceivables.id
          WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id) as documentReferenceCode
  FROM DObJournalEntry
         LEFT JOIN CObEnterprise company ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id;

