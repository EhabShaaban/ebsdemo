drop view IObSalesInvoiceCompanyTaxesDetailsGeneralModel;
drop table iobsalesinvoicecompanytaxesdetails;
ALTER TABLE IObJournalEntryItemTaxSubAccount
    Drop CONSTRAINT If EXISTS FK_IObJournalEntryItemTaxSubAccount_iobcompanyTaxdetails;
DROP VIEW IF EXISTS IObCompanyTaxesDetailsGeneralModel;
DROP VIEW If EXISTS LObCompanyTaxesGeneralModel;
DROP VIEW If EXISTS LObVendorTaxesGeneralModel;
DROP VIEW If EXISTS IObTaxAccountingInfoGeneralModel;
DROP TABLE If EXISTS IObCompanyTaxesDetails;
DROP TABLE If EXISTS iobtaxaccountinginfo;
ALTER TABLE LObTaxInfo ADD IF NOT EXISTS description VARCHAR(255);
ALTER TABLE LObTaxInfo ADD IF NOT EXISTS percentage DOUBLE PRECISION;
ALTER TABLE LObTaxInfo ADD IF NOT EXISTS objectTypeCode  VARCHAR(255);
ALTER TABLE LObTaxInfo ADD IF NOT EXISTS sysflag boolean DEFAULT false;

DROP TABLE IF EXISTS LObVendorTaxes cascade ;

CREATE TABLE LObVendorTaxes (
    id             INT8          NOT NULL     REFERENCES lobtaxinfo (id) ON DELETE cascade ,
    vendorId      INT8           NOT NULL     REFERENCES MObVendor (id) ON DELETE restrict ,

    PRIMARY KEY (id)
);


DROP TABLE IF EXISTS LObCompanyTaxes cascade ;

CREATE TABLE LObCompanyTaxes (
    id            INT8           NOT NULL     REFERENCES lobtaxinfo (id) ON DELETE cascade,
    companyId     INT8          NOT NULL     REFERENCES CObCompany (id) ON DELETE restrict,

    PRIMARY KEY (id)
);

ALTER TABLE IObJournalEntryItemTaxSubAccount
    ADD CONSTRAINT FK_IObJournalEntryItemTaxSubAccount_iobcompanyTaxdetails FOREIGN KEY (subAccountId)
        REFERENCES LObCompanyTaxes (id) ON DELETE RESTRICT;