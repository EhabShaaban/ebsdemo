CREATE OR REPLACE  VIEW LObCompanyTaxesGeneralModel AS
SELECT lobcompanytaxes.id,
       lobtaxinfo.code,
       lobtaxinfo.creationDate,
       lobtaxinfo.modifiedDate,
       lobtaxinfo.creationinfo,
       lobtaxinfo.modificationInfo,
       lobtaxinfo.name,
       lobtaxinfo.percentage,
       cobenterprise.code as companyCode,
       cobenterprise.name as companyName,
       lobtaxinfo.description,
       lobtaxinfo.sysflag
From lobcompanytaxes left join lobtaxinfo on lobtaxinfo.id = lobcompanytaxes.id
left join cobcompany on lobcompanytaxes.companyid = cobcompany.id
left join cobenterprise on cobenterprise.id = cobcompany.id;

CREATE OR REPLACE  VIEW LObVendorTaxesGeneralModel AS
SELECT lobvendortaxes.id,
       lobtaxinfo.code,
       lobtaxinfo.creationDate,
       lobtaxinfo.modifiedDate,
       lobtaxinfo.creationinfo,
       lobtaxinfo.modificationInfo,
       lobtaxinfo.name,
       lobtaxinfo.percentage,
       masterdata.code as vendorCode,
       masterdata.name as vendorName,
       lobtaxinfo.description,
       lobtaxinfo.sysflag
From lobvendortaxes left join lobtaxinfo on lobtaxinfo.id = lobvendortaxes.id
left join masterdata on lobvendortaxes.vendorid = masterdata.id and masterdata.objecttypecode = '2'
