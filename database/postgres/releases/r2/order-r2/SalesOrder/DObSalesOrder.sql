drop table if exists IObSalesOrderTax;

CREATE TABLE IObSalesOrderTax
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObSalesOrder (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    name             JSON                     NOT NULL,
    percentage       DOUBLE PRECISION         NOT NULL,
    PRIMARY KEY (id)
);
