DROP VIEW IF EXISTS IObSalesOrderTaxGeneralModel;

CREATE VIEW IObSalesOrderTaxGeneralModel AS
SELECT iobsalesordertax.id,
       iobsalesordertax.creationDate,
       iobsalesordertax.modifiedDate,
       iobsalesordertax.creationInfo,
       iobsalesordertax.modificationInfo,
       iobsalesordertax.refinstanceid,
       dobsalesorder.code AS salesOrderCode,
       iobsalesordertax.code,
       iobsalesordertax.name,
       iobsalesordertax.percentage
FROM iobsalesordertax
         JOIN dobsalesorder ON dobsalesorder.id = iobsalesordertax.refInstanceId