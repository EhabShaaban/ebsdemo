drop view if exists IObPurchaseOrderItemsGeneralModel CASCADE;

CREATE VIEW IObPurchaseOrderItemsGeneralModel AS
SELECT ioborderlinedetails.id,
       doborder.id                                                 as purchaseOrderId, --purchaseID
       doborder.code                                               as ordercode,
       (Select masterdata.name
        from masterdata
        where masterdata.id = ioborderlinedetails.itemId)          AS itemname,
       mobitemgeneralmodel.code                                    as itemcode,
       mobitemgeneralmodel.basicUnitOfMeasure                      as basicUnitOfMeasureId,
       mobitemgeneralmodel.basicUnitOfMeasurecode                      as basicUnitOfMeasureCode,
       mobitemgeneralmodel.basicUnitOfMeasuresymbol                      as basicUnitOfMeasureSymbol,
       mobitemgeneralmodel.batchmanagementrequired,
       itemvendorrecord.itemvendorcode,
       itemvendorrecord.itemId,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId) as unitsymbol,
       (select SUM(ioborderlinedetailsquantities.quantity) as quantity
        from ioborderlinedetailsquantities
        where ioborderlinedetails.id =
              ioborderlinedetailsquantities.refinstanceid)         as itemQuantity,
       ioborderlinedetails.quantityindn

FROM doborder
         join ioborderlinedetails on doborder.id = ioborderlinedetails.refinstanceid
         join mobitemgeneralmodel on ioborderlinedetails.itemid = mobitemgeneralmodel.id
         left join itemvendorrecord
              on ioborderlinedetails.itemid = itemvendorrecord.itemid
                  ANd doborder.vendorid = itemvendorrecord.vendorid
                  And ioborderlinedetails.unitofmeasureid = itemvendorrecord.uomid
where MObItemGeneralModel.basicunitofmeasure is not null
  and mobitemgeneralmodel.batchmanagementrequired is not null;
