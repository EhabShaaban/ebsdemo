drop table if exists IObSalesReturnOrderCompanyTaxes;
drop table if exists IObSalesReturnOrderItems;
drop table if exists IObSalesReturnOrderDetails;
drop table if exists IObSalesReturnOrderCompanyData;
drop table if exists DObSalesReturnOrder;
drop table if exists CObSalesReturnOrderType;

CREATE TABLE CObSalesReturnOrderType
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024)            NOT NULL,
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE DObSalesReturnOrder
(
    id                     BIGSERIAL                NOT NULL,
    code                   VARCHAR(1024)            NOT NULL,
    creationDate           TIMESTAMP With time zone NOT NULL,
    modifiedDate           TIMESTAMP With time zone NOT NULL,
    creationInfo           VARCHAR(1024)            NOT NULL,
    modificationInfo       VARCHAR(1024)            NOT NULL,
    currentStates          VARCHAR(1024)            NOT NULL,
    salesReturnOrderTypeId INT8                     NOT NULL REFERENCES CObSalesReturnOrderType (id) ON DELETE RESTRICT,
    documentOwnerId        INT8                     NOT NULL REFERENCES EBSUser (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesReturnOrderCompanyData
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObSalesReturnOrder (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    businessUnitId   INT8                     NOT NULL REFERENCES cobenterprise (id) ON DELETE RESTRICT,
    companyId        INT8                     NOT NULL REFERENCES CObCompany (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesReturnOrderDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObSalesReturnOrder (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    salesInvoiceId   INT8                     NOT NULL REFERENCES dobsalesinvoice (id) ON DELETE RESTRICT,
    customerId       INT8                     NOT NULL REFERENCES mobcustomer (id) ON DELETE RESTRICT,
    currencyId       INT8                     NOT NULL REFERENCES cobcurrency (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesReturnOrderItems
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObSalesReturnOrder (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    itemId           INT8                     NOT NULL REFERENCES mobitem (id) ON DELETE RESTRICT,
    orderUnitId      INT8                     NOT NULL REFERENCES cobmeasure (id) ON DELETE RESTRICT,
    returnQuantity   FLOAT                    NOT NULL,
    salesPrice       FLOAT                    NOT NULL,
    returnReasonId   INT8 REFERENCES LObReason (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesReturnOrderTax
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObSalesReturnOrder (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    name             JSON                     NOT NULL,
    percentage       DOUBLE PRECISION         NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesReturnOrderCycleDates
(
    id               BIGSERIAL                NOT NULL,
    refinstanceid    INT8                     NOT NULL REFERENCES DObSalesReturnOrder (id) ON DELETE CASCADE,
    creationdate     timestamp with time zone NOT NULL,
    modifieddate     timestamp with time zone NOT NULL,
    creationinfo     varchar(1024)            NOT NULL,
    modificationinfo varchar(1024)            NOT NULL,
    ShippingDate     timestamp with time zone,
    PRIMARY KEY (id)
);