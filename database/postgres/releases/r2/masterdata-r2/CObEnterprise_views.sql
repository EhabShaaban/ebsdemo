DROP VIEW IF EXISTS CObProductManagerGeneralModel;

CREATE VIEW CObProductManagerGeneralModel AS
SELECT CObEnterprise.id,
       CObEnterprise.code,
       CObEnterprise.name,
       CObEnterprise.currentStates,
       CObEnterprise.creationDate,
       CObEnterprise.modifiedDate,
       CObEnterprise.creationInfo,
       CObEnterprise.modificationInfo
FROM CObEnterprise
WHERE CObEnterprise.objectTypeCode = '10';
