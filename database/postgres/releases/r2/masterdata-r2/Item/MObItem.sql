
ALTER TABLE MObItem ADD COLUMN productManagerId INT8;

ALTER TABLE MObItem
    ADD CONSTRAINT MObItem_ProductManagerId_Fkey FOREIGN KEY (productManagerId)
        REFERENCES cobenterprise (id) ON DELETE RESTRICT;
