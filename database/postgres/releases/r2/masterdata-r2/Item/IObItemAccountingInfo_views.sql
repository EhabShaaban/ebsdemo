drop view if exists IObItemAccountingInfoGeneralModel CASCADE;


create view IObItemAccountingInfoGeneralModel as
select
    IObItemAccountingInfo.id,
    IObItemAccountingInfo.refInstanceId,
    IObItemAccountingInfo.creationDate,
    IObItemAccountingInfo.modifiedDate,
    IObItemAccountingInfo.creationInfo,
    IObItemAccountingInfo.modificationInfo,
    IObItemAccountingInfo.costFactor,
    hobglaccount.code as accountCode,
    hobglaccount.name as accountName,
    (select
         MasterData.code
     from MasterData
     where MasterData.id = refInstanceId
    )
                      as itemCode
from IObItemAccountingInfo
         join hobglaccount on IObItemAccountingInfo.chartOfAccountId = hobglaccount.id;