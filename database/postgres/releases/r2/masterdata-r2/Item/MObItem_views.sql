
create or replace view MObItemGeneralModel as
select MasterData.id,
       MasterData.code,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.currentstates,
       MasterData.name,
       MObItem.oldItemNumber,
       MObItem.itemgroup,
       MObItem.basicunitofmeasure,
       MObItem.marketname,
       CObItem.code as typeCode,
       CObItem.name as typeName,
       (select cobmeasure.code
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasurecode,
       (select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasuresymbol,
       (select cobmeasure.name
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasurename,
       MObItem.batchmanagementrequired,
       (select CObMaterialGroup.name
        from CObMaterialGroup
        where MObItem.itemgroup = CObMaterialGroup.id
       ) as itemgroupname,
       (select CObMaterialGroup.code
        from CObMaterialGroup
        where MObItem.itemgroup = CObMaterialGroup.id
       ) as itemgroupcode,
       (select string_agg(cobenterprise.code, ', ')
        from cobenterprise
        where cobenterprise.id in (select iobitempurchaseunit.purchaseunitid
                                   from iobitempurchaseunit
                                   where iobitempurchaseunit.refInstanceId = MObItem.id)
          and cobenterprise.objectTypeCode = '5'
       ) as purchaseunitscodes,
       (select array_to_json(array_agg(cobenterprise.name))
        from cobenterprise
        where cobenterprise.id in (select iobitempurchaseunit.purchaseunitid
                                   from iobitempurchaseunit
                                   where iobitempurchaseunit.refInstanceId = MObItem.id)
          and cobenterprise.objectTypeCode = '5'
       ) as purchaseunitsnames,
       (select cobenterprise.code
        from cobenterprise
        where cobenterprise.id = MObItem.productmanagerid
          and cobenterprise.objectTypeCode = '10'
       ) as productmanagercode,
       (select cobenterprise.name
        from cobenterprise
        where cobenterprise.id = MObItem.productmanagerid
          and cobenterprise.objectTypeCode = '10'
       ) as productmanagername,
       IObItemAccountingInfo.costFactor
from mobitem
         left join MasterData on mobitem.id = MasterData.id
         left join cobitem on mobitem.typeid = cobitem.id
         left join IObItemAccountingInfo on IObItemAccountingInfo.refinstanceid = mobitem.id;
         