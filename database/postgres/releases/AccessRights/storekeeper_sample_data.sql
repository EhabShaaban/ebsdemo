INSERT INTO public.storekeeper(id, code, name) VALUES (25, '0006', '{"en":"Ahmed.Ali","ar":"أحمد علي"}');

INSERT INTO public.storekeeper(id, code, name) VALUES (39, '0007', '{"en":"Mahmoud.Abdelaziz","ar":"محمود عبد العزيز"}');

INSERT INTO public.storekeeper(id, code, name) VALUES (62, '0008', '{"en":"Ali.Hasan","ar": "علي حسن"}');

INSERT INTO public.storekeeper(id, code, name) VALUES (32, '0009', '{"en":"Hamada.khalaf","ar": "حمادة خلف"}');

INSERT INTO public.storekeeper(id, code, name) VALUES (43, '0010', '{"en":"Ahmed.Gamal","ar": "أحمد جمال"}');



CREATE SEQUENCE storekeeper_id_seq;
ALTER TABLE storekeeper
    ALTER COLUMN id SET DEFAULT nextval('storekeeper_id_seq'),
    ALTER COLUMN id SET NOT NULL;
ALTER SEQUENCE storekeeper_id_seq OWNED BY storekeeper.id;
SELECT setval('storekeeper_id_seq', COALESCE(max(id)+1, 70)) FROM storekeeper;

