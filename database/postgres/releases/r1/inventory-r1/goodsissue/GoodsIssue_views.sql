DROP VIEW IF Exists DObGoodsIssueDataGeneralModel CASCADE;

CREATE VIEW DObGoodsIssueDataGeneralModel AS
SELECT ROW_NUMBER() OVER () AS id,
       DObGoodsIssue.code,
       c1.id                                           as purchaseUnitId,
       c1.code                                         as purchaseUnitCode,
       c1.name                                         as purchaseUnitName,
       DObGoodsIssue.currentStates,
       m1.id                                           as itemId,
       m1.code                                         as itemCode,
       m1.name                                         as itemName,
       cobmeasure.id                                   AS unitOfMeasureId,
       cobmeasure.code                                 AS unitOfMeasureCode,
       cobmeasure.name                                 AS unitOfMeasureName,
       IObGoodsIssueItem.quantity,
       IObGoodsIssueItem.batchNo,
       IObGoodsIssueItem.price,
       CObGoodsIssue.code                              as goodsIssueTypeCode,
       CObGoodsIssue.name                              as goodsIssueTypeName,
       DObGoodsIssue.creationInfo,
       DObGoodsIssue.creationDate,
       DObGoodsIssue.modificationInfo,
       DObGoodsIssue.modifiedDate,
       DObSalesInvoice.code                            as invoiceCode,
       c2.id                                           as companyId,
       c2.code                                         as companyCode,
       c2.name                                         as companyName,
       c3.id                                           as plantId,
       c3.code                                         as plantCode,
       c3.name                                         as plantName,
       c4.id                                           as storeHouseId,
       c4.code                                         as storeHouseCode,
       c4.name                                         as storeHouseName,
       storekeeper.code                                as storekeeperCode,
       storekeeper.name                                as storekeeperName,
       m2.code                                         as customerCode,
       m2.name                                         as customerName,
       lobaddress.address::json ->> 'en'               as address,
       LObContactPerson.name                           as contactPersonName,
       c5.code                                         as salesRepresentativeCode,
       c5.name                                         as salesRepresentativeName,
       IObGoodsIssueConsignee.comments,
       c6.code                                         as kapCode,
       c6.name                                         as kapName,
       case when iobalternativeuom.conversionfactor is null then 1
       else iobalternativeuom.conversionfactor end as conversionfactor,
       c2.name::json ->> 'ar'                          as companyArName,
       enterpriseTelephone.contactValue                as companyTelephone,
       enterpriseFax.contactValue                      as companyFax,
       iobcompanylogodetails.logo                      as companyLogo,
       IObEnterpriseAddress.addressLine                as companyAddress,
       IObEnterpriseAddress.addressLine::json ->> 'ar' as companyArAddress,
       m2.name::json ->> 'ar'                          as customerArName

from DObGoodsIssue
         left join cobenterprise c1 on DObGoodsIssue.purchaseunitid = c1.id
         left join IObGoodsIssueItem on DObGoodsIssue.id = IObGoodsIssueItem.refinstanceid
         left join mobitem on iobgoodsissueitem.itemid = mobitem.id
         left join masterdata m1 on mobitem.id = m1.id
         left join cobmeasure on iobgoodsissueitem.unitOfMeasureId = cobmeasure.id
         left join CObGoodsIssue on DObGoodsIssue.typeid = CObGoodsIssue.id
         left join IObGoodsIssueStore on dobgoodsissue.id = iobgoodsissuestore.refinstanceid
         left join cobcompany on iobgoodsissuestore.companyid = cobcompany.id
         left join cobenterprise c2 on c2.id = cobcompany.id
         left join cobplant on iobgoodsissuestore.plantid = cobplant.id
         left join cobenterprise c3 on c3.id = cobplant.id
         left join cobstorehouse on iobgoodsissuestore.storehouseid = cobstorehouse.id
         left join cobenterprise c4 on c4.id = cobstorehouse.id
         left join storekeeper on iobgoodsissuestore.storekeeperid = storekeeper.id
         left join IObGoodsIssueConsignee on DObGoodsIssue.id = IObGoodsIssueConsignee.refinstanceid
         left join IObGoodsIssueSalesInvoiceConsignee
                   on IObGoodsIssueConsignee.id = IObGoodsIssueSalesInvoiceConsignee.id
         left join DObSalesInvoice
                   on IObGoodsIssueSalesInvoiceConsignee.salesInvoiceId = DObSalesInvoice.id
         left join mobcustomer on IObGoodsIssueConsignee.customerid = mobcustomer.id
         left join MObBusinessPartner on mobcustomer.id = MObBusinessPartner.id
         left join masterdata m2 on MObBusinessPartner.id = m2.id
         left join iobcustomeraddress on mobcustomer.id = iobcustomeraddress.refinstanceid
         left join lobaddress on iobcustomeraddress.addressid = lobaddress.id
         left join IObCustomerContactPerson
                   on mobcustomer.id = IObCustomerContactPerson.refinstanceid
         left join LObContactPerson
                   on IObCustomerContactPerson.contactpersonid = LObContactPerson.id
         left join CObenterprise c5 on IObGoodsIssueConsignee.salesrepresentativeid = c5.id
         left join cobenterprise c6 on MObCustomer.kapid = c6.id
         left join iobalternativeuom
                   on IObGoodsIssueItem.itemId = iobalternativeuom.refinstanceid and
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         left join IObEnterpriseContact enterpriseTelephone
                   on cobcompany.id = enterpriseTelephone.refinstanceid and
                      enterpriseTelephone.objectTypeCode = '1' and
                      enterpriseTelephone.contactTypeId = 1
         left join IObEnterpriseContact enterpriseFax
                   on cobcompany.id = enterpriseFax.refinstanceid and
                      enterpriseFax.objectTypeCode = '1' and
                      enterpriseFax.contactTypeId = 2
         left join iobcompanylogodetails on cobcompany.id = iobcompanylogodetails.refinstanceid
         left join IObEnterpriseAddress on cobcompany.id = IObEnterpriseAddress.refinstanceid;
