
CREATE VIEW TObStoreTransactionGeneralModel AS
select id,
       code,
       originalAddingDate,
       updatingActualCostDate,
       transactionOperation,
       creationinfo,
       creationdate,
       modificationinfo,
       modifieddate,
       getEnterpriseCodeByIdAndType(companyid, '1')                       as companyCode,
       getEnterpriseNameByIdAndType(companyid, '1')                       as companyName,
       getEnterpriseCodeByIdAndType(plantid, '3')                         as plantCode,
       getEnterpriseNameByIdAndType(plantid, '3')                         as plantName,
       getEnterpriseCodeByIdAndType(storehouseid, '4')                    as storehouseCode,
       getEnterpriseNameByIdAndType(storehouseid, '4')                    as storehouseName,
       getEnterpriseCodeByIdAndType(purchaseunitid, '5')                  as purchaseUnitCode,
       getEnterpriseNameByIdAndType(purchaseunitid, '5')                  as purchaseUnitName,
       getEnterpriseNameByIdAndType(purchaseunitid, '5')::json ->> 'en'   as purchaseUnitNameEn,
       stocktype,
       batchNo,
       getItemCodeById(itemid)                                            as itemCode,
       getItemNameById(itemid)                                            as itemName,
       getUnitOfMeasureCodeById(unitofmeasureid)                          as unitOfMeasureCode,
       getUnitOfMeasureNameById(unitofmeasureid)                          as unitOfMeasureName,
       quantity,
       round(cast(cost as numeric), 4)                                    as cost,
       round(cast(transactioncost as numeric), 4)                         as transactioncost,
       getStoreTransactionRefDocumentCode(objectTypeCode, transaction.id) as refDocumentCode,
       getStoreTransactionRefDocumentType(objectTypeCode, transaction.id) as refDocumentType,
       remainingquantity,
       getRefStoreTransactionCode(reftransactionid)                       as refTransactionCode
from TObStoreTransaction transaction;


CREATE VIEW TObGoodsReceiptStoreTransactionGeneralModel AS
SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.originalAddingDate,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
              round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionOperation,
       c1.code              as companyCode,
       c1.name              as companyName,
       c2.code              as storehouseCode,
       c2.name              as storehouseName,
       c3.code              as plantCode,
       c3.name              as plantName,
       c4.code              as purchaseUnitCode,
       c4.name              as purchaseUnitName,
       cobmeasure.symbol    AS unitOfMeasureSymbol,
       cobmeasure.name      AS unitOfMeasureName,
       cobmeasure.code      AS unitOfMeasureCode,
       t2.code              as refTransactionCode,
       masterdata.name      AS itemName,
       masterdata.code      AS itemCode,
       DObGoodsReceipt.code AS goodsReceiptCode

FROM TObStoreTransaction
         left join TObGoodsReceiptStoreTransaction
                   on TObStoreTransaction.id = TObGoodsReceiptStoreTransaction.id
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left JOIN TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         JOIN mobitem on TObStoreTransaction.itemId = mobitem.id
         JOIN masterdata on mobitem.id = masterdata.id
         JOIN DObGoodsReceipt
              on TObGoodsReceiptStoreTransaction.documentReferenceId = DObGoodsReceipt.id;



CREATE VIEW TObGoodsIssueStoreTransactionGeneralModel AS
SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
              round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionoperation,
       c1.code            as companyCode,
       c1.name            as companyName,
       c2.code            as storehouseCode,
       c2.name            as storehouseName,
       c3.code            as plantCode,
       c3.name            as plantName,
       c4.code            as purchaseUnitCode,
       c4.name            as purchaseUnitName,
       cobmeasure.symbol  as unitOfMeasureSymbol,
       cobmeasure.name    as unitOfMeasureName,
       cobmeasure.code    as unitOfMeasureCode,
       t2.code            as refTransactionCode,
       masterdata.name    as itemName,
       masterdata.code    as itemCode,
       DObGoodsIssue.code as goodsIssueCode

FROM TObStoreTransaction
         left join TObGoodsIssueStoreTransaction
                   on TObStoreTransaction.id = TObGoodsIssueStoreTransaction.id
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         join mobitem on TObStoreTransaction.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         join DObGoodsIssue on TObGoodsIssueStoreTransaction.documentReferenceId = DObGoodsIssue.id;


CREATE VIEW TObGoodsReceiptStoreTransactionDataGeneralModel AS
SELECT row_number()
       OVER ()                                                       as id,
       DObGoodsReceipt.creationdate,
       DObGoodsReceipt.modifiedDate,
       DObGoodsReceipt.creationInfo,
       DObGoodsReceipt.modificationInfo,
       DObGoodsReceipt.code,
       DObGoodsReceipt.id                                            as goodsReceiptId,
       DObGoodsReceipt.storehouseid,
       DObGoodsReceipt.plantid,

       cobplant.companyid,

       IObGoodsReceiptPurchaseOrderData.purchaseunitid,

       IObGoodsReceiptRecievedItemsData.itemid,

       cobmeasure.symbol                                             as baseUnitSymbol,
       masterdata.code                                               as itemCode,
       masterdata.name                                               as itemName,
       (IObGoodsReceiptItemQuantities.receivedqtyuoe *
        alternative1.conversionfactor)                               AS receivedQtyBase,

       (iobgoodsreceiptitembatches.receivedqtyuoe *
        alternative2.conversionfactor)                               AS receivedBatchQtyBase,

       IObGoodsReceiptItemQuantities.estimatedcost                   AS estimatedCostQty,
       iobgoodsreceiptitembatches.estimatedcost                      AS estimatedCostBatch,

       IObGoodsReceiptItemQuantities.actualcost                      AS actualCostQty,
       iobgoodsreceiptitembatches.actualcost                         AS actualCostBatch,

       IObGoodsReceiptItemQuantities.receivedqtyuoe                  as receivedQty,
       IObGoodsReceiptItemQuantities.unitofentryid                   as receivedUoeId,
       IObGoodsReceiptItemQuantities.stocktype as receivedStockType,


       IObGoodsReceiptItemBatches.batchcode                          AS batchNo,
       IObGoodsReceiptItemBatches.receivedqtyuoe                     as batchQty,
       IObGoodsReceiptItemBatches.unitofentryid                      as batchUoeId,
       IObGoodsReceiptItemBatches.stocktype  as batchStockType

from DObGoodsReceipt
         left join cobplant on DObGoodsReceipt.plantid = cobplant.id
         left join IObGoodsReceiptPurchaseOrderData
                   on DObGoodsReceipt.id = IObGoodsReceiptPurchaseOrderData.refinstanceid
         left join IObGoodsReceiptRecievedItemsData
                   on DObGoodsReceipt.id = IObGoodsReceiptRecievedItemsData.refinstanceid
         left join IObGoodsReceiptItemQuantities
                   on IObGoodsReceiptRecievedItemsData.id =
                      IObGoodsReceiptItemQuantities.refinstanceid

         left join IObGoodsReceiptItemBatches
                   on IObGoodsReceiptRecievedItemsData.id = IObGoodsReceiptItemBatches.refinstanceid
         LEFT JOIN mobitem on IObGoodsReceiptRecievedItemsData.itemid = mobitem.id
         LEFT JOIN iobalternativeuom alternative2 ON mobitem.id = alternative2.refinstanceid and
                                                     alternative2.alternativeunitofmeasureid =
                                                     iobgoodsreceiptitembatches.unitofentryid
         LEFT JOIN iobalternativeuom alternative1 ON mobitem.id = alternative1.refinstanceid and
                                                     alternative1.alternativeunitofmeasureid =
                                                     IObGoodsReceiptItemQuantities.unitofentryid
         LEFT JOIN masterdata on mobitem.id = masterdata.id
         LEFT join cobmeasure on cobmeasure.id = mobitem.basicunitofmeasure;


CREATE VIEW TObStoreTransactionAllDataGeneralModel AS
    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           DObGoodsIssue.code                 as documentCode

    FROM TObStoreTransaction
             left join TObGoodsIssueStoreTransaction
                       on TObStoreTransaction.id = TObGoodsIssueStoreTransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             join DObGoodsIssue
                  on TObGoodsIssueStoreTransaction.documentReferenceId = DObGoodsIssue.id

    union

    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           DObGoodsReceipt.code               as documentCode

    FROM TObStoreTransaction
             left join TObGoodsReceiptStoreTransaction
                       on TObStoreTransaction.id = TObGoodsReceiptStoreTransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             JOIN DObGoodsReceipt
                  on TObGoodsReceiptStoreTransaction.documentReferenceId = DObGoodsReceipt.id

    union

    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.cost as numeric), 4) as cost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.transactionCost as numeric), 4) as transactionCost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           dobstocktransformation.code               as documentCode

    FROM TObStoreTransaction
             left join tobstocktransformationstoretransaction
                       on TObStoreTransaction.id = tobstocktransformationstoretransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             JOIN dobstocktransformation
                  on tobstocktransformationstoretransaction.documentReferenceId = dobstocktransformation.id;


--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------

CREATE or REPLACE FUNCTION getStoreTransactionRefDocumentType(objectTypeCode varchar, transactionId bigint)
    RETURNS json AS
$$
BEGIN
    RETURN case
               when objecttypecode = 'GR' then '{"en":"Goods Receipt","ar": "إذن إضافة"}'
               when objecttypecode = 'GI' then '{"en":"Goods Issue","ar": "اذون صرف البضاعة"}'
               when objecttypecode = 'STK_TR' then '{"en":"Stock Transformation","ar": "تحويل المخزون"}'
               end;
END;
$$
    LANGUAGE PLPGSQL;




CREATE or REPLACE FUNCTION getStoreTransactionRefDocumentCode(objectTypeCode varchar, transactionId bigint)
    RETURNS varchar AS
$$
BEGIN
    RETURN case
               when objecttypecode = 'GR' and (select documentreferenceid
                                               from tobgoodsreceiptstoretransaction gr
                                               where gr.id = transactionId) is null then 'Legacy System نظام المتكامل'
               when objecttypecode = 'GR' then
                   (select code
                    from dobgoodsreceipt
                    where id = (select documentreferenceid
                                from tobgoodsreceiptstoretransaction gr
                                where gr.id = transactionId))

               when objecttypecode = 'GI' then
                   (select code
                    from dobgoodsissue
                    where id = (select documentreferenceid
                                from TObGoodsIssueStoreTransaction gi
                                where gi.id = transactionId))

               when objecttypecode = 'STK_TR' then
                   (select code
                    from dobstocktransformation
                    where id = (select documentreferenceid
                                from TObstocktransformationStoreTransaction gi
                                where gi.id = transactionId))

        end;
END;
$$
    LANGUAGE PLPGSQL;