drop view if exists TObStoreTransactionGeneralModel;
drop view if exists TObStoreTransactionAllDataGeneralModel;
drop view if exists TObGoodsReceiptStoreTransactionGeneralModel;
drop view if exists TObGoodsIssueStoreTransactionGeneralModel;
drop view if exists TObGoodsReceiptStoreTransactionDataGeneralModel;

ALTER TABLE TObStoreTransaction RENAME COLUMN objectTypeCode TO transactionOperation;
ALTER TABLE TObStoreTransaction ADD COLUMN objectTypeCode VARCHAR(1024);
ALTER TABLE TObStoreTransaction ADD COLUMN originalAddingDate TIMESTAMP With time zone;
ALTER TABLE TObStoreTransaction ALTER COLUMN creationDate TYPE TIMESTAMP With time zone;
ALTER TABLE TObStoreTransaction ALTER COLUMN modifiedDate TYPE TIMESTAMP With time zone;
ALTER TABLE TObStoreTransaction ADD COLUMN updatingActualCostDate TIMESTAMP With time zone;

ALTER TABLE TObGoodsReceiptStoreTransaction ALTER COLUMN documentReferenceId DROP NOT NULL;

CREATE TABLE TObStockTransformationStoreTransaction
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE TObStockTransformationStoreTransaction
    ADD CONSTRAINT FK_TObStockTransformationStoreTransaction_DObStoreTransaction FOREIGN KEY (id) REFERENCES TObStoreTransaction (id) ON DELETE RESTRICT;

ALTER TABLE TObStockTransformationStoreTransaction
    ADD CONSTRAINT FK_TObStockTransformationStoreTransaction_DObStockTransformation FOREIGN KEY (documentReferenceId)
        REFERENCES DObStockTransformation (id) ON DELETE RESTRICT;

ALTER TABLE tobstoretransaction alter column objectTypeCode set NOT NULL;