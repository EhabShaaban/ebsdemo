drop table if exists CObStockTransformationType;
drop table if exists CObStockTransformationReason;
drop table if exists DObStockTransformation;
drop table if exists IObStockTransformationDetails;
drop table if exists IObStockTransformationPostingDetails;

create TABLE CObStockTransformationType
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024)                    ,
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

create TABLE CObStockTransformationReason
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024)                    ,
    name             JSON                     NOT NULL,
    typeId           INT8                     NOT NULL,
    fromStockType    VARCHAR(1024)            NOT NULL,
    toStockType      VARCHAR(1024)            NOT NULL,
    PRIMARY KEY (id)
);

alter table CObStockTransformationReason
    add CONSTRAINT FK_CObStockTransformationType_CObStockTransformationReason
        FOREIGN KEY (typeId) REFERENCES CObStockTransformationType (id) ON delete RESTRICT;

create TABLE DObStockTransformation
(
    id                      BIGSERIAL                NOT NULL,
    code                    VARCHAR(1024)            NOT NULL,
    creationDate            TIMESTAMP With time zone NOT NULL,
    modifiedDate            TIMESTAMP With time zone NOT NULL,
    creationInfo            VARCHAR(1024)            NOT NULL,
    modificationInfo        VARCHAR(1024)            NOT NULL,
    currentStates           VARCHAR(1024)            NOT NULL,
    purchaseUnitId          INT8                     NOT NULL,
    transformationTypeId    INT8                     NOT NULL,
    PRIMARY KEY (id)
);

alter table DObStockTransformation
    add CONSTRAINT FK_CObStockTransformationType_DObStockTransformation
        FOREIGN KEY (transformationTypeId) REFERENCES CObStockTransformationType (id) ON delete RESTRICT;

alter table DObStockTransformation
    add CONSTRAINT FK_CObEnterprise_DObStockTransformation
        FOREIGN KEY (purchaseUnitId) REFERENCES CObEnterprise (id) ON delete RESTRICT;

create TABLE IObStockTransformationDetails
(
    id                     BIGSERIAL                NOT NULL,
    refInstanceId          INT8                     NOT NULL,
    creationDate           TIMESTAMP With time zone NOT NULL,
    modifiedDate           TIMESTAMP With time zone NOT NULL,
    creationInfo           VARCHAR(1024)            NOT NULL,
    modificationInfo       VARCHAR(1024)            NOT NULL,
    newStockType           VARCHAR(1024)            NOT NULL,
    transformedQty         NUMERIC                  NOT NULL,
    storeTransactionId     INT8                     NOT NULL,
    newstoreTransactionId  INT8                             ,
    transformationReasonId INT8                     NOT NULL,
    PRIMARY KEY (id)
);


alter table IObStockTransformationDetails
    add CONSTRAINT FK_DObStockTransformation_IObStockTransformationDetails
        FOREIGN KEY (refInstanceId) REFERENCES DObStockTransformation (id) ON delete CASCADE;

alter table IObStockTransformationDetails
    add CONSTRAINT FK_storeTransactionId_IObStockTransformationDetails
        FOREIGN KEY (storeTransactionId) REFERENCES TObStoreTransaction (id) ON delete RESTRICT;

alter table IObStockTransformationDetails
    add CONSTRAINT FK_newStoreTransactionId_IObStockTransformationDetails
        FOREIGN KEY (newstoreTransactionId) REFERENCES TObStoreTransaction (id) ON delete RESTRICT;

alter table IObStockTransformationDetails
    add CONSTRAINT FK_CObStockTransformationReason_IObStockTransformationDetails
        FOREIGN KEY (transformationReasonId) REFERENCES CObStockTransformationReason (id) ON delete RESTRICT;


create TABLE IObStockTransformationPostingDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    postedBy         VARCHAR(1024)            NOT NULL,
    postingDate      TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (id)
);

alter table IObStockTransformationPostingDetails
    add CONSTRAINT FK_IObStockTransformationPostingDetails_DObStockTransformation FOREIGN KEY (refInstanceId) REFERENCES dobstocktransformation (id) ON delete CASCADE;


