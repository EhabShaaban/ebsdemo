drop view if exists DObStockTransformationGeneralModel;

CREATE VIEW DObStockTransformationGeneralModel
AS SELECT
       DObStockTransformation.id                                              ,
       DObStockTransformation.code                                            ,
       DObStockTransformation.creationDate                                    ,
       DObStockTransformation.modifiedDate                                    ,
       DObStockTransformation.creationInfo                                    ,
       DObStockTransformation.modificationInfo                                ,
       DObStockTransformation.currentStates                                   ,
       IObStockTransformationDetails.newStockType                             ,
       IObStockTransformationDetails.transformedQty                           ,
       CObEnterprise.code                            as purchaseUnitCode      ,
       CObEnterprise.name                            as purchaseUnitname      ,
       CObEnterprise.name :: json ->> 'en'           as purchaseUnitNameEn    ,
       refStoreTransaction.code                      as refstoreTransactionCode      ,
       newStoreTransaction.Code                      as newstoreTransactionCode      ,
       type.code                                     as stockTransformationTypeCode  ,
       type.name                                     as stockTransformationTypeName  ,
       reason.code                                   as transformationReasonCode,
       reason.name                                   as transformationReasonName

   FROM DObStockTransformation
            LEFT JOIN IObStockTransformationDetails ON DObStockTransformation.id = IObStockTransformationDetails.refInstanceId
            LEFT JOIN CObEnterprise ON DObStockTransformation.purchaseUnitId = CObEnterprise.id
            LEFT JOIN CObStockTransformationType type ON DObStockTransformation.transformationTypeId = type.id
            LEFT JOIN CObStockTransformationReason reason ON IObStockTransformationDetails.transformationReasonId = reason.id
            LEFT JOIN TObStoreTransaction refStoreTransaction ON IObStockTransformationDetails.storeTransactionId = refStoreTransaction.id
            LEFT JOIN TObStoreTransaction newStoreTransaction ON IObStockTransformationDetails.newstoreTransactionId = newStoreTransaction.id;
