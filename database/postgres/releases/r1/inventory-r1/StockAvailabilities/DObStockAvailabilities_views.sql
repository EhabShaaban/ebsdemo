drop view if exists DObStockAvailabilitiesGeneralModel;

CREATE VIEW DObStockAvailabilitiesGeneralModel AS
select
       row_number() over () as id,
       grouped.companyCode,
       c1.name                as companyName,
       grouped.plantCode,
       c2.name                as plantName,
       grouped.storehouseCode,
       c3.name                as storehouseName,
       grouped.purchaseUnitCode,
       c4.name                as purchaseUnitName,
       c4.name::json ->> 'en' as purchaseUnitNameEn,
       grouped.stocktype,
       grouped.itemCode,
       m1.name                as itemName,
       grouped.unitOfMeasureCode,
       cm1.symbol             as unitOfMeasureName,
       grouped.totalQty
from (select c1.code                                    as companyCode,
             c2.code                                    as plantCode,
             c3.code                                    as storehouseCode,
             c4.code                                    as purchaseUnitCode,
             TObStoreTransaction.stocktype,
             m1.code                                    as itemCode,
             cm1.code                                   as unitOfMeasureCode,
             sum(TObStoreTransaction.remainingquantity) as totalQty
      from TObStoreTransaction
               join cobenterprise c1 on TObStoreTransaction.companyId = c1.id and c1.objecttypecode = '1'
               left join cobenterprise c2 on TObStoreTransaction.plantid = c2.id and c2.objecttypecode = '3'
               left join cobenterprise c3 on TObStoreTransaction.storehouseid = c3.id and c3.objecttypecode = '4'
               left join cobenterprise c4 on TObStoreTransaction.purchaseunitid = c4.id and c4.objecttypecode = '5'
               left join masterdata m1 on TObStoreTransaction.itemid = m1.id and m1.objecttypecode = '1'
               left join cobmeasure cm1 on TObStoreTransaction.unitofmeasureid = cm1.id
      group by itemcode,
               unitofmeasurecode,
               purchaseunitcode,
               storehousecode, companycode, plantcode, stocktype) grouped
         left join cobenterprise c1 on c1.code = grouped.companyCode and c1.objecttypecode = '1'
         left join cobenterprise c2 on c2.code = grouped.plantCode and c2.objecttypecode = '3'
         left join cobenterprise c3 on c3.code = grouped.storehouseCode and c3.objecttypecode = '4'
         left join cobenterprise c4 on c4.code = grouped.purchaseUnitCode and c4.objecttypecode = '5'
         left join masterdata m1 on grouped.itemCode = m1.code and m1.objecttypecode = '1'
         left join cobmeasure cm1 on grouped.unitOfMeasureCode = cm1.code



