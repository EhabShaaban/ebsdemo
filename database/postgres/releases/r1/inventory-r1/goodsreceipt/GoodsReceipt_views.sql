CREATE VIEW IObGoodsReceiptRecievedItemsGeneralModel AS
SELECT IObGoodsReceiptRecievedItemsData.id,
       IObGoodsReceiptRecievedItemsData.refinstanceid AS goodsreceiptinstanceid,
       IObGoodsReceiptRecievedItemsData.differencereason,
       IObGoodsReceiptRecievedItemsData.objecttypecode,
       mobitemgeneralmodel.code                       as itemcode,
       mobitemgeneralmodel.name                       as itemname,
       DObGoodsReceipt.code                           as goodsReceiptCode,
       cobmeasure.symbol                              as baseunit,
       cobmeasure.code                                as baseunitcode,
       (SELECT quantityInDn
        FROM IObOrderLineDetailsGeneralModel
                 left join iobgoodsreceiptpurchaseordergeneralmodel
                           ON iobgoodsreceiptpurchaseordergeneralmodel.purchaseOrderCode =
                              IObOrderLineDetailsGeneralModel.orderCode
        WHERE iobgoodsreceiptpurchaseordergeneralmodel.goodsReceiptCode = DObGoodsReceipt.code
          AND IObOrderLineDetailsGeneralModel.itemCode = mobitemgeneralmodel.code
       )                                              AS quantityInDn
FROM IObGoodsReceiptRecievedItemsData
         JOIN DObGoodsReceipt ON IObGoodsReceiptRecievedItemsData.refinstanceId = DObGoodsReceipt.id
         left join mobitemgeneralmodel
                   on IObGoodsReceiptRecievedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode;

CREATE VIEW IObGoodsReceiptOrdinaryRecievedItemGeneralModel AS
SELECT IObGoodsReceiptItemQuantities.id,
       IObGoodsReceiptItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptItemQuantities.receivedqtyuoe,
       IObGoodsReceiptItemQuantities.stocktype,
       IObGoodsReceiptItemQuantities.defectDescription,
       IObGoodsReceiptItemQuantities.estimatedCost,
       IObGoodsReceiptItemQuantities.actualCost,
       cobmeasure.symbol                           AS unitofentry,
       cobmeasure.code                             AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptItemQuantities.unitofentryid
                     THEN IObGoodsReceiptItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                    AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptItemQuantities.unitofentryid
                     THEN (IObGoodsReceiptItemQuantities.receivedqtyuoe )
                     * iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                    AS receivedqtyWithDefectsbase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                    AS baseunitfactor,
       mobitemgeneralmodel.code                    as itemCode,
       mobitemgeneralmodel.name                    as itemName,
       dobgoodsreceipt.code                        AS goodsreceiptcode,
       (SELECT iobgoodsreceiptrecieveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptrecieveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptrecieveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptrecieveditemsgeneralmodel.goodsreceiptcode =
              dobgoodsreceipt.code)                AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseordergeneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseordergeneralmodel.goodsReceiptCode = DObGoodsReceipt.code
       )                                           AS itemCodeAtVendor

FROM IObGoodsReceiptItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptRecievedItemsData
              ON IObGoodsReceiptRecievedItemsData.id = IObGoodsReceiptItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptRecievedItemsData.itemid = mobitemgeneralmodel.id
         JOIN dobgoodsreceipt ON dobgoodsreceipt.id = iobgoodsreceiptrecieveditemsdata.refinstanceid
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = IObGoodsReceiptItemQuantities.unitofentryid;


CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS

SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.stocktype,
       iobgoodsreceiptitembatches.defectDescription,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE iobgoodsreceiptitembatches.receivedqtyuoe
                 END ::numeric,
             3)                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobgoodsreceipt.code                     as goodsReceiptCode,
       (SELECT baseUnit
        FROM IObGoodsReceiptRecievedItemsGeneralModel
        WHERE mobitemgeneralmodel.code =
              IObGoodsReceiptRecievedItemsGeneralModel.itemCode
          AND iobgoodsreceiptrecieveditemsgeneralmodel.goodsreceiptcode =
              dobgoodsreceipt.code)             AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseordergeneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseordergeneralmodel.goodsReceiptCode = DObGoodsReceipt.code
       )                                        AS itemCodeAtVendor

FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptRecievedItemsData
              ON IObGoodsReceiptRecievedItemsData.id = iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptRecievedItemsData.itemid = mobitemgeneralmodel.id
         JOIN dobgoodsreceipt ON dobgoodsreceipt.id = iobgoodsreceiptrecieveditemsdata.refinstanceid
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;