﻿DROP VIEW IF EXISTS IObGoodsReceiptRecievedItemsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObGoodsReceiptRecievedItemsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObGoodsReceiptOrdinaryRecievedItemGeneralModel CASCADE;
DROP VIEW IF EXISTS TObGoodsReceiptStoreTransactionDataGeneralModel CASCADE;
DROP VIEW IF EXISTS IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel CASCADE;

Alter table IObGoodsReceiptRecievedItemsData drop stockType;
ALTER TABLE IObGoodsReceiptItemQuantities drop defectsQty;
ALTER TABLE IObGoodsReceiptItemBatches drop defectsQty;

ALTER TABLE IObGoodsReceiptItemQuantities RENAME COLUMN defectsStockType TO stockType;
ALTER TABLE IObGoodsReceiptItemBatches RENAME COLUMN defectsStockType TO stockType;
