DROP VIEW IF EXISTS userPermissionGeneralModel CASCADE;
DROP TABLE IF EXISTS userauthenticator CASCADE;

ALTER SEQUENCE IF EXISTS lobgeolocaleseq RENAME TO lobgeolocale_id_seq;
ALTER SEQUENCE IF EXISTS cobgeolocaleseq RENAME TO cobgeolocale_id_seq;
ALTER SEQUENCE IF EXISTS iobuomconversionseq RENAME TO iobuomconversion_id_seq;
ALTER SEQUENCE IF EXISTS localizedlobmeasureseq RENAME TO localizedlobmeasure_id_seq;
ALTER SEQUENCE IF EXISTS cobmeasureseq RENAME TO cobmeasure_id_seq;
ALTER SEQUENCE IF EXISTS localizedlobgeolocaleseq RENAME TO localizedlobgeolocale_id_seq;
ALTER SEQUENCE IF EXISTS robattachmentseq RENAME TO robattachment_id_seq;
ALTER SEQUENCE IF EXISTS iobitempurchaseunitseq RENAME TO iobitempurchaseunit_id_seq;
ALTER SEQUENCE IF EXISTS iobvendorpurchaseunitseq RENAME TO iobvendorpurchaseunit_id_seq;
ALTER SEQUENCE IF EXISTS iobalternativeuomseq RENAME TO iobalternativeuom_id_seq;
ALTER SEQUENCE IF EXISTS cobcontrolpointseq RENAME TO cobcontrolpoint_id_seq;
ALTER SEQUENCE IF EXISTS iobcontrolpointusersseq RENAME TO iobcontrolpointusers_id_seq;
ALTER SEQUENCE IF EXISTS objectconditionfieldsseq RENAME TO objectconditionfields_id_seq;
ALTER SEQUENCE IF EXISTS ioborderattachmentsequence RENAME TO ioborderattachment_id_seq;
ALTER SEQUENCE IF EXISTS iobinvoicedetailsseq RENAME TO iobinvoicedetails_id_seq;
ALTER SEQUENCE IF EXISTS iobinvoicedetailsdownpaymentseq RENAME TO iobinvoicedetailsdownpayment_id_seq;
ALTER SEQUENCE IF EXISTS iobitemaccountinginfoseq RENAME TO iobitemaccountinginfo_id_seq;
ALTER SEQUENCE IF EXISTS iobvendoraccountinginfoseq RENAME TO iobvendoraccountinginfo_id_seq;

DROP SEQUENCE IF EXISTS iobenterpriseaddressseq;
DROP SEQUENCE IF EXISTS iobenterprisebasicdataseq;
DROP SEQUENCE IF EXISTS iobenterprisecontactse;
DROP SEQUENCE IF EXISTS cobmaterialsequence;
DROP SEQUENCE IF EXISTS iobmaterialallowedviewsequence;
DROP SEQUENCE IF EXISTS iobmaterialrestrictionsequence;
DROP SEQUENCE IF EXISTS localizedlobmaterialsequence;
DROP SEQUENCE IF EXISTS lobattachmenttypesequence;
DROP SEQUENCE IF EXISTS sequence_lobsimplematerialdata;
DROP SEQUENCE IF EXISTS iobsistandarduomseq;
DROP SEQUENCE IF EXISTS iobqualityupdatingsequence;
DROP SEQUENCE IF EXISTS iobenterprisecontactseq;

CREATE VIEW userPermissionGeneralModel AS
  SELECT row_number() OVER ()   as id,
       permission.permissionexpression,
       permission.objectname,
       ebsuser.username,
       permissionassignment.authorizationconditionid
   FROM permission
       JOIN permissionassignment ON permission.id = permissionassignment.permissionid
       FULL OUTER JOIN authorizationcondition ON permissionassignment.authorizationconditionid = authorizationcondition.id
       JOIN roleassignment ON roleassignment.roleid = permissionassignment.roleid
       JOIN userassignment ON userassignment.roleid = roleassignment.parentroleid
       JOIN role ON role.id = userassignment.roleid
       JOIN useraccount ON useraccount.id = userassignment.userid
       JOIN ebsuser ON ebsuser.id = useraccount.userid;

CREATE TABLE userauthenticator (
    id bigint NOT NULL,
    accountid bigint NOT NULL,
    authenticatorid bigint NOT NULL
);