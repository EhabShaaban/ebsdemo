DO $$
    DECLARE i TEXT;
    DECLARE tableName TEXT;
    DECLARE stat TEXT;
    BEGIN
        FOR i, tableName IN (SELECT sequence_name , split_part(sequence_name, '_', 1)
                  FROM information_schema.sequences
                  WHERE sequence_catalog = 'ebdk_core_v2'
                    AND sequence_schema = 'public'
                    AND data_type = 'bigint'
                    AND sequence_name not in ('approvalcyclenumsequence')
            ) LOOP
            stat = concat('(SELECT MAX(id) FROM ', tablename, ')');
            EXECUTE 'Select setval('''||i||''', '|| stat ||')';
        END LOOP;
    END$$;