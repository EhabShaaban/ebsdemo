drop view if exists ItemsLastSalesPrice;


create view ItemsLastSalesPriceGeneralModel as
select iobsalesinvoiceitem.id,
       dobsalesinvoice.code                                                 as salesInvoiceCode,
       iobsalesinvoicepostingdetails.creationdate                           as salesInvoicePostingDate,

       uom.code                                                             as uomCode,
       concat(uom.code, ' - ', uom.name :: json ->> 'en')                   as uomCodeName,

       item.code                                                            as itemCode,
       concat(item.code, ' - ', item.name :: json ->> 'en')                 as itemCodeName,

       customer.code                                                        as customerCode,
       concat(customer.code, ' - ', customer.name :: json ->> 'en')         as customerCodeName,

       purchaseUnit.code                                                    as purchaseUnitCode,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name :: json ->> 'en') as purchaseUnitCodeName,

       iobsalesinvoiceitem.salesprice
from iobsalesinvoiceitem
         LEFT JOIN dobsalesinvoice on iobsalesinvoiceitem.refinstanceid = dobsalesinvoice.id AND
                                      dobsalesinvoice.currentstates like '%Active%'
         LEFT JOIN masterdata item on iobsalesinvoiceitem.itemid = item.id and item.objecttypecode = '1'
         left join cobmeasure uom on uom.id = iobsalesinvoiceitem.orderunitid
         left join cobenterprise company on company.id = dobsalesinvoice.companyid and company.objecttypecode = '1'
         left join cobenterprise purchaseUnit
                   on purchaseUnit.id = dobsalesinvoice.purchaseunitid and purchaseUnit.objecttypecode = '5'
         left join iobsalesinvoicebusinesspartner on iobsalesinvoicebusinesspartner.refinstanceid = dobsalesinvoice.id
         join iobsalesinvoiceCustomerbusinesspartner
              on iobsalesinvoiceCustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         join masterdata customer
              on customer.id = iobsalesinvoiceCustomerbusinesspartner.customerid and customer.objecttypecode = '3'
         left join iobsalesinvoicepostingdetails on dobsalesinvoice.id = iobsalesinvoicepostingdetails.refinstanceid;
