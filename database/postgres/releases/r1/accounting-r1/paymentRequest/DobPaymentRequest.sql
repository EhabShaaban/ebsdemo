drop table if exists IObPaymentRequestApprovalCycle;
drop table if exists IObPaymentRequestApprover;

ALTER TABLE IObPaymentRequestInvoicePaymentDetails DROP CONSTRAINT FK_iobpaymentrequestInvoicepaymentdetails_iobpaymentrequestpaymentdetails;

ALTER TABLE IObPaymentRequestInvoicePaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestInvoicepaymentdetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE CASCADE;


ALTER TABLE IObPaymentRequestDownPaymentPaymentDetails DROP CONSTRAINT FK_iobpaymentrequestDownPaymentpaymentdetails_iobpaymentrequestpaymentdetails;

ALTER TABLE IObPaymentRequestDownPaymentPaymentDetails
    ADD CONSTRAINT FK_iobpaymentrequestDownPaymentpaymentdetails_iobpaymentrequestpaymentdetails FOREIGN KEY (id) REFERENCES IObPaymentRequestPaymentDetails (id) ON DELETE CASCADE;


ALTER TABLE IObInvoiceDetailsDownPayment DROP CONSTRAINT FK_IObInvoiceDetailsDownPayment_downpayment;

ALTER TABLE IObInvoiceDetailsDownPayment
    ADD CONSTRAINT FK_IObInvoiceDetailsDownPayment_downpayment FOREIGN KEY (DownPaymentid) REFERENCES DObPaymentRequest (id) ON DELETE CASCADE;




-------------------------------------------------


CREATE TABLE IObPaymentRequestApprovalCycle (
  id                 BIGSERIAL     NOT NULL,
  refInstanceId      INT8          NOT NULL REFERENCES dobpaymentrequest (id) ON DELETE cascade ,
  creationDate       TIMESTAMP WITH TIME ZONE   NOT NULL,
  modifiedDate       TIMESTAMP WITH TIME ZONE   NOT NULL,
  creationInfo       VARCHAR(1024) NOT NULL,
  modificationInfo   VARCHAR(1024) NOT NULL,
  approvalCycleNum   bigint        NOT NULL DEFAULT nextval('approvalCycleNumSequence' :: regclass),
  startDate          TIMESTAMP WITH TIME ZONE  NOT NULL,
  endDate            TIMESTAMP WITH TIME ZONE ,
  finalDecision      VARCHAR(10) ,
  PRIMARY KEY (id)
);

CREATE TABLE IObPaymentRequestApprover (
  id                 BIGSERIAL     NOT NULL,
  refInstanceId      INT8          NOT NULL REFERENCES IObPaymentRequestApprovalCycle (id) ON DELETE cascade ,
  creationDate       TIMESTAMP WITH TIME ZONE  NOT NULL,
  modifiedDate       TIMESTAMP WITH TIME ZONE  NOT NULL,
  creationInfo       VARCHAR(1024) NOT NULL,
  modificationInfo   VARCHAR(1024) NOT NULL,
  controlPointId     INT8          NOT NULL REFERENCES cobcontrolpoint (id) ON DELETE RESTRICT,
  approverId         INT8          NOT NULL REFERENCES ebsuser (id) ON DELETE RESTRICT,
  decisionDatetime   TIMESTAMP WITH TIME ZONE,
  notes              VARCHAR(2048),
  decision           VARCHAR(10) ,
  PRIMARY KEY (id)
);