DROP VIEW IF EXISTS IObPaymentRequestApproverGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestApprovalCycleGeneralModel;

CREATE VIEW IObPaymentRequestApprovalCycleGeneralModel AS
SELECT iobpaymentrequestapprovalcycle.id,
       dobpaymentrequest.code        as paymentRequestCode,
       iobpaymentrequestapprovalcycle.refinstanceid,
       iobpaymentrequestapprovalcycle.creationDate,
       iobpaymentrequestapprovalcycle.creationInfo,
       iobpaymentrequestapprovalcycle.modificationInfo,
       iobpaymentrequestapprovalcycle.modifieddate,
       iobpaymentrequestapprovalcycle.approvalCycleNum,
       iobpaymentrequestapprovalcycle.startDate,
       iobpaymentrequestapprovalcycle.endDate,
       iobpaymentrequestapprovalcycle.finalDecision
FROM iobpaymentrequestapprovalcycle
         LEFT JOIN dobpaymentrequest ON dobpaymentrequest.id = iobpaymentrequestapprovalcycle.refInstanceId;




CREATE VIEW IObPaymentRequestApproverGeneralModel AS
SELECT iobpaymentrequestapprover.id ,
       iobpaymentrequestapprover.refinstanceid,
       iobpaymentrequestapprover.creationDate,
       iobpaymentrequestapprover.creationInfo,
       iobpaymentrequestapprover.modificationInfo,
       iobpaymentrequestapprover.modifieddate,
       dobpaymentrequest.code               as paymentRequestCode,
       iobpaymentrequestapprovalcycle.id    as approvalCycleId,
       iobpaymentrequestapprovalcycle.approvalCycleNum,
       cobcontrolpoint.name        as controlPointName,
       cobcontrolpoint.code        as controlPointCode,
       iobcontrolpointusers.ismain as isMainApprover,
       UserInfo.name               as approverName,
       iobpaymentrequestapprover.decisionDatetime,
       iobpaymentrequestapprover.decision,
       iobpaymentrequestapprover.notes
FROM iobpaymentrequestapprovalcycle
         LEFT JOIN dobpaymentrequest ON dobpaymentrequest.id = iobpaymentrequestapprovalcycle.refInstanceId
         LEFT JOIN iobpaymentrequestapprover ON iobpaymentrequestapprovalcycle.id = iobpaymentrequestapprover.refInstanceId
         LEFT JOIN cobcontrolpoint ON iobpaymentrequestapprover.controlPointId = cobcontrolpoint.id
         LEFT JOIN iobcontrolpointusers
                   ON iobcontrolpointusers.refinstanceid = cobcontrolpoint.id AND
                      iobcontrolpointusers.userid = iobpaymentrequestapprover.approverId
         LEFT JOIN UserInfo ON UserInfo.id = iobpaymentrequestapprover.approverId
ORDER BY iobpaymentrequestapprover.id ASC;

