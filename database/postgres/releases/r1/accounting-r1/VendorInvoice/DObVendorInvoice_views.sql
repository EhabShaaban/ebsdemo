DROP VIEW IF EXISTS IObInvoiceItemsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObInvoiceRemainingGeneralModel CASCADE;
DROP VIEW IF EXISTS IObInvoiceSummaryGeneralModel CASCADE;

CREATE VIEW IObInvoiceItemsGeneralModel AS
SELECT IObInvoiceItem.id,
       dobinvoicegeneralmodel.code,
       (SELECT code
        FROM mobitemgeneralmodel
        WHERE mobitemgeneralmodel.id = IObInvoiceItem.itemid)      AS itemCode,
       (SELECT name
        FROM mobitemgeneralmodel
        WHERE mobitemgeneralmodel.id = IObInvoiceItem.itemid)      AS itemName,
       (SELECT basicunitofmeasurecode
        FROM mobitemgeneralmodel
        WHERE mobitemgeneralmodel.id = IObInvoiceItem.itemid)      AS baseUnitCode,
       (SELECT cobmeasure.symbol
        FROM cobmeasure
        WHERE cobmeasure.id = IObInvoiceItem.baseunitofmeasureid)  AS baseUnitSymbol,
       (SELECT cobmeasure.code
        FROM cobmeasure
        WHERE cobmeasure.id = IObInvoiceItem.orderunitofmeasureid) AS orderUnitCode,
       (SELECT cobmeasure.symbol
        FROM cobmeasure
        WHERE cobmeasure.id = IObInvoiceItem.orderunitofmeasureid) AS orderUnitSymbol,
       (SELECT CASE
                   WHEN (IObInvoiceItem.baseunitofmeasureid =
                         IObInvoiceItem.orderunitofmeasureid)
                       THEN 1
                   ELSE (SELECT iobalternativeuom.conversionfactor
                         FROM iobalternativeuom
                         WHERE IObInvoiceItem.itemid = iobalternativeuom.refinstanceid
                           AND IObInvoiceItem.orderunitofmeasureid =
                               iobalternativeuom.alternativeunitofmeasureid
                   ) END AS conversionFactorToBase
       ),
       IObInvoiceItem.creationDate,
       IObInvoiceItem.modifiedDate,
       IObInvoiceItem.creationInfo,
       IObInvoiceItem.modificationInfo,
       IObInvoiceItem.quantityinorderunit,
       IObInvoiceItem.price
FROM dobinvoicegeneralmodel
         JOIN IObInvoiceItem ON dobinvoicegeneralmodel.id = IObInvoiceItem.refInstanceId;


CREATE VIEW IObInvoiceSummaryGeneralModel AS
SELECT dobInvoice.*,
       invoiceDetails.tax,
       (SELECT SUM((invoiceItem.price) * (invoiceItem.quantityinorderunit * invoiceItem.conversionfactortobase)) AS totalAmount
        from iobinvoiceitemsgeneralmodel invoiceItem WHERE invoiceItem.code = dobInvoice.code) AS totalAmount,

       (SELECT SUM(paymentDetails.netamount) AS amountPaid
        FROM iobpaymentrequestinvoicepaymentdetails invoicepaymentDetails
                    join iobpaymentrequestpaymentdetails paymentDetails on paymentDetails.id = invoicepaymentDetails.id
                    JOIN dobpaymentrequest paymentRequest ON paymentDetails.refinstanceid = paymentRequest.id AND paymentRequest.currentstates like '%PaymentDone%'
                    JOIN dobinvoice invoice ON invoicepaymentDetails.duedocumentid = invoice.id AND invoice.code = dobInvoice.code) AS amountPaid,

       (SELECT SUM(downpaymentamount) AS downPayment
        FROM iobinvoicedetailsdownpayment invoiceDownPayment
                    WHERE invoiceDownPayment.refinstanceid = invoiceDetails.id
       AND invoiceDetails.refinstanceid = dobInvoice.id
       ) AS downPayment
FROM dobinvoice dobInvoice LEFT JOIN iobinvoicedetails invoiceDetails ON dobInvoice.id = invoiceDetails.refinstanceid;


CREATE VIEW IObInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       (CASE WHEN invoiceSummary.totalAmount IS NULL THEN 0 ELSE invoiceSummary.totalAmount + invoiceSummary.totalAmount * invoiceSummary.tax / 100 END -
        CASE WHEN invoiceSummary.amountpaid IS NULL THEN 0 ELSE  invoiceSummary.amountpaid END -
        CASE WHEN invoiceSummary.downpayment IS NULL THEN 0 ELSE invoiceSummary.downpayment END)    AS remaining
    FROM DObInvoiceGeneralModel dobInvoice
    Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
    ON dobInvoice.id = invoiceSummary.id;