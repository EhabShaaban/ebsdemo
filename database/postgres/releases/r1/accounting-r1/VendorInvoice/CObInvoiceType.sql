
UPDATE cobinvoice
SET code = 'GOODS_INVOICE'
WHERE cobinvoice.id = 1;

UPDATE cobinvoice
SET code = 'SHIPMENT_INVOICE'
WHERE cobinvoice.id = 2;

UPDATE cobinvoice
SET code = 'CUSTOM_TRUSTEE_INVOICE'
WHERE cobinvoice.id = 3;

UPDATE cobinvoice
SET code = 'INSURANCE_INVOICE'
WHERE cobinvoice.id = 4;