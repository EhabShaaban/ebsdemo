DROP TABLE IF EXISTS IObInvoiceItem CASCADE;

CREATE TABLE IObInvoiceItem
(
    id                   BIGSERIAL     NOT NULL,
    refInstanceId        INT8          NOT NULL,
    creationDate         TIMESTAMP     NOT NULL,
    modifiedDate         TIMESTAMP     NOT NULL,
    creationInfo         VARCHAR(1024) NOT NULL,
    modificationInfo     VARCHAR(1024) NOT NULL,
    itemid               INT8          NOT NULL,
    baseunitofmeasureid  INT8          NOT NULL,
    orderunitofmeasureid INT8          NOT NULL,
    quantityinorderunit  NUMERIC       NOT NULL,
    price                NUMERIC       NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_DObInvoice FOREIGN KEY (refInstanceId)
        REFERENCES DObInvoice (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_MObItem FOREIGN KEY (itemid)
        REFERENCES mobitem (id) ON UPDATE CASCADE ON DELETE CASCADE;


ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_CObMeasure_BaseUnitOfMeasure FOREIGN KEY (baseunitofmeasureid)
        REFERENCES cobmeasure (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObInvoiceItem
    ADD CONSTRAINT FK_IObInvoiceItem_CObMeasure_OrderUnitOfMeasure FOREIGN KEY (orderunitofmeasureid)
        REFERENCES CObMeasure (id) ON UPDATE CASCADE ON DELETE CASCADE;