drop view if exists IObGoodsReceiptAccountingDetailsGeneralModel;
drop view if exists DobGoodsReceiptAccountingDocumentGeneralModel;
drop view if exists IObGoodsReceiptAccountingDocumentItemsGeneralModel;
drop view if exists IObGoodsReceiptAccountingDocumentDetailsGeneralModel;


create view IObGoodsReceiptAccountingDetailsGeneralModel AS
select IObGoodsReceiptAccountingDetails.id,
       IObGoodsReceiptAccountingDetails.creationDate,
       IObGoodsReceiptAccountingDetails.creationInfo,
       IObGoodsReceiptAccountingDetails.modifiedDate,
       IObGoodsReceiptAccountingDetails.modificationInfo,
       dobgoodsreceiptaccountingdocument.code,
       creditSubAccount.code                                                as creditSubAccountCode,
       debitSubAccount.code                                                 as debitSubAccountCode,
       concat(creditAccount.code, ' - ', creditAccount.name::json ->> 'en') as creditAccountCodeName,
       concat(debitAccount.code, ' - ', debitAccount.name::json ->> 'en')   as debitAccountCodeName

from IObGoodsReceiptAccountingDetails
         left join dobgoodsreceiptaccountingdocument
                   on iobgoodsreceiptaccountingdetails.refinstanceid = dobGoodsReceiptAccountingDocument.id
         left join doborder creditSubAccount
                   on IObGoodsReceiptAccountingDetails.creditsubaccountId = creditSubAccount.id
         left join doborder debitSubAccount
                   on IObGoodsReceiptAccountingDetails.debitsubaccountId = debitSubAccount.id
         left join HObGLAccount creditAccount on IObGoodsReceiptAccountingDetails.creditaccountId = creditAccount.id
         left join HObGLAccount debitAccount on IObGoodsReceiptAccountingDetails.debitaccountId = debitAccount.id
         left join cobenterprise company on dobgoodsreceiptaccountingdocument.companyid = company.id and company.objecttypecode = '1'
         left join cobenterprise purchaseUnit on dobgoodsreceiptaccountingdocument.purchaseunitid = purchaseUnit.id and purchaseUnit.objecttypecode = '5';


create view DobGoodsReceiptAccountingDocumentGeneralModel AS
select dobGoodsReceiptAccountingDocument.id,
       dobGoodsReceiptAccountingDocument.code,
       dobGoodsReceiptAccountingDocument.creationinfo,
       dobGoodsReceiptAccountingDocument.creationdate,
       dobGoodsReceiptAccountingDocument.modificationinfo,
       dobGoodsReceiptAccountingDocument.modifieddate,
       dobGoodsReceiptAccountingDocument.currentstates,
       concat(company.code, ' - ', company.name::json ->> 'en')             as companyCodeName,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name::json ->> 'en')   as purchaseUnitCodeName

from dobGoodsReceiptAccountingDocument

         left join cobenterprise company on dobgoodsreceiptaccountingdocument.companyid = company.id and company.objecttypecode = '1'
         left join cobenterprise purchaseUnit on dobgoodsreceiptaccountingdocument.purchaseunitid = purchaseUnit.id and purchaseUnit.objecttypecode = '5';


create view IObGoodsReceiptAccountingDocumentItemsGeneralModel AS
select IObGoodsReceiptAccountingDocumentItems.id,
       dobgoodsreceiptaccountingdocument.code as userCode,
       iobgoodsreceiptaccountingdocumentitems.refinstanceid,
       iobgoodsreceiptaccountingdocumentitems.quantity,
       iobgoodsreceiptaccountingdocumentitems.stocktype,
       iobgoodsreceiptaccountingdocumentitems.tobeconsideredincostingper,
       iobgoodsreceiptaccountingdocumentitems.itemactualcost,
       concat(masterdata.code, ' - ', masterdata.name::json ->> 'en') as itemCodeName,
       concat(cobmeasure.code, ' - ', cobmeasure.name::json ->> 'en') as uomCodeName,
       iobgoodsreceiptaccountingdocumentitems.creationDate,
       iobgoodsreceiptaccountingdocumentitems.creationInfo,
       iobgoodsreceiptaccountingdocumentitems.modifiedDate,
       iobgoodsreceiptaccountingdocumentitems.modificationInfo

from IObGoodsReceiptAccountingDocumentItems

         left join masterdata on IObGoodsReceiptAccountingDocumentItems.itemid = masterdata.id and masterdata.objecttypecode = '1'
         left join cobmeasure on IObGoodsReceiptAccountingDocumentItems.uomid = cobmeasure.id
    left join dobgoodsreceiptaccountingdocument
    on IObGoodsReceiptAccountingDocumentItems.refinstanceid = dobGoodsReceiptAccountingDocument.id;


create view IObGoodsReceiptAccountingDocumentDetailsGeneralModel AS
select IObGoodsReceiptAccountingDocumentDetails.id,
       dobgoodsreceiptaccountingdocument.code as userCode,
       IObGoodsReceiptAccountingDocumentDetails.refinstanceid,
       IObGoodsReceiptAccountingDocumentDetails.creationDate,
       IObGoodsReceiptAccountingDocumentDetails.creationInfo,
       IObGoodsReceiptAccountingDocumentDetails.modifiedDate,
       IObGoodsReceiptAccountingDocumentDetails.modificationInfo,
       dobgoodsreceipt.code as goodsReceiptCode,
       doborder.code as purchaseOrderCode
from IObGoodsReceiptAccountingDocumentDetails
         left join dobgoodsreceipt
                   on IObGoodsReceiptAccountingDocumentDetails.goodsreceiptid = dobgoodsreceipt.id
         left join dobgoodsreceiptaccountingdocument
                   on IObGoodsReceiptAccountingDocumentDetails.refinstanceid = dobGoodsReceiptAccountingDocument.id
         left join iobgoodsreceiptpurchaseorderdata
                   on iobgoodsreceiptpurchaseorderdata.refinstanceid = dobgoodsreceipt.id
         left join doborder
                   on iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborder.id;
