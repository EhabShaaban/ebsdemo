drop view if exists IObGoodsReceiptAccountingDetailsGeneralModel;
drop table if exists dobGoodsReceiptAccountingDocument;
drop table if exists IObGoodsReceiptAccountingDetails;
drop table if exists IObGoodsReceiptAccountingDocumentDetails;

CREATE TABLE dobGoodsReceiptAccountingDocument
(
    id                   BIGSERIAL                NOT NULL,
    refDocumentId        INT8                     NOT NULL,
    companyId         INT8                     NOT NULL,
    purchaseUnitId    INT8                     NOT NULL,
    code                 VARCHAR(1024)            NOT NULL,
    creationDate         TIMESTAMP With time zone NOT NULL,
    modifiedDate         TIMESTAMP With time zone NOT NULL,
    creationInfo         VARCHAR(1024)            NOT NULL,
    modificationInfo     VARCHAR(1024)            NOT NULL,
    currentStates        VARCHAR(1024)            NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE IObGoodsReceiptAccountingDetails
(
    id                 BIGSERIAL                NOT NULL,
    refInstanceId      INT8                     NOT NULL REFERENCES dobGoodsReceiptAccountingDocument (id) ON DELETE CASCADE,
    creationDate       timestamp with time zone NOT NULL,
    modifiedDate       timestamp with time zone NOT NULL,
    creationInfo       VARCHAR(1024)            NOT NULL,
    modificationInfo   VARCHAR(1024)            NOT NULL,
    creditaccountId    INT8                     NOT NULL REFERENCES HObGLAccount (id) ON DELETE RESTRICT,
    creditsubaccountId INT8 REFERENCES doborder (id) ON DELETE RESTRICT,
    debitaccountId     INT8                     NOT NULL REFERENCES HObGLAccount (id) ON DELETE RESTRICT,
    debitsubaccountId  INT8 REFERENCES doborder (id) ON DELETE CASCADE,
    PRIMARY KEY (id)
);


CREATE TABLE IObGoodsReceiptAccountingDocumentItems
(
    id                          BIGSERIAL                  NOT NULL,
    refInstanceId               INT8                       NOT NULL REFERENCES dobGoodsReceiptAccountingDocument (id) ON DELETE CASCADE,
    creationDate                timestamp with time zone   NOT NULL,
    modifiedDate                timestamp with time zone   NOT NULL,
    creationInfo                VARCHAR(1024)              NOT NULL,
    modificationInfo            VARCHAR(1024)              NOT NULL,
    itemId                      INT8                       NOT NULL REFERENCES mobitem (id) ON DELETE RESTRICT,
    uomId                       INT8                                REFERENCES cobmeasure (id) ON DELETE RESTRICT,
    Quantity                    float                     NOT NULL,
    stockType                   VARCHAR(1024)              NOT NULL,
    toBeConsideredinCostingPer  float                     NOT NULL,
    itemActualCost              float                     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObGoodsReceiptAccountingDocumentDetails
(
    id                 BIGSERIAL                NOT NULL,
    refInstanceId      INT8                     NOT NULL REFERENCES dobGoodsReceiptAccountingDocument (id) ON DELETE CASCADE,
    creationDate       timestamp with time zone NOT NULL,
    modifiedDate       timestamp with time zone NOT NULL,
    creationInfo       VARCHAR(1024)            NOT NULL,
    modificationInfo   VARCHAR(1024)            NOT NULL,
    goodsReceiptId     INT8                     NOT NULL,
    PRIMARY KEY (id)
);
