DROP VIEW if exists IObCollectionRequestCompanyDetailsGeneralModel;
DROP VIEW if exists DObCollectionRequestGeneralModel;
DROP VIEW if exists IObCollectionRequestCompanyDetailsGeneralModel;
DROP VIEW if exists IObCollectionRequestAccountingDetailsGeneralModel;
DROP VIEW if exists IObCollectionRequestBankGeneralModel;
DROP VIEW if exists DObNotesReceivablesGeneralModel;
DROP VIEW if exists IObCompanyBankDetailsGeneralModel;

-----------------------------------------------------------

ALTER TABLE IObCollectionRequestAccountingDetails ADD COLUMN debitSubAccountId INT8;

ALTER TABLE IObCollectionRequestAccountingDetails
    ADD CONSTRAINT FK_IObCollectionRequestAccountingDetails_debitAccountId FOREIGN KEY (debitSubAccountId) REFERENCES iobcompanybankdetails (id) ON DELETE cascade;

------------------

ALTER TABLE IObCustomerCollectionRequestAccountingDetails RENAME COLUMN subAccountId TO creditSubAccountId;

ALTER TABLE IObCustomerCollectionRequestAccountingDetails
    DROP CONSTRAINT FK_IObCustomerCollectionRequestAccountingDetails_subAccountId;
ALTER TABLE IObCustomerCollectionRequestAccountingDetails
    ADD CONSTRAINT FK_IObCustomerCollectionRequestAccountingDetails_subAccountId FOREIGN KEY (creditSubAccountId) REFERENCES MObCustomer (id) ON DELETE cascade;


ALTER TABLE IObNotesReceivableCollectionRequestAccountingDetails RENAME COLUMN subAccountId TO creditSubAccountId;

ALTER TABLE IObNotesReceivableCollectionRequestAccountingDetails
    DROP CONSTRAINT FK_IObNotesReceivableCollectionRequestAccountingDetails_subAccountId;
ALTER TABLE IObNotesReceivableCollectionRequestAccountingDetails
    ADD CONSTRAINT FK_IObNotesReceivableCollectionRequestAccountingDetails_subAccountId FOREIGN KEY (creditSubAccountId) REFERENCES dobnotesreceivables (id) ON DELETE cascade;

--------------------------

ALTER TABLE DObCollectionRequest DROP CONSTRAINT FK_DObCollectionRequest_company;
ALTER TABLE DObCollectionRequest DROP COLUMN companyId;

-----------------------------

create table IObCollectionRequestCompanyDetails
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    companyId        INT8                     NOT NULL,
    bankDetailsId    INT8                     ,
    PRIMARY KEY (id)
);
ALTER TABLE IObCollectionRequestCompanyDetails
    ADD CONSTRAINT FK_IObCollectionRequestCompanyDetails_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObCollectionRequest (id) ON DELETE cascade;

ALTER TABLE IObCollectionRequestCompanyDetails
    ADD CONSTRAINT FK_IObCollectionRequestCompanyDetails_company FOREIGN KEY (companyId) REFERENCES cobcompany (id) ON DELETE RESTRICT;

ALTER TABLE IObCollectionRequestCompanyDetails
    ADD CONSTRAINT FK_IObCollectionRequestCompanyDetails_bankDetails FOREIGN KEY (bankDetailsId) REFERENCES iobcompanybankdetails (id) ON DELETE RESTRICT;

------------------------------------
