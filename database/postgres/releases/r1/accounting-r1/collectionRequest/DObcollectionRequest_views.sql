DROP FUNCTION if exists getcollectionrequestsubaccountcode(bigint, character varying);
CREATE or REPLACE FUNCTION getCollectionRequestSubAccountCode(glSubAccountId bigint, glAccount VARCHAR)
    RETURNS id_code_name AS
$$
DECLARE
    result id_code_name ;

BEGIN
    case
        when glAccount = 'Local_Customer' then
            select id, code, name
            INTO result.id, result.code, result.name
            from masterdata
            where masterdata.id = glSubAccountId
              and objecttypecode = '3';

        when glAccount = 'Notes_Receivables' then
            select id, code, ('{"en":"NONE","ar": "NONE"}')
            INTO result.id, result.code, result.name
            from dobnotesreceivables
            where dobnotesreceivables.id = glSubAccountId;

        when glAccount = 'Banks' then
            select iobcompanybankdetails.id, accountno, name
            INTO result.id, result.code, result.name
            from iobcompanybankdetails
                     left join cobbank on cobbank.id = iobcompanybankdetails.bankid
            where iobcompanybankdetails.id = glSubAccountId;

        else
            return null;
        end case;
    return result;
END;
$$
    LANGUAGE PLPGSQL;

----------------------------------

---------------------------------------
DROP VIEW if exists IObCollectionRequestCompanyDetailsGeneralModel;

CREATE VIEW IObCollectionRequestCompanyDetailsGeneralModel AS
SELECT companyDetails.id,
       companyDetails.refInstanceId,
       companyDetails.creationDate,
       companyDetails.modifiedDate,
       companyDetails.creationInfo,
       companyDetails.modificationInfo,
       company.code              AS companycode,
       company.name              AS companyname,
       CObBank.code              AS bankcode,
       CObBank.name              AS bankname,
       DObCollectionRequest.code as collecttionRequestCode,
       bankdetails.accountno     as accountNumber
FROM IObCollectionRequestCompanyDetails companyDetails
         JOIN cobenterprise company ON companyDetails.companyid = company.id
         left JOIN iobcompanybankdetails bankdetails
                   ON companyDetails.bankDetailsId = bankdetails.id
         left JOIN CObBank ON bankdetails.bankid = CObBank.id
         JOIN DObCollectionRequest ON DObCollectionRequest.id = companyDetails.refInstanceId;
------------------------------------

DROP VIEW if exists DObCollectionRequestGeneralModel;

CREATE VIEW DObCollectionRequestGeneralModel AS
SELECT cr.id,
       cr.code,
       cr.creationDate,
       cr.modifiedDate,
       cr.creationInfo,
       cr.modificationInfo,
       cr.currentStates,
       businessUnit.name                                                                   AS purchaseUnitName,
       businessUnit.name :: json ->> 'en'                                                  AS purchaseUnitNameEn,
       businessUnit.code                                                                   AS purchaseUnitcode,
       cr.collectiontype,
       cr.collectionform,
       company.code                                                                        AS companycode,
       company.name                                                                        AS companyname,
       cobcurrency.code                                                                    AS companylocalcurrencycode,
       cobcurrency.name                                                                    AS companylocalcurrencyname,
       cobcurrency.iso                                                                     AS companylocalcurrencyiso,
       masterdata.name                                                                     AS customername,
       masterdata.code                                                                     AS customercode,
       iobcollectionrequestdetails.amount,
       refDocumentType.code                                                                AS refdocumenttypecode,
       refDocumentType.name                                                                AS refdocumenttypename,
       getRefDocumentCodeBasedOnType(iobcollectionrequestdetails.id,
                                     refDocumentType.code)                                 AS refDocumentCode
FROM dobcollectionrequest cr
         left join iobcollectionrequestdetails on cr.id = iobcollectionrequestdetails.refinstanceid
         left join CObCollectionRequestReferenceDocumentType refDocumentType
                   on iobcollectionrequestdetails.refDocumentTypeId = refDocumentType.id
         left join masterdata on iobcollectionrequestdetails.customerid = masterdata.id
         left join cobenterprise businessUnit on cr.businessunitid = businessUnit.id
         left join IObCollectionRequestCompanyDetails
                   on IObCollectionRequestCompanyDetails.refInstanceId = cr.id
         left join cobenterprise company
                   on IObCollectionRequestCompanyDetails.companyid = company.id
         left join cobcompany on IObCollectionRequestCompanyDetails.companyid = cobcompany.id
         left join iobenterprisebasicdata on cobcompany.id = iobenterprisebasicdata.refinstanceid
         left join cobcurrency on iobenterprisebasicdata.currencyid = cobcurrency.id;

----------------------
DROP VIEW if exists IObCollectionRequestCompanyDetailsGeneralModel;

CREATE VIEW IObCollectionRequestCompanyDetailsGeneralModel AS
SELECT companyDetails.id,
       companyDetails.refInstanceId,
       companyDetails.creationDate,
       companyDetails.modifiedDate,
       companyDetails.creationInfo,
       companyDetails.modificationInfo,
       company.code              AS companycode,
       company.name              AS companyname,
       CObBank.code              AS bankcode,
       CObBank.name              AS bankname,
       DObCollectionRequest.code as collecttionRequestCode,
       bankdetails.accountno     as accountNumber
FROM IObCollectionRequestCompanyDetails companyDetails
         JOIN cobenterprise company ON companyDetails.companyid = company.id
         left JOIN iobcompanybankdetails bankdetails
                   ON companyDetails.bankDetailsId = bankdetails.id
         left JOIN CObBank ON bankdetails.bankid = CObBank.id
         JOIN DObCollectionRequest ON DObCollectionRequest.id = companyDetails.refInstanceId;

-----------------------------
DROP VIEW if exists IObCollectionRequestAccountingDetailsGeneralModel;

CREATE VIEW IObCollectionRequestAccountingDetailsGeneralModel AS
SELECT accountingDetails.id,
       DObCollectionRequest.code,
       accountingDetails.creationDate,
       accountingDetails.modifiedDate,
       accountingDetails.creationInfo,
       accountingDetails.modificationInfo,
       accountingDetails.accountForm                                    as accountform,
       accountingDetails.type,
       hobglaccount.id                                                  as accountId,
       hobglaccount.code                                                as accountCode,
       hobglaccount.name                                                as accountName,
       (select id
        from getCollectionRequestSubAccountCode(accountingDetails.id,
                                                leafglaccount.leadger)) as subAccountId,
       (select code
        from getCollectionRequestSubAccountCode(accountingDetails.id,
                                                leafglaccount.leadger)) as subAccountCode,
       (select name
        from getCollectionRequestSubAccountCode(accountingDetails.id,
                                                leafglaccount.leadger)) as subAccountName,
       iobcompanybankdetails.accountno                                  AS bankAccountNumber,
       cobBank.name                                                     AS bankName,
       IObCollectionRequestDetails.amount                               as amount,
       IObCollectionRequestDetails.currencyId                           as amountCurrencyId,
       cobcompanygeneralmodel.currencyid                                as companyCurrencyId
from IObCollectionRequestAccountingDetails accountingDetails
         left join DObCollectionRequest on DObCollectionRequest.id = accountingDetails.refInstanceId
         left join hobglaccount on hobglaccount.id = accountingDetails.accountId
         left join leafglaccount on leafglaccount.id = hobglaccount.id
         left join IObCollectionRequestDetails
                   on accountingDetails.refInstanceId = IObCollectionRequestDetails.refInstanceId
         left join IObCollectionRequestCompanyDetails
                   on IObCollectionRequestCompanyDetails.refInstanceId = DObCollectionRequest.id
         left join cobcompanygeneralmodel
                   on IObCollectionRequestCompanyDetails.companyId = cobcompanygeneralmodel.id
         left join iobcompanybankdetails
                   on accountingDetails.debitSubAccountId = iobcompanybankdetails.id
         left join cobBank on iobcompanybankdetails.bankid = cobBank.id;
------------------------------------
DROP VIEW if exists DObNotesReceivablesGeneralModel;

CREATE VIEW DObNotesReceivablesGeneralModel AS
SELECT DObNotesReceivables.id,
       DObNotesReceivables.code,
       DObNotesReceivables.creationInfo,
       DObNotesReceivables.modificationInfo,
       DObNotesReceivables.creationDate,
       DObNotesReceivables.modifiedDate,
       DObNotesReceivables.currentStates,
       DObNotesReceivables.source,
       MasterData.code                                          AS customerCode,
       MasterData.name                                          AS customerName,
       BusinessUnit.code                                        AS purchaseUnitCode,
       BusinessUnit.name                                        AS purchaseUnitName,
       BusinessUnit.name :: json ->> 'en'                       AS purchaseUnitNameEn,
       IObNotesReceivablesDetails.notesInformation,
       IObNotesReceivablesDetails.dueDate,
       IObNotesReceivablesDetails.amount :: VARCHAR             AS amount,
       CObCurrency.iso                                          AS amountCurrencyIso,
       IObNotesReceivablesDetails.issuingBank,
       concat(Company.code, ' - ', Company.name::json ->> 'en') AS companyCodeName,
       Company.code                                             AS companyCode,
       IObNotesReceivablesDetails.refDocumentType,

       (CASE
            WHEN IObNotesReceivablesdetails.refdocumenttype = 'SALES_INVOICE'
                THEN ('{"en":"Sales Invoice","ar":"فاتورة مبيعات"}') :: json
            ELSE NULL END
           )                                                    AS refDocumentName,

       (CASE
            WHEN IObNotesReceivablesDetails.refDocumentType = 'SALES_INVOICE'
                THEN (SELECT code
                      FROM dobsalesinvoicegeneralmodel
                      WHERE dobsalesinvoicegeneralmodel.id = (SELECT RefDocumentId
                                                              FROM IObNotesReceivablesSalesInvoiceData
                                                              WHERE IObNotesReceivablesSalesInvoiceData.id =
                                                                    IObNotesReceivablesDetails.id))
            ELSE ''
           END)                                                 AS refDocumentCode,

       (CASE
            WHEN IObNotesReceivablesDetails.refDocumentType = 'SALES_INVOICE'
                THEN getNotesReceivableRemaining(DObNotesReceivables.code)
            ELSE 0
           END)                                                 AS remaining

FROM DObNotesReceivables
         LEFT JOIN IObNotesReceivablesdetails
                   ON IObNotesReceivablesdetails.refinstanceid = DObNotesReceivables.id
         LEFT JOIN CObEnterprise BusinessUnit
                   ON DObNotesReceivables.purchaseunitid = BusinessUnit.id AND
                      BusinessUnit.objecttypecode = '5'
         LEFT JOIN CObEnterprise Company
                   ON dobnotesreceivables.companyid = Company.id AND Company.objecttypecode = '1'
         LEFT JOIN CObCurrency ON CObCurrency.id = DObNotesReceivables.currencyid
         LEFT JOIN MasterData ON MasterData.id = DObNotesReceivables.customerid AND
                                 MasterData.objecttypecode = '3';
------------------------------------
DROP VIEW if exists IObCompanyBankDetailsGeneralModel;

CREATE VIEW IObCompanyBankDetailsGeneralModel AS
SELECT iobcompanybankdetails.id,
       cobenterprise.code                                       companyCode,
       cobbank.code,
       cobbank.name,
       iobcompanybankdetails.accountno,
       concat(cobbank.name::json ->> 'en', ' - ', accountno) AS bankAccountNumber
FROM iobcompanybankdetails
         JOIN cobcompany
              on iobcompanybankdetails.refinstanceid = cobcompany.id
         JOIN cobbank ON iobcompanybankdetails.bankid = cobbank.id
         JOIN cobenterprise ON cobcompany.id = cobenterprise.id;