drop table if exists IObSalesOrderCompanyStoreData;
drop table if exists IObSalesOrderData;
drop table if exists DObSalesOrder;
drop table if exists CObSalesOrderType;

CREATE TABLE CObSalesOrderType
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024),
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE DObSalesOrder
(
    id                 BIGSERIAL                NOT NULL,
    code               VARCHAR(1024)            NOT NULL,
    creationDate       TIMESTAMP With time zone NOT NULL,
    modifiedDate       TIMESTAMP With time zone NOT NULL,
    creationInfo       VARCHAR(1024)            NOT NULL,
    modificationInfo   VARCHAR(1024)            NOT NULL,
    currentStates      VARCHAR(1024)            NOT NULL,
    salesOrderTypeId   INT8                     NOT NULL REFERENCES CObSalesOrderType (id) ON DELETE RESTRICT,
    businessUnitId     INT8                     NOT NULL REFERENCES cobenterprise (id) ON DELETE RESTRICT,
    salesResponsibleId INT8                     NOT NULL REFERENCES cobenterprise (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesOrderData
(
    id                   BIGSERIAL                NOT NULL,
    refInstanceId        INT8                     NOT NULL REFERENCES DObSalesOrder (id) ON DELETE CASCADE,
    creationDate         TIMESTAMP With time zone NOT NULL,
    modifiedDate         TIMESTAMP With time zone NOT NULL,
    creationInfo         VARCHAR(1024)            NOT NULL,
    modificationInfo     VARCHAR(1024)            NOT NULL,
    customerId           INT8 REFERENCES mobcustomer (id) ON DELETE RESTRICT,
    customerAddressId    INT8 REFERENCES iobcustomeraddress (id) ON DELETE RESTRICT,
    contactPersonId      INT8 REFERENCES iobcustomercontactperson (id) ON DELETE RESTRICT,
    paymentTermId        INT8 REFERENCES cobpaymentterms (id) ON DELETE RESTRICT,
    currencyId           INT8 REFERENCES cobcurrency (id) ON DELETE RESTRICT,
    expectedDeliveryDate TIMESTAMP With time zone,
    notes                VARCHAR(1024),
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesOrderCompanyStoreData
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObSalesOrder (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    companyId        INT8                     NOT NULL REFERENCES CObCompany (id) ON DELETE RESTRICT,
    storeId          INT8 REFERENCES CObStorehouse (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObSalesOrderItem
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObSalesOrder (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    itemId           INT8                     NOT NULL REFERENCES mobitem (id) ON DELETE restrict,
    orderUnitId      INT8                     NOT NULL REFERENCES cobmeasure (id) ON DELETE restrict,
    quantity         FLOAT                    NOT NULL,
    salesPrice       FLOAT                    NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE IObSalesOrderCycleDates
(
    id               BIGSERIAL                NOT NULL,
    refinstanceid    INT8                     NOT NULL REFERENCES DObSalesOrder (id) ON DELETE CASCADE,
    creationdate     timestamp with time zone NOT NULL,
    modifieddate     timestamp with time zone NOT NULL,
    creationinfo     varchar(1024)            NOT NULL,
    modificationinfo varchar(1024)            NOT NULL,
    approvalDate     timestamp with time zone,
    cancellationDate timestamp with time zone,
    PRIMARY KEY (id)
);