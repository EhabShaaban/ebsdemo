DROP VIEW IF EXISTS DObSalesOrderGeneralModel CASCADE;
DROP VIEW IF EXISTS IObSalesOrderCompanyAndStoreDataGeneralModel CASCADE;
DROP VIEW IF EXISTS IObSalesOrderDataGeneralModel CASCADE;

CREATE VIEW DObSalesOrderGeneralModel AS
SELECT dobsalesorder.id,
       dobsalesorder.code,
       dobsalesorder.creationDate,
       dobsalesorder.modifiedDate,
       dobsalesorder.creationInfo,
       dobsalesorder.modificationInfo,
       dobsalesorder.currentStates,
       cobsalesordertype.code               AS salesOrderTypeCode,
       cobsalesordertype.name               AS salesOrderTypeName,
       businessunit.code                    AS purchaseUnitCode,
       businessunit.name                    AS purchaseUnitName,
       businessunit.name::json ->> 'en'     AS purchaseUnitNameEn,
       salesResponsible.code                AS salesResponsibleCode,
       salesResponsible.name                AS salesResponsibleName,
       salesResponsible.name::json ->> 'en' AS salesResponsibleNameEn,
       masterdata.code                      AS customerCode,
       masterdata.name                      AS customerName,
       company.code                         AS companyCode,
       company.name                         AS companyName,
       store.code                           AS storeCode,
       store.name                           AS storeName,
       iobsalesorderdata.expectedDeliveryDate,
       iobsalesorderdata.notes
FROM dobsalesorder
         LEFT JOIN
     cobsalesordertype ON dobsalesorder.salesordertypeid = cobsalesordertype.id
         LEFT JOIN cobenterprise businessunit ON dobsalesorder.businessunitid = businessunit.id AND
                                                 businessunit.objecttypecode = '5'
         LEFT JOIN cobenterprise salesResponsible
                   ON dobsalesorder.salesresponsibleid = salesResponsible.id AND
                      salesResponsible.objecttypecode = '7'
         LEFT JOIN iobsalesorderdata ON dobsalesorder.id = iobsalesorderdata.refinstanceid

         LEFT JOIN mobcustomer ON iobsalesorderdata.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN iobsalesordercompanystoredata ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
         LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
                                            company.objecttypecode = '1'
         LEFT JOIN cobenterprise store ON iobsalesordercompanystoredata.storeid = store.id AND
                                          store.objecttypecode = '4';

CREATE VIEW IObSalesOrderCompanyAndStoreDataGeneralModel AS
SELECT iobsalesordercompanystoredata.id,
       dobsalesorder.id   AS refinstanceId,
       dobsalesorder.code AS salesOrderCode,
       iobsalesordercompanystoredata.creationDate,
       iobsalesordercompanystoredata.modifiedDate,
       iobsalesordercompanystoredata.creationInfo,
       iobsalesordercompanystoredata.modificationInfo,
       company.code       AS companyCode,
       company.name       AS companyName,
       store.code         AS storeCode,
       store.name         AS storeName
FROM iobsalesordercompanystoredata
         LEFT JOIN dobsalesorder ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
         LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
                                            company.objecttypecode = '1'
         LEFT JOIN cobenterprise store ON iobsalesordercompanystoredata.storeid = store.id AND
                                          store.objecttypecode = '4';

CREATE VIEW IObSalesOrderDataGeneralModel AS
SELECT iobsalesorderdata.id,
       iobsalesorderdata.refinstanceid,
       dobsalesorder.code     AS salesOrderCode,
       iobsalesorderdata.creationDate,
       iobsalesorderdata.modifiedDate,
       iobsalesorderdata.creationInfo,
       iobsalesorderdata.modificationInfo,
       masterdata.code        AS customerCode,
       masterdata.name        AS customerName,
       iobcustomeraddress.id  AS customerAddressId,
       lobaddress.addressname AS customerAddressName,
       lobaddress.address     AS customerAddress,

       iobcustomercontactperson.id AS customerContactPersonId,
       lobcontactperson.name  AS contactPersonName,

       cobpaymentterms.code   AS paymentTermCode,
       cobpaymentterms.name   AS paymentTermName,

       cobcurrency.iso        AS currencyIso,
       cobcurrency.code as currencyCode,
       cobcurrency.name as currencyName,
       mobcustomer.creditlimit,

       iobsalesorderdata.expecteddeliverydate,
       iobsalesorderdata.notes

FROM iobsalesorderdata
         LEFT JOIN dobsalesorder ON dobsalesorder.id = iobsalesorderdata.refinstanceid
         LEFT JOIN iobcustomeraddress ON iobsalesorderdata.customeraddressid = iobcustomeraddress.id
         LEFT JOIN lobaddress ON iobcustomeraddress.addressid = lobaddress.id
         LEFT JOIN iobcustomercontactperson
                   ON iobsalesorderdata.contactpersonid = iobcustomercontactperson.id
         LEFT JOIN lobcontactperson
                   ON iobcustomercontactperson.contactpersonid = lobcontactperson.id
         LEFT JOIN cobpaymentterms ON iobsalesorderdata.paymenttermid = cobpaymentterms.id

         LEFT JOIN iobsalesordercompanystoredata
                   ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
         LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
                                            company.objecttypecode = '1'
         LEFT JOIN iobenterprisebasicdata ON company.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN cobcurrency ON cobcurrency.id = iobenterprisebasicdata.currencyid

         LEFT JOIN mobcustomer ON iobsalesorderdata.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id;

CREATE VIEW IObSalesOrderItemGeneralModel AS
SELECT IObSalesOrderItem.id,
       IObSalesOrderItem.refinstanceid,
       IObSalesOrderItem.creationDate,
       IObSalesOrderItem.modifiedDate,
       IObSalesOrderItem.creationInfo,
       IObSalesOrderItem.modificationInfo,
       dobsalesorder.code                                        as salesOrderCode,
       masterdata.code                                           AS itemCode,
       masterdata.name                                           AS itemName,
       cobitem.code                                              AS itemTypeCode,
       cobitem.name                                              AS itemTypeName,
       cobmeasure.code                                           as unitOfMeasureCode,
       cobmeasure.name                                           as unitOfMeasureName,
       cobmeasure.symbol                                         as unitOfMeasureSymbol,
       IObSalesOrderItem.quantity,
       IObSalesOrderItem.salesprice,
       IObSalesOrderItem.quantity * IObSalesOrderItem.salesprice as totalAmount
FROM dobsalesorder
         join IObSalesOrderItem on IObSalesOrderItem.refInstanceId = dobsalesorder.id
         join mobitem on IObSalesOrderItem.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         join cobitem on mobitem.typeid = cobitem.id
         join cobmeasure on IObSalesOrderItem.orderUnitId = cobmeasure.id;
