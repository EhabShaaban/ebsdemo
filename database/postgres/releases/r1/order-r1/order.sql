drop view if exists IObOrderLineDetailsGeneralModel CASCADE;
drop view if exists IObOrderLineDetailsSummaryGeneralModel CASCADE;
drop view if exists DObPurchaseOrderTotalAmountGeneralModel CASCADE;
drop view if exists IObPurchaseOrderItemsGeneralModel CASCADE;

CREATE VIEW IObOrderLineDetailsGeneralModel AS
         SELECT ioborderlinedetails.id,
         ioborderlinedetails.creationInfo,
         ioborderlinedetails.creationDate,
         ioborderlinedetails.modificationInfo,
         ioborderlinedetails.modifiedDate,
         doborder.code                      AS ordercode,
         mobitemgeneralmodel.name,
         mobitemgeneralmodel.code           AS itemcode,
         (Select cobmeasure.symbol from cobmeasure where cobmeasure.id = ioborderlinedetails.unitOfMeasureId) as unitsymbol,
        (select SUM(IObOrderLineDetailsQuantitiesGeneralModel.quantityinbase) as quantity from IObOrderLineDetailsQuantitiesGeneralModel where ioborderlinedetails.id = IObOrderLineDetailsQuantitiesGeneralModel.refinstanceid) as itemQuantity,
         ioborderlinedetails.quantityindn,
         mobitemgeneralmodel.itemgroupname
  FROM ioborderlinedetails
  LEFT JOIN doborder ON doborder.id = ioborderlinedetails.refinstanceid
  LEFT JOIN mobitemgeneralmodel ON mobitemgeneralmodel.id = ioborderlinedetails.itemid;

CREATE VIEW IObOrderLineDetailsSummaryGeneralModel AS
SELECT doborder.*,
       sum(ioborderlinedetailsquantitiesgeneralmodel.price *
           ioborderlinedetailsquantitiesgeneralmodel.quantityinbase) AS TotalAmount,
       sum((ioborderlinedetailsquantitiesgeneralmodel.discountpercentage *
            ioborderlinedetailsquantitiesgeneralmodel.quantityinbase *
            ioborderlinedetailsquantitiesgeneralmodel.price) / 100)  AS TotalDiscount
FROM doborder
         LEFT JOIN ioborderlinedetailsgeneralmodel
                   ON doborder.code = ioborderlinedetailsgeneralmodel.ordercode
         LEFT JOIN ioborderlinedetailsquantitiesgeneralmodel
              ON ioborderlinedetailsgeneralmodel.id = ioborderlinedetailsquantitiesgeneralmodel.refinstanceid
GROUP BY doborder.id;

CREATE VIEW DObPurchaseOrderTotalAmountGeneralModel AS
SELECT dobpurchaseordergeneralmodel.*,
       (CASE WHEN ioborderlinedetailssummarygeneralmodel.totalAmount IS NULL THEN 0
           ELSE ioborderlinedetailssummarygeneralmodel.totalAmount END -
       CASE WHEN ioborderlinedetailssummarygeneralmodel.totaldiscount IS NULL THEN 0
           ELSE ioborderlinedetailssummarygeneralmodel.totaldiscount END)               AS TotalAmount
FROM ioborderlinedetailssummarygeneralmodel JOIN dobpurchaseordergeneralmodel
ON ioborderlinedetailssummarygeneralmodel.id = dobpurchaseordergeneralmodel.id;

CREATE VIEW IObPurchaseOrderItemsGeneralModel AS
SELECT ioborderlinedetails.id,
       doborder.id                                                 as purchaseOrderId, --purchaseID
       doborder.code                                               as ordercode,
       (Select masterdata.name
        from masterdata
        where masterdata.id = ioborderlinedetails.itemId)          AS itemname,
       mobitemgeneralmodel.code                                    as itemcode,
       mobitemgeneralmodel.basicUnitOfMeasure                      as basicUnitOfMeasureId,
       mobitemgeneralmodel.batchmanagementrequired,
       itemvendorrecord.itemvendorcode,
       itemvendorrecord.itemId,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId) as unitsymbol,
       (select SUM(ioborderlinedetailsquantities.quantity) as quantity
        from ioborderlinedetailsquantities
        where ioborderlinedetails.id =
              ioborderlinedetailsquantities.refinstanceid)         as itemQuantity,
       ioborderlinedetails.quantityindn

FROM doborder
         join ioborderlinedetails on doborder.id = ioborderlinedetails.refinstanceid
         join mobitemgeneralmodel on ioborderlinedetails.itemid = mobitemgeneralmodel.id
         join itemvendorrecord
              on ioborderlinedetails.itemid = itemvendorrecord.itemid
                  ANd doborder.vendorid = itemvendorrecord.vendorid

where MObItemGeneralModel.basicunitofmeasure is not null
  and mobitemgeneralmodel.batchmanagementrequired is not null;
