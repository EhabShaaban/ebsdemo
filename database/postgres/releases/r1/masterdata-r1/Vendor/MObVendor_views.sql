drop view if exists IObVendorAccountingInfoGeneralModel;
drop view if exists MObVendorGeneralModel;

create view MObVendorGeneralModel as
select MasterData.id,
       MasterData.code,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.name              as vendorname,
       MasterData.currentstates,
       MObVendor.oldVendorNumber,
       MObVendor.accountWithVendor,
       CObVendor.code as typeCode,
       CObVendor.name as typeName,
       (select code
        from cobenterprise
        where id = MObVendor.purchaseresponsibleid
          and objecttypecode = '6') as purchaseResponsibleCode,
       (select name
        from cobenterprise
        where id = MObVendor.purchaseresponsibleid
          and objecttypecode = '6') as purchaseResponsibleName,
       (select string_agg(cobenterprise.code, ', ')
        from cobenterprise
        where cobenterprise.id in (select IObVendorPurchaseUnit.purchaseunitid
                                   from IObVendorPurchaseUnit
                                   where IObVendorPurchaseUnit.refInstanceId = MObVendor.id)
          and cobenterprise.objectTypeCode = '5'
       )                            as purchaseunitscodes,
       (select array_to_json(array_agg(cobenterprise.name))
        from cobenterprise
        where cobenterprise.id in (select IObVendorPurchaseUnit.purchaseunitid
                                   from IObVendorPurchaseUnit
                                   where IObVendorPurchaseUnit.refInstanceId = MObVendor.id)
          and cobenterprise.objectTypeCode = '5'
       )                            as purchaseunitsnames,
       (select string_agg(concat(cobenterprise.code, ' - ', cobenterprise.name::json ->> 'en') , ', ')
        from cobenterprise
        where cobenterprise.id in (select IObVendorPurchaseUnit.purchaseunitid
                                   from IObVendorPurchaseUnit
                                   where IObVendorPurchaseUnit.refInstanceId = MObVendor.id)
          and cobenterprise.objectTypeCode = '5'
       )                            as purchaseunits

from MObVendor
         left join MasterData on MObVendor.id = MasterData.id
         left join CObVendor on MObVendor.typeId = CObVendor.id;

create view IObVendorAccountingInfoGeneralModel as
SELECT iobvendoraccountinginfo.id,
       iobvendoraccountinginfo.refinstanceid,
       iobvendoraccountinginfo.creationdate,
       iobvendoraccountinginfo.modifieddate,
       iobvendoraccountinginfo.creationinfo,
       iobvendoraccountinginfo.modificationinfo,
       hobglaccount.code                                               AS accountcode,
       hobglaccount.name                                               AS accountname,
       leafglaccount.leadger                                           AS accountleadger,
       (SELECT masterdata.code
        FROM masterdata
        WHERE (masterdata.id = iobvendoraccountinginfo.refinstanceid)) AS vendorcode,
       (SELECT masterdata.name
        FROM masterdata
        WHERE (masterdata.id = iobvendoraccountinginfo.refinstanceid)) AS vendorname,
       (SELECT mobvendorgeneralmodel.purchaseunitscodes
        FROM mobvendorgeneralmodel
        WHERE (mobvendorgeneralmodel.id = iobvendoraccountinginfo.refinstanceid)) AS vendorpurchaseunitcodes
FROM iobvendoraccountinginfo
         full outer join  hobglaccount ON iobvendoraccountinginfo.chartofaccountid = hobglaccount.id
         full outer join leafglaccount on hobglaccount.id = leafglaccount.id
where iobvendoraccountinginfo.id is not null;


