drop view IF EXISTS iobvendoraccountinginfogeneralmodel;
drop table IF EXISTS CObVendor;
drop table IF EXISTS IObVendorAccountingInfo;
drop sequence IF EXISTS IObVendorAccountingInfoSeq;


create sequence IObVendorAccountingInfoSeq
    increment by 1
    minvalue 1
    NO MAXVALUE
    START with 1;


create TABLE IObVendorAccountingInfo
(
    id               INT8 DEFAULT nextval('IObVendorAccountingInfoSeq' :: REGCLASS),
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    chartOfAccountId   INT8          NOT NULL,
    primary key (id)
);

alter table IObVendorAccountingInfo
    add CONSTRAINT chartOfAccountId_chartOfAccountId_fk FOREIGN KEY (chartOfAccountId) REFERENCES hobglaccount (id) ON update CASCADE ON delete RESTRICT;

alter table IObVendorAccountingInfo
    add CONSTRAINT chartOfAccountId_refInstanceId_fk FOREIGN KEY (refInstanceId) REFERENCES MObVendor (id) ON update CASCADE ON delete CASCADE;

CREATE TABLE CObVendor
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024),
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE MObVendor ADD COLUMN typeId INT8;
ALTER TABLE MObVendor DROP COLUMN vendorType;
ALTER TABLE MObVendor ALTER COLUMN oldvendornumber DROP NOT NULL;
ALTER TABLE MObVendor ALTER COLUMN purchaseresponsibleid DROP NOT NULL;

ALTER TABLE MObVendor
    ADD CONSTRAINT FK_CObVendor_MObVendor FOREIGN KEY (typeId)
        REFERENCES CObVendor (id) ON DELETE RESTRICT;