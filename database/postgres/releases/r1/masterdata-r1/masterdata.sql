drop view if exists IObAlternativeUoMGeneralModel CASCADE ;
drop view if exists itemvendorrecordunitofmeasuresgeneralmodel CASCADE;

create view IObAlternativeUoMGeneralModel as
select iobalternativeuom.id,
       iobalternativeuom.creationdate,
       iobalternativeuom.modifieddate,
       iobalternativeuom.creationinfo,
       iobalternativeuom.modificationinfo,
       iobalternativeuom.olditemnumber,
       iobalternativeuom.conversionfactor,
       iobalternativeuom.alternativeunitofmeasureid,
       cobmeasure.name                                                    as alternativeunitofmeasurename,
       cobmeasure.symbol                                                  as alternativeunitofmeasuresymbol,
       cobmeasure.code                                                    as alternativeunitofmeasurecode,
       (select mobitemgeneralmodel.basicunitofmeasurecode
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as basicunitofmeasurecode,
        (select mobitemgeneralmodel.basicunitofmeasurename
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as basicunitofmeasurename,
        (select mobitemgeneralmodel.basicunitofmeasuresymbol
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as basicunitofmeasuresymbol,
     (select mobitemgeneralmodel.code
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as itemcode,
     (select mobitemgeneralmodel.name
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as itemname,
       (select mobitemgeneralmodel.id
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as itemid
from (iobalternativeuom
         left join cobmeasure on ((cobmeasure.id = iobalternativeuom.alternativeunitofmeasureid)));

create view itemvendorrecordunitofmeasuresgeneralmodel as
SELECT iobalternativeuomgeneralmodel.id,
       iobalternativeuomgeneralmodel.olditemnumber,
       iobalternativeuomgeneralmodel.conversionfactor,
       iobalternativeuomgeneralmodel.alternativeunitofmeasurename,
       iobalternativeuomgeneralmodel.alternativeunitofmeasuresymbol,
       iobalternativeuomgeneralmodel.alternativeunitofmeasurecode,
       iobalternativeuomgeneralmodel.itemcode,
       itemvendorrecordgeneralmodel.measureid,
       itemvendorrecordgeneralmodel.price,
       itemvendorrecordgeneralmodel.currencycode,
       itemvendorrecordgeneralmodel.vendorcode,
       itemvendorrecordgeneralmodel.itemvendorcode AS itemcodeatvendor,
       itemvendorrecordgeneralmodel.purchaseunitcode,
       itemvendorrecordgeneralmodel.id             as itemvendorrecordid
FROM (itemvendorrecordgeneralmodel
         JOIN iobalternativeuomgeneralmodel ON ((itemvendorrecordgeneralmodel.itemid =
                                                 iobalternativeuomgeneralmodel.itemid)
    AND itemvendorrecordgeneralmodel.measureid =
        iobalternativeuomgeneralmodel.alternativeunitofmeasureid));
