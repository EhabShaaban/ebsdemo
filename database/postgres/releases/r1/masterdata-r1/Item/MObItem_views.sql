drop view if exists MObItemGeneralModel CASCADE;
drop view if exists IObItemAccountingInfoGeneralModel CASCADE;
drop view if exists MObItemUOMGeneralModel CASCADE;

create view MObItemGeneralModel as
select MasterData.id,
       MasterData.code,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.currentstates,
       MasterData.name,
       MObItem.oldItemNumber,
       MObItem.itemgroup,
       MObItem.basicunitofmeasure,
       MObItem.marketname,
       CObItem.code as typeCode,
       CObItem.name as typeName,
       (select cobmeasure.code
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasurecode,
       (select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasuresymbol,
       (select cobmeasure.name
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasurename,
       MObItem.batchmanagementrequired,
       (select CObMaterialGroup.name
        from CObMaterialGroup
        where MObItem.itemgroup = CObMaterialGroup.id
       ) as itemgroupname,
       (select CObMaterialGroup.code
        from CObMaterialGroup
        where MObItem.itemgroup = CObMaterialGroup.id
       ) as itemgroupcode,
       (select string_agg(cobenterprise.code, ', ')
        from cobenterprise
        where cobenterprise.id in (select iobitempurchaseunit.purchaseunitid
                                   from iobitempurchaseunit
                                   where iobitempurchaseunit.refInstanceId = MObItem.id)
          and cobenterprise.objectTypeCode = '5'
       ) as purchaseunitscodes,
       (select array_to_json(array_agg(cobenterprise.name))
        from cobenterprise
        where cobenterprise.id in (select iobitempurchaseunit.purchaseunitid
                                   from iobitempurchaseunit
                                   where iobitempurchaseunit.refInstanceId = MObItem.id)
          and cobenterprise.objectTypeCode = '5'
       ) as purchaseunitsnames
from mobitem
         left join MasterData on mobitem.id = MasterData.id
         left join cobitem on mobitem.typeid = cobitem.id;


create view IObItemAccountingInfoGeneralModel as
select
    IObItemAccountingInfo.id,
    IObItemAccountingInfo.refInstanceId,
    IObItemAccountingInfo.creationDate,
    IObItemAccountingInfo.modifiedDate,
    IObItemAccountingInfo.creationInfo,
    IObItemAccountingInfo.modificationInfo,
    hobglaccount.code as accountCode,
    hobglaccount.name as accountName,
    (select
         MasterData.code
     from MasterData
     where MasterData.id = refInstanceId
    )
                      as itemCode
from IObItemAccountingInfo
         join hobglaccount on IObItemAccountingInfo.chartOfAccountId = hobglaccount.id;


CREATE VIEW MObItemUOMGeneralModel AS
SELECT ROW_NUMBER() OVER () AS id, *
from (
         SELECT cobmeasure.creationdate,
                cobmeasure.creationinfo,
                cobmeasure.modificationinfo,
                cobmeasure.modifieddate,
                cobmeasure.code,
                cobmeasure.symbol,
                masterdata.code              AS itemcode,
                mobitemgeneralmodel.typecode AS itemType
         FROM mobitemgeneralmodel
                  JOIN masterdata ON mobitemgeneralmodel.id = masterdata.id
                  JOIN cobmeasure ON mobitemgeneralmodel.basicunitofmeasure = cobmeasure.id
         UNION ALL
         SELECT cobmeasure.creationdate,
                cobmeasure.creationinfo,
                cobmeasure.modificationinfo,
                cobmeasure.modifieddate,
                cobmeasure.code,
                cobmeasure.symbol,
                masterdata.code              AS itemcode,
                mobitemgeneralmodel.typecode AS itemType
         FROM iobalternativeuom
                  JOIN mobitemgeneralmodel ON iobalternativeuom.refinstanceid = mobitemgeneralmodel.id
                  JOIN masterdata ON mobitemgeneralmodel.id = masterdata.id
                  JOIN cobmeasure ON iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
     ) As MObItemUOMGeneralModel

