drop table if exists CObItem;

CREATE TABLE CObItem
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP With time zone NOT NULL,
    modifiedDate     TIMESTAMP With time zone NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentstates    VARCHAR(1024),
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE MObItem ADD COLUMN marketName VARCHAR(250);
ALTER TABLE MObItem ADD COLUMN typeId INT8;
ALTER TABLE MObItem ALTER COLUMN itemGroup DROP NOT NULL ;
ALTER TABLE MObItem ALTER COLUMN batchManagementRequired DROP NOT NULL ;

ALTER TABLE MObItem
    ADD CONSTRAINT FK_CObItem_MObItem FOREIGN KEY (typeId)
        REFERENCES CObItem (id) ON DELETE RESTRICT;

drop view IF EXISTS iobitemaccountinginfogeneralmodel;

drop table IF EXISTS IObItemAccountingInfo;
drop sequence IF EXISTS IObItemAccountingInfoSeq;

create sequence IObItemAccountingInfoSeq
    increment by 1
    minvalue 1
    NO MAXVALUE
    START with 1;

create TABLE IObItemAccountingInfo
(
    id               INT8 DEFAULT nextval('IObItemAccountingInfoSeq' :: REGCLASS),
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    chartOfAccountId   INT8          NOT NULL,
    primary key (id)
);


alter table IObItemAccountingInfo
    add CONSTRAINT refInstanceId FOREIGN KEY (refInstanceId) REFERENCES MObItem (id) ON update CASCADE ON delete CASCADE;
alter table IObItemAccountingInfo
    add CONSTRAINT chartOfAccountId_fk FOREIGN KEY (chartOfAccountId) REFERENCES hobglaccount (id) ON update CASCADE ON delete RESTRICT;




