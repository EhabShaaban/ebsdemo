DROP VIEW IF Exists DObGoodsIssueGeneralModel;
DROP VIEW IF Exists IObGoodsIssueConsigneeGeneralModel;
DROP VIEW IF Exists MObCustomerGeneralModel;
DROP VIEW IF Exists IObCustomerBusinessUnitGeneralModel;
DROP VIEW IF Exists IObCustomerAddressGeneralModel;
DROP VIEW IF Exists IObCustomerContactPersonGeneralModel;

create view MObCustomerGeneralModel as
select MObCustomer.id,
       MasterData.creationDate,
       MasterData.modifiedDate,
       MasterData.creationInfo,
       MasterData.modificationInfo,
       MasterData.currentStates,
       MasterData.code    as code,
       MasterData.name,
       cobenterprise.code as kapCode,
       cobenterprise.name as kapName
from MObCustomer
         LEFT JOIN MasterData on MObCustomer.id = MasterData.id AND MasterData.objecttypecode = '3'
         LEFT JOIN cobenterprise on MObCustomer.kapId = cobenterprise.id;

create view IObCustomerBusinessUnitGeneralModel as
SELECT row_number() over ()               as id,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.currentstates,
       MasterData.code,
       MObCustomer.id                     as customerId,
       MasterData.name                    as customerName,
       cobenterprise.code                 as businessUnitcode,
       cobenterprise.name:: json ->> 'en' as businessUnitName,
       cobenterprise.name:: json ->> 'en' as purchaseUnitName
From masterdata
         join MObCustomer on masterdata.id = MObCustomer.id
    AND masterdata.objecttypecode = '3'
         join iobcustomerbusinessunit on iobcustomerbusinessunit.refinstanceid = MObCustomer.id
         join cobenterprise on cobenterprise.id = iobcustomerbusinessunit.businessunitid;

CREATE VIEW IObCustomerAddressGeneralModel AS
SELECT iobcustomeraddress.id,
       iobcustomeraddress.creationinfo,
       iobcustomeraddress.creationdate,
       iobcustomeraddress.modificationinfo,
       iobcustomeraddress.modifieddate,
       masterData.currentstates,
       masterData.code,
       masterData.name,
       address.addressname AS addressName,
       address.address     AS addressDescription
FROM iobcustomeraddress
         JOIN lobaddress address on iobcustomeraddress.addressid = address.id
         JOIN mobcustomer customer on iobcustomeraddress.refinstanceid = customer.id
         JOIN masterdata masterData on customer.id = masterData.id;

CREATE VIEW IObCustomerContactPersonGeneralModel AS
SELECT iobcustomercontactperson.id,
       iobcustomercontactperson.creationinfo,
       iobcustomercontactperson.creationdate,
       iobcustomercontactperson.modificationinfo,
       iobcustomercontactperson.modifieddate,
       masterData.currentstates,
       masterData.code,
       masterData.name As customerName,
       contactperson.name AS contactPersonName
FROM iobcustomercontactperson
         JOIN lobcontactperson contactperson on iobcustomercontactperson.contactpersonid = contactperson.id
         JOIN mobcustomer customer on iobcustomercontactperson.refinstanceid = customer.id
         JOIN masterdata masterData on customer.id = masterData.id;

CREATE VIEW IObGoodsIssueConsigneeGeneralModel AS
select IObGoodsIssueConsignee.id,
       IObGoodsIssueConsignee.refInstanceId,
       IObGoodsIssueConsignee.comments,
       (SELECT DObGoodsIssue.code
        FROM DObGoodsIssue
        WHERE IObGoodsIssueConsignee.refInstanceId =
              DObGoodsIssue.id)                                                AS goodsIssueCode,
       (select masterdata.name
        from masterdata
        where IObGoodsIssueConsignee.customerId = masterdata.id)               as customerName,
       (select masterdata.code
        from masterdata
        where IObGoodsIssueConsignee.customerId = masterdata.id)               as customerCode,
       (select lobaddress.address
        from lobaddress
        where IObGoodsIssueConsignee.addressId = lobaddress.id)::json ->> 'en' as address,
       (select lobcontactperson.name
        from lobcontactperson
        where IObGoodsIssueConsignee.contactPersonId =
              lobcontactperson.id)                                             as contactPersonName,
       (select lobcontactperson.name:: json ->> 'en'
        from lobcontactperson
        where IObGoodsIssueConsignee.contactPersonId =
              lobcontactperson.id)                                             as contactPersonNameEn,
       (select ebsuser.username
        from ebsuser
        where IObGoodsIssueConsignee.salesRepresentativeId = ebsuser.id)       as salesRepresentativeName,
       (select MObCustomerGeneralModel.kapCode
        from MObCustomerGeneralModel
        where IObGoodsIssueConsignee.customerId = MObCustomerGeneralModel.id)  as kapCode,
       (select MObCustomerGeneralModel.kapName
        from MObCustomerGeneralModel
        where IObGoodsIssueConsignee.customerId = MObCustomerGeneralModel.id)  as kapName,
       (select dobsalesinvoice.code
        from dobsalesinvoice
        where IObGoodsIssueSalesInvoiceConsignee.salesInvoiceId =
              dobsalesinvoice.id)                                              as salesInvoice
from IObGoodsIssueConsignee
         LEFT JOIN IObGoodsIssueSalesInvoiceConsignee
                   on IObGoodsIssueConsignee.id = IObGoodsIssueSalesInvoiceConsignee.id;


CREATE VIEW DObGoodsIssueGeneralModel AS
SELECT DObGoodsIssue.id,
       DObGoodsIssue.code,
       DObGoodsIssue.creationDate,
       DObGoodsIssue.modifiedDate,
       DObGoodsIssue.creationInfo,
       DObGoodsIssue.modificationInfo,
       DObGoodsIssue.currentStates,
       (select cobenterprise.name
        from cobenterprise
        where DObGoodsIssue.purchaseUnitId = cobenterprise.id) as purchaseUnitName,
       (select cobenterprise.name:: json ->> 'en'
        from cobenterprise
        where DObGoodsIssue.purchaseUnitId = cobenterprise.id) as purchaseUnitNameEn,
       (select cobenterprise.code
        from cobenterprise
        where DObGoodsIssue.purchaseUnitId = cobenterprise.id) as purchaseUnitCode,
       (select cobgoodsissue.name
        from cobgoodsissue
        where DObGoodsIssue.typeid = cobgoodsissue.id)         as typeName,
       (select cobgoodsissue.name:: json ->> 'en'
        from cobgoodsissue
        where DObGoodsIssue.typeid = cobgoodsissue.id)         as typeNameEn,
       (select cobgoodsissue.code
        from cobgoodsissue
        where DObGoodsIssue.typeid = cobgoodsissue.id)         as typeCode,
       IObGoodsIssueStoreGeneralModel.storehouseName,
       IObGoodsIssueStoreGeneralModel.storehouseCode,
       IObGoodsIssueConsigneeGeneralModel.customerName,
       IObGoodsIssueConsigneeGeneralModel.customerCode,
       IObGoodsIssueStoreGeneralModel.companyCode,
       IObGoodsIssueStoreGeneralModel.companyName,
       IObGoodsIssueStoreGeneralModel.plantCode,
       IObGoodsIssueStoreGeneralModel.plantName,
       IObGoodsIssueStoreGeneralModel.storekeeperCode,
       IObGoodsIssueStoreGeneralModel.storekeeperName

FROM DObGoodsIssue
         left join IObGoodsIssueStoreGeneralModel
                   on DObGoodsIssue.id = IObGoodsIssueStoreGeneralModel.refInstanceId
         left join IObGoodsIssueConsigneeGeneralModel
                   on DObGoodsIssue.id = IObGoodsIssueConsigneeGeneralModel.refInstanceId;

