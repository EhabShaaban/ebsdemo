DROP VIEW IF EXISTS MObItemUOMGeneralModel;

CREATE VIEW MObItemUOMGeneralModel AS
SELECT ROW_NUMBER() OVER () AS id, *
from (
         SELECT cobmeasure.creationdate,
                cobmeasure.creationinfo,
                cobmeasure.modificationinfo,
                cobmeasure.modifieddate,
                cobmeasure.code,
                cobmeasure.symbol,
                masterdata.code              AS itemcode,
                masterdata.name              AS itemName,
                mobitemgeneralmodel.typecode AS itemType
         FROM mobitemgeneralmodel
                  JOIN masterdata ON mobitemgeneralmodel.id = masterdata.id
                  JOIN cobmeasure ON mobitemgeneralmodel.basicunitofmeasure = cobmeasure.id
         UNION ALL
         SELECT cobmeasure.creationdate,
                cobmeasure.creationinfo,
                cobmeasure.modificationinfo,
                cobmeasure.modifieddate,
                cobmeasure.code,
                cobmeasure.symbol,
                masterdata.code              AS itemCode,
                masterdata.name              AS itemName,
                mobitemgeneralmodel.typecode AS itemType
         FROM iobalternativeuom
                  JOIN mobitemgeneralmodel ON iobalternativeuom.refinstanceid = mobitemgeneralmodel.id
                  JOIN masterdata ON mobitemgeneralmodel.id = masterdata.id
                  JOIN cobmeasure ON iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
     ) As MObItemUOMGeneralModel

