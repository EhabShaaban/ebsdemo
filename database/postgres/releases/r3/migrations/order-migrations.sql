INSERT INTO IObOrderDocumentRequiredDocuments(refInstanceId, creationDate, modifiedDate,
                                              creationInfo, modificationInfo, numberOfCopies,
                                              attachmentTypeId)
SELECT ioborderrequireddocuments.refinstanceid,
       ioborderrequireddocuments.creationDate,
       ioborderrequireddocuments.modifiedDate,
       ioborderrequireddocuments.creationInfo,
       ioborderrequireddocuments.modificationInfo,
       ioborderrequireddocuments.numberOfCopies,
       ioborderrequireddocuments.attachmentTypeId
FROM ioborderrequireddocuments;

DROP VIEW ioborderrequireddocumentsgeneralmodel;
DROP TABLE ioborderrequireddocuments;


INSERT INTO doborderdocument(id, code, creationdate, modifieddate, creationinfo, modificationinfo,
                             currentstates, documentownerid, objecttypecode)
SELECT id,
       code,
       creationdate,
       modifieddate,
       creationinfo,
       modificationinfo,
       currentstates,
       1,
       'PurchaseOrder'::VARCHAR
FROM DObPurchaseOrder;


UPDATE doborderdocument
SET objecttypecode = 'IMPORT_PO'
FROM dobpurchaseorder
WHERE doborderdocument.id = dobpurchaseorder.id
  AND ordertypeid = 1;

UPDATE doborderdocument
SET objecttypecode = 'LOCAL_PO'
FROM dobpurchaseorder
WHERE doborderdocument.id = dobpurchaseorder.id
  AND ordertypeid = 2;

INSERT INTO iobpurchaseorderfulfillervendor(creationdate, modifieddate, creationinfo,
                                            modificationinfo, refinstanceid,
                                            vendorid)
SELECT now(), now(), 'Admin', 'Admin', id, vendorid
FROM dobpurchaseorder;

insert into iobordercompany (creationdate, modifieddate, creationinfo, modificationinfo,
                             refinstanceid, companyid,
                             businessunitid, partyrole, bankaccountid)
select doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       dobpurchaseorder.id,
       iobenterprisedata.enterpriseid,
       dobpurchaseorder.purchaseunitid,
       'BILL_TO',
       (select iobcompanybankdetails.id
        from iobcompanybankdetails
        where iobordercompanydatadetails.bankaccount = iobcompanybankdetails.accountno
          and iobordercompanydatadetails.payerbankdetailsid = iobcompanybankdetails.bankid)
from dobpurchaseorder
         join
     doborderdocument
     on doborderdocument.id = dobpurchaseorder.id
         left join iobenterprisedata on dobpurchaseorder.id = iobenterprisedata.refinstanceid and
                                        iobenterprisedata.enterprisetype = 'Company'
         left join iobordercompanydatadetails
                   on iobordercompanydatadetails.refinstanceid = dobpurchaseorder.id;


ALTER TABLE DObPurchaseOrder
    DROP COLUMN code,
    DROP COLUMN creationdate,
    DROP COLUMN modifieddate,
    DROP COLUMN creationinfo,
    DROP COLUMN modificationinfo,
    DROP COLUMN currentstates,
    DROP COLUMN version,
    DROP COLUMN vendorId,
    DROP COLUMN purchaseUnitId,
    DROP COLUMN purchaseResponsibleId,
    DROP COLUMN vendorName,
    DROP COLUMN purchaseUnitName,
    DROP COLUMN ordertypeid,
    DROP COLUMN ordertypecode,
    DROP COLUMN ordertypename,
    DROP COLUMN purchaseResponsibleName;

DROP VIEW cobordertypegeneralmodel;
DROP TABLE cobordertype;
DROP TABLE iobordercompanydatadetails;
DROP TABLE ioborderheader;



