DROP TABLE IF EXISTS IObConsigneeCompany;
DROP TABLE IF EXISTS IObPurchaseOrderBillToCompany;
DROP TABLE IF EXISTS IObOrderCompany;
DROP TABLE IF EXISTS IObPurchaseOrderFulfillerVendor;
DROP TABLE IF EXISTS IObOrderItem;
DROP TABLE IF EXISTS IObOrderPaymentDetails;
DROP TABLE IF EXISTS IObOrderDeliveryTerm;
DROP TABLE IF EXISTS IObOrderTax;

CREATE TABLE IObOrderCompany
(
    id               BIGSERIAL                NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    refinstanceId    INT8 REFERENCES DObOrderDocument (id) ON DELETE CASCADE,
    companyId        INT8 REFERENCES CObCompany (id) ON DELETE RESTRICT,
    businessUnitId   INT8 REFERENCES CObEnterprise (id) ON DELETE RESTRICT,
    bankAccountId    INT8 REFERENCES iobcompanybankdetails (id) ON DELETE RESTRICT,
    partyRole        VARCHAR(1024)            NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObPurchaseOrderFulfillerVendor
(
    id               BIGSERIAL                NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    refinstanceId    INT8 REFERENCES DObPurchaseOrder (id) ON DELETE CASCADE,
    vendorId         INT8 REFERENCES MObVendor (id) ON DELETE RESTRICT,
    referencePoId    INT8 REFERENCES DObPurchaseOrder (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderItem
(
    id               BIGSERIAL                NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    refinstanceId    INT8 REFERENCES DObOrderDocument (id) ON DELETE CASCADE,
    itemId           INT8 REFERENCES MObItem (id) ON DELETE RESTRICT,
    orderUnitId      INT8 REFERENCES CObMeasure (id) ON DELETE RESTRICT,
    quantity         DOUBLE PRECISION,
    price            DOUBLE PRECISION,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderPaymentDetails
(
    id               BIGSERIAL                NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    refinstanceId    INT8 REFERENCES DObOrderDocument (id) ON DELETE CASCADE,
    paymentTermId    INT8 REFERENCES CObPaymentTerms (id) ON DELETE RESTRICT,
    currencyId       INT8 REFERENCES CObCurrency (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderDeliveryTerm
(
    id                    BIGSERIAL                NOT NULL,
    creationDate          TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate          TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo          VARCHAR(1024)            NOT NULL,
    modificationInfo      VARCHAR(1024)            NOT NULL,
    refinstanceId         INT8 REFERENCES DObOrderDocument (id) ON DELETE CASCADE,
    incotermId            INT8 REFERENCES lobsimplematerialdata (id) ON DELETE RESTRICT,
    loadingPortId         INT8 REFERENCES localizedlobmaterial (id) ON DELETE RESTRICT,
    dischargePortId       INT8 REFERENCES localizedlobmaterial (id) ON DELETE RESTRICT,
    modeOfTransportId     INT8 REFERENCES localizedlobshipping (id) ON DELETE RESTRICT,
    shippingInstructionId INT8 REFERENCES cobshipping (id) ON DELETE RESTRICT,
    expectedDeliveryDate  TIMESTAMP WITH TIME ZONE NOT NULL,
    noOfContainers        INT8                     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObOrderTax
(
    id               BIGSERIAL                NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    refinstanceId    INT8 REFERENCES DObOrderDocument (id) ON DELETE CASCADE,
    code             VARCHAR(1024)            NOT NULL,
    name             JSON                     NOT NULL,
    percentage       DOUBLE PRECISION         NOT NULL,
    PRIMARY KEY (id)
);
