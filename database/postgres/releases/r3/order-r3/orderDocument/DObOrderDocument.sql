DROP TABLE IF EXISTS IObOrderDocumentRequiredDocuments;
DROP TABLE IF EXISTS DObOrderDocument;

CREATE TABLE DObOrderDocument
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentStates    VARCHAR(1024)            NOT NULL,
    objectTypeCode   VARCHAR(1024)            NOT NULL,
    documentOwnerId  INT8 REFERENCES ebsuser (id) ON DELETE RESTRICT DEFAULT 1,
    PRIMARY KEY (id)
);

ALTER TABLE doborder
    ADD CONSTRAINT DObPurchaseOrder_DObOrderDocument
        FOREIGN KEY (id) REFERENCES DObOrderDocument (id) ON DELETE CASCADE;

CREATE TABLE IObOrderDocumentRequiredDocuments
(
    id               BIGSERIAL                NOT NULL,
    refInstanceId    INT8                     NOT NULL REFERENCES DObOrderDocument (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    numberOfCopies   INT8,
    attachmentTypeId INT8 REFERENCES lobattachmenttype (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);

ALTER TABLE doborder
    RENAME TO DObPurchaseOrder;
