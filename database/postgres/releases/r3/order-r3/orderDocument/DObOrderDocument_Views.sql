DROP VIEW IF EXISTS IObOrderDocumentRequiredDocumentsGeneralModel;

CREATE VIEW IObOrderDocumentRequiredDocumentsGeneralModel AS
SELECT ioborderdocumentrequireddocuments.id,
       doborderdocument.code,
       ioborderdocumentrequireddocuments.creationdate,
       ioborderdocumentrequireddocuments.modifieddate,
       ioborderdocumentrequireddocuments.creationinfo,
       ioborderdocumentrequireddocuments.modificationinfo,
       doborderdocument.objecttypecode,
       ioborderdocumentrequireddocuments.numberOfCopies,
       (SELECT lobattachmenttype.name
        FROM lobattachmenttype
        WHERE ioborderdocumentrequireddocuments.attachmentTypeId =
              lobattachmenttype.id) AS attachmentname,
       (SELECT lobattachmenttype.code
        FROM lobattachmenttype
        WHERE ioborderdocumentrequireddocuments.attachmentTypeId =
              lobattachmenttype.id) AS attachmentcode
FROM doborderdocument
         RIGHT JOIN ioborderdocumentrequireddocuments
                    ON doborderdocument.id = ioborderdocumentrequireddocuments.refinstanceid;

-- ---------------------------------------------------------------
DROP VIEW IF EXISTS IObGoodsReceiptSalesReturnDataGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptAccountingDocumentDetailsGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptReceivedItemsGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptPurchaseOrderDataGeneralModel;
DROP VIEW IF EXISTS DObGoodsReceiptDataGeneralModel;
DROP VIEW IF EXISTS DObGoodsReceiptGeneralModel;

DROP VIEW IF EXISTS DObPurchaseOrderTotalAmountGeneralModel;
DROP VIEW IF EXISTS IObOrderLineDetailsSummaryGeneralModel;
DROP VIEW IF EXISTS IObOrderLineDetailsGeneralModel;
DROP VIEW IF EXISTS IObOrderLineDetailsQuantitiesGeneralModel;
DROP VIEW IF EXISTS IObPurchaseOrderEstimatedCostGeneralModel;

DROP VIEW IF EXISTS IObPurchaseOrderItemsGeneralModel;
DROP VIEW IF EXISTS IObVendorInvoiceDetailsGeneralModel;
DROP VIEW IF EXISTS IObVendorInvoiceRemainingGeneralModel;
DROP VIEW IF EXISTS DObVendorInvoiceGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestPaymentDetailsGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestAccountDetailsGeneralModel;
DROP VIEW IF EXISTS IObGoodsReceiptAccountingDetailsGeneralModel;
DROP VIEW IF EXISTS SubAccountGeneralModel;

DROP VIEW IF EXISTS SubAccount;
DROP VIEW IF EXISTS IObOrderApproverGeneralModel;
DROP VIEW IF EXISTS IObOrderApprovalCycleGeneralModel;
DROP VIEW IF EXISTS DObPurchaseOrderAttachmentGeneralModel;
DROP VIEW IF EXISTS DObPurchaseOrderConsigneeSectionGeneralModel;
DROP VIEW IF EXISTS DObPurchaseOrderCompanySectionGeneralModel;
DROP VIEW IF EXISTS IObOrderCompanyGeneralModel;

DROP VIEW IF EXISTS DObPurchaseOrderHeaderPDFData CASCADE;
DROP VIEW IF EXISTS DObPurchaseOrderVendorPDFData CASCADE;
DROP VIEW IF EXISTS DObPurchaseOrderConsigneePDFData CASCADE;
DROP VIEW IF EXISTS DObPurchaseOrderCompanyPDFData CASCADE;
DROP VIEW IF EXISTS PaymentAndDeliveryGeneralModel;
DROP VIEW IF EXISTS DObPurchaseOrderCycleDatesGeneralModel;
DROP VIEW IF EXISTS DObPurchaseOrderGeneralModel;
DROP VIEW IF EXISTS IObOrderItemSummaryGeneralModel;

-- -------------------------------------------------------------------------------------------------

CREATE VIEW DObPurchaseOrderGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       doborderdocument.documentownerid,
       userinfo.name                      AS documentOwnerName,
       iobordercompany.businessunitid     AS purchaseUnitId,
       businessUnit.code                  AS purchaseUnitCode,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       iobpurchaseorderfulfillervendor.vendorid,
       vendor.code                        AS vendorCode,
       vendor.name                        AS vendorName,
       cobcurrency.code                   AS currencyCode,
       cobcurrency.iso                    AS currencyISO,
       cobcurrency.name                   AS currencyName,
       refDocument.code                   As refDocumentCode,
       refDocument.objecttypecode         As refDocumentType
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON dobpurchaseorder.id = iobpurchaseorderfulfillervendor.refinstanceid
         LEFT JOIN masterdata vendor
                   ON iobpurchaseorderfulfillervendor.vendorid = vendor.id
         LEFT JOIN cobenterprise businessUnit on iobordercompany.businessunitid = businessUnit.id
         JOIN userinfo ON doborderdocument.documentownerid = userinfo.id
         JOIN ebsuser ON doborderdocument.documentownerid = ebsuser.id
         left join ioborderpaymenttermsdetails
                   on dobpurchaseorder.id = ioborderpaymenttermsdetails.refinstanceid
         left join cobcurrency on ioborderpaymenttermsdetails.currencyid = cobcurrency.id
         left join doborderdocument refDocument
                   on iobpurchaseorderfulfillervendor.referencepoid = refDocument.id;



CREATE VIEW IObOrderLineDetailsQuantitiesGeneralModel AS
SELECT Quantities.id,
       Quantities.refinstanceid,
       Quantities.quantity,
       Quantities.price,
       Quantities.discountpercentage,
       round(case
                 when ioborderlinedetails.unitOfMeasureId = Quantities.orderunitid
                     then Quantities.quantity
                 else
                     (Quantities.quantity * (IObAlternativeUoM.conversionFactor)) end :: numeric,
             3)                                                 AS quantityinbase,
       (SELECT CObMeasure.symbol
        FROM CObMeasure
        WHERE Quantities.orderunitid = CObMeasure.id)           AS orderunitsymbol,
       (SELECT cobmeasure_1.code
        FROM cobmeasure cobmeasure_1
        WHERE (quantities.orderunitid = cobmeasure_1.id))       AS orderunitcode,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM ItemVendorRecordGeneralModel
                 JOIN DObPurchaseOrderGeneralModel
                      ON ItemVendorRecordGeneralModel.vendorid =
                         DObPurchaseOrderGeneralModel.vendorId
                          AND ItemVendorRecordGeneralModel.purchaseunitid =
                              DObPurchaseOrderGeneralModel.purchaseunitid
                 JOIN IObOrderLineDetails
                      ON ItemVendorRecordGeneralModel.itemid = IObOrderLineDetails.itemid
                 JOIN IObOrderLineDetailsQuantities
                      ON ItemVendorRecordGeneralModel.measureid =
                         IObOrderLineDetailsQuantities.orderunitid
        WHERE DObPurchaseOrderGeneralModel.id = IObOrderLineDetails.refinstanceid
          AND IObOrderLineDetails.id = IObOrderLineDetailsQuantities.refinstanceid
          AND IObOrderLineDetailsQuantities.id = Quantities.id) AS itemCodeAtVendor
FROM IObOrderLineDetailsQuantities AS Quantities
         LEFT JOIN IObOrderLineDetails
                   ON IObOrderLineDetails.id = Quantities.refinstanceid
         LEFT JOIN IObAlternativeUoM
                   ON IObOrderLineDetails.itemid = IObAlternativeUoM.refinstanceid
                       AND IObAlternativeUoM.alternativeunitofmeasureid = Quantities.orderunitid
         LEFT JOIN CObMeasure ON IObOrderLineDetails.unitofmeasureid = CObMeasure.id;


CREATE VIEW IObOrderLineDetailsGeneralModel AS
SELECT ioborderlinedetails.id,
       ioborderlinedetails.creationInfo,
       ioborderlinedetails.creationDate,
       ioborderlinedetails.modificationInfo,
       ioborderlinedetails.modifiedDate,
       doborderdocument.code                                           AS ordercode,
       mobitemgeneralmodel.name,
       mobitemgeneralmodel.code                                        AS itemcode,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId)     as unitsymbol,
       (select SUM(IObOrderLineDetailsQuantitiesGeneralModel.quantityinbase) as quantity
        from IObOrderLineDetailsQuantitiesGeneralModel
        where ioborderlinedetails.id =
              IObOrderLineDetailsQuantitiesGeneralModel.refinstanceid) as itemQuantity,
       ioborderlinedetails.quantityindn,
       mobitemgeneralmodel.itemgroupname
FROM ioborderlinedetails
         LEFT JOIN doborderdocument ON doborderdocument.id = ioborderlinedetails.refinstanceid AND
                                       doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN mobitemgeneralmodel ON mobitemgeneralmodel.id = ioborderlinedetails.itemid;


CREATE VIEW IObOrderLineDetailsSummaryGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       businessUnit.code                                             AS businessUnitCode,
       businessUnit.name                                             AS businessUnitName,
       sum(ioborderlinedetailsquantitiesgeneralmodel.price *
           ioborderlinedetailsquantitiesgeneralmodel.quantityinbase) AS TotalAmount,
       sum((ioborderlinedetailsquantitiesgeneralmodel.discountpercentage *
            ioborderlinedetailsquantitiesgeneralmodel.quantityinbase *
            ioborderlinedetailsquantitiesgeneralmodel.price) / 100)  AS TotalDiscount
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
         LEFT JOIN cobenterprise businessUnit on iobordercompany.businessunitid = businessUnit.id
         LEFT JOIN ioborderlinedetailsgeneralmodel
                   ON doborderdocument.code = ioborderlinedetailsgeneralmodel.ordercode
         LEFT JOIN ioborderlinedetailsquantitiesgeneralmodel
                   ON ioborderlinedetailsgeneralmodel.id =
                      ioborderlinedetailsquantitiesgeneralmodel.refinstanceid
GROUP BY doborderdocument.id, businessUnit.id;

DROP FUNCTION IF EXISTS getDObOrderCompanyCurrencyInformation(integer);
CREATE OR REPLACE FUNCTION getDObOrderCompanyCurrencyInformation(dobOrderId INT8)
    RETURNS TABLE
            (
                currencyCode VARCHAR(1024),
                currencyIso  VARCHAR(4),
                currencyName JSON
            )
AS
$$
BEGIN
    RETURN QUERY (SELECT cobcurrency.code,
                         cobcurrency.iso,
                         cobcurrency.name
                  FROM iobordercompany
                           LEFT JOIN iobenterprisebasicdata
                                     ON iobordercompany.companyid =
                                        iobenterprisebasicdata.refinstanceid
                           LEFT JOIN cobcurrency
                                     ON iobenterprisebasicdata.currencyid = cobcurrency.id
                  WHERE iobordercompany.refinstanceid = dobOrderId);
END;
$$
    LANGUAGE PLPGSQL;

CREATE VIEW IObPurchaseOrderEstimatedCostGeneralModel AS
SELECT IObPurchaseOrderEstimatedCost.id,
       IObPurchaseOrderEstimatedCost.creationDate,
       IObPurchaseOrderEstimatedCost.modifiedDate,
       IObPurchaseOrderEstimatedCost.creationInfo,
       IObPurchaseOrderEstimatedCost.modificationInfo,
       DObPurchaseOrderGeneralModel.objecttypecode,
       CObEstimatedCostType.code,
       CObEstimatedCostType.name,
       IObPurchaseOrderEstimatedCost.amount,
       DObPurchaseOrderGeneralModel.code AS purchaseOrderCode,
       (SELECT currencyIso
        From getDObOrderCompanyCurrencyInformation(IObPurchaseOrderEstimatedCost.refInstanceId))
FROM IObPurchaseOrderEstimatedCost
         JOIN DObPurchaseOrderGeneralModel
              ON IObPurchaseOrderEstimatedCost.refInstanceId =
                 DObPurchaseOrderGeneralModel.id
         JOIN CObEstimatedCostType
              ON CObEstimatedCostType.id = IObPurchaseOrderEstimatedCost.estimatedCostTypeId;


CREATE VIEW DObPurchaseOrderTotalAmountGeneralModel AS
SELECT dobpurchaseordergeneralmodel.*,
       (CASE
            WHEN ioborderlinedetailssummarygeneralmodel.totalAmount IS NULL THEN 0
            ELSE ioborderlinedetailssummarygeneralmodel.totalAmount END -
        CASE
            WHEN ioborderlinedetailssummarygeneralmodel.totaldiscount IS NULL THEN 0
            ELSE ioborderlinedetailssummarygeneralmodel.totaldiscount END) AS TotalAmount
FROM ioborderlinedetailssummarygeneralmodel
         JOIN dobpurchaseordergeneralmodel
              ON ioborderlinedetailssummarygeneralmodel.id = dobpurchaseordergeneralmodel.id;



CREATE VIEW DObPurchaseOrderCycleDatesGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       doborderdocument.creationInfo,
       doborderdocument.creationDate,
       doborderdocument.modificationInfo,
       doborderdocument.modifiedDate,
       iobordercycledates.actualproductionfinisheddate AS productionFinishedDate,
       iobordercycledates.actualpIRequestedDate        AS pIRequestedDate,
       iobordercycledates.actualconfirmationDate       as confirmationDate,
       iobordercycledates.actualproductionFinishedDate as productionFnishedDate,
       iobordercycledates.actualshippingDate           as shippingDate,
       iobordercycledates.actualarrivalDate            as arrivalDate,
       iobordercycledates.actualclearanceDate          as clearanceDate,
       iobordercycledates.actualdeliveryCompleteDate   as deliveryCompleteDate,
       (SELECT ioborderapprovalcycle.enddate
        FROM ioborderapprovalcycle
                 LEFT JOIN DObPurchaseOrder
                           ON DObPurchaseOrder.id = ioborderapprovalcycle.refinstanceid
        WHERE ioborderapprovalcycle.finaldecision = 'APPROVED'
          AND ioborderapprovalcycle.refinstanceid =
              iobordercycledates.refinstanceid)        AS approvalDate
FROM doborderdocument
         JOIN DObPurchaseOrder ON doborderdocument.id = dobpurchaseorder.id
         JOIN iobordercycledates ON doborderdocument.id = iobordercycledates.refinstanceid AND
                                    objecttypecode LIKE '%_PO';


CREATE VIEW PaymentAndDeliveryGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       cobenterprise.name                                                                                 AS purchaseunitname,
       cobenterprise.code                                                                                 AS purchaseunitcode,
       ioborderdeliverydetails.incotermname,
       (SELECT lobsimplematerialdata.code
        FROM lobsimplematerialdata
        WHERE (ioborderdeliverydetails.incotermid =
               lobsimplematerialdata.id))                                                                 AS incotermcode,

       (SELECT ioborderpaymenttermsdetails.currencyName
        FROM ioborderpaymenttermsdetails
        WHERE (dobpurchaseorder.id =
               ioborderpaymenttermsdetails.refinstanceid))                                                AS currency,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE (cobcurrency.id = (SELECT ioborderpaymenttermsdetails.currencyid
                                 FROM ioborderpaymenttermsdetails
                                 WHERE dobpurchaseorder.id =
                                       ioborderpaymenttermsdetails.refinstanceid)))                       AS currencyiso,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE (cobcurrency.id = (SELECT ioborderpaymenttermsdetails.currencyid
                                 FROM ioborderpaymenttermsdetails
                                 WHERE dobpurchaseorder.id =
                                       ioborderpaymenttermsdetails.refinstanceid)))                       AS currencycode,

       (SELECT ioborderpaymenttermsdetails.paymenttermname
        FROM ioborderpaymenttermsdetails
        WHERE (dobpurchaseorder.id =
               ioborderpaymenttermsdetails.refinstanceid))                                                AS paymenttermname,
       (SELECT cobpaymentterms.code
        FROM cobpaymentterms
        WHERE (cobpaymentterms.id = (SELECT ioborderpaymenttermsdetails.paymenttermid
                                     FROM ioborderpaymenttermsdetails
                                     WHERE dobpurchaseorder.id =
                                           ioborderpaymenttermsdetails.refinstanceid)))                   AS paymenttermcode,

       ioborderdeliverydetails.shippinginstructionsname,
       (SELECT cobshipping.code
        FROM cobshipping
        WHERE (ioborderdeliverydetails.shippinginstructionsid =
               cobshipping.id))                                                                           AS shippinginstructionscode,

       (SELECT iobordercontainersdetails.quantity
        FROM iobordercontainersdetails
        WHERE (iobordercontainersdetails.refinstanceid =
               dobpurchaseorder.id))                                                                      AS containerno,

       (SELECT localizedlobmaterial.name
        FROM (iobordercontainersdetails
                 JOIN localizedlobmaterial
                      ON (((iobordercontainersdetails.containerid = localizedlobmaterial.id) AND
                           (iobordercontainersdetails.refinstanceid =
                            dobpurchaseorder.id)))))                                                      AS containername,
       (SELECT localizedlobmaterial.code
        FROM (iobordercontainersdetails
                 JOIN localizedlobmaterial
                      ON (((iobordercontainersdetails.containerid = localizedlobmaterial.id) AND
                           (iobordercontainersdetails.refinstanceid =
                            dobpurchaseorder.id)))))                                                      AS containercode,

       ioborderdeliverydetails.collectiondate,
       ioborderdeliverydetails.numberOfDays,

       ioborderdeliverydetails.modeoftransportname,
       (SELECT localizedlobshipping.code
        FROM localizedlobshipping
        WHERE (ioborderdeliverydetails.modeoftransportid =
               localizedlobshipping.id))                                                                  AS modeoftransportcode,

       (SELECT portscountry.countryname
        FROM portscountry
        WHERE (ioborderdeliverydetails.loadingportid = portscountry.id))                                  AS loadingcountry,
       (SELECT countrydefinition.isocode
        FROM countrydefinition
        WHERE (countrydefinition.id = (SELECT countryid
                                       FROM portscountry
                                       WHERE portscountry.id = ioborderdeliverydetails.loadingportid)))   AS loadingcountrycode,

       ioborderdeliverydetails.loadingportname,
       (SELECT localizedlobmaterial.code
        FROM localizedlobmaterial
        WHERE (ioborderdeliverydetails.loadingportid =
               localizedlobmaterial.id))                                                                  AS loadingportcode,

       (SELECT portscountry.countryname
        FROM portscountry
        WHERE (ioborderdeliverydetails.dischargeportid = portscountry.id))                                AS dischargecountry,
       (SELECT countrydefinition.isocode
        FROM countrydefinition
        WHERE (countrydefinition.id = (SELECT countryid
                                       FROM portscountry
                                       WHERE portscountry.id = ioborderdeliverydetails.dischargeportid))) AS dischargecountrycode,

       ioborderdeliverydetails.dischargeportname,
       (SELECT localizedlobmaterial.code
        FROM localizedlobmaterial
        WHERE (ioborderdeliverydetails.dischargeportid =
               localizedlobmaterial.id))                                                                  AS dischargeportcode
FROM (doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
         LEFT JOIN cobenterprise ON cobenterprise.id = iobordercompany.businessunitid
         LEFT JOIN ioborderdeliverydetails
                   ON ((dobpurchaseorder.id = ioborderdeliverydetails.refinstanceid)));


CREATE VIEW IObOrderCompanyGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       iobordercompany.partyrole,
       businessUnit.id                 AS purchaseUnitId,
       businessUnit.name               AS purchaseUnitName,
       businessUnit.code               AS purchaseUnitCode,
       company.id                      AS companyId,
       company.code                    AS companyCode,
       company.name                    AS companyName,
       (SELECT currencyCode From getDObOrderCompanyCurrencyInformation(dobpurchaseorder.id)),
       (SELECT currencyIso From getDObOrderCompanyCurrencyInformation(dobpurchaseorder.id)),
       (SELECT currencyName From getDObOrderCompanyCurrencyInformation(dobpurchaseorder.id)),
       IObCompanyBankDetails.id        AS bankAccountId,
       companyBank.code                AS bankCode,
       companyBank.name                AS bankName,
       IObCompanyBankDetails.accountno AS bankAccountNumber
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         JOIN iobordercompany ON dobpurchaseorder.id = iobordercompany.refinstanceid
         JOIN cobenterprise businessUnit ON businessUnit.id = iobordercompany.businessunitid
         LEFT JOIN IObCompanyBankDetails
                   ON IObCompanyBankDetails.id = iobordercompany.bankAccountId
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN cobenterprise company ON company.id = iobordercompany.companyid;



CREATE VIEW DObPurchaseOrderConsigneeSectionGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.currentstates,
       doborderdocument.objecttypecode,
       businessUnit.name                                             As purchasingunitName,
       businessUnit.code                                             AS purchasingunitCode,
       (select cobenterprise.name
        from cobenterprise
                 left join iobenterprisedata on iobenterprisedata.enterpriseid = cobenterprise.id
        where dobpurchaseorder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype like 'Consignee')     AS consigneeName,
       (SELECT code
        FROM iobenterprisedata
                 LEFT JOIN cobenterprise ON cobenterprise.id = iobenterprisedata.enterpriseid
        where iobenterprisedata.enterprisetype like 'Consignee'
          AND dobpurchaseorder.id = iobenterprisedata.refinstanceid) AS consigneeCode,
       (select cobenterprise.name
        from cobenterprise
                 left join iobenterprisedata on iobenterprisedata.enterpriseid = cobenterprise.id
        where dobpurchaseorder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype like 'Plant')         AS plantName,
       (SELECT code
        FROM iobenterprisedata
                 LEFT JOIN cobenterprise ON cobenterprise.id = iobenterprisedata.enterpriseid
        where iobenterprisedata.enterprisetype like 'Plant'
          AND dobpurchaseorder.id = iobenterprisedata.refinstanceid) AS plantCode,
       (select cobenterprise.name
        from cobenterprise
                 left join iobenterprisedata on iobenterprisedata.enterpriseid = cobenterprise.id
        where dobpurchaseorder.id = iobenterprisedata.refinstanceid
          AND iobenterprisedata.enterprisetype like 'Storehouse')    AS storehouseName,
       (SELECT code
        FROM iobenterprisedata
                 LEFT JOIN cobenterprise ON cobenterprise.id = iobenterprisedata.enterpriseid
        where iobenterprisedata.enterprisetype like 'Storehouse'
          AND dobpurchaseorder.id = iobenterprisedata.refinstanceid) AS storehouseCode
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobordercompany ON dobpurchaseorder.id = iobordercompany.refinstanceid
         LEFT JOIN cobenterprise businessUnit on iobordercompany.businessunitid = businessUnit.id;



CREATE VIEW DObPurchaseOrderAttachmentGeneralModel AS
SELECT IObOrderAttachment.id,
       doborderdocument.objecttypecode,
       doborderdocument.code,
       doborderdocument.currentstates,
       IObOrderAttachment.creationDate,
       IObOrderAttachment.modifiedDate,
       IObOrderAttachment.modificationInfo,
       userinfo.name As       creationInfo,
       lobattachmenttype.name attachmentTypeName,
       lobattachmenttype.code attachmentTypeCode,
       robattachment.format,
       robattachment.name     attachmentName
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         JOIN ioborderattachment
              ON dobpurchaseorder.id = IObOrderAttachment.refinstanceid
         JOIN lobattachmenttype On lobattachmenttype.id = IObOrderAttachment.lobAttachmentId
         JOIN robattachment on robattachment.id = IObOrderAttachment.robAttachmentId
         JOIN userinfo ON userinfo.id =
                          (select id from ebsuser where username = IObOrderAttachment.creationInfo);



CREATE VIEW IObOrderApprovalCycleGeneralModel AS
SELECT row_number() OVER ()  as id,
       doborderdocument.code as purchaseOrderCode,
       IObOrderApprovalCycle.approvalCycleNum,
       IObOrderApprovalCycle.startDate,
       IObOrderApprovalCycle.endDate,
       IObOrderApprovalCycle.finalDecision
FROM IObOrderApprovalCycle
         LEFT JOIN doborderdocument ON doborderdocument.id = IObOrderApprovalCycle.refInstanceId AND
                                       objecttypecode LIKE '%_PO';



CREATE VIEW IObOrderApproverGeneralModel AS
SELECT row_number() OVER ()        as id,
       doborderdocument.code       as purchaseOrderCode,
       IObOrderApprovalCycle.id    as approvalCycleId,
       IObOrderApprovalCycle.approvalCycleNum,
       cobcontrolpoint.name        as controlPointName,
       cobcontrolpoint.code        as controlPointCode,
       iobcontrolpointusers.ismain as isMainApprover,
       UserInfo.name               as approverName,
       IObOrderApprover.decisionDatetime,
       IObOrderApprover.decision,
       IObOrderApprover.notes
FROM IObOrderApprovalCycle
         LEFT JOIN doborderdocument ON doborderdocument.id = IObOrderApprovalCycle.refInstanceId AND
                                       objecttypecode LIKE '%_PO'
         LEFT JOIN IObOrderApprover ON IObOrderApprovalCycle.id = IObOrderApprover.refInstanceId
         LEFT JOIN cobcontrolpoint ON IObOrderApprover.controlPointId = cobcontrolpoint.id
         LEFT JOIN iobcontrolpointusers
                   ON iobcontrolpointusers.refinstanceid = cobcontrolpoint.id AND
                      iobcontrolpointusers.userid = IObOrderApprover.approverId
         LEFT JOIN UserInfo ON UserInfo.id = IObOrderApprover.approverId
ORDER BY IObOrderApprover.id ASC;



Create View SubAccount As
select id as subaccountid, code, null as name, 'PO' as ledger
from doborderdocument
WHERE objecttypecode LIKE '%_PO'
union all
select id as subaccountid, code, name, 'Banks' as ledger
from cobbank
union all
select id as subaccountid, code, name, 'Local_Customer' as ledger
from masterdata
where objecttypecode = '3'
union all
select id as subaccountid, code, name, 'Taxes' as ledger
from lobtaxinfo
union all
select id as subaccountid, code, name, 'Local_Vendors' as ledger
from masterdata
where objecttypecode = '2'
union all
select id as subaccountid, code, name, 'Import_Vendors' as ledger
from masterdata
where objecttypecode = '2'
union all
select id as subaccountid, code, null, 'NotesReceivable' as ledger
from dobnotesreceivables;



Create View SubAccountGeneralModel As
select row_number() over () as id, SubAccount.*
from SubAccount;



create view IObGoodsReceiptAccountingDetailsGeneralModel AS
select IObGoodsReceiptAccountingDetails.id,
       IObGoodsReceiptAccountingDetails.creationDate,
       IObGoodsReceiptAccountingDetails.creationInfo,
       IObGoodsReceiptAccountingDetails.modifiedDate,
       IObGoodsReceiptAccountingDetails.modificationInfo,
       dobgoodsreceiptaccountingdocument.code,
       creditSubAccount.code                                              as creditSubAccountCode,
       debitSubAccount.code                                               as debitSubAccountCode,
       concat(creditAccount.code, ' - ',
              creditAccount.name::json ->> 'en')                          as creditAccountCodeName,
       concat(debitAccount.code, ' - ', debitAccount.name::json ->> 'en') as debitAccountCodeName
from IObGoodsReceiptAccountingDetails
         left join dobgoodsreceiptaccountingdocument
                   on iobgoodsreceiptaccountingdetails.refinstanceid =
                      dobGoodsReceiptAccountingDocument.id
         left join doborderdocument creditSubAccount
                   on IObGoodsReceiptAccountingDetails.creditsubaccountId = creditSubAccount.id AND
                      creditSubAccount.objecttypecode LIKE '%_PO'
         left join doborderdocument debitSubAccount
                   on IObGoodsReceiptAccountingDetails.debitsubaccountId = debitSubAccount.id AND
                      debitSubAccount.objecttypecode LIKE '%_PO'
         left join HObGLAccount creditAccount
                   on IObGoodsReceiptAccountingDetails.creditaccountId = creditAccount.id
         left join HObGLAccount debitAccount
                   on IObGoodsReceiptAccountingDetails.debitaccountId = debitAccount.id
         left join cobenterprise company
                   on dobgoodsreceiptaccountingdocument.companyid = company.id and
                      company.objecttypecode = '1'
         left join cobenterprise purchaseUnit
                   on dobgoodsreceiptaccountingdocument.purchaseunitid = purchaseUnit.id and
                      purchaseUnit.objecttypecode = '5';


CREATE TYPE glAccount AS
(
    glAccountCode VARCHAR(1024),
    glAccountName JSON,
    glAccountId   BIGINT
);

CREATE or REPLACE FUNCTION getGlAccount(glAccountId bigint, vendorLedgerAccountId bigint,
                                        subLedger VARCHAR, accountingEntry VARCHAR)
    RETURNS glAccount AS
$$
DECLARE
    result glAccount ;

BEGIN
    case
        when subledger = 'Local_Vendors' and accountingEntry = 'DEBIT' then
            select id, code, name
            INTO result.glaccountid, result.glaccountcode, result.glaccountname
            from hobglaccount
            where hobglaccount.id = vendorLedgerAccountId;

        else
            select id, code, name
            INTO result.glaccountid, result.glaccountcode, result.glaccountname
            from hobglaccount
            where hobglaccount.id = glAccountId;
        end case;
    return result;
END
$$
    LANGUAGE PLPGSQL;


-- TODO: Refactor to be one General Model for Accounting Document
CREATE or replace VIEW IObPaymentRequestAccountDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code                                                    AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger                                                      AS subLedger,
       (select glaccountcode
        from getGlAccount(accountDetails.glaccountid, iobvendoraccountinginfo.chartofaccountid,
                          accountDetails.subledger,
                          accountDetails.accountingEntry))                           as glAccountCode,
       (select glaccountname
        from getGlAccount(accountDetails.glaccountid, iobvendoraccountinginfo.chartofaccountid,
                          accountDetails.subledger,
                          accountDetails.accountingEntry))                           as glAccountName,
       (select glaccountid
        from getGlAccount(accountDetails.glaccountid, iobvendoraccountinginfo.chartofaccountid,
                          accountDetails.subledger, accountDetails.accountingEntry)) as glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.code
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               doborderdocument.code
               )
           when accountDetails.accountingEntry = 'CREDIT'
               then (bankSubAccount.accountno)
           end                                                                       as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendoraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               orderaccountingdetails.glSubAccountId
               )
           when accountDetails.accountingEntry = 'CREDIT'
               then (bankaccountingdetails.glSubAccountId)
           end                                                                       as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.name
               )
           when accountDetails.accountingEntry = 'CREDIT' THEN
               (cobbank.name) end                                                    as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObPaymentRequest pr on accountDetails.refinstanceid = pr.id
         left join dobaccountingdocument on dobaccountingdocument.id = pr.id
         left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails
                   on vendoraccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join hobglaccount account on account.id = accountDetails.glaccountid

--     ---------------------------------
         left join masterdata vendor on vendor.id = vendoraccountingdetails.glSubAccountId and
                                        vendor.objecttypecode = '2'
         left join iobvendoraccountinginfo on vendor.id = iobvendoraccountinginfo.refinstanceid
         left join hobglaccount vendorAccount
                   on vendorAccount.id = iobvendoraccountinginfo.chartofaccountid
    ----------------------------------
         left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails
                   on orderaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails
                   on bankaccountingdetails.id = accountDetails.id AND
                      accountDetails.accountingEntry = 'CREDIT'

         left join doborderdocument
                   on doborderdocument.id = orderaccountingdetails.glSubAccountId AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         left join cobbank on cobbank.id = bankSubAccount.bankid;



CREATE VIEW IObPaymentRequestPaymentDetailsGeneralModel as
select pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                            as businessPartnerCode,
       md.name                            as businessPartnerName,
       pd.duedocumenttype,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           else doborderdocument.code end as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso                    as currencyISO,
       cobcurrency.name                   as currencyName,
       cobcurrency.code                   as currencyCode,
       companyCurrency.iso                AS companyCurrencyIso,
       companyCurrency.code               AS companyCurrencyCode,
       companyCurrency.id                 AS companyCurrencyId,
       pd.description
from iobpaymentrequestpaymentdetails pd
         left join IObPaymentRequestInvoicePaymentDetails ipd on ipd.id = pd.id
         left join IObPaymentRequestPurchaseOrderPaymentDetails dpd on dpd.id = pd.id
         left join dobvendorinvoice on dobvendorinvoice.id = ipd.duedocumentid
         left join dobaccountingdocument vendorInvoice on vendorInvoice.id = dobvendorinvoice.id
         left join doborderdocument
                   on doborderdocument.id = dpd.duedocumentid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join masterdata md on md.id = pd.businessPartnerId
         left join dobpaymentrequest pr on pr.id = pd.refinstanceid
         left join dobaccountingdocument on dobaccountingdocument.id = pr.id
         left join IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
         left join cobcurrency on pd.currencyid = cobcurrency.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = companyData.companyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid;



CREATE VIEW DObVendorInvoiceGeneralModel AS
SELECT dobvendorInvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                                                     AS vendorCode,
       masterdata.name                                                     AS vendorName,
       concat_ws(' - ', masterdata.code, masterdata.name :: json ->> 'en') AS vendorCodeName,
       cobvendorinvoice.code                                               AS invoiceTypeCode,
       cobvendorinvoice.name                                               AS invoiceTypeName,
       cobvendorinvoice.name :: json ->> 'en'                              AS invoiceTypeNameEn,
       purchaseUnit.code                                                   AS purchaseUnitCode,
       purchaseUnit.name                                                   AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                  AS purchaseUnitNameEn,
       company.code                                                        AS companyCode,
       company.name                                                        AS companyName,
       cobcurrency.code                                                    AS currencyCode,
       cobcurrency.name                                                    AS currencyName,
       cobcurrency.iso                                                     AS currencyISO,
       cobpaymentterms.code                                                AS paymentTermCode,
       doborderdocument.code                                               AS purchaseOrderCode,
       iobvendorinvoicedetails.invoiceNumber                               AS invoiceNumber
FROM dobvendorInvoice
         left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN masterdata ON dobvendorInvoice.vendorId = masterdata.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit
                   ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
                       AND purchaseUnit.objecttypecode = '5'
         LEFT JOIN cobenterprise company ON company.id = iobvendorinvoicecompanydata.companyId
    AND company.objecttypecode = '1'
         LEFT JOIN IObVendorInvoicePurchaseOrder
                   ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobvendorinvoice ON cobvendorinvoice.id = dobvendorInvoice.typeId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms
                   ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument
                   ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN iobvendorinvoicedetails
                   ON iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id;



CREATE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE
           WHEN invoiceSummary.totalremaining IS NULL THEN 0
           ELSE invoiceSummary.totalremaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
                   ON dobInvoice.id = invoiceSummary.id;



CREATE VIEW IObVendorInvoiceDetailsGeneralModel AS
SELECT dobaccountingdocument.code            as invoiceCode,
       dobvendorInvoice.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                       AS vendorCode,
       masterdata.name :: json ->> 'en'      AS vendorName,
       iobvendorinvoicedetails.refinstanceid,
       iobvendorinvoicedetails.invoiceNumber,
       iobvendorinvoicedetails.invoiceDate,
       cobcurrency.code                      AS currencyCode,
       cobcurrency.iso                       AS currencyISO,
       cobcurrency.name                      AS currencyName,
       cobpaymentterms.code                  AS paymentTermCode,
       cobpaymentterms.name :: json ->> 'en' AS paymentTerm,
       doborderdocument.code                 AS purchaseOrderCode,
       purchaseUnit.code                     AS purchaseUnitCode,
       purchaseUnit.name                     AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'    AS purchaseUnitNameEn,
       (select string_agg(CONCAT(dobaccountingdocument.code, '-',
                                 IObPaymentRequestPaymentDetails.netamount), ', ')
        from dobaccountingdocument,
             iobpaymentrequestpaymentdetails
        where dobaccountingdocument.id in (select iobvendorinvoicedetailsdownpayment.DownPaymentid
                                           from iobvendorinvoicedetailsdownpayment
                                           where iobvendorinvoicedetailsdownpayment.refInstanceId =
                                                 iobvendorinvoicedetails.id)
          and dobaccountingdocument.objecttypecode = 'PaymentRequest'
          and iobpaymentrequestpaymentdetails.refinstanceid = dobaccountingdocument.id
       )                                     as DownPayment
FROM dobvendorInvoice
         left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN masterdata ON dobvendorInvoice.vendorId = masterdata.id
         LEFT JOIN IObVendorInvoicePurchaseOrder
                   ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms
                   ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument
                   ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN iobvendorinvoicedetails
                   on iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit
                   ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
                       AND purchaseUnit.objecttypecode = '5';


CREATE VIEW IObPurchaseOrderItemsGeneralModel AS
SELECT ioborderlinedetails.id,
       doborderdocument.id                                         as purchaseOrderId, --purchaseID
       doborderdocument.code                                       as ordercode,
       (Select masterdata.name
        from masterdata
        where masterdata.id = ioborderlinedetails.itemId)          AS itemname,
       mobitemgeneralmodel.code                                    as itemcode,
       mobitemgeneralmodel.basicUnitOfMeasure                      as basicUnitOfMeasureId,
       mobitemgeneralmodel.basicUnitOfMeasurecode                  as basicUnitOfMeasureCode,
       mobitemgeneralmodel.basicUnitOfMeasuresymbol                as basicUnitOfMeasureSymbol,
       mobitemgeneralmodel.batchmanagementrequired,
       itemvendorrecord.itemvendorcode,
       itemvendorrecord.itemId,
       (Select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = ioborderlinedetails.unitOfMeasureId) as unitsymbol,
       (select SUM(ioborderlinedetailsquantities.quantity) as quantity
        from ioborderlinedetailsquantities
        where ioborderlinedetails.id =
              ioborderlinedetailsquantities.refinstanceid)         as itemQuantity,
       ioborderlinedetails.quantityindn
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON dobpurchaseorder.id = iobpurchaseorderfulfillervendor.refinstanceid
         join ioborderlinedetails on doborderdocument.id = ioborderlinedetails.refinstanceid
         join mobitemgeneralmodel on ioborderlinedetails.itemid = mobitemgeneralmodel.id
         left join itemvendorrecord
                   on ioborderlinedetails.itemid = itemvendorrecord.itemid
                       ANd iobpurchaseorderfulfillervendor.vendorid = itemvendorrecord.vendorid
                       And ioborderlinedetails.unitofmeasureid = itemvendorrecord.uomid
where MObItemGeneralModel.basicunitofmeasure is not null
  and mobitemgeneralmodel.batchmanagementrequired is not null;


\ir 'DObPurchaseOrderPDF_AfterRefactor.sql'

CREATE VIEW IObPurchaseOrderVendorGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.code,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.objecttypecode,
       businessUnit.code                  AS purchaseUnitCode,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       iobpurchaseorderfulfillervendor.vendorid,
       vendor.code                        AS vendorCode,
       vendor.name                        AS vendorName,
       ReferenceOrderDocument.id          AS referencePOId,
       ReferenceOrderDocument.code        AS referencePOCode
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON dobpurchaseorder.id = iobpurchaseorderfulfillervendor.refinstanceid
         LEFT JOIN dobpurchaseorder ReferencePurchaseOrderPO
                   ON iobpurchaseorderfulfillervendor.referencePoId = ReferencePurchaseOrderPO.id
         LEFT JOIN doborderdocument ReferenceOrderDocument
                   ON ReferencePurchaseOrderPO.id = ReferenceOrderDocument.id
         LEFT JOIN masterdata vendor
                   ON iobpurchaseorderfulfillervendor.vendorid = vendor.id
         LEFT JOIN cobenterprise businessUnit on iobordercompany.businessunitid = businessUnit.id;


ALTER TABLE iobpurchaseorderestimatedcost
    DROP COLUMN ordertypeid;


CREATE VIEW IObOrderItemGeneralModel AS
SELECT ioborderitem.id,
       doborderdocument.code                      AS orderCode,
       ioborderitem.refinstanceid,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.objecttypecode,
       mobitemgeneralmodel.code                   AS itemCode,
       mobitemgeneralmodel.name                   AS itemName,
       mobitemgeneralmodel.typecode               AS itemTypeCode,
       mobitemgeneralmodel.typename               AS itemTypeName,
       cobmeasure.code                            AS orderUnitCode,
       cobmeasure.name                            AS orderUnitName,
       cobmeasure.symbol                          AS orderUnitSymbol,
       ioborderitem.quantity,
       ioborderitem.price,
       ioborderitem.quantity * ioborderitem.price AS itemTotalAmount
FROM doborderdocument
         JOIN ioborderitem ON doborderdocument.id = ioborderitem.refinstanceid
         JOIN mobitemgeneralmodel ON ioborderitem.itemid = mobitemgeneralmodel.id
         JOIN cobmeasure ON ioborderitem.orderunitid = cobmeasure.id;


CREATE VIEW IObOrderTaxGeneralModel AS
SELECT iobordertax.id,
       iobordertax.creationDate,
       iobordertax.modifiedDate,
       iobordertax.creationInfo,
       iobordertax.modificationInfo,
       iobordertax.refinstanceid,
       doborderdocument.objecttypecode,
       doborderdocument.code                       AS orderCode,
       iobordertax.code,
       iobordertax.name,
       iobordertax.percentage,
       (SELECT SUM(itemTotalAmount) * (iobordertax.percentage / 100)
        FROM IObOrderItemGeneralModel
        WHERE refinstanceid = doborderdocument.id) AS amount
FROM iobordertax
         LEFT JOIN doborderdocument ON iobordertax.refinstanceid = doborderdocument.id;


CREATE or REPLACE FUNCTION getOrderTotalAmountBeforeTaxes(orderId bigint)
    RETURNS float AS
$$
BEGIN
    RETURN (
        (SELECT SUM(itemTotalAmount)
         FROM IObOrderItemGeneralModel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getOrderTotalTaxes(orderId bigint)
    RETURNS float AS
$$
BEGIN
    RETURN (
        (SELECT SUM(amount)
         FROM iobordertaxgeneralmodel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;

CREATE VIEW IObOrderItemSummaryGeneralModel AS
SELECT doborderdocument.id,
       doborderdocument.objecttypecode,
       doborderdocument.code                               AS orderCode,
       getOrderTotalAmountBeforeTaxes(doborderdocument.id) AS totalAmountBeforeTaxes,
       getOrderTotalTaxes(doborderdocument.id)             AS totalTaxes,
       getOrderTotalAmountBeforeTaxes(doborderdocument.id)
           + getOrderTotalTaxes(doborderdocument.id)       AS totalAmountAfterTaxes
FROM doborderdocument;

CREATE VIEW IObOrderPaymentDetailsGeneralModel AS
SELECT ioborderpaymentdetails.id,
       doborderdocument.code              AS orderCode,
       doborderdocument.id                AS refInstanceId,
       doborderdocument.creationdate,
       doborderdocument.modifieddate,
       doborderdocument.creationinfo,
       doborderdocument.modificationinfo,
       doborderdocument.objecttypecode,
       doborderdocument.currentstates,
       businessUnit.name                  As purchaseUnitName,
       businessUnit.code                  AS purchaseUnitCode,
       businessUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       cobcurrency.name                   AS currencyName,
       cobcurrency.iso                    AS currencyIso,
       cobcurrency.code                   AS currencyCode,
       cobpaymentterms.name               AS paymenttermname,
       cobpaymentterms.code               AS paymenttermcode
FROM doborderdocument
         JOIN dobpurchaseorder ON doborderdocument.id = dobpurchaseorder.id
         JOIN iobordercompany ON doborderdocument.id = iobordercompany.refinstanceid
         JOIN cobenterprise businessUnit ON businessUnit.id = iobordercompany.businessunitid
         JOIN ioborderpaymentdetails on doborderdocument.id = ioborderpaymentdetails.refinstanceid
         LEFT JOIN cobcurrency on cobcurrency.id = ioborderpaymentdetails.currencyId
         LEFT JOIN cobpaymentterms on cobpaymentterms.id = ioborderpaymentdetails.paymenttermid;

DROP VIEW IF EXISTS IObSalesOrderTaxGeneralModel;

CREATE VIEW IObSalesOrderTaxGeneralModel AS
SELECT iobsalesordertax.id,
       iobsalesordertax.creationDate,
       iobsalesordertax.modifiedDate,
       iobsalesordertax.creationInfo,
       iobsalesordertax.modificationInfo,
       iobsalesordertax.refinstanceid,
       dobsalesorder.code                                                     AS salesOrderCode,
       iobsalesordertax.code,
       iobsalesordertax.name,
       iobsalesordertax.percentage,
       (SELECT sum(iobsalesorderitemgeneralmodel.totalamount) *
               (iobsalesordertax.percentage / 100::double precision)
        FROM iobsalesorderitemgeneralmodel
        WHERE iobsalesorderitemgeneralmodel.refinstanceid = dobsalesorder.id) AS amount
FROM iobsalesordertax
         JOIN dobsalesorder ON dobsalesorder.id = iobsalesordertax.refInstanceId;


CREATE OR REPLACE FUNCTION getSalesOrderTotalAmountBeforeTaxes(orderId bigint)
    RETURNS DOUBLE PRECISION AS
$$
BEGIN
    RETURN (
        (SELECT CASE WHEN SUM(totalamount) IS NULL THEN 0 ELSE SUM(totalamount) END
         FROM iobsalesorderitemgeneralmodel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION getSalesOrderTotalTaxes(orderId bigint)
    RETURNS DOUBLE PRECISION AS
$$
BEGIN
    RETURN (
        (SELECT CASE WHEN SUM(amount) IS NULL THEN 0 ELSE SUM(amount) END
         FROM iobsalesordertaxgeneralmodel
         WHERE refinstanceid = orderId));

END;
$$
    LANGUAGE PLPGSQL;


CREATE VIEW IObSalesOrderSummaryGeneralModel AS
SELECT dobsalesorder.id,
       dobsalesorder.code                                    AS orderCode,
       getSalesOrderTotalAmountBeforeTaxes(dobsalesorder.id) AS totalAmountBeforeTaxes,
       getSalesOrderTotalTaxes(dobsalesorder.id)             AS totalTaxes,
       getSalesOrderTotalAmountBeforeTaxes(dobsalesorder.id)
           + getSalesOrderTotalTaxes(dobsalesorder.id)       AS totalAmountAfterTaxes
FROM dobsalesorder;
