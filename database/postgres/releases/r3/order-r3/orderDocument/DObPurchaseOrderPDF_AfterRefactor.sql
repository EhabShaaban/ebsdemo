CREATE VIEW DObPurchaseOrderCompanyPDFData AS
SELECT doborderdocument.id                                       as purchaseOrderId,
       doborderdocument.code,
       (SELECT name
        FROM CObEnterprise
        where CObEnterprise.id = iobordercompany.companyid) AS companyName,
       (SELECT logo
        FROM iobcompanylogodetails
        where iobcompanylogodetails.refinstanceid =
              iobordercompany.companyid)                    AS companyLogo,
       IObEnterpriseAddress.addressLine                          AS companyAddress,
       IObEnterpriseAddress.postalCode                           AS companyPostalCode,
       (SELECT name
        FROM CObGeoLocale
        where IObEnterpriseAddress.countryId = CObGeoLocale.id)  AS companyCountryName,
       (SELECT name
        FROM CObGeoLocale
        where IObEnterpriseAddress.cityId = CObGeoLocale.id)     AS companyCityName,
       (SELECT contactValue
        FROM IObEnterpriseContact
        WHERE IObEnterpriseContact.refinstanceid = iobordercompany.companyid
          AND IObEnterpriseContact.objectTypeCode = '1'
          AND contactTypeId = 1)                                 AS companyTelephone,
       (SELECT contactValue
        FROM IObEnterpriseContact
        WHERE IObEnterpriseContact.refinstanceid = iobordercompany.companyid
          AND IObEnterpriseContact.objectTypeCode = '1'
          AND contactTypeId = 2)                                 AS companyFax

FROM doborderdocument
         LEFT JOIN iobordercompany
                   ON doborderdocument.id = iobordercompany.refinstanceid
                       AND objecttypecode LIKE '%_PO'
         LEFT JOIN IObEnterpriseAddress
                   ON IObEnterpriseAddress.refinstanceid = iobordercompany.companyid;


CREATE VIEW DObPurchaseOrderConsigneePDFData AS
SELECT doborderdocument.id                                       as purchaseOrderId,
       doborderdocument.code,
       (SELECT name
        FROM CObEnterprise
        where CObEnterprise.id = IObEnterpriseData.enterpriseId) AS consigneeName,
       IObEnterpriseAddress.addressLine                          AS consigneeAddress,
       IObEnterpriseAddress.postalCode                           AS consigneePostalCode,
       (SELECT name
        FROM CObGeoLocale
        where IObEnterpriseAddress.countryId = CObGeoLocale.id)  AS consigneeCountryName,
       (SELECT name
        FROM CObGeoLocale
        where IObEnterpriseAddress.cityId = CObGeoLocale.id)     AS consigneeCityName,
       (SELECT contactValue
        FROM IObEnterpriseContact
        WHERE IObEnterpriseContact.refinstanceid = IObEnterpriseData.enterpriseId
          AND IObEnterpriseContact.objectTypeCode = '1'
          AND contactTypeId = 1)                                 AS consigneeTelephone,
       (SELECT contactValue
        FROM IObEnterpriseContact
        WHERE IObEnterpriseContact.refinstanceid = IObEnterpriseData.enterpriseId
          AND IObEnterpriseContact.objectTypeCode = '1'
          AND contactTypeId = 2)                                 AS consigneeFax
FROM doborderdocument
         LEFT JOIN IObEnterpriseData
                   ON doborderdocument.id = IObEnterpriseData.refinstanceid
                       AND objecttypecode LIKE '%_PO'
                       AND enterpriseType like 'Consignee'
         LEFT JOIN IObEnterpriseAddress
                   ON IObEnterpriseAddress.refinstanceid = IObEnterpriseData.enterpriseId;

CREATE VIEW DObPurchaseOrderVendorPDFData AS
SELECT doborderdocument.id   AS purchaseOrderId,
       doborderdocument.code AS purchaseOrderCode,
       MasterData.name       AS vendorName
FROM doborderdocument
         JOIN dobpurchaseorder on doborderdocument.id = dobpurchaseorder.id
         LEFT JOIN iobpurchaseorderfulfillervendor
                   ON dobpurchaseorder.id = iobpurchaseorderfulfillervendor.refinstanceid
         LEFT JOIN MasterData ON iobpurchaseorderfulfillervendor.vendorid = masterdata.id;


CREATE VIEW DObPurchaseOrderHeaderPDFData AS
SELECT doborderdocument.id,
       doborderdocument.code                                 AS purchaseOrderCode,
       (SELECT userinfo.name
        FROM userinfo,
             ebsuser
        WHERE userinfo.id = ebsuser.id
          AND ebsuser.id = doborderdocument.documentownerid) AS purchaseResponsibleName,
       DObPurchaseOrderCompanyPDFData.companyName,
       DObPurchaseOrderCompanyPDFData.companyAddress,
       DObPurchaseOrderCompanyPDFData.companyPostalCode,
       DObPurchaseOrderCompanyPDFData.companyCountryName,
       DObPurchaseOrderCompanyPDFData.companyCityName,
       DObPurchaseOrderCompanyPDFData.companyTelephone,
       DObPurchaseOrderCompanyPDFData.companyFax,
       DObPurchaseOrderCompanyPDFData.companyLogo,

       DObPurchaseOrderConsigneePDFData.consigneeName,
       DObPurchaseOrderConsigneePDFData.consigneeAddress,
       DObPurchaseOrderConsigneePDFData.consigneePostalCode,
       DObPurchaseOrderConsigneePDFData.consigneeCountryName,
       DObPurchaseOrderConsigneePDFData.consigneeCityName,
       DObPurchaseOrderConsigneePDFData.consigneeTelephone,
       DObPurchaseOrderConsigneePDFData.consigneeFax,

       DObPurchaseOrderVendorPDFData.vendorName,

       PaymentAndDeliveryGeneralModel.modeoftransportname,
       PaymentAndDeliveryGeneralModel.incotermName,
       PaymentAndDeliveryGeneralModel.dischargePortName,
       PaymentAndDeliveryGeneralModel.paymentTermName,
       PaymentAndDeliveryGeneralModel.collectionDate,
       PaymentAndDeliveryGeneralModel.numberOfDays           AS numberOfDaysAfterConfirmation,
       (SELECT iso
        FROM CObCurrency
        where CObCurrency.code =
              PaymentAndDeliveryGeneralModel.currencyCode)   as currencyISO,
       (SELECT instructionText
        FROM CObShipping
                 LEFT JOIN CObShippingInstructions ON CObShipping.id = CObShippingInstructions.id
        WHERE objectTypeCode = '2'
          AND PaymentAndDeliveryGeneralModel.shippingInstructionsCode =
              CObShipping.code)                              as shippingInstructionsText
FROM doborderdocument
         LEFT JOIN DObPurchaseOrderGeneralModel
                   ON DObPurchaseOrderGeneralModel.id = doborderdocument.id AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN DObPurchaseOrderConsigneePDFData
                   ON DObPurchaseOrderConsigneePDFData.purchaseOrderId = doborderdocument.id
         LEFT JOIN DObPurchaseOrderCompanyPDFData
                   ON DObPurchaseOrderCompanyPDFData.purchaseOrderId = doborderdocument.id
         LEFT JOIN DObPurchaseOrderVendorPDFData
                   ON DObPurchaseOrderVendorPDFData.purchaseOrderId = doborderdocument.id
         LEFT JOIN PaymentAndDeliveryGeneralModel
                   ON PaymentAndDeliveryGeneralModel.id = doborderdocument.id;