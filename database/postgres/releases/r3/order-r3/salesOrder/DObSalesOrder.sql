alter table DObSalesOrder add column totalAmount FLOAT DEFAULT 0;
alter table DObSalesOrder add column remaining FLOAT DEFAULT 0;