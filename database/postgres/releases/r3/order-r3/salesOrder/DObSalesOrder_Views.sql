DROP VIEW IF EXISTS DObSalesOrderGeneralModel CASCADE;

CREATE VIEW DObSalesOrderGeneralModel AS
SELECT dobsalesorder.id,
       dobsalesorder.code,
       dobsalesorder.creationDate,
       dobsalesorder.modifiedDate,
       dobsalesorder.creationInfo,
       dobsalesorder.modificationInfo,
       dobsalesorder.currentStates,
       cobsalesordertype.code               AS salesOrderTypeCode,
       cobsalesordertype.name               AS salesOrderTypeName,
       businessunit.code                    AS purchaseUnitCode,
       businessunit.name                    AS purchaseUnitName,
       businessunit.name::json ->> 'en'     AS purchaseUnitNameEn,
       salesResponsible.code                AS salesResponsibleCode,
       salesResponsible.name                AS salesResponsibleName,
       salesResponsible.name::json ->> 'en' AS salesResponsibleNameEn,
       masterdata.code                      AS customerCode,
       masterdata.name                      AS customerName,
       company.code                         AS companyCode,
       company.name                         AS companyName,
       store.code                           AS storeCode,
       store.name                           AS storeName,
       iobsalesorderdata.expectedDeliveryDate,
       iobsalesorderdata.notes,
       dobsalesorder.totalAmount,
       dobsalesorder.remaining
FROM dobsalesorder
         LEFT JOIN
     cobsalesordertype ON dobsalesorder.salesordertypeid = cobsalesordertype.id
         LEFT JOIN cobenterprise businessunit ON dobsalesorder.businessunitid = businessunit.id AND
                                                 businessunit.objecttypecode = '5'
         LEFT JOIN cobenterprise salesResponsible
                   ON dobsalesorder.salesresponsibleid = salesResponsible.id AND
                      salesResponsible.objecttypecode = '7'
         LEFT JOIN iobsalesorderdata ON dobsalesorder.id = iobsalesorderdata.refinstanceid

         LEFT JOIN mobcustomer ON iobsalesorderdata.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN iobsalesordercompanystoredata ON dobsalesorder.id = iobsalesordercompanystoredata.refinstanceid
         LEFT JOIN cobenterprise company ON iobsalesordercompanystoredata.companyid = company.id AND
                                            company.objecttypecode = '1'
         LEFT JOIN cobenterprise store ON iobsalesordercompanystoredata.storeid = store.id AND
                                          store.objecttypecode = '4';
