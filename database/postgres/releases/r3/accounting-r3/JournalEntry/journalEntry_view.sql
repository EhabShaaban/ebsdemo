drop view if exists DObJournalEntryGeneralModel;



CREATE or REPLACE FUNCTION getSubAccountCodeByJournalItemIdAndType(journalItemId bigint, journalItemType varchar)
    RETURNS varchar AS
$$
BEGIN
    RETURN
        (SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitempurchaseordersubaccount
                  LEFT JOIN subaccountgeneralmodel ON iobjournalentryitempurchaseordersubaccount.subaccountid =
                                                      subaccountgeneralmodel.subaccountid
             AND journalItemType = subaccountgeneralmodel.ledger
             AND journalItemType = 'PO'
         where iobjournalentryitempurchaseordersubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemvendorsubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemvendorsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                AND iobjournalentryitemvendorsubaccount.type = subaccountgeneralmodel.ledger
                                AND journalItemType = 'Vendors'
         where iobjournalentryitemvendorsubaccount.id = journalItemId
         UNION
         SELECT iobcompanybankdetails.accountno
         FROM iobcompanybankdetails
                  RIGHT JOIN iobjournalentryitembanksubaccount
                             ON iobjournalentryitembanksubaccount.subaccountid = iobcompanybankdetails.id
                                 AND journalItemType = 'Banks'
         WHERE iobjournalentryitembanksubaccount.id = journalItemId
         UNION
         SELECT cobtreasury.code
         FROM cobtreasury
                  RIGHT JOIN iobjournalentryitemtreasurysubaccount
                             ON iobjournalentryitemtreasurysubaccount.subaccountid = cobtreasury.id
                                 AND journalItemType = 'Treasuries'
         WHERE iobjournalentryitemtreasurysubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemtaxsubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemtaxsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                AND journalItemType = subaccountgeneralmodel.ledger
                                AND journalItemType = 'Taxes'
         where iobjournalentryitemtaxsubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitemcustomersubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitemcustomersubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                AND iobjournalentryitemcustomersubaccount.type = subaccountgeneralmodel.ledger
                                AND journalItemType = 'Customers'
         where iobjournalentryitemcustomersubaccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM IObJournalEntryItemNotesReceivableSubAccount
                  LEFT JOIN subaccountgeneralmodel
                            ON IObJournalEntryItemNotesReceivableSubAccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                AND journalItemType = subaccountgeneralmodel.ledger
                                AND journalItemType = 'NotesReceivable'
         where IObJournalEntryItemNotesReceivableSubAccount.id = journalItemId
         UNION
         SELECT subaccountgeneralmodel.code
         FROM iobjournalentryitempurchaseordersubaccount
                  LEFT JOIN subaccountgeneralmodel
                            ON iobjournalentryitempurchaseordersubaccount.subaccountid =
                               subaccountgeneralmodel.subaccountid
                                and subaccountgeneralmodel.ledger = 'PO'
         where iobjournalentryitempurchaseordersubaccount.id = journalItemId);
END;
$$
    LANGUAGE PLPGSQL;

CREATE or REPLACE FUNCTION getSubAccountNameByJournalItemIdAndType(journalItemId bigint, journalItemType varchar)
    RETURNS json AS
$$
BEGIN
    RETURN (SELECT subaccountgeneralmodel.name
            FROM iobjournalentryitemvendorsubaccount
                     LEFT JOIN subaccountgeneralmodel
                               ON iobjournalentryitemvendorsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                   AND iobjournalentryitemvendorsubaccount.type = subaccountgeneralmodel.ledger
                                   AND journalItemType = 'Vendors'
            where iobjournalentryitemvendorsubaccount.id = journalItemId
            UNION ALL
            SELECT cobbank.name
            FROM cobbank
                     RIGHT JOIN iobcompanybankdetails
                                ON iobcompanybankdetails.bankid = cobbank.id
                     RIGHT JOIN iobjournalentryitembanksubaccount
                                ON iobjournalentryitembanksubaccount.subaccountid = iobcompanybankdetails.id
                                    AND journalItemType = 'Banks'
            WHERE iobjournalentryitembanksubaccount.id = journalItemId
            UNION All
            SELECT cobtreasury.name
            FROM cobtreasury
                     RIGHT JOIN iobjournalentryitemtreasurysubaccount
                                ON iobjournalentryitemtreasurysubaccount.subaccountid = cobtreasury.id
                                    AND journalItemType = 'Treasuries'
            WHERE iobjournalentryitemtreasurysubaccount.id = journalItemId
            union all
            SELECT subaccountgeneralmodel.name
            FROM iobjournalentryitemCustomersubaccount
                     LEFT JOIN subaccountgeneralmodel
                               ON iobjournalentryitemCustomersubaccount.subaccountid =
                                  subaccountgeneralmodel.subaccountid
                                   AND iobjournalentryitemCustomersubaccount.type = subaccountgeneralmodel.ledger
                                   AND journalItemType = 'Customers'
            where iobjournalentryitemCustomersubaccount.id = journalItemId
            union all
            SELECT subaccountgeneralmodel.name
            FROM iobjournalentryitemTaxsubaccount
                     LEFT JOIN subaccountgeneralmodel
                               ON iobjournalentryitemTaxsubaccount.subaccountid = subaccountgeneralmodel.subaccountid
                                   AND journalItemType = subaccountgeneralmodel.ledger
                                   AND journalItemType = 'Taxes'
            where iobjournalentryitemTaxsubaccount.id = journalItemId);
END;
$$
    LANGUAGE PLPGSQL;



CREATE OR REPLACE VIEW DObJournalEntryGeneralModel AS
SELECT DObJournalEntry.code,
       DObJournalEntry.currentStates,
       DObJournalEntry.id,
       DObJournalEntry.creationDate,
       DObJournalEntry.modifiedDate,
       DObJournalEntry.creationInfo,
       DObJournalEntry.modificationInfo,
       DObJournalEntry.objectTypeCode                                 as documentReferenceType,
       DObJournalEntry.dueDate,
       company.code                                                   AS companyCode,
       company.name                                                   AS companyName,
       purchaseUnit.code                                              AS purchaseUnitCode,
       purchaseUnit.name                                              AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                             AS purchaseUnitNameEn,
       (SELECT dobaccountingdocument.code
        FROM DObVendorInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObVendorInvoiceJournalEntry.documentreferenceid = dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObVendorInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'VendorInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM DObPaymentRequestJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObPaymentRequestJournalEntry.documentreferenceid = dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObPaymentRequestJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'PaymentRequest'
        UNION
        SELECT DObCreditDebitNote.code
        FROM DObCreditNoteJournalEntry
                 LEFT JOIN DObCreditDebitNote ON DObCreditNoteJournalEntry.documentreferenceid = DObCreditDebitNote.id
        WHERE DObCreditDebitNote.type = 'CREDIT_NOTE'
          AND DObJournalEntry.id = DObCreditNoteJournalEntry.id
        UNION
        SELECT DObCreditDebitNote.code
        FROM DObDebitNoteJournalEntry
                 LEFT JOIN DObCreditDebitNote ON DObDebitNoteJournalEntry.documentreferenceid = DObCreditDebitNote.id
        WHERE DObCreditDebitNote.type = 'DEBIT_NOTE'
          AND DObJournalEntry.id = DObDebitNoteJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM DObSalesInvoiceJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON DObSalesInvoiceJournalEntry.documentreferenceid = dobaccountingdocument.id
        WHERE DObJournalEntry.id = DObSalesInvoiceJournalEntry.id
          AND dobaccountingdocument.objecttypecode = 'SalesInvoice'
        UNION
        SELECT dobaccountingdocument.code
        FROM dobcollectionJournalEntry
                 LEFT JOIN dobaccountingdocument
                           ON dobcollectionJournalEntry.documentreferenceid = dobaccountingdocument.id
        WHERE DObJournalEntry.id = dobcollectionJournalEntry.id
          AND dobaccountingdocument.objecttypecode in ('C_SI', 'C_NR', 'C_SO')
        UNION
        SELECT dobnotesreceivables.code
        FROM dobNotesReceivableJournalEntry
                 LEFT JOIN dobnotesreceivables
                           ON dobNotesReceivableJournalEntry.documentreferenceid = dobnotesreceivables.id
        WHERE DObJournalEntry.id = dobNotesReceivableJournalEntry.id
        UNION
        SELECT dobaccountingdocument.code
        FROM doblandedcostjournalentry
                 LEFT JOIN dobaccountingdocument
                           ON doblandedcostjournalentry.documentreferenceid = dobaccountingdocument.id
        WHERE DObJournalEntry.id = doblandedcostjournalentry.id and dobaccountingdocument.objecttypecode = 'LandedCost') as documentReferenceCode
FROM DObJournalEntry
         LEFT JOIN CObEnterprise company ON DObJournalEntry.companyid = company.id
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id;

CREATE or REPLACE FUNCTION getJournalEntryCodeDependsOnObjectTypeCode(objectTypeCode varchar, accouningDocumentId bigint)
    RETURNS varchar AS
$$
BEGIN
    RETURN case
               when objecttypecode = 'PaymentRequest' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobpaymentrequestjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode in ('Collection') then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id = (select id
                                             from dobcollectionjournalentry
                                             where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'SalesInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsalesinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'VendorInvoice' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from dobsalesinvoicejournalentry where documentreferenceid = accouningDocumentId))
               when objecttypecode = 'LandedCost' then
                   (select journalentry.code
                    from dobjournalentry journalentry
                    where journalentry.id =
                          (select id from doblandedcostjournalentry where documentreferenceid = accouningDocumentId))
        end;
END;
$$
    LANGUAGE PLPGSQL;

CREATE OR REPLACE VIEW IObJournalEntryItemGeneralModel AS
SELECT iobjournalentryitem.id,
       iobjournalentryitem.creationdate,
       iobjournalentryitem.creationinfo,
       iobjournalentryitem.modifieddate,
       iobjournalentryitem.modificationinfo,
       iobjournalentryitem.refInstanceId,
       dobjournalentry.code,
       dobjournalentry.duedate,
       dobjournalentry.objecttypecode                                                            as referenceType,
       getSubLedgerByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type)      as subLedger,
       (SELECT hobglaccountgeneralmodel.code
        FROM hobglaccountgeneralmodel
        WHERE hobglaccountgeneralmodel.id = iobjournalentryitem.accountid
       )                                                                                         AS AccountCode,
       (SELECT hobglaccountgeneralmodel.name
        FROM hobglaccountgeneralmodel
        WHERE hobglaccountgeneralmodel.id = iobjournalentryitem.accountid)                       AS AccountName,
       getSubAccountCodeByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type) AS subAccountCode,
       getSubAccountNameByJournalItemIdAndType(iobjournalentryitem.id, iobjournalentryitem.type) AS subAccountName,
       iobjournalentryitem.debit,
       iobjournalentryitem.credit,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE iobjournalentryitem.currency = cobcurrency.id)                                     AS currencyCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE iobjournalentryitem.currency = cobcurrency.id)                                     AS currencyName,
       (SELECT cobcurrency.code
        FROM cobcurrency
        WHERE iobjournalentryitem.companyCurrency = cobcurrency.id)                              AS companycurrencyCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        WHERE iobjournalentryitem.companyCurrency = cobcurrency.id)                              AS companycurrencyName,
       iobjournalentryitem.currencyprice,
       purchaseUnit.code                                                                         AS purchaseUnitCode,
       purchaseUnit.name                                                                         AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en'                                                        AS purchaseUnitNameEn,
       iobjournalentryitem.exchangeRateId                                                        as exhangeRateId,
       cobexchangerate.code                                                                      as exchangeratecode
FROM iobjournalentryitem
         LEFT JOIN dobjournalentry
                   ON iobjournalentryitem.refinstanceid = dobjournalentry.id
                       AND (dobjournalentry.objecttypecode = 'VendorInvoice'
                           OR dobjournalentry.objecttypecode = 'PaymentRequest'
                           OR dobjournalentry.objecttypecode = 'SalesInvoice'
                           OR dobjournalentry.objecttypecode = 'Collection'
                           OR dobjournalentry.objecttypecode = 'NotesReceivable'
                           OR dobjournalentry.objecttypecode = 'LandedCost')
         LEFT JOIN CObEnterprise purchaseUnit ON DObJournalEntry.purchaseunitid = purchaseUnit.id
         left join cobexchangerate on cobexchangerate.id = iobjournalentryitem.exchangerateid;


