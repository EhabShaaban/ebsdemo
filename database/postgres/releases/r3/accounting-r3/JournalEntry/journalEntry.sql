Drop TABLE If EXISTS IObJournalEntryItemTreasurySubAccount;

CREATE TABLE IObJournalEntryItemTreasurySubAccount
(
    id           BIGSERIAL NOT NULL REFERENCES IObJournalEntryItem On DELETE CASCADE ,
    subAccountId INT8 REFERENCES cobtreasury On DELETE RESTRICT ,
    PRIMARY KEY (id)
);

CREATE TABLE IObJournalEntryItemUnrealizedCurrencyGainLossSubAccount
(
    id           BIGSERIAL NOT NULL,
    subAccountId INT8,
    PRIMARY KEY (id)
);