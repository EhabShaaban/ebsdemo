Drop View If EXISTS IObVendorInvoiceDetailsGeneralModel;
CREATE or REPLACE VIEW IObVendorInvoiceRemainingGeneralModel AS
SELECT dobInvoice.*,
       CASE WHEN invoiceSummary.totalremaining IS NULL THEN 0 ELSE invoiceSummary.totalremaining end AS remaining
FROM DObVendorInvoiceGeneralModel dobInvoice
         Left JOIN IObInvoiceSummaryGeneralModel invoiceSummary
                   ON dobInvoice.id = invoiceSummary.id;


CREATE or REPLACE VIEW IObVendorInvoiceTaxesGeneralModel AS
SELECT IObVendorInvoiceTaxes.id,
       IObVendorInvoiceTaxes.creationDate,
       IObVendorInvoiceTaxes.modifiedDate,
       IObVendorInvoiceTaxes.creationInfo,
       IObVendorInvoiceTaxes.modificationInfo,
       IObVendorInvoiceTaxes.refinstanceid,
       dobaccountingdocument.code   AS invoicecode,
       IObVendorInvoiceTaxes.code   AS taxcode,
       IObVendorInvoiceTaxes.name   AS taxname,
       LObTaxInfo.percentage        as taxpercentage,
       IObVendorInvoiceTaxes.amount as taxAmount
FROM IObVendorInvoiceTaxes
         left join dobaccountingdocument
                   on dobaccountingdocument.id = IObVendorInvoiceTaxes.refInstanceId
                       and dobaccountingdocument.objecttypecode = 'VendorInvoice'

         left join dobvendorInvoice on dobvendorInvoice.id = IObVendorInvoiceTaxes.refinstanceid
         left join LObTaxInfo on LObTaxInfo.code = IObVendorInvoiceTaxes.code; --TODO:remove relation using code after change taxes analysis

create or replace view dobVendorInvoicePostPreparationGeneralModel as
select dobaccountingdocument.id,
       code,
       IObVendorInvoicePurchaseOrder.currencyid,
       latestexchangerate.exchangerateid as exchangerateid,
       latestexchangerate.currencyprice  as currencyprice
from dobaccountingdocument
         inner join iobaccountingdocumentcompanydata
                    on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
         inner join iobenterprisebasicdata companydata
                    on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
         inner join IObVendorInvoicePurchaseOrder
                    on dobaccountingdocument.id = iobvendorinvoicepurchaseorder.refinstanceid
         inner join getLatestCurrencyPrice(IObVendorInvoicePurchaseOrder.currencyid,
                                           companydata.currencyid) latestexchangerate
                    on latestexchangerate.firstcurrencyid = IObVendorInvoicePurchaseOrder.currencyid
                        and latestexchangerate.secondcurrencyid = companydata.currencyid
                        and dobaccountingdocument.objecttypecode = 'VendorInvoice';



CREATE OR REPLACE VIEW IObVendorInvoiceDetailsGeneralModel AS
SELECT dobaccountingdocument.code         as invoiceCode,
       dobvendorInvoice.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       masterdata.code                    AS vendorCode,
       masterdata.name :: json ->> 'en'   AS vendorName,
       iobvendorinvoicedetails.refinstanceid,
       iobvendorinvoicedetails.invoiceNumber,
       iobvendorinvoicedetails.invoiceDate,
       cobcurrency.code                   AS currencyCode,
       cobcurrency.iso                    AS currencyISO,
       cobcurrency.name                   AS currencyName,
       cobpaymentterms.code               AS paymentTermCode,
       cobpaymentterms.name               AS paymentTerm,
       doborderdocument.code              AS purchaseOrderCode,
       purchaseUnit.code                  AS purchaseUnitCode,
       purchaseUnit.name                  AS purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' AS purchaseUnitNameEn,
       (select string_agg(CONCAT(dobaccountingdocument.code, '-', IObPaymentRequestPaymentDetails.netamount), ', ')
        from dobaccountingdocument,
             iobpaymentrequestpaymentdetails
        where dobaccountingdocument.id in (select iobvendorinvoicedetailsdownpayment.DownPaymentid
                                           from iobvendorinvoicedetailsdownpayment
                                           where iobvendorinvoicedetailsdownpayment.refInstanceId =
                                                 iobvendorinvoicedetails.id)
          and dobaccountingdocument.objecttypecode = 'PaymentRequest'
          and iobpaymentrequestpaymentdetails.refinstanceid = dobaccountingdocument.id
       )                                  as DownPayment

FROM dobvendorInvoice
         left join dobaccountingdocument on dobaccountingdocument.id = dobvendorInvoice.id
         LEFT JOIN masterdata ON dobvendorInvoice.vendorId = masterdata.id
         LEFT JOIN IObVendorInvoicePurchaseOrder ON dobvendorInvoice.id = IObVendorInvoicePurchaseOrder.refInstanceId
         LEFT JOIN cobcurrency ON cobcurrency.id = IObVendorInvoicePurchaseOrder.currencyId
         LEFT JOIN cobpaymentterms ON cobpaymentterms.id = IObVendorInvoicePurchaseOrder.paymentTermId
         LEFT JOIN doborderdocument ON doborderdocument.id = IObVendorInvoicePurchaseOrder.purchaseOrderId
         LEFT JOIN iobvendorinvoicedetails on iobvendorinvoicedetails.refInstanceId = dobvendorInvoice.id
         LEFT JOIN IObAccountingDocumentCompanyData iobvendorinvoicecompanydata
                   ON dobvendorInvoice.id = iobvendorinvoicecompanydata.refinstanceid AND
                      iobvendorinvoicecompanydata.objecttypecode = 'VendorInvoice'
         LEFT JOIN cobenterprise purchaseUnit ON purchaseUnit.id = iobvendorinvoicecompanydata.purchaseUnitId
    AND purchaseUnit.objecttypecode = '5';

Drop View If EXISTS IObVendorInvoiceItemsGeneralModel;

CREATE or REPLACE VIEW IObVendorInvoiceItemsGeneralModel AS
SELECT IObVendorInvoiceItem.id,
       vendorInvoice.code,
       item.code                                                                               as itemCode,
       item.id                                                                                 as itemId,
       item.name                                                                               as itemName,
       baseUnitOfMeasure.code                                                                  as baseUnitCode,
       baseUnitOfMeasure.symbol                                                                as baseUnitSymbol,
       orderUnitOfMeasure.code                                                                 as orderUnitCode,
       orderUnitOfMeasure.symbol                                                               as orderunitsymbol,
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  AS conversionFactorToBase,
       ((IObVendorInvoiceItem.quantityinorderunit *
         getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                   IObVendorInvoiceItem.itemid)) * IObVendorInvoiceItem.price) as totalAmount,
       IObVendorInvoiceItem.creationDate,
       IObVendorInvoiceItem.modifiedDate,
       IObVendorInvoiceItem.creationInfo,
       IObVendorInvoiceItem.modificationInfo,
       IObVendorInvoiceItem.quantityinorderunit,
       IObVendorInvoiceItem.refinstanceid,
       IObVendorInvoiceItem.price,
       IObVendorInvoiceItem.price *
       getConversionFactorToBase(IObVendorInvoiceItem.baseunitofmeasureid, IObVendorInvoiceItem.orderunitofmeasureid,
                                 IObVendorInvoiceItem.itemid)                                  as orderUnitPrice

FROM IObVendorInvoiceItem
         left join masterdata item on IObVendorInvoiceItem.itemId = item.id
         left join cobmeasure orderUnitOfMeasure on IObVendorInvoiceItem.orderunitofmeasureid = orderUnitOfMeasure.id
         left join mobitem on IObVendorInvoiceItem.itemId = mobitem.id
         left join cobmeasure baseUnitOfMeasure on mobitem.basicunitofmeasure = baseUnitOfMeasure.id
         left join dobaccountingdocument vendorInvoice on IObVendorInvoiceItem.refInstanceId = vendorInvoice.id and
                                                          vendorInvoice.objecttypecode = 'VendorInvoice';




