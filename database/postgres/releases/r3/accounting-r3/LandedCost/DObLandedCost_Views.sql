CREATE OR REPLACE VIEW DObLandedCostGeneralModel AS
SELECT LC.id,
       AccDoc.code,
       AccDoc.creationdate,
       AccDoc.modifieddate,
       AccDoc.creationinfo,
       AccDoc.modificationinfo,
       AccDoc.currentstates,
       cobenterprise.code                AS purchaseUnitCode,
       cobenterprise.name                AS purchaseUnitName,
       cobenterprise.name::json ->> 'en' AS purchaseUnitNameEn,
       CObLandedCostType.code            AS typeCode,
       CObLandedCostType.name            AS typeName,
       doborderdocument.code             AS purchaseOrderCode
FROM DObLandedCost LC
         LEFT JOIN DObAccountingDocument AccDoc
                   ON LC.id = AccDoc.id AND AccDoc.objecttypecode = 'LandedCost'
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON LC.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         LEFT JOIN cobenterprise ON IObLandedCostCompanyData.purchaseUnitId = cobenterprise.id
         LEFT JOIN IObLandedCostDetails LCD ON LC.id = LCD.refinstanceid
         LEFT JOIN doborderdocument ON LCD.purchaseorderid = doborderdocument.id AND
                                       doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN CObLandedCostType ON CObLandedCostType.id = LC.typeId;

create or replace view IObLandedCostDetailsGeneralModel as
select ioblandedcostdetails.id,
       ioblandedcostdetails.refinstanceid,
       ioblandedcostdetails.creationdate,
       ioblandedcostdetails.creationinfo,
       ioblandedcostdetails.modificationinfo,
       ioblandedcostdetails.modifieddate,
       landedCost.code                                           as landedCostCode,
       dobaccountingdocument.code                                as vendorInvoiceCode,
       dobaccountingdocument.id                                  as vendorInvoiceId,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           dobaccountingdocument.objecttypecode) as invoiceAmount,
       doborderdocument.code                                     as purchaseOrderCode,
       doborderdocument.id                                       as purchaseOrderId,
       companyCurrency.iso                                       as companyCurrencyIso,
       invoiceCurrency.iso                                       as invoiceCurrencyIso,
       ioblandedcostdetails.damageStock,
       cobenterprise.code                                        as businessUnitCode,
       cobenterprise.name                                        as businessUnitName,
       concat(cobenterprise.code, ' ', '-', ' ',
              cobenterprise.name::json ->> 'en')                 as businessUnitCodeName,
       ( case when currencyPrice is null then
                  (select case
                   when getAmountPaidViaAccountingDocument(dobaccountingdocument.code,
                                                           dobaccountingdocument.objecttypecode) =
                        0.0
                       then (select avg(iobAccountingdocumentactivationdetails.currencyprice)
                             from iobAccountingdocumentactivationdetails
                                      left JOIN iobpaymentrequestpaymentdetails
                                                on iobpaymentrequestpaymentdetails.refinstanceid =
                                                   iobAccountingdocumentactivationdetails.refinstanceid
                                      left join iobpaymentrequestinvoicepaymentdetails
                                                on iobpaymentrequestinvoicepaymentdetails.id =
                                                   iobpaymentrequestpaymentdetails.id
                             where iobpaymentrequestinvoicepaymentdetails.duedocumentid =
                                   ioblandedcostdetails.vendorinvoiceid
                   )
                   else (select cobexchangerate.firstvalue * cobexchangerate.secondvalue
                         from cobexchangerate
                         where (cobexchangerate.secondcurrencyid = iobenterprisebasicdata.currencyid
                             and cobexchangerate.firstcurrencyid =
                                 iobvendorinvoicepurchaseorder.currencyid)
                         ORDER BY id DESC
                         LIMIT 1) end
       ) else currencyPrice end)                                as currencyPrice
from ioblandedcostdetails
         left join dobaccountingdocument landedCost
                   on ioblandedcostdetails.refinstanceid = landedCost.id
         left join doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join dobaccountingdocument
                   on ioblandedcostdetails.vendorinvoiceid = dobaccountingdocument.id

         left join iobordercompany on iobordercompany.refinstanceid = doborderdocument.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         left join iobvendorinvoicepurchaseorder
                   on iobvendorinvoicepurchaseorder.refinstanceid = ioblandedcostdetails.vendorinvoiceid

         left join cobcurrency invoiceCurrency
                   on invoiceCurrency.id = iobvendorinvoicepurchaseorder.currencyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid
         left join doblandedcost on doblandedcost.id = ioblandedcostdetails.refinstanceid
         LEFT JOIN IObAccountingDocumentCompanyData IObLandedCostCompanyData
                   ON landedCost.id = IObLandedCostCompanyData.refinstanceid AND
                      IObLandedCostCompanyData.objecttypecode = 'LandedCost'
         left join cobenterprise on cobenterprise.id = IObLandedCostCompanyData.purchaseUnitId;



create or REPLACE view IObLandedCostFactorItemsGeneralModel as
select ioblandedcostfactoritems.id,
       dobaccountingdocument.code,
       ioblandedcostfactoritems.refinstanceid,
       masterdata.code               as itemCode,
       masterdata.name               as itemName,
       estimateValue                 as estimatedvalue,
       actualValue                   as actualvalue,
       (estimateValue - actualValue) as difference,
       companyCurrency.iso           as companyCurrency,
       doborderdocument.code         as orderCode,
       ioblandedcostfactoritems.creationDate,
       ioblandedcostfactoritems.modifiedDate,
       ioblandedcostfactoritems.creationInfo,
       ioblandedcostfactoritems.modificationInfo
from ioblandedcostfactoritems
         left join dobaccountingdocument
                   on dobaccountingdocument.id = ioblandedcostfactoritems.refInstanceId and
                      objecttypecode = 'LandedCost'
         left JOIN masterdata on masterdata.id = ioblandedcostfactoritems.costItemId
         left JOIN ioblandedcostdetails
                   on ioblandedcostdetails.refinstanceid = ioblandedcostfactoritems.refInstanceId
         left join doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
        left join iobordercompany
                   on iobordercompany.refinstanceid = doborderdocument.id
        left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid;


create or replace view IObLandedCostFactorItemsSummary as
select dobaccountingdocument.code,
       dobaccountingdocument.id,
       companyCurrency.iso                     as companyCurrency,
       sum(estimatevalue)                      as estimateTotalAmount,
       sum(actualvalue)                        as actualTotalAmount,
       (sum(estimatevalue) - sum(actualvalue)) as differenceTotalAmount
from ioblandedcostfactoritems
         left join dobaccountingdocument
                   on dobaccountingdocument.id = ioblandedcostfactoritems.refinstanceid and
                      objecttypecode = 'LandedCost'
         left JOIN ioblandedcostdetails
                   on ioblandedcostdetails.refinstanceid = ioblandedcostfactoritems.refInstanceId
         left join doborderdocument
                   on doborderdocument.id = ioblandedcostdetails.purchaseorderid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         left join iobordercompany
                   on iobordercompany.refinstanceid = doborderdocument.id
         left join iobenterprisebasicdata
                   on iobenterprisebasicdata.refinstanceid = iobordercompany.companyid
         left join cobcurrency companyCurrency
                   on companyCurrency.id = iobenterprisebasicdata.currencyid
Group By (ioblandedcostfactoritems.refinstanceid, dobaccountingdocument.id,
          dobaccountingdocument.code,
          companyCurrency.iso);


CREATE OR REPLACE VIEW IObLandedCostAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       doborderdocument.code      AS glSubAccountCode,
       doborderdocument.id        AS glSubAccountId,
       null::json                 As glSubAccountName,
       accountDetails.amount
FROM IObAccountingDocumentAccountingDetails accountDetails
         join dobaccountingdocument on dobaccountingdocument.id = accountDetails.refinstanceid
         join hobglaccount account on account.id = accountDetails.glaccountid
        left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails
              on orderaccountingdetails.id = accountDetails.id
        left join doborderdocument on doborderdocument.id = orderaccountingdetails.glSubAccountId
WHERE dobaccountingdocument.objecttypecode = 'LandedCost'
  AND accountDetails.objecttypecode = 'LandedCost';

 CREATE OR REPLACE VIEW DObLandedCostItemsInvoicesGeneralModel
 AS
 select row_number() OVER () as id, dobaccountingdocument.code as vendorInvoiceCode, doborderdocument.code as purchaseOrderCode ,
       masterdata.id as itemId,
       ((iobvendorinvoiceitem.quantityinorderunit* getConversionFactorToBase(iobvendorinvoiceitem.baseunitofmeasureid, iobvendorinvoiceitem.orderunitofmeasureid, iobvendorinvoiceitem.itemid)) * iobvendorinvoiceitem.price  )           as totalAmount
from iobvendorinvoiceitem left join dobaccountingdocument
on iobvendorinvoiceitem.refinstanceid = dobaccountingdocument.id and objecttypecode = 'VendorInvoice'
left join dobvendorinvoice on dobvendorinvoice.id = dobaccountingdocument.id
left join iobvendorinvoicedetails on iobvendorinvoicedetails.refinstanceid = dobaccountingdocument.id
left join iobvendorinvoicepurchaseorder on iobvendorinvoicedetails.id = iobvendorinvoicepurchaseorder.id
left join doborderdocument on iobvendorinvoicepurchaseorder.purchaseorderid = doborderdocument.id
left join masterdata on masterdata.id = iobvendorinvoiceitem.itemid
where dobvendorinvoice.typeid != 1 and dobaccountingdocument.currentstates like '%Posted%';



 CREATE OR REPLACE VIEW DObLandedCostItemsPaymentRequestsGeneralModel
 AS
 select row_number() OVER () as id, dobaccountingdocument.code as paymentRequestCode, doborderdocument.code as purchaseOrderCode ,
       masterdata.id as itemId,
       iobpaymentrequestpaymentdetails.netamount
from iobpaymentrequestpaymentdetails left join dobaccountingdocument
on iobpaymentrequestpaymentdetails.refinstanceid = dobaccountingdocument.id and objecttypecode = 'PaymentRequest' and iobpaymentrequestpaymentdetails.duedocumenttype = 'PURCHASEORDER'
    left join iobpaymentrequestpurchaseorderpaymentdetails on iobpaymentrequestpaymentdetails.id = iobpaymentrequestpurchaseorderpaymentdetails.id
left join doborderdocument on iobpaymentrequestpurchaseorderpaymentdetails.duedocumentid = doborderdocument.id
left join masterdata on masterdata.id = iobpaymentrequestpaymentdetails.costfactoritemid
where  dobaccountingdocument.currentstates like '%PaymentDone%';

