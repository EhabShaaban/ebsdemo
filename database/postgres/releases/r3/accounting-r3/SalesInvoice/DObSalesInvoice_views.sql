DROP VIEW IF EXISTS IObSalesInvoiceBusinessPartnerGeneralModel;
DROP VIEW IF EXISTS DObSalesInvoiceDeliverToCustomerGeneralModel;
DROP VIEW IF EXISTS DObSalesInvoicePostPreparationGeneralModel;
DROP FUNCTION If EXISTS getLatestCurrencyPrice(firstCurrency bigint, secondCurrency bigint);
-----------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION getLatestCurrencyPrice(firstCurrency bigint, secondCurrency bigint)
    RETURNS table
            (
                currencyPrice    float,
                exchangerateId   bigint,
                firstcurrencyid  bigint,
                secondcurrencyid bigint
            )
AS
$$
BEGIN
    RETURN QUERY (
        (select firstvalue * secondvalue,
                id,
                cobexchangerate.firstcurrencyid,
                cobexchangerate.secondcurrencyid
         from cobexchangerate
         where cobexchangerate.firstcurrencyid = firstCurrency
           and cobexchangerate.secondcurrencyid = secondCurrency
         ORDER BY validfrom DESC
         LIMIT 1));

END;
$$
    LANGUAGE PLPGSQL;



create OR REPLACE VIEW IObSalesInvoicePostingDetailsGeneralModel AS
SELECT postingDetails.id,
       postingDetails.refinstanceid,
       postingDetails.creationDate,
       postingDetails.creationInfo,
       postingDetails.modificationInfo,
       postingDetails.modifieddate,
       invoice.code AS invoiceCode,
       postingDetails.duedate
FROM IObSalesInvoicePostingDetails postingDetails
         LEFT JOIN dobaccountingdocument invoice
                   on postingDetails.refinstanceid = invoice.id and
                      invoice.objecttypecode = 'SalesInvoice';

create OR REPLACE view DObSalesInvoiceJournalEntryPreparationGeneralModel as
select dobaccountingdocument.id,
       dobaccountingdocument.code,
       customerbusinesspartner.customerid,
       postingdetails.duedate,
       iobaccountingdocumentcompanydata.companyid,
       iobaccountingdocumentcompanydata.purchaseunitid,
       dobaccountingdocument.id                        as documentreferenceid,
       businesspartner.currencyid                      as customercurrencyid,
       iobenterprisebasicdata.currencyid               as companycurrencyid,
       currecyPrice.currencyPrice,
       currecyPrice.exchangerateId,
       getInvoiceTotalAmountBeforTaxes(dobaccountingdocument.code,
                                       'SalesInvoice') as amountBeforeTaxes,
       getInvoiceNetAmount(dobaccountingdocument.code,
                           'SalesInvoice')             as amountAfterTaxes,
       iobcustomeraccountinginfo.chartofaccountid      as customeraccountId,
       customerAccount.leadger                         as customerLeadger,
       lobglobalglaccountconfig.accountid              as salesaccountid
from dobaccountingdocument
         left join iobaccountingdocumentactivationdetails postingdetails
                   on postingdetails.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicebusinesspartner businesspartner
                   on businesspartner.refinstanceid = dobaccountingdocument.id
         left join iobsalesinvoicecustomerbusinesspartner customerbusinesspartner
                   on customerbusinesspartner.id = businesspartner.id
         left join iobaccountingdocumentcompanydata
                   on iobaccountingdocumentcompanydata.refinstanceid = dobaccountingdocument.id
         left join iobenterprisebasicdata on iobaccountingdocumentcompanydata.companyid =
                                             iobenterprisebasicdata.refinstanceid
         left join getLatestCurrencyPrice(businesspartner.currencyid,
                                          iobenterprisebasicdata.currencyid) currecyPrice
                   on currecyPrice.firstcurrencyid = businesspartner.currencyid
                       and currecyPrice.secondcurrencyid = iobenterprisebasicdata.currencyid
         left join iobcustomeraccountinginfo
                   on iobcustomeraccountinginfo.refinstanceid = customerbusinesspartner.customerid
         left join leafglaccount customerAccount
                   on customerAccount.id = iobcustomeraccountinginfo.chartofaccountid
         left join lobglobalglaccountconfig on lobglobalglaccountconfig.code = 'SalesGLAccount'
where dobaccountingdocument.objecttypecode = 'SalesInvoice'
  and postingdetails.objecttypecode = 'SalesInvoice'
  and iobaccountingdocumentcompanydata.objecttypecode = 'SalesInvoice';


create OR REPLACE view IObSalesInvoiceJournalEntryItemsPreparationGeneralModel as
select iobsalesinvoicetaxes.id,
       dobaccountingdocument.code,
       leafglaccount.id as taxesaccountid,
       lobtaxinfo.id    as taxId,
       (iobsalesinvoicetaxes.percentage *
        getInvoiceTotalAmountBeforTaxes(dobaccountingdocument.code, 'SalesInvoice')) /
       100              as taxamount
from dobaccountingdocument
         left join iobsalesinvoicetaxes
                   on iobsalesinvoicetaxes.refinstanceid = dobaccountingdocument.id
         left join lobglobalglaccountconfig accountconfig on accountconfig.code = 'TaxesGLAccount'
         left join leafglaccount on leafglaccount.id = accountconfig.accountid
         left join lobtaxinfo on iobsalesinvoicetaxes.code = lobtaxinfo.code
where dobaccountingdocument.objecttypecode = 'SalesInvoice';

CREATE OR REPLACE VIEW IObInvoiceSummaryGeneralModel AS
SELECT id,
       code                                                     as invoiceCode,
       objectTypeCode                                           as invoiceType,
       getInvoiceTotalAmountBeforTaxes(code, objecttypecode)    as totalAmountBeforeTaxes,
       getInvoiceTotalTaxAmount(code, objecttypecode)           as taxAmount,
       getInvoiceNetAmount(code, objecttypecode)                as netAmount,
       getDownPaymentForInvoice(code, objecttypecode)           as downpayment,
       getAmountPaidViaAccountingDocument(code, objecttypecode) as totalRemaining
FROM dobaccountingdocument
WHERE objecttypecode in ('SalesInvoice', 'VendorInvoice');


CREATE OR REPLACE VIEW DObSalesInvoiceGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationinfo,
       dobaccountingdocument.currentStates,
       dobaccountingdocument.modificationInfo,
       creationuserinfo.name                                                as creationInfoName,
       dobsalesorder.code                                                   as salesOrder,
       cobcompanygeneralmodel.currencyiso                                   as companyCurrencyIso,
       cobsalesinvoice.code                as invoiceTypeCode,
       cobsalesinvoice.name  as invoiceType,
       purchaseUnit.code as purchaseUnitCode,
       purchaseUnit.name as purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' as purchaseUnitNameEn,
       company.code as companyCode,
       company.name as companyName,
       masterdata.code                                                      AS customerCode,
       masterdata.name                                                      as customer,
       dobsalesinvoice.documentOwnerId                                      as documentOwnerId,
       userinfo.name as documentOwner,
       ebsuser.username as documentOwnerUserName,
       getAmountPaidViaAccountingDocument(dobaccountingdocument.code, 'SalesInvoice')  AS remaining
FROM DObSalesInvoice
         LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN iobsalesinvoicebusinesspartner
                   ON DObSalesInvoice.id = iobsalesinvoicebusinesspartner.refinstanceid
         LEFT JOIN iobsalesinvoicecustomerbusinesspartner
                   ON iobsalesinvoicecustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN mobcustomer ON iobsalesinvoicecustomerbusinesspartner.customerId = mobcustomer.id
         LEFT JOIN masterdata ON mobcustomer.id = masterdata.id
         left join ebsuser creationebsuser on creationebsuser.username = dobaccountingdocument.creationinfo
         left join userinfo creationuserinfo on creationuserinfo.id = creationebsuser.id
         left join cobcurrency on cobcurrency.id = IObSalesInvoiceBusinessPartner.currencyId
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesOrderId
         left join cobcompanygeneralmodel
                   on cobcompanygeneralmodel.id = iobsalesinvoicecompanydata.companyId
         left join cobsalesinvoice on cobsalesinvoice.id = dobsalesinvoice.typeid
         left join cobenterprise purchaseUnit on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid
         left join cobenterprise company on company.id = iobsalesinvoicecompanydata.companyid
         left join userinfo on  userinfo.id = dobsalesinvoice.documentOwnerId
         left join ebsuser on  ebsuser.id = dobsalesinvoice.documentOwnerId;

DROP VIEW ItemsLastSalesPriceGeneralModel;
CREATE VIEW ItemsLastSalesPriceGeneralModel AS
SELECT iobsalesinvoiceitem.id,
       dobaccountingdocument.code                                           as salesInvoiceCode,
       iobAccountingdocumentactivationdetails.creationdate                     as salesInvoicePostingDate,

       uom.code                                                             as uomCode,
       concat(uom.code, ' - ', uom.name :: json ->> 'en')                   as uomCodeName,

       item.code                                                            as itemCode,
       concat(item.code, ' - ', item.name :: json ->> 'en')                 as itemCodeName,

       customer.code                                                        as customerCode,
       concat(customer.code, ' - ', customer.name :: json ->> 'en')         as customerCodeName,

       purchaseUnit.code                                                    as purchaseUnitCode,
       concat(purchaseUnit.code, ' - ', purchaseUnit.name :: json ->> 'en') as purchaseUnitCodeName,
       iobsalesinvoiceitem.price                                            as salesPrice,
       iobsalesinvoiceitem.price *
       getconversionfactortobase(iobsalesinvoiceitem.baseunitofmeasureid,
                                 iobsalesinvoiceitem.orderunitofmeasureid,
                                 iobsalesinvoiceitem.itemid)                AS orderUnitPrice
from iobsalesinvoiceitem
         LEFT JOIN dobaccountingdocument
                   on iobsalesinvoiceitem.refinstanceid = dobaccountingdocument.id
                       and dobaccountingdocument.objecttypecode = 'SalesInvoice' and
                      dobaccountingdocument.currentstates like '%Active%'
         left join dobsalesinvoice on dobaccountingdocument.id = dobsalesinvoice.id
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         LEFT JOIN masterdata item
                   on iobsalesinvoiceitem.itemid = item.id and item.objecttypecode = '1'
         left join cobmeasure uom on uom.id = iobsalesinvoiceitem.orderunitofmeasureid
         left join cobenterprise company
                   on company.id = iobsalesinvoicecompanydata.companyid and
                      company.objecttypecode = '1'
         left join cobenterprise purchaseUnit
                   on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid and
                      purchaseUnit.objecttypecode = '5'
         left join iobsalesinvoicebusinesspartner
                   on iobsalesinvoicebusinesspartner.refinstanceid = dobsalesinvoice.id
         join iobsalesinvoiceCustomerbusinesspartner
              on iobsalesinvoiceCustomerbusinesspartner.id = iobsalesinvoicebusinesspartner.id
         join masterdata customer
              on customer.id = iobsalesinvoiceCustomerbusinesspartner.customerid and
                 customer.objecttypecode = '3'
         left join iobAccountingdocumentactivationdetails
                   on dobsalesinvoice.id = iobAccountingdocumentactivationdetails.refinstanceid and
                      iobAccountingdocumentactivationdetails.objecttypecode = 'SalesInvoice';

create OR REPLACE view IObSalesInvoiceBusinessPartnerGeneralModel as
select iobsalesinvoicebusinesspartner.id,
       iobsalesinvoicebusinesspartner.refinstanceid,
       iobsalesinvoicebusinesspartner.creationDate,
       iobsalesinvoicebusinesspartner.modifiedDate,
       iobsalesinvoicebusinesspartner.creationInfo,
       iobsalesinvoicebusinesspartner.modificationInfo,
       iobsalesinvoicebusinesspartner.type,
       dobsalesorder.code                                                         as salesOrder,
       iobsalesinvoicebusinesspartner.downPayment,
       dobaccountingdocument.code as code,
       dobaccountingdocument.currentStates,
       masterdata.code                                                            as businessPartnerCode,
       masterdata.name                                                            as businessPartnerName,
       concat(masterdata.code, ' - ', masterdata.name :: json ->> 'en')           as businesspartnercodename,

       cobcurrency.iso                                                            as currencyIso,
       cobcurrency.code                                                           as currencyCode,
       cobcurrency.name                                                           as currencyName,

       cobpaymentterms.code                                                       AS paymentTermCode,
       cobpaymentterms.name                                                       AS paymentTermName,
       concat(cobpaymentterms.code, ' - ', cobpaymentterms.name :: json ->> 'en') as paymentTermCodeName
from iobsalesinvoicebusinesspartner
         left join iobsalesinvoicecustomerbusinesspartner
                   on iobsalesinvoicebusinesspartner.id = iobsalesinvoicecustomerbusinesspartner.id
         left join mobcustomer on iobsalesinvoicecustomerbusinesspartner.customerid = mobcustomer.id
         left join masterdata on masterdata.id = mobcustomer.id
         left join dobaccountingdocument on dobaccountingdocument.id = iobsalesinvoicebusinesspartner.refInstanceId and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         left join cobcurrency on cobcurrency.id = iobsalesinvoicebusinesspartner.currencyId
         left join cobpaymentterms on cobpaymentterms.id = iobsalesinvoicebusinesspartner.paymentTermId
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesorderid;



CREATE OR REPLACE VIEW DObSalesInvoiceDeliverToCustomerGeneralModel AS
SELECT dobsalesinvoice.id,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       purchaseUnit.code as purchaseUnitCode,
       purchaseUnit.name as purchaseUnitName,
       purchaseUnit.name :: json ->> 'en' as purchaseUnitNameEn,
       cobsalesinvoice.code                as invoiceTypeCode,
       dobsalesorder.currentstates                as salesOrderCurrentStates
FROM DObSalesInvoice
         LEFT JOIN dobaccountingdocument ON DObSalesInvoice.id = dobaccountingdocument.id and
                                            dobaccountingdocument.objecttypecode = 'SalesInvoice'
         LEFT JOIN IObAccountingDocumentCompanyData iobsalesinvoicecompanydata
                   ON DObSalesInvoice.id = iobsalesinvoicecompanydata.refinstanceid AND
                      iobsalesinvoicecompanydata.objecttypecode = 'SalesInvoice'
         left join cobenterprise purchaseUnit on purchaseUnit.id = iobsalesinvoicecompanydata.purchaseunitid
         left join cobsalesinvoice on cobsalesinvoice.id = DObSalesInvoice.typeid
         left join iobsalesinvoicebusinesspartner on DObSalesInvoice.id =  iobsalesinvoicebusinesspartner.refinstanceid
         left join dobsalesorder on dobsalesorder.id = iobsalesinvoicebusinesspartner.salesorderid;

CREATE OR REPLACE VIEW DObSalesInvoicePostPreparationGeneralModel AS
select
    dobaccountingdocument.id,
    dobaccountingdocument.code,
    latestexchangerate.exchangerateid as exchangerateid,
    latestexchangerate.currencyprice as currencyprice
from dobaccountingdocument inner join iobsalesinvoicebusinesspartner
                                      on dobaccountingdocument.id = iobsalesinvoicebusinesspartner.refinstanceid
                           inner join iobaccountingdocumentcompanydata on dobaccountingdocument.id = iobaccountingdocumentcompanydata.refinstanceid
    AND iobaccountingdocumentcompanydata.objecttypecode = 'SalesInvoice'
                           inner join iobenterprisebasicdata companydata on companydata.refinstanceid = iobaccountingdocumentcompanydata.companyid
                           inner join getLatestCurrencyPrice(iobsalesinvoicebusinesspartner.currencyid, companydata.currencyid) latestexchangerate
                                      on latestexchangerate.firstcurrencyid = iobsalesinvoicebusinesspartner.currencyid
                                          and latestexchangerate.secondcurrencyid = companydata.currencyid
                                          and
                                         dobaccountingdocument.objecttypecode = 'SalesInvoice';



