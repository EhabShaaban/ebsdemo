ALTER FUNCTION  getAmountFromPostedCollectionRequests(varchar) RENAME TO getAmountFromPostedCollections;
DROP FUNCTION getAmountFromPostedCollections(varchar);
CREATE or REPLACE FUNCTION getAmountFromPostedCollections(SICode varchar)
    RETURNS float AS $$
BEGIN
    RETURN case when (select SUM (DObCollectionGeneralModel.amount)
                      from DObCollectionGeneralModel
                      where DObCollectionGeneralModel.refdocumentcode = SICode
                      and DObCollectionGeneralModel.currentStates like '%Active%'
                      and DObCollectionGeneralModel.refdocumenttypecode like '%SALES_INVOICE%') is null then 0
    else (select SUM (DObCollectionGeneralModel.amount)
                      from DObCollectionGeneralModel
                      where DObCollectionGeneralModel.refdocumentcode = SICode
                      and DObCollectionGeneralModel.currentStates like '%Active%'
                      and DObCollectionGeneralModel.refdocumenttypecode like '%SALES_INVOICE%')  end;
    END; $$
    LANGUAGE PLPGSQL;


DROP FUNCTION getAmountPaidViaAccountingDocument(VARCHAR, VARCHAR);
CREATE or REPLACE FUNCTION getAmountPaidViaAccountingDocument(iCode VARCHAR, invoiceType VARCHAR)
    RETURNS float AS $$
BEGIN
    RETURN
        CASE WHEN invoiceType = 'SalesInvoice' THEN
            round((getInvoiceTotalAmountBeforTaxes(iCode, invoiceType)
                       + getInvoiceTotalTaxAmount(iCode, invoiceType)
                       - getDownPaymentForInvoice(iCode, invoiceType)
                       - getAmountFromPostedNotesReceivalbles(iCode)
                       - getAmountFromPostedCollections(iCode)) ::numeric, 3)
        ELSE
            round((getInvoiceTotalAmountBeforTaxes(iCode, invoiceType)
                       + getInvoiceTotalTaxAmount(iCode, invoiceType)
                - getDownPaymentForInvoice(iCode, invoiceType)
                - COALESCE((SELECT SUM(paymentDetails.netamount) AS amountPaid
             FROM iobpaymentrequestinvoicepaymentdetails invoicepaymentDetails
             JOIN iobpaymentrequestpaymentdetails paymentDetails on paymentDetails.id = invoicepaymentDetails.id
             JOIN dobpaymentrequest paymentRequest ON paymentDetails.refinstanceid = paymentRequest.id
             JOIN dobaccountingdocument on dobaccountingdocument.id =paymentRequest.id AND dobaccountingdocument.currentstates like '%PaymentDone%'
             JOIN dobaccountingdocument invoice ON invoicepaymentDetails.duedocumentid = invoice.id AND invoice.objecttypecode = 'VendorInvoice'
             WHERE invoice.code = iCode),0)) ::numeric, 3)
        END;
END; $$
    LANGUAGE PLPGSQL;
