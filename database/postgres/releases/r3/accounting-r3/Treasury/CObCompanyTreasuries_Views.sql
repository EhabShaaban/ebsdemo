

CREATE OR Replace VIEW  IObCompanyTreasuriesGeneralModel AS
select
iobcompanytreasuries.id,
iobcompanytreasuries.refinstanceid,
iobcompanytreasuries.creationDate,
iobcompanytreasuries.modifieddate,
iobcompanytreasuries.creationinfo,
iobcompanytreasuries.modificationinfo,
cobenterprise.code as companyCode,
cobenterprise.name as companyName,
cobtreasury.code as treasuryCode,
cobtreasury.name as treasuryName
from cobenterprise left join iobcompanytreasuries on  cobenterprise.id = iobcompanytreasuries.refinstanceid
inner join cobtreasury on cobtreasury.id = iobcompanytreasuries.treasuryid
where cobenterprise.objecttypecode = '1';