Drop TABLE IF EXISTS IObCompanyTreasuries;
Drop TABLE IF EXISTS CObTreasury;

CREATE TABLE CObTreasury
(
    id               BIGSERIAL     NOT NULL,
    name             JSON          NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE IObCompanyTreasuries
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL REFERENCES cobcompany (id) ON DELETE CASCADE,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    treasuryId           INT8     NOT NULL REFERENCES CObTreasury (id) ON DELETE RESTRICT,
    PRIMARY KEY (id)
);