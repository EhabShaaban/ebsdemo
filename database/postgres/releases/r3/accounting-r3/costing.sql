
create or replace view vendorInvoiceItemWeight as
select invoiceItem.refinstanceid                                          as invoiceId,
       invoiceItem.code                                                   as invoiceCode,
       LCDetails.purchaseorderid                                          as purchaseorderid,
       LCDetails.purchaseordercode                                        as purchaseorderCode,
       dobinventorydocument.id                                            as goodsreceiptId,
       dobinventorydocument.code                                          as goodsreceiptCode,
       LC.code                                                            as landedCostCode,
       LC.id                                                              as landedCostId,
       LC.currentstates                                                   as landedCostState,
       invoiceItem.id                                                     as itemId,
       invoiceItem.itemcode                                               as itemCode,
       invoiceItem.totalamount /
       getInvoiceTotalAmountBeforTaxes(invoiceItem.code, 'VendorInvoice') as itemWeight,
       case
           when
                   LC.currentstates like '%ActualValuesCompleted%'
               then LCSummary.actualtotalamount
           else
               LCSummary.estimatetotalamount
           end                                                            as landedCostTotal,
       invoiceItem.orderunitprice                                         as itemUnitPrice,
       LCDetails.currencyprice,
       invoiceItem.orderunitcode,
       invoiceItem.orderunitsymbol
from IObVendorInvoiceItemsGeneralModel invoiceItem
         left join ioblandedcostdetailsgeneralmodel LCDetails
                   on invoiceItem.refinstanceid = LCDetails.vendorinvoiceId
         inner join dobaccountingdocument LC
                    on LCDetails.refinstanceid = LC.id and
                       LC.objecttypecode = 'LandedCost' and LC.currentstates not like '%Draft%'
         left join IObLandedCostFactorItemsSummary LCSummary on LCSummary.id = LC.id
         inner join iobgoodsreceiptpurchaseorderdata
                    on LCDetails.purchaseorderid = iobgoodsreceiptpurchaseorderdata.purchaseorderid
         inner join dobinventorydocument on dobinventorydocument.id = iobgoodsreceiptpurchaseorderdata.refinstanceid
    and dobinventorydocument.currentstates like '%Active%';

create or replace view unrestrictedItemsSingleUnitFinalCost as
select vendorinvoiceitemweight.itemId                                     as itemId,
       vendorinvoiceitemweight.landedCostState                            as landedCostState,
       vendorinvoiceitemweight.landedCostCode                             as landedCostCode,
       vendorinvoiceitemweight.goodsreceiptcode,
       itemweight * landedcosttotal                                       as itemTotalLandedCost,
       (itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe as itemSingleUnitLandedCost,
       (((itemweight * landedcosttotal) / GRUnrestrectedItem.receivedqtyuoe) +
       (itemunitprice * currencyprice))                                    as itemSingleUnitFinalCost,
       GRUnrestrectedItem.stocktype,
       GRUnrestrectedItem.receivedqtyuoe,
       GRUnrestrectedItem.itemcode,
       GRUnrestrectedItem.unitofentrycode,
       purchaseorderid,
       purchaseorderCode
from vendorinvoiceitemweight
         inner join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                     union
                     select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                     from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRUnrestrectedItem
                    on GRUnrestrectedItem.goodsreceiptcode = vendorinvoiceitemweight.goodsreceiptcode
                        and GRUnrestrectedItem.stocktype = 'UNRESTRICTED_USE'
                        and vendorinvoiceitemweight.itemcode = GRUnrestrectedItem.itemcode
                        and vendorinvoiceitemweight.orderunitcode = GRUnrestrectedItem.unitofentrycode
;


create or replace view purchasedItemsTotalFinalCost as
select unrestrictedItemsSingleUnitFinalCost.itemId                           as itemid,
       unrestrictedItemsSingleUnitFinalCost.landedCostState                  as landedCostState,
       unrestrictedItemsSingleUnitFinalCost.landedCostCode                   as landedCostCode,
       unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode,
       unrestrictedItemsSingleUnitFinalCost.itemcode,
       unrestrictedItemsSingleUnitFinalCost.unitofentrycode,
       GRDamagedItem.receivedqtyuoe                                          as damageQty,
       unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe                   as unrestrictedQty,
       GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost                as damageItemCost,
       case
           when GRDamagedItem.receivedqtyuoe is null then
               itemsingleunitfinalcost
           else
                   itemsingleunitfinalcost +
                   ((GRDamagedItem.receivedqtyuoe * itemsingleunitfinalcost) /
                    unrestrictedItemsSingleUnitFinalCost.receivedqtyuoe) end as itemSingleUnitFinalCostWithDamagedItemsCost,
       purchaseorderid,
       purchaseorderCode
from unrestrictedItemsSingleUnitFinalCost
         left join (select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptbatchedrecieveditemsbatchesgeneralmodel
                    union
                    select receivedqtyuoe, unitofentrycode, stocktype, itemcode, goodsreceiptcode
                    from iobgoodsreceiptpurchaseorderordinaryreceiveditemgeneralmodel) as GRDamagedItem
                   on GRDamagedItem.goodsreceiptcode = unrestrictedItemsSingleUnitFinalCost.goodsreceiptcode
                       and GRDamagedItem.stocktype = 'DAMAGED_STOCK'
                       and unrestrictedItemsSingleUnitFinalCost.itemcode = GRDamagedItem.itemcode
                       and unrestrictedItemsSingleUnitFinalCost.unitofentrycode = GRDamagedItem.unitofentrycode;
