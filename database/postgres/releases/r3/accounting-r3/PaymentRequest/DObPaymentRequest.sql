DROP TABLE IF EXISTS IObAccountingDocumentTreasuryAccountingDetails;

ALTER TABLE dobpaymentrequest DROP COLUMN paymentForm CASCADE;
ALTER TABLE IObPaymentRequestPaymentDetails ADD COLUMN paymentForm			VARCHAR(1024) NOT NULL;
ALTER TABLE IObPaymentRequestPaymentDetails ADD COLUMN bankAccountId		INT8 REFERENCES iobcompanybankdetails (id) ON DELETE RESTRICT;
ALTER TABLE IObPaymentRequestPaymentDetails ADD COLUMN costFactorItemId		INT8 REFERENCES MObItem (id) ON DELETE RESTRICT;
ALTER TABLE IObPaymentRequestPaymentDetails ADD COLUMN treasuryId	     	INT8 REFERENCES cobtreasury (id) ON DELETE RESTRICT;



CREATE TABLE IObAccountingDocumentTreasuryAccountingDetails
(
    id               BIGSERIAL     NOT NULL  REFERENCES IObAccountingDocumentAccountingDetails (id) ON DELETE cascade,
    glSubAccountId   INT8 REFERENCES cobtreasury  (id) ON DELETE RESTRICT ,
    PRIMARY KEY (id)
);
