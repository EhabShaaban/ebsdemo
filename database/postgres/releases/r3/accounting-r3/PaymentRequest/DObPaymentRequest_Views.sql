---TODO: change glaccountleafs subledger of cash to treasuries
DROP VIEW IF EXISTS DObPaymentRequestGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestPaymentDetailsGeneralModel;
DROP VIEW IF EXISTS IObPaymentRequestAccountDetailsGeneralModel;


CREATE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       dobaccountingdocument.code,
       DObPaymentRequest.paymentType,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       paymentdetails.paymentForm,
       paymentdetails.netAmount           AS amountValue,
       cobcurrency.iso                    AS amountCurrency,
       companyData.purchaseUnitId,
       purchaseunit.name                  AS purchaseUnitName,
       purchaseunit.name:: json ->> 'en'  AS purchaseUnitNameEn,
       purchaseunit.code                  AS purchaseUnitCode,
       businesspartner.code               AS businessPartnerCode,
       businesspartner.name               AS businessPartnerName,
       duedocumenttype                    AS dueDocumentType,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           else doborderdocument.code end as referenceDocumentCode,
       case
           when duedocumenttype = 'INVOICE' then '{
             "en": "Vendor Invoice",
             "ar": "فاتورة مورد"
           }'::json
           else '{
             "en": "Purchase Order",
             "ar": "أمر شراء"
           }'::json end                   as referenceDocumentType

FROM DObPaymentRequest
         left join dobaccountingdocument on dobaccountingdocument.id = DObPaymentRequest.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData
                   ON dobpaymentrequest.id = companyData.refinstanceid
         LEFT JOIN cobenterprise purchaseunit ON purchaseunit.id = companyData.purchaseunitid
         LEFT JOIN iobpaymentrequestpaymentdetails paymentdetails on dobpaymentrequest.id = paymentdetails.refinstanceid
         LEFT JOIN mobbusinesspartner on mobbusinesspartner.id = paymentdetails.businesspartnerid
         LEFT JOIN masterdata businesspartner on mobbusinesspartner.id = businesspartner.id
         left join IObPaymentRequestInvoicePaymentDetails invoicepaymentdetails
                   on invoicepaymentdetails.id = paymentdetails.id
         left join IObPaymentRequestPurchaseOrderPaymentDetails orderpaymentdetails
                   on orderpaymentdetails.id = paymentdetails.id
         left join dobaccountingdocument vendorInvoice on vendorInvoice.id = invoicepaymentdetails.duedocumentid
         left join doborderdocument on doborderdocument.id = orderpaymentdetails.duedocumentid
         left join cobcurrency on paymentdetails.currencyid = cobcurrency.id;

CREATE VIEW IObPaymentRequestPaymentDetailsGeneralModel as
SELECT pd.id,
       pd.refInstanceId,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.code,
       dobaccountingdocument.currentStates,
       md.code                    as businessPartnerCode,
       md.name                    as businessPartnerName,
       pd.duedocumenttype,
       case
           when duedocumenttype = 'INVOICE' then vendorInvoice.code
           else doborderdocument.code end as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso            as currencyISO,
       cobcurrency.name           as currencyName,
       cobcurrency.code           as currencyCode,
       companyCurrency.iso        AS companyCurrencyIso,
       companyCurrency.code       AS companyCurrencyCode,
       companyCurrency.id         AS companyCurrencyId,
       pd.description,
       pd.paymentForm,
       IObCompanyBankDetails.id                              AS bankAccountId,
       companyBank.code                                      AS bankAccountCode,
       companyBank.name                                      AS bankAccountName,
       cobtreasury.code                                      AS treasuryCode,
       cobtreasury.name                                      AS treasuryName,
       IObCompanyBankDetails.accountno                       AS bankAccountNumber,
       item.code											 AS costFactorItemCode,
       item.name											 AS costFactorItemName
FROM iobpaymentrequestpaymentdetails pd
         LEFT JOIN IObPaymentRequestInvoicePaymentDetails ipd on ipd.id = pd.id
         LEFT JOIN IObPaymentRequestPurchaseOrderPaymentDetails dpd on dpd.id = pd.id
         left join dobvendorinvoice on dobvendorinvoice.id = ipd.duedocumentid
         left join dobaccountingdocument vendorInvoice on vendorInvoice.id = dobvendorinvoice.id
         left join doborderdocument
                   on doborderdocument.id = dpd.duedocumentid AND
                      doborderdocument.objecttypecode LIKE '%_PO'
         LEFT JOIN masterdata md on md.id = pd.businessPartnerId
         LEFT JOIN dobpaymentrequest pr on pr.id = pd.refinstanceid
         LEFT JOIN dobaccountingdocument on dobaccountingdocument.id = pr.id
         LEFT JOIN IObAccountingDocumentCompanyData companyData on pr.id = companyData.refinstanceid
         LEFT JOIN cobcurrency on pd.currencyid = cobcurrency.id
         LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
         LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid
         LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = pd.bankAccountId
         LEFT JOIN cobbank companyBank ON companyBank.id = IObCompanyBankDetails.bankid
         LEFT JOIN MObItemGeneralModel item on item.id = pd.costFactorItemId
         LEFT JOIN cobtreasury on cobtreasury.id = pd.treasuryId;


 -- TODO: Refactor to be one General Model for Accounting Document
CREATE VIEW IObPaymentRequestAccountDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger   AS subLedger,
       account.code               AS glAccountCode,
       account.name               AS glAccountName,
       account.id                 AS glAccountId,
       accountDetails.amount,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.code
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               doborderdocument.code
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks'
               then (bankSubAccount.accountno)
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (cobtreasury.code)
           end                    as glSubAccountCode,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendoraccountingdetails.glSubAccountId
               )
           when accountDetails.subledger = 'PO' AND accountDetails.accountingEntry = 'DEBIT' then (
               orderaccountingdetails.glSubAccountId
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks'
               then (bankaccountingdetails.glSubAccountId)
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (treasuryaccountingdetails.glSubAccountId)
           end                    as glSubAccountId,
       case
           when accountDetails.subledger = 'Local_Vendors' AND
                accountDetails.accountingEntry = 'DEBIT' then (
               vendor.name
               )
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Banks' THEN
               (cobbank.name)
           when accountDetails.accountingEntry = 'CREDIT' AND
                accountDetails.subledger = 'Treasuries'
               then (cobtreasury.name)
           end as glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
         left join DObPaymentRequest pr on accountDetails.refinstanceid = pr.id
         left join dobaccountingdocument on dobaccountingdocument.id = pr.id
         left join hobglaccount account on account.id = accountDetails.glaccountid
         left join iobaccountingdocumentvendoraccountingdetails vendoraccountingdetails on vendoraccountingdetails.id = accountDetails.id AND
                                                                                           accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentorderaccountingdetails orderaccountingdetails on orderaccountingdetails.id = accountDetails.id AND
                                                                                         accountDetails.accountingEntry = 'DEBIT'
         left join iobaccountingdocumentbankaccountingdetails bankaccountingdetails on bankaccountingdetails.id = accountDetails.id AND
                                                                                       accountDetails.accountingEntry = 'CREDIT'
         left join iobaccountingdocumenttreasuryaccountingdetails treasuryaccountingdetails on treasuryaccountingdetails.id = accountDetails.id AND
                                                                                               accountDetails.accountingEntry = 'CREDIT'
         left join masterdata vendor on vendor.id = vendoraccountingdetails.glSubAccountId and vendor.objecttypecode = '2'
         left join doborderdocument on doborderdocument.id = orderaccountingdetails.glSubAccountId
         left join IObCompanyBankDetails bankSubAccount
                   on bankSubAccount.id = bankaccountingdetails.glsubaccountid
         left join cobbank on cobbank.id = bankSubAccount.bankid
         left join cobtreasury on cobtreasury.id = treasuryaccountingdetails.glsubaccountid;


         