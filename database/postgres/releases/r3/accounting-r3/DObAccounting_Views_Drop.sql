DROP VIEW IF EXISTS IObLandedCostFactorItemsSummary;
DROP VIEW IF EXISTS IObLandedCostFactorItemsGeneralModel;
DROP VIEW IF EXISTS IObLandedCostDetailsGeneralModel;
DROP VIEW IF EXISTS DObLandedCostGeneralModel;
DROP VIEW IF EXISTS IObLandedCostAccountingDetailsGeneralModel;

DROP  VIEW If EXISTS IObSalesInvoicePostingDetailsGeneralModel;
DROP  VIEW If EXISTS DObSalesInvoiceJournalEntryPreparationGeneralModel;
DROP  VIEW If EXISTS IObSalesInvoiceJournalEntryItemsPreparationGeneralModel;
DROP  VIEW If EXISTS IObJournalEntryItemGeneralModel;
DROP VIEW IF EXISTS IObInvoiceRemainingGeneralModel  ;
DROP VIEW IF EXISTS dobnotesreceivablesgeneralmodel ;
DROP VIEW IF EXISTS dobsalesinvoicegeneralmodel ;
DROP VIEW IF EXISTS IObVendorInvoiceRemainingGeneralModel;
DROP VIEW IF EXISTS iobinvoicesummarygeneralmodel  ;

drop view if exists DObCollectionRequestGeneralModel;
drop view if exists iobcollectionrequestcompanydetailsgeneralmodel;
drop view if exists IObCollectionRequestDetailsGeneralModel;
DROP VIEW IF Exists IObCollectionRequestAccountingDetailsGeneralModel;

DROP VIEW IF Exists IObAccountingDocumentPostingDetailsGeneralModel;
