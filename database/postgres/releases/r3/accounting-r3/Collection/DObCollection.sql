drop table if exists IObCollectionSalesOrderDetails;
drop table if exists IObCollectionRequestCompanyDetails;
---------------------------------------------------------------------------------------------

ALTER TABLE cobcollectionrequestreferencedocumenttype RENAME TO cobcollectiontype;

ALTER TABLE dobcollectionrequestjournalentry RENAME TO dobcollectionjournalentry;

ALTER TABLE dobcollectionrequest RENAME TO dobcollection;

ALTER TABLE dobcollection drop column collectionForm;

ALTER TABLE dobcollection ADD column documentOwnerId INT8 NOT NULL ;

ALTER TABLE iobcollectionrequestsalesinvoicedetails RENAME TO iobcollectionsalesinvoicedetails;

ALTER TABLE iobcollectionrequestnotesreceivabledetails RENAME TO iobcollectionnotesreceivabledetails;

ALTER SEQUENCE cobcollectionrequestreferencedocumenttype_id_seq RENAME TO cobcollectiontype_id_seq;

ALTER SEQUENCE dobcollectionrequest_id_seq RENAME TO dobcollection_id_seq;

ALTER SEQUENCE iobcollectionrequestsalesinvoicedetails_id_seq RENAME TO iobcollectionsalesinvoicedetails_id_seq;

ALTER SEQUENCE iobcollectionrequestnotesreceivabledetails_id_seq RENAME TO iobcollectionnotesreceivabledetails_id_seq;

drop sequence IF EXISTS FK_IObCollectionRequestDetails_customer;

ALTER TABLE dobcollection
    ADD CONSTRAINT FK_EBSUser_dobcollection FOREIGN KEY (documentOwnerId)
        REFERENCES EBSUser (id) ON DELETE RESTRICT;

---------------------------------------------------------------------------------------------
ALTER TABLE iobcollectionrequestdetails RENAME TO iobcollectiondetails;

ALTER SEQUENCE iobcollectionrequestdetails_id_seq RENAME TO iobcollectiondetails_id_seq;

ALTER TABLE iobcollectiondetails ADD column collectionMethod VARCHAR(1024)  NOT NULL;

ALTER TABLE iobcollectiondetails ADD column bankAccountId INT8;

ALTER TABLE iobcollectiondetails ADD column treasuryId INT8;

ALTER TABLE iobcollectiondetails RENAME column customerid to businessPartnerId;

ALTER TABLE iobcollectiondetails
    ADD CONSTRAINT FK_IObCollectionDetails_businessPartner FOREIGN KEY (businessPartnerId) REFERENCES mobbusinesspartner (id) ON DELETE restrict;

ALTER TABLE iobcollectiondetails
    ADD CONSTRAINT FK_IObCollectionDetails_bankAccount FOREIGN KEY (bankAccountId) REFERENCES iobcompanybankdetails (id) ON DELETE restrict;

ALTER TABLE iobcollectiondetails
    ADD CONSTRAINT FK_IObCollectionDetails_treasury FOREIGN KEY (treasuryId) REFERENCES cobtreasury (id) ON DELETE restrict;

---------------------------------------------------------------------------------------------
create table IObCollectionSalesOrderDetails
(
    id                BIGSERIAL NOT NULL,
    salesOrderId      INT8      NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObCollectionSalesOrderDetails
    ADD CONSTRAINT FK_IObCollectionSalesOrderDetails_notesReceivableId FOREIGN KEY (salesOrderId) REFERENCES DObsalesorder (id) ON DELETE restrict;

ALTER TABLE IObCollectionSalesOrderDetails
    ADD CONSTRAINT FK_IObCollectionSalesOrderDetails_id FOREIGN KEY (id) REFERENCES IObCollectionDetails (id) ON DELETE cascade;
