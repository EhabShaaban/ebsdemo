DROP FUNCTION getRefDocumentCodeBasedOnType(BIGINT, CHARACTER VARYING);
CREATE OR REPLACE FUNCTION getRefDocumentCodeBasedOnType(collectionDetailsId BIGINT, refDocumentType VARCHAR)
  RETURNS VARCHAR AS
$$
BEGIN
  RETURN CASE
           WHEN refDocumentType = 'SALES_INVOICE' THEN
             (SELECT code
              FROM dobaccountingdocument
              WHERE dobaccountingdocument.id =
                    (SELECT salesinvoiceid
                     FROM iobcollectionsalesinvoicedetails
                     WHERE iobcollectionsalesinvoicedetails.id = collectionDetailsId)
                AND objecttypecode = 'SalesInvoice')
           WHEN refDocumentType = 'NOTES_RECEIVABLE' THEN
             (SELECT code
              FROM dobnotesreceivables
              WHERE dobnotesreceivables.id =
                    (SELECT notesReceivableId
                     FROM IObCollectionNotesReceivableDetails
                     WHERE IObCollectionNotesReceivableDetails.id = collectionDetailsId))
           WHEN refDocumentType = 'SALES_ORDER' THEN
             (SELECT code
              FROM dobsalesorder
              WHERE dobsalesorder.id =
                    (SELECT salesOrderId
                     FROM IObCollectionSalesOrderDetails
                     WHERE IObCollectionSalesOrderDetails.id = collectionDetailsId))
    END;
END;
$$
  LANGUAGE PLPGSQL;

CREATE VIEW DObCollectionGeneralModel AS
SELECT c.id,
       dobaccountingdocument.code,
       dobaccountingdocument.creationDate,
       dobaccountingdocument.modifiedDate,
       dobaccountingdocument.creationInfo,
       dobaccountingdocument.modificationInfo,
       dobaccountingdocument.currentStates,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode,
       c.collectiontype,
       company.code                                        AS companycode,
       company.name                                        AS companyname,
       cobcurrency.code                                    AS companylocalcurrencycode,
       cobcurrency.name                                    AS companylocalcurrencyname,
       cobcurrency.iso                                     AS companylocalcurrencyiso,
       masterdata.name                                     AS businessPartnername,
       masterdata.code                                     AS businessPartnercode,
       CASE
         WHEN masterdata.objecttypecode = '3' THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
         WHEN masterdata.objecttypecode = '2' THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
         ELSE NULL END
                                                           AS businessPartnerType,
       iobcollectiondetails.amount,
       refDocumentType.code                                AS refdocumenttypecode,
       refDocumentType.name                                AS refdocumenttypename,
       getRefDocumentCodeBasedOnType(iobcollectiondetails.id,
                                     refDocumentType.code) AS refDocumentCode,
       dobaccountingdocument.objecttypecode                AS collectionDocumentType,

       c.documentOwnerId                                   AS documentOwnerId,
       (SELECT name
        FROM UserInfo
        WHERE UserInfo.id = c.documentOwnerId)             AS documentOwner,
       (SELECT username
        FROM EBSUser
        WHERE EBSUser.id = c.documentOwnerId)              AS documentOwnerUserName,
        iobcollectiondetails.collectionmethod

FROM dobcollection c
       LEFT JOIN iobcollectiondetails ON c.id = iobcollectiondetails.refinstanceid
       LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = c.id
       LEFT JOIN cobcollectiontype refDocumentType
                 ON iobcollectiondetails.refDocumentTypeId = refDocumentType.id
       LEFT JOIN mobbusinessPartner ON iobcollectiondetails.businessPartnerid = mobbusinessPartner.id
       LEFT JOIN masterdata ON mobbusinessPartner.id = masterdata.id
       LEFT JOIN iobaccountingdocumentcompanydata
                 ON iobaccountingdocumentcompanydata.refInstanceId = c.id
                   AND iobaccountingdocumentcompanydata.objecttypecode = 'Collection'
       LEFT JOIN cobenterprise businessUnit
                 ON iobaccountingdocumentcompanydata.purchaseUnitId = businessUnit.id
       LEFT JOIN cobenterprise company
                 ON iobaccountingdocumentcompanydata.companyid = company.id
       LEFT JOIN cobcompany ON iobaccountingdocumentcompanydata.companyid = cobcompany.id
       LEFT JOIN iobenterprisebasicdata ON cobcompany.id = iobenterprisebasicdata.refinstanceid
       LEFT JOIN cobcurrency ON iobenterprisebasicdata.currencyid = cobcurrency.id;

CREATE OR REPLACE VIEW IObCollectionCompanyDetailsGeneralModel AS
SELECT companyDetails.id,
       companyDetails.refInstanceId,
       companyDetails.creationDate,
       companyDetails.modifiedDate,
       companyDetails.creationInfo,
       companyDetails.modificationInfo,
       businessUnit.name                  AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en' AS purchaseUnitNameEn,
       businessUnit.code                  AS purchaseUnitcode,
       company.code                       AS companycode,
       company.name                       AS companyname,
       CObBank.code                       AS bankcode,
       CObBank.name                       AS bankname,
       dobaccountingdocument.code         AS collectionCode,
       bankdetails.accountno              AS accountNumber
FROM iobaccountingdocumentcompanydata companyDetails
       JOIN cobenterprise company ON companyDetails.companyid = company.id
       LEFT JOIN cobenterprise businessUnit ON companyDetails.purchaseUnitId = businessUnit.id
       LEFT JOIN iobcompanybankdetails bankdetails
                 ON companyDetails.bankAccountId = bankdetails.id
       LEFT JOIN CObBank ON bankdetails.bankid = CObBank.id
       JOIN DObCollection ON DObCollection.id = companyDetails.refInstanceId
       LEFT JOIN dobaccountingdocument ON dobaccountingdocument.id = DObCollection.id
WHERE companyDetails.objecttypecode = 'Collection';



CREATE OR REPLACE VIEW IObCollectionDetailsGeneralModel AS
SELECT cd.id,
       cd.refInstanceId,
       crAccountingDocument.code                           AS collectionCode,
       crAccountingDocument.creationDate,
       crAccountingDocument.modifiedDate,
       crAccountingDocument.creationInfo,
       crAccountingDocument.modificationInfo,
       crAccountingDocument.currentStates,
       refDocumentType.code                                AS refdocumenttype,
       getRefDocumentCodeBasedOnType(cd.id,
                                     refDocumentType.code) AS refDocumentCode,
       cd.amount,
       cobcurrency.iso                                     AS currencyISO,
       cobcurrency.name                                    AS currencyName,
       cobcurrency.code                                    AS currencyCode,
       companyCurrency.iso                                 AS companyCurrencyIso,
       companyCurrency.code                                AS companyCurrencyCode,
       businessPartner.code                                AS businessPartnerCode,
       businessPartner.name                                AS businessPartnername,
       CASE
         WHEN businessPartner.objecttypecode = '3' THEN cast('{"ar": "عميل", "en": "Customer"}' AS JSON)
         WHEN businessPartner.objecttypecode = '2' THEN cast('{"ar": "بائع", "en": "Vendor"}' AS JSON)
         ELSE NULL END
                                                           AS businessPartnerType,
       cd.collectionMethod,
       cobbank.code                                        AS bankAccountCode,
       cobbank.name                                        AS bankAccountName,
       cobtreasury.code                                    AS treasuryCode,
       cobtreasury.name                                    AS treasuryName,
       IObCompanyBankDetails.accountno                     AS bankAccountNumber,
       businessUnit.name                                   AS purchaseUnitName,
       businessUnit.name :: JSON ->> 'en'                  AS purchaseUnitNameEn,
       businessUnit.code                                   AS purchaseUnitcode

FROM iobcollectiondetails cd
       LEFT JOIN cobcollectiontype refDocumentType
                 ON cd.refDocumentTypeId = refDocumentType.id
       LEFT JOIN mobbusinessPartner ON cd.businessPartnerid = mobbusinessPartner.id
       LEFT JOIN MasterData businessPartner ON businessPartner.id = cd.businessPartnerid
       LEFT JOIN dobcollection c ON c.id = cd.refinstanceid
       INNER JOIN dobaccountingdocument crAccountingDocument ON c.id = crAccountingDocument.id
       LEFT JOIN cobcurrency ON cd.currencyid = cobcurrency.id
       LEFT JOIN iobaccountingdocumentcompanydata companyDetails
                 ON crAccountingDocument.id = companyDetails.refinstanceid
       LEFT JOIN cobenterprise businessUnit ON companyDetails.purchaseUnitId = businessUnit.id
       LEFT JOIN IObCompanyBankDetails ON IObCompanyBankDetails.id = cd.bankAccountId
       LEFT JOIN cobbank ON cobbank.id = IObCompanyBankDetails.bankid
       LEFT JOIN cobtreasury ON cobtreasury.id = cd.treasuryId
       LEFT JOIN IObAccountingDocumentCompanyData companyData on c.id = companyData.refinstanceid
       LEFT JOIN iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = companyData.companyid
       LEFT JOIN cobcurrency companyCurrency on companyCurrency.id = iobenterprisebasicdata.currencyid;

CREATE OR REPLACE VIEW IObCollectionAccountingDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       dobaccountingdocument.code   AS documentCode,
       accountDetails.creationDate,
       accountDetails.creationInfo,
       accountDetails.modificationInfo,
       accountDetails.modifieddate,
       accountDetails.accountingEntry,
       accountDetails.subledger     AS subLedger,
       account.code                 AS glAccountCode,
       account.name                 AS glAccountName,
       account.id                   AS glAccountId,
       accountDetails.amount,
       CASE
         WHEN accountDetails.subledger = 'Local_Customer' AND
              accountDetails.accountingEntry = 'CREDIT' THEN (
           customer.code
           )
         WHEN accountDetails.subledger = 'Notes_Receivables'
           AND accountDetails.accountingEntry = 'CREDIT' THEN (
           dobnotesreceivables.code
           )
         WHEN accountDetails.subledger = 'Banks' AND accountDetails.accountingEntry = 'DEBIT'
           THEN (bankSubAccount.accountno)
         WHEN accountDetails.subledger = 'Treasuries' AND accountDetails.accountingEntry = 'DEBIT'
           THEN (treasury.code)
         END                        AS glSubAccountCode,
       CASE
         WHEN accountDetails.subledger = 'Local_Customer' AND
              accountDetails.accountingEntry = 'CREDIT' THEN
           (customeraccountingdetails.glSubAccountId)
         WHEN accountDetails.subledger = 'Notes_Receivables'
           AND accountDetails.accountingEntry = 'CREDIT' THEN
           (notesreceivableaccountingdetails.glSubAccountId)
         WHEN accountDetails.subledger = 'Banks' AND accountDetails.accountingEntry = 'DEBIT'
           THEN (bankaccountingdetails.glSubAccountId)
         WHEN accountDetails.subledger = 'Treasuries' AND accountDetails.accountingEntry = 'DEBIT'
           THEN (treasuryaccountingdetails.glSubAccountId)
         END                        AS glSubAccountId,
       CASE
         WHEN accountDetails.subledger = 'Local_Customer' AND
              accountDetails.accountingEntry = 'CREDIT'
           THEN (customer.name)
         WHEN accountDetails.subledger = 'Banks' AND accountDetails.accountingEntry = 'DEBIT'
           THEN (cobbank.name)
         WHEN accountDetails.subledger = 'Treasuries' AND accountDetails.accountingEntry = 'DEBIT'
           THEN (treasury.name) END AS glSubAccountName
FROM IObAccountingDocumentAccountingDetails accountDetails
       LEFT JOIN dobcollection c ON accountDetails.refinstanceid = c.id
       INNER JOIN dobaccountingdocument ON dobaccountingdocument.id = c.id
       LEFT JOIN hobglaccount account ON account.id = accountDetails.glaccountid
       LEFT JOIN iobaccountingdocumentcustomeraccountingDetails customeraccountingdetails
                 ON customeraccountingdetails.id = accountDetails.id AND
                    accountDetails.accountingEntry = 'CREDIT'
       LEFT JOIN iobaccountingdocumentnotesreceivableaccountingdetails notesreceivableaccountingdetails
                 ON notesreceivableaccountingdetails.id = accountDetails.id AND
                    accountDetails.accountingEntry = 'CREDIT'
       LEFT JOIN iobaccountingdocumentbankaccountingdetails bankaccountingdetails
                 ON bankaccountingdetails.id = accountDetails.id AND
                    accountDetails.accountingEntry = 'DEBIT'
       LEFT JOIN iobaccountingdocumenttreasuryaccountingdetails treasuryaccountingdetails
                 ON treasuryaccountingdetails.id = accountDetails.id AND
                    accountDetails.accountingEntry = 'DEBIT'
       LEFT JOIN cobtreasury treasury ON treasuryaccountingdetails.glsubaccountid = treasury.id
       LEFT JOIN masterdata customer
                 ON customer.id = customeraccountingdetails.glSubAccountId AND customer.objecttypecode = '3'
       LEFT JOIN dobnotesreceivables ON dobnotesreceivables.id = notesreceivableaccountingdetails.glSubAccountId
       LEFT JOIN IObCompanyBankDetails bankSubAccount
                 ON bankSubAccount.id = bankaccountingdetails.glsubaccountid
       LEFT JOIN cobbank ON cobbank.id = bankSubAccount.bankid;