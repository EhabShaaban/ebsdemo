
CREATE Or Replace VIEW IObAccountingDocumentActivationDetailsGeneralModel AS
SELECT activationDetails.id,
       activationDetails.refinstanceid,
       activationDetails.creationDate,
       activationDetails.creationInfo,
       activationDetails.modificationInfo,
       activationDetails.modifieddate,
       accountingDocument.code,
       accountingDocument.objecttypecode                                 as documentType,
       activationDetails.accountant,
       activationDetails.activationdate,
       activationDetails.duedate,
       exchangeRate.code                                                 AS exchangeRateCode,
       exchangeRate.id                                                   AS exchangeRateId,
       exchangeRate.firstvalue,
       exchangeRate.secondvalue,
       exchangeRate.firstcurrencyiso,
       exchangeRate.secondcurrencyiso,
       activationDetails.currencyprice,
       getJournalEntryCodeDependsOnObjectTypeCode(activationDetails.objecttypecode,
                                                  accountingDocument.id) as journalEntryCode,
       activationDetails.objecttypecode
FROM IObAccountingDocumentActivationDetails activationDetails
         left JOIN dobaccountingdocument accountingDocument
                   on accountingDocument.id = activationDetails.refinstanceid
         left join cobexchangerategeneralmodel exchangeRate
                   on activationDetails.exchangerateid = exchangeRate.id;

