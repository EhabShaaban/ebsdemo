

ALTER TABLE IObAccountingDocumentPostingDetails RENAME TO IObAccountingDocumentActivationDetails;

ALTER TABLE IObAccountingDocumentActivationDetails RENAME column postingdate to activationDate;

ALTER SEQUENCE IObAccountingDocumentPostingDetails_id_seq RENAME TO IObAccountingDocumentActivationDetails_id_seq;
------------------------------------------------------------------------------------------