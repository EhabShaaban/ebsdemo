DROP FUNCTION getNotesReceivableRemaining(varchar);
DROP FUNCTION getNotesReceivablePaidAmount(varchar);

CREATE or REPLACE FUNCTION getNotesReceivablePaidAmount(NRCode varchar)
  RETURNS float AS $$
DECLARE remaining float;
BEGIN
  remaining = (
              select SUM(DObCollectionGeneralModel.amount)
              from DObCollectionGeneralModel
              where DObCollectionGeneralModel.refdocumentcode = NRCode
                and DObCollectionGeneralModel.currentStates like '%Active%'
                and DObCollectionGeneralModel.refdocumenttypecode like '%NOTES_RECEIVABLE%');
  return case when
    remaining is null then 0
  else remaining
    end;
END; $$
LANGUAGE PLPGSQL;
CREATE or REPLACE FUNCTION getNotesReceivableRemaining(NRCode varchar)
    RETURNS float AS $$
BEGIN
    RETURN (
            (select amount
             from iobnotesreceivablesdetails
             where refinstanceid = (select id from dobnotesreceivables where code = NRCode))
             - getNotesReceivablePaidAmount(NRCode));

END; $$
LANGUAGE PLPGSQL;

