
CREATE OR Replace VIEW DocumentOwnerGeneralModel AS
SELECT DISTINCT permission.permissionexpression,
                permission.objectname,
                userInfo.id   as userId,
                userInfo.name as userName,
                permissionassignment.authorizationconditionid,
                authorizationcondition.condition
FROM permission
         LEFT JOIN permissionassignment ON permission.id = permissionassignment.permissionid
         FULL OUTER JOIN authorizationcondition ON permissionassignment.authorizationconditionid =
                                                   authorizationcondition.id
         JOIN roleassignment ON roleassignment.roleid = permissionassignment.roleid
         JOIN userassignment ON userassignment.roleid = roleassignment.parentroleid
         JOIN role ON role.id = userassignment.roleid
         JOIN useraccount ON userassignment.userid = useraccount.id
         JOIN ebsuser ON useraccount.userid = ebsuser.id
         JOIN userInfo ON userInfo.id = ebsuser.id
WHERE permission.permissionexpression LIKE '%:CanBeDocumentOwner'
