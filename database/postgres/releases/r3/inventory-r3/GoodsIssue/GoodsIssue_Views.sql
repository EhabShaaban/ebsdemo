CREATE OR REPLACE VIEW DObGoodsIssueGeneralModel AS
SELECT DObInventoryDocument.id,
       DObInventoryDocument.code,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.currentStates,
       ce4.name                           AS purchaseUnitName,
       ce4.name:: JSON ->> 'en'           AS purchaseUnitNameEn,
       ce4.code                           AS purchaseUnitCode,
       cobgoodsissue.name                 AS typeName,
       cobgoodsissue.name:: JSON ->> 'en' AS typeNameEn,
       cobgoodsissue.code                 AS typeCode,
       ce2.name                           AS plantName,
       ce2.code                           AS plantCode,
       ce3.code                           AS storehouseCode,
       ce3.name                           AS storehouseName,
       storekeeper.code                   AS storekeeperCode,
       storekeeper.name                   AS storekeeperName,
       ce1.code                           AS companyCode,
       ce1.name                           AS companyName,
       MasterData.name                    AS customerName,
       MasterData.code                    AS customerCode,
       DObInventoryDocument.inventoryDocumentType

FROM DObInventoryDocument
       LEFT JOIN IObGoodsIssueSalesInvoiceData
                 ON DObInventoryDocument.id = IObGoodsIssueSalesInvoiceData.refInstanceId
                   AND DObInventoryDocument.inventoryDocumentType = 'GI_SI'
       LEFT JOIN MasterData
                 ON IObGoodsIssueSalesInvoiceData.customerId = MasterData.Id AND MasterData.objecttypecode = '3'
       LEFT JOIN IObInventoryCompany
                 ON DObInventoryDocument.id = IObInventoryCompany.refInstanceId
       LEFT JOIN dobgoodsissuesalesinvoice
                 ON DObInventoryDocument.id = dobgoodsissuesalesinvoice.id
       JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
       JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
       JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
       JOIN cobenterprise ce4 ON ce4.id = DObInventoryDocument.purchaseunitid
       JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid
       JOIN cobgoodsissue ON dobgoodsissuesalesinvoice.typeid = cobgoodsissue.id
WHERE DObInventoryDocument.inventoryDocumentType = 'GI_SI'

UNION ALL

SELECT DObInventoryDocument.id,
       DObInventoryDocument.code,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.currentStates,
       ce4.name                           AS purchaseUnitName,
       ce4.name:: JSON ->> 'en'           AS purchaseUnitNameEn,
       ce4.code                           AS purchaseUnitCode,
       cobgoodsissue.name                 AS typeName,
       cobgoodsissue.name:: JSON ->> 'en' AS typeNameEn,
       cobgoodsissue.code                 AS typeCode,
       ce2.name                           AS plantName,
       ce2.code                           AS plantCode,
       ce3.code                           AS storehouseCode,
       ce3.name                           AS storehouseName,
       storekeeper.code                   AS storekeeperCode,
       storekeeper.name                   AS storekeeperName,
       ce1.code                           AS companyCode,
       ce1.name                           AS companyName,
       MasterData.name                    AS customerName,
       MasterData.code                    AS customerCode,
       DObInventoryDocument.inventoryDocumentType

FROM DObInventoryDocument
       LEFT JOIN IObGoodsIssueSalesOrderData
                 ON DObInventoryDocument.id = IObGoodsIssueSalesOrderData.refInstanceId
                   AND DObInventoryDocument.inventoryDocumentType = 'GI_SO'
       LEFT JOIN MasterData
                 ON IObGoodsIssueSalesOrderData.customerId = MasterData.Id AND MasterData.objecttypecode = '3'
       LEFT JOIN IObInventoryCompany
                 ON DObInventoryDocument.id = IObInventoryCompany.refInstanceId
       LEFT JOIN dobgoodsissuesalesorder
                 ON DObInventoryDocument.id = dobgoodsissuesalesorder.id
       LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
       LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
       LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
       LEFT JOIN cobenterprise ce4 ON ce4.id = DObInventoryDocument.purchaseunitid
       LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid
       JOIN cobgoodsissue ON dobgoodsissuesalesorder.typeid = cobgoodsissue.id
WHERE DObInventoryDocument.inventoryDocumentType = 'GI_SO';
