ALTER TABLE tobstoretransaction RENAME COLUMN cost TO estimatecost;

ALTER TABLE tobstoretransaction RENAME COLUMN transactioncost TO actualcost;