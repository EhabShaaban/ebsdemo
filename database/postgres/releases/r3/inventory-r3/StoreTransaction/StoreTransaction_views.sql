CREATE VIEW TObStoreTransactionGeneralModel AS
select transaction.id,
       transaction.code,
       transaction.originalAddingDate,
       transaction.updatingActualCostDate,
       transaction.transactionOperation,
       transaction.creationinfo,
       transaction.creationdate,
       transaction.modificationinfo,
       transaction.modifieddate,
       c1.code AS purchaseUnitCode,
       c1.name AS purchaseUnitName,
       c1.name::json ->> 'en' AS purchaseUnitNameEn,
       c2.code as companyCode,
       c2.name as companyName,
       c3.code as plantCode,
       c3.name as plantName,
       c4.code as storehouseCode,
       c4.name as storehouseName,
       transaction.stocktype,
       transaction.batchNo,
       m.code as itemCode,
       m.name as itemName,
       cm.code as unitOfMeasureCode,
       cm.symbol as unitOfMeasureName,
       transaction.quantity,
       round(cast(transaction.estimatecost as numeric), 4)                            as estimatecost,
       round(cast(transaction.actualcost as numeric), 4)                              as actualcost,
       getStoreTransactionRefDocumentCode(transaction.objectTypeCode, transaction.id) as refDocumentCode,
       getStoreTransactionRefDocumentType(transaction.objectTypeCode, transaction.id) as refDocumentType,
       transaction.remainingquantity,
       reftransaction.code as refTransactionCode
from TObStoreTransaction transaction
 join cobenterprise c1 ON transaction.purchaseunitid = c1.id
 join cobenterprise c2 ON transaction.companyid = c2.id
 join cobenterprise c3 ON transaction.plantid = c3.id
 join cobenterprise c4 ON transaction.storehouseid = c4.id
 join masterdata m ON transaction.itemid = m.id
 join cobmeasure cm ON transaction.unitofmeasureid = cm.id
 LEFT join TObStoreTransaction reftransaction ON transaction.reftransactionid = reftransaction.id;


CREATE VIEW TObGoodsIssueStoreTransactionGeneralModel AS
SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
              round(cast(TObStoreTransaction.estimatecost as numeric), 4) as estimatecost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.actualcost as numeric), 4) as actualcost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionoperation,
       c1.code            as companyCode,
       c1.name            as companyName,
       c2.code            as storehouseCode,
       c2.name            as storehouseName,
       c3.code            as plantCode,
       c3.name            as plantName,
       c4.code            as purchaseUnitCode,
       c4.name            as purchaseUnitName,
       cobmeasure.symbol  as unitOfMeasureSymbol,
       cobmeasure.name    as unitOfMeasureName,
       cobmeasure.code    as unitOfMeasureCode,
       t2.code            as refTransactionCode,
       masterdata.name    as itemName,
       masterdata.code    as itemCode,
       DObInventoryDocument.code as goodsIssueCode

FROM TObStoreTransaction
         left join TObGoodsIssueStoreTransaction
                   on TObStoreTransaction.id = TObGoodsIssueStoreTransaction.id
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         join mobitem on TObStoreTransaction.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         join DObInventoryDocument on TObGoodsIssueStoreTransaction.documentReferenceId = DObInventoryDocument.id;



CREATE VIEW TObStoreTransactionAllDataGeneralModel AS
    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.estimatecost as numeric), 4) as estimatecost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.actualcost as numeric), 4) as actualcost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           DObInventoryDocument.code                 as documentCode

    FROM TObStoreTransaction
             left join TObGoodsIssueStoreTransaction
                       on TObStoreTransaction.id = TObGoodsIssueStoreTransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             join DObInventoryDocument
                  on TObGoodsIssueStoreTransaction.documentReferenceId = DObInventoryDocument.id

    union

SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.originalAddingDate,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
       round(cast(TObStoreTransaction.estimatecost as numeric), 4) as estimatecost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.actualcost as numeric), 4) as actualcost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionOperation,
       c1.code                            as companyCode,
       c1.name :: json ->> 'en'           as companyName,
       c2.code                            as storehouseCode,
       c2.name :: json ->> 'en'           as storehouseName,
       c3.code                            as plantCode,
       c3.name :: json ->> 'en'           as plantName,
       c4.code                            as purchaseUnitCode,
       c4.name :: json ->> 'en'           as purchaseUnitName,
       cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
       cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
       cobmeasure.code                    as unitOfMeasureCode,
       t2.code                            as refTransactionCode,
       masterdata.name :: json ->> 'en'   as itemName,
       masterdata.code                    as itemCode,
       'Legacy System نظام المتكامل'      as documentCode

FROM TObStoreTransaction
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         join mobitem on TObStoreTransaction.itemId = mobitem.id
         join masterdata on mobitem.id = masterdata.id
         JOIN tobgoodsreceiptstoretransaction on TObStoreTransaction.id = tobgoodsreceiptstoretransaction.id
and tobgoodsreceiptstoretransaction.documentreferenceid is null

    union

    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.estimatecost as numeric), 4) as estimatecost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.actualcost as numeric), 4) as actualcost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           dobinventorydocument.code               as documentCode

    FROM TObStoreTransaction
             left join TObGoodsReceiptStoreTransaction
                       on TObStoreTransaction.id = TObGoodsReceiptStoreTransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             JOIN dobinventorydocument
                  on TObGoodsReceiptStoreTransaction.documentReferenceId = dobinventorydocument.id

    union

    SELECT TObStoreTransaction.id,
           TObStoreTransaction.code,
           TObStoreTransaction.originalAddingDate,
           TObStoreTransaction.creationDate,
           TObStoreTransaction.modifiedDate,
           TObStoreTransaction.creationInfo,
           TObStoreTransaction.modificationInfo,
           TObStoreTransaction.quantity,
                  round(cast(TObStoreTransaction.estimatecost as numeric), 4) as estimatecost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.actualcost as numeric), 4) as actualcost,
           TObStoreTransaction.batchNo,
           TObStoreTransaction.remainingQuantity,
           TObStoreTransaction.transactionOperation,
           c1.code                            as companyCode,
           c1.name :: json ->> 'en'           as companyName,
           c2.code                            as storehouseCode,
           c2.name :: json ->> 'en'           as storehouseName,
           c3.code                            as plantCode,
           c3.name :: json ->> 'en'           as plantName,
           c4.code                            as purchaseUnitCode,
           c4.name :: json ->> 'en'           as purchaseUnitName,
           cobmeasure.symbol :: json ->> 'en' as unitOfMeasureSymbol,
           cobmeasure.name :: json ->> 'en'   as unitOfMeasureName,
           cobmeasure.code                    as unitOfMeasureCode,
           t2.code                            as refTransactionCode,
           masterdata.name :: json ->> 'en'   as itemName,
           masterdata.code                    as itemCode,
           DObInventoryDocument.code           as documentCode

    FROM TObStoreTransaction
             left join tobstocktransformationstoretransaction
                       on TObStoreTransaction.id = tobstocktransformationstoretransaction.id
             JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
             JOIN cobenterprise c1 on c1.id = cobcompany.id
             JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
             JOIN cobenterprise c2 on c2.id = cobstorehouse.id
             JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
             JOIN cobenterprise c3 on c3.id = cobplant.id
             JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
             JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
             left Join TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
             join mobitem on TObStoreTransaction.itemId = mobitem.id
             join masterdata on mobitem.id = masterdata.id
             JOIN DObInventoryDocument
                  on tobstocktransformationstoretransaction.documentReferenceId = DObInventoryDocument.id and inventoryDocumentType = 'StkTr';




CREATE or REPLACE FUNCTION getStoreTransactionRefDocumentCode(objectTypeCode varchar, transactionId bigint)
  RETURNS varchar AS
$$
BEGIN
  RETURN case
           when objecttypecode = 'GR' and (select documentreferenceid
                                           from tobgoodsreceiptstoretransaction gr
                                           where gr.id = transactionId) is null then 'Legacy System نظام المتكامل'
           when objecttypecode = 'GR' then
             (select code
              from dobinventorydocument
              where id = (select documentreferenceid
                          from tobgoodsreceiptstoretransaction gr
                          where gr.id = transactionId))

           when objecttypecode = 'GI' then
             (select code
              from dobinventorydocument
              where id = (select documentreferenceid
                          from TObGoodsIssueStoreTransaction gi
                          where gi.id = transactionId))

           when objecttypecode = 'STK_TR' then
             (select code
              from dobinventorydocument
              where id = (select documentreferenceid
                          from TObstocktransformationStoreTransaction gi
                          where gi.id = transactionId))

    end;
END;
$$
  LANGUAGE PLPGSQL;

CREATE VIEW TObGoodsReceiptStoreTransactionGeneralModel AS
SELECT TObStoreTransaction.id,
       TObStoreTransaction.code,
       TObStoreTransaction.originalAddingDate,
       TObStoreTransaction.creationDate,
       TObStoreTransaction.modifiedDate,
       TObStoreTransaction.creationInfo,
       TObStoreTransaction.modificationInfo,
       TObStoreTransaction.quantity,
              round(cast(TObStoreTransaction.estimatecost as numeric), 4) as estimatecost,
       TObStoreTransaction.stockType,
       round(cast(TObStoreTransaction.actualcost as numeric), 4) as actualcost,
       TObStoreTransaction.batchNo,
       TObStoreTransaction.remainingQuantity,
       TObStoreTransaction.transactionOperation,
       c1.code              as companyCode,
       c1.name              as companyName,
       c2.code              as storehouseCode,
       c2.name              as storehouseName,
       c3.code              as plantCode,
       c3.name              as plantName,
       c4.code              as purchaseUnitCode,
       c4.name              as purchaseUnitName,
       cobmeasure.symbol    AS unitOfMeasureSymbol,
       cobmeasure.name      AS unitOfMeasureName,
       cobmeasure.code      AS unitOfMeasureCode,
       t2.id                as refTransactionId,
       t2.code              as refTransactionCode,
       masterdata.name      AS itemName,
       masterdata.code      AS itemCode,
       getStoreTransactionRefDocumentCode(TObStoreTransaction.objecttypecode,TObStoreTransaction.id)    AS goodsReceiptCode

FROM TObStoreTransaction
         JOIN cobcompany on cobcompany.id = TObStoreTransaction.companyId
         JOIN cobenterprise c1 on c1.id = cobcompany.id
         JOIN cobstorehouse on cobstorehouse.id = TObStoreTransaction.storehouseId
         JOIN cobenterprise c2 on c2.id = cobstorehouse.id
         JOIN cobplant on cobplant.id = TObStoreTransaction.plantId
         JOIN cobenterprise c3 on c3.id = cobplant.id
         JOIN cobenterprise c4 on TObStoreTransaction.purchaseUnitId = c4.id
         JOIN cobmeasure on TObStoreTransaction.unitOfMeasureId = cobmeasure.id
         left JOIN TObStoreTransaction t2 on TObStoreTransaction.refTransactionId = t2.id
         JOIN mobitem on TObStoreTransaction.itemId = mobitem.id
         JOIN masterdata on mobitem.id = masterdata.id
  where TObStoreTransaction.objecttypecode = 'GR';

CREATE VIEW TObGoodsReceiptStoreTransactionDataGeneralModel AS
SELECT ROW_NUMBER()
                                                                    OVER ()                                                   AS id,
       DObInventoryDocument.creationdate,
       DObInventoryDocument.modifiedDate,
       DObInventoryDocument.creationInfo,
       DObInventoryDocument.modificationInfo,
       DObInventoryDocument.code,
       DObGoodsReceiptPurchaseOrder.id                           AS goodsReceiptId,
       iobinventorycompany.storehouseid,
       iobinventorycompany.plantid,

       cobplant.companyid,

       dobinventoryDocument.purchaseunitid,

       IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid,

       c1.symbol                                                 AS baseUnitSymbol,
       masterdata.code                                           AS itemCode,
       masterdata.name                                           AS itemName,
       (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
        alternative1.conversionfactor)                           AS receivedQtyBase,

       (iobgoodsreceiptitembatches.receivedqtyuoe *
        alternative2.conversionfactor)                           AS receivedBatchQtyBase,

       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedcost  AS estimatedCostQty,
       iobgoodsreceiptitembatches.estimatedcost                  AS estimatedCostBatch,

       IObGoodsReceiptPurchaseOrderItemQuantities.actualcost     AS actualCostQty,
       iobgoodsreceiptitembatches.actualcost                     AS actualCostBatch,

       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe AS receivedQty,
       IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid  AS receivedUoeId,
       c2.code                                                   AS receivedUoeCode,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype      AS receivedStockType,


       IObGoodsReceiptItemBatches.batchcode                      AS batchNo,
       IObGoodsReceiptItemBatches.receivedqtyuoe                 AS batchQty,
       IObGoodsReceiptItemBatches.unitofentryid                  AS batchUoeId,
       IObGoodsReceiptItemBatches.stocktype                      AS batchStockType

FROM DObGoodsReceiptPurchaseOrder
         LEFT JOIN dobinventorydocument ON DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id AND
                                           dobinventorydocument.inventorydocumenttype = 'GR_PO'
         LEFT JOIN iobinventorycompany ON iobinventorycompany.refinstanceid = dobinventorydocument.id
         LEFT JOIN cobplant ON iobinventorycompany.plantid = cobplant.id
         LEFT JOIN IObGoodsReceiptPurchaseOrderData
                   ON DObGoodsReceiptPurchaseOrder.id = IObGoodsReceiptPurchaseOrderData.refinstanceid
         LEFT JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
                   ON DObGoodsReceiptPurchaseOrder.id = IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         LEFT JOIN IObGoodsReceiptPurchaseOrderItemQuantities
                   ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                      IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid

         LEFT JOIN IObGoodsReceiptItemBatches
                   ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id = IObGoodsReceiptItemBatches.refinstanceid
         LEFT JOIN mobitem ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitem.id
         LEFT JOIN iobalternativeuom alternative2 ON mobitem.id = alternative2.refinstanceid AND
                                                     alternative2.alternativeunitofmeasureid =
                                                     iobgoodsreceiptitembatches.unitofentryid
         LEFT JOIN iobalternativeuom alternative1 ON mobitem.id = alternative1.refinstanceid AND
                                                     alternative1.alternativeunitofmeasureid =
                                                     IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
         LEFT JOIN masterdata ON mobitem.id = masterdata.id
         LEFT JOIN cobmeasure c1 ON c1.id = mobitem.basicunitofmeasure
         LEFT JOIN cobmeasure c2 ON c2.id = IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;
