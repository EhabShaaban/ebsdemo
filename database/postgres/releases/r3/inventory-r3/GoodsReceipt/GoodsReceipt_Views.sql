
CREATE VIEW DObGoodsReceiptGeneralModel AS
SELECT DObGoodsReceiptPurchaseOrder.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborderdocument.code                       AS refDocumentCode,
       CObGoodsReceipt.code                        AS type,
       CObGoodsReceipt.name                        AS typeName,
       PurchaseUnitData.name :: JSON ->> 'en'      AS purchaseUnitNameEn,
       PurchaseUnitData.name                       AS purchaseUnitName,
       PurchaseUnitData.code                       AS PurchaseUnitCode,
       ce1.name                                    AS companyName,
       ce1.code                                    AS companyCode,
       ce2.name                                    AS plantName,
       ce2.code                                    AS plantCode,
       IObGoodsReceiptPurchaseOrderData.vendorName AS businessPartnerName,
       masterdata.code                             AS businessPartnercode,
       ce3.code                                    AS storehouseCode,
       ce3.name                                    AS storehouseName,
       storekeeper.code                            AS storekeeperCode,
       storekeeper.name                            AS storekeeperName
FROM DObGoodsReceiptPurchaseOrder
       LEFT JOIN dobinventorydocument ON DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
       LEFT JOIN iobinventorycompany
                 ON iobinventorycompany.refinstanceid = dobinventorydocument.id
       LEFT JOIN IObInventoryActivationDetails
                 ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
       JOIN iobgoodsreceiptpurchaseorderdata
            ON iobgoodsreceiptpurchaseorderdata.refinstanceid = DObGoodsReceiptPurchaseOrder.id
       LEFT JOIN cobenterprise AS PurchaseUnitData
                 ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                   AND PurchaseUnitData.objectTypeCode = '5'
       LEFT JOIN masterdata
                 ON IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id AND
                    masterdata.objecttypecode LIKE '2'
       LEFT JOIN doborderdocument
                 ON iobgoodsreceiptpurchaseorderdata.purchaseOrderId = doborderdocument.id AND
                    doborderdocument.objecttypecode LIKE '%_PO'
       LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = DObGoodsReceiptPurchaseOrder.typeid
       LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
       LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
       LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
       LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid

UNION ALL
SELECT DObGoodsReceiptSalesReturn.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       dobsalesreturnorder.code               AS refDocumentCode,
       CObGoodsReceipt.code                   AS type,
       CObGoodsReceipt.name                   AS typeName,
       PurchaseUnitData.name :: JSON ->> 'en' AS purchaseUnitNameEn,
       PurchaseUnitData.name                  AS purchaseUnitName,
       PurchaseUnitData.code                  AS PurchaseUnitCode,
       ce1.name                               AS companyName,
       ce1.code                               AS companyCode,
       ce2.name                               AS plantName,
       ce2.code                               AS plantCode,
       masterdata.name                        AS businessPartnerName,
       masterdata.code                        AS businessPartnercode,
       ce3.code                               AS storehouseCode,
       ce3.name                               AS storehouseName,
       storekeeper.code                       AS storekeeperCode,
       storekeeper.name                       AS storekeeperName

FROM DObGoodsReceiptSalesReturn
       LEFT JOIN dobinventorydocument ON DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
       LEFT JOIN iobinventorycompany
                 ON iobinventorycompany.refinstanceid = dobinventorydocument.id
       LEFT JOIN IObInventoryActivationDetails
                 ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
       JOIN IObGoodsReceiptSalesReturnData
            ON IObGoodsReceiptSalesReturnData.refinstanceid = DObGoodsReceiptSalesReturn.id
       LEFT JOIN dobsalesreturnorder
                 ON iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
       LEFT JOIN cobenterprise AS PurchaseUnitData
                 ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
                   AND PurchaseUnitData.objectTypeCode = '5'
       LEFT JOIN mobcustomer
                 ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
       LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
       LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
       LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = dobgoodsreceiptsalesreturn.typeid
       LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
       LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
       LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
       LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid;


CREATE VIEW DObGoodsReceiptDataGeneralModel AS
SELECT DObGoodsReceiptPurchaseOrder.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       doborderdocument.code                       AS refDocumentCode,
       CObGoodsReceipt.code                        AS type,
       CObGoodsReceipt.name                        AS typeName,
       PurchaseUnitData.id                         AS purchaseUnitId,
       PurchaseUnitData.name :: JSON ->> 'en'      AS purchaseUnitNameEn,
        PurchaseUnitData.name                       AS purchaseUnitName,
        PurchaseUnitData.code                       AS PurchaseUnitCode,
        ce1.id                                      AS companyId,
        ce1.name                                    AS companyName,
        ce1.code                                    AS companyCode,
        ce2.id                                      AS plantId,
        ce2.name                                    AS plantName,
        ce2.code                                    AS plantCode,
        ce3.id                                      AS storehouseId,
        ce3.name                                    AS storehouseName,
        ce3.code                                    AS storehouseCode,
        IObGoodsReceiptPurchaseOrderData.vendorName AS businessPartnerName,
        masterdata.code                             AS businessPartnercode,
        storekeeper.code                            AS storekeeperCode,
        storekeeper.name                            AS storekeeperName
        FROM DObGoodsReceiptPurchaseOrder
        LEFT JOIN dobinventorydocument ON DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
        LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = DObGoodsReceiptPurchaseOrder.typeid
        LEFT JOIN iobinventorycompany
        ON iobinventorycompany.refinstanceid = dobinventorydocument.id
        LEFT JOIN IObInventoryActivationDetails
        ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
        JOIN iobgoodsreceiptpurchaseorderdata
        ON iobgoodsreceiptpurchaseorderdata.refinstanceid = DObGoodsReceiptPurchaseOrder.id
        LEFT JOIN cobenterprise AS PurchaseUnitData
        ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
        AND PurchaseUnitData.objectTypeCode = '5'
        LEFT JOIN masterdata ON IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id AND
        masterdata.objecttypecode LIKE '2'
        LEFT JOIN doborderdocument
        ON iobgoodsreceiptpurchaseorderdata.purchaseOrderId = doborderdocument.id AND
        doborderdocument.objecttypecode LIKE '%_PO'
        LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
        LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
        LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
        LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid

        UNION ALL
SELECT DObGoodsReceiptSalesReturn.id,
       DObInventoryDocument.code,
       IObInventoryActivationDetails.activationDate,
       DObInventoryDocument.creationDate,
       DObInventoryDocument.modifieddate,
       DObInventoryDocument.creationinfo,
       DObInventoryDocument.modificationinfo,
       DObInventoryDocument.currentStates,
       DObInventoryDocument.inventoryDocumentType,
       dobsalesreturnorder.code               AS refDocumentCode,
       CObGoodsReceipt.code                   AS type,
       CObGoodsReceipt.name                   AS typeName,
       PurchaseUnitData.id                    AS purchaseUnitId,
       PurchaseUnitData.name :: JSON ->> 'en' AS purchaseUnitNameEn,
       PurchaseUnitData.name                  AS purchaseUnitName,
       PurchaseUnitData.code                  AS PurchaseUnitCode,
       ce1.id                                 AS companyId,
       ce1.name                               AS companyName,
       ce1.code                               AS companyCode,
       ce2.id                                 AS plantId,
       ce2.name                               AS plantName,
       ce2.code                               AS plantCode,
       ce3.id                                 AS storehouseId,
       ce3.name                               AS storehouseName,
       ce3.code                               AS storehouseCode,
       masterdata.name                        AS businessPartnerName,
       masterdata.code                        AS businessPartnercode,
       storekeeper.code                       AS storekeeperCode,
       storekeeper.name                       AS storekeeperName

FROM DObGoodsReceiptSalesReturn
    LEFT JOIN dobinventorydocument ON DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
    LEFT JOIN CObGoodsReceipt ON CObGoodsReceipt.id = dobgoodsreceiptsalesreturn.typeid
    LEFT JOIN iobinventorycompany
    ON iobinventorycompany.refinstanceid = dobinventorydocument.id
    LEFT JOIN IObInventoryActivationDetails
    ON IObInventoryActivationDetails.refinstanceid = dobinventorydocument.id
    JOIN IObGoodsReceiptSalesReturnData
    ON IObGoodsReceiptSalesReturnData.refinstanceid = DObGoodsReceiptSalesReturn.id
    LEFT JOIN dobsalesreturnorder
    ON iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
    LEFT JOIN cobenterprise AS PurchaseUnitData
    ON dobinventorydocument.purchaseunitid = PurchaseUnitData.id
    AND PurchaseUnitData.objectTypeCode = '5'
    LEFT JOIN mobcustomer ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
    LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
    LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
    LEFT JOIN cobenterprise ce1 ON ce1.id = iobinventorycompany.companyid
    LEFT JOIN cobenterprise ce2 ON ce2.id = iobinventorycompany.plantid
    LEFT JOIN cobenterprise ce3 ON ce3.id = iobinventorycompany.storehouseid
    LEFT JOIN storekeeper ON storekeeper.id = iobinventorycompany.storekeeperid;



CREATE VIEW IObGoodsReceiptPurchaseOrderDataGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderData.id,
       IObGoodsReceiptPurchaseOrderData.refInstanceId,
       IObGoodsReceiptPurchaseOrderData.deliveryNote,
       IObGoodsReceiptPurchaseOrderData.comments,
       dobinventorydocument.code AS goodsReceiptCode,
       POData.purchaseOrderCode,
       VendorData.vendorCode,
       IObGoodsReceiptPurchaseOrderData.vendorName,
       CObEnterprise.code        AS purchaseUnitCode,
       CObEnterprise.name        AS purchaseUnitName,
       IObGoodsReceiptPurchaseOrderData.purchaseResponsibleName
FROM IObGoodsReceiptPurchaseOrderData
         LEFT JOIN DObGoodsReceiptPurchaseOrder
                   ON IObGoodsReceiptPurchaseOrderData.refInstanceId =
                      DObGoodsReceiptPurchaseOrder.id
         LEFT join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN CObEnterprise
                   on CObEnterprise.objectTypeCode = '5'
                       AND dobinventorydocument.purchaseunitid = CObEnterprise.id
         LEFT JOIN (SELECT id, masterdata.code AS vendorCode, masterdata.name AS vendorName
                    FROM masterdata
                    WHERE masterdata.objectTypeCode = '2') AS VendorData
                   ON IObGoodsReceiptPurchaseOrderData.vendorId = VendorData.id
         LEFT JOIN (SELECT id, doborderdocument.code AS purchaseOrderCode
                    FROM doborderdocument
                    WHERE objecttypecode LIKE '%_PO') AS POData
                   ON IObGoodsReceiptPurchaseOrderData.purchaseOrderId = POData.id;


CREATE VIEW IObGoodsReceiptReceivedItemsGeneralModel AS
SELECT row_number() over ()                                                        AS id,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.id                            AS grItemId,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid                 AS goodsreceiptinstanceid,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationdate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modifieddate,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.creationinfo,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                                   as goodsReceiptCode,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.differencereason,
       IObGoodsReceiptPurchaseOrderReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                                    as itemcode,
       mobitemgeneralmodel.name                                                    as itemname,
       mobitemgeneralmodel.batchmanagementrequired                                 as isBatchManaged,
       cobmeasure.symbol                                                           as baseunit,
       cobmeasure.code                                                             as baseunitcode,
       (SELECT quantityInDn
        FROM IObOrderLineDetailsGeneralModel
                 left join iobgoodsreceiptpurchaseorderdatageneralmodel
                           ON iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseOrderCode =
                              IObOrderLineDetailsGeneralModel.orderCode
        WHERE iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
          AND IObOrderLineDetailsGeneralModel.itemCode = mobitemgeneralmodel.code) AS quantityInDn,
       null                                                                        AS returnedQuantityBase
FROM IObGoodsReceiptPurchaseOrderReceivedItemsData
         JOIN DObGoodsReceiptPurchaseOrder
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceId =
                 DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument
                   on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_PO'
         left join mobitemgeneralmodel
                   on IObGoodsReceiptPurchaseOrderReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
UNION ALL
SELECT row_number() over () +
       (SELECT COUNT(*) FROM IObGoodsReceiptPurchaseOrderReceivedItemsData) AS id,
       IObGoodsReceiptSalesReturnReceivedItemsData.id                       AS grItemId,
       IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid            AS goodsreceiptinstanceid,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationdate,
       IObGoodsReceiptSalesReturnReceivedItemsData.modifieddate,
       IObGoodsReceiptSalesReturnReceivedItemsData.creationinfo,
       IObGoodsReceiptSalesReturnReceivedItemsData.modificationinfo,
       dobinventorydocument.code                                            as goodsReceiptCode,
       IObGoodsReceiptSalesReturnReceivedItemsData.differencereason,
       IObGoodsReceiptSalesReturnReceivedItemsData.objecttypecode,
       mobitemgeneralmodel.code                                             as itemcode,
       mobitemgeneralmodel.name                                             as itemname,
       mobitemgeneralmodel.batchmanagementrequired                          as isBatchManaged,
       cobmeasure.symbol                                                    as baseunit,
       cobmeasure.code                                                      as baseunitcode,
       null                                                                 AS quantityInDn,
       (SELECT SUM(IObSalesReturnOrderItemGeneralModel.returnQuantityInBase)
        FROM IObSalesReturnOrderItemGeneralModel
        WHERE IObSalesReturnOrderItemGeneralModel.refinstanceid =
              iobgoodsreceiptsalesreturndata.salesreturnid
          AND IObSalesReturnOrderItemGeneralModel.itemId =
              mobitemgeneralmodel.id)                                       AS returnedQuantityBase
FROM IObGoodsReceiptSalesReturnReceivedItemsData
         JOIN dobgoodsreceiptsalesreturn
              ON IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceId =
                 dobgoodsreceiptsalesreturn.id
         left JOIN dobinventorydocument
                   on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id and
                      dobinventorydocument.inventorydocumenttype = 'GR_SR'
         left JOIN mobitemgeneralmodel
                   on IObGoodsReceiptSalesReturnReceivedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode
         left JOIN iobgoodsreceiptsalesreturndata
                   ON iobgoodsreceiptsalesreturndata.refinstanceid = dobgoodsreceiptsalesreturn.id;


CREATE VIEW IObGoodsReceiptPurchaseOrderOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderItemQuantities.id,
       IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe,
       IObGoodsReceiptPurchaseOrderItemQuantities.stocktype,
       IObGoodsReceiptPurchaseOrderItemQuantities.notes,
       IObGoodsReceiptPurchaseOrderItemQuantities.estimatedCost,
       IObGoodsReceiptPurchaseOrderItemQuantities.actualCost,
       cobmeasure.symbol                                        AS unitofentry,
       cobmeasure.code                                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN (IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe)
                     * iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptPurchaseOrderItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                                 AS receivedqtyWithDefectsbase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                                 AS baseunitfactor,
       mobitemgeneralmodel.code                                 as itemCode,
       mobitemgeneralmodel.name                                 as itemName,
       dobinventorydocument.code                                AS goodsreceiptcode,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                                        AS itemCodeAtVendor
FROM IObGoodsReceiptPurchaseOrderItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 IObGoodsReceiptPurchaseOrderItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptPurchaseOrderItemQuantities.unitofentryid;



CREATE VIEW IObGoodsReceiptSalesReturnOrdinaryReceivedItemGeneralModel AS
SELECT IObGoodsReceiptSalesReturnItemQuantities.id,
       IObGoodsReceiptSalesReturnItemQuantities.refinstanceid AS ordinaryitemid,
       dobinventorydocument.code                              AS goodsreceiptcode,
       IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe,
       IObGoodsReceiptSalesReturnItemQuantities.stocktype,
       IObGoodsReceiptSalesReturnItemQuantities.notes,
       cobmeasure.symbol                                      AS unitofentry,
       cobmeasure.code                                        AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
                     THEN IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptSalesReturnItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                               AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptSalesReturnItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                               AS baseunitfactor,
       masterdata.code                                        as itemCode,
       masterdata.name                                        as itemName,
       (SELECT iobgoodsreceiptreceiveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE masterdata.code::text = iobgoodsreceiptreceiveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)                      AS baseUnitSymbol
FROM IObGoodsReceiptSalesReturnItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptSalesReturnItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptSalesReturnReceivedItemsData
              ON IObGoodsReceiptSalesReturnReceivedItemsData.id =
                 IObGoodsReceiptSalesReturnItemQuantities.refinstanceid
         JOIN masterdata
              ON IObGoodsReceiptSalesReturnReceivedItemsData.itemid = masterdata.id
         JOIN DObGoodsReceiptSalesReturn ON DObGoodsReceiptSalesReturn.id =
                                            IObGoodsReceiptSalesReturnReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptSalesReturn.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON masterdata.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid =
        IObGoodsReceiptSalesReturnItemQuantities.unitofentryid;


CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS
SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.stocktype,
       iobgoodsreceiptitembatches.notes,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE iobgoodsreceiptitembatches.receivedqtyuoe
                 END ::numeric,
             3)                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobinventorydocument.code                as goodsReceiptCode,
       (SELECT baseUnit
        FROM iobgoodsreceiptreceiveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code =
              iobgoodsreceiptreceiveditemsgeneralmodel.itemCode
          AND iobgoodsreceiptreceiveditemsgeneralmodel.goodsreceiptcode =
              dobinventorydocument.code)        AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseorderdatageneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseorderdatageneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseorderdatageneralmodel.goodsReceiptCode =
              dobinventorydocument.code
       )                                        AS itemCodeAtVendor
FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptPurchaseOrderReceivedItemsData
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.id =
                 iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptPurchaseOrderReceivedItemsData.itemid = mobitemgeneralmodel.id
         JOIN DObGoodsReceiptPurchaseOrder ON DObGoodsReceiptPurchaseOrder.id =
                                              IObGoodsReceiptPurchaseOrderReceivedItemsData.refinstanceid
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;


create view IObGoodsReceiptAccountingDocumentDetailsGeneralModel AS
select IObGoodsReceiptAccountingDocumentDetails.id,
       dobgoodsreceiptaccountingdocument.code as userCode,
       IObGoodsReceiptAccountingDocumentDetails.refinstanceid,
       IObGoodsReceiptAccountingDocumentDetails.creationDate,
       IObGoodsReceiptAccountingDocumentDetails.creationInfo,
       IObGoodsReceiptAccountingDocumentDetails.modifiedDate,
       IObGoodsReceiptAccountingDocumentDetails.modificationInfo,
       dobinventorydocument.code              as goodsReceiptCode,
       doborderdocument.code                  as purchaseOrderCode
from IObGoodsReceiptAccountingDocumentDetails
         left join DObGoodsReceiptPurchaseOrder
                   on IObGoodsReceiptAccountingDocumentDetails.goodsreceiptid =
                      DObGoodsReceiptPurchaseOrder.id
         left join dobinventorydocument on DObGoodsReceiptPurchaseOrder.id = dobinventorydocument.id
         left join dobgoodsreceiptaccountingdocument
                   on IObGoodsReceiptAccountingDocumentDetails.refinstanceid =
                      dobGoodsReceiptAccountingDocument.id
         left join iobgoodsreceiptpurchaseorderdata
                   on iobgoodsreceiptpurchaseorderdata.refinstanceid =
                      DObGoodsReceiptPurchaseOrder.id
         left join doborderdocument
                   on iobgoodsreceiptpurchaseorderdata.purchaseorderid = doborderdocument.id AND
                      objecttypecode LIKE '%_PO';


CREATE VIEW IObGoodsReceiptSalesReturnDataGeneralModel AS
SELECT IObGoodsReceiptSalesReturnData.id,
       IObGoodsReceiptSalesReturnData.refInstanceId,
       dobinventorydocument.code          AS goodsReceiptCode,
       dobsalesreturnorder.code           AS salesReturnOrderCode,
       masterdata.name                    as customerName,
       masterdata.code                    as customercode,
       CObEnterprise.code                 AS purchaseUnitCode,
       CObEnterprise.name                 AS purchaseUnitName,
       documentownergeneralmodel.userid   AS salesRepresentativeId,
       documentownergeneralmodel.username AS salesRepresentativeName,
       IObGoodsReceiptSalesReturnData.comments
FROM IObGoodsReceiptSalesReturnData
         LEFT JOIN dobgoodsreceiptsalesreturn
                   ON IObGoodsReceiptSalesReturnData.refInstanceId = dobgoodsreceiptsalesreturn.id
         LEFT join dobinventorydocument on dobgoodsreceiptsalesreturn.id = dobinventorydocument.id
         left join dobsalesreturnorder
                   on iobgoodsreceiptsalesreturndata.salesreturnid = dobsalesreturnorder.id
         LEFT JOIN mobcustomer ON IObGoodsReceiptSalesReturnData.customerid = mobcustomer.id
         LEFT JOIN mobbusinesspartner ON mobcustomer.id = mobbusinesspartner.id
         LEFT JOIN masterdata ON mobbusinesspartner.id = masterdata.id
         LEFT JOIN CObEnterprise
                   on CObEnterprise.objectTypeCode = '5'
                       AND dobinventorydocument.purchaseunitid = CObEnterprise.id
         LEFT JOIN documentownergeneralmodel
                   ON dobsalesreturnorder.documentownerid = documentownergeneralmodel.userid AND
                      documentownergeneralmodel.objectname = 'SalesReturnOrder';

