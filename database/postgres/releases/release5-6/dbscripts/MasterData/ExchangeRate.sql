drop table if exists cobexchangerate;

create table cobexchangerate
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    validFrom        TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentstates    VARCHAR(1024) NOT NULL,
    firstValue       float         NOT NULL,
    secondValue      float         NOT NULL,
    firstCurrencyId  INT8          NOT NULL,
    secondCurrencyId INT8          NOT NULL,
    Type             VARCHAR(1024) not null,
    name             json          not null,
    PRIMARY KEY (id)
);

ALTER TABLE cobexchangerate
    ADD CONSTRAINT FK_first_cobexchangerate_cobcurrency FOREIGN KEY (firstCurrencyId) REFERENCES cobcurrency (id) ON DELETE RESTRICT;
ALTER TABLE cobexchangerate
    ADD CONSTRAINT FK_second_cobexchangerate_cobcurrency FOREIGN KEY (secondCurrencyId) REFERENCES cobcurrency (id) ON DELETE RESTRICT;

CREATE VIEW CObExchangeRateGeneralModel AS
SELECT cobexchangerate.id,
       cobexchangerate.code,
       cobexchangerate.creationDate,
       cobexchangerate.modifiedDate,
       cobexchangerate.creationInfo,
       cobexchangerate.modificationInfo,
       cobexchangerate.currentstates,
       cobexchangerate.validFrom,
       cobexchangerate.firstValue,
       cobexchangerate.secondValue,
       cobexchangerate.firstCurrencyId,
       cobexchangerate.secondCurrencyId,
       cobexchangerate.Type AS exchangetype,
       cobexchangerate.name,
       firstCurrency.iso    AS firstCurrencyIso,
       firstCurrency.code   AS firstCurrencyCode,
       secondCurrency.iso   AS secondCurrencyIso,
       secondCurrency.code  AS secondCurrencyCode
FROM cobexchangerate
         LEFT JOIN CObCurrency firstCurrency
                   ON cobexchangerate.firstCurrencyId = firstCurrency.id
         LEFT JOIN CObCurrency secondCurrency
                   ON cobexchangerate.secondCurrencyId = secondCurrency.id
