DROP TABLE IF EXISTS LObGlobalGLAccountConfig CASCADE;
DROP TABLE IF EXISTS HObGLAccount CASCADE;
DROP TABLE IF EXISTS LeafGLAccount CASCADE;

CREATE TABLE HObGLAccount
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    name             json          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    level            VARCHAR(1024) NOT NULL,
    parentId         INT8,
    PRIMARY KEY (id)
);

CREATE TABLE LeafGLAccount
(
    id          BIGSERIAL NOT NULL,
    type        varchar(50),
    creditDebit varchar(50),
    leadger     VARCHAR(1024),
    PRIMARY KEY (id)
);

DROP VIEW IF EXISTS HObGLAccountGeneralModel;

CREATE VIEW HObGLAccountGeneralModel AS
SELECT *,

       (SELECT type
        FROM LeafGLAccount
        WHERE a1.id = id)       AS type,

       (SELECT creditDebit
        FROM LeafGLAccount
        WHERE a1.id = id)       AS creditDebit,

       (SELECT leadger
        FROM LeafGLAccount
        WHERE a1.id = id)       AS leadger,

       (SELECT code
        FROM HObGLAccount
        WHERE a1.parentId = id) AS parentCode,

       (SELECT name
        FROM HObGLAccount
        WHERE a1.parentId = id) AS parentName,

       (SELECT concat_ws(' - ', code, name :: json ->> 'en')
        FROM HObGLAccount
        WHERE a1.parentId = id) AS parentCodeName,

       (SELECT concat_ws(' ', 'Level', level)
        FROM HObGLAccount
        WHERE a1.id = id)       AS enLevel,
       (SELECT concat_ws(' ', 'مستوي', level)
        FROM HObGLAccount
        WHERE a1.id = id)       AS arLevel
FROM HObGLAccount a1;

ALTER TABLE LeafGLAccount
    ADD CONSTRAINT FKGLAccount FOREIGN KEY (id) REFERENCES HObGLAccount (id);


Create Table LObGlobalGLAccountConfig
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    description      VARCHAR(2048),
    sysFlag          BOOL          NOT NULL,
    accountId        INT8,
    PRIMARY KEY (id)
);

ALTER TABLE LObGlobalGLAccountConfig
ADD CONSTRAINT LObGlobalGLAccountConfig_accountId_FK FOREIGN KEY (accountId) REFERENCES HObGLAccount (id) ON DELETE RESTRICT;

Create View LObGlobalGLAccountConfigGeneralModel As
  Select LObGlobalGLAccountConfig.id,
        LObGlobalGLAccountConfig.code,
        LObGlobalGLAccountConfig.creationDate,
        LObGlobalGLAccountConfig.modifiedDate,
        LObGlobalGLAccountConfig.creationInfo,
        LObGlobalGLAccountConfig.modificationInfo,
        LObGlobalGLAccountConfig.description,
        LObGlobalGLAccountConfig.sysFlag,
        HObGLAccount.id as glAccountId,
        HObGLAccount.code as accountCode,
        HObGLAccount.name as accountName
  from LObGlobalGLAccountConfig
  left join HObGLAccount on HObGLAccount.id=LObGlobalGLAccountConfig.accountId;