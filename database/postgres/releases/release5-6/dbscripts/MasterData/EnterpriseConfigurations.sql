DROP TABLE IF EXISTS CObBank CASCADE;
DROP TABLE IF EXISTS IObEnterpriseAddress CASCADE;
DROP TABLE IF EXISTS IObEnterpriseBasicData CASCADE;
DROP TABLE IF EXISTS IObEnterpriseContact CASCADE;
DROP TABLE IF EXISTS SharedLocalizedLookup CASCADE;
DROP TABLE IF EXISTS RObAttachment CASCADE;
DROP TABLE IF EXISTS CObEnterprise CASCADE;
DROP TABLE IF EXISTS CObPlant CASCADE;
DROP TABLE IF EXISTS IObBankAddressDetails CASCADE;
DROP TABLE IF EXISTS IObCompanyBankDetails CASCADE;
DROP TABLE IF EXISTS IObTaxAccountingInfo CASCADE;
DROP SEQUENCE IF EXISTS IObEnterpriseAddressSeq;
DROP SEQUENCE IF EXISTS IObEnterpriseBasicDataSeq;
DROP SEQUENCE IF EXISTS IObEnterpriseContactSeq;

DROP SEQUENCE IF EXISTS RObAttachmentSeq;
DROP VIEW IF EXISTS CObPlantGeneralModel CASCADE;
DROP VIEW IF EXISTS CObStorehouseGeneralModel CASCADE;
DROP VIEW IF EXISTS IObCompanyBankDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS CObCompanyGeneralModel CASCADE;
DROP VIEW IF EXISTS CObBankGeneralModel CASCADE;
DROP VIEW IF EXISTS CObPurchasingResponsibleGeneralModel CASCADE;
DROP VIEW IF EXISTS CObPurchasingUnitGeneralModel CASCADE;
DROP VIEW IF EXISTS IObCompanyTaxesDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObTaxAccountingInfoGeneralModel CASCADE;

CREATE SEQUENCE IObEnterpriseAddressSeq INCREMENT BY 1 MINVALUE 1 NO MAXVALUE START WITH 1 NO CYCLE;
CREATE SEQUENCE IObEnterpriseBasicDataSeq INCREMENT BY 1 MINVALUE 1 NO MAXVALUE START WITH 1 NO CYCLE;
CREATE SEQUENCE IObEnterpriseContactSeq INCREMENT BY 1 MINVALUE 1 NO MAXVALUE START WITH 1 NO CYCLE;

CREATE SEQUENCE RObAttachmentSeq INCREMENT BY 1 MINVALUE 1 NO MAXVALUE START WITH 1 NO CYCLE;

CREATE TABLE CObBank
(
    id               BIGSERIAL     NOT NULL,
    name             JSON          NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE CObCompany
(
    id                INT8          NOT NULL,
    companyGroupId    INT8              NULL,
    taxAdministrativeId INT8   ,
    regNumber         VARCHAR(1024) ,
    taxCardNumber     VARCHAR(1024) ,
    taxFileNumber     VARCHAR(1024) ,
    PRIMARY KEY (id)
);

CREATE TABLE IObCompanyTaxesDetails
(
    id            BIGSERIAL        NOT NULL,
    refInstanceId INT8             NOT NULL,
    taxId         INT8             NOT NULL,
    taxPercentage DOUBLE PRECISION NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObCompanyPurchasingUnits
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    purchasingUnitId INT8          NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObCompanyBankDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    bankId           INT8          NOT NULL,
    accountNo        VARCHAR(17),
    swiftCode        VARCHAR(11),
    iban             VARCHAR(28),
    beneficiary      VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE IObCompanyLogoDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    logo             bytea         NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObBankAddressDetails
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    addressLine      JSON,
    countryId        INT8,
    cityId           INT8,
    postalCode       VARCHAR(15),
    PRIMARY KEY (id)
);

CREATE TABLE IObEnterpriseAddress
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    objectTypeCode   VARCHAR(1024) NOT NULL,
    addressLine      JSON,
    countryId        INT8,
    cityId           INT8,
    postalCode       VARCHAR(15),
    PRIMARY KEY (id)
);

CREATE TABLE IObEnterpriseBasicData
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    currencyId       INT8,

    PRIMARY KEY (id)
);
CREATE TABLE IObEnterpriseContact
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    objectTypeCode   VARCHAR(1024) NOT NULL,
    contactValue     VARCHAR(1024),
    contactTypeId    INT8,
    PRIMARY KEY (id)
);
CREATE TABLE SharedLocalizedLookup
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    objectTypeCode   INT2          NOT NULL,
    name             JSON          NOT NULL,
    description      VARCHAR(1024),
    isSystemLookup   BOOL          NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE RObAttachment
(
    id               INT8 DEFAULT nextval('RObAttachmentSeq' :: REGCLASS) NOT NULL,
    creationDate     TIMESTAMP                                            NOT NULL,
    modifiedDate     TIMESTAMP                                            NOT NULL,
    creationInfo     TEXT                                                 NOT NULL,
    modificationInfo TEXT                                                 NOT NULL,
    content          BYTEA                                                NOT NULL,
    name             VARCHAR(512)                                         NOT NULL,
    format           VARCHAR(512)                                         NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObEnterpriseContact
    ADD CONSTRAINT FKIObEnterpr666472 FOREIGN KEY (contactTypeId) REFERENCES SharedLocalizedLookup (id) ON DELETE RESTRICT;

CREATE TABLE CObEnterprise
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    objectTypeCode   VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    description      VARCHAR(1024),
    currentStates    VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);
CREATE TABLE CObPlant
(
    id        INT8 NOT NULL,
    companyId INT8,
    PRIMARY KEY (id)
);
CREATE TABLE CObStorehouse
(
    id      INT8 NOT NULL,
    plantId INT8,
    PRIMARY KEY (id)
);

 create TABLE IObTaxAccountingInfo
(
    id               INT8          NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    chartOfAccountId   INT8          NOT NULL,
    primary key (id)
);




CREATE VIEW CObPurchasingUnitGeneralModel AS
SELECT CObEnterprise.id,
       CObEnterprise.code,
       CObEnterprise.currentStates,
       CObEnterprise.name,
       CObEnterprise.description,
       CObEnterprise.creationDate,
       CObEnterprise.modifiedDate,
       CObEnterprise.creationInfo,
       CObEnterprise.modificationInfo,
       (SELECT name.value
        FROM json_each_text(CObEnterprise.name) name
        WHERE name.key = 'en'
       ) AS purchaseUnitName
FROM CObEnterprise
WHERE CObEnterprise.objectTypeCode = '5';

CREATE VIEW CObPurchasingResponsibleGeneralModel AS
SELECT CObEnterprise.id,
       CObEnterprise.code,
       CObEnterprise.currentStates,
       CObEnterprise.name,
       CObEnterprise.description,
       CObEnterprise.creationDate,
       CObEnterprise.modifiedDate,
       CObEnterprise.creationInfo,
       CObEnterprise.modificationInfo
FROM CObEnterprise
WHERE CObEnterprise.objectTypeCode = '6';

CREATE VIEW CObPlantGeneralModel AS
SELECT CObEnterprise.id,
       CObEnterprise.code,
       CObEnterprise.creationDate,
       CObEnterprise.modifiedDate,
       CObEnterprise.creationInfo,
       CObEnterprise.modificationInfo,
       CObEnterprise.currentStates,
       CObEnterprise.name,
       cobPlant.companyid,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE cobenterprise.id = cobPlant.companyid) AS companyCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE cobenterprise.id = cobPlant.companyid) AS companyName
FROM CObEnterprise
         LEFT JOIN
     CObPlant ON CObEnterprise.id = CObPlant.id
WHERE (CObEnterprise.objectTypeCode = '3');


CREATE VIEW CObStorehouseGeneralModel AS
SELECT CObEnterprise.id,
       CObEnterprise.code,
       CObEnterprise.creationDate,
       CObEnterprise.modifiedDate,
       CObEnterprise.creationInfo,
       CObEnterprise.modificationInfo,
       CObEnterprise.currentStates,
       CObEnterprise.name,
       CObStorehouse.plantId,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE cobenterprise.id = CObStorehouse.plantId) AS plantName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE cobenterprise.id = CObStorehouse.plantId) AS plantCode
FROM CObEnterprise
         LEFT JOIN
     CObStorehouse ON CObEnterprise.id = CObStorehouse.id
WHERE (CObEnterprise.objectTypeCode = '4');

CREATE VIEW CObCompanyGeneralModel AS
SELECT cobenterprise.id,
       cobenterprise.code,
       cobenterprise.creationdate,
       cobenterprise.modifieddate,
       cobenterprise.creationinfo,
       cobenterprise.modificationinfo,
       cobenterprise.currentstates,
       cobenterprise.name                                   AS companyname,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE cobenterprise.id = cobcompany.companygroupid) AS companygroupname,
       cobcompany.companygroupid,
       iobenterprisebasicdata.currencyid,
       cobcurrency.iso                                      AS currencyIso,
       iobenterpriseaddress.countryid,
       (SELECT cobgeolocale.name
        FROM cobgeolocale
        WHERE cobgeolocale.id = iobenterpriseaddress.countryid
       )                                                    AS countryname,
       (SELECT cobgeolocale.name
        FROM cobgeolocale
        WHERE cobgeolocale.id = iobenterpriseaddress.cityid
       )                                                    AS cityname,
       iobenterpriseaddress.cityid,
       iobenterpriseaddress.postalcode

FROM cobcompany
         LEFT JOIN cobenterprise ON cobcompany.id = cobenterprise.id
         LEFT JOIN iobenterprisebasicdata
                   ON cobenterprise.id = iobenterprisebasicdata.refinstanceid
         LEFT JOIN iobenterpriseaddress
                   ON cobenterprise.id = iobenterpriseaddress.refinstanceid
         LEFT JOIN cobcurrency ON cobcurrency.id = iobenterprisebasicdata.currencyid
WHERE cobenterprise.objecttypecode :: TEXT = '1' :: TEXT
ORDER BY cobenterprise.id;

CREATE VIEW CObBankGeneralModel AS
SELECT cobbank.id,
       cobbank.code,
       cobbank.creationdate,
       cobbank.modifieddate,
       cobbank.creationinfo,
       cobbank.modificationinfo,
       cobbank.name AS bankname,
       iobbankaddressdetails.countryid,
       (SELECT cobgeolocale.name
        FROM cobgeolocale
        WHERE cobgeolocale.id = iobbankaddressdetails.countryid
       )            AS countryname,
       (SELECT cobgeolocale.name
        FROM cobgeolocale
        WHERE cobgeolocale.id = iobbankaddressdetails.cityid
       )            AS cityname,
       iobbankaddressdetails.cityid,
       iobbankaddressdetails.addressLine

FROM cobbank
         LEFT JOIN iobbankaddressdetails
                   ON cobbank.id = iobbankaddressdetails.refinstanceid

ORDER BY cobbank.id ASC;

CREATE VIEW IObCompanyBankDetailsGeneralModel AS
SELECT iobcompanybankdetails.id,
       cobenterprise.code companyCode,
       cobbank.code,
       cobbank.name,
       iobcompanybankdetails.accountno
FROM iobcompanybankdetails
         JOIN cobcompany
              on iobcompanybankdetails.refinstanceid = cobcompany.id
         JOIN cobbank ON iobcompanybankdetails.bankid = cobbank.id
         JOIN cobenterprise ON cobcompany.id = cobenterprise.id;

CREATE VIEW IObCompanyTaxesDetailsGeneralModel AS
SELECT IObCompanyTaxesDetails.id,
        LObTaxInfo.code AS taxCode,
        CObEnterprise.code AS companycode,
        CObEnterprise.name AS companyname,
        LObTaxInfo.name AS taxname,
        IObCompanyTaxesDetails.taxPercentage AS taxpercentage
FROM cobcompany,cobenterprise,iobcompanytaxesdetails,LObTaxInfo
        WHERE CObCompany.id = CObEnterprise.id
        AND CObEnterprise.objectTypeCode = '1'
        AND LObTaxInfo.id = IObCompanyTaxesDetails.taxId
        AND CObCompany.id = IObCompanyTaxesDetails.refInstanceId;

create view IObTaxAccountingInfoGeneralModel as
SELECT IObTaxAccountingInfo.id,
       IObTaxAccountingInfo.refinstanceid,
       IObTaxAccountingInfo.creationdate,
       IObTaxAccountingInfo.modifieddate,
       IObTaxAccountingInfo.creationinfo,
       IObTaxAccountingInfo.modificationinfo,
       hobglaccount.code                                               AS accountcode,
       hobglaccount.name                                               AS accountname,
       leafglaccount.leadger                                           AS accountledger,
       (SELECT lobtaxinfo.code
        FROM lobtaxinfo
        WHERE (lobtaxinfo.id = IObTaxAccountingInfo.refinstanceid)) AS taxcode,
        (SELECT lobtaxinfo.name
        FROM lobtaxinfo
        WHERE (lobtaxinfo.id = IObTaxAccountingInfo.refinstanceid)) AS taxname
FROM IObTaxAccountingInfo
         join  hobglaccount ON IObTaxAccountingInfo.chartofaccountid = hobglaccount.id
         join leafglaccount on hobglaccount.id = leafglaccount.id;

ALTER TABLE IObCompanyPurchasingUnits
    ADD CONSTRAINT FKIObCompanyPurchasingUnitsUnitId FOREIGN KEY (purchasingUnitId) REFERENCES CObEnterprise (id) ON DELETE CASCADE;

ALTER TABLE IObCompanyPurchasingUnits
    ADD CONSTRAINT FKIObCompanyPurchasingUnits FOREIGN KEY (refInstanceId) REFERENCES CObEnterprise (id) ON DELETE CASCADE;

ALTER TABLE IObCompanyBankDetails
    ADD CONSTRAINT FKIObCompanyBankDetails FOREIGN KEY (bankId) REFERENCES CObBank (id) ON DELETE RESTRICT;
ALTER TABLE IObCompanyBankDetails
    ADD CONSTRAINT FKIObCompanyBankDetailsCompanyFk FOREIGN KEY (refInstanceId) REFERENCES CObCompany (id) ON DELETE CASCADE;

ALTER TABLE IObEnterpriseBasicData
    ADD CONSTRAINT FKIObEnterpriseBasicData FOREIGN KEY (refInstanceId) REFERENCES CObCompany (id) ON DELETE CASCADE;

ALTER TABLE IObEnterpriseBasicData
    ADD CONSTRAINT FKIObEnterpriseBasicData_COpCurrency FOREIGN KEY (currencyId) REFERENCES CobCurrency (id) ON DELETE CASCADE;
ALTER TABLE IObEnterpriseContact
    ADD CONSTRAINT FKIObEnterpr483119 FOREIGN KEY (refInstanceId) REFERENCES CObEnterprise (id) ON DELETE CASCADE;
ALTER TABLE IObEnterpriseAddress
    ADD CONSTRAINT FKIObEnterpr267896 FOREIGN KEY (refInstanceId) REFERENCES CObEnterprise (id) ON DELETE CASCADE;
ALTER TABLE IObBankAddressDetails
    ADD CONSTRAINT FKIObBankAddressDetails FOREIGN KEY (refInstanceId) REFERENCES CObBank (id) ON DELETE CASCADE;

ALTER TABLE CObPlant
    ADD CONSTRAINT FKCObPlant FOREIGN KEY (id) REFERENCES CObEnterprise (id) ON DELETE CASCADE;
ALTER TABLE CObPlant
    ADD CONSTRAINT FKCObPlantCompanyId FOREIGN KEY (companyId) REFERENCES CObEnterprise (id) ON DELETE CASCADE;
ALTER TABLE CObStorehouse
    ADD CONSTRAINT FKCObCObStorehouse FOREIGN KEY (id) REFERENCES CObEnterprise (id) ON DELETE CASCADE;
ALTER TABLE CObStorehouse
    ADD CONSTRAINT FKCObCObStorehousePlantId FOREIGN KEY (plantId) REFERENCES CObEnterprise (id) ON DELETE CASCADE;
ALTER TABLE CObCompany
    ADD CONSTRAINT FKCObCompanyID FOREIGN KEY (id) REFERENCES CObEnterprise (id) ON DELETE CASCADE;
ALTER TABLE CObCompany
    ADD CONSTRAINT FKCObCompanyGroupID FOREIGN KEY (companyGroupId) REFERENCES CObEnterprise (id) ON DELETE CASCADE;

ALTER TABLE CObCompany
    ADD CONSTRAINT FK_CObCompany_LObTaxAdministrative FOREIGN KEY (taxAdministrativeId) REFERENCES lobtaxadministrative (id) ON DELETE restrict;

ALTER TABLE IObCompanyTaxesDetails
    ADD CONSTRAINT FK_IObCompanyTaxesDetails_CObCompany FOREIGN KEY (refInstanceId) REFERENCES CObCompany (id) ON DELETE CASCADE;

ALTER TABLE IObCompanyTaxesDetails
    ADD CONSTRAINT FK_IObCompanyTaxesDetails_LObTaxInfo FOREIGN KEY (taxId) REFERENCES LObTaxInfo (id) ON DELETE restrict;

ALTER TABLE IObTaxAccountingInfo
    ADD CONSTRAINT FK_IObTaxAccountingInfo_LObTaxInfo FOREIGN KEY (refInstanceId) REFERENCES lobtaxinfo (id) ON DELETE RESTRICT;

ALTER TABLE IObTaxAccountingInfo
    ADD CONSTRAINT FK_IObTaxAccountingInfo_HObGlAccount FOREIGN KEY (chartOfAccountId) REFERENCES hobglaccount (id) ON DELETE RESTRICT;
