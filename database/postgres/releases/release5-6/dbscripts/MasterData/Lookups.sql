drop table if exists LObTaxAdministrative;
drop table if exists LObAddress;
drop table if exists LObContactPerson;
drop table if exists LObIssueFrom;
drop table if exists LObRiskLevel;
drop table if exists LObLegalType;
drop table if exists LObTaxInfo;

CREATE TABLE LObIssueFrom
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE LObTaxAdministrative
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024),
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE LObAddress
(

    id               BIGSERIAL     NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    addressName      JSON,
    address          JSON,
    district         VARCHAR(1024),
    postalCode       VARCHAR(1024),
    countryId        INT8,
    cityId           INT8,
    governmentId     INT8,
    objectTypeCode   VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);



CREATE TABLE LObContactPerson
(
    id               BIGSERIAL     NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    title            VARCHAR(1024),
    telephone        VARCHAR(1024),
    email            VARCHAR(1024),
    department       VARCHAR(1024),
    PRIMARY KEY (id)
);


CREATE TABLE LObRiskLevel
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE LObLegalType
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE LObTaxInfo
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    name             JSON                     NOT NULL,
    PRIMARY KEY (id)
);

