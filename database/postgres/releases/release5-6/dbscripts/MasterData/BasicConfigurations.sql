DROP VIEW IF EXISTS CObUoMGeneralModel;
DROP VIEW IF EXISTS CObSubdivisionGeneralModel;
DROP VIEW IF EXISTS CObCountryGeneralModel;

DROP TABLE IF EXISTS CountryDefinition CASCADE;
DROP TABLE IF EXISTS SubdivisionDefinition CASCADE;
DROP TABLE IF EXISTS LObGeoLocale CASCADE;
DROP TABLE IF EXISTS CObGeoLocale CASCADE;

DROP TABLE IF EXISTS IObUoMConversion CASCADE;
DROP TABLE IF EXISTS LocalizedLObMeasure CASCADE;
DROP TABLE IF EXISTS CObMeasure CASCADE;
DROP TABLE IF EXISTS UTCOffset CASCADE;
DROP TABLE IF EXISTS LocalizedLObGeoLocale CASCADE;
DROP SEQUENCE IF EXISTS LObGeoLocaleSeq;
DROP SEQUENCE IF EXISTS CObGeoLocaleSeq;
DROP SEQUENCE IF EXISTS IObUoMConversionSeq;
DROP SEQUENCE IF EXISTS IObSIStandardUoMSeq;
DROP SEQUENCE IF EXISTS LocalizedLObMeasureSeq;
DROP SEQUENCE IF EXISTS CObMeasureSeq;
DROP SEQUENCE IF EXISTS LocalizedLObGeoLocaleSeq;

CREATE SEQUENCE LObGeoLocaleSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1;
CREATE SEQUENCE CObGeoLocaleSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1;

CREATE SEQUENCE IObUoMConversionSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1;
CREATE SEQUENCE IObSIStandardUoMSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1;
CREATE SEQUENCE LocalizedLObMeasureSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1;
CREATE SEQUENCE CObMeasureSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1;
CREATE SEQUENCE LocalizedLObGeoLocaleSeq
  INCREMENT BY 1
  MINVALUE 1
  NO MAXVALUE
  START WITH 1;

CREATE TABLE CObCurrency (
  id               BIGSERIAL     NOT NULL,
  code             VARCHAR(1024),
  creationDate     TIMESTAMP     NOT NULL,
  modifiedDate     TIMESTAMP     NOT NULL,
  creationInfo     VARCHAR(1024) NOT NULL,
  modificationInfo VARCHAR(1024) NOT NULL,
  name             JSON          NOT NULL,
  currentStates    VARCHAR(1024) NOT NULL,
  iso              VARCHAR(50)    NOT NULL,
  subUnit          JSON,
  decimalPlaces    INT8,
  PRIMARY KEY (id)
);

CREATE TABLE CountryDefinition (
  id                 INT8          NOT NULL,
  isoCode            VARCHAR(1024) NOT NULL,
  dateFormatId       INT8,
  decimalFormatId    INT8,
  languageId         INT8,
  standardTimezoneId INT8,
  dstTimezoneId      INT8,
  addressFormatId    INT8,
  PRIMARY KEY (id)
);
CREATE TABLE SubdivisionDefinition (
  id                 INT8 NOT NULL,
  categoryId         INT8,
  countryId          INT8 NOT NULL,
  level              INT2,
  parentId           INT8,
  languageId         INT8,
  standardTimezoneId INT8,
  dstTimezoneId      INT8,
  PRIMARY KEY (id)
);
CREATE TABLE LObGeoLocale (
  id               INT8 DEFAULT nextval('LObGeoLocaleSeq' :: REGCLASS) NOT NULL,
  code             VARCHAR(1024),
  creationDate     TIMESTAMP                                           NOT NULL,
  modifiedDate     TIMESTAMP                                           NOT NULL,
  creationInfo     VARCHAR(1024)                                       NOT NULL,
  modificationInfo VARCHAR(1024)                                       NOT NULL,
  objectTypeCode   VARCHAR(1024)                                       NOT NULL,
  name             VARCHAR(1024)                                       NOT NULL,
  description      VARCHAR(2048),
  sysFlag          BOOL                                                NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE CObGeoLocale (
  id               INT8 DEFAULT nextval('CObGeoLocaleSeq' :: REGCLASS) NOT NULL,
  code             VARCHAR(1024),
  creationDate     TIMESTAMP                                           NOT NULL,
  modifiedDate     TIMESTAMP                                           NOT NULL,
  creationInfo     VARCHAR(1024)                                       NOT NULL,
  modificationInfo VARCHAR(1024)                                       NOT NULL,
  objectTypeCode   VARCHAR(1024)                                       NOT NULL,
  currentStates    VARCHAR(1024)                                       NOT NULL,
  name             JSON                                                NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE IObUoMConversion (
  id                 INT8 DEFAULT nextval('IObUoMConversionSeq' :: REGCLASS) NOT NULL,
  refInstanceId      INT8                                                    NOT NULL,
  creationDate       TIMESTAMP                                               NOT NULL,
  modifiedDate       TIMESTAMP                                               NOT NULL,
  creationInfo       VARCHAR(1024)                                           NOT NULL,
  modificationInfo   VARCHAR(1024)                                           NOT NULL,
  numerator          FLOAT4                                                  NOT NULL,
  denominator        double precision                                        NOT NULL,
  roundDecimalPlaces INT2 DEFAULT 2                                          NOT NULL,
  description        VARCHAR(2048),
  PRIMARY KEY (id)
);
CREATE TABLE LocalizedLObMeasure (
  id               INT8 DEFAULT nextval('LocalizedLObMeasureSeq' :: REGCLASS) NOT NULL,
  code             VARCHAR(1024),
  creationDate     TIMESTAMP                                                  NOT NULL,
  modifiedDate     TIMESTAMP                                                  NOT NULL,
  creationInfo     VARCHAR(1024)                                              NOT NULL,
  modificationInfo VARCHAR(1024)                                              NOT NULL,
  objectTypeCode   VARCHAR(1024)                                              NOT NULL,
  name             JSON                                                       NOT NULL,
  description      VARCHAR(2048),
  sysFlag          BOOL                                                       NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE CObMeasure (
  id               INT8 DEFAULT nextval('CObMeasureSeq' :: REGCLASS) NOT NULL,
  code             VARCHAR(1024),
  creationDate     TIMESTAMP                                         NOT NULL,
  modifiedDate     TIMESTAMP                                         NOT NULL,
  creationInfo     VARCHAR(1024)                                     NOT NULL,
  modificationInfo VARCHAR(1024)                                     NOT NULL,
  isStandard   BOOL                                                  NOT NULL,
  currentStates    VARCHAR(1024)                                     NOT NULL,
  name             JSON                                              NOT NULL,
  symbol           JSON,
  internationalSystem BOOL,
  dimensionId         INT8,
  isoCode             VARCHAR(255),
  PRIMARY KEY (id)
);

CREATE TABLE UTCOffset (
  id        INT8         NOT NULL,
  utcOffset VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);
CREATE TABLE LocalizedLObGeoLocale (
  id               INT8 DEFAULT nextval('LocalizedLObGeoLocaleSeq' :: REGCLASS) NOT NULL,
  code             VARCHAR(1024),
  creationDate     TIMESTAMP                                                    NOT NULL,
  modifiedDate     TIMESTAMP                                                    NOT NULL,
  creationInfo     VARCHAR(1024)                                                NOT NULL,
  modificationInfo VARCHAR(1024)                                                NOT NULL,
  objectTypeCode   VARCHAR(1024)                                                NOT NULL,
  name             JSON                                                         NOT NULL,
  description      VARCHAR(2048),
  sysFlag          BOOL                                                         NOT NULL,
  PRIMARY KEY (id)
);
CREATE OR REPLACE VIEW CObCountryGeneralModel AS
  SELECT
    CObGeoLocale.id,
    CObGeoLocale.code,
    CObGeoLocale.creationDate,
    CObGeoLocale.modifiedDate,
    CObGeoLocale.creationinfo,
    CObGeoLocale.modificationinfo,
    CObGeoLocale.currentStates,
    CObGeoLocale.name,
    CountryDefinition.isoCode,

    CASE WHEN CountryDefinition.languageId IS NOT NULL
      THEN (SELECT LocalizedLObGeoLocale.name
            FROM LocalizedLObGeoLocale
            WHERE LocalizedLObGeoLocale.id = CountryDefinition.languageId)
    ELSE NULL
    END
      AS language,

    CASE WHEN CountryDefinition.standardTimezoneId IS NOT NULL
      THEN (SELECT LObGeoLocale.name
            FROM LObGeoLocale
            WHERE LObGeoLocale.id = CountryDefinition.standardTimezoneId)
    ELSE NULL
    END
      AS standardTimezone,

    CASE WHEN CountryDefinition.dstTimezoneId IS NOT NULL
      THEN (SELECT LObGeoLocale.name
            FROM LObGeoLocale
            WHERE LObGeoLocale.id = CountryDefinition.dstTimezoneId)
    ELSE NULL
    END
      AS dstTimezone,

    CASE WHEN CountryDefinition.dateFormatId IS NOT NULL
      THEN (SELECT LObGeoLocale.name
            FROM LObGeoLocale
            WHERE LObGeoLocale.id = CountryDefinition.dateFormatId)
    ELSE NULL
    END
      AS decimalFormat,

    CASE WHEN CountryDefinition.decimalFormatId IS NOT NULL
      THEN (SELECT LObGeoLocale.name
            FROM LObGeoLocale
            WHERE LObGeoLocale.id = CountryDefinition.decimalFormatId)
    ELSE NULL
    END
      AS dateFormat,

    CASE WHEN CountryDefinition.addressFormatId IS NOT NULL
      THEN (SELECT LObGeoLocale.name
            FROM LObGeoLocale
            WHERE LObGeoLocale.id = CountryDefinition.addressFormatId)
    ELSE NULL
    END
      AS addressFormat

  FROM
    CObGeoLocale
    RIGHT JOIN
    CountryDefinition ON CObGeoLocale.id = CountryDefinition.id

  ORDER BY
    CObGeoLocale.id ASC;

CREATE OR REPLACE VIEW CObSubdivisionGeneralModel AS
  SELECT
    CObGeoLocale.id,
    CObGeoLocale.code,
    CObGeoLocale.creationDate,
    CObGeoLocale.modifiedDate,
    CObGeoLocale.creationinfo,
    CObGeoLocale.modificationinfo,
    CObGeoLocale.currentStates,
    CObGeoLocale.name,
    SubdivisionDefinition.level,
    SubdivisionDefinition.countryId,

    CASE WHEN SubdivisionDefinition.categoryId IS NOT NULL
      THEN (SELECT LocalizedLObGeoLocale.name
            FROM LocalizedLObGeoLocale
            WHERE LocalizedLObGeoLocale.id = SubdivisionDefinition.categoryId)
    ELSE NULL
    END
      AS category,

    CASE WHEN SubdivisionDefinition.countryId IS NOT NULL
      THEN (SELECT CObGeoLocale.name
            FROM CObGeoLocale
            WHERE CObGeoLocale.id = SubdivisionDefinition.countryId)
    ELSE NULL
    END
      AS country,

    CASE WHEN SubdivisionDefinition.languageId IS NOT NULL
      THEN (SELECT LocalizedLObGeoLocale.name
            FROM LocalizedLObGeoLocale
            WHERE LocalizedLObGeoLocale.id = SubdivisionDefinition.languageId)
    ELSE NULL
    END
      AS language,

    CASE WHEN SubdivisionDefinition.standardTimezoneId IS NOT NULL
      THEN (SELECT LObGeoLocale.name
            FROM LObGeoLocale
            WHERE LObGeoLocale.id = SubdivisionDefinition.standardTimezoneId)
    ELSE NULL
    END
      AS standardTimezone,

    CASE WHEN SubdivisionDefinition.dstTimezoneId IS NOT NULL
      THEN (SELECT LObGeoLocale.name
            FROM LObGeoLocale
            WHERE LObGeoLocale.id = SubdivisionDefinition.dstTimezoneId)
    ELSE NULL
    END
      AS dstTimezone,

    CASE WHEN SubdivisionDefinition.parentId IS NOT NULL
      THEN (SELECT CObGeoLocale.name
            FROM CObGeoLocale
            WHERE CObGeoLocale.id = SubdivisionDefinition.parentId)
    ELSE NULL
    END
      AS parent

  FROM
    CObGeoLocale
    RIGHT JOIN
    SubdivisionDefinition ON CObGeoLocale.id = SubdivisionDefinition.id

  ORDER BY
    CObGeoLocale.id ASC;

CREATE OR REPLACE VIEW CObUoMGeneralModel AS
  SELECT
    CObMeasure.id,
    CObMeasure.code,
    CObMeasure.creationDate,
    CObMeasure.modifiedDate,
    CObMeasure.creationinfo,
    CObMeasure.modificationinfo,
    CObMeasure.isStandard,
    CObMeasure.currentStates,
    CObMeasure.name,
    CObMeasure.symbol,
    CObMeasure.isoCode,
    CObMeasure.internationalSystem,
    CASE WHEN CObMeasure.dimensionId IS NOT NULL
      THEN (SELECT LocalizedLObMeasure.name
            FROM LocalizedLObMeasure
            WHERE LocalizedLObMeasure.id = CObMeasure.dimensionId)
    ELSE NULL
    END
      AS dimension
  FROM
    CObMeasure;

ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio121375 FOREIGN KEY (parentId) REFERENCES CObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef718578 FOREIGN KEY (languageId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio689670 FOREIGN KEY (id) REFERENCES CObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio7217 FOREIGN KEY (categoryId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef455050 FOREIGN KEY (id) REFERENCES CObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef948969 FOREIGN KEY (standardTimezoneId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef928680 FOREIGN KEY (dstTimezoneId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef180238 FOREIGN KEY (addressFormatId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef662306 FOREIGN KEY (dateFormatId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef578069 FOREIGN KEY (decimalFormatId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio953198 FOREIGN KEY (languageId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio788000 FOREIGN KEY (standardTimezoneId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio808289 FOREIGN KEY (dstTimezoneId) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio957316 FOREIGN KEY (countryId) REFERENCES CountryDefinition (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CObMeasure
  ADD CONSTRAINT FKCObMeasure126333 FOREIGN KEY (dimensionId) REFERENCES LocalizedLObMeasure (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE IObUoMConversion
  ADD CONSTRAINT FKIObUoMConv912193 FOREIGN KEY (refInstanceId) REFERENCES CObMeasure (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE UTCOffset
  ADD CONSTRAINT FKUTCOffset302839 FOREIGN KEY (id) REFERENCES LObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio258914 FOREIGN KEY (languageId) REFERENCES LocalizedLObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE SubdivisionDefinition
  ADD CONSTRAINT FKSubdivisio701501 FOREIGN KEY (categoryId) REFERENCES LocalizedLObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE CountryDefinition
  ADD CONSTRAINT FKCountryDef24294 FOREIGN KEY (languageId) REFERENCES LocalizedLObGeoLocale (id) ON UPDATE CASCADE ON DELETE CASCADE;
