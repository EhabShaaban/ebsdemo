-- ----------------------------------- MD Settings -------------------------------

drop view IF EXISTS IObMaterialRestrictionGeneralModel;
drop view IF EXISTS IObMaterialAllowedViewGeneralModel;
drop view IF EXISTS CObMaterialGroupGeneralModel;
drop view IF EXISTS CObMaterialTypeGeneralModel;
drop view IF EXISTS CObMaterialStatusGeneralModel;
drop table IF EXISTS CObMaterial CASCADE;
drop table IF EXISTS CObMaterialStatus CASCADE;
drop table IF EXISTS CObMaterialType CASCADE;
drop table IF EXISTS CObMaterialGroup CASCADE;
drop table IF EXISTS IObMaterialAllowedView CASCADE;
drop table IF EXISTS IObMaterialRestriction CASCADE;
drop table IF EXISTS LocalizedLObMaterial CASCADE;
drop table IF EXISTS LObAttachmentType CASCADE;

drop table IF EXISTS PortsCountry CASCADE;
drop sequence IF EXISTS CObMaterialSequence;
drop sequence IF EXISTS IObMaterialAllowedViewSequence;
drop sequence IF EXISTS IObMaterialRestrictionSequence;
drop sequence IF EXISTS IObQualityUpdatingSequence;
drop sequence IF EXISTS LObAttachmentTypeSequence;

drop table IF EXISTS itemvendorrecord CASCADE;

drop view IF EXISTS LObAttachmentTypeGeneralModel CASCADE;
drop view IF EXISTS ItemVendorRecordGeneralModel CASCADE;
drop view IF EXISTS iobitemaccountinginfogeneralmodel CASCADE;
drop view IF EXISTS iobvendoraccountinginfogeneralmodel CASCADE;


drop sequence IF EXISTS iobpurchasingrulesdataitemvendorrecordsequence;

create sequence CObMaterialSequence increment by 1 minvalue 1 NO MAXVALUE START with 1 NO CYCLE;
create sequence IObMaterialAllowedViewSequence increment by 1 minvalue 1 NO MAXVALUE START with 1 NO CYCLE;
create sequence IObMaterialRestrictionSequence increment by 1 minvalue 1 NO MAXVALUE START with 1 NO CYCLE;
create sequence IObQualityUpdatingSequence increment by 1 minvalue 1 NO MAXVALUE START with 1 NO CYCLE;
create sequence LocalizedLObMaterialSequence increment by 1 minvalue 1 NO MAXVALUE START with 1 NO CYCLE;
create sequence LObAttachmentTypeSequence increment by 1 minvalue 1 NO MAXVALUE START with 1 NO CYCLE;

create TABLE CObPaymentTerms
(
    id                    BIGSERIAL     NOT NULL,
    code                  varchar(1024),
    creationDate          timestamp with time zone NOT NULL,
    modifiedDate          timestamp with time zone     NOT NULL,
    creationInfo          varchar(1024) NOT NULL,
    modificationInfo      varchar(1024) NOT NULL,
    currentStates         varchar(1024) NOT NULL,
    name                  json          NOT NULL,
    appliedToVendor       bool,
    appliedToCustomer     bool,
    requiredDownPayment   bool,
    downPaymentPercentage int8,
    downPaymentMethod     int8,
    downPaymentDue        int8,
    description           varchar(1024),
    PRIMARY KEY (id)
);

create TABLE IObPaymentTermsInstallments
(
    id                       BIGSERIAL     NOT NULL,
    refInstanceId            int8          NOT NULL,
    creationDate             timestamp with time zone     NOT NULL,
    modifiedDate             timestamp with time zone     NOT NULL,
    creationInfo             varchar(1024) NOT NULL,
    modificationInfo         varchar(1024) NOT NULL,
    paymentMethod            int8,
    paymentPercentage        int8,
    creditPeriod             int8,
    defaultBaselineDate      int8,
    baselineFixedDate        int8,
    baselineAdditionalMonths int8,
    PRIMARY KEY (id)
);

create TABLE CObMaterial
(
    id               BIGSERIAL     NOT NULL,
    code             varchar(1024),
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     varchar(1024) NOT NULL,
    modificationInfo varchar(1024) NOT NULL,
    objectTypeCode   varchar(1024) NOT NULL,
    currentStates    varchar(1024) NOT NULL,
    name             json          NOT NULL,
    PRIMARY KEY (id)
);
create TABLE CObMaterialStatus
(
    id          int8 NOT NULL,
    description varchar(1024),
    PRIMARY KEY (id)
);
create TABLE CObMaterialType
(
    id                  int8 NOT NULL,
    internalProcurement varchar(256),
    externalProcurement varchar(256),
    valuationMethod     varchar(256),
    accountCategory     int8,
    quantityUpdating    varchar(256),
    valueUpdating       varchar(256),
    PRIMARY KEY (id)
);

create TABLE CObMaterialGroup
(
    id               BIGSERIAL     NOT NULL,
    level            varchar(256)  NOT NULL,
    parentid         int8,
    description      varchar(1024),
    code             varchar(1024) NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     varchar(1024) NOT NULL,
    modificationInfo varchar(1024) NOT NULL,
    name             json          NOT NULL,
    PRIMARY KEY (id)
);
create TABLE IObMaterialAllowedView
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    int8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     varchar(1024) NOT NULL,
    modificationInfo varchar(1024) NOT NULL,
    materialView     int8          NOT NULL,
    PRIMARY KEY (id)
);
create TABLE IObMaterialRestriction
(
    id                BIGSERIAL     NOT NULL,
    refInstanceId     int8          NOT NULL,
    creationDate      timestamp with time zone     NOT NULL,
    modifiedDate      timestamp with time zone     NOT NULL,
    creationInfo      varchar(1024) NOT NULL,
    modificationInfo  varchar(1024) NOT NULL,
    businessOperation int8          NOT NULL,
    restriction       varchar(256),
    PRIMARY KEY (id)
);

create TABLE LocalizedLObMaterial
(
    id               BIGSERIAL     NOT NULL,
    code             varchar(1024),
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     varchar(1024) NOT NULL,
    modificationInfo varchar(1024) NOT NULL,
    objectTypeCode   varchar(1024) NOT NULL,
    name             json          NOT NULL,
    description      varchar(2048),
    sysFlag          bool          NOT NULL,
    PRIMARY KEY (id)
);

create TABLE LObAttachmentType
(
    id               BIGSERIAL     NOT NULL,
    code             varchar(1024),
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     varchar(1024) NOT NULL,
    modificationInfo varchar(1024) NOT NULL,
    name             json          NOT NULL,
    description      varchar(2048),
    sysFlag          bool          NOT NULL,
    PRIMARY KEY (id)
);

create TABLE PortsCountry
(
    id          int8 NOT NULL,
    countryid   int8 NOT NULL,
    countryName json NOT NULL,
    PRIMARY KEY (id)
);
create view CObMaterialStatusGeneralModel as
select CObMaterial.id,
       CObMaterial.code,
       CObMaterial.creationDate,
       CObMaterial.modifiedDate,
       CObMaterial.creationInfo,
       CObMaterial.modificationInfo,
       CObMaterial.currentStates,
       CObMaterial.name,
       CObMaterialStatus.description
       from CObMaterialStatus
         left join
     CObMaterial on CObMaterialStatus.id = CObMaterial.id;

create view CObMaterialTypeGeneralModel as
select CObMaterial.id,
       CObMaterial.code,
       CObMaterial.creationDate,
       CObMaterial.modifiedDate,
       CObMaterial.creationInfo,
       CObMaterial.modificationInfo,
       CObMaterial.currentStates,
       CObMaterial.name,
       CObMaterialType.internalProcurement,
       CObMaterialType.externalProcurement,
       CObMaterialType.valuationMethod,
       CObMaterialType.accountCategory,
       CObMaterialType.quantityUpdating,
       CObMaterialType.valueUpdating
from CObMaterialType
         left join
     CObMaterial on CObMaterialType.id = CObMaterial.id;

create view CObMaterialGroupGeneralModel as
select id,
       code,
       creationDate,
       modifiedDate,
       creationInfo,
       modificationInfo,
       name,
       level,
       (select A.code
        from CObMaterialGroup A
        where CObMaterialGroup.parentid = A.id) as parentCode,
       (select A.name
        from CObMaterialGroup A
        where CObMaterialGroup.parentid = A.id) as parentName
from CObMaterialGroup;

create view IObMaterialAllowedViewGeneralModel as
select IObMaterialAllowedView.id,
       IObMaterialAllowedView.refInstanceId,
       IObMaterialAllowedView.creationDate,
       IObMaterialAllowedView.modifiedDate,
       IObMaterialAllowedView.creationInfo,
       IObMaterialAllowedView.modificationInfo,
       IObMaterialAllowedView.materialView
from IObMaterialAllowedView;

create view IObMaterialRestrictionGeneralModel as
select IObMaterialRestriction.id,
       IObMaterialRestriction.refInstanceId,
       IObMaterialRestriction.creationDate,
       IObMaterialRestriction.modifiedDate,
       IObMaterialRestriction.creationInfo,
       IObMaterialRestriction.modificationInfo,
       IObMaterialRestriction.businessOperation,
       IObMaterialRestriction.restriction
from IObMaterialRestriction;

create view CObPaymentTermsGeneralModel as
select cobpaymentterms.id,
       cobpaymentterms.code,
       cobpaymentterms.creationdate,
       cobpaymentterms.modifieddate,
       cobpaymentterms.creationinfo,
       cobpaymentterms.modificationinfo,
       cobpaymentterms.currentstates,
       cobpaymentterms.name,
       cobpaymentterms.appliedtovendor,
       cobpaymentterms.appliedtocustomer,
       cobpaymentterms.requireddownpayment,
       cobpaymentterms.downpaymentpercentage,
       cobpaymentterms.downpaymentmethod,
       cobpaymentterms.downpaymentdue,
       (select localizedlobmaterial.name
        from localizedlobmaterial
        where cobpaymentterms.downpaymentmethod = localizedlobmaterial.id) as downpaymentmethodname,
       (select localizedlobmaterial.name
        from localizedlobmaterial
        where cobpaymentterms.downpaymentdue = localizedlobmaterial.id)    as downpaymentduename
from cobpaymentterms;


alter table CObPaymentTerms
    add CONSTRAINT FKdownPaymentMethod FOREIGN KEY (downPaymentMethod) REFERENCES LocalizedLObMaterial (id);
alter table CObPaymentTerms
    add CONSTRAINT FKdownPaymentDue FOREIGN KEY (downPaymentDue) REFERENCES LocalizedLObMaterial (id);

alter table IObPaymentTermsInstallments
    add CONSTRAINT FKpaymentMethod FOREIGN KEY (paymentMethod) REFERENCES LocalizedLObMaterial (id);
alter table IObPaymentTermsInstallments
    add CONSTRAINT FKdefaultBaselineDate FOREIGN KEY (defaultBaselineDate) REFERENCES LocalizedLObMaterial (id);

alter table CObMaterialStatus
    add CONSTRAINT FKCObMateria228834 FOREIGN KEY (id) REFERENCES CObMaterial (id) ON delete Cascade;
alter table CObMaterialType
    add CONSTRAINT FKCObMateria779136 FOREIGN KEY (id) REFERENCES CObMaterial (id) ON delete Cascade;
alter table IObMaterialRestriction
    add CONSTRAINT FKIObMateria974291 FOREIGN KEY (refInstanceId) REFERENCES CObMaterialStatus (id) ON delete Cascade;
alter table IObMaterialAllowedView
    add CONSTRAINT FKIObMateria66219 FOREIGN KEY (refInstanceId) REFERENCES CObMaterialType (id) ON delete Cascade;
alter table IObMaterialAllowedView
    add CONSTRAINT FKIObMateria675453 FOREIGN KEY (materialView) REFERENCES LocalizedLObMaterial (id) ON delete Restrict;
alter table IObMaterialRestriction
    add CONSTRAINT FKIObMateria272429 FOREIGN KEY (businessOperation) REFERENCES LocalizedLObMaterial (id) ON delete Restrict;
alter table PortsCountry
    add CONSTRAINT FKPortsCountry167272 FOREIGN KEY (id) REFERENCES LocalizedLObMaterial (id) ON delete Cascade;
alter table PortsCountry
    add CONSTRAINT FKPortsCountry654543 FOREIGN KEY (countryid) REFERENCES CountryDefinition (id) ON delete Cascade;
alter table CObMaterialGroup
    add CONSTRAINT FKHObMateria473527 FOREIGN KEY (parentid) REFERENCES CObMaterialGroup (id) ON delete CASCADE;

-- ------------------------------------------------- ITEM ------------------------------------
drop view IF EXISTS IObAlternativeUoMGeneralModel;
drop view IF EXISTS MObItemGeneralModel;
drop view IF EXISTS MObItemAuthorizationGeneralModel;
drop table IF EXISTS IObAlternativeUoM CASCADE;
drop table IF EXISTS IObItemPurchaseUnit;
drop table IF EXISTS IObVendorPurchaseUnit;
drop table IF EXISTS MObItem CASCADE;
drop table IF EXISTS MObVendor CASCADE;
drop table IF EXISTS MasterData CASCADE;

drop sequence IF EXISTS IObItemPurchaseUnitSeq;
drop sequence IF EXISTS IObVendorPurchaseUnitSeq;
drop sequence IF EXISTS IObAlternativeUoMSeq;

create sequence IObItemPurchaseUnitSeq
    increment by 1
    minvalue 1
    NO MAXVALUE
    START with 1;

create sequence IObVendorPurchaseUnitSeq
    increment by 1
    minvalue 1
    NO MAXVALUE
    START with 1;

create sequence IObAlternativeUoMSeq
    increment by 1
    minvalue 1
    NO MAXVALUE
    START with 1;

create TABLE MasterData
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    objectTypeCode   VARCHAR(1024) NOT NULL,
    currentStates    VARCHAR(1024) NOT NULL,
    name             JSON          NOT NULL,
    PRIMARY KEY (id)
);

create TABLE MObItem
(
    id                      INT8                 NOT NULL,
    oldItemNumber           VARCHAR(1024)        NOT NULL,
    basicUnitOfMeasure      INT8                 NOT NULL,
    itemGroup               INT8                 NOT NULL,
    batchManagementRequired BOOL DEFAULT 'false' NOT NULL,
    PRIMARY KEY (id)
);

create TABLE MObBusinessPartner
(
  id int8 NOT NULL,
  primary KEY (id)
);


create TABLE MObVendor
(
    id                    INT8          NOT NULL,
    vendorType            INT8 DEFAULT 0,
    purchaseresponsibleid INT8          NOT NULL,
    oldvendornumber       VARCHAR(1024) NOT NULL,
    accountwithvendor     VARCHAR(1024),
    PRIMARY KEY (id)
);

create TABLE IObItemPurchaseUnit
(
    id               INT8 DEFAULT nextval('IObItemPurchaseUnitSeq' :: REGCLASS),
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    purchaseunitid   INT8          NOT NULL,
    primary key (id)
);

create TABLE IObItemAccountingInfo
(
    id               INT8 DEFAULT nextval('IObItemPurchaseUnitSeq' :: REGCLASS),
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    chartOfAccountId   INT8          NOT NULL,
    primary key (id)
);

create TABLE IObVendorAccountingInfo
(
    id               INT8 DEFAULT nextval('IObVendorPurchaseUnitSeq' :: REGCLASS),
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    chartOfAccountId   INT8          NOT NULL,
    primary key (id)
);


create TABLE IObVendorPurchaseUnit
(
    id               INT8 DEFAULT nextval('IObVendorPurchaseUnitSeq' :: REGCLASS),
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    purchaseunitid   INT8          NOT NULL,
    purchaseUnitName VARCHAR(1024) NOT NULL,
    primary key (id)
);

create TABLE IObAlternativeUoM
(
    id                         INT8 DEFAULT nextval('IObAlternativeUoMSeq' :: REGCLASS) NOT NULL,
    refInstanceId              INT8                                                     NOT NULL,
    creationDate               timestamp with time zone                                                NOT NULL,
    modifiedDate               timestamp with time zone                                                NOT NULL,
    creationInfo               VARCHAR(1024)                                            NOT NULL,
    modificationInfo           VARCHAR(1024)                                            NOT NULL,
    oldItemNumber              VARCHAR(1024)                                            NOT NULL,
    alternativeUnitOfMeasureId INT8                                                     NOT NULL,
    conversionFactor           FLOAT4                                                   NOT NULL,
    PRIMARY KEY (id)
);

create view MObItemGeneralModel as
select MasterData.id,
       MasterData.code,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.currentstates,
       MasterData.name,
       MObItem.oldItemNumber,
       MObItem.itemgroup,
       MObItem.basicunitofmeasure,
       (select cobmeasure.code
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasurecode,
       (select cobmeasure.symbol
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasuresymbol,
       (select cobmeasure.name
        from cobmeasure
        where cobmeasure.id = MObItem.basicunitofmeasure
       ) as basicunitofmeasurename,
       MObItem.batchmanagementrequired,
       (select CObMaterialGroup.name
        from CObMaterialGroup
        where MObItem.itemgroup = CObMaterialGroup.id
       ) as itemgroupname,
       (select CObMaterialGroup.code
        from CObMaterialGroup
        where MObItem.itemgroup = CObMaterialGroup.id
       ) as itemgroupcode,
       (select string_agg(cobenterprise.code, ', ')
        from cobenterprise
        where cobenterprise.id in (select iobitempurchaseunit.purchaseunitid
                                   from iobitempurchaseunit
                                   where iobitempurchaseunit.refInstanceId = MObItem.id)
          and cobenterprise.objectTypeCode = '5'
       ) as purchaseunitscodes,
       (select array_to_json(array_agg(cobenterprise.name))
        from cobenterprise
        where cobenterprise.id in (select iobitempurchaseunit.purchaseunitid
                                   from iobitempurchaseunit
                                   where iobitempurchaseunit.refInstanceId = MObItem.id)
          and cobenterprise.objectTypeCode = '5'
       ) as purchaseunitsnames
from mobitem
         left join MasterData on mobitem.id = MasterData.id;

create view IObItemAccountingInfoGeneralModel as
select
    IObItemAccountingInfo.id,
    IObItemAccountingInfo.refInstanceId,
    IObItemAccountingInfo.creationDate,
    IObItemAccountingInfo.modifiedDate,
    IObItemAccountingInfo.creationInfo,
    IObItemAccountingInfo.modificationInfo,
    hobglaccount.code as accountCode,
    hobglaccount.name as accountName,
(select
    MasterData.code
    from MasterData
    where MasterData.id = refInstanceId
)
                                             as itemCode
from IObItemAccountingInfo
         join hobglaccount on IObItemAccountingInfo.chartOfAccountId = hobglaccount.id;

create view IObAlternativeUoMGeneralModel as
select iobalternativeuom.id,
       iobalternativeuom.creationdate,
       iobalternativeuom.modifieddate,
       iobalternativeuom.creationinfo,
       iobalternativeuom.modificationinfo,
       iobalternativeuom.olditemnumber,
       iobalternativeuom.conversionfactor,
       iobalternativeuom.alternativeunitofmeasureid,
       cobmeasure.name                                                    as alternativeunitofmeasurename,
       cobmeasure.symbol                                                  as alternativeunitofmeasuresymbol,
       cobmeasure.code                                                    as alternativeunitofmeasurecode,
       (select mobitemgeneralmodel.basicunitofmeasurecode
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as basicunitofmeasurecode,
        (select mobitemgeneralmodel.basicunitofmeasurename
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as basicunitofmeasurename,
        (select mobitemgeneralmodel.basicunitofmeasuresymbol
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as basicunitofmeasuresymbol,
     (select mobitemgeneralmodel.code
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as itemcode,
     (select mobitemgeneralmodel.name
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as itemname,
       (select mobitemgeneralmodel.id
        from mobitemgeneralmodel
        where (iobalternativeuom.refinstanceid = mobitemgeneralmodel.id)) as itemid
from (iobalternativeuom
         left join cobmeasure on ((cobmeasure.id = iobalternativeuom.alternativeunitofmeasureid)));

alter table MObItem
    add CONSTRAINT FKMObItem646556 FOREIGN KEY (id) REFERENCES MasterData (id) ON update CASCADE ON delete CASCADE;
alter table MObItem
    add CONSTRAINT itemGroupId FOREIGN KEY (itemGroup) REFERENCES CObMaterialGroup (id) ON update CASCADE ON delete CASCADE;
alter table MObItem
    add CONSTRAINT basicUnitOfMeasure FOREIGN KEY (basicUnitOfMeasure) REFERENCES cobmeasure (id) ON update CASCADE ON delete CASCADE;
alter table IObItemPurchaseUnit
    add CONSTRAINT refInstanceId FOREIGN KEY (refInstanceId) REFERENCES MObItem (id) ON update CASCADE ON delete CASCADE;
alter table IObItemAccountingInfo
    add CONSTRAINT refInstanceId FOREIGN KEY (refInstanceId) REFERENCES MObItem (id) ON update CASCADE ON delete CASCADE;
alter table IObItemPurchaseUnit
    add CONSTRAINT purchaseUnitId FOREIGN KEY (purchaseunitid) REFERENCES cobenterprise (id) ON update CASCADE ON delete RESTRICT;
alter table IObAlternativeUoM
    add CONSTRAINT FKIObAlterna308375 FOREIGN KEY (refInstanceId) REFERENCES mobitem (id) ON update CASCADE ON delete CASCADE;
alter table IObAlternativeUoM
    add CONSTRAINT FKIObAlterna308376 FOREIGN KEY (alternativeUnitOfMeasureId) REFERENCES CObMeasure (id) ON update CASCADE ON delete CASCADE;

alter table IObVendorPurchaseUnit
    add CONSTRAINT refInstanceId FOREIGN KEY (refInstanceId) REFERENCES MObVendor (id) ON update CASCADE ON delete CASCADE;
alter table IObVendorPurchaseUnit
    add CONSTRAINT purchaseUnitId FOREIGN KEY (purchaseunitid) REFERENCES cobenterprise (id) ON update CASCADE ON delete RESTRICT;

alter table IObAlternativeUoM
    add CONSTRAINT IObAlternativeUoM_cobmeasure FOREIGN KEY (alternativeUnitOfMeasureId) REFERENCES cobmeasure (id) ON delete RESTRICT;

alter table IObItemAccountingInfo
    add CONSTRAINT chartOfAccountId_fk FOREIGN KEY (chartOfAccountId) REFERENCES hobglaccount (id) ON update CASCADE ON delete RESTRICT;

alter table IObVendorAccountingInfo
    add CONSTRAINT chartOfAccountId_chartOfAccountId_fk FOREIGN KEY (chartOfAccountId) REFERENCES hobglaccount (id) ON update CASCADE ON delete RESTRICT;

alter table IObVendorAccountingInfo
    add CONSTRAINT chartOfAccountId_refInstanceId_fk FOREIGN KEY (refInstanceId) REFERENCES MObVendor (id) ON update CASCADE ON delete CASCADE;

-- ------------------------------------------------- VENDOR ------------------------------------

drop view IF EXISTS MObVendorGeneralModel;

drop table IF EXISTS LObSimpleMaterialData;

-- sequences

-- Sequence: Sequence_LObSimpleMaterialData
create sequence Sequence_LObSimpleMaterialData
    increment by 1
    minvalue 1
    maxvalue 9223372036854775807
    start with 1
    NO CYCLE
;

-- Table: LObSimpleMaterialData
create TABLE LObSimpleMaterialData
(
    id               bigserial                   NOT NULL,
    code             character varying(1024),
    creationdate     timestamp with time zone NOT NULL,
    modifieddate     timestamp with time zone NOT NULL,
    creationinfo     character varying(1024)     NOT NULL,
    modificationinfo character varying(1024)     NOT NULL,
    objecttypecode   character varying(1024)     NOT NULL,
    name             varchar(1024)               NOT NULL,
    description      character varying(2048),
    sysflag          boolean                     NOT NULL,
    CONSTRAINT lObsimplematerialdata_pkey PRIMARY KEY (id)
);

-- views
-- View: MObVendorGeneralModel
create view MObVendorGeneralModel as
select MasterData.id,
       MasterData.code,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.name              as vendorname,
       MasterData.currentstates,
       MObVendor.oldVendorNumber,
       MObVendor.accountWithVendor,
       (select code
        from cobenterprise
        where id = MObVendor.purchaseresponsibleid
          and objecttypecode = '6') as purchaseResponsibleCode,
       (select name
        from cobenterprise
        where id = MObVendor.purchaseresponsibleid
          and objecttypecode = '6') as purchaseResponsibleName,
       (select string_agg(cobenterprise.code, ', ')
        from cobenterprise
        where cobenterprise.id in (select IObVendorPurchaseUnit.purchaseunitid
                                   from IObVendorPurchaseUnit
                                   where IObVendorPurchaseUnit.refInstanceId = MObVendor.id)
          and cobenterprise.objectTypeCode = '5'
       )                            as purchaseunitscodes,
       (select array_to_json(array_agg(cobenterprise.name))
        from cobenterprise
        where cobenterprise.id in (select IObVendorPurchaseUnit.purchaseunitid
                                   from IObVendorPurchaseUnit
                                   where IObVendorPurchaseUnit.refInstanceId = MObVendor.id)
          and cobenterprise.objectTypeCode = '5'
       )                            as purchaseunitsnames

from MObVendor
         left join MasterData on MObVendor.id = MasterData.id;

create view IObVendorAccountingInfoGeneralModel as
SELECT iobvendoraccountinginfo.id,
         iobvendoraccountinginfo.refinstanceid,
         iobvendoraccountinginfo.creationdate,
         iobvendoraccountinginfo.modifieddate,
         iobvendoraccountinginfo.creationinfo,
         iobvendoraccountinginfo.modificationinfo,
         hobglaccount.code                                               AS accountcode,
         hobglaccount.name                                               AS accountname,
         leafglaccount.leadger                                           AS accountleadger,
         (SELECT masterdata.code
          FROM masterdata
          WHERE (masterdata.id = iobvendoraccountinginfo.refinstanceid)) AS vendorcode,
         (SELECT masterdata.name
          FROM masterdata
          WHERE (masterdata.id = iobvendoraccountinginfo.refinstanceid)) AS vendorname,
         (SELECT mobvendorgeneralmodel.purchaseunitscodes
          FROM mobvendorgeneralmodel
          WHERE (mobvendorgeneralmodel.id = iobvendoraccountinginfo.refinstanceid)) AS vendorpurchaseunitcodes
FROM iobvendoraccountinginfo
         full outer join  hobglaccount ON iobvendoraccountinginfo.chartofaccountid = hobglaccount.id
         full outer join leafglaccount on hobglaccount.id = leafglaccount.id
where iobvendoraccountinginfo.id is not null;


create view MObVendorPurchaseUnitGeneralModel as
select row_number() over ()               as id,
       MasterData.code,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.currentstates,
       MasterData.code                    as vendorcode,
       cobenterprise.code                 as purchaseunitcode,
       cobenterprise.name:: json ->> 'en' as purchaseunitname
From iobvendorpurchaseunit
         join masterdata on iobvendorpurchaseunit.refinstanceid = masterdata.id AND
                            masterdata.objecttypecode = '2'
         join cobenterprise on cobenterprise.id = iobvendorpurchaseunit.purchaseunitid;


create view MObItemPurchaseUnitGeneralModel as
select row_number() over ()               as id,
       MasterData.code,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.currentstates,
       MasterData.code                    as itemcode,
       cobenterprise.code                 as purchaseunitcode,
       cobenterprise.name:: json ->> 'en' as purchaseunitname
From IObItemPurchaseUnit
         join masterdata on IObItemPurchaseUnit.refinstanceid = masterdata.id AND
                            masterdata.objecttypecode = '1'
         join cobenterprise on cobenterprise.id = IObItemPurchaseUnit.purchaseunitid;


create view MObItemAuthorizationGeneralModel as
select row_number() over ()               as id,
       md.id                              as itemid,
       md.code,
       md.creationdate,
       md.creationinfo,
       md.modifieddate,
       md.modificationinfo,
       md.currentstates,
       (select cob.name ->> 'en'::text
        from cobenterprise cob
        where cob.id = ip.purchaseunitid) AS purchaseunitname,
       md.name                            AS itemname
FROM masterdata md
         LEFT JOIN iobitempurchaseunit ip ON md.id = ip.refinstanceid
WHERE md.objecttypecode::text = '1'::text;
-- foreign keys

alter table MObVendor
    add CONSTRAINT FKMObVendor646556 FOREIGN KEY (id) REFERENCES MObBusinessPartner (id) ON update CASCADE ON delete CASCADE;

alter table MObBusinessPartner
    add CONSTRAINT FKMObBusinessPartner646556 FOREIGN KEY (id) REFERENCES MasterData (id) ON update CASCADE ON delete CASCADE;

-- End of file.

-- ------------------------------------------------- IVR ------------------------------------

create TABLE itemvendorrecord
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    vendorId         INT8          NOT NULL REFERENCES MObVendor (id) ON delete RESTRICT,
    itemId           INT8          NOT NULL,
    oldItemNumber    VARCHAR(1024) NOT NULL,
    uomId            INT8,
    currencyId       INT8,
    itemVendorCode   VARCHAR(1024),
    price            FLOAT,
    purchaseUnitId   INT8          NOT NULL,
    alternativeUomId INT8,
    PRIMARY KEY (id)
);

create view LObAttachmentTypeGeneralModel as
select lobattachmenttype.id,
       lobattachmenttype.code,
       lobattachmenttype.name
from lobattachmenttype;

create view ItemVendorRecordGeneralModel as
select itemvendorrecord.id                as id,
       itemvendorrecord.code              as usercode,
       itemvendorrecord.creationdate,
       itemvendorrecord.creationinfo,
       itemvendorrecord.modifieddate,
       itemvendorrecord.modificationinfo,
       vendor.name                        as vendorname,
       vendor.code                        as vendorcode,
       item.name                          as itemname,
       item.code                          as itemcode,
       measure.symbol                     as uomsymbol,
       measure.code                       as uomcode,
       purchaseunit.name :: json ->> 'en' As purchaseUnitNameEn,
       purchaseunit.name                  AS purchaseunitname,
       purchaseunit.code                  AS purchaseunitcode,
       itemvendorrecord.itemvendorcode,
       itemvendorrecord.oldItemNumber,
       currency.name                      as currencyname,
       currency.code                      as currencycode,
       currency.iso                       as currencysymbol,
       itemvendorrecord.price,
       item.id                            as itemId,
       vendor.id                          as vendorId,
       purchaseunit.id                    AS purchaseUnitId,
       measure.id                         as measureId
FROM itemvendorrecord
         LEFT JOIN masterdata as vendor
                   on vendor.id = itemvendorrecord.vendorid and vendor.objecttypecode like '2'
         LEFT JOIN masterdata as item
                   on item.id = itemvendorrecord.itemid and item.objecttypecode like '1'
         LEFT JOIN cobmeasure as measure on measure.id = itemvendorrecord.uomId
         LEFT JOIN cobcurrency as currency on currency.id = itemvendorrecord.currencyid
         LEFT JOIN cobenterprise as purchaseunit
                   on purchaseunit.id = itemvendorrecord.purchaseunitid AND
                      purchaseunit.objecttypecode like '5'
order by itemvendorrecord.id desc;

create view ItemVendorCommonPurchaseUnitsGeneralModel as
select cobenterprise.id,
       cobenterprise.creationdate,
       cobenterprise.creationinfo,
       cobenterprise.modifieddate,
       cobenterprise.modificationinfo,
       item.code          as itemCode,
       vendor.code        as vendorCode,
       cobenterprise.code as purchaseUnitCode,
       cobenterprise.name as purchaseUnitName
from cobenterprise,
     masterdata vendor,
     masterdata item,
     iobvendorpurchaseunit,
     iobitempurchaseunit
where iobvendorpurchaseunit.purchaseunitid = cobenterprise.id
  and iobvendorpurchaseunit.purchaseunitid = iobitempurchaseunit.purchaseunitid
  and vendor.id = iobvendorpurchaseunit.refinstanceid
  and item.id = iobitempurchaseunit.refinstanceid
group by (cobenterprise.id, itemCode, vendorCode, purchaseUnitCode);

alter table itemvendorrecord
    add CONSTRAINT itemvendorrecordDelete FOREIGN KEY (itemId) REFERENCES masterdata (id) ON delete RESTRICT;

alter table itemvendorrecord
    add CONSTRAINT itemvendorrecord_cobmeasure FOREIGN KEY (uomId) REFERENCES cobmeasure (id) ON delete RESTRICT;

alter table itemvendorrecord
    add CONSTRAINT itemvendorrecord_iobalternativeuom FOREIGN KEY (alternativeUomId) REFERENCES IObAlternativeUoM (id) ON delete RESTRICT;

