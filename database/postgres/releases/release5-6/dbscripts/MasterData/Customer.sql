drop table if exists IObCustomerTaxInfo;
drop table if exists IObCustomerAddress;
drop table if exists IObCustomerContactPerson;
drop table if exists IObCustomerBusinessUnit;
drop table if exists CObCustomer;
drop table if exists MObCustomer;
drop table if exists IObCustomerAccountingInfo;
drop view if exists IObCustomerAccountingInfoGeneralModel;
DROP VIEW IF EXISTS IObCustomerBusinessUnitGeneralModel ;

create table CObCustomer (
   id               BIGSERIAL     NOT NULL,
   code             VARCHAR(1024) NOT NULL,
   name             json          NOT NULL,
   creationDate     TIMESTAMP     NOT NULL,
   modifiedDate     TIMESTAMP     NOT NULL,
   creationInfo     VARCHAR(1024) NOT NULL,
   modificationInfo VARCHAR(1024) NOT NULL,
   currentStates    VARCHAR(1024) NOT NULL,
   PRIMARY KEY (id)
);


create TABLE MObCustomer
(
    id                    BIGSERIAL          NOT NULL,
    kapId                 INT8          ,
    riskLevelId             INT8,
    legalTypeId             INT8,
    customerTypeId          INT8,
    creditLimit           float,
    PRIMARY KEY (id)
);

create view MObCustomerGeneralModel as
select id ,
(select code  from masterdata where MObCustomer.id = masterdata.id) as code,
(select name  from masterdata where MObCustomer.id = masterdata.id) as customerName,
(select code  from cobenterprise where MObCustomer.kapId = cobenterprise.id) as kapCode,
(select name  from cobenterprise where MObCustomer.kapId = cobenterprise.id) as kapName
from MObCustomer;



alter table MObCustomer
    add CONSTRAINT FK_MObCustomer_id FOREIGN KEY (id) REFERENCES MObBusinessPartner (id) ON update CASCADE ON delete CASCADE;

alter table MObCustomer
    add CONSTRAINT FK_MObCustomer_kap FOREIGN KEY (kapId) REFERENCES CObenterprise (id) ON delete RESTRICT;

alter table MObCustomer
    add CONSTRAINT FK_MObCustomer_riskLevel FOREIGN KEY (riskLevelId) REFERENCES LObRiskLevel (id) ON delete RESTRICT;

alter table MObCustomer
    add CONSTRAINT FK_MObCustomer_legalType FOREIGN KEY (legalTypeId) REFERENCES LObLegalType (id) ON delete RESTRICT;

alter table MObCustomer
    add CONSTRAINT FK_MObCustomer_customerType FOREIGN KEY (customerTypeId) REFERENCES CObCustomer (id) ON delete RESTRICT;

create TABLE IObCustomerTaxInfo
(
    id               BIGSERIAL          NOT NULL,
    refInstanceId       INT8          NOT NULL,
    creationDate        TIMESTAMP     NOT NULL,
    modifiedDate        TIMESTAMP     NOT NULL,
    creationInfo        VARCHAR(1024) NOT NULL,
    modificationInfo    VARCHAR(1024) NOT NULL,
    registrationNumber  VARCHAR(1024) ,
    taxCard             VARCHAR(1024) ,
    issueFromId         INT8,
    taxAdministrativeId INT8,
    taxFileNumber       VARCHAR(1024) ,
    primary key (id)
);

alter table IObCustomerTaxInfo
    add CONSTRAINT FK_IObCustomerTaxInfo_issuefromId FOREIGN KEY (issueFromId) REFERENCES LObIssueFrom (id) ON delete RESTRICT;

alter table IObCustomerTaxInfo
    add CONSTRAINT FK_IObCustomerTaxInfo_taxAdministrativeId FOREIGN KEY (taxAdministrativeId) REFERENCES LObTaxAdministrative (id) ON delete RESTRICT;

ALTER TABLE IObCustomerTaxInfo
    ADD CONSTRAINT FK_IObCustomerTaxInfo_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES MObCustomer (id) ON DELETE CASCADE;




create TABLE IObCustomerAddress
(
    id               BIGSERIAL          NOT NULL,
    refInstanceId       INT8          NOT NULL,
    creationDate        TIMESTAMP     NOT NULL,
    modifiedDate        TIMESTAMP     NOT NULL,
    creationInfo        VARCHAR(1024) NOT NULL,
    modificationInfo    VARCHAR(1024) NOT NULL,
    addressId INT8,
    type  VARCHAR(1024) ,
    primary key (id)
);


ALTER TABLE IObCustomerAddress
    ADD CONSTRAINT FK_IObCustomerAddress_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES MObCustomer (id) ON DELETE CASCADE;

ALTER TABLE IObCustomerAddress
    ADD CONSTRAINT FK_IObCustomerAddress_address FOREIGN KEY (addressId) REFERENCES LObAddress (id) ON DELETE CASCADE;

create TABLE IObCustomerContactPerson
(
    id               BIGSERIAL          NOT NULL,
    refInstanceId       INT8          NOT NULL,
    creationDate        TIMESTAMP     NOT NULL,
    modifiedDate        TIMESTAMP     NOT NULL,
    creationInfo        VARCHAR(1024) NOT NULL,
    modificationInfo    VARCHAR(1024) NOT NULL,
    contactPersonId INT8,
    primary key (id)
);


ALTER TABLE IObCustomerContactPerson
    ADD CONSTRAINT FK_IObCustomerContactPerson_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES MObCustomer (id) ON DELETE CASCADE;


ALTER TABLE IObCustomerContactPerson
    ADD CONSTRAINT FK_IObCustomerContactPerson_contactPersonId FOREIGN KEY (contactPersonId) REFERENCES LObContactPerson (id) ON DELETE CASCADE;


create TABLE IObCustomerBusinessUnit
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    businessUnitId   INT8          NOT NULL,
    primary key (id)
);


ALTER TABLE IObCustomerBusinessUnit
    ADD CONSTRAINT FK_IObCustomerBusinessUnit_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES MObCustomer (id) ON DELETE CASCADE;

ALTER TABLE IObCustomerBusinessUnit
    ADD CONSTRAINT FK_IObCustomerBusinessUnit_businessunit FOREIGN KEY (businessUnitId) REFERENCES cobenterprise (id) ON DELETE RESTRICT;



create view IObCustomerBusinessUnitGeneralModel as
SELECT row_number() over ()               as id,
       MasterData.creationdate,
       MasterData.creationinfo,
       MasterData.modifieddate,
       MasterData.modificationinfo,
       MasterData.currentstates,
       MasterData.code,
       MObCustomer.id                     as customerId,
       MasterData.name                    as customerName,
       cobenterprise.code                 as businessUnitcode,
       cobenterprise.name:: json ->> 'en' as businessUnitName
From masterdata
         join MObCustomer on masterdata.id = MObCustomer.id
    AND masterdata.objecttypecode = '3'
         join iobcustomerbusinessunit on iobcustomerbusinessunit.refinstanceid = MObCustomer.id
         join cobenterprise on cobenterprise.id = iobcustomerbusinessunit.businessunitid;
         
       
 create TABLE IObCustomerAccountingInfo
(
    id               INT8          NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     timestamp with time zone     NOT NULL,
    modifiedDate     timestamp with time zone     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    chartOfAccountId   INT8          NOT NULL,
    primary key (id)
);

ALTER TABLE IObCustomerAccountingInfo
    ADD CONSTRAINT FK_IObCustomerAccountingInfo_MObBusinessPartner FOREIGN KEY (refInstanceId) REFERENCES mobbusinesspartner (id) ON DELETE RESTRICT;

ALTER TABLE IObCustomerAccountingInfo
    ADD CONSTRAINT FK_IObCustomerAccountingInfo_HObGlAccount FOREIGN KEY (chartOfAccountId) REFERENCES hobglaccount (id) ON DELETE RESTRICT;

create view IObCustomerAccountingInfoGeneralModel as
SELECT IObCustomerAccountingInfo.id,
       IObCustomerAccountingInfo.refinstanceid,
       IObCustomerAccountingInfo.creationdate,
       IObCustomerAccountingInfo.modifieddate,
       IObCustomerAccountingInfo.creationinfo,
       IObCustomerAccountingInfo.modificationinfo,
       hobglaccount.code                                               AS accountcode,
       hobglaccount.name                                               AS accountname,
       leafglaccount.leadger                                           AS accountledger,
       (SELECT masterdata.code
        FROM masterdata
        WHERE (masterdata.id = IObCustomerAccountingInfo.refinstanceid)) AS customercode,
       (SELECT masterdata.name
        FROM masterdata
        WHERE (masterdata.id = IObCustomerAccountingInfo.refinstanceid)) AS customername
FROM IObCustomerAccountingInfo
         join  hobglaccount ON IObCustomerAccountingInfo.chartofaccountid = hobglaccount.id
         join leafglaccount on hobglaccount.id = leafglaccount.id;
