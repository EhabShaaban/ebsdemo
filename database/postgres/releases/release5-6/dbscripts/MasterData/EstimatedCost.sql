DROP TABLE IF EXISTS CObEstimatedCostType CASCADE;


CREATE TABLE CObEstimatedCostType
(
    id               BIGSERIAL     NOT NULL,
    code             VARCHAR(1024) NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    name             JSON,
    currentStates    VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE IObPurchaseOrderEstimatedCost
(
    id                  BIGSERIAL     NOT NULL,
    refInstanceId       INT8          NOT NULL,
    creationDate        TIMESTAMP     NOT NULL,
    modifiedDate        TIMESTAMP     NOT NULL,
    creationInfo        VARCHAR(1024) NOT NULL,
    modificationInfo    VARCHAR(1024) NOT NULL,
    orderTypeId         INT8          NOT NULL,
    estimatedCostTypeId INT8          NOT NULL,
    amount              DOUBLE PRECISION,
    PRIMARY KEY (id)
);


ALTER TABLE IObPurchaseOrderEstimatedCost
    ADD CONSTRAINT FK_IObPurchaseOrderEstimatedCost_DObOrder FOREIGN KEY (refInstanceId) REFERENCES DObOrder (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE IObPurchaseOrderEstimatedCost
    ADD CONSTRAINT FK_IObPurchaseOrderEstimatedCost_CObEstimatedCostType FOREIGN KEY (estimatedCostTypeId) REFERENCES CObEstimatedCostType (id) ON UPDATE CASCADE ON DELETE RESTRICT;


CREATE VIEW IObPurchaseOrderEstimatedCostGeneralModel AS
SELECT IObPurchaseOrderEstimatedCost.id,
       IObPurchaseOrderEstimatedCost.creationDate,
       IObPurchaseOrderEstimatedCost.modifiedDate,
       IObPurchaseOrderEstimatedCost.creationInfo,
       IObPurchaseOrderEstimatedCost.modificationInfo,
       IObPurchaseOrderEstimatedCost.orderTypeId,
       CObEstimatedCostType.code,
       CObEstimatedCostType.name,
       IObPurchaseOrderEstimatedCost.amount,
       DObPurchaseOrderGeneralModel.code                                    AS purchaseOrderCode,
       (SELECT currencyIso From getDObOrderCompanyCurrencyInformation(IObPurchaseOrderEstimatedCost.refInstanceId))
FROM IObPurchaseOrderEstimatedCost
         JOIN DObPurchaseOrderGeneralModel
              ON IObPurchaseOrderEstimatedCost.refInstanceId =
                 DObPurchaseOrderGeneralModel.id
         JOIN CObEstimatedCostType
              ON CObEstimatedCostType.id = IObPurchaseOrderEstimatedCost.estimatedCostTypeId;
