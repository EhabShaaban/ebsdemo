drop table if exists IObGoodsIssueConsignee;
drop table if exists IObGoodsIssueStore;
drop table if exists IObGoodsIssueItem;
drop table if exists IObGoodsIssuePostingDetails;
drop table if exists DObGoodsIssue;
drop table if exists CObGoodsIssue;
drop table if exists IObGoodsIssueSalesInvoiceConsignee;

create table DObGoodsIssue
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentStates    VARCHAR(1024)            NOT NULL,
    typeId           INT8                     NOT NULL,
    purchaseUnitId   INT8                     NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE DObGoodsIssue
    ADD CONSTRAINT FK_dobgoodsissue_purchaseunit FOREIGN KEY (purchaseUnitId) REFERENCES cobenterprise (id) ON DELETE RESTRICT;

create table CObGoodsIssue
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    name             json                     NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentStates    VARCHAR(1024)            NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE DObGoodsIssue
    ADD CONSTRAINT FK_COBGoodsIssue_DObGoodsIssue FOREIGN KEY (typeId) REFERENCES COBGoodsIssue (id) ON DELETE RESTRICT;


CREATE TABLE IObGoodsIssueStore
(

    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    companyId        INT8          NOT NULL,
    plantId          INT8          NOT NULL,
    storehouseId     INT8          NOT NULL,
    storeKeeperId    INT8          NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE IObGoodsIssueStore
    ADD CONSTRAINT FK_IObGoodsIssueStore_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObGoodsIssue (id) ON DELETE CASCADE;

ALTER TABLE IObGoodsIssueStore
    ADD CONSTRAINT FK_IObGoodsIssueStore_store FOREIGN KEY (storehouseId) REFERENCES cobstorehouse (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueStore
    ADD CONSTRAINT FK_IObGoodsIssueStore_storekeeper FOREIGN KEY (storeKeeperId) REFERENCES storekeeper (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueStore
    ADD CONSTRAINT FK_IObGoodsIssueStore_plant FOREIGN KEY (plantId) REFERENCES cobplant (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueStore
    ADD CONSTRAINT FK_IObGoodsIssueStore_company FOREIGN KEY (companyId) REFERENCES cobcompany (id) ON DELETE RESTRICT;


CREATE TABLE IObGoodsIssueConsignee
(
    id                    BIGSERIAL     NOT NULL,
    refInstanceId         INT8          NOT NULL,
    creationDate          TIMESTAMP     NOT NULL,
    modifiedDate          TIMESTAMP     NOT NULL,
    creationInfo          VARCHAR(1024) NOT NULL,
    modificationInfo      VARCHAR(1024) NOT NULL,
    customerId            INT8,
    addressId             INT8,
    contactPersonId       INT8,
    salesRepresentativeId INT8,
    type                  VARCHAR(50),
    comments              VARCHAR(1024),
    PRIMARY KEY (id)
);


CREATE TABLE IObGoodsIssueSalesInvoiceConsignee
(
    id                    BIGSERIAL     NOT NULL,
    salesInvoiceId        INT8         NOT NULL,
    PRIMARY KEY (id)
);


ALTER TABLE IObGoodsIssueConsignee
    ADD CONSTRAINT FK_IObGoodsIssueConsignee_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObGoodsIssue (id) ON DELETE CASCADE;

ALTER TABLE IObGoodsIssueConsignee
    ADD CONSTRAINT FK_IObGoodsIssueConsignee_customer FOREIGN KEY (customerId) REFERENCES MObCustomer (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueConsignee
    ADD CONSTRAINT FK_IObGoodsIssueConsignee_address FOREIGN KEY (addressId) REFERENCES lobaddress (Id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueConsignee
    ADD CONSTRAINT FK_IObGoodsIssueConsignee_contactPerson FOREIGN KEY (contactPersonId) REFERENCES lobcontactperson (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueConsignee
    ADD CONSTRAINT FK_IObGoodsIssueConsignee_salesrepresentative FOREIGN KEY (salesrepresentativeId) REFERENCES ebsuser (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueSalesInvoiceConsignee
    ADD CONSTRAINT FK_goodsissuesalesinvoiceconsignee_goodsissueconsignee FOREIGN KEY (id) REFERENCES IObGoodsIssueConsignee (id) ON DELETE CASCADE ;

ALTER TABLE IObGoodsIssueSalesInvoiceConsignee
    ADD CONSTRAINT FK_goodsissuesalesinvoiceconsignee_invoice FOREIGN KEY (salesInvoiceId) REFERENCES dobsalesinvoice (id) ON DELETE RESTRICT;

CREATE TABLE IObGoodsIssueItem
(
    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    itemId           INT8,
    unitOfMeasureId  INT8,
    quantity         INT8,
    batchNo          VARCHAR(1024),
    price            double precision,
    PRIMARY KEY (id)
);

ALTER TABLE IObGoodsIssueItem
    ADD CONSTRAINT FK_iobGoodsIssueItem_item FOREIGN KEY (itemId) REFERENCES mobitem (id) ON DELETE RESTRICT;

ALTER TABLE IObGoodsIssueItem
    ADD CONSTRAINT FK_iobGoodsIssueItem_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObGoodsIssue (id) ON DELETE CASCADE;

ALTER TABLE IObGoodsIssueItem
    ADD CONSTRAINT FK_iobGoodsIssueItem_unitOfMeasure FOREIGN KEY (unitOfMeasureId) REFERENCES cobmeasure (id) ON DELETE RESTRICT;


CREATE TABLE IObGoodsIssuePostingDetails
(

    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    postedBy         VARCHAR(1024) NOT NULL,
    postingDate      TIMESTAMP     NOT NULL,
    PRIMARY KEY (id)
);
ALTER TABLE IObGoodsIssuePostingDetails
    ADD CONSTRAINT FK_IObGoodsIssuePostingDetails_refinstanceid FOREIGN KEY (refInstanceId) REFERENCES DObGoodsIssue (id) ON DELETE CASCADE;

