DROP VIEW IF Exists IObGoodsIssueConsigneeGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssueStoreGeneralModel CASCADE;
DROP VIEW IF Exists DObGoodsIssueDataGeneralModel CASCADE;
DROP VIEW IF Exists DObGoodsIssueGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssueItemGeneralModel CASCADE;
DROP VIEW IF Exists IObGoodsIssuePostingDetailsGeneralModel CASCADE;

CREATE VIEW IObGoodsIssueConsigneeGeneralModel AS
select IObGoodsIssueConsignee.id,
       IObGoodsIssueConsignee.refInstanceId,
       IObGoodsIssueConsignee.comments,
       (SELECT DObGoodsIssue.code
        FROM DObGoodsIssue
        WHERE IObGoodsIssueConsignee.refInstanceId =
              DObGoodsIssue.id)                                                AS goodsIssueCode,
       (select masterdata.name
        from masterdata
        where IObGoodsIssueConsignee.customerId = masterdata.id)               as customerName,
       (select masterdata.code
        from masterdata
        where IObGoodsIssueConsignee.customerId = masterdata.id)               as customerCode,
       (select lobaddress.address
        from lobaddress
        where IObGoodsIssueConsignee.addressId = lobaddress.id)::json ->> 'en' as address,
       (select lobcontactperson.name
        from lobcontactperson
        where IObGoodsIssueConsignee.contactPersonId =
              lobcontactperson.id)                                             as contactPersonName,
       (select lobcontactperson.name:: json ->> 'en'
        from lobcontactperson
        where IObGoodsIssueConsignee.contactPersonId =
              lobcontactperson.id)                                             as contactPersonNameEn,
       (select ebsuser.username
        from ebsuser
        where IObGoodsIssueConsignee.salesRepresentativeId = ebsuser.id)       as salesRepresentativeName,
       (select MObCustomerGeneralModel.kapCode
        from MObCustomerGeneralModel
        where IObGoodsIssueConsignee.customerId = MObCustomerGeneralModel.id)  as kapCode,
       (select MObCustomerGeneralModel.kapName
        from MObCustomerGeneralModel
        where IObGoodsIssueConsignee.customerId = MObCustomerGeneralModel.id)  as kapName,
       (select dobsalesinvoice.code
        from dobsalesinvoice
        where IObGoodsIssueSalesInvoiceConsignee.salesInvoiceId =
              dobsalesinvoice.id)                                              as salesInvoice
from IObGoodsIssueConsignee
         LEFT JOIN IObGoodsIssueSalesInvoiceConsignee
                   on IObGoodsIssueConsignee.id = IObGoodsIssueSalesInvoiceConsignee.id;

CREATE VIEW IObGoodsIssueStoreGeneralModel AS
(
SELECT IObGoodsIssueStore.id,
       IObGoodsIssueStore.refInstanceId,
       (SELECT DObGoodsIssue.code
        FROM DObGoodsIssue
        WHERE IObGoodsIssueStore.refInstanceId =
              DObGoodsIssue.id)                                   AS goodsIssueCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueStore.companyId = cobenterprise.id)    AS companyName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObGoodsIssueStore.companyId = cobenterprise.id)    AS companyCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueStore.plantId = cobenterprise.id)      AS plantName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObGoodsIssueStore.plantId = cobenterprise.id)      AS plantCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        WHERE IObGoodsIssueStore.storehouseId = cobenterprise.id) AS storehouseName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        WHERE IObGoodsIssueStore.storehouseId = cobenterprise.id) AS storehouseCode,
       (SELECT storekeeper.name
        FROM storekeeper
        WHERE IObGoodsIssueStore.storeKeeperId = storekeeper.id)  AS storeKeeperName,
       (SELECT storekeeper.code
        FROM storekeeper
        WHERE IObGoodsIssueStore.storeKeeperId = storekeeper.id)  AS storeKeeperCode
FROM IObGoodsIssueStore
    );

CREATE VIEW DObGoodsIssueDataGeneralModel AS
SELECT DObGoodsIssue.id,
       DObGoodsIssue.code,
       c1.id                                           as purchaseUnitId,
       c1.code                                         as purchaseUnitCode,
       c1.name                                         as purchaseUnitName,
       DObGoodsIssue.currentStates,
       m1.id                                           as itemId,
       m1.code                                         as itemCode,
       m1.name                                         as itemName,
       cobmeasure.id                                   AS unitOfMeasureId,
       cobmeasure.code                                 AS unitOfMeasureCode,
       cobmeasure.name                                 AS unitOfMeasureName,
       IObGoodsIssueItem.quantity,
       IObGoodsIssueItem.batchNo,
       IObGoodsIssueItem.price,
       CObGoodsIssue.code                              as goodsIssueTypeCode,
       CObGoodsIssue.name                              as goodsIssueTypeName,
       DObGoodsIssue.creationInfo,
       DObGoodsIssue.creationDate,
       DObGoodsIssue.modificationInfo,
       DObGoodsIssue.modifiedDate,
       DObSalesInvoice.code                            as invoiceCode,
       c2.id                                           as companyId,
       c2.code                                         as companyCode,
       c2.name                                         as companyName,
       c3.id                                           as plantId,
       c3.code                                         as plantCode,
       c3.name                                         as plantName,
       c4.id                                           as storeHouseId,
       c4.code                                         as storeHouseCode,
       c4.name                                         as storeHouseName,
       storekeeper.code                                as storekeeperCode,
       storekeeper.name                                as storekeeperName,
       m2.code                                         as customerCode,
       m2.name                                         as customerName,
       lobaddress.address::json ->> 'en'               as address,
       LObContactPerson.name                           as contactPersonName,
       c5.code                                         as salesRepresentativeCode,
       c5.name                                         as salesRepresentativeName,
       IObGoodsIssueConsignee.comments,
       c6.code                                         as kapCode,
       c6.name                                         as kapName,
       iobalternativeuom.conversionfactor,
       c2.name::json ->> 'ar'                          as companyArName,
       enterpriseTelephone.contactValue                as companyTelephone,
       enterpriseFax.contactValue                      as companyFax,
       iobcompanylogodetails.logo                      as companyLogo,
       IObEnterpriseAddress.addressLine                as companyAddress,
       IObEnterpriseAddress.addressLine::json ->> 'ar' as companyArAddress,
       m2.name::json ->> 'ar'                          as customerArName

from DObGoodsIssue
         left join cobenterprise c1 on DObGoodsIssue.purchaseunitid = c1.id
         left join IObGoodsIssueItem on DObGoodsIssue.id = IObGoodsIssueItem.refinstanceid
         left join mobitem on iobgoodsissueitem.itemid = mobitem.id
         left join masterdata m1 on mobitem.id = m1.id
         left join cobmeasure on iobgoodsissueitem.unitOfMeasureId = cobmeasure.id
         left join CObGoodsIssue on DObGoodsIssue.typeid = CObGoodsIssue.id
         left join IObGoodsIssueStore on dobgoodsissue.id = iobgoodsissuestore.refinstanceid
         left join cobcompany on iobgoodsissuestore.companyid = cobcompany.id
         left join cobenterprise c2 on c2.id = cobcompany.id
         left join cobplant on iobgoodsissuestore.plantid = cobplant.id
         left join cobenterprise c3 on c3.id = cobplant.id
         left join cobstorehouse on iobgoodsissuestore.storehouseid = cobstorehouse.id
         left join cobenterprise c4 on c4.id = cobstorehouse.id
         left join storekeeper on iobgoodsissuestore.storekeeperid = storekeeper.id
         left join IObGoodsIssueConsignee on DObGoodsIssue.id = IObGoodsIssueConsignee.refinstanceid
         left join IObGoodsIssueSalesInvoiceConsignee
                   on IObGoodsIssueConsignee.id = IObGoodsIssueSalesInvoiceConsignee.id
         left join DObSalesInvoice
                   on IObGoodsIssueSalesInvoiceConsignee.salesInvoiceId = DObSalesInvoice.id
         left join mobcustomer on IObGoodsIssueConsignee.customerid = mobcustomer.id
         left join MObBusinessPartner on mobcustomer.id = MObBusinessPartner.id
         left join masterdata m2 on MObBusinessPartner.id = m2.id
         left join iobcustomeraddress on mobcustomer.id = iobcustomeraddress.refinstanceid
         left join lobaddress on iobcustomeraddress.addressid = lobaddress.id
         left join IObCustomerContactPerson
                   on mobcustomer.id = IObCustomerContactPerson.refinstanceid
         left join LObContactPerson
                   on IObCustomerContactPerson.contactpersonid = LObContactPerson.id
         left join CObenterprise c5 on IObGoodsIssueConsignee.salesrepresentativeid = c5.id
         left join cobenterprise c6 on MObCustomer.kapid = c6.id
         left join iobalternativeuom
                   on IObGoodsIssueItem.itemId = iobalternativeuom.refinstanceid and
                      iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id
         left join IObEnterpriseContact enterpriseTelephone
                   on cobcompany.id = enterpriseTelephone.refinstanceid and
                      enterpriseTelephone.objectTypeCode = '1' and
                      enterpriseTelephone.contactTypeId = 1
         left join IObEnterpriseContact enterpriseFax
                   on cobcompany.id = enterpriseFax.refinstanceid and
                      enterpriseFax.objectTypeCode = '1' and
                      enterpriseFax.contactTypeId = 2
         left join iobcompanylogodetails on cobcompany.id = iobcompanylogodetails.refinstanceid
         left join IObEnterpriseAddress on cobcompany.id = IObEnterpriseAddress.refinstanceid;

CREATE VIEW DObGoodsIssueGeneralModel AS
SELECT DObGoodsIssue.id,
       DObGoodsIssue.code,
       DObGoodsIssue.creationDate,
       DObGoodsIssue.modifiedDate,
       DObGoodsIssue.creationInfo,
       DObGoodsIssue.modificationInfo,
       DObGoodsIssue.currentStates,
       (select cobenterprise.name
        from cobenterprise
        where DObGoodsIssue.purchaseUnitId = cobenterprise.id) as purchaseUnitName,
       (select cobenterprise.name:: json ->> 'en'
        from cobenterprise
        where DObGoodsIssue.purchaseUnitId = cobenterprise.id) as purchaseUnitNameEn,
       (select cobenterprise.code
        from cobenterprise
        where DObGoodsIssue.purchaseUnitId = cobenterprise.id) as purchaseUnitCode,
       (select cobgoodsissue.name
        from cobgoodsissue
        where DObGoodsIssue.typeid = cobgoodsissue.id)         as typeName,
       (select cobgoodsissue.name:: json ->> 'en'
        from cobgoodsissue
        where DObGoodsIssue.typeid = cobgoodsissue.id)         as typeNameEn,
       (select cobgoodsissue.code
        from cobgoodsissue
        where DObGoodsIssue.typeid = cobgoodsissue.id)         as typeCode,
       IObGoodsIssueStoreGeneralModel.storehouseName,
       IObGoodsIssueStoreGeneralModel.storehouseCode,
       IObGoodsIssueConsigneeGeneralModel.customerName,
       IObGoodsIssueConsigneeGeneralModel.customerCode,
       IObGoodsIssueStoreGeneralModel.companyCode,
       IObGoodsIssueStoreGeneralModel.companyName,
       IObGoodsIssueStoreGeneralModel.plantCode,
       IObGoodsIssueStoreGeneralModel.plantName,
       IObGoodsIssueStoreGeneralModel.storekeeperCode,
       IObGoodsIssueStoreGeneralModel.storekeeperName

FROM DObGoodsIssue
         left join IObGoodsIssueStoreGeneralModel
                   on DObGoodsIssue.id = IObGoodsIssueStoreGeneralModel.refInstanceId
         left join IObGoodsIssueConsigneeGeneralModel
                   on DObGoodsIssue.id = IObGoodsIssueConsigneeGeneralModel.refInstanceId;

CREATE VIEW IObGoodsIssueItemGeneralModel AS
select iobgoodsissueitem.id,
       iobgoodsissueitem.refInstanceId,
       (SELECT DObGoodsIssue.code
        FROM DObGoodsIssue
        WHERE iobgoodsissueitem.refInstanceId =
              DObGoodsIssue.id)                                  AS goodsIssueCode,
       (SELECT masterdata.name
        FROM masterdata,
             mobitem
        WHERE iobgoodsissueitem.itemId = mobitem.id
          and mobitem.id = masterdata.id)                        AS itemName,
       (SELECT masterdata.name::json ->> 'en'
        FROM masterdata,
             mobitem
        WHERE iobgoodsissueitem.itemId = mobitem.id
          and mobitem.id = masterdata.id)                        AS itemNameEn,
       (SELECT masterdata.code
        FROM masterdata,
             mobitem
        WHERE iobgoodsissueitem.itemId = mobitem.id
          and mobitem.id = masterdata.id)                        AS itemCode,
       (SELECT cobmeasure.symbol
        FROM cobmeasure
        WHERE iobgoodsissueitem.unitOfMeasureId = cobmeasure.id) AS unitOfMeasureSymbol,
       (SELECT cobmeasure.symbol::json ->> 'en'
        FROM cobmeasure
        WHERE iobgoodsissueitem.unitOfMeasureId = cobmeasure.id) AS unitOfMeasureSymbolEn,
       (SELECT cobmeasure.code
        FROM cobmeasure
        WHERE iobgoodsissueitem.unitOfMeasureId = cobmeasure.id) AS unitOfMeasureCode,
       iobgoodsissueitem.quantity,
       iobgoodsissueitem.batchNo,
       iobgoodsissueitem.price
from iobgoodsissueitem;


-----------------------------------------------------------------------------------------------------------

CREATE VIEW IObGoodsIssuePostingDetailsGeneralModel AS
(
SELECT iobgoodsissuepostingdetails.id,
       iobgoodsissuepostingdetails.refInstanceId,
       (SELECT DObGoodsIssue.code
        FROM DObGoodsIssue
        WHERE iobgoodsissuepostingdetails.refInstanceId =
              DObGoodsIssue.id) AS goodsIssueCode,
       iobgoodsissuepostingdetails.postedBy,
       iobgoodsissuepostingdetails.postingDate
from iobgoodsissuepostingdetails);















