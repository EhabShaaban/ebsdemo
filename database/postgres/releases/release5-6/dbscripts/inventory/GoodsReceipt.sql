﻿DROP TABLE IF EXISTS IObGoodsReceiptItemBatches CASCADE;
DROP TABLE IF EXISTS IObGoodsReceiptItemQuantities CASCADE;
DROP TABLE IF EXISTS IObGoodsReceiptRecievedItemsData CASCADE;
DROP TABLE IF EXISTS IObGoodsReceiptPurchaseOrderData CASCADE;
DROP TABLE IF EXISTS DObGoodsReceipt CASCADE;
DROP TABLE IF EXISTS StoreKeeper CASCADE;

CREATE TABLE DObGoodsReceipt
(
    id               BIGSERIAL                NOT NULL,
    code             VARCHAR(1024)            NOT NULL,
    creationDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    modifiedDate     TIMESTAMP WITH TIME ZONE NOT NULL,
    postingDate      TIMESTAMP WITH TIME ZONE,
    creationInfo     VARCHAR(1024)            NOT NULL,
    modificationInfo VARCHAR(1024)            NOT NULL,
    currentStates    VARCHAR(1024)            NOT NULL,
    type             VARCHAR(1024)            NOT NULL,
    storeKeeperId    INT8,
    storeKeeperName  json,
    storehouseId     INT8,
    storehouseName   json,
    plantId          INT8                     NOT NULL,
    plantName        json                     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IObGoodsReceiptPurchaseOrderData
(

    id                      BIGSERIAL     NOT NULL,
    refInstanceId           INT8          NOT NULL,
    creationDate            TIMESTAMP     NOT NULL,
    modifiedDate            TIMESTAMP     NOT NULL,
    creationInfo            VARCHAR(1024) NOT NULL,
    modificationInfo        VARCHAR(1024) NOT NULL,
    purchaseOrderId         INT8          NOT NULL,
    vendorId                INT8          NOT NULL REFERENCES MObVendor (id) ON DELETE RESTRICT,
    vendorName              json          NOT NULL,
    deliveryNote            VARCHAR(1024), -- packing list
    purchaseUnitId          INT8          NOT NULL,
    purchaseUnitName        json          NOT NULL,
    purchaseResponsibleId   INT8          NOT NULL,
    purchaseResponsibleName json          NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (refInstanceId) REFERENCES DObGoodsReceipt (id) ON DELETE CASCADE
);

CREATE TABLE StoreKeeper
(
    id   INT8,
    name json          NOT NULL,
    code VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id)
);


CREATE TABLE IObGoodsReceiptRecievedItemsData
(

    id               BIGSERIAL     NOT NULL,
    refInstanceId    INT8          NOT NULL,
    creationDate     TIMESTAMP     NOT NULL,
    modifiedDate     TIMESTAMP     NOT NULL,
    creationInfo     VARCHAR(1024) NOT NULL,
    modificationInfo VARCHAR(1024) NOT NULL,
    itemId           INT8          NOT NULL,
    stockType        VARCHAR(1024),
    differenceReason VARCHAR(1024),
    objectTypeCode   VARCHAR(1024) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (refInstanceId) REFERENCES DObGoodsReceipt (id) ON DELETE CASCADE
);

CREATE TABLE IObGoodsReceiptItemQuantities
(
    id                 BIGSERIAL     NOT NULL,
    refInstanceId      INT8          NOT NULL,
    creationDate       TIMESTAMP     NOT NULL,
    modifiedDate       TIMESTAMP     NOT NULL,
    creationInfo       VARCHAR(1024) NOT NULL,
    modificationInfo   VARCHAR(1024) NOT NULL,
    receivedQtyUoE     double precision,
    unitOfEntryId      INT8,
    defectsQty         double precision,
    defectsStockType   VARCHAR(1024),
    defectDescription  VARCHAR(1024),
    itemvendorrecordid INT8,
    estimatedCost      DOUBLE PRECISION,
    actualCost         DOUBLE PRECISION,
    PRIMARY KEY (id),
    FOREIGN KEY (refInstanceId) REFERENCES IObGoodsReceiptRecievedItemsData (id) ON DELETE CASCADE
);

CREATE TABLE IObGoodsReceiptItemBatches
(
    id                 BIGSERIAL     NOT NULL,
    refInstanceId      INT8          NOT NULL,
    creationDate       TIMESTAMP     NOT NULL,
    modifiedDate       TIMESTAMP     NOT NULL,
    creationInfo       VARCHAR(1024) NOT NULL,
    modificationInfo   VARCHAR(1024) NOT NULL,
    batchCode          VARCHAR(1024),
    receivedQtyUoE     double precision,
    unitOfEntryId      INT8,
    productionDate     TIMESTAMP,
    expirationDate     TIMESTAMP,
    defectsQty         double precision,
    defectsStockType   VARCHAR(1024),
    defectDescription  VARCHAR(1024),
    itemvendorrecordid INT8,
    estimatedCost      DOUBLE PRECISION,
    actualCost         DOUBLE PRECISION,
    PRIMARY KEY (id),
    FOREIGN KEY (refInstanceId) REFERENCES IObGoodsReceiptRecievedItemsData (id) ON DELETE CASCADE
);


ALTER TABLE DObGoodsReceipt
    ADD CONSTRAINT FK_dobgoodsreceipt_storekeeper FOREIGN KEY (storeKeeperId) REFERENCES StoreKeeper (id) ON DELETE RESTRICT;

ALTER TABLE DObGoodsReceipt
    ADD CONSTRAINT FK_DObGoodsReceipt_store FOREIGN KEY (storehouseId) REFERENCES cobstorehouse (id) ON DELETE RESTRICT;


ALTER TABLE DObGoodsReceipt
    ADD CONSTRAINT FK_DObGoodsReceipt_plant FOREIGN KEY (plantId) REFERENCES cobplant (id) ON DELETE RESTRICT;


ALTER TABLE IObGoodsReceiptItemQuantities
    ADD CONSTRAINT GR_QTY_IVR FOREIGN KEY (itemvendorrecordid) REFERENCES itemvendorrecord (id) ON DELETE RESTRICT
        NOT DEFERRABLE
            INITIALLY IMMEDIATE;

ALTER TABLE IObGoodsReceiptItemBatches
    ADD CONSTRAINT GR_BATCH_IVR FOREIGN KEY (itemvendorrecordid) REFERENCES itemvendorrecord (id) ON DELETE RESTRICT
        NOT DEFERRABLE
            INITIALLY IMMEDIATE;

ALTER TABLE IObGoodsReceiptItemQuantities
    ADD CONSTRAINT IObGoodsReceiptItemQuantities_CObMeasure FOREIGN KEY (unitOfEntryId) REFERENCES cobmeasure (id) ON DELETE RESTRICT
        NOT DEFERRABLE
            INITIALLY IMMEDIATE;

ALTER TABLE IObGoodsReceiptItemBatches
    ADD CONSTRAINT IObGoodsReceiptItemBatches_CObMeasure FOREIGN KEY (unitOfEntryId) REFERENCES cobmeasure (id) ON DELETE RESTRICT
        NOT DEFERRABLE
            INITIALLY IMMEDIATE;

