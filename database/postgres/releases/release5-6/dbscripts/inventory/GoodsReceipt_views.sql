﻿DROP VIEW IF EXISTS IObGoodsReceiptOrdinaryRecievedItemGeneralModel CASCADE;
DROP VIEW IF EXISTS IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel CASCADE;
DROP VIEW IF EXISTS IObGoodsReceiptRecievedItemsGeneralModel CASCADE;
DROP VIEW IF EXISTS DObGoodsReceiptGeneralModel CASCADE;
DROP VIEW IF EXISTS IObGoodsReceiptHeaderGeneralModel CASCADE;
DROP VIEW IF EXISTS DObGoodsReceiptAuthorizationGeneralModel CASCADE;
DROP VIEW IF EXISTS goodsreceiptitemdatageneralmodel CASCADE;

CREATE VIEW DObGoodsReceiptGeneralModel AS
SELECT DObGoodsReceipt.id,
       DObGoodsReceipt.code,
       DObGoodsReceipt.postingDate,
       DObGoodsReceipt.type,
       DObGoodsReceipt.creationDate,
       DObGoodsReceipt.modifiedDate,
       DObGoodsReceipt.creationInfo,
       DObGoodsReceipt.modificationInfo,
       DObGoodsReceipt.currentStates,
       (select name
        from cobenterprise
        where cobenterprise.id = dobgoodsreceipt.storehouseid)                as storehouseName,
       (select code
        from cobenterprise
        where cobenterprise.id = dobgoodsreceipt.storehouseid)                as storehouseCode,
       (select name
        from cobenterprise
        where cobenterprise.id = dobgoodsreceipt.plantid)                     as plantName,
       (select code
        from cobenterprise
        where cobenterprise.id = dobgoodsreceipt.plantid)                     as plantCode,
       IObGoodsReceiptPurchaseOrderData.vendorName,
       masterdata.code                                                        as vendorcode,
       (SELECT purchaseunitname.value
        FROM iobgoodsreceiptpurchaseorderdata
                 JOIN json_each_text(
                iobgoodsreceiptpurchaseorderdata.purchaseunitname) purchaseunitname
                      ON true And purchaseunitname.key = 'en'
                          AND dobgoodsreceipt.id =
                              iobgoodsreceiptpurchaseorderdata.refinstanceid) AS purchaseunitname
FROM DObGoodsReceipt
         LEFT JOIN IObGoodsReceiptPurchaseOrderData
                   ON DObGoodsReceipt.id = IObGoodsReceiptPurchaseOrderData.refInstanceId
         LEFT JOIN masterdata On IObGoodsReceiptPurchaseOrderData.vendorId = masterdata.id and
                                 masterdata.objecttypecode like '2';

CREATE VIEW IObGoodsReceiptHeaderGeneralModel AS
SELECT row_number()
       OVER ()                                                            as id,
       DObGoodsReceipt.code                                               AS goodsReceiptCode,
       DObGoodsReceipt.creationDate,
       DObGoodsReceipt.modifieddate,
       DObGoodsReceipt.creationinfo,
       DObGoodsReceipt.modificationinfo,
       DObGoodsReceipt.postingDate,
       DObGoodsReceipt.type,
       DObGoodsReceipt.currentStates,
       iobgoodsreceiptpurchaseorderdata.purchaseunitname :: json ->> 'en' As PurchaseUnitName,
       PurchaseUnitData.code                                              AS PurchaseUnitCode,
       PlantData.plantCode,
       StorehouseData.storehouseCode,
       StoreKeeperData.storekeeperCode,
       StoreKeeperData.storekeeperName,
       StorehouseData.storehouseName,
       PlantData.plantName
FROM DObGoodsReceipt
         JOIN iobgoodsreceiptpurchaseorderdata
              on iobgoodsreceiptpurchaseorderdata.refinstanceid = dobgoodsreceipt.id
         LEFT JOIN (SELECT id, CObEnterprise.code AS plantCode, CObEnterprise.name AS plantName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '3') AS PlantData
                   ON DObGoodsReceipt.plantId = PlantData.id
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS storehouseCode,
                           CObEnterprise.name AS storehouseName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '4') AS StorehouseData
                   ON DObGoodsReceipt.storehouseId = StorehouseData.id
         LEFT JOIN (SELECT id,
                           StoreKeeper.code AS storekeeperCode,
                           StoreKeeper.name AS storekeeperName
                    FROM StoreKeeper) AS StoreKeeperData
                   ON DObGoodsReceipt.storeKeeperId = StoreKeeperData.id
         LEFT JOIN cobenterprise as PurchaseUnitData
                   ON iobgoodsreceiptpurchaseorderdata.purchaseunitid = PurchaseUnitData.id
                       AND PurchaseUnitData.objectTypeCode = '5';

create view goodsreceiptitemdatageneralmodel as
SELECT *
FROM (
         SELECT DISTINCT ON (itemvendorrecordgeneralmodel.itemcode , itemvendorrecordgeneralmodel.vendorcode ,
             itemvendorrecordgeneralmodel.purchaseunitcode) row_number() OVER ()            AS id,
                                                            itemvendorrecordgeneralmodel.itemcode,
                                                            itemvendorrecordgeneralmodel.itemname,
                                                            itemvendorrecordgeneralmodel.vendorcode,
                                                            itemvendorrecordgeneralmodel.purchaseunitname,
                                                            mobitem.batchmanagementrequired as batched,
                                                            cobmeasure.symbol               as baseunit,
                                                            cobmeasure.code                 as baseUnitCode
         FROM itemvendorrecordgeneralmodel,
              mobitem,
              cobmeasure
         where itemid = mobitem.id
           and cobmeasure.id = mobitem.basicunitofmeasure) itemvendorrecordgeneralmodel
ORDER BY CAST(itemvendorrecordgeneralmodel.itemcode AS INTEGER) DESC;

CREATE VIEW IObGoodsReceiptPurchaseOrderGeneralModel AS
SELECT IObGoodsReceiptPurchaseOrderData.id,
       IObGoodsReceiptPurchaseOrderData.refInstanceId,
       IObGoodsReceiptPurchaseOrderData.deliveryNote,
       DObGoodsReceipt.code AS goodsReceiptCode,
       POData.purchaseOrderCode,
       VendorData.vendorCode,
       IObGoodsReceiptPurchaseOrderData.vendorName,
       PurchaseUnitData.purchaseUnitCode,
       IObGoodsReceiptPurchaseOrderData.purchaseUnitName,
       PurchaseResponsibleData.purchaseResponsibleCode,
       IObGoodsReceiptPurchaseOrderData.purchaseResponsibleName
FROM IObGoodsReceiptPurchaseOrderData
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS purchaseUnitCode,
                           CObEnterprise.name AS purchaseUnitName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '5') AS PurchaseUnitData
                   ON IObGoodsReceiptPurchaseOrderData.purchaseunitid = PurchaseUnitData.id
         LEFT JOIN (SELECT id,
                           CObEnterprise.code AS purchaseResponsibleCode,
                           CObEnterprise.name AS purchaseResponsibleName
                    FROM CObEnterprise
                    WHERE CObEnterprise.objectTypeCode = '6') AS PurchaseResponsibleData
                   ON IObGoodsReceiptPurchaseOrderData.purchaseResponsibleId =
                      PurchaseResponsibleData.id
         LEFT JOIN (SELECT id, masterdata.code AS vendorCode, masterdata.name AS vendorName
                    FROM masterdata
                    WHERE masterdata.objectTypeCode = '2') AS VendorData
                   ON IObGoodsReceiptPurchaseOrderData.vendorId = VendorData.id
         LEFT JOIN (SELECT id, doborder.code AS purchaseOrderCode FROM doborder) AS POData
                   ON IObGoodsReceiptPurchaseOrderData.purchaseOrderId = POData.id
         JOIN DObGoodsReceipt
              ON IObGoodsReceiptPurchaseOrderData.refInstanceId = DObGoodsReceipt.id;


CREATE VIEW IObGoodsReceiptRecievedItemsGeneralModel AS

SELECT IObGoodsReceiptRecievedItemsData.id,
       IObGoodsReceiptRecievedItemsData.refinstanceid AS goodsreceiptinstanceid,
       IObGoodsReceiptRecievedItemsData.stockType,
       IObGoodsReceiptRecievedItemsData.differencereason,
       IObGoodsReceiptRecievedItemsData.objecttypecode,
       mobitemgeneralmodel.code                       as itemcode,
       mobitemgeneralmodel.name                       as itemname,
       DObGoodsReceipt.code                           as goodsReceiptCode,
       cobmeasure.symbol                              as baseunit,
       cobmeasure.code                                as baseunitcode,
       (SELECT quantityInDn
        FROM IObOrderLineDetailsGeneralModel
                 left join iobgoodsreceiptpurchaseordergeneralmodel
                           ON iobgoodsreceiptpurchaseordergeneralmodel.purchaseOrderCode =
                              IObOrderLineDetailsGeneralModel.orderCode
        WHERE iobgoodsreceiptpurchaseordergeneralmodel.goodsReceiptCode = DObGoodsReceipt.code
          AND IObOrderLineDetailsGeneralModel.itemCode = mobitemgeneralmodel.code
       )                                              AS quantityInDn
FROM IObGoodsReceiptRecievedItemsData
         JOIN DObGoodsReceipt ON IObGoodsReceiptRecievedItemsData.refinstanceId = DObGoodsReceipt.id
         left join mobitemgeneralmodel
                   on IObGoodsReceiptRecievedItemsData.itemId = mobitemgeneralmodel.id
         left JOIN cobmeasure ON cobmeasure.code = mobitemgeneralmodel.basicUnitOfMeasureCode;

CREATE VIEW IObGoodsReceiptOrdinaryRecievedItemGeneralModel AS

SELECT IObGoodsReceiptItemQuantities.id,
       IObGoodsReceiptItemQuantities.refinstanceid AS ordinaryitemid,
       IObGoodsReceiptItemQuantities.receivedqtyuoe,
       IObGoodsReceiptItemQuantities.defectsqty,
       IObGoodsReceiptItemQuantities.defectsstocktype,
       IObGoodsReceiptItemQuantities.defectDescription,
       IObGoodsReceiptItemQuantities.estimatedCost,
       IObGoodsReceiptItemQuantities.actualCost,
       IObGoodsReceiptRecievedItemsData.stockType,
       cobmeasure.symbol                           AS unitofentry,
       cobmeasure.code                             AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptItemQuantities.unitofentryid
                     THEN IObGoodsReceiptItemQuantities.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptItemQuantities.receivedqtyuoe
                 END ::numeric,
             3)                                    AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptItemQuantities.unitofentryid
                     THEN (IObGoodsReceiptItemQuantities.receivedqtyuoe +
                           IObGoodsReceiptItemQuantities.defectsQty)
                     * iobalternativeuom.conversionfactor
                 ELSE IObGoodsReceiptItemQuantities.receivedqtyuoe +
                      IObGoodsReceiptItemQuantities.defectsQty
                 END ::numeric,
             3)                                    AS receivedqtyWithDefectsbase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      IObGoodsReceiptItemQuantities.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                    AS baseunitfactor,
       mobitemgeneralmodel.code                    as itemCode,
       mobitemgeneralmodel.name                    as itemName,
       dobgoodsreceipt.code                        AS goodsreceiptcode,
       (SELECT iobgoodsreceiptrecieveditemsgeneralmodel.baseunit
        FROM iobgoodsreceiptrecieveditemsgeneralmodel
        WHERE mobitemgeneralmodel.code::text = iobgoodsreceiptrecieveditemsgeneralmodel.itemcode::text
          AND iobgoodsreceiptrecieveditemsgeneralmodel.goodsreceiptcode =
              dobgoodsreceipt.code)                AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseordergeneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseordergeneralmodel.goodsReceiptCode = DObGoodsReceipt.code
       )                                           AS itemCodeAtVendor

FROM IObGoodsReceiptItemQuantities
         JOIN cobmeasure ON IObGoodsReceiptItemQuantities.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptRecievedItemsData
              ON IObGoodsReceiptRecievedItemsData.id = IObGoodsReceiptItemQuantities.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptRecievedItemsData.itemid = mobitemgeneralmodel.id
         JOIN dobgoodsreceipt ON dobgoodsreceipt.id = iobgoodsreceiptrecieveditemsdata.refinstanceid
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = IObGoodsReceiptItemQuantities.unitofentryid;

CREATE VIEW IObGoodsReceiptBatchedRecievedItemsBatchesGeneralModel AS

SELECT iobgoodsreceiptitembatches.id,
       iobgoodsreceiptitembatches.refinstanceid AS batcheditemid,
       iobgoodsreceiptitembatches.batchcode,
       iobgoodsreceiptitembatches.productiondate,
       iobgoodsreceiptitembatches.expirationdate,
       iobgoodsreceiptitembatches.receivedqtyuoe,
       iobgoodsreceiptitembatches.defectsqty,
       iobgoodsreceiptitembatches.defectsstocktype,
       iobgoodsreceiptitembatches.defectDescription,
       iobgoodsreceiptitembatches.estimatedCost,
       iobgoodsreceiptitembatches.actualCost,
       cobmeasure.symbol                        AS unitofentry,
       cobmeasure.code                          AS unitofentrycode,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobgoodsreceiptitembatches.receivedqtyuoe *
                          iobalternativeuom.conversionfactor
                 ELSE iobgoodsreceiptitembatches.receivedqtyuoe
                 END ::numeric,
             3)                                 AS receivedqtybase,
       round(CASE
                 WHEN iobalternativeuom.alternativeunitofmeasureid =
                      iobgoodsreceiptitembatches.unitofentryid
                     THEN iobalternativeuom.conversionfactor
                 ELSE 1 END ::numeric,
             3)                                 AS baseunitfactor,
       mobitemgeneralmodel.code                 as itemCode,
       dobgoodsreceipt.code                     as goodsReceiptCode,
       (SELECT baseUnit
        FROM IObGoodsReceiptRecievedItemsGeneralModel
        WHERE mobitemgeneralmodel.code =
              IObGoodsReceiptRecievedItemsGeneralModel.itemCode
          AND iobgoodsreceiptrecieveditemsgeneralmodel.goodsreceiptcode =
              dobgoodsreceipt.code)             AS baseUnitSymbol,
       (SELECT ItemVendorRecordGeneralModel.itemVendorCode
        FROM iobgoodsreceiptpurchaseordergeneralmodel
                 LEFT JOIN ItemVendorRecordGeneralModel
                           ON ItemVendorRecordGeneralModel.itemCode = mobitemgeneralmodel.code
                               AND ItemVendorRecordGeneralModel.vendorCode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.vendorCode
                               AND ItemVendorRecordGeneralModel.purchaseunitcode =
                                   iobgoodsreceiptpurchaseordergeneralmodel.purchaseUnitCode
                               AND ItemVendorRecordGeneralModel.uomCode = cobmeasure.code
        where iobgoodsreceiptpurchaseordergeneralmodel.goodsReceiptCode = DObGoodsReceipt.code
       )                                        AS itemCodeAtVendor

FROM iobgoodsreceiptitembatches
         JOIN cobmeasure ON iobgoodsreceiptitembatches.unitofentryid = cobmeasure.id
         JOIN IObGoodsReceiptRecievedItemsData
              ON IObGoodsReceiptRecievedItemsData.id = iobgoodsreceiptitembatches.refinstanceid
         JOIN mobitemgeneralmodel
              ON IObGoodsReceiptRecievedItemsData.itemid = mobitemgeneralmodel.id
         JOIN dobgoodsreceipt ON dobgoodsreceipt.id = iobgoodsreceiptrecieveditemsdata.refinstanceid
         LEFT JOIN iobalternativeuom ON mobitemgeneralmodel.id = iobalternativeuom.refinstanceid
    and iobalternativeuom.alternativeunitofmeasureid = iobgoodsreceiptitembatches.unitofentryid;
CREATE VIEW ItemUnitOfEntries AS
SELECT iobalternativeuom.id,
       cobmeasure.code                    AS measureCode,
       cobmeasure.symbol,
       masterdata.code                    as itemCode,
       (round(conversionFactor)::numeric) As quantityinbaseunit
FROM iobalternativeuom
         JOIN mobitem ON iobalternativeuom.refinstanceid = mobitem.id
         JOIN masterdata ON mobitem.id = masterdata.id
         JOIN cobmeasure ON iobalternativeuom.alternativeunitofmeasureid = cobmeasure.id;

