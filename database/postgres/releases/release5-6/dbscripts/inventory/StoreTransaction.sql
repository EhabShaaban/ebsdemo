drop table if exists TObGoodsIssueStoreTransaction;
drop table if exists TObGoodsReceiptStoreTransaction;
drop table if exists TObStoreTransaction;

CREATE TABLE TObStoreTransaction
(
    id                BIGSERIAL     NOT NULL,
    code              VARCHAR(1024) NOT NULL,
    creationDate      TIMESTAMP     NOT NULL,
    modifiedDate      TIMESTAMP     NOT NULL,
    creationInfo      VARCHAR(1024) NOT NULL,
    modificationInfo  VARCHAR(1024) NOT NULL,
    itemId            INT8          NOT NULL,
    unitOfMeasureId   INT8          NOT NULL,
    quantity          float         NOT NULL,
    cost              double precision,
    stockType         VARCHAR(1024),
    transactionCost   double precision,
    batchNo           VARCHAR(1024),
    remainingQuantity float DEFAULT 0,
    refTransactionId  INT8,
    objectTypeCode    VARCHAR(1024) NOT NULL,
    companyId         INT8          NOT NULL,
    plantId           INT8          NOT NULL,
    storehouseId      INT8          NOT NULL,
    purchaseUnitId    INT8          NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE TObStoreTransaction
    ADD CONSTRAINT FK_TObStoreTransaction_item FOREIGN KEY (itemId) REFERENCES mobitem (id) ON DELETE RESTRICT;

ALTER TABLE TObStoreTransaction
    ADD CONSTRAINT FK_TObStoreTransaction_reftransaction FOREIGN KEY (refTransactionId) REFERENCES TObStoreTransaction (id) ON DELETE RESTRICT;

alter table TObStoreTransaction
    add CONSTRAINT FK_TObStoreTransaction_cobmeasure FOREIGN KEY (unitOfMeasureId) REFERENCES cobmeasure (id) ON update CASCADE ON delete RESTRICT;

ALTER TABLE TObStoreTransaction
    ADD CONSTRAINT FK_TObStoreTransaction_company FOREIGN KEY (companyId) REFERENCES cobcompany (id) ON DELETE RESTRICT;

ALTER TABLE TObStoreTransaction
    ADD CONSTRAINT FK_TObStoreTransaction_plant FOREIGN KEY (plantId) REFERENCES cobplant (id) ON DELETE RESTRICT;

ALTER TABLE TObStoreTransaction
    ADD CONSTRAINT FK_TObStoreTransaction_store FOREIGN KEY (storehouseId) REFERENCES cobstorehouse (id) ON DELETE RESTRICT;

ALTER TABLE TObStoreTransaction
    ADD CONSTRAINT FK_TObStoreTransaction_purchaseUnit FOREIGN KEY (purchaseUnitId) REFERENCES cobenterprise (id) ON DELETE RESTRICT;


CREATE TABLE TObGoodsIssueStoreTransaction
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE TObGoodsIssueStoreTransaction
    ADD CONSTRAINT FK_TObGoodsIssueStoreTransaction_DObStoreTransaction FOREIGN KEY (id) REFERENCES TObStoreTransaction (id) ON DELETE RESTRICT;

ALTER TABLE TObGoodsIssueStoreTransaction
    ADD CONSTRAINT FK_TObGoodsIssueStoreTransaction_DObGoodsIssue FOREIGN KEY (documentReferenceId)
        REFERENCES DObGoodsIssue (id) ON DELETE RESTRICT;



CREATE TABLE TObGoodsReceiptStoreTransaction
(
    id                  INT8 NOT NULL,
    documentReferenceId INT8 NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE TObGoodsReceiptStoreTransaction
    ADD CONSTRAINT FK_TObGoodsReceiptStoreTransaction_DObStoreTransaction FOREIGN KEY (id) REFERENCES TObStoreTransaction (id) ON DELETE RESTRICT;

ALTER TABLE TObGoodsReceiptStoreTransaction
    ADD CONSTRAINT FK_TObGoodsReceiptStoreTransaction_DObGoodsReceipt FOREIGN KEY (documentReferenceId)
        REFERENCES DObGoodsReceipt (id) ON DELETE RESTRICT;


--------------------------------------------------------

