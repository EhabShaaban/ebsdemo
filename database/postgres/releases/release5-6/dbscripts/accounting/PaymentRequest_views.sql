DROP VIEW IF EXISTS DObPaymentRequestGeneralModel CASCADE;
DROP VIEW IF EXISTS IObPaymentRequestPaymentDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObPaymentRequestAccountDetailsGeneralModel CASCADE;
DROP VIEW IF EXISTS IObPaymentRequestPostingDetailsGeneralModel CASCADE;

CREATE VIEW DObPaymentRequestGeneralModel AS
SELECT DObPaymentRequest.id,
       DObPaymentRequest.code,
       DObPaymentRequest.paymentType,
       DObPaymentRequest.creationDate,
       DObPaymentRequest.modifiedDate,
       DObPaymentRequest.creationInfo,
       DObPaymentRequest.modificationInfo,
       DObPaymentRequest.currentStates,
       DObPaymentRequest.paymentForm,
       pd.netAmount                                                as amount,
       DObPaymentRequest.purchaseUnitId,
       (select cobenterprise.name
        from cobenterprise
        where DObPaymentRequest.purchaseUnitId = cobenterprise.id) as purchaseUnitName,
       (select cobenterprise.name:: json ->> 'en'
        from cobenterprise
        where DObPaymentRequest.purchaseUnitId = cobenterprise.id) as purchaseUnitNameEn,
       (select cobenterprise.code
        from cobenterprise
        where DObPaymentRequest.purchaseUnitId = cobenterprise.id) as purchaseUnitCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        where cobcurrency.id = pd.currencyId)                      as currencyISO,
       (SELECT cobcurrency.code
        FROM cobcurrency
        where cobcurrency.id = pd.currencyId)                      as currencyCode
FROM DObPaymentRequest
         full join IObPaymentRequestPaymentDetails pd on DObPaymentRequest.id = pd.refInstanceId;


create view IObPaymentRequestPaymentDetailsGeneralModel as
select pd.id,
       pd.refInstanceId,
       pr.creationDate,
       pr.modifiedDate,
       pr.creationInfo,
       pr.modificationInfo,
       pr.code,
       pr.currentStates,
       md.code                    as businessPartnerCode,
       md.name                    as businessPartnerName,
       pd.duedocumenttype,
       case
           when duedocumenttype = 'INVOICE' then dobinvoice.code
           else doborder.code end as dueDocumentCode,
       pd.netamount,
       cobcurrency.iso            as currencyISO,
       cobcurrency.name           as currencyName,
       cobcurrency.code           as currencyCode,
       pd.description
from iobpaymentrequestpaymentdetails pd
         left join IObPaymentRequestInvoicePaymentDetails ipd on ipd.id = pd.id
         left join IObPaymentRequestDownPaymentPaymentDetails dpd on dpd.id = pd.id
         left join dobinvoice on dobinvoice.id = ipd.duedocumentid
         left join doborder on doborder.id = dpd.duedocumentid
         left join masterdata md on md.id = pd.businessPartnerId
         left join dobpaymentrequest pr on pr.id = pd.refinstanceid
         left join cobcurrency on pd.currencyid = cobcurrency.id;

Create or REPLACE View SubAccount As
  select id as subaccountid, code, null as name, 'PO' as ledger
  from doborder
  union all
  select id as subaccountid, code, name, 'Banks' as ledger
  from cobbank
  union all
  select id as subaccountid, code, name, 'Local_Customer' as ledger
  from masterdata
  where objecttypecode = '3'
  union all
  select id as subaccountid, code, name, 'Taxes' as ledger
  from lobtaxinfo
  union all
  select id as subaccountid, code, name, 'Local_Vendors' as ledger
  from masterdata
  where objecttypecode = '2'
  union all
  select id as subaccountid, code, name, 'Import_Vendors' as ledger
  from masterdata
  where objecttypecode = '2'
   union all
  select id as subaccountid, code, null, 'NotesReceivable' as ledger
  from dobnotesreceivables;


Create View SubAccountGeneralModel As
select row_number() over () as id, SubAccount.*
from SubAccount;

create VIEW IObPaymentRequestAccountDetailsGeneralModel AS
SELECT accountDetails.id,
       accountDetails.refInstanceId,
       pr.code,
       pr.creationDate,
       pr.creationInfo,
       pr.modificationInfo,
       pr.modifieddate,
       cobenterprise.code              AS companyCode,
       cobenterprise.name              AS companyName,
       cobbank.code                    AS bankAccountCode,
       cobbank.name                    AS bankAccountName,
       IObCompanyBankDetails.accountno AS bankAccountNumber,
       hobglaccount.name               AS accountName,
       hobglaccount.code               AS accountCode,
       LeafGLAccount.leadger           AS accountLeadger,
       case
           when LeafGLAccount.leadger is not null then (
               select SubAccountGeneralModel.code
               from SubAccountGeneralModel
               where SubAccountGeneralModel.subaccountid = accountDetails.subAccountId
                 and LeafGLAccount.leadger = SubAccountGeneralModel.ledger
           ) end                       as subAccountCode,
       case
           when LeafGLAccount.leadger is not null then (
               select SubAccountGeneralModel.name
               from SubAccountGeneralModel
               where SubAccountGeneralModel.subaccountid = accountDetails.subAccountId
                 and LeafGLAccount.leadger = SubAccountGeneralModel.ledger
           ) end                       as subAccountName,
       cobcurrency.id                  AS currencyId,
       cobcurrency.iso                 AS currencyIso,
       cobcurrency.code                AS currencyCode,
       accountDetails.price
FROM IObPaymentRequestAccountDetails accountDetails
         left join DObPaymentRequest pr on accountDetails.refinstanceid = pr.id
         left join IObCompanyBankDetails on IObCompanyBankDetails.id = accountDetails.bankAccountId
         left join cobbank on cobbank.id = IObCompanyBankDetails.bankid
         left join cobenterprise on cobenterprise.id = accountDetails.companyid
         left join LeafGLAccount on LeafGLAccount.id = accountDetails.accountId
         left join hobglaccount on hobglaccount.id = LeafGLAccount.id
         left join iobenterprisebasicdata on iobenterprisebasicdata.refinstanceid = accountDetails.companyid
         left join cobcurrency on cobcurrency.id = iobenterprisebasicdata.currencyid;


create VIEW IObPaymentRequestPostingDetailsGeneralModel AS
SELECT postingDetails.id,
       postingDetails.refinstanceid,
       postingDetails.creationDate,
       postingDetails.creationInfo,
       postingDetails.modificationInfo,
       postingDetails.modifieddate,
       paymentRequest.code,
       postingDetails.accountant,
       postingDetails.postingdate,
       postingDetails.duedate,
       exchangeRate.code                                AS exchangeRateCode,
       exchangeRate.firstvalue,
       exchangeRate.secondvalue,
       exchangeRate.firstcurrencyiso,
       exchangeRate.secondcurrencyiso,
       postingDetails.currencyprice,
       (select journalentry.code
        from dobjournalentry journalentry
        where PRjournalentry.id = journalentry.id)      as journalEntryCode
FROM IObPaymentRequestPostingDetails postingDetails
         JOIN DObPaymentRequest paymentRequest on postingDetails.refinstanceid = paymentRequest.id
         JOIN cobexchangerategeneralmodel exchangeRate on postingDetails.exchangerateid = exchangeRate.id
         LEFT JOIN dobpaymentrequestjournalentry PRjournalentry
                   on PRjournalentry.documentreferenceid = paymentRequest.id