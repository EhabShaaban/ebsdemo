DROP VIEW IF EXISTS DObCreditDebitNoteGeneralModel CASCADE;

CREATE VIEW DObCreditDebitNoteGeneralModel AS
SELECT DObCreditDebitNote.id,
       DObCreditDebitNote.code,
       DObCreditDebitNote.postingDate,
       DObCreditDebitNote.type,
       DObCreditDebitNote.creationDate,
       DObCreditDebitNote.modifiedDate,
       DObCreditDebitNote.creationInfo,
       DObCreditDebitNote.modificationInfo,
       DObCreditDebitNote.currentStates,
       DObCreditDebitNote.amount,
       DObCreditDebitNote.currencyPrice,
       CASE (SELECT masterdata.objecttypecode
             FROM masterdata
             where masterdata.id = DObCreditDebitNote.businessPartnerId)
           WHEN '2' THEN 'Vendor'
           ELSE 'Customer'
           END                                                      as businessPartnerType,
       (SELECT masterdata.name
        FROM masterdata
        where masterdata.id = DObCreditDebitNote.businessPartnerId) as businessPartnerName,
       (SELECT masterdata.code
        FROM masterdata
        where masterdata.id = DObCreditDebitNote.businessPartnerId) as businessPartnerCode,
       (SELECT cobenterprise.name
        FROM cobenterprise
        where cobenterprise.id = DObCreditDebitNote.purchaseUnitId
          AND cobenterprise.objecttypecode = '5')                   as purchaseUnitName,
       (SELECT cobenterprise.code
        FROM cobenterprise
        where cobenterprise.id = DObCreditDebitNote.purchaseUnitId
          AND cobenterprise.objecttypecode = '5')                   as purchaseUnitCode,
       (SELECT cobcurrency.iso
        FROM cobcurrency
        where cobcurrency.id = DObCreditDebitNote.currencyId)       as currencyISO,
       (SELECT cobcurrency.code
        FROM cobcurrency
        where cobcurrency.id = DObCreditDebitNote.currencyId)       as currencyCode
FROM DObCreditDebitNote;